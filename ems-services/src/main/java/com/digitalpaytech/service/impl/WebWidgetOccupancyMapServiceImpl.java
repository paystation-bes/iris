package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.dto.GeoLocationEntry;
import com.digitalpaytech.dto.WidgetData;
import com.digitalpaytech.dto.WidgetMapInfo;
import com.digitalpaytech.dto.WidgetMetricInfo;
import com.digitalpaytech.service.WebWidgetHelperService;
import com.digitalpaytech.service.WebWidgetService;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebWidgetUtil;
import com.digitalpaytech.util.WidgetConstants;
import com.digitalpaytech.util.support.WebObjectId;

@Component("webWidgetOccupancyMapService")
@Transactional(propagation = Propagation.REQUIRED)
public class WebWidgetOccupancyMapServiceImpl implements WebWidgetService {
    public static final Map<Integer, String> TIER_TYPES_FIELDS_MAP = new HashMap<Integer, String>(10);
    public static final String LOCATION_ID_FIELD_NAME = "o.LocationId ";
    private static final String WDATA = "wData";
    private static final String WLABEL = "wLabel";
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private WebWidgetHelperService webWidgetHelperService;
    
    static {
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_LOCATION, LOCATION_ID_FIELD_NAME);
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_PARENT_LOCATION, LOCATION_ID_FIELD_NAME);
    }
    
    public final EntityDao getEntityDao() {
        return this.entityDao;
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final WebWidgetHelperService getWebWidgetHelperService() {
        return this.webWidgetHelperService;
    }
    
    public final void setWebWidgetHelperService(final WebWidgetHelperService webWidgetHelperService) {
        this.webWidgetHelperService = webWidgetHelperService;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    @SuppressWarnings({ "PMD.AvoidInstantiatingObjectsInLoops" })
    public final WidgetData getWidgetData(final Widget widget, final UserAccount userAccount) {
        
        @SuppressWarnings("unchecked")
        final List<WidgetMetricInfo> rawWidgetData = createPartialList(widget, userAccount, false);
        @SuppressWarnings("unchecked")
        final List<WidgetMetricInfo> rawWidgetCaseData = createPartialList(widget, userAccount, true);
        final Map<String, WidgetMetricInfo> rawWidgetCaseDataMap = new HashMap<String, WidgetMetricInfo>();
        for (WidgetMetricInfo widgetMetricInfo : rawWidgetCaseData) {
            rawWidgetCaseDataMap.put(widgetMetricInfo.getTier1TypeName(), widgetMetricInfo);
        }
        
        final WidgetMapInfo widgetMapInfo = new WidgetMapInfo();
        
        final List<GeoLocationEntry> geoLocationInfo = new ArrayList<GeoLocationEntry>();
        for (WidgetMetricInfo widgetMetricInfo : rawWidgetData) {
            final GeoLocationEntry geoLocationEntry = new GeoLocationEntry();
            geoLocationEntry.setRandomId(new WebObjectId(Location.class, widgetMetricInfo.getTier1TypeId()));
            geoLocationEntry.setLocationName(widgetMetricInfo.getTier1TypeName());
            geoLocationEntry.setPaidOccupancy(widgetMetricInfo.getWidgetMetricValue().floatValue());
            geoLocationEntry.setPhysicalOccupancy(getPhysicalOccupancy(rawWidgetCaseDataMap, widgetMetricInfo.getTier1TypeName()));
            geoLocationEntry.setJsonString(widgetMetricInfo.getJsonString());
            geoLocationInfo.add(geoLocationEntry);
        }
        
        // add the records left in the physical occupancy map
        for (Entry<String, WidgetMetricInfo> entry : rawWidgetCaseDataMap.entrySet()) {
            final String locationName = entry.getKey();
            final WidgetMetricInfo widgetMetricInfo = entry.getValue();
            final WebObjectId randomId = new WebObjectId(Location.class, widgetMetricInfo.getTier1TypeId());
            if (!containsRandomId(geoLocationInfo, randomId)) {
                final GeoLocationEntry geoLocationEntry = new GeoLocationEntry();
                geoLocationEntry.setRandomId(new WebObjectId(Location.class, widgetMetricInfo.getTier1TypeId()));
                geoLocationEntry.setLocationName(locationName);
                geoLocationEntry.setPaidOccupancy(0f);
                geoLocationEntry.setPhysicalOccupancy(widgetMetricInfo.getWidgetMetricValue().floatValue());
                geoLocationEntry.setJsonString(widgetMetricInfo.getJsonString());
                geoLocationInfo.add(geoLocationEntry);
            }
        }
        
        widgetMapInfo.setGeoLocationInfo(geoLocationInfo);
        
        final WidgetData widgetData = new WidgetData();
        widgetData.setWidgetMapInfo(widgetMapInfo);
        widgetData.setIsZeroValues(false);
        widgetData.setWidgetName(widget.getName());
        widgetData.setChartType(widget.getWidgetChartType().getId());
        widgetData.setRangeTypeId(widget.getWidgetRangeType().getId());
        widgetData.setMetricTypeId(widget.getWidgetMetricType().getId());
        
        return widgetData;
    }
    
    private boolean containsRandomId(final List<GeoLocationEntry> geoLocationInfo, final WebObjectId randomId) {
        for (GeoLocationEntry geoLocationEntry : geoLocationInfo) {
            if (geoLocationEntry != null && geoLocationEntry.getRandomId().equals(randomId)) {
                return true;
            }
        }
        return false;
    }
    
    //Complexity is a common problem with widget query creation methods
    @SuppressWarnings({ "checkstyle:cyclomaticcomplexity", "checkstyle:npathcomplexity" })
    private List<WidgetMetricInfo> createPartialList(final Widget widget, final UserAccount userAccount, final boolean isPhysical) {
        
        final StringBuilder queryStr = new StringBuilder(StandardConstants.CONSTANT_2000);
        
        /* Main SELECT expression */
        this.webWidgetHelperService.appendSelection(queryStr, widget);
        // Specific code to cover closed/open intervals within the day
        queryStr.append(", wLabel.JsonString AS JsonString").append(", IFNULL(SUM(CASE WHEN ").append(isPhysical ? WDATA : WLABEL)
                .append(".TotalNumberOfSpaces = 0 THEN 0").append(" ELSE wData.TotalNumberOfPermits END)/SUM(").append(isPhysical ? WDATA : WLABEL)
                .append(".TotalNumberOfSpaces), 0) * 100 AS WidgetMetricValue ");
                
        /* Main FROM expression */
        queryStr.append("FROM (");
        
        /* Prepare subsetMap */
        final Map<Integer, StringBuilder> subsetMap = this.webWidgetHelperService.createTierSubsetMap(widget);
        
        /* Sub JOIN Temp Table "wData" */
        final String tableType = WidgetConstants.TABLE_RANGE_TYPE_HOUR_STRING;
        
        /* "wData" SELECT expression */
        queryStr.append("SELECT o.CustomerId");
        final boolean isTierTypeTime =
                this.webWidgetHelperService.appendFieldMember(queryStr, widget, TIER_TYPES_FIELDS_MAP, (Map<Integer, String>) null, false);
        queryStr.append(", SUM(o.NoOfPermits) AS TotalNumberOfPermits ");
        queryStr.append(", SUM(o.NumberOfSpaces) AS TotalNumberOfSpaces ");
        
        queryStr.append("FROM ").append(isPhysical ? "Case" : StandardConstants.STRING_EMPTY_STRING).append("Occupancy").append(tableType)
                .append(" o ").append("INNER JOIN Time t ON(");
                
        final int timeRange = widget.getWidgetRangeType().getId();
        
        if (timeRange == WidgetConstants.RANGE_TYPE_NOW && userAccount.getCustomer().isIsParent()) {
            queryStr.append("o.TimeIdGMT = t.Id ");
        } else {
            queryStr.append("o.TimeIdLocal = t.Id ");
        }
        
        this.webWidgetHelperService.calculateRange(widget, userAccount.getCustomer().getId(), queryStr, widget.getWidgetRangeType().getId(),
                                                   widget.getWidgetTierTypeByWidgetTier1Type().getId(), true, true);
                                                   
        queryStr.append(") ");
        
        /* "wData" WHERE expression to speed things up */
        this.webWidgetHelperService.appendDataConditions(queryStr, widget, "o.CustomerId", TIER_TYPES_FIELDS_MAP, subsetMap);
        
        /* "wData" GROUP BY expression */
        queryStr.append("GROUP BY o.CustomerId ");
        this.webWidgetHelperService.appendFieldMember(queryStr, widget, TIER_TYPES_FIELDS_MAP, (Map<Integer, String>) null, true);
        
        queryStr.append(") AS wData ");
        
        /* Sub JOIN Temp Table "wLabel */
        this.webWidgetHelperService.appendLabelTable(queryStr, widget, subsetMap, !isPhysical, WidgetConstants.TABLE_RANGE_TYPE_HOUR, isTierTypeTime);
        
        /* Main GROUP BY */
        this.webWidgetHelperService.appendGrouping(queryStr, widget);
        
        /* Main ORDER BY */
        this.webWidgetHelperService.appendOrdering(queryStr, widget);
        
        SQLQuery query = this.entityDao.createSQLQuery(queryStr.toString());
        
        final boolean isNowAndParent =
                userAccount.getCustomer().isIsParent() && widget.getWidgetRangeType().getId() == WidgetConstants.RANGE_TYPE_NOW;
                
        query = WebWidgetUtil.querySetParameter(this.webWidgetHelperService, widget, query, userAccount, isNowAndParent);
        query = WebWidgetUtil.queryAddScalar(widget, query);
        
        query.setResultTransformer(Transformers.aliasToBean(WidgetMetricInfo.class));
        query.setCacheable(true);
        
        final List<WidgetMetricInfo> responseList = query.list();
        return responseList;
    }
    
    private static Float getPhysicalOccupancy(final Map<String, WidgetMetricInfo> rawWidgetCaseDataMap, final String locationName) {
        final WidgetMetricInfo widgetMetricInfo = rawWidgetCaseDataMap.get(locationName);
        if (widgetMetricInfo == null) {
            return null;
        } else {
            return widgetMetricInfo.getWidgetMetricValue().floatValue();
        }
    }
}
