package com.digitalpaytech.service.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.CustomerMigrationStatusType;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.CustomerMigrationStatusTypeService;

@Service("customerMigrationStatusTypeService")
public class CustomerMigrationStatusTypeServiceImpl implements CustomerMigrationStatusTypeService {

	@Autowired
	private EntityService entityService;
	
	private List<CustomerMigrationStatusType> CustomerMigrationStatusTypes;
	private Map<Byte, CustomerMigrationStatusType> CustomerMigrationStatusTypesMap;
	private Map<String, CustomerMigrationStatusType> CustomerMigrationStatusTypeNamesMap;
	
	
	@Override
	public List<CustomerMigrationStatusType> loadAll() {
		if (CustomerMigrationStatusTypes == null) {
			CustomerMigrationStatusTypes = entityService.loadAll(CustomerMigrationStatusType.class);
		}
		return CustomerMigrationStatusTypes;
	}

	@Override
	public Map<Byte, CustomerMigrationStatusType> getCustomerMigrationStatusTypesMap() {
		if (CustomerMigrationStatusTypes == null) {
			loadAll();
		}
		if (CustomerMigrationStatusTypesMap == null) {
			CustomerMigrationStatusTypesMap = new HashMap<Byte, CustomerMigrationStatusType>(CustomerMigrationStatusTypes.size());
			Iterator<CustomerMigrationStatusType> iter = CustomerMigrationStatusTypes.iterator();
			while (iter.hasNext()) {
				CustomerMigrationStatusType CustomerMigrationStatusType = iter.next();
				CustomerMigrationStatusTypesMap.put(CustomerMigrationStatusType.getId(), CustomerMigrationStatusType);
			}
		}
		return CustomerMigrationStatusTypesMap;
	}
	
	@Override
	public CustomerMigrationStatusType findCustomerMigrationStatusType(String name) {
        if (CustomerMigrationStatusTypes == null) {
            loadAll();
            
            CustomerMigrationStatusTypeNamesMap = new HashMap<String, CustomerMigrationStatusType>(CustomerMigrationStatusTypes.size());
            Iterator<CustomerMigrationStatusType> iter = CustomerMigrationStatusTypes.iterator();
            while (iter.hasNext()) {
                CustomerMigrationStatusType CustomerMigrationStatusType = iter.next();
                CustomerMigrationStatusTypeNamesMap.put(CustomerMigrationStatusType.getName().toLowerCase(), CustomerMigrationStatusType);
            }
        }
        return CustomerMigrationStatusTypeNamesMap.get(name.toLowerCase());
	}

	public void setEntityService(EntityService entityService) {
    	this.entityService = entityService;
    }

	@Override
	public String getText(byte CustomerMigrationStatusTypeId) {
		if (CustomerMigrationStatusTypesMap == null)
			getCustomerMigrationStatusTypesMap();
		
		if (!CustomerMigrationStatusTypesMap.containsKey(CustomerMigrationStatusTypeId))
			return null;
		else
			return CustomerMigrationStatusTypesMap.get(CustomerMigrationStatusTypeId).getName();
	}

    @Override
    public CustomerMigrationStatusType findCustomerMigrationStatusType(byte CustomerMigrationStatusTypeId) {
        if (CustomerMigrationStatusTypesMap == null)
            getCustomerMigrationStatusTypesMap();
        
        if (!CustomerMigrationStatusTypesMap.containsKey(CustomerMigrationStatusTypeId))
            return null;
        else
            return CustomerMigrationStatusTypesMap.get(CustomerMigrationStatusTypeId);
    }
}
