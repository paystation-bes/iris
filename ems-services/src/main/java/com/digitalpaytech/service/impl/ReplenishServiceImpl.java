package com.digitalpaytech.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.service.ReplenishService;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Replenish;

@Service("replenishService")
@Transactional(propagation=Propagation.REQUIRED)
public class ReplenishServiceImpl implements ReplenishService {

    @Autowired
    private EntityDao entityDao;

    
    @Override
    public void processReplenish(Replenish replenish) {
        entityDao.save(replenish);
    }
    
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
}
