package com.digitalpaytech.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.dto.WidgetData;
import com.digitalpaytech.dto.WidgetMetricInfo;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.WebWidgetHelperService;
import com.digitalpaytech.service.WebWidgetService;
import com.digitalpaytech.util.WebWidgetUtil;
import com.digitalpaytech.util.WidgetConstants;

@Component("webWidgetUtilizationService")
@Transactional(propagation = Propagation.REQUIRED)
public class WebWidgetUtilizationServiceImpl implements WebWidgetService {
    public static final HashMap<Integer, String> TIER_TYPES_FIELDS_MAP = new HashMap<Integer, String>(10);
    static {
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_HOUR, "t.Date, t.DayOfYear, t.HourOfDay, t.Id ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_DAY, "t.Date, t.DayOfYear ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_MONTH, "t.Year, t.Month, t.MonthNameAbbrev ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_LOCATION, "u.LocationId ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_PARENT_LOCATION, "u.LocationId ");
    }
    
    @Autowired
    private EntityDao entityDao;
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    @Autowired
    private WebWidgetHelperService webWidgetHelperService;
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public void setEmsPropertiesService(EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public void setWebWidgetHelperService(WebWidgetHelperService webWidgetHelperService) {
        this.webWidgetHelperService = webWidgetHelperService;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public WidgetData getWidgetData(Widget widget, UserAccount userAccount) {
        
        StringBuilder queryStr = new StringBuilder();
        
        String queryLimit = emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_WIDGET_DATA_QUERY_ROW_LIMIT,
                                                                  EmsPropertiesService.MAX_DEFAULT_WIDGET_DATA_QUERY_ROW_LIMIT);
        
        int tier1Id = widget.getWidgetTierTypeByWidgetTier1Type().getId();
        int timeRange = widget.getWidgetRangeType().getId();
        int tableRangeType = 0;
        switch (timeRange) {
            case WidgetConstants.RANGE_TYPE_YESTERDAY:
            case WidgetConstants.RANGE_TYPE_LAST_7DAYS:
            case WidgetConstants.RANGE_TYPE_LAST_30DAYS:
                //            case WidgetConstants.RANGE_TYPE_THIS_WEEK:
            case WidgetConstants.RANGE_TYPE_LAST_WEEK:
                tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_DAY;
                break;
            //            case WidgetConstants.RANGE_TYPE_THIS_MONTH:
            case WidgetConstants.RANGE_TYPE_LAST_MONTH:
                if (tier1Id == WidgetConstants.TIER_TYPE_DAY) {
                    tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_DAY;
                } else {
                    tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_MONTH;
                }
                break;
            case WidgetConstants.RANGE_TYPE_LAST_12MONTHS:
                //            case WidgetConstants.RANGE_TYPE_THIS_YEAR:
            case WidgetConstants.RANGE_TYPE_LAST_YEAR:
            case WidgetConstants.RANGE_TYPE_YEAR_TO_DATE:
                tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_MONTH;
                break;
        }
        
        /* Main SELECT expression */
        this.webWidgetHelperService.appendSelection(queryStr, widget);
        // Specific code to cover closed/open intervals within the day
        if (tableRangeType == WidgetConstants.TABLE_RANGE_TYPE_HOUR) {
            queryStr.append(", IFNULL(SUM(CASE WHEN wLabel.MaxUtilizationMinutes = 0 THEN 0 ELSE wData.TotalMinutesPurchased END)/SUM(wLabel.MaxUtilizationMinutes), 0) AS WidgetMetricValue ");
        } else {
            queryStr.append(", IFNULL(SUM(wData.TotalMinutesPurchased)/SUM(wLabel.MaxUtilizationMinutes), 0) * 100 AS WidgetMetricValue ");
        }
        
        /* Main FROM expression */
        queryStr.append("FROM (");
        
        /* Prepare subsetMap */
        Map<Integer, StringBuilder> subsetMap = this.webWidgetHelperService.createTierSubsetMap(widget);
        
        /* Sub JOIN Temp Table "wData" */
        String tableType = null;
        if (tableRangeType == WidgetConstants.TABLE_RANGE_TYPE_HOUR) {
            tableType = WidgetConstants.TABLE_RANGE_TYPE_HOUR_STRING;
        } else if (tableRangeType == WidgetConstants.TABLE_RANGE_TYPE_DAY) {
            tableType = WidgetConstants.TABLE_RANGE_TYPE_DAY_STRING;
        } else if (tableRangeType == WidgetConstants.TABLE_RANGE_TYPE_MONTH) {
            tableType = WidgetConstants.TABLE_RANGE_TYPE_MONTH_STRING;
        }
        
        /* "wData" SELECT expression */
        queryStr.append("SELECT u.CustomerId");
        boolean isTierTypeTime = this.webWidgetHelperService.appendFieldMember(queryStr, widget, TIER_TYPES_FIELDS_MAP, (Map<Integer, String>) null, false);
        queryStr.append(", SUM(u.TotalMinutesPurchased) AS TotalMinutesPurchased ");
        //        queryStr.append(", SUM(IF(u.MaxUtilizationMinutes = 0, 100, u.MaxUtilizationMinutes)) AS TotalMaxUtilizationMinutes ");
        
        /* "wData" FROM expression */
        queryStr.append("FROM Utilization");
        queryStr.append(tableType);
        queryStr.append(" u ");
        
        queryStr.append("INNER JOIN Time t ON(u.TimeIdLocal = t.Id ");
        
        this.webWidgetHelperService.calculateRange(widget, userAccount.getCustomer().getId(), queryStr, widget.getWidgetRangeType().getId(), widget
                .getWidgetTierTypeByWidgetTier1Type().getId(), true, true);
        
        queryStr.append(") ");
        
        /* "wData" WHERE expression to speed things up */
        this.webWidgetHelperService.appendDataConditions(queryStr, widget, "u.CustomerId", TIER_TYPES_FIELDS_MAP, subsetMap);
        
        /* "wData" GROUP BY expression */
        queryStr.append("GROUP BY u.CustomerId");
        this.webWidgetHelperService.appendFieldMember(queryStr, widget, TIER_TYPES_FIELDS_MAP, (Map<Integer, String>) null, true);
        
        queryStr.append(") AS wData ");
        
        /* Sub JOIN Temp Table "wLabel */
        this.webWidgetHelperService.appendLabelTable(queryStr, widget, subsetMap, true, tableRangeType, isTierTypeTime);
        
        /* Main GROUP BY */
        this.webWidgetHelperService.appendGrouping(queryStr, widget);
        
        /* Main ORDER BY */
        this.webWidgetHelperService.appendOrdering(queryStr, widget);
        
        /* Main LIMIT */
        this.webWidgetHelperService.appendLimit(queryStr, widget, queryLimit);
        
        SQLQuery query = entityDao.createSQLQuery(queryStr.toString());
        
        query = WebWidgetUtil.querySetParameter(webWidgetHelperService, widget, query, userAccount, false);
        query = WebWidgetUtil.queryAddScalar(widget, query);
        
        query.setResultTransformer(Transformers.aliasToBean(WidgetMetricInfo.class));
        query.setCacheable(true);
        
        WidgetData widgetData = new WidgetData();
        
        @SuppressWarnings("unchecked")
        List<WidgetMetricInfo> rawWidgetData = query.list();
        
        widgetData = webWidgetHelperService.convertData(widget, rawWidgetData, userAccount, queryLimit);
        widgetData.setTrendValue(getWidgetTrendValues(widget, widgetData, userAccount));
        
        return widgetData;
    }
    
    protected List<String> getWidgetTrendValues(Widget widget, WidgetData widgetData, UserAccount userAccount) {
        
        List<String> trendValue = new ArrayList<String>();
        
        if (!(widget.getTrendAmount() == null || widget.getTrendAmount() == 0)) {
            Float dollarValue = new BigDecimal(widget.getTrendAmount()).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP).floatValue();
            trendValue.add(dollarValue.toString());
            return trendValue;
        }
        
        return null;
    }
}
