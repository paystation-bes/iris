package com.digitalpaytech.service.impl;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.ActivePermit;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.service.ActivePermitService;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.util.StandardConstants;

@Service("activePermitService")
@Transactional(propagation = Propagation.REQUIRED)
public class ActivePermitServiceImpl implements ActivePermitService {
    
    private static final String SQL_STALL_STATUS_SELECT = "SELECT ap.SpaceNumber AS SpaceNumber, ap.PaystationSettingName AS PaystationSettingName,"
                                                          + " MAX(ap.PermitBeginGMT) as PermitBeginGmt, MAX(ap.PermitExpireGMT) as PermitExpireGmt";
    private static final String SQL_STALL_STATUS_FROM = " FROM ActivePermit ap WHERE ap.CustomerId = :customerId";
    
    private static final String SQL_WRAPPER_BEGIN = "SELECT ap3.SpaceNumber AS SpaceNumber, ap3.PaystationSettingName AS PaystationSettingName,"
                                                    + " ap3.PermitBeginGMT AS PermitBeginGmt, ap3.PermitExpireGMT AS PermitExpireGmt"
                                                    + " FROM ActivePermit ap3 INNER JOIN Location l ON ap3.CustomerId = l.CustomerId"
                                                    + " AND l.Id = ap3.PermitLocationId"
                                                    + " INNER JOIN (";
    
    private static final String SQL_WRAPPER_REST = " AND ap3.CustomerId = apres.CustomerId";
    private static final String SQL_WRAPPER_END =
            ") apres ON  ap3.SpaceNumber = apres.SpaceNumber AND ap3.permitExpireGMT = apres.PermitExpireGMT" + SQL_WRAPPER_REST;
    private static final String SQL_WRAPPER_END_MOST_RECENT =
            ") apres ON  ap3.SpaceNumber = apres.SpaceNumber AND ap3.permitBeginGmt = apres.PermitBeginGmt" + SQL_WRAPPER_REST;
    private static final String SQL_GET_SPACE_REST = " ap.SpaceNumber AS SpaceNumber, ap.CustomerId AS CustomerId";
    private static final String SQL_GET_SPACE_INFO_CORE = "SELECT MAX(ap.PermitExpireGMT) AS PermitExpireGMT," + SQL_GET_SPACE_REST;
    private static final String SQL_GET_SPACE_INFO_CORE_MOST_RECENT = "SELECT MAX(ap.PermitBeginGMT) AS PermitBeginGMT," + SQL_GET_SPACE_REST;
    
    private static final String SQL_WHERE_ADD_PAYSTATIONSETTING = " AND ap.PaystationSettingName = :paystationSettingName";
    private static final String SQL_WHERE_ADD_LOCATION = " AND ap.PermitLocationId = :locationId";
    private static final String SQL_WHERE_ADD_POSLIST = " AND ap.PointOfSaleId IN (:posList)";
    private static final String SQL_WHERE_CUSTOMERID = " WHERE ap3.CustomerId = :customerId ";
    private static final String SQL_OUTER_WHERE_ADD_SPACENUMBER_BETWEEN = " AND ap3.SpaceNumber BETWEEN :startSpace AND :endSpace";
    private static final String SQL_WHERE_ADD_SPACENUMBER_BETWEEN = " AND ap.SpaceNumber BETWEEN :startSpace AND :endSpace";
    
    private static final String SQL_GROUP_BY_SPACE = " GROUP BY ap.SpaceNumber";
    private static final String SQL_ORDER_BY_SPACE = " ORDER BY SpaceNumber ASC";
    
    private static final String SQL_GET_LOTS_FOR_STALL_REPORT_FROM_LAST_14_DAYS =
            "SELECT DISTINCT ap.PaystationSettingName FROM ActivePermit ap " + "WHERE ap.CustomerId = :customerId "
                                                                                  + "AND ap.PermitExpireGMT>=DATE_ADD(UTC_TIMESTAMP(), INTERVAL -14 DAY)"
                                                                                  + "ORDER BY ap.PaystationSettingName";
    
    private static final String SQL_GET_ACTIVEPERMIT_BY_CUSTOMER_AND_SPACERANGE =
            SQL_WRAPPER_BEGIN + SQL_GET_SPACE_INFO_CORE + SQL_STALL_STATUS_FROM + SQL_WHERE_ADD_SPACENUMBER_BETWEEN + SQL_GROUP_BY_SPACE
                                                                                  + SQL_WRAPPER_END + SQL_ORDER_BY_SPACE;
    
    private static final String SQL_GET_ACTIVEPERMIT_BY_CUSTOMER_AND_SPACERANGE_MOST_RECENT =
            SQL_WRAPPER_BEGIN + SQL_GET_SPACE_INFO_CORE_MOST_RECENT + SQL_STALL_STATUS_FROM + SQL_WHERE_ADD_SPACENUMBER_BETWEEN + SQL_GROUP_BY_SPACE
                                                                                              + SQL_WRAPPER_END_MOST_RECENT + SQL_WHERE_CUSTOMERID
                                                                                              + SQL_OUTER_WHERE_ADD_SPACENUMBER_BETWEEN
                                                                                              + SQL_ORDER_BY_SPACE;
    
    private static final String SQL_GET_ACTIVEPERMIT_BY_LOCATION_AND_SPACERANGE =
            SQL_STALL_STATUS_SELECT + SQL_STALL_STATUS_FROM + SQL_WHERE_ADD_SPACENUMBER_BETWEEN + SQL_WHERE_ADD_LOCATION + SQL_GROUP_BY_SPACE
                                                                                  + SQL_ORDER_BY_SPACE;
    
    private static final String SQL_GET_ACTIVEPERMIT_BY_PAYSTATIONSETTING_AND_SPACERANGE =
            SQL_WRAPPER_BEGIN + SQL_GET_SPACE_INFO_CORE + SQL_STALL_STATUS_FROM + SQL_WHERE_ADD_SPACENUMBER_BETWEEN + SQL_WHERE_ADD_PAYSTATIONSETTING
                                                                                           + SQL_GROUP_BY_SPACE + SQL_WRAPPER_END
                                                                                           + SQL_ORDER_BY_SPACE;
    
    private static final String SQL_GET_ACTIVEPERMIT_BY_PAYSTATIONSETTING_AND_SPACERANGE_MOST_RECENT =
            SQL_WRAPPER_BEGIN + SQL_GET_SPACE_INFO_CORE_MOST_RECENT + SQL_STALL_STATUS_FROM + SQL_WHERE_ADD_SPACENUMBER_BETWEEN
                                                                                                       + SQL_WHERE_ADD_PAYSTATIONSETTING
                                                                                                       + SQL_GROUP_BY_SPACE
                                                                                                       + SQL_WRAPPER_END_MOST_RECENT
                                                                                                       + SQL_ORDER_BY_SPACE;
    
    private static final String SQL_GET_ACTIVEPERMIT_BY_POSLIST_AND_SPACERANGE =
            SQL_WRAPPER_BEGIN + SQL_GET_SPACE_INFO_CORE + SQL_STALL_STATUS_FROM + SQL_WHERE_ADD_SPACENUMBER_BETWEEN + SQL_WHERE_ADD_POSLIST
                                                                                 + SQL_GROUP_BY_SPACE + SQL_WRAPPER_END + SQL_ORDER_BY_SPACE;
    
    private static final String SQL_GET_ACTIVEPERMIT_BY_POSLIST_AND_SPACERANGE_MOST_RECENT =
            SQL_WRAPPER_BEGIN + SQL_GET_SPACE_INFO_CORE_MOST_RECENT + SQL_STALL_STATUS_FROM + SQL_WHERE_ADD_SPACENUMBER_BETWEEN
                                                                                             + SQL_WHERE_ADD_POSLIST + SQL_GROUP_BY_SPACE
                                                                                             + SQL_WRAPPER_END_MOST_RECENT + SQL_ORDER_BY_SPACE;
    
    @Autowired
    private EntityDao entityDao;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final ActivePermit getActivePermitByCustomerAndLicencePlate(final int customerId, final String licencePlateNumber,
        final int querySpaceBy) {
        final Calendar limitDate = Calendar.getInstance();
        limitDate.add(Calendar.DAY_OF_YEAR, -StandardConstants.WEEKS_IN_DAYS_2);
        
        final String[] parameters = { "customerId", "licencePlateNumber", "limitDate" };
        final Object[] values = { customerId, licencePlateNumber, limitDate.getTime() };
        
        List<ActivePermit> activePermitList = null;
        if (querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT
            || querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_EXPIRY_TIME_OF_MOST_RECENT_PERMIT) {
            activePermitList =
                    this.entityDao.findByNamedQueryAndNamedParam("ActivePermit.findLatestExpiredPermitByCustomerAndLicencePlate", parameters, values);
            
        } else {
            activePermitList = this.entityDao
                    .findByNamedQueryAndNamedParam("ActivePermit.findLatestExpiredPermitByCustomerAndLicencePlateLatestExpiry", parameters, values);
        }
        if (activePermitList != null && !activePermitList.isEmpty()) {
            return activePermitList.get(0);
        } else {
            return null;
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final ActivePermit getActivePermitByLocationAndLicencePlate(final int locationId, final String licencePlateNumber) {
        final Calendar limitDate = Calendar.getInstance();
        limitDate.add(Calendar.DAY_OF_YEAR, -StandardConstants.WEEKS_IN_DAYS_2);
        
        final String[] parameters = { "locationId", "licencePlateNumber", "limitDate" };
        final Object[] values = { locationId, licencePlateNumber, limitDate.getTime() };
        
        final List<ActivePermit> activePermitList =
                this.entityDao.findByNamedQueryAndNamedParam("ActivePermit.findLatestExpiredPermitByLocationAndLicencePlate", parameters, values);
        
        if (activePermitList != null && !activePermitList.isEmpty()) {
            return activePermitList.get(0);
        } else {
            return null;
        }
    }
    
    /**
     * 1st, get list of PointOfSale objects by locationId
     * 2nd, create a sql query in ActivePermitService to return 'LicencePlateNumber' & 'PermitExpireGMT' in ActivePermit objects.
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<ActivePermit> getValidByLocationId(final Integer locationId, final Date reportDate, final int startPosition,
        final int recordsReturned) {
        
        final SQLQuery q = this.entityDao.createSQLQuery(createValidActivePermitsSql());
        q.addScalar("LicencePlateNumber", new StringType());
        q.addScalar("PermitExpireGmt", new TimestampType());
        q.setParameter("locationId", locationId);
        q.setParameter("reportDate", reportDate);
        q.setFirstResult(startPosition);
        q.setMaxResults(recordsReturned);
        q.setResultTransformer(Transformers.aliasToBean(ActivePermit.class));
        final List<ActivePermit> activePermitList = q.list();
        return activePermitList;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final int getValidByLocationIdTotalCount(final Integer locationId, final Date reportDate) {
        final SQLQuery q = this.entityDao.createSQLQuery(createValidActivePermitsSqlTotalCount());
        q.setParameter("locationId", locationId);
        q.setParameter("reportDate", reportDate);
        final List<BigInteger> list = q.list();
        if (list != null) {
            return list.get(0).intValue();
        }
        return -1;
    }
    
    // TODO Might need optimization
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<ActivePermit> getValidByCustomerId(final Integer customerId, final Date reportDate, final int startPosition,
        final int recordsReturned, final int querySpaceBy) {
        String query = null;
        if (querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT
            || querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_EXPIRY_TIME_OF_MOST_RECENT_PERMIT) {
            query = createValidActivePermitsSqlForCustomerMostRecent();
        } else {
            query = createValidActivePermitsSqlForCustomer();
        }
        final SQLQuery q = this.entityDao.createSQLQuery(query);
        
        q.addScalar("LicencePlateNumber", new StringType());
        q.addScalar("PermitExpireGmt", new TimestampType());
        q.setParameter("customerId", customerId);
        q.setParameter("reportDate", reportDate);
        q.setFirstResult(startPosition);
        q.setMaxResults(recordsReturned);
        q.setResultTransformer(Transformers.aliasToBean(ActivePermit.class));
        final List<ActivePermit> activePermitList = q.list();
        return activePermitList;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final int getValidByCustomerIdTotalCount(final Integer customerId, final Date reportDate, final int querySpaceBy) {
        String query = null;
        if (querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT
            || querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_EXPIRY_TIME_OF_MOST_RECENT_PERMIT) {
            query = createValidActivePermitsSqlForCustomerMostRecentTotalCount();
        } else {
            query = createValidActivePermitsSqlForCustomerTotalCount();
        }
        final SQLQuery q = this.entityDao.createSQLQuery(query);
        q.setParameter("customerId", customerId);
        q.setParameter("reportDate", reportDate);
        final List<BigInteger> list = q.list();
        if (list != null) {
            return list.get(0).intValue();
        }
        return -1;
    }
    
    /*
     * e.g.
     * SELECT ap.LicencePlateNumber AS LicencePlateNumber , MAX(ap.PermitExpireGMT) AS PermitExpireGmt
     * FROM ActivePermit ap
     * WHERE ap.PermitLocationId = 15
     * AND ap.LicencePlateNumber IS NOT NULL AND ap.LicencePlateNumber != '' AND ap.LicencePlateNumber != '1'
     * AND ap.PermitBeginGMT <= '2013-11-28 12:40:33' AND ap.PermitExpireGMT > '2013-11-28 12:40:33'
     * GROUP BY ap.LicencePlateNumber
     * ORDER BY ap.LicencePlateNumber
     */
    private static String createValidActivePermitsSql() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("SELECT ap.LicencePlateNumber AS LicencePlateNumber , MAX(ap.PermitExpireGMT) AS PermitExpireGmt ");
        bdr.append(createValidActivePermitsSqlBody());
        bdr.append("GROUP BY ap.LicencePlateNumber ORDER BY ap.LicencePlateNumber ");
        return bdr.toString();
    }
    
    /*
     * e.g.
     * SELECT COUNT(*)
     * FROM ActivePermit ap
     * WHERE ap.PermitLocationId = 15
     * AND ap.LicencePlateNumber IS NOT NULL AND ap.LicencePlateNumber != '' AND ap.LicencePlateNumber != '1'
     * AND ap.PermitBeginGMT <= '2013-11-28 12:40:33' AND ap.PermitExpireGMT > '2013-11-28 12:40:33'
     */
    private static String createValidActivePermitsSqlTotalCount() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("SELECT COUNT(*) ");
        bdr.append(createValidActivePermitsSqlBody());
        return bdr.toString();
    }
    
    private static String createValidActivePermitsSqlBody() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("FROM ActivePermit ap WHERE ap.PermitLocationId = :locationId ");
        bdr.append("AND ap.LicencePlateNumber IS NOT NULL AND ap.LicencePlateNumber != ''  ");
        bdr.append("AND ap.PermitBeginGMT <=:reportDate AND ap.PermitExpireGMT > :reportDate ");
        return bdr.toString();
    }
    
    /*
     * e.g.
     * SELECT ap.LicencePlateNumber AS LicencePlateNumber , MAX(ap.PermitExpireGMT) AS PermitExpireGmt
     * FROM ActivePermit ap
     * WHERE ap.CustomerId = 3
     * AND ap.LicencePlateNumber IS NOT NULL AND ap.LicencePlateNumber != '' AND ap.LicencePlateNumber != '1'
     * AND ap.PermitBeginGMT <= '2014-11-28 12:40:33' AND ap.PermitExpireGMT > '2013-11-28 12:40:33'
     * GROUP BY ap.LicencePlateNumber
     * ORDER BY ap.LicencePlateNumber
     */
    private static String createValidActivePermitsSqlForCustomer() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("SELECT ap.LicencePlateNumber AS LicencePlateNumber , MAX(ap.PermitExpireGMT) AS PermitExpireGmt ");
        bdr.append(createValidActivePermitsSqlBodyForCustomer());
        bdr.append("GROUP BY ap.LicencePlateNumber ORDER BY ap.LicencePlateNumber ");
        return bdr.toString();
    }
    
    /*
     * e.g.
     * SELECT COUNT(*) FROM ActivePermit ap
     * WHERE ap.CustomerId = 6d AND ap.LicencePlateNumber IS NOT NULL AND ap.LicencePlateNumber != '' AND ap.LicencePlateNumber != '1'
     * AND ap.PermitBeginGMT <= '2014-10-24 15:45:55'
     * AND ap.PermitExpireGMT > '2014-10-24 15:45:55'
     */
    private static String createValidActivePermitsSqlForCustomerTotalCount() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("SELECT COUNT(*) ");
        bdr.append(createValidActivePermitsSqlBodyForCustomer());
        return bdr.toString();
    }
    
    private static String createValidActivePermitsSqlBodyForCustomer() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("FROM ActivePermit ap WHERE ap.CustomerId = :customerId ");
        bdr.append("AND ap.LicencePlateNumber IS NOT NULL AND ap.LicencePlateNumber != ''  ");
        bdr.append("AND ap.PermitBeginGMT <=:reportDate AND ap.PermitExpireGMT > :reportDate ");
        return bdr.toString();
    }
    
    /*
     * SELECT ap3.LicencePlateNumber AS LicencePlateNumber, ap3.PermitExpireGMT AS PermitExpireGmt FROM ActivePermit ap3
     * INNER JOIN
     * (SELECT ap.CustomerId AS CustomerId, ap.LicencePlateNumber AS LicencePlateNumber, MAX(ap.PermitBeginGMT) AS PermitBeginGmt
     * FROM ActivePermit ap WHERE ap.CustomerId = 6
     * AND ap.LicencePlateNumber IS NOT NULL AND ap.LicencePlateNumber != '' AND ap.LicencePlateNumber != '1'
     * AND ap.PermitBeginGMT <= '2014-10-24 15:45:55'
     * AND ap.PermitExpireGMT > '2014-10-24 15:45:55'
     * GROUP BY ap.LicencePlateNumber ORDER BY ap.LicencePlateNumber)
     * apres ON ap3.LicencePlateNumber = apres.LicencePlateNumber
     * AND ap3.permitBeginGMT = apres.PermitBeginGmt AND ap3.CustomerId = apres.CustomerId
     */
    private static String createValidActivePermitsSqlForCustomerMostRecent() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("SELECT ap3.LicencePlateNumber AS LicencePlateNumber, ap3.PermitExpireGMT AS PermitExpireGmt ");
        bdr.append(createValidActivePermitsSqlBodyForCustomerMostRecent());
        return bdr.toString();
    }
    
    /*
     * SELECT count(*) FROM ActivePermit ap3
     * INNER JOIN
     * (SELECT ap.CustomerId AS CustomerId, ap.LicencePlateNumber AS LicencePlateNumber, MAX(ap.PermitBeginGMT) AS PermitBeginGmt
     * FROM ActivePermit ap WHERE ap.CustomerId = 6
     * AND ap.LicencePlateNumber IS NOT NULL AND ap.LicencePlateNumber != '' AND ap.LicencePlateNumber != '1'
     * AND ap.PermitBeginGMT <= '2014-10-24 15:45:55'
     * AND ap.PermitExpireGMT > '2014-10-24 15:45:55'
     * GROUP BY ap.LicencePlateNumber ORDER BY ap.LicencePlateNumber)
     * apres ON ap3.LicencePlateNumber = apres.LicencePlateNumber
     * AND ap3.permitBeginGMT = apres.PermitBeginGmt AND ap3.CustomerId = apres.CustomerId
     */
    private static String createValidActivePermitsSqlForCustomerMostRecentTotalCount() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("SELECT COUNT(*) ");
        bdr.append(createValidActivePermitsSqlBodyForCustomerMostRecent());
        return bdr.toString();
    }
    
    private static String createValidActivePermitsSqlBodyForCustomerMostRecent() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("FROM ActivePermit ap3 INNER JOIN (");
        bdr.append("SELECT ap.CustomerId AS CustomerId, ap.LicencePlateNumber AS LicencePlateNumber, MAX(ap.PermitBeginGMT) AS PermitBeginGmt ");
        bdr.append("FROM ActivePermit ap WHERE ap.CustomerId = :customerId ");
        bdr.append("AND ap.LicencePlateNumber IS NOT NULL AND ap.LicencePlateNumber != '' ");
        bdr.append("AND ap.PermitBeginGMT <=:reportDate AND ap.PermitExpireGMT > :reportDate ");
        bdr.append("GROUP BY ap.LicencePlateNumber ORDER BY ap.LicencePlateNumber) apres ");
        bdr.append("ON  ap3.LicencePlateNumber = apres.LicencePlateNumber AND ap3.permitBeginGMT = apres.PermitBeginGmt AND ap3.CustomerId = apres.CustomerId ");
        return bdr.toString();
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<ActivePermit> getPaystationSettingsFromLast14Days(final int customerId) {
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_GET_LOTS_FOR_STALL_REPORT_FROM_LAST_14_DAYS);
        
        q.addScalar("PaystationSettingName", new StringType());
        q.setParameter("customerId", customerId);
        q.setResultTransformer(Transformers.aliasToBean(ActivePermit.class));
        return q.list();
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<ActivePermit> getActivePermitByCustomerAndSpaceRange(final int customerId, final int startSpace, final int endSpace,
        final int querySpaceBy) {
        String query = null;
        if (querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT
            || querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_EXPIRY_TIME_OF_MOST_RECENT_PERMIT) {
            query = SQL_GET_ACTIVEPERMIT_BY_CUSTOMER_AND_SPACERANGE_MOST_RECENT;
        } else {
            query = SQL_GET_ACTIVEPERMIT_BY_CUSTOMER_AND_SPACERANGE;
        }
        final SQLQuery q = this.entityDao.createSQLQuery(query);
        q.addScalar("SpaceNumber", new IntegerType());
        q.addScalar("PaystationSettingName", new StringType());
        q.addScalar("PermitBeginGmt", new TimestampType());
        q.addScalar("PermitExpireGmt", new TimestampType());
        q.setParameter("customerId", customerId);
        q.setParameter("startSpace", startSpace);
        q.setParameter("endSpace", endSpace);
        q.setResultTransformer(Transformers.aliasToBean(ActivePermit.class));
        return q.list();
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<ActivePermit> getActivePermitByLocationAndSpaceRange(final int customerId, final int locationId, final int startSpace,
        final int endSpace) {
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_GET_ACTIVEPERMIT_BY_LOCATION_AND_SPACERANGE);
        q.addScalar("SpaceNumber", new IntegerType());
        q.addScalar("PaystationSettingName", new StringType());
        q.addScalar("PermitBeginGmt", new TimestampType());
        q.addScalar("PermitExpireGmt", new TimestampType());
        q.setParameter("customerId", customerId);
        q.setParameter("locationId", locationId);
        q.setParameter("startSpace", startSpace);
        q.setParameter("endSpace", endSpace);
        q.setResultTransformer(Transformers.aliasToBean(ActivePermit.class));
        return q.list();
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<ActivePermit> getActivePermitByPaystationSettingAndSpaceRange(final int customerId, final String paystationSettingName,
        final int startSpace, final int endSpace, final int querySpaceBy) {
        String query = null;
        if (querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT
            || querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_EXPIRY_TIME_OF_MOST_RECENT_PERMIT) {
            query = SQL_GET_ACTIVEPERMIT_BY_PAYSTATIONSETTING_AND_SPACERANGE_MOST_RECENT;
        } else {
            query = SQL_GET_ACTIVEPERMIT_BY_PAYSTATIONSETTING_AND_SPACERANGE;
        }
        final SQLQuery q = this.entityDao.createSQLQuery(query);
        q.addScalar("SpaceNumber", new IntegerType());
        q.addScalar("PaystationSettingName", new StringType());
        q.addScalar("PermitBeginGmt", new TimestampType());
        q.addScalar("PermitExpireGmt", new TimestampType());
        q.setParameter("customerId", customerId);
        q.setParameter("paystationSettingName", paystationSettingName);
        q.setParameter("startSpace", startSpace);
        q.setParameter("endSpace", endSpace);
        q.setResultTransformer(Transformers.aliasToBean(ActivePermit.class));
        return q.list();
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<ActivePermit> getActivePermitByPosListAndSpaceRange(final int customerId, final List<PointOfSale> posList, final int startSpace,
        final int endSpace, final int querySpaceBy) {
        String query = null;
        if (querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT
            || querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_EXPIRY_TIME_OF_MOST_RECENT_PERMIT) {
            query = SQL_GET_ACTIVEPERMIT_BY_POSLIST_AND_SPACERANGE_MOST_RECENT;
        } else {
            query = SQL_GET_ACTIVEPERMIT_BY_POSLIST_AND_SPACERANGE;
        }
        final SQLQuery q = this.entityDao.createSQLQuery(query);
        q.addScalar("SpaceNumber", new IntegerType());
        q.addScalar("PaystationSettingName", new StringType());
        q.addScalar("PermitBeginGmt", new TimestampType());
        q.addScalar("PermitExpireGmt", new TimestampType());
        q.setParameter("customerId", customerId);
        q.setParameterList("posList", posList);
        q.setParameter("startSpace", startSpace);
        q.setParameter("endSpace", endSpace);
        q.setResultTransformer(Transformers.aliasToBean(ActivePermit.class));
        return q.list();
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final ActivePermit getActivePermitByCustomerAndAddTimeNumber(final int customerId, final int addTimeNumber) {
        final Calendar limitDate = Calendar.getInstance();
        limitDate.add(Calendar.DAY_OF_YEAR, -StandardConstants.WEEKS_IN_DAYS_2);
        
        final String[] parameters = { "customerId", "addTimeNumber", "limitDate" };
        final Object[] values = { customerId, addTimeNumber, limitDate.getTime() };
        
        final List<ActivePermit> activePermitList =
                this.entityDao.findByNamedQueryAndNamedParam("ActivePermit.findLatestExpiredPermitByCustomerAndAddTimeNumber", parameters, values);
        if (activePermitList != null && !activePermitList.isEmpty()) {
            return activePermitList.get(0);
        } else {
            return null;
        }
    }
    
    //TODO: refactor getActivePermitByCustomerAndAddTimeNumberOrderByLatestExpiry to remove duplicate code
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final ActivePermit getActivePermitByCustomerAndAddTimeNumberOrderByLatestExpiry(final int customerId, final int addTimeNumber) {
        final Calendar limitDate = Calendar.getInstance();
        limitDate.add(Calendar.DAY_OF_YEAR, -StandardConstants.WEEKS_IN_DAYS_2);
        
        final String[] parameters = { "customerId", "addTimeNumber", "limitDate" };
        final Object[] values = { customerId, addTimeNumber, limitDate.getTime() };
        
        final List<ActivePermit> activePermitList =
                this.entityDao.findByNamedQueryAndNamedParam("ActivePermit.findLatestExpiredPermitByCustomerAndAddTimeNumberOrderByLatestExpiry",
                                                             parameters, values);
        if (activePermitList != null && !activePermitList.isEmpty()) {
            return activePermitList.get(0);
        } else {
            return null;
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final ActivePermit getActivePermitByLocationAndAddTimeNumber(final int customerId, final int locationId, final int addTimeNumber) {
        final Calendar limitDate = Calendar.getInstance();
        limitDate.add(Calendar.DAY_OF_YEAR, -StandardConstants.WEEKS_IN_DAYS_2);
        
        final String[] parameters = { "customerId", "locationId", "addTimeNumber", "limitDate" };
        final Object[] values = { customerId, locationId, addTimeNumber, limitDate.getTime() };
        
        final List<ActivePermit> activePermitList =
                this.entityDao.findByNamedQueryAndNamedParam("ActivePermit.findLatestExpiredPermitByLocationAndAddTimeNumber", parameters, values);
        if (activePermitList != null && !activePermitList.isEmpty()) {
            return activePermitList.get(0);
        } else {
            return null;
        }
    }
    
    //TODO: refactor getActivePermitByLocationAndAddTimeNumberOrderByLatestExpiry to remove duplicate code
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final ActivePermit getActivePermitByLocationAndAddTimeNumberOrderByLatestExpiry(final int customerId, final int locationId,
        final int addTimeNumber) {
        final Calendar limitDate = Calendar.getInstance();
        limitDate.add(Calendar.DAY_OF_YEAR, -StandardConstants.WEEKS_IN_DAYS_2);
        
        final String[] parameters = { "customerId", "locationId", "addTimeNumber", "limitDate" };
        final Object[] values = { customerId, locationId, addTimeNumber, limitDate.getTime() };
        
        final List<ActivePermit> activePermitList =
                this.entityDao.findByNamedQueryAndNamedParam("ActivePermit.findLatestExpiredPermitByLocationAndAddTimeNumberOrderByLatestExpiry",
                                                             parameters, values);
        if (activePermitList != null && !activePermitList.isEmpty()) {
            return activePermitList.get(0);
        } else {
            return null;
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final ActivePermit getActivePermitByPaystationSettingAndAddTimeNumber(final int customerId, final String paystationSettingName,
        final int addTimeNumber) {
        final Calendar limitDate = Calendar.getInstance();
        limitDate.add(Calendar.DAY_OF_YEAR, -StandardConstants.WEEKS_IN_DAYS_2);
        
        final String[] parameters = { "customerId", "paystationSettingName", "addTimeNumber", "limitDate" };
        final Object[] values = { customerId, paystationSettingName, addTimeNumber, limitDate.getTime() };
        
        final List<ActivePermit> activePermitList = this.entityDao
                .findByNamedQueryAndNamedParam("ActivePermit.findLatestExpiredPermitByPaystationSettingAndAddTimeNumber", parameters, values);
        if (activePermitList != null && !activePermitList.isEmpty()) {
            return activePermitList.get(0);
        } else {
            return null;
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final ActivePermit getActivePermitByCustomerAndSpaceNumber(final int customerId, final int spaceNumber) {
        final String[] parameters = { "customerId", "spaceNumber" };
        final Object[] values = { customerId, spaceNumber };
        
        final List<ActivePermit> activePermitList =
                this.entityDao.findByNamedQueryAndNamedParam("ActivePermit.findLatestExpiredPermitByCustomerAndSpaceNumber", parameters, values);
        if (activePermitList != null && !activePermitList.isEmpty()) {
            return activePermitList.get(0);
        } else {
            return null;
        }
    }
    
    //TODO: refactor getActivePermitByCustomerAndSpaceNumberOrderByLatestExpiry to remove duplicate code
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final ActivePermit getActivePermitByCustomerAndSpaceNumberOrderByLatestExpiry(final int customerId, final int spaceNumber) {
        final String[] parameters = { "customerId", "spaceNumber" };
        final Object[] values = { customerId, spaceNumber };
        
        final List<ActivePermit> activePermitList = this.entityDao
                .findByNamedQueryAndNamedParam("ActivePermit.findLatestExpiredPermitByCustomerAndSpaceNumberOrderByLatestExpiry", parameters, values);
        if (activePermitList != null && !activePermitList.isEmpty()) {
            return activePermitList.get(0);
        } else {
            return null;
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final ActivePermit getActivePermitByLocationAndSpaceNumber(final int customerId, final int locationId, final int spaceNumber) {
        final String[] parameters = { "customerId", "locationId", "spaceNumber" };
        final Object[] values = { customerId, locationId, spaceNumber };
        
        final List<ActivePermit> activePermitList =
                this.entityDao.findByNamedQueryAndNamedParam("ActivePermit.findLatestExpiredPermitByLocationAndSpaceNumber", parameters, values);
        if (activePermitList != null && !activePermitList.isEmpty()) {
            return activePermitList.get(0);
        } else {
            return null;
        }
    }
    
    //TODO: refactor getActivePermitByLocationAndSpaceNumberOrderByLatestExpiry to remove duplicate code
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final ActivePermit getActivePermitByLocationAndSpaceNumberOrderByLatestExpiry(final int customerId, final int locationId,
        final int spaceNumber) {
        final String[] parameters = { "customerId", "locationId", "spaceNumber" };
        final Object[] values = { customerId, locationId, spaceNumber };
        
        final List<ActivePermit> activePermitList = this.entityDao
                .findByNamedQueryAndNamedParam("ActivePermit.findLatestExpiredPermitByLocationAndSpaceNumberOrderByLatestExpiry", parameters, values);
        if (activePermitList != null && !activePermitList.isEmpty()) {
            return activePermitList.get(0);
        } else {
            return null;
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final ActivePermit getActivePermitByPaystationSettingAndSpaceNumber(final int customerId, final String paystationSettingName,
        final int spaceNumber) {
        final String[] parameters = { "customerId", "paystationSettingName", "spaceNumber" };
        final Object[] values = { customerId, paystationSettingName, spaceNumber };
        
        final List<ActivePermit> activePermitList = this.entityDao
                .findByNamedQueryAndNamedParam("ActivePermit.findLatestExpiredPermitByPaystationSettingAndSpaceNumber", parameters, values);
        if (activePermitList != null && !activePermitList.isEmpty()) {
            return activePermitList.get(0);
        } else {
            return null;
        }
    }
    
    @Override
    public final ActivePermit findActivePermitByOriginalPermitId(final Long originalPermitId) {
        return (ActivePermit) this.entityDao.findUniqueByNamedQueryAndNamedParam("ActivePermit.findActivePermitByOriginalPermitId",
                                                                                 "originalPermitId", originalPermitId, true);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final ActivePermit findActivePermitByLocationIdAndLicencePlateNumber(final Integer permitLocationId, final String licencePlateNumber) {
        final String[] parameters = { "permitLocationId", "licencePlateNumber" };
        final Object[] values = { permitLocationId, licencePlateNumber };
        
        final List<ActivePermit> activePermitList =
                entityDao.findByNamedQueryAndNamedParam("ActivePermit.findActivePermitByLocationIdAndLicencePlateNumber", parameters, values, true);
        
        if (activePermitList != null && !activePermitList.isEmpty()) {
            return activePermitList.get(0);
        } else {
            return null;
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final ActivePermit findActivePermitByLocationIdAndLicencePlateNumberOrderByLatestExpiry(final Integer permitLocationId,
        final String licencePlateNumber) {
        final String[] parameters = { "permitLocationId", "licencePlateNumber" };
        final Object[] values = { permitLocationId, licencePlateNumber };
        
        final List<ActivePermit> activePermitList =
                entityDao.findByNamedQueryAndNamedParam("ActivePermit.findActivePermitByLocationIdAndLicencePlateNumberOrderByLatestExpiry",
                                                        parameters, values, true);
        
        if (activePermitList != null && !activePermitList.isEmpty()) {
            return activePermitList.get(0);
        } else {
            return null;
        }
    }
    
    @Override
    public final ActivePermit findActivePermitByCustomerIdAndLicencePlateNumber(final Integer customerId, final String licencePlateNumber) {
        Query q = this.entityDao.getCurrentSession().getNamedQuery("ActivePermit.findActivePermitByCustomerIdAndLicencePlateNumber");
        q = q.setParameter("customerId", customerId).setParameter("licencePlateNumber", licencePlateNumber).setMaxResults(1).setCacheable(true);
        final List<ActivePermit> list = q.list();
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }
    
    //TODO: refactor findActivePermitByCustomerIdAndLicencePlateNumberOrderByLatestExpiry to remove duplicate code
    @Override
    public final ActivePermit findActivePermitByCustomerIdAndLicencePlateNumberOrderByLatestExpiry(final Integer customerId,
        final String licencePlateNumber) {
        Query q =
                this.entityDao.getCurrentSession().getNamedQuery("ActivePermit.findActivePermitByCustomerIdAndLicencePlateNumberOrderByLatestExpiry");
        q = q.setParameter("customerId", customerId).setParameter("licencePlateNumber", licencePlateNumber).setMaxResults(1).setCacheable(true);
        final List<ActivePermit> list = q.list();
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }
    
    @Override
    public void deleteActivePermit(ActivePermit activePermit) {
        this.entityDao.delete(activePermit);
    }
}
