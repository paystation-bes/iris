package com.digitalpaytech.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.WidgetSubsetMember;
import com.digitalpaytech.dto.WidgetData;
import com.digitalpaytech.dto.WidgetMetricInfo;
import com.digitalpaytech.dto.WidgetSeries;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.WebWidgetHelperService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebWidgetUtil;
import com.digitalpaytech.util.WidgetConstants;

@Service("webWidgetHelperService")
@Transactional(propagation = Propagation.REQUIRED)
public class WebWidgetHelperServiceImpl implements WebWidgetHelperService {
    
    @Autowired
    private EntityDao entityDao;
    @Autowired
    private CustomerAdminService customerAdminService;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final void setCustomerAdminService(final CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    @Override
    public final List<Integer> getAllChildCustomerId(final int parentCustomerId) {
        final List<Integer> result = new ArrayList<Integer>();
        @SuppressWarnings("unchecked")
        final List<Customer> list = this.entityDao.findByNamedQueryAndNamedParam("Customer.findAllChildCustomersByParentCustomerId",
                                                                                 "parentCustomerId", parentCustomerId, true);
        
        for (Customer c : list) {
            result.add(c.getId());
        }
        return result;
    }
    
    @Override
    public final String getCustomerTimeZoneByCustomerId(final int customerId) {
        
        // @SuppressWarnings("unchecked")
        // List<CustomerProperty> list = entityDao.findByNamedQueryAndNamedParam("CustomerProperty.findCustomerPropertyByCustomerId", 
        // "customerId", customerId, true);
        
        String timeZone = WebCoreConstants.GMT;
        
        final CustomerProperty property = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        if (property != null) {
            timeZone = property.getPropertyValue();
        }
        return timeZone;
    }
    
    private String combineStrings(final String... strings) {
        final StringBuilder bdr = new StringBuilder();
        for (String s : strings) {
            bdr.append(s);
        }
        return bdr.toString();
    }
    
    private <T> void mapAdd(final Map<T, Integer> map, final T name, final int addition) {
        final Integer cnt = map.get(name);
        if (addition != 0) {
            map.put(name, (cnt == null) ? 1 : cnt + addition);
        } else if (cnt == null) {
            map.put(name, 0);
        }
    }
    
    private <T> void mapIndex(final Map<T, Integer> map, final boolean trimZero) {
        int index = 0;
        final Iterator<T> keyItr = map.keySet().iterator();
        while (keyItr.hasNext()) {
            final T key = keyItr.next();
            if (trimZero && (map.get(key) == 0)) {
                keyItr.remove();
            } else {
                map.put(key, index++);
            }
        }
    }
    
    @Override
    public final WidgetData convertData(final Widget widget, final List<WidgetMetricInfo> rawData, final UserAccount userAccount,
        final String queryLimit) {
        final Map<String, Integer> uniqueTier1Names = new LinkedHashMap<String, Integer>();
        final Map<String, Integer> uniqueTier2Names = new LinkedHashMap<String, Integer>();
        final Map<String, Integer> uniqueTier3Names = new LinkedHashMap<String, Integer>();
        WidgetData widgetData = new WidgetData();
        
        widgetData.setWidgetName(widget.getName());
        widgetData.setChartType(widget.getWidgetChartType().getId());
        widgetData.setMetricTypeId(widget.getWidgetMetricType().getId());
        widgetData.setRangeTypeId(widget.getWidgetRangeType().getId());
        widgetData.setRandomId(widget.getRandomId());
        
        if (rawData.size() == Integer.parseInt(queryLimit)) {
            widgetData.setIsLimitExceeded(true);
            return widgetData;
        }
        if (rawData.size() == 0) {
            widgetData.setIsZeroValues(true);
            widgetData.setIsEmpty(true);
            return widgetData;
        }
        SimpleDateFormat widgetPattern = null;
        boolean useTimeTierFormat = false;
        if (widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_HOUR
            || widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_DAY
            || widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_MONTH) {
            useTimeTierFormat = true;
            final SimpleDateFormat defaultFormat = new SimpleDateFormat(DateUtil.WIDGET_LABEL_FORMAT);
            final Calendar labelDate = (Calendar) widget.getDateRangeStart().clone();
            final Calendar endDate = (Calendar) widget.getDateRangeEnd().clone();
            if (widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_HOUR) {
                String dateFormat;
                if (widget.getWidgetChartType().getId() == WidgetConstants.CHART_TYPE_LIST) {
                    dateFormat = DateUtil.WIDGET_BY_HOUR_LABEL_FORMAT;
                } else {
                    dateFormat = DateUtil.WIDGET_LABEL_FORMAT;
                }
                widgetPattern = new SimpleDateFormat(dateFormat);
                // For hourly data 23:00-00:00 TierTypeId (DayOfYear) 
                // depend on the interval startTime but Tier1TypeName show endTime.
                // Therefore TierTypeId should reflect finished day not the new day.
                // Except for occupancy type widgets.
                final Calendar dayLabelDate = (Calendar) labelDate.clone();
                if (widget.getWidgetMetricType().getId() != WidgetConstants.METRIC_TYPE_COMPARE_OCCUPANCY
                    && widget.getWidgetMetricType().getId() != WidgetConstants.METRIC_TYPE_PAID_OCCUPANCY
                    && widget.getWidgetMetricType().getId() != WidgetConstants.METRIC_TYPE_PHYSICAL_OCCUPANCY) {
                    dayLabelDate.add(Calendar.HOUR, -1);
                }
                while (!labelDate.after(endDate)) {
                    mapAdd(uniqueTier1Names, DateUtil.createDateString(widgetPattern, labelDate.getTimeZone(), labelDate.getTime())
                                             + StandardConstants.STRING_UNDERSCORE + dayLabelDate.get(Calendar.DAY_OF_YEAR), 1);
                    labelDate.add(Calendar.HOUR_OF_DAY, 1);
                    dayLabelDate.add(Calendar.HOUR_OF_DAY, 1);
                }
                
            } else if (widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_DAY) {
                widgetPattern = new SimpleDateFormat(DateUtil.DATE_ONLY_FORMAT_WIDGET);
                while (!labelDate.after(widget.getDateRangeEnd())) {
                    mapAdd(uniqueTier1Names, DateUtil.createDateString(widgetPattern, labelDate.getTimeZone(), labelDate.getTime())
                                             + StandardConstants.STRING_UNDERSCORE + labelDate.get(Calendar.DAY_OF_YEAR), 1);
                    labelDate.add(Calendar.DAY_OF_YEAR, 1);
                }
            } else if (widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_MONTH) {
                // For Total Revenue by Month, Total Purchases by Month, Average Turnover by Month, Utilization by Month.
                while (!labelDate.after(widget.getDateRangeEnd())) {
                    // e.g. Aug 2013_8, Nov 2013_11
                    mapAdd(uniqueTier1Names,
                           combineStrings(labelDate.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US), StandardConstants.STRING_EMPTY_SPACE,
                                          String.valueOf(labelDate.get(Calendar.YEAR)), StandardConstants.STRING_UNDERSCORE,
                                          String.valueOf(labelDate.get(Calendar.MONTH) + 1)), 1);
                    labelDate.add(Calendar.MONTH, 1);
                }
            }
            boolean hasNonZero = false;
            for (WidgetMetricInfo widgetMetricInfo : rawData) {
                final boolean isNotZero = (widgetMetricInfo.getWidgetMetricValue() != null)
                                          && (widgetMetricInfo.getWidgetMetricValue().compareTo(BigDecimal.ZERO) != 0);
                if (widgetMetricInfo.getTier2TypeName() != null) {
                    mapAdd(uniqueTier2Names,
                           widgetMetricInfo.getTier2TypeName() + StandardConstants.STRING_UNDERSCORE + widgetMetricInfo.getTier2TypeId(), isNotZero
                                   ? 1 : 0);
                }
                
                if (widgetMetricInfo.getTier3TypeName() != null) {
                    mapAdd(uniqueTier3Names,
                           widgetMetricInfo.getTier3TypeName() + StandardConstants.STRING_UNDERSCORE + widgetMetricInfo.getTier3TypeId(), isNotZero
                                   ? 1 : 0);
                }
                
                hasNonZero |= isNotZero;
                try {
                    if (widgetMetricInfo.getTier1TypeName() != null) {
                        final Date originalDate = defaultFormat.parse(widgetMetricInfo.getTier1TypeName());
                        final Calendar adjustedCal = Calendar.getInstance();
                        adjustedCal.setTime(originalDate);
                        adjustedCal.add(Calendar.MILLISECOND, -labelDate.getTimeZone().getOffset(originalDate.getTime()));
                        widgetMetricInfo.setTier1TypeName(DateUtil.createDateString(widgetPattern, labelDate.getTimeZone(), adjustedCal.getTime()));
                    }
                } catch (ParseException pe) {
                }
            }
            
            widgetData.setIsZeroValues((widget.getWidgetChartType().getId() != WidgetConstants.CHART_TYPE_SINGLE_VALUE) && !hasNonZero);
            
            //Inserts missing subsetMembers into uniqueTierName list to pad for missing data on specified subsets
            // Commented out the subset series name because UI do not send "ALL" for Tier2 & Tier3
            //            if (widget.isIsSubsetTier2() || widget.isIsSubsetTier3()) {
            //                Set<WidgetSubsetMember> subsetMembers = widget.getWidgetSubsetMembers();
            //                for (WidgetSubsetMember subsetMember : subsetMembers) {
            //                    if (subsetMember.getWidgetTierType().getId().intValue() == widget.getWidgetTierTypeByWidgetTier2Type().getId().intValue()) {
            //                        uniqueTier2Names.add(subsetMember.getMemberName() + StandardConstants.STRING_UNDERSCORE + subsetMember.getMemberId());
            //                    } else if (subsetMember.getWidgetTierType().getId().intValue() == 
            //                               widget.getWidgetTierTypeByWidgetTier3Type().getId().intValue()) {
            //                        uniqueTier3Names.add(subsetMember.getMemberName() + StandardConstants.STRING_UNDERSCORE + subsetMember.getMemberId());
            //                    }
            //                }
            //            }
            
        } else {
            boolean hasNonZero = false;
            if (widget.getWidgetFilterType().getId() == WidgetConstants.FILTER_TYPE_BOTTOM) {
                for (int i = rawData.size() - 1; i >= 0; i--) {
                    final WidgetMetricInfo widgetMetricInfo = rawData.get(i);
                    final boolean isNotZero = (widgetMetricInfo.getWidgetMetricValue() != null)
                                              && (widgetMetricInfo.getWidgetMetricValue().compareTo(new BigDecimal(0)) != 0);
                    if (widgetMetricInfo.getTier1TypeName() != null) {
                        mapAdd(uniqueTier1Names,
                               widgetMetricInfo.getTier1TypeName() + StandardConstants.STRING_UNDERSCORE + widgetMetricInfo.getTier1TypeId(),
                               (isNotZero || (!widgetMetricInfo.isTier1TypeHidden())) ? 1 : 0);
                    }
                    
                    if (isNotZero) {
                        if (widgetMetricInfo.getTier2TypeName() != null) {
                            mapAdd(uniqueTier2Names,
                                   widgetMetricInfo.getTier2TypeName() + StandardConstants.STRING_UNDERSCORE + widgetMetricInfo.getTier2TypeId(),
                                   isNotZero ? 1 : 0);
                        }
                        if (widgetMetricInfo.getTier3TypeName() != null) {
                            mapAdd(uniqueTier3Names,
                                   widgetMetricInfo.getTier3TypeName() + StandardConstants.STRING_UNDERSCORE + widgetMetricInfo.getTier3TypeId(),
                                   isNotZero ? 1 : 0);
                        }
                    }
                    
                    hasNonZero |= isNotZero;
                }
                
            } else {
                for (WidgetMetricInfo widgetMetricInfo : rawData) {
                    final boolean isNotZero = (widgetMetricInfo.getWidgetMetricValue() != null)
                                              && (widgetMetricInfo.getWidgetMetricValue().compareTo(new BigDecimal(0)) != 0);
                    if (widgetMetricInfo.getTier1TypeName() != null) {
                        mapAdd(uniqueTier1Names,
                               widgetMetricInfo.getTier1TypeName() + StandardConstants.STRING_UNDERSCORE + widgetMetricInfo.getTier1TypeId(),
                               (isNotZero || (!widgetMetricInfo.isTier1TypeHidden())) ? 1 : 0);
                    }
                    
                    if (isNotZero) {
                        if (widgetMetricInfo.getTier2TypeName() != null) {
                            mapAdd(uniqueTier2Names,
                                   widgetMetricInfo.getTier2TypeName() + StandardConstants.STRING_UNDERSCORE + widgetMetricInfo.getTier2TypeId(),
                                   isNotZero ? 1 : 0);
                        }
                        if (widgetMetricInfo.getTier3TypeName() != null) {
                            mapAdd(uniqueTier3Names,
                                   widgetMetricInfo.getTier3TypeName() + StandardConstants.STRING_UNDERSCORE + widgetMetricInfo.getTier3TypeId(),
                                   isNotZero ? 1 : 0);
                        }
                    }
                    
                    hasNonZero |= isNotZero;
                }
            }
            widgetData.setIsZeroValues((widget.getWidgetChartType().getId() != WidgetConstants.CHART_TYPE_SINGLE_VALUE) && !hasNonZero);
            
            //Inserts missing subsetMembers into uniqueTierName list to pad for missing data on specified subsets
            if (widget.isIsSubsetTier1() || widget.isIsSubsetTier2() || widget.isIsSubsetTier3()) {
                final List<WidgetSubsetMember> subsetMembers = widget.getWidgetSubsetMembers();
                for (WidgetSubsetMember subsetMember : subsetMembers) {
                    if (subsetMember.getWidgetTierType().getId().intValue() == widget.getWidgetTierTypeByWidgetTier1Type().getId().intValue()) {
                        mapAdd(uniqueTier1Names, subsetMember.getMemberName() + StandardConstants.STRING_UNDERSCORE + subsetMember.getMemberId(), 0);
                    }
                    // Commented out the subset series name because UI do not send "ALL" for Tier2 & Tier3
                    //                    else if (subsetMember.getWidgetTierType().getId().intValue() == 
                    //                             widget.getWidgetTierTypeByWidgetTier2Type().getId().intValue()) {
                    //                        uniqueTier2Names.add(subsetMember.getMemberName() + StandardConstants.STRING_UNDERSCORE 
                    //                                           + subsetMember.getMemberId());
                    //                    } else if (subsetMember.getWidgetTierType().getId().intValue() == 
                    //                             widget.getWidgetTierTypeByWidgetTier3Type().getId().intValue()) {
                    //                        uniqueTier3Names.add(subsetMember.getMemberName() + StandardConstants.STRING_UNDERSCORE 
                    //                                           + subsetMember.getMemberId());
                    //                    }
                }
            }
            
        }
        
        mapIndex(uniqueTier1Names, true);
        mapIndex(uniqueTier2Names, true);
        mapIndex(uniqueTier3Names, true);
        
        //For 3 tiered widgets
        if (uniqueTier3Names.size() > 0) {
            for (String tier2Name : uniqueTier2Names.keySet()) {
                for (String tier3Name : uniqueTier3Names.keySet()) {
                    final WidgetSeries series = new WidgetSeries();
                    
                    final String stackName = tier2Name.substring(0, tier2Name.lastIndexOf(StandardConstants.STRING_UNDERSCORE));
                    final String seriesName = tier3Name.substring(0, tier3Name.lastIndexOf(StandardConstants.STRING_UNDERSCORE));
                    
                    // uniqueTier1Names.size(),StandardConstants.STRING_ZERO));
                    final List<String> defaultData = new ArrayList<String>(
                            Collections.nCopies(uniqueTier1Names.size(), StandardConstants.STRING_ZERO));
                    
                    series.setStackName(stackName);
                    series.setSeriesName(seriesName);
                    series.setSeriesData(defaultData);
                    
                    // creates key using tier1Name_tier2name
                    widgetData.addToSeriesMap(series, tier2Name, tier3Name);
                }
            }
            for (WidgetMetricInfo widgetMetricInfo : rawData) {
                if (widgetMetricInfo.getWidgetMetricValue() == null || widgetMetricInfo.getWidgetMetricValue().compareTo(BigDecimal.ZERO) == 0) {
                    continue;
                }
                
                final String key = widgetMetricInfo.getTier2TypeName() + StandardConstants.STRING_UNDERSCORE + widgetMetricInfo.getTier2TypeId()
                                   + StandardConstants.STRING_UNDERSCORE + widgetMetricInfo.getTier3TypeName() + StandardConstants.STRING_UNDERSCORE
                                   + widgetMetricInfo.getTier3TypeId();
                final Integer value = uniqueTier1Names.get(widgetMetricInfo.getTier1TypeName() + StandardConstants.STRING_UNDERSCORE
                                                           + widgetMetricInfo.getTier1TypeId());
                final int index = (value == null) ? 0 : value;
                
                final BigDecimal metricTotal = format(widgetMetricInfo.getWidgetMetricValue(), widget);
                // verify that the index was found before inserting data
                if (index >= 0 && widgetData.getSeriesMap().containsKey(key)) {
                    widgetData.getSeriesMap().get(key).insertSeriesData(index, metricTotal.toPlainString());
                }
            }
        } else if (uniqueTier2Names.size() > 0) {
            // For 2 tiered widgets
            for (String tier2Name : uniqueTier2Names.keySet()) {
                final WidgetSeries series = new WidgetSeries();
                
                final String seriesName = tier2Name.substring(0, tier2Name.lastIndexOf(StandardConstants.STRING_UNDERSCORE));
                
                final int size = defaultDataSetSize(widget, uniqueTier1Names.size(), 2, seriesName, rawData, userAccount);
                final List<String> defaultData = new ArrayList<String>(Collections.nCopies(size, StandardConstants.STRING_ZERO));
                
                series.setSeriesName(seriesName);
                series.setSeriesData(defaultData);
                // creates key using tier1Name_tier2name
                widgetData.addToSeriesMap(series, StandardConstants.STRING_NULL, tier2Name);
            }
            for (WidgetMetricInfo widgetMetricInfo : rawData) {
                if (widgetMetricInfo.getWidgetMetricValue() == null || widgetMetricInfo.getWidgetMetricValue().compareTo(BigDecimal.ZERO) == 0) {
                    continue;
                }
                final String key = StandardConstants.STRING_NULL + StandardConstants.STRING_UNDERSCORE + widgetMetricInfo.getTier2TypeName()
                                   + StandardConstants.STRING_UNDERSCORE + widgetMetricInfo.getTier2TypeId();
                final Integer value = uniqueTier1Names.get(widgetMetricInfo.getTier1TypeName() + StandardConstants.STRING_UNDERSCORE
                                                           + widgetMetricInfo.getTier1TypeId());
                final int index = (value == null) ? 0 : value;
                
                final BigDecimal metricTotal = format(widgetMetricInfo.getWidgetMetricValue(), widget);
                // verify that the index was found before inserting data
                if (index >= 0 && widgetData.getSeriesMap().containsKey(key)) {
                    widgetData.getSeriesMap().get(key).insertSeriesData(index, metricTotal.toPlainString());
                }
            }
        } else if (widget.getWidgetChartType().getId() != WidgetConstants.CHART_TYPE_SINGLE_VALUE) {
            // For single tiered widgets
            final WidgetSeries series = new WidgetSeries();
            
            final int size = defaultDataSetSize(widget, uniqueTier1Names.size(), 1, widget.getWidgetMetricType().getName(), rawData, userAccount);
            final List<String> defaultData = new ArrayList<String>(Collections.nCopies(size, StandardConstants.STRING_ZERO));
            
            series.setSeriesName(widget.getWidgetMetricType().getName());
            series.setSeriesData(defaultData);
            widgetData.addToSeriesMap(series, "null", widget.getWidgetMetricType().getName());
            
            for (WidgetMetricInfo widgetMetricInfo : rawData) {
                if (widgetMetricInfo.getWidgetMetricValue() == null || widgetMetricInfo.getWidgetMetricValue().compareTo(BigDecimal.ZERO) == 0) {
                    continue;
                }
                final String key = "null_" + widget.getWidgetMetricType().getName();
                final Integer value = uniqueTier1Names.get(widgetMetricInfo.getTier1TypeName() + StandardConstants.STRING_UNDERSCORE
                                                           + widgetMetricInfo.getTier1TypeId());
                final int index = (value == null) ? 0 : value;
                
                final BigDecimal metricTotal = format(widgetMetricInfo.getWidgetMetricValue(), widget);
                // verify that the index was found before inserting data
                if (index >= 0 && widgetData.getSeriesMap().containsKey(key)) {
                    widgetData.getSeriesMap().get(key).insertSeriesData(index, metricTotal.toPlainString());
                }
            }
        } else {
            // For single value chart type widgets
            BigDecimal metricTotal = rawData.get(0).getWidgetMetricValue();
            metricTotal = format(metricTotal, widget);
            widgetData.setSingleValue(metricTotal.toPlainString());
        }
        
        for (String tier1Name : uniqueTier1Names.keySet()) {
            final String name = tier1Name.substring(0, tier1Name.lastIndexOf(StandardConstants.STRING_UNDERSCORE));
            widgetData.addTier1Names(name);
        }
        
        final int tier1Id = widget.getWidgetTierTypeByWidgetTier1Type().getId();
        if (tier1Id == WidgetConstants.TIER_TYPE_HOUR || tier1Id == WidgetConstants.TIER_TYPE_DAY || tier1Id == WidgetConstants.TIER_TYPE_MONTH
            || tier1Id == WidgetConstants.TIER_TYPE_WEEK) {
            final Calendar startTimeCalendar = (Calendar) widget.getDateRangeStart().clone();
            if (tier1Id == WidgetConstants.TIER_TYPE_HOUR) {
                final int rangeType = widget.getWidgetRangeType().getId();
                final int metricType = widget.getWidgetMetricType().getId();
            }
            final Date startTime = startTimeCalendar.getTime();
            widgetData = WebWidgetUtil.getStartTimeAndInterval(widget, widgetData, startTime);
        }
        
        return widgetData;
    }
    
    public final BigDecimal format(final BigDecimal metric, final Widget widget) {
        BigDecimal result = metric;
        if (widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_REVENUE
            || widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_COLLECTIONS
            || widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_SETTLED_CARD
            || widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_CARD_PROCESSING_REV) {
            result = result.divide(StandardConstants.BIGDECIMAL_100);
        } else if (widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_AVERAGE_PRICE
                   || widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_AVERAGE_REVENUE) {
            result = new BigDecimal(result.intValue());
            result = result.divide(StandardConstants.BIGDECIMAL_100);
        } else if (widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_TURNOVER
                   || widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_UTILIZATION
                   || widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_PAID_OCCUPANCY
                   || widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_PHYSICAL_OCCUPANCY
                   || widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_COMPARE_OCCUPANCY
                   || widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_AVERAGE_DURATION) {
            result = result.setScale(2, RoundingMode.HALF_UP);
            result = result.stripTrailingZeros();
            if (result.equals(new BigDecimal("0.00"))) {
                result = new BigDecimal(0);
            }
        }
        return result;
    }
    
    /**
     * Returns the length (number of X-axis values) needed for the requested widget tier type.
     * Length is adjusted for parent customers viewing child data in a different time zone.
     * 
     * 
     * @param widget
     * @param defaultSize
     * @param uniqueNameTier
     * @param uniqueName
     * @param rawData
     * @param userAccount
     * @return
     */
    // TODO Probably not necessary
    @Deprecated
    private int defaultDataSetSize(final Widget widget, final int defaultSize, final int uniqueNameTier, final String uniqueName,
        final List<WidgetMetricInfo> rawData, final UserAccount userAccount) {
        
        //        if (userAccount.getCustomer().isIsParent() && widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_HOUR
        //            && widget.getWidgetRangeType().getId() != WidgetConstants.RANGE_TYPE_YESTERDAY) {
        //            
        //            int childCustomerId = 0;
        //            
        //            forLoop: for (WidgetMetricInfo widgetMetricInfo : rawData) {
        //                switch (uniqueNameTier) {
        //                    case (1):
        //                        if (uniqueName.equals(widgetMetricInfo.getTier1TypeName()) && widgetMetricInfo.getCustomerId() != 0) {
        //                            childCustomerId = widgetMetricInfo.getCustomerId();
        //                            break forLoop;
        //                        }
        //                        break;
        //                    case (2):
        //                        if (uniqueName.equals(widgetMetricInfo.getTier2TypeName()) && widgetMetricInfo.getCustomerId() != 0) {
        //                            childCustomerId = widgetMetricInfo.getCustomerId();
        //                            break forLoop;
        //                        }
        //                        break;
        //                    case (3):
        //                        if (uniqueName.equals(widgetMetricInfo.getTier3TypeName()) && widgetMetricInfo.getCustomerId() != 0) {
        //                            childCustomerId = widgetMetricInfo.getCustomerId();
        //                            break forLoop;
        //                        }
        //                        break;
        //                }
        //            }
        //            
        //            String parentTimeZone = getCustomerTimeZoneByCustomerId(userAccount.getCustomer().getId());
        //            CustomerProperty customerProperty = customerAdminService
        //        .getCustomerPropertyByCustomerIdAndCustomerPropertyType(userAccount.getCustomer().getId(), WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        //            
        //            
        //            if (childCustomerId == 0) {
        //                childCustomerId = userAccount.getCustomer().getId();
        //            }
        //            customerProperties = customerAdminService.getCustomerPropertyByCustomerId(childCustomerId);
        //            String childTimeZone = "GMT"; // Default Timezone         
        //            for (CustomerProperty property : customerProperties) {
        //                if (property.getCustomerPropertyType().getId() == WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE) {
        //                    childTimeZone = property.getPropertyValue();
        //                    break;
        //                }
        //            }
        //            
        //            Calendar parentCal = new GregorianCalendar(TimeZone.getTimeZone(parentTimeZone));
        //            parentCal.setTimeInMillis(new Date().getTime());
        //            Calendar childCal = new GregorianCalendar(TimeZone.getTimeZone(childTimeZone));
        //            childCal.setTimeInMillis(new Date().getTime());
        //            
        //            int diffInHours = parentCal.get(Calendar.HOUR_OF_DAY) - childCal.get(Calendar.HOUR_OF_DAY);
        //            int diffInDays = parentCal.get(Calendar.DAY_OF_MONTH) - childCal.get(Calendar.DAY_OF_MONTH);
        //            
        //            if (diffInDays != 0) {
        //                diffInHours += StandardConstants.HOURS_IN_DAYS_1;
        //            }
        //            
        //            if (diffInHours < 0) {
        //                return defaultSize;
        //            }
        //            
        //            return defaultSize - diffInHours;
        //        } else {
        //            return defaultSize;
        //        }
        return defaultSize;
    }
    
    @Override
    public final void calculateRange(final Widget widget, final int customerId, final int rangeTypeId) {
        // Time that will be used for the widget. Needs to be calculated here because different parts of the widget need a unique value
        widget.setWidgetRunDate(DateUtil.getCurrentGmtDate());
        final String timeZone = getCustomerTimeZoneByCustomerId(customerId);
        
        final Calendar cal = new GregorianCalendar(TimeZone.getTimeZone(timeZone));
        cal.setTime(widget.getWidgetRunDate());
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        switch (rangeTypeId) {
            case WidgetConstants.RANGE_TYPE_TODAY:
                widget.setDateRangeEnd((Calendar) cal.clone());
                cal.set(Calendar.HOUR_OF_DAY, 0);
                widget.setDateRangeStart(cal);
                break;
            case WidgetConstants.RANGE_TYPE_YESTERDAY:
                cal.set(Calendar.HOUR_OF_DAY, 0);
                widget.setDateRangeEnd((Calendar) cal.clone());
                cal.add(Calendar.DAY_OF_YEAR, -1);
                widget.setDateRangeStart(cal);
                break;
            case WidgetConstants.RANGE_TYPE_LAST_24HOURS:
                widget.setDateRangeEnd((Calendar) cal.clone());
                cal.add(Calendar.HOUR_OF_DAY, -StandardConstants.HOURS_IN_DAYS_1);
                widget.setDateRangeStart(cal);
                break;
            case WidgetConstants.RANGE_TYPE_LAST_7DAYS:
                cal.set(Calendar.HOUR_OF_DAY, 0);
                widget.setDateRangeEnd((Calendar) cal.clone());
                cal.add(Calendar.DAY_OF_YEAR, -StandardConstants.DAYS_IN_A_WEEK);
                widget.setDateRangeStart(cal);
                break;
            case WidgetConstants.RANGE_TYPE_LAST_30DAYS:
                cal.set(Calendar.HOUR_OF_DAY, 0);
                widget.setDateRangeEnd((Calendar) cal.clone());
                cal.add(Calendar.DAY_OF_YEAR, -StandardConstants.CONSTANT_30);
                widget.setDateRangeStart(cal);
                break;
            case WidgetConstants.RANGE_TYPE_LAST_WEEK:
                cal.add(Calendar.WEEK_OF_YEAR, -1);
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                widget.setDateRangeEnd((Calendar) cal.clone());
                cal.add(Calendar.DAY_OF_YEAR, -StandardConstants.DAYS_IN_A_WEEK);
                widget.setDateRangeStart(cal);
                break;
            case WidgetConstants.RANGE_TYPE_LAST_MONTH:
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.DAY_OF_MONTH, 1);
                widget.setDateRangeEnd((Calendar) cal.clone());
                cal.add(Calendar.MONTH, -1);
                widget.setDateRangeStart(cal);
                break;
            case WidgetConstants.RANGE_TYPE_LAST_12MONTHS:
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.DAY_OF_MONTH, 1);
                widget.setDateRangeEnd((Calendar) cal.clone());
                cal.add(Calendar.MONTH, -StandardConstants.MONTHS_IN_A_YEAR);
                widget.setDateRangeStart(cal);
                break;
            case WidgetConstants.RANGE_TYPE_LAST_YEAR:
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.DAY_OF_MONTH, 1);
                cal.set(Calendar.MONTH, Calendar.JANUARY);
                widget.setDateRangeEnd((Calendar) cal.clone());
                cal.add(Calendar.YEAR, -1);
                widget.setDateRangeStart(cal);
                break;
            case WidgetConstants.RANGE_TYPE_YEAR_TO_DATE:
                widget.setDateRangeEnd((Calendar) cal.clone());
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.DAY_OF_MONTH, 1);
                cal.set(Calendar.MONTH, Calendar.JANUARY);
                widget.setDateRangeStart(cal);
                break;
            default:
                break;
        }
    }
    
    @Override
    public final void calculateRange(final Widget widget, final int customerId, final StringBuilder query, final int rangeTypeId,
        final int tier1TypeId, final boolean usePartitionedTable, final boolean calculateWidgetRanges) {
        // Time that will be used for the widget. Needs to be calculated here because different parts of the widget need a unique value
        widget.setWidgetRunDate(DateUtil.getCurrentGmtDate());
        if (calculateWidgetRanges
            && (tier1TypeId == WidgetConstants.TIER_TYPE_HOUR || tier1TypeId == WidgetConstants.TIER_TYPE_DAY || tier1TypeId == WidgetConstants.TIER_TYPE_MONTH)) {
            final String timeZone = getCustomerTimeZoneByCustomerId(customerId);
            
            final Calendar cal = new GregorianCalendar(TimeZone.getTimeZone(timeZone));
            cal.setTime(widget.getWidgetRunDate());
            //            cal.add(Calendar.HOUR_OF_DAY, TimeZone.getTimeZone(timeZone).getOffset(cal.getTimeInMillis())/3600000);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            switch (rangeTypeId) {
                case WidgetConstants.RANGE_TYPE_TODAY:
                    widget.setDateRangeEnd((Calendar) cal.clone());
                    if (widget.getWidgetMetricType().getId() != WidgetConstants.METRIC_TYPE_PAID_OCCUPANCY
                        && widget.getWidgetMetricType().getId() != WidgetConstants.METRIC_TYPE_PHYSICAL_OCCUPANCY
                        && widget.getWidgetMetricType().getId() != WidgetConstants.METRIC_TYPE_COMPARE_OCCUPANCY) {
                        cal.set(Calendar.HOUR_OF_DAY, 1);
                    } else {
                        cal.set(Calendar.HOUR_OF_DAY, 0);
                    }
                    widget.setDateRangeStart(cal);
                    break;
                case WidgetConstants.RANGE_TYPE_YESTERDAY:
                    if (widget.getWidgetMetricType().getId() != WidgetConstants.METRIC_TYPE_PAID_OCCUPANCY
                        && widget.getWidgetMetricType().getId() != WidgetConstants.METRIC_TYPE_PHYSICAL_OCCUPANCY
                        && widget.getWidgetMetricType().getId() != WidgetConstants.METRIC_TYPE_COMPARE_OCCUPANCY) {
                        cal.set(Calendar.HOUR_OF_DAY, 0);
                    } else {
                        cal.add(Calendar.DAY_OF_YEAR, -1);
                        cal.set(Calendar.HOUR_OF_DAY, StandardConstants.LAST_HOUR_OF_THE_DAY);
                    }
                    widget.setDateRangeEnd((Calendar) cal.clone());
                    cal.add(Calendar.HOUR_OF_DAY, -StandardConstants.LAST_HOUR_OF_THE_DAY);
                    widget.setDateRangeStart(cal);
                    break;
                case WidgetConstants.RANGE_TYPE_LAST_24HOURS:
                    widget.setDateRangeEnd((Calendar) cal.clone());
                    cal.add(Calendar.HOUR_OF_DAY, -StandardConstants.LAST_HOUR_OF_THE_DAY);
                    widget.setDateRangeStart(cal);
                    break;
                case WidgetConstants.RANGE_TYPE_LAST_7DAYS:
                    cal.add(Calendar.DAY_OF_YEAR, -1);
                    cal.set(Calendar.HOUR_OF_DAY, 0);
                    widget.setDateRangeEnd((Calendar) cal.clone());
                    cal.add(Calendar.DAY_OF_YEAR, -StandardConstants.CONSTANT_6);
                    widget.setDateRangeStart(cal);
                    break;
                case WidgetConstants.RANGE_TYPE_LAST_30DAYS:
                    cal.add(Calendar.DAY_OF_YEAR, -1);
                    cal.set(Calendar.HOUR_OF_DAY, 0);
                    widget.setDateRangeEnd((Calendar) cal.clone());
                    cal.add(Calendar.DAY_OF_YEAR, -StandardConstants.DAYS_29);
                    widget.setDateRangeStart(cal);
                    break;
                //                case WidgetConstants.RANGE_TYPE_THIS_WEEK:
                //                    cal.add(Calendar.DAY_OF_YEAR, -1);
                //                    cal.set(Calendar.HOUR_OF_DAY, 0);
                //                    widget.setDateRangeEnd((Calendar) cal.clone());
                //                    cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                //                    widget.setDateRangeStart(cal);
                //                    break;
                //                case WidgetConstants.RANGE_TYPE_THIS_MONTH:
                //                    cal.add(Calendar.DAY_OF_YEAR, -1);
                //                    cal.set(Calendar.HOUR_OF_DAY, 0);
                //                    widget.setDateRangeEnd((Calendar) cal.clone());
                //                    cal.set(Calendar.DAY_OF_MONTH, 1);
                //                    widget.setDateRangeStart(cal);
                //                    break;
                case WidgetConstants.RANGE_TYPE_LAST_WEEK:
                    cal.add(Calendar.WEEK_OF_YEAR, -1);
                    cal.set(Calendar.HOUR_OF_DAY, 0);
                    cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
                    widget.setDateRangeEnd((Calendar) cal.clone());
                    cal.add(Calendar.DAY_OF_YEAR, -StandardConstants.CONSTANT_6);
                    widget.setDateRangeStart(cal);
                    break;
                case WidgetConstants.RANGE_TYPE_LAST_MONTH:
                    cal.set(Calendar.HOUR_OF_DAY, 0);
                    cal.set(Calendar.DAY_OF_MONTH, 1);
                    cal.add(Calendar.DAY_OF_YEAR, -1);
                    widget.setDateRangeEnd((Calendar) cal.clone());
                    cal.set(Calendar.DAY_OF_MONTH, 1);
                    widget.setDateRangeStart(cal);
                    break;
                case WidgetConstants.RANGE_TYPE_LAST_12MONTHS:
                    cal.set(Calendar.HOUR_OF_DAY, 0);
                    cal.set(Calendar.DAY_OF_MONTH, 1);
                    cal.add(Calendar.MONTH, -1);
                    widget.setDateRangeEnd((Calendar) cal.clone());
                    cal.add(Calendar.MONTH, -StandardConstants.CONSTANT_11);
                    widget.setDateRangeStart(cal);
                    break;
                //                case WidgetConstants.RANGE_TYPE_THIS_YEAR:
                //                    cal.set(Calendar.HOUR_OF_DAY, 0);
                //                    cal.set(Calendar.DAY_OF_MONTH, 1);
                //                    widget.setDateRangeEnd((Calendar) cal.clone());
                //                    cal.set(Calendar.MONTH, Calendar.JANUARY);
                //                    widget.setDateRangeStart(cal);
                //                    break;
                case WidgetConstants.RANGE_TYPE_LAST_YEAR:
                    cal.set(Calendar.HOUR_OF_DAY, 0);
                    cal.set(Calendar.DAY_OF_MONTH, 1);
                    cal.set(Calendar.MONTH, Calendar.JANUARY);
                    cal.add(Calendar.MONTH, -1);
                    widget.setDateRangeEnd((Calendar) cal.clone());
                    cal.set(Calendar.MONTH, Calendar.JANUARY);
                    widget.setDateRangeStart(cal);
                    break;
                case WidgetConstants.RANGE_TYPE_YEAR_TO_DATE:
                    widget.setDateRangeEnd((Calendar) cal.clone());
                    cal.set(Calendar.HOUR_OF_DAY, 0);
                    cal.set(Calendar.DAY_OF_MONTH, 1);
                    cal.set(Calendar.MONTH, Calendar.JANUARY);
                    widget.setDateRangeStart(cal);
                    break;
                default:
                    break;
            }
        }
        
        switch (rangeTypeId) {
            case WidgetConstants.RANGE_TYPE_NA:
                break;
            case WidgetConstants.RANGE_TYPE_NOW:
                /* Now */
                query.append("AND t.Id = (SELECT MAX(Id) FROM `Time` t3 WHERE t3.Date = DATE(:currentLocalTime) AND t3.DateTime <= :currentLocalTime ) ");
                break;
            case WidgetConstants.RANGE_TYPE_TODAY:
                /* Today */
                if ((widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_PAID_OCCUPANCY
                     || widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_PHYSICAL_OCCUPANCY || widget.getWidgetMetricType()
                        .getId() == WidgetConstants.METRIC_TYPE_COMPARE_OCCUPANCY)
                    && widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_HOUR) {
                    query.append("AND t.Date = DATE(:currentLocalTime) AND t.HourOfDay <= HOUR(:currentLocalTime) ");
                    query.append("AND t.QuarterOfHour = 0 ");
                } else {
                    query.append("AND t.Date = DATE(:currentLocalTime) AND t.HourOfDay < HOUR(:currentLocalTime) ");
                }
                break;
            case WidgetConstants.RANGE_TYPE_YESTERDAY:
                /* Yesterday */
                query.append("AND t.Date = DATE(DATE_SUB(:currentLocalTime, INTERVAL 1 DAY)) ");
                if ((widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_PAID_OCCUPANCY
                     || widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_PHYSICAL_OCCUPANCY || widget.getWidgetMetricType()
                        .getId() == WidgetConstants.METRIC_TYPE_COMPARE_OCCUPANCY)
                    && widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_HOUR) {
                    query.append("AND t.QuarterOfHour = 0 ");
                }
                break;
            case WidgetConstants.RANGE_TYPE_LAST_24HOURS:
                /* Last 24 Hours */
                if ((widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_PAID_OCCUPANCY
                     || widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_PHYSICAL_OCCUPANCY || widget.getWidgetMetricType()
                        .getId() == WidgetConstants.METRIC_TYPE_COMPARE_OCCUPANCY)
                    && widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_HOUR) {
                    query.append("AND t.Id > (SELECT MIN(t3.Id) FROM `Time` t3 ")
                            .append("WHERE t3.HourOfDay = HOUR(DATE_SUB(:currentLocalTime, INTERVAL 24 HOUR)) ")
                            .append("AND t3.Date = DATE(DATE_SUB(:currentLocalTime, INTERVAL 24 HOUR))) ")
                            .append("AND t.Id <=  (SELECT MIN(t4.Id) FROM `Time` t4 ")
                            .append("WHERE t4.HourOfDay = HOUR(:currentLocalTime) AND t4.Date = DATE(:currentLocalTime)) ")
                            .append("AND t.QuarterOfHour = 0 ");
                } else {
                    query.append("AND t.Id >= (SELECT MIN(t3.Id) FROM `Time` t3 ")
                            .append("WHERE t3.HourOfDay = HOUR(DATE_SUB(:currentLocalTime, INTERVAL 24 HOUR)) ")
                            .append("AND t3.Date = DATE(DATE_SUB(:currentLocalTime, INTERVAL 24 HOUR))) ")
                            .append("AND t.Id <  (SELECT MIN(t4.Id) FROM `Time` t4 ")
                            .append("WHERE t4.HourOfDay = HOUR(:currentLocalTime) AND t4.Date = DATE(:currentLocalTime)) ");
                }
                
                break;
            case WidgetConstants.RANGE_TYPE_LAST_7DAYS:
                /* Last 7 Days */
                query.append("AND t.Id >= (SELECT t3.Id FROM `Time` t3 ")
                        .append("WHERE t3.Date = DATE(DATE_SUB(:currentLocalTime, INTERVAL 7 DAY)) AND t3.QuarterOfDay = 0) ")
                        .append("AND t.Id <  (SELECT t4.Id FROM `Time` t4 ")
                        .append("WHERE t4.Date = DATE(:currentLocalTime) AND t4.QuarterOfDay = 0) ");
                break;
            case WidgetConstants.RANGE_TYPE_LAST_30DAYS:
                /* Last 30 Days */
                query.append("AND t.Id >= (SELECT t3.Id FROM `Time` t3 ")
                        .append("WHERE t3.Date = DATE(DATE_SUB(:currentLocalTime, INTERVAL 30 DAY)) AND t3.QuarterOfDay = 0) ")
                        .append("AND t.Id <  (SELECT t4.Id FROM `Time` t4 ")
                        .append("WHERE t4.Date = DATE(:currentLocalTime) AND t4.QuarterOfDay = 0) ");
                break;
            //            case WidgetConstants.RANGE_TYPE_THIS_WEEK:
            //                /* This Week */
            //                query.append("AND t.Date >= DATE(DATE_SUB(:currentLocalTime, INTERVAL 1 WEEK)) AND t.WeekOfYear = WEEK(:currentLocalTime,2) ");
            //                query.append("AND t.DateTime <= :currentLocalTime ");
            //                break;
            //            case WidgetConstants.RANGE_TYPE_THIS_MONTH:
            //                /* This Month */
            //                query.append("AND t.Year = YEAR(:currentLocalTime) AND t.Month = MONTH(:currentLocalTime) AND t.DateTime <= :currentLocalTime ");
            //                break;
            case WidgetConstants.RANGE_TYPE_LAST_WEEK:
                /* Last Week */
                query.append("AND t.Date >= DATE(DATE_SUB(:currentLocalTime, INTERVAL 2 WEEK)) ")
                        .append("AND t.WeekOfYear = WEEK(DATE_SUB(:currentLocalTime, INTERVAL 1 WEEK), 2) ");
                break;
            case WidgetConstants.RANGE_TYPE_LAST_MONTH:
                /* Last Month */
                query.append("AND t.Year = YEAR(DATE_SUB(:currentLocalTime, INTERVAL 1 MONTH)) ")
                        .append("AND t.Month = MONTH(DATE_SUB(:currentLocalTime, INTERVAL 1 MONTH)) ");
                break;
            case WidgetConstants.RANGE_TYPE_LAST_12MONTHS:
                /* Last 12 Months */
                query.append("AND t.Id >= (SELECT MIN(t3.Id) FROM `Time` t3 ")
                        .append("WHERE t3.Year = YEAR(DATE_SUB(:currentLocalTime, INTERVAL 12 MONTH)) ")
                        .append("AND t3.Month = MONTH(DATE_SUB(:currentLocalTime, INTERVAL 12 MONTH)) AND t3.QuarterOfDay = 0 AND t3.DayOfMonth = 1) ")
                        .append("AND t.Id <  (SELECT MIN(t4.Id) FROM `Time` t4 ")
                        .append("WHERE t4.Year = YEAR(:currentLocalTime) ")
                        .append("AND t4.Month = MONTH(:currentLocalTime) AND t4.DateTime <= :currentLocalTime AND t4.QuarterOfDay = 0 AND t4.DayOfMonth = 1) ");
                break;
            //            case WidgetConstants.RANGE_TYPE_THIS_YEAR:
            //                /* This Year */
            //                query.append("AND t.Year = YEAR(:currentLocalTime) AND t.DateTime <= :currentLocalTime ");
            //                break;
            case WidgetConstants.RANGE_TYPE_LAST_YEAR:
                /* Last Year */
                query.append("AND t.Year = YEAR(DATE_SUB(:currentLocalTime, INTERVAL 1 YEAR)) ");
                break;
            case WidgetConstants.RANGE_TYPE_YEAR_TO_DATE:
                /* Year to Date */
                query.append("AND t.Year = YEAR(:currentLocalTime) AND t.DateTime <= :currentLocalTime ");
                break;
            default:
                break;
        }
        
        if (usePartitionedTable) {
            switch (rangeTypeId) {
                case WidgetConstants.RANGE_TYPE_NA:
                case WidgetConstants.RANGE_TYPE_NOW:
                case WidgetConstants.RANGE_TYPE_TODAY:
                case WidgetConstants.RANGE_TYPE_YESTERDAY:
                case WidgetConstants.RANGE_TYPE_LAST_24HOURS:
                    break;
                case WidgetConstants.RANGE_TYPE_LAST_7DAYS:
                case WidgetConstants.RANGE_TYPE_LAST_30DAYS:
                    //                case WidgetConstants.RANGE_TYPE_THIS_WEEK:
                case WidgetConstants.RANGE_TYPE_LAST_WEEK:
                    query.append("AND t.QuarterOfDay = 0 ");
                    break;
                //                case WidgetConstants.RANGE_TYPE_THIS_MONTH:
                case WidgetConstants.RANGE_TYPE_LAST_MONTH:
                    if (tier1TypeId == WidgetConstants.TIER_TYPE_DAY) {
                        query.append("AND t.QuarterOfDay = 0 ");
                    } else {
                        query.append("AND t.QuarterOfDay = 0 AND t.DayOfMonth = 1 ");
                    }
                    break;
                case WidgetConstants.RANGE_TYPE_LAST_12MONTHS:
                    //                case WidgetConstants.RANGE_TYPE_THIS_YEAR:
                case WidgetConstants.RANGE_TYPE_LAST_YEAR:
                case WidgetConstants.RANGE_TYPE_YEAR_TO_DATE:
                    query.append("AND t.QuarterOfDay = 0 AND t.DayOfMonth = 1 ");
                    break;
                default:
                    break;
            }
        }
        
    }
    
    @Override
    public final void appendSelection(final StringBuilder query, final Widget widget) {
        query.append("SELECT wLabel.CustomerId AS CustomerId ");
        final int widgetMetricTypeId = widget.getWidgetMetricType().getId();
        final int tier1Id = widget.getWidgetTierTypeByWidgetTier1Type().getId();
        if (tier1Id != WidgetConstants.TIER_TYPE_NA) {
            switch (tier1Id) {
            /*
             * Not supported in EMS 7.0
             * case WebCoreConstants.WIDGET_TIER_TYPE_15MINUTES:
             * query.append(", t.DateTime AS Tier1TypeId, t.TimeAmPm AS Tier1TypeName ");
             * break;
             */
                case WidgetConstants.TIER_TYPE_HOUR:
                    /* Hour */
                    query.append(", wData.DayOfYear AS Tier1TypeId");
                    if (widgetMetricTypeId == WidgetConstants.METRIC_TYPE_PAID_OCCUPANCY
                        || widgetMetricTypeId == WidgetConstants.METRIC_TYPE_PHYSICAL_OCCUPANCY
                        || widgetMetricTypeId == WidgetConstants.METRIC_TYPE_COMPARE_OCCUPANCY) {
                        query.append(", (SELECT MIN(t2.DateTime) ");
                    } else {
                        query.append(", (SELECT TIMESTAMP(DATE_ADD(MIN(t2.DateTime), INTERVAL 1 HOUR)) ");
                    }
                    query.append("FROM Time t2 where t2.Date = wData.Date AND t2.HourOfDay = wData.HourOfDay) AS Tier1TypeName");
                    query.append(", 0 AS Tier1TypeHidden");
                    query.append(", wData.HourOfDay AS HourOfDay ");
                    query.append(", NULL AS StartTime");
                    
                    break;
                case WidgetConstants.TIER_TYPE_DAY:
                    /* Day */
                    query.append(", wData.DayOfYear AS Tier1TypeId");
                    query.append(", DATE_FORMAT(wData.Date, '%b %d, %Y') AS Tier1TypeName");
                    query.append(", 0 AS Tier1TypeHidden");
                    query.append(", NULL AS StartTime ");
                    
                    break;
                /*
                 * Not supported in EMS 7.0
                 * case WebCoreConstants.WIDGET_TIER_TYPE_WEEK:
                 * query.append(", t.Year AS Tier1TypeId, t.WeekOfYear AS Tier1TypeName ");
                 * break;
                 */
                case WidgetConstants.TIER_TYPE_MONTH:
                    /* Month */
                    query.append(", wData.Month AS Tier1TypeId");
                    query.append(", CONCAT(wData.MonthNameAbbrev,' ', wData.Year) AS Tier1TypeName");
                    query.append(", 0 AS Tier1TypeHidden");
                    query.append(", wData.Year AS Year");
                    query.append(", wData.Month AS Month ");
                    query.append(", NULL AS StartTime");
                    
                    break;
                default:
                    query.append(", wLabel.Tier1TypeId AS Tier1TypeId, wLabel.Tier1TypeName AS Tier1TypeName, wLabel.Tier1TypeHidden AS Tier1TypeHidden ");
                    if (tier1Id == WidgetConstants.TIER_TYPE_PAYSTATION) {
                        query.append(", wLabel.SerialNumber AS SerialNumber ");
                    }
                    
                    break;
            }
        }
        
        final int tier2Id = widget.getWidgetTierTypeByWidgetTier2Type().getId();
        if (tier2Id != WidgetConstants.TIER_TYPE_NA) {
            query.append(", wLabel.Tier2TypeId AS Tier2TypeId, wLabel.Tier2TypeName AS Tier2TypeName, wLabel.Tier2TypeHidden AS Tier2TypeHidden ");
        }
        
        final int tier3Id = widget.getWidgetTierTypeByWidgetTier3Type().getId();
        if (tier3Id != WidgetConstants.TIER_TYPE_NA) {
            query.append(", wLabel.Tier3TypeId AS Tier3TypeId, wLabel.Tier3TypeName AS Tier3TypeName, wLabel.Tier3TypeHidden AS Tier3TypeHidden ");
        }
    }
    
    @Override
    public final void appendLabelTable(final StringBuilder query, final Widget widget, final Map<Integer, StringBuilder> subsetMap,
        final boolean useETLLocationDetail, final Integer etlLocationDetailTypeId, final boolean isTierTypeTime) {
        final int tier1 = widget.getWidgetTierTypeByWidgetTier1Type().getId();
        final int tier2 = widget.getWidgetTierTypeByWidgetTier2Type().getId();
        final int tier3 = widget.getWidgetTierTypeByWidgetTier3Type().getId();
        final int metricType = widget.getWidgetMetricType().getId();
        final int filter = widget.getWidgetFilterType().getId();
        // For Rate and Top Bottom widget, we don't want NULL value.
        if (tier1 == WidgetConstants.TIER_TYPE_RATE || filter == WidgetConstants.FILTER_TYPE_TOP || filter == WidgetConstants.FILTER_TYPE_BOTTOM
            || metricType == WidgetConstants.METRIC_TYPE_OCCUPANCY_MAP && !useETLLocationDetail) {
            query.append("INNER JOIN ");
        } else {
            query.append("RIGHT JOIN ");
        }
        
        query.append("( SELECT c.Id AS CustomerId ");
        
        final StringBuilder from = new StringBuilder("FROM Customer c ");
        final StringBuilder where = new StringBuilder();
        final StringBuilder on = new StringBuilder();
        final List<String> groupBy = new ArrayList<String>();
        final Set<Integer> selectedTierTypes = new HashSet<Integer>(5);
        
        appendLabelTableTier(query, from, where, on, groupBy, WidgetConstants.TIER_1, tier1, widget, subsetMap, selectedTierTypes);
        appendLabelTableTier(query, from, where, on, groupBy, WidgetConstants.TIER_2, tier2, widget, subsetMap, selectedTierTypes);
        appendLabelTableTier(query, from, where, on, groupBy, WidgetConstants.TIER_3, tier3, widget, subsetMap, selectedTierTypes);
        if (metricType == WidgetConstants.METRIC_TYPE_OCCUPANCY_MAP) {
            query.append(", llg.JSONString AS JsonString ");
            from.append("INNER JOIN LocationLineGeopoint llg ON (l.Id = llg.locationId) ");
        }
        
        if (useETLLocationDetail) {
            query.append(", SUM(wLocationDetail.NumberOfSpaces) AS NumberOfSpaces ");
            query.append(", SUM(wLocationDetail.TotalNumberOfSpaces) AS TotalNumberOfSpaces ");
            query.append(", SUM(wLocationDetail.MaxUtilizationMinutes) AS MaxUtilizationMinutes ");
            if (isTierTypeTime) {
                query.append(", wLocationDetail.TimeId AS TimeId ");
                on.append("AND wData.TimeId = wLabel.TimeId ");
                groupBy.add("wLocationDetail.TimeId ");
            }
            if (tier1 != WidgetConstants.TIER_TYPE_LOCATION && tier2 != WidgetConstants.TIER_TYPE_LOCATION
                && tier3 != WidgetConstants.TIER_TYPE_LOCATION && tier1 != WidgetConstants.TIER_TYPE_PARENT_LOCATION
                && tier2 != WidgetConstants.TIER_TYPE_PARENT_LOCATION && tier3 != WidgetConstants.TIER_TYPE_PARENT_LOCATION) {
                from.append("INNER JOIN Location l ON(c.Id = l.CustomerId) ");
            }
            from.append("INNER JOIN (SELECT eld.LocationId, SUM(eld.NumberOfSpaces) AS NumberOfSpaces, ")
                    .append("SUM(eld.TotalNumberOfSpaces) AS TotalNumberOfSpaces, SUM(eld.MaxUtilizationMinutes) AS MaxUtilizationMinutes ");
            if (isTierTypeTime) {
                from.append(", t.Id AS TimeId ");
            }
            from.append("FROM Time t ");
            from.append("INNER JOIN ETLLocationDetail eld ON (t.Id = eld.TimeIdLocal) ");
            from.append("WHERE eld.CustomerId IN(:customerId) ");
            from.append("AND eld.ETLLocationDetailTypeId = ");
            from.append(etlLocationDetailTypeId);
            from.append(StandardConstants.STRING_EMPTY_SPACE);
            calculateRange(widget, 0, from, widget.getWidgetRangeType().getId(), widget.getWidgetTierTypeByWidgetTier1Type().getId(), true, false);
            from.append("GROUP BY eld.LocationId");
            if (isTierTypeTime) {
                from.append(", t.Id ");
            }
            from.append(") AS wLocationDetail ON (wLocationDetail.LocationId = l.Id) ");
        }
        
        query.append(from);
        
        //        if(selectedTierTypes.contains(WidgetConstants.TIER_TYPE_REVENUE_TYPE) && (!subsetMap.containsKey(WidgetConstants.TIER_TYPE_REVENUE_TYPE))) {
        //          where.append("AND rt.Level = 2 ");
        //        }
        
        query.append("WHERE c.Id IN(:customerId) ");
        //        if(selectedTierTypes.contains(WidgetConstants.TIER_TYPE_ORG)) {
        //          query.append("WHERE c.ParentCustomerId IN(:customerId) ");
        //        }
        //        else {
        //          query.append("WHERE c.Id IN(:customerId) ");
        //        }
        
        query.append(where);
        
        if (!groupBy.isEmpty()) {
            query.append("GROUP BY ");
            query.append(groupBy.get(0));
            if (groupBy.size() > 1) {
                for (int i = 1; i < groupBy.size(); i++) {
                    query.append(StandardConstants.STRING_COMMA).append(StandardConstants.STRING_EMPTY_SPACE);
                    query.append(groupBy.get(i));
                }
            }
        }
        query.append(") AS wLabel ");
        query.append("ON(wData.CustomerId = wLabel.CustomerId ");
        query.append(on);
        query.append(") ");
    }
    
    private void appendLabelTableTier(final StringBuilder select, final StringBuilder from, final StringBuilder where, final StringBuilder on,
        final List<String> groupBy, final int tierNumber, final int tierTypeId, final Widget widget, final Map<Integer, StringBuilder> subsetMap,
        final Set<Integer> selectedTierTypes) {
        final StringBuilder subsetIds = subsetMap.get(tierTypeId);
        switch (tierTypeId) {
            case WidgetConstants.TIER_TYPE_NA:
                /* N/A */
                break;
            /*
             * Not supported in EMS 7.0
             * case WebCoreConstants.WIDGET_TIER_TYPE_15MINUTES:
             * query.append(", t.DateTime AS Tier1TypeId, t.TimeAmPm AS Tier1TypeName ");
             * break;
             */
            case WidgetConstants.TIER_TYPE_HOUR:
                /* Hour */
            case WidgetConstants.TIER_TYPE_DAY:
                /* Day */
            case WidgetConstants.TIER_TYPE_MONTH:
                /* Month */
                break;
            case WidgetConstants.TIER_TYPE_ROUTE:
                /* Route */
                select.append(", ro.Id AS Tier");
                select.append(tierNumber);
                select.append("TypeId");
                select.append(", ro.Name AS Tier");
                select.append(tierNumber);
                select.append("TypeName");
                
                select.append(", 0 AS Tier");
                select.append(tierNumber);
                select.append("TypeHidden");
                
                select.append(", rp.PointOfSaleId AS PointOfSaleId, rp.RouteId AS RouteId ");
                
                from.append("INNER JOIN Route ro ON(c.Id = ro.CustomerId) ");
                from.append("INNER JOIN RoutePOS rp ON(ro.Id = rp.RouteId) ");
                from.append("INNER JOIN PointOfSale pos ON(rp.PointOfSaleId = pos.Id) ");
                from.append("INNER JOIN POSStatus posStatus ON(pos.Id = posStatus.PointOfSaleId) ");
                
                where.append("AND posStatus.IsDeleted = 0 ");
                if (subsetIds != null) {
                    where.append("AND ro.Id IN(");
                    where.append(subsetIds);
                    where.append(") ");
                }
                
                on.append("AND wData.PointOfSaleId = wLabel.PointOfSaleId AND wData.RouteId = wLabel.RouteId ");
                
                groupBy.add("ro.Id, pos.Id");
                
                break;
            case WidgetConstants.TIER_TYPE_PAYSTATION:
                /* Pay Station */
                select.append(", pos.Id AS Tier");
                select.append(tierNumber);
                select.append("TypeId");
                select.append(", pos.Name AS Tier");
                select.append(tierNumber);
                select.append("TypeName");
                
                select.append(", ((posStatus.IsActivated | posStatus.IsVisible) <> 1) | posStatus.IsDecommissioned AS Tier");
                select.append(tierNumber);
                select.append("TypeHidden");
                
                select.append(", pos.Serialnumber AS SerialNumber ");
                select.append(", pos.Id AS PointOfSaleId ");
                
                from.append("INNER JOIN PointOfSale pos ON(c.Id = pos.CustomerId) ");
                from.append("INNER JOIN POSStatus posStatus ON(pos.Id = posStatus.PointOfSaleId) ");
                
                where.append("AND posStatus.IsDeleted = 0 ");
                if (subsetIds != null) {
                    where.append("AND pos.Id IN(");
                    where.append(subsetIds);
                    where.append(") ");
                }
                
                on.append("AND wData.PointOfSaleId = wLabel.Tier");
                on.append(tierNumber);
                on.append("TypeId ");
                groupBy.add("pos.Id");
                
                break;
            case WidgetConstants.TIER_TYPE_LOCATION:
                /* Location */
                select.append(", l.Id AS Tier");
                select.append(tierNumber);
                select.append("TypeId");
                select.append(", l.Name AS Tier");
                select.append(tierNumber);
                select.append("TypeName ");
                
                select.append(", l.IsUnassigned | l.IsDeleted AS Tier");
                select.append(tierNumber);
                select.append("TypeHidden ");
                
                from.append("INNER JOIN Location l ON(c.Id = l.CustomerId) ");
                
                where.append("AND l.IsDeleted = 0 AND l.IsParent = 0 ");
                
                if (subsetIds != null) {
                    where.append("AND l.Id IN(");
                    where.append(subsetIds);
                    where.append(") ");
                }
                
                on.append("AND wData.LocationId = wLabel.Tier");
                on.append(tierNumber);
                on.append("TypeId ");
                
                groupBy.add("l.Id");
                break;
            case WidgetConstants.TIER_TYPE_PARENT_LOCATION:
                /* Parent Location */
                if (widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_OCCUPANCY_MAP) {
                    select.append(", l.Id AS Tier");
                    select.append(tierNumber);
                    select.append("TypeId");
                    select.append(", l.Name AS Tier");
                    select.append(tierNumber);
                    select.append("TypeName");
                    
                    select.append(", l.IsUnassigned AS Tier");
                    select.append(tierNumber);
                    select.append("TypeHidden");
                    
                    on.append("AND wData.LocationId = wLabel.Tier").append(tierNumber).append("TypeId ");
                    
                    groupBy.add("l.Id");
                } else {
                    select.append(", pl.Id AS Tier");
                    select.append(tierNumber);
                    select.append("TypeId");
                    select.append(", pl.Name AS Tier");
                    select.append(tierNumber);
                    select.append("TypeName");
                    
                    select.append(", l.IsUnassigned AS Tier");
                    select.append(tierNumber);
                    select.append("TypeHidden");
                    select.append(", l.Id AS LocationId ");
                    
                    on.append("AND wData.ParentLocationId = wLabel.Tier").append(tierNumber).append("TypeId ");
                    
                    groupBy.add("pl.Id");
                }
                from.append("INNER JOIN Location pl ON(c.Id = pl.CustomerId AND pl.IsParent = 1 AND pl.IsDeleted = 0) ");
                from.append("INNER JOIN Location l ON(pl.Id = l.ParentLocationId AND l.IsDeleted = 0 AND l.IsParent = 0) ");
                
                if (subsetIds != null) {
                    where.append("AND pl.Id IN(");
                    where.append(subsetIds);
                    where.append(") ");
                }
                
                break;
            case WidgetConstants.TIER_TYPE_REVENUE_TYPE:
                /* Revenue Type */
                select.append(", rt.Id AS Tier");
                select.append(tierNumber);
                select.append("TypeId");
                select.append(", rt.Name as Tier");
                select.append(tierNumber);
                select.append("TypeName ");
                
                select.append(", 0 AS Tier");
                select.append(tierNumber);
                select.append("TypeHidden ");
                
                from.append("INNER JOIN RevenueType rt ");
                
                if (subsetIds != null) {
                    where.append("AND rt.Id IN(");
                    where.append(subsetIds);
                    where.append(") ");
                }
                
                on.append("AND wData.RevenueTypeId = wLabel.Tier");
                on.append(tierNumber);
                on.append("TypeId ");
                
                groupBy.add("rt.Id");
                
                break;
            case WidgetConstants.TIER_TYPE_TRANSACTION_TYPE:
                /* Transaction Type */
                select.append(", tt.Id AS Tier");
                select.append(tierNumber);
                select.append("TypeId");
                select.append(", tt.Name AS Tier");
                select.append(tierNumber);
                select.append("TypeName ");
                
                select.append(", 0 AS Tier");
                select.append(tierNumber);
                select.append("TypeHidden ");
                
                from.append("INNER JOIN TransactionType tt ");
                
                if (subsetIds != null) {
                    where.append("AND tt.Id IN(");
                    where.append(subsetIds);
                    where.append(") ");
                }
                
                on.append("AND wData.TransactionTypeId = wLabel.Tier");
                on.append(tierNumber);
                on.append("TypeId ");
                
                groupBy.add("tt.Id");
                
                break;
            case WidgetConstants.TIER_TYPE_COLLECTION_TYPE:
                /* Collection Type */
                select.append(", ct.Id AS Tier");
                select.append(tierNumber);
                select.append("TypeId");
                select.append(", ct.Name AS Tier");
                select.append(tierNumber);
                select.append("TypeName ");
                
                select.append(", 0 AS Tier");
                select.append(tierNumber);
                select.append("TypeHidden ");
                
                from.append("INNER JOIN CollectionType ct ");
                
                if (subsetIds != null) {
                    where.append("AND ct.Id IN(");
                    where.append(subsetIds);
                    where.append(") ");
                }
                
                on.append("AND wData.CollectionTypeId = wLabel.Tier");
                on.append(tierNumber);
                on.append("TypeId ");
                
                groupBy.add("ct.Id");
                
                break;
            case WidgetConstants.TIER_TYPE_RATE:
                /* UnifiedRate */
                select.append(", ra.Id AS Tier");
                select.append(tierNumber);
                select.append("TypeId");
                select.append(", ra.Name AS Tier");
                select.append(tierNumber);
                select.append("TypeName ");
                
                select.append(", 0 AS Tier");
                select.append(tierNumber);
                select.append("TypeHidden ");
                
                from.append("INNER JOIN UnifiedRate ra ON(c.Id = ra.CustomerId) ");
                
                if (subsetIds != null) {
                    where.append("AND ra.Id IN(");
                    where.append(subsetIds);
                    where.append(") ");
                }
                
                on.append("AND wData.UnifiedRateId = wLabel.Tier");
                on.append(tierNumber);
                on.append("TypeId ");
                
                groupBy.add("ra.Id");
                
                break;
            case WidgetConstants.TIER_TYPE_MERCHANT_ACCOUNT:
                /* MerchantAccount */
                select.append(", ma.Id AS Tier");
                select.append(tierNumber);
                select.append("TypeId");
                select.append(", IF(ma.TerminalName IS NULL OR ma.TerminalName = '', ma.Name, ma.TerminalName) AS Tier");
                select.append(tierNumber);
                select.append("TypeName ");
                
                select.append(", 0 AS Tier");
                select.append(tierNumber);
                select.append("TypeHidden ");
                
                from.append("INNER JOIN MerchantAccount ma ON(c.Id = ma.CustomerId) ");
                
                if (subsetIds != null) {
                    where.append("AND ma.Id IN(");
                    where.append(subsetIds);
                    where.append(") ");
                }
                
                on.append("AND wData.MerchantAccountId = wLabel.Tier");
                on.append(tierNumber);
                on.append("TypeId ");
                
                groupBy.add("ma.Id");
                
                break;
            case WidgetConstants.TIER_TYPE_ORG:
                /* Organization */
                where.append("AND ParentCustomerId IS NOT NULL ");
            case WidgetConstants.TIER_TYPE_ALL_ORG:
                /* All Organizations */
                select.append(", c.Id AS Tier");
                select.append(tierNumber);
                select.append("TypeId");
                select.append(", c.Name AS Tier");
                select.append(tierNumber);
                select.append("TypeName");
                
                select.append(", 0 AS Tier");
                select.append(tierNumber);
                select.append("TypeHidden");
                
                select.append(", c.ParentCustomerId AS ParentCustomerId ");
                
                if (subsetIds != null) {
                    where.append("AND c.Id IN(");
                    where.append(subsetIds);
                    where.append(") ");
                }
                
                on.append("AND wData.CustomerId = wLabel.Tier");
                on.append(tierNumber);
                on.append("TypeId ");
                
                groupBy.add("c.Id");
                
                break;
            case WidgetConstants.TIER_TYPE_ALERT_TYPE:
                /* Collection Type */
                select.append(", act.Id AS Tier");
                select.append(tierNumber);
                select.append("TypeId");
                select.append(", act.Name AS Tier");
                select.append(tierNumber);
                select.append("TypeName ");
                
                select.append(", 0 AS Tier");
                select.append(tierNumber);
                select.append("TypeHidden ");
                
                from.append("INNER JOIN AlertClassType act ");
                
                if (subsetIds != null) {
                    where.append("AND act.Id IN(");
                    where.append(subsetIds);
                    where.append(") ");
                }
                
                on.append("AND wData.AlertClassTypeId = wLabel.Tier");
                on.append(tierNumber);
                on.append("TypeId ");
                
                groupBy.add("act.Id");
                
                break;
            case WidgetConstants.TIER_TYPE_CARD_PROCESS_METHOD_TYPE:
                /* Card Process Method Type */
                select.append(", cpmt.Id AS Tier");
                select.append(tierNumber);
                select.append("TypeId, cpmt.Name as Tier");
                select.append(tierNumber);
                select.append("TypeName, 0 AS Tier");
                select.append(tierNumber);
                select.append("TypeHidden ");
                
                from.append("INNER JOIN CardProcessMethodType cpmt ");
                
                if (subsetIds != null && subsetIds.length() > 0) {
                    where.append("AND cpmt.Id IN (");
                    where.append(subsetIds);
                    where.append(") ");
                }
                
                on.append("AND wData.CardProcessMethodTypeId = wLabel.Tier");
                on.append(tierNumber);
                on.append("TypeId ");
                
                groupBy.add("cpmt.Id");
                break;
            case WidgetConstants.TIER_TYPE_CITATION_TYPE:
                /* Location */
                select.append(", ct.Id AS Tier");
                select.append(tierNumber);
                select.append("TypeId");
                select.append(", ct.Name AS Tier");
                select.append(tierNumber);
                select.append("TypeName ");
                
                select.append(", 0 AS Tier");
                select.append(tierNumber);
                select.append("TypeHidden ");
                //select.append("TypeHidden, ct.FlexCitationTypeId AS FlexCitationTypeId ");                
                
                from.append("INNER JOIN CitationType ct ON(c.Id = ct.CustomerId) ");
                
                if (subsetIds != null) {
                    where.append("AND ct.Id IN(");
                    where.append(subsetIds);
                    where.append(") ");
                }
                
                on.append("AND wData.CitationTypeId = wLabel.Tier");
                on.append(tierNumber);
                on.append("TypeId ");
                
                groupBy.add("ct.Id");
                break;
            default:
                break;
        }
        
        selectedTierTypes.add(tierTypeId);
    }
    
    @Override
    public final Map<Integer, StringBuilder> createTierSubsetMap(final Widget widget) {
        final Map<Integer, StringBuilder> subsetMap = new HashMap<Integer, StringBuilder>();
        for (WidgetSubsetMember subsetMember : widget.getWidgetSubsetMembers()) {
            StringBuilder subsetIds = subsetMap.get(subsetMember.getWidgetTierType().getId());
            if (subsetIds != null) {
                subsetIds.append(",");
            } else {
                subsetIds = new StringBuilder();
                subsetMap.put(subsetMember.getWidgetTierType().getId(), subsetIds);
            }
            
            subsetIds.append(subsetMember.getMemberId());
        }
        
        return subsetMap;
    }
    
    private void appendGrouping(final List<String> groupBy, final int tierNumber, final int tierTypeId) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("wLabel.Tier").append(tierNumber).append("TypeId");
        
        switch (tierTypeId) {
            case WidgetConstants.TIER_TYPE_NA:
                /* N/A */
                break;
            case WidgetConstants.TIER_TYPE_HOUR:
                /* Hour */
                groupBy.add("wData.Date, wData.HourOfDay");
                break;
            case WidgetConstants.TIER_TYPE_DAY:
                /* Day */
                groupBy.add("wData.Date");
                break;
            case WidgetConstants.TIER_TYPE_MONTH:
                /* Month */
                groupBy.add("wData.Year, wData.Month");
                break;
            case WidgetConstants.TIER_TYPE_ROUTE:
                /* Route */
            case WidgetConstants.TIER_TYPE_PAYSTATION:
                /* Pay Station */
            case WidgetConstants.TIER_TYPE_LOCATION:
                /* Location */
            case WidgetConstants.TIER_TYPE_PARENT_LOCATION:
                /* Parent Location */
            case WidgetConstants.TIER_TYPE_REVENUE_TYPE:
                /* Revenue Type */
            case WidgetConstants.TIER_TYPE_TRANSACTION_TYPE:
                /* Transaction Type */
            case WidgetConstants.TIER_TYPE_COLLECTION_TYPE:
                /* Collection Type */
            case WidgetConstants.TIER_TYPE_RATE:
                /* UnifiedRate */
            case WidgetConstants.TIER_TYPE_MERCHANT_ACCOUNT:
                /* Merchant Account */
            case WidgetConstants.TIER_TYPE_ORG:
                /* Organization */
            case WidgetConstants.TIER_TYPE_ALL_ORG:
                /* All Organizations */
            case WidgetConstants.TIER_TYPE_ALERT_TYPE:
                /* Alert Type */
                groupBy.add("wLabel.Tier" + tierNumber + "TypeId");
                break;
            case WidgetConstants.TIER_TYPE_CARD_PROCESS_METHOD_TYPE:
                /* Card Processing Method Type */
            case WidgetConstants.TIER_TYPE_CITATION_TYPE:
                /* Citation Type */
                groupBy.add(bdr.toString());
                break;
            default:
                break;
        }
    }
    
    @Override
    public final void appendGrouping(final StringBuilder query, final Widget widget) {
        final List<String> groupBy = new ArrayList<String>();
        appendGrouping(groupBy, WidgetConstants.TIER_1, widget.getWidgetTierTypeByWidgetTier1Type().getId());
        appendGrouping(groupBy, WidgetConstants.TIER_2, widget.getWidgetTierTypeByWidgetTier2Type().getId());
        appendGrouping(groupBy, WidgetConstants.TIER_3, widget.getWidgetTierTypeByWidgetTier3Type().getId());
        
        if (!groupBy.isEmpty()) {
            query.append("GROUP BY ");
            for (int groupIdx = 0; groupIdx < groupBy.size(); groupIdx++) {
                query.append(groupBy.get(groupIdx));
                if (groupIdx != groupBy.size() - 1) {
                    query.append(StandardConstants.STRING_COMMA).append(StandardConstants.STRING_EMPTY_SPACE);
                }
            }
            
            query.append(StandardConstants.STRING_EMPTY_SPACE);
        }
    }
    
    private void appendOrdering(final List<String> orderBy, final int tierNumber, final int tierTypeId) {
        switch (tierTypeId) {
            case WidgetConstants.TIER_TYPE_NA:
                /* N/A */
                break;
            case WidgetConstants.TIER_TYPE_HOUR:
                /* Hour */
                orderBy.add("wData.Date, wData.HourOfDay");
                break;
            case WidgetConstants.TIER_TYPE_DAY:
                /* Day */
                orderBy.add("wData.Date");
                break;
            case WidgetConstants.TIER_TYPE_MONTH:
                /* Month */
                orderBy.add("wData.Year, wData.Month");
                break;
            case WidgetConstants.TIER_TYPE_ROUTE:
                /* Route */
            case WidgetConstants.TIER_TYPE_PAYSTATION:
                /* Pay Station */
            case WidgetConstants.TIER_TYPE_LOCATION:
                /* Location */
            case WidgetConstants.TIER_TYPE_PARENT_LOCATION:
                /* Parent Location */
            case WidgetConstants.TIER_TYPE_REVENUE_TYPE:
                /* Revenue Type */
            case WidgetConstants.TIER_TYPE_TRANSACTION_TYPE:
                /* Transaction Type */
            case WidgetConstants.TIER_TYPE_COLLECTION_TYPE:
                /* Collection Type */
            case WidgetConstants.TIER_TYPE_RATE:
                /* UnifiedRate */
            case WidgetConstants.TIER_TYPE_MERCHANT_ACCOUNT:
                /* Merchant Account */
            case WidgetConstants.TIER_TYPE_ORG:
                /* Organization */
            case WidgetConstants.TIER_TYPE_ALL_ORG:
                /* All Organizations */
            case WidgetConstants.TIER_TYPE_CITATION_TYPE:
                orderBy.add("Tier" + tierNumber + "TypeName");
                break;
            case WidgetConstants.TIER_TYPE_ALERT_TYPE:
                /* Alert Type */
            case WidgetConstants.TIER_TYPE_CARD_PROCESS_METHOD_TYPE:
                /* Card Processing Method Type */
            default:
                break;
        }
    }
    
    @Override
    public final void appendOrdering(final StringBuilder query, final Widget widget) {
        final List<String> orderBy = new ArrayList<String>();
        
        if (widget.getWidgetFilterType().getId() == WidgetConstants.FILTER_TYPE_TOP) {
            orderBy.add("WidgetMetricValue DESC");
        } else if (widget.getWidgetFilterType().getId() == WidgetConstants.FILTER_TYPE_BOTTOM) {
            orderBy.add("WidgetMetricValue ASC");
        }
        
        appendOrdering(orderBy, WidgetConstants.TIER_1, widget.getWidgetTierTypeByWidgetTier1Type().getId());
        appendOrdering(orderBy, WidgetConstants.TIER_2, widget.getWidgetTierTypeByWidgetTier2Type().getId());
        appendOrdering(orderBy, WidgetConstants.TIER_3, widget.getWidgetTierTypeByWidgetTier3Type().getId());
        
        if (!orderBy.isEmpty()) {
            query.append("ORDER BY ");
            for (int orderIdx = 0; orderIdx < orderBy.size(); orderIdx++) {
                query.append(orderBy.get(orderIdx));
                if (orderIdx != orderBy.size() - 1) {
                    query.append(StandardConstants.STRING_COMMA).append(StandardConstants.STRING_EMPTY_SPACE);
                }
            }
            
            query.append(StandardConstants.STRING_EMPTY_SPACE);
        }
    }
    
    @Override
    public final void appendLimit(final StringBuilder query, final Widget widget, final String queryLimit) {
        final int filterId = widget.getWidgetFilterType().getId();
        
        switch (filterId) {
            case WidgetConstants.FILTER_TYPE_TOP:
                query.append(" LIMIT :N ; ");
                break;
            case WidgetConstants.FILTER_TYPE_BOTTOM:
                query.append(" LIMIT :N ; ");
                break;
            default:
                /*
                 * used for - WIDGET_FILTER_TYPE_ALL
                 * - WIDGET_FILTER_TYPE_SUBSET
                 */
                query.append(" LIMIT " + queryLimit + "; ");
                break;
        }
    }
    
    @Override
    public final boolean appendFieldMember(final StringBuilder query, final Widget widget, final Map<Integer, String> tierTypesMembersMap,
        final Map<Integer, String> tierTypesAliasesMap, final boolean isForGroupBy) {
        
        boolean isCardProcessingWidget = false;
        if (widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_CARD_PROCESSING_REV
            || widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_CARD_PROCESSING_TX) {
            isCardProcessingWidget = true;
        }
        final int chartTypeId = widget.getWidgetChartType().getId();
        appendFieldMemberTier(query, widget.getWidgetTierTypeByWidgetTier1Type().getId(), tierTypesMembersMap, tierTypesAliasesMap, isForGroupBy,
                              isCardProcessingWidget, chartTypeId);
        appendFieldMemberTier(query, widget.getWidgetTierTypeByWidgetTier2Type().getId(), tierTypesMembersMap, tierTypesAliasesMap, isForGroupBy,
                              isCardProcessingWidget, chartTypeId);
        appendFieldMemberTier(query, widget.getWidgetTierTypeByWidgetTier3Type().getId(), tierTypesMembersMap, tierTypesAliasesMap, isForGroupBy,
                              isCardProcessingWidget, chartTypeId);
        
        boolean isTierTypeTime = false;
        if (!isForGroupBy) {
            isTierTypeTime = widget.getWidgetTierTypeByWidgetTier1Type().getId().intValue() == WidgetConstants.TIER_TYPE_HOUR
                             || widget.getWidgetTierTypeByWidgetTier1Type().getId().intValue() == WidgetConstants.TIER_TYPE_DAY
                             || widget.getWidgetTierTypeByWidgetTier1Type().getId().intValue() == WidgetConstants.TIER_TYPE_MONTH;
            
            if (isTierTypeTime) {
                query.append(", t.Id AS TimeId ");
            }
        }
        return isTierTypeTime;
    }
    
    private void appendFieldMemberTier(final StringBuilder query, final int tierTypeId, final Map<Integer, String> tierTypesMembersMap,
        final Map<Integer, String> tierTypesAliasesMap, final boolean isForGroupBy, final boolean isCardProcessingWidget, final int chartTypeId) {
        final String expr = tierTypesMembersMap.get(tierTypeId);
        final String alias = (tierTypesAliasesMap == null) ? null : tierTypesAliasesMap.get(tierTypeId);
        if (isForGroupBy) {
            if (WidgetConstants.TIER_TYPE_PARENT_LOCATION == tierTypeId) {
                if (WidgetConstants.CHART_TYPE_OCCUPANCY_MAP == chartTypeId && expr != null) {
                    // bugfix/IRIS-5773-Occupancy-Map-by-Location(P)-calculation
                    query.append(StandardConstants.STRING_COMMA).append(StandardConstants.STRING_EMPTY_SPACE);
                    query.append(expr);
                }
                query.append(", pl.Id");
            } else {
                if (expr != null) {
                    query.append(StandardConstants.STRING_COMMA).append(StandardConstants.STRING_EMPTY_SPACE);
                    query.append(expr);
                }
            }
        } else {
            if (expr != null) {
                query.append(StandardConstants.STRING_COMMA).append(StandardConstants.STRING_EMPTY_SPACE);
                query.append(expr);
                if (alias != null) {
                    query.append(" AS ");
                    query.append(alias);
                    query.append(StandardConstants.STRING_EMPTY_SPACE);
                }
            }
            if (WidgetConstants.TIER_TYPE_PARENT_LOCATION == tierTypeId && !isCardProcessingWidget) {
                query.append(", pl.Id AS ParentLocationId");
            } else if (WidgetConstants.TIER_TYPE_PARENT_LOCATION == tierTypeId && isCardProcessingWidget) {
                query.append(", pl.ParentLocationId AS ParentLocationId");
            }
        }
    }
    
    @Override
    public final void appendDataConditions(final StringBuilder query, final Widget widget, final String customerIdField,
        final Map<Integer, String> tierTypesFieldsMap, final Map<Integer, StringBuilder> subsetMap) {
        final StringBuilder where = new StringBuilder("WHERE ");
        where.append(customerIdField);
        where.append(" IN(:customerId) ");
        
        appendDataConditionsTier(query, where, widget.getWidgetTierTypeByWidgetTier1Type().getId(), tierTypesFieldsMap, subsetMap);
        appendDataConditionsTier(query, where, widget.getWidgetTierTypeByWidgetTier2Type().getId(), tierTypesFieldsMap, subsetMap);
        appendDataConditionsTier(query, where, widget.getWidgetTierTypeByWidgetTier3Type().getId(), tierTypesFieldsMap, subsetMap);
        
        query.append(where);
    }
    
    public final void appendDataConditionsTier(final StringBuilder from, final StringBuilder where, final int tierTypeId,
        final Map<Integer, String> tierTypesFieldsMap, final Map<Integer, StringBuilder> subsetMap) {
        final String field = tierTypesFieldsMap.get(tierTypeId);
        final StringBuilder subsetIds = subsetMap.get(tierTypeId);
        if (field != null) {
            switch (tierTypeId) {
                case WidgetConstants.TIER_TYPE_NA:
                    /* N/A */
                    break;
                case WidgetConstants.TIER_TYPE_ROUTE:
                    /* Route */
                    final String posField = tierTypesFieldsMap.get(WidgetConstants.TIER_TYPE_PAYSTATION);
                    if (posField != null) {
                        from.append("INNER JOIN RoutePOS rp ON(");
                        from.append(posField);
                        from.append(" = rp.PointOfSaleId) ");
                        
                        if (subsetIds != null) {
                            where.append("AND rp.RouteId IN(");
                            where.append(subsetIds);
                            where.append(") ");
                        }
                    }
                    
                    break;
                case WidgetConstants.TIER_TYPE_PARENT_LOCATION:
                    /* Parent Location */
                    
                    from.append("INNER JOIN Location l ON(");
                    from.append(field);
                    from.append(" = l.Id AND l.IsParent = 0 AND l.IsDeleted = 0) ");
                    from.append("INNER JOIN Location pl ON (pl.Id = l.ParentLocationId) ");
                    
                    if (subsetIds != null) {
                        where.append("AND l.ParentLocationId IN(");
                        where.append(subsetIds);
                        where.append(") ");
                    }
                    
                    break;
                default:
                    if (subsetIds != null) {
                        where.append("AND ");
                        where.append(field);
                        where.append(" IN(");
                        where.append(subsetIds);
                        where.append(") ");
                    }
                    
                    break;
            }
        }
    }
}
