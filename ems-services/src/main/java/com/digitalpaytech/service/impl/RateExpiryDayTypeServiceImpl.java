package com.digitalpaytech.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.RateExpiryDayType;
import com.digitalpaytech.service.RateExpiryDayTypeService;

@Service("rateExpiryDayTypeService")
@Transactional(propagation = Propagation.SUPPORTS)
public class RateExpiryDayTypeServiceImpl implements RateExpiryDayTypeService {
    
    @Autowired
    private EntityDao entityDao;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public final RateExpiryDayType findRateExpiryDayTypeById(final Byte rateExpiryDayTypeId) {
        return this.entityDao.get(RateExpiryDayType.class, rateExpiryDayTypeId);
    }
    
}
