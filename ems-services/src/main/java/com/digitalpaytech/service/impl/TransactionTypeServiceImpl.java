package com.digitalpaytech.service.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.TransactionType;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.TransactionTypeService;

@Service("transactionTypeService")
public class TransactionTypeServiceImpl implements TransactionTypeService {
    
    @Autowired
    private EntityService entityService;
    
    private List<TransactionType> transactionTypes;
    private Map<Integer, TransactionType> transactionTypesMap;
    private Map<String, TransactionType> transactionTypeNamesMap;
    
    @Override
    public final List<TransactionType> loadAll() {
        if (this.transactionTypes == null) {
            this.transactionTypes = this.entityService.loadAll(TransactionType.class);
        }
        return this.transactionTypes;
    }
    
    @Override
    public final Map<Integer, TransactionType> getTransactionTypesMap() {
        loadStaticTables();
        
        return this.transactionTypesMap;
    }
    
    @Override
    public TransactionType findTransactionType(final String name) {
        loadStaticTables();
        
        return this.transactionTypeNamesMap.get(name.toLowerCase());
    }
    
    public final void setEntityService(final EntityService entityService) {
        this.entityService = entityService;
    }
    
    @Override
    public final String getText(final int transactionId) {
        loadStaticTables();
        
        if (!this.transactionTypesMap.containsKey(transactionId)) {
            return null;
        } else {
            return this.transactionTypesMap.get(transactionId).getName();
        }
    }
    
    private void loadStaticTables() {
        if (this.transactionTypes == null) {
            loadAll();
        }
        if (this.transactionTypesMap == null) {
            this.transactionTypesMap = new HashMap<Integer, TransactionType>(this.transactionTypes.size());
            this.transactionTypeNamesMap = new HashMap<String, TransactionType>(this.transactionTypes.size());
            final Iterator<TransactionType> iter = this.transactionTypes.iterator();
            while (iter.hasNext()) {
                final TransactionType transactionType = iter.next();
                this.transactionTypesMap.put(transactionType.getId(), transactionType);
                this.transactionTypeNamesMap.put(transactionType.getName().toLowerCase(), transactionType);
            }
        }
    }
}
