package com.digitalpaytech.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Carrier;
import com.digitalpaytech.service.CarrierService;

@Service("carrierService")
@Transactional(propagation=Propagation.SUPPORTS)
public class CarrierServiceImpl implements CarrierService {
    
    @Autowired
    private EntityDao entityDao;
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public List<Carrier> loadAll() {
        return entityDao.loadAll(Carrier.class);
    }
    
    @Override
    public Carrier findCarrier(String name) {
        return (Carrier) this.entityDao.findUniqueByNamedQueryAndNamedParam("Carrier.findByName", "name", name);
    }
    
    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void save(Carrier carrier) {
        this.entityDao.save(carrier);
    }
}
