package com.digitalpaytech.service.impl;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CcFailLog;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.service.CcFailLogService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.MerchantAccountService;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

@Service("ccFailLogService")
@Transactional(propagation=Propagation.REQUIRED)
public class CcFailLogServiceImpl implements CcFailLogService {

    private static final Logger LOG = Logger.getLogger(CcFailLogServiceImpl.class);
    
    @Autowired
    private EntityDao entityDao;

    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private MerchantAccountService merchantAccountService;


    @Override
    public final void logFailedCcTransaction(final CcFailLog ccFailLog) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("CcFailLogServiceImpl, +++ CC fail reason: " + ccFailLog.getReason());
        }
        if (ccFailLog.getReason().length() > 100) {
            ccFailLog.setReason(ccFailLog.getReason().substring(0, 100));
        }
        this.entityDao.save(ccFailLog);
    }

    @Override
    public final void logFailedCcTransaction(final PointOfSale pointOfSale, 
                                             final MerchantAccount merchantAccount, 
                                             final Date procDate,
                                             final Date purchasedDate, 
                                             final int ticketNumber, 
                                             final int ccType, 
                                             final String failedReason) {
        Date processingDate = procDate;
        String reason = failedReason;
        if (processingDate == null) {
            processingDate = new Date();
        }
        if (StringUtils.isBlank(reason)) {
            reason = "No exception message.";
        }
        if (merchantAccount != null && merchantAccount.getId() != null) {
            final CcFailLog ccFailLog = new CcFailLog();
            // the following two method calls prevent a hibernate exception caused only when 
            // Elavon transactions fail in the outStandingCardTransactionCleanupTask.
            // Hibernate throws an "unable to init proxy - no session" error only for retried Elavon post auth transactions which fail on re-attempt.
            ccFailLog.setPointOfSale(this.pointOfSaleService.findPointOfSaleById(pointOfSale.getId()));
            ccFailLog.setMerchantAccount(this.merchantAccountService.findById(merchantAccount.getId()));
            ccFailLog.setProcessingDate(processingDate);
            ccFailLog.setPurchasedDate(purchasedDate);
            ccFailLog.setTicketNumber(ticketNumber);
            ccFailLog.setCctype(ccType);
            ccFailLog.setReason(reason);
            this.logFailedCcTransaction(ccFailLog);
        } else {
            LOG.warn("+++ Unable to insert failed credit card transaction into CCFailLog because merchant account is NULL! +++");
        }

    }

    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    public final void setMerchantAccountService(final MerchantAccountService merchantAccountService) {
        this.merchantAccountService = merchantAccountService;
    }
}
