package com.digitalpaytech.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.RouteType;
import com.digitalpaytech.service.RouteTypeService;

@Service("routeTypeService")
@Transactional(propagation=Propagation.REQUIRED)
public class RouteTypeServiceImpl implements RouteTypeService {

	@Autowired
	private EntityDao entityDao;
	
	public void setEntityDao(EntityDao entityDao) {
		this.entityDao = entityDao;
	}
	
	@Transactional(propagation=Propagation.SUPPORTS)
	public Collection<RouteType> findAllRouteType() {
		return entityDao.loadAll(RouteType.class);
	}
}
