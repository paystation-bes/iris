package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.dao.DataIntegrityViolationException;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CustomerEmail;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportEmail;
import com.digitalpaytech.domain.ReportLocationValue;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.domain.ReportRepeatType;
import com.digitalpaytech.domain.ReportStatusType;
import com.digitalpaytech.domain.ReportType;
import com.digitalpaytech.dto.AdhocReportInfo;
import com.digitalpaytech.service.ReportDefinitionService;
import com.digitalpaytech.service.ReportHistoryService;
import com.digitalpaytech.service.ReportQueueService;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.ReportingUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.support.WebObjectId;

@Service("reportDefinitionService")
@Transactional(propagation = Propagation.SUPPORTS)
public class ReportDefinitionServiceImpl implements ReportDefinitionService {
    private static final Logger LOG = Logger.getLogger(ReportDefinitionServiceImpl.class);
    private static final String CANNOT_SAVE_REPORT_QUEUE = "Cannot save reportQueue: ";
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private ReportingUtil reportingUtil;
    
    @Autowired
    private ReportQueueService reportQueueService;
    
    @Autowired
    private ReportHistoryService reportHistoryService;
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public void setReportingUtil(ReportingUtil reportingUtil) {
        this.reportingUtil = reportingUtil;
    }
    
    @Override
    public void updateReportDefinition(ReportDefinition reportDefinition) {
        entityDao.update(entityDao.merge(reportDefinition));
    }
    
    @Override
    public ReportDefinition getReportDefinition(int id) {
        return (ReportDefinition) entityDao.get(ReportDefinition.class, id);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final boolean findReadyReportsWithLock() {
        final ReportDefinition reportDefinition = 
                (ReportDefinition) this.entityDao.findUniqueByNamedQueryAndNamedParamWithLock("ReportDefinition.findReadyReports",
                                                                                              "currentDate", new Date(), "rd");
        final String[] timeZoneParameters = { "customerId", "customerPropertyType" };
        if (reportDefinition != null) {
            
            final Object[] values = { reportDefinition.getCustomer().getId(), WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE };
            final CustomerProperty timeZoneProperty = (CustomerProperty) this.entityDao
                    .findUniqueByNamedQueryAndNamedParam("CustomerProperty.findCustomerPropertyByCustomerIdAndCustomerPropertyType", timeZoneParameters,
                                                         values, true);
            final ReportQueue reportQueue = this.reportingUtil.createReportQueueEntry(reportDefinition, timeZoneProperty.getPropertyValue(), false);
            try {
                this.entityDao.save(reportQueue);
                this.entityDao.update(updateReportDefinitionStatus(reportDefinition, timeZoneProperty.getPropertyValue()));
            
            } catch (DataIntegrityViolationException dive) {
                LOG.error(CANNOT_SAVE_REPORT_QUEUE + reportQueue, dive);
            } catch (HibernateException he) {
                LOG.error(CANNOT_SAVE_REPORT_QUEUE + reportQueue, he);
            }
            return true;
        } else {
            return false;
        }
        
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<ReportDefinition> findScheduledReportsByUserAccountId(int userAccountId) {
        List<ReportDefinition> listReports = entityDao.findByNamedQueryAndNamedParam("ReportDefinition.findScheduledReportsByUserAccountId", "userAccountId",
                                                                                     userAccountId);
        return listReports;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<ReportDefinition> findScheduledReportsByCustomerId(int customerId) {
        List<ReportDefinition> listReports = entityDao.findByNamedQueryAndNamedParam("ReportDefinition.findScheduledReportsByCustomerId", "customerId",
                                                                                     customerId);
        return listReports;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<ReportDefinition> findScheduledReportsByCustomerIdAndUserAccountId(int userAccountId, int customerId) {
        List<ReportDefinition> listReports = entityDao.findByNamedQueryAndNamedParam("ReportDefinition.findScheduledReportsByCustomerIdAndUserAccountId", new String[] {"userAccountId","customerId"},
                                                                                     new Integer[] {userAccountId, customerId});
        return listReports;
    }

    private ReportDefinition updateReportDefinitionStatus(ReportDefinition reportDefinition, String timeZone) {
        if (ReportingConstants.REPORT_REPEAT_TYPE_ONLINE != reportDefinition.getReportRepeatType().getId()) {
            this.reportingUtil.calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), timeZone, false);
        } else {
            ReportStatusType reportStatusType = new ReportStatusType();
            reportStatusType.setId(ReportingConstants.REPORT_STATUS_SCHEDULED);
            reportDefinition.setReportStatusType(reportStatusType);
        }
        return reportDefinition;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveOrUpdateReportDefinition(ReportDefinition reportDefinition, ReportDefinition originalReportDefinition,
                                             Collection<ReportLocationValue> reportLocationValueList, Set<ReportEmail> emailList) {
        if (originalReportDefinition != null) {
            entityDao.saveOrUpdate(originalReportDefinition);
        }
        
        reportDefinition.setId((Integer) null);
        entityDao.saveOrUpdate(reportDefinition);
        
        if (emailList != null && !emailList.isEmpty()) {
            String[] custEmailParams = new String[] { "customerId", "email" };
            for (ReportEmail reportEmail : emailList) {
                CustomerEmail customerEmail = reportEmail.getCustomerEmail();
                CustomerEmail existingEmail = (CustomerEmail) entityDao
                        .findUniqueByNamedQueryAndNamedParam("CustomerEmail.findCustomerEmailByCustomerIdAndEmail", custEmailParams, new Object[] {
                                customerEmail.getCustomer().getId(), customerEmail.getEmail() }, true);
                if (existingEmail == null) {
                    entityDao.save(customerEmail);
                } else {
                    reportEmail.setCustomerEmail(existingEmail);
                }
                reportEmail.setReportDefinition(reportDefinition);
                
                entityDao.save(reportEmail);
            }
        }
        
        if (reportLocationValueList != null && !reportLocationValueList.isEmpty()) {
            for (ReportLocationValue reportLocationValue : reportLocationValueList) {
                reportLocationValue.setReportDefinition(reportDefinition);
            }
            entityDao.saveOrUpdateAll(reportLocationValueList);
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveOrUpdateReportDefinitionForOnline(ReportDefinition reportDefinition, ReportDefinition originalReportDefinition,
                                                      Collection<ReportLocationValue> reportLocationValueList, Set<ReportEmail> emailList, String timeZone) {
        if (originalReportDefinition != null) {
            entityDao.saveOrUpdate(originalReportDefinition);
        }
        
        ReportStatusType reportStatus = new ReportStatusType();
        reportStatus.setId(ReportingConstants.REPORT_STATUS_SCHEDULED);
        reportDefinition.setReportStatusType(reportStatus);
        
        saveOrUpdateReportDefinition(reportDefinition, originalReportDefinition, reportLocationValueList, emailList);
        
        ReportQueue reportQueue = this.reportingUtil.createReportQueueEntry(reportDefinition, timeZone, true);
        
        entityDao.save(reportQueue);
        
        reportDefinition.getReportQueues().add(reportQueue);
    }
    
    @Override
    public List<String> findEmailsByReportDefinitionId(int reportDefinitionId) {
        List<ReportEmail> reportEmailList = entityDao.findByNamedQueryAndNamedParam("ReportEmail.findReportEmailByReportDefinitionId", "reportDefinitionId",
                                                                                    reportDefinitionId);
        if (reportEmailList != null) {
            List<String> emailList = new ArrayList<String>();
            for (ReportEmail reportEmail : reportEmailList) {
                emailList.add(reportEmail.getCustomerEmail().getEmail());
            }
            return emailList;
        } else {
            return null;
        }
    }
    
    private static final String SQL_GET_READY_REPORT_COUNT = "SELECT COUNT(*) FROM ReportDefinition rd WHERE rd.ReportStatusTypeId = 1";
    
    @Override
    public int getReadyReportCount() {
        SQLQuery q = entityDao.createSQLQuery(SQL_GET_READY_REPORT_COUNT);
        return Integer.parseInt(q.list().get(0).toString());
    }

	@Override
	public AdhocReportInfo runAdhocReport(ReportDefinition reportDefinition, String timeZone) {
		AdhocReportInfo result = new AdhocReportInfo();
		
		// Make a copy of ReportDefinition !
		ReportDefinition defClone = new ReportDefinition(reportDefinition);
		defClone.setId((Integer) null);
		defClone.setTitle((String) null);
		defClone.setReportRepeatTimeOfDay((String) null);
		defClone.setScheduledToRunGmt((Date) null);
		defClone.setCreatedGmt((Date) null);
		defClone.setLastModifiedGmt((Date) null);
		defClone.setLastModifiedByUserId((Integer) null);
		defClone.setVersion((Integer) null);
		
		defClone.setIsAdhoc(true);
		defClone.setReportRepeatType(this.entityDao.get(ReportRepeatType.class, (int) ReportingConstants.REPORT_REPEAT_TYPE_ONLINE));
		defClone.setReportStatusType((ReportStatusType) null);
		
		defClone.setReportLocationValues(reportDefinition.getReportLocationValues());
		
		Date now = new Date();
		
		// 2. If existed, check the queue and repo to see if it is running or completed with in period defined in EMSProperties
		// 3. If #2 is true, return existing one. If #2 is false, create new one (or re-submit the existing ReportDefinition).
		// 4. Return Id of report queue
		ReportDefinition existingDefinition = getAdhocReportDefinition(defClone);
		if(existingDefinition == null) {
			existingDefinition = defClone;
			
			existingDefinition.setReportStatusType(this.entityDao.get(ReportStatusType.class, (int) ReportingConstants.REPORT_STATUS_READY));
			existingDefinition.setScheduledToRunGmt(now);
			existingDefinition.setCreatedGmt(now);
			existingDefinition.setLastModifiedGmt(now);
			existingDefinition.setLastModifiedByUserId(reportDefinition.getLastModifiedByUserId());
			existingDefinition.setVersion(0);
			if(existingDefinition.getTitle() == null) {
				ReportType repType = this.entityDao.get(ReportType.class, existingDefinition.getReportType().getId());
				existingDefinition.setReportType(repType);
				existingDefinition.setTitle(repType.getName());
			}
			
			saveOrUpdateReportDefinitionForOnline(existingDefinition, (ReportDefinition) null, reportDefinition.getReportLocationValues(), new HashSet<ReportEmail>(2), timeZone);
			result.setReportDefinitionId(new WebObjectId(ReportDefinition.class, existingDefinition.getId()));
			
			Iterator<ReportQueue> itr = existingDefinition.getReportQueues().iterator();
			if(itr.hasNext()) {
				ReportQueue q = itr.next();
				result.setQueuedGmt(q.getQueuedGmt().getTime());
			}
		}
		else {
			result.setReportDefinitionId(new WebObjectId(ReportDefinition.class, existingDefinition.getId()));
			
			ReportQueue queued = this.reportQueueService.findRecentlyQueuedReport(existingDefinition.getId());
			if(queued != null) {
				result.setQueuedGmt(queued.getQueuedGmt().getTime());
			}
			else {
				existingDefinition.setReportStatusType(this.entityDao.get(ReportStatusType.class, (int) ReportingConstants.REPORT_STATUS_LAUNCHED));
				existingDefinition.setScheduledToRunGmt(now);
				
				ReportQueue reportQueue = this.reportingUtil.createReportQueueEntry(existingDefinition, timeZone, false);
				this.reportQueueService.saveReportQueue(reportQueue);
				
				result.setQueuedGmt(reportQueue.getQueuedGmt().getTime());
			}
		}
		
		return result;
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public ReportDefinition getAdhocReportDefinition(ReportDefinition reportDefinition) {
		ReportDefinition existingDef = null;
		
		HashSet<ReportLocationValue.Identity> selectedLocs = new HashSet<ReportLocationValue.Identity>(reportDefinition.getReportLocationValues().size());
		for(ReportLocationValue rlv : reportDefinition.getReportLocationValues()) {
			selectedLocs.add(rlv.createIdentity());
		}
		
		Criteria defQuery = this.entityDao.createCriteria(ReportDefinition.class)
				.setProjection(Projections.property("id"));
		
		@SuppressWarnings("unchecked")
		List<Integer> matchedDefinitionIds = appendReportDefinitionConditions(defQuery, reportDefinition).list();
		if(matchedDefinitionIds.size() > 0) {
			ReportDefinition currDef = null, prevDef = null;
			boolean matches = true;
			ReportLocationValue currVal = null;
			
			@SuppressWarnings("unchecked")
			Iterator<ReportLocationValue> locsItr = this.entityDao.getNamedQuery("ReportLocationValue.findReportLocationValuesByReportDefinition")
					.setParameterList("reportDefinitionId", matchedDefinitionIds)
					.list()
					.iterator();
			// All the ReportDefinition(s) at this point will have the same number of ReportLocationValue(s).
			while(locsItr.hasNext() && (existingDef == null)) {
				currVal = locsItr.next();
				currDef = currVal.getReportDefinition();
				
				if(prevDef == null) {
					prevDef = currDef;
					matches = selectedLocs.contains(currVal.createIdentity());
				}
				else if((prevDef != null) && (prevDef.getId().equals(currVal.getId()))) {
					matches &= selectedLocs.contains(currVal.createIdentity());
				}
				else if(matches) {
					// currVal is the first item of currDef. If everything still matches, this means that we found the correct one.
					existingDef = prevDef;
				}
				else {
					prevDef = currDef;
					matches = selectedLocs.contains(currVal.createIdentity());
				}
			}
			
			if(matches && (existingDef == null)) {
				existingDef = prevDef;
			}
		}
		
		return existingDef;
	}
	
	protected Criteria appendReportDefinitionConditions(Criteria query, ReportDefinition example) {
		if(example.getId() != null) {
			query.add(Restrictions.eq("id", example.getId()));
		}
		else {
			/* These will be ignored if they are null */
			if((example.getReportType() != null) && (example.getReportType().getId() > 0)) {
				query.add(Restrictions.eq("reportType.id", example.getReportType().getId()));
			}
			if((example.getUserAccount() != null) && (example.getUserAccount().getId() != null)) {
				query.add(Restrictions.eq("userAccount.id", example.getUserAccount().getId()));
			}
			if((example.getReportStatusType() != null) && (example.getReportStatusType().getId() > 0)) {
				query.add(Restrictions.eq("reportStatusType.id", example.getReportStatusType().getId()));
			}
			if((example.getReportRepeatType() != null) && (example.getReportRepeatType().getId() > 0)) {
				query.add(Restrictions.eq("reportRepeatType.id", example.getReportRepeatType().getId()));
			}
			if((example.getCustomer() != null) && (example.getCustomer().getId() != null)) {
				query.add(Restrictions.eq("customer.id", example.getCustomer().getId()));
			}
			
			/* IsNull must be in the Criteria for these */
			if((example.getReportCollectionFilterType() != null) && (example.getReportCollectionFilterType().getId() > 0)) {
                query.add(Restrictions.eq("reportCollectionFilterType.id", example.getReportCollectionFilterType().getId()));
            }
            else {
            	query.add(Restrictions.isNull("reportCollectionFilterType"));
            }
			
			query.add(Example.create(example)
					.excludeNone()
					.excludeProperty("id")
					.excludeProperty("version")
					.excludeProperty("scheduledToRunGmt")
					.excludeProperty("createdGmt")
					.excludeProperty("lastModifiedGmt")
					.excludeProperty("lastModifiedByUserId")
					.excludeProperty("title")
			);
			
			if((example.getReportEmails() != null) && (example.getReportEmails().size() > 0)) {
				query.add(Restrictions.sizeEq("reportLocationValues", example.getReportEmails().size()));
			}
			if((example.getReportLocationValues() != null) && (example.getReportLocationValues().size() > 0)) {
				query.add(Restrictions.sizeEq("reportLocationValues", example.getReportLocationValues().size()));
			}
		}
		
		return query;
	}

	@Override
	public ReportDefinition getReportDefinitionFromHistory(int reportHistoryId) {
		return (ReportDefinition) this.entityDao.getNamedQuery("ReportDefinition.findByHistoryId")
				.setParameter("historyId", reportHistoryId)
				.uniqueResult();
	}
}
