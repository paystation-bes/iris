package com.digitalpaytech.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.SigningLog;
import com.digitalpaytech.service.SigningLogService;

@Service("SigningLog")
public class SigningLogServiceImpl implements SigningLogService {

	@Autowired
	private EntityDao entityDao;
	
	public void setEntityDao(EntityDao entityDao) {
		this.entityDao = entityDao;
	}
	
	@Override
	public void recordSignatureData(SigningLog signingLog) {
		entityDao.save(signingLog);
	}

}
