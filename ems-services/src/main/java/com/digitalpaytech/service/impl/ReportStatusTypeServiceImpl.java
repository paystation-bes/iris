package com.digitalpaytech.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.ReportStatusType;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.ReportStatusTypeService;


@Service("reportStatusTypeService")
public class ReportStatusTypeServiceImpl implements ReportStatusTypeService {

	@Autowired
	private EntityService entityService;

	   public void setEntityService(EntityService entityService) {
	        this.entityService = entityService;
	    }


	@Override
	public List<ReportStatusType> loadAll() {
		return entityService.loadAll(ReportStatusType.class);
	}

}
