package com.digitalpaytech.service.impl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.domain.ExtensibleRate;
import com.digitalpaytech.domain.ParkingPermission;
import com.digitalpaytech.domain.ParkingPermissionDayOfWeek;
import com.digitalpaytech.domain.SpecialPermissionDate;
import com.digitalpaytech.domain.SpecialRateDate;
import com.digitalpaytech.dto.SmsAlertInfo;
import com.digitalpaytech.dto.SmsRateInfo;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.ExtensibleRateAndPermissionService;
import com.digitalpaytech.service.ExtensibleRateService;
import com.digitalpaytech.service.ParkingPermissionService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;

@Service("extensibleRateAndPermissionService")
@Transactional(propagation = Propagation.SUPPORTS)
public class ExtensibleRateAndPermissionServiceImpl implements ExtensibleRateAndPermissionService {
    
    private final static Logger logger = Logger.getLogger(ExtensibleRateAndPermissionServiceImpl.class);
    private Map<String, SmsRateInfo> cache;
    private ConcurrentLinkedQueue<String> cacheKeyQueue;
    
    @Autowired
    private ExtensibleRateService extensibleRateService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private ParkingPermissionService parkingPermissionService;
    
    private transient ScheduledExecutorService singleThreadScheduler;
    
    @PostConstruct
    protected void init() {
        this.cache = Collections.synchronizedMap(new HashMap<String, SmsRateInfo>());
        this.cacheKeyQueue = new ConcurrentLinkedQueue<String>();
        this.singleThreadScheduler = Executors.newSingleThreadScheduledExecutor();
        this.singleThreadScheduler.scheduleAtFixedRate(this.removeExpired(), 10, 10, TimeUnit.SECONDS);
    }
    
    @PreDestroy
    protected void cleanup() {
        this.singleThreadScheduler.shutdown();
    }
    
    @Override
    public SmsRateInfo getSmsRateInfo(SmsAlertInfo smsAlertInfo) {
        int maxAllowedTimeBasedOnPermission = 0;
        // check cache first by cell phone number
        String cacheKey = smsAlertInfo.getMobileNumber();
        SmsRateInfo smsRateInfo = cache.get(smsAlertInfo.getMobileNumber());
        if (smsRateInfo != null) {
            if (smsAlertInfo.getExpiryDate().getTime() == smsRateInfo.getExpiryDateGMT().getTime()) {
                logger.info("+++++ Return SmsRateInfo in cache. ++++++++++++++");
                return smsRateInfo;
            } else {
                cache.remove(cacheKey);
                cacheKeyQueue.remove(cacheKey);
                logger.info("+++ mobile number: " + cacheKey + " removed from the cache due to outdated transaction +++");
            }
        }
        
        // create a new smsRateInfo when not in cache
        int cacheSize = 0;
        try {
            cacheSize = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.SMS_RATE_INFO_CACHE_SIZE,
                EmsPropertiesService.DEFAULT_SMS_RATE_INFO_CACHE_SIZE);
        } catch (Exception e) {
            cacheSize = EmsPropertiesService.DEFAULT_SMS_RATE_INFO_CACHE_SIZE;
        }
        smsRateInfo = new SmsRateInfo();
        String localTimeZone = smsAlertInfo.getTimeZone();
        smsRateInfo.setExpiryDateGMT(smsAlertInfo.getExpiryDate());
        
        Date expiryDateLocal = DateUtil.changeTimeZone(smsAlertInfo.getExpiryDate(), localTimeZone);
        Date purchasedDateLocal = DateUtil.changeTimeZone(smsAlertInfo.getOriginalPurchasedDate(), localTimeZone);
        Integer locationId = smsAlertInfo.getLocationId();
        if (logger.isDebugEnabled()) {
            logger.debug("+++ purchased date (local time): " + purchasedDateLocal);
            logger.debug("+++ expiry date (local time): " + expiryDateLocal);
        }
        
        Calendar cal = Calendar.getInstance();
        cal.setTime(expiryDateLocal);
        
        // check permissions        
        maxAllowedTimeBasedOnPermission = getMaxTimeAllowedBasedOnParkingPermission(smsRateInfo, localTimeZone, purchasedDateLocal, expiryDateLocal,
            locationId);
        
        if (maxAllowedTimeBasedOnPermission <= 0) {
            ExtensibleRate currentEMSRate = this.getCurrentExtensibleRate(locationId, expiryDateLocal);
            if (currentEMSRate != null) {
                SpecialRateDate emsSpecialRateDate = currentEMSRate.getSpecialRateDate();
                if (emsSpecialRateDate != null) {
                    Date endDateLocal = emsSpecialRateDate.getEndDateLocal();
                    Date specialRateEndDateGMT = DateUtil.getDatetimeInGMT(endDateLocal, localTimeZone);
                    Date startDateLocal = emsSpecialRateDate.getStartDateLocal();
                    
                    currentEMSRate.setStartDateLocal(startDateLocal);
                    currentEMSRate.setEndDateLocal(endDateLocal);
                    currentEMSRate.setEndDateGMT(specialRateEndDateGMT);
                } else {
                    Calendar cal1 = Calendar.getInstance();
                    cal1.setTime(expiryDateLocal);
                    cal1.set(Calendar.HOUR_OF_DAY, currentEMSRate.getBeginHourLocal());
                    cal1.set(Calendar.MINUTE, currentEMSRate.getBeginMinuteLocal());
                    cal1.set(Calendar.SECOND, 0);
                    Date rateStartDateLocal = cal1.getTime();
                    
                    cal1.set(Calendar.HOUR_OF_DAY, currentEMSRate.getEndHourLocal());
                    cal1.set(Calendar.MINUTE, currentEMSRate.getEndMinuteLocal());
                    cal1.set(Calendar.SECOND, 0);
                    if (currentEMSRate.getEndHourLocal() == 0) {
                        cal.add(Calendar.DAY_OF_MONTH, 1);
                    }
                    Date rateEndDateLocal = cal.getTime();
                    Date rateEndDateGMT = DateUtil.getDatetimeInGMT(rateEndDateLocal, localTimeZone);
                    
                    currentEMSRate.setStartDateLocal(rateStartDateLocal);
                    currentEMSRate.setEndDateLocal(rateEndDateLocal);
                    currentEMSRate.setEndDateGMT(rateEndDateGMT);
                }
            }
            smsRateInfo.setEmsRate1(currentEMSRate);
            updateCache(cacheKey, smsRateInfo, cacheSize);
            return smsRateInfo;
        }
        
        smsRateInfo.setMaxAllowedTimeByPermission(maxAllowedTimeBasedOnPermission);
        
        // step3:  check rates
        ExtensibleRate[] rates = get2Rates(smsRateInfo, localTimeZone, maxAllowedTimeBasedOnPermission, expiryDateLocal, locationId);
        
        if (rates[0] != null && rates[1] != null) {
            smsRateInfo.setIsSecondPermissionNoParking(false);
        }
        
        if (rates[0] != null) {
            smsRateInfo.setMaxExtensionTimeForFreeParking(timeDiffInMinutes(expiryDateLocal, rates[0].getEndDateLocal()));
            
            if (rates[0].getEndDateLocal().getTime() >= this.addDateInMinutes(getRoundDownDate(expiryDateLocal), maxAllowedTimeBasedOnPermission)
                    .getTime()) {
                if (smsRateInfo.getIsSecondPermissionNoParking()) {
                    rates[1] = null;
                }
            }
            
            logger.info("++++ smsRateInfo.isSecondPermissionNoParking: " + smsRateInfo.getIsSecondPermissionNoParking() + " ++++");
        }
        
        smsRateInfo.setEmsRate1(rates[0]);
        smsRateInfo.setEmsRate2(rates[1]);
        updateCache(cacheKey, smsRateInfo, cacheSize);
        return smsRateInfo;
    }
    
    public ExtensibleRate getCurrentExtensibleRate(Integer locationId, Date targetDateLocal) {
        Collection<ExtensibleRate> lists = this.extensibleRateService.getCurrentExtensibleRate(locationId, targetDateLocal);
        if (!lists.isEmpty()) {
            return lists.iterator().next();
        }
        return null;
    }
    
    public List<ExtensibleRate> processExtensibleRateOvelap(List<ExtensibleRate> emsRates, String localTimeZone) {
        List<ExtensibleRate> unOverlapedList = new ArrayList<ExtensibleRate>();
        ExtensibleRate r1;
        ExtensibleRate r2;
        for (ExtensibleRate rate : emsRates) {
            rate.setStartDateLocal(getRoundDownDate(rate.getStartDateLocal()));
            rate.setEndDateLocal(getRoundDownDate(rate.getEndDateLocal()));
        }
        if (emsRates.size() > 0) {
            if (emsRates.size() == 1) {
                unOverlapedList.add(emsRates.get(0).copyExtensibleRate());
            } else {
                r1 = emsRates.get(0).copyExtensibleRate();
                
                for (int i = 1; i < emsRates.size(); i++) {
                    int pos = 0;
                    boolean isExist = false;
                    for (int j = 0; j < unOverlapedList.size(); j++) {
                        if (unOverlapedList.get(j).equals(r1)) {
                            pos = j;
                            isExist = true;
                            unOverlapedList.remove(j);
                        }
                    }
                    
                    r2 = emsRates.get(i).copyExtensibleRate();
                    // r1 overlaps r2
                    if (r1.is2RateOverlap(r2)) {
                        if (r1.getExtensibleRateType().getId() == WebCoreConstants.EXTENSIBLE_TYPE_SPECIAL_RATE) {
                            // same time
                            if (r1.getStartDateLocal().compareTo(r2.getStartDateLocal()) == 0
                                && r1.getEndDateLocal().compareTo(r2.getEndDateLocal()) == 0) {
                                addRateToList(unOverlapedList, r1, isExist, pos);
                            } else {
                                // same start time
                                if (r1.getStartDateLocal().compareTo(r2.getStartDateLocal()) == 0) {
                                    // r1 end date > r2 end date
                                    if (r1.getEndDateLocal().after(r2.getEndDateLocal())) {
                                        addRateToList(unOverlapedList, r1, isExist, pos);
                                    } else {
                                        r2.setBeginHourLocal(r1.getEndHourLocal());
                                        r2.setBeginMinuteLocal(r1.getEndMinuteLocal());
                                        setRateStartDateTimeLocal(r2);
                                        addRateToList(unOverlapedList, r1, isExist, pos);
                                        r1 = r2;
                                    }
                                }
                                // same end time
                                else if (r1.getEndDateLocal().compareTo(r2.getEndDateLocal()) == 0) {
                                    addRateToList(unOverlapedList, r1, isExist, pos);
                                }
                                // different start date and end date
                                else {
                                    // r1 end date > r2 end date
                                    if (r1.getEndDateLocal().after(r2.getEndDateLocal())) {
                                        addRateToList(unOverlapedList, r1, isExist, pos);
                                    } else {
                                        r2.setBeginHourLocal(r1.getEndHourLocal());
                                        r2.setBeginMinuteLocal(r1.getEndMinuteLocal());
                                        setRateStartDateTimeLocal(r2);
                                        addRateToList(unOverlapedList, r1, isExist, pos);
                                        r1 = r2;
                                    }
                                }
                            }
                        } else if (r2.getExtensibleRateType().getId() == WebCoreConstants.EXTENSIBLE_TYPE_SPECIAL_RATE) {
                            // same time
                            if (r1.getStartDateLocal().compareTo(r2.getStartDateLocal()) == 0
                                && r1.getEndDateLocal().compareTo(r2.getEndDateLocal()) == 0) {
                                r1 = r2;
                                addRateToList(unOverlapedList, r1, isExist, pos);
                            } else {
                                ExtensibleRate tempR;
                                // same start time
                                if (r1.getStartDateLocal().compareTo(r2.getStartDateLocal()) == 0) {
                                    // r1 end date > r2 end date
                                    if (r1.getEndDateLocal().after(r2.getEndDateLocal())) {
                                        tempR = r1.copyExtensibleRate();
                                        r1 = r2;
                                        tempR.setBeginHourLocal(r2.getEndHourLocal());
                                        tempR.setBeginMinuteLocal(r2.getEndMinuteLocal());
                                        setRateStartDateTimeLocal(tempR);
                                        r2 = tempR;
                                        addRateToList(unOverlapedList, r1, isExist, pos);
                                        r1 = r2;
                                    } else {
                                        r1 = r2;
                                        addRateToList(unOverlapedList, r1, isExist, pos);
                                    }
                                }
                                // same end time
                                else if (r1.getEndDateLocal().compareTo(r2.getEndDateLocal()) == 0) {
                                    r1.setEndHourLocal(r2.getBeginHourLocal());
                                    r1.setEndMinuteLocal(r2.getBeginMinuteLocal());
                                    //                                  setRateEndDateTimeLocal(r1);
                                    r1.setEndDateLocal(r2.getStartDateLocal());
                                    setRateEndDateTimeGMT(r1, localTimeZone);
                                    addRateToList(unOverlapedList, r1, isExist, pos);
                                    r1 = r2;
                                }
                                // different start date and end date
                                else {
                                    // r1 end date > r2 end date
                                    if (r1.getEndDateLocal().after(r2.getEndDateLocal())) {
                                        // at this point, there should be divided into three rates
                                        tempR = r1.copyExtensibleRate();
                                        r1.setEndHourLocal(r2.getBeginHourLocal());
                                        r1.setEndMinuteLocal(r2.getBeginMinuteLocal());
                                        //                                      setRateEndDateTimeLocal(r1);
                                        r1.setEndDateLocal(r2.getStartDateLocal());
                                        // reset end date in GMT;
                                        setRateEndDateTimeGMT(r1, localTimeZone);
                                        addRateToList(unOverlapedList, r1, isExist, pos);
                                        for (int j = 0; j < unOverlapedList.size(); j++) {
                                            if (unOverlapedList.get(j).equals(r2)) {
                                                pos = j;
                                                isExist = true;
                                                unOverlapedList.remove(j);
                                            }
                                        }
                                        addRateToList(unOverlapedList, r2, isExist, pos);
                                        tempR.setBeginHourLocal(r2.getEndHourLocal());
                                        tempR.setBeginMinuteLocal(r2.getEndMinuteLocal());
                                        setRateStartDateTimeLocal(tempR);
                                        r1 = tempR;
                                    } else {
                                        r1.setEndHourLocal(r2.getBeginHourLocal());
                                        r1.setEndMinuteLocal(r2.getBeginMinuteLocal());
                                        //                                      setRateEndDateTimeLocal(r1);
                                        r1.setEndDateLocal(r2.getStartDateLocal());
                                        setRateEndDateTimeGMT(r1, localTimeZone);
                                        addRateToList(unOverlapedList, r1, isExist, pos);
                                        r1 = r2;
                                    }
                                }
                            }
                        } else {
                            logger.warn("!!! should not come here, since both rates are nomal rates, one of rate1 and rate2 is wrong  !!!");
                        }
                    }
                    // no overlap
                    else {
                        addRateToList(unOverlapedList, r1, isExist, pos);
                        r1 = r2;
                    }
                    // add last item, only add if not in list
                    if (i == emsRates.size() - 1) {
                        isExist = false;
                        for (int j = 0; j < unOverlapedList.size(); j++) {
                            if (unOverlapedList.get(j).equals(r1)) {
                                isExist = true;
                            }
                        }
                        if (!isExist) {
                            unOverlapedList.add(r1);
                        }
                    }
                }
            }
        }
        //        logger.debug("------- after rates overlap processing, following are updated rate list --------");
        //        for (ExtensibleRate rate : unOverlapedList) {
        //            logger.debug(rate);
        //        }
        return unOverlapedList;
    }
    
    public List<ParkingPermission> processParkingPermissionOverlap(SmsRateInfo smsRateInfo, List<ParkingPermission> emsParkingPermissions,
        String localTimeZone) {
        List<ParkingPermission> unOverlappedList = new ArrayList<ParkingPermission>();
        ParkingPermission p1;
        ParkingPermission p2;
        for (ParkingPermission permission : emsParkingPermissions) {
            permission.setStartDateLocal(getRoundDownDate(permission.getStartDateLocal()));
            permission.setEndDateLocal(getRoundDownDate(permission.getEndDateLocal()));
        }
        if (emsParkingPermissions.size() > 0) {
            if (emsParkingPermissions.size() == 1) {
                unOverlappedList.add(emsParkingPermissions.get(0).copyParkingPermission());
            } else {
                p1 = emsParkingPermissions.get(0).copyParkingPermission();
                for (int i = 1; i < emsParkingPermissions.size(); i++) {
                    p2 = emsParkingPermissions.get(i).copyParkingPermission();
                    // p1 overlaps p2
                    if (p1.is2PermissionOverlap(p2)) {
                        if (p1.getParkingPermissionType().getId() == WebCoreConstants.PERMISSION_TYPE_RESTRICTED_PARKING) {
                            smsRateInfo.setIsSecondPermissionNoParking(true);
                            smsRateInfo.setNoParkingLocalEndTimeDisplay(getPermissionEndDateForDisplay(p1.getEndHourLocal(), p1.getEndMinuteLocal(),
                                true));
                            smsRateInfo.setIsNoParkingLongerThan24H(p1.getEndDateLocal(), localTimeZone);
                            return unOverlappedList;
                        } else if (p1.getParkingPermissionType().getId() == WebCoreConstants.PERMISSION_TYPE_SPECIAL_PARKING
                                   && p2.getParkingPermissionType().getId() != WebCoreConstants.PERMISSION_TYPE_RESTRICTED_PARKING) {
                            // same time
                            if (p1.getStartDateLocal().compareTo(p2.getStartDateLocal()) == 0
                                && p1.getEndDateLocal().compareTo(p2.getEndDateLocal()) == 0) {
                                unOverlappedList.add(p1);
                                continue;
                            } else {
                                // same start time
                                if (p1.getStartDateLocal().compareTo(p2.getStartDateLocal()) == 0) {
                                    // p1 end date > p2 end date
                                    if (p1.getEndDateLocal().after(p2.getEndDateLocal())) {
                                        unOverlappedList.add(p1);
                                        continue;
                                    } else {
                                        p2.setBeginHourLocal(p1.getEndHourLocal());
                                        p2.setBeginMinuteLocal(p1.getEndMinuteLocal());
                                        setPermissionStartDateTimeLocal(p2);
                                    }
                                }
                                // same end time
                                else if (p1.getEndDateLocal().compareTo(p2.getEndDateLocal()) == 0) {
                                    unOverlappedList.add(p1);
                                    continue;
                                }
                                // different start date and end date
                                else {
                                    // p1 end date > p2 end date
                                    if (p1.getEndDateLocal().after(p2.getEndDateLocal())) {
                                        unOverlappedList.add(p1);
                                        continue;
                                    } else {
                                        p2.setBeginHourLocal(p1.getEndHourLocal());
                                        p2.setBeginMinuteLocal(p1.getEndMinuteLocal());
                                        setPermissionStartDateTimeLocal(p2);
                                    }
                                }
                            }
                            
                            if (i == 1) {
                                unOverlappedList.add(p1);
                            }
                            
                            unOverlappedList.add(p2);
                            p1 = p2;
                        } else if (p2.getParkingPermissionType().getId() == WebCoreConstants.PERMISSION_TYPE_RESTRICTED_PARKING) {
                            int pos = 0;
                            boolean isExist = false;
                            for (int j = 0; j < unOverlappedList.size(); j++) {
                                if (unOverlappedList.get(j).equals(p1)) {
                                    pos = j;
                                    isExist = true;
                                    unOverlappedList.remove(j);
                                }
                            }
                            
                            p1.setEndHourLocal(p2.getBeginHourLocal());
                            p1.setEndMinuteLocal(p2.getBeginMinuteLocal());
                            //                                                                                       setPermissionEndDateTimeLocal(p1);  
                            p1.setEndDateLocal(p2.getStartDateLocal());
                            if (p1.getStartDateLocal().compareTo(p1.getEndDateLocal()) == 0) {
                                smsRateInfo.setIsSecondPermissionNoParking(true);
                                smsRateInfo.setNoParkingLocalEndTimeDisplay(getPermissionEndDateForDisplay(p2.getEndHourLocal(),
                                    p2.getEndMinuteLocal(), true));
                                smsRateInfo.setIsNoParkingLongerThan24H(p2.getEndDateLocal(), localTimeZone);
                                return unOverlappedList;
                            } else {
                                if (i == 1 || isExist) {
                                    unOverlappedList.add(pos, p1);
                                }
                            }
                            smsRateInfo.setIsSecondPermissionNoParking(true);
                            smsRateInfo.setNoParkingLocalEndTimeDisplay(getPermissionEndDateForDisplay(p2.getEndHourLocal(), p2.getEndMinuteLocal(),
                                true));
                            smsRateInfo.setIsNoParkingLongerThan24H(p2.getEndDateLocal(), localTimeZone);
                            return unOverlappedList;
                        } else if (p2.getParkingPermissionType().getId() == WebCoreConstants.PERMISSION_TYPE_SPECIAL_PARKING) {
                            int pos = 0;
                            boolean isExist = false;
                            for (int j = 0; j < unOverlappedList.size(); j++) {
                                if (unOverlappedList.get(j).equals(p1)) {
                                    pos = j;
                                    isExist = true;
                                    unOverlappedList.remove(j);
                                    break;
                                }
                            }
                            // same time
                            if (p1.getStartDateLocal().compareTo(p2.getStartDateLocal()) == 0
                                && p1.getEndDateLocal().compareTo(p2.getEndDateLocal()) == 0) {
                                p1 = p2;
                                unOverlappedList.add(p1);
                                continue;
                            } else {
                                boolean isNeedIncreasePosition = false;
                                ParkingPermission tempP;
                                // same start time
                                if (p1.getStartDateLocal().compareTo(p2.getStartDateLocal()) == 0) {
                                    // p1 end date > p2 end date
                                    if (p1.getEndDateLocal().after(p2.getEndDateLocal())) {
                                        tempP = p1.copyParkingPermission();
                                        p1 = p2;
                                        tempP.setBeginHourLocal(p2.getEndHourLocal());
                                        tempP.setBeginMinuteLocal(p2.getEndMinuteLocal());
                                        setPermissionStartDateTimeLocal(tempP);
                                        p2 = tempP;
                                    } else {
                                        p1 = p2;
                                        unOverlappedList.add(p1);
                                        continue;
                                    }
                                }
                                // same end time
                                else if (p1.getEndDateLocal().compareTo(p2.getEndDateLocal()) == 0) {
                                    p1.setEndHourLocal(p2.getBeginHourLocal());
                                    p1.setEndMinuteLocal(p2.getBeginMinuteLocal());
                                    p1.setEndDateLocal(p2.getStartDateLocal());
                                    // setPermissionEndDateTimeLocal(p1);
                                }
                                // different start date and end date
                                else {
                                    // p1 end date > p2 end date
                                    if (p1.getEndDateLocal().after(p2.getEndDateLocal())) {
                                        // at this point, there should be divided into three permissions
                                        tempP = p1.copyParkingPermission();
                                        p1.setEndHourLocal(p2.getBeginHourLocal());
                                        p1.setEndMinuteLocal(p2.getBeginMinuteLocal());
                                        setPermissionEndDateTimeLocal(p1);
                                        p1.setEndDateLocal(p2.getStartDateLocal());
                                        if (i == 1) {
                                            unOverlappedList.add(p1);
                                        } else {
                                            if (isExist) {
                                                unOverlappedList.add(pos, p1);
                                                isNeedIncreasePosition = true;
                                            }
                                        }
                                        p1 = p2;
                                        tempP.setBeginHourLocal(p2.getEndHourLocal());
                                        tempP.setBeginMinuteLocal(p2.getEndMinuteLocal());
                                        setPermissionStartDateTimeLocal(tempP);
                                        p2 = tempP;
                                    } else {
                                        p1.setEndHourLocal(p2.getBeginHourLocal());
                                        p1.setEndMinuteLocal(p2.getBeginMinuteLocal());
                                        p1.setEndDateLocal(p2.getStartDateLocal());
                                    }
                                }
                                
                                if (i == 1) {
                                    unOverlappedList.add(p1);
                                } else {
                                    if (isExist) {
                                        if (isNeedIncreasePosition) {
                                            pos++;
                                            unOverlappedList.add(pos, p1);
                                        } else {
                                            unOverlappedList.add(pos, p1);
                                        }
                                    }
                                }
                                unOverlappedList.add(p2);
                                p1 = p2;
                            }
                        } else {
                            logger.warn("!!! should not come here, since both permissions are normal permissions, one of permission1 and permission2 is wrong  !!!");
                        }
                    }
                    // no overlap
                    else {
                        if (i == 1) {
                            unOverlappedList.add(p1);
                        }
                        unOverlappedList.add(p2);
                        p1 = p2;
                    }
                }
            }
        }
        //        logger.info("------- after permission overlap processing, followings are updated permission list --------");
        //        for (ParkingPermission permission : unOverlapedList) {
        //            logger.info(permission);
        //        }
        return unOverlappedList;
    }
    
    public List<ParkingPermission> processEmsParkingPermissionMerge(List<ParkingPermission> emsParkingPermissions, String localTimeZone) {
        List<ParkingPermission> mergedList = new ArrayList<ParkingPermission>();
        ParkingPermission p1;
        ParkingPermission p2;
        //        logger.info("------- following permissions have been loaded to check whether they can be merged --------");
        //        for (ParkingPermission permission : emsParkingPermissions) {
        //            logger.info(permission);
        //        }
        
        if (emsParkingPermissions.size() > 0) {
            if (emsParkingPermissions.size() == 1) {
                mergedList.add(emsParkingPermissions.get(0).copyParkingPermission());
            } else {
                p1 = emsParkingPermissions.get(0).copyParkingPermission();
                
                for (int i = 1; i < emsParkingPermissions.size(); i++) {
                    p2 = emsParkingPermissions.get(i).copyParkingPermission();
                    // p1, p2 are same?
                    if (p1.isSameParkingPermission(p2)) {
                        int pos = 0;
                        for (int j = 0; j < mergedList.size(); j++) {
                            if (mergedList.get(j).equals(p1)) {
                                pos = j;
                                mergedList.remove(j);
                            }
                        }
                        if (p1.getParkingPermissionType().getId() == WebCoreConstants.PERMISSION_TYPE_UNLIMITED_PARKING) {
                            p1.setMaxDurationMinutes(p1.getMaxDurationMinutes() + p2.getMaxDurationMinutes());
                            if (timeDiffInMinutes(p1.getStartDateLocal(), p1.getEndDateLocal()) > WebCoreConstants.DEFAULT_MAX_EXTENDABLE_TIME_IN_MINUTES) {
                                p1.setMaxDurationMinutes(WebCoreConstants.DEFAULT_MAX_EXTENDABLE_TIME_IN_MINUTES);
                            }
                        }
                        p1.setEndHourLocal(p2.getEndHourLocal());
                        p1.setEndMinuteLocal(p2.getEndMinuteLocal());
                        p1.setEndDateLocal(p2.getEndDateLocal());
                        mergedList.add(pos, p1);
                    } else {
                        if (i == 1) {
                            mergedList.add(p1);
                        }
                        
                        mergedList.add(p2);
                        p1 = p2;
                    }
                }
            }
        }
        
        //        logger.info("------- after permission merging, followings are updated permission list --------");
        //        for (ParkingPermission permission : mergedList) {
        //            logger.info(permission);
        //        }
        return mergedList;
    }
    
    private int getMaxTimeAllowedBasedOnParkingPermission(SmsRateInfo smsRateInfo, String localTimeZone, Date localOriginalPurchasedDate,
        Date localExpiryDate, int regionId) {
        localOriginalPurchasedDate = getRoundDownDate(localOriginalPurchasedDate);
        List<ParkingPermission> permissions = this.getParkingPermissionsForSMSMessage(localTimeZone, localExpiryDate,
            addDateInMinutes(localExpiryDate, WebCoreConstants.DEFAULT_MAX_EXTENDABLE_TIME_IN_MINUTES), regionId);
        
        if (!permissions.isEmpty()) {
            Date roundDownedExpiryDateLocal = this.getRoundDownDate(localExpiryDate);
            
            /* get permission list with no overlap. */
            permissions = this.processParkingPermissionOverlap(smsRateInfo, permissions, localTimeZone);
            /* get merged permission list. */
            permissions = this.processEmsParkingPermissionMerge(permissions, localTimeZone);
            
            // remove all permissions with end date <= expiry date
            ArrayList<ParkingPermission> toBeRemoved = new ArrayList<ParkingPermission>();
            for (ParkingPermission p : permissions) {
                if (p.getEndDateLocal().compareTo(roundDownedExpiryDateLocal) <= 0) {
                    toBeRemoved.add(p);
                }
            }
            for (ParkingPermission removeMe : toBeRemoved) {
                permissions.remove(removeMe);
            }
            
            /* get connected permission list. */
            permissions = this.processEmsParkingPermissionConnection(smsRateInfo, permissions, localTimeZone);
            
            /* convert list value to EMSParkingPermission array. */
            ParkingPermission[] permissionArray = new ParkingPermission[permissions.size()];
            permissionArray = permissions.toArray(permissionArray);
            
            for (ParkingPermission permission : permissionArray) {
                Date permissionStartDateLocal = permission.getStartDateLocal();
                Date permissionEndDateLocal = permission.getEndDateLocal();
                
                /*
                 * Set SmsRateInfo 'isTimeLimited' property if it doesn't have ExtensibleRate and the current time is between
                 * permissionStartDateLocal and permissionEndDateLocal.
                 */
                if (smsRateInfo.getEmsRate1() == null && DateUtil.isInBetween(new Date(), permissionStartDateLocal, permissionEndDateLocal)) {
                    // 1 = true
                    smsRateInfo.setIsTimeLimited(1);
                }
                
                if (roundDownedExpiryDateLocal.compareTo(permissionEndDateLocal) >= 0) {
                    continue;
                }
                
                if (roundDownedExpiryDateLocal.getTime() >= permissionStartDateLocal.getTime()
                    && roundDownedExpiryDateLocal.getTime() <= permissionEndDateLocal.getTime()) {
                    if (permission.getParkingPermissionType().getId() == WebCoreConstants.PERMISSION_TYPE_RESTRICTED_PARKING) {
                        return 0;
                    }
                    
                    /* has permission1 */
                    smsRateInfo.setHasPermission1(true);
                    
                    int totalParkedTimeinCurrentPermisson = 0;
                    if (localOriginalPurchasedDate.before(permissionStartDateLocal)) {
                        totalParkedTimeinCurrentPermisson = timeDiffInMinutes(permissionStartDateLocal, localExpiryDate);
                    } else {
                        totalParkedTimeinCurrentPermisson = timeDiffInMinutes(localOriginalPurchasedDate, localExpiryDate);
                    }
                    
                    /* calculate available time in current permission. */
                    int availableParkingTimeInCurrentPermission = permission.getMaxDurationMinutes() - totalParkedTimeinCurrentPermisson;
                    if (availableParkingTimeInCurrentPermission < 0) {
                        availableParkingTimeInCurrentPermission = 0;
                    }
                    
                    Date canParkUntil;
                    if (localOriginalPurchasedDate.before(permissionStartDateLocal)) {
                        canParkUntil = addDateInMinutes(permissionStartDateLocal, availableParkingTimeInCurrentPermission);
                    } else {
                        canParkUntil = addDateInMinutes(roundDownedExpiryDateLocal, availableParkingTimeInCurrentPermission);
                    }
                    if (canParkUntil.before(permissionEndDateLocal)) {
                        // that means max allowed time reach before end of permission
                        smsRateInfo.setIsSecondPermissionNoParking(false);
                    }
                    
                    if (addDateInMinutes(roundDownedExpiryDateLocal, availableParkingTimeInCurrentPermission).getTime() >= permission
                            .getEndDateLocal().getTime()) {
                        
                        int result;
                        if (permissionArray.length == 1) {
                            int tempDiff = timeDiffInMinutes(localExpiryDate, permission.getEndDateLocal());
                            int tempMax = permission.getMaxDurationMinutes() - totalParkedTimeinCurrentPermisson;
                            
                            result = tempDiff > tempMax ? tempMax : tempDiff;
                        } else {
                            Date d = addDateInMinutes(permissionArray[permissionArray.length - 1].getStartDateLocal(),
                                permissionArray[permissionArray.length - 1].getMaxDurationMinutes());
                            if (d.after(permissionArray[permissionArray.length - 1].getEndDateLocal())) {
                                result = timeDiffInMinutes(localExpiryDate, permissionArray[permissionArray.length - 1].getEndDateLocal());
                            } else {
                                result = timeDiffInMinutes(localExpiryDate, d);
                            }
                        }
                        
                        return result < WebCoreConstants.DEFAULT_MAX_EXTENDABLE_TIME_IN_MINUTES ? result
                                : WebCoreConstants.DEFAULT_MAX_EXTENDABLE_TIME_IN_MINUTES;
                    } else {
                        return availableParkingTimeInCurrentPermission < WebCoreConstants.DEFAULT_MAX_EXTENDABLE_TIME_IN_MINUTES
                                ? availableParkingTimeInCurrentPermission : WebCoreConstants.DEFAULT_MAX_EXTENDABLE_TIME_IN_MINUTES;
                    }
                }
            }
            
            /* no permission defined for the expiry date, therefore it is restricted parking. */
            logger.info("+++ no permission defined for the expiry date, therefore it is restricted parking. 0 for availableParkingTimeInCurrentPermission. +++");
            return 0;
        } else {
            logger.info("+++++++++++++ no parking permission defined ++++++++++++++");
            return 0;
        }
    }
    
    public List<ParkingPermission> processEmsParkingPermissionConnection(SmsRateInfo smsRateInfo, List<ParkingPermission> emsParkingPermissions,
        String localTimeZone) {
        List<ParkingPermission> connectedList = new ArrayList<ParkingPermission>();
        ParkingPermission p1;
        ParkingPermission p2;
        //        logger.info("------- following permissions have been loaded to check whether they are connected --------");
        //        for (ParkingPermission permission : emsParkingPermissions) {
        //            logger.info(permission);
        //        }
        if (emsParkingPermissions.size() > 0) {
            if (emsParkingPermissions.size() == 1) {
                connectedList.add(emsParkingPermissions.get(0).copyParkingPermission());
            } else {
                p1 = emsParkingPermissions.get(0).copyParkingPermission();
                
                for (int i = 1; i < emsParkingPermissions.size(); i++) {
                    p2 = emsParkingPermissions.get(i).copyParkingPermission();
                    if (p1.isConnectedParkingPermission(p2)) {
                        if (i == 1) {
                            // first permission always add in
                            connectedList.add(p1);
                        } else if (p1.isAllowParkingForFullPermissinTime()) {
                            // only add if maxDuration >= endTime - startTime
                            // see permission.isAllowParkingForFullPermissinTime() method
                            connectedList.add(p1);
                        } else {
                            // should stop here if maxDuration < endTime - startTime
                            connectedList.add(p1);
                            break;
                        }
                        p1 = p2;
                        // last item
                        if (i == emsParkingPermissions.size() - 1) {
                            if (p2.getParkingPermissionType().getId() == WebCoreConstants.PERMISSION_TYPE_RESTRICTED_PARKING) {
                                smsRateInfo.setIsSecondPermissionNoParking(true);
                                smsRateInfo.setNoParkingLocalEndTimeDisplay(getPermissionEndDateForDisplay(p2.getEndHourLocal(),
                                    p2.getEndMinuteLocal(), true));
                                smsRateInfo.setIsNoParkingLongerThan24H(p2.getEndDateLocal(), localTimeZone);
                            } else {
                                connectedList.add(p2);
                                smsRateInfo.setIsTimeLimited(p2.getParkingPermissionType().getId());
                            }
                        }
                    } else {
                        connectedList.add(p1);
                        smsRateInfo.setIsSecondPermissionNoParking(true);
                        smsRateInfo.setNoParkingLocalEndTimeDisplay(getPermissionEndDateForDisplay(p2.getBeginHourLocal(), p2.getBeginMinuteLocal(),
                            false));
                        smsRateInfo.setFreeParkingEndDate(p1.getEndDateLocal());
                        smsRateInfo.setIsTimeLimited(p1.getParkingPermissionType().getId());
                        smsRateInfo.setIsNoParkingLongerThan24H(p2.getStartDateLocal(), localTimeZone);
                        break;
                    }
                }
            }
        }
        
        //        logger.info("------- after permissions connection check, followings are updated permission list --------");
        //        for (ParkingPermission permission : connectedList) {
        //            logger.info(permission);
        //        }
        return connectedList;
    }
    
    private List<ExtensibleRate> processExtensibleRateMerge(List<ExtensibleRate> emsRates, String localTimeZone) {
        List<ExtensibleRate> mergedList = new ArrayList<ExtensibleRate>();
        ExtensibleRate r1;
        ExtensibleRate r2;
        //        logger.info("------- following rates have been loaded to check whether they can be merged --------");
        //        for (ExtensibleRate rate : emsRates) {
        //            logger.info(rate);
        //        }
        if (emsRates.size() > 0) {
            if (emsRates.size() == 1) {
                mergedList.add(emsRates.get(0).copyExtensibleRate());
            } else {
                r1 = emsRates.get(0).copyExtensibleRate();
                
                for (int i = 1; i < emsRates.size(); i++) {
                    r2 = emsRates.get(i).copyExtensibleRate();
                    if (r1.isConnectedRate(r2)) {
                        if (r1.isSameRate(r2)) {
                            int pos = 0;
                            for (int j = 0; j < mergedList.size(); j++) {
                                if (mergedList.get(j).equals(r1)) {
                                    pos = j;
                                    mergedList.remove(j);
                                }
                            }
                            
                            r1.setEndHourLocal(r2.getEndHourLocal());
                            r1.setEndMinuteLocal(r2.getEndMinuteLocal());
                            r1.setEndDateLocal(r2.getEndDateLocal());
                            r1.setEndDateGMT(r2.getEndDateGMT());
                            
                            mergedList.add(pos, r1);
                        } else {
                            if (i == 1) {
                                mergedList.add(r1);
                            }
                            mergedList.add(r2);
                            r1 = r2;
                        }
                    } else {
                        if (i == 1) {
                            mergedList.add(r1);
                        }
                        mergedList.add(r2);
                        r1 = r2;
                    }
                }
            }
        }
        //        logger.debug("------- after rates merging, following are updated rate list --------");
        //        for (ExtensibleRate rate : mergedList) {
        //            logger.debug(rate);
        //        }
        return mergedList;
    }
    
    private void updateCache(String cacheKey, SmsRateInfo smsRateInfo, int cacheSize) {
        // cache smsRateInfo info           
        if (cache.size() > cacheSize) {
            String oldestKey = cacheKeyQueue.poll();
            if (oldestKey != null) {
                cache.remove(oldestKey);
            }
        }
        cache.put(cacheKey, smsRateInfo);
    }
    
    private ExtensibleRate[] get2Rates(SmsRateInfo smsRateInfo, String localTimeZone, int maxAllowedTimeBasedOnPermission, Date expiryDateLocal,
        int regionId) {
        List<ExtensibleRate> rates = this.getExtensibleRatesForSMSMessage(localTimeZone, expiryDateLocal,
            addDateInMinutes(expiryDateLocal, maxAllowedTimeBasedOnPermission), regionId);
        ExtensibleRate firstRate = null;
        ExtensibleRate secondRate = null;
        
        if (!rates.isEmpty()) {
            /* get the list with no overlap. */
            rates = processExtensibleRateOvelap(rates, localTimeZone);
            
            /* get the merged rate list. */
            rates = processExtensibleRateMerge(rates, localTimeZone);
            
            Date roundDownedExpiryDateLocal = getRoundDownDate(expiryDateLocal);
            
            if (rates.size() == 1) {
                Calendar freeParkingEnd = Calendar.getInstance();
                freeParkingEnd.setTime(rates.get(0).getStartDateLocal());
                freeParkingEnd.add(Calendar.DATE, 1);
                if (smsRateInfo.getFreeParkingEndDate() != null && freeParkingEnd.getTime().after(smsRateInfo.getFreeParkingEndDate())) {
                    smsRateInfo.setFreeParkingTime(timeDiffInMinutes(rates.get(0).getEndDateLocal(), smsRateInfo.getFreeParkingEndDate()));
                } else {
                    smsRateInfo.setFreeParkingTime(timeDiffInMinutes(rates.get(0).getEndDateLocal(), freeParkingEnd.getTime()));
                }
            } else {
                if (smsRateInfo.getFreeParkingEndDate() != null && rates.get(1).getStartDateLocal().after(smsRateInfo.getFreeParkingEndDate())) {
                    smsRateInfo.setFreeParkingTime(timeDiffInMinutes(rates.get(0).getEndDateLocal(), smsRateInfo.getFreeParkingEndDate()));
                } else {
                    smsRateInfo.setFreeParkingTime(timeDiffInMinutes(rates.get(0).getEndDateLocal(), rates.get(1).getStartDateLocal()));
                }
            }
            
            // remove all rates with end date <= expiry date
            ArrayList<ExtensibleRate> toBeRemoved = new ArrayList<ExtensibleRate>();
            for (ExtensibleRate r : rates) {
                if (r.getEndDateLocal().compareTo(roundDownedExpiryDateLocal) <= 0) {
                    toBeRemoved.add(r);
                }
            }
            for (ExtensibleRate removeMe : toBeRemoved) {
                rates.remove(removeMe);
            }
            
            /* convert list values to EMSRate array. */
            ExtensibleRate[] rateArray = new ExtensibleRate[rates.size()];
            rateArray = rates.toArray(rateArray);
            /* in case where more than one rate exists. */
            if (rateArray.length > 1) {
                ExtensibleRate r1;
                for (int i = 0; i < rateArray.length; i++) {
                    r1 = rateArray[i];
                    if (roundDownedExpiryDateLocal.getTime() >= r1.getEndDateLocal().getTime()) {
                        continue;
                    }
                    
                    if (roundDownedExpiryDateLocal.getTime() >= r1.getStartDateLocal().getTime()
                        && roundDownedExpiryDateLocal.getTime() <= r1.getEndDateLocal().getTime()) {
                        firstRate = r1.copyExtensibleRate();
                        if (i < rateArray.length - 1) {
                            if (firstRate.isConnectedRate(rateArray[i + 1])) {
                                if (addDateInMinutes(roundDownedExpiryDateLocal, maxAllowedTimeBasedOnPermission).compareTo(
                                    firstRate.getEndDateLocal()) <= 0) {
                                    smsRateInfo.setIsRate2Need(false);
                                } else {
                                    secondRate = rateArray[i + 1];
                                }
                            } else {
                                int newMaxAllowedTime = timeDiffInMinutes(roundDownedExpiryDateLocal, rateArray[i + 1].getStartDateLocal());
                                if (smsRateInfo.getMaxAllowedTimeByPermission() - newMaxAllowedTime >= 0) {
                                    smsRateInfo.setIsSecondPermissionNoParking(false);
                                    smsRateInfo.setMaxAllowedTimeByPermission(newMaxAllowedTime);
                                }
                            }
                        }
                        
                        break;
                    } else if (roundDownedExpiryDateLocal.getTime() < r1.getStartDateLocal().getTime()) {
                        if (secondRate == null) {
                            secondRate = r1.copyExtensibleRate();
                            break;
                        }
                    }
                }
                if (firstRate == null && secondRate == null) {
                    logger.warn("!!!! should not come here since both rates are null !!!!");
                } else if (firstRate == null && secondRate != null) {
                    /* first rate = free parking, second rate != null */
                    logger.info("++++ first rate = free parking, second rate != null ++++");
                } else if (firstRate != null && secondRate == null) {
                    logger.info("++++ only found rate1, second rate = null ++++");
                } else {
                    logger.info("++++ found rate1 and rate2 ++++");
                }
            } else /* only one rate available */
            {
                if (roundDownedExpiryDateLocal.getTime() >= rateArray[0].getStartDateLocal().getTime()
                    && roundDownedExpiryDateLocal.getTime() <= rateArray[0].getEndDateLocal().getTime()) {
                    if (addDateInMinutes(roundDownedExpiryDateLocal, maxAllowedTimeBasedOnPermission).after(rateArray[0].getEndDateLocal())) {
                        smsRateInfo.setIsSecondPermissionNoParking(false);
                    }
                    
                    /* firstRate != null, secondRate = free parking */
                    firstRate = rateArray[0].copyExtensibleRate();
                    smsRateInfo.setIsRate2Need(false);
                } else if (roundDownedExpiryDateLocal.getTime() < rateArray[0].getStartDateLocal().getTime()) {
                    /* firstRate = free parking, secondRate != null */
                    secondRate = rateArray[0].copyExtensibleRate();
                } else {
                    logger.info("++++ both rates are null ++++");
                }
            }
        } else {
            logger.info("++++ No rates defined. firstRate = null, secondRate = null ++++");
        }
        
        return new ExtensibleRate[] { firstRate, secondRate };
    }
    
    private int timeDiffInMinutes(Date dateStart, Date dateEnd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateStart);
        cal.set(Calendar.SECOND, 0);
        Date newDateStart = cal.getTime();
        cal.setTime(dateEnd);
        cal.set(Calendar.SECOND, 0);
        Date newDateEnd = cal.getTime();
        
        int diff = (int) (newDateEnd.getTime() - newDateStart.getTime()) / 60000;
        return diff;
    }
    
    private Date getRoundDownDate(Date date) {
        Calendar cal = Calendar.getInstance();
        long timeMs = date.getTime();
        timeMs = timeMs / 1000 * 1000;
        date.setTime(timeMs);
        cal.setTime(date);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }
    
    private Date addDateInMinutes(Date date, int minutes) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, minutes);
        return cal.getTime();
    }
    
    private void addRateToList(List<ExtensibleRate> list, ExtensibleRate rate, boolean isExisting, int position) {
        if (isExisting) {
            list.add(position, rate);
        } else {
            list.add(rate);
        }
    }
    
    private void setRateStartDateTimeLocal(ExtensibleRate rate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(rate.getStartDateLocal());
        cal.set(Calendar.HOUR_OF_DAY, rate.getBeginHourLocal());
        cal.set(Calendar.MINUTE, rate.getBeginMinuteLocal());
        cal.set(Calendar.SECOND, 0);
        rate.setStartDateLocal(cal.getTime());
    }
    
    private void setRateEndDateTimeGMT(ExtensibleRate rate, String localTimeZone) {
        Date dateLocal = DateUtil.changeTimeZone(new Date(), localTimeZone);
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateLocal);
        cal.set(Calendar.HOUR_OF_DAY, rate.getEndHourLocal());
        cal.set(Calendar.MINUTE, rate.getEndMinuteLocal());
        cal.set(Calendar.SECOND, 0);
        if (rate.getEndHourLocal() == 0) {
            cal.add(Calendar.DAY_OF_MONTH, 1);
        }
        Date rate3NewEndDateGMT = DateUtil.getDatetimeInGMT(cal.getTime(), localTimeZone);
        rate.setEndDateGMT(rate3NewEndDateGMT);
    }
    
    private final Runnable removeExpired() {
        return new Runnable() {
            public void run() {
                for (final String name : cache.keySet()) {
                    if (System.currentTimeMillis() > cache.get(name).getExpiryDateGMT().getTime()) {
                        cacheKeyQueue.remove(name);
                        cache.remove(name);
                    }
                }
            }
        };
    }
    
    private String getPermissionEndDateForDisplay(int hourLocal, int minuteLocal, boolean isEndTime) {
        DecimalFormat hourMinuteFormatter = new DecimalFormat("00");
        StringBuilder builder = new StringBuilder();
        String str = hourMinuteFormatter.format(hourLocal);
        if (isEndTime && str.equals("00")) {
            str = "24";
        }
        builder.append(str);
        builder.append(":");
        builder.append(hourMinuteFormatter.format(minuteLocal));
        return builder.toString();
    }
    
    private List<ParkingPermission> getParkingPermissionsForSMSMessage(String localTimeZone, Date localExpiryDate, Date maxPermitDate,
        Integer locationId) {
        List result;
        int dayOfWeek = getDateOfWeek(localExpiryDate);
        if (dayOfWeek == Calendar.SATURDAY) {
            result = this.parkingPermissionService.getWeekendParkingPermissionsForSMSMessage(localExpiryDate, maxPermitDate, locationId);
        } else {
            result = this.parkingPermissionService.getParkingPermissionsForSMSMessage(localExpiryDate, maxPermitDate, locationId);
        }
        List<ParkingPermission> permissions = new ArrayList<ParkingPermission>();
        List<ParkingPermissionDayOfWeek> permissionDayOfWeeks = new ArrayList<ParkingPermissionDayOfWeek>();
        if (result.size() > 0) {
            Iterator it = result.iterator();
            while (it.hasNext()) {
                Object[] resultObj = (Object[]) it.next();
                ParkingPermission p = (ParkingPermission) resultObj[0];
                
                SpecialPermissionDate specialPermission = (SpecialPermissionDate) resultObj[2];
                /*
                 * if specialParkingPermissionId is not null but EMSSpecialParkingPermissionDate.getStartDateLocal() is null,
                 * then it is not active data so don't include it in the result set.
                 */
                if (p.getSpecialPermissionDate() != null && p.getSpecialPermissionDate().getId() != 0L
                    && specialPermission.getStartDateLocal() == null) {
                    continue;
                }
                
                permissions.add(p);
                ParkingPermissionDayOfWeek pow = (ParkingPermissionDayOfWeek) resultObj[1];
                permissionDayOfWeeks.add(pow);
            }
        }
        // clone permissions
        ParkingPermission[] pArray = new ParkingPermission[permissions.size()];
        pArray = permissions.toArray(pArray);
        permissions.removeAll(permissions);
        for (ParkingPermission p : pArray) {
            permissions.add(p.copyParkingPermission());
        }
        // clone permissionDayOfWeek
        ParkingPermissionDayOfWeek[] powArray = new ParkingPermissionDayOfWeek[permissionDayOfWeeks.size()];
        powArray = permissionDayOfWeeks.toArray(powArray);
        permissionDayOfWeeks.removeAll(permissionDayOfWeeks);
        for (ParkingPermissionDayOfWeek pow : powArray) {
            permissionDayOfWeeks.add(pow.copyParkingPermissionDayOfWeek());
        }
        
        Date expiryDateDateinGMT = DateUtil.getDatetimeInGMT(localExpiryDate, localTimeZone);
        
        if (!permissions.isEmpty()) {
            //            if (logger.isDebugEnabled()) {
            //                logger.debug("------- following permissions have been loaded to check --------");
            //                for (ParkingPermission permission : permissions) {
            //                    logger.debug(permission);
            //                }
            //            }
            
            Date dateLocal = DateUtil.changeTimeZone(expiryDateDateinGMT, localTimeZone);
            ParkingPermission permission;
            ParkingPermissionDayOfWeek pow;
            int dowP1 = -1;
            for (int i = 0; i < permissions.size(); i++) {
                permission = permissions.get(i);
                pow = permissionDayOfWeeks.get(i);
                SpecialPermissionDate specialPermission = permission.getSpecialPermissionDate();
                if (specialPermission != null) {
                    Date endDateLocal = specialPermission.getEndDateLocal();
                    Date startDateLocal = specialPermission.getStartDateLocal();
                    
                    permission.setStartDateLocal(startDateLocal);
                    permission.setEndDateLocal(endDateLocal);
                    
                    //                    if (logger.isDebugEnabled()) {
                    //                        Date specialPermissionEndDateGMT = DateUtil.getDatetimeInGMT(endDateLocal, localTimeZone);
                    //                        Date specialPermissionStartDateGMT = DateUtil.getDatetimeInGMT(startDateLocal, localTimeZone);
                    //                        logger.debug("+++ specialPermissionStartDateGMT: " + specialPermissionStartDateGMT);
                    //                        logger.debug("+++ specialPermissionEndDateGMT: " + specialPermissionEndDateGMT);
                    //                        logger.info("++++ found special permission: " + permission + " +++");
                    //                    }
                } else {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(dateLocal);
                    
                    // If Permission1 is Saturday, then Permission2 will Be Sunday and we must move
                    // the calendar into the next week or setting DAY_OF_WEEK = 1 will roll the calendar
                    // back to Sunday at the beginning of the week (we want Sunday at the beginning of the
                    // next week).
                    if (i == 0) {
                        dowP1 = pow.getDayOfWeek().getId();
                    } else if (dowP1 == WebCoreConstants.SATURDAY && pow.getDayOfWeek().getId() == WebCoreConstants.SUNDAY) {
                        cal.add(Calendar.DATE, 1);
                    }                    
                    
                    cal.set(Calendar.HOUR_OF_DAY, permission.getBeginHourLocal());
                    cal.set(Calendar.MINUTE, permission.getBeginMinuteLocal());
                    cal.set(Calendar.SECOND, 0);
                    cal.set(Calendar.DAY_OF_WEEK, pow.getDayOfWeek().getId());
                    Date permissionStartDateLocal = cal.getTime();
                    
                    cal.set(Calendar.HOUR_OF_DAY, permission.getEndHourLocal());
                    cal.set(Calendar.MINUTE, permission.getEndMinuteLocal());
                    cal.set(Calendar.SECOND, 0);
                    cal.set(Calendar.DAY_OF_WEEK, pow.getDayOfWeek().getId());
                    if (permission.getEndHourLocal() == 0) {
                        cal.add(Calendar.DAY_OF_MONTH, 1);
                        dateLocal = DateUtils.addDays(dateLocal, 1);
                    }
                    Date permissionEndDateLocal = cal.getTime();
                    
                    permission.setStartDateLocal(permissionStartDateLocal);
                    permission.setEndDateLocal(permissionEndDateLocal);
                    
                }
            }
        }
        
        return permissions;
    }
    
    private void setPermissionStartDateTimeLocal(ParkingPermission permission) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(permission.getStartDateLocal());
        cal.set(Calendar.HOUR_OF_DAY, permission.getBeginHourLocal());
        cal.set(Calendar.MINUTE, permission.getBeginMinuteLocal());
        cal.set(Calendar.SECOND, 0);
        permission.setStartDateLocal(cal.getTime());
    }
    
    private void setPermissionEndDateTimeLocal(ParkingPermission permission) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(permission.getEndDateLocal());
        cal.set(Calendar.HOUR_OF_DAY, permission.getEndHourLocal());
        cal.set(Calendar.MINUTE, permission.getEndMinuteLocal());
        cal.set(Calendar.SECOND, 0);
        if (permission.getEndHourLocal() == 0) {
            cal.add(Calendar.DAY_OF_MONTH, 1);
        }
        permission.setEndDateLocal(cal.getTime());
    }
    
    private List<ExtensibleRate> getExtensibleRatesForSMSMessage(String localTimeZone, Date expiryDateLocal, Date localMaxExtendedDate,
        Integer locationId) {
        List result;
        int dayOfWeek1 = getDateOfWeek(expiryDateLocal);
        int dayOfWeek2 = getDateOfWeek(localMaxExtendedDate);
        if (dayOfWeek1 == Calendar.SATURDAY && dayOfWeek2 == Calendar.SUNDAY) {
            result = this.extensibleRateService.getWeekendEmsRatesForSMSMessage(expiryDateLocal, localMaxExtendedDate, locationId);
        } else {
            result = this.extensibleRateService.getEmsRatesForSMSMessage(expiryDateLocal, localMaxExtendedDate, locationId);
        }
        List<ExtensibleRate> rates = new ArrayList<ExtensibleRate>();
        
        List<Integer> rateDayOfWeeks = new ArrayList<Integer>();
        if (result.size() > 0) {
            Iterator it = result.iterator();
            while (it.hasNext()) {
                Object[] resultObj = (Object[]) it.next();
                ExtensibleRate r = (ExtensibleRate) resultObj[0];
                
                SpecialRateDate specialRate = (SpecialRateDate) resultObj[2];
                /*
                 * if specialRateId is not null but EMSSpecialRateDate.getStartDateLocal() is null, then it is not active data
                 * so don't include it in the result set.
                 */
                if (r.getSpecialRateDate() != null && r.getSpecialRateDate().getId() != 0L && specialRate.getStartDateLocal() == null) {
                    continue;
                }
                
                rates.add(r);
                
                Byte dayOfWeekValue = (Byte) resultObj[3];
                
                rateDayOfWeeks.add(dayOfWeekValue.intValue());
            }
        }
        // clone rates
        ExtensibleRate[] rArray = new ExtensibleRate[rates.size()];
        rArray = rates.toArray(rArray);
        rates.removeAll(rates);
        for (ExtensibleRate r : rArray) {
            rates.add(r.copyExtensibleRate());
        }
        
        Date expiryDateDateinGMT = DateUtil.getDatetimeInGMT(expiryDateLocal, localTimeZone);
        
        if (!rates.isEmpty()) {
            //            if (logger.isDebugEnabled()) {
            //                logger.debug("------- following rates have been loaded to check --------");
            //                for (ExtensibleRate emsRate : rates) {
            //                    logger.debug(emsRate);
            //                }
            //            }
            Date dateLocal = DateUtil.changeTimeZone(expiryDateDateinGMT, localTimeZone);
            ExtensibleRate emsRate;
            int dowR1 = -1;
          
            for (int i = 0; i < rates.size(); i++) {
                emsRate = rates.get(i);
                Integer dow = rateDayOfWeeks.get(i);
                SpecialRateDate emsSpecialRateDate = emsRate.getSpecialRateDate();
                if (emsSpecialRateDate != null) {
                    Date endDateLocal = emsSpecialRateDate.getEndDateLocal();
                    Date specialRateEndDateGMT = DateUtil.getDatetimeInGMT(endDateLocal, localTimeZone);
                    Date startDateLocal = emsSpecialRateDate.getStartDateLocal();
                    Date specialRateStartDateGMT = DateUtil.getDatetimeInGMT(startDateLocal, localTimeZone);
                    
                    emsRate.setStartDateLocal(startDateLocal);
                    emsRate.setEndDateLocal(endDateLocal);
                    emsRate.setEndDateGMT(specialRateEndDateGMT);
                    
                    //                    logger.debug("+++ expiryDateDateinGMT: " + expiryDateDateinGMT);
                    //                    logger.debug("+++ specialRateStartDateGMT: " + specialRateStartDateGMT);
                    //                    logger.debug("+++ specialRateEndDateGMT: " + specialRateEndDateGMT);
                    //                    logger.info("++++ found special rate: " + emsRate + " +++");
                } else {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(dateLocal);
                    
                    if (i == 0) {
                        dowR1 = dow;
                    } else if (dowR1 == WebCoreConstants.SATURDAY && dow == WebCoreConstants.SUNDAY) {
                        cal.add(Calendar.DATE, 1);
                    } 
                    
                    cal.set(Calendar.HOUR_OF_DAY, emsRate.getBeginHourLocal());
                    cal.set(Calendar.MINUTE, emsRate.getBeginMinuteLocal());
                    cal.set(Calendar.SECOND, 0);
                    cal.set(Calendar.DAY_OF_WEEK, dow);
                    Date rateStartDateLocal = cal.getTime();
                    Date rateStartDateGMT = DateUtil.getDatetimeInGMT(rateStartDateLocal, localTimeZone);
                    
                    cal.set(Calendar.HOUR_OF_DAY, emsRate.getEndHourLocal());
                    cal.set(Calendar.MINUTE, emsRate.getEndMinuteLocal());
                    cal.set(Calendar.SECOND, 0);
                    cal.set(Calendar.DAY_OF_WEEK, dow);
                    if (emsRate.getEndHourLocal() == 0) {
                        cal.add(Calendar.DAY_OF_MONTH, 1);
                        dateLocal = DateUtils.addDays(dateLocal, 1);
                    }
                    Date rateEndDateLocal = cal.getTime();
                    Date rateEndDateGMT = DateUtil.getDatetimeInGMT(rateEndDateLocal, localTimeZone);
                    
                    emsRate.setStartDateLocal(rateStartDateLocal);
                    emsRate.setEndDateLocal(rateEndDateLocal);
                    emsRate.setEndDateGMT(rateEndDateGMT);
                    
                }
            }
        }
        
        return rates;
    }
    
    private int getDateOfWeek(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_WEEK);
    }
}
