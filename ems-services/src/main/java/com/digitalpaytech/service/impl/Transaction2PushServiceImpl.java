package com.digitalpaytech.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.domain.Transaction2Push;
import com.digitalpaytech.service.Transaction2PushService;
import com.digitalpaytech.dao.EntityDao;

@Service("transaction2PushService")
@Transactional(propagation=Propagation.SUPPORTS)
public class Transaction2PushServiceImpl implements Transaction2PushService {
    
    @Autowired
    private EntityDao entityDao;

    @Override
    public void saveTransaction2Push(Transaction2Push transaction2Push) {
        entityDao.save(transaction2Push);
    }
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }    
}
