package com.digitalpaytech.service.impl;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SortedMap;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.client.MerchantClient;
import com.digitalpaytech.client.dto.ObjectResponse;
import com.digitalpaytech.client.dto.merchant.CardReaderConfig;
import com.digitalpaytech.client.dto.merchant.ForTransactionResponse;
import com.digitalpaytech.client.dto.merchant.Merchant;
import com.digitalpaytech.client.dto.merchant.MerchantPage;
import com.digitalpaytech.client.dto.merchant.Terminal;
import com.digitalpaytech.client.dto.merchant.TerminalPage;
import com.digitalpaytech.client.util.CommunicationException;
import com.digitalpaytech.client.util.HystrixExceptionUtil;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.data.InfoAndMerchantAccount;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.GatewayProcessor;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MerchantAccountMigration;
import com.digitalpaytech.domain.MerchantPOS;
import com.digitalpaytech.domain.MerchantStatusType;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.dto.FilterDTOTransformer;
import com.digitalpaytech.dto.merchant.MerchantDetails;
import com.digitalpaytech.exception.DuplicateMerchantAccountException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.ribbon.ClientFactory;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.LinkCardTypeService;
import com.digitalpaytech.service.MerchantAccountMigrationService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.systemadmin.SystemAdminService;
import com.digitalpaytech.service.ProcessorService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.json.JSON;
import com.fasterxml.jackson.core.type.TypeReference;
import com.netflix.hystrix.exception.HystrixRuntimeException;

@Service("merchantAccountService")
@Transactional(propagation = Propagation.REQUIRED)
@SuppressWarnings({ "unchecked", "PMD.TooManyMethods", "PMD.GodClass" })
public class MerchantAccountServiceImpl implements MerchantAccountService {
    
    private static final String CARD_TYPE_ID2_LOG_MSG = ", cardTypeId: ";
    private static final String MERCHANT_ACCOUNT_NOT_FOUND =
            "Cannot find MerchantAccount by posId and cardTypeId, or there are too many return values. posId: ";
    private static final String ID_LABEL = "id";
    private static final String FOR_VALUE_CARD_LABEL = "forValueCard";
    private static final String TERMINAL_TOKEN_LABEL = "terminalToken";
    private static final Logger LOG = Logger.getLogger(MerchantAccountServiceImpl.class);
    private static final String ERROR_MSG = "Duplicate MerchantAccount in database with these information: ";
    private static final String UPDATE_REFERENCE_COUNTER_STMT = "UPDATE MerchantAccount ma SET ma.referenceCounter = :value WHERE ma.id = :id";
    private static final String SELECT_REFERENCE_COUNTER_STMT = "SELECT ma.ReferenceCounter FROM MerchantAccount ma WHERE ma.Id = :id FOR UPDATE";
    private static final String MERCHANT_DETAILS_PROCESSORS_PATH_PREFIX = "com/digitalpaytech/dto/merchant/impl/";
    private static final String MERCHANT_ACCOUNT_ID_LOG = ", MerchantAccount id: ";
    private static final String OBJECT_RESPONSES_ERROR_LOG = "ObjectResponse<T>.getResponse is null, : ";
    private static final String IS_LINK = "isLink";
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private ClientFactory clientFactory;
    
    @Autowired
    private CommonProcessingService commonProcessingService;
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private MessageHelper messageHelper;
    
    @Autowired
    private List<MerchantDetails> merchantDetailsList;
    
    @Autowired
    private MerchantAccountMigrationService merchantAccountMigrationService;
    
    @Autowired
    private ProcessorService processorService;
    
    @Autowired
    private LinkCardTypeService linkCardTypeService;
    
    @Autowired
    private SystemAdminService systemAdminService;
    
    private MerchantClient merchantClient;
    
    private JSON json;
    
    @PostConstruct
    public final void init() {
        this.json = new JSON();
        this.merchantClient = this.clientFactory.from(MerchantClient.class);
        
        LOG.info("merchantDetailsList size: " + this.merchantDetailsList.size());
        
        if (this.merchantDetailsList.isEmpty()) {
            throw new IllegalStateException("Cannot load all MerchantDetails objects in: " + MERCHANT_DETAILS_PROCESSORS_PATH_PREFIX);
        }
    }
    
    @Override
    public List<MerchantAccount> findMerchantAccounts(final int customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("MerchantAccount.findMerchantAccountsByCustomerId", CUSTOMER_ID, customerId, true);
    }
    
    @Override
    public List<MerchantAccount> findMerchantAccounts(final int customerId, final boolean forValueCard) {
        return this.entityDao.getNamedQuery("MerchantAccount.findByCustomerAndAcceptValueCardFlag").setParameter(CUSTOMER_ID, customerId)
                .setParameter(FOR_VALUE_CARD_LABEL, forValueCard).list();
    }
    
    @Override
    public final List<MerchantAccount> findUnusedMerchantAccounts(final int customerId, final boolean forValueCard) {
        return this.entityDao.getNamedQuery("MerchantAccount.findByCustomerAcceptValueCardFlagAndUnused").setParameter(CUSTOMER_ID, customerId)
                .setParameter(FOR_VALUE_CARD_LABEL, forValueCard).list();
    }
    
    @Override
    public final List<MerchantAccount> findMerchAcctsIncludeDeleted(final int customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("MerchantAccount.findMerchtAcctsByCustomerIdIncludeDeleted", CUSTOMER_ID, customerId,
                                                            true);
    }
    
    /**
     * Searches MerchantAccount table with processor id, field1 value and field2 ~ 6 values if exist.
     * 
     * @param criteriaMap
     *            Database query criteria Map object. The Map needs to contain the following:
     * 
     *            MerchantAccountService.PROCESSOR_ID | processId (required, integer in String format)
     *            MerchantAccountService.FIELD_1 | field1 (required, data in String format)
     *            MerchantAccountService.FIELD_2 | field2 (optional, data in String format)
     *            MerchantAccountService.FIELD_3 | field3 (optional, data in String format)
     *            MerchantAccountService.FIELD_4 | field4 (optional, data in String format)
     *            MerchantAccountService.FIELD_5 | field5 (optional, data in String format)
     *            MerchantAccountService.FIELD_6 | field6 (optional, data in String format)
     * 
     * @return MerchantAccount Return null if record doesn't exist.
     */
    @Override
    public MerchantAccount findByProcessorAndFields(final Map<String, String> criteriaMap) throws DuplicateMerchantAccountException {
        final Query query = this.entityDao.getCurrentSession().createQuery(createQueryString(criteriaMap, false, false));
        addParameters(query, criteriaMap, false);
        
        final List<MerchantAccount> list = query.list();
        return getReturnStatement(list, ERROR_MSG + criteriaMap.toString());
    }
    
    /**
     * HQL:
     * FROM MerchantAccount ma WHERE ma.processor.id = :processorId AND ma.field1 = :field1 AND ma.field2 = :field2 AND ma.field3 IS NOT NULL AND ma.isDeleted =
     * FALSE
     */
    @Override
    public List<MerchantAccount> findByProcessorAndFields(final String[] paramNames, final Object[] values) {
        return this.entityDao.findByNamedQueryAndNamedParam("MerchantAccount.findByProcessorAndFields", paramNames, values, true);
    }
    
    private MerchantAccount getReturnStatement(final List<MerchantAccount> list, final String errorMessage) throws DuplicateMerchantAccountException {
        if (list == null || list.isEmpty()) {
            return null;
            
        } else if (list.size() > 1) {
            throw new DuplicateMerchantAccountException(ERROR_MSG + errorMessage);
        }
        return list.get(0);
    }
    
    /**
     * e.g. FROM MerchantAccount ma WHERE processor.id = :processorId
     * AND field1 = :field1
     * AND field2 = 'ABC' (if exists)
     * AND field3 = '12345' (if exists)
     * AND field4 = 'A1B2C3' (if exists)
     * AND field5 = '0987' (if exists)
     * AND field6 = 'DPT' (if exists)
     * 
     * @param criteriaMap
     *            Map<String, String> contains search criteria name & value.
     * @return String query statement depending on criteria (criteriaMap).
     */
    public String createQueryString(final Map<String, String> criteriaMap, final boolean addCustomer, final boolean addEnabled) {
        final StringBuilder bdr = new StringBuilder().append("FROM MerchantAccount ma WHERE ma.processor.id = :processorId");
        if (addCustomer) {
            bdr.append(" AND ma.customer.id = :customerId");
        }
        if (addEnabled) {
            bdr.append(" AND ma.merchantStatusType NOT IN (2, 3)");
        }
        
        bdr.append(" AND ma.field1 = :field1");
        if (criteriaMap.get(FIELD_2) != null) {
            bdr.append(" AND ma.field2 = :field2");
        }
        if (criteriaMap.get(FIELD_3) != null) {
            bdr.append(" AND ma.field3 = :field3");
        }
        if (criteriaMap.get(FIELD_4) != null) {
            bdr.append(" AND ma.field4 = :field4");
        }
        if (criteriaMap.get(FIELD_5) != null) {
            bdr.append(" AND ma.field5 = :field5");
        }
        if (criteriaMap.get(FIELD_6) != null) {
            bdr.append(" AND ma.field6 = :field6");
        }
        return bdr.toString();
    }
    
    private void addParameters(final Query query, final Map<String, String> criteriaMap, final boolean addCustomer) {
        query.setParameter(PROCESSOR_ID, Integer.parseInt(criteriaMap.get(PROCESSOR_ID)));
        if (addCustomer) {
            query.setParameter(CUSTOMER_ID, Integer.parseInt(criteriaMap.get(CUSTOMER_ID)));
        }
        
        query.setParameter(FIELD_1, criteriaMap.get(FIELD_1));
        if (criteriaMap.get(FIELD_2) != null) {
            query.setParameter(FIELD_2, criteriaMap.get(FIELD_2));
        }
        if (criteriaMap.get(FIELD_3) != null) {
            query.setParameter(FIELD_3, criteriaMap.get(FIELD_3));
        }
        if (criteriaMap.get(FIELD_4) != null) {
            query.setParameter(FIELD_4, criteriaMap.get(FIELD_4));
        }
        if (criteriaMap.get(FIELD_5) != null) {
            query.setParameter(FIELD_5, criteriaMap.get(FIELD_5));
        }
        if (criteriaMap.get(FIELD_6) != null) {
            query.setParameter(FIELD_6, criteriaMap.get(FIELD_6));
        }
    }
    
    @Override
    public final void saveMerchantAccount(final MerchantAccount merchantAccount) {
        this.entityDao.save(merchantAccount);
    }
    
    @Override
    public void updateMerchantAccount(final MerchantAccount merchantAccount) {
        this.entityDao.update(merchantAccount);
    }
    
    @Override
    public void saveOrUpdateMerchantAccount(final MerchantAccount merchantAccount) {
        this.entityDao.saveOrUpdate(merchantAccount);
    }
    
    @Override
    public final MerchantAccount findById(final int merchantAccountId) {
        return (MerchantAccount) this.entityDao.get(MerchantAccount.class, merchantAccountId);
    }
    
    @Override
    public final MerchantAccount findWithProcessorById(final Integer merchantAccountId) throws CommunicationException {
        return findWithProcessorById(merchantAccountId, false);
    }
    
    @Override
    public final MerchantAccount findWithProcessorById(final Integer merchantAccountId, final boolean forTest) throws CommunicationException {
        final List<MerchantAccount> list =
                this.entityDao.findByNamedQueryAndNamedParam("MerchantAccount.findByIdAndJoinFetchProcessor", ID_LABEL, merchantAccountId);
        if (list == null || list.isEmpty()) {
            return null;
        }
        final MerchantAccount ma = list.get(0);
        if (!ma.getIsLink()) {
            return ma;
        }
        final Optional<MerchantAccount> optAcct = this.populateMerchantTerminalInformation(ma, forTest);
        if (optAcct.isPresent()) {
            return optAcct.get();
        } else {
            return null;
        }
    }
    
    @Override
    public final void deleteMerchantAccount(final MerchantAccount merchantAccount) {
        this.entityDao.delete(merchantAccount);
    }
    
    @Override
    public List<MerchantAccount> findValidMerchantAccountsByCustomerId(final Integer customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("MerchantAccount.findValidMerchantAccountsByCustomerId", CUSTOMER_ID, customerId);
    }
    
    @Override
    public List<MerchantAccount> findRefundableMerchantAccounts(final Integer customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("MerchantAccount.getRefundableMerchantAccounts", CUSTOMER_ID, customerId);
    }
    
    /**
     * In EMS 6.3.11 com.digitalpioneer.cardprocessing.CardProcessor, getNewReferenceNumber method calls 'merchantAccountAppService_.getNewReferenceNumber'
     * in a synchronized block, and actually business logic is in 'merchantAccountServiceImpl.updateNewReferenceNumber'.
     * 
     * Use native sql query to select and update merchant account reference counter right away, don't keep it in the cache.
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public long updateNewReferenceNumber(final MerchantAccount merchantAccount) {
        
        final SQLQuery sqlquery = this.entityDao.createSQLQuery(SELECT_REFERENCE_COUNTER_STMT);
        sqlquery.setInteger(ID_LABEL, merchantAccount.getId());
        final List<BigInteger> list = sqlquery.list();
        long reference = list.get(0).longValue() + 1;
        
        // First Data Processor does not allow "000000"
        if (String.valueOf(reference).endsWith(CardProcessingConstants.SIX_ZERO)) {
            reference = reference + 1;
        }
        
        // Update Reference.
        final Query query = this.entityDao.getCurrentSession().createQuery(UPDATE_REFERENCE_COUNTER_STMT);
        query.setLong("value", reference);
        query.setInteger(ID_LABEL, merchantAccount.getId());
        query.executeUpdate();
        return reference;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Collection<MerchantAccount> getValidMerchantAccountsForProcessor(final Integer customerId, final Integer processorId) {
        return this.entityDao.findByNamedQueryAndNamedParam("MerchantAccount.getValidMerchantAccountsForProcessor",
                                                            new String[] { CUSTOMER_ID, PROCESSOR_ID }, new Object[] { customerId, processorId });
    }
    
    @Override
    public final Collection<MerchantPOS> findMerchantPosesByMerchantAccountIdAndCardTypeId(final Integer merchantAccountId, final int cardTypeId) {
        return this.entityDao.findByNamedQueryAndNamedParam("MerchantPOS.findMerchantPosesByMerchantAccountIdAndCardTypeId",
                                                            new String[] { MERCHANT_ACCOUNT_ID, CARD_TYPE_ID },
                                                            new Object[] { merchantAccountId, cardTypeId });
    }
    
    @Override
    public final Collection<MerchantPOS> findMerchantPosesByMerchantAccountId(final Integer merchantAccountId) {
        return this.entityDao.findByNamedQueryAndNamedParam("MerchantPOS.findMerchantPosesByMerchantAccountId", MERCHANT_ACCOUNT_ID,
                                                            merchantAccountId);
    }
    
    /**
     * @return MerchantAccount Return 'null' if couldn't find the data.
     */
    @Override
    public MerchantAccount findByPointOfSaleIdAndCardTypeId(final Integer pointOfSaleId, final Integer cardTypeId) {
        final List<MerchantPOS> merchantPoss = this.pointOfSaleService.findMerchPOSByPOSIdNoDeleted(pointOfSaleId);
        for (MerchantPOS merchantPos : merchantPoss) {
            if (merchantPos.getCardType().getId() == cardTypeId) {
                return findById(merchantPos.getMerchantAccount().getId());
            }
        }
        return null;
    }
    
    /**
     * @return MerchantAccount Return 'null' if no data or if merchant acct. is deleted or
     *         if it is not assigned to this point of sale.
     */
    @Override
    public MerchantAccount findByPOSAndCardTypeAndNotDeletedAndMerchantStatusEnabled(final Integer pointOfSaleId, final Integer cardTypeId) {
        final List<MerchantPOS> merchantPoss = this.pointOfSaleService.findMerchPOSByPOSIdNoDeleted(pointOfSaleId);
        for (MerchantPOS merchantPos : merchantPoss) {
            if (merchantPos.getCardType().getId() == cardTypeId) {
                final MerchantAccount merchantAcct = findById(merchantPos.getMerchantAccount().getId());
                if (merchantAcct.getMerchantStatusType().getId() == WebCoreConstants.MERCHANT_STATUS_TYPE_ENABLED_ID) {
                    return merchantAcct;
                }
            }
        }
        return null;
    }
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    @Override
    public List<MerchantAccount> findAllMerchantAccountsByCustomerId(final Integer customerId) {
       
        return this.entityDao.findByNamedQueryAndNamedParam("MerchantAccount.findAllMerchantAccountsByCustomerId", CUSTOMER_ID, customerId);
    }
    
    @Override
    public final MerchantAccount findActiveByProcessorAndFields(final Map<String, String> criteriaMap) throws DuplicateMerchantAccountException {
        final Query query = this.entityDao.getCurrentSession().createQuery(createQueryString(criteriaMap, false, true));
        addParameters(query, criteriaMap, false);
        
        final List<MerchantAccount> list = query.list();
        return getReturnStatement(list, ERROR_MSG + criteriaMap.toString());
    }
    
    @Override
    public final MerchantAccount findByProcessorAndFieldsAndCustomer(final Map<String, String> criteriaMap) throws DuplicateMerchantAccountException {
        boolean addEnabled = false;
        if (criteriaMap.get(MERCHANT_STATUS_TYPE_ID) != null) {
            addEnabled = true;
        }
        final Query query = this.entityDao.getCurrentSession().createQuery(createQueryString(criteriaMap, true, addEnabled));
        addParameters(query, criteriaMap, true);
        
        final List<MerchantAccount> list = query.list();
        return getReturnStatement(list, ERROR_MSG + criteriaMap.toString());
    }
    
    @Override
    public final List<MerchantAccount> findAllMerchantAccountsForReportsByCustomerId(final Integer customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("MerchantAccount.findAllMerchantAccountsForReportsByCustomerId", CUSTOMER_ID, customerId);
    }
    
    /**
     * Populates 'field3' and 'field4' for Alliance or First Data Nashville.
     * 
     * @param merchantAccount
     *            MerchantAccount object needs to be fill data in field 3 & 4.
     * @return InfoAndMerchantAccount if there are existing MerchantAccount with same information, it will return a InfoAndMerchantAccount
     *         object with merchant account names in 'info' property and null in 'merchantAccount'. If input merchantAccount object is populated
     *         with necessary data, it will return a InfoAndMerchantAccount object with null in 'info' and the merchantAccount object.
     */
    @Override
    public InfoAndMerchantAccount preFillMerchantAccount(final MerchantAccount merchantAccount) {
        
        final Object[] objs = WebCoreUtil.createObjectArray(PROCESSOR_ID, FIELD_1, FIELD_2);
        final String[] params = Arrays.asList(objs).toArray(new String[objs.length]);
        final Object[] values =
                WebCoreUtil.createObjectArray(merchantAccount.getProcessor().getId(), merchantAccount.getField1(), merchantAccount.getField2());
        
        final List<MerchantAccount> existList = findByProcessorAndFields(params, values);
        
        final StringBuilder nameList = new StringBuilder();
        for (MerchantAccount tmpAccount : existList) {
            nameList.append(tmpAccount.getUIName()).append(" ,");
        }
        if (nameList.length() > 0) {
            // Remove the trailing space and comma.
            return new InfoAndMerchantAccount(nameList.substring(0, nameList.length() - 2), null);
        }
        
        final MerchantAccount tmpAccount = findByDeletedProcessorFieldsAndMaxReferenceCounter(merchantAccount);
        if (tmpAccount != null) {
            merchantAccount.setReferenceCounter(tmpAccount.getReferenceCounter());
            merchantAccount.setField3(tmpAccount.getField3());
            merchantAccount.setField4(tmpAccount.getField4());
        }
        return new InfoAndMerchantAccount(null, merchantAccount);
    }
    
    @Override
    public final MerchantAccount findByDeletedProcessorFieldsAndMaxReferenceCounter(final MerchantAccount merchantAccount) {
        final Object[] objs = WebCoreUtil.createObjectArray(PROCESSOR_ID, "field1", "field2");
        final String[] params = Arrays.asList(objs).toArray(new String[objs.length]);
        final Object[] values =
                WebCoreUtil.createObjectArray(merchantAccount.getProcessor().getId(), merchantAccount.getField1(), merchantAccount.getField2());
        
        final Query q = this.entityDao.getCurrentSession().getNamedQuery("MerchantAccount.findByDeletedProcessorFieldsAndMaxReferenceCounter")
                .setCacheable(false);
        for (int i = 0; i < params.length; i++) {
            q.setParameter(params[i], values[i]);
        }
        q.setMaxResults(1);
        
        final List<MerchantAccount> list = q.list();
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }
    
    @Override
    public List<FilterDTO> getMerchantAccountFiltersByCustomerId(final List<FilterDTO> result, final int customerId,
        final RandomKeyMapping keyMapping) {
        return this.entityDao.getNamedQuery("MerchantAccount.findMerchantAccountFiltersByCustomerId")
                .setResultTransformer(new FilterDTOTransformer(keyMapping, MerchantAccount.class, result)).setParameter(CUSTOMER_ID, customerId)
                .list();
    }
    
    @Override
    public List<FilterDTO> getRefundableMerchantAccountFiltersByCustomerId(final List<FilterDTO> result, final int customerId,
        final RandomKeyMapping keyMapping) {
        return this.entityDao.getNamedQuery("MerchantAccount.findRefundableMerchantAccountFiltersByCustomerId")
                .setResultTransformer(new FilterDTOTransformer(keyMapping, MerchantAccount.class, result)).setParameter(CUSTOMER_ID, customerId)
                .list();
    }
    
    @Override
    public final List<String> findCustomerNamesByProcessorId(final int processorId) {
        return this.entityDao.findByNamedQueryAndNamedParam("MerchantAccount.findCustomerNamesByProcessorId", PROCESSOR_ID, processorId);
    }
    
    @Override
    public final List<CustomerProperty> findTimeZoneByProcessorId(final Integer processorId) {
        return this.entityDao.findByNamedQueryAndNamedParam("CustomerProperty.findTimeZoneByProcessorId", HibernateConstants.PROCESSOR_ID,
                                                            processorId);
    }
    
    @Override
    public final MerchantAccount findFromMechantPOSByPointOfSaleAndProcId(final Integer pointOfSaleId, final Integer processorId) {
        final String[] params = { POINT_OF_SALE_ID, PROCESSOR_ID };
        final Object[] values = new Object[] { pointOfSaleId, processorId };
        return (MerchantAccount) this.entityDao.findUniqueByNamedQueryAndNamedParam("MerchantAccount.findMerchantAccountFromMerchantPOS", params,
                                                                                    values, false);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final MerchantAccount findByDeletedMerchantPOSPointOfSaleIdAndCardTypeId(final Integer pointOfSaleId, final Integer cardTypeId) {
        final String[] params = { POINT_OF_SALE_ID, CARD_TYPE_ID };
        final Object[] values = new Object[] { pointOfSaleId, cardTypeId };
        final List<MerchantAccount> list = this.entityDao
                .findByNamedQueryAndNamedParam("MerchantAccount.findByDeletedMerchantPOSPointOfSaleIdAndCardTypeId", params, values, false);
        if (list == null || list.isEmpty()) {
            return null;
        }
        if (list.size() > 1) {
            if (LOG.isDebugEnabled()) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append(MERCHANT_ACCOUNT_NOT_FOUND).append(pointOfSaleId).append(CARD_TYPE_ID2_LOG_MSG).append(cardTypeId);
                LOG.debug(bdr.toString());
            }
            return null;
        }
        return list.get(0);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final MerchantAccount findByMerchantPOSPointOfSaleIdAndCardTypeId(final Integer pointOfSaleId, final Integer cardTypeId) {
        final String[] params = { POINT_OF_SALE_ID, CARD_TYPE_ID };
        final Object[] values = new Object[] { pointOfSaleId, cardTypeId };
        final List<MerchantAccount> list =
                this.entityDao.findByNamedQueryAndNamedParam("MerchantAccount.findByMerchantPOSPointOfSaleIdAndCardTypeId", params, values, false);
        if (list == null || list.isEmpty() || list.size() > 1) {
            if (LOG.isDebugEnabled()) {
                final StringBuilder bdr =
                        new StringBuilder().append(MERCHANT_ACCOUNT_NOT_FOUND).append(pointOfSaleId).append(CARD_TYPE_ID2_LOG_MSG).append(cardTypeId);
                LOG.debug(bdr.toString());
            }
            return null;
        }
        return list.get(0);
    }
    
    @Override
    public final List<GatewayProcessor> findGatewayProcessors() {
        return this.entityDao.loadAll(GatewayProcessor.class);
    }
    
    @Override
    public final MerchantPOS findMerchantPosesByMerchantAccountIdAndCardTypeIdAndPOSId(final int merchantAccountId, final int cardTypeId,
        final int pointOfSaleId) {
        final List<MerchantPOS> list =
                this.entityDao.findByNamedQueryAndNamedParam("MerchantPOS.findMerchantPosesByMerchantAccountIdAndCardTypeIdAndPOSId",
                                                             new String[] { MERCHANT_ACCOUNT_ID, CARD_TYPE_ID, POINT_OF_SALE_ID, },
                                                             new Object[] { merchantAccountId, cardTypeId, pointOfSaleId, });
        if (list == null || list.isEmpty() || list.size() > 1) {
            return null;
        }
        return list.get(0);
    }
    
    @Override
    public MerchantPOS findMerchantPOSByMerchantAccountTerminalTokenAndSerialNumber(final String terminalToken, final String serialnumber) {
        final List<MerchantPOS> merchantPosList = this.entityDao
                .findByNamedQueryAndNamedParam("MerchantPOS.findMerchantPOSByMerchantAccountTerminalTokenAndSerialNumber",
                                               new String[] { TERMINAL_TOKEN_LABEL, "serialNumber" }, new Object[] { terminalToken, serialnumber });
        if (merchantPosList == null || merchantPosList.isEmpty()) {
            return null;
        }
        
        if (merchantPosList.size() > 1) {
            LOG.warn(String.format("Multiple mappings found from terminalToken %d to pos %s", terminalToken, serialnumber));
        }
        
        return merchantPosList.get(0);
    }
    
    @Override
    public boolean hasLinkMerchantAccount(final int customerId) {
        return countMerchantAccountsByLink(customerId, true);
    }
    
    @Override
    public boolean hasIrisMerchantAccount(final Integer customerId) {
        return countMerchantAccountsByLink(customerId, false);
    }
    
    private boolean countMerchantAccountsByLink(final int customerId, final boolean isLink) {
        final Long count =
                (Long) this.entityDao.findUniqueByNamedQueryAndNamedParam("MerchantAccount.countByIsLink", new String[] { "customerId", IS_LINK },
                                                                          new Object[] { customerId, isLink }, false);
        if (count == null) {
            return false;
        } else {
            return count > 0;
        }
    }
    
    /**
     * This method uses 'isLink = true'.
     */
    @Override
    public MerchantAccount findByTerminalTokenAndIsLink(final String terminalToken) {
        return findByTerminalTokenAndIsLink(terminalToken, true);
    }
    
    @Override
    public MerchantAccount findByTerminalTokenAndIsLink(final String terminalToken, final boolean isLink) {
        final List<MerchantAccount> merchantAccounts =
                this.entityDao.findByNamedQueryAndNamedParam("MerchantAccount.findByTerminalTokenAndIsLink",
                                                             new String[] { TERMINAL_TOKEN_LABEL, IS_LINK }, new Object[] { terminalToken, isLink });
        if (merchantAccounts == null || merchantAccounts.isEmpty()) {
            return null;
        }
        
        if (merchantAccounts.size() > 1) {
            LOG.warn(String.format("Multiple Merchant Accounts found with terminal token %s returning top result", terminalToken));
        }
        
        return merchantAccounts.get(0);
    }
    
    @Override
    public MerchantAccount migrateMerchantAccount(final Integer customerId, final MerchantAccount merchantAccount) throws InvalidDataException {
        
        final Merchant merchant = locateMerchant(customerId, merchantAccount);
        final Terminal ter;
        if (merchant.getTerminals().size() == StandardConstants.CONSTANT_1) {
            ter = merchant.getTerminals().get(StandardConstants.CONSTANT_0);
        } else {
            ter = merchant.getTerminals().get(merchant.getTerminals().size() - StandardConstants.CONSTANT_1);
        }
        
        // 1. Call Kafka pos_terminal_assignments to populate PointOfSale collection for Linux PS.
        final Customer customer = this.customerService.findCustomer(customerId);
        this.findMerchantPosesByMerchantAccountId(merchantAccount.getId()).forEach(mpos -> {
            if (mpos.getPointOfSale() != null && mpos.getPointOfSale().isIsLinux()) {
                // Use Credit Card for Card Type and null for TerminalToken.
                this.linkCardTypeService.pushPosTerminalAssignments(customer.getUnifiId(), 
                                                                    mpos.getPointOfSale().getSerialNumber(), 
                                                                    systemAdminService.findCardTypeById(WebCoreConstants.CARD_TYPE_CREDIT_CARD),
                                                                    ter.getId());
            }
        });
        
        final MerchantAccount toBeMigratedMerchantAccount;
        // 2. Search it by TerminalToken if Kafka consumer MerchantAccountUpdateTask completed the new MerchantAccount first.
        final MerchantAccount migratedMerchantAccount = this.findByTerminalTokenAndIsLink(ter.getId().toString());
        if (migratedMerchantAccount == null) {
            // Create a new MerchantAccount with Terminal information.
            toBeMigratedMerchantAccount = new MerchantAccount();
            toBeMigratedMerchantAccount.setCustomer(merchantAccount.getCustomer());
            toBeMigratedMerchantAccount.setProcessor(this.processorService.findProcessorReadyForMigration(merchantAccount.getProcessor().getId()));
            toBeMigratedMerchantAccount.setName(merchantAccount.getName());
            toBeMigratedMerchantAccount.setTerminalName(StringUtils.left(ter.getName(), HibernateConstants.VARCHAR_LENGTH_30));
            toBeMigratedMerchantAccount.setTerminalToken(ter.getId().toString());
            toBeMigratedMerchantAccount.setCloseQuarterOfDay(merchantAccount.getCloseQuarterOfDay());
            toBeMigratedMerchantAccount.setIsValidated(ter.getIsValidated());
            toBeMigratedMerchantAccount.setIsLink(true);
            toBeMigratedMerchantAccount.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
            toBeMigratedMerchantAccount.setLastModifiedByUserId(StandardConstants.CONSTANT_1);
            final MerchantStatusType type = new MerchantStatusType();
            type.setId(ter.getIsEnabled() ? WebCoreConstants.MERCHANT_STATUS_TYPE_ENABLED_ID : WebCoreConstants.MERCHANT_STATUS_TYPE_DISABLED_ID);
            toBeMigratedMerchantAccount.setMerchantStatusType(type);
            toBeMigratedMerchantAccount.setReferenceCounter(merchantAccount.getReferenceCounter());
            this.saveMerchantAccount(toBeMigratedMerchantAccount);
        } else {
            // migratedMerchantAccount has been created by MerchantAccountUpdateTask.
            migratedMerchantAccount.setReferenceCounter(merchantAccount.getReferenceCounter());
            this.updateMerchantAccount(migratedMerchantAccount);
            toBeMigratedMerchantAccount = migratedMerchantAccount;
        }
        // 3. Update MerchantPOS with the new MerchantAccount Id.  
        this.findMerchantPosesByMerchantAccountId(merchantAccount.getId()).forEach(mp -> {
                mp.setMerchantAccount(toBeMigratedMerchantAccount);
                mp.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
                this.entityDao.saveOrUpdate(mp);
            });
        
        // 4. Update old Merchant Account with TerminalToken.
        merchantAccount.setPreMigrationTerminalToken(merchantAccount.getTerminalToken());
        merchantAccount.setTerminalToken(ter.getId().toString());
        final MerchantStatusType migratedType = new MerchantStatusType();
        migratedType.setId(WebCoreConstants.MERCHANT_STATUS_TYPE_MIGRATED_ID);
        merchantAccount.setMerchantStatusType(migratedType);
        merchantAccount.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        this.updateMerchantAccount(merchantAccount);
        return toBeMigratedMerchantAccount;
    }
    
    @Override
    public CardReaderConfig getCardReaderConfig(final String terminalToken) throws InvalidDataException {
        try {
            final String jsonString = this.merchantClient.getCardReaderConfig(terminalToken).execute().toString(Charset.defaultCharset());
            return deserialize(jsonString, new TypeReference<ObjectResponse<CardReaderConfig>>() {
            });
        } catch (JsonException je) {
            LOG.error("Failed to get card reader config", je);
            throw new InvalidDataException(je);
        } catch (HystrixRuntimeException hre) {
            final String exp = HystrixExceptionUtil.getRootCause(hre);
            LOG.error(exp);
            throw new InvalidDataException(exp);
        }
        
    }
    
    private Merchant locateMerchant(final Integer customerId, final MerchantAccount merchantAccount) throws InvalidDataException {
        SortedMap<String, String> merchantDetailsMap = null;
        SortedMap<String, String> terminalDetailsMap = null;
        
        for (MerchantDetails details : this.merchantDetailsList) {
            if (details.getProcessorId() == merchantAccount.getProcessor().getId()) {
                merchantDetailsMap = details.buildMerchantDetails(this.messageHelper, merchantAccount);
                terminalDetailsMap = details.buildTerminalDetails(this.messageHelper, merchantAccount, merchantDetailsMap);
                break;
            }
        }
        if (merchantDetailsMap == null) {
            throw new InvalidDataException("Cannot find correct MerchantDetailsMap, processorId: " + merchantAccount.getProcessor().getId());
        }
        Merchant merchant = null;
        try {
            final String resultJson =
                    this.merchantClient.lookupMerchantMigration(merchantDetailsMap, customerId).execute().toString(Charset.defaultCharset());
            merchant = deserialize(resultJson, new TypeReference<ObjectResponse<Merchant>>() {
            });
            updateMerchant(merchant, merchantAccount);
            final String terminalName = createTerminaName(merchant, merchantAccount);
            // Add a Terminal and update status.
            createAndUpdateTerminal(merchant, terminalName, terminalDetailsMap, merchantAccount);
            
        } catch (JsonException jex) {
            LOG.error(jex);
            throw new InvalidDataException(jex);
        } catch (HystrixRuntimeException hre) {
            if (HystrixExceptionUtil.isCommunicationException(hre)) {
                final int httpCode = HystrixExceptionUtil.getCommunicationException(hre).getResponseStatus();
                if (httpCode == WebCoreConstants.HTTP_STATUS_CODE_404) {
                    LOG.info("Received Not Found from merchant-service, creating new merchant account");
                    merchant = createMerchant(customerId, merchantAccount, merchantDetailsMap);
                    updateMerchant(merchant, merchantAccount);
                    // Add a Terminal and update status.
                    createAndUpdateTerminal(merchant, terminalDetailsMap, merchantAccount);
                } else {
                    LOG.error(hre);
                    throw new InvalidDataException(hre);
                }
            } else {
                LOG.error(hre);
                throw new InvalidDataException(hre);
            }
        }
        return merchant;
    }
    
    private Merchant createMerchant(final Integer customerId, final MerchantAccount merchantAccount,
        final SortedMap<String, String> merchantDetailsMap) throws InvalidDataException {
        
        final Merchant merchant = new Merchant();
        merchant.setName(merchantAccount.getName());
        merchant.setCustomerId(customerId);
        merchant.setProcessorType(merchantAccount.getProcessor().getName());
        merchant.setMerchantDetails(merchantDetailsMap);
        merchant.setTimeZone(merchantAccount.getTimeZone());
        merchant.setCloseQuarterOfDay(new Integer(merchantAccount.getCloseQuarterOfDay()));
        
        final String badCardThreshold = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(merchantAccount.getCustomer().getId(),
                                                                        WebCoreConstants.CUSTOMER_PROPERTY_TYPE_CREDIT_CARD_OFFLINE_RETRY)
                .getPropertyValue();
        merchant.setBadCardThreshold(Integer.valueOf(badCardThreshold));
        
        final String numRetries =
                this.customerAdminService
                        .getCustomerPropertyByCustomerIdAndCustomerPropertyType(merchantAccount.getCustomer().getId(),
                                                                                WebCoreConstants.CUSTOMER_PROPERTY_TYPE_MAX_OFFLINE_RETRY)
                        .getPropertyValue();
        merchant.setNumberOfRetries(Integer.valueOf(numRetries));
        
        try {
            final String resultJson = this.merchantClient.addMerchant(merchant).execute().toString(Charset.defaultCharset());
            return deserialize(resultJson, new TypeReference<ObjectResponse<Merchant>>() {
            });
            
        } catch (HystrixRuntimeException hre) {
            final String errorMessage = HystrixExceptionUtil.getFullStackTrace(hre);
            LOG.error(errorMessage);
            throw new InvalidDataException(errorMessage, hre);
            
        } catch (JsonException je) {
            final String msg = "Cannot create new Merchant: " + merchant + MERCHANT_ACCOUNT_ID_LOG + merchantAccount.getId();
            LOG.error(msg, je);
            throw new InvalidDataException(msg, je);
        }
    }
    
    private Merchant updateMerchant(final Merchant merchant, final MerchantAccount merchantAccount) throws InvalidDataException {
        merchant.setIsEnabled(Boolean.TRUE);
        merchant.setName(merchantAccount.getName());
        merchant.setTimeZone(merchantAccount.getTimeZone());
        merchant.setCloseQuarterOfDay(new Integer(merchantAccount.getCloseQuarterOfDay()));
        merchant.setTerminals(null);
        merchant.setCreatedUTC(null);
        merchant.setUpdatedUTC(null);
        merchant.setIsLocked(null);
        try {
            final String resultJson =
                    this.merchantClient.updateMerchant(merchant, merchant.getId().toString()).execute().toString(Charset.defaultCharset());
            return deserialize(resultJson, new TypeReference<ObjectResponse<Merchant>>() {
            });
            
        } catch (HystrixRuntimeException hre) {
            final String errorMessage = HystrixExceptionUtil.getRootCause(hre);
            LOG.error(errorMessage);
            throw new InvalidDataException(errorMessage, hre);
            
        } catch (JsonException je) {
            final String msg =
                    "Cannot update Merchant id: " + merchant.getId() + ", merchant: " + merchant + MERCHANT_ACCOUNT_ID_LOG + merchantAccount.getId();
            LOG.error(msg, je);
            throw new InvalidDataException(msg, je);
        }
    }
    
    private Merchant createAndUpdateTerminal(final Merchant merchant, final String terminalName, final SortedMap<String, String> terminalDetailsMap,
        final MerchantAccount merchantAccount) throws InvalidDataException {
        
        final Terminal terminal = new Terminal();
        terminal.setName(terminalName);
        terminal.setTerminalDetails(terminalDetailsMap);
        try {
            final String resultJson =
                    this.merchantClient.addTerminal(merchant.getId().toString(), terminal).execute().toString(Charset.defaultCharset());
            
            final long refCounter = Long.valueOf(this.commonProcessingService.getNewReferenceNumber(merchantAccount));
            merchantAccount.setReferenceCounter(refCounter);

            final Terminal updatedTerminal =
                    updateTerminal(merchant.getId().toString(),
                                   (Terminal) deserialize(resultJson, new TypeReference<ObjectResponse<Terminal>>() {
                                       }), refCounter);
            
            if (merchant.getTerminals() != null) {
                merchant.getTerminals().add(updatedTerminal);
            } else {
                final List<Terminal> terminals = new ArrayList<>(StandardConstants.CONSTANT_1);
                terminals.add(updatedTerminal);
                merchant.setTerminals(terminals);
            }
            return merchant;
        } catch (JsonException | HystrixRuntimeException e) {
            if (e instanceof HystrixRuntimeException && HystrixExceptionUtil.isCommunicationException((HystrixRuntimeException) e)) {
                final HystrixRuntimeException hre = (HystrixRuntimeException) e;
                LOG.error(HystrixExceptionUtil.getFullStackTrace(hre));
                
                final int statusCode = HystrixExceptionUtil.getCommunicationException(hre).getResponseStatus();
                // Terminal Name already exist.
                if (statusCode == WebCoreConstants.HTTP_STATUS_CODE_409) {
                    merchant.setTerminals(getTerminals(merchant.getId().toString()));
                    return merchant;
                }
            }
            
            final String msg = "Cannot create Terminal, " + createErrorLog(merchant.getId().toString(), terminal) + MERCHANT_ACCOUNT_ID_LOG
                               + merchantAccount.getId();
            LOG.error(msg, e);
            throw new InvalidDataException(msg, e);
        }
    }
    
    private List<Terminal> getTerminals(final String merchantToken) throws InvalidDataException {
        try {
            final String resultJson = this.merchantClient.getTerminals(merchantToken).execute().toString(Charset.defaultCharset());
            
            final ObjectResponse<TerminalPage> objectResponse = this.json.deserialize(resultJson, new TypeReference<ObjectResponse<TerminalPage>>() {
            });
            if (objectResponse.getResponse() == null) {
                throw new JsonException(OBJECT_RESPONSES_ERROR_LOG + objectResponse);
            }
            final TerminalPage terms = objectResponse.getResponse();
            final List<Terminal> list = new ArrayList<Terminal>();
            terms.getContent().forEach(t -> {
                    list.add(t);
                });
            return list;
            
        } catch (HystrixRuntimeException hre) {
            final String errorMessage = HystrixExceptionUtil.getRootCause(hre);
            LOG.error(errorMessage);
            throw new InvalidDataException(errorMessage, hre);
            
        } catch (JsonException je) {
            final String msg = "Cannot get Terminals, merchantToken: " + merchantToken;
            LOG.error(msg, je);
            throw new InvalidDataException(msg, je);
        }
    }
    
    private Terminal updateTerminal(final String merchantToken, final Terminal terminal, final long referenceCounter)
        throws InvalidDataException {
        terminal.setIsEnabled(Boolean.TRUE);
        terminal.setIsLocked(null);
        terminal.setReferenceCounter(referenceCounter);
        terminal.setIsValidated(null);
        terminal.setCreatedUTC(null);
        terminal.setUpdatedUTC(null);
        try {
            final String resultJson = this.merchantClient.updateTerminal(merchantToken, terminal.getId().toString(), terminal).execute()
                    .toString(Charset.defaultCharset());
            
            return deserialize(resultJson, new TypeReference<ObjectResponse<Terminal>>() {
            });
        } catch (HystrixRuntimeException hre) {
            final String errorMessage = HystrixExceptionUtil.getFullStackTrace(hre);
            LOG.error(errorMessage);
            throw new InvalidDataException(errorMessage, hre);
        } catch (JsonException je) {
            final String msg = "Cannot update Terminal, " + createErrorLog(merchantToken, terminal);
            LOG.error(msg, je);
            throw new InvalidDataException(msg, je);
        }
    }
    
    private String createErrorLog(final String merchantToken, final Terminal terminal) {
        return "merchant id: " + merchantToken + ", terminal: " + terminal;
    }
    
    private Merchant createAndUpdateTerminal(final Merchant merchant, final SortedMap<String, String> terminalDetailsMap, final MerchantAccount merchantAccount)
        throws InvalidDataException {
        return createAndUpdateTerminal(merchant, createTerminaName(merchant, merchantAccount), terminalDetailsMap, merchantAccount);
    }
    
    private String createTerminaName(final Merchant merchant, final MerchantAccount merchantAccount) {
        final String name;
        if (StringUtils.isNotBlank(merchantAccount.getTerminalName())) {
            name = merchantAccount.getTerminalName();
        } else {
            name = merchant.getName() + StandardConstants.CHAR_DASH + Terminal.class.getSimpleName();
        }
        if (merchant.getTerminals() != null && merchant.getTerminals().size() > StandardConstants.CONSTANT_0) {
            return name + StandardConstants.CHAR_DASH + (merchant.getTerminals().size() + StandardConstants.CONSTANT_1);
        } else {
            return name;
        }
    }
    
    private <T extends Object> T deserialize(final String resultJson, final TypeReference<ObjectResponse<T>> objectResponseTypeRef)
        throws JsonException {
        final ObjectResponse<T> objectResponse = this.json.deserialize(resultJson, objectResponseTypeRef);
        if (objectResponse.getResponse() != null) {
            return objectResponse.getResponse();
        }
        throw new JsonException(OBJECT_RESPONSES_ERROR_LOG + objectResponse);
    }
    
    @Override
    public final List<MerchantAccount> populateMigrationInfo(final List<MerchantAccount> merchantAccounts) {
        if (merchantAccounts == null || merchantAccounts.isEmpty()) {
            return merchantAccounts;
        }
        Integer customerId = merchantAccounts.get(0).getCustomer().getId();
        final List<MerchantAccountMigration> migrations = this.merchantAccountMigrationService.findByCustomerId(customerId);
        
        Map<Integer, MerchantAccountMigration> merchantAccountIdToMigration = new HashMap<Integer, MerchantAccountMigration>();
        migrations.forEach(mig -> merchantAccountIdToMigration.put(mig.getMerchantAccount().getId(), mig));
        
        List<Processor> processors = this.processorService.findAll();
        Map<Integer, Processor> procIdToProc = new HashMap<Integer, Processor>();
        Map<String, Processor> procNameToProc = new HashMap<String, Processor>();
        processors.forEach(proc -> {
            procIdToProc.put(proc.getId(), proc);
            procNameToProc.put(proc.getName(), proc);
        });
        
        return merchantAccounts.stream().map(ma -> {
            final MerchantAccountMigration migration = merchantAccountIdToMigration.get(ma.getId());
            
            if (migration != null) {
                ma.setMigrationStatusType(migration.getMerchantAccountMigrationStatus());
            } else {
                final MerchantStatusType mst = ma.getMerchantStatusType();
                
                final boolean merchantStatusValid = mst.getId() == WebCoreConstants.MERCHANT_STATUS_TYPE_ENABLED_ID
                                                    || mst.getId() == WebCoreConstants.MERCHANT_STATUS_TYPE_DISABLED_ID;
                
                ma.setIsMigrationAvailable(isProcReadyForMigration(procIdToProc, procNameToProc, ma.getProcessor().getId()) && merchantStatusValid
                                           && ma.getIsValidated());
            }
            return ma;
        }).collect(Collectors.toList());
    }

    private boolean isProcReadyForMigration(Map<Integer, Processor> procIdToProc, Map<String, Processor> procNameToProc, Integer merchantAccountProcessorId) {
        Processor maProc = procIdToProc.get(merchantAccountProcessorId);
        String linkProcName = this.processorService.getCorrespondingLinkProcName(maProc.getName());
        boolean readyForMigration = this.processorService.isProcessorReadyForMigration(procNameToProc.get(linkProcName), true);
        return readyForMigration;
    }
    
    @Override
    public final String testTransaction(final MerchantAccount merchantAccount) {
        final String err = "Cannot locate Merchant";
        final String dotLink = this.messageHelper.getMessageWithDefault(WebCoreConstants.LINK_PROCESSOR_NAME_SUFFIX_KEY,
                                                                        WebCoreConstants.LINK_PROCESSOR_NAME_SUFFIX);
        
        final List<Merchant> merchants = new ArrayList<>();
        try {
            final String resultJson = this.merchantClient.getMerchantsByProcessorType(merchantAccount.getCustomer().getUnifiId(), 
                 WebCoreUtil.getProcessorNameInMerchantService(dotLink, merchantAccount.getProcessor().getName())).execute().toString(Charset.defaultCharset());
            
            final ObjectResponse<MerchantPage> objResp = this.json.deserialize(resultJson, new TypeReference<ObjectResponse<MerchantPage>>() { });
            if (objResp.getResponse() == null || objResp.getResponse().getContent() == null || objResp.getResponse().getContent().isEmpty()) {
                return WebCoreConstants.RESPONSE_FALSE + StandardConstants.STRING_COLON + err;
            }
            
            objResp.getResponse().getContent().forEach(m -> {
                    merchants.add(m);
                });
        } catch (JsonException | HystrixRuntimeException e) {
            LOG.error(err + " by customerId: " + merchantAccount.getCustomer().getId() 
                      + ", merchantAccountId: " + merchantAccount.getId(), e);
            return WebCoreConstants.RESPONSE_FALSE + StandardConstants.STRING_COLON + err;
        }

        // Validate the terminalToken in MerchantAccount exists in Merchants.        
        String merchantToken = null;
        for (Merchant merchant : merchants) {
            if (!merchant.getTerminals().isEmpty()) {
                final Optional<String> optTerminalToken = merchant.getTerminals().stream()
                                                                                 .map(t -> t.getId().toString())
                                                                                 .filter(s -> s.equalsIgnoreCase(merchantAccount.getTerminalToken()))
                                                                                 .findFirst();
                if (optTerminalToken.isPresent()) {
                    merchantToken = merchant.getId().toString();
                    break;
                }
            }
        }
                                                        
        if (StringUtils.isBlank(merchantToken)) {
            LOG.error("TerminalToken: " + merchantAccount.getTerminalToken() + " doesn't exist in MongoDB Merchant collection");
            return WebCoreConstants.RESPONSE_FALSE + StandardConstants.STRING_COLON + "TerminalToken doesn't exist in Merchant";
        }
        
        final String resultJson = 
                this.merchantClient.testTransaction(merchantToken, merchantAccount.getTerminalToken()).execute().toString(Charset.defaultCharset());
        
        if (LOG.isDebugEnabled()) {
            LOG.debug("resultJson: " + resultJson);
        }
        
        if (StringUtils.isBlank(resultJson)) {
            final String errNoResp = "No response";
            LOG.error(errNoResp + " returned by Merchant Service endpoint " + "/api/v1/merchants/{merchantToken}/terminals/{terminalToken}/test for "
                      + "merchantToken: " + merchantToken + ", terminalToken: " + merchantAccount.getTerminalToken());
            return WebCoreConstants.RESPONSE_FALSE + StandardConstants.STRING_COLON + errNoResp;
        }
        
        if (resultJson.contains(CardProcessingConstants.SUCCESS_STRING)) {
            return WebCoreConstants.RESPONSE_TRUE;
        } else {
            return WebCoreConstants.RESPONSE_FALSE;
        }
    }
    
    
    /**
     * populateMerchantTerminalInformation retrieves Merchant and Terminal information by calling 
     * Merchant Service - POST /api/v1/terminals/{terminalToken}/forTransaction endpoint.
     * This endpoint will increases referenceCounter by one.
     * @param merchantAccount LINK merchantAccount
     * @param forTest For testing Link Processor. The valid values are: true, on, or yes.
     * @return Optional<MerchantAccount> If error happens or no result is returned it will return Optional.empty. 
     * @throws CommunicationException 
     */
    @Override
    public final Optional<MerchantAccount> populateMerchantTerminalInformation(final MerchantAccount merchantAccount, final boolean forTest) 
            throws CommunicationException {
        
        final String err = "Cannot retrieve Merchant and Terminal information by TerminalToken." 
                + StandardConstants.STRING_EMPTY_SPACE 
                + merchantAccount.getTerminalToken();
        try {
            final String resultJson = this.merchantClient.forTransaction(merchantAccount.getTerminalToken(), 
                                                                         Boolean.toString(forTest)).execute().toString(Charset.defaultCharset());
            final ObjectResponse<ForTransactionResponse> objResp
                = this.json.deserialize(resultJson, new TypeReference<ObjectResponse<ForTransactionResponse>>() { });
            if (objResp.getResponse() == null || !(objResp.getResponse() instanceof ForTransactionResponse)) {
                LOG.error(err);
                return Optional.empty();
            }
            final ForTransactionResponse forTrans = objResp.getResponse();
            for (MerchantDetails details : this.merchantDetailsList) {
                if (details.getProcessorType().equalsIgnoreCase(merchantAccount.getProcessor().getName())) {
                    details.buildMerchantAccount(this.messageHelper, merchantAccount, forTrans);
                    break;
                }
            }
            if (merchantAccount.areFieldsEmpty()) {
                LOG.error("No matching processorId. MerchantAccount.ProcessorId: " + merchantAccount.getProcessor().getId());
                return Optional.empty();
            }
        } catch (JsonException | HystrixRuntimeException e) {
            LOG.error(err, e);
            if (e instanceof HystrixRuntimeException && HystrixExceptionUtil.isCommunicationException((HystrixRuntimeException) e)) {
                throw HystrixExceptionUtil.getCommunicationException((HystrixRuntimeException) e);
            }
            return Optional.empty();
        }
        this.entityDao.evict(merchantAccount);
        return Optional.of(merchantAccount);
    }
}
