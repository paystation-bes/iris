package com.digitalpaytech.service.impl;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.ActivityLogin;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.service.ActivityLoginService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebSecurityConstants;

@Component("activityLoginService")
@Transactional(propagation = Propagation.SUPPORTS)
public class ActivityLoginServiceImpl implements ActivityLoginService {
    
    @Autowired
    private EntityDao entityDao;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    public Serializable saveActivityLogin(final ActivityLogin activityLogin) {
        
        return this.entityDao.save(activityLogin);
    }
    
    //  private static final String SQL_FIND_ACTIVITYLOGIN_BY_SUCCESSFULLOGIN = "SELECT COUNT(*) FROM ActivityLogin al WHERE al.ActivityGMT = 
    // (SELECT MAX(al2.ActivityGMT) FROM ActivityLogin al2 WHERE al2.UserAccountId = al.UserAccountId) 
    // AND al.ActivityGMT >= DATE_SUB(UTC_TIMESTAMP() - SECOND(UTC_TIMESTAMP()), INTERVAL 1 MINUTE)";
    @Override
    public final int countSuccessfulLoginWithinLastMinute(final Date endTime) {
        final Date startTime = new Date(endTime.getTime() - 60000);
        final Number result = (Number) this.entityDao.getNamedQuery("ActivityLogin.countSuccessLoginWithinDateRange")
                .setTimestamp("startTime", startTime).setTimestamp("endTime", endTime).uniqueResult();
        
        return result.intValue();
    }
    
    private Calendar getLockTime(final int maxLockTimeMinutes) {
        final Calendar lockTime = Calendar.getInstance();
        lockTime.add(Calendar.MINUTE, -maxLockTimeMinutes);
        return lockTime;
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    public List<ActivityLogin> findInvalidActivityLoginByLastModifiedGMT(final String username, final int maxLockTimeMinutes) {
        final List<UserAccount> accounts = this.entityDao.findByNamedQueryAndNamedParam("UserAccount.findUndeletedUserAccountByUserName", "userName",
                                                                                        username.toLowerCase(), true);
        if (!accounts.isEmpty()) {
            final Calendar lockTime = getLockTime(maxLockTimeMinutes);
            return this.entityDao.findByNamedQueryAndNamedParam("ActivityLogin.findInvalidActivityLoginByActivityGMT", new String[] {
                "userAccountId", "lockMinutes" }, new Object[] { accounts.get(0).getId(), lockTime.getTime() });
        }
        
        final List<ActivityLogin> result = new ArrayList<ActivityLogin>(0);
        return result;
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public int findCountInvalidActivityLoginByActivityGMT(final Integer userAccountId, final int loginResultTypeId, final int maxLockTimeMinutes) {
        final Date lastSuccess = findLastSuccessByUserAccountId(userAccountId);
        final Date now = DateUtil.getCurrentGmtDate();
        Calendar lockTime = null;
        /*
         * SRS 3.2.1-2: A successful login shall reset the failed login counter.
         * PRD 3.2.3: When the EMS locks an account, it shall be released after 5 minutes
         */
        if (lastSuccess != null && DateUtil.isInBetween(lastSuccess, getMinutesAgo(now, maxLockTimeMinutes), now)) {
            lockTime = new GregorianCalendar();
            lockTime.setTime(lastSuccess);
        } else {
            lockTime = getLockTime(maxLockTimeMinutes);
        }
        
        final String[] params = { "userAccountId", "loginResultTypeId", "lockMinutes" };
        final Object[] values = new Object[] { userAccountId, loginResultTypeId, lockTime.getTime() };
        final Long c = (Long) this.entityDao.findUniqueByNamedQueryAndNamedParam("ActivityLogin.findCountInvalidActivityLoginByActivityGMT", params,
                                                                                 values, false);
        if (c == null) {
            return 0;
        }
        return c.intValue();
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public Date findLastSuccessByUserAccountId(final Integer userAccountId) {
        final List<Timestamp> list = this.entityDao.findByNamedQueryAndNamedParam("ActivityLogin.findLastSuccess", "userAccountId", userAccountId, false);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    /**
     * @param Date
     *            date object to be subtracted.
     * @param minutes
     *            How many minutes ago, e.g. 5 -> 5 minutes ago
     * @return Date Return Date object that sets to certain minutes ago.
     */
    private Date getMinutesAgo(final Date date, final int minutes) {
        final Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, minutes * -1);
        return cal.getTime();
    }
    
    @Override
    public List<ActivityLogin> findAllActivityForUsageCalculation(final Integer customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("ActivityLogin.findAllActivityForUsageCalculation", "customerId", customerId, true);
    }
}
