package com.digitalpaytech.service.impl;

import com.digitalpaytech.domain.SensorInfoType;
import com.digitalpaytech.dao.OpenSessionDao;
import com.digitalpaytech.service.SensorInfoTypeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.Iterator;
import javax.annotation.PostConstruct;

@Service("sensorInfoTypeService")
public class SensorInfoTypeServiceImpl implements SensorInfoTypeService {
    
    @Autowired
    private OpenSessionDao openSessionDao;

    private Map<Integer, SensorInfoType> map;
    
    @PostConstruct
    private void loadAllToMap() {
        List<SensorInfoType> list = openSessionDao.loadAll(SensorInfoType.class);
        map = new HashMap<Integer, SensorInfoType>(list.size());
        
        Iterator<SensorInfoType> iter = list.iterator();
        while (iter.hasNext()) {
            SensorInfoType sit = iter.next();
            map.put(sit.getId(), sit);
        }
    }
    
    
    @Override
    public SensorInfoType findSensorInfoType(Integer id) {
        return map.get(id);
    }
}
