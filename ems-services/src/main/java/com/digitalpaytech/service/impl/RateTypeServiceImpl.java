package com.digitalpaytech.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.RateType;
import com.digitalpaytech.service.RateTypeService;
import com.digitalpaytech.util.WebCoreConstants;

@Service("rateTypeService")
@Transactional(propagation = Propagation.SUPPORTS)
public class RateTypeServiceImpl implements RateTypeService {
    
    @Autowired
    private EntityDao entityDao;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public final RateType findRateTypeById(final Byte rateTypeId) {
        return this.entityDao.get(RateType.class, rateTypeId);
    }
    
    @Override
    public final List<RateType> findAllRateTypes() {
        return this.entityDao.loadAll(RateType.class);
    }
    
    @Override
    public final List<RateType> findAllRateTypesWithoutHoliday() {
        final List<RateType> rateTypeList = findAllRateTypes();
        for (RateType rateType : rateTypeList) {
            if (rateType.getId() == WebCoreConstants.RATE_TYPE_HOLIDAY) {
                rateTypeList.remove(rateType);
                break;
            }
        }
        return rateTypeList;
    }
}
