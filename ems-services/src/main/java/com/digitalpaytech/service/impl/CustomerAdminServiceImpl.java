package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.dao.OpenSessionDao;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerAgreement;
import com.digitalpaytech.domain.CustomerCard;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerPropertyType;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosStatus;
import com.digitalpaytech.domain.RestAccount;
import com.digitalpaytech.domain.SubscriptionType;
import com.digitalpaytech.domain.TimezoneV;
import com.digitalpaytech.domain.TimezoneVId;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.WebServiceEndPoint;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.dto.RestAccountInfo;
import com.digitalpaytech.dto.WebServiceEndPointInfo;
import com.digitalpaytech.dto.customeradmin.GlobalConfiguration;
import com.digitalpaytech.dto.customeradmin.SystemNotification;
import com.digitalpaytech.service.CardTypeService;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.LicensePlateFileUploadStatusService;
import com.digitalpaytech.service.LicensePlateService;
import com.digitalpaytech.service.NotificationService;
import com.digitalpaytech.service.WebWidgetHelperService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Service("customerAdminService")
@Transactional(propagation = Propagation.SUPPORTS)
@SuppressWarnings({ "PMD.ExcessiveImports", "PMD.GodClass" })
public class CustomerAdminServiceImpl implements CustomerAdminService {
    
    //Need to use in LicensePlateFileUploadStatusService
    public static final String LABEL_SETTINGS_GLOBAL_PREF_NO_PREFERRED_PARKERS = "alert.empty";
    public static final String LABEL_SETTINGS_GLOBAL_PREF_PROCESS_STATUS_KEY_PREFIX = "label.settings.preferredparking.processStatus.";
    
    private static final Logger LOG = Logger.getLogger(CustomerAdminServiceImpl.class);
    
    private static final String LABEL_SETTINGS_GLOBAL_PREF_JURISDICTION_TYPE_LOCATION = "label.settings.globalPref.jurisdiction.type.location";
    
    private static final String LABEL_SETTINGS_GLOBAL_PREF_JURISDICTION_TYPE_CUSTOMER = "label.settings.globalPref.jurisdiction.type.customer";
    private static final String LABEL_SETTINGS_GLOBAL_PREF_JURISDICTION_TYPE_DISABLED = "label.settings.globalPref.jurisdiction.type.disabled";
    
    private static final String LABEL_SETTINGS_GLOBAL_PREF_QUERY_SPACES_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT =
            "label.settings.globalPref.querySpacesBy.customer.expiry.time.of.most.recent.permit";
    
    private static final String LABEL_SETTINGS_GLOBAL_PREF_QUERY_SPACES_BY_CUSTOMER_LATEST_EXPIRY_TIME =
            "label.settings.globalPref.querySpacesBy.customer.latest.expiry.time";
    
    private static final String LABEL_SETTINGS_GLOBAL_PREF_QUERY_SPACES_BY_LOCATION_EXPIRY_TIME_OF_MOST_RECENT_PERMIT =
            "label.settings.globalPref.querySpacesBy.location.expiry.time.of.most.recent.permit";
    
    private static final String LABEL_SETTINGS_GLOBAL_PREF_QUERY_SPACES_BY_LOCATION_LATEST_EXPIRY_TIME =
            "label.settings.globalPref.querySpacesBy.location.latest.expiry.time";
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private OpenSessionDao openSessionDao;
    
    @Autowired
    private WebWidgetHelperService webWidgetHelperService;
    
    @Autowired
    private NotificationService notificationService;
    
    @Autowired
    private CardTypeService cardTypeService;
    
    @Autowired
    private LicensePlateFileUploadStatusService licensePlateFileUploadStatusService;
    
    @Autowired
    private MessageHelper messages;
    
    private List<TimezoneVId> timeZones;
    private List<CustomerPropertyType> customerPropertyTypes;
    private List<CardType> cardTypes;
    private List<FilterDTO> querySpacesBy;
    
    private List<FilterDTO> jurisdictionTypesPreferred;
    private List<FilterDTO> jurisdictionTypesLimited;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final void setOpenSessionDao(final OpenSessionDao openSessionDao) {
        this.openSessionDao = openSessionDao;
    }
    
    public final void setWebWidgetHelperService(final WebWidgetHelperService webWidgetHelperService) {
        this.webWidgetHelperService = webWidgetHelperService;
    }
    
    public final void setNotificationService(final NotificationService notificationService) {
        this.notificationService = notificationService;
    }
    
    public final void setCardTypeService(final CardTypeService cardTypeService) {
        this.cardTypeService = cardTypeService;
    }
    
    public final void setLicensePlateFileUploadStatusService(final LicensePlateFileUploadStatusService licensePlateFileUploadStatusService) {
        this.licensePlateFileUploadStatusService = licensePlateFileUploadStatusService;
    }
    
    @PostConstruct
    @SuppressWarnings("PMD.UnusedPrivateMethod")
    private void loadCustomerAdminProperties() {
        @SuppressWarnings("unchecked")
        final List<TimezoneV> timezoneVList = this.openSessionDao.findByNamedQuery("TimezoneV.findAvailableTimeZoneVs");
        
        if (timezoneVList != null) {
            final List<TimezoneVId> timezoneVIdList = new ArrayList<TimezoneVId>();
            for (TimezoneV timezone : timezoneVList) {
                timezoneVIdList.add(timezone.getId());
            }
            
            this.timeZones = timezoneVIdList;
        }
        
        LOG.info("timeZones, customerPropertyTypes are loaded.");
        
        this.customerPropertyTypes = this.openSessionDao.loadAll(CustomerPropertyType.class);
        
        this.querySpacesBy = new ArrayList<>();
        
        this.querySpacesBy.add(new FilterDTO(createString(PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_LATEST_EXPIRY_TIME),
                this.messages.getMessage(LABEL_SETTINGS_GLOBAL_PREF_QUERY_SPACES_BY_LOCATION_LATEST_EXPIRY_TIME)));
        this.querySpacesBy.add(new FilterDTO(createString(PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_EXPIRY_TIME_OF_MOST_RECENT_PERMIT),
                this.messages.getMessage(LABEL_SETTINGS_GLOBAL_PREF_QUERY_SPACES_BY_LOCATION_EXPIRY_TIME_OF_MOST_RECENT_PERMIT)));
        this.querySpacesBy.add(new FilterDTO(createString(PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME),
                this.messages.getMessage(LABEL_SETTINGS_GLOBAL_PREF_QUERY_SPACES_BY_CUSTOMER_LATEST_EXPIRY_TIME)));
        this.querySpacesBy.add(new FilterDTO(createString(PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT),
                this.messages.getMessage(LABEL_SETTINGS_GLOBAL_PREF_QUERY_SPACES_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT)));
        
        this.jurisdictionTypesPreferred = new ArrayList<>();
        this.jurisdictionTypesPreferred.add(new FilterDTO(createString(PaystationConstants.PAYSTATION_JURISDICTION_PREFERRED_DISABLED),
                this.messages.getMessage(LABEL_SETTINGS_GLOBAL_PREF_JURISDICTION_TYPE_DISABLED)));
        this.jurisdictionTypesPreferred.add(new FilterDTO(createString(PaystationConstants.PAYSTATION_JURISDICTION_PREFERRED_BY_CUSTOMER),
                this.messages.getMessage(LABEL_SETTINGS_GLOBAL_PREF_JURISDICTION_TYPE_CUSTOMER)));
        this.jurisdictionTypesPreferred.add(new FilterDTO(createString(PaystationConstants.PAYSTATION_JURISDICTION_PREFERRED_BY_LOCATION),
                this.messages.getMessage(LABEL_SETTINGS_GLOBAL_PREF_JURISDICTION_TYPE_LOCATION)));
        
        this.jurisdictionTypesLimited = new ArrayList<>();
        this.jurisdictionTypesLimited.add(new FilterDTO(createString(PaystationConstants.PAYSTATION_JURISDICTION_LIMITED_DISABLED),
                this.messages.getMessage(LABEL_SETTINGS_GLOBAL_PREF_JURISDICTION_TYPE_DISABLED)));
        this.jurisdictionTypesLimited.add(new FilterDTO(createString(PaystationConstants.PAYSTATION_JURISDICTION_LIMITED_BY_CUSTOMER),
                this.messages.getMessage(LABEL_SETTINGS_GLOBAL_PREF_JURISDICTION_TYPE_CUSTOMER)));
        this.jurisdictionTypesLimited.add(new FilterDTO(createString(PaystationConstants.PAYSTATION_JURISDICTION_LIMITED_BY_LOCATION),
                this.messages.getMessage(LABEL_SETTINGS_GLOBAL_PREF_JURISDICTION_TYPE_LOCATION)));
        
    }
    
    private String createString(final int i) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append(i);
        return bdr.toString();
    }
    
    /**
     * Global Preferences includes Registration Information (Legal Name of Company & Activation Date), Current Time Zone and Service Information.
     */
    @Override
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    public final GlobalConfiguration findGlobalPreferences(final Customer customer, final GlobalConfiguration globalConfiguration) {
        /*
         * Registration Information
         * Scenario 1: Customer without Parent Customer:
         * IF Customer.ParentCustomerId is null THEN
         * Legal Name of Company = CustomerAgreement.Organization WHERE CustomerAgreement.CustomerId = Customer.Id
         * Activation Date = converted_into_local_time(CustomerAgreement.AgreedGMT) WHERE CustomerAgreement.CustomerId = Customer.Id
         * In this scenario the Customer.Id with the CustomerAgreement is a 'child' Customer, meaning Customer.CustomerTypeId = 3 = Child
         * Scenario 2: Customer with Parent Customer
         * IF Customer.ParentCustomerId is NOT null THEN
         * Legal Name of Company = CustomerAgreement.Organization WHERE CustomerAgreement.CustomerId = Customer.ParentCustomerId
         * Activation Date = converted_into_local_time(CustomerAgreement.AgreedGMT) WHERE CustomerAgreement.CustomerId = Customer.ParentCustomerId
         * In this scenario the Customer.Id (as identified by Customer.ParentCustomerId) with the CustomerAgreement is a 'parent' Customer, meaning
         * Customer.CustomerTypeId = 2 = Parent for the Customer identified by Customer.ParentCustomerId
         */
        final String timezone = this.webWidgetHelperService.getCustomerTimeZoneByCustomerId(customer.getId());
        globalConfiguration.setTimeZone(timezone);
        
        final Set<CustomerAgreement> agreements;
        if (customer.getParentCustomer() == null) {
            agreements = customer.getCustomerAgreements();
        } else {
            agreements = customer.getParentCustomer().getCustomerAgreements();
        }
        
        final Iterator<CustomerAgreement> iter = agreements.iterator();
        if (iter.hasNext()) {
            final CustomerAgreement agr = iter.next();
            setGlobalPrefs(globalConfiguration, agr, timezone);
        }
        
        /* retrieve public and private web service endpoints */
        setWebServiceEndPointsList(globalConfiguration, customer);
        
        /* retrieve REST API account */
        setRESTApiAccount(globalConfiguration, customer);
        
        /* retrieve all service information. */
        globalConfiguration.setSubscriptionTypes(this.entityDao.loadAll(SubscriptionType.class));
        
        /* retrieve subscribed service information. Hide private subscriptions */
        setSubscribedServices(globalConfiguration, customer);
        
        /* retrieve all CustomerProperty. */
        setCustomerProperties(globalConfiguration, customer);
        
        /* retrieve Preferred Parkings status. */
        this.licensePlateFileUploadStatusService.setPreferredParkingsStatus(globalConfiguration, customer);
        
        return globalConfiguration;
    }
    
    private void setWebServiceEndPointsList(final GlobalConfiguration globalConfiguration, final Customer customer) {
        @SuppressWarnings("unchecked")
        final List<WebServiceEndPoint> result = this.entityDao.findByNamedQueryAndNamedParam("WebServiceEndPoint.findWebServiceEndPointByCustomerId",
                                                                                             HibernateConstants.CUSTOMER_ID, customer.getId(), true);
        final List<WebServiceEndPointInfo> infos = new ArrayList<WebServiceEndPointInfo>();
        WebServiceEndPointInfo wesInfo = null;
        for (WebServiceEndPoint webServiceEndPoint : result) {
            wesInfo = new WebServiceEndPointInfo(webServiceEndPoint.getWebServiceEndPointType().getName(), webServiceEndPoint.getToken());
            wesInfo.setComments(webServiceEndPoint.getComments());
            infos.add(wesInfo);
        }
        globalConfiguration.setWebServiceEndPointList(infos);
        
        @SuppressWarnings("unchecked")
        final List<WebServiceEndPoint> resultPrivate =
                this.entityDao.findByNamedQueryAndNamedParam("WebServiceEndPoint.findPrivateWebServiceEndPointByCustomerId",
                                                             HibernateConstants.CUSTOMER_ID, customer.getId(), true);
        final List<WebServiceEndPointInfo> infosPrivate = new ArrayList<WebServiceEndPointInfo>();
        WebServiceEndPointInfo wesInfoPrivate = null;
        for (WebServiceEndPoint webServiceEndPoint : resultPrivate) {
            wesInfoPrivate = new WebServiceEndPointInfo(webServiceEndPoint.getWebServiceEndPointType().getName(), webServiceEndPoint.getToken());
            wesInfoPrivate.setComments(webServiceEndPoint.getComments());
            infosPrivate.add(wesInfoPrivate);
        }
        globalConfiguration.setPrivateWebServiceEndPointList(infosPrivate);
    }
    
    private void setRESTApiAccount(final GlobalConfiguration globalConfiguration, final Customer customer) {
        @SuppressWarnings("unchecked")
        final List<RestAccount> accounts = this.entityDao.findByNamedQueryAndNamedParam("RestAccount.findRestAccountByCustomerId",
                                                                                        HibernateConstants.CUSTOMER_ID, customer.getId(), true);
        
        final List<RestAccountInfo> restInfos = new ArrayList<RestAccountInfo>();
        RestAccountInfo restInfo = null;
        for (RestAccount ra : accounts) {
            restInfo = new RestAccountInfo(ra.getRestAccountType().getName(), ra.getAccountName(), ra.getSecretKey(),
                    ra.getPointOfSale().getSerialNumber());
            restInfo.setComments(ra.getComments());
            restInfos.add(restInfo);
        }
        globalConfiguration.setRestAccountList(restInfos);
    }
    
    private void setSubscribedServices(final GlobalConfiguration globalConfiguration, final Customer customer) {
        @SuppressWarnings("unchecked")
        final List<CustomerSubscription> customerServices =
                this.entityDao.findByNamedQueryAndNamedParam("CustomerSubscription.findCustomerSubscriptionByCustomerId",
                                                             HibernateConstants.CUSTOMER_ID, customer.getId(), true);
        final Iterator<CustomerSubscription> iterator = customerServices.iterator();
        while (iterator.hasNext()) {
            final CustomerSubscription customerSubscription = iterator.next();
            if (!customerSubscription.isIsEnabled() && customerSubscription.getSubscriptionType().isIsPrivate()) {
                iterator.remove();
            }
        }
        
        globalConfiguration.setSubscribedServices(customerServices);
    }
    
    private void setCustomerProperties(final GlobalConfiguration globalConfiguration, final Customer customer) {
        final List<CustomerProperty> customerProperties = getCustomerPropertyByCustomerId(customer.getId());
        globalConfiguration.setCustomerProperties(customerProperties);
        for (CustomerProperty property : customerProperties) {
            
            switch (property.getCustomerPropertyType().getId()) {
                case WebCoreConstants.CUSTOMER_PROPERTY_TYPE_SMS_WARNING_PERIOD:
                    globalConfiguration.setWarningPeriod(property.getPropertyValue());
                    break;
                case WebCoreConstants.CUSTOMER_PROPERTY_TYPE_QUERY_SPACES_BY:
                    String label = null;
                    globalConfiguration.setQuerySpacesByValue(Integer.parseInt(property.getPropertyValue()));
                    if (globalConfiguration.getQuerySpacesByValue() == PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_LATEST_EXPIRY_TIME) {
                        label = this.messages.getMessage(LABEL_SETTINGS_GLOBAL_PREF_QUERY_SPACES_BY_LOCATION_LATEST_EXPIRY_TIME);
                        
                    } else if (globalConfiguration
                            .getQuerySpacesByValue() == PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_EXPIRY_TIME_OF_MOST_RECENT_PERMIT) {
                        label = this.messages.getMessage(LABEL_SETTINGS_GLOBAL_PREF_QUERY_SPACES_BY_LOCATION_EXPIRY_TIME_OF_MOST_RECENT_PERMIT);
                        
                    } else if (globalConfiguration
                            .getQuerySpacesByValue() == PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME) {
                        label = this.messages.getMessage(LABEL_SETTINGS_GLOBAL_PREF_QUERY_SPACES_BY_CUSTOMER_LATEST_EXPIRY_TIME);
                        
                    } else if (globalConfiguration
                            .getQuerySpacesByValue() == PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT) {
                        label = this.messages.getMessage(LABEL_SETTINGS_GLOBAL_PREF_QUERY_SPACES_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT);
                    }
                    globalConfiguration.setQuerySpacesByLabel(label);
                    break;
                case WebCoreConstants.CUSTOMER_PROPERTY_TYPE_JURISDICTION_TYPE_PREFERRED:
                    final String jurisdictionPreferredTypeLabel;
                    globalConfiguration.setJurisdictionTypePreferred(Integer.parseInt(property.getPropertyValue()));
                    if (globalConfiguration.getJurisdictionTypePreferred() == PaystationConstants.PAYSTATION_JURISDICTION_PREFERRED_DISABLED) {
                        jurisdictionPreferredTypeLabel = this.messages.getMessage(LABEL_SETTINGS_GLOBAL_PREF_JURISDICTION_TYPE_DISABLED);
                    } else if (globalConfiguration
                            .getJurisdictionTypePreferred() == PaystationConstants.PAYSTATION_JURISDICTION_PREFERRED_BY_CUSTOMER) {
                        jurisdictionPreferredTypeLabel = this.messages.getMessage(LABEL_SETTINGS_GLOBAL_PREF_JURISDICTION_TYPE_CUSTOMER);
                    } else {
                        jurisdictionPreferredTypeLabel = this.messages.getMessage(LABEL_SETTINGS_GLOBAL_PREF_JURISDICTION_TYPE_LOCATION);
                    }
                    globalConfiguration.setJurisdictionTypePreferredLabel(jurisdictionPreferredTypeLabel);
                    break;
                case WebCoreConstants.CUSTOMER_PROPERTY_TYPE_JURISDICTION_TYPE_LIMITED:
                    final String jurisdictionLimitedTypeLabel;
                    globalConfiguration.setJurisdictionTypeLimited(Integer.parseInt(property.getPropertyValue()));
                    if (globalConfiguration.getJurisdictionTypeLimited() == PaystationConstants.PAYSTATION_JURISDICTION_LIMITED_DISABLED) {
                        jurisdictionLimitedTypeLabel = this.messages.getMessage(LABEL_SETTINGS_GLOBAL_PREF_JURISDICTION_TYPE_DISABLED);
                    } else if (globalConfiguration.getJurisdictionTypeLimited() == PaystationConstants.PAYSTATION_JURISDICTION_LIMITED_BY_CUSTOMER) {
                        jurisdictionLimitedTypeLabel = this.messages.getMessage(LABEL_SETTINGS_GLOBAL_PREF_JURISDICTION_TYPE_CUSTOMER);
                    } else {
                        jurisdictionLimitedTypeLabel = this.messages.getMessage(LABEL_SETTINGS_GLOBAL_PREF_JURISDICTION_TYPE_LOCATION);
                    }
                    globalConfiguration.setJurisdictionTypeLimitedLabel(jurisdictionLimitedTypeLabel);
                    break;
                default:
                    break;
            }
        }
    }
    
    @Override
    public final GlobalConfiguration findGlobalPreferencesTimeZoneOnly(final Customer customer, final GlobalConfiguration globalConfiguration) {
        
        final String timezone = this.webWidgetHelperService.getCustomerTimeZoneByCustomerId(customer.getId());
        globalConfiguration.setTimeZone(timezone);
        
        globalConfiguration.setTimeZones(getTimeZones());
        
        return globalConfiguration;
    }
    
    /**
     * Sets Global Preferences information such as 'Legal Name of Company', 'Activation Date', 'Time Zones'.
     * 
     * @param globalConfiguration
     *            GlobalConfiguration is a pojo containing data for Customer Settings pages.
     * @param customerAgreement
     *            CustomerAgreement object that matches to searching criteria.
     * @param timezone
     *            Customer local time zone.
     * @return GlobalConfiguration Populated GlobalConfiguration object.
     */
    private GlobalConfiguration setGlobalPrefs(final GlobalConfiguration globalConfiguration, final CustomerAgreement customerAgreement,
        final String timezone) {
        globalConfiguration.setLegalName(customerAgreement.getOrganization());
        globalConfiguration.setActivationDate(DateUtil.changeTimeZone(customerAgreement.getAgreedGmt(), timezone));
        if (globalConfiguration.getTimeZones() == null) {
            globalConfiguration.setTimeZones(getTimeZones());
        }
        if (globalConfiguration.getQuerySpacesByOptions() == null) {
            globalConfiguration.setQuerySpacesByOptions(getQuerySpacesByOptions());
        }
        if (globalConfiguration.getJurisdictionTypePreferred() == null) {
            globalConfiguration.setJurisdictionTypesPreferred(getJurisdictionTypesPreferred());
        }
        if (globalConfiguration.getJurisdictionTypesLimited() == null) {
            globalConfiguration.setJurisdictionTypesLimited(getJurisdictionTypesLimited());
        }
        
        return globalConfiguration;
    }
    
    private List<FilterDTO> getJurisdictionTypesPreferred() {
        return this.jurisdictionTypesPreferred;
    }
    
    public final List<FilterDTO> getJurisdictionTypesLimited() {
        return this.jurisdictionTypesLimited;
    }
    
    @Override
    public final List<TimezoneVId> getTimeZones() {
        return this.timeZones;
    }
    
    @Override
    public final List<FilterDTO> getQuerySpacesByOptions() {
        return this.querySpacesBy;
    }
    
    @Override
    public final List<CustomerPropertyType> getCustomerPropertyTypes() {
        return this.customerPropertyTypes;
    }
    
    @Override
    public final List<CardType> getCardTypes() {
        if (this.cardTypes == null) {
            this.cardTypes = this.cardTypeService.loadAll();
        }
        return this.cardTypes;
    }
    
    @Override
    public final void saveOrUpdateCustomerProperty(final CustomerProperty customerProperty) {
        this.entityDao.saveOrUpdate(customerProperty);
    }
    
    @Override
    public final SystemNotification findSystemNotifications() {
        final SystemNotification systemNotification = new SystemNotification();
        systemNotification.setCurrentFutureNotifications(this.notificationService.findNotificationByEffectiveDate());
        systemNotification.setPastNotifications(this.notificationService.findNotificationInPast());
        return systemNotification;
    }
    
    @Override
    public final void saveOrUpdatePointOfSale(final PointOfSale pointOfSale) {
        this.entityDao.saveOrUpdate(pointOfSale);
    }
    
    @Override
    public final void saveOrUpdatePaystation(final Paystation payStation) {
        this.entityDao.saveOrUpdate(payStation);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<Customer> findAllChildCustomers(final int parentCustomerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("Customer.findAllChildCustomersByParentCustomerId", "parentCustomerId", parentCustomerId);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<CustomerProperty> getCustomerPropertyByCustomerId(final int customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("CustomerProperty.findCustomerPropertyByCustomerId", HibernateConstants.CUSTOMER_ID,
                                                            customerId, true);
    }
    
    @Override
    public final Set<CustomerProperty> findCustomerPropertyByCustomerId(final int customerId) {
        final List<CustomerProperty> list = getCustomerPropertyByCustomerId(customerId);
        final Set<CustomerProperty> set = new HashSet<CustomerProperty>(list.size());
        final Iterator<CustomerProperty> iter = list.iterator();
        while (iter.hasNext()) {
            set.add(iter.next());
        }
        return set;
    }
    
    @Override
    @SuppressWarnings({ "checkstyle:designforextension" })
    public CustomerProperty getCustomerPropertyByCustomerIdAndCustomerPropertyType(final int customerId, final int customerPropertyType) {
        final String[] paramNames = { HibernateConstants.CUSTOMER_ID, "customerPropertyType" };
        final Object[] values = { customerId, customerPropertyType };
        return (CustomerProperty) this.entityDao.findUniqueByNamedQueryAndNamedParam(
                                                                                     "CustomerProperty.findCustomerPropertyByCustomerIdAndCustomerPropertyType",
                                                                                     paramNames, values, true);
    }
    
    @Override
    public final void saveOrUpdateCustomerCardType(final CustomerCardType customerCardType) {
        this.entityDao.saveOrUpdate(customerCardType);
    }
    
    @Override
    public final void saveOrUpdatePointOfSaleStatus(final PosStatus pointOfSaleStatus) {
        this.entityDao.saveOrUpdate(pointOfSaleStatus);
        
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<UserAccount> getUserAccountsByCustomerId(final int customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("UserAccount.findUserAccountsByCustomerId", HibernateConstants.CUSTOMER_ID, customerId);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<UserAccount> getUserAccountsWithAliasUsersByCustomerId(final int customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("UserAccount.findUserAccountsWithAliasUsersByCustomerId", HibernateConstants.CUSTOMER_ID,
                                                            customerId);
    }
    
    @Override
    public final Customer findCustomerByCustomerId(final int customerId) {
        return (Customer) this.entityDao.get(Customer.class, customerId);
    }
    
    /**
     * Check if any CustomerCard is not yet expired AND not deleted.
     * 
     * @return boolean return 'true' if there is at least one CustomerCard that is not yet expired and not deleted
     */
    @Override
    public final boolean hasActiveCustomerCard(final CustomerCardType customerCardType) {
        final Set<CustomerCard> set = customerCardType.getCustomerCards();
        for (CustomerCard custCard : set) {
            if (custCard.getCardExpireGmt() != null && custCard.getCardExpireGmt().after(DateUtil.getCurrentGmtDate()) && !custCard.isIsDeleted()) {
                return true;
            }
        }
        return false;
    }
    
    public final void setCardTypes(final List<CardType> cardTypes) {
        this.cardTypes = cardTypes;
    }
    
    /**
     * For some reasons in TransactionBaseThread it couldn't get a proper CustomerCardTypeService object by 'auto-wired' and
     * when it called customerCardTypeService.getCardType, Spring returned "HibernateException: No Hibernate Session bound to thread...".
     * Tried a few different ideas but only this way works.
     */
    @Override
    public final CustomerCardType findCustomerCardTypeById(final int customerCardTypeId) {
        return (CustomerCardType) this.entityDao.get(CustomerCardType.class, customerCardTypeId);
    }
}
