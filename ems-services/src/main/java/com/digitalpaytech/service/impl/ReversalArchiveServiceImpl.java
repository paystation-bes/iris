package com.digitalpaytech.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.service.ReversalArchiveService;

@Service("reversalArchiveService")
@Transactional(propagation = Propagation.SUPPORTS)
public class ReversalArchiveServiceImpl implements ReversalArchiveService {
    
    @Autowired
    private EntityDao entityDao;
    
    @Override
    public final void cleanupCardData() {
        this.entityDao.updateByNamedQuery("ReversalArchive.cleanupCardData", null, null);
    }
    
    @Override
    public final void cleanupCardDataForMigratedCustomers() {
        this.entityDao.updateByNamedQuery("ReversalArchive.cleanupCardDataForMigratedCustomers", null, null);
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
}
