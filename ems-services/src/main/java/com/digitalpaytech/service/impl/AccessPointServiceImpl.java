package com.digitalpaytech.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.AccessPoint;
import com.digitalpaytech.service.AccessPointService;

@Service("accessPointService")
@Transactional(propagation=Propagation.SUPPORTS)
public class AccessPointServiceImpl implements AccessPointService {
    
    @Autowired
    private EntityDao entityDao;
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public List<AccessPoint> loadAll() {
        return entityDao.loadAll(AccessPoint.class);
    }
    
    @Override
    public AccessPoint findAccessPoint(String name) {
        return (AccessPoint) this.entityDao.findUniqueByNamedQueryAndNamedParam("AccessPoint.findByName", "name", name);
    }
    
    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void save(AccessPoint accessPoint) {
        this.entityDao.save(accessPoint);
    }
    
}
