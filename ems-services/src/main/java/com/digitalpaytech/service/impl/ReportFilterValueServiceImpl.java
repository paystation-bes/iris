package com.digitalpaytech.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.domain.ReportFilterValue;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.service.ReportFilterValueService;
import com.digitalpaytech.service.ReportQueueService;

@Service("reportFilterValueService")
@Transactional(propagation=Propagation.SUPPORTS)
public class ReportFilterValueServiceImpl implements ReportFilterValueService {

	@Autowired
	private EntityDao entityDao;
	
	public void setEntityDao(EntityDao entityDao) {
    	this.entityDao = entityDao;
    }

	@Override
	public List<ReportFilterValue> findAll() {
		return entityDao.loadAll(ReportFilterValue.class);
	}


}
