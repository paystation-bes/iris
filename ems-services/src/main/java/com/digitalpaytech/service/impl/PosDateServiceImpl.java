package com.digitalpaytech.service.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.PosDate;
import com.digitalpaytech.service.PosDateService;
import com.digitalpaytech.util.WebCoreConstants;

@Service("posDateService")
@Transactional(propagation = Propagation.REQUIRED)
public class PosDateServiceImpl implements PosDateService {
    
    @Autowired
    private EntityDao entityDao;
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<PosDate> findPosDateByPointOfSale(int pointOfSaleId, String sortOrder, String sortItem, Integer page) {
        
        Criteria crit = entityDao.createCriteria(PosDate.class);
        crit.add(Restrictions.isNotNull("comment"));
        crit.add(Restrictions.eq("pointOfSale.id", pointOfSaleId));
        crit.createAlias("lastModifiedByUserId", "lmbui");
        crit.setFetchMode("lmbui", FetchMode.JOIN);
        
        crit.setCacheable(true);
        
        crit.setMaxResults(WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT);
        crit.setFirstResult(((page != null ? page.intValue() : 1) - 1) * WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT);
        
        if (!StringUtils.isBlank(sortOrder) && sortOrder.equals("ASC")) {
            if (!StringUtils.isBlank(sortItem) && sortItem.equals("date")) {
                crit.addOrder(Order.asc("changedGmt"));
            } else if (!StringUtils.isBlank(sortItem) && sortItem.equals("comment")) {
                crit.addOrder(Order.asc("comment"));
            } else if (!StringUtils.isBlank(sortItem) && sortItem.equals("username")) {
                crit.addOrder(Order.asc("lmbui.userName"));
            } else {
                crit.addOrder(Order.asc("changedGmt"));
            }
        } else if (!StringUtils.isBlank(sortOrder) && sortOrder.equals("DESC")) {
            if (!StringUtils.isBlank(sortItem) && sortItem.equals("date")) {
                crit.addOrder(Order.desc("changedGmt"));
            } else if (!StringUtils.isBlank(sortItem) && sortItem.equals("comment")) {
                crit.addOrder(Order.desc("comment"));
            } else if (!StringUtils.isBlank(sortItem) && sortItem.equals("username")) {
                crit.addOrder(Order.desc("lmbui.userName"));
            } else {
                crit.addOrder(Order.desc("changedGmt"));
            }
        } else {
            crit.addOrder(Order.desc("changedGmt"));
        }
        
        return crit.list();
        
    }
}
