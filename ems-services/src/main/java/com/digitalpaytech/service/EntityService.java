package com.digitalpaytech.service;

import java.util.List;

public interface EntityService {
    Object merge(Object entity);
    <T> List<T> loadAll(Class<T> entityClass);
    void evict(Object entity);
}
