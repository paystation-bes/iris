package com.digitalpaytech.service;

public class BatchProcessingException extends Exception {
    private static final long serialVersionUID = -3323458019641172658L;

    public BatchProcessingException() {
        super();
    }
    
    public BatchProcessingException(String message) {
        super(message);
    }
    
    public BatchProcessingException(String message, Throwable cause) {
        super(message, cause);
    }
}
