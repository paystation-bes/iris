package com.digitalpaytech.service;

import com.digitalpaytech.domain.SensorInfoType;

public interface SensorInfoTypeService {
    public SensorInfoType findSensorInfoType(Integer id);
}
