package com.digitalpaytech.service;

import java.util.Collection;
import java.util.List;

import com.digitalpaytech.domain.CustomerAlertType;

public interface CustomerAlertTypeProcessor {
    List<Integer> save(CustomerAlertType customerAlertType);
    
    void update(CustomerAlertType customerAlertType, Collection<String> emailAddresses, int oldRouteId, int newRouteId);
    
    CustomerAlertType delete(CustomerAlertType customerAlertType, Integer userAccountId);
    
    boolean isAlertThresholdSwitchAllowed(int oldAlertThresholdTypeId, int newAlertThresholdTypeId);
}
