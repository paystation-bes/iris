package com.digitalpaytech.service.support;

public class CustomerMessage {
    private Long customerId;
    private Boolean isDefault;

    public CustomerMessage() {
        super();
    }
    
    public CustomerMessage(final Long customerId, final Boolean isDefault) {
        this.customerId = customerId;
        this.isDefault = isDefault;
    }
    
    public final Long getCustomerId() {
        return this.customerId;
    }
    public final void setCustomerId(final Long customerId) {
        this.customerId = customerId;
    }
    public final Boolean getIsDefault() {
        return this.isDefault;
    }
    public final void setIsDefault(final Boolean isDefault) {
        this.isDefault = isDefault;
    }
}
