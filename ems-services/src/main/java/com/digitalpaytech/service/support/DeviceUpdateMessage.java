package com.digitalpaytech.service.support;

import java.io.Serializable;

public class DeviceUpdateMessage implements Serializable {
    private static final long serialVersionUID = 7276513528844087802L;
    private Integer customerId;
    private String deviceSerialNumber;
    private String deviceName;
    private String deviceType;
    private String deviceSubType;
    private boolean isDeleted;
    
    public DeviceUpdateMessage() {
        super();
    }
    
    public DeviceUpdateMessage(final Integer customerId, final String deviceSerialNumber, final String deviceName, final String deviceSubType,
            final boolean isDeleted) {
        super();
        this.customerId = customerId;
        this.deviceSerialNumber = deviceSerialNumber;
        this.deviceName = deviceName;
        this.deviceType = "PayStation";
        this.deviceSubType = deviceSubType;
        this.isDeleted = isDeleted;
    }
    
    public final Integer getCustomerId() {
        return this.customerId;
    }
    
    public final void setCustomerId(final Integer customerId) {
        this.customerId = customerId;
    }
    
    public final String getDeviceSerialNumber() {
        return this.deviceSerialNumber;
    }
    
    public final void setDeviceSerialNumber(final String deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }
    
    public final String getDeviceName() {
        return this.deviceName;
    }
    
    public final void setDeviceName(final String deviceName) {
        this.deviceName = deviceName;
    }
    
    public final String getDeviceType() {
        return this.deviceType;
    }
    
    public final void setDeviceType(final String deviceType) {
        this.deviceType = deviceType;
    }
    
    public final String getDeviceSubType() {
        return this.deviceSubType;
    }
    
    public final void setDeviceSubType(final String deviceSubType) {
        this.deviceSubType = deviceSubType;
    }
    
    public final boolean getIsDeleted() {
        return this.isDeleted;
    }
    
    public final void setIsDeleted(final boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
    
}
