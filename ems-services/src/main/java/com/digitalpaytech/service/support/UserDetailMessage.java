package com.digitalpaytech.service.support;

import java.util.Set;
import java.util.HashSet;

public class UserDetailMessage {
    private Long userId;
    private String password;
    private String email;
    private String actionType;
    private String firstName;
    private String lastName;
    private Set<CustomerMessage> customers;
    private Boolean isEnabled;
    
    public UserDetailMessage() {
        super();
    }
    
    public UserDetailMessage(final Long userId, 
                             final String password, 
                             final String email, 
                             final String actionType, 
                             final String firstName, 
                             final String lastName,
                             final Set<CustomerMessage> customers, 
                             final Boolean isEnabled) {
        this.userId = userId;
        this.password = password;
        this.email = email;
        this.actionType = actionType;
        this.firstName = firstName;
        this.lastName = lastName;
        this.customers = customers;
        this.isEnabled = isEnabled;
    }

    public UserDetailMessage(final Long userId, 
                             final String password, 
                             final String email, 
                             final String actionType, 
                             final String firstName, 
                             final String lastName,
                             final Boolean isEnabled,
                             final Long customerId,
                             final Boolean isDefault) {
        
        final Set<CustomerMessage> customerMessages = new HashSet<CustomerMessage>(1);
        customerMessages.add(new CustomerMessage(customerId, isDefault));
        this.userId = userId;
        this.password = password;
        this.email = email;
        this.actionType = actionType;
        this.firstName = firstName;
        this.lastName = lastName;
        this.isEnabled = isEnabled;
        this.customers = customerMessages;
    }
    
    public final Long getUserId() {
        return this.userId;
    }
    public final void setUserId(final Long userId) {
        this.userId = userId;
    }
    public final String getPassword() {
        return this.password;
    }
    public final void setPassword(final String password) {
        this.password = password;
    }
    public final String getEmail() {
        return this.email;
    }
    public final void setEmail(final String email) {
        this.email = email;
    }
    public final String getActionType() {
        return this.actionType;
    }
    public final void setActionType(final String actionType) {
        this.actionType = actionType;
    }
    public final String getFirstName() {
        return this.firstName;
    }
    public final void setFirstName(final String firstName) {
        this.firstName = firstName;
    }
    public final String getLastName() {
        return this.lastName;
    }
    public final void setLastName(final String lastName) {
        this.lastName = lastName;
    }
    public final Set<CustomerMessage> getCustomers() {
        return this.customers;
    }
    public final void setCustomers(final Set<CustomerMessage> customers) {
        this.customers = customers;
    }
    public final Boolean getIsEnabled() {
        return this.isEnabled;
    }
    public final void setIsEnabled(final Boolean isEnabled) {
        this.isEnabled = isEnabled;
    }
}
