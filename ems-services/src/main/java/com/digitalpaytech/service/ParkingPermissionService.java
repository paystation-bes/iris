package com.digitalpaytech.service;

import java.util.Date;
import java.util.List;

import com.digitalpaytech.domain.ParkingPermission;

public interface ParkingPermissionService {
    public ParkingPermission findByNameAndLocationId(String name, Integer locationId);
    
    @SuppressWarnings("rawtypes")
    public List getWeekendParkingPermissionsForSMSMessage(Date localExpiryDate, Date maxPermitDate, Integer locationId);
    
    @SuppressWarnings("rawtypes")
    public List getParkingPermissionsForSMSMessage(Date localExpiryDate, Date maxPermitDate, Integer locationId);
}
