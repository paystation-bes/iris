package com.digitalpaytech.service;

import javax.mail.MessagingException;

import com.digitalpaytech.util.dto.Email;

public interface EmailService {
    void sendNow(Email email) throws MessagingException;
}
