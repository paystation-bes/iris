package com.digitalpaytech.service;

import java.util.Collection;
import java.util.List;

import com.digitalpaytech.domain.CollectionType;

public interface CollectionTypeService {
    
    Collection<CollectionType> findAllCollectionTypes();
    
    CollectionType findCollectionType(String name);
    
    CollectionType findCollectionType(int id);
    
    int setLock(int clusterId, int numberOfPaystionToBeExecuted);
    
    List<Integer> calculatePaystationBalance(int collectionLockId, int timeInterval, int preAuthDelayTime);
    
    void processCollectionFlagUnsettledPreAuth(int preAuthDelayTime);
    
    void processCollectionReleaseLock(int clusterId);
    
    void processDeleteCollectionLock();
    
    int getPOSBalanceRecalcableCount();
    
}
