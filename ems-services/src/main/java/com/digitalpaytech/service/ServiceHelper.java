package com.digitalpaytech.service;

import com.digitalpaytech.dto.BatchProcessStatus;

public interface ServiceHelper {
    <B> B bean(Class<B> beanClass);
   
    <B> B bean(String beanName, Class<B> beanClass);
    
    BatchProcessStatus getProcessStatus(String key);
}
