package com.digitalpaytech.service;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.digitalpaytech.domain.AlertThresholdType;
import com.digitalpaytech.domain.EventDefinition;
import com.digitalpaytech.domain.EventDeviceType;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.dto.ActiveAlertSearchCriteria;
import com.digitalpaytech.dto.AlertMapEntry;
import com.digitalpaytech.dto.AlertSearchCriteria;
import com.digitalpaytech.dto.SummarySearchCriteria;
import com.digitalpaytech.dto.customeradmin.AlertInfoEntry;
import com.digitalpaytech.dto.customeradmin.AlertSummaryEntry;
import com.digitalpaytech.dto.customeradmin.CollectionSummaryEntry;

public interface AlertsService {
    
    EventDefinition findEventDefinitionByDeviceStatusActionSeverityId(int deviceTypeId, int statusTypeId, int actionTypeId, int severityTypeId);
    
    EventDefinition findEventDefinitionById(int eventDefinitionId);
    
    List<EventDeviceType> findAllEventDeviceTypes();
    
    EventDeviceType findEventDeviceTypeById(int deviceTypeId);
    
    List<EventSeverityType> findAllEventSeverityTypes();
    
    List<AlertMapEntry> findPointOfSalesWithAlertMap(ActiveAlertSearchCriteria criteria);
    
    Integer locatePageContainsPointOfSaleWithAlert(ActiveAlertSearchCriteria criteria);
    
    List<AlertMapEntry> findPointOfSalesWithAlertDetail(ActiveAlertSearchCriteria criteria, String timeZone);
    
    List<AlertMapEntry> findActivePointOfSalesByRecentCollections(ActiveAlertSearchCriteria criteria, Object[] collectionTypes, Date minDate,
        Date maxDate);
    
    List<CollectionSummaryEntry> findPointOfSalesWithBalanceAndAlertSeverity(SummarySearchCriteria criteria, String timeZone);
    
    List<AlertSummaryEntry> findPointOfSalesForAlertSummary(SummarySearchCriteria criteria, String timeZone);
    
    List<EventDeviceType> findAllEventDeviceTypesExceptPCM();
    
    List<AlertThresholdType> findAlertThresholdTypeByAlertClassTypeId(Byte alertClassTypeId);
    
    List<AlertInfoEntry> findHistory(AlertSearchCriteria criteria, TimeZone timezone, String sortItem, String sortOrder);
    
    List<AlertMapEntry> findPointOfSaleWithClearedPSAlert(AlertSearchCriteria criteria, TimeZone timezone);
}
