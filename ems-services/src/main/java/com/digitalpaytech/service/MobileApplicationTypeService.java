package com.digitalpaytech.service;

import com.digitalpaytech.domain.MobileApplicationType;

public interface MobileApplicationTypeService {

    MobileApplicationType getMobileApplicationTypeById(int id);

}
