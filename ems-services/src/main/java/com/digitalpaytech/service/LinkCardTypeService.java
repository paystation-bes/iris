package com.digitalpaytech.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.util.dto.Pair;

public interface LinkCardTypeService {
    
    String getCardType(int customerId, String encryptedCardData) throws CryptoException;
    
    Collection<Pair<Pattern, String>> getCollapsedBinRanges();
    
    Map<String, List<String>> getBinRanges();
    
    String getCreditCardTypeName(String cardNumber);
    
    List<String> getAllCardNames();
    
    boolean pushPosTerminalAssignments(Integer unifiCustId, String deviceSerialNumber, CardType cardType,
        UUID terminalToken);
}
