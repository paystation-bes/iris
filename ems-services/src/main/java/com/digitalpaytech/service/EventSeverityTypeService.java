package com.digitalpaytech.service;

import com.digitalpaytech.domain.EventSeverityType;

public interface EventSeverityTypeService {
    EventSeverityType findEventSeverityType(Integer id);
}
