package com.digitalpaytech.service.systemadmin;

import java.util.List;

import com.digitalpaytech.client.dto.FirmwareConfig;
import com.digitalpaytech.client.dto.fms.DeviceUser;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MerchantPOS;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.InvalidDataException;

public interface SystemAdminService {
    
    List<Customer> findAllNonDPTCustomers();
    
    List<MerchantAccount> findValidMerchantAccountsByCustomerId(Integer id);
    
    MerchantAccount findMerchantAccountById(Integer merchantAccountId);
    
    void saveOrUpdateMerchantPos(MerchantPOS merchantPos);
    
    CardType findCardTypeById(int cardTypeId);
    
    int createCustomer(Customer customer, UserAccount userAccount, String timeZone);
    
    boolean updateCustomer(Customer customer, UserAccount userAccount);
    
    int createPointOfSale(String serialNumber, Integer customerId, UserAccount userAccount);
    
    int movePointOfSale(Integer posId, Customer oldCustomer, Customer newCustomer, UserAccount userAccount, Boolean isLinux)
        throws InvalidDataException;
    
    void updateMerchantPos(MerchantPOS merchantPos);
    
    FirmwareConfig getFirmwareConfig(String serialNumber) throws ApplicationException;
    
    boolean updateUserInMitreId(PointOfSale pointOfSale, Boolean isActivated, DeviceUser deviceUser) throws InvalidDataException;
    
    void updateDeviceInLCDMS(PointOfSale pointOfSale, Customer customer, Boolean isDeleted) throws InvalidDataException;
    
    void deleteMitreIdAndTelemetryAndLCDMS(PointOfSale pointOfSale, Customer customer) throws InvalidDataException;
    
    void addOrUpdateMitreIdAndTelemetryAndLCDMS(PointOfSale pos, Customer customer, Boolean isLinux, Boolean isActivated, String actionType)
        throws InvalidDataException;
    
    boolean recreateMitreIdAndTelemetryAndLCDMS(String oldSerialNumber, String newSerialNumber, PointOfSale pointOfSale, Customer customer,
        Boolean isActive) throws InvalidDataException;
}
