package com.digitalpaytech.service.systemadmin.impl;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.type.IntegerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.client.TelemetryClient;
import com.digitalpaytech.client.dto.FirmwareConfig;
import com.digitalpaytech.client.dto.fms.DeviceUser;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.LocationPosLog;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MerchantPOS;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosAlertStatus;
import com.digitalpaytech.domain.PosBalance;
import com.digitalpaytech.domain.PosDate;
import com.digitalpaytech.domain.PosHeartbeat;
import com.digitalpaytech.domain.PosMacAddress;
import com.digitalpaytech.domain.PosSensorState;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.PosStatus;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.ribbon.ClientFactory;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.paystation.PosServiceStateService;
import com.digitalpaytech.service.support.DeviceUpdateMessage;
import com.digitalpaytech.service.support.UserDetailMessage;
import com.digitalpaytech.service.systemadmin.SystemAdminService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.ErrorConstants;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.json.JSON;
import com.digitalpaytech.util.json.JSONConfigurationBean;
import com.digitalpaytech.util.kafka.AbstractMessageProducer;
import com.digitalpaytech.util.kafka.InvalidTopicTypeException;
import com.digitalpaytech.util.kafka.KafkaKeyConstants;
import com.fasterxml.jackson.core.type.TypeReference;
import com.netflix.hystrix.exception.HystrixBadRequestException;
import com.netflix.hystrix.exception.HystrixRuntimeException;

@Service("systemAdminService")
@Transactional(propagation = Propagation.REQUIRED)
public class SystemAdminServiceImpl implements SystemAdminService {
    private static final String CALL_SP_MOVE_POINT_OF_SALE =
            "{ call sp_MovePointOfSale(:p_CustomerId_new, :p_CustomerId_old, :p_SerialNumber, :p_ProvisionedGMT, :p_UserId, :p_IsLinux) }";
    private static final Logger LOG = Logger.getLogger(SystemAdminServiceImpl.class);
    private static final String QNAME_REFRESH_POS = "PointOfSale.refreshCacheAfterInsert";
    
    private static final String PARAM_POINT_OF_SALE_IDS = "pointOfSaleIds";
    
    private static final String ATTR_POINT_OF_SALE_ID = "PointOfSaleId";
    
    private static final String PARAM_CUSTOMER_ID = "p_CustomerId";
    private static final String PARAM_PARENT_CUSTOMER_ID = "p_ParentCustomerId";
    private static final String PARAM_IS_PARENT = "p_IsParent";
    private static final String PARAM_CUSTOMER_TYPE_ID = "p_CustomerTypeId";
    private static final String PARAM_UNIFI_ID = "p_UnifiId";
    private static final String PARAM_CUSTOMER_STATUS_TYPE_ID = "p_CustomerStatusTypeId";
    private static final String PARAM_MODEM_TYPE_ID = "p_ModemTypeId";
    private static final String PARAM_SERIAL_NUMBER = "p_SerialNumber";
    private static final String PARAM_PROVISIONED_GMT = "p_ProvisionedGMT";
    private static final String PARAM_USER_ID = "p_UserId";
    private static final String PARAM_PAYSTAION_ID_OLD = "p_PaystationId_old";
    private static final String PARAM_POINT_OF_SALE_ID_OLD = "p_PointOfSaleId_old";
    private static final String PARAM_PAYSTATION_TYPE_ID = "p_PaystationTypeId";
    private static final String PARAM_IS_LINUX = "p_IsLinux";
    
    private static final String FAILED_SEND_MESSAGE_MITREID = "Failed to send the message to Metreid";
    private static final String FAILED_SEND_MESSAGE_LDCMS = "Failed to send the message to LDCMS";
    private static final String CUSTOMER_NOT_DEFINED = "Customer not defined.";
    private static final String FAILED_MOVE_PS_DB = "Failed to move paystation between customer in the database.";
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private CryptoService cryptoService;
    
    @Autowired
    private UserAccountService userAccountService;
    
    @Autowired
    private PointOfSaleService posService;
    
    @Autowired
    private PosServiceStateService posServiceStateService;
    
    @Autowired
    private ClientFactory clientFactory;
    
    @Autowired
    @Qualifier("linkMessageProducer")
    private AbstractMessageProducer producer;
    
    private JSON json;
    
    @Autowired
    private MessageHelper messageHelper;
    
    @PostConstruct
    public final void init() {
        final JSONConfigurationBean jsonConfig = new JSONConfigurationBean(true, false, false);
        this.json = new JSON(jsonConfig);
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<Customer> findAllNonDPTCustomers() {
        return this.entityDao.findByNamedQuery("Customer.findAllNonDPTcustomers", true);
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public final MerchantAccount findMerchantAccountById(final Integer merchantAccountId) {
        return (MerchantAccount) this.entityDao.get(MerchantAccount.class, merchantAccountId);
    }
    
    @Override
    public final void saveOrUpdateMerchantPos(final MerchantPOS merchantPos) {
        this.entityDao.saveOrUpdate(merchantPos);
    }
    
    @Override
    public final void updateMerchantPos(final MerchantPOS merchantPos) {
        this.entityDao.update(merchantPos);
    }
    
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<MerchantAccount> findValidMerchantAccountsByCustomerId(final Integer customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("MerchantAccount.findValidMerchantAccountsByCustomerId", HibernateConstants.CUSTOMER_ID,
                                                            customerId);
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public final CardType findCardTypeById(final int cardTypeId) {
        return (CardType) this.entityDao.get(CardType.class, cardTypeId);
    }
    
    @Override
    public final int createCustomer(final Customer customer, final UserAccount userAccount, final String timeZone) {
        final SQLQuery query = entityDao
                .createSQLQuery("{ call sp_InsertCustomer(:p_CustomerTypeId, :p_CustomerStatusTypeId, :p_ParentCustomerId, :p_CustomerName, "
                        + ":p_TrialExpiryGMT, :p_IsParent, :p_UserName, :p_Password, :p_IsStandardReports, "
                        + ":p_IsAlerts, :p_IsRealTimeCC, :p_IsBatchCC, :p_IsRealTimeValue, :p_IsCoupons, :p_IsValueCards, "
                        + ":p_IsSmartCards, :p_IsExtendByPhone, :p_IsDigitalAPIRead, :p_IsDigitalAPIWrite, :p_Is3rdPartyPayByCell, :p_IsDigitalCollect, "
                        + ":p_IsOnlineConfiguration, :p_IsFlexIntegration, :p_IsCaseIntegration, :p_IsDigitalAPIXChange, "
                        + ":p_IsOnlineRate, :p_Timezone, :p_UserId, :p_PasswordSalt, :" + PARAM_UNIFI_ID + ") }");
        query.addSynchronizedEntityClass(Customer.class);
        query.setParameter(PARAM_CUSTOMER_TYPE_ID, customer.getCustomerType().getId());
        query.setParameter(PARAM_CUSTOMER_STATUS_TYPE_ID, customer.getCustomerStatusType().getId());
        if (customer.getParentCustomer() == null) {
            query.setParameter(PARAM_PARENT_CUSTOMER_ID, null);
        } else {
            query.setParameter(PARAM_PARENT_CUSTOMER_ID, customer.getParentCustomer().getId());
        }
        query.setParameter("p_CustomerName", customer.getName());
        query.setParameter("p_TrialExpiryGMT", customer.getTrialExpiryGmt());
        if (customer.isIsParent()) {
            query.setParameter(PARAM_IS_PARENT, 1);
        } else {
            query.setParameter(PARAM_IS_PARENT, 0);
        }
        query.setParameter("p_UserName", userAccount.getUserName());
        query.setParameter("p_Password", userAccount.getPassword());
        query.setParameter("p_IsStandardReports",
                           getSubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_STANDARD_REPORT, customer.getCustomerSubscriptions()));
        query.setParameter("p_IsAlerts", getSubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_ALERTS, customer.getCustomerSubscriptions()));
        query.setParameter("p_IsRealTimeCC",
                           getSubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_REALTIME_CC_PROCESSING, customer.getCustomerSubscriptions()));
        query.setParameter("p_IsBatchCC",
                           getSubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_BATCH_CC_PROCESSING, customer.getCustomerSubscriptions()));
        query.setParameter("p_IsRealTimeValue", getSubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_REALTIME_CAMPUS_CARD_PROCESSING,
                                                                    customer.getCustomerSubscriptions()));
        query.setParameter("p_IsCoupons", getSubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_COUPONS, customer.getCustomerSubscriptions()));
        query.setParameter("p_IsValueCards", getSubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_PASSCARDS, customer.getCustomerSubscriptions()));
        query.setParameter("p_IsSmartCards",
                           getSubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_SMART_CARDS, customer.getCustomerSubscriptions()));
        query.setParameter("p_IsExtendByPhone",
                           getSubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_EXTEND_BY_PHONE, customer.getCustomerSubscriptions()));
        query.setParameter("p_IsDigitalAPIRead",
                           getSubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_API_READ, customer.getCustomerSubscriptions()));
        query.setParameter("p_IsDigitalAPIWrite",
                           getSubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_API_WRITE, customer.getCustomerSubscriptions()));
        query.setParameter("p_Is3rdPartyPayByCell",
                           getSubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_3RD_PARTY_PAY_BY_CELL, customer.getCustomerSubscriptions()));
        query.setParameter("p_IsDigitalCollect",
                           getSubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_COLLECT, customer.getCustomerSubscriptions()));
        query.setParameter("p_IsOnlineConfiguration", getSubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_ONLINE_PAYSTATION_CONFIGURATION,
                                                                          customer.getCustomerSubscriptions()));
        query.setParameter("p_IsFlexIntegration",
                           getSubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_FLEX_INTEGRATION, customer.getCustomerSubscriptions()));
        query.setParameter("p_IsCaseIntegration",
                           getSubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_CASE_INTEGRATION, customer.getCustomerSubscriptions()));
        query.setParameter("p_IsDigitalAPIXChange",
                           getSubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_API_XCHANGE, customer.getCustomerSubscriptions()));
        query.setParameter("p_IsOnlineRate",
                           getSubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_ONLINE_RATE_CONFIGURATION, customer.getCustomerSubscriptions()));
        
        query.setParameter("p_Timezone", timeZone);
        query.setParameter(PARAM_USER_ID, userAccount.getLastModifiedByUserId());
        query.setParameter("p_PasswordSalt", userAccount.getPasswordSalt());
        query.setParameter(PARAM_UNIFI_ID, customer.getUnifiId());
        query.addScalar("CustomerId", new IntegerType());
        
        @SuppressWarnings("rawtypes")
        final List result = query.list();
        
        if (result == null || result.isEmpty()) {
            return 0;
        } else {
            final Integer customerId = (Integer) result.get(0);
            if (customerId != 0) {
                this.entityDao.bringToCache(this.entityDao.getNamedQuery("Customer.refreshCacheAfterInsert").setParameter("customerId", customerId));
            }
            
            return customerId;
        }
    }
    
    private int getSubscriptionType(final int subscriptionTypeId, final Set<CustomerSubscription> customerSubscriptions) {
        int returnValue = 0;
        
        if (!customerSubscriptions.isEmpty()) {
            for (CustomerSubscription sub : customerSubscriptions) {
                if (subscriptionTypeId == sub.getSubscriptionType().getId()) {
                    returnValue = 1;
                    break;
                }
            }
        }
        
        return returnValue;
    }
    
    private SQLQuery addPosCacheSynchroinzation(final SQLQuery query) {
        return query.addSynchronizedEntityClass(Paystation.class).addSynchronizedEntityClass(PointOfSale.class)
                .addSynchronizedEntityClass(PosBalance.class).addSynchronizedEntityClass(PosServiceState.class)
                .addSynchronizedEntityClass(PosStatus.class).addSynchronizedEntityClass(PosAlertStatus.class)
                .addSynchronizedEntityClass(PosMacAddress.class).addSynchronizedEntityClass(PosHeartbeat.class)
                .addSynchronizedEntityClass(PosSensorState.class).addSynchronizedEntityClass(PosDate.class)
                .addSynchronizedEntityClass(LocationPosLog.class);
    }
    
    @Override
    public final int createPointOfSale(final String serialNumber, final Integer customerId, final UserAccount userAccount) {
        
        final SQLQuery query = this.entityDao
                .createSQLQuery("{ call sp_InsertPointOfSale(:p_CustomerId, :p_PaystationTypeId, :p_ModemTypeId, :p_SerialNumber, :p_ProvisionedGMT, :p_UserId, :p_PaystationId_old, :p_PointOfSaleId_old) }");
        
        addPosCacheSynchroinzation(query);
        
        query.addScalar(ATTR_POINT_OF_SALE_ID, new IntegerType());
        query.setParameter(PARAM_CUSTOMER_ID, customerId);
        query.setParameter(PARAM_MODEM_TYPE_ID, WebCoreConstants.MODEM_TYPE_UNKNOWN);
        query.setParameter(PARAM_SERIAL_NUMBER, serialNumber);
        query.setParameter(PARAM_PROVISIONED_GMT, DateUtil.getCurrentGmtDate());
        query.setParameter(PARAM_USER_ID, userAccount.getId());
        query.setParameter(PARAM_PAYSTAION_ID_OLD, 0);
        query.setParameter(PARAM_POINT_OF_SALE_ID_OLD, 0);
        
        if (serialNumber.matches(WebSecurityConstants.SERIAL_NUMBER_INTELLAPAY_REGEX)) {
            query.setParameter(PARAM_PAYSTATION_TYPE_ID, WebCoreConstants.PAY_STATION_TYPE_TEST_INTELLEPAY);
        } else if (serialNumber.matches(WebSecurityConstants.SERIAL_NUMBER_VIRTUAL_REGEX)) {
            query.setParameter(PARAM_PAYSTATION_TYPE_ID, WebCoreConstants.PAY_STATION_TYPE_TEST_VIRTUAL);
        } else if (serialNumber.matches(WebSecurityConstants.SERIAL_NUMBER_DPT_INTERNAL_REGEX)) {
            query.setParameter(PARAM_PAYSTATION_TYPE_ID, WebCoreConstants.PAY_STATION_TYPE_TEST_DEMO);
        } else if (serialNumber.matches(WebSecurityConstants.SERIAL_NUMBER_LUKE_REGEX)) {
            query.setParameter(PARAM_PAYSTATION_TYPE_ID, WebCoreConstants.PAY_STATION_TYPE_LUKE);
        } else if (serialNumber.matches(WebSecurityConstants.SERIAL_NUMBER_LUKE_RADIUS_REGEX)) {
            query.setParameter(PARAM_PAYSTATION_TYPE_ID, WebCoreConstants.PAY_STATION_TYPE_LUKE_RADIUS);
        } else if (serialNumber.matches(WebSecurityConstants.SERIAL_NUMBER_SHELBY_REGEX)) {
            query.setParameter(PARAM_PAYSTATION_TYPE_ID, WebCoreConstants.PAY_STATION_TYPE_SHELBY);
        } else if (serialNumber.matches(WebSecurityConstants.SERIAL_NUMBER_LUKE_2_REGEX)) {
            query.setParameter(PARAM_PAYSTATION_TYPE_ID, WebCoreConstants.PAY_STATION_TYPE_LUKE_2);
        } else if (serialNumber.matches(WebSecurityConstants.SERIAL_NUMBER_LUKE_B_REGEX)) {
            query.setParameter(PARAM_PAYSTATION_TYPE_ID, WebCoreConstants.PAY_STATION_TYPE_LUKE_B);
        } else {
            return WebCoreConstants.PAY_STATION_CREATION_ERROR_UNKNOWN_TYPE;
        }
        
        final int result = (Integer) query.uniqueResult();
        if (result > 0) {
            final List<Integer> ids = new ArrayList<Integer>(2);
            ids.add(result);
            
            this.entityDao.bringToCache(this.entityDao.getNamedQuery(QNAME_REFRESH_POS).setParameterList(PARAM_POINT_OF_SALE_IDS, ids));
        }
        return result;
    }
    
    @Override
    public final boolean updateCustomer(final Customer customer, final UserAccount userAccount) {
        this.userAccountService.saveOrUpdateUserAccount(userAccount);
        return this.customerService.update(customer);
    }
    
    @Override
    @Transactional(rollbackFor = InvalidDataException.class)
    public final int movePointOfSale(final Integer posId, final Customer oldCustomer, final Customer newCustomer, final UserAccount userAccount,
        final Boolean isLinux)
        throws InvalidDataException {
        int newPosId = 0;
        final PointOfSale oldPOS = this.posService.findPointOfSaleById(posId);
        
        final PosStatus posStatus = oldPOS.getPosStatus();
        if (posStatus.isIsActivated()) {
            posStatus.setIsActivated(false);
            posStatus.setIsActivatedGmt(DateUtil.getCurrentGmtDate());
            this.entityDao.update(posStatus);
            this.entityDao.flush();
        }
        
        try {
            newPosId = movePosInDB(posId, oldCustomer.getId(), newCustomer.getId(), userAccount, oldPOS, isLinux);
        } catch (HibernateException hbe) {
            final String failedMovePSDBMessage =
                    FAILED_MOVE_PS_DB + " [newPosId] is less or equal to 0. Pay station SerialNumber  = " + oldPOS.getSerialNumber();
            LOG.error(failedMovePSDBMessage, hbe);
            throw new InvalidDataException(failedMovePSDBMessage, hbe);
        }
        
        if (newPosId <= 0) {
            final String failedMovePSDBMessage = FAILED_MOVE_PS_DB + " Pay station SerialNumber  = " + oldPOS.getSerialNumber();
            final InvalidDataException invalidDataException = new InvalidDataException(failedMovePSDBMessage);
            LOG.error(failedMovePSDBMessage, invalidDataException);
            throw invalidDataException;
        }
        
        final PointOfSale newPOS = this.posService.findPointOfSaleById(newPosId);
        final PosStatus newPosStatus = newPOS.getPosStatus();
        
        if (isPointOfSaleLinux(oldPOS)) {
            deleteMitreIdAndTelemetryAndLCDMS(oldPOS, oldCustomer);
        }
        
        addOrUpdateMitreIdAndTelemetryAndLCDMS(newPOS, newCustomer, isLinux, newPosStatus.isIsActivated(), WebCoreConstants.LINK_ACTION_TYPE_ADD);
        setNewBINRange(newPosId);
        
        return newPosId;
    }
    
    private void setNewBINRange(final Integer pointOfSaleId) {
        final PointOfSale pos = this.posService.findPointOfSaleById(pointOfSaleId);
        final PosServiceState pss = this.posServiceStateService.findLinuxPosServiceStateBySerialNumber(pos.getSerialNumber());
        pss.setIsNewBINRange(true);
        this.posServiceStateService.updatePosServiceState(pss);
    }
    
    //cannot be final for testing
    @SuppressWarnings({ "checkstyle:designforextension" })
    public int movePosInDB(final Integer posId, final Integer oldCustomerId, final Integer newCustomerId, final UserAccount userAccount,
        final PointOfSale pos, final Boolean isLinux) throws HibernateException {
        final SQLQuery query = this.entityDao
                .createSQLQuery(CALL_SP_MOVE_POINT_OF_SALE);
        
        addPosCacheSynchroinzation(query);
        
        query.addScalar(ATTR_POINT_OF_SALE_ID, new IntegerType());
        query.setParameter("p_CustomerId_new", newCustomerId);
        query.setParameter("p_CustomerId_old", oldCustomerId);
        query.setParameter(PARAM_SERIAL_NUMBER, pos.getSerialNumber());
        query.setParameter(PARAM_PROVISIONED_GMT, DateUtil.getCurrentGmtDate());
        query.setParameter(PARAM_USER_ID, userAccount.getId());
        query.setParameter(PARAM_IS_LINUX, isLinux);
        
        final Object result = query.uniqueResult();
        
        final int newPosId = (Integer) result;
        if (newPosId > 0) {
            final List<Integer> ids = new ArrayList<Integer>(2);
            
            ids.add(posId);
            this.entityDao.clearFromCache(this.entityDao.getNamedQuery(QNAME_REFRESH_POS).setParameterList(PARAM_POINT_OF_SALE_IDS, ids));
            
            ids.add(newPosId);
            this.entityDao.bringToCache(this.entityDao.getNamedQuery(QNAME_REFRESH_POS).setParameterList(PARAM_POINT_OF_SALE_IDS, ids));
        }
        
        return newPosId;
    }
    
    @Override
    public final FirmwareConfig getFirmwareConfig(final String serialNumber) throws ApplicationException {
        try {
            final String firmwareJson = getTelemetryClient().getFirmwareConfig(serialNumber).execute().toString(Charset.defaultCharset());
            if (StringUtils.isBlank(firmwareJson)) {
                throw new ApplicationException("FirewareConfig JSON doesn't exist for Serial Number: " + serialNumber);
            }
            return this.json.deserialize(firmwareJson, new TypeReference<FirmwareConfig>() {
            });
        } catch (JsonException je) {
            LOG.error("Cannot deserialize json using serial number: " + serialNumber, je);
            throw new ApplicationException(je);
        } catch (RuntimeException re) {
            LOG.error(re);
            if (re.getCause() instanceof HystrixBadRequestException) {
                LOG.error("HystrixBadRequestException occurred - : " + re.getCause().getMessage());
                throw new ApplicationException(re.getCause());
            }
            throw re;
        }
    }
    
    /**
     * If a pay station is activated/deactivated update user status ("Enabled"/"Disabled") in Kafka message to LINK MitreId.
     * 
     * @throws InvalidDataException
     */
    @Override
    public final boolean updateUserInMitreId(final PointOfSale pointOfSale, final Boolean isActive, final DeviceUser deviceUser)
        throws InvalidDataException {
        
        if (isPointOfSaleLinux(pointOfSale)) {
            sendUserDetailMessageMitreId(getUnifiIdAsLong(pointOfSale.getCustomer().getUnifiId()), pointOfSale.getSerialNumber(), isActive,
                                         WebCoreConstants.LINK_ACTION_TYPE_UPDATE);
        }
        
        if (LOG.isInfoEnabled()) {
            LOG.info("Kafka message has been sent.");
        }
        return true;
    }

    private boolean isPointOfSaleLinux(final PointOfSale pointOfSale) {
        return BooleanUtils.isTrue(pointOfSale.isIsLinux());
    }
    
    @Override
    public final void updateDeviceInLCDMS(final PointOfSale pointOfSale, final Customer customer, final Boolean isDeleted)
        throws InvalidDataException {
        
        if (isPointOfSaleLinux(pointOfSale)) {
            sendMessageLCDMS(pointOfSale, customer, isDeleted);
        }
    }
    
    /**
     * When actionType == ADD, it'll create the Pay Station no MitreId, Telemetry Service and LCDMD.
     * When actionType == UPDATE, it'll only update the status of the Pay Station on MitreId.
     * 
     * @throws InvalidDataException
     */
    @Override
    public final void addOrUpdateMitreIdAndTelemetryAndLCDMS(final PointOfSale pos, final Customer customer, final Boolean isLinux,
        final Boolean isActive, final String actionType) throws InvalidDataException {
        
        if (isPointOfSaleLinux(pos) || BooleanUtils.isTrue(isLinux)) {
            
            sendUserDetailMessageMitreId(getUnifiIdAsLong(customer.getUnifiId()), pos.getSerialNumber(), isActive, actionType);
            sendMessageLCDMS(pos, customer, false);
            
            if (actionType == WebCoreConstants.LINK_ACTION_TYPE_ADD) {
                try {
                    getTelemetryClient().createDevice(pos.getSerialNumber(), String.valueOf(customer.getId())).execute();
                } catch (HystrixRuntimeException hre) {
                    LOG.error(TelemetryClient.FAILED_SEND_REQUEST_TELEMETRY, hre);
                    throw new InvalidDataException(TelemetryClient.FAILED_SEND_REQUEST_TELEMETRY, hre);
                }
            }
            pos.setIsLinux(true);
        }
    }
    
    @Override
    public final void deleteMitreIdAndTelemetryAndLCDMS(final PointOfSale pointOfSale, final Customer customer) throws InvalidDataException {
        
        if (isPointOfSaleLinux(pointOfSale)) {
            try {
                getTelemetryClient().delete(pointOfSale.getSerialNumber()).execute().toString(Charset.defaultCharset());
            } catch (HystrixRuntimeException hre) {
                logErrorSendRequestTelemetryService(pointOfSale, hre);
                throw new InvalidDataException(TelemetryClient.FAILED_SEND_REQUEST_TELEMETRY, hre);
            }
            sendMessageLCDMS(pointOfSale, customer, true);
            sendUserDetailMessageMitreId(getUnifiIdAsLong(customer.getUnifiId()), pointOfSale.getSerialNumber(), false,
                                         WebCoreConstants.LINK_ACTION_TYPE_DELETE);
        }
    }
    
    @Override
    public final boolean recreateMitreIdAndTelemetryAndLCDMS(final String oldSerialNumber, final String newSerialNumber,
        final PointOfSale pointOfSale, final Customer customer, final Boolean isActive) throws InvalidDataException {
        
        if (isPointOfSaleLinux(pointOfSale)) {
            pointOfSale.setSerialNumber(oldSerialNumber);
            deleteMitreIdAndTelemetryAndLCDMS(pointOfSale, customer);
            
            pointOfSale.setSerialNumber(newSerialNumber);
            addOrUpdateMitreIdAndTelemetryAndLCDMS(pointOfSale, customer, true, isActive, WebCoreConstants.LINK_ACTION_TYPE_ADD);
            return true;
        }
        return false;
    }
    
    private void sendMessageLCDMS(final PointOfSale pos, final Customer customer, final boolean isDeleted) throws InvalidDataException {
        
        if (customer == null) {
            final InvalidDataException invalidDataException = new InvalidDataException(CUSTOMER_NOT_DEFINED);
            LOG.error("Customer is null", invalidDataException);
            throw invalidDataException;
        }
        
        checkUnifiIdAndThrowsException(getUnifiIdAsLong(customer.getUnifiId()));
        
        final String deviceSubType;
        final Paystation payStation = pos.getPaystation();
        
        if (payStation != null) {
            deviceSubType = payStation.getPaystationType().getName();
        } else {
            deviceSubType = "Unknown";
        }
        
        final DeviceUpdateMessage deviceUpdateMessage =
                new DeviceUpdateMessage(customer.getUnifiId(), pos.getSerialNumber(), pos.getName(), deviceSubType, isDeleted);
        
        try {
            this.producer.send(KafkaKeyConstants.LCDMS_KAFKA_CREATE_TOPIC_NAME, pos.getSerialNumber(), deviceUpdateMessage).get();
        } catch (JsonException | InvalidTopicTypeException | InterruptedException | ExecutionException e) {
            LOG.error(FAILED_SEND_MESSAGE_LDCMS, e);
            throw new InvalidDataException(FAILED_SEND_MESSAGE_LDCMS, e);
        }
    }
    
    private TelemetryClient getTelemetryClient() {
        return this.clientFactory.from(TelemetryClient.class);
    }
    
    public final AbstractMessageProducer getProducer() {
        return producer;
    }
    
    public final void setProducer(final AbstractMessageProducer producer) {
        this.producer = producer;
    }
    
    private UserDetailMessage createUserDetailMessage(final Long linkCustomerId, final String serialNumber, final Boolean isActivated,
        final String actionType) throws CryptoException {
        final String email = serialNumber + WebCoreConstants.LINK_EMAIL_PS_SUFFIX;
        final String encryptedPw =
                actionType.equalsIgnoreCase(WebCoreConstants.LINK_ACTION_TYPE_ADD) ? this.cryptoService.generateDevicePassword(serialNumber) : null;
        
        return new UserDetailMessage(null, encryptedPw, email, actionType, serialNumber, serialNumber, isActivated, linkCustomerId, true);
    }
    
    private void sendUserDetailMessageMitreId(final Long linkCustomerId, final String serialNumber, final Boolean isActivated,
        final String actionType) throws InvalidDataException {
        
        checkUnifiIdAndThrowsException(linkCustomerId);
        
        try {
            this.producer.sendWithByteArray(KafkaKeyConstants.LINK_KAFKA_MITREID_USERMESSAGE_TOPIC_NAME, null,
                                            createUserDetailMessage(linkCustomerId, serialNumber, isActivated, actionType))
                    .get();
            
        } catch (JsonException | InvalidTopicTypeException | CryptoException | InterruptedException | ExecutionException e) {
            logErrorSendUserDetailMessag(linkCustomerId, serialNumber, actionType, e);
            throw new InvalidDataException(FAILED_SEND_MESSAGE_MITREID, e);
        }
    }
    
    private Long getUnifiIdAsLong(final Integer unifiId) {
        return unifiId != null ? new Long(unifiId) : StandardConstants.INT_MINUS_ONE;
    }
    
    private void checkUnifiIdAndThrowsException(final Long unifiId) throws InvalidDataException {
        if (unifiId == null || unifiId < 0) {
            final InvalidDataException invalidDataException = new InvalidDataException(this.messageHelper.getMessage(ErrorConstants.NOT_UNIFI));
            LOG.error("UnifiId is null", invalidDataException);
            throw invalidDataException;
        }
    }
    
    private void logErrorSendUserDetailMessag(final Long linkCustomerId, final String serialNumber, final String actionType, Exception e) {
        final StringBuilder bdr = new StringBuilder(100);
        bdr.append("Error calling MITREid user creation, serialNumber: ").append(serialNumber).append(", linkCustomerId: ").append(linkCustomerId)
                .append(", actionType: ").append(actionType);
        LOG.error(bdr.toString(), e);
    }
    
    private void logErrorSendRequestTelemetryService(final PointOfSale pointOfSale, HystrixRuntimeException exception) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append(TelemetryClient.FAILED_SEND_REQUEST_TELEMETRY).append(", serial number is: ").append(pointOfSale.getSerialNumber());
        LOG.error(bdr.toString(), exception);
    }
    
    public final void setCryptoService(final CryptoService cryptoService) {
        this.cryptoService = cryptoService;
    }
}
