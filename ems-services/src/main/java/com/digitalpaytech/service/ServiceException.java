package com.digitalpaytech.service;

public class ServiceException extends RuntimeException {
    private static final long serialVersionUID = -770047054517581171L;

    public ServiceException() {
        
    }
    
    public ServiceException(String message) {
        super(message);
    }
    
    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public ServiceException(Throwable cause) {
        super(cause);
    }
}
