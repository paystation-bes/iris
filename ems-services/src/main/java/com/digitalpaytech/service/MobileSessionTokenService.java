package com.digitalpaytech.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.digitalpaytech.domain.MobileSessionToken;
import com.digitalpaytech.dto.MobileTokenMapEntry;

public interface MobileSessionTokenService {
    MobileSessionToken findMobileSessionTokenBySessionToken(String sessionToken);
    
    long getTokenCount(String sessionToken);
    
    List<MobileSessionToken> findMobileSessionTokenByUserAccountId(int userAccountId);
    
    void saveMobileSessionToken(MobileSessionToken mobileSessionToken);
    
    void updateMobileSessionToken(MobileSessionToken mobileSesionToken);
    
    void delete(MobileSessionToken mobileSessionToken);
    
    List<MobileSessionToken> findMobileSessionTokenByUserAccountIdAndApplicationId(int userAccountId, int applicationId);
    
    MobileSessionToken findMobileSessionTokenByUserAccountAndMobileLicense(int userAccountId, int mobileLicenseId);
    
    List<MobileSessionToken> findMobileSessionTokensByMobileLicense(Integer mobileLicenseKeyId);
    
    List<MobileSessionToken> findMobileSessionTokensByMobileLicenses(List<Integer> mobileLicenseKeyIds);
    
    void deleteMobileSessionTokensByMobileLicenses(List<Integer> mobileLicenseKeyIds);
    
    MobileSessionToken findMobileSessionTokenById(int id);
    
    MobileSessionToken findMobileSessionTokenByIdJoin(int id);
    
    List<MobileTokenMapEntry> findMobileSessionTokensMap(int customerId, int mobileApplicationId, Date expiryDate, String timeZone,
        Map<Integer, String> statusNames);
    
    List<MobileSessionToken> findProvisionedMobileSessionTokenByCustomerAndApplicationByPage(int customerId, int applicationId, int pageNo
                                                                                             , Date maxUpdatedTime);
    
    List<MobileTokenMapEntry> findRecentUsersForTokens(List<MobileSessionToken> mobileSessionTokens, String timeZone, Map<Integer, String> statusNames);
    
}
