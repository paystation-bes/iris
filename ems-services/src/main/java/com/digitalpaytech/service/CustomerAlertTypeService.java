package com.digitalpaytech.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.digitalpaytech.domain.CustomerAlertEmail;
import com.digitalpaytech.domain.CustomerAlertType;

public interface CustomerAlertTypeService {
    
    Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdOrderByNameAsc(Integer customerId, Integer filterType);
    
    Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdOrderByNameDesc(Integer customerId, Integer filterType);
    
    Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdOrderByAlertTypeAsc(Integer customerId, Integer filterType);
    
    Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdOrderByAlertTypeDesc(Integer customerId, Integer filterType);
    
    Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdAlertThresholdTypeIdAndIsActive(Integer customerId, Integer alertThresholdTypeId,
        boolean isActive);
    
    CustomerAlertType findCustomerAlertTypeById(Integer id);
    
    CustomerAlertType findCustomerAlertTypeByIdAndEvict(Integer id);
    
    CustomerAlertType findDefaultCustomerAlertTypeByCustomerIdAndAlertThresholdTypeId(Integer customerId, int alertThresholdTypeId);
    
    void saveCustomerAlertType(CustomerAlertType customerAlertType);
    
    void updateCustomerAlertType(CustomerAlertType customerAlertType, Collection<String> emailAddresses);
    
    void deleteCustomerAlertType(CustomerAlertType customerAlertType, Integer userAccountId);
    
    int countByRouteId(Integer... routeIds);
    
    void disableNonDefaultAlert(Integer customerId, Date date, Integer userId);
    
    List<CustomerAlertEmail> findCustomerAlertEmailByCustomerAlertTypeId(Integer customerAlertTypeId);
    
    List<CustomerAlertType> findCustomerAlertTypesByRouteId(Integer routeId);
    
    List<Integer> findCustomerAlertTypeIdsByRouteId(Integer routeId);
    
    List<CustomerAlertType> findCustomerAlertTypeByIdList(List<Integer> customerAlertTypeIdList);
    //    List<CustomerAlertType> findCustomerAlertTypesByRouteIdAndPointOfSaleId(Integer routeId, Integer posId);
}
