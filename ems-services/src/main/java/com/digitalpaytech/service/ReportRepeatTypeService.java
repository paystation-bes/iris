package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.ReportRepeatType;

public interface ReportRepeatTypeService {
    public List<ReportRepeatType> loadAll();
}
