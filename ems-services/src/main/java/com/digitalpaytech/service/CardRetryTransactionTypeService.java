package com.digitalpaytech.service;

import com.digitalpaytech.domain.CardRetryTransactionType;

public interface CardRetryTransactionTypeService {
    CardRetryTransactionType findCardRetryTransactionType(Integer id);
}
