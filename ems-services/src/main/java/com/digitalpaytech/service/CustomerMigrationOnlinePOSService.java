package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.CustomerMigrationOnlinePOS;

public interface CustomerMigrationOnlinePOSService {
    public void deleteCustomerMigrationOnlinePOSService(CustomerMigrationOnlinePOS customerMigrationOnlinePOS);
    
    public List<CustomerMigrationOnlinePOS> findOnlinePOSByCustomerId(Integer customerId);
    
}
