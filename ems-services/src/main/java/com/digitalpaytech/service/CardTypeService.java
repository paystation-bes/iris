package com.digitalpaytech.service;

import java.util.List;
import java.util.Map;

import com.digitalpaytech.domain.CardType;

public interface CardTypeService {
    List<CardType> loadAll();
    
    Map<Integer, CardType> getCardTypesMap();
    
    CardType findByName(String name);
}
