package com.digitalpaytech.service;

import java.util.List;
import java.util.Map;

import com.digitalpaytech.domain.CustomerMobileDevice;
import com.digitalpaytech.dto.customeradmin.MobileDeviceInfo;

public interface CustomerMobileDeviceService {
    
    List<CustomerMobileDevice> findBlockedMobileDevicesByCustomer(Integer customerId, Integer pageNumber, Integer pageSize);
    
    CustomerMobileDevice findCustomerMobileDeviceById(Integer customerMobileDeviceId);
    
    CustomerMobileDevice findBlockedMobileDevice(Integer customerId, Integer mobileDeviceId);
    
    void unBlockDevice(Integer customerId, Integer customerMobileDeviceId);
    
    void blockDevice(Integer customerId, Integer customerMobileDeviceId, int userAccountId);
    
    void save(CustomerMobileDevice customerMobileDevice);
    
    void delete(CustomerMobileDevice customerMobileDevice);
    
    void saveOrUpdate(CustomerMobileDevice customerMobileDevice);
    
    CustomerMobileDevice findCustomerMobileDeviceByCustomerAndDeviceUid(Integer customerId, String uid);
    
    void releaseDevice(Integer customerId, Integer customerMobileDeviceId, int userAccountId);
    
    void releaseDevice(Integer customerId, Integer customerMobileDeviceId, Integer applicationType, int userAccountId);
    
    List<CustomerMobileDevice> findCustomerMobileDeviceByCustomerAndApplication(Integer customerId, Integer applicationId, Integer page,
        Integer pageSize);
    
    List<CustomerMobileDevice> findCustomerMobileDeviceByCustomer(Integer customerId, Integer page, Integer pageSize);
    
    MobileDeviceInfo findDetailsForCustomerMobileDevice(Integer customerMobileDeviceId, Integer mobileApplicationTypeId,
        Map<Integer, String> statusNames);
    
}
