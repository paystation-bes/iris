package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.CaseCustomerAccount;
import com.digitalpaytech.domain.CaseCustomerFacility;
import com.digitalpaytech.domain.CaseLocationLot;
import com.digitalpaytech.domain.CaseOccupancyHour;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.dto.CaseLocationLotSearchCriteria;
import com.digitalpaytech.dto.CaseLocationLotSelectorDTO;
//import com.digitalpaytech.helper.CaseRESTLotOccupancy;
import com.digitalpaytech.util.RandomKeyMapping;

@SuppressWarnings("PMD.TooManyMethods")
public interface CaseService {
    
    CaseCustomerAccount findCaseCustomerAccountById(Integer caseCustomerAccountId);
    
    CaseCustomerAccount findCaseCustomerAccountByCaseAccountId(Integer caseAccountId);
    
    CaseCustomerAccount findCaseCustomerAccountByCustomerId(Integer customerId);
    
    List<CaseCustomerAccount> findAssignedCaseCustomerAccount();
    
    List<CaseCustomerAccount> findUnassignedCaseCustomerAccountByNameKeyword(Integer customerId, String keyword);
    
    List<CaseCustomerAccount> findCaseCustomerAccountByNameKeyword(String keyword);
    
    void saveCaseCustomerAccount(CaseCustomerAccount caseCustomerAccount);
    
    void updateCaseCustomerAccount(CaseCustomerAccount caseCustomerAccount);
    
    List<CaseLocationLot> findCaseLocationLotByCustomerId(Integer customerId);
    
    List<CaseLocationLot> findUnassignedLocationLotByCustomerId(Integer customerId);
    
    List<CaseLocationLot> findCaseLocationLotByLocationId(Integer locationId);
    
    CaseLocationLot findCaseLocationLotByLotId(Integer caseLotId);
    
    CaseLocationLot findCaseLocationLotById(Integer caseLocationLotId);
    
    boolean isCaseDataExtractionServer();
    
    CaseCustomerFacility findCaseCustomerFacilityByCaseFacilityId(Integer caseFacilityId);
    
    List<CaseCustomerFacility> findCaseCustomerFacilityByCustomerId(Integer customerId);
    
    void saveCaseCustomerFacility(CaseCustomerFacility caseCustomerFacility);
    
    void updateCaseCustomerFacility(CaseCustomerFacility caseCustomerFacility);
    
    void saveCaseLocationLot(CaseLocationLot caseLocationLot);
    
    void updateCaseLocationLot(CaseLocationLot caseLocationLot);
    
    List<CaseLocationLotSelectorDTO> findCaseLocationLotSelector(CaseLocationLotSearchCriteria criteria, RandomKeyMapping keyMapping);
    
    boolean isCaseCustomerAccountListEmpty();
    
    Integer findLatestCaseOccupancyHourTimeIdGMTByLocationId(Integer locationId);
    
    List<Location> findActiveCaseLocationLots();
    
    CaseCustomerFacility findCaseCustomerFacilityByCaseLotId(Integer caseLotId);
    
    Integer findLatestCaseOccupancyHourTimeIdLocalByLocationId(Integer locationId);
    
    CaseOccupancyHour findCaseOccupancyHourByLocationIdAndTimeIdGMT(Integer locationId, Integer timeId);
    
    void saveOrUpdateCaseOccupancyHour(CaseOccupancyHour caseOccupancyHour);
    
}
