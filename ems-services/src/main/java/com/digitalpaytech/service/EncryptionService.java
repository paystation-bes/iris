package com.digitalpaytech.service;

import com.digitalpaytech.dto.EncryptionInfo;

public interface EncryptionService {
    
    static final String PRODUCTION = "Production";
    static final String DEVELOPMENT = "Development";
    static final String CRYPTO_SERVER = "CryptoServer";
    
    static final String HMAC_SHA256_ALGORITHM = "HmacSHA256";
    static final String HMAC_SHA256_CHARSET = "UTF-8";
    
    static final String SIGNING_PROXY_SUFFIX = "-PROXY";
    
    boolean isForce2LicensePage();
    
    EncryptionInfo getEncryptionInfo();
    
    void initEncryptionMode();
    
    String createHMACSecretKey();
    
    boolean verifyHMACSignature(String hostName, String httpMethod, String passInSignature, Object queryParamList, String xmlString, String encodedKey);
}
