package com.digitalpaytech.service;

import com.digitalpaytech.util.StandardConstants;

public interface EmsPropertiesService {
    
    String DEFAULT_MAX_LOGIN_ATTEMPT = "3";
    String MAX_LOGIN_ATTEMPT = "MaximumLoginAttempt";
    String DEFAULT_MAX_LOGIN_LOCKUP_MINUTES = "5";
    String MAX_LOGIN_LOCKUP_MINUTES = "MaximumLoginLockUpMinutes";
    String LICENSE_SERVER_URL = "ServerUrl";
    String MAIL_SERVER_URL = "MailServerUrl";
    String EMS_ALERT_FROM_EMAIL_ADDRESS = "EmsAlertFromEmailAddress";
    String EMS_ADMIN_ALERT_TO_EMAIL_ADDRESS = "EmsAdminAlertToEmailAddress";
    String SERVICE_AGREEMENT_TO_EMAIL_ADDRESS = "ServiceAgreementToEmailAddress";
    String EMS_ADDRESS_STRING = "no-reply@digitalpaytech.com";
    String EMS_SERVICE_AGREEMENT_TO_ADDRESS_STRING = "emsbilling@digitalpaytech.com";
    String PROD_SUPPORT_EMAIL_ADDRESS = "ProdSupportEmailAddress";
    String PROD_SUPPORT_EMAIL_ADDRESS_STRING = "prodsupport@digitalpaytech.com";
    String MAX_SESSION_TIMEOUT_MINUTE = "MaximumSessionTimeoutMinutes";
    String MAX_WIDGET_DATA_QUERY_ROW_LIMIT = "MaximumWidgetDataRows";
    String MAX_DEFAULT_WIDGET_DATA_QUERY_ROW_LIMIT = "2000";
    int MAX_DEFAULT_SESSION_TIMEOUT = 20;
    String MAX_PAST_CUSTOMER_ADMIN_NOTIFICATIONS_LIMIT = "MaximumPastCustomerNotifications";
    int MAX_DEFAULT_PAST_CUSTOMER_ADMIN_NOTIFICATIONS_LIMIT = 5;
    String MAX_CC_RETRY_TIMES = "MaximumCcRetryTimes";
    int MAX_DEFAULT_CC_RETRY_TIMES = 10;
    String MAX_RETRY_TIMES = "MaximumRetryTimes";
    int MAX_DEFAULT_RETRY_TIMES = 30;
    String CUSTOMER_DEFAULT_TIMEZONE = "CustomerDefaultTimeZone";
    String CUSTOMER_DEFAULT_TIMEZONE_VALUE = "GMT";
    String CONCORD_EMS_APPLICATION_ID = "ConcordEmsVersion";
    String DEFAULT_CONCORD_EMS_APPLICATION_ID = "EMS v6.2.0.28";
    String EMS_APPLICATION_ID = "EmsVersion";
    String DEFAULT_EMS_APPLICATION_ID = "EMS v7.0.0";
    String LINK2GOV_CONNECTION_TIMEOUT = "Link2GovConnectionTimeout";
    String LINK2GOV_CHARSET = "Link2GovCharset";
    String LINK2GOV_CONVENIENCE_FEE = "Link2GovConvenienceFee";
    String LINK2GOV_AUTH = "Link2GovAuth";
    String LINK2GOV_SETTLE = "Link2GovSettle";
    String LINK2GOV_SALE = "Link2GovSale";
    String LINK2GOV_REFUND = "Link2GovRefund";
    String CARD_AUTHORIZATION_BACK_UP_MAX_HOLDING_DAYS = "CardAuthorizationBackUpMaxHoldingDays";
    int DEFAULT_CARD_AUTHORIZATION_BACK_UP_MAX_HOLDING_DAYS = 180;
    String SENSOR_DATA_BATCH_COUNT = "SensorDataBatchCount";
    String DEFAULT_SENSOR_BATCH_SIZE = "100";
    String SENSOR_PROCESSING_THREAD_COUNT = "SensorProcessingThreadCount";
    String DEFAULT_SENSOR_PROCESS_THREAD_COUNT = StandardConstants.STRING_ONE;
    // Disable Processing Uploaded Transaction file
    String DISABLE_PROCESSING_UPLOADED_TRANSFILE = "DisableProcUploadTranFile";
    String FILE_PARSER_THREAD_COUNT = "FileParserThreadCount";
    
    String CSV_UPLOAD_PROCTIME_PER_K = "CsvUploadProcessingMillisPerThousand";
    
    String REROUTE_TO_EMS6_URL = "RerouteToEms6Url";
    
    /* key rotation */
    String ROTATE_KEYS_IN_IRIS = "RotateKeyInIris";
    
    /* keystore file directory */
    String CRYPTO_DIRECTORY = "CryptoDirectory";
    String FILE_SEPARATOR = "file.separator";
    String DEFAULT_CRYPTO_DIRECTORY =
            System.getProperty("catalina.home") + System.getProperty(FILE_SEPARATOR) + "certs" + System.getProperty(FILE_SEPARATOR);
    
    // Development Mode
    String DEVELOPMENT_MODE = "DevelopmentMode";
    
    // HSM
    String ENCRYPTION_MODE = "EncryptionMode";
    String HSM_DIR = "HSMDir";
    String DEFAULT_HSM_DIRECTORY = System.getProperty(FILE_SEPARATOR) + "opt" + System.getProperty(FILE_SEPARATOR) + "nfast";
    
    String ENABLE_SIGNING_PROXY = "EnableSigningProxy";
    
    /* Ban time frame to change External Key */
    String MINIMAL_INTERVAL_TO_CHANGE_EXTERNAL_KEY = "MinIntervalToChangeExternalKey";
    // for SS account
    String EMS_ACCOUNT_NAME = "EmsAccountName";
    String EMS_ACCOUNT_TOKEN = "EmsAccountToken";
    String SS_LOCATION = "SigningServiceLocation";
    // For SS V2
    String SIGNING_SERVICE_LOCATION = "SigningServiceV2Location";
    String SIGNING_SERVICE_ACCOUNT_NAME = "SigningServiceV2AccountName";
    String SIGNING_SERVICE_TOKEN = "SigningServiceV2Token";
    
    // Cluster configs
    String SCHEDULED_CARD_PROCESS_SERVER = "ScheduledCardProcessingServer";
    String AUTO_CRYPTO_KEY_PROCESS_SERVER = "AutoCryptoKeyProcessingServer";
    String FLEX_DATA_EXTRACTION_PROCESS_SERVER = "FlexDataExtractionProcessingServer";
    String CASE_DATA_EXTRACTION_PROCESS_SERVER = "CaseDataExtractionProcessingServer";
    String DEFAULT_FLEX_DATA_EXTRACTION_PROCESS_SERVER = "EmsMain";
    String DELAYED_EVENTS_PROCESSING_SERVER = "DelayedEventsProcessingServer";
    String MERCHANT_ACCOUNT_MIGRATION_PROCESSING_SERVER = "MerchantAccountMigrationProcessingServer";
    
    // Display card processing queue status
    String CARD_PROCESSING_QUEUE_STATUS = "DisplayCardProcessingQueueStatus";
    
    String NUMBER_OF_BATCH_REENCRYPT_PEAKTIME = "NumberOfBatchReencryptPeakTime";
    String NUMBER_OF_BATCH_REENCRYPT = "NumberOfBatchReencryptOffPeakTime";
    String SLEEP_PEAKTIME = "SleepInPeakTimeInMilliSecond";
    String SLEEP_OFFPEAKTIME = "SleepInOffPeakTimeInMilliSecond";
    String START_PEAKTIME = "StartPeakTime";
    String END_PEAKTIME = "EndPeakTime";
    
    // Report property names
    String REPORTS_PDF_TRANSACTION_DETAILS_MAX_RECORDS = "ReportsPdfTransactionDetailsMaxRecords";
    String REPORTS_CSV_TRANSACTION_DETAILS_MAX_RECORDS = "ReportsCsvTransactionDetailsMaxRecords";
    String REPORTS_TRANSACTION_SUMMARY_MAX_RECORDS = "ReportsTransactionSummaryMaxRecords";
    String REPORTS_PDF_AUDIT_DETAILS_MAX_RECORDS = "ReportsPdfAuditDetailsMaxRecords";
    String REPORTS_CSV_AUDIT_DETAILS_MAX_RECORDS = "ReportsCsvAuditDetailsMaxRecords";
    String REPORTS_AUDIT_SUMMARY_MAX_RECORDS = "ReportsAuditSummaryMaxRecords";
    String REPORTS_MAX_RECORDS_USING_VIRTUALIZER = "ReportsMaxRecordsUsingVirtualizer";
    String REPORTS_TAXES_MAX_RECORDS = "ReportsTaxesMaxRecords";
    String REPORTS_SIZE_LIMIT_KB = "ReportsSizeLimitKb";
    String REPORTS_ZIPPED_LIMIT_MB = "ReportsZippedLimitMb";
    int DEFAULT_REPORTS_ZIPPED_LIMIT_MB = 9;
    String REPORTS_SIZE_WARNING_KB = "ReportsSizeWarningKb";
    String REPORTS_TIME_LIMIT_DAYS = "ReportsTimeLimitDays";
    String REPORTS_TIME_WARNING_DAYS = "ReportsTimeWarningDays";
    String REPORTS_SYSTEM_ADMIN_TIME_LIMIT_DAYS = "ReportsSystemAdminTimeLimitDays";
    String REPORTS_QUEUE_TIME = "ReportsQueueTime";
    String REPORTS_FILE_NAME_TIME_FORMAT = "ReportsFileNameTimeFormat";
    String REPORTS_QUEUE_IDLE_TIME_LIMIT_MINUTES = "ReportsQueueIdleTimeLimitMinutes";
    String REPORTS_BACKGROUND_SERVER = "ReportBackgroundServer";
    String REPORTS_ADHOC_CLEANUP_MINS = "ReportAdhocCleanupMinutes";
    int DEFAULT_REPORTS_ADHOC_CLEANUP_MINS = 60;
    
    // Periodic running total calculation
    String RUNNING_TOTAL_CALC_TIME_INTERVAL = "RunningTotalCalcInterval";
    String NUMBER_OF_PAYSTATIONS_BE_EXECUTED = "NumPaystationsBeExecuted";
    String PREAUTH_DELAY_TIME = "PreAuthDelayTime";
    int DEFAULT_RUNNING_TOTAL_CALC_TIME_INTERVAL = 300;
    int DEFAULT_NUMBER_OF_PAYSTATIONS_BE_EXECUTED = 25;
    int DEFAULT_PREAUTH_DELAY_TIME = 180;
    
    // Card Processing Threads
    String CARD_SETTLEMENT_THREAD_COUNT = "CardSettlementThreadCount";
    String CARD_STORE_FORWARD_THREAD_COUNT = "CardStoreForwardThreadCount";
    String CARD_REVERSAL_THREAD_COUNT = "CardReversalThreadCount";
    String CARD_STORE_FORWARD_QUEUE_LIMIT = "CardStoreForwardQueueLimit";
    int DEFAULT_CARD_STORE_FORWARD_QUEUE_LIMIT = 5000;
    
    // Web Service
    String TRANSACTION_INFO_MAX_RECORDS = "TransactionInfoMaxRecords";
    String DISABLE_COMPRESSION_CHECK = "DisableCompressionCheck";
    String TRANSACTION_DATA_MAX_RECORDS = "TransactionDataMaxRecords";
    
    int TRANSACTION_DATA_MAX_RECORDS_DEFAULT = 30000;
    
    int DEFAULT_CARD_STORE_FORWARD_THREAD_COUNT = 10;
    int DEFAULT_CARD_SETTLEMENT_THREAD_COUNT = 10;
    int DEFAULT_REVERSAL_THREAD_COUNT = 1;
    
    // Archiving
    String ARCHIVE_MAX_DAYS_FOR_POS_SENSOR_INFO_AND_POS_BATTERY_INFO = "ArchiveMaxDaysForSersorLogAndBatteryInfo";
    int DEFAULT_ARCHIVE_MAX_DAYS_FOR_POS_SENSOR_INFO_AND_POS_BATTERY_INFO = 90;
    String ARCHIVE_SERVER_NAME = "ArchiveProcessingServer";
    String DEFAULT_ARCHIVE_SERVER_NAME = "VANAPP001";
    String ARCHIVE_POS_SENSOR_INFO_BATTERY_INFO_BATCH_SIZE = "ArchiveSensorPaystationBatchSize";
    int DEFAULT_ARCHIVE_POS_SENSOR_INFO_BATTERY_INFO_BATCH_SIZE = 20;
    
    // Change
    
    /* SMS */
    String SMS_SIMULATED_ENV = "SmsSimulatedEnv";
    boolean SMS_SIMULATED_ENV_DEFAULT = false;
    String SMS_RATE_INFO_CACHE_SIZE = "SmsRateInfoCacheSize";
    int DEFAULT_SMS_RATE_INFO_CACHE_SIZE = 500;
    String SMS_CALLBACK_IP_ADDRESS = "SmsCallBackIPAddress";
    String SMS_CALLBACK_IP_ADDRESS_DEFAULT = "69.28.206.213-69.28.206.215";
    String SMS_IS_SMS_CALLBACK_IP_VALIDATE = "IsSMSCallBackIPValidate";
    boolean SMS_IS_SMS_CALLBACK_IP_VALIDATE_DEFAULT = false;
    
    String MAX_UPLOAD_FILE_SIZE_IN_BYTES = "MaxUploadFileSizeInBytes";
    int DEFAULT_MAX_UPLOAD_FILE_SIZE_IN_BYTES = 10485760;
    
    /* REST Coupon API - Coupon Enhancement */
    String COUPON_REST_MAX_SIZE = "CouponRESTMaxSize";
    String COUPON_REST_MAX_NUMBERS = "CouponRESTMaxNumbers";
    String COUPON_REST_MAX_GET_RECORDS = "CouponRESTMaxGetRecords";
    String COUPON_REST_MAX_GET_ALL = "CouponRESTMaxGetAll";
    int DEFAULT_COUPON_REST_MAX_SIZE = 500000;
    int DEFAULT_COUPON_REST_MAX_NUMBERS = 1000;
    int DEFAULT_COUPON_REST_MAX_GET_RECORDS = 70;
    int DEFAULT_COUPON_REST_MAX_GET_ALL = 10000;
    
    /* Customer Migration */
    String CUSTOMER_MIGRATION_SERVER = "CustomerMigrationServer";
    String RUN_DAILY_CUSTOMER_MIGRATION = "RunDailyCustomerMigration";
    String CUSTOMER_MIGRATION_BATCH_SIZE = "CustomerMigrationBatchSize";
    String CUSTOMER_MIGRATION_PROCESS_DELAY_IN_HOURS = "CMProcessDelayInHours";
    String DATA_VALIDATION_WAIT_TIME_IN_MINUTES = "DataValidationWaitTimeInMins";
    String CUSTOMER_MIGRATION_HEARTBEAT_DELAY_IN_MINUTES = "CMHeartbeatDelayInMins";
    String CUSTOMER_MIGRATION_CUSTOMER_VALIDATION_DELAY_IN_MINUTES = "CMBoardedCustomerValidationInMins";
    String CUSTOMER_MIGRATION_PAY_STATION_DELAY_IN_HOURS = "CMMigratedCustomerPayStationInHours";
    String DEFAULT_EMS6_URLREROUTE_ID = "DefaultEMS6URLRerouteId";
    String CUSTOMER_MIGRATION_ALERT_EMAIL = "CustomerMigrationAlertEmail";
    String CUSTOMER_MIGRATION_IT_EMAIL = "CustomerMigrationITEmail";
    String CUSTOMER_MIGRATION_WAIT_TIME = "CustomerMigrationWaitTime";
    
    boolean RUN_DAILY_CUSTOMER_MIGRATION_DEFAULT = false;
    int CUSTOMER_MIGRATION_BATCH_SIZE_DEFAULT = 10;
    int CUSTOMER_MIGRATION_PROCESS_DELAY_IN_HOURS_DEFAULT = 48;
    int DATA_VALIDATION_WAIT_TIME_IN_MINUTES_DEFAULT = 15;
    int CUSTOMER_MIGRATION_HEARTBEAT_DELAY_IN_MINUTES_DEFAULT = 60;
    int CUSTOMER_MIGRATION_CUSTOMER_VALIDATION_DELAY_IN_MINUTES_DEFAULT = 60;
    int CUSTOMER_MIGRATION_PAY_STATION_DELAY_IN_HOURS_DEFAULT = 60;
    int DEFAULT_EMS6_URLREROUTE_ID_DEFAULT = 1;
    String CUSTOMER_MIGRATION_ALERT_EMAIL_DEFAULT = "customermigration@digitalpaytech.com";
    String CUSTOMER_MIGRATION_IT_EMAIL_DEFAULT = "itsupport@digitalpaytech.com";
    long CUSTOMER_MIGRATION_WAIT_TIME_DEFAULT = 30 * 24 * 60 * 60 * 1000;
    
    /* Transaction */
    String TRANSACTION_ARCHIVE_LIMIT = "TransactionArchiveYMDH";
    String DEFAULT_TRANSACTION_ARCHIVE_LIMIT = "2.0.20.0";
    
    /* Transaction Retry Count */
    String TRANSACTION_MAX_RETRY_COUNT = "TransactionMaxRetryCount";
    int DEFAULT_TRANSACTION_MAX_RETRY_COUNT = 1;
    
    /* MAP Server URL */
    String MAPPING_URL_PROPERTY_NAME = "MappingURL";
    
    /* Digital Collect API */
    String MOBILE_SESSION_TOKEN_EXPIRY_INTERVAL = "MobileSessionTokenExpiryInterval";
    int DEFAULT_MOBILE_SESSION_TOKEN_EXPIRY_INTERVAL = 60;
    
    String MOBILE_SIGNATURE_VERSION = "MobileSignatureVersion";
    
    String DEFAULT_MOBILE_SIGNATURE_VERSION = StandardConstants.STRING_ONE;
    
    String SINGLE_TRANSACTION_CARD_DATA_STORAGE_DURATION = "SingleTrxCardDataStoreInDays";
    int DEFAULT_SINGLE_TRANSACTION_CARD_DATA_STORAGE_DURATION = 30;
    
    /* Duplicate SerialNumber Check */
    String DUPLICATE_SERIALNUMBER_COUNT = "DuplicateCheckCount";
    int DEFAULT_DUPLICATE_SERIALNUMBER_COUNT = 3;
    
    String DUPLICATE_SERIALNUMBER_EMAIL = "DuplicateCheckEmail";
    String DEFAULT_DUPLICATE_SERIALNUMBER_EMAIL = "dptcs@digitalpaytech.com";
    
    String DPT_CUSTOMER_ID = "DPTCustomerId";
    int DEFAULT_DPT_CUSTOMER_ID = 7;
    
    String HASH_ALGORITHM_TYPE_SHA_1_ID = "HashAlgorithmTypeSha1Id";
    int DEFAULT_HASH_ALGORITHM_TYPE_SHA_1_ID = 1;
    
    int DEFAULT_HASH_ALGORITHM_TYPE_SHA_256_ID = 2;
    
    String COLLECTION_USER_INTERVAL = "CollectionUserIntervalMins";
    int DEFAULT_COLLECTION_USER_INTERVAL = 10;
    
    String PLATE_REPORT_RETURN_SIZE = "PlateReportReturnSize";
    int DEFAULT_PLATE_REPORT_RETURN_SIZE = 1000;
    
    String AUTHORIZE_NET_SOLUTION_ID = "AuthorizeNetSolutionId";
    String DEFAULT_AUTHORIZE_NET_SOLUTION_ID = "A1000071";
    
    /* Flex */
    String FLEX_CITATION_WEEK_INTERPOLATION = "FlexCitationWeekInterpolation";
    int DEFAULT_FLEX_CITATION_WEEK_INTERPOLATION = 0;
    
    String FLEX_CITATION_OFFSET_MINUTE = "FlexCitationOffsetMinute";
    int DEFAULT_FLEX_CITATION_OFFSET_MINUTE = 0;
    String FLEX_CITATION_MAX_ROW_COUNT = "FlexCitationMaxRowCount";
    int DEFAULT_FLEX_CITATION_MAX_ROW_COUNT = 1000;
    String FLEX_CITATION_MAX_TRY_LIMIT = "FlexCitationMaxTryLimit";
    int DEFAULT_FLEX_CITATION_MAX_TRY_LIMIT = 20;
    
    /* Elavon viaConex */
    String ELAVON_VIACONEX_SPEC_VERSION = "ElavonVersion";
    String DEFAULT_ELAVON_VIACONEX_SPEC_VERSION = "4017";
    
    String ELAVON_VIACONEX_REGISTRATION_KEY = "ElavonRegistrationKey";
    String ELAVON_VIACONEX_APPLICATION_ID = "ElavonApplicationID";
    
    String ELAVON_VIACONEX_CLOSE_BATCH_COUNT = "ElavonCloseBatchCount";
    int DEFAULT_ELAVON_VIACONEX_CLOSE_BATCH_COUNT = 7500;
    String REAL_TIME_ELAVON_RETRY_ATTEMPTS = "RealTimeElavonRetryAttempts";
    String ELAVON_RETRY_WAITING_TIME = "ElavonRetryWaitingTime";
    String ELAVON_PREAUTH_EXPIRY_TIME_IN_HRS = "ElavonPreAuthExpiryTimeInHours";
    String ELAVON_MAX_PREAUTH_REVERSAL_LIMIT = "ElavonMaxPreAuthReversalLimit";
    int DEFAULT_ELAVON_MAX_PREAUTH_REVERSAL_LIMIT = 100;
    
    // Real-time connection and read timeout.
    String ELAVON_VIACONEX_CONNECTION_TIMEOUT_REAL_TIME = "ElavonConnTimeoutMsRealTime";
    int DEFAULT_ELAVON_VIACONEX_CONNECTION_TIMEOUT_REAL_TIME = 3000;
    String ELAVON_VIACONEX_READ_TIMEOUT_REAL_TIME = "ElavonReadTimeoutMsRealTime";
    int DEFAULT_ELAVON_VIACONEX_READ_TIMEOUT_REAL_TIME = 7000;
    
    // Store & forward, offline, single phase connection and read timeout.
    String ELAVON_VIACONEX_CONNECTION_TIMEOUT_STORE_FORWARD_SINGLE_PHASE = "ElavonConnTimeoutMsSfSinglePhase";
    int DEFAULT_ELAVON_VIACONEX_CONNECTION_TIMEOUT_STORE_FORWARD_SINGLE_PHASE = 3000;
    String ELAVON_VIACONEX_READ_TIMEOUT_STORE_FORWARD_SINGLE_PHASE = "ElavonReadTimeoutMsSfSinglePhase";
    int DEFAULT_ELAVON_VIACONEX_READ_TIMEOUT_STORE_FORWARD_SINGLE_PHASE = 7500;
    
    // Close batch connection and read timeout.
    String ELAVON_VIACONEX_CONNECTION_TIMEOUT_CLOSE_BATCH = "ElavonConnTimeoutMsCloseBatch";
    int DEFAULT_ELAVON_VIACONEX_CONNECTION_TIMEOUT_CLOSE_BATCH = 2000;
    String ELAVON_VIACONEX_READ_TIMEOUT_CLOSE_BATCH = "ElavonReadTimeoutMsCloseBatch";
    int DEFAULT_ELAVON_VIACONEX_READ_TIMEOUT_CLOSE_BATCH = 5000;
    
    // Default connection and read timeout.
    String ELAVON_VIACONEX_CONNECTION_TIMEOUT_DEFAULT = "ElavonConnTimeoutMsDefault";
    int DEFAULT_ELAVON_VIACONEX_CONNECTION_TIMEOUT_DEFAULT = 3000;
    String ELAVON_VIACONEX_READ_TIMEOUT_DEFAULT = "ElavonReadTimeoutMsDefault";
    int DEFAULT_ELAVON_VIACONEX_READ_TIMEOUT_DEFAULT = 8000;
    
    // Reversal retry max times
    String ELAVON_VIACONEX_REVERSAL_TIMEOUT_RETRY_TIMES = "ElavonReversalTimeoutMaxRetryTimes";
    int DEFAULT_ELAVON_VIACONEX_REVERSAL_TIMEOUT_RETRY_TIMES = 3;
    String ELAVON_VIACONEX_REVERSAL_RETRY_TIMES = "ElavonReversalMaxRetryTimes";
    int DEFAULT_ELAVON_VIACONEX_REVERSAL_RETRY_TIMES = 3;
    
    // Settlement (close-batch) redis expire baseline.
    String ELAVON_VIACONEX_REDIS_SETTLE_EXPIRE_BASELINE = "ElavonRedisCloseBatchLockPerTxnMs";
    int DEFAULT_ELAVON_VIACONEX_REDIS_SETTLE_EXPIRE_MS_BASELINE = 350;
    
    // Transaction redis expire
    String ELAVON_VIACONEX_REDIS_TX_EXPIRE = "ElavonRedisLockTxnMs";
    int DEFAULT_ELAVON_VIACONEX_REDIS_TX_EXPIRE_MS = 1100;
    
    String ELAVON_VIACONEX_CLOSE_TIME_INTERVAL_MINUTES = "ElavonCloseTimeIntervalMinutes";
    int DEFAULT_ELAVON_VIACONEX_CLOSE_TIME_INTERVAL_MINUTES = 15;
    
    String FLEX_CITATION_MAP_WEEK_INTERPOLATION = "FlexCitationMapWeekInterpolation";
    int DEFAULT_FLEX_CITATION_MAP_WEEK_INTERPOLATION = 0;
    
    /* TD Merchant */
    
    String TD_MERCHANT_SOCKET_TIMEOUT = "TDMerchantSocketTimeout";
    int DEFAULT_TD_MERCHANT_SOCKET_TIMEOUT = 5000;
    
    String TD_MERCHANT_CONNECTION_TIMEOUT = "TDMerchantConnectionTimeout";
    int DEFAULT_TD_MERCHANT_CONNECTION_TIMEOUT = 5000;
    
    String TD_MERCHANT_GATEWAY_VERSION = "TDMerchantGatewayVersion";
    String DEFAULT_TD_MERCHANT_GATEWAY_VERSION = "v1";
    
    String TD_MERCHANT_MAX_TRANSACTION_SEARCH_DAYS = "TDMerchantMaxTransactionSearchDays";
    int DEFAULT_TD_MERCHANT_MAX_TRANSACTION_SEARCH_DAYS = 30;
    
    /* Case */
    String CASE_AUTHORIZATION = "CaseAuthorization";
    String DEFAULT_CASE_AUTHORIZATION = "Bearer ZTBjNWQwZjUxZDMwNjZkZmNmOTdiZDA4MDczMTM0MjA=";
    String CASE_URL = "CaseURL";
    String DEFAULT_CASE_URL = "http://54.197.238.92";
    String CASE_REQUEST_TIMEOUT_TIME = "CaseRequestTimeoutTime";
    int DEFAULT_CASE_REQUEST_TIMEOUT_TIME = 300;
    
    String EXECUTE_PATCH = "ExecutePatch";
    
    /* Pay station list search results limit */
    String PAY_STATION_SEARCH_RESULTS_LIMIT_SIZE = "PayStationSearchResultsLimitSize";
    int DEFAULT_PAY_STATION_SEARCH_RESULTS_LIMIT_SIZE = 20;
    
    /* Configuration Group schedule max minutes before */
    String CONFIGURATION_GROUP_SCHEDULE_MAX_MINUTES_BEFORE = "ConfigGroupScheduleMinutesBefore";
    int DEFAULT_CONFIGURATION_GROUP_SCHEDULE_MAX_MINUTES_BEFORE = 5;
    
    /* EMV Refund retry */
    String EMV_REFUND_RETRY_MAX_COUNT = "EMVRefundRetryMaxCount";
    int DEFAULT_EMV_REFUND_RETRY_MAX_COUNT = 10;
    String EMV_REFUND_RETRY_SELECT_LIMIT = "EMVRefundRetrySelectLimit";
    int DEFAULT_EMV_REFUND_RETRY_SELECT_LIMIT = 200;
    
    /* CPSReversal retry */
    String CPS_REVERSAL_RETRY_SELECT_LIMIT = "CPSReversalRetrySelectLimit";
    int DEFAULT_CPS_REVERSAL_RETRY_SELECT_LIMIT = 50;
    
    /* CPSReversal retry max count */
    String CPS_REVERSAL_RETRY_MAX_COUNT = "CPSReversalRetryMaxCount";
    int DEFAULT_CPS_REVERSAL_RETRY_MAX_COUNT = 50;
    
    /* MerchantAccount Migration wait time */
    String MERCHANT_ACCOUNT_MIGRATION_WAIT_TIME_IN_MINUTES = "MerchantAccountMigrationWaitTimeInMins";
    int MERCHANT_ACCOUNT_MIGRATION_WAIT_TIME_IN_MINUTES_DEFAULT = 5;

    /* Digital API */
    String DIGITAL_API_READ_MAX_COUNT = "DigitalAPIReadMaxCount";
    int DIGITAL_API_READ_MAX_COUNT_DEFAULT = 3;
    
    String REDIRECTABLE_SOURCE_HOSTS = "RedirectableSourceHosts";
    String DEFAULT_REDIRECTABLE_SOURCE_HOSTS = "iris.digitalpaytech.com, ps.digitalpaytech.com";
    
    String REDIRECTABLE_DEST_HOSTS = "RedirectableDestHosts";
    String DEFAULT_REDIRECTABLE_DEST_HOSTS = "t2iris.digitalpaytech.com";

    String USE_LINK_CRYPTO = "UseLinkCrypto";
    boolean DEFAULT_USE_LINK_CRYPTO = false;
    
    String getPropertyValue(String propertyName);
    
    String getPropertyValue(String propertyName, String defaultValue);
    
    String getPropertyValue(String propertyName, String defaultValue, boolean forceGet);
    
    String getPropertyValue(String propertyName, boolean forceGet);
    
    int getPropertyValueAsInt(String propertyName, int defaultValue);
    
    int getPropertyValueAsInt(String propertyName, int defaultValue, boolean forceGet);
    
    long getPropertyValueAsLong(String propertyName, long defaultValue);
    
    long getPropertyValueAsLong(String propertyName, long defaultValue, boolean forceGet);
    
    int getPropertyValueAsIntQuiet(String propertyName, int defaultValue);
    
    void findPropertiesFile();
    
    boolean isWindowsOS();
    
    void updatePrimaryServer(String newPrimaryServer);
    
    void updatePatchExecution(boolean execute);
    
    void loadPropertiesFile();
    
    boolean getPropertyValueAsBoolean(String propertyName, boolean defaultValue);
    
    boolean getPropertyValueAsBoolean(String propertyName, boolean defaultValue, boolean forceGet);
    
    <V> V getProperty(String key, Class<V> valueClass, V defaulValue);
    
    String getIrisVersion();
    
    String getIrisBuildNumber();
    
    String getIrisBuildDate();
    
    String getStaticContentPath();
    
    String getStaticContentPatam();
    
    String getIrisDeploymentMode();
}
