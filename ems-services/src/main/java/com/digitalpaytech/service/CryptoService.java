package com.digitalpaytech.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.digitalpaytech.client.dto.KeyPackage;
import com.digitalpaytech.client.dto.RSAKeyInfo;
import com.digitalpaytech.domain.CryptoKey;
import com.digitalpaytech.dto.HashAlgorithm;
import com.digitalpaytech.dto.crypto.HashAlgorithmData;
import com.digitalpaytech.dto.systemadmin.CryptoKeyListInfo;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.util.KeyValuePair;

public interface CryptoService {
    int PURPOSE_CREDIT_CARD_LOCAL = 0;
    int PURPOSE_CREDIT_CARD_REMOTE = 1;

    // Separate Modulus and Exponent
    String PUBLIC_KEY_DILIMITER = ":";
    
    // in days
    int DEFAULT_MINIMAL_INTERVAL_TO_CHANGE_EXTERNAL_KEY = 30;
    
    // in days
    int DEFAULT_DELAY_INTERVAL_TO_REMOVE_INTERNAL_KEY = 10;
    
    String THREE_ZERO = "000";
    
    /**
     * Encrypts the data using the current internal key.
     *
     * @param data
     *            - The String to encrypt.
     * @return The encrypted data returned as a Base64 encoded String.
     * @throws CryptoException
     *             - if the data cannot be encrypted.
     */
    String encryptData(String data) throws CryptoException;
    
    @Deprecated
    /**
     * Please use #encryptData(String) instead. The purpose can only be one value anyway!
     * 
     */
    String encryptData(int purpose, String data) throws CryptoException;
    
    /**
     * Decrypts the data that was previously encrypted using approved EMS encryption techniques. As the key
     * information is appended to the begin of the encrypted string, EMS can determine which encryption
     * transformation was used and which key was used for the encryption.
     * 
     * @param data
     *            - The Base64 encoded String to decrypt.
     * @return The decrypted String.
     * @throws CryptoException
     *             - if the data cannot be decrypted.
     */
    String decryptData(String data) throws CryptoException;
    
    /**
     * This method will detect whether ps still uses old key and send email to EMS admin,
     * while decrypts CC data.
     * 
     * <p>
     * Specific for external key
     * </p>
     * 
     * @param data
     * @param commAddress
     * @param purchasedDate
     * @return
     * @throws CryptoException
     */
    String decryptData(String data, int pointOfSaleId, Date purchasedDate) throws CryptoException;
    
    boolean isNewKeyForData(String data) throws CryptoException;
    
    /**
     * Check and process External key for using 6 months
     * 
     */
    void checkAndProcessExternalCryptoKey(Map<Integer, HashAlgorithmData> hashAlgorithmDataMap);
    
    void generateExternalCryptoKey(String comment, Map<Integer, HashAlgorithmData> hashAlgorithmDataMap) throws CryptoException;
    
    /**
     * Check and remove deprecated key for using 12 months
     * 
     */
    void checkAndRemoveDeprecatedExternalCryptoKey();
    
    /**
     * Check and process Internal Key for using 12 months Return true - a new internal key is created
     */
    boolean checkAndProcessInternalCryptoKey(Map<Integer, HashAlgorithmData> hashAlgorithmDataMap);
    
    void generateInternalCryptoKey(String comment, Map<Integer, HashAlgorithmData> hashAlgorithmDataMap) throws CryptoException;
    
    /**
     * Remove Internal key from keystore if the date is exceed expiry + 7 days
     * 
     */
    void checkAndRemoveInternalCryptoKey();
    
    /**
     * Check if the key store is up to date. Will send an alert to EMS admin if the next internal or external
     * key is not in the key store.
     * 
     * @return true if both next internal key and next external key are in key store. false if either next
     *         internal key or next external key in not in key store.
     */
    boolean isKeyStoreUpToDate();
    
    String createKeyInfo(int cryptoTypeId, int cryptoIndex);
    
    int countSpareKey(CryptoKey key) throws CryptoException;
    
    int getNearestKeyIndex(CryptoKey maxKey) throws CryptoException;
    
    void checkAndUpdateSigningKeyStore(List<String> activeSignAlgorithms);
    
    CryptoKeyListInfo listActiveKeys(int purpose) throws CryptoException;
    
    KeyValuePair<Integer, Integer> splitKeyInfo(String keyName);
    
    RSAKeyInfo retrieveKeyInfo(String keyAlias) throws CryptoException;
    
    KeyPackage retrieveKey(String keyAlias) throws CryptoException;
    
    KeyPackage retrieveKey(String keyAlias, HashAlgorithm hashAlg) throws CryptoException;
    
    String currentExternalKey() throws CryptoException;
        
    String currentExternalKey(boolean isCryptoLink, String serialNumber) throws CryptoException;
    
    String currentInternalKey() throws CryptoException;
    
    String generateDevicePassword(String serialNumber);
    
    boolean isLinkCryptoKey(String encryptedData);
}
