package com.digitalpaytech.service;

import java.util.Date;
import java.util.List;

import com.digitalpaytech.domain.CitationMap;

public interface CitationMapService {
    
    List<CitationMap> findByDateRange(Integer customerId, Date startDate, Date endDate);
    
    List<CitationMap> findByDateRangeAndCitationType(Integer customerId, Date startDate, Date endDate, List<Integer> citationTypeIdList);
    
}
