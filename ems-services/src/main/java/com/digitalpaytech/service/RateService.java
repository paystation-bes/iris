package com.digitalpaytech.service;

import java.util.Date;
import java.util.List;

import com.digitalpaytech.domain.Rate;

public interface RateService {
    
    Rate findRateById(Integer rateId);
    
    Rate findRateByCustomerIdAndName(Integer customerId, Byte rateTypeId, String name);
    
    List<Rate> findRatesByCustomerId(Integer customerId);
    
    List<Rate> findRatesByCustomerIdAndRateTypeId(Integer customerId, Byte rateTypeId);
    
    List<Rate> findPagedRateList(Integer customerId, Byte rateTypeId, Date maxUpdateTime, int pageNumber);
    
    void saveOrUpdateRate(Rate rate);
    
    void deleteRate(Rate rate, Integer userAccountId);
    
    List<Rate> searchRates(Integer customerId, String keyword);
    
    int findRatePage(Integer rateId, Integer customerId, Byte rateTypeId, Date maxUpdateTime);
    
}
