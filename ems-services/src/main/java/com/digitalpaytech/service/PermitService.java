package com.digitalpaytech.service;

import java.util.Date;
import java.util.List;

import com.digitalpaytech.domain.ActivePermit;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.PointOfSale;

public interface PermitService {
    
    Permit findLatestExpiredPermitByPurchaseId(Long purchaseId);
    
    /*
     * Return time is left as ActivePermit because these methods are specified date versions of similar methods in ActivePermitService.
     * Under StallInfoService there are some methods that use either these or ActivePermit version depending on user input.
     */
    
    List<ActivePermit> getStallStatusValidForPurchaseExpiryStallBySpecifiedPosDate(List<PointOfSale> posList, int startSpace, int endSpace,
        Date specifiedPosDate, int stallType);
    
    List<ActivePermit> getStallStatusByLocationBySpecifiedPosDate(int locationId, int startSpace, int endSpace, Date specifiedPosDate);
    
    List<ActivePermit> getActivePermitByCustomerAndSpaceRange(int customerId, int startSpace, int endSpace, Date specifiedPosDate);
    
    List<ActivePermit> getActivePermitByLocationAndSpaceRange(int locationId, int startSpace, int endSpace, Date specifiedPosDate);
    
    List<ActivePermit> getActivePermitByPaystationSettingAndSpaceRange(String paystationSettingName, int startSpace, int endSpace,
        Date specifiedPosDate);
    
    List<ActivePermit> getActivePermitByPosListAndSpaceRange(List<PointOfSale> posList, int startSpace, int endSpace, Date specifiedPosDate);
    
    Permit findPermitByIdForSms(Long id);
    
    Permit findPermitByPurchaseId(Long purchaseId);
    
    List<Permit> findPermitByPermitNumber(int permitNumber);
    
    void saveExtendedPermit(Permit originalPermit, Permit permit);
    
    Permit findLatestOriginalPermitByPointOfSaleIdAndPermitNumber(int pointOfSaleId, int permitNumber);
    
    Permit findLatestOriginalPermit(int locationId, int spaceNumber, int addTimeNumber, String licencePlateNumber);
    
    List<Permit> findExtentionsByOriginalPermitId(Long originalPermitId);
    
}
