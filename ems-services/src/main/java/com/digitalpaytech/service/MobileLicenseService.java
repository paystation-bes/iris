package com.digitalpaytech.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.digitalpaytech.domain.MobileApplicationType;
import com.digitalpaytech.domain.MobileLicense;
import com.digitalpaytech.domain.MobileLicenseStatusType;
import com.digitalpaytech.dto.MobileLicenseInfo;
import com.digitalpaytech.dto.MobileTokenMapEntry;
import com.digitalpaytech.dto.customeradmin.MobileDeviceInfo;


public interface MobileLicenseService {
    
    public MobileLicense findMobileLicense(int mobileLicenseId);
    
    public List<MobileLicense> findMobileLicenseByCustomer(Integer customerId);
    
    public void saveOrUpdateMobileLicense(MobileLicense mobileLicense);

    public void deleteMobileLicense(MobileLicense mobileLicense);

    public MobileLicense findMobileLicenseByCustomerAndApplicationAndDevice(
            int customerId, int applicationId, int deviceId);
    
    public MobileLicense findAvailableMobileLicenseByCustomerAndApplication(int customerId, int applicationId);

    public MobileLicenseStatusType findMobileLicenseStatusTypeById(int id);

    public MobileLicenseStatusType findMobileLicenseStatusTypeByName(String name);

    public MobileLicense findProvisionedMobileLicenseByCustomerAndApplicationAndDevice(int customerId, int applicationId, int deviceId);

    public List<MobileLicense> findProvisionedMobileLicenseByCustomerAndApplication(int customerId, int applicationId);

    public List<MobileLicense> findProvisionedMobileLicensesByCustomerAndDevice(int customerId, int deviceId);

    public List<MobileLicense> findProvisionedMobileLicensesAndActiveTokensByCustomerIdAndDeviceId(int customerId, int deviceId);

    public List<MobileApplicationType> getMobileAppsForCustomer(Integer customerId);

    public List<MobileLicense> findMobileLicenseByCustomerAndApplication(int customerId, int applicationId, Integer pageNumber, Integer pageSize);

    public List<MobileLicenseInfo> findMobileLicenseInfoForCustomerAndApplication(Integer customerId, Integer applicationId, String timeZone);

    public void updateLicenseCount(Integer customerId, Integer applicationId, int count);

    public long countProvisionedLicensesForCustomerAndType(Integer customerId, Integer applicationId);

    public long countTotalLicensesForCustomerAndType(Integer customerId, Integer applicationId);

    public List<MobileDeviceInfo> findMobileDevicesByCustomerAndApplication(Integer customerId, Integer applicationId, Integer pageNumber, Integer pageSize);

}
