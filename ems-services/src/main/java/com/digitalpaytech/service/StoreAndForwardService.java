package com.digitalpaytech.service;

import java.util.Date;

import com.digitalpaytech.domain.CPSStoreAndForwardAttempt;

public interface StoreAndForwardService {
    
    CPSStoreAndForwardAttempt findByPointOfSaleIdAndPurchasedDateAndTicketNumber(int pointOfSaleId, Date purchasedDate, int ticketNumber);
    
    CPSStoreAndForwardAttempt findByChargeToken(String chargeToken);
    
    void saveCPSStoreAndForwardAttempt(CPSStoreAndForwardAttempt cPSStoreAndForwardAttempt);
    
    void deleteCPSStoreAndForwardAttempt(CPSStoreAndForwardAttempt cPSStoreAndForwardAttempt);
}
