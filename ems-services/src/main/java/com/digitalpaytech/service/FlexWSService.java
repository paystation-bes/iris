package com.digitalpaytech.service;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import com.digitalpaytech.data.CitationWidgetBase;
import com.digitalpaytech.domain.CitationType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.FlexDataWSCallStatus;
import com.digitalpaytech.domain.FlexETLDataType;
import com.digitalpaytech.domain.FlexLocationFacility;
import com.digitalpaytech.domain.FlexLocationProperty;
import com.digitalpaytech.domain.FlexWSUserAccount;
import com.digitalpaytech.dto.FlexLocationFacilitySearchCriteria;
import com.digitalpaytech.dto.FlexLocationFacilitySelectorDTO;
import com.digitalpaytech.dto.FlexLocationPropertySearchCriteria;
import com.digitalpaytech.dto.FlexLocationPropertySelectorDTO;
import com.digitalpaytech.dto.dataset.ContraventionTypeQueryDataSet.ViolationFlexRecord;
import com.digitalpaytech.dto.dataset.FacilityQueryDataSet.FacilityFlexRecord;
import com.digitalpaytech.dto.dataset.PropertyQueryDataSet.PropertyFlexRecord;

@SuppressWarnings({ "PMD.TooManyMethods" })
public interface FlexWSService {
    
    FlexWSUserAccount findById(Integer id);
    
    FlexWSUserAccount findFlexWSUserAccountByCustomerId(Integer customerId);
    
    FlexDataWSCallStatus findLastCallByCustomerIdAndTypeId(Integer customerId, Byte flexETLDataTypeId);
    
    void saveFlexDataWSCallStatus(FlexDataWSCallStatus flexDataWSCallStatus);

    void updateFlexDataWSCallStatus(FlexDataWSCallStatus flexDataWSCallStatus);

    FlexDataWSCallStatus findFlexDataWSCallStatusById(Integer callStatusId);

    FlexETLDataType getFlexETLDataType(byte flexETLDataTypeId);
    
    List<FlexLocationFacility> findAllFlexLocationFacilitiesByCustomer(int customerId);
    
    void saveFlexLocationFacility(FlexLocationFacility flexLocationFacility);
    
    void updateFlexLocationFacility(FlexLocationFacility flexLocationFacility);
    
    List<FlexLocationProperty> findAllFlexLocationPropertiesByCustomer(int customerId);
    
    void saveFlexLocationProperty(FlexLocationProperty flexLocationProperty);
    
    void updateFlexLocationProperty(FlexLocationProperty flexLocationProperty);
    
    List<CitationType> findCitationTypeByCustomerId(Integer customerId);
    
    CitationType findCitationTypeByCustomerIdAndFlexCitationTypeId(Integer customerId, Integer flexCitationTypeId);
    
    void saveCitationType(CitationType flexLocationProperty);
    
    void updateCitationType(CitationType flexLocationProperty);
    
    List<FlexLocationFacilitySelectorDTO> findFlexLocationFacilitySelector(FlexLocationFacilitySearchCriteria criteria);
    
    List<FlexLocationFacility> findFlexLocationFacilityByLocationId(Integer locationIds);
    
    List<FlexLocationFacility> findUnselectedFlexLocationFacilityByCustomerId(Integer customerId, List<Integer> locationIds);
    
    List<FlexLocationProperty> findUnselectedFlexLocationPropertyByCustomerId(Integer customerId, List<Integer> locationIds);
    
    FlexLocationFacility findFlexLocationFacilityById(Integer id);
    
    List<FlexLocationProperty> findFlexLocationPropertyByLocationId(Integer locationId);
    
    FlexLocationProperty findFlexLocationPropertyById(Integer id);
    
    List<FlexLocationPropertySelectorDTO> findFlexLocationPropertySelector(FlexLocationPropertySearchCriteria criteria);
    
    void saveFlexAccount(FlexWSUserAccount user);
    
    void saveWidgetData(Collection<? extends Serializable> widgetData, Integer lastId, Integer callStatus);

    void blockFlexAccount(Integer flexAccountId, FlexWSUserAccount.Blocker blocker);
    
    void unblockFlexAccount(Integer flexAccountId, FlexWSUserAccount.Blocker blocker);
    
    void summarizeCitationDayData(Integer customerId, int dayOfMonth, int monthOfYear, int year);
    
    void summarizeCitationMonthData(Integer customerId, int monthOfYear, int year);
    
    boolean isFlexDataExtractionServer();
    
    void syncFacilities(List<FacilityFlexRecord> modifiedFacilities, Customer customer, Integer callStatusId);
    
    void syncProperties(List<PropertyFlexRecord> modifiedProperties, Customer customer, Integer callStatusId);
    
    void syncCitationTypes(List<ViolationFlexRecord> modifiedCitationTypes, Customer customer, Integer callStatusId);
    
    CitationWidgetBase findCitationWidgetData(String className, Integer customerId, Integer flexCitationTypeId, Integer timeId);
}
