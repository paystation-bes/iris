package com.digitalpaytech.service;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.digitalpaytech.domain.ReportContent;
import com.digitalpaytech.domain.ReportRepository;
import com.digitalpaytech.dto.ReportSearchCriteria;
import com.digitalpaytech.dto.customeradmin.ReportRepositoryInfo;
import com.digitalpaytech.util.RandomKeyMapping;

public interface ReportRepositoryService {

    public void saveOrUpdateReportRepository(ReportRepository reportRepository);

    public void deleteReportRepository(int reportRepositoryId);

    public ReportRepository getReportRepository(int id);

    public void deleteOldReportsByDay(Date limitDay);

    public void deleteOldSystemAdminReports(Date limitDay);
    
    public void addWarningToOldReportsByDay(Date limitDay);

    public List<Object[]> getUserAccountFileSizeTotals();

    public List<Object[]> getUserAccountUnWarnedFileSizeTotals();

    public void deleteOldestReports(int userAccountId, int maxSize, int totalSize);

    public void addWarningToOldestReports(int userAccountId, int maxSize, int totalSize);

    public ReportRepository findByReportHistoryId(int reportHistoryId);

    public List<ReportRepository> findByUserAccountId(int userAccountId);

    public List<ReportRepository> findByCustomerId(int customerId);

    public int countUnreadReportsByUserAccountId(int userAccountId);

    public ReportContent findReportContentByReportRepositoryId(int reportRepositoryId);
    
    public int deleteOlderAdhocReport(Date targetTime);

    public List<ReportRepository> findByUserAccountIdForSystemAdmin(int userAccountId, int customerId);
    
    public List<ReportRepositoryInfo.RepositoryInfo> findCompletedReports(ReportSearchCriteria criteria, RandomKeyMapping keyMapping, TimeZone timeZone);

}
