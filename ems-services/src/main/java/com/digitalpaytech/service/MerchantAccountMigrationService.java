package com.digitalpaytech.service;

import java.util.Collection;
import java.util.List;

import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MerchantAccountMigration;

public interface MerchantAccountMigrationService {
    
    void save(MerchantAccountMigration merchantAccountMigration);
    
    void delete(MerchantAccountMigration merchantAccountMigration);
    
    MerchantAccountMigration findById(Integer id);
    
    Collection<MerchantAccountMigration> findByDelayUntilGMTPastAndPending();
    
    void disableMerchantAccountAndStartsMigration(MerchantAccountMigration merchantAccountMigration);
    
    MerchantAccountMigration findByMerchantAccountId(Integer merchantAccountId);
    
    boolean preAuthBlockMigration(MerchantAccount ma);
    
    boolean reversalsBlockMigraion(MerchantAccount ma);
    
    boolean postAuthBlockMigration(MerchantAccount ma);

    List<MerchantAccountMigration> findByCustomerId(Integer customerId);    
}
