package com.digitalpaytech.dto;

public class SearchCustomerResultSetting {
    private String randomId;
    private String name;
    private String parentName;
    private Boolean isParent;
    private String customerName;
    private String label;
    
    public SearchCustomerResultSetting() {
        super();
    }
    
    public SearchCustomerResultSetting(final String randomId, final String name) {
        this.randomId = randomId;
        this.name = name;
    }
    
    public SearchCustomerResultSetting(final String randomId, final String name, final String label) {
        this.randomId = randomId;
        this.name = name;
        this.label = label;
    }
    
    public SearchCustomerResultSetting(final String randomId, final String name, final String parentName, final Boolean isParent) {
        this.randomId = randomId;
        this.name = name;
        this.parentName = parentName;
        this.isParent = isParent;
    }
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final String getName() {
        return this.name;
    }
    
    public final void setName(final String name) {
        this.name = name;
    }
    
    public final String getLabel() {
        return this.label;
    }
    
    public final void setLabel(final String label) {
        this.label = label;
    }
    
    public final String getParentName() {
        return this.parentName;
    }
    
    public final void setParentName(final String parentName) {
        this.parentName = parentName;
    }
    
    public final String getCustomerName() {
        return this.customerName;
    }
    
    public final void setCustomerName(final String customerName) {
        this.customerName = customerName;
    }
    
    public final Boolean getIsParent() {
        return this.isParent;
    }
    
    public final void setIsParent(final Boolean isParent) {
        this.isParent = isParent;
    }
}
