package com.digitalpaytech.dto;

public class CardProcessorInfo {
    private String name;
    private String cardRetryCount;
    private String randomProcessorId;
    private String isPaused;
    private boolean isLink;
    
    public CardProcessorInfo() {
    }
    
    public CardProcessorInfo(final String name, final String cardRetryCount, final String randomProcessorId, final String isPaused,
            final boolean isLink) {
        this.name = name;
        this.cardRetryCount = cardRetryCount;
        this.randomProcessorId = randomProcessorId;
        this.isPaused = isPaused;
        this.isLink = isLink;
    }
    
    public final String getName() {
        return this.name;
    }
    
    public final void setName(final String name) {
        this.name = name;
    }
    
    public final String getCardRetryCount() {
        return this.cardRetryCount;
    }
    
    public final void setCardRetryCount(final String cardRetryCount) {
        this.cardRetryCount = cardRetryCount;
    }
    
    public final String getRandomProcessorId() {
        return this.randomProcessorId;
    }
    
    public final void setRandomProcessorId(final String randomProcessorId) {
        this.randomProcessorId = randomProcessorId;
    }
    
    public final String getIsPaused() {
        return this.isPaused;
    }
    
    public final void setIsPaused(final String isPaused) {
        this.isPaused = isPaused;
    }
    
    public boolean getIsLink() {
        return this.isLink;
    }
    
    public void setIsLink(final boolean isLink) {
        this.isLink = isLink;
    }
    
}
