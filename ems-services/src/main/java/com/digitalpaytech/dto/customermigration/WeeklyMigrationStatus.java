package com.digitalpaytech.dto.customermigration;

import java.io.Serializable;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class WeeklyMigrationStatus implements Serializable {
    
    private static final long serialVersionUID = 4463426989185917009L;
    private int boardedCount;
    private int requestedCount;
    private int scheduledCount;
    private int runningCount;
    private int failedCount;
    private int migratedCount;
    @XStreamImplicit(itemFieldName = "weeklyMigrationList")
    private List<WeeklyMigrationInfo> weeklyMigrationList;
    
    public final int getBoardedCount() {
        return this.boardedCount;
    }
    
    public final void setBoardedCount(final int boardedCount) {
        this.boardedCount = boardedCount;
    }
    
    public final int getRequestedCount() {
        return this.requestedCount;
    }
    
    public final void setRequestedCount(final int requestedCount) {
        this.requestedCount = requestedCount;
    }
    
    public final int getScheduledCount() {
        return this.scheduledCount;
    }
    
    public final void setScheduledCount(final int scheduledCount) {
        this.scheduledCount = scheduledCount;
    }
    
    public final int getRunningCount() {
        return this.runningCount;
    }
    
    public final void setRunningCount(final int runningCount) {
        this.runningCount = runningCount;
    }
    
    public final int getFailedCount() {
        return this.failedCount;
    }
    
    public final void setFailedCount(final int failedCount) {
        this.failedCount = failedCount;
    }
    
    public final int getMigratedCount() {
        return this.migratedCount;
    }
    
    public final void setMigratedCount(final int migratedCount) {
        this.migratedCount = migratedCount;
    }
    
    public final List<WeeklyMigrationInfo> getWeeklyMigrationList() {
        return this.weeklyMigrationList;
    }
    
    public final void setWeeklyMigrationList(final List<WeeklyMigrationInfo> weeklyMigrationList) {
        this.weeklyMigrationList = weeklyMigrationList;
    }
    
}
