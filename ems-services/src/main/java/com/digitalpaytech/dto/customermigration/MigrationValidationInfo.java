package com.digitalpaytech.dto.customermigration;

import java.io.Serializable;

public class MigrationValidationInfo implements Serializable {
    
    private static final long serialVersionUID = -8043024453479742867L;
    private String name;
    private String warning;
    private String url;
    private boolean isBlocking;
    private boolean isPassed;
    
    public final String getName() {
        return this.name;
    }
    
    public final void setName(final String name) {
        this.name = name;
    }
    
    public final String getWarning() {
        return this.warning;
    }
    
    public final void setWarning(final String warning) {
        this.warning = warning;
    }
    
    public final String getUrl() {
        return this.url;
    }
    
    public final void setUrl(final String url) {
        this.url = url;
    }
    
    public final boolean getIsBlocking() {
        return this.isBlocking;
    }
    
    public final void setIsBlocking(final boolean isBlocking) {
        this.isBlocking = isBlocking;
    }
    
    public final boolean getIsPassed() {
        return this.isPassed;
    }
    
    public final void setIsPassed(final boolean isPassed) {
        this.isPassed = isPassed;
    }
}
