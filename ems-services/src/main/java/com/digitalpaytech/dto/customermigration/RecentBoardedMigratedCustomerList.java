package com.digitalpaytech.dto.customermigration;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias(value = "recentBoardedMigratedCustomerList")
public class RecentBoardedMigratedCustomerList implements Serializable {
    
    private static final long serialVersionUID = 6884670206698907628L;
    private String randomId;
    private String customerName;
    private String customerDate;
    private Integer validationStatus;
    private Integer paystationsCount;
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final String getCustomerName() {
        return this.customerName;
    }
    
    public final void setCustomerName(final String customerName) {
        this.customerName = customerName;
    }
    
    public final String getCustomerDate() {
        return this.customerDate;
    }
    
    public final void setCustomerDate(final String customerDate) {
        this.customerDate = customerDate;
    }
    
    public final Integer getValidationStatus() {
        return this.validationStatus;
    }
    
    public final void setValidationStatus(final Integer validationStatus) {
        this.validationStatus = validationStatus;
    }
    
    public final Integer getPaystationsCount() {
        return this.paystationsCount;
    }
    
    public final void setPaystationsCount(final Integer paystationsCount) {
        this.paystationsCount = paystationsCount;
    }
}
