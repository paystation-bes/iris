package com.digitalpaytech.dto.customermigration;

import java.io.Serializable;
import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias(value = "customerMigrationData")
public class CustomerMigrationData implements Serializable {
    
    private static final long serialVersionUID = 5119934703165209691L;
    private String date;
    private int boardedCount;
    private int migratedCount;
    private int remainingCount;
    private int scheduledCount;
    
    public final String getDate() {
        return this.date;
    }
    
    public final void setDate(final String date) {
        this.date = date;
    }
    
    public final int getBoardedCount() {
        return this.boardedCount;
    }
    
    public final void setBoardedCount(final int boardedCount) {
        this.boardedCount = boardedCount;
    }
    
    public final int getMigratedcount() {
        return this.migratedCount;
    }
    
    public final void setMigratedCount(final int migratedCount) {
        this.migratedCount = migratedCount;
    }
    
    public final int getRemainingCount() {
        return this.remainingCount;
    }
    
    public final void setRemainingCount(final int remainingCount) {
        this.remainingCount = remainingCount;
    }
    
    public final int getScheduledCount() {
        return this.scheduledCount;
    }
    
    public final void setScheduledCount(final int scheduledCount) {
        this.scheduledCount = scheduledCount;
    }
    
}
