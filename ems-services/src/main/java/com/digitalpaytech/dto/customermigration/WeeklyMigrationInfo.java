package com.digitalpaytech.dto.customermigration;

import java.io.Serializable;

public class WeeklyMigrationInfo implements Serializable {
    
    private static final long serialVersionUID = 8631306127551841912L;
    private String randomId;
    private String customerName;
    private int dayOfWeek;
    private String status;
    private transient int customerId;
    private byte validationStatus;
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final String getCustomerName() {
        return this.customerName;
    }
    
    public final void setCustomerName(final String customerName) {
        this.customerName = customerName;
    }
    
    public final int getDayOfWeek() {
        return this.dayOfWeek;
    }
    
    public final void setDayOfWeek(final int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }
    
    public final String getStatus() {
        return this.status;
    }
    
    public final void setStatus(final String status) {
        this.status = status;
    }
    
    public final int getCustomerId() {
        return this.customerId;
    }
    
    public final void setCustomerId(final int customerId) {
        this.customerId = customerId;
    }
    
    public final byte getValidationStatus() {
        return this.validationStatus;
    }
    
    public final void setValidationStatus(final byte validationStatus) {
        this.validationStatus = validationStatus;
    }
    
}
