package com.digitalpaytech.dto.customermigration;

import java.io.Serializable;

public class ScheduleMigrationInfo implements Serializable {
    
    private static final long serialVersionUID = -1557128124950327554L;
    private String randomId;
    private String customerName;
    private String status;
    private String boardedDate;
    private Long boardedTime;
    private String migrateDate;
    private Long migrateTime;
    private boolean isContacted;
    private int usage;
    private byte validationStatus;
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final String getCustomerName() {
        return this.customerName;
    }
    
    public final void setCustomerName(final String customerName) {
        this.customerName = customerName;
    }
    
    public final String getStatus() {
        return this.status;
    }
    
    public final void setStatus(final String status) {
        this.status = status;
    }
    
    public final String getBoardedDate() {
        return this.boardedDate;
    }
    
    public final void setBoardedDate(final String boardedDate) {
        this.boardedDate = boardedDate;
    }
    
    public final Long getBoardedTime() {
        return this.boardedTime;
    }
    
    public final void setBoardedTime(final Long boardedTime) {
        this.boardedTime = boardedTime;
    }
    
    public final String getMigrateDate() {
        return this.migrateDate;
    }
    
    public final void setMigrateDate(final String migrateDate) {
        this.migrateDate = migrateDate;
    }
    
    public final Long getMigrateTime() {
        return this.migrateTime;
    }
    
    public final void setMigrateTime(final Long migrateTime) {
        this.migrateTime = migrateTime;
    }
    
    public final boolean getIsContacted() {
        return this.isContacted;
    }
    
    public final void setIsContacted(final boolean isContacted) {
        this.isContacted = isContacted;
    }
    
    public final int getUsage() {
        return this.usage;
    }
    
    public final void setUsage(final int usage) {
        this.usage = usage;
    }
    
    public final byte getValidationStatus() {
        return this.validationStatus;
    }
    
    public final void setValidationStatus(final byte validationStatus) {
        this.validationStatus = validationStatus;
    }
    
}
