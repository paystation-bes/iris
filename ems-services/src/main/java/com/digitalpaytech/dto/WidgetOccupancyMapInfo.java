package com.digitalpaytech.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.support.RelativeDateTime;
import com.digitalpaytech.util.support.WebObjectId;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class WidgetOccupancyMapInfo implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = -7643665873256470035L;
    @XStreamImplicit(itemFieldName = "mapLines")
    private List<MapEntry> mapInfo;
    private boolean missingPayStation;
    private RelativeDateTime lastUpdate;
    
    public WidgetOccupancyMapInfo() {
        this.mapInfo = new ArrayList<MapEntry>();
        this.missingPayStation = false;
    }
    
    public final void addMapEntry(final int payStationType, final PointOfSale pointOfSale) {
        final String name = pointOfSale.getName();
        final BigDecimal latitude = pointOfSale.getLatitude();
        final BigDecimal longitude = pointOfSale.getLongitude();
        
        final int criticalCount = pointOfSale.getPosAlertStatus().getCommunicationCritical()
                                  + pointOfSale.getPosAlertStatus().getCollectionCritical() + pointOfSale.getPosAlertStatus().getPayStationCritical();
        final int majorCount = pointOfSale.getPosAlertStatus().getCommunicationMajor() + pointOfSale.getPosAlertStatus().getCollectionMajor()
                               + pointOfSale.getPosAlertStatus().getPayStationMajor();
        final int minorCount = pointOfSale.getPosAlertStatus().getCommunicationMinor() + pointOfSale.getPosAlertStatus().getCollectionMinor()
                               + pointOfSale.getPosAlertStatus().getPayStationMinor();
        final int severity = criticalCount > 0 ? WebCoreConstants.SEVERITY_CRITICAL : majorCount > 0 ? WebCoreConstants.SEVERITY_MAJOR
                : minorCount > 0 ? WebCoreConstants.SEVERITY_MINOR : WebCoreConstants.SEVERITY_CLEAR;
        
        final MapEntry mapEntry = new MapEntry(payStationType, name, latitude, longitude, severity, null, null, 0, null, null, null,
                pointOfSale.getSerialNumber());
        this.mapInfo.add(mapEntry);
    }
    
    public final void addMapEntry(final int payStationType, final PointOfSale pos, final Integer id, final int severity) {
        
        BigDecimal latitude;
        BigDecimal longitude;
        
        if (pos.getLatitude() == null || pos.getLongitude() == null) {
            latitude = BigDecimal.ZERO;
            longitude = BigDecimal.ZERO;
        } else {
            latitude = pos.getLatitude();
            longitude = pos.getLongitude();
        }
        
        final String name = pos.getName();
        final MapEntry mapEntry = new MapEntry(payStationType, name, latitude, longitude, severity, null, null, 0, null, null, pos.getLocation()
                .getName(), pos.getSerialNumber());
        mapEntry.setRandomId(new WebObjectId(PointOfSale.class, id));
        this.mapInfo.add(mapEntry);
    }
    
    public final void addMapEntry(final int payStationType, final PointOfSale pos, final Integer posId, final int severity,
        final String relativeDateTime, final String batteryVoltage, final int paperStatus, final String lastCollection,
        final RunningTotalInfo runningTotal) {
        
        if (payStationType != WebCoreConstants.PAY_STATION_TYPE_TEST_VIRTUAL) {
            if (pos.getLatitude() == null || pos.getLongitude() == null) {
                this.missingPayStation = true;
                return;
            }
            
            final String name = pos.getName();
            final BigDecimal latitude = pos.getLatitude();
            final BigDecimal longitude = pos.getLongitude();
            final MapEntry mapEntry = new MapEntry(payStationType, name, latitude, longitude, severity, relativeDateTime, batteryVoltage,
                    paperStatus, lastCollection, runningTotal, pos.getLocation().getName(), pos.getSerialNumber());
            mapEntry.setRandomId(new WebObjectId(PointOfSale.class, posId));
            this.mapInfo.add(mapEntry);
        }
    }
    
    public final void addMapEntry(final MapEntry mapEntry) {
        if ((mapEntry.getLatitude() == null) || (mapEntry.getLongitude() == null)) {
            this.missingPayStation = true;
            return;
        }
        
        this.mapInfo.add(mapEntry);
    }
    
    public final List<MapEntry> getMapInfo() {
        return this.mapInfo;
    }
    
    public final void setMapInfo(final List<MapEntry> mapInfo) {
        this.mapInfo = mapInfo;
    }
    
    public final boolean isMissingPayStation() {
        return this.missingPayStation;
    }
    
    public final void setMissingPayStation(final boolean missingPayStation) {
        this.missingPayStation = missingPayStation;
    }
    
    public final RelativeDateTime getLastUpdate() {
        return this.lastUpdate;
    }
    
    public final void setLastUpdate(final RelativeDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
    
}
