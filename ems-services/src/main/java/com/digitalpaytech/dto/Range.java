package com.digitalpaytech.dto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.digitalpaytech.util.StandardConstants;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("range")
public class Range {
    
    @XStreamAsAttribute
    private String type;
    
    @XStreamImplicit(itemFieldName = "filter")
    private List<Filter> filters;
    
    public Range() {
    }
    
    public Range(final String type, final List<Filter> filters) {
        this.type = type;
        this.filters = filters;
    }
    
    public final boolean isCorrectType(final String rangeType) {
        if (this.type.trim().indexOf(StandardConstants.STRING_COMMA) == -1 && this.type.equalsIgnoreCase(rangeType.trim())) {
            return true;
        }
        
        final String[] typeArray = this.type.split(StandardConstants.STRING_COMMA);
        for (String typeString : typeArray) {
            if (typeString.trim().equalsIgnoreCase(rangeType.trim())) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Finds Filter objects that match with input Filter value.
     * e.g. value is 'Top N' and there are 4 Tier1 object with same filter value.
     * 
     * @param filter
     *            String type value, e.g. All, Top N, Bottom N.
     * @return List<Filter> Returns empty list if couldn't find suitable Filter data, or input filter is null.
     */
    public final List<Filter> getFilters(final String filter) {
        final List<Filter> filterObjsList = new ArrayList<Filter>();
        final Iterator<Filter> iter = this.filters.iterator();
        while (iter.hasNext()) {
            final Filter filterObj = iter.next();
            // Input tier 1 selection is empty (all) & metric tier 1 allows empty (all).
            if (filter != null && filter.trim().length() == 0 && filterObj.getType().trim().length() == 0) {
                filterObjsList.add(filterObj);
                
                // Input tier 1 selection not is empty, tier 1 value equals to tier1Obj in metric.
            } else if (filter != null && filter.trim().length() != 0 && filter.equalsIgnoreCase(filterObj.getType())) {
                filterObjsList.add(filterObj);
            }
        }
        return filterObjsList;
    }
    
    @Override
    public final String toString() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("Range type: ").append(this.type).append(", Tier1 types: ");
        Filter filter;
        String s;
        final Iterator<Filter> iter = this.filters.iterator();
        while (iter.hasNext()) {
            filter = iter.next();
            if (filter == null) {
                s = "null";
            } else {
                s = filter.toString();
            }
            bdr.append(s).append(", ");
        }
        return bdr.toString();
    }
    
    public final String getType() {
        return this.type;
    }
    
    public final void setType(final String type) {
        this.type = type;
    }
    
    public final List<Filter> getFilters() {
        return this.filters;
    }
    
    public final void setFilters(final List<Filter> filters) {
        this.filters = filters;
    }
    
    public final List<Tier1> getTier1s() {
        final List<Tier1> tier1s = new ArrayList<Tier1>();
        for (Filter filter : this.filters) {
            tier1s.addAll(filter.getTier1s());
        }
        return tier1s;
    }
    
    public final List<Tier1> getTier1s(final String tier1) {
        final List<Tier1> tier1ObjsList = new ArrayList<Tier1>();
        final List<Tier1> tier1s = new ArrayList<Tier1>();
        for (Filter filter : this.filters) {
            tier1s.addAll(filter.getTier1s());
        }
        final Iterator<Tier1> iter = tier1s.iterator();
        while (iter.hasNext()) {
            final Tier1 tier1Obj = iter.next();
            // Input tier 1 selection is empty (all) & metric tier 1 allows empty (all).
            if (tier1 != null && tier1.trim().length() == 0 && tier1Obj.getType().trim().length() == 0) {
                tier1ObjsList.add(tier1Obj);
                
                // Input tier 1 selection not is empty, tier 1 value equals to tier1Obj in metric.
            } else if (tier1 != null && tier1.trim().length() != 0 && tier1.equalsIgnoreCase(tier1Obj.getType())) {
                tier1ObjsList.add(tier1Obj);
            }
        }
        return tier1ObjsList;
    }
}
