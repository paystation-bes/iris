package com.digitalpaytech.dto;

import com.digitalpaytech.domain.CustomerBadCard;
import com.digitalpaytech.domain.CustomerCard;

public class CardIdentity {
    private Integer cardType;
    private String cardNumber;
    
    public CardIdentity(final CustomerCard card) {
        this.cardType = card.getCustomerCardType().getId();
        this.cardNumber = card.getCardNumber();
    }
    
    public CardIdentity(final CustomerBadCard badCard) {
        this.cardType = badCard.getCustomerCardType().getId();
        this.cardNumber = badCard.getCardNumberOrHash();
    }
    
    public CardIdentity(final Integer cardType, final String cardNumber) {
        this.cardType = cardType;
        this.cardNumber = cardNumber;
    }
    
    @Override
    public final int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.cardNumber == null) ? 0 : this.cardNumber.hashCode());
        result = prime * result + ((this.cardType == null) ? 0 : this.cardType.hashCode());
        return result;
    }
    
    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CardIdentity other = (CardIdentity) obj;
        if (this.cardNumber == null) {
            if (other.cardNumber != null) {
                return false;
            }
        } else if (!this.cardNumber.equals(other.cardNumber)) {
            return false;
        }
        if (this.cardType == null) {
            if (other.cardType != null) {
                return false;
            }
        } else if (!this.cardType.equals(other.cardType)) {
            return false;
        }
        return true;
    }
}
