package com.digitalpaytech.dto;

import java.io.Serializable;
import java.util.Date;

public class PointOfSaleSearchCriteria implements Serializable {
    
    private static final long serialVersionUID = 3773965906553017996L;
    
    private Integer customerId;
    private Integer routeId;
    private Integer locationId;
    private Integer pointOfSaleId;
    
    private Integer selectedRouteId;
    private Integer selectedLocationId;
    
    private boolean showHidden;
    private boolean showDeactivated = true;
    private boolean showDecommissioned = true;
    
    private boolean includeAlertStatus = true;
    
    private Integer page;
    private Integer itemsPerPage;
    
    private Date maxUpdatedTime;
    
    public PointOfSaleSearchCriteria() {
        
    }
    
    public final Integer getCustomerId() {
        return this.customerId;
    }
    
    public final void setCustomerId(final Integer customerId) {
        this.customerId = customerId;
    }
    
    public final Integer getRouteId() {
        return this.routeId;
    }
    
    public final void setRouteId(final Integer routeId) {
        this.routeId = routeId;
    }
    
    public final Integer getLocationId() {
        return this.locationId;
    }
    
    public final void setLocationId(final Integer locationId) {
        this.locationId = locationId;
    }
    
    public final boolean isShowHidden() {
        return this.showHidden;
    }
    
    public final void setShowHidden(final boolean showHidden) {
        this.showHidden = showHidden;
    }
    
    public final boolean isShowDeactivated() {
        return this.showDeactivated;
    }
    
    public final void setShowDeactivated(final boolean showDeactivated) {
        this.showDeactivated = showDeactivated;
    }
    
    public final boolean isShowDecommissioned() {
        return this.showDecommissioned;
    }
    
    public final void setShowDecommissioned(final boolean showDecommissioned) {
        this.showDecommissioned = showDecommissioned;
    }
    
    public final boolean isIncludeAlertStatus() {
        return this.includeAlertStatus;
    }
    
    public final void setIncludeAlertStatus(final boolean includeAlertStatus) {
        this.includeAlertStatus = includeAlertStatus;
    }
    
    public final Integer getPage() {
        return this.page;
    }
    
    public final void setPage(final Integer page) {
        this.page = page;
    }
    
    public final Integer getItemsPerPage() {
        return this.itemsPerPage;
    }
    
    public final void setItemsPerPage(final Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }
    
    public final Date getMaxUpdatedTime() {
        return this.maxUpdatedTime;
    }
    
    public final void setMaxUpdatedTime(final Date maxUpdatedTime) {
        this.maxUpdatedTime = maxUpdatedTime;
    }
    
    public final Integer getPointOfSaleId() {
        return this.pointOfSaleId;
    }
    
    public final void setPointOfSaleId(final Integer pointOfSaleId) {
        this.pointOfSaleId = pointOfSaleId;
    }
    
    public final Integer getSelectedRouteId() {
        return this.selectedRouteId;
    }
    
    public final void setSelectedRouteId(final Integer selectedRouteId) {
        this.selectedRouteId = selectedRouteId;
    }
    
    public final Integer getSelectedLocationId() {
        return this.selectedLocationId;
    }
    
    public final void setSelectedLocationId(final Integer selectedLocationId) {
        this.selectedLocationId = selectedLocationId;
    }
    
}
