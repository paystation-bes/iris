package com.digitalpaytech.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.support.WebObjectId;

public class MapEntry implements Serializable {
    
    private static final long serialVersionUID = 3968614274810524601L;
    private int payStationType;
    private int alertIcon;
    private int severity;
    private int alertsCount;
    private String serialNumber;
    private String name;
    private WebObjectId randomId;
    // private String randomId should map to a POS object
    private BigDecimal latitude;
    private BigDecimal longitude;
    
    private String locationName;
    private String lastSeen;
    private String batteryVoltage;
    private int batteryStatus;
    private int paperStatus;
    private String lastCollection;
    
    private RunningTotalInfo runningTotal;
    
    public MapEntry() {
        
    }
    
    public MapEntry(final Class<?> objectType, final Integer id) {
        this.randomId = new WebObjectId(objectType, id);
    }
    
    public MapEntry(final int payStationType, final String name, final BigDecimal latitude, final BigDecimal longitude, final int severity,
            final String lastSeen, final String batteryVoltage, final int paperStatus, final String lastCollection,
            final RunningTotalInfo runningTotal, final String locationName, final String serialNumber) {
        this.payStationType = payStationType;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.severity = severity;
        this.lastSeen = lastSeen;
        this.batteryVoltage = batteryVoltage;
        this.paperStatus = paperStatus;
        this.lastCollection = lastCollection;
        this.runningTotal = runningTotal;
        this.locationName = locationName;
        this.serialNumber = serialNumber;
    }
    
    public final void addDetails(final MapEntry details) {
        this.locationName = details.locationName;
        this.lastSeen = details.lastSeen;
        this.batteryVoltage = details.batteryVoltage;
        this.paperStatus = details.paperStatus;
        this.lastCollection = details.lastCollection;
        
        this.runningTotal = details.runningTotal;
    }
    
    @Override
    public final int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.randomId == null) ? 0 : super.hashCode());
        return result;
    }
    
    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MapEntry other = (MapEntry) obj;
        if (this.randomId == null) {
            if (other.randomId != null) {
                return false;
            }
        } else if (!this.randomId.equals(other.randomId)) {
            return false;
        }
        return true;
    }
    
    public final int getPayStationType() {
        return this.payStationType;
    }
    
    public final void setPayStationType(final int payStationType) {
        this.payStationType = payStationType;
    }
    
    public final String getName() {
        return this.name;
    }
    
    public final void setName(final String name) {
        this.name = name;
    }
    
    public final WebObjectId getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final WebObjectId randomId) {
        this.randomId = randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = SessionTool.getInstance().getKeyMapping().getKeyObject(randomId);
    }
    
    public final BigDecimal getLatitude() {
        return this.latitude;
    }
    
    public final void setLatitude(final BigDecimal latitude) {
        this.latitude = latitude;
    }
    
    public final BigDecimal getLongitude() {
        return this.longitude;
    }
    
    public final void setLongitude(final BigDecimal longitude) {
        this.longitude = longitude;
    }
    
    public final int getSeverity() {
        return this.severity;
    }
    
    public final void setSeverity(final int severity) {
        this.severity = severity;
    }
    
    public final String getLastSeen() {
        return this.lastSeen;
    }
    
    public final void setLastSeen(final String lastSeen) {
        this.lastSeen = lastSeen;
    }
    
    public final String getBatteryVoltage() {
        return this.batteryVoltage;
    }
    
    public final void setBatteryVoltage(final String batteryVoltage) {
        this.batteryVoltage = batteryVoltage;
    }
    
    public final int getPaperStatus() {
        return this.paperStatus;
    }
    
    public final void setPaperStatus(final int paperStatus) {
        this.paperStatus = paperStatus;
    }
    
    public final String getLastCollection() {
        return this.lastCollection;
    }
    
    public final void setLastCollection(final String lastCollection) {
        this.lastCollection = lastCollection;
    }
    
    public final RunningTotalInfo getRunningTotal() {
        return this.runningTotal;
    }
    
    public final void setRunningTotal(final RunningTotalInfo runningTotal) {
        this.runningTotal = runningTotal;
    }
    
    public final String getLocationName() {
        return this.locationName;
    }
    
    public final void setLocationName(final String locationName) {
        this.locationName = locationName;
    }
    
    public final String getSerialNumber() {
        return this.serialNumber;
    }
    
    public final void setSerialNumber(final String serialNumber) {
        this.serialNumber = serialNumber;
    }
    
    public final int getAlertsCount() {
        return this.alertsCount;
    }
    
    public final void setAlertsCount(final int alertsCount) {
        this.alertsCount = alertsCount;
    }
    
    public final int getAlertIcon() {
        return this.alertIcon;
    }
    
    public final void setAlertIcon(final int alertIcon) {
        this.alertIcon = alertIcon;
    }
    
    public final int getBatteryStatus() {
        return this.batteryStatus;
    }
    
    public final void setBatteryStatus(final int batteryStatus) {
        this.batteryStatus = batteryStatus;
    }
    
}
