package com.digitalpaytech.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MobileLicenseListInfo implements Serializable {
    
    private static final long serialVersionUID = 2903049418359868244L;
    
    private List<MobileLicenseInfo> licenseInfos;
    private int licenseCount;
    
    public MobileLicenseListInfo() {
        this.licenseInfos = new ArrayList<MobileLicenseInfo>();
    }
    
    public final int getLicenseCount() {
        return this.licenseCount;
    }
    
    public final void setLicenseCount(final int licenseCount) {
        this.licenseCount = licenseCount;
    }
    
    public final void addMobileLicenseInfo(final MobileLicenseInfo info) {
        this.licenseInfos.add(info);
    }
    
    public final void setMobileLicenseInfos(final List<MobileLicenseInfo> infos) {
        this.licenseInfos = infos;
    }
    
    public final List<MobileLicenseInfo> getMobileLicenseInfos() {
        return this.licenseInfos;
    }
}
