package com.digitalpaytech.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Transient;

public class RateInfo implements Serializable {
    
    private static final long serialVersionUID = 6118661628874400467L;
    private String randomId;
    private String name;
    private Byte rateType;
    private Boolean paymentCoin;
    private Boolean paymentBill;
    private Boolean paymentCC;
    private Boolean paymentSC;
    private Boolean paymentRF;
    private Boolean paymentPC;
    private Boolean paymentCoupon;
    private Boolean promptForCoupon;
    private Boolean enableAddTime;
    private Boolean useAddTimeNumber;
    private Boolean useSpaceOrPlate;
    private Boolean useEbP;
    private BigDecimal ebPServiceFeeAmount;
    private Integer ebPMiniumExt;
    private BigDecimal rateAmount;
    private Integer maxDays;
    private BigDecimal monAmount;
    private BigDecimal tueAmount;
    private BigDecimal wedAmount;
    private BigDecimal thuAmount;
    private BigDecimal friAmount;
    private BigDecimal satAmount;
    private BigDecimal sunAmount;
    private Boolean useMaxPurchases;
    private Integer maxPurchases;
    private Integer daysBeforeStart;
    private Integer daysAfterStart;
    private BigDecimal minPaymentAmount;
    private BigDecimal maxPaymentAmount;
    private Boolean useFlatRate;
    private BigDecimal flatRateAmount;
    private String flatRateStartTime;
    private String flatRateEndTime;
    private Boolean useLastIncrement;
    private Boolean useInc01;
    private Boolean useInc02;
    private Boolean useInc03;
    private Boolean useInc04;
    private Boolean useInc05;
    private Boolean useInc06;
    private Boolean useInc07;
    private Boolean useInc08;
    private Boolean useInc09;
    private Boolean useInc10;
    private Integer minsInc01;
    private Integer minsInc02;
    private Integer minsInc03;
    private Integer minsInc04;
    private Integer minsInc05;
    private Integer minsInc06;
    private Integer minsInc07;
    private Integer minsInc08;
    private Integer minsInc09;
    private Integer minsInc10;
    private Integer maxHours;
    private Integer[] ruleType;
    private Integer[] ruleHours;
    private BigDecimal[] ruleRateAmount;
    private Integer messageDisplayMins;
    private String message1;
    private String message2;
    private String message3;
    private String message4;
    
    @Transient
    private Integer rateId;
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final String getName() {
        return this.name;
    }
    
    public final void setName(final String name) {
        this.name = name;
    }
    
    public final Byte getRateType() {
        return this.rateType;
    }
    
    public final void setRateType(final Byte rateType) {
        this.rateType = rateType;
    }
    
    public final Boolean getPaymentCoin() {
        return this.paymentCoin;
    }
    
    public final void setPaymentCoin(final Boolean paymentCoin) {
        this.paymentCoin = paymentCoin;
    }
    
    public final Boolean getPaymentBill() {
        return this.paymentBill;
    }
    
    public final void setPaymentBill(final Boolean paymentBill) {
        this.paymentBill = paymentBill;
    }
    
    public final Boolean getPaymentCC() {
        return this.paymentCC;
    }
    
    public final void setPaymentCC(final Boolean paymentCC) {
        this.paymentCC = paymentCC;
    }
    
    public final Boolean getPaymentSC() {
        return this.paymentSC;
    }
    
    public final void setPaymentSC(final Boolean paymentSC) {
        this.paymentSC = paymentSC;
    }
    
    public final Boolean getPaymentRF() {
        return this.paymentRF;
    }
    
    public final void setPaymentRF(final Boolean paymentRF) {
        this.paymentRF = paymentRF;
    }
    
    public final Boolean getPaymentPC() {
        return this.paymentPC;
    }
    
    public final void setPaymentPC(final Boolean paymentPC) {
        this.paymentPC = paymentPC;
    }
    
    public final Boolean getPaymentCoupon() {
        return this.paymentCoupon;
    }
    
    public final void setPaymentCoupon(final Boolean paymentCoupon) {
        this.paymentCoupon = paymentCoupon;
    }
    
    public final Boolean getPromptForCoupon() {
        return this.promptForCoupon;
    }
    
    public final void setPromptForCoupon(final Boolean promptForCoupon) {
        this.promptForCoupon = promptForCoupon;
    }
    
    public final Boolean getEnableAddTime() {
        return this.enableAddTime;
    }
    
    public final void setEnableAddTime(final Boolean enableAddTime) {
        this.enableAddTime = enableAddTime;
    }
    
    public final Boolean getUseAddTimeNumber() {
        return this.useAddTimeNumber;
    }
    
    public final void setUseAddTimeNumber(final Boolean useAddTimeNumber) {
        this.useAddTimeNumber = useAddTimeNumber;
    }
    
    public final Boolean getUseSpaceOrPlate() {
        return this.useSpaceOrPlate;
    }
    
    public final void setUseSpaceOrPlate(final Boolean useSpaceOrPlate) {
        this.useSpaceOrPlate = useSpaceOrPlate;
    }
    
    public final Boolean getUseEbP() {
        return this.useEbP;
    }
    
    public final void setUseEbP(final Boolean useEbP) {
        this.useEbP = useEbP;
    }
    
    public final BigDecimal getEbPServiceFeeAmount() {
        return this.ebPServiceFeeAmount;
    }
    
    public final void setEbPServiceFeeAmount(final BigDecimal ebPServiceFeeAmount) {
        this.ebPServiceFeeAmount = ebPServiceFeeAmount;
    }
    
    public final Integer getEbPMiniumExt() {
        return this.ebPMiniumExt;
    }
    
    public final void setEbPMiniumExt(final Integer ebPMiniumExt) {
        this.ebPMiniumExt = ebPMiniumExt;
    }
    
    public final BigDecimal getRateAmount() {
        return this.rateAmount;
    }
    
    public final void setRateAmount(final BigDecimal rateAmount) {
        this.rateAmount = rateAmount;
    }
    
    public final Integer getMaxDays() {
        return this.maxDays;
    }
    
    public final void setMaxDays(final Integer maxDays) {
        this.maxDays = maxDays;
    }
    
    public final BigDecimal getMonAmount() {
        return this.monAmount;
    }
    
    public final void setMonAmount(final BigDecimal monAmount) {
        this.monAmount = monAmount;
    }
    
    public final BigDecimal getTueAmount() {
        return this.tueAmount;
    }
    
    public final void setTueAmount(final BigDecimal tueAmount) {
        this.tueAmount = tueAmount;
    }
    
    public final BigDecimal getWedAmount() {
        return this.wedAmount;
    }
    
    public final void setWedAmount(final BigDecimal wedAmount) {
        this.wedAmount = wedAmount;
    }
    
    public final BigDecimal getThuAmount() {
        return this.thuAmount;
    }
    
    public final void setThuAmount(final BigDecimal thuAmount) {
        this.thuAmount = thuAmount;
    }
    
    public final BigDecimal getFriAmount() {
        return this.friAmount;
    }
    
    public final void setFriAmount(final BigDecimal friAmount) {
        this.friAmount = friAmount;
    }
    
    public final BigDecimal getSatAmount() {
        return this.satAmount;
    }
    
    public final void setSatAmount(final BigDecimal satAmount) {
        this.satAmount = satAmount;
    }
    
    public final BigDecimal getSunAmount() {
        return this.sunAmount;
    }
    
    public final void setSunAmount(final BigDecimal sunAmount) {
        this.sunAmount = sunAmount;
    }
    
    public final Boolean getUseMaxPurchases() {
        return this.useMaxPurchases;
    }
    
    public final void setUseMaxPurchases(final Boolean useMaxPurchases) {
        this.useMaxPurchases = useMaxPurchases;
    }
    
    public final Integer getMaxPurchases() {
        return this.maxPurchases;
    }
    
    public final void setMaxPurchases(final Integer maxPurchases) {
        this.maxPurchases = maxPurchases;
    }
    
    public final Integer getDaysBeforeStart() {
        return this.daysBeforeStart;
    }
    
    public final void setDaysBeforeStart(final Integer daysBeforeStart) {
        this.daysBeforeStart = daysBeforeStart;
    }
    
    public final Integer getDaysAfterStart() {
        return this.daysAfterStart;
    }
    
    public final void setDaysAfterStart(final Integer daysAfterStart) {
        this.daysAfterStart = daysAfterStart;
    }
    
    public final BigDecimal getMinPaymentAmount() {
        return this.minPaymentAmount;
    }
    
    public final void setMinPaymentAmount(final BigDecimal minPaymentAmount) {
        this.minPaymentAmount = minPaymentAmount;
    }
    
    public final BigDecimal getMaxPaymentAmount() {
        return this.maxPaymentAmount;
    }
    
    public final void setMaxPaymentAmount(final BigDecimal maxPaymentAmount) {
        this.maxPaymentAmount = maxPaymentAmount;
    }
    
    public final Boolean getUseFlatRate() {
        return this.useFlatRate;
    }
    
    public final void setUseFlatRate(final Boolean useFlatRate) {
        this.useFlatRate = useFlatRate;
    }
    
    public final BigDecimal getFlatRateAmount() {
        return this.flatRateAmount;
    }
    
    public final void setFlatRateAmount(final BigDecimal flatRateAmount) {
        this.flatRateAmount = flatRateAmount;
    }
    
    public final String getFlatRateStartTime() {
        return this.flatRateStartTime;
    }
    
    public final void setFlatRateStartTime(final String flatRateStartTime) {
        this.flatRateStartTime = flatRateStartTime;
    }
    
    public final String getFlatRateEndTime() {
        return this.flatRateEndTime;
    }
    
    public final void setFlatRateEndTime(final String flatRateEndTime) {
        this.flatRateEndTime = flatRateEndTime;
    }
    
    public final Boolean getUseLastIncrement() {
        return this.useLastIncrement;
    }
    
    public final void setUseLastIncrement(final Boolean useLastIncrement) {
        this.useLastIncrement = useLastIncrement;
    }
    
    public final Boolean getUseInc01() {
        return this.useInc01;
    }
    
    public final void setUseInc01(final Boolean useInc01) {
        this.useInc01 = useInc01;
    }
    
    public final Boolean getUseInc02() {
        return this.useInc02;
    }
    
    public final void setUseInc02(final Boolean useInc02) {
        this.useInc02 = useInc02;
    }
    
    public final Boolean getUseInc03() {
        return this.useInc03;
    }
    
    public final void setUseInc03(final Boolean useInc03) {
        this.useInc03 = useInc03;
    }
    
    public final Boolean getUseInc04() {
        return this.useInc04;
    }
    
    public final void setUseInc04(final Boolean useInc04) {
        this.useInc04 = useInc04;
    }
    
    public final Boolean getUseInc05() {
        return this.useInc05;
    }
    
    public final void setUseInc05(final Boolean useInc05) {
        this.useInc05 = useInc05;
    }
    
    public final Boolean getUseInc06() {
        return this.useInc06;
    }
    
    public final void setUseInc06(final Boolean useInc06) {
        this.useInc06 = useInc06;
    }
    
    public final Boolean getUseInc07() {
        return this.useInc07;
    }
    
    public final void setUseInc07(final Boolean useInc07) {
        this.useInc07 = useInc07;
    }
    
    public final Boolean getUseInc08() {
        return this.useInc08;
    }
    
    public final void setUseInc08(final Boolean useInc08) {
        this.useInc08 = useInc08;
    }
    
    public final Boolean getUseInc09() {
        return this.useInc09;
    }
    
    public final void setUseInc09(final Boolean useInc09) {
        this.useInc09 = useInc09;
    }
    
    public final Boolean getUseInc10() {
        return this.useInc10;
    }
    
    public final void setUseInc10(final Boolean useInc10) {
        this.useInc10 = useInc10;
    }
    
    public final Integer getMinsInc01() {
        return this.minsInc01;
    }
    
    public final void setMinsInc01(final Integer minsInc01) {
        this.minsInc01 = minsInc01;
    }
    
    public final Integer getMinsInc02() {
        return this.minsInc02;
    }
    
    public final void setMinsInc02(final Integer minsInc02) {
        this.minsInc02 = minsInc02;
    }
    
    public final Integer getMinsInc03() {
        return this.minsInc03;
    }
    
    public final void setMinsInc03(final Integer minsInc03) {
        this.minsInc03 = minsInc03;
    }
    
    public final Integer getMinsInc04() {
        return this.minsInc04;
    }
    
    public final void setMinsInc04(final Integer minsInc04) {
        this.minsInc04 = minsInc04;
    }
    
    public final Integer getMinsInc05() {
        return this.minsInc05;
    }
    
    public final void setMinsInc05(final Integer minsInc05) {
        this.minsInc05 = minsInc05;
    }
    
    public final Integer getMinsInc06() {
        return this.minsInc06;
    }
    
    public final void setMinsInc06(final Integer minsInc06) {
        this.minsInc06 = minsInc06;
    }
    
    public final Integer getMinsInc07() {
        return this.minsInc07;
    }
    
    public final void setMinsInc07(final Integer minsInc07) {
        this.minsInc07 = minsInc07;
    }
    
    public final Integer getMinsInc08() {
        return this.minsInc08;
    }
    
    public final void setMinsInc08(final Integer minsInc08) {
        this.minsInc08 = minsInc08;
    }
    
    public final Integer getMinsInc09() {
        return this.minsInc09;
    }
    
    public final void setMinsInc09(final Integer minsInc09) {
        this.minsInc09 = minsInc09;
    }
    
    public final Integer getMinsInc10() {
        return this.minsInc10;
    }
    
    public final void setMinsInc10(final Integer minsInc10) {
        this.minsInc10 = minsInc10;
    }
    
    public final Integer getMaxHours() {
        return this.maxHours;
    }
    
    public final void setMaxHours(final Integer maxHours) {
        this.maxHours = maxHours;
    }
    
    public final Integer[] getRuleType() {
        return this.ruleType;
    }
    
    public final void setRuleType(final Integer[] ruleType) {
        this.ruleType = ruleType;
    }
    
    public final Integer[] getRuleHours() {
        return this.ruleHours;
    }
    
    public final void setRuleHours(final Integer[] ruleHours) {
        this.ruleHours = ruleHours;
    }
    
    public final BigDecimal[] getRuleRateAmount() {
        return this.ruleRateAmount;
    }
    
    public final void setRuleRateAmount(final BigDecimal[] ruleRateAmount) {
        this.ruleRateAmount = ruleRateAmount;
    }
    
    public final Integer getMessageDisplayMins() {
        return this.messageDisplayMins;
    }
    
    public final void setMessageDisplayMins(final Integer messageDisplayMins) {
        this.messageDisplayMins = messageDisplayMins;
    }
    
    public final String getMessage1() {
        return this.message1;
    }
    
    public final void setMessage1(final String message1) {
        this.message1 = message1;
    }
    
    public final String getMessage2() {
        return this.message2;
    }
    
    public final void setMessage2(final String message2) {
        this.message2 = message2;
    }
    
    public final String getMessage3() {
        return this.message3;
    }
    
    public final void setMessage3(final String message3) {
        this.message3 = message3;
    }
    
    public final String getMessage4() {
        return this.message4;
    }
    
    public final void setMessage4(final String message4) {
        this.message4 = message4;
    }
    
    public final Integer getRateId() {
        return this.rateId;
    }
    
    public final void setRateId(final Integer rateId) {
        this.rateId = rateId;
    }
    
}
