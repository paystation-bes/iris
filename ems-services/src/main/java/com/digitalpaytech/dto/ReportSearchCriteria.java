package com.digitalpaytech.dto;

public final class ReportSearchCriteria {
    private boolean sysadmin;
    private Integer customerId;
    private Integer userAccountId;
    
    public ReportSearchCriteria() {
        
    }

    public boolean isSysadmin() {
        return this.sysadmin;
    }

    public void setSysadmin(final boolean sysadmin) {
        this.sysadmin = sysadmin;
    }

    public Integer getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(final Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getUserAccountId() {
        return this.userAccountId;
    }

    public void setUserAccountId(final Integer userAccountId) {
        this.userAccountId = userAccountId;
    }
}
