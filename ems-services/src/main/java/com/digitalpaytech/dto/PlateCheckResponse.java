package com.digitalpaytech.dto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PlateCheckResponse implements Serializable {
    private static final long serialVersionUID = 3690466259182723890L;
    
    @JsonProperty
    private boolean isPreferred;
    
    @JsonProperty
    private int dailyLimitedCount;
    
    @JsonProperty
    private int dailyPreferredCount;
    
    public PlateCheckResponse() {
        // Default constructor
    }
    
    public PlateCheckResponse(final boolean isPreferred, final int dailyLimitedCount, final int dailyPreferredCount) {
        this.isPreferred = isPreferred;
        this.dailyLimitedCount = dailyLimitedCount;
        this.dailyPreferredCount = dailyPreferredCount;
    }
    
    public final boolean getIsPreferred() {
        return this.isPreferred;
    }
    
    public final void setIsPreferred(final boolean isPreferred) {
        this.isPreferred = isPreferred;
    }
    
    public final int getDailyLimitedCount() {
        return this.dailyLimitedCount;
    }
    
    public final void setDailyLimitedCount(final int dailyLimitedCount) {
        this.dailyLimitedCount = dailyLimitedCount;
    }
    
    public final int getDailyPreferredCount() {
        return this.dailyPreferredCount;
    }
    
    public final void setDailyPreferredCount(final int dailyPreferredCount) {
        this.dailyPreferredCount = dailyPreferredCount;
    }
}
