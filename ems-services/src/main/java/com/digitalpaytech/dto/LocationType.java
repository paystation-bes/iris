package com.digitalpaytech.dto;

import com.digitalpaytech.util.WebCoreConstants;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("locationType")
public class LocationType extends SettingsType {
    
    private static final long serialVersionUID = -707518974836736719L;
    private String orgId;
    
    public LocationType() {
        setStatus(WebCoreConstants.ACTIVE);
    }
    
    public final String getOrgId() {
        return this.orgId;
    }
    
    public final void setOrgId(final String orgId) {
        this.orgId = orgId;
    }
}
