package com.digitalpaytech.dto;

import java.io.Serializable;

import com.digitalpaytech.util.support.WebObjectId;

public class AdhocReportInfo implements Serializable {
    
    public static final String PARAM_ID = "id";
    
    private static final long serialVersionUID = 936021829859298527L;
    
    private WebObjectId reportDefinitionId;
    private Long queuedGmt;
    private WebObjectId reportHistoryId;
    
    public AdhocReportInfo() {
        
    }
    
    public AdhocReportInfo(final WebObjectId reportDefinitionId, final Long queuedGmt) {
        this.reportDefinitionId = reportDefinitionId;
        this.queuedGmt = queuedGmt;
    }
    
    public final void appendAsParameterString(final StringBuilder result) {
        result.append(PARAM_ID);
        result.append("=");
        result.append(this.reportDefinitionId);
        
        if (this.queuedGmt != null) {
            result.append("_");
            result.append(this.queuedGmt);
        }
    }
    
    public final WebObjectId getReportDefinitionId() {
        return this.reportDefinitionId;
    }
    
    public final void setReportDefinitionId(final WebObjectId reportDefinitionId) {
        this.reportDefinitionId = reportDefinitionId;
    }
    
    public final Long getQueuedGmt() {
        return this.queuedGmt;
    }
    
    public final void setQueuedGmt(final Long queuedGmt) {
        this.queuedGmt = queuedGmt;
    }
    
    public final WebObjectId getReportHistoryId() {
        return this.reportHistoryId;
    }
    
    public final void setReportHistoryId(final WebObjectId reportHistoryId) {
        this.reportHistoryId = reportHistoryId;
    }
}
