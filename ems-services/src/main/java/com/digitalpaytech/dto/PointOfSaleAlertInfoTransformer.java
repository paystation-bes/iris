package com.digitalpaytech.dto;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.transform.AliasToBeanResultTransformer;

public class PointOfSaleAlertInfoTransformer extends AliasToBeanResultTransformer {
    
    private static final long serialVersionUID = 8089550990498202941L;
    
    private Map<String, Integer> aliasIdxMap;
    
    public PointOfSaleAlertInfoTransformer() {
        super(PointOfSaleAlertInfo.class);
    }
    
    @Override
    public final Object transformTuple(final Object[] tuple, final String[] aliases) {
        resolveIdx(tuple, aliases);
        final PointOfSaleAlertInfo result = new PointOfSaleAlertInfo();
        result.setPosId((Integer) tuple[this.aliasIdxMap.get("id")]);
        result.setPosName((String) tuple[this.aliasIdxMap.get("name")]);
        result.setSerialNumber((String) tuple[this.aliasIdxMap.get("serialNumber")]);
        result.setHeartBeat((Date) tuple[this.aliasIdxMap.get("heartBeat")]);
        result.setAlertTypeId((Short) transformOrNull(tuple, "typeId"));
        result.setEventSeverity((Integer) transformOrNull(tuple, "severity"));
        result.setCreated((Date) transformOrNull(tuple, "created"));
        result.setCleared((Date) transformOrNull(tuple, "cleared"));
        result.setIsActive((Boolean) transformOrNull(tuple, "isActive"));
        result.setLastCoinCollection((Date) tuple[this.aliasIdxMap.get("lastCoinCollection")]);
        result.setLastBillCollection((Date) tuple[this.aliasIdxMap.get("lastBillCollection")]);
        result.setLastCardCollection((Date) tuple[this.aliasIdxMap.get("lastCardCollection")]);
        return result;
    }
    
    private Object transformOrNull(final Object[] tuple, final String alias) {
        if (this.aliasIdxMap.get(alias) == null) {
            return null;
        }
        return tuple[this.aliasIdxMap.get(alias)];
    }
    
    private void resolveIdx(final Object[] tuple, final String[] aliases) {
        if (this.aliasIdxMap == null) {
            int idx = aliases.length;
            this.aliasIdxMap = new HashMap<String, Integer>(idx * 2, 0.75F);
            while (--idx >= 0) {
                this.aliasIdxMap.put(aliases[idx], idx);
            }
        }
    }
}
