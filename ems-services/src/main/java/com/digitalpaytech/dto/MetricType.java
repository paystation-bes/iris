package com.digitalpaytech.dto;

import com.digitalpaytech.util.WebCoreConstants;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("metricType")
public class MetricType extends SettingsType {
    private static final long serialVersionUID = -7457350743815495798L;
    
    public MetricType() {
        setStatus(WebCoreConstants.ACTIVE);
    }
}
