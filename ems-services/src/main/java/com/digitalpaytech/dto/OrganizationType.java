package com.digitalpaytech.dto;

import com.digitalpaytech.util.WebCoreConstants;

public class OrganizationType extends SettingsType {
    private static final long serialVersionUID = 1909035000350280393L;
    
    public OrganizationType() {
        setStatus(WebCoreConstants.ACTIVE);
    }
}
