package com.digitalpaytech.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import com.digitalpaytech.util.WebCoreConstants;

public class CollectionSearchCriteria implements Serializable {
    private static final long serialVersionUID = 638840575684765209L;
    
    private Integer customerId;
    private Integer locationId;
    private Integer routeId;
    private Integer pointOfSaleId;
    
    private Collection<Integer> collectionTypeIds;
    
    private Date minCollectionDate;
    private Date maxCollectionDate;
    
    private Integer page = 0;
    private Integer itemsPerPage = WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT;
    private Integer targetId;
    
    public CollectionSearchCriteria() {
        
    }
    
    public final Integer getCustomerId() {
        return this.customerId;
    }
    
    public final void setCustomerId(final Integer customerId) {
        this.customerId = customerId;
    }
    
    public final Integer getLocationId() {
        return this.locationId;
    }
    
    public final void setLocationId(final Integer locationId) {
        this.locationId = locationId;
    }
    
    public final Integer getRouteId() {
        return this.routeId;
    }
    
    public final void setRouteId(final Integer routeId) {
        this.routeId = routeId;
    }
    
    public final Collection<Integer> getCollectionTypeIds() {
        return this.collectionTypeIds;
    }
    
    public final void setCollectionTypeIds(final Collection<Integer> collectionTypeIds) {
        this.collectionTypeIds = collectionTypeIds;
    }
    
    public final Integer getPage() {
        return this.page;
    }
    
    public final void setPage(final Integer page) {
        this.page = page;
    }
    
    public final Integer getItemsPerPage() {
        return this.itemsPerPage;
    }
    
    public final void setItemsPerPage(final Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }
    
    public final Integer getTargetId() {
        return this.targetId;
    }
    
    public final void setTargetId(final Integer targetId) {
        this.targetId = targetId;
    }
    
    public final Date getMaxCollectionDate() {
        return this.maxCollectionDate;
    }
    
    public final void setMaxCollectionDate(final Date maxCollectionDate) {
        this.maxCollectionDate = maxCollectionDate;
    }
    
    public final Date getMinCollectionDate() {
        return this.minCollectionDate;
    }
    
    public final void setMinCollectionDate(final Date minCollectionDate) {
        this.minCollectionDate = minCollectionDate;
    }
    
    public final Integer getPointOfSaleId() {
        return this.pointOfSaleId;
    }
    
    public final void setPointOfSaleId(final Integer pointOfSaleId) {
        this.pointOfSaleId = pointOfSaleId;
    }
}
