package com.digitalpaytech.dto;

import java.util.HashMap;
import java.util.Map;

import org.hibernate.transform.AliasToBeanResultTransformer;

public class PosAlertStatusDetailSummaryTransformer extends AliasToBeanResultTransformer {
    
    private static final long serialVersionUID = -4292615752397274477L;
    
    private Map<Integer, PosAlertStatusDetailSummary> result;
    
    private int posIdIdx = -1;
    private int totalMinorIdx = -1;
    private int totalMajorIdx = -1;
    private int totalCriticalIdx = -1;
    
    public PosAlertStatusDetailSummaryTransformer() {
        this(new HashMap<Integer, PosAlertStatusDetailSummary>());
    }
    
    public PosAlertStatusDetailSummaryTransformer(final Map<Integer, PosAlertStatusDetailSummary> result) {
        super(PosAlertStatusDetailSummary.class);
        this.result = result;
    }
    
    @Override
    public final Object transformTuple(final Object[] tuple, final String[] aliases) {
        if (this.posIdIdx < 0) {
            resolveIdx(tuple, aliases);
        }
        
        final Integer posId = (Integer) tuple[this.posIdIdx];
        
        final PosAlertStatusDetailSummary pasds = new PosAlertStatusDetailSummary();
        pasds.setTotalMinor(((Number) tuple[this.totalMinorIdx]).intValue());
        pasds.setTotalMajor(((Number) tuple[this.totalMajorIdx]).intValue());
        pasds.setTotalCritical(((Number) tuple[this.totalCriticalIdx]).intValue());
        
        this.result.put(posId, pasds);
        
        return pasds;
    }
    
    private void resolveIdx(final Object[] tuple, final String[] aliases) {
        int idx = aliases.length;
        while (--idx >= 0) {
            
            if (aliases[idx].equals("posId")) {
                this.posIdIdx = idx;
            } else if (aliases[idx].equals("totalMinor")) {
                this.totalMinorIdx = idx;
            } else if (aliases[idx].equals("totalMajor")) {
                this.totalMajorIdx = idx;
            } else if (aliases[idx].equals("totalCritical")) {
                this.totalCriticalIdx = idx;
            }
        }
    }
    
    public final Map<Integer, PosAlertStatusDetailSummary> getResult() {
        return this.result;
    }
    
}
