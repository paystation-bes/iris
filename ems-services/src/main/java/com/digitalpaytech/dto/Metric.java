package com.digitalpaytech.dto;

import java.util.Iterator;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("metric")
public class Metric {
    
    @XStreamAsAttribute
    private String name;
    
    @XStreamImplicit(itemFieldName = "range")
    private List<Range> ranges;
    
    public Metric() {
    }
    
    public Metric(final String name, final List<Range> ranges) {
        this.name = name;
        this.ranges = ranges;
    }
    
    public final Range getRange(final String type) {
        Range range;
        final Iterator<Range> iter = this.ranges.iterator();
        while (iter.hasNext()) {
            range = iter.next();
            if (range.isCorrectType(type)) {
                return range;
            }
        }
        return null;
    }
    
    public final String getName() {
        return this.name;
    }
    
    public final void setName(final String name) {
        this.name = name;
    }
    
    public final List<Range> getRanges() {
        return this.ranges;
    }
    
    public final void setRanges(final List<Range> ranges) {
        this.ranges = ranges;
    }
}
