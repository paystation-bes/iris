package com.digitalpaytech.dto;

import java.io.Serializable;
import java.util.Date;

import com.digitalpaytech.util.cache.ObjectCache;

public class BatchProcessStatus implements Serializable {
    private static final long serialVersionUID = -1983654368815226653L;
    
    private transient ObjectCache cache;
    private transient String key;
    
    private Date startTime;
    private Date endTime;
    
    private boolean error;
    private boolean warning;
    private String message;
    private Throwable exception;
    
    public BatchProcessStatus() {
        
    }
    
    protected BatchProcessStatus(final BatchProcessStatus other) {
        this.startTime = other.startTime;
        this.endTime = other.endTime;
        
        this.error = other.error;
        this.message = other.message;
        this.exception = other.exception;
    }
    
    public void reconnect(final ObjectCache cache, final String key) {
        this.cache = cache;
        this.key = key;
    }
    
    public final void flush() {
        if ((this.cache != null) && (this.key != null)) {
            this.cache.put(BatchProcessStatus.class, this.key, this);
        }
    }
    
    public final void start() {
        this.startTime = new Date();
        
        flush();
    }
    
    public final void warn() {
        this.warning = true;
        
        flush();
    }
    
    public final void success() {
        success((String) null);
    }
    
    public final void success(final String newMessage) {
        this.endTime = new Date();
        
        this.error = false;
        this.message = newMessage;
        
        flush();
    }
    
    public final void fail() {
        fail((String) null, (Exception) null);
    }
    
    public final void fail(final String newMessage) {
        fail(newMessage, (Exception) null);
    }
    
    public final void fail(final String newMessage, final Throwable newException) {
        this.endTime = new Date();
        
        this.error = true;
        this.message = newMessage;
        this.exception = newException;
        
        flush();
    }
    
    public final boolean isProcessing() {
        return (this.startTime != null) && (this.endTime == null);
    }
    
    public final boolean isDone() {
        return this.endTime != null;
    }
    
    public final boolean isError() {
        return this.error;
    }
    
    public final boolean hasWarning() {
        return this.warning;
    }
    
    public final String getMessage() {
        return this.message;
    }
    
    public final Throwable getException() {
        return this.exception;
    }
    
    public final Date getStartTime() {
        return this.startTime;
    }
    
    public final Date getEndTime() {
        return this.endTime;
    }
}
