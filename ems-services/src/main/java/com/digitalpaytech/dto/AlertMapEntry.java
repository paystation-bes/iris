package com.digitalpaytech.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

import com.digitalpaytech.util.support.ValueProxy;
import com.digitalpaytech.util.support.WebObjectId;

public class AlertMapEntry implements Serializable {
    
    private static final long serialVersionUID = 5486855970537844990L;
    
    private WebObjectId posRandomId;
    private String serial;
    private Integer payStationType;
    
    private BigDecimal latitude;
    private BigDecimal longitude;
    
    private Integer severity;
    private Integer numberOfAlerts;
    
    private String pointOfSaleName;
    private String module;
    private ValueProxy<StringBuilder, Integer> route;
    
    private Date alertGMT;
    private String alertDate;
    
    public AlertMapEntry() {
        
    }
    
    public final WebObjectId getPosRandomId() {
        return this.posRandomId;
    }
    
    public final void setPosRandomId(final WebObjectId posRandomId) {
        this.posRandomId = posRandomId;
    }
    
    public final String getSerial() {
        return this.serial;
    }
    
    public final void setSerial(final String serial) {
        this.serial = serial;
    }
    
    public final Integer getPayStationType() {
        return this.payStationType;
    }
    
    public final void setPayStationType(final Integer payStationType) {
        this.payStationType = payStationType;
    }
    
    public final BigDecimal getLatitude() {
        return this.latitude;
    }
    
    public final void setLatitude(final BigDecimal latitude) {
        this.latitude = latitude;
    }
    
    public final BigDecimal getLongitude() {
        return this.longitude;
    }
    
    public final void setLongitude(final BigDecimal longitude) {
        this.longitude = longitude;
    }
    
    public final Integer getSeverity() {
        return this.severity;
    }
    
    public final void setSeverity(final Integer severity) {
        this.severity = severity;
    }
    
    public final Integer getNumberOfAlerts() {
        return this.numberOfAlerts;
    }
    
    public final void setNumberOfAlerts(final Integer numberOfAlerts) {
        this.numberOfAlerts = numberOfAlerts;
    }
    
    public final String getPointOfSaleName() {
        return this.pointOfSaleName;
    }
    
    public final void setPointOfSaleName(final String pointOfSaleName) {
        this.pointOfSaleName = pointOfSaleName;
    }
    
    public final String getModule() {
        return this.module;
    }
    
    public final void setModule(final String module) {
        this.module = module;
    }
    
    public final ValueProxy<StringBuilder, Integer> getRoute() {
        return this.route;
    }
    
    public final void setRoute(final ValueProxy<StringBuilder, Integer> route) {
        this.route = route;
    }
    
    public final Date getAlertGMT() {
        return this.alertGMT;
    }
    
    public final void setAlertGMT(final Date alertGMT) {
        this.alertGMT = alertGMT;
    }
    
    public final String getAlertDate() {
        return this.alertDate;
    }
    
    public final void setAlertDate(final String alertDate) {
        this.alertDate = alertDate;
    }
}
