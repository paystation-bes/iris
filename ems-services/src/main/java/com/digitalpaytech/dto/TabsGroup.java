package com.digitalpaytech.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.digitalpaytech.domain.Tab;
import com.digitalpaytech.domain.UserAccount;

public class TabsGroup implements Serializable {
    
    private static final long serialVersionUID = 2554703820019386001L;
    
    private Map<String, Tab> tabTreeMap;
    private List<Integer> sectionIds;
    private List<Integer> widgetIds;
    private List<Integer> subsetMemberIds;
    private UserAccount userAccount;
    
    public TabsGroup(final Map<String, Tab> tabTreeMap, final List<Integer> sectionIdList, final List<Integer> widgetIdList,
            final List<Integer> subsetMemberIdList, final UserAccount userAccount) {
        this.sectionIds = new ArrayList<Integer>();
        this.widgetIds = new ArrayList<Integer>();
        this.subsetMemberIds = new ArrayList<Integer>();
        
        if (sectionIdList != null && !sectionIdList.isEmpty()) {
            this.sectionIds.addAll(sectionIdList);
        }
        if (widgetIdList != null && !widgetIdList.isEmpty()) {
            this.widgetIds.addAll(widgetIdList);
        }
        if (subsetMemberIdList != null && !subsetMemberIdList.isEmpty()) {
            this.subsetMemberIds.addAll(subsetMemberIdList);
        }
        this.tabTreeMap = tabTreeMap;
        this.userAccount = userAccount;
    }
    
    public final Map<String, Tab> getTabTreeMap() {
        return this.tabTreeMap;
    }
    
    public final void setTabTreeMap(final Map<String, Tab> tabTreeMap) {
        this.tabTreeMap = tabTreeMap;
    }
    
    public final UserAccount getUserAccount() {
        return this.userAccount;
    }
    
    public final void setUserAccount(final UserAccount userAccount) {
        this.userAccount = userAccount;
    }
    
    public final List<Integer> getSectionIds() {
        return this.sectionIds;
    }
    
    public final void setSectionIds(final List<Integer> sectionIds) {
        this.sectionIds = sectionIds;
    }
    
    public final List<Integer> getWidgetIds() {
        return this.widgetIds;
    }
    
    public final void setWidgetIds(final List<Integer> widgetIds) {
        this.widgetIds = widgetIds;
    }
    
    public final List<Integer> getSubsetMemberIds() {
        return this.subsetMemberIds;
    }
    
    public final void setSubsetMemberIds(final List<Integer> subsetMemberIds) {
        this.subsetMemberIds = subsetMemberIds;
    }
    
}
