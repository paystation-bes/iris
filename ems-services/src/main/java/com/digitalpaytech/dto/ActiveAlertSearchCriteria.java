package com.digitalpaytech.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import com.digitalpaytech.util.WebCoreConstants;

public class ActiveAlertSearchCriteria implements Serializable {
    private static final long serialVersionUID = -5203361543541619416L;
    
    private Integer customerId;
    private Collection<Integer> locationIds;
    private Collection<Integer> routeIds;
    
    private boolean isPaystationAlertSelected;
    private Collection<Integer> alertThresholdTypeIds;
    private Collection<Integer> severityIds;
    private Collection<Integer> deviceTypeIds;
    
    private boolean placed;
    
    private Integer page = 0;
    private Integer itemsPerPage = WebCoreConstants.PAY_STATIONS_PER_PAGE;
    private Integer targetId;
    private Date maxAlertGmt;
    
    //    private String eclusiveOrderBy;    
    //    private String eclusiveOrderDir;
    //    
    //    private boolean isSummary;
    
    public ActiveAlertSearchCriteria() {
        
    }
    
    public final boolean isPlaced() {
        return this.placed;
    }
    
    public final void setPlaced(final boolean placed) {
        this.placed = placed;
    }
    
    public final Severity translateSeverities() {
        return new Severity(this.severityIds);
    }
    
    public final Integer getCustomerId() {
        return this.customerId;
    }
    
    public final void setCustomerId(final Integer customerId) {
        this.customerId = customerId;
    }
    
    public final Collection<Integer> getLocationIds() {
        return this.locationIds;
    }
    
    public final void setLocationIds(final Collection<Integer> locationIds) {
        this.locationIds = locationIds;
    }
    
    public final Collection<Integer> getRouteIds() {
        return this.routeIds;
    }
    
    public final void setRouteIds(final Collection<Integer> routeIds) {
        this.routeIds = routeIds;
    }
    
    public final Collection<Integer> getAlertThresholdTypeIds() {
        return this.alertThresholdTypeIds;
    }
    
    public final boolean getIsPaystationAlertSelected() {
        return this.isPaystationAlertSelected;
    }
    
    public final void setIsPaystationAlertSelected(final boolean isPaystationAlertSelected) {
        this.isPaystationAlertSelected = isPaystationAlertSelected;
    }
    
    public final void setAlertThresholdTypeIds(final Collection<Integer> alertThresholdTypeIds) {
        this.alertThresholdTypeIds = alertThresholdTypeIds;
    }
    
    public final Collection<Integer> getSeverityIds() {
        return this.severityIds;
    }
    
    public final void setSeverityIds(final Collection<Integer> severityIds) {
        this.severityIds = severityIds;
    }
    
    public final Collection<Integer> getDeviceTypeIds() {
        return this.deviceTypeIds;
    }
    
    public final void setDeviceTypeIds(final Collection<Integer> deviceTypeIds) {
        this.deviceTypeIds = deviceTypeIds;
    }
    
    public final Integer getPage() {
        return this.page;
    }
    
    public final void setPage(final Integer page) {
        this.page = page;
    }
    
    public final Integer getItemsPerPage() {
        return this.itemsPerPage;
    }
    
    public final void setItemsPerPage(final Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }
    
    public final Integer getTargetId() {
        return this.targetId;
    }
    
    public final void setTargetId(final Integer targetId) {
        this.targetId = targetId;
    }
    
    public final Date getMaxAlertGmt() {
        return this.maxAlertGmt;
    }
    
    public final void setMaxAlertGmt(final Date maxAlertGmt) {
        this.maxAlertGmt = maxAlertGmt;
    }
    
    public static class Severity implements Serializable {
        private static final long serialVersionUID = 5268860577510359201L;
        
        private boolean critical;
        private boolean major;
        private boolean minor;
        
        public Severity(final Collection<Integer> severityIds) {
            if ((severityIds == null) || (severityIds.size() <= 0) || (severityIds.toArray()[0] == null)) {
                this.critical = true;
                this.major = true;
                this.minor = true;
            } else {
                for (Integer severity : severityIds) {
                    switch (severity) {
                        case WebCoreConstants.SEVERITY_CRITICAL:
                            this.critical = true;
                            break;
                        case WebCoreConstants.SEVERITY_MAJOR:
                            this.major = true;
                            break;
                        case WebCoreConstants.SEVERITY_MINOR:
                            this.minor = true;
                            break;
                        default:
                    }
                }
            }
        }
        
        public final boolean isCritical() {
            return this.critical;
        }
        
        public final boolean isMajor() {
            return this.major;
        }
        
        public final boolean isMinor() {
            return this.minor;
        }
    }
}
