package com.digitalpaytech.dto;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class SearchCustomerResult {
    
    @XStreamImplicit(itemFieldName = "posSearchResult")
    private List<SearchCustomerResultSetting> posSearchResult;
    
    @XStreamImplicit(itemFieldName = "customerSearchResult")
    private List<SearchCustomerResultSetting> customerSearchResult;
    
    @XStreamImplicit(itemFieldName = "posNameSearchResult")
    private List<SearchCustomerResultSetting> posNameSearchResult;
    
    public SearchCustomerResult() {
        this.posSearchResult = new ArrayList<SearchCustomerResultSetting>();
        this.customerSearchResult = new ArrayList<SearchCustomerResultSetting>();
        this.posNameSearchResult = new ArrayList<SearchCustomerResultSetting>();
    }
    
    public final List<SearchCustomerResultSetting> getPosSearchResult() {
        return this.posSearchResult;
    }
    
    public final void addPosSearchResult(final SearchCustomerResultSetting newPosSearchResult) {
        this.posSearchResult.add(newPosSearchResult);
    }
    
    public final List<SearchCustomerResultSetting> getCustomerSearchResult() {
        return this.customerSearchResult;
    }
    
    public final void addCustomerSearchResult(final SearchCustomerResultSetting customerSearchResult) {
        this.customerSearchResult.add(customerSearchResult);
    }
    
    public final List<SearchCustomerResultSetting> getPosNameSearchResult() {
        return this.posNameSearchResult;
    }
    
    public final void addPosNameSearchResult(final SearchCustomerResultSetting posNameSearchResult) {
        this.posNameSearchResult.add(posNameSearchResult);
    }
}
