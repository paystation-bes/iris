package com.digitalpaytech.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListResultDTO<T> implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 4043849244235794023L;
    private String token;
    private Long dataKey;
    
    private List<T> data;
    
    public ListResultDTO() {
        this.data = new ArrayList<T>();
    }
    
    public ListResultDTO(final List<T> data) {
        this.data = data;
    }
    
    public ListResultDTO(final Long dataKey, final List<T> data) {
        this.dataKey = dataKey;
        this.data = data;
    }
    
    public ListResultDTO(final String token, final Long dataKey, final List<T> data) {
        this.token = token;
        this.dataKey = dataKey;
        this.data = data;
    }
    
    public final String getToken() {
        return this.token;
    }
    
    public final void setToken(final String token) {
        this.token = token;
    }
    
    public final Long getDataKey() {
        return this.dataKey;
    }
    
    public final void setDataKey(final Long dataKey) {
        this.dataKey = dataKey;
    }
    
    public final List<T> getData() {
        return this.data;
    }
    
    public final void setData(final List<T> data) {
        this.data = data;
    }
}
