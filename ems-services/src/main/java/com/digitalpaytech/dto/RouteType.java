package com.digitalpaytech.dto;

import com.digitalpaytech.util.WebCoreConstants;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("routeType")
public class RouteType extends SettingsType {
    
    private static final long serialVersionUID = 4954558849788098109L;
    
    private String orgId;
    
    public RouteType() {
        setStatus(WebCoreConstants.ACTIVE);
    }
    
    public final String getOrgId() {
        return this.orgId;
    }
    
    public final void setOrgId(final String orgId) {
        this.orgId = orgId;
    }
}
