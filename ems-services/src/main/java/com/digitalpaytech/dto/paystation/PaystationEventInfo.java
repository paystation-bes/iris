package com.digitalpaytech.dto.paystation;

import java.io.Serializable;
import java.util.Date;

import com.digitalpaytech.util.WebCoreConstants;

public class PaystationEventInfo implements Serializable {
    
    private static final long serialVersionUID = 6298186431423383868L;
    private int pointOfSaleId;
    private Integer eventDeviceTypeId;
    private String deviceName;
    private Integer eventTypeId;
    private String statusName;
    private Integer eventActionTypeId;
    private String actionName;
    private Date alertGmt;
    private String severityName;
    private Integer eventSeverityTypeId;
    private Integer isActive;
    private String alertInfo;
    
    public PaystationEventInfo() {
    
    }
    
    public final int getPointOfSaleId() {
        return this.pointOfSaleId;
    }
    
    public final void setPointOfSaleId(final int pointOfSaleId) {
        this.pointOfSaleId = pointOfSaleId;
    }
    
    public final Integer getEventDeviceTypeId() {
        return this.eventDeviceTypeId;
    }
    
    public final void setEventDeviceTypeId(final Integer eventDeviceTypeId) {
        this.eventDeviceTypeId = eventDeviceTypeId;
    }
    
    public final String getDeviceName() {
        return this.deviceName;
    }
    
    public final void setDeviceName(final String deviceName) {
        this.deviceName = deviceName;
    }
    
    public final int getEventTypeId() {
        return this.eventTypeId;
    }
    
    public final void setEventTypeId(final int eventTypeId) {
        this.eventTypeId = eventTypeId;
    }
    
    public final String getStatusName() {
        return this.statusName;
    }
    
    public final void setStatusName(final String statusName) {
        this.statusName = statusName;
    }
    
    public final Integer getEventActionTypeId() {
        return this.eventActionTypeId;
    }
    
    public final void setEventActionTypeId(final Integer eventActionTypeId) {
        this.eventActionTypeId = eventActionTypeId;
    }
    
    public final String getActionName() {
        return this.actionName;
    }
    
    public final void setActionName(final String actionName) {
        this.actionName = actionName;
    }
    
    public final Date getAlertGmt() {
        return this.alertGmt;
    }
    
    public final void setAlertGmt(final Date alertGmt) {
        this.alertGmt = alertGmt;
    }
    
    public final String getSeverityName() {
        return this.severityName;
    }
    
    public final void setSeverityName(final String severityName) {
        this.severityName = severityName;
    }
    
    public final String getState() {
        if ((this.eventDeviceTypeId == WebCoreConstants.EVENT_DEVICE_TYPE_PRINTER
             || this.eventDeviceTypeId == WebCoreConstants.EVENT_DEVICE_TYPE_CARD_READER)
            && this.eventTypeId == WebCoreConstants.EVENT_STATUS_TYPE_PRESENT
            && this.eventActionTypeId == WebCoreConstants.EVENT_ACTION_TYPE_CLEAR) {
            return WebCoreConstants.EVENT_ACTION_TYPE_ALERTED_STRING;
        } else if ((this.eventDeviceTypeId == WebCoreConstants.EVENT_DEVICE_TYPE_PRINTER
                    || this.eventDeviceTypeId == WebCoreConstants.EVENT_DEVICE_TYPE_CARD_READER)
                   && this.eventTypeId == WebCoreConstants.EVENT_STATUS_TYPE_PRESENT
                   && this.eventActionTypeId == WebCoreConstants.EVENT_ACTION_TYPE_SET) {
            return WebCoreConstants.EVENT_ACTION_TYPE_CLEARED_STRING;
        }
        return this.actionName;
    }
    
    public final String getEvent() {
        return this.deviceName + " - " + this.statusName;
    }
    
    public final Integer getEventSeverityTypeId() {
        return this.eventSeverityTypeId;
    }
    
    public final void setEventSeverityTypeId(final Integer eventSeverityTypeId) {
        this.eventSeverityTypeId = eventSeverityTypeId;
    }

    public final Integer getIsActive() {
        return this.isActive;
    }

    public final void setIsActive(final Integer isActive) {
        this.isActive = isActive;
    }

    public final String getAlertInfo() {
        return this.alertInfo;
    }

    public final void setAlertInfo(final String alertInfo) {
        this.alertInfo = alertInfo;
    }
    
}
