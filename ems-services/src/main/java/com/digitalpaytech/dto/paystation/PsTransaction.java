package com.digitalpaytech.dto.paystation;

import java.util.ArrayList;
import java.util.List;

/**
 * Used by Apache commons Digester.
 */

public class PsTransaction {
    private String version;
    private List<TransactionDto> transactionDtos;
    
    public PsTransaction() {
        transactionDtos = new ArrayList<TransactionDto>();
    }
    
    public List<TransactionDto> getTransactionDtos() {
        return transactionDtos;
    }

    public void setTransactionDtos(List<TransactionDto> transactionDtos) {
        this.transactionDtos = transactionDtos;
    }

    public void addTransactionDto(TransactionDto transactionDto) {
        transactionDtos.add(transactionDto);
    }
    
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}