package com.digitalpaytech.dto.paystation;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.digitalpaytech.annotation.StorageType;
import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.annotation.StoryLookup;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.util.DateUtil;

@StoryAlias(EventData.ALIAS_EVENT_DATA)
public class EventData implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 7631759302490255067L;
    public static final String ALIAS_EVENT_DATA = "Event Data";
    private String severity;
    private int pointOfSaleId;
    private String type;
    private String action;
    private String information;
    private Date timeStamp;
    
    public EventData() {
    }
    
    public EventData(EventData event2) {
        this.pointOfSaleId = event2.getPointOfSaleId();
        this.severity = event2.getSeverity();
    }
    
    @StoryAlias("severity")
    public String getSeverity() {
        return severity;
    }
    
    public void setSeverity(String severity) {
        this.severity = severity;
    }
    
    @StoryAlias("type")
    public String getType() {
        return type;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
    @StoryAlias("Action")
    public String getAction() {
        return action;
    }
    
    public void setAction(String action) {
        this.action = action;
    }
    
    @StoryAlias("Information")
    public String getInformation() {
        return information;
    }
    
    public void setInformation(String information) {
        this.information = information;
    }
    
    @StoryAlias("Timestamp")
    public Date getTimeStamp() {
        return timeStamp;
    }
    
    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
    
    @StoryAlias("pay station")
    @StoryLookup(type = PointOfSale.ALIAS, searchAttribute = PointOfSale.ALIAS_NAME, returnAttribute = PointOfSale.ALIAS_ID, storage = StorageType.DB)
    public int getPointOfSaleId() {
        return pointOfSaleId;
    }
    
    public void setPointOfSaleId(int pointOfSaleId) {
        this.pointOfSaleId = pointOfSaleId;
    }
    
    public String toString() {
        StringBuilder str = new StringBuilder("{");
        str.append("pointOfSaleId: " + getPointOfSaleId());
        str.append(", timeStamp: ");
        str.append(DateUtil.createDateString(new SimpleDateFormat(DateUtil.DATABASE_FORMAT), getTimeStamp()));
        str.append(", severity: " + getSeverity());
        str.append(", type: " + getType());
        str.append(", action: " + getAction());
        str.append(", information: " + getInformation());
        str.append('}');
        return (str.toString());
    }
}
