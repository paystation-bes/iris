package com.digitalpaytech.dto.paystation;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class CardTransaction {
    
    private int amount;
    private String processedDate;
    private String processorTransactionId;
    private String referenceNumber;
    private String authorizationNumber;
    private String last4Digits;
    private String cardType;
    private String transactionType;
    private String terminalToken;
    private String processorType;    
    private CardEaseData processorSpecificData;
    
    public CardTransaction() {
        //Empty Constructor
    }
    
    public final int getAmount() {
        return this.amount;
    }
    
    public final void setAmount(final int amount) {
        this.amount = amount;
    }
    
    public final String getProcessedDate() {
        return this.processedDate;
    }
    
    public final void setProcessedDate(final String processedDate) {
        this.processedDate = processedDate;
    }
    
    public final String getProcessorTransactionId() {
        return this.processorTransactionId;
    }
    
    public final void setProcessorTransactionId(final String processorTransactionId) {
        this.processorTransactionId = processorTransactionId;
    }
    
    public final String getReferenceNumber() {
        return this.referenceNumber;
    }
    
    public final void setReferenceNumber(final String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }
    
    public final String getAuthorizationNumber() {
        return this.authorizationNumber;
    }
    
    public final void setAuthorizationNumber(final String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }
    
    public final String getLast4Digits() {
        return this.last4Digits;
    }
    
    public final void setLast4Digits(final String last4Digits) {
        this.last4Digits = last4Digits;
    }
    
    public final String getCardType() {
        return this.cardType;
    }
    
    public final void setCardType(final String cardType) {
        this.cardType = cardType;
    }
    
    public final String getTransactionType() {
        return this.transactionType;
    }
    
    public final void setTransactionType(final String transactionType) {
        this.transactionType = transactionType;
    }
    
    public final String getProcessorType() {
        return this.processorType;
    }
    
    public final void setProcessorType(final String processorType) {
        this.processorType = processorType;
    }
    
    public final CardEaseData getProcessorSpecificData() {
        return this.processorSpecificData;
    }
    
    public final void setProcessorSpecificData(final CardEaseData processorSpecificData) {
        this.processorSpecificData = processorSpecificData;
    }
    
    public String getTerminalToken() {
        return terminalToken;
    }
    
    public void setTerminalToken(String terminalToken) {
        this.terminalToken = terminalToken;
    }
}
