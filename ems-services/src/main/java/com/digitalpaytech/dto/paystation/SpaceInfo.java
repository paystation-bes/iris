package com.digitalpaytech.dto.paystation;

import java.util.Date;

public class SpaceInfo {
	private String paystationSettingName = null;
	private String addTimeNumber = null;
	private String timeZone = null;
	private Date startDate = null;
	private Date endDate = null;
    private String startDateLocal = null;
    private String endDateLocal= null;	
	private String stallNumber = null;
	private int totalTimePurchased = 0;

	public SpaceInfo() {
		// Empty constructor
	}

	public SpaceInfo(String paystationSettingName, String stallNumber, String timeZone,
			Date startDate, Date endDate) {
		this.paystationSettingName = paystationSettingName;
		this.stallNumber = stallNumber;
		this.timeZone = timeZone;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public String getPaystationSettingName() {
		return paystationSettingName;
	}

	public void setPaystationSettingName(String paystationSettingName) {
		this.paystationSettingName = paystationSettingName;
	}

	public String getAddTimeNumber() {
		return addTimeNumber;
	}

	public void setAddTimeNumber(String addTimeNumber) {
		this.addTimeNumber = addTimeNumber;
	}

	public void setAddTimeNumber(int value) {
		this.addTimeNumber = Integer.toString(value);
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getTotalTimePurchased() {
		return totalTimePurchased;
	}

	public void setTotalTimePurchased(int i) {
		this.totalTimePurchased = i;
	}

	public String getStallNumber() {
		return stallNumber;
	}

	public void setStallNumber(String value) {
		this.stallNumber = value;
	}

	public void setStallNumber(int value) {
		this.stallNumber = Integer.toString(value);
	}

	public String getStartDateLocal() {
		return startDateLocal;
	}

	public void setStartDateLocal(String startDateLocal) {
		this.startDateLocal = startDateLocal;
	}

	public String getEndDateLocal() {
		return endDateLocal;
	}

	public void setEndDateLocal(String endDateLocal) {
		this.endDateLocal = endDateLocal;
	}

}