package com.digitalpaytech.dto.paystation;

import com.digitalpaytech.annotation.StoryAlias;

@StoryAlias (value = EmvTelemetryDto.ALIAS)
public class EmvTelemetryDto {
    public static final String ALIAS = "EMV Telemetry data";
    
    private static final String ALIAS_TYPE = "Type";
    private static final String ALIAS_REFERENCE_COUNTER = "Reference Counter";
    private static final String ALIAS_TIME_STAMP = "Time Stamp";
    private static final String ALIAS_SERIAL_NUMBER = "Serial Number";
    private static final String ALIAS_TELEMETRY = "Telemetry";
    
    private String type;
    private String referenceCounter;
    private String timeStamp = "";
    private String paystationCommAddress = "";
    private String telemetry;

    public EmvTelemetryDto() {
        
    }
    
    public EmvTelemetryDto(final EmvTelemetryDto other) {
        this.type = other.type;
        this.referenceCounter = other.referenceCounter;
        this.timeStamp = other.timeStamp;
        this.paystationCommAddress = other.paystationCommAddress;
        this.telemetry = other.telemetry;
    }
    
    @StoryAlias (value = ALIAS_TYPE)
    public final String getType() {
        return this.type;
    }
    
    public final void setType(final String type) {
        this.type = type;
    }
    
    @StoryAlias (value = ALIAS_REFERENCE_COUNTER)
    public final String getReferenceCounter() {
        return this.referenceCounter;
    }
    
    public final void setReferenceCounter(final String referenceCounter) {
        this.referenceCounter = referenceCounter;
    }
    
    @StoryAlias (value = ALIAS_TIME_STAMP)
    public final String getTimeStamp() {
        return this.timeStamp;
    }
    
    public final void setTimeStamp(final String timeStamp) {
        this.timeStamp = timeStamp;
    }
    
    @StoryAlias (value = ALIAS_SERIAL_NUMBER)
    public final String getPaystationCommAddress() {
        return this.paystationCommAddress;
    }
    
    public final void setPaystationCommAddress(final String paystationCommAddress) {
        this.paystationCommAddress = paystationCommAddress;
    }
    
    @StoryAlias (value = ALIAS_TELEMETRY)
    public final String getTelemetry() {
        return this.telemetry;
    }
    
    public final void setTelemetry(final String telemetry) {
        this.telemetry = telemetry;
    }
    
}
