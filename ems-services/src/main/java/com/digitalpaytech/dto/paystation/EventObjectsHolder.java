package com.digitalpaytech.dto.paystation;

import java.util.Date;

import com.digitalpaytech.domain.EventActionType;
import com.digitalpaytech.domain.EventDeviceType;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.EventStatusType;

public class EventObjectsHolder {
    private EventDeviceType eventDeviceType;
    private EventActionType eventActionType;
    private EventStatusType eventStatusType; 
    private EventSeverityType eventSeverityType;
    private Date timeStamp;
    private boolean isCleared;
    private boolean hasAlertsSubscription;
    
    public EventObjectsHolder() {
    }

    public EventObjectsHolder(EventDeviceType eventDeviceType, 
                              EventActionType eventActionType, 
                              EventStatusType eventStatusType, 
                              EventSeverityType eventSeverityType, 
                              Date timeStamp, 
                              boolean hasAlertsSubscription) {
        this.eventDeviceType = eventDeviceType;
        this.eventActionType = eventActionType;
        this.eventStatusType = eventStatusType;
        this.eventSeverityType = eventSeverityType;
        this.timeStamp = timeStamp;
        this.hasAlertsSubscription = hasAlertsSubscription;
    }
    
    public EventObjectsHolder(EventDeviceType eventDeviceType, EventActionType eventActionType, EventStatusType eventStatusType,  
                              EventSeverityType eventSeverityType, Date timeStamp, boolean isCleared, boolean hasAlertsSubscription) {
        this(eventDeviceType, eventActionType, eventStatusType, eventSeverityType, timeStamp, hasAlertsSubscription);
        this.isCleared = isCleared;
    }

    public EventDeviceType getEventDeviceType() {
        return eventDeviceType;
    }
    public void setEventDeviceType(EventDeviceType eventDeviceType) {
        this.eventDeviceType = eventDeviceType;
    }
    public EventActionType getEventActionType() {
        return eventActionType;
    }
    public void setEventActionType(EventActionType eventActionType) {
        this.eventActionType = eventActionType;
    }
    public EventStatusType getEventStatusType() {
        return eventStatusType;
    }
    public void setEventStatusType(EventStatusType eventStatusType) {
        this.eventStatusType = eventStatusType;
    }
    public EventSeverityType getEventSeverityType() {
        return eventSeverityType;
    }
    public void setEventSeverityType(EventSeverityType eventSeverityType) {
        this.eventSeverityType = eventSeverityType;
    }
    public boolean isCleared() {
        return isCleared;
    }
    public void setCleared(boolean isCleared) {
        this.isCleared = isCleared;
    }
    public Date getTimeStamp() {
        return timeStamp;
    }
    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
    public boolean hasAlertsSubscription() {
        return hasAlertsSubscription;
    }
    public void setHasAlertsSubscription(boolean hasAlertsSubscription) {
        this.hasAlertsSubscription = hasAlertsSubscription;
    }
}
