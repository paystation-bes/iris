package com.digitalpaytech.dto.paystation;

import java.io.Serializable;
import java.util.Date;

import com.digitalpaytech.util.StableDateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class LicensePlateMessage implements Serializable {
    
    private static final long serialVersionUID = 7658510943530177253L;
    
    private String licensePlate;
    private Integer customerId;
    private String locationName;
    private boolean isPreferred;
    private boolean isPreferredByLocation;
    private boolean isLimited;
    private boolean isLimitedByLocation;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StableDateUtil.UTC_TRANSACTION_DATE_TIME_FORMAT, timezone = StableDateUtil.UTC)
    private Date purchaseUTC;
    private String localDate;
    
    public LicensePlateMessage() {
        super();
    }
    
    public LicensePlateMessage(final String licensePlate, final Integer customerId, final String locationName, final boolean isPreferred,
            final boolean isPreferredByLocation, final boolean isLimited, final boolean isLimitedByLocation, final Date purchaseUTC,
            final String localDate) {
        super();
        this.licensePlate = licensePlate;
        this.customerId = customerId;
        this.locationName = locationName;
        this.isPreferred = isPreferred;
        this.isPreferredByLocation = isPreferredByLocation;
        this.isLimited = isLimited;
        this.isLimitedByLocation = isLimitedByLocation;
        this.purchaseUTC = purchaseUTC;
        this.localDate = localDate;
    }
    
    public final String getLicensePlate() {
        return this.licensePlate;
    }
    
    public final void setLicensePlate(final String licensePlate) {
        this.licensePlate = licensePlate;
    }
    
    public final Integer getCustomerId() {
        return this.customerId;
    }
    
    public final void setCustomerId(final Integer customerId) {
        this.customerId = customerId;
    }
    
    public final String getLocationName() {
        return this.locationName;
    }
    
    public final void setLocationName(final String locationName) {
        this.locationName = locationName;
    }
    
    public final boolean getIsPreferred() {
        return this.isPreferred;
    }
    
    public final void setIsPreferred(final boolean isPreferred) {
        this.isPreferred = isPreferred;
    }
    
    public final boolean getIsPreferredByLocation() {
        return this.isPreferredByLocation;
    }
    
    public final void setIsPreferredByLocation(final boolean isPreferredByLocation) {
        this.isPreferredByLocation = isPreferredByLocation;
    }
    
    public final boolean getIsLimited() {
        return this.isLimited;
    }
    
    public final void setIsLimited(final boolean isLimited) {
        this.isLimited = isLimited;
    }
    
    public final boolean getIsLimitedByLocation() {
        return this.isLimitedByLocation;
    }
    
    public final void setIsLimitedByLocation(final boolean isLimitedByLocation) {
        this.isLimitedByLocation = isLimitedByLocation;
    }
    
    public final Date getPurchaseUTC() {
        return this.purchaseUTC;
    }
    
    public final void setPurchaseUTC(final Date purchaseUTC) {
        this.purchaseUTC = purchaseUTC;
    }
    
    public final String getLocalDate() {
        return this.localDate;
    }
    
    public final void setLocalDate(final String localDate) {
        this.localDate = localDate;
    }
    
    @Override
    public String toString() {
        return "LicensePlateMessage [licensePlate=" + licensePlate + ", customerId=" + customerId + ", locationName=" + locationName
               + ", isPreferred=" + isPreferred + ", isPreferredByLocation=" + isPreferredByLocation + ", isLimited=" + isLimited
               + ", isLimitedByLocation=" + isLimitedByLocation + ", purchaseUTC=" + purchaseUTC + ", localDate=" + localDate + "]";
    }
    
}
