package com.digitalpaytech.dto.paystation;

import java.util.Date;

public class TransactionInfoDto {
    private String serialNumber;
    private String paystationName;
    private String settingName;
    private String paystationType;
    private Integer locationId;
    private String locationName;
    private Integer ticketNumber;
    private Integer spaceNumber;
    private Date purchaseGMT;
    private Date permitExpireGMT;
    private Byte paymentType;
    private Byte transactionType;
    private String cardType;
    private Integer last4DigitsOfCardNumber;
    private String authorizationNumber;
    private Date processingDate;
    private Integer processorTransactionType;
    private String referenceNumber;
    private String merchantAccountName;
    private Integer numRetries;
    private String processorTransactionId;
    private String couponNumber;
    private Byte percentDiscount;
    private Float dollarDiscountAmount;
    private Float excessPaymentAmount;
    private Float chargedAmount;
    private Float cashPaidAmount;
    private Float cardPaidAmount;
    private Float changeDispensedAmount;
    private Byte isRefundSlip;
    private String customCardData;
    private String customCardType;
    private Integer rateId;
    private String rateName;
    private Float rateAmount;
    private String licencePlateNumber;
    private String groupName;
    
    public final String getSerialNumber() {
        return this.serialNumber;
    }
    
    public final void setSerialNumber(final String serialNumber) {
        this.serialNumber = serialNumber;
    }
    
    public final String getPaystationName() {
        return this.paystationName;
    }
    
    public final void setPaystationName(final String paystationName) {
        this.paystationName = paystationName;
    }
    
    public final String getSettingName() {
        return this.settingName;
    }
    
    public final void setSettingName(final String settingName) {
        this.settingName = settingName;
    }
    
    public final String getPaystationType() {
        return this.paystationType;
    }
    
    public final void setPaystationType(final String paystationType) {
        this.paystationType = paystationType;
    }
    
    public final Integer getLocationId() {
        return this.locationId;
    }
    
    public final void setLocationId(final Integer locationId) {
        this.locationId = locationId;
    }
    
    public final String getLocationName() {
        return this.locationName;
    }
    
    public final void setLocationName(final String locationName) {
        this.locationName = locationName;
    }
    
    public final Integer getTicketNumber() {
        return this.ticketNumber;
    }
    
    public final void setTicketNumber(final Integer ticketNumber) {
        this.ticketNumber = ticketNumber;
    }
    
    public final Integer getSpaceNumber() {
        return this.spaceNumber;
    }
    
    public final void setSpaceNumber(final Integer spaceNumber) {
        this.spaceNumber = spaceNumber;
    }
    
    public final Date getPurchaseGMT() {
        return this.purchaseGMT;
    }
    
    public final void setPurchaseGMT(final Date purchaseGMT) {
        this.purchaseGMT = purchaseGMT;
    }
    
    public final Date getPermitExpireGMT() {
        return this.permitExpireGMT;
    }
    
    public final void setPermitExpireGMT(final Date permitExpireGMT) {
        this.permitExpireGMT = permitExpireGMT;
    }
    
    public final Byte getPaymentType() {
        return this.paymentType;
    }
    
    public final void setPaymentType(final Byte paymentType) {
        this.paymentType = paymentType;
    }
    
    public final Byte getTransactionType() {
        return this.transactionType;
    }
    
    public final void setTransactionType(final Byte transactionType) {
        this.transactionType = transactionType;
    }
    
    public final String getCardType() {
        return this.cardType;
    }
    
    public final void setCardType(final String cardType) {
        this.cardType = cardType;
    }
    
    public final Integer getLast4DigitsOfCardNumber() {
        return this.last4DigitsOfCardNumber;
    }
    
    public final void setLast4DigitsOfCardNumber(final Integer last4DigitsOfCardNumber) {
        this.last4DigitsOfCardNumber = last4DigitsOfCardNumber;
    }
    
    public final String getAuthorizationNumber() {
        return this.authorizationNumber;
    }
    
    public final void setAuthorizationNumber(final String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }
    
    public final Date getProcessingDate() {
        return this.processingDate;
    }
    
    public final void setProcessingDate(final Date processingDate) {
        this.processingDate = processingDate;
    }
    
    public final Integer getProcessorTransactionType() {
        return this.processorTransactionType;
    }
    
    public final void setProcessorTransactionType(final Integer processorTransactionType) {
        this.processorTransactionType = processorTransactionType;
    }
    
    public final String getReferenceNumber() {
        return this.referenceNumber;
    }
    
    public final void setReferenceNumber(final String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }
    
    public final String getMerchantAccountName() {
        return this.merchantAccountName;
    }
    
    public final void setMerchantAccountName(final String merchantAccountName) {
        this.merchantAccountName = merchantAccountName;
    }
    
    public final Integer getNumRetries() {
        return this.numRetries;
    }
    
    public final void setNumRetries(final Integer numRetries) {
        this.numRetries = numRetries;
    }
    
    public final String getProcessorTransactionId() {
        return this.processorTransactionId;
    }
    
    public final void setProcessorTransactionId(final String processorTransactionId) {
        this.processorTransactionId = processorTransactionId;
    }
    
    public final String getCouponNumber() {
        return this.couponNumber;
    }
    
    public final void setCouponNumber(final String couponNumber) {
        this.couponNumber = couponNumber;
    }
    
    public final Byte getPercentDiscount() {
        return this.percentDiscount;
    }
    
    public final void setPercentDiscount(final Byte percentDiscount) {
        this.percentDiscount = percentDiscount;
    }
    
    public final Float getDollarDiscountAmount() {
        return this.dollarDiscountAmount;
    }
    
    public final void setDollarDiscountAmount(final Float dollarDiscountAmount) {
        this.dollarDiscountAmount = dollarDiscountAmount;
    }
    
    public final Float getExcessPaymentAmount() {
        return this.excessPaymentAmount;
    }
    
    public final void setExcessPaymentAmount(final Float excessPaymentAmount) {
        this.excessPaymentAmount = excessPaymentAmount;
    }
    
    public final Float getChargedAmount() {
        return this.chargedAmount;
    }
    
    public final void setChargedAmount(final Float chargedAmount) {
        this.chargedAmount = chargedAmount;
    }
    
    public final Float getCashPaidAmount() {
        return this.cashPaidAmount;
    }
    
    public final void setCashPaidAmount(final Float cashPaidAmount) {
        this.cashPaidAmount = cashPaidAmount;
    }
    
    public final Float getCardPaidAmount() {
        return this.cardPaidAmount;
    }
    
    public final void setCardPaidAmount(final Float cardPaidAmount) {
        this.cardPaidAmount = cardPaidAmount;
    }
    
    public final Float getChangeDispensedAmount() {
        return this.changeDispensedAmount;
    }
    
    public final void setChangeDispensedAmount(final Float changeDispensedAmount) {
        this.changeDispensedAmount = changeDispensedAmount;
    }
    
    public final Byte isRefundSlip() {
        return this.isRefundSlip;
    }
    
    public final void setIsRefundSlip(final Byte isRefundSlip) {
        this.isRefundSlip = isRefundSlip;
    }
    
    public final String getCustomCardData() {
        return this.customCardData;
    }
    
    public final void setCustomCardData(final String customCardData) {
        this.customCardData = customCardData;
    }
    
    public final String getCustomCardType() {
        return this.customCardType;
    }
    
    public final void setCustomCardType(final String customCardType) {
        this.customCardType = customCardType;
    }
    
    public final Integer getRateId() {
        return this.rateId;
    }
    
    public final void setRateId(final Integer rateId) {
        this.rateId = rateId;
    }
    
    public final String getRateName() {
        return this.rateName;
    }
    
    public final void setRateName(final String rateName) {
        this.rateName = rateName;
    }
    
    public final Float getRateAmount() {
        return this.rateAmount;
    }
    
    public final void setRateAmount(final Float rateAmount) {
        this.rateAmount = rateAmount;
    }
    
    public final String getLicencePlateNumber() {
        return this.licencePlateNumber;
    }
    
    public final void setLicencePlateNumber(final String licencePlateNumber) {
        this.licencePlateNumber = licencePlateNumber;
    }
    
    public final String getGroupName() {
        return this.groupName;
    }
    
    public final void setGroupName(final String groupName) {
        this.groupName = groupName;
    }
    
}
