package com.digitalpaytech.dto.paystation;

import java.io.Serializable;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.dto.EmbeddedTxObject;
import com.digitalpaytech.dto.rest.paystation.PaystationTransaction;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.xml.XMLUtil;

/**
 * Confluence - PS2 Transaction: http://confluence:8080/display/EMS/PS2+Transaction
 * Confluence - PS2 Transaction Request: http://confluence:8080/display/EMS/Transaction+Request
 * 
 * IMPORTANT - make sure properties match with ems-ui/src/main/resources/transactionRules.xml.
 */

@StoryAlias(value = TransactionDto.ALIAS)
@SuppressWarnings({ "PMD.TooManyFields", "PMD.ExcessivePublicCount", "PMD.GodClass" })
public class TransactionDto implements Serializable {
    public static final String ALIAS = "transaction data";
    public static final String ALIAS_CARD_AUTHORIZATION_ID = "card authorization id";
    public static final String ALIAS_AUTHORIZATION_NUMBER = "authorization number";
    public static final String ALIAS_EMS_PREAUTH_ID = "ems preauth id";
    public static final String ALIAS_CARD_EASE_DATA = "card ease data";
    public static final String ALIAS_PAID_BY_CHIP = "paid by chip";
    public static final String ALIAS_TRANS_DATE = "transaction date";
    public static final String ALIAS_PREFERRED_RATE = "preferred rate";
    public static final String ALIAS_LIMITED_RATE = "limited rate";
    public static final String ALIAS_CPS_REPONSE = "cps response";
    public static final String ALIAS_TAX_THREE_RATE = "tax three rate";
    public static final String ALIAS_TAX_THREE_VALUE = "tax three value";
    public static final String ALIAS_TAX_THREE_NAME = "tax three name";
    private static final String ALIAS_TAX_ONE_RATE = "tax one rate";
    private static final String ALIAS_TAX_ONE_VALUE = "tax one value";
    private static final String ALIAS_TAX_ONE_NAME = "tax one name";
    
    private static final long serialVersionUID = -2835783133911246186L;
 
    private String paystationCommAddress = StandardConstants.STRING_EMPTY_STRING;
    private String number = StandardConstants.STRING_ZERO;
    private String lotNumber = StandardConstants.STRING_EMPTY_STRING;
    private String addTimeNum = StandardConstants.STRING_ZERO;
    private String licensePlateNo;
    private String stallNumber = StandardConstants.STRING_ZERO;
    private String type;
    private String purchasedDate = StandardConstants.STRING_EMPTY_STRING;
    private String transDateTime = StandardConstants.STRING_EMPTY_STRING;
    private String parkingTimePurchased = StandardConstants.STRING_EMPTY_STRING;
    private String expiryDate = StandardConstants.STRING_EMPTY_STRING;
    private String originalAmount = StandardConstants.STRING_ZERO;
    private String chargedAmount = StandardConstants.STRING_ZERO;
    private String couponNumber;
    private String couponPercent;
    private String couponAmount;
    private String cashPaid = StandardConstants.STRING_ZERO;
    private String cardPaid = StandardConstants.STRING_ZERO;
    private String cardData;
    private String tax1Rate;
    private String tax1Value;
    private String tax1Name;
    private String tax2Rate;
    private String tax2Value;
    private String tax2Name;
    private String tax3Rate;
    private String tax3Value;
    private String tax3Name;
    private String rateName = StandardConstants.STRING_EMPTY_STRING;
    private String rateID = StandardConstants.STRING_EMPTY_STRING;
    private String rateValue = StandardConstants.STRING_ZERO;
    private String taxType = StandardConstants.STRING_EMPTY_STRING;
    private String numberBillsAccepted = StandardConstants.STRING_ZERO;
    private String numberCoinsAccepted = StandardConstants.STRING_ZERO;
    private String hopperDispensed = StandardConstants.STRING_EMPTY_STRING;
    private String hopper1CoinsDispensed = StandardConstants.STRING_EMPTY_STRING;
    private String hopper2CoinsDispensed = StandardConstants.STRING_EMPTY_STRING;
    private String cardAuthorizationId;
    private String changeDispensed = StandardConstants.STRING_ZERO;
    private String isRefundSlip;
    private String emsPreAuthId;
    private String smartCardPaid = StandardConstants.STRING_ZERO;
    private String smartCardData = StandardConstants.STRING_EMPTY_STRING;
    private String smartCardType;
    private String smartCardMerchantInfo;
    private String smartCardSettlementInfo;
    private String mobileNumber = StandardConstants.STRING_EMPTY_STRING;
    private String creditCardData = StandardConstants.STRING_EMPTY_STRING;
    private String paidByRFID;
    private String cardAmountOther = StandardConstants.STRING_ZERO;
    private String cardAmountSC = StandardConstants.STRING_ZERO;
    private String cardAmountVisa = StandardConstants.STRING_ZERO;
    private String cardAmountMC = StandardConstants.STRING_ZERO;
    private String cardAmountAmex = StandardConstants.STRING_ZERO;
    private String cardAmountDiners = StandardConstants.STRING_ZERO;
    private String cardAmountDiscover = StandardConstants.STRING_EMPTY_STRING;
    private String coinCol;
    private String billCol;
    private String authorizationNumber;
    private String emvTransactionData;
    private String paidByChipCard;
    
    // PS1
    private String excessPayment;
    
    // PS will still send with these elements but value will be 0.
    private String numberHopper1CoinsDispensed;
    private String numberHopper2CoinsDispensed;
    
    // Paystation version
    private String version;
    
    private String cardEaseData;
    private String cpsResponse;
    private EmbeddedTxObject embeddedTxObject;
    
    private Boolean preferredRate;
    private Boolean limitedRate;
    
    public TransactionDto() {
        super();
    }
    
    /*
     * If you ever need this implementation for other POJO. Simply use this replace pattern on your favourite editor...
     * Find -> ^([^=;]*)(.*)?;
     * Replace -> this.\1= other.\1;
     */
    public TransactionDto(final TransactionDto other) {
        this.paystationCommAddress = other.paystationCommAddress;
        this.number = other.number;
        this.lotNumber = other.lotNumber;
        this.addTimeNum = other.addTimeNum;
        this.licensePlateNo = other.licensePlateNo;
        this.stallNumber = other.stallNumber;
        this.type = other.type;
        this.purchasedDate = other.purchasedDate;
        this.transDateTime = other.transDateTime;
        this.parkingTimePurchased = other.parkingTimePurchased;
        this.expiryDate = other.expiryDate;
        this.originalAmount = other.originalAmount;
        this.chargedAmount = other.chargedAmount;
        this.couponNumber = other.couponNumber;
        this.couponPercent = other.couponPercent;
        this.couponAmount = other.couponAmount;
        this.cashPaid = other.cashPaid;
        this.cardPaid = other.cardPaid;
        this.cardData = other.cardData;
        this.tax1Rate = other.tax1Rate;
        this.tax1Value = other.tax1Value;
        this.tax1Name = other.tax1Name;
        this.tax2Rate = other.tax2Rate;
        this.tax2Value = other.tax2Value;
        this.tax2Name = other.tax2Name;
        this.rateName = other.rateName;
        this.rateID = other.rateID;
        this.rateValue = other.rateValue;
        this.taxType = other.taxType;
        this.numberBillsAccepted = other.numberBillsAccepted;
        this.numberCoinsAccepted = other.numberCoinsAccepted;
        this.hopperDispensed = other.hopperDispensed;
        this.hopper1CoinsDispensed = other.hopper1CoinsDispensed;
        this.hopper2CoinsDispensed = other.hopper2CoinsDispensed;
        this.cardAuthorizationId = other.cardAuthorizationId;
        this.changeDispensed = other.changeDispensed;
        this.isRefundSlip = other.isRefundSlip;
        this.emsPreAuthId = other.emsPreAuthId;
        this.smartCardPaid = other.smartCardPaid;
        this.smartCardData = other.smartCardData;
        this.smartCardType = other.smartCardType;
        this.smartCardMerchantInfo = other.smartCardMerchantInfo;
        this.smartCardSettlementInfo = other.smartCardSettlementInfo;
        this.mobileNumber = other.mobileNumber;
        this.creditCardData = other.creditCardData;
        this.paidByRFID = other.paidByRFID;
        this.cardAmountOther = other.cardAmountOther;
        this.cardAmountSC = other.cardAmountSC;
        this.cardAmountVisa = other.cardAmountVisa;
        this.cardAmountMC = other.cardAmountMC;
        this.cardAmountAmex = other.cardAmountAmex;
        this.cardAmountDiners = other.cardAmountDiners;
        this.cardAmountDiscover = other.cardAmountDiscover;
        this.coinCol = other.coinCol;
        this.billCol = other.billCol;
        this.excessPayment = other.excessPayment;
        this.numberHopper1CoinsDispensed = other.numberHopper1CoinsDispensed;
        this.numberHopper2CoinsDispensed = other.numberHopper2CoinsDispensed;
        this.paidByChipCard = other.paidByChipCard;
        this.cardEaseData = other.cardEaseData;
        this.preferredRate = other.preferredRate;
        this.limitedRate = other.limitedRate;
    }
    
    /*
     * If you ever need this implementation for other POJO. Simply use this replace pattern on your favourite editor...
     * Find -> ^([^=;]*)(.*)?;
     * Replace -> this.\1= other.\1;
     */
    public TransactionDto(final PaystationTransaction other) {
        this.paystationCommAddress = other.getPaystationCommAddress();
        this.number = other.getNumber();
        this.lotNumber = other.getLotNumber();
        this.addTimeNum = other.getAddTimeNum();
        this.licensePlateNo = other.getLicensePlateNo();
        this.stallNumber = other.getStallNumber();
        this.type = other.getType();
        this.purchasedDate = other.getPurchasedDate();
        this.transDateTime = other.getTimestamp();
        this.parkingTimePurchased = other.getParkingTimePurchased();
        this.expiryDate = other.getExpiryDate();
        this.originalAmount = other.getOriginalAmount();
        this.chargedAmount = other.getChargedAmount();
        this.couponNumber = other.getCouponNumber();
        this.cashPaid = other.getCashPaid();
        this.cardPaid = other.getCardPaid();
        this.cardData = other.getCardData();
        this.tax1Rate = other.getTax1Rate();
        this.tax1Value = other.getTax1Value();
        this.tax1Name = other.getTax1Name();
        this.tax2Rate = other.getTax2Rate();
        this.tax2Value = other.getTax2Value();
        this.tax2Name = other.getTax2Name();
        this.rateName = other.getRateName();
        this.rateID = other.getRateId();
        this.rateValue = other.getRateValue();
        this.taxType = other.getTaxType();
        this.numberBillsAccepted = other.getNumberBillsAccepted();
        this.numberCoinsAccepted = other.getNumberCoinsAccepted();
        this.hopperDispensed = other.getHopperDispensed();
        this.changeDispensed = other.getChangeDispensed();
        this.isRefundSlip = other.getIsRefundSlip();
        this.smartCardPaid = other.getSmartCardPaid();
        this.smartCardData = other.getSmartCardData();
        this.smartCardType = other.getSmartCardType();
        this.smartCardMerchantInfo = other.getSmartCardMerchantInfo();
        this.smartCardSettlementInfo = other.getSmartCardSettlementInfo();
        this.mobileNumber = other.getMobileNumber();
        this.creditCardData = other.getCreditCardData();
        this.paidByRFID = other.getPaidByRFID();
        this.cardAmountOther = other.getCardAmountOther();
        this.cardAmountSC = other.getCardAmountSC();
        this.cardAmountVisa = other.getCardAmountVisa();
        this.cardAmountMC = other.getCardAmountMC();
        this.cardAmountAmex = other.getCardAmountAmex();
        this.cardAmountDiners = other.getCardAmountDiners();
        this.cardAmountDiscover = other.getCardAmountDiscover();
        this.coinCol = other.getCoinCol();
        this.billCol = other.getBillCol();
    }
    
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("paystationCommAddress", paystationCommAddress).append("number", number)
                .append("licensePlateNo", licensePlateNo).append("stallNumber", stallNumber).append("type", type)
                .append("purchasedDate", purchasedDate).append("transDateTime", transDateTime).append("parkingTimePurchased", parkingTimePurchased)
                .append("expiryDate", expiryDate).append("originalAmount", originalAmount).append("chargedAmount", chargedAmount)
                .append("couponNumber", couponNumber).append("couponPercent", couponPercent).append("couponAmount", couponAmount)
                .append("cashPaid", cashPaid).append("cardPaid", cardPaid).append("cardData", cardData)
                .append("cardAuthorizationId", cardAuthorizationId).append("emsPreAuthId", emsPreAuthId).append("mobileNumber", mobileNumber)
                .append("creditCardData", creditCardData).append("paidByRFID", paidByRFID).append("cardAmountOther", cardAmountOther)
                .append("cardAmountSC", cardAmountSC).append("cardAmountVisa", cardAmountVisa).append("cardAmountMC", cardAmountMC)
                .append("cardAmountAmex", cardAmountAmex).append("cardAmountDiners", cardAmountDiners)
                .append("cardAmountDiscover", cardAmountDiscover).append("coinCol", coinCol).append("billCol", billCol)
                .append("authorizationNumber", authorizationNumber).append("emvTransactionData", emvTransactionData)
                .append("preferredRate", preferredRate).append("limitedRate", limitedRate).append("\r\ncardEaseData", cardEaseData)
                .append("cpsResponse", cpsResponse).toString();
    }
    
    @StoryAlias("serial number")
    public final String getPaystationCommAddress() {
        return this.paystationCommAddress;
    }
    
    public final void setPaystationCommAddress(final String paystationCommAddress) {
        this.paystationCommAddress = paystationCommAddress;
    }
    
    @StoryAlias("ticket number")
    public final String getNumber() {
        return this.number;
    }
    
    public final void setNumber(final String number) {
        this.number = number;
    }
    
    @StoryAlias("lot number")
    public final String getLotNumber() {
        return this.lotNumber;
    }
    
    public final void setLotNumber(final String lotNumber) {
        this.lotNumber = lotNumber;
    }
    
    @StoryAlias("license plate number")
    public final String getLicensePlateNo() {
        return this.licensePlateNo;
    }
    
    public final void setLicensePlateNo(final String licensePlateNo) {
        this.licensePlateNo = licensePlateNo;
    }
    
    @StoryAlias("stall number")
    public final String getStallNumber() {
        return this.stallNumber;
    }
    
    public final void setStallNumber(final String stallNumber) {
        this.stallNumber = stallNumber;
    }
    
    public final String getAddTimeNum() {
        return this.addTimeNum;
    }
    
    public final void setAddTimeNum(final String addTimeNum) {
        this.addTimeNum = addTimeNum;
    }
    
    @StoryAlias("type")
    public final String getType() {
        return this.type;
    }
    
    public final void setType(final String type) {
        this.type = type;
    }
    
    @StoryAlias("purchase date")
    public final String getPurchasedDate() {
        return this.purchasedDate;
    }
    
    public final void setPurchasedDate(final String purchasedDate) {
        this.purchasedDate = purchasedDate;
    }
    
    @StoryAlias(ALIAS_TRANS_DATE)
    public final String getTransDateTime() {
        return this.transDateTime;
    }
    
    public final void setTransDateTime(final String transDateTime) {
        this.transDateTime = transDateTime;
    }
    
    @StoryAlias("parking ticket purchased")
    public final String getParkingTimePurchased() {
        return this.parkingTimePurchased;
    }
    
    public final void setParkingTimePurchased(final String parkingTimePurchased) {
        this.parkingTimePurchased = parkingTimePurchased;
    }
    
    @StoryAlias("original amount")
    public final String getOriginalAmount() {
        return this.originalAmount;
    }
    
    public final void setOriginalAmount(final String originalAmount) {
        this.originalAmount = originalAmount;
    }
    
    @StoryAlias("charged amount")
    public final String getChargedAmount() {
        return this.chargedAmount;
    }
    
    public final void setChargedAmount(final String chargedAmount) {
        this.chargedAmount = chargedAmount;
    }
    
    @StoryAlias("coupon number")
    public final String getCouponNumber() {
        return this.couponNumber;
    }
    
    public final void setCouponNumber(final String couponNumber) {
        this.couponNumber = couponNumber;
    }
    
    @StoryAlias("cash paid")
    public final String getCashPaid() {
        return this.cashPaid;
    }
    
    public final void setCashPaid(final String cashPaid) {
        this.cashPaid = cashPaid;
    }
    
    @StoryAlias("card paid")
    public final String getCardPaid() {
        return this.cardPaid;
    }
    
    public final void setCardPaid(final String cardPaid) {
        this.cardPaid = cardPaid;
    }
    
    @StoryAlias("card data")
    public final String getCardData() {
        return this.cardData;
    }
    
    public final void setCardData(final String cardData) {
        this.cardData = cardData;
    }
    
    @StoryAlias("expiry time")
    public final String getExpiryDate() {
        return this.expiryDate;
    }
    
    public final void setExpiryDate(final String expiryDate) {
        this.expiryDate = expiryDate;
    }
    
    @StoryAlias(ALIAS_TAX_ONE_RATE)
    public final String getTax1Rate() {
        return this.tax1Rate;
    }
    
    public final void setTax1Rate(final String tax1Rate) {
        this.tax1Rate = tax1Rate;
    }
    
    @StoryAlias(ALIAS_TAX_ONE_VALUE)
    public final String getTax1Value() {
        return this.tax1Value;
    }
    
    public final void setTax1Value(final String tax1Value) {
        this.tax1Value = tax1Value;
    }
    
    @StoryAlias(ALIAS_TAX_ONE_NAME)
    public final String getTax1Name() {
        return this.tax1Name;
    }
    
    public final void setTax1Name(final String tax1Name) {
        this.tax1Name = tax1Name;
    }
    
    public final String getTax2Rate() {
        return this.tax2Rate;
    }
    
    public final void setTax2Rate(final String tax2Rate) {
        this.tax2Rate = tax2Rate;
    }
    
    public final String getTax2Value() {
        return this.tax2Value;
    }
    
    public final void setTax2Value(final String tax2Value) {
        this.tax2Value = tax2Value;
    }
    
    public final String getTax2Name() {
        return this.tax2Name;
    }
    
    public final void setTax2Name(final String tax2Name) {
        this.tax2Name = tax2Name;
    }
    
    @StoryAlias(ALIAS_TAX_THREE_RATE)
    public String getTax3Rate() {
        return this.tax3Rate;
    }
    
    public void setTax3Rate(final String tax3Rate) {
        this.tax3Rate = tax3Rate;
    }
    
    @StoryAlias(ALIAS_TAX_THREE_VALUE)
    public String getTax3Value() {
        return this.tax3Value;
    }
    
    public void setTax3Value(final String tax3Value) {
        this.tax3Value = tax3Value;
    }
    
    @StoryAlias(ALIAS_TAX_THREE_NAME)
    public String getTax3Name() {
        return this.tax3Name;
    }
    
    public void setTax3Name(final String tax3Name) {
        this.tax3Name = tax3Name;
    }
    
    @StoryAlias("rate name")
    public final String getRateName() {
        return this.rateName;
    }
    
    public final void setRateName(final String rateName) {
        this.rateName = rateName;
    }
    
    @StoryAlias("rate id")
    public final String getRateID() {
        return this.rateID;
    }
    
    public final void setRateID(final String rateID) {
        this.rateID = rateID;
    }
    
    @StoryAlias("rate value")
    public final String getRateValue() {
        return this.rateValue;
    }
    
    public final void setRateValue(final String rateValue) {
        this.rateValue = rateValue;
    }
    
    @StoryAlias("tax type")
    public final String getTaxType() {
        return this.taxType;
    }
    
    public final void setTaxType(final String taxType) {
        this.taxType = taxType;
    }
    
    public final String getNumberBillsAccepted() {
        return this.numberBillsAccepted;
    }
    
    public final void setNumberBillsAccepted(final String numberBillsAccepted) {
        this.numberBillsAccepted = numberBillsAccepted;
    }
    
    public final String getNumberCoinsAccepted() {
        return this.numberCoinsAccepted;
    }
    
    public final void setNumberCoinsAccepted(final String numberCoinsAccepted) {
        this.numberCoinsAccepted = numberCoinsAccepted;
    }
    
    public final String getHopperDispensed() {
        return this.hopperDispensed;
    }
    
    public final void setHopperDispensed(final String hopperDispensed) {
        this.hopperDispensed = hopperDispensed;
    }
    
    @StoryAlias(ALIAS_CARD_AUTHORIZATION_ID)
    public final String getCardAuthorizationId() {
        return this.cardAuthorizationId;
    }
    
    public final void setCardAuthorizationId(final String cardAuthorizationId) {
        this.cardAuthorizationId = cardAuthorizationId;
    }
    
    public final String getChangeDispensed() {
        return this.changeDispensed;
    }
    
    public final void setChangeDispensed(final String changeDispensed) {
        this.changeDispensed = changeDispensed;
    }
    
    @StoryAlias("refund slip")
    public final String getIsRefundSlip() {
        return this.isRefundSlip;
    }
    
    public final void setIsRefundSlip(final String isRefundSlip) {
        this.isRefundSlip = isRefundSlip;
    }
    
    @StoryAlias(ALIAS_EMS_PREAUTH_ID)
    public final String getEmsPreAuthId() {
        return this.emsPreAuthId;
    }
    
    public final void setEmsPreAuthId(final String emsPreAuthId) {
        this.emsPreAuthId = emsPreAuthId;
    }
    
    public final String getSmartCardPaid() {
        return this.smartCardPaid;
    }
    
    public final void setSmartCardPaid(final String smartCardPaid) {
        this.smartCardPaid = smartCardPaid;
    }
    
    public final String getSmartCardData() {
        return this.smartCardData;
    }
    
    public final void setSmartCardData(final String smartCardData) {
        this.smartCardData = smartCardData;
    }
    
    public final String getExcessPayment() {
        return this.excessPayment;
    }
    
    public final void setExcessPayment(final String excessPayment) {
        this.excessPayment = excessPayment;
    }
    
    public final String getSmartCardType() {
        return this.smartCardType;
    }
    
    public final void setSmartCardType(final String smartCardType) {
        this.smartCardType = smartCardType;
    }
    
    public final String getSmartCardMerchantInfo() {
        return this.smartCardMerchantInfo;
    }
    
    public final void setSmartCardMerchantInfo(final String smartCardMerchantInfo) {
        this.smartCardMerchantInfo = smartCardMerchantInfo;
    }
    
    public final String getSmartCardSettlementInfo() {
        return this.smartCardSettlementInfo;
    }
    
    public final void setSmartCardSettlementInfo(final String smartCardSettlementInfo) {
        this.smartCardSettlementInfo = smartCardSettlementInfo;
    }
    
    public final String getMobileNumber() {
        return this.mobileNumber;
    }
    
    public final void setMobileNumber(final String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
    
    public final String getCreditCardData() {
        return this.creditCardData;
    }
    
    public final void setCreditCardData(final String creditCardData) {
        this.creditCardData = creditCardData;
    }
    
    public final String getPaidByRFID() {
        return this.paidByRFID;
    }
    
    public final void setPaidByRFID(final String paidByRFID) {
        this.paidByRFID = paidByRFID;
    }
    
    public final String getCardAmountOther() {
        return this.cardAmountOther;
    }
    
    public final void setCardAmountOther(final String cardAmountOther) {
        this.cardAmountOther = cardAmountOther;
    }
    
    public final String getCardAmountSC() {
        return this.cardAmountSC;
    }
    
    public final void setCardAmountSC(final String cardAmountSC) {
        this.cardAmountSC = cardAmountSC;
    }
    
    public final String getCardAmountVisa() {
        return this.cardAmountVisa;
    }
    
    public final void setCardAmountVisa(final String cardAmountVisa) {
        this.cardAmountVisa = cardAmountVisa;
    }
    
    public final String getCardAmountMC() {
        return this.cardAmountMC;
    }
    
    public final void setCardAmountMC(final String cardAmountMC) {
        this.cardAmountMC = cardAmountMC;
    }
    
    public final String getCardAmountAmex() {
        return this.cardAmountAmex;
    }
    
    public final void setCardAmountAmex(final String cardAmountAmex) {
        this.cardAmountAmex = cardAmountAmex;
    }
    
    public final String getCardAmountDiners() {
        return this.cardAmountDiners;
    }
    
    public final void setCardAmountDiners(final String cardAmountDiners) {
        this.cardAmountDiners = cardAmountDiners;
    }
    
    public final String getCardAmountDiscover() {
        return this.cardAmountDiscover;
    }
    
    public final void setCardAmountDiscover(final String cardAmountDiscover) {
        this.cardAmountDiscover = cardAmountDiscover;
    }
    
    @StoryAlias("coin collected")
    public final String getCoinCol() {
        return this.coinCol;
    }
    
    public final void setCoinCol(final String coinCol) {
        this.coinCol = coinCol;
    }
    
    @StoryAlias("bill collected")
    public final String getBillCol() {
        return this.billCol;
    }
    
    public final void setBillCol(final String billCol) {
        this.billCol = billCol;
    }
    
    public final String getHopper1CoinsDispensed() {
        return this.hopper1CoinsDispensed;
    }
    
    public final void setHopper1CoinsDispensed(final String hopper1CoinsDispensed) {
        this.hopper1CoinsDispensed = hopper1CoinsDispensed;
    }
    
    public final String getHopper2CoinsDispensed() {
        return this.hopper2CoinsDispensed;
    }
    
    public final void setHopper2CoinsDispensed(final String hopper2CoinsDispensed) {
        this.hopper2CoinsDispensed = hopper2CoinsDispensed;
    }
    
    public final String getCouponPercent() {
        return this.couponPercent;
    }
    
    public final void setCouponPercent(final String couponPercent) {
        this.couponPercent = couponPercent;
    }
    
    public final String getCouponAmount() {
        return this.couponAmount;
    }
    
    public final void setCouponAmount(final String couponAmount) {
        this.couponAmount = couponAmount;
    }
    
    public final String getNumberHopper1CoinsDispensed() {
        return this.numberHopper1CoinsDispensed;
    }
    
    public final void setNumberHopper1CoinsDispensed(final String numberHopper1CoinsDispensed) {
        this.numberHopper1CoinsDispensed = numberHopper1CoinsDispensed;
    }
    
    public final String getNumberHopper2CoinsDispensed() {
        return this.numberHopper2CoinsDispensed;
    }
    
    public final void setNumberHopper2CoinsDispensed(final String numberHopper2CoinsDispensed) {
        this.numberHopper2CoinsDispensed = numberHopper2CoinsDispensed;
    }
    
    public final String getVersion() {
        return this.version;
    }
    
    public final void setVersion(final String version) {
        this.version = version;
    }
    
    @StoryAlias(ALIAS_AUTHORIZATION_NUMBER)
    public final String getAuthorizationNumber() {
        return this.authorizationNumber;
    }
    
    public final void setAuthorizationNumber(final String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }
    
    public final String getEmvTransactionData() {
        return this.emvTransactionData;
    }
    
    public final void setEmvTransactionData(final String emvTransactionData) {
        this.emvTransactionData = emvTransactionData;
    }
    
    @StoryAlias(ALIAS_PAID_BY_CHIP)
    public final String getPaidByChipCard() {
        return this.paidByChipCard;
    }

    public final void setPaidByChipCard(final String paidByChipCard) {
        this.paidByChipCard = paidByChipCard;
    }

    @StoryAlias(ALIAS_CARD_EASE_DATA)
    public final String getCardEaseData() {
        return this.cardEaseData;
    }
    
    public final void setCardEaseData(final String cardEaseData) {
        this.cardEaseData = cardEaseData;
    }
    
    public final void setCPSResponse(final String cpsResponse) {
        this.cpsResponse = cpsResponse;
    }
    
    @StoryAlias(ALIAS_CPS_REPONSE)
    public final String getCPSResponse() {
        return this.cpsResponse;
    }
    
    public final EmbeddedTxObject getEmbeddedTxObject() {
        return this.embeddedTxObject;
    }
    
    public final void setEmbeddedTxObject(final EmbeddedTxObject embeddedTxObject) {
        this.embeddedTxObject = embeddedTxObject;
    }
    
    @StoryAlias(ALIAS_PREFERRED_RATE)
    public final Boolean getPreferredRate() {
        return this.preferredRate;
    }
    
    public final void setPreferredRate(final Boolean preferredRate) {
        this.preferredRate = preferredRate;
    }
    
    public final void setPreferredRate(final String preferredRate) {
        this.preferredRate = XMLUtil.numStringToBooleanObject(preferredRate);
    }
    
    @StoryAlias(ALIAS_LIMITED_RATE)
    public final Boolean getLimitedRate() {
        return this.limitedRate;
    }
    
    public final void setLimitedRate(final String limitedRate) {
        this.limitedRate = XMLUtil.numStringToBooleanObject(limitedRate);
    }
    
    public final void setLimitedRate(final Boolean limitedRate) {
        this.limitedRate = limitedRate;
    }
    
}
