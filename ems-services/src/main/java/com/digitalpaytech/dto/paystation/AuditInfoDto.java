package com.digitalpaytech.dto.paystation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class AuditInfoDto implements Serializable {
    private static final long serialVersionUID = 2236654829486579732L;
    
    private String paystationSerialNumber;
    private String paystationName;
    private String paystationLocation;
    private Date auditPeriodStartDate;
    private Date auditPeriodEndDate;
    private int auditReportNumber;
    private int numberPermitsSold;
    private BigDecimal totalCollection;
    private BigDecimal totalRevenue;
    private BigDecimal overpayment;
    private BigDecimal changeIssued;
    private BigDecimal refundsIssued;
    private BigDecimal totalCoinBag;
    private BigDecimal totalBillStacker;
    private BigDecimal billStacker1;
    private BigDecimal billStacker2;
    private BigDecimal billStacker5;
    private BigDecimal billStacker10;
    private BigDecimal billStacker20;
    private BigDecimal billStacker50;
    private BigDecimal totalCreditCard;
    private BigDecimal creditCardVisa;
    private BigDecimal creditCardMasterCard;
    private BigDecimal creditCardAMEX;
    private BigDecimal creditCardDiscover;
    private BigDecimal creditCardDiners;
    private BigDecimal creditCardOther;
    private BigDecimal totalSmartCard;
    private BigDecimal smartCardCharge;
    private BigDecimal smartCardRecharge;
    private BigDecimal coinChangerReplenished;
    private BigDecimal coinChangerOverfillBag;
    private BigDecimal coinChangerAcceptedFloat;
    private BigDecimal coinChangerDispensed;
    private BigDecimal coinChangerTestDispensed;
    private BigDecimal coinHopper1Type;
    private BigDecimal coinHopper1Replenished;
    private BigDecimal coinHopper1Dispensed;
    private BigDecimal coinHopper1TestDispensed;
    private BigDecimal coinHopper2Type;
    private BigDecimal coinHopper2Replenished;
    private BigDecimal coinHopper2Dispensed;
    private BigDecimal coinHopper2TestDispensed;
    
    public String getPaystationSerialNumber() {
        return paystationSerialNumber;
    }
    
    public void setPaystationSerialNumber(String paystationSerialNumber) {
        this.paystationSerialNumber = paystationSerialNumber;
    }
    
    public String getPaystationName() {
        return paystationName;
    }
    
    public void setPaystationName(String paystationName) {
        this.paystationName = paystationName;
    }
    
    public String getPaystationLocation() {
        return paystationLocation;
    }
    
    public void setPaystationLocation(String paystationLocation) {
        this.paystationLocation = paystationLocation;
    }
    
    public Date getAuditPeriodStartDate() {
        return auditPeriodStartDate;
    }
    
    public void setAuditPeriodStartDate(Date auditPeriodStartDate) {
        this.auditPeriodStartDate = auditPeriodStartDate;
    }
    
    public Date getAuditPeriodEndDate() {
        return auditPeriodEndDate;
    }
    
    public void setAuditPeriodEndDate(Date auditPeriodEndDate) {
        this.auditPeriodEndDate = auditPeriodEndDate;
    }
    
    public int getAuditReportNumber() {
        return auditReportNumber;
    }
    
    public void setAuditReportNumber(int auditReportNumber) {
        this.auditReportNumber = auditReportNumber;
    }
    
    public int getNumberPermitsSold() {
        return numberPermitsSold;
    }
    
    public void setNumberPermitsSold(int numberPermitsSold) {
        this.numberPermitsSold = numberPermitsSold;
    }
    
    public BigDecimal getTotalCollection() {
        return totalCollection;
    }
    
    public void setTotalCollection(BigDecimal totalCollection) {
        this.totalCollection = totalCollection;
    }
    
    public BigDecimal getTotalRevenue() {
        return totalRevenue;
    }
    
    public void setTotalRevenue(BigDecimal totalRevenue) {
        this.totalRevenue = totalRevenue;
    }
    
    public BigDecimal getOverpayment() {
        return overpayment;
    }
    
    public void setOverpayment(BigDecimal overpayment) {
        this.overpayment = overpayment;
    }
    
    public BigDecimal getChangeIssued() {
        return changeIssued;
    }
    
    public void setChangeIssued(BigDecimal changeIssued) {
        this.changeIssued = changeIssued;
    }
    
    public BigDecimal getRefundsIssued() {
        return refundsIssued;
    }
    
    public void setRefundsIssued(BigDecimal refundsIssued) {
        this.refundsIssued = refundsIssued;
    }
    
    public BigDecimal getTotalCoinBag() {
        return totalCoinBag;
    }
    
    public void setTotalCoinBag(BigDecimal totalCoinBag) {
        this.totalCoinBag = totalCoinBag;
    }
    
    public BigDecimal getTotalBillStacker() {
        return totalBillStacker;
    }
    
    public void setTotalBillStacker(BigDecimal totalBillStacker) {
        this.totalBillStacker = totalBillStacker;
    }
    
    public BigDecimal getBillStacker1() {
        return billStacker1;
    }
    
    public void setBillStacker1(BigDecimal billStacker1) {
        this.billStacker1 = billStacker1;
    }
    
    public BigDecimal getBillStacker2() {
        return billStacker2;
    }
    
    public void setBillStacker2(BigDecimal billStacker2) {
        this.billStacker2 = billStacker2;
    }
    
    public BigDecimal getBillStacker5() {
        return billStacker5;
    }
    
    public void setBillStacker5(BigDecimal billStacker5) {
        this.billStacker5 = billStacker5;
    }
    
    public BigDecimal getBillStacker10() {
        return billStacker10;
    }
    
    public void setBillStacker10(BigDecimal billStacker10) {
        this.billStacker10 = billStacker10;
    }
    
    public BigDecimal getBillStacker20() {
        return billStacker20;
    }
    
    public void setBillStacker20(BigDecimal billStacker20) {
        this.billStacker20 = billStacker20;
    }
    
    public BigDecimal getBillStacker50() {
        return billStacker50;
    }
    
    public void setBillStacker50(BigDecimal billStacker50) {
        this.billStacker50 = billStacker50;
    }
    
    public BigDecimal getTotalCreditCard() {
        return totalCreditCard;
    }
    
    public void setTotalCreditCard(BigDecimal totalCreditCard) {
        this.totalCreditCard = totalCreditCard;
    }
    
    public BigDecimal getCreditCardVisa() {
        return creditCardVisa;
    }
    
    public void setCreditCardVisa(BigDecimal creditCardVisa) {
        this.creditCardVisa = creditCardVisa;
    }
    
    public BigDecimal getCreditCardMasterCard() {
        return creditCardMasterCard;
    }
    
    public void setCreditCardMasterCard(BigDecimal creditCardMasterCard) {
        this.creditCardMasterCard = creditCardMasterCard;
    }
    
    public BigDecimal getCreditCardAMEX() {
        return creditCardAMEX;
    }
    
    public void setCreditCardAMEX(BigDecimal creditCardAMEX) {
        this.creditCardAMEX = creditCardAMEX;
    }
    
    public BigDecimal getCreditCardDiscover() {
        return creditCardDiscover;
    }
    
    public void setCreditCardDiscover(BigDecimal creditCardDiscover) {
        this.creditCardDiscover = creditCardDiscover;
    }
    
    public BigDecimal getCreditCardDiners() {
        return creditCardDiners;
    }
    
    public void setCreditCardDiners(BigDecimal creditCardDiners) {
        this.creditCardDiners = creditCardDiners;
    }
    
    public BigDecimal getCreditCardOther() {
        return creditCardOther;
    }
    
    public void setCreditCardOther(BigDecimal creditCardOther) {
        this.creditCardOther = creditCardOther;
    }
    
    public BigDecimal getTotalSmartCard() {
        return totalSmartCard;
    }
    
    public void setTotalSmartCard(BigDecimal totalSmartCard) {
        this.totalSmartCard = totalSmartCard;
    }
    
    public BigDecimal getSmartCardCharge() {
        return smartCardCharge;
    }
    
    public void setSmartCardCharge(BigDecimal smartCardCharge) {
        this.smartCardCharge = smartCardCharge;
    }
    
    public BigDecimal getSmartCardRecharge() {
        return smartCardRecharge;
    }
    
    public void setSmartCardRecharge(BigDecimal smartCardRecharge) {
        this.smartCardRecharge = smartCardRecharge;
    }
    
    public BigDecimal getCoinChangerReplenished() {
        return coinChangerReplenished;
    }
    
    public void setCoinChangerReplenished(BigDecimal coinChangerReplenished) {
        this.coinChangerReplenished = coinChangerReplenished;
    }
    
    public BigDecimal getCoinChangerOverfillBag() {
        return coinChangerOverfillBag;
    }
    
    public void setCoinChangerOverfillBag(BigDecimal coinChangerOverfillBag) {
        this.coinChangerOverfillBag = coinChangerOverfillBag;
    }
    
    public BigDecimal getCoinChangerAcceptedFloat() {
        return coinChangerAcceptedFloat;
    }
    
    public void setCoinChangerAcceptedFloat(BigDecimal coinChangerAcceptedFloat) {
        this.coinChangerAcceptedFloat = coinChangerAcceptedFloat;
    }
    
    public BigDecimal getCoinChangerDispensed() {
        return coinChangerDispensed;
    }
    
    public void setCoinChangerDispensed(BigDecimal coinChangerDispensed) {
        this.coinChangerDispensed = coinChangerDispensed;
    }
    
    public BigDecimal getCoinChangerTestDispensed() {
        return coinChangerTestDispensed;
    }
    
    public void setCoinChangerTestDispensed(BigDecimal coinChangerTestDispensed) {
        this.coinChangerTestDispensed = coinChangerTestDispensed;
    }
    
    public BigDecimal getCoinHopper1Type() {
        return coinHopper1Type;
    }
    
    public void setCoinHopper1Type(BigDecimal coinHopper1Type) {
        this.coinHopper1Type = coinHopper1Type;
    }
    
    public BigDecimal getCoinHopper1Replenished() {
        return coinHopper1Replenished;
    }
    
    public void setCoinHopper1Replenished(BigDecimal coinHopper1Replenished) {
        this.coinHopper1Replenished = coinHopper1Replenished;
    }
    
    public BigDecimal getCoinHopper1Dispensed() {
        return coinHopper1Dispensed;
    }
    
    public void setCoinHopper1Dispensed(BigDecimal coinHopper1Dispensed) {
        this.coinHopper1Dispensed = coinHopper1Dispensed;
    }
    
    public BigDecimal getCoinHopper1TestDispensed() {
        return coinHopper1TestDispensed;
    }
    
    public void setCoinHopper1TestDispensed(BigDecimal coinHopper1TestDispensed) {
        this.coinHopper1TestDispensed = coinHopper1TestDispensed;
    }
    
    public BigDecimal getCoinHopper2Type() {
        return coinHopper2Type;
    }
    
    public void setCoinHopper2Type(BigDecimal coinHopper2Type) {
        this.coinHopper2Type = coinHopper2Type;
    }
    
    public BigDecimal getCoinHopper2Replenished() {
        return coinHopper2Replenished;
    }
    
    public void setCoinHopper2Replenished(BigDecimal coinHopper2Replenished) {
        this.coinHopper2Replenished = coinHopper2Replenished;
    }
    
    public BigDecimal getCoinHopper2Dispensed() {
        return coinHopper2Dispensed;
    }
    
    public void setCoinHopper2Dispensed(BigDecimal coinHopper2Dispensed) {
        this.coinHopper2Dispensed = coinHopper2Dispensed;
    }
    
    public BigDecimal getCoinHopper2TestDispensed() {
        return coinHopper2TestDispensed;
    }
    
    public void setCoinHopper2TestDispensed(BigDecimal coinHopper2TestDispensed) {
        this.coinHopper2TestDispensed = coinHopper2TestDispensed;
    }
    
}
