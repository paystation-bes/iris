package com.digitalpaytech.dto.paystation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class CollectionInfoDto implements Serializable {
    private static final long serialVersionUID = -7482250384066225896L;
    
    private String paystationSerialNumber;
    private String paystationName;
    private String paystationRegion;
    private Date collectionPeriodStartDate;
    private Date collectionPeriodEndDate;
    private int collectionReportNumber;
    private String collectionTypeName;
    private int startPermitNumber;
    private int endPermitNumber;
    private int numberPermitsSold;
    private BigDecimal billStacker1;
    private BigDecimal billStacker2;
    private BigDecimal billStacker5;
    private BigDecimal billStacker10;
    private BigDecimal billStacker20;
    private BigDecimal billStacker50;
    private BigDecimal creditCardVisa;
    private BigDecimal creditCardMasterCard;
    private BigDecimal creditCardAMEX;
    private BigDecimal creditCardDiscover;
    private BigDecimal creditCardDiners;
    private BigDecimal creditCardOther;
    private BigDecimal smartCard;
    private BigDecimal coinCanister005;
    private BigDecimal coinCanister010;
    private BigDecimal coinCanister025;
    private BigDecimal coinCanister100;
    private BigDecimal coinCanister200;
    
    public String getPaystationSerialNumber() {
        return paystationSerialNumber;
    }
    
    public void setPaystationSerialNumber(String paystationSerialNumber) {
        this.paystationSerialNumber = paystationSerialNumber;
    }
    
    public String getPaystationName() {
        return paystationName;
    }
    
    public void setPaystationName(String paystationName) {
        this.paystationName = paystationName;
    }
    
    public String getPaystationRegion() {
        return paystationRegion;
    }
    
    public void setPaystationRegion(String paystationRegion) {
        this.paystationRegion = paystationRegion;
    }
    
    public Date getCollectionPeriodStartDate() {
        return collectionPeriodStartDate;
    }
    
    public void setCollectionPeriodStartDate(Date collectionPeriodStartDate) {
        this.collectionPeriodStartDate = collectionPeriodStartDate;
    }
    
    public Date getCollectionPeriodEndDate() {
        return collectionPeriodEndDate;
    }
    
    public void setCollectionPeriodEndDate(Date collectionPeriodEndDate) {
        this.collectionPeriodEndDate = collectionPeriodEndDate;
    }
    
    public int getCollectionReportNumber() {
        return collectionReportNumber;
    }
    
    public void setCollectionReportNumber(int collectionReportNumber) {
        this.collectionReportNumber = collectionReportNumber;
    }
    
    public String getCollectionTypeName() {
        return collectionTypeName;
    }
    
    public void setCollectionTypeName(String collectionTypeName) {
        this.collectionTypeName = collectionTypeName;
    }
    
    public int getStartPermitNumber() {
        return startPermitNumber;
    }
    
    public void setStartPermitNumber(int startPermitNumber) {
        this.startPermitNumber = startPermitNumber;
    }
    
    public int getEndPermitNumber() {
        return endPermitNumber;
    }
    
    public void setEndPermitNumber(int endPermitNumber) {
        this.endPermitNumber = endPermitNumber;
    }
    
    public int getNumberPermitsSold() {
        return numberPermitsSold;
    }
    
    public void setNumberPermitsSold(int numberPermitsSold) {
        this.numberPermitsSold = numberPermitsSold;
    }
    
    public BigDecimal getBillStacker1() {
        return billStacker1;
    }
    
    public void setBillStacker1(BigDecimal billStacker1) {
        this.billStacker1 = billStacker1;
    }
    
    public BigDecimal getBillStacker2() {
        return billStacker2;
    }
    
    public void setBillStacker2(BigDecimal billStacker2) {
        this.billStacker2 = billStacker2;
    }
    
    public BigDecimal getBillStacker5() {
        return billStacker5;
    }
    
    public void setBillStacker5(BigDecimal billStacker5) {
        this.billStacker5 = billStacker5;
    }
    
    public BigDecimal getBillStacker10() {
        return billStacker10;
    }
    
    public void setBillStacker10(BigDecimal billStacker10) {
        this.billStacker10 = billStacker10;
    }
    
    public BigDecimal getBillStacker20() {
        return billStacker20;
    }
    
    public void setBillStacker20(BigDecimal billStacker20) {
        this.billStacker20 = billStacker20;
    }
    
    public BigDecimal getBillStacker50() {
        return billStacker50;
    }
    
    public void setBillStacker50(BigDecimal billStacker50) {
        this.billStacker50 = billStacker50;
    }
    
    public BigDecimal getCreditCardVisa() {
        return creditCardVisa;
    }
    
    public void setCreditCardVisa(BigDecimal creditCardVisa) {
        this.creditCardVisa = creditCardVisa;
    }
    
    public BigDecimal getCreditCardMasterCard() {
        return creditCardMasterCard;
    }
    
    public void setCreditCardMasterCard(BigDecimal creditCardMasterCard) {
        this.creditCardMasterCard = creditCardMasterCard;
    }
    
    public BigDecimal getCreditCardAMEX() {
        return creditCardAMEX;
    }
    
    public void setCreditCardAMEX(BigDecimal creditCardAMEX) {
        this.creditCardAMEX = creditCardAMEX;
    }
    
    public BigDecimal getCreditCardDiscover() {
        return creditCardDiscover;
    }
    
    public void setCreditCardDiscover(BigDecimal creditCardDiscover) {
        this.creditCardDiscover = creditCardDiscover;
    }
    
    public BigDecimal getCreditCardDiners() {
        return creditCardDiners;
    }
    
    public void setCreditCardDiners(BigDecimal creditCardDiners) {
        this.creditCardDiners = creditCardDiners;
    }
    
    public BigDecimal getCreditCardOther() {
        return creditCardOther;
    }
    
    public void setCreditCardOther(BigDecimal creditCardOther) {
        this.creditCardOther = creditCardOther;
    }
    
    public BigDecimal getSmartCard() {
        return smartCard;
    }
    
    public void setSmartCard(BigDecimal smartCard) {
        this.smartCard = smartCard;
    }
    
    public BigDecimal getCoinCanister005() {
        return coinCanister005;
    }
    
    public void setCoinCanister005(BigDecimal coinCanister005) {
        this.coinCanister005 = coinCanister005;
    }
    
    public BigDecimal getCoinCanister010() {
        return coinCanister010;
    }
    
    public void setCoinCanister010(BigDecimal coinCanister010) {
        this.coinCanister010 = coinCanister010;
    }
    
    public BigDecimal getCoinCanister025() {
        return coinCanister025;
    }
    
    public void setCoinCanister025(BigDecimal coinCanister025) {
        this.coinCanister025 = coinCanister025;
    }
    
    public BigDecimal getCoinCanister100() {
        return coinCanister100;
    }
    
    public void setCoinCanister100(BigDecimal coinCanister100) {
        this.coinCanister100 = coinCanister100;
    }
    
    public BigDecimal getCoinCanister200() {
        return coinCanister200;
    }
    
    public void setCoinCanister200(BigDecimal coinCanister200) {
        this.coinCanister200 = coinCanister200;
    }
    
}
