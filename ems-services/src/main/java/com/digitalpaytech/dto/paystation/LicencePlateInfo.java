package com.digitalpaytech.dto.paystation;

import java.io.Serializable;
import java.util.Date;

public class LicencePlateInfo implements Serializable {
    
    private static final long serialVersionUID = -4819164944461417025L;
    private String licencePlateNumber = null;
    private Date permitBeginGmt = null;
    private Date permitExpireGmt= null;
    private String permitBeginLocal = null;
    private String permitExpireLocal= null;
    private String locationName = null;
    
    public LicencePlateInfo(String licencePlateNumber, Date permitBeginGmt, Date permitExpireGmt, String locationName) {
        super();
        this.licencePlateNumber = licencePlateNumber;
        this.permitBeginGmt = permitBeginGmt;
        this.permitExpireGmt = permitExpireGmt;
        this.locationName = locationName;
    }
    
    public LicencePlateInfo() {
        super();
    }
    
    public String getLicencePlateNumber() {
        return licencePlateNumber;
    }
    
    public void setLicencePlateNumber(String licencePlateNumber) {
        this.licencePlateNumber = licencePlateNumber;
    }
    
    public Date getPermitBeginGmt() {
        return permitBeginGmt;
    }
    
    public void setPermitBeginGmt(Date permitBeginGmt) {
        this.permitBeginGmt = permitBeginGmt;
    }
    
    public Date getPermitExpireGmt() {
        return permitExpireGmt;
    }
    
    public void setPermitExpireGmt(Date permitExpireGmt) {
        this.permitExpireGmt = permitExpireGmt;
    }
    
    public String getLocationName() {
        return locationName;
    }
    
    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }
    
    @Override
    public String toString() {
        return licencePlateNumber + ":" + permitBeginGmt + ":" + permitExpireGmt + ":" + locationName;
    }

	public String getPermitBeginLocal() {
		return permitBeginLocal;
	}

	public void setPermitBeginLocal(String permitBeginLocal) {
		this.permitBeginLocal = permitBeginLocal;
	}

	public String getPermitExpireLocal() {
		return permitExpireLocal;
	}

	public void setPermitExpireLocal(String permitExpireLocal) {
		this.permitExpireLocal = permitExpireLocal;
	}
}
