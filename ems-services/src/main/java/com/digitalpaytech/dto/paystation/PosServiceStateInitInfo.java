package com.digitalpaytech.dto.paystation;

public class PosServiceStateInitInfo {
    private String paystationSetting;
    private String primaryVersion;
    private String bbSerialNumber;
    
    public PosServiceStateInitInfo() {
    }

    public PosServiceStateInitInfo(String paystationSetting, String primaryVersion, String bbSerialNumber) {
        this.paystationSetting = paystationSetting;
        this.primaryVersion = primaryVersion;
        this.bbSerialNumber = bbSerialNumber;
    }

    public String getPaystationSetting() {
        return paystationSetting;
    }

    public void setPaystationSetting(String paystationSetting) {
        this.paystationSetting = paystationSetting;
    }

    public String getPrimaryVersion() {
        return primaryVersion;
    }

    public void setPrimaryVersion(String primaryVersion) {
        this.primaryVersion = primaryVersion;
    }

    public String getBbSerialNumber() {
        return bbSerialNumber;
    }

    public void setBbSerialNumber(String bBSerialNumber) {
        this.bbSerialNumber = bBSerialNumber;
    }
}
