package com.digitalpaytech.dto.paystation;

import java.io.Serializable;
import java.util.Map;
import java.util.HashMap;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.dto.EmbeddedTxObject;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@StoryAlias(CardEaseData.ALIAS)
public class CardEaseData implements EmbeddedTxObject, Serializable {
    public static final String CARD_REFERENCE = "cardReference";
    public static final String CARD_HASH = "cardHash";
    public static final String ALIAS = "card ease data";
    private static final long serialVersionUID = 7194900116758402852L;
    
    @JsonProperty("pan")
    private String pan;
    
    @JsonProperty("ref")
    private String ref;
    
    @JsonProperty(CARD_REFERENCE)
    private String cardReference;
    
    @JsonProperty(CARD_HASH)
    private String cardHash;
    
    @JsonProperty("date")
    private String date;
    
    @JsonProperty("acquirer")
    private String acquirer;
    
    @JsonProperty("cvm")
    private String cvm;
    
    @JsonProperty("tid")
    private String tid;
    
    @JsonProperty("Auth Code")
    private String authCode;
    
    // Data in JSON is using key name "Application".
    @JsonProperty("application")
    private String application;
    
    // Data for CPS needs to be using key name "APL".
    @JsonProperty("apl")
    private String apl;
    
    @JsonProperty("aid")
    private String aid;

    private Map<String, Object> paymentDetails;
    
    public CardEaseData() {
        this.paymentDetails = new HashMap<>();
    }
    
    @Override
    public final boolean isCPSStoreForward() {
        return false;
    }
    
    public final String getPan() {
        return this.pan;
    }
    
    public final void setPan(final String pan) {
        this.pan = pan;
    }
    
    public final String getRef() {
        return this.ref;
    }
    
    public final void setRef(final String ref) {
        this.ref = ref;
    }
    
    public final String getAcquirer() {
        return this.acquirer;
    }
    
    public final void setAcquirer(final String acquirer) {
        this.acquirer = acquirer;
    }
    
    public final String getCardReference() {
        return this.cardReference;
    }
    
    public final void setCardReference(final String cardReference) {
        this.cardReference = cardReference;
    }
    
    public final String getCardHash() {
        return this.cardHash;
    }
    
    public final void setCardHash(final String cardHash) {
        this.cardHash = cardHash;
    }
    
    public final String getDate() {
        return this.date;
    }
    
    public final void setDate(final String date) {
        this.date = date;
    }
    
    public final String getTid() {
        return this.tid;
    }
    
    public final void setTid(final String tid) {
        this.tid = tid;
    }
    
    public final String getCvm() {
        return this.cvm;
    }
    
    public final void setCvm(final String cvm) {
        this.cvm = cvm;
    }
    
    public final String getApplication() {
        return this.application;
    }
    
    public final void setApplication(final String application) {
        this.application = application;
    }
    
    public final String getApl() {
        return this.apl;
    }
    
    public final void setApl(final String apl) {
        this.apl = apl;
    }
    
    public final String getAid() {
        return this.aid;
    }
    
    public final void setAid(final String aid) {
        this.aid = aid;
    }
    
    public final String getAuthCode() {
        return this.authCode;
    }
    
    public final void setAuthCode(final String authCode) {
        this.authCode = authCode;
    }
    
    public final void switchApplicationToApl() {
        if (this.apl == null) {
            this.apl = this.application;
        }
        this.application = null;
    }
    
    public final Map<String, Object> getPaymentDetails() {
        return this.paymentDetails;
    }

    public final void setPaymentDetails(final Map<String, Object> paymentDetails) {
        this.paymentDetails = paymentDetails;
    }

    @Override
    public final String toString() {
        return "CardEaseData [pan=" + this.pan + ", ref=" + this.ref + ", cardReference=" + this.cardReference + ", cardHash=" + this.cardHash 
                + ", date=" + this.date + ", acquirer=" + this.acquirer + ", cvm=" + this.cvm + ", tid=" + this.tid + ", authCode=" + this.authCode 
                + ", application=" + this.application + ", apl=" + this.apl + ", aid=" + this.aid + ", paymentDetails=" + this.paymentDetails + "]";
    }
}
