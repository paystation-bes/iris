package com.digitalpaytech.dto;

import com.digitalpaytech.util.WebCoreConstants;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("displayType")
public class DisplayType extends SettingsType {
    
    private static final long serialVersionUID = 8198796901148063265L;
    private transient int actuaId;
    
    public DisplayType() {
        setStatus(WebCoreConstants.ACTIVE);
    }
    
    public final int getActuaId() {
        return this.actuaId;
    }
    
    public final void setActuaId(final int actuaId) {
        this.actuaId = actuaId;
    }
}
