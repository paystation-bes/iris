package com.digitalpaytech.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import com.digitalpaytech.util.WebCoreConstants;

@XStreamAlias("alertType")
public class AlertWidgetType extends SettingsType {
    private static final long serialVersionUID = -5834740972890818822L;
    
    public AlertWidgetType() {
        setStatus(WebCoreConstants.ACTIVE);
    }
}
