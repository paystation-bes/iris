package com.digitalpaytech.dto;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.transform.AliasToBeanResultTransformer;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.support.WebObjectId;

public class AlertMapEntryMinimalTransformer extends AliasToBeanResultTransformer {
    
    private static final long serialVersionUID = -2408407213999954440L;
    
    private Map<String, Integer> aliasIdxMap;
    
    public AlertMapEntryMinimalTransformer() {
        super(AlertMapEntry.class);
    }
    
    @Override
    public final Object transformTuple(final Object[] tuple, final String[] aliases) {
        resolveIdx(tuple, aliases);
        
        final AlertMapEntry result = new AlertMapEntry();
        
        final Integer id = (Integer) tuple[this.aliasIdxMap.get("psId")];
        result.setPosRandomId(new WebObjectId(PointOfSale.class, id));
        result.setSerial((String) tuple[this.aliasIdxMap.get("psSerialNumber")]);
        result.setPayStationType((Integer) tuple[this.aliasIdxMap.get("psType")]);
        result.setLatitude((BigDecimal) tuple[this.aliasIdxMap.get("latitude")]);
        result.setLongitude((BigDecimal) tuple[this.aliasIdxMap.get("longitude")]);
        result.setPointOfSaleName((String) tuple[this.aliasIdxMap.get("posName")]);
        result.setSeverity(defaultIfNull(tuple, "maxSeverity", 0));
        result.setNumberOfAlerts(defaultIfNull(tuple, "sum", 0));
        return result;
    }
    
    private void resolveIdx(final Object[] tuple, final String[] aliases) {
        if (this.aliasIdxMap == null) {
            int idx = aliases.length;
            this.aliasIdxMap = new HashMap<String, Integer>(idx * 2, WebCoreConstants.ALERT_THRESHOLD_0_75);
            while (--idx >= 0) {
                this.aliasIdxMap.put(aliases[idx], idx);
            }
        }
    }
    
    private int defaultIfNull(final Object[] tuple, final String propertyName, final int defaultValue) {
        int result = defaultValue;
        
        final Integer idx = this.aliasIdxMap.get(propertyName);
        if (idx != null) {
            result = ((Number) tuple[idx]).intValue();
        }
        
        return result;
    }
}
