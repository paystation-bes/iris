package com.digitalpaytech.dto;

import java.util.HashMap;
import java.util.Map;

import org.hibernate.transform.AliasToBeanResultTransformer;

import com.digitalpaytech.domain.PointOfSale;

public class PosHashTransformer extends AliasToBeanResultTransformer {
    
    private static final long serialVersionUID = 7568895714267768113L;
    private Map<Integer, PointOfSale> result = new HashMap<Integer, PointOfSale>(10, 0.3F);
    
    public PosHashTransformer() {
        super(PointOfSale.class);
    }
    
    @Override
    public final Object transformTuple(final Object[] tuple, final String[] aliases) {
        PointOfSale pos = (PointOfSale) tuple[0];
        
        final PointOfSale existing = this.result.get(pos.getId());
        if (existing != null) {
            pos = existing;
        } else {
            this.result.put(pos.getId(), pos);
        }
        
        return null;
    }
    
    public final Map<Integer, PointOfSale> getResult() {
        return this.result;
    }
}
