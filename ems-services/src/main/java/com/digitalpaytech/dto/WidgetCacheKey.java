package com.digitalpaytech.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.WidgetSubsetMember;

public final class WidgetCacheKey implements Serializable {
    private static final long serialVersionUID = 4025401817563789066L;
    
    private int customerId;
    private int metricType;
    
    private int filterType;
    private int rangeType;
    private int limitType;
    
    private int tier1TypeId;
    private int tier2TypeId;
    private int tier3TypeId;
    
    private boolean isShowPaystations;
    private boolean isTrendByLocation;
    
    private long trendAmount;
    private int chartType;
    
    private int[] tier1SubsetIds;
    private int[] tier2SubsetIds;
    private int[] tier3SubsetIds;
    
    private String identity;
    
    // This constructor is for Kryo to be able to deserialize this
    public WidgetCacheKey() {
        
    }

    public WidgetCacheKey(final Widget widget, final int customerId) {
        this();
        
        this.customerId = customerId;
        this.metricType = (widget.getWidgetMetricType() == null) ? 0 : widget.getWidgetMetricType().getId();
        
        this.filterType = (widget.getWidgetFilterType() == null) ? 0 : widget.getWidgetFilterType().getId();
        this.rangeType = (widget.getWidgetRangeType() == null) ? 0 : widget.getWidgetRangeType().getId();
        this.limitType = (widget.getWidgetLimitType() == null) ? 0 : widget.getWidgetLimitType().getId();
        
        this.isShowPaystations = widget.getIsShowPaystations();
        this.isTrendByLocation = widget.isIsTrendByLocation();
        
        this.trendAmount = (widget.getTrendAmount() == null) ? 0L : widget.getTrendAmount();
        this.chartType = (widget.getWidgetChartType() == null) ? 0 : widget.getWidgetChartType().getId();
        
        processTierTypes(widget);
        processTiers(widget);
        
        this.identity = buildIdentity();
    }
    
    private void processTierTypes(final Widget widget) {
        this.tier1TypeId = ((widget.getWidgetTierTypeByWidgetTier1Type() == null) || (widget.getWidgetTierTypeByWidgetTier1Type().getId() == null))
                ? 0 : widget.getWidgetTierTypeByWidgetTier1Type().getId();
        this.tier2TypeId = ((widget.getWidgetTierTypeByWidgetTier2Type() == null) || (widget.getWidgetTierTypeByWidgetTier2Type().getId() == null))
                ? 0 : widget.getWidgetTierTypeByWidgetTier2Type().getId();
        this.tier3TypeId = ((widget.getWidgetTierTypeByWidgetTier3Type() == null) || (widget.getWidgetTierTypeByWidgetTier3Type().getId() == null))
                ? 0 : widget.getWidgetTierTypeByWidgetTier3Type().getId();
    }
    
    private void processTiers(final Widget widget) {
        final Set<Integer> tier1 = new TreeSet<Integer>();
        final Set<Integer> tier2 = new TreeSet<Integer>();
        final Set<Integer> tier3 = new TreeSet<Integer>();
        for (WidgetSubsetMember member : widget.getWidgetSubsetMembers()) {
            final int tierTypeId = member.getWidgetTierType().getId();
            if (this.tier1TypeId == tierTypeId) {
                tier1.add(member.getMemberId());
            } else if (this.tier2TypeId == tierTypeId) {
                tier2.add(member.getMemberId());
            } else if (this.tier3TypeId == tierTypeId) {
                tier3.add(member.getMemberId());
            }
        }
        
        this.tier1SubsetIds = toIntArray(tier1);
        this.tier2SubsetIds = toIntArray(tier2);
        this.tier3SubsetIds = toIntArray(tier3);
    }
    
    public String getIdentity() {
        return this.identity;
    }
    
    @Override
    public int hashCode() {
        return this.identity.hashCode();
    }
    
    @Override
    public boolean equals(final Object obj) {
        boolean result = false;
        if (obj instanceof WidgetCacheKey) {
            result = this.identity.equals(((WidgetCacheKey) obj).identity);
        }
        
        return result;
    }
    
    private String buildIdentity() {
        final StringBuilder result = new StringBuilder();
        
        result.append("c:");
        result.append(this.customerId);
        result.append(",mt:");
        result.append(this.metricType);
        result.append(",ft:");
        result.append(this.filterType);
        result.append(",rt:");
        result.append(this.rangeType);
        result.append(",lt:");
        result.append(this.limitType);
        result.append(",ct:");
        result.append(this.chartType);
        result.append(",ta:");
        result.append(this.trendAmount);
        result.append(",tl:");
        result.append(this.isTrendByLocation);
        result.append(",ps:");
        result.append(this.isShowPaystations);
        result.append(",t1:");
        result.append(this.tier1TypeId);
        result.append(join(this.tier1SubsetIds));
        result.append(",t2:");
        result.append(this.tier2TypeId);
        result.append(join(this.tier2SubsetIds));
        result.append(",t3:");
        result.append(this.tier3TypeId);
        result.append(join(this.tier3SubsetIds));
        
        return result.toString();
    }
    
    private String join(final int[] arr) {
        final StringBuilder result = new StringBuilder();
        result.append("[");
        for (int val : arr) {
            result.append(val);
            result.append(",");
        }
        
        result.append("]");
        
        return result.toString();
    }
    
    private int[] toIntArray(final Collection<Integer> raw) {
        final int[] result = new int[raw.size()];
        int idx = 0;
        for (int val : raw) {
            result[idx++] = val;
        }
        
        return result;
    }
}
