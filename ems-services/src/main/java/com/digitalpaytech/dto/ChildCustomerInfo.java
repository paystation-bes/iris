package com.digitalpaytech.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias(value = "childCustomerInfo")
public class ChildCustomerInfo {
    private String randomizedCustomerId;
    private String name;
    private String status;
    private boolean isDefaultAlias;
    
    public ChildCustomerInfo() {
        
    }
    
    public ChildCustomerInfo(final String randomizedCustomerId, final String name, final String status, final boolean isDefaultAlias) {
        this.randomizedCustomerId = randomizedCustomerId;
        this.name = name;
        this.setStatus(status);
        this.isDefaultAlias = isDefaultAlias;
    }
    
    public final String getRandomizedCustomerId() {
        return this.randomizedCustomerId;
    }
    
    public final void setRandomizedCustomerId(final String randomizedCustomerId) {
        this.randomizedCustomerId = randomizedCustomerId;
    }
    
    public final String getName() {
        return this.name;
    }
    
    public final void setName(final String name) {
        this.name = name;
    }
    
    public final String getStatus() {
        return this.status;
    }
    
    public final void setStatus(final String status) {
        this.status = status;
    }

    public boolean getIsDefaultAlias() {
        return isDefaultAlias;
    }

    public void setIsDefaultAliasr(boolean isDefaultAlias) {
        this.isDefaultAlias = isDefaultAlias;
    }
}
