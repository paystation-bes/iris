package com.digitalpaytech.dto;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;

public class FilterDTO implements Serializable {
    private static final long serialVersionUID = -8435957250071360297L;
    private String label;
    private transient Number id;
    private String value;
    private String desc;
    private String extraData;
    private String processStatusString;
    private String lastUpdatedDateTime;
    private List<String> errorRows;
    
    public FilterDTO() {
        
    }
    
    public FilterDTO(final Number id) {
        this.id = id;
    }
    
    public FilterDTO(final String value, final String label) {
        this(value, label, (String) null);
    }
    
    public FilterDTO(final String value, final String label, final String desc) {
        this.value = value;
        this.label = label;
        this.desc = desc;
    }
    
    public final String getLabel() {
        return this.label;
    }
    
    public final void setLabel(final String label) {
        this.label = label;
    }
    
    public final String getValue() {
        return this.value;
    }
    
    public final void setValue(final String value) {
        this.value = value;
    }
    
    public final String getDesc() {
        return this.desc;
    }
    
    public final void setDesc(final String desc) {
        this.desc = desc;
    }
    
    public final Number getId() {
        return this.id;
    }
    
    public final void setId(final Number id) {
        this.id = id;
    }
    
    public final String getExtraData() {
        return this.extraData;
    }
    
    public final void setExtraData(final String extraData) {
        this.extraData = extraData;
    }
    
    public final String getProcessStatusString() {
        return this.processStatusString;
    }

    public final void setProcessStatusString(final String processStatusString) {
        this.processStatusString = processStatusString;
    }

    public final String[] getErrorRows() {
        if (this.errorRows == null || this.errorRows.isEmpty()) {
            return null;
        }
        return this.errorRows.toArray(new String[this.errorRows.size()]);
    }

    public final void setErrorRows(final List<String> errorRows) {
        this.errorRows = errorRows;
    }

    public final String getLastUpdatedDateTime() {
        return this.lastUpdatedDateTime;
    }

    public final void setLastUpdatedDateTime(final String lastUpdatedDateTime) {
        this.lastUpdatedDateTime = lastUpdatedDateTime;
    }

    public static class LabelComparator implements Comparator<FilterDTO> {
        @Override
        public final int compare(final FilterDTO o1, final FilterDTO o2) {
            return o1.getLabel().compareTo(o2.getLabel());
        }
    }
}
