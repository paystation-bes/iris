package com.digitalpaytech.dto;

import com.digitalpaytech.util.WebCoreConstants;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("revType")
public class RevType extends SettingsType {
    
    private static final long serialVersionUID = -4166916662783466142L;
    
    private int level;
    private String parentRandomId;
    
    public RevType() {
        setStatus(WebCoreConstants.ACTIVE);
    }
    
    public final int getLevel() {
        return this.level;
    }
    
    public final void setLevel(final int level) {
        this.level = level;
    }
    
    public final String getParentRandomId() {
        return this.parentRandomId;
    }
    
    public final void setParentRandomId(final String parentRandomId) {
        this.parentRandomId = parentRandomId;
    }
}
