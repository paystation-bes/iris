package com.digitalpaytech.dto;

public class RandomIdNameSetting {
    
    private String randomId;
    private String name;
    private int label;
    
    public RandomIdNameSetting() {
        
    }
    
    public RandomIdNameSetting(final String randomId, final String name) {
        this.randomId = randomId;
        this.name = name;
    }
    
    public RandomIdNameSetting(final String randomId, final String name, final int label) {
        this.randomId = randomId;
        this.name = name;
        this.label = label;
    }
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final String getName() {
        return this.name;
    }
    
    public final void setName(final String name) {
        this.name = name;
    }
    
    public final int getLabel() {
        return this.label;
    }
    
    public final void setLabel(final int label) {
        this.label = label;
    }
}
