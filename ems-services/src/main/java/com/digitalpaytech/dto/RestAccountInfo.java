package com.digitalpaytech.dto;

import java.util.ArrayList;
import java.util.List;

import com.digitalpaytech.domain.RestAccount;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.WebCoreConstants;

public class RestAccountInfo {
    
    private String type;
    private String name;
    private String key;
    private String virtualPS;
    private String randomId;
    private String comments;
    
    public RestAccountInfo() {
        
    }
    
    public RestAccountInfo(final String type, final String name, final String key, final String virtualPS) {
        this.type = type;
        this.name = name;
        this.key = key;
        this.virtualPS = virtualPS;
    }
    
    public RestAccountInfo(final String type, final String name, final String key, final String virtualPS, final String randomId) {
        this.type = type;
        this.name = name;
        this.key = key;
        this.virtualPS = virtualPS;
        this.randomId = randomId;
    }
    
    public RestAccountInfo(final String type, final String name, final String key, final String virtualPS, final String randomId,
            final String comments) {
        this.type = type;
        this.name = name;
        this.key = key;
        this.virtualPS = virtualPS;
        this.randomId = randomId;
        this.comments = comments;
    }
    
    public final String getType() {
        return this.type;
    }
    
    public final void setType(final String type) {
        this.type = type;
    }
    
    public final String getName() {
        return this.name;
    }
    
    public final void setName(final String name) {
        this.name = name;
    }
    
    public final String getKey() {
        return this.key;
    }
    
    public final void setKey(final String key) {
        this.key = key;
    }
    
    public final String getVirtualPS() {
        return this.virtualPS;
    }
    
    public final void setVirtualPS(final String virtualPS) {
        this.virtualPS = virtualPS;
    }
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final String getComments() {
        return this.comments;
    }
    
    public final void setComments(final String comments) {
        this.comments = comments;
    }
    
    public static List<RestAccountInfo> convertToRestAccountInfo(final List<RestAccount> inputList, final RandomKeyMapping keyMapping) {
        if (inputList == null) {
            return null;
        }
        final List<RestAccountInfo> returnList = new ArrayList<RestAccountInfo>();
        
        for (RestAccount ra : inputList) {
            returnList.add(new RestAccountInfo(ra.getRestAccountType().getName(), ra.getAccountName(), ra.getSecretKey(), ra.getPointOfSale()
                    .getName(), keyMapping.getRandomString(ra, WebCoreConstants.ID_LOOK_UP_NAME), ra.getComments()));
        }
        
        return returnList;
    }
    
}
