package com.digitalpaytech.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

public class TransactionSearchCriteria implements Serializable {
    
    private static final long serialVersionUID = -1040749581082903660L;
    
    private Integer customerId;
    
    private Short cardLast4Digits;
    private Integer cardCheckSum;
    private String cardHash;
    
    private String cardType;
    private String authNumber;
    private Collection<Integer> merchantAccountIds;
    private Collection<Integer> pointOfSaleIds;
    private Date startDate;
    private Date endDate;
    
    public TransactionSearchCriteria() {
        
    }
    
    public final Integer getCustomerId() {
        return this.customerId;
    }
    
    public final void setCustomerId(final Integer customerId) {
        this.customerId = customerId;
    }
    
    public final Short getCardLast4Digits() {
        return this.cardLast4Digits;
    }
    
    public final void setCardLast4Digits(final Short cardLast4Digits) {
        this.cardLast4Digits = cardLast4Digits;
    }
    
    public final Integer getCardCheckSum() {
        return this.cardCheckSum;
    }
    
    public final void setCardCheckSum(final Integer cardCheckSum) {
        this.cardCheckSum = cardCheckSum;
    }
    
    public final String getCardHash() {
        return this.cardHash;
    }
    
    public final void setCardHash(final String cardHash) {
        this.cardHash = cardHash;
    }
    
    public final String getCardType() {
        return this.cardType;
    }
    
    public final void setCardType(final String cardType) {
        this.cardType = cardType;
    }
    
    public final String getAuthNumber() {
        return this.authNumber;
    }
    
    public final void setAuthNumber(final String authNumber) {
        this.authNumber = authNumber;
    }
    
    public final Collection<Integer> getMerchantAccountIds() {
        return this.merchantAccountIds;
    }
    
    public final void setMerchantAccountIds(final Collection<Integer> merchantAccountIds) {
        this.merchantAccountIds = merchantAccountIds;
    }
    
    public final Date getStartDate() {
        return this.startDate;
    }
    
    public final void setStartDate(final Date startDate) {
        this.startDate = startDate;
    }
    
    public final Date getEndDate() {
        return this.endDate;
    }
    
    public final void setEndDate(final Date endDate) {
        this.endDate = endDate;
    }
    
    public final Collection<Integer> getPointOfSaleIds() {
        return this.pointOfSaleIds;
    }
    
    public final void setPointOfSaleIds(final Collection<Integer> pointOfSaleIds) {
        this.pointOfSaleIds = pointOfSaleIds;
    }
}
