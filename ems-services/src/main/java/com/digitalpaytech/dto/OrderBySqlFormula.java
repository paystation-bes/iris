package com.digitalpaytech.dto;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Order;

public class OrderBySqlFormula extends Order {
    
    private static final long serialVersionUID = -4674897550026205586L;
    
    private String sqlFormula;
    
    protected OrderBySqlFormula(final String sqlFormula) {
        super(sqlFormula, true);
        this.sqlFormula = sqlFormula;
    }
    
    public final String toString() {
        return this.sqlFormula;
    }
    
    @Override
    public final String toSqlString(final Criteria criteria, final CriteriaQuery criteriaQuery) {
        return this.sqlFormula;
    }
    
    /**
     * Custom order
     * 
     * @param sqlFormula
     *            an SQL formula that will be appended to the resulting SQL query
     * @return Order
     */
    public static Order sqlFormula(final String sqlFormula) {
        return new OrderBySqlFormula(sqlFormula);
    }
}
