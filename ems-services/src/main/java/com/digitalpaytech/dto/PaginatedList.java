package com.digitalpaytech.dto;

import java.io.Serializable;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class PaginatedList<T> implements Serializable {
    private static final long serialVersionUID = 6026961990361770132L;
    
    private int page;
    
    private String token;
    private String dataKey;
    
    @XStreamImplicit(itemFieldName = "elements")
    private List<T> elements;
    
    public PaginatedList() {
        
    }
    
    public PaginatedList(final List<T> elements, final String dataKey, final String token) {
        this(elements, dataKey, token, 0);
    }
    
    public PaginatedList(final List<T> elements, final String dataKey, final String token, final int page) {
        this.elements = elements;
        this.dataKey = dataKey;
        this.token = token;
        this.page = page;
    }
    
    public final int getPage() {
        return this.page;
    }
    
    public final void setPage(final int page) {
        this.page = page;
    }
    
    public final String getToken() {
        return this.token;
    }
    
    public final void setToken(final String token) {
        this.token = token;
    }
    
    public final String getDataKey() {
        return this.dataKey;
    }
    
    public final void setDataKey(final String dataKey) {
        this.dataKey = dataKey;
    }
    
    public final List<T> getElements() {
        return this.elements;
    }
    
    public final void setElements(final List<T> elements) {
        this.elements = elements;
    }
}
