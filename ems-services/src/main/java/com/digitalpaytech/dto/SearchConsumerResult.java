package com.digitalpaytech.dto;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class SearchConsumerResult {
    @XStreamImplicit(itemFieldName = "searchConsumer")
    private List<SearchConsumerResultSetting> searchConsumer;
    
    public SearchConsumerResult() {
        this.searchConsumer = new ArrayList<SearchConsumerResultSetting>();
    }
    
    public final List<SearchConsumerResultSetting> getSearchConsumerResult() {
        return this.searchConsumer;
    }
    
    public final void addSearchConsumerResult(final SearchConsumerResultSetting searchConsumerResultSetting) {
        this.searchConsumer.add(searchConsumerResultSetting);
    }
}
