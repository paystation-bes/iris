package com.digitalpaytech.dto;

import java.util.ArrayList;
import java.util.List;

import com.digitalpaytech.dto.customeradmin.MobileDeviceInfo;

public class MobileSubscriptionInfo {
    private int licenseCount;
    private int licenseUsed;
    private List<MobileDeviceInfo> devices;
    
    public MobileSubscriptionInfo() {
        this.devices = new ArrayList<MobileDeviceInfo>();
    }
    
    public final int getLicenseCount() {
        return this.licenseCount;
    }
    
    public final void setLicenseCount(final int licenseCount) {
        this.licenseCount = licenseCount;
    }
    
    public final int getLicenseUsed() {
        return this.licenseUsed;
    }
    
    public final void setLicenseUsed(final int licenseUsed) {
        this.licenseUsed = licenseUsed;
    }
    
    public final List<MobileDeviceInfo> getDevices() {
        return this.devices;
    }
    
    public final void setDevices(final List<MobileDeviceInfo> devices) {
        this.devices = devices;
    }
    
    public final void addDevice(final MobileDeviceInfo device) {
        devices.add(device);
    }
}
