package com.digitalpaytech.dto;

import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;

public class SQLString {
    public static enum JoinType {
        INNER("INNER JOIN"), LEFT("LEFT JOIN"), RIGHT("RIGHT JOIN");
        
        private String joinStr;
        
        JoinType(final String joinStr) {
            this.joinStr = joinStr;
        }
        
        public String getJoinString() {
            return this.joinStr;
        }
    }
    
    public static enum BooleanConnector {
        AND, OR;
    }
    
    private StringBuilder select;
    private StringBuilder from;
    private StringBuilder where;
    private StringBuilder groupBy;
    private StringBuilder orderBy;
    
    public SQLString(final String mainTableExpr) {
        this.from = new StringBuilder(mainTableExpr);
    }
    
    public final void select(final String fieldName, final String alias) {
        if (this.select == null) {
            this.select = new StringBuilder("SELECT ");
        } else {
            this.select.append(StandardConstants.STRING_COMMA).append(WebCoreConstants.EMPTY_SPACE);
        }
        
        this.select.append(fieldName);
        this.select.append(" AS ");
        this.select.append(alias);
    }
    
    public final void innerJoin(final String joinTableAndAlias, final String conditions) {
        join(JoinType.INNER, joinTableAndAlias, conditions);
    }
    
    public final void leftJoin(final String joinTableAndAlias, final String conditions) {
        join(JoinType.INNER, joinTableAndAlias, conditions);
    }
    
    public final void rightJoin(final String joinTableAndAlias, final String conditions) {
        join(JoinType.INNER, joinTableAndAlias, conditions);
    }
    
    public final void join(final JoinType type, final String joinTableAndAlias, final String conditions) {
        this.from.append(" ");
        if (type == null) {
            this.from.append(JoinType.INNER.getJoinString());
        } else {
            this.from.append(type.getJoinString());
        }
        
        if ((conditions != null) && (conditions.length() > 0)) {
            this.from.append(" ON(");
            this.from.append(conditions);
            this.from.append(")");
        }
    }
    
    public final void where(final String conditionsExpr, final BooleanConnector connector) {
        if (this.where == null) {
            this.where = new StringBuilder(" WHERE ");
        } else {
            this.where.append((connector == null) ? BooleanConnector.AND : connector);
        }
        
        this.where.append(conditionsExpr);
    }
    
    public final void groupBy(final String fieldName) {
        if (this.groupBy == null) {
            this.groupBy = new StringBuilder(" GROUP BY");
        } else {
            this.groupBy.append(", ");
        }
        
        this.groupBy.append(fieldName);
    }
    
    public final void orderBy(final String fieldName) {
        if (this.orderBy == null) {
            this.orderBy = new StringBuilder(" ORDER BY");
        } else {
            this.orderBy.append(", ");
        }
        
        this.orderBy.append(fieldName);
    }
    
    @Override
    public final String toString() {
        final StringBuilder result = new StringBuilder();
        if (this.select == null) {
            result.append("SELECT 1");
        } else {
            result.append(this.select);
        }
        
        if (this.from != null) {
            result.append(this.from);
        }
        
        if (this.where != null) {
            result.append(this.where);
        }
        
        if (this.groupBy != null) {
            result.append(this.groupBy);
        }
        
        if (this.orderBy != null) {
            result.append(this.orderBy);
        }
        
        return result.toString();
    }
}
