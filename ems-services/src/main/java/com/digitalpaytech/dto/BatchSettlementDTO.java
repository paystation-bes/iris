package com.digitalpaytech.dto;

import java.util.List;

import com.digitalpaytech.domain.ActiveBatchSummary;
import com.digitalpaytech.domain.BatchSettlement;
import com.digitalpaytech.domain.BatchSettlementDetail;

public class BatchSettlementDTO {
    private BatchSettlement batchSettlement;
    private List<BatchSettlementDetail> batchSettlementDetails;
    private ActiveBatchSummary activeBatchSummary;
    
    public BatchSettlementDTO(final BatchSettlement batchSettlement, 
                              final List<BatchSettlementDetail> batchSettlementDetails,
                              final ActiveBatchSummary activeBatchSummary) {
        this.batchSettlement = batchSettlement;
        this.batchSettlementDetails = batchSettlementDetails;
        this.activeBatchSummary = activeBatchSummary;
    }
    
    public final BatchSettlement getBatchSettlement() {
        return this.batchSettlement;
    }
    public final void setBatchSettlement(final BatchSettlement batchSettlement) {
        this.batchSettlement = batchSettlement;
    }
    public final List<BatchSettlementDetail> getBatchSettlementDetails() {
        return this.batchSettlementDetails;
    }
    public final void setBatchSettlementDetails(final List<BatchSettlementDetail> batchSettlementDetails) {
        this.batchSettlementDetails = batchSettlementDetails;
    }
    public final ActiveBatchSummary getActiveBatchSummary() {
        return this.activeBatchSummary;
    }
    public final void setActiveBatchSummary(final ActiveBatchSummary activeBatchSummary) {
        this.activeBatchSummary = activeBatchSummary;
    }
}
