package com.digitalpaytech.dto;

import java.io.Serializable;

public class MobileLicenseInfo implements Serializable {
    
    private static final long serialVersionUID = -463776358739883049L;
    
    private String appType;
    private String deviceName;
    private String lastUsed;
    private String randomId;
    private String uid;
    
    public MobileLicenseInfo() {
        
    }
    
    public final String getUid() {
        return this.uid;
    }
    
    public final void setUid(final String uid) {
        this.uid = uid;
    }
    
    public final String getAppType() {
        return this.appType;
    }
    
    public final void setAppType(final String appType) {
        this.appType = appType;
    }
    
    public final String getDeviceName() {
        return this.deviceName;
    }
    
    public final void setDeviceName(final String deviceName) {
        this.deviceName = deviceName;
    }
    
    public final String getLastUsed() {
        return this.lastUsed;
    }
    
    public final void setLastUsed(final String lastUsed) {
        this.lastUsed = lastUsed;
    }
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
}
