package com.digitalpaytech.dto;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.transform.AliasToBeanResultTransformer;

import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.dto.customeradmin.CollectionSummaryEntry;
import com.digitalpaytech.util.support.WebObjectId;

public class CollectionSummaryEntryTransformer extends AliasToBeanResultTransformer {
    
    private static final long serialVersionUID = 2595126442386745959L;
    
    private Map<String, Integer> aliasIdxMap;
    
    private Map<Integer, CollectionSummaryEntry> entriesHash;
    
    public CollectionSummaryEntryTransformer() {
        super(CollectionSummaryEntry.class);
    }
    
    public CollectionSummaryEntryTransformer(final Map<Integer, CollectionSummaryEntry> entriesHash) {
        super(CollectionSummaryEntry.class);
        
        this.entriesHash = entriesHash;
    }
    
    @Override
    public final Object transformTuple(final Object[] tuple, final String[] aliases) {
        resolveIdx(tuple, aliases);
        
        final CollectionSummaryEntry result = new CollectionSummaryEntry();
        
        final Integer posId = (Integer) tuple[this.aliasIdxMap.get("psId")];
        result.setPosRandomId(new WebObjectId(PointOfSale.class, posId));
        final Integer locationId = (Integer) tuple[this.aliasIdxMap.get("locationId")];
        result.setLocationRandomId(new WebObjectId(Location.class, locationId));
        
        result.setSerial((String) tuple[this.aliasIdxMap.get("psSerialNumber")]);
        result.setPointOfSaleName((String) tuple[this.aliasIdxMap.get("pointOfSaleName")]);
        result.setLocationName((String) tuple[this.aliasIdxMap.get("locationName")]);
        
        result.setBillAmount((Integer) tuple[this.aliasIdxMap.get("billAmount")]);
        result.setCoinAmount((Integer) tuple[this.aliasIdxMap.get("coinAmount")]);
        result.setCardAmount((Integer) tuple[this.aliasIdxMap.get("cardAmount")]);
        result.setTotalAmount((Integer) tuple[this.aliasIdxMap.get("totalAmount")]);
        
        result.setBillCount((Short) tuple[this.aliasIdxMap.get("billCount")]);
        result.setCoinCount((Short) tuple[this.aliasIdxMap.get("coinCount")]);
        result.setCardCount((Short) tuple[this.aliasIdxMap.get("cardCount")]);
        
        if (this.entriesHash != null) {
            this.entriesHash.put(posId, result);
        }
        
        result.setCollectionDate((Date) tuple[this.aliasIdxMap.get("collectionDate")]);
        
        return result;
    }
    
    private void resolveIdx(final Object[] tuple, final String[] aliases) {
        if (this.aliasIdxMap == null) {
            int idx = aliases.length;
            this.aliasIdxMap = new HashMap<String, Integer>(idx * 2, 0.75F);
            while (--idx >= 0) {
                this.aliasIdxMap.put(aliases[idx], idx);
            }
        }
    }
    
    private int defaultIfNull(final Object[] tuple, final String propertyName, final int defaultValue) {
        int result = defaultValue;
        
        final Integer idx = this.aliasIdxMap.get(propertyName);
        if (idx != null) {
            if (tuple[idx] != null) {
                result = ((Number) tuple[idx]).intValue();
            } else {
                result = 0;
            }
        }
        
        return result;
    }
    
}
