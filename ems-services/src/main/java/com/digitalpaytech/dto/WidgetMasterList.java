package com.digitalpaytech.dto;

import java.util.ArrayList;
import java.util.List;

import com.digitalpaytech.util.WidgetConstants;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

public class WidgetMasterList {
    
    private List<ArrayList<WidgetMasterListInfo>> metricTypes;
    private String[] categoryNames;
    
    @XStreamOmitField
    private List<WidgetMasterListInfo> activeAlerts;
    @XStreamOmitField
    private List<WidgetMasterListInfo> revenue;
    @XStreamOmitField
    private List<WidgetMasterListInfo> collections;
    @XStreamOmitField
    private List<WidgetMasterListInfo> settledCard;
    @XStreamOmitField
    private List<WidgetMasterListInfo> purchases;
    @XStreamOmitField
    private List<WidgetMasterListInfo> occupancy;
    @XStreamOmitField
    private List<WidgetMasterListInfo> turnover;
    @XStreamOmitField
    private List<WidgetMasterListInfo> utilization;
    @XStreamOmitField
    private List<WidgetMasterListInfo> map;
    @XStreamOmitField
    private List<WidgetMasterListInfo> averagePrice;
    @XStreamOmitField
    private List<WidgetMasterListInfo> averageRevenue;
    @XStreamOmitField
    private List<WidgetMasterListInfo> averageDuration;
    @XStreamOmitField
    private List<WidgetMasterListInfo> cardPurchases;
    @XStreamOmitField
    private List<WidgetMasterListInfo> cardRevenue;
    @XStreamOmitField
    private List<WidgetMasterListInfo> citations;
    @XStreamOmitField
    private List<WidgetMasterListInfo> citationMaps;
    @XStreamOmitField
    private List<WidgetMasterListInfo> realOccupancies;
    @XStreamOmitField
    private List<WidgetMasterListInfo> compareOccupancies;
    @XStreamOmitField
    private List<WidgetMasterListInfo> occupancyMaps;
    
    public WidgetMasterList() {
        this.activeAlerts = new ArrayList<WidgetMasterListInfo>();
        this.averageDuration = new ArrayList<WidgetMasterListInfo>();
        this.averagePrice = new ArrayList<WidgetMasterListInfo>();
        this.averageRevenue = new ArrayList<WidgetMasterListInfo>();
        this.cardPurchases = new ArrayList<WidgetMasterListInfo>();
        this.cardRevenue = new ArrayList<WidgetMasterListInfo>();
        this.settledCard = new ArrayList<WidgetMasterListInfo>();
        this.collections = new ArrayList<WidgetMasterListInfo>();
        this.map = new ArrayList<WidgetMasterListInfo>();
        this.occupancy = new ArrayList<WidgetMasterListInfo>();
        this.purchases = new ArrayList<WidgetMasterListInfo>();
        this.revenue = new ArrayList<WidgetMasterListInfo>();
        this.turnover = new ArrayList<WidgetMasterListInfo>();
        this.utilization = new ArrayList<WidgetMasterListInfo>();
        this.citations = new ArrayList<WidgetMasterListInfo>();
        this.citationMaps = new ArrayList<WidgetMasterListInfo>();
        this.realOccupancies = new ArrayList<WidgetMasterListInfo>();
        this.compareOccupancies = new ArrayList<WidgetMasterListInfo>();
        this.occupancyMaps = new ArrayList<WidgetMasterListInfo>();
        
        this.metricTypes = new ArrayList<ArrayList<WidgetMasterListInfo>>();
        
        this.metricTypes.add((ArrayList<WidgetMasterListInfo>) (this.activeAlerts));
        this.metricTypes.add((ArrayList<WidgetMasterListInfo>) (this.averageDuration));
        this.metricTypes.add((ArrayList<WidgetMasterListInfo>) (this.averagePrice));
        this.metricTypes.add((ArrayList<WidgetMasterListInfo>) (this.averageRevenue));
        this.metricTypes.add((ArrayList<WidgetMasterListInfo>) (this.cardPurchases));
        this.metricTypes.add((ArrayList<WidgetMasterListInfo>) (this.cardRevenue));
        this.metricTypes.add((ArrayList<WidgetMasterListInfo>) (this.settledCard));
        this.metricTypes.add((ArrayList<WidgetMasterListInfo>) (this.collections));
        this.metricTypes.add((ArrayList<WidgetMasterListInfo>) (this.map));
        this.metricTypes.add((ArrayList<WidgetMasterListInfo>) (this.occupancy));
        this.metricTypes.add((ArrayList<WidgetMasterListInfo>) (this.purchases));
        this.metricTypes.add((ArrayList<WidgetMasterListInfo>) (this.revenue));
        this.metricTypes.add((ArrayList<WidgetMasterListInfo>) (this.turnover));
        this.metricTypes.add((ArrayList<WidgetMasterListInfo>) (this.utilization));
        this.metricTypes.add((ArrayList<WidgetMasterListInfo>) (this.citations));
        this.metricTypes.add((ArrayList<WidgetMasterListInfo>) (this.citationMaps));
        this.metricTypes.add((ArrayList<WidgetMasterListInfo>) (this.realOccupancies));
        this.metricTypes.add((ArrayList<WidgetMasterListInfo>) (this.compareOccupancies));
        this.metricTypes.add((ArrayList<WidgetMasterListInfo>) (this.occupancyMaps));
        
    }
    
    @SuppressWarnings({ "checkstyle:cyclomaticcomplexity" })
    public final void add(final WidgetMasterListInfo masterListInfo) {
        
        switch (masterListInfo.getMetricType()) {
            case WidgetConstants.METRIC_TYPE_REVENUE:
                this.addRevenue(masterListInfo);
                break;
            case WidgetConstants.METRIC_TYPE_COLLECTIONS:
                this.addCollections(masterListInfo);
                break;
            case WidgetConstants.METRIC_TYPE_SETTLED_CARD:
                this.addSettledCard(masterListInfo);
                break;
            case WidgetConstants.METRIC_TYPE_CARD_PROCESSING_REV:
                this.addCardRevenue(masterListInfo);
                break;
            case WidgetConstants.METRIC_TYPE_CARD_PROCESSING_TX:
                this.addCardPurchases(masterListInfo);
                break;
            case WidgetConstants.METRIC_TYPE_PURCHASES:
                this.addPurchases(masterListInfo);
                break;
            case WidgetConstants.METRIC_TYPE_PAID_OCCUPANCY:
                this.addOccupancy(masterListInfo);
                break;
            case WidgetConstants.METRIC_TYPE_TURNOVER:
                this.addTurnover(masterListInfo);
                break;
            case WidgetConstants.METRIC_TYPE_UTILIZATION:
                this.addUtilization(masterListInfo);
                break;
            case WidgetConstants.METRIC_TYPE_ACTIVE_ALERTS:
                this.addActiveAlerts(masterListInfo);
                break;
            case WidgetConstants.METRIC_TYPE_MAP:
                this.addMap(masterListInfo);
                break;
            case WidgetConstants.METRIC_TYPE_AVERAGE_PRICE:
                this.addAveragePrice(masterListInfo);
                break;
            case WidgetConstants.METRIC_TYPE_AVERAGE_REVENUE:
                this.addAverageRevenue(masterListInfo);
                break;
            case WidgetConstants.METRIC_TYPE_AVERAGE_DURATION:
                this.addAverageDuration(masterListInfo);
                break;
            case WidgetConstants.METRIC_TYPE_CITATIONS:
                this.addCitation(masterListInfo);
                break;
            case WidgetConstants.METRIC_TYPE_CITATION_MAP:
                this.addCitationMap(masterListInfo);
                break;
            case WidgetConstants.METRIC_TYPE_PHYSICAL_OCCUPANCY:
                this.addRealOccupancy(masterListInfo);
                break;
            case WidgetConstants.METRIC_TYPE_COMPARE_OCCUPANCY:
                this.addCompareOccupancy(masterListInfo);
                break;
            case WidgetConstants.METRIC_TYPE_OCCUPANCY_MAP:
                this.addOccupancyMap(masterListInfo);
                break;
            default:
                break;
        }
    }
    
    public final List<WidgetMasterListInfo> getRevenue() {
        return this.revenue;
    }
    
    public final void setRevenue(final List<WidgetMasterListInfo> revenue) {
        this.revenue = revenue;
    }
    
    public final void addRevenue(final WidgetMasterListInfo newRevenue) {
        this.revenue.add(newRevenue);
    }
    
    public final List<WidgetMasterListInfo> getCollections() {
        return this.collections;
    }
    
    public final void setCollections(final List<WidgetMasterListInfo> collections) {
        this.collections = collections;
    }
    
    public final void addCollections(final WidgetMasterListInfo collection) {
        this.collections.add(collection);
    }
    
    public final List<WidgetMasterListInfo> getSettledCard() {
        return this.settledCard;
    }
    
    public final void setSettledCard(final List<WidgetMasterListInfo> settledCard) {
        this.settledCard = settledCard;
    }
    
    public final void addSettledCard(final WidgetMasterListInfo newSettledCard) {
        this.settledCard.add(newSettledCard);
    }
    
    public final List<WidgetMasterListInfo> getPurchases() {
        return this.purchases;
    }
    
    public final void setPurchases(final List<WidgetMasterListInfo> purchases) {
        this.purchases = purchases;
    }
    
    public final void addPurchases(final WidgetMasterListInfo purchase) {
        this.purchases.add(purchase);
    }
    
    public final List<WidgetMasterListInfo> getOccupancy() {
        return this.occupancy;
    }
    
    public final void setOccupancy(final List<WidgetMasterListInfo> occupancy) {
        this.occupancy = occupancy;
    }
    
    public final void addOccupancy(final WidgetMasterListInfo newOccupancy) {
        this.occupancy.add(newOccupancy);
    }
    
    public final List<WidgetMasterListInfo> getTurnover() {
        return this.turnover;
    }
    
    public final void setTurnover(final List<WidgetMasterListInfo> turnover) {
        this.turnover = turnover;
    }
    
    public final void addTurnover(final WidgetMasterListInfo newTurnover) {
        this.turnover.add(newTurnover);
    }
    
    public final List<WidgetMasterListInfo> getUtilization() {
        return this.utilization;
    }
    
    public final void setUtilization(final List<WidgetMasterListInfo> utilization) {
        this.utilization = utilization;
    }
    
    public final void addUtilization(final WidgetMasterListInfo newUtilization) {
        this.utilization.add(newUtilization);
    }
    
    public final List<WidgetMasterListInfo> getActiveAlerts() {
        return this.activeAlerts;
    }
    
    public final void setActiveAlerts(final List<WidgetMasterListInfo> activeAlerts) {
        this.activeAlerts = activeAlerts;
    }
    
    public final void addActiveAlerts(final WidgetMasterListInfo activeAlert) {
        this.activeAlerts.add(activeAlert);
    }
    
    public final List<WidgetMasterListInfo> getMap() {
        return this.map;
    }
    
    public final void setMap(final List<WidgetMasterListInfo> map) {
        this.map = map;
    }
    
    public final void addMap(final WidgetMasterListInfo newMap) {
        this.map.add(newMap);
    }
    
    public final List<WidgetMasterListInfo> getAveragePrice() {
        return this.averagePrice;
    }
    
    public final void setAveragePrice(final List<WidgetMasterListInfo> averagePrice) {
        this.averagePrice = averagePrice;
    }
    
    public final void addAveragePrice(final WidgetMasterListInfo newAveragePrice) {
        this.averagePrice.add(newAveragePrice);
    }
    
    public final List<WidgetMasterListInfo> getAverageRevenue() {
        return this.averageRevenue;
    }
    
    public final void setAverageRevenue(final List<WidgetMasterListInfo> averageRevenue) {
        this.averageRevenue = averageRevenue;
    }
    
    public final void addAverageRevenue(final WidgetMasterListInfo newAverageRevenue) {
        this.averageRevenue.add(newAverageRevenue);
    }
    
    public final List<WidgetMasterListInfo> getAverageDuration() {
        return this.averageDuration;
    }
    
    public final void setAverageDuration(final List<WidgetMasterListInfo> averageDuration) {
        this.averageDuration = averageDuration;
    }
    
    public final void addAverageDuration(final WidgetMasterListInfo newAverageDuration) {
        this.averageDuration.add(newAverageDuration);
    }
    
    public final List<WidgetMasterListInfo> getCardPurchases() {
        return this.cardPurchases;
    }
    
    public final void setCardPurchases(final List<WidgetMasterListInfo> cardPurchases) {
        this.cardPurchases = cardPurchases;
    }
    
    public final void addCardPurchases(final WidgetMasterListInfo cardPurchase) {
        this.cardPurchases.add(cardPurchase);
    }
    
    public final List<WidgetMasterListInfo> getCardRevenue() {
        return this.cardRevenue;
    }
    
    public final void setCardRevenue(final List<WidgetMasterListInfo> cardRevenue) {
        this.cardRevenue = cardRevenue;
    }
    
    public final void addCardRevenue(final WidgetMasterListInfo newCardRevenue) {
        this.cardRevenue.add(newCardRevenue);
    }
    
    public final String[] getCategoryNames() {
        return this.categoryNames;
    }
    
    public final void setCategoryNames(final String[] categoryNames) {
        this.categoryNames = categoryNames;
    }
    
    public final void setCitations(final List<WidgetMasterListInfo> citations) {
        this.citations = citations;
    }
    
    public final void addCitation(final WidgetMasterListInfo citation) {
        this.citations.add(citation);
    }
    
    public final List<WidgetMasterListInfo> getCitationMaps() {
        return this.citationMaps;
    }
    
    public final void setCitationMaps(final List<WidgetMasterListInfo> citationMaps) {
        this.citationMaps = citationMaps;
    }
    
    public final void addCitationMap(final WidgetMasterListInfo citation) {
        this.citationMaps.add(citation);
    }
    
    public final List<WidgetMasterListInfo> getRealOccupancies() {
        return this.realOccupancies;
    }
    
    public final void setRealOccupancies(final List<WidgetMasterListInfo> realOccupancies) {
        this.realOccupancies = realOccupancies;
    }
    
    public final void addRealOccupancy(final WidgetMasterListInfo realOccupancy) {
        this.realOccupancies.add(realOccupancy);
    }
    
    public final List<WidgetMasterListInfo> getCompareOccupancies() {
        return this.compareOccupancies;
    }
    
    public final void setCompareOccupancies(final List<WidgetMasterListInfo> compareOccupancies) {
        this.compareOccupancies = compareOccupancies;
    }
    
    public final void addCompareOccupancy(final WidgetMasterListInfo compareOccupancy) {
        this.compareOccupancies.add(compareOccupancy);
    }
    
    public final List<WidgetMasterListInfo> getOccupancyMaps() {
        return this.occupancyMaps;
    }
    
    public final void setOccupancyMaps(final List<WidgetMasterListInfo> occupancyMaps) {
        this.occupancyMaps = occupancyMaps;
    }
    
    public final void addOccupancyMap(final WidgetMasterListInfo singleOccupancyMap) {
        this.occupancyMaps.add(singleOccupancyMap);
    }
    
}
