package com.digitalpaytech.dto;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("metrics")
public class WidgetMetrics {
    
    @XStreamImplicit(itemFieldName = "metric")
    private List<Metric> metrics;
    
    private Map<String, Metric> metricsMap;
    
    public WidgetMetrics() {
    }
    
    private void configMetricsMap() {
        if (this.metrics != null) {
            this.metricsMap = new HashMap<String, Metric>(this.metrics.size());
            final Iterator<Metric> iter = this.metrics.iterator();
            while (iter.hasNext()) {
                final Metric metric = iter.next();
                this.metricsMap.put(metric.getName(), metric);
            }
        }
    }
    
    public final Metric getMetric(final String metricName) {
        if (this.metricsMap == null) {
            configMetricsMap();
        }
        return this.metricsMap.get(metricName);
    }
    
    public final List<Metric> getMetrics() {
        return this.metrics;
    }
    
    public final void setMetrics(final List<Metric> metrics) {
        this.metrics = metrics;
    }
}
