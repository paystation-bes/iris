package com.digitalpaytech.dto.emv.impl;

import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.dto.cps.CPSTransactionDataType;
import com.digitalpaytech.dto.cps.CPSTransactionDto;

public class CreditcallTransactionDataTypeImpl implements CPSTransactionDataType {
    private final CPSTransactionDto cpsTransactionDto;
    
    public CreditcallTransactionDataTypeImpl(final CPSTransactionDto cpsTransactionDto) {
        this.cpsTransactionDto = cpsTransactionDto;
    }
    
    @Override
    public final MerchantAccount getMerchantAccount() {
        return this.cpsTransactionDto.getMerchantAccount();
    }
    
    @Override
    public final int getProcessorId() {
        return this.getMerchantAccount().getProcessor().getId();
    }
    
    @Override
    public final CPSTransactionDto getCPSTransactionDto() {
        return this.cpsTransactionDto;
    }
}
