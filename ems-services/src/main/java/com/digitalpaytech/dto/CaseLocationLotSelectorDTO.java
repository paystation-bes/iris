package com.digitalpaytech.dto;

import java.io.Serializable;

public class CaseLocationLotSelectorDTO implements Serializable {
    
    private static final long serialVersionUID = -1728492057557663384L;
    
    private String randomId;
    private String lotName;
    private String status;
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final String getLotName() {
        return this.lotName;
    }
    
    public final void setLotName(final String lotName) {
        this.lotName = lotName;
    }
    
    public final String getStatus() {
        return this.status;
    }
    
    public final void setStatus(final String status) {
        this.status = status;
    }
    
}
