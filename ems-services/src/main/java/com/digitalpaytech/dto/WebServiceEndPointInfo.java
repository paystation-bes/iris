package com.digitalpaytech.dto;

import java.util.ArrayList;
import java.util.List;

import com.digitalpaytech.domain.WebServiceEndPoint;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.WebCoreConstants;

public class WebServiceEndPointInfo {
    
    private String type;
    private String key;
    private String randomId;
    private String comments;
    public WebServiceEndPointInfo() {
        
    }
    
    public WebServiceEndPointInfo(final String type, final String key) {
        this.type = type;
        this.key = key;
    }
    
    public WebServiceEndPointInfo(final String type, final String key, final String randomId) {
        this.type = type;
        this.key = key;
        this.randomId = randomId;
    }
    
    public WebServiceEndPointInfo(final String type, final String key, final String randomId, final String comments) {
        this.type = type;
        this.key = key;
        this.randomId = randomId;
        this.comments = comments;
    }
    
    public final String getType() {
        return this.type;
    }
    
    public final void setType(final String type) {
        this.type = type;
    }
    
    public final String getKey() {
        return this.key;
    }
    
    public final void setKey(final String key) {
        this.key = key;
    }
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final String getComments(){
    	return this.comments;
    }
    
    public final void setComments(final String comments){
    	this.comments = comments;
    }
    
    public static List<WebServiceEndPointInfo> convertToWebServiceEndPointInfo(final List<WebServiceEndPoint> inputList, final RandomKeyMapping keyMapping) {
        if (inputList == null) {
            return null;
        }
        
        final List<WebServiceEndPointInfo> returnList = new ArrayList<WebServiceEndPointInfo>();
        
        for (WebServiceEndPoint wsep : inputList) {
            returnList.add(new WebServiceEndPointInfo(wsep.getWebServiceEndPointType().getName(), wsep.getToken(), keyMapping
                    .getRandomString(wsep, WebCoreConstants.ID_LOOK_UP_NAME), wsep.getComments()));
        }
        
        return returnList;
    }
    
}
