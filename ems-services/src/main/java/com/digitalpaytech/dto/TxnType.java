package com.digitalpaytech.dto;

import com.digitalpaytech.util.WebCoreConstants;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("txnType")
public class TxnType extends SettingsType {
    private static final long serialVersionUID = 5939876235348981746L;
    
    public TxnType() {
        setStatus(WebCoreConstants.ACTIVE);
    }
}
