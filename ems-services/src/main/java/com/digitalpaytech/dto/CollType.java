package com.digitalpaytech.dto;

import com.digitalpaytech.util.WebCoreConstants;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("CollType")
public class CollType extends SettingsType {
    
    private static final long serialVersionUID = -4485458915672311184L;
    
    private int level;
    private String parentRandomId;
    
    public CollType() {
        setStatus(WebCoreConstants.ACTIVE);
    }
    
    public final int getLevel() {
        return this.level;
    }
    
    public final void setLevel(final int level) {
        this.level = level;
    }
    
    public final String getParentRandomId() {
        return this.parentRandomId;
    }
    
    public final void setParentRandomId(final String parentRandomId) {
        this.parentRandomId = parentRandomId;
    }
}
