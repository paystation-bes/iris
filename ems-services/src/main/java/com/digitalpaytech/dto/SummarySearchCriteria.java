package com.digitalpaytech.dto;


public class SummarySearchCriteria extends ActiveAlertSearchCriteria {
    
    private static final long serialVersionUID = 1941026903785665485L;
    private String exclusiveOrderBy;    
    private String exclusiveOrderDir;        
    
    
    public SummarySearchCriteria() {
        
    }             
    
    public final String getExclusiveOrderBy() {
        return this.exclusiveOrderBy;
    }

    public final void setExclusiveOrderBy(final String exclusiveOrderBy) {
        this.exclusiveOrderBy = exclusiveOrderBy;
    }

    public final String getExclusiveOrderDir() {
        return this.exclusiveOrderDir;
    }

    public final void setExclusiveOrderDir(final String exclusiveOrderDir) {
        this.exclusiveOrderDir = exclusiveOrderDir;
    }     

}
