package com.digitalpaytech.dto;

import java.io.Serializable;

import com.digitalpaytech.util.support.WebObjectId;

public class FlexLocationPropertySelectorDTO implements Serializable {

    private static final long serialVersionUID = -1728492057557663384L;
    
    private WebObjectId randomId;
    private int proUid;
    private String proName;
    private String status;
    
    public WebObjectId getRandomId() {
        return randomId;
    }
    public void setRandomId(WebObjectId randomId) {
        this.randomId = randomId;
    }
    public int getProUid() {
        return proUid;
    }
    public void setProUid(int i) {
        this.proUid = i;
    }
    public String getProName() {
        return proName;
    }
    public void setProName(String proName) {
        this.proName = proName;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

}
