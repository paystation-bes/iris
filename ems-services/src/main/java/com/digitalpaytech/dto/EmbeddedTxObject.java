package com.digitalpaytech.dto;

public interface EmbeddedTxObject {
    boolean isCPSStoreForward();
}
