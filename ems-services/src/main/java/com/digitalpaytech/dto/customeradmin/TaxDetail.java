package com.digitalpaytech.dto.customeradmin;

import java.io.Serializable;

public class TaxDetail implements Serializable {
    private static final long serialVersionUID = -3599129864357555247L;
    
    private String name;
    private String amount;
    private String rate;
    
    public TaxDetail() {
        
    }
    
    public TaxDetail(final String name, final String amount, final String rate) {
        this.name = name;
        this.amount = amount;
        this.rate = rate;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getAmount() {
        return this.amount;
    }
    
    public void setAmount(final String amount) {
        this.amount = amount;
    }
    
    public String getRate() {
        return this.rate;
    }
    
    public void setRate(final String rate) {
        this.rate = rate;
    }
}
