package com.digitalpaytech.dto.customeradmin;

import java.math.BigDecimal;


public class PlacementMapBordersInfo {
    
    private BigDecimal minLatitude;
    private BigDecimal maxLatitude;
    private BigDecimal minLongitude;
    private BigDecimal maxLongitude;

    public PlacementMapBordersInfo() {
        
    }
    
    public PlacementMapBordersInfo(BigDecimal minLatitude, BigDecimal maxLatitude, BigDecimal minLongitude, BigDecimal maxLongitude) {
        super();
        this.minLatitude = minLatitude;
        this.maxLatitude = maxLatitude;
        this.minLongitude = minLongitude;
        this.maxLongitude = maxLongitude;
    }
    
    public BigDecimal getMinLatitude() {
        return minLatitude;
    }
    public void setMinLatitude(BigDecimal minLatitude) {
        this.minLatitude = minLatitude;
    }
    public BigDecimal getMaxLatitude() {
        return maxLatitude;
    }
    public void setMaxLatitude(BigDecimal maxLatitude) {
        this.maxLatitude = maxLatitude;
    }
    public BigDecimal getMinLongitude() {
        return minLongitude;
    }
    public void setMinLongitude(BigDecimal minLongitude) {
        this.minLongitude = minLongitude;
    }
    public BigDecimal getMaxLongitude() {
        return maxLongitude;
    }
    public void setMaxLongitude(BigDecimal maxLongitude) {
        this.maxLongitude = maxLongitude;
    }

}