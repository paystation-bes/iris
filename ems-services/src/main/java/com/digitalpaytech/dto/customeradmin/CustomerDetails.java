package com.digitalpaytech.dto.customeradmin;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerAgreement;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.dto.RestAccountInfo;
import com.digitalpaytech.dto.WebServiceEndPointInfo;
import com.digitalpaytech.util.WebCoreConstants;

public class CustomerDetails {
    
    private Customer customer;
    private Customer parentCustomer;
    private CustomerAgreement customerAgreement;
    private List<CustomerSubscription> customerSubscriptions;
    private List<MerchantAccount> merchantAccounts;
    private List<MerchantAccount> creditCardMerchantAccounts;
    private List<MerchantAccount> valueCardMerchantAccounts;
    private List<WebServiceEndPointInfo> webServiceEndPoints;
    private List<WebServiceEndPointInfo> privateWebServiceEndPoints;
    private List<RestAccountInfo> restAccounts;
    private List<Customer> childCustomers;
    private boolean isMetric;
    private transient Set<Integer> validSubScriptions;
    private String timeZone;
    private Date trialExpiryLocal;
    
    public CustomerDetails() {
    }
    
    public CustomerDetails(final Customer customer, final Customer parentCustomer, final CustomerAgreement customerAgreement,
            final List<CustomerSubscription> customerSubscriptions, final List<MerchantAccount> merchantAccounts,
            final List<WebServiceEndPointInfo> webServiceEndPoints, final List<WebServiceEndPointInfo> privateWebServiceEndPoints,
            final List<RestAccountInfo> restAccounts, final List<Customer> childCustomers, final boolean isMetric) {
        
        this.customer = customer;
        this.parentCustomer = parentCustomer;
        this.customerAgreement = customerAgreement;
        this.customerSubscriptions = customerSubscriptions;
        this.merchantAccounts = merchantAccounts;
        this.webServiceEndPoints = webServiceEndPoints;
        this.privateWebServiceEndPoints = privateWebServiceEndPoints;
        this.restAccounts = restAccounts;
        this.childCustomers = childCustomers;
        this.isMetric = isMetric;
        
        updateValidSubscriptions();
    }
    
    public final Customer getCustomer() {
        return this.customer;
    }
    
    public final Customer getParentCustomer() {
        return this.parentCustomer;
    }
    
    public final CustomerAgreement getCustomerAgreement() {
        return this.customerAgreement;
    }
    
    public final void setCustomerAgreement(final CustomerAgreement customerAgreement) {
        this.customerAgreement = customerAgreement;
    }
    
    public final List<CustomerSubscription> getCustomerSubscriptions() {
        return this.customerSubscriptions;
    }
    
    public final void setCustomerSubscriptions(final List<CustomerSubscription> customerSubscriptions) {
        this.customerSubscriptions = customerSubscriptions;
    }
    
    public final List<MerchantAccount> getMerchantAccounts() {
        return this.merchantAccounts;
    }
    
    public final void setMerchantAccounts(final List<MerchantAccount> merchantAccounts) {
        this.merchantAccounts = merchantAccounts;
    }
    
    public final List<WebServiceEndPointInfo> getWebServiceEndPoints() {
        return this.webServiceEndPoints;
    }
    
    public final void setWebServiceEndPoints(final List<WebServiceEndPointInfo> webServiceEndPoints) {
        this.webServiceEndPoints = webServiceEndPoints;
    }
    
    public final List<WebServiceEndPointInfo> getPrivateWebServiceEndPoints() {
        return this.privateWebServiceEndPoints;
    }
    
    public final void setPrivateWebServiceEndPoints(final List<WebServiceEndPointInfo> privateWebServiceEndPoints) {
        this.privateWebServiceEndPoints = privateWebServiceEndPoints;
    }
    
    public final List<RestAccountInfo> getRestAccounts() {
        return this.restAccounts;
    }
    
    public final void setRestAccounts(final List<RestAccountInfo> restAccounts) {
        this.restAccounts = restAccounts;
    }
    
    public final List<Customer> getChildCustomers() {
        return this.childCustomers;
    }
    
    public final void setChildCustomers(final List<Customer> childCustomers) {
        this.childCustomers = childCustomers;
    }
    
    public final boolean hasSubscription(final int subscriptionTypeId) {
        return this.validSubScriptions.contains(subscriptionTypeId);
    }
    
    public final boolean hasMobileSubscription() {
        return this.validSubScriptions.contains(WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_COLLECT)
               || this.validSubScriptions.contains(WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_MAINTAIN)
               || this.validSubScriptions.contains(WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_PATROL);
    }
    
    public final List<MerchantAccount> getCreditCardMerchantAccounts() {
        return this.creditCardMerchantAccounts;
    }
    
    public final void setCreditCardMerchantAccounts(final List<MerchantAccount> creditCardMerchantAccounts) {
        this.creditCardMerchantAccounts = creditCardMerchantAccounts;
    }
    
    public final List<MerchantAccount> getValueCardMerchantAccounts() {
        return this.valueCardMerchantAccounts;
    }
    
    public final void setValueCardMerchantAccounts(final List<MerchantAccount> valueCardMerchantAccounts) {
        this.valueCardMerchantAccounts = valueCardMerchantAccounts;
    }
    
    private void updateValidSubscriptions() {
        Set<CustomerSubscription> subsList = this.customer.getCustomerSubscriptions();
        final Set<Integer> childSubsList = new HashSet<Integer>(subsList.size());
        for (CustomerSubscription sub : subsList) {
            if (sub.isIsEnabled()) {
                childSubsList.add(sub.getSubscriptionType().getId());
            }
        }
        
        if (this.parentCustomer == null) {
            this.parentCustomer = this.customer.getParentCustomer();
        }
        
        if (this.parentCustomer == null) {
            this.validSubScriptions = childSubsList;
        } else {
            subsList = this.parentCustomer.getCustomerSubscriptions();
            
            this.validSubScriptions = new HashSet<Integer>(subsList.size());
            for (CustomerSubscription sub : subsList) {
                if (sub.isIsEnabled() && childSubsList.contains(sub.getSubscriptionType().getId())) {
                    this.validSubScriptions.add(sub.getSubscriptionType().getId());
                }
            }
        }
    }
    
    public final String getTimeZone() {
        return this.timeZone;
    }
    
    public final void setTimeZone(final String timeZone) {
        this.timeZone = timeZone;
    }
    
    public final void setIsMetric(final boolean isMetric) {
        this.isMetric = isMetric;
    }
    
    public final boolean getIsMetric() {
        return this.isMetric;
    }
    
    public final Date getTrialExpiryLocal() {
        return this.trialExpiryLocal;
    }
    
    public final void setTrialExpiryLocal(final Date trialExpiryLocal) {
        this.trialExpiryLocal = trialExpiryLocal;
    }
}
