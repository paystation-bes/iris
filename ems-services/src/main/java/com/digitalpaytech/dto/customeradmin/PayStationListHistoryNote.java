package com.digitalpaytech.dto.customeradmin;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias(value = "payStationListHistoryNote")
public class PayStationListHistoryNote {
    
    private String user;
    private String comment;
    private String date;
    
    public PayStationListHistoryNote() {
        
    }
    
    public PayStationListHistoryNote(String user, String comment, String date) {
        this.user = user;
        this.comment = comment;
        this.date = date;
    }
    
    public String getUser() {
        return user;
    }
    
    public void setUser(String user) {
        this.user = user;
    }
    
    public String getComment() {
        return comment;
    }
    
    public void setComment(String comment) {
        this.comment = comment;
    }
    
    public String getDate() {
        return date;
    }
    
    public void setDate(String date) {
        this.date = date;
    }
}
