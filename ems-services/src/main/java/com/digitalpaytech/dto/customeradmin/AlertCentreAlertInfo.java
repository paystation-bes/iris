package com.digitalpaytech.dto.customeradmin;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.digitalpaytech.domain.EventDeviceType;
import com.digitalpaytech.domain.EventType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.domain.PosEventHistory;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.support.RelativeDateTime;
import com.digitalpaytech.util.support.WebObjectId;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

public class AlertCentreAlertInfo implements Serializable {
    private static final long serialVersionUID = 5688353802944382570L;
    
    private String pointOfSaleName;
    private Integer payStationType;
    private String serial;
    private WebObjectId posRandomId;
    private WebObjectId alertRandomId;
    
    private String location;
    @XStreamImplicit(itemFieldName = "route")
    private List<String> route;
    
    private RelativeDateTime alertDate;
    private RelativeDateTime resolvedDate;
    private String duration;
    
    private BigDecimal longitude;
    private BigDecimal latitude;
    
    private String module;
    private int numberOfAlerts;
    private int severity;
    private String alertMessage;
    
    private boolean isClearedByUser;
    private boolean isClearable;
    
    private String lastModifiedByUserName;
    
    @XStreamOmitField
    //Used for date comparator
    private Date date;
    
    public AlertCentreAlertInfo() {
        this.route = new ArrayList<String>();
    }
    
    public AlertCentreAlertInfo(final PosEventCurrent posEventCurrent, final String alertMessage, final int highestSeverity, final String timeZone,
            final Date date, final RandomKeyMapping keyMapping, final MessageHelper messageHelper, final boolean isClearable) {
        
        this.severity = highestSeverity;
        
        if (posEventCurrent.getEventType().getId() == WebCoreConstants.EVENT_TYPE_CUSTOMERDEFINED_ALERT) {
            this.module = posEventCurrent.getCustomerAlertType().getName();
            this.alertMessage = posEventCurrent.getCustomerAlertType().getName();
        } else {
            this.module = messageHelper.getMessage(posEventCurrent.getEventType().getEventDeviceType().getName());
            this.alertMessage = alertMessage;
        }
        
        this.alertDate = new RelativeDateTime(date, timeZone);
        this.date = date;
        this.alertRandomId = new WebObjectId(PosEventCurrent.class, posEventCurrent.getId());
        this.isClearable = isClearable;
    }
    
    // For Resolved Alerts Detail
    public AlertCentreAlertInfo(final PosEventHistory posEventHistory, final PosEventHistory posEventHistoryClear, final String alertMessage,
            final String timeZone, final List<Route> routeList, final MessageHelper messageHelper, final String lastModifiedByUserName) {
        
        this.route = new ArrayList<String>();
        
        final PointOfSale pointOfSale = posEventHistory.getPointOfSale();
        
        this.pointOfSaleName = pointOfSale.getName();
        this.payStationType = pointOfSale.getPaystation().getPaystationType().getId();
        this.serial = pointOfSale.getSerialNumber();
        
        this.location = pointOfSale.getLocation().getName();
        
        for (Route newRoute : routeList) {
            if (newRoute.getRouteType().getId() == WebCoreConstants.ROUTE_TYPE_MAINTENANCE) {
                this.addRoute(newRoute.getName());
            }
        }
        
        final EventType eventType = posEventHistory.getEventType();
        final EventDeviceType eventDeviceType = eventType.getEventDeviceType();
        if (eventDeviceType.getId() == WebCoreConstants.EVENT_DEVICE_TYPE_ALERT) {
            this.module = posEventHistory.getCustomerAlertType().getName();
        } else {
            this.module = messageHelper.getMessage(eventDeviceType.getName());
        }
        
        if (eventDeviceType.getId() == WebCoreConstants.EVENT_DEVICE_TYPE_ALERT) {
            this.alertMessage = posEventHistory.getCustomerAlertType().getName();
        } else {
            this.alertMessage = alertMessage;
        }
        this.alertDate = new RelativeDateTime(posEventHistory.getTimestampGmt(), timeZone);
        this.resolvedDate = new RelativeDateTime(posEventHistoryClear.getTimestampGmt(), timeZone);
        this.duration = DateUtil.calculateDuration(posEventHistory.getTimestampGmt(), posEventHistoryClear.getTimestampGmt());
        this.isClearedByUser = posEventHistoryClear.getLastModifiedByUserId() != WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID;
        this.setLastModifiedByUserName(lastModifiedByUserName);
        
    }
    
    public AlertCentreAlertInfo(final AlertInfoEntry alertInfo) {
        final BigInteger id = RandomKeyMapping.pack((Long) alertInfo.getClearId().getId(), (Long) alertInfo.getAlertId().getId());
        this.alertRandomId = new WebObjectId(PosEventHistory.class, id);
        this.posRandomId = alertInfo.getPosRandomId();
        
        this.alertMessage = alertInfo.getAlertMessage();
        this.pointOfSaleName = alertInfo.getPointOfSaleName();
        this.severity = alertInfo.getSeverity();
        this.alertDate = alertInfo.getAlertDate();
        this.resolvedDate = alertInfo.getResolvedDate();
        this.duration = alertInfo.getDuration();
        this.module = alertInfo.getModule();
        this.serial = alertInfo.getSerialNumber();
        this.latitude = alertInfo.getLatitude();
        this.longitude = alertInfo.getLongitude();
        this.payStationType = alertInfo.getPayStationType();
        
        if ((this.alertDate != null) && (this.resolvedDate != null)) {
            this.duration = DateUtil.calculateDuration(this.alertDate.getDate(), this.resolvedDate.getDate());
        }
        
        this.lastModifiedByUserName = alertInfo.getLastModifiedByUserName();
    }
    
    public final String getPointOfSaleName() {
        return this.pointOfSaleName;
    }
    
    public final void setPointOfSaleName(final String pointOfSaleName) {
        this.pointOfSaleName = pointOfSaleName;
    }
    
    public final Integer getPayStationType() {
        return this.payStationType;
    }
    
    public final void setPayStationType(final Integer payStationType) {
        this.payStationType = payStationType;
    }
    
    public final WebObjectId gePostRandomId() {
        return this.posRandomId;
    }
    
    public final void setPosRandomId(final WebObjectId posRandomId) {
        this.posRandomId = posRandomId;
    }
    
    public final WebObjectId geAlertRandomId() {
        return this.alertRandomId;
    }
    
    public final void setAlertRandomId(final WebObjectId alertRandomId) {
        this.alertRandomId = alertRandomId;
    }
    
    public final String getSerial() {
        return this.serial;
    }
    
    public final void setSerial(final String serial) {
        this.serial = serial;
    }
    
    public final String getLocation() {
        return this.location;
    }
    
    public final void setLocation(final String location) {
        this.location = location;
    }
    
    public final List<String> getRoute() {
        return this.route;
    }
    
    public final void setRoute(final List<String> route) {
        this.route = route;
    }
    
    public final void addRoute(final String newRoute) {
        this.route.add(newRoute);
    }
    
    public final RelativeDateTime getAlertDate() {
        return this.alertDate;
    }
    
    public final void setAlertDate(final RelativeDateTime alertDate) {
        this.alertDate = alertDate;
    }
    
    public final BigDecimal getLongitude() {
        return this.longitude;
    }
    
    public final void setLongitude(final BigDecimal longitude) {
        this.longitude = longitude;
    }
    
    public final BigDecimal getLatitude() {
        return this.latitude;
    }
    
    public final void setLatitude(final BigDecimal latitude) {
        this.latitude = latitude;
    }
    
    public final String getModule() {
        return this.module;
    }
    
    public final void setModule(final String module) {
        this.module = module;
    }
    
    public final int getNumberOfAlerts() {
        return this.numberOfAlerts;
    }
    
    public final void setNumberOfAlerts(final int numberOfAlerts) {
        this.numberOfAlerts = numberOfAlerts;
    }
    
    public final int getSeverity() {
        return this.severity;
    }
    
    public final void setSeverity(final int severity) {
        this.severity = severity;
    }
    
    public final String getAlertMessage() {
        return this.alertMessage;
    }
    
    public final void setAlertMessage(final String alertMessage) {
        this.alertMessage = alertMessage;
    }
    
    public final Date getDate() {
        return this.date;
    }
    
    public final void setDate(final Date date) {
        this.date = date;
    }
    
    public final RelativeDateTime getResolvedDate() {
        return this.resolvedDate;
    }
    
    public final void setResolvedDate(final RelativeDateTime resolvedDate) {
        this.resolvedDate = resolvedDate;
    }
    
    public final String getDuration() {
        return this.duration;
    }
    
    public final void setDuration(final String duration) {
        this.duration = duration;
    }
    
    public final boolean getIsClearedByUser() {
        return this.isClearedByUser;
    }
    
    public final void setIsClearedByUser(final boolean isClearedByUser) {
        this.isClearedByUser = isClearedByUser;
    }
    
    public final WebObjectId getPosRandomId() {
        return this.posRandomId;
    }
    
    public final WebObjectId getAlertRandomId() {
        return this.alertRandomId;
    }
    
    public final boolean getIsClearable() {
        return this.isClearable;
    }
    
    public final void setIsClearable(final boolean isClearable) {
        this.isClearable = isClearable;
    }
    
    public final String getLastModifiedByUserName() {
        return this.lastModifiedByUserName;
    }
    
    public final void setLastModifiedByUserName(final String lastModifiedByUserName) {
        this.lastModifiedByUserName = lastModifiedByUserName;
    }
}
