package com.digitalpaytech.dto.customeradmin;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias(value = "RateListInfo")
public class RateListInfo {
    
    private String randomRateId;
    private String name;
    private Integer rateTypeId;
    private String rateTypeName;
    private String rateValue;
    private String misc;
    
    public RateListInfo() {
    }
    
    public final String getRandomRateId() {
        return this.randomRateId;
    }
    
    public final void setRandomRateId(final String randomRateId) {
        this.randomRateId = randomRateId;
    }
    
    public final String getName() {
        return this.name;
    }
    
    public final void setName(final String name) {
        this.name = name;
    }
    
    public final Integer getRateTypeId() {
        return this.rateTypeId;
    }
    
    public final void setRateTypeId(final Integer rateTypeId) {
        this.rateTypeId = rateTypeId;
    }
    
    public final String getRateTypeName() {
        return this.rateTypeName;
    }
    
    public final void setRateTypeName(final String rateTypeName) {
        this.rateTypeName = rateTypeName;
    }
    
    public final String getRateValue() {
        return this.rateValue;
    }
    
    public final void setRateValue(final String rateValue) {
        this.rateValue = rateValue;
    }
    
    public final String getMisc() {
        return this.misc;
    }
    
    public final void setMisc(final String misc) {
        this.misc = misc;
    }
    
}
