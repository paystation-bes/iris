package com.digitalpaytech.dto.customeradmin;

import java.util.HashMap;
import java.util.Map;

import org.hibernate.transform.AliasToBeanResultTransformer;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerType;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.WebCoreConstants;

public class LocationTreeCustomerTransformer extends AliasToBeanResultTransformer {
	private static final long serialVersionUID = -3891243710772219130L;
	
	private int idIdx = -1;
	private int parentIdIdx = -1;
	private int nameIdx = -1;
	private int customerTypeIdx = -1;
	
	private RandomKeyMapping keyMapping;
	
	private Map<Integer, LocationTree> parentNodes;
	private Map<Integer, LocationTree> prevParentNodes;
	private Map<Integer, LocationTree> childNodes;
	
	public LocationTreeCustomerTransformer(RandomKeyMapping keyMapping) {
		this(keyMapping, new HashMap<Integer, LocationTree>());
	}
	
	public LocationTreeCustomerTransformer(RandomKeyMapping keyMapping, Map<Integer, LocationTree> childNodes) {
		super(LocationTree.class);
		
		this.keyMapping = keyMapping;
		
		this.parentNodes = new HashMap<Integer, LocationTree>();
		this.prevParentNodes = new HashMap<Integer, LocationTree>();
		this.childNodes = childNodes;
	}

	@Override
	public Object transformTuple(Object[] tuple, String[] aliases) {
		if(this.idIdx < 0) {
			resolveIdx(tuple, aliases);
		}
		
		LocationTree node = new LocationTree();
		
		Integer id = (Integer) tuple[this.idIdx];
		node.setRandomId(this.keyMapping.getRandomString(Customer.class, id));
		node.setOrganization(true);
		node.setName((String) tuple[this.nameIdx]);
		
		Integer parentId = (Integer) tuple[this.parentIdIdx];
		if(parentId != null) {
			LocationTree parent = prevParentNodes.get(parentId);
			if(parent != null) {
				parent.addChild(node);
			}
		}
		
		CustomerType type = (CustomerType) tuple[this.customerTypeIdx];
		if(WebCoreConstants.CUSTOMER_TYPE_CHILD == type.getId()) {
			node.setParent(false);
			this.childNodes.put(id, node);
		}
		else if(WebCoreConstants.CUSTOMER_TYPE_PARENT == type.getId()) {
			node.setParent(true);
			this.parentNodes.put(id, node);
		}
		
		return node;
	}
	
	private void resolveIdx(Object[] tuple, String[] aliases) {
		int idx = aliases.length;
		while(--idx >= 0) {
			if(aliases[idx].equals("id")) {
				this.idIdx = idx;
			}
			else if(aliases[idx].equals("parentId")) {
				this.parentIdIdx = idx;
			}
			else if(aliases[idx].equals("name")) {
				this.nameIdx = idx;
			}
			else if(aliases[idx].equals("customerType")) {
				this.customerTypeIdx = idx;
			}
		}
	}
	
	public void cleareLevel() {
		Map<Integer, LocationTree> buffer = this.prevParentNodes;
		this.prevParentNodes = this.parentNodes;
		this.parentNodes = buffer;
		this.parentNodes.clear();
	}

	public Map<Integer, LocationTree> getParentNodes() {
		return parentNodes;
	}

	public Map<Integer, LocationTree> getChildNodes() {
		return childNodes;
	}
}
