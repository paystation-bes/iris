package com.digitalpaytech.dto.customeradmin;

import java.util.Date;

import com.digitalpaytech.domain.Customer;

public class BannedCard {
    private Integer id;
    private String uuid;
    private Date addedGMT;
    private Short cardExpiry;
    private Short last4DigitsOfCardNumber;
    private String cardNumber;
    private String comment;
    private String cardData;
    private String source;
    private String cardType;
    private int cardTypeId;
    private Customer customer;
    private int numRetries;
    
    public BannedCard() {
    }
    
    public final Integer getId() {
        return this.id;
    }
    
    public final void setId(final Integer id) {
        this.id = id;
    }
    
    public final String getUuid() {
        return this.uuid;
    }
    
    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }
    
    public final Date getAddedGMT() {
        return this.addedGMT;
    }
    
    public final void setAddedGMT(final Date addedGMT) {
        this.addedGMT = addedGMT;
    }
    
    public final Short getCardExpiry() {
        return this.cardExpiry;
    }
    
    public final void setCardExpiry(final Short cardExpiry) {
        this.cardExpiry = cardExpiry;
    }
    
    public final Short getLast4DigitsOfCardNumber() {
        return this.last4DigitsOfCardNumber;
    }
    
    public final void setLast4DigitsOfCardNumber(final Short last4DigitsOfCardNumber) {
        this.last4DigitsOfCardNumber = last4DigitsOfCardNumber;
    }
    
    public final String getCardNumber() {
        return this.cardNumber;
    }
    
    public final void setCardNumber(final String cardNumber) {
        this.cardNumber = cardNumber;
    }
    
    public final String getComment() {
        return this.comment;
    }
    
    public final void setComment(final String comment) {
        this.comment = comment;
    }
    
    public final String getCardData() {
        return this.cardData;
    }
    
    public final void setCardData(final String cardData) {
        this.cardData = cardData;
    }
    
    public final String getSource() {
        return this.source;
    }
    
    public final void setSource(final String source) {
        this.source = source;
    }
    
    public final String getCardType() {
        return this.cardType;
    }
    
    public final void setCardType(final String cardType) {
        this.cardType = cardType;
    }
    
    public final int getCardTypeId() {
        return this.cardTypeId;
    }
    
    public final void setCardTypeId(final int cardTypeId) {
        this.cardTypeId = cardTypeId;
    }
    
    public final Customer getCustomer() {
        return this.customer;
    }
    
    public final void setCustomer(final Customer customer) {
        this.customer = customer;
    }
    
    public final int getNumRetries() {
        return this.numRetries;
    }
    
    public final void setNumRetries(final int numRetries) {
        this.numRetries = numRetries;
    }
}