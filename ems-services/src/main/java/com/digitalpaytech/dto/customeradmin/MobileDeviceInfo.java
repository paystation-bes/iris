package com.digitalpaytech.dto.customeradmin;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.digitalpaytech.dto.MobileAppInfo;

public class MobileDeviceInfo implements Serializable{
    private static final long serialVersionUID = 2116723311195875318L;
    //EMS 4922
    //The JSON should include the random ID, Friendly Name, Status, application assigned to and count 
    //of applications it is assigned to in case of Multiple licenses on a single device. 
    private String randomId;
    private String name;
    private int statusId;
    private String status;
    private String application;
    private String description;
    private Integer applicationCount;
    private BigDecimal latitude;
    private BigDecimal longitude;
    private String userName;
    private String firstName;
    private String lastName;
    private String userRandomId;
    private String uid;
    private Boolean loggedIn;
    private List<MobileAppInfo> appInfos;
    public Boolean getLoggedIn(){
        return loggedIn;
    }
    public void setLoggedIn(Boolean loggedIn){
        this.loggedIn = loggedIn;
    }
    public String getUid(){
        return uid;
    }
    public void setUid(String uid){
        this.uid = uid;
    }
    public String getFirstName(){
    	return firstName;
    }
    public void setFirstName(String firstName){
    	this.firstName = firstName;
    }
    public String getLastName(){
    	return lastName;
    }
    public void setLastName(String lastName){
    	this.lastName = lastName;
    }
    public String getRandomId(){
        return randomId;
    }
    public void setRandomId(String randomId){
        this.randomId = randomId;
    }
    public String getUserRandomId(){
        return userRandomId;
    }
    public void setUserRandomId(String userRandomId){
        this.userRandomId = userRandomId;
    }
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public int getStatusId(){
        return statusId;
    }
    public void setStatusId(int statusId){
        this.statusId = statusId;
    }
    public String getStatus(){
        return status;
    }
    public void setStatus(String status){
        this.status = status;
    }
    public String getApplication(){
        return application;
    }
    public void setApplication(String application){
        this.application = application;
    }
    public Integer getApplicationCount(){
        return applicationCount;
    }
    public void setApplicationCount(Integer applicationCount){
        this.applicationCount = applicationCount;
    }
    public void incrementApplicationCount(){
        this.applicationCount++;
    }
    public String getDescription(){
        return description;
    }
    public void setDescription(String description){
        this.description = description;
    }
    public BigDecimal getLatitude(){
        return latitude;
    }
    public void setLatitude(BigDecimal latitude){
        this.latitude = latitude;
    }
    public BigDecimal getLongitude(){
        return longitude;
    }
    public void setLongitude(BigDecimal longitude){
        this.longitude = longitude;
    }
    public String getUserName(){
        return userName;
    }
    public void setUserName(String userName){
        this.userName = userName;
    }
    public List<MobileAppInfo> getAppInfos(){
        return appInfos;
    }
    public void setAppInfos(List<MobileAppInfo> appInfos){
        this.appInfos = appInfos;
    }
    public void addAppInfo(String appName, String randomId, int status, String statusString){
        if(appInfos == null){
            appInfos = new LinkedList<MobileAppInfo>();
        }
        MobileAppInfo appInfo = new MobileAppInfo(appName, randomId);
        appInfo.setStatus(statusString);
        appInfo.setStatusId(status);
        this.appInfos.add(appInfo);
    }
}
