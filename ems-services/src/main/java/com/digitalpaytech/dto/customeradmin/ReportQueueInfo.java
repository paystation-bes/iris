package com.digitalpaytech.dto.customeradmin;

import java.util.ArrayList;
import java.util.List;

import com.digitalpaytech.util.support.RelativeDateTime;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class ReportQueueInfo {
    
    @XStreamImplicit(itemFieldName = "queueInfoList")
    private List<QueueInfo> queueList;
    
    public List<QueueInfo> getQueueList() {
        return queueList;
    }
    
    public void setQueueList(List<QueueInfo> queueList) {
        this.queueList = queueList;
    }
    
    public ReportQueueInfo() {
        queueList = new ArrayList<QueueInfo>();
    }
    
    static public class QueueInfo {
        
        private String randomId;
        private String title;
        private int status;
        private String statusExplanation;
        private RelativeDateTime time;

        public QueueInfo() {
        }
        
        public QueueInfo(String randomId, String title, String nextScheduledTime, int status) {
            this.randomId = randomId;
            this.title = title;
            this.status = status;
        }

		public String getRandomId() {
            return randomId;
        }
        
        public void setRandomId(String randomId) {
            this.randomId = randomId;
        }
        
        public String getTitle() {
            return title;
        }
        
        public void setTitle(String title) {
            this.title = title;
        }
        
        public int getStatus() {
            return status;
        }
        
        public void setStatus(int status) {
            this.status = status;
        }

        public String getStatusExplanation() {
            return statusExplanation;
        }

        public void setStatusExplanation(String statusExplanation) {
            this.statusExplanation = statusExplanation;
        }
        
        public RelativeDateTime getTime() {
			return time;
		}

		public void setTime(RelativeDateTime time) {
			this.time = time;
		}
    }
}
