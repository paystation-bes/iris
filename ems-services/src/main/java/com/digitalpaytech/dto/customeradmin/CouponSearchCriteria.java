package com.digitalpaytech.dto.customeradmin;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class CouponSearchCriteria implements Serializable {
	private static final long serialVersionUID = -8272314638044988948L;

	private Integer customerId;
	
	private List<Integer> couponIds;
	
	private Integer ignoredConsumerId;
	private List<Integer> ignoredCouponIds;
	
	private String filterValue;
	private String discountValue;
	private boolean disableFilterValueAsDiscount;

	private boolean usedCoupon;
	private boolean unusedCoupon;
	
	private Date validForDate;
	private Date lastValidDate;
	
	private Integer page;
	private Integer itemsPerPage;
	private String[] orderColumn;
	private boolean orderDesc = false;
	
	private Date maxUpdatedTime;
	
	private boolean hideOfflineCoupon = true;
	
	private Integer endCouponId;
	
	public CouponSearchCriteria() {
		
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public String[] getOrderColumn() {
		return orderColumn;
	}

	public void setOrderColumn(String[] orderColumn) {
		this.orderColumn = orderColumn;
	}

	public boolean isOrderDesc() {
		return orderDesc;
	}

	public void setOrderDesc(boolean orderDesc) {
		this.orderDesc = orderDesc;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getItemsPerPage() {
		return itemsPerPage;
	}

	public void setItemsPerPage(Integer itemsPerPage) {
		this.itemsPerPage = itemsPerPage;
	}

	public List<Integer> getCouponIds() {
		return couponIds;
	}

	public void setCouponIds(List<Integer> couponIds) {
		this.couponIds = couponIds;
	}
	
	public String getFilterValue() {
		return filterValue;
	}

	public void setFilterValue(String filterValue) {
		this.filterValue = filterValue;
	}
	
	public String getDiscountValue() {
		return discountValue;
	}
	
	public void setDiscountValue(String discountValue) {
		this.discountValue = discountValue;
	}
	
	public boolean isDisableFilterValueAsDiscount() {
		return disableFilterValueAsDiscount;
	}

	public void setDisableFilterValueAsDiscount(boolean disableFilterValueAsDiscount) {
		this.disableFilterValueAsDiscount = disableFilterValueAsDiscount;
	}

	public List<Integer> getIgnoredCouponIds() {
		return ignoredCouponIds;
	}

	public void setIgnoredCouponIds(List<Integer> ignoredCouponIds) {
		this.ignoredCouponIds = ignoredCouponIds;
	}

	public Date getMaxUpdatedTime() {
		return maxUpdatedTime;
	}

	public void setMaxUpdatedTime(Date maxUpdatedTime) {
		this.maxUpdatedTime = maxUpdatedTime;
	}

	public boolean isUsedCoupon() {
		return usedCoupon;
	}

	public void setUsedCoupon(boolean usedCoupon) {
		this.usedCoupon = usedCoupon;
	}

	public boolean isUnusedCoupon() {
		return unusedCoupon;
	}

	public void setUnusedCoupon(boolean unusedCoupon) {
		this.unusedCoupon = unusedCoupon;
	}

	public boolean isHideOfflineCoupon() {
		return hideOfflineCoupon;
	}

	public void setHideOfflineCoupon(boolean hideOfflineCoupon) {
		this.hideOfflineCoupon = hideOfflineCoupon;
	}

	public Integer getIgnoredConsumerId() {
		return ignoredConsumerId;
	}

	public void setIgnoredConsumerId(Integer ignoredConsumerId) {
		this.ignoredConsumerId = ignoredConsumerId;
	}

	public Date getValidForDate() {
		return validForDate;
	}

	public void setValidForDate(Date validForDate) {
		this.validForDate = validForDate;
	}

	public Date getLastValidDate() {
		return lastValidDate;
	}

	public void setLastValidDate(Date lastValidDate) {
		this.lastValidDate = lastValidDate;
	}
	
	public Integer getEndCouponId() {
		return endCouponId;
	}

	public void setEndCouponId(Integer endCouponId) {
		this.endCouponId = endCouponId;
	}
}
