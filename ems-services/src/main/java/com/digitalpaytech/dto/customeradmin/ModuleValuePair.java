package com.digitalpaytech.dto.customeradmin;

public class ModuleValuePair {
	
	private String randomId;
	private String name;
	
	public ModuleValuePair(){		
	}
	
	public ModuleValuePair(String randomId, String moduleName) {
		this.randomId = randomId;
		this.name = moduleName;
	}

	public String getrandomId() {
		return randomId;
	}

	public void setrandomId(String randomId) {
		this.randomId = randomId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	
}
