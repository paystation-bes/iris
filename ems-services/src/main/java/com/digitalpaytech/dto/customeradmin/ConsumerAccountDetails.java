package com.digitalpaytech.dto.customeradmin;


public class ConsumerAccountDetails {
    
    private String randomConsumerId;
    private String firstName;
    private String lastName;
    private String email;
    private String description;            
    
    public ConsumerAccountDetails(){        
    }
    
    public ConsumerAccountDetails(String firstName, String lastName, String email, String description){
        
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.description = description;                       
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    } 

    public String getRandomConsumerId() {
        return randomConsumerId;
    }

    public void setRandomConsumerId(String randomConsumerId) {
        this.randomConsumerId = randomConsumerId;
    }    
    
}
