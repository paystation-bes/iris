package com.digitalpaytech.dto.customeradmin;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias(value = "CustomerAdminConsumerAccountInfo")
public class CustomerAdminConsumerAccountInfo {
    
    private String randomConsumerId;
    private String firstName;
    private String lastName;
    private String emailAddress;        
    private String description;
    
    public CustomerAdminConsumerAccountInfo(String randomConsumerId, String firstName, 
                                            String lastName, String emailAddress, String description){
        
        this.randomConsumerId = randomConsumerId;        
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.description = description;  
    }
    
    public String getRandomConsumerId() {
        return randomConsumerId;
    }
    public void setRandomConsumerId(String randomConsumerId) {
        this.randomConsumerId = randomConsumerId;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getEmailAddress() {
        return emailAddress;
    }
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    } 
}
