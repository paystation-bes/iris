package com.digitalpaytech.dto.customeradmin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.util.WebCoreConstants;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias(value = "transactionReceiptDetails")
@StoryAlias(value = TransactionReceiptDetails.ALIAS, code = "transactionReceiptDetail")
public class TransactionReceiptDetails implements Serializable {
    public static final String ALIAS = "transaction receipt detail";
    public static final String ALIAS_CVM = "cvm";
    public static final String ALIAS_AID = "aid";
    public static final String ALIAS_APL = "apl";
    
    private static final long serialVersionUID = -7977575424501823566L;
    
    private String purchaseRandomId;
    private String processorTransactionRandomId;
    private String locationName;
    private String payStationName;
    private String payStationSerial;
    private String transactionNumber;
    private String lotNumber;
    private int stallNumber;
    private String licensePlateNumber;
    private String transactionType;
    private int processorTransactionTypeId;
    private String paymentType;
    private String cvm;
    private String apl;
    private String aid;
    private int addTimeNumber;
    private String purchasedDate;
    private String purchasedTime;
    private String monthlyStartDate;
    private String expiryDate;
    private String expiryTime;
    private Float originalAmount;
    private Float chargedAmount;
    private Float cashPaid;
    private Float cardPaid;
    private Float smartCardPaid;
    private Float valueCardPaid;
    private Float excessPayment;
    private Float changeDispensed;
    private String refundSlip;
    private Boolean isRefundSlip;
    private String cardType;
    private String cardNumber;
    private String authorizationNumber;
    private String referenceNumber;
    private String status;
    private String processingDate;
    private Float amountRefundedSlip;
    private Float amountRefundedCard;
    private Boolean isCouponOffline;
    private String couponNumber;
    private String smartCardData;
    private String rateName;
    private String paystationSetting;
    private Float totalPaid;
    private Float totalParking;
    private String mobileNumber;
    private Boolean isCreditCard;
    private Boolean isPND;
    private Boolean isPBL;
    private Boolean isPBS;
    private Boolean isPANNeeded;
    private Boolean isRefundable;
    private String refundAuthorizationNumber;
    private String refundReferenceNumber;
    @XStreamImplicit(itemFieldName = "emailAddressHistoryDetailList")
    private List<EmailAddressHistoryDetail> emailAddressHistoryDetailList;
    @XStreamImplicit(itemFieldName = "taxDetailList")
    private List<TaxDetail> taxDetailList;
    
    // T2 Flex <PERMITNUMBER>, <FACILITYNAME>, <ISDESTROYED>, <ISLOST>, <ISCUSTODY>, <ISRETURNED>, <ISTERMINATED>
    private boolean isFlex;
    private String permitNumber;
    private String facilityName;
    private Boolean isActive;
    private Boolean isDestroyed;
    private Boolean isLost;
    private Boolean isCustody;
    private Boolean isReturned;
    private Boolean isTerminated;
    private String accountName;
    
    public TransactionReceiptDetails() {
        this.emailAddressHistoryDetailList = new ArrayList<EmailAddressHistoryDetail>();
        this.taxDetailList = new ArrayList<TaxDetail>();
    }
    
    public final Boolean getIsCreditCard() {
        return this.isCreditCard;
    }
    
    public final void setIsCreditCard(final Boolean isCreditCard) {
        this.isCreditCard = isCreditCard;
    }
    
    public final String getRateName() {
        return this.rateName;
    }
    
    public final void setRateName(final String rateName) {
        this.rateName = rateName;
    }
    
    public final Float getTotalPaid() {
        return this.totalPaid;
    }
    
    public final void setTotalPaid(final Float totalPaid) {
        this.totalPaid = totalPaid;
    }
    
    public final Float getTotalParking() {
        return this.totalParking;
    }
    
    public final void setTotalParking(final Float totalParking) {
        this.totalParking = totalParking;
    }
    
    public final String getMobileNumber() {
        return this.mobileNumber;
    }
    
    public final void setMobileNumber(final String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
    
    public final String getPaystationSetting() {
        return this.paystationSetting;
    }
    
    public final void setPaystationSetting(final String paystationSetting) {
        this.paystationSetting = paystationSetting;
    }
    
    public final String getPurchaseRandomId() {
        return this.purchaseRandomId;
    }
    
    public final void setPurchaseRandomId(final String purchaseRandomId) {
        this.purchaseRandomId = purchaseRandomId;
    }
    
    public final String getLocationName() {
        return this.locationName;
    }
    
    public final void setLocationName(final String locationName) {
        this.locationName = locationName;
    }
    
    public final String getPayStationName() {
        return this.payStationName;
    }
    
    public final void setPayStationName(final String payStationName) {
        this.payStationName = payStationName;
    }
    
    public final String getPayStationSerial() {
        return this.payStationSerial;
    }
    
    public final void setPayStationSerial(final String payStationSerial) {
        this.payStationSerial = payStationSerial;
    }
    
    public final String getTransactionNumber() {
        return this.transactionNumber;
    }
    
    public final void setTransactionNumber(final String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }
    
    public final String getLotNumber() {
        return this.lotNumber;
    }
    
    public final void setLotNumber(final String lotNumber) {
        this.lotNumber = lotNumber;
    }
    
    public final int getStallNumber() {
        return this.stallNumber;
    }
    
    public final void setStallNumber(final int stallNumber) {
        this.stallNumber = stallNumber;
    }
    
    public final String getLicensePlateNumber() {
        return this.licensePlateNumber;
    }
    
    public final void setLicensePlateNumber(final String licensePlateNumber) {
        this.licensePlateNumber = licensePlateNumber;
    }
    
    public final String getTransactionType() {
        return this.transactionType;
    }
    
    public final void setTransactionType(final String transactionType) {
        this.transactionType = transactionType;
    }
    
    public final String getPaymentType() {
        return this.paymentType;
    }
    
    public final void setPaymentType(final String paymentType) {
        this.paymentType = paymentType;
    }
    
    public final int getAddTimeNumber() {
        return this.addTimeNumber;
    }
    
    public final void setAddTimeNumber(final int addTimeNumber) {
        this.addTimeNumber = addTimeNumber;
    }
    
    public final String getPurchasedDate() {
        return this.purchasedDate;
    }
    
    public final void setPurchasedDate(final String purchasedDate) {
        this.purchasedDate = purchasedDate;
    }
    
    public final String getPurchasedTime() {
        return this.purchasedTime;
    }
    
    public final void setPurchasedTime(final String purchasedTime) {
        this.purchasedTime = purchasedTime;
    }
    
    public final String getMonthlyStartDate() {
        return this.monthlyStartDate;
    }
    
    public final void setMonthlyStartDate(final String monthlyStartDate) {
        this.monthlyStartDate = monthlyStartDate;
    }
    
    public final String getExpiryDate() {
        return this.expiryDate;
    }
    
    public final void setExpiryDate(final String expiryDate) {
        this.expiryDate = expiryDate;
    }
    
    public final String getExpiryTime() {
        return this.expiryTime;
    }
    
    public final void setExpiryTime(final String expiryTime) {
        this.expiryTime = expiryTime;
    }
    
    public final Float getChargedAmount() {
        return this.chargedAmount;
    }
    
    public final void setChargedAmount(final Float chargedAmount) {
        this.chargedAmount = chargedAmount;
    }
    
    public final Float getOriginalAmount() {
        return this.originalAmount;
    }
    
    public final void setOriginalAmount(final Float originalAmount) {
        this.originalAmount = originalAmount;
    }
    
    public final Float getCashPaid() {
        return this.cashPaid;
    }
    
    public final void setCashPaid(final Float cashPaid) {
        this.cashPaid = cashPaid;
    }
    
    public final Float getCardPaid() {
        return this.cardPaid;
    }
    
    public final void setCardPaid(final Float cardPaid) {
        this.cardPaid = cardPaid;
    }
    
    public final Float getSmartCardPaid() {
        return this.smartCardPaid;
    }
    
    public final void setSmartCardPaid(final Float smartCardPaid) {
        this.smartCardPaid = smartCardPaid;
    }
    
    public final Float getValueCardPaid() {
        return this.valueCardPaid;
    }
    
    public final void setValueCardPaid(final Float valueCardPaid) {
        this.valueCardPaid = valueCardPaid;
    }
    
    public final Float getExcessPayment() {
        return this.excessPayment;
    }
    
    public final void setExcessPayment(final Float excessPayment) {
        this.excessPayment = excessPayment;
    }
    
    public final Float getChangeDispensed() {
        return this.changeDispensed;
    }
    
    public final void setChangeDispensed(final Float changeDispensed) {
        this.changeDispensed = changeDispensed;
    }
    
    public final String getRefundSlip() {
        return this.refundSlip;
    }
    
    public final void setIsRefundSlip(final Boolean isRefundSlip) {
        this.isRefundSlip = isRefundSlip;
    }
    
    public final Boolean isRefundSlip() {
        return this.isRefundSlip;
    }
    
    public final void setRefundSlip(final String refundSlip) {
        this.refundSlip = refundSlip;
    }
    
    public final void setRefundSlip(final boolean refundSlip) {
        if (refundSlip) {
            this.refundSlip = WebCoreConstants.RESPONSE_TRUE;
            this.isRefundSlip = true;
        } else {
            this.refundSlip = WebCoreConstants.RESPONSE_FALSE;
            this.isRefundSlip = false;
        }
    }
    
    public final String getCardType() {
        return this.cardType;
    }
    
    public final void setCardType(final String cardType) {
        this.cardType = cardType;
    }
    
    public final String getCardNumber() {
        return this.cardNumber;
    }
    
    public final void setCardNumber(final String cardNumber) {
        this.cardNumber = cardNumber;
    }
    
    public final String getAuthorizationNumber() {
        return this.authorizationNumber;
    }
    
    public final void setAuthorizationNumber(final String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }
    
    public final String getReferenceNumber() {
        return this.referenceNumber;
    }
    
    public final void setReferenceNumber(final String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }
    
    public final String getStatus() {
        return this.status;
    }
    
    public final void setStatus(final String status) {
        this.status = status;
    }
    
    public final String getProcessingDate() {
        return this.processingDate;
    }
    
    public final void setProcessingDate(final String processingDate) {
        this.processingDate = processingDate;
    }
    
    public final Float getAmountRefundedSlip() {
        return this.amountRefundedSlip;
    }
    
    public final void setAmountRefundedSlip(final Float amountRefundedSlip) {
        this.amountRefundedSlip = amountRefundedSlip;
    }
    
    public final Float getAmountRefundedCard() {
        return this.amountRefundedCard;
    }
    
    public final void setAmountRefundedCard(final Float amountRefundedCard) {
        this.amountRefundedCard = amountRefundedCard;
    }
    
    public final Boolean isIsCouponOffline() {
        return this.isCouponOffline;
    }
    
    public final void setIsCouponOffline(final Boolean isCouponOffline) {
        this.isCouponOffline = isCouponOffline;
    }
    
    public final String getCouponNumber() {
        return this.couponNumber;
    }
    
    public final void setCouponNumber(final String couponNumber) {
        this.couponNumber = couponNumber;
    }
    
    public final int getProcessorTransactionTypeId() {
        return this.processorTransactionTypeId;
    }
    
    public final void setProcessorTransactionTypeId(final int processorTransactionTypeId) {
        this.processorTransactionTypeId = processorTransactionTypeId;
    }
    
    public final List<EmailAddressHistoryDetail> getEmailAddressHistoryDetailList() {
        return this.emailAddressHistoryDetailList;
    }
    
    public final void setEmailAddressHistoryDetailList(final List<EmailAddressHistoryDetail> emailAddressHistoryDetailList) {
        this.emailAddressHistoryDetailList = emailAddressHistoryDetailList;
    }
    
    public final List<TaxDetail> getTaxDetailList() {
        return this.taxDetailList;
    }
    
    public final void setTaxDetailList(final List<TaxDetail> taxDetailList) {
        this.taxDetailList = taxDetailList;
    }
    
    public final Boolean isPND() {
        return this.isPND;
    }
    
    public final void setIsPND(final Boolean isPND) {
        this.isPND = isPND;
    }
    
    public final Boolean isPBL() {
        return this.isPBL;
    }
    
    public final void setIsPBL(final Boolean isPBL) {
        this.isPBL = isPBL;
    }
    
    public final Boolean isPBS() {
        return this.isPBS;
    }
    
    public final void setIsPBS(final Boolean isPBS) {
        this.isPBS = isPBS;
    }
    
    public String getPermitNumber() {
        return permitNumber;
    }
    
    public void setPermitNumber(String permitNumber) {
        this.permitNumber = permitNumber;
    }
    
    public String getFacilityName() {
        return facilityName;
    }
    
    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }
    
    public Boolean getIsDestroyed() {
        return isDestroyed;
    }
    
    public void setIsDestroyed(Boolean isDestroyed) {
        this.isDestroyed = isDestroyed;
    }
    
    public Boolean getIsLost() {
        return isLost;
    }
    
    public void setIsLost(Boolean isLost) {
        this.isLost = isLost;
    }
    
    public Boolean getIsCustody() {
        return isCustody;
    }
    
    public void setIsCustody(Boolean isCustody) {
        this.isCustody = isCustody;
    }
    
    public Boolean getIsReturned() {
        return isReturned;
    }
    
    public void setIsReturned(Boolean isReturned) {
        this.isReturned = isReturned;
    }
    
    public Boolean getIsTerminated() {
        return isTerminated;
    }
    
    public void setIsTerminated(Boolean isTerminated) {
        this.isTerminated = isTerminated;
    }
    
    public Boolean getIsActive() {
        return isActive;
    }
    
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }
    
    public final Boolean getIsFlex() {
        return this.isFlex;
    }
    
    public final void setIsFlex(final boolean isFlex) {
        this.isFlex = isFlex;
    }
    
    public final String getAccountName() {
        return this.accountName;
    }
    
    public final void setAccountName(final String accountName) {
        this.accountName = accountName;
    }
    
    public Boolean getIsPANNeeded() {
        return this.isPANNeeded;
    }
    
    public void setIsPANNeeded(final Boolean isPANNeeded) {
        this.isPANNeeded = isPANNeeded;
    }
    
    public Boolean getIsRefundable() {
        return this.isRefundable;
    }
    
    public void setIsRefundable(final Boolean isRefundable) {
        this.isRefundable = isRefundable;
    }
    
    public String getRefundAuthorizationNumber() {
        return this.refundAuthorizationNumber;
    }
    
    public void setRefundAuthorizationNumber(final String refundAuthorizationNumber) {
        this.refundAuthorizationNumber = refundAuthorizationNumber;
    }
    
    public String getRefundReferenceNumber() {
        return this.refundReferenceNumber;
    }
    
    public void setRefundReferenceNumber(final String refundReferenceNumber) {
        this.refundReferenceNumber = refundReferenceNumber;
    }
    
    public String getProcessorTransactionRandomId() {
        return processorTransactionRandomId;
    }
    
    public void setProcessorTransactionRandomId(String processorTransactionRandomId) {
        this.processorTransactionRandomId = processorTransactionRandomId;
    }

    @StoryAlias(ALIAS_CVM)
    public final String getCvm() {
        return this.cvm;
    }

    public final void setCvm(final String cvm) {
        this.cvm = cvm;
    }

    @StoryAlias(ALIAS_APL)
    public final String getApl() {
        return this.apl;
    }

    public final void setApl(final String apl) {
        this.apl = apl;
    }

    @StoryAlias(ALIAS_AID)
    public final String getAid() {
        return this.aid;
    }

    public final void setAid(final String aid) {
        this.aid = aid;
    }
    
}
