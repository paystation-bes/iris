package com.digitalpaytech.dto.customeradmin;

import java.util.HashMap;
import java.util.Map;

import org.hibernate.transform.AliasToBeanResultTransformer;

public class ActivePosAlertSummaryTransformer extends AliasToBeanResultTransformer {
	private static final long serialVersionUID = -3451381928385821781L;
	
	private Map<Integer, ActivePosAlertSummary> result;
	
	private int posIdIdx = -1;
	private int paperLeverIdx = -1;
	private int highestSeverityIdx = -1;
	private int alertsCountIdx = -1;
	
	public ActivePosAlertSummaryTransformer() {
		this(new HashMap<Integer, ActivePosAlertSummary>());
	}
	
	public ActivePosAlertSummaryTransformer(Map<Integer, ActivePosAlertSummary> result) {
		super(ActivePosAlertSummary.class);
		this.result = result;
	}

	@Override
	public Object transformTuple(Object[] tuple, String[] aliases) {
		if(this.posIdIdx < 0) {
			resolveIdx(tuple, aliases);
		}
		
		Integer posId = (Integer) tuple[this.posIdIdx];
		
		ActivePosAlertSummary summ = new ActivePosAlertSummary();
		summ.setAlertsCount(((Number) tuple[this.alertsCountIdx]).intValue());
		summ.setHighestSeverity(((Number) tuple[this.highestSeverityIdx]).intValue());
		summ.setPaperLevel(((Number) tuple[this.paperLeverIdx]).intValue());
		
		this.result.put(posId, summ);
		
		return summ;
	}
	
	private void resolveIdx(Object[] tuple, String[] aliases) {
		int idx = aliases.length;
		while(--idx >= 0) {
			if(aliases[idx].equals("posId")) {
				this.posIdIdx = idx;
			}
			else if(aliases[idx].equals("paperLevel")) {
				this.paperLeverIdx = idx;
			}
			else if(aliases[idx].equals("maxSeverity")) {
				this.highestSeverityIdx = idx;
			}
			else if(aliases[idx].equals("allAlerts")) {
				this.alertsCountIdx = idx;
			}
		}
	}

	public Map<Integer, ActivePosAlertSummary> getResult() {
		return result;
	}
}
