package com.digitalpaytech.dto.customeradmin;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class LocationRouteFilterInfo {
		
	@XStreamImplicit(itemFieldName="locationInfo")
	private List<LocationInfo> locations;
	@XStreamImplicit(itemFieldName="routeInfo")
	private List<RouteInfo> routes;
	
	public LocationRouteFilterInfo(){	
		locations = new ArrayList<LocationInfo>();
		routes = new ArrayList<RouteInfo>();
	}

	public List<LocationInfo> getLocations() {
		return locations;
	}
	public void setLocations(List<LocationInfo> locations) {
		this.locations = locations;
	}
	public void addLocation(String name, String randomId, String parentName, boolean isParent) {
		this.locations.add(new LocationInfo(name, randomId, parentName, isParent));
	}
	
	public List<RouteInfo> getRoutes() {
		return routes;
	}
	public void setRoutes(List<RouteInfo> routes) {
		this.routes = routes;
	}
	public void addRoute(String name, String randomId) {
		this.routes.add(new RouteInfo(name, randomId));
	}

	public class LocationInfo{
		private String name;
		private String randomId;
		private String parentName;
		private boolean isParent;
		
		public LocationInfo(String name, String randomId, String parentName, boolean isParent){
			this.name = name;
			this.randomId = randomId;
			this.parentName = parentName;
			this.isParent = isParent;
		}

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getRandomId() {
            return randomId;
        }

        public void setRandomId(String randomId) {
            this.randomId = randomId;
        }

        public String getParentName() {
            return parentName;
        }

        public void setParentName(String parentName) {
            this.parentName = parentName;
        }

        public boolean getIsParent() {
            return isParent;
        }

        public void setIsParent(boolean isParent) {
            this.isParent = isParent;
        }
	}
	
	public class RouteInfo{
		private String name;
		private String randomId;

		public RouteInfo(String name, String randomId){
			this.name = name;
			this.randomId = randomId;
		}

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getRandomId() {
            return randomId;
        }

        public void setRandomId(String randomId) {
            this.randomId = randomId;
        }		
	}
}