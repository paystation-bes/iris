package com.digitalpaytech.dto.customeradmin;

public class SeverityValuePair {

    private String randomId;
    private String name;

    public SeverityValuePair(){        
    }
    
    public SeverityValuePair(String name, String randomId){     
        this.name = name;
        this.randomId = randomId;
    }

    public String getRandomId() {
        return randomId;
    }

    public void setRandomId(String randomId) {
        this.randomId = randomId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }       
}
