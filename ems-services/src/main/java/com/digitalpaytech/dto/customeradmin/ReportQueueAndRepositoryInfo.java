package com.digitalpaytech.dto.customeradmin;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class ReportQueueAndRepositoryInfo {
    
    @XStreamAlias(value = "ReportQueueInfo")
    private ReportQueueInfo reportQueueInfo;
    
    @XStreamAlias(value = "ReportRepositoryInfo")
    private ReportRepositoryInfo reportRepositoryInfo;
    
    public ReportQueueAndRepositoryInfo() {
    }
    
    public ReportQueueInfo getReportQueueInfo() {
        return reportQueueInfo;
    }
    
    public void setReportQueueInfo(ReportQueueInfo reportQueueInfo) {
        this.reportQueueInfo = reportQueueInfo;
    }
    
    public ReportRepositoryInfo getReportRepositoryInfo() {
        return reportRepositoryInfo;
    }
    
    public void setReportRepositoryInfo(ReportRepositoryInfo reportRepositoryInfo) {
        this.reportRepositoryInfo = reportRepositoryInfo;
    }
    
}
