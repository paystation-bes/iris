package com.digitalpaytech.dto.customeradmin;

import java.util.ArrayList;
import java.util.Collection;

import com.digitalpaytech.dto.UserName;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class RoleSettingDetails {
    
    private String randomId;
    private String roleName;
    private boolean isEnabled;
    private Integer typeId;
    
    @XStreamImplicit(itemFieldName = "assignedUsers")
    private Collection<UserName> assignedUsers;
    
    @XStreamImplicit(itemFieldName = "permissionList")
    private Collection<PermissionTree> permissionList;
    
    public RoleSettingDetails() {
        this.assignedUsers = new ArrayList<UserName>();
    }
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final String getRoleName() {
        return this.roleName;
    }
    
    public final void setRoleName(final String roleName) {
        this.roleName = roleName;
    }
    
    public final boolean getIsEnabled() {
        return this.isEnabled;
    }
    
    public final void setIsEnabled(final boolean isEnabled) {
        this.isEnabled = isEnabled;
    }
    
    public final Collection<UserName> getAssignedUsers() {
        return this.assignedUsers;
    }
    
    public final void setAssignedUsers(final Collection<UserName> assignedUsers) {
        this.assignedUsers = assignedUsers;
    }
    
    public final void addAssignedUser(final UserName userName) {
        this.assignedUsers.add(userName);
    }
    
    public final Collection<PermissionTree> getPermissionList() {
        return this.permissionList;
    }
    
    public final void setPermissionList(final Collection<PermissionTree> permissionList) {
        this.permissionList = permissionList;
    }
    
    public final Integer getTypeId() {
        return this.typeId;
    }
    
    public final void setTypeId(final Integer typeId) {
        this.typeId = typeId;
    }
}
