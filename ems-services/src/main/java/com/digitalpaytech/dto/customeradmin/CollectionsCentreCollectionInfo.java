package com.digitalpaytech.dto.customeradmin;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosCollection;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.support.RelativeDateTime;
import com.digitalpaytech.util.support.WebObjectId;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class CollectionsCentreCollectionInfo {
    
    private String pointOfSaleName;
    private String serial;
    private WebObjectId randomId;
    private RelativeDateTime lastSeen;
    
    private String location;
    
    @XStreamImplicit(itemFieldName = "route")
    private List<String> route;
    
    private BigDecimal longitude;
    private BigDecimal latitude;
    
    private String revenueType;
    private int numberOfAlerts;
    private int severity;
    private String alertMessage;
    
    private Integer amount;
    private Integer payStationType;
    private Integer alertThresholdTypeId;
    private Integer collectionTypeId;
    private WebObjectId collectionRandomId;
    
    private String collectionUserName;
    
    public CollectionsCentreCollectionInfo() {
        this.route = new ArrayList<String>();
    }
    
    public CollectionsCentreCollectionInfo(final PointOfSale pointOfSale, final Paystation paystation, final String revenueType,
            final int numberOfAlerts, final int highestSeverity, final String alertMessage, final RandomKeyMapping keyMapping,
            final RelativeDateTime lastSeen, final Integer alertThresholdTypeId, final List<Route> routeList) {
        
        this.route = new ArrayList<String>();
        
        this.pointOfSaleName = pointOfSale.getName();
        this.serial = pointOfSale.getSerialNumber();
        this.randomId = new WebObjectId(PointOfSale.class, pointOfSale.getId());
        this.lastSeen = lastSeen;
        this.location = pointOfSale.getLocation().getName();
        this.latitude = pointOfSale.getLatitude();
        this.longitude = pointOfSale.getLongitude();
        
        for (Route currentRoute : routeList) {
            if (currentRoute.getRouteType().getId() == WebCoreConstants.ROUTE_TYPE_COLLECTIONS) {
                this.addRoute(currentRoute.getName());
            }
        }
        
        this.revenueType = revenueType;
        this.numberOfAlerts = numberOfAlerts;
        this.severity = highestSeverity;
        this.alertMessage = alertMessage;
        this.payStationType = paystation.getPaystationType().getId();
        this.alertThresholdTypeId = alertThresholdTypeId;
    }
    
    public CollectionsCentreCollectionInfo(final PointOfSale pointOfSale, final Paystation paystation, final String revenueType,
            final RandomKeyMapping keyMapping, final RelativeDateTime lastSeen, final Integer amount, final PosCollection posCollection,
            final List<Route> routeList) {
        
        this.route = new ArrayList<String>();
        
        this.pointOfSaleName = pointOfSale.getName();
        this.serial = pointOfSale.getSerialNumber();
        this.randomId = new WebObjectId(PointOfSale.class, pointOfSale.getId());
        this.lastSeen = lastSeen;
        this.location = pointOfSale.getLocation().getName();
        this.latitude = pointOfSale.getLatitude();
        this.longitude = pointOfSale.getLongitude();
        
        for (Route currentRoute : routeList) {
            if (currentRoute.getRouteType().getId() == WebCoreConstants.ROUTE_TYPE_COLLECTIONS) {
                this.addRoute(currentRoute.getName());
            }
        }
        
        this.revenueType = revenueType;
        this.amount = amount;
        this.payStationType = paystation.getPaystationType().getId();
        this.collectionRandomId = new WebObjectId(PosCollection.class, posCollection.getId());
    }
    
    public final String getPointOfSaleName() {
        return this.pointOfSaleName;
    }
    
    public final void setPointOfSaleName(final String pointOfSaleName) {
        this.pointOfSaleName = pointOfSaleName;
    }
    
    public final String getSerial() {
        return this.serial;
    }
    
    public final void setSerial(final String serial) {
        this.serial = serial;
    }
    
    public final WebObjectId getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final WebObjectId randomId) {
        this.randomId = randomId;
    }
    
    public final String getLocation() {
        return this.location;
    }
    
    public final void setLocation(final String location) {
        this.location = location;
    }
    
    public final List<String> getRoute() {
        return this.route;
    }
    
    public final void setRoute(final List<String> route) {
        this.route = route;
    }
    
    public final void addRoute(final String newRoute) {
        this.route.add(newRoute);
    }
    
    public final BigDecimal getLongitude() {
        return this.longitude;
    }
    
    public final void setLongitude(final BigDecimal longitude) {
        this.longitude = longitude;
    }
    
    public final BigDecimal getLatitude() {
        return this.latitude;
    }
    
    public final void setLatitude(final BigDecimal latitude) {
        this.latitude = latitude;
    }
    
    public final String getRevenueType() {
        return this.revenueType;
    }
    
    public final void setRevenueType(final String revenueType) {
        this.revenueType = revenueType;
    }
    
    public final int getNumberOfAlerts() {
        return this.numberOfAlerts;
    }
    
    public final void setNumberOfAlerts(final int numberOfAlerts) {
        this.numberOfAlerts = numberOfAlerts;
    }
    
    public final int getSeverity() {
        return this.severity;
    }
    
    public final void setSeverity(final int severity) {
        this.severity = severity;
    }
    
    public final String getAlertMessage() {
        return this.alertMessage;
    }
    
    public final void setAlertMessage(final String alertMessage) {
        this.alertMessage = alertMessage;
    }
    
    public final RelativeDateTime getLastSeen() {
        return this.lastSeen;
    }
    
    public final void setLastSeen(final RelativeDateTime lastSeen) {
        this.lastSeen = lastSeen;
    }
    
    public final Integer getAmount() {
        return this.amount;
    }
    
    public final void setAmount(final Integer amount) {
        this.amount = amount;
    }
    
    public final Integer getPayStationType() {
        return this.payStationType;
    }
    
    public final void setPayStationType(final Integer payStationType) {
        this.payStationType = payStationType;
    }
    
    public final Integer getAlertThresholdTypeId() {
        return this.alertThresholdTypeId;
    }
    
    public final void setAlertThresholdTypeId(final Integer alertThresholdTypeId) {
        this.alertThresholdTypeId = alertThresholdTypeId;
    }
    
    public final WebObjectId getCollectionRandomId() {
        return this.collectionRandomId;
    }
    
    public final void setCollectionRandomId(final WebObjectId collectionRandomId) {
        this.collectionRandomId = collectionRandomId;
    }
    
    public final Integer getCollectionTypeId() {
        return this.collectionTypeId;
    }
    
    public final void setCollectionTypeId(final Integer collectionTypeId) {
        this.collectionTypeId = collectionTypeId;
    }
    
    public final String getCollectionUserName() {
        return this.collectionUserName;
    }
    
    public final void setCollectionUserName(final String collectionUserName) {
        this.collectionUserName = collectionUserName;
    }
}
