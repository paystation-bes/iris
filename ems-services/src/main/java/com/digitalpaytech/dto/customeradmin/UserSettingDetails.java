package com.digitalpaytech.dto.customeradmin;

import java.util.Collection;
import java.util.List;

import com.digitalpaytech.dto.ChildCustomerInfo;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class UserSettingDetails {
    
    private String randomId;
    private String firstName;
    private String lastName;
    private String userName;
    private String emailAddress;
    private boolean status;
    private boolean isDeleted;
    private boolean isAllChilds;
    private boolean isAlias;
    
    @XStreamImplicit(itemFieldName = "activityLogList")
    private Collection<ActivityLogDetail> activityLogList;
    
    @XStreamImplicit(itemFieldName = "roleList")
    private Collection<RoleSettingInfo> roleList;
    
    @XStreamImplicit(itemFieldName = "childCustomerInfos")
    private List<ChildCustomerInfo> childCustomerInfos;
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final String getFirstName() {
        return this.firstName;
    }
    
    public final void setFirstName(final String firstName) {
        this.firstName = firstName;
    }
    
    public final String getLastName() {
        return this.lastName;
    }
    
    public final void setLastName(final String lastName) {
        this.lastName = lastName;
    }
    
    public final String getUserName() {
        return this.userName;
    }
    
    public final void setUserName(final String userName) {
        this.userName = userName;
    }
    
    public final String getEmailAddress() {
        return this.emailAddress;
    }
    
    public final void setEmailAddress(final String emailAddress) {
        this.emailAddress = emailAddress;
    }
    
    public final boolean getStatus() {
        return this.status;
    }
    
    public final void setStatus(final boolean status) {
        this.status = status;
    }
    
    public final boolean getIsDeleted() {
        return this.isDeleted;
    }
    
    public final void setIsDeleted(final boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
    
    public final Collection<ActivityLogDetail> getActivityLogList() {
        return this.activityLogList;
    }
    
    public final void setActivityLogList(final Collection<ActivityLogDetail> activityLogList) {
        this.activityLogList = activityLogList;
    }
    
    public final Collection<RoleSettingInfo> getRoleList() {
        return this.roleList;
    }
    
    public final void setRoleList(final Collection<RoleSettingInfo> roleList) {
        this.roleList = roleList;
    }
    
    public final List<ChildCustomerInfo> getChildCustomerInfos() {
        return this.childCustomerInfos;
    }
    
    public final void setChildCustomerInfos(final List<ChildCustomerInfo> childCustomerInfos) {
        this.childCustomerInfos = childCustomerInfos;
    }
    
    public final boolean getIsAllChilds() {
        return this.isAllChilds;
    }
    
    public final void setIsAllChilds(final boolean isAllChilds) {
        this.isAllChilds = isAllChilds;
    }

    public boolean getIsAlias() {
        return isAlias;
    }

    public void setIsAlias(boolean isAlias) {
        this.isAlias = isAlias;
    }
}
