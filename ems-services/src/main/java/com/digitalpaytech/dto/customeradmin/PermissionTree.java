package com.digitalpaytech.dto.customeradmin;

import java.util.Collection;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias(value = "PermissionTree")
public class PermissionTree {

	private String randomPermissionId;
	private String permissionName;
	private String status;
	@XStreamImplicit(itemFieldName = "childPermissions")
	private Collection<PermissionTree> childPermissions;

	public PermissionTree() {
		
	}
	
	public PermissionTree(String permissionName) {
		this.permissionName = permissionName;
	}
	
	public PermissionTree(String randomPermissionId, String permissionName) {
		this.randomPermissionId = randomPermissionId;
		this.permissionName = permissionName;
	}
	
	public PermissionTree(String randomPermissionId, String permissionName, String status) {
		this.randomPermissionId = randomPermissionId;
		this.permissionName = permissionName;
		this.status = status;
	}
	
	public PermissionTree(String permissionName, Collection<PermissionTree> childPermissions) {
		this.permissionName = permissionName;
		this.childPermissions = childPermissions;
	}
	
	public PermissionTree(String randomPermissionId, String permissionName, Collection<PermissionTree> childPermissions) {
		this.randomPermissionId = randomPermissionId;
		this.permissionName = permissionName;
		this.childPermissions = childPermissions;
	}
	
	public String getRandomPermissionId() {
		return randomPermissionId;
	}

	public void setRandomPermissionId(String randomPermissionId) {
		this.randomPermissionId = randomPermissionId;
	}

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Collection<PermissionTree> getChildPermissions() {
		return childPermissions;
	}

	public void setChildPermissions(Collection<PermissionTree> childPermissions) {
		this.childPermissions = childPermissions;
	}
}
