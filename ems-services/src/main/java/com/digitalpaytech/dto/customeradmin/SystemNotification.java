package com.digitalpaytech.dto.customeradmin;

import java.util.List;

import com.digitalpaytech.domain.Notification;

public class SystemNotification {

	private List<Notification> currentFutureNotifications;
	private List<Notification> pastNotifications;

	public List<Notification> getCurrentFutureNotifications() {
		return currentFutureNotifications;
	}

	public void setCurrentFutureNotifications(
			List<Notification> currentFutureNotifications) {
		this.currentFutureNotifications = currentFutureNotifications;
	}

	public List<Notification> getPastNotifications() {
		return pastNotifications;
	}

	public void setPastNotifications(List<Notification> pastNotifications) {
		this.pastNotifications = pastNotifications;
	}
}
