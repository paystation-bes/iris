package com.digitalpaytech.dto.customeradmin;

import java.util.HashMap;
import java.util.Map;

import org.hibernate.transform.AliasToBeanResultTransformer;

import com.digitalpaytech.domain.Location;
import com.digitalpaytech.util.RandomKeyMapping;

public class LocationTreeTransformer extends AliasToBeanResultTransformer {
	private static final long serialVersionUID = 8481285867467308213L;
	
	private RandomKeyMapping keyMapping;
	private Map<Integer, LocationTree> organizationsMap;
	private Map<Integer, LocationTree> locationsMap;
	
	private int idIdx = -1;
	private int parentIdIdx = -1;
	private int nameIdx = -1;
	private int isParentIdx = -1;
	private int isUnassignedIdx = -1;
	private int isDeletedIdx = -1;
	private int customerIdIdx = -1;
	
	public LocationTreeTransformer(RandomKeyMapping keyMapping, Map<Integer, LocationTree> organizationsMap) {
		super(LocationTree.class);
		
		this.keyMapping = keyMapping;
		this.organizationsMap = organizationsMap;
		this.locationsMap = new HashMap<Integer, LocationTree>();
	}

	@Override
	public Object transformTuple(Object[] tuple, String[] aliases) {
		if(this.idIdx < 0) {
			resolveIdx(tuple, aliases);
		}
		
		Integer id = (Integer) tuple[this.idIdx];
		LocationTree locTree = this.locationsMap.get(id);
		if(locTree == null) {
			locTree = new LocationTree(id);
			this.locationsMap.put(id, locTree);
		}
		
        locTree.setParentId((Integer) tuple[this.parentIdIdx]);
		locTree.setName((String) tuple[this.nameIdx]);
		
        locTree.setParent((Boolean) tuple[this.isParentIdx]);
        locTree.setUnassigned((Boolean) tuple[this.isUnassignedIdx]);
        locTree.setDeleted((Boolean) tuple[this.isDeletedIdx]);
        
		locTree.setRandomId(keyMapping.getRandomString(Location.class, id));
		locTree.setLocation(true);
		
		if(locTree.getParentId() == null) {
			Integer customerId = (Integer) tuple[this.customerIdIdx];
			LocationTree orgNode = this.organizationsMap.get(customerId);
			if(orgNode != null) {
				orgNode.addChild(locTree);
			}
		}
		else {
			LocationTree parentLoc = this.locationsMap.get(locTree.getParentId());
        	if(parentLoc != null) {
        		locTree.setChildLocation(true);
        	}
        	else {
        		parentLoc = new LocationTree(locTree.getParentId());
        		this.locationsMap.put(locTree.getParentId(), parentLoc);
        	}
        	
        	parentLoc.getChildren().add(locTree);
		}
		
		return locTree;
	}
	
	private void resolveIdx(Object[] tuple, String[] aliases) {
		int idx = aliases.length;
		while(--idx >= 0) {
			if(aliases[idx].equals("id")) {
				this.idIdx = idx;
			}
			else if(aliases[idx].equals("parentId")) {
				this.parentIdIdx = idx;
			}
			else if(aliases[idx].equals("name")) {
				this.nameIdx = idx;
			}
			else if(aliases[idx].equals("isUnassigned")) {
				this.isUnassignedIdx = idx;
			}
			else if(aliases[idx].equals("isDeleted")) {
				this.isDeletedIdx = idx;
			}
			else if(aliases[idx].equals("isParent")) {
				this.isParentIdx = idx;
			}
			else if(aliases[idx].equals("customerId")) {
				this.customerIdIdx = idx;
			}
		}
	}

	public Map<Integer, LocationTree> getLocationsMap() {
		return locationsMap;
	}
}
