package com.digitalpaytech.dto.customeradmin;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosBalance;
import com.digitalpaytech.domain.PosCollection;
import com.digitalpaytech.domain.PosCollectionUser;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class CollectionsCentrePayStationInfo {
    
    private int coinCount;
    private int billCount;
    private int creditCount;
    
    private int coinCountSeverity;
    private int billCountSeverity;
    private int creditCountSeverity;
    
    private BigDecimal coinAmount;
    private BigDecimal billAmount;
    private BigDecimal creditAmount;
    private BigDecimal runningTotal;
    
    private int coinAmountSeverity;
    private int billAmountSeverity;
    private int creditAmountSeverity;
    private int runningTotalSeverity;
    
    private String name;
    private String serial;
    private String lastSeen;
    private String lastCollection;
    
    private int lastCollectionSeverity;
    
    private String location;
    
    private String collectionUserName;
    
    private Integer collectionType;
    private Integer payStationType;
    
    @XStreamImplicit(itemFieldName = "route")
    private List<String> route;
    
    public CollectionsCentrePayStationInfo() {
        this.route = new ArrayList<String>();
    }
    
    public CollectionsCentrePayStationInfo(final int coinCount, final int billCount, final int creditCount, final int coinCountSeverity,
        final int billCountSeverity, final int creditCountSeverity, final BigDecimal coinAmount, final BigDecimal billAmount,
        final BigDecimal creditAmount, final BigDecimal runningTotal, final int coinAmountSeverity, final int billAmountSeverity,
        final int creditAmountSeverity, final int runningTotalSeverity, final String name, final String serial, final String lastSeen,
        final String lastCollection, final int lastCollectionSeverity, final String location, final String collectionUserName,
        final Integer payStationType, final List<String> route) {
        super();
        this.coinCount = coinCount;
        this.billCount = billCount;
        this.creditCount = creditCount;
        this.coinCountSeverity = coinCountSeverity;
        this.billCountSeverity = billCountSeverity;
        this.creditCountSeverity = creditCountSeverity;
        this.coinAmount = coinAmount;
        this.billAmount = billAmount;
        this.creditAmount = creditAmount;
        this.runningTotal = runningTotal;
        this.coinAmountSeverity = coinAmountSeverity;
        this.billAmountSeverity = billAmountSeverity;
        this.creditAmountSeverity = creditAmountSeverity;
        this.runningTotalSeverity = runningTotalSeverity;
        this.name = name;
        this.serial = serial;
        this.lastSeen = lastSeen;
        this.lastCollection = lastCollection;
        this.lastCollectionSeverity = lastCollectionSeverity;
        this.location = location;
        this.collectionUserName = collectionUserName;
        this.payStationType = payStationType;
        this.route = route;
    }
    
    public final int getCoinCount() {
        return this.coinCount;
    }
    
    public final void setCoinCount(final int coinCount) {
        this.coinCount = coinCount;
    }
    
    public final int getBillCount() {
        return this.billCount;
    }
    
    public final void setBillCount(final int billCount) {
        this.billCount = billCount;
    }
    
    public final Integer getCreditCount() {
        return this.creditCount;
    }
    
    public final void setCreditCount(final Integer creditCount) {
        this.creditCount = creditCount;
    }
    
    public final BigDecimal getCoinAmount() {
        return this.coinAmount;
    }
    
    public final void setCoinAmount(final BigDecimal coinAmount) {
        this.coinAmount = coinAmount;
    }
    
    public final BigDecimal getBillAmount() {
        return this.billAmount;
    }
    
    public final void setBillAmount(final BigDecimal billAmount) {
        this.billAmount = billAmount;
    }
    
    public final BigDecimal getCreditAmount() {
        return this.creditAmount;
    }
    
    public final void setCreditAmount(final BigDecimal creditAmount) {
        this.creditAmount = creditAmount;
    }
    
    public final BigDecimal getRunningTotal() {
        return this.runningTotal;
    }
    
    public final void setRunningTotal(final BigDecimal runningTotal) {
        this.runningTotal = runningTotal;
    }
    
    public final String getName() {
        return this.name;
    }
    
    public final void setName(final String name) {
        this.name = name;
    }
    
    public final String getSerial() {
        return this.serial;
    }
    
    public final void setSerial(final String serial) {
        this.serial = serial;
    }
    
    public final String getLastSeen() {
        return this.lastSeen;
    }
    
    public final void setLastSeen(final String lastSeen) {
        this.lastSeen = lastSeen;
    }
    
    public final String getLastCollection() {
        return this.lastCollection;
    }
    
    public final void setLastCollection(final String lastCollection) {
        this.lastCollection = lastCollection;
    }
    
    public final int getLastCollectionSeverity() {
        return this.lastCollectionSeverity;
    }
    
    public final void setLastCollectionSeverity(final int lastCollectionSeverity) {
        this.lastCollectionSeverity = lastCollectionSeverity;
    }
    
    public final String getLocation() {
        return this.location;
    }
    
    public final void setLocation(final String location) {
        this.location = location;
    }
    
    public final String getCollectionUserName() {
        return this.collectionUserName;
    }
    
    public final void setCollectionUserName(final String collectionUserName) {
        this.collectionUserName = collectionUserName;
    }
    
    public final List<String> getRoute() {
        return this.route;
    }
    
    public final void setRoute(final List<String> route) {
        this.route = route;
    }
    
    public final int getCoinCountSeverity() {
        return this.coinCountSeverity;
    }
    
    public final void setCoinCountSeverity(final int coinCountSeverity) {
        this.coinCountSeverity = coinCountSeverity;
    }
    
    public final int getBillCountSeverity() {
        return this.billCountSeverity;
    }
    
    public final void setBillCountSeverity(final int billCountSeverity) {
        this.billCountSeverity = billCountSeverity;
    }
    
    public final int getCreditCountSeverity() {
        return this.creditCountSeverity;
    }
    
    public final void setCreditCountSeverity(final int creditCountSeverity) {
        this.creditCountSeverity = creditCountSeverity;
    }
    
    public final int getCoinAmountSeverity() {
        return this.coinAmountSeverity;
    }
    
    public final void setCoinAmountSeverity(final int coinAmountSeverity) {
        this.coinAmountSeverity = coinAmountSeverity;
    }
    
    public final int getBillAmountSeverity() {
        return this.billAmountSeverity;
    }
    
    public final void setBillAmountSeverity(final int billAmountSeverity) {
        this.billAmountSeverity = billAmountSeverity;
    }
    
    public final int getCreditAmountSeverity() {
        return this.creditAmountSeverity;
    }
    
    public final void setCreditAmountSeverity(final int creditAmountSeverity) {
        this.creditAmountSeverity = creditAmountSeverity;
    }
    
    public final int getRunningTotalSeverity() {
        return this.runningTotalSeverity;
    }
    
    public final void setRunningTotalSeverity(final int runningTotalSeverity) {
        this.runningTotalSeverity = runningTotalSeverity;
    }
    
    public final Integer getPayStationType() {
        return this.payStationType;
    }
    
    public final void setPayStationType(final Integer payStationType) {
        this.payStationType = payStationType;
    }
    
    public final Integer getCollectionType() {
        return this.collectionType;
    }
    
    public final void setCollectionType(final Integer collectionType) {
        this.collectionType = collectionType;
    }
    
    public final void setPayStationDetails(final PointOfSale pointOfSale, final String lastSeenString, final List<Route> routeList) {
        
        this.name = pointOfSale.getName();
        this.serial = pointOfSale.getSerialNumber();
        this.location = pointOfSale.getLocation().getName();
        this.payStationType = pointOfSale.getPaystation().getPaystationType().getId();
        this.lastSeen = lastSeenString;
        for (Route currentRoute : routeList) {
            this.route.add(currentRoute.getName());
        }
    }
    
    public final void setRunningTotalDetails(final PosBalance pointOfSaleBalance, final String lastCollectionString) {
        
        this.coinCount = pointOfSaleBalance.getCoinCount();
        this.billCount = pointOfSaleBalance.getBillCount();
        this.creditCount = pointOfSaleBalance.getUnsettledCreditCardCount();
        
        final BigDecimal hundred = new BigDecimal(StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1);
        
        this.coinAmount = new BigDecimal(pointOfSaleBalance.getCoinAmount()).divide(hundred).setScale(2, BigDecimal.ROUND_HALF_UP);
        this.billAmount = new BigDecimal(pointOfSaleBalance.getBillAmount()).divide(hundred).setScale(2, BigDecimal.ROUND_HALF_UP);
        this.creditAmount = new BigDecimal(pointOfSaleBalance.getUnsettledCreditCardAmount()).divide(hundred).setScale(2, BigDecimal.ROUND_HALF_UP);
        this.runningTotal = new BigDecimal(pointOfSaleBalance.getTotalAmount()).divide(hundred).setScale(2, BigDecimal.ROUND_HALF_UP);
        
        this.lastCollection = lastCollectionString;
    }
    
    public final void setPOSCollectionaDetails(final PosCollection posCollection) {
        
        this.coinCount = posCollection.getCoinCount05() + posCollection.getCoinCount10() + posCollection.getCoinCount25()
                         + posCollection.getCoinCount100() + posCollection.getCoinCount200();
        this.billCount = posCollection.getBillCount1() + posCollection.getBillCount2() + posCollection.getBillCount5()
                         + posCollection.getBillCount10() + posCollection.getBillCount20() + posCollection.getBillCount50();
        
        final BigDecimal hundred = new BigDecimal(StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1);
        
        this.coinAmount = new BigDecimal(posCollection.getCoinTotalAmount()).divide(hundred).setScale(2, BigDecimal.ROUND_HALF_UP);
        this.billAmount = new BigDecimal(posCollection.getBillTotalAmount()).divide(hundred).setScale(2, BigDecimal.ROUND_HALF_UP);
        this.creditAmount = new BigDecimal(posCollection.getCardTotalAmount()).divide(hundred).setScale(2, BigDecimal.ROUND_HALF_UP);
        
        this.runningTotal = this.coinAmount.add(this.billAmount).add(this.creditAmount);
        
        this.collectionType = posCollection.getCollectionType().getId();
        
        final Set<PosCollectionUser> collectionUsers = posCollection.getPosCollectionUsers();
        if (collectionUsers != null && !collectionUsers.isEmpty()) {
            String userName = null;
            for (PosCollectionUser collectionUser : collectionUsers) {
                if (collectionUser.getUserAccount() != null) {
                    try {
                        userName = URLDecoder.decode(collectionUser.getUserAccount().getFullName(), WebSecurityConstants.URL_ENCODING_LATIN1);
                    } catch (UnsupportedEncodingException uee) {
                        userName = collectionUser.getUserAccount().getFullName();
                    }
                    break;
                }
            }
            this.collectionUserName = userName;
        }
    }
    
    //TODO performance can be improved severity is already calculated just pass it to this method no need to calculate again.
    public final void setSeverity(final int alertTypeId, final int severity) {
        
        switch (alertTypeId) {
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_COUNT:
                this.coinCountSeverity = severity > this.coinCountSeverity ? severity : this.coinCountSeverity;
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_DOLLARS:
                this.coinAmountSeverity = severity > this.coinAmountSeverity ? severity : this.coinAmountSeverity;
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_COUNT:
                this.billCountSeverity = severity > this.billCountSeverity ? severity : this.billCountSeverity;
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_DOLLARS:
                this.billAmountSeverity = severity > this.billAmountSeverity ? severity : this.billAmountSeverity;
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_COUNT:
                this.creditCountSeverity = severity > this.creditCountSeverity ? severity : this.creditCountSeverity;
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_DOLLARS:
                this.creditAmountSeverity = severity > this.creditAmountSeverity ? severity : this.creditAmountSeverity;
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_RUNNING_TOTAL_DOLLAR:
                this.runningTotalSeverity = severity > this.runningTotalSeverity ? severity : this.runningTotalSeverity;
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_OVERDUE_COLLECTION:
                this.lastCollectionSeverity = severity > this.lastCollectionSeverity ? severity : this.lastCollectionSeverity;
                break;
            default:
                break;
        }
    }
    
}