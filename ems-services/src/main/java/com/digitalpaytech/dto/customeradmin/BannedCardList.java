package com.digitalpaytech.dto.customeradmin;

import java.io.Serializable;
import java.util.Collection;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class BannedCardList implements Serializable {
	private static final long serialVersionUID = -46193264696961005L;
	
	private String token;
	private Long dataKey;
	@XStreamImplicit(itemFieldName = "bannedCardInfo")
	private Collection<BannedCardInfo> bannedCardInfo;

	public BannedCardList() {
		
	}
	
	public BannedCardList(String token, Collection<BannedCardInfo> bannedCardInfo) {
		this(token, (Long) null, bannedCardInfo);
	}
	
	public BannedCardList(Long dataKey, Collection<BannedCardInfo> bannedCardInfo) {
		this((String) null, dataKey, bannedCardInfo);
	}
	
	public BannedCardList(String token, Long dataKey, Collection<BannedCardInfo> bannedCardInfo) {
		this.token = token;
		this.dataKey = dataKey;
		this.bannedCardInfo = bannedCardInfo;
	}
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Collection<BannedCardInfo> getBannedCardInfo() {
		return bannedCardInfo;
	}

	public void setBannedCardInfo(Collection<BannedCardInfo> bannedCardInfo) {
		this.bannedCardInfo = bannedCardInfo;
	}

	public Long getDataKey() {
		return dataKey;
	}

	public void setDataKey(Long dataKey) {
		this.dataKey = dataKey;
	}
}
