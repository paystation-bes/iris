package com.digitalpaytech.dto.customeradmin;

import java.util.List;

import com.digitalpaytech.util.KeyValuePair;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@SuppressWarnings({ "PMD.TooManyFields" })
public class PayStationDetails {
    
    private String name;
    private String serialNumber;
    private String controllerSerialNumber;
    private String payStationType;
    private String softwareVersion;
    private String firmwareVersion;
    private String creditCardMerchantAccount;
    private String valueCardMerchantAccount;
    private String location;
    private String locationRandomId;
    private String setting;
    
    @XStreamImplicit(itemFieldName = "groupAssignment")
    private List<String> groupAssignment;
    
    @XStreamImplicit(itemFieldName = "groupAssignmentRandomIds")
    private List<String> groupAssignmentRandomIds;
    
    @XStreamImplicit(itemFieldName = "installedModules")
    private List<String> installedModules;
    
    private String modemType;
    private String modemSettingsCCID;
    private String modemSettingsAPN;
    private String modemSettingsCarrier;
    private String activationDate;
    private String status;
    private String shareParkingDataBy;
    private String modemSettingsMEID;
    private String modemSettingsHardwareManufacturer;
    private String modemSettingsHardwareModel;
    private String modemSettingsHardwareFirmware;
    
    @XStreamImplicit(itemFieldName = "telemetryData")
    private List<KeyValuePair<String, String>> telemetryData;
    
    private String groupId;
    private String groupName;
    
    private String upgradeStatus;
    
    public PayStationDetails() {
        super();
    }
    
    public PayStationDetails(final String name, final String serialNumber, final String controllerSerialNumber, final String payStationType,
            final String softwareVersion, final String firmwareVersion, final String creditCardMerchantAccount, final String valueCardMerchantAccount,
            final String location, final String setting, final List<String> groupAssignment, final List<String> installedModules,
            final String modemType, final String modemSettingsCCID, final String modemSettingsMEID, final String modemSettingsAPN,
            final String activationDate, final String status, final String shareParkingDataBy, final String modemSettingsHardwareManufacturer,
            final String modemSettingsHardwareModel, final String modemSettingsHardwareFirmware) {
        /* final String verrusEnabled, */
        super();
        
        this.name = name;
        this.serialNumber = serialNumber;
        this.controllerSerialNumber = controllerSerialNumber;
        this.payStationType = payStationType;
        this.softwareVersion = softwareVersion;
        this.firmwareVersion = firmwareVersion;
        this.creditCardMerchantAccount = creditCardMerchantAccount;
        this.valueCardMerchantAccount = valueCardMerchantAccount;
        this.location = location;
        this.setting = setting;
        this.groupAssignment = groupAssignment;
        this.installedModules = installedModules;
        this.modemType = modemType;
        this.modemSettingsCCID = modemSettingsCCID;
        this.modemSettingsAPN = modemSettingsAPN;
        
        this.modemSettingsMEID = modemSettingsMEID;
        
        this.activationDate = activationDate;
        this.status = status;
        this.shareParkingDataBy = shareParkingDataBy;
        
        this.modemSettingsHardwareManufacturer = modemSettingsHardwareManufacturer;
        this.modemSettingsHardwareModel = modemSettingsHardwareModel;
        this.modemSettingsHardwareFirmware = modemSettingsHardwareFirmware;
    }
    
    public final String getName() {
        return this.name;
    }
    
    public final void setName(final String name) {
        this.name = name;
    }
    
    public final String getSerialNumber() {
        return this.serialNumber;
    }
    
    public final void setSerialNumber(final String serialNumber) {
        this.serialNumber = serialNumber;
    }
    
    public final String getControllerSerialNumber() {
        return this.controllerSerialNumber;
    }
    
    public final void setControllerSerialNumber(final String controllerSerialNumber) {
        this.controllerSerialNumber = controllerSerialNumber;
    }
    
    public final String getPayStationType() {
        return this.payStationType;
    }
    
    public final void setPayStationType(final String payStationType) {
        this.payStationType = payStationType;
    }
    
    public final String getSoftwareVersion() {
        return this.softwareVersion;
    }
    
    public final void setSoftwareVersion(final String softwareVersion) {
        this.softwareVersion = softwareVersion;
    }
    
    public final String getFirmwareVersion() {
        return this.firmwareVersion;
    }
    
    public final void setFirmwareVersion(final String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }
    
    public final String getCreditCardMerchantAccount() {
        return this.creditCardMerchantAccount;
    }
    
    public final void setCreditCardMerchantAccount(final String creditCardMerchantAccount) {
        this.creditCardMerchantAccount = creditCardMerchantAccount;
    }
    
    public final String getValueCardMerchantAccount() {
        return this.valueCardMerchantAccount;
    }
    
    public final void setValueCardMerchantAccount(final String valueCardMerchantAccount) {
        this.valueCardMerchantAccount = valueCardMerchantAccount;
    }
    
    public final String getLocation() {
        return this.location;
    }
    
    public final void setLocation(final String location) {
        this.location = location;
    }
    
    public final String getSetting() {
        return this.setting;
    }
    
    public final void setSetting(final String setting) {
        this.setting = setting;
    }
    
    public final List<String> getGroupAssignment() {
        return this.groupAssignment;
    }
    
    public final void setGroupAssignment(final List<String> groupAssignment) {
        this.groupAssignment = groupAssignment;
    }
    
    public final List<String> getInstalledModules() {
        return this.installedModules;
    }
    
    public final void setInstalledModules(final List<String> installedModules) {
        this.installedModules = installedModules;
    }
    
    public final String getModemSettingsCCID() {
        return this.modemSettingsCCID;
    }
    
    public final void setModemSettingsCCID(final String modemSettingsCCID) {
        this.modemSettingsCCID = modemSettingsCCID;
    }
    
    public final String getModemSettingsMEID() {
        return this.modemSettingsMEID;
    }
    
    public final void setModemSettingsMEID(final String modemSettingsMEID) {
        this.modemSettingsMEID = modemSettingsMEID;
    }
    
    public final String getModemSettingsAPN() {
        return this.modemSettingsAPN;
    }
    
    public final void setModemSettingsAPN(final String modemSettingsAPN) {
        this.modemSettingsAPN = modemSettingsAPN;
    }
    
    public final String getActivationDate() {
        return this.activationDate;
    }
    
    public final void setActivationDate(final String activationDate) {
        this.activationDate = activationDate;
    }
    
    public final String getStatus() {
        return this.status;
    }
    
    public final void setStatus(final String status) {
        this.status = status;
    }
    
    public final String getModemType() {
        return this.modemType;
    }
    
    public final void setModemType(final String modemType) {
        this.modemType = modemType;
    }
    
    public final String getShareParkingDataBy() {
        return this.shareParkingDataBy;
    }
    
    public final void setShareParkingDataBy(final String shareParkingDataBy) {
        this.shareParkingDataBy = shareParkingDataBy;
    }
    
    public final String getModemSettingsCarrier() {
        return this.modemSettingsCarrier;
    }
    
    public final void setModemSettingsCarrier(final String modemSettingsCarrier) {
        this.modemSettingsCarrier = modemSettingsCarrier;
    }
    
    public final String getLocationRandomId() {
        return this.locationRandomId;
    }
    
    public final void setLocationRandomId(final String locationRandomId) {
        this.locationRandomId = locationRandomId;
    }
    
    public final List<String> getGroupAssignmentRandomIds() {
        return this.groupAssignmentRandomIds;
    }
    
    public final void setGroupAssignmentRandomIds(final List<String> groupAssignmentRandomIds) {
        this.groupAssignmentRandomIds = groupAssignmentRandomIds;
    }
    
    public final List<KeyValuePair<String, String>> getTelemetryData() {
        return this.telemetryData;
    }
    
    public final void setTelemetryData(final List<KeyValuePair<String, String>> telemetryData) {
        this.telemetryData = telemetryData;
    }
    
    public final String getGroupId() {
        return this.groupId;
    }
    
    public final void setGroupId(final String groupId) {
        this.groupId = groupId;
    }
    
    public final String getGroupName() {
        return this.groupName;
    }
    
    public final void setGroupName(final String groupName) {
        this.groupName = groupName;
    }

    public final String getUpgradeStatus() {
        return this.upgradeStatus;
    }

    public final void setUpgradeStatus(final String upgradeStatus) {
        this.upgradeStatus = upgradeStatus;
    }
    
    public String getModemSettingsHardwareManufacturer() {
        return modemSettingsHardwareManufacturer;
    }
    
    public void setModemSettingsHardwareManufacturer(final String modemSettingsHardwareManufacturer) {
        this.modemSettingsHardwareManufacturer = modemSettingsHardwareManufacturer;
    }
    
    public String getModemSettingsHardwareModel() {
        return modemSettingsHardwareModel;
    }
    
    public void setModemSettingsHardwareModel(final String modemSettingsHardwareModel) {
        this.modemSettingsHardwareModel = modemSettingsHardwareModel;
    }
    
    public String getModemSettingsHardwareFirmware() {
        return modemSettingsHardwareFirmware;
    }
    
    public void setModemSettingsHardwareFirmware(final String modemSettingsHardwareFirmware) {
        this.modemSettingsHardwareFirmware = modemSettingsHardwareFirmware;
    }
    
}
