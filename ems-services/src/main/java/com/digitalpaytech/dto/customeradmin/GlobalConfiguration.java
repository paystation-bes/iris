package com.digitalpaytech.dto.customeradmin;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerPropertyType;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.SubscriptionType;
import com.digitalpaytech.domain.TimezoneVId;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.dto.RestAccountInfo;
import com.digitalpaytech.dto.WebServiceEndPointInfo;

@SuppressWarnings("PMD.TooManyFields")
public class GlobalConfiguration {
    
    private String legalName;
    private Date activationDate;
    private List<FilterDTO> querySpacesByOptions;
    private List<TimezoneVId> timeZones;
    private int querySpacesByValue;
    private String querySpacesByLabel;
    private String timeZone;
    private String warningPeriod;
    private List<CustomerProperty> customerProperties;
    private List<CustomerPropertyType> customerPropertyTypes;
    private List<CustomerSubscription> subscribedServices;
    private List<SubscriptionType> subscriptionTypes;
    private List<WebServiceEndPointInfo> webServiceEndPointList;
    private List<WebServiceEndPointInfo> privateWebServiceEndPointList;
    private List<RestAccountInfo> restAccountList;
    private Integer jurisdictionTypePreferred;
    private Integer jurisdictionTypeLimited;
    private String jurisdictionTypePreferredLabel;
    private String jurisdictionTypeLimitedLabel;
    private List<FilterDTO> jurisdictionTypesPreferred;
    private List<FilterDTO> jurisdictionTypesLimited;
    private FilterDTO preferredParkersStatus;
    
    public final TimezoneVId findTimeZone(final String timezoneName) {
        if (!StringUtils.isEmpty(timezoneName)) {
            TimezoneVId tz;
            final Iterator<TimezoneVId> iter = this.timeZones.iterator();
            while (iter.hasNext()) {
                tz = iter.next();
                if (timezoneName.equals(tz.getName())) {
                    return tz;
                }
            }
        }
        
        return null;
    }
    
    public final List<TimezoneVId> getTimeZones() {
        return this.timeZones;
    }
    
    public final void setTimeZones(final List<TimezoneVId> timeZones) {
        this.timeZones = timeZones;
    }
    
    public final String getLegalName() {
        return this.legalName;
    }
    
    public final void setLegalName(final String legalName) {
        this.legalName = legalName;
    }
    
    public final Date getActivationDate() {
        return this.activationDate;
    }
    
    public final void setActivationDate(final Date activationDate) {
        this.activationDate = activationDate;
    }
    
    public final String getTimeZone() {
        return this.timeZone;
    }
    
    public final void setTimeZone(final String timeZone) {
        this.timeZone = timeZone;
    }
    
    public final List<CustomerPropertyType> getCustomerPropertyTypes() {
        return this.customerPropertyTypes;
    }
    
    public final void setCustomerPropertyTypes(final List<CustomerPropertyType> customerPropertyTypes) {
        this.customerPropertyTypes = customerPropertyTypes;
    }
    
    public final List<CustomerSubscription> getSubscribedServices() {
        return this.subscribedServices;
    }
    
    public final void setSubscribedServices(final List<CustomerSubscription> subscribedServices) {
        this.subscribedServices = subscribedServices;
    }
    
    public final List<SubscriptionType> getSubscriptionTypes() {
        return this.subscriptionTypes;
    }
    
    public final void setSubscriptionTypes(final List<SubscriptionType> subscriptionTypes) {
        this.subscriptionTypes = subscriptionTypes;
    }
    
    public final List<WebServiceEndPointInfo> getWebServiceEndPointList() {
        return this.webServiceEndPointList;
    }
    
    public final void setWebServiceEndPointList(final List<WebServiceEndPointInfo> webServiceEndPointList) {
        this.webServiceEndPointList = webServiceEndPointList;
    }
    
    public final List<WebServiceEndPointInfo> getPrivateWebServiceEndPointList() {
        return this.privateWebServiceEndPointList;
    }
    
    public final void setPrivateWebServiceEndPointList(final List<WebServiceEndPointInfo> privateWebServiceEndPointList) {
        this.privateWebServiceEndPointList = privateWebServiceEndPointList;
    }
    
    public final List<RestAccountInfo> getRestAccountList() {
        return this.restAccountList;
    }
    
    public final void setRestAccountList(final List<RestAccountInfo> restAccountList) {
        this.restAccountList = restAccountList;
    }
    
    public final String getWarningPeriod() {
        return this.warningPeriod;
    }
    
    public final void setWarningPeriod(final String warningPeriod) {
        this.warningPeriod = warningPeriod;
    }
    
    public final List<CustomerProperty> getCustomerProperties() {
        return this.customerProperties;
    }
    
    public final void setCustomerProperties(final List<CustomerProperty> customerProperties) {
        this.customerProperties = customerProperties;
    }
    
    public final List<FilterDTO> getQuerySpacesByOptions() {
        return this.querySpacesByOptions;
    }
    
    public final void setQuerySpacesByOptions(final List<FilterDTO> querySpaceBys) {
        this.querySpacesByOptions = querySpaceBys;
    }
    
    public final int getQuerySpacesByValue() {
        return this.querySpacesByValue;
    }
    
    public final void setQuerySpacesByValue(final int querySpacesByValue) {
        this.querySpacesByValue = querySpacesByValue;
    }
    
    public final String getQuerySpacesByLabel() {
        return this.querySpacesByLabel;
    }
    
    public final void setQuerySpacesByLabel(final String querySpaceByLabel) {
        this.querySpacesByLabel = querySpaceByLabel;
    }
    
    public final Integer getJurisdictionTypePreferred() {
        return this.jurisdictionTypePreferred;
    }
    
    public final void setJurisdictionTypePreferred(final Integer jurisdictionTypePreferred) {
        this.jurisdictionTypePreferred = jurisdictionTypePreferred;
    }
    
    public final Integer getJurisdictionTypeLimited() {
        return this.jurisdictionTypeLimited;
    }
    
    public final void setJurisdictionTypeLimited(final Integer jurisdictionTypeLimited) {
        this.jurisdictionTypeLimited = jurisdictionTypeLimited;
    }
    
    public final String getJurisdictionTypePreferredLabel() {
        return this.jurisdictionTypePreferredLabel;
    }
    
    public final void setJurisdictionTypePreferredLabel(final String jurisdictionTypePreferredLabel) {
        this.jurisdictionTypePreferredLabel = jurisdictionTypePreferredLabel;
    }
    
    public final String getJurisdictionTypeLimitedLabel() {
        return this.jurisdictionTypeLimitedLabel;
    }
    
    public final void setJurisdictionTypeLimitedLabel(final String jurisdictionTypeLimitedLabel) {
        this.jurisdictionTypeLimitedLabel = jurisdictionTypeLimitedLabel;
    }

    public final List<FilterDTO> getJurisdictionTypesPreferred() {
        return this.jurisdictionTypesPreferred;
    }

    public final void setJurisdictionTypesPreferred(final List<FilterDTO> jurisdictionTypesPreferred) {
        this.jurisdictionTypesPreferred = jurisdictionTypesPreferred;
    }

    public final List<FilterDTO> getJurisdictionTypesLimited() {
        return this.jurisdictionTypesLimited;
    }

    public final void setJurisdictionTypesLimited(final List<FilterDTO> jurisdictionTypesLimited) {
        this.jurisdictionTypesLimited = jurisdictionTypesLimited;
    }

    public final FilterDTO getPreferredParkersStatus() {
        return this.preferredParkersStatus;
    }

    public final void setPreferredParkersStatus(final FilterDTO preferredParkersStatus) {
        this.preferredParkersStatus = preferredParkersStatus;
    }
}
