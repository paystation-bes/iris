package com.digitalpaytech.dto.customeradmin;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


import com.digitalpaytech.util.support.RelativeDateTime;
import com.digitalpaytech.util.support.WebObjectId;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * Similar to CollectionsCentreCollectionInfo or CollectionsCentrePayStationInfo
 *  but stores the pay station info at
 * the root JSON level, and lists collection-specific info in a list of 
 * "CollectionsCentreMinimalInfo" objects.
 * 
 * Used to display a pay station and its list of recent collections on the map.
 * @author danielm
 *
 */
public class CollectionsCentrePosInfo {
    private String pointOfSaleName;
    private String serial;
    private WebObjectId randomId;
    private RelativeDateTime lastSeen;
    
    private String location;
    
    @XStreamImplicit(itemFieldName = "route")
    private List<String> route;
    
    private BigDecimal longitude;
    private BigDecimal latitude;
    
    private Integer payStationType;
    
    private int numberOfAlerts;
    private int severity;
    private String alertMessage;
    
    @XStreamImplicit(itemFieldName = "collections")
    private List<CollectionsCentreMinimalInfo> collectionInfos;
    
    public CollectionsCentrePosInfo(){
        collectionInfos = new ArrayList<CollectionsCentreMinimalInfo>();
    }
    
    public String getPointOfSaleName() {
        return pointOfSaleName;
    }
    
    public void setPointOfSaleName(String pointOfSaleName) {
        this.pointOfSaleName = pointOfSaleName;
    }
    
    public String getSerial() {
        return serial;
    }
    
    public void setSerial(String serial) {
        this.serial = serial;
    }
    
    public WebObjectId getRandomId() {
        return randomId;
    }
    
    public void setRandomId(WebObjectId randomId) {
        this.randomId = randomId;
    }
    
    public String getLocation() {
        return location;
    }
    
    public void setLocation(String location) {
        this.location = location;
    }
    
    public List<String> getRoute() {
        return route;
    }
    
    public void setRoute(List<String> route) {
        this.route = route;
    }
    
    public void addRoute(String route) {
        this.route.add(route);
    }
    
    public BigDecimal getLongitude() {
        return longitude;
    }
    
    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }
    
    public BigDecimal getLatitude() {
        return latitude;
    }
    
    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }
    
    public int getNumberOfAlerts() {
        return numberOfAlerts;
    }
    
    public void setNumberOfAlerts(int numberOfAlerts) {
        this.numberOfAlerts = numberOfAlerts;
    }
    
    public int getSeverity() {
        return severity;
    }
    
    public void setSeverity(int severity) {
        this.severity = severity;
    }
    
    public String getAlertMessage() {
        return alertMessage;
    }
    
    public void setAlertMessage(String alertMessage) {
        this.alertMessage = alertMessage;
    }
    
    public RelativeDateTime getLastSeen() {
        return lastSeen;
    }
    
    public void setLastSeen(RelativeDateTime lastSeen) {
        this.lastSeen = lastSeen;
    }
    public Integer getPayStationType() {
        return payStationType;
    }
    public void setPayStationType(Integer payStationType){
        this.payStationType = payStationType;
    }
    public void addCollectionInfo(CollectionsCentreMinimalInfo info){
        collectionInfos.add(info);
    }
    public void addCollectionInfo(CollectionsCentreCollectionInfo collectionsCentreCollectionInfo){
        collectionInfos.add(new CollectionsCentreMinimalInfo(collectionsCentreCollectionInfo));
    }
    public List<CollectionsCentreMinimalInfo> getCollectionInfos(){
        return collectionInfos;
    }
    public void setCollectionInfos(List<CollectionsCentreMinimalInfo> collectionInfos){
        this.collectionInfos = collectionInfos;
    }
}
