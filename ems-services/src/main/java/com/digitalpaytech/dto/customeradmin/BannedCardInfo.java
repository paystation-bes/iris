package com.digitalpaytech.dto.customeradmin;

import java.util.Collection;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * This class holds the banned card information to be displayed as a banned card search result.
 * 
 * @author Brian Kim
 *
 */
public class BannedCardInfo {

	private String randomBadCardId;
	private String cardNumber;
	private String cardExpiry;
	private String cardType;
	private String comment;
	private String source;
	private String dateAdded;
	private String lastModifiedDate;
	@XStreamImplicit(itemFieldName = "importFailedCardNumber")
	private Collection<String> importFailedCardNumber;
	
	public BannedCardInfo() {
	}
	
	public BannedCardInfo(String randomBadCardId, String cardNumber,
			String cardExpiry, String cardType, String comment, 
			Collection<String> importFailedCardNumber) {
		this.randomBadCardId = randomBadCardId;
		this.cardNumber = cardNumber;
		this.cardExpiry = cardExpiry;
		this.cardType = cardType;
		this.comment = comment;
		this.importFailedCardNumber = importFailedCardNumber;
	}

	public BannedCardInfo(String cardNumber, String cardExpiry,
			String cardType, String comment,
			String source, String dateAdded, String lastModifiedDate) {
		this.cardNumber = cardNumber;
		this.cardExpiry = cardExpiry;
		this.cardType = cardType;
		this.comment = comment;
		this.source = source;
		this.dateAdded = dateAdded;
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getRandomBadCardId() {
		return randomBadCardId;
	}

	public void setRandomBadCardId(String randomBadCardId) {
		this.randomBadCardId = randomBadCardId;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCardExpiry() {
		return cardExpiry;
	}

	public void setCardExpiry(String cardExpiry) {
		this.cardExpiry = cardExpiry;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(String dateAdded) {
		this.dateAdded = dateAdded;
	}

	public String getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(String lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public Collection<String> getImportFailedCardNumber() {
		return importFailedCardNumber;
	}

	public void setImportFailedCardNumber(Collection<String> importFailedCardNumber) {
		this.importFailedCardNumber = importFailedCardNumber;
	}

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
