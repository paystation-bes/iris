package com.digitalpaytech.dto.customeradmin;

import java.util.ArrayList;
import java.util.List;

import com.digitalpaytech.dto.customeradmin.ActivityLogEntry;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class CustomerActivityLog {

	@XStreamImplicit(itemFieldName="activityLogs")
	private List<ActivityLogEntry> activityLogs;
	
	public CustomerActivityLog(){	
		activityLogs = new ArrayList<ActivityLogEntry>();
	}
	
	public CustomerActivityLog(List<ActivityLogEntry> activityLogs){		
		this.activityLogs = activityLogs;
	}
	
	public List<ActivityLogEntry> getActivityLogs() {
		return activityLogs;
	}
	public void setActivityLogs(List<ActivityLogEntry> activityLogs) {
		this.activityLogs = activityLogs;
	}
	public void addActivityLog(ActivityLogEntry activityLog) {
		this.activityLogs.add(activityLog);
	}
		
}
