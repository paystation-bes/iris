package com.digitalpaytech.dto.customeradmin;

import java.util.Map;

import org.hibernate.transform.AliasToBeanResultTransformer;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.util.RandomKeyMapping;

public class LocationTreePointOfSaleTransformer extends AliasToBeanResultTransformer {
	private static final long serialVersionUID = 3194622593777784322L;
	
	private RandomKeyMapping keyMapping;
	private Map<Integer, LocationTree> locationsMap;
	private LocationTree currLoc;
	
	private int idIdx = -1;
	private int parentIdIdx = -1;
	private int nameIdx = -1;
	private int paystationTypeIdx = -1;
	private int isUnassignedIdx = -1;
	private int isDeletedIdx = -1;
	private int isVisibleIdx = -1;
	private int isActivatedIdx = -1;
	
	public LocationTreePointOfSaleTransformer(RandomKeyMapping keyMapping, Map<Integer, LocationTree> locationsMap) {
		super(LocationTree.class);
		
		this.keyMapping = keyMapping;
		this.locationsMap = locationsMap;
	}
	
	@Override
	public Object transformTuple(Object[] tuple, String[] aliases) {
		if(this.idIdx < 0) {
			resolveIdx(tuple, aliases);
		}
		
		LocationTree pos = new LocationTree((Integer) tuple[this.idIdx]);
		pos.setRandomId(this.keyMapping.getRandomString(PointOfSale.class, pos.getId()));
		pos.setParentId((Integer) tuple[this.parentIdIdx]);
		pos.setName((String) tuple[this.nameIdx]);
		pos.setPaystationType((Integer) tuple[this.paystationTypeIdx]);
		pos.setUnassigned((Boolean) tuple[this.isUnassignedIdx]);
		pos.setDeleted((Boolean) tuple[this.isDeletedIdx]);
		pos.setVisible((Boolean) tuple[this.isVisibleIdx]);
		pos.setActivated((Boolean) tuple[this.isActivatedIdx]);
		pos.setPayStation(true);
		pos.setParent(false);
		
		if(pos.getParentId() != null) {
			if((this.currLoc == null) || (!this.currLoc.getId().equals(pos.getParentId()))) {
				this.currLoc = this.locationsMap.remove(pos.getParentId());
				if(this.currLoc == null) {
					//log.warn("Found zombie PointOfSale: " + pos.getId());
				}
			}
			
			if(this.currLoc != null) {
				this.currLoc.getChildren().add(pos);
			}
		}
		
		return pos;
	}
	
	private void resolveIdx(Object[] tuple, String[] aliases) {
		int idx = aliases.length;
		while(--idx >= 0) {
			if(aliases[idx].equals("id")) {
				this.idIdx = idx;
			}
			else if(aliases[idx].equals("parentId")) {
				this.parentIdIdx = idx;
			}
			else if(aliases[idx].equals("name")) {
				this.nameIdx = idx;
			}
			else if(aliases[idx].equals("paystationType")) {
				this.paystationTypeIdx = idx;
			}
			else if(aliases[idx].equals("isUnassigned")) {
				this.isUnassignedIdx = idx;
			}
			else if(aliases[idx].equals("isDeleted")) {
				this.isDeletedIdx = idx;
			}
			else if(aliases[idx].equals("isVisible")) {
				this.isVisibleIdx = idx;
			}
			else if(aliases[idx].equals("isActivated")) {
				this.isActivatedIdx = idx;
			}
		}
	}
}
