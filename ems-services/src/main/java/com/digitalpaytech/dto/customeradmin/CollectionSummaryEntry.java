package com.digitalpaytech.dto.customeradmin;

import java.util.Date;

public class CollectionSummaryEntry extends PaystationSummaryEntry {
    
    private static final long serialVersionUID = 5460066454939723791L;
    private Integer billAmount;
    private Integer coinAmount;
    private Integer cardAmount;
    private Integer totalAmount;
    
    private Short billCount;
    private Short coinCount;
    private Short cardCount;

	private Date collectionDate;
    
    public CollectionSummaryEntry() {
        
    }
    
    public final Integer getBillAmount() {
        return this.billAmount;
    }
    
    public final void setBillAmount(final Integer billAmount) {
        this.billAmount = billAmount;
    }
    
    public final Integer getCoinAmount() {
        return this.coinAmount;
    }
    
    public final void setCoinAmount(final Integer coinAmount) {
        this.coinAmount = coinAmount;
    }
    
    public final Integer getCardAmount() {
        return this.cardAmount;
    }
    
    public final void setCardAmount(final Integer cardAmount) {
        this.cardAmount = cardAmount;
    }
    
    public final Integer getTotalAmount() {
        return this.totalAmount;
    }
    
    public final void setTotalAmount(final Integer totalAmount) {
        this.totalAmount = totalAmount;
    }
    
    public final Short getBillCount() {
        return this.billCount;
    }
    
    public final void setBillCount(final Short billCount) {
        this.billCount = billCount;
    }
    
    public final Short getCoinCount() {
        return this.coinCount;
    }
    
    public final void setCoinCount(final Short coinCount) {
        this.coinCount = coinCount;
    }
    
    public final Short getCardCount() {
        return this.cardCount;
    }
    
    public final void setCardCount(final Short cardCount) {
        this.cardCount = cardCount;
    }
    
    public Date getCollectionDate() {
		return collectionDate;
	}

	public void setCollectionDate(Date collectionDate) {
		this.collectionDate = collectionDate;
	}
	
}
