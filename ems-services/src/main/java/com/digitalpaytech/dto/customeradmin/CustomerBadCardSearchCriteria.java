package com.digitalpaytech.dto.customeradmin;

import java.io.Serializable;
import java.util.Date;

public class CustomerBadCardSearchCriteria implements Serializable, Cloneable {
    private static final long serialVersionUID = 5787086284722456148L;
    private Integer customerId;
    private String cardNumber;
    private Short cardExpiry;
    private Integer cardType;
    private Integer customerCardType;
    private Integer page;
    private String[] orderColumn;
    private boolean orderDesc;
    private Integer itemsPerPage;
    private Date maxUpdatedTime;
    private int maxRetry;

    
    public CustomerBadCardSearchCriteria() {
    }
    
    public final Integer getCustomerId() {
        return this.customerId;
    }
    
    public final void setCustomerId(final Integer customerId) {
        this.customerId = customerId;
    }
    
    public final String getCardNumber() {
        return this.cardNumber;
    }
    
    public final void setCardNumber(final String cardNumber) {
        this.cardNumber = cardNumber;
    }
    
    public final Integer getCardType() {
        return this.cardType;
    }
    
    public final void setCardType(final Integer cardType) {
        this.cardType = cardType;
    }
    
    public final Integer getPage() {
        return this.page;
    }
    
    public final void setPage(final Integer page) {
        this.page = page;
    }
    
    public final String[] getOrderColumn() {
        return this.orderColumn;
    }
    
    public final void setOrderColumn(final String[] orderColumn) {
        this.orderColumn = orderColumn;
    }
    
    public final boolean isOrderDesc() {
        return this.orderDesc;
    }
    
    public final void setOrderDesc(final boolean orderDesc) {
        this.orderDesc = orderDesc;
    }
    
    public final Integer getItemsPerPage() {
        return this.itemsPerPage;
    }
    
    public final void setItemsPerPage(final Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }
    
    public final Date getMaxUpdatedTime() {
        return this.maxUpdatedTime;
    }
    
    public final void setMaxUpdatedTime(final Date maxUpdatedTime) {
        this.maxUpdatedTime = maxUpdatedTime;
    }

    public final Short getCardExpiry() {
        return this.cardExpiry;
    }

    public final void setCardExpiry(final Short cardExpiry) {
        this.cardExpiry = cardExpiry;
    }

    public final int getMaxRetry() {
        return this.maxRetry;
    }

    public final void setMaxRetry(final int maxRetry) {
        this.maxRetry = maxRetry;
    }

    public Integer getCustomerCardType() {
        return this.customerCardType;
    }

    public void setCustomerCardType(final Integer customerCardType) {
        this.customerCardType = customerCardType;
    }
}
