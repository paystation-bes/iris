package com.digitalpaytech.dto.customeradmin;

import java.io.Serializable;
import java.util.Date;

public class CouponDTO implements Serializable {
	private static final long serialVersionUID = 1265479934521526340L;
	
	private String randomId;
	private String customerRandomId;
	private transient Integer id;
	private String coupon;
	private short percentDiscount;
	private int dollarDiscountAmount;
	
	private String discountType;
	private String discount;
	
	private Date startDateLocal;
	private String startDate;
	
	private Date endDateLocal;
	private String endDate;
	
	private Character numUsesType;
	private String maxNumberOfUses;
	private String numberOfUsesRemaining;
	
	private String locationRandomId;
	private String locationName;
	
	private String description;
    private boolean pndEnabled;
    private boolean pbsEnabled;
	private int validForNumOfDay;
	private String spaceRange;
	
	private boolean offline;
	
	private String consumerName;
	private String consumerRandomId;
	
	public CouponDTO() {
		
	}

	public String getRandomId() {
		return randomId;
	}

	public void setRandomId(String randomId) {
		this.randomId = randomId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCoupon() {
		return coupon;
	}

	public void setCoupon(String coupon) {
		this.coupon = coupon;
	}

	public short getPercentDiscount() {
		return percentDiscount;
	}

	public void setPercentDiscount(short percentDiscount) {
		this.percentDiscount = percentDiscount;
	}

	public int getDollarDiscountAmount() {
		return dollarDiscountAmount;
	}

	public void setDollarDiscountAmount(int dollarDiscountAmount) {
		this.dollarDiscountAmount = dollarDiscountAmount;
	}

	public Date getStartDateLocal() {
		return startDateLocal;
	}

	public void setStartDateLocal(Date startDateLocal) {
		this.startDateLocal = startDateLocal;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public Date getEndDateLocal() {
		return endDateLocal;
	}

	public void setEndDateLocal(Date endDateLocal) {
		this.endDateLocal = endDateLocal;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getMaxNumberOfUses() {
		return maxNumberOfUses;
	}

	public void setMaxNumberOfUses(String maxNumberOfUses) {
		this.maxNumberOfUses = maxNumberOfUses;
	}

	public String getNumberOfUsesRemaining() {
		return numberOfUsesRemaining;
	}

	public void setNumberOfUsesRemaining(String numberOfUsesRemaining) {
		this.numberOfUsesRemaining = numberOfUsesRemaining;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public String getLocationRandomId() {
		return locationRandomId;
	}

	public void setLocationRandomId(String locationRandomId) {
		this.locationRandomId = locationRandomId;
	}

	public int getValidForNumOfDay() {
		return validForNumOfDay;
	}

	public void setValidForNumOfDay(int validForNumOfDay) {
		this.validForNumOfDay = validForNumOfDay;
	}

	public boolean isPndEnabled() {
		return pndEnabled;
	}

	public void setPndEnabled(boolean pndEnabled) {
		this.pndEnabled = pndEnabled;
	}

	public boolean isPbsEnabled() {
		return pbsEnabled;
	}

	public void setPbsEnabled(boolean pbsEnabled) {
		this.pbsEnabled = pbsEnabled;
	}

	public String getSpaceRange() {
		return spaceRange;
	}

	public void setSpaceRange(String spaceRange) {
		this.spaceRange = spaceRange;
	}

	public String getCustomerRandomId() {
		return customerRandomId;
	}

	public void setCustomerRandomId(String customerRandomId) {
		this.customerRandomId = customerRandomId;
	}

	public Character getNumUsesType() {
		return numUsesType;
	}

	public void setNumUsesType(Character numUsesType) {
		this.numUsesType = numUsesType;
	}

	public boolean isOffline() {
		return offline;
	}

	public void setOffline(boolean offline) {
		this.offline = offline;
	}

	public String getConsumerName() {
		return consumerName;
	}

	public void setConsumerName(String consumerName) {
		this.consumerName = consumerName;
	}

	public String getConsumerRandomId() {
		return consumerRandomId;
	}

	public void setConsumerRandomId(String consumerRandomId) {
		this.consumerRandomId = consumerRandomId;
	}
}
