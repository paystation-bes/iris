package com.digitalpaytech.dto.customeradmin;

import java.io.Serializable;

public class RouteInputFormInfo implements Serializable {
	private static final long serialVersionUID = 6384201626637769601L;
	
	private String randomId;
	private String name;
	private String parentName;
	private boolean isParent;

	public String getRandomId() {
		return randomId;
	}

	public void setRandomId(String randomId) {
		this.randomId = randomId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

    public boolean getIsParent() {
        return isParent;
    }

    public void setIsParent(boolean isParent) {
        this.isParent = isParent;
    }
}
