package com.digitalpaytech.dto.customeradmin;

import java.math.BigDecimal;
import java.util.Date;
import java.util.TimeZone;

import com.digitalpaytech.domain.PosEventHistory;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.support.RelativeDateTime;
import com.digitalpaytech.util.support.WebObjectId;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

public class AlertInfoEntry {
    
    private RelativeDateTime alertDate;
    private RelativeDateTime resolvedDate;
    
    private String alertMessage;
    private Integer severity;
    private String severityName;
    private String module;
    private Boolean isActive;
    private Boolean isInfo;
    private Boolean isClearable;
    private String duration;
    
    private WebObjectId alertId;
    private WebObjectId clearId;
    
    private WebObjectId posAlertRandomId;
    private String pointOfSaleName;
    private WebObjectId posRandomId;
    
    private Integer payStationType;
    private String serialNumber;
    
    private BigDecimal longitude;
    private BigDecimal latitude;
    
    private String lastModifiedByUserName;
    
    //Used for date comparator
    @XStreamOmitField
    private Date date;
    
    public AlertInfoEntry() {
    }
    
    public AlertInfoEntry(final Integer posEventHistoryId, final Integer posEventCurrentId, final Date alertDate, final Date resolvedDate,
            final String alertMessage, final Integer severity, final String module, final TimeZone timeZone, final Boolean isActive,
            final Boolean isInfo, final String severityName) {
        this.alertDate = new RelativeDateTime(alertDate, timeZone);
        this.date = alertDate;
        if (resolvedDate != null) {
            this.resolvedDate = new RelativeDateTime(resolvedDate, timeZone);
            this.duration = DateUtil.calculateDuration(alertDate, resolvedDate);
        }
        this.alertMessage = alertMessage;
        this.severity = severity;
        this.module = module;
        this.isActive = isActive == null ? true : isActive;
        this.isInfo = isInfo == null ? false : isInfo;
        
        this.severityName = severityName;
        this.posAlertRandomId = new WebObjectId(PosEventHistory.class, posEventHistoryId);
    }
    
    public final RelativeDateTime getAlertDate() {
        return this.alertDate;
    }
    
    public final void setAlertDate(final RelativeDateTime alertDate) {
        this.alertDate = alertDate;
    }
    
    public final RelativeDateTime getResolvedDate() {
        return this.resolvedDate;
    }
    
    public final void setResolvedDate(final RelativeDateTime resolvedDate) {
        this.resolvedDate = resolvedDate;
    }
    
    public final String getAlertMessage() {
        return this.alertMessage;
    }
    
    public final void setAlertMessage(final String alertMessage) {
        this.alertMessage = alertMessage;
    }
    
    public final Integer getSeverity() {
        return this.severity;
    }
    
    public final void setSeverity(final Integer severity) {
        this.severity = severity;
    }
    
    public final String getModule() {
        return this.module;
    }
    
    public final void setModule(final String module) {
        this.module = module;
    }
    
    public final Boolean getIsActive() {
        return this.isActive;
    }
    
    public final void setIsActive(final Boolean isActive) {
        this.isActive = isActive;
    }
    
    public final Boolean getIsInfo() {
        return this.isInfo;
    }
    
    public final void setIsInfo(final Boolean isInfo) {
        this.isInfo = isInfo;
    }
    
    public final Date getDate() {
        return this.date;
    }
    
    public final void setDate(final Date date) {
        this.date = date;
    }
    
    public final String getDuration() {
        return this.duration;
    }
    
    public final void setDuration(final String duration) {
        this.duration = duration;
    }
    
    public final String getSeverityName() {
        return this.severityName;
    }
    
    public final void setSeverityName(final String severityName) {
        this.severityName = severityName;
    }
    
    public final WebObjectId getPosAlertRandomId() {
        return this.posAlertRandomId;
    }
    
    public final void setPosAlertRandomId(final WebObjectId posAlertRandomId) {
        this.posAlertRandomId = posAlertRandomId;
    }
    
    public final String getPointOfSaleName() {
        return this.pointOfSaleName;
    }
    
    public final void setPointOfSaleName(final String pointOfSaleName) {
        this.pointOfSaleName = pointOfSaleName;
    }
    
    public final WebObjectId getPosRandomId() {
        return this.posRandomId;
    }
    
    public final void setPosRandomId(final WebObjectId posRandomId) {
        this.posRandomId = posRandomId;
    }
    
    public final Integer getPayStationType() {
        return this.payStationType;
    }
    
    public final void setPayStationType(final Integer payStationType) {
        this.payStationType = payStationType;
    }
    
    public final String getSerialNumber() {
        return this.serialNumber;
    }
    
    public final void setSerialNumber(final String serialNumber) {
        this.serialNumber = serialNumber;
    }
    
    public final BigDecimal getLongitude() {
        return this.longitude;
    }
    
    public final void setLongitude(final BigDecimal longitude) {
        this.longitude = longitude;
    }
    
    public final BigDecimal getLatitude() {
        return this.latitude;
    }
    
    public final void setLatitude(final BigDecimal latitude) {
        this.latitude = latitude;
    }
    
    public final WebObjectId getAlertId() {
        return this.alertId;
    }
    
    public final void setAlertId(final WebObjectId alertId) {
        this.alertId = alertId;
    }
    
    public final WebObjectId getClearId() {
        return this.clearId;
    }
    
    public final void setClearId(final WebObjectId clearId) {
        this.clearId = clearId;
    }
    
    public final String getLastModifiedByUserName() {
        return this.lastModifiedByUserName;
    }
    
    public final void setLastModifiedByUserName(final String lastModifiedByUserName) {
        this.lastModifiedByUserName = lastModifiedByUserName;
    }
    
    public final Boolean getIsClearable() {
        return this.isClearable;
    }
    
    public final void setIsClearable(final Boolean isClearable) {
        this.isClearable = isClearable;
    }
}
