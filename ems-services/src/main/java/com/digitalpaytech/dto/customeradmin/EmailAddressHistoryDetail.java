package com.digitalpaytech.dto.customeradmin;

import java.io.Serializable;

public class EmailAddressHistoryDetail implements Serializable {
    private static final long serialVersionUID = 7277956753471719793L;
    
    private String emailAddress;
    private String sentEmailDate;
    private Boolean isEmailSent;
    
    public EmailAddressHistoryDetail() {
        
    }
    
    public EmailAddressHistoryDetail(String emailAddress, String sentEmailDate, Boolean isEmailSent) {
        this.emailAddress = emailAddress;
        this.sentEmailDate = sentEmailDate;
        this.isEmailSent = isEmailSent;
    }
    
    public Boolean getIsEmailSent() {
        return isEmailSent;
    }
    
    public void setIsEmailSent(Boolean isEmailSent) {
        this.isEmailSent = isEmailSent;
    }
    
    public String getSentEmailDate() {
        return sentEmailDate;
    }
    
    public void setSentEmailDate(String sentEmailDate) {
        this.sentEmailDate = sentEmailDate;
    }
    
    public String getEmailAddress() {
        return emailAddress;
    }
    
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
