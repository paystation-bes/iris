package com.digitalpaytech.dto.customeradmin;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias(value = "SortedSettingListInfo")
public class SortedSettingListInfo {

	private String randomId;
	private String name;
	private String type;
	private boolean isActive;
	private Short alertTypeId;
	
	public SortedSettingListInfo() {
	}
	
	public SortedSettingListInfo(String name, String type) {
		this.name = name;
		this.type = type;
	}
	
	public SortedSettingListInfo(String randomId, String name, String type) {
		this.randomId = randomId;
		this.name = name;
		this.type = type;
	}
	
	public SortedSettingListInfo(String randomId, String name, String type, boolean isActive, Short alertTypeId) {
        this.randomId = randomId;
        this.name = name;
        this.type = type;
        this.isActive = isActive;
        this.alertTypeId = alertTypeId;
    }

	public String getRandomId() {
		return randomId;
	}

	public void setRandomId(String randomId) {
		this.randomId = randomId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Short getAlertTypeId() {
        return alertTypeId;
    }

    public void setAlertTypeId(Short alertTypeId) {
        this.alertTypeId = alertTypeId;
    }
}
