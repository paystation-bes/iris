package com.digitalpaytech.dto.customeradmin;

import java.util.ArrayList;
import java.util.Collection;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class DefinedAlertDetails {
    
    private String randomAlertId;
    private String name;
    private boolean status;
    private String alertType;
    private Short alertTypeId;
    private Float alertThresholdExceed;
    private Short alertThresholdExceedDisplay;
    private String route;
    private String randomRouteId;
    private boolean isDelayed;
    private Integer delayedByMinutes;
    
    @XStreamImplicit(itemFieldName = "emailList")
    private Collection<String> emailList;
    
    public DefinedAlertDetails() {
        this.emailList = new ArrayList<String>();
    }
    
    public String getRandomAlertId() {
        return this.randomAlertId;
    }
    
    public void setRandomAlertId(final String randomAlertId) {
        this.randomAlertId = randomAlertId;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public boolean getStatus() {
        return this.status;
    }
    
    public void setStatus(final boolean status) {
        this.status = status;
    }
    
    public String getAlertType() {
        return this.alertType;
    }
    
    public void setAlertType(final String alertType) {
        this.alertType = alertType;
    }
    
    public Float getAlertThresholdExceed() {
        return this.alertThresholdExceed;
    }
    
    public void setAlertThresholdExceed(final Float alertThresholdExceed) {
        this.alertThresholdExceed = alertThresholdExceed;
    }
    
    public String getRoute() {
        return this.route;
    }
    
    public void setRoute(final String route) {
        this.route = route;
    }
    
    public String getRandomRouteId() {
        return this.randomRouteId;
    }
    
    public void setRandomRouteId(final String randomRouteId) {
        this.randomRouteId = randomRouteId;
    }
    
    public Collection<String> getEmailList() {
        return this.emailList;
    }
    
    public void setEmailList(final Collection<String> emailList) {
        this.emailList = emailList;
    }
    
    public Short getAlertTypeId() {
        return this.alertTypeId;
    }
    
    public void setAlertTypeId(final Short alertTypeId) {
        this.alertTypeId = alertTypeId;
    }
    
    public Short getAlertThresholdExceedDisplay() {
        return this.alertThresholdExceedDisplay;
    }
    
    public void setAlertThresholdExceedDisplay(final Short alertThresholdExceedDisplay) {
        this.alertThresholdExceedDisplay = alertThresholdExceedDisplay;
    }
    
    public boolean getIsDelayed() {
        return this.isDelayed;
    }
    
    public void setIsDelayed(final boolean isDelayed) {
        this.isDelayed = isDelayed;
    }
    
    public Integer getDelayedByMinutes() {
        return this.delayedByMinutes;
    }
    
    public void setDelayedByMinutes(final Integer delayedByMinutes) {
        this.delayedByMinutes = delayedByMinutes;
    }
    
}
