package com.digitalpaytech.dto.customeradmin;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias(value = "permissionTreeWrapper")
public class PermissionTreeWrapper {
    
    @XStreamImplicit(itemFieldName = "permissionTrees")
    private List<PermissionTree> permissionTrees;
    @XStreamImplicit(itemFieldName = "secondaryPermissionTrees")
    private List<PermissionTree> secondaryPermissionTrees;
    
    public final List<PermissionTree> getPermissionTrees() {
        return this.permissionTrees;
    }
    
    public final void setPermissionTrees(final List<PermissionTree> permissionTrees) {
        this.permissionTrees = permissionTrees;
    }
    
    public final List<PermissionTree> getSecondaryPermissionTrees() {
        return this.secondaryPermissionTrees;
    }
    
    public final void setSecondaryPermissionTrees(final List<PermissionTree> secondaryPermissionTrees) {
        this.secondaryPermissionTrees = secondaryPermissionTrees;
    }
    
}
