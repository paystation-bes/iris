package com.digitalpaytech.dto.customeradmin;

import java.sql.Timestamp;

public class ActivityLogDetail {

	private String logTypeName;
	private String detail;
	private Timestamp dateTime;
	
	public ActivityLogDetail() {
		
	}
	
	public ActivityLogDetail(String logTypeName, String detail, Timestamp dateTime) {
		this.logTypeName = logTypeName;
		this.detail = detail;
		this.dateTime = dateTime;
	}

	public String getLogTypeName() {
		return logTypeName;
	}

	public void setLogTypeName(String logTypeName) {
		this.logTypeName = logTypeName;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Timestamp getDateTime() {
		return dateTime;
	}

	public void setDateTime(Timestamp dateTime) {
		this.dateTime = dateTime;
	}
}
