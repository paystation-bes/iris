package com.digitalpaytech.dto.customeradmin;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias(value = "roleSettingInfo")
public class RoleSettingInfo {
    
    private String randomizedRoleId;
    private String roleName;
    private String status;
    private boolean isEnabled;
    private boolean isLocked;
    private boolean hasUsers;
    private boolean isFromParent;
    private String customerName;
    private int customerTypeId;
    
    public RoleSettingInfo(final String randomizedRoleId, final String roleName) {
        this.randomizedRoleId = randomizedRoleId;
        this.roleName = roleName;
    }
    
    public RoleSettingInfo(final String randomizedRoleId, final String roleName, final String status, final int customerTypeId,
            final boolean isFromParent) {
        this.randomizedRoleId = randomizedRoleId;
        this.roleName = roleName;
        this.status = status;
        this.customerTypeId = customerTypeId;
        this.isFromParent = isFromParent;
    }
    
    public RoleSettingInfo(final String randomizedRoleId, final String roleName, final String status, final int customerTypeId,
            final boolean isFromParent, final String customerName) {
        this.randomizedRoleId = randomizedRoleId;
        this.roleName = roleName;
        this.status = status;
        this.customerTypeId = customerTypeId;
        this.isFromParent = isFromParent;
        this.customerName = customerName;
    }
    
    public RoleSettingInfo(final String randomizedRoleId, final String roleName, final String status, final boolean isEnabled, final boolean isLocked) {
        this.randomizedRoleId = randomizedRoleId;
        this.roleName = roleName;
        this.status = status;
        this.isEnabled = isEnabled;
        this.isLocked = isLocked;
    }
    
    public RoleSettingInfo(final String randomizedRoleId, final String roleName, final String status, final boolean isEnabled,
            final boolean isLocked, final boolean hasUsers, final int customerTypeId, final boolean isFromParent) {
        this.randomizedRoleId = randomizedRoleId;
        this.roleName = roleName;
        this.status = status;
        this.isEnabled = isEnabled;
        this.isLocked = isLocked;
        this.hasUsers = hasUsers;
        this.customerTypeId = customerTypeId;
        this.isFromParent = isFromParent;
    }
    
    public final String getRandomizedRoleId() {
        return this.randomizedRoleId;
    }
    
    public final void setRandomizedRoleId(final String randomizedRoleId) {
        this.randomizedRoleId = randomizedRoleId;
    }
    
    public final String getRoleName() {
        return this.roleName;
    }
    
    public final void setRoleName(final String roleName) {
        this.roleName = roleName;
    }
    
    public final String getStatus() {
        return this.status;
    }
    
    public final void setStatus(final String status) {
        this.status = status;
    }
    
    public final boolean getIsEnabled() {
        return this.isEnabled;
    }
    
    public final void setIsEnabled(final boolean isEnabled) {
        this.isEnabled = isEnabled;
    }
    
    public final boolean getIsLocked() {
        return this.isLocked;
    }
    
    public final void setIsLocked(final boolean isLocked) {
        this.isLocked = isLocked;
    }
    
    public final boolean isHasUsers() {
        return this.hasUsers;
    }
    
    public final void setHasUsers(final boolean hasUsers) {
        this.hasUsers = hasUsers;
    }
    
    public final int getCustomerTypeId() {
        return this.customerTypeId;
    }
    
    public final void setCustomerTypeId(final int customerTypeId) {
        this.customerTypeId = customerTypeId;
    }
    
    public final boolean getIsFromParent() {
        return this.isFromParent;
    }
    
    public final void setIsFromParent(final boolean isFromParent) {
        this.isFromParent = isFromParent;
    }
    
    public final String getCustomerName() {
        return this.customerName;
    }
    
    public final void setCustomerName(final String customerName) {
        this.customerName = customerName;
    }
    
}
