package com.digitalpaytech.dto.customeradmin;

import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;

import com.digitalpaytech.util.WebCoreConstants;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class LocationPOSTree implements Serializable {
    
    private static final long serialVersionUID = -7935517276345286703L;
    private String randomId;
    private String name;
    // 0 All, 1 Parent Location, 2 Child Location, 3 Paystation
    private int type;
    private int paystationType;
    private boolean isSelected;
    
//    @XStreamImplicit(itemFieldName = "LocationPOSTree")
    private ArrayList<LocationPOSTree> children;
    
    public LocationPOSTree(String randomId, String name, int type, int paystationType) {
        super();
        this.randomId = randomId;
        this.name = name;
        this.type = type;
        this.paystationType = paystationType;
        this.isSelected = false;
        this.children = new ArrayList<LocationPOSTree>();
    }
    
    public LocationPOSTree(String randomId, String name, int type) {
        super();
        this.randomId = randomId;
        this.name = name;
        this.type = type;
        this.paystationType = 0;
        this.isSelected = false;
        this.children = new ArrayList<LocationPOSTree>();
    }
    
    public String getRandomId() {
        return randomId;
    }
    
    public void setRandomId(String randomId) {
        this.randomId = randomId;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public int getType() {
        return type;
    }
    
    public void setType(int type) {
        this.type = type;
    }
    
    public int getPaystationType() {
        return paystationType;
    }
    
    public void setPaystationType(int paystationType) {
        this.paystationType = paystationType;
    }
    
    public ArrayList<LocationPOSTree> getChildren() {
        return children;
    }
    
    public void setChildren(ArrayList<LocationPOSTree> children) {
        this.children = children;
    }
    
    public void addChild(LocationPOSTree child) {
        this.children.add(child);
    }
    
    public List<LocationPOSTree> findAllLocations(LocationPOSTree locationTree) {
        List<LocationPOSTree> allChildren = new ArrayList<LocationPOSTree>();
        for (LocationPOSTree childLocationTree : locationTree.getChildren()) {
            allChildren.addAll(findAllLocations(childLocationTree));
        }
        allChildren.add(locationTree);
        return allChildren;
    }
    
    public List<LocationPOSTree> findAllLocations() {
        List<LocationPOSTree> allChildren = new ArrayList<LocationPOSTree>();
        for (LocationPOSTree childLocationTree : this.getChildren()) {
            if (childLocationTree.getType() != WebCoreConstants.LOCATIONPOS_TYPE_PAY_STATION) {
                allChildren.add(childLocationTree);
                allChildren.addAll(childLocationTree.findAllLocations());
            }
        }
        return allChildren;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
}
