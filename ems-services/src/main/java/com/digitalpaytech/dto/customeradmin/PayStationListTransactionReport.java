package com.digitalpaytech.dto.customeradmin;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias(value = "payStationListTransactionReportList")
public class PayStationListTransactionReport {

	private String randomId;
	private String date;
	private String type;
	private String paymentType;
	private Float amount;
	
	public PayStationListTransactionReport(){		
	}

	public PayStationListTransactionReport(String randomId, String date,
			String type, String paymentType, Float amount) {
		super();
		this.randomId = randomId;
		this.date = date;
		this.type = type;
		this.paymentType = paymentType;
		this.amount = amount;
	}

	public String getRandomId() {
		return randomId;
	}

	public void setRandomId(String randomId) {
		this.randomId = randomId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public Float getAmount() {
		return amount;
	}

	public void setAmount(Float amount) {
		this.amount = amount;
	}
}
