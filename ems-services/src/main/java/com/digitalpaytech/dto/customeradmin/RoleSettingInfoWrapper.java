package com.digitalpaytech.dto.customeradmin;

import java.util.List;

import com.digitalpaytech.dto.ChildCustomerInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias(value = "roleSettingInfoWrapper")
public class RoleSettingInfoWrapper {
    
    @XStreamImplicit(itemFieldName = "roleSettingInfos")
    private List<RoleSettingInfo> roleSettingInfos;
    @XStreamImplicit(itemFieldName = "childCustomerInfos")
    private List<ChildCustomerInfo> childCustomerInfos;
    private boolean isAllChilds;
    
    public final List<RoleSettingInfo> getRoleSettingInfos() {
        return this.roleSettingInfos;
    }
    
    public final void setRoleSettingInfos(final List<RoleSettingInfo> roleSettingInfos) {
        this.roleSettingInfos = roleSettingInfos;
    }
    
    public final List<ChildCustomerInfo> getChildCustomerInfos() {
        return this.childCustomerInfos;
    }
    
    public final void setChildCustomerInfos(final List<ChildCustomerInfo> childCustomerInfos) {
        this.childCustomerInfos = childCustomerInfos;
    }
    
    public final boolean isAllChilds() {
        return this.isAllChilds;
    }
    
    public final void setAllChilds(final boolean isAllChilds) {
        this.isAllChilds = isAllChilds;
    }
    
}
