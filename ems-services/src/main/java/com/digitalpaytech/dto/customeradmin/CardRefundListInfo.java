package com.digitalpaytech.dto.customeradmin;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * This class holds 
 * 
 * @author Brian Kim
 *
 */
@XStreamAlias(value = "CardRefundListInfo")
public class CardRefundListInfo {
	public static final Comparator<CardRefundListInfo> CMPRTR_TX_DATE = new Comparator<CardRefundListInfo>() {
		@Override
		public int compare(CardRefundListInfo o1, CardRefundListInfo o2) {
			int result = o2.getTransactionDate().compareTo(o1.getTransactionDate());
			if(result == 0) {
				result = o1.getAmount().compareTo(o2.getAmount());
				if(result == 0) {
					result = o1.getRandomProcessorTransactionId().compareTo(o2.getRandomProcessorTransactionId());
				}
			}
			
			return result;
		}
	};

	private String randomProcessorTransactionId;
	private String cardNumber;
	private String expiry;
	private Date transactionDate;
	private String timeZone;
	private BigDecimal amount;
	
	private String transactionDateString;
	private String amountString;
	
	public CardRefundListInfo() {
		
	}
	
	public CardRefundListInfo(String randomProcessorTransactionId,
			String cardNumber, String expiry, Date transactionDate,
			String timeZone, BigDecimal amount) {
		this.randomProcessorTransactionId = randomProcessorTransactionId;
		this.cardNumber = cardNumber;
		this.expiry = expiry;
		this.transactionDate = transactionDate;
		this.timeZone = timeZone;
		this.amount = amount;
	}

	public String getRandomProcessorTransactionId() {
		return randomProcessorTransactionId;
	}

	public void setRandomProcessorTransactionId(String randomProcessorTransactionId) {
		this.randomProcessorTransactionId = randomProcessorTransactionId;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getExpiry() {
		return expiry;
	}

	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

    public String getTransactionDateString() {
        return transactionDateString;
    }

    public void setTransactionDateString(String transactionDateString) {
        this.transactionDateString = transactionDateString;
    }

    public String getAmountString() {
        return amountString;
    }

    public void setAmountString(String amountString) {
        this.amountString = amountString;
    }
}
