package com.digitalpaytech.dto.customeradmin;

import java.math.BigDecimal;
import java.util.Date;

public class CustomerAdminPayStationSettingPayStationInfo implements java.io.Serializable {
    
    private static final long serialVersionUID = -3363352510545241695L;
    private Integer settingsFileId;
    private String settingsFileName;
    private Date uploadGmt;
    
    private Integer pointOfSaleId;
    private Date downloadGmt;
    
    private String name;
    private String randomId;
    private String serialNumber;
    private String locationName;
    private String downloadDate;
    private Integer payStationType;
    private BigDecimal latitude;
    private BigDecimal longitude;
    private Boolean isUpdateInProgress;
    private Boolean isNotExist;
    
    public CustomerAdminPayStationSettingPayStationInfo() {
        
    }
    
    public CustomerAdminPayStationSettingPayStationInfo(String name, String randomId, String serialNumber, String locationName, String downloadDate,
            Integer payStationType, BigDecimal latitude, BigDecimal longitude, Boolean isUpdateInProgress, Boolean isNotExist) {
        this.name = name;
        this.randomId = randomId;
        this.serialNumber = serialNumber;
        this.locationName = locationName;
        this.downloadDate = downloadDate;
        this.payStationType = payStationType;
        this.latitude = latitude;
        this.longitude = longitude;
        this.isUpdateInProgress = isUpdateInProgress;
        this.isNotExist = isNotExist;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getRandomId() {
        return randomId;
    }
    
    public void setRandomId(String randomId) {
        this.randomId = randomId;
    }
    
    public String getSerialNumber() {
        return serialNumber;
    }
    
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
    
    public String getLocationName() {
        return locationName;
    }
    
    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }
    
    public String getDownloadDate() {
        return downloadDate;
    }
    
    public void setDownloadDate(String downloadDate) {
        this.downloadDate = downloadDate;
    }
    
    public Integer getPayStationType() {
        return payStationType;
    }
    
    public void setPayStationType(Integer payStationType) {
        this.payStationType = payStationType;
    }
    
    public BigDecimal getLatitude() {
        return latitude;
    }
    
    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }
    
    public BigDecimal getLongitude() {
        return longitude;
    }
    
    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }
    
    public Boolean isUpdateInProgress() {
        return isUpdateInProgress;
    }
    
    public void setUpdateInProgress(Boolean isUpdateInProgress) {
        this.isUpdateInProgress = isUpdateInProgress;
    }
    
    public Boolean getIsUpdateInProgress() {
        return isUpdateInProgress;
    }
    
    public void setIsUpdateInProgress(Boolean isUpdateInProgress) {
        this.isUpdateInProgress = isUpdateInProgress;
    }
    
    public Boolean isNotExist() {
        return isNotExist;
    }
    
    public void setNotExist(Boolean isNotExist) {
        this.isNotExist = isNotExist;
    }
    
    public Boolean getIsNotExist() {
        return isNotExist;
    }
    
    public void setIsNotExist(Boolean isNotExist) {
        this.isNotExist = isNotExist;
    }
    
    public Integer getSettingsFileId() {
        return settingsFileId;
    }
    
    public void setSettingsFileId(Integer settingsFileId) {
        this.settingsFileId = settingsFileId;
    }
    
    public String getSettingsFileName() {
        return settingsFileName;
    }
    
    public void setSettingsFileName(String settingsFileName) {
        this.settingsFileName = settingsFileName;
    }
    
    public Date getUploadGmt() {
        return uploadGmt;
    }
    
    public void setUploadGmt(Date uploadGmt) {
        this.uploadGmt = uploadGmt;
    }
    
    public Integer getPointOfSaleId() {
        return pointOfSaleId;
    }
    
    public void setPointOfSaleId(Integer pointOfSaleId) {
        this.pointOfSaleId = pointOfSaleId;
    }
    
    public Date getDownloadGmt() {
        return downloadGmt;
    }
    
    public void setDownloadGmt(Date downloadGmt) {
        this.downloadGmt = downloadGmt;
    }
    
}