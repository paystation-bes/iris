package com.digitalpaytech.dto.customeradmin;

import java.math.BigDecimal;
import java.util.Date;
import java.util.TimeZone;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosEventHistory;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.ResultTransformerTemplate;
import com.digitalpaytech.util.WebCoreConstants;

public class AlertInfoEntryTransformer extends ResultTransformerTemplate<AlertInfoEntry> {
    public static final String ATTR_ID = "Id";
    public static final String ATTR_CLEAR_ID = "ClearId";
    public static final String ATTR_POS_ID = "PointOfSaleId";
    public static final String ATTR_ALERT_TYPE_ID = "AlertTypeId";
    public static final String ATTR_ALERT_TYPE_NAME = "AlertTypeName";
    public static final String ATTR_EVENT_TYPE_ID = "EventTypeId";
    public static final String ATTR_EVENT_TYPE_NAME = "EventTypeName";
    public static final String ATTR_DEVICE_ID = "DeviceId";
    public static final String ATTR_DEVICE_NAME = "DeviceName";
    public static final String ATTR_CAT_ID = "CustomerAlertId";
    public static final String ATTR_CAT_NAME = "CustomerAlertName";
    public static final String ATTR_SEVERITY_ID = "SeverityId";
    public static final String ATTR_SEVERITY_NAME = "SeverityName";
    public static final String ATTR_ACTION_TYPE_ID = "ActionTypeId";
    public static final String ATTR_ACTION_TYPE_NAME = "ActionTypeName";
    public static final String ATTR_IS_ACTIVE = "IsActive";
    public static final String ATTR_ALERT_GMT = "AlertGMT";
    public static final String ATTR_CLEAR_GMT = "ClearGMT";
    public static final String ATTR_POS_NAME = "PointOfSaleName";
    public static final String ATTR_SERIAL_NUMBER = "SerialNumber";
    public static final String ATTR_LATITUDE = "Latitude";
    public static final String ATTR_LONGITUDE = "Longitude";
    public static final String ATTR_PAY_STATION_TYPE = "PayStationType";
    
    public static final String ATTR_LAST_MODIFIED_BY_USER_NAME = "LastModifiedByUserName";
    
    private static final long serialVersionUID = -3914305356158746708L;

    public AlertInfoEntryTransformer(final MessageHelper messageHelper, final TimeZone customerTimeZone) {
        super(AlertInfoEntry.class, messageHelper, customerTimeZone);
    }
    
    @Override
    protected AlertInfoEntry createInstance(final Object[] tuple) {
        final AlertInfoEntry result = new AlertInfoEntry();
        result.setAlertId(getAttrWebObjectId(tuple, ATTR_ID, PosEventHistory.class));
        result.setPosAlertRandomId(getAttrWebObjectId(tuple, ATTR_ID, PosEventHistory.class));
        result.setClearId(getAttrWebObjectId(tuple, ATTR_CLEAR_ID, PosEventHistory.class));
        result.setDate((Date) getAttr(tuple, ATTR_ALERT_GMT));
        result.setAlertDate(getAttrRelativeTime(tuple, ATTR_ALERT_GMT));
        result.setResolvedDate(getAttrRelativeTime(tuple, ATTR_CLEAR_GMT));
        result.setSeverity(getAttrInt(tuple, ATTR_SEVERITY_ID));
        result.setSeverityName((String) getAttr(tuple, ATTR_SEVERITY_NAME));
        result.setIsActive(getAttrBoolean(tuple, ATTR_IS_ACTIVE, true));
        result.setIsClearable(getAttr(tuple, ATTR_CAT_ID) == null);
        final Integer actionTypeId = getAttrInt(tuple, ATTR_ACTION_TYPE_ID);
        if (actionTypeId != null) {
            result.setIsInfo(WebCoreConstants.EVENT_ACTION_TYPE_INFO == actionTypeId.intValue());
        } else {
            result.setIsInfo(false);
        }
        
        final Integer deviceId = getAttrInt(tuple, ATTR_DEVICE_ID);
        if (deviceId == null) {
            throw new NullPointerException("EventDeviceTypeId must never be null");
        } else if (deviceId == WebCoreConstants.EVENT_DEVICE_TYPE_ALERT) {
            result.setModule((String) getAttr(tuple, ATTR_ALERT_TYPE_NAME));
            result.setAlertMessage((String) getAttr(tuple, ATTR_CAT_NAME));
        } else {
            result.setModule(getAttrMessage(tuple, ATTR_DEVICE_NAME));
            
            final StringBuilder msg = new StringBuilder();
            Object buff = getAttrMessage(tuple, ATTR_EVENT_TYPE_NAME);
            if (buff != null) {
                msg.append((String) buff);
            }
            
            buff = getAttr(tuple, ATTR_ACTION_TYPE_NAME);
            if (buff != null) {
                msg.append(WebCoreConstants.EMPTY_SPACE);
                msg.append((String) buff);
            }
            
            result.setAlertMessage(msg.toString());
        }
        
        result.setPosRandomId(getAttrWebObjectId(tuple, ATTR_POS_ID, PointOfSale.class));
        result.setPointOfSaleName((String) getAttr(tuple, ATTR_POS_NAME));
        result.setSerialNumber((String) getAttr(tuple, ATTR_SERIAL_NUMBER));
        result.setLatitude((BigDecimal) getAttr(tuple, ATTR_LATITUDE));
        result.setLongitude((BigDecimal) getAttr(tuple, ATTR_LONGITUDE));
        result.setPayStationType((Integer) getAttr(tuple, ATTR_PAY_STATION_TYPE));
        
        result.setLastModifiedByUserName((String) getAttr(tuple, ATTR_LAST_MODIFIED_BY_USER_NAME));
        
        return result;
    }
}
