package com.digitalpaytech.dto.customeradmin;

import java.util.Date;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class CustomerAdminPayStationSettingInfo implements java.io.Serializable {
    
    private static final long serialVersionUID = 5180428559711321330L;
    private String name;
    private Integer settingsFileId;
    private String randomId;
    private Date uploadGmt;
    private String uploadDate;
    private Integer payStationCount;
    private Boolean isUpdateInProgress;
    private Boolean isNotExist;
    private Boolean isFileDeleted;
    
    @XStreamImplicit(itemFieldName = "payStationList")
    private List<CustomerAdminPayStationSettingPayStationInfo> payStationList;

    public CustomerAdminPayStationSettingInfo() {
        this.isUpdateInProgress = false;
        this.isNotExist = false;
        this.isFileDeleted = false;
    }
    
    public CustomerAdminPayStationSettingInfo(String name, String randomId, String uploadDate, Integer payStationCount, Boolean isUpdateInProgress,
                                              Boolean isNotExist, Boolean isFileDeleted) {
        this.name = name;
        this.randomId = randomId;
        this.uploadDate = uploadDate;
        this.payStationCount = payStationCount;
        this.isUpdateInProgress = isUpdateInProgress;
        this.isNotExist = isNotExist;
        this.isFileDeleted = isFileDeleted;
    }
    
    public CustomerAdminPayStationSettingInfo(String name, Integer settingsFileId, Date uploadGmt, Integer payStationCount, Boolean isUpdateInProgress,
                                              Boolean isNotExist, Boolean isFileDeleted) {
        this.name = name;
        this.settingsFileId = settingsFileId;
        this.uploadGmt = uploadGmt;
        this.payStationCount = payStationCount;
        this.isUpdateInProgress = isUpdateInProgress;
        this.isNotExist = isNotExist;
        this.isFileDeleted = isFileDeleted;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getRandomId() {
        return randomId;
    }
    
    public void setRandomId(String randomId) {
        this.randomId = randomId;
    }
    
    public String getUploadDate() {
        return uploadDate;
    }
    
    public void setUploadDate(String uploadDate) {
        this.uploadDate = uploadDate;
    }
    
    public Integer getPayStationCount() {
        return payStationCount;
    }
    
    public void setPayStationCount(Integer payStationCount) {
        this.payStationCount = payStationCount;
    }
    
    public Boolean isUpdateInProgress() {
        return isUpdateInProgress;
    }
    
    public Boolean getIsUpdateInProgress() {
        return isUpdateInProgress;
    }
    
    public void setIsUpdateInProgress(Boolean isUpdateInProgress) {
        this.isUpdateInProgress = isUpdateInProgress;
    }
    
    public Boolean isNotExist() {
        return isNotExist;
    }
    
    public void setNotExist(Boolean isNotExist) {
        this.isNotExist = isNotExist;
    }
    
    public Boolean getIsNotExist() {
        return isNotExist;
    }
    
    public void setIsNotExist(Boolean isNotExist) {
        this.isNotExist = isNotExist;
    }
    
    public Boolean isFileDeleted() {
        return isFileDeleted;
    }
    
    public void setFileDeleted(Boolean isFileDeleted) {
        this.isFileDeleted = isFileDeleted;
    }
    
    public Boolean getIsFileDeleted() {
        return isFileDeleted;
    }
    
    public void setIsFileDeleted(Boolean isFileDeleted) {
        this.isFileDeleted = isFileDeleted;
    }
    
    public List<CustomerAdminPayStationSettingPayStationInfo> getPayStationList() {
        return payStationList;
    }
    
    public void setPayStationList(List<CustomerAdminPayStationSettingPayStationInfo> payStationList) {
        this.payStationList = payStationList;
    }

    public Integer getSettingsFileId() {
        return settingsFileId;
    }

    public void setSettingsFileId(Integer settingsFileId) {
        this.settingsFileId = settingsFileId;
    }

    public Date getUploadGmt() {
        return uploadGmt;
    }

    public void setUploadGmt(Date uploadGmt) {
        this.uploadGmt = uploadGmt;
    }
    
}