package com.digitalpaytech.dto.customeradmin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Transient;

public class LocationTree implements Serializable {
    private static final long serialVersionUID = 5398783764030053075L;
    
    private String randomId;
    private String name;
    
    private Integer type;
    private Integer paystationType;
    private Integer severity;
    
    private boolean isParent;
    private boolean isUnassigned;
    private boolean isDeleted;
    private boolean isVisible = true;
    private boolean isActivated = true;
    
    private boolean isOrganization = false;
    private boolean isLocation = false;
    private boolean isChildLocation = false;
    private boolean isRoute = false;
    private boolean isPayStation = false;
    
    @Transient
    private transient Integer id;
    
    @Transient
    private transient Integer parentId;
    
    private List<LocationTree> children = new ArrayList<LocationTree>(1);
    
    public LocationTree() {
        
    }
    
    public LocationTree(int id) {
        this.id = id;
    }
    
    public LocationTree(String randomId, String name, boolean isParent, boolean isUnassigned) {
        this.randomId = randomId;
        this.name = name;
        this.isParent = isParent;
        this.isUnassigned = isUnassigned;
    }
    
    public LocationTree(String randomId, String name, boolean isParent, boolean isUnassigned, int id) {
        this.randomId = randomId;
        this.name = name;
        this.isParent = isParent;
        this.isUnassigned = isUnassigned;
        this.id = id;
    }
    
    public LocationTree(String randomId, String name, boolean isParent, boolean isUnassigned, Integer severity) {
        this.randomId = randomId;
        this.name = name;
        this.isParent = isParent;
        this.isUnassigned = isUnassigned;
        this.severity = severity;
    }
    
    public LocationTree(String randomId, String name, boolean isParent, boolean isUnassigned, int id, Integer severity) {
        this.randomId = randomId;
        this.name = name;
        this.isParent = isParent;
        this.isUnassigned = isUnassigned;
        this.id = id;
        this.severity = severity;
    }
    
    public String getRandomId() {
        return randomId;
    }
    
    public void setRandomId(String randomId) {
        this.randomId = randomId;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public boolean getIsParent() {
        return isParent;
    }
    
    public boolean isIsParent() {
        return isParent;
    }
    
    public boolean isParent() {
        return isParent;
    }
    
    public void setParent(boolean isParent) {
        this.isParent = isParent;
    }
    
    public boolean getIsUnassigned() {
        return isUnassigned;
    }
    
    public boolean isIsUnassigned() {
        return isUnassigned;
    }
    
    public boolean isUnassigned() {
        return isUnassigned;
    }
    
    public void setUnassigned(boolean isUnassigned) {
        this.isUnassigned = isUnassigned;
    }
    
    public List<LocationTree> getChildren() {
        return children;
    }
    
    public void setChildren(List<LocationTree> children) {
        this.children = children;
    }
    
    public void addChild(LocationTree child) {
        this.children.add(child);
    }
    
    public List<LocationTree> findAllLocations(LocationTree locationTree) {
        List<LocationTree> allChildren = new ArrayList<LocationTree>();
        for (LocationTree childLocationTree : locationTree.getChildren()) {
            allChildren.addAll(findAllLocations(childLocationTree));
        }
        allChildren.add(locationTree);
        return allChildren;
    }
    
    public String printTree(LocationTree locationTree, String level) {
        StringBuilder strBuilder = new StringBuilder();
        if (locationTree == null) {
            return "";
        } else {
            strBuilder.append(level + locationTree.getName() + "\n");
            List<LocationTree> children = locationTree.getChildren();
            for (LocationTree child : children) {
                strBuilder.append(printTree(child, level + "-"));
            }
        }
        return strBuilder.toString();
    }
    
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public boolean isPayStation() {
        return isPayStation;
    }
    
    public void setPayStation(boolean isPayStation) {
        this.isPayStation = isPayStation;
    }
    
    public Integer getType() {
        return type;
    }
    
    public void setType(Integer type) {
        this.type = type;
    }
    
    public Integer getPaystationType() {
        return paystationType;
    }
    
    public void setPaystationType(Integer paystationType) {
        this.paystationType = paystationType;
    }
    
    public Integer getParentId() {
        return parentId;
    }
    
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }
    
    public boolean isLocation() {
        return isLocation;
    }
    
    public void setLocation(boolean isLocation) {
        this.isLocation = isLocation;
    }
    
    public boolean isChildLocation() {
        return isChildLocation;
    }
    
    public void setChildLocation(boolean isChildLocation) {
        this.isChildLocation = isChildLocation;
    }
    
    public boolean isDeleted() {
        return isDeleted;
    }
    
    public void setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
    
    public boolean isOrganization() {
        return isOrganization;
    }
    
    public void setOrganization(boolean isOrganization) {
        this.isOrganization = isOrganization;
    }
    
    public boolean isRoute() {
        return isRoute;
    }
    
    public void setRoute(boolean isRoute) {
        this.isRoute = isRoute;
    }
    
    public boolean isVisible() {
        return isVisible;
    }
    
    public void setVisible(boolean isVisible) {
        this.isVisible = isVisible;
    }
    
    public boolean isActivated() {
        return isActivated;
    }
    
    public void setActivated(boolean isActivated) {
        this.isActivated = isActivated;
    }
    
    public Integer getSeverity() {
        return severity;
    }
    
    public void setSeverity(Integer severity) {
        this.severity = severity;
    }
    
}
