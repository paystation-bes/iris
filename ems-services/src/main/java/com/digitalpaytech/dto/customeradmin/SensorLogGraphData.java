package com.digitalpaytech.dto.customeradmin;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class SensorLogGraphData {

	@XStreamImplicit(itemFieldName="sensorGraph")
	private List<SensorLogGraphEntry> graphData;
	
	public SensorLogGraphData(){
		graphData = new ArrayList<SensorLogGraphEntry>();
	}
	
	public List<SensorLogGraphEntry> getGraphData() {
		return graphData;
	}

	public void setGraphData(List<SensorLogGraphEntry> graphData) {
		this.graphData = graphData;
	}
	
	public void addSensorLogEntry(Date date, float value){
		SensorLogGraphEntry logEntry = new SensorLogGraphEntry();
				
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);		
		
		List<BigDecimal> entry = new ArrayList<BigDecimal>();
		
		entry.add(BigDecimal.valueOf(cal.getTimeInMillis()));		
		entry.add(BigDecimal.valueOf(value).setScale(3, BigDecimal.ROUND_HALF_UP).stripTrailingZeros());
		
		logEntry.setGraphData(entry);
		
		graphData.add(logEntry);		
	}

	private static class SensorLogGraphEntry{

		@XStreamImplicit(itemFieldName="sensorDataPoint")
		private List<BigDecimal> graphData;
		
		SensorLogGraphEntry(){		
			graphData = new ArrayList<BigDecimal>();
		}

		public List<BigDecimal> getGraphData() {
			return graphData;
		}
		public void setGraphData(List<BigDecimal> graphData) {
			this.graphData = graphData;
		}
		
	}
}
