package com.digitalpaytech.dto.customeradmin;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias(value = "payStationListCollectionReport")
public class PayStationListCollectionReport {

	private String randomId;
	private String date;
	private String type;
	private Float amount;
	
	public PayStationListCollectionReport(){		
	}

	public PayStationListCollectionReport(String randomId, String date,
			String type, Float amount) {
		super();
		this.randomId = randomId;
		this.date = date;
		this.type = type;
		this.amount = amount;
	}

	public String getRandomId() {
		return randomId;
	}

	public void setRandomId(String randomId) {
		this.randomId = randomId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Float getAmount() {
		return amount;
	}

	public void setAmount(Float amount) {
		this.amount = amount;
	}
}
