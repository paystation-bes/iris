package com.digitalpaytech.dto.customeradmin;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.WebCoreConstants;

public class AlertCentreSummaryInfo {
    private float batteryVoltage;
    private int batteryLevel;
    private int paperLevel;
    
    private int payStationSeverity;
    private int lastSeenSeverity;
    
    private String posName;
    private String serial;
    private String lastSeen;
    private String locationName;
    private String locationRandomId;
    private String routeName;
    private String routeRandomId;
    
    private String posRandomId;
    
    private Integer payStationType;
    
    public AlertCentreSummaryInfo() {
    }
    
    public final float getBatteryVoltage() {
        return this.batteryVoltage;
    }
    
    public final void setBatteryVoltage(final float batteryVoltage) {
        this.batteryVoltage = batteryVoltage;
    }
    
    public final int getBatteryLevel() {
        return this.batteryLevel;
    }
    
    public final void setBatteryLevel(final int batteryLevel) {
        this.batteryLevel = batteryLevel;
    }
    
    public final int getPaperLevel() {
        return this.paperLevel;
    }
    
    public final void setPaperLevel(final int paperLevel) {
        this.paperLevel = paperLevel;
    }
    
    public final String getPosName() {
        return this.posName;
    }
    
    public final void setPosName(final String posName) {
        this.posName = posName;
    }
    
    public final String getSerial() {
        return this.serial;
    }
    
    public final void setSerial(final String serial) {
        this.serial = serial;
    }
    
    public final String getLastSeen() {
        return this.lastSeen;
    }
    
    public final void setLastSeen(final String lastSeen) {
        this.lastSeen = lastSeen;
    }
    
    public final String getLocationName() {
        return this.locationName;
    }
    
    public final void setLocationName(final String locationName) {
        this.locationName = locationName;
    }
    
    public final String getRouteName() {
        return this.routeName;
    }
    
    public final void setRouteName(final String routeName) {
        this.routeName = routeName;
    }
    
    public final Integer getPayStationType() {
        return this.payStationType;
    }
    
    public final void setPayStationType(final Integer payStationType) {
        this.payStationType = payStationType;
    }
    
    public final void setPayStationDetails(final PointOfSale pointOfSale, final String lastSeen, final String routeName, final String routeRandomId,
                                           final RandomKeyMapping keyMapping) {
        
        this.posName = pointOfSale.getName();
        this.serial = pointOfSale.getSerialNumber();
        this.locationName = pointOfSale.getLocation().getName();
        this.payStationType = pointOfSale.getPaystation().getPaystationType().getId();
        this.lastSeen = lastSeen;
        this.locationRandomId = keyMapping.getRandomString(pointOfSale.getLocation(), WebCoreConstants.ID_LOOK_UP_NAME);
        this.routeName = routeName;
        this.setRouteRandomId(routeRandomId);
        
    }
    
    public final String getLocationRandomId() {
        return this.locationRandomId;
    }
    
    public final void setLocationRandomId(final String locationRandomId) {
        this.locationRandomId = locationRandomId;
    }
    
    public final String getPosRandomId() {
        return this.posRandomId;
    }
    
    public final void setPosRandomId(final String posRandomId) {
        this.posRandomId = posRandomId;
    }
    
    public final int getPayStationSeverity() {
        return this.payStationSeverity;
    }
    
    public final void setPayStationSeverity(final int payStationSeverity) {
        this.payStationSeverity = payStationSeverity;
    }
    
    public final int getLastSeenSeverity() {
        return this.lastSeenSeverity;
    }
    
    public final void setLastSeenSeverity(final int lastSeenSeverity) {
        this.lastSeenSeverity = lastSeenSeverity;
    }
    
    public final String getRouteRandomId() {
        return this.routeRandomId;
    }
    
    public final void setRouteRandomId(final String routeRandomId) {
        this.routeRandomId = routeRandomId;
    }
}
