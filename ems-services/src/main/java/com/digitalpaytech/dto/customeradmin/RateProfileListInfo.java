package com.digitalpaytech.dto.customeradmin;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias(value = "RateListInfo")
public class RateProfileListInfo {
    
    private String randomRateId;
    private String name;
    private Integer permitIssueTypeId;
    private String permitIssueTypeName;
    private Boolean isPublished;
    
    public RateProfileListInfo() {
    }
    
    public final String getRandomRateId() {
        return this.randomRateId;
    }
    
    public final void setRandomRateId(final String randomRateId) {
        this.randomRateId = randomRateId;
    }
    
    public final String getName() {
        return this.name;
    }
    
    public final void setName(final String name) {
        this.name = name;
    }
    
    public final Integer getPermitIssueTypeId() {
        return this.permitIssueTypeId;
    }
    
    public final void setPermitIssueTypeId(final Integer permitIssueTypeId) {
        this.permitIssueTypeId = permitIssueTypeId;
    }
    
    public final String getPermitIssueTypeName() {
        return this.permitIssueTypeName;
    }
    
    public final void setPermitIssueTypeName(final String permitIssueTypeName) {
        this.permitIssueTypeName = permitIssueTypeName;
    }
    
    public final Boolean getIsPublished() {
        return this.isPublished;
    }
    
    public final void setIsPublished(final Boolean isPublished) {
        this.isPublished = isPublished;
    }

}
