package com.digitalpaytech.dto.customeradmin;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;

import com.digitalpaytech.domain.ActivityLog;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

public class ActivityLogEntry {
	
	private String userName;
	private String activityMessage;
	private String relativeDateTime;

	@XStreamOmitField
	private String logType;
	@XStreamOmitField
	private String timeZone;
	@XStreamOmitField
	private Date dateTime;
	
	public ActivityLogEntry(){		
	}

	public ActivityLogEntry(ActivityLog activityLog, String timeZone, String activityMessage) throws UnsupportedEncodingException{
		
		this.userName = URLDecoder.decode(activityLog.getUserAccount().getRealUserName(), WebSecurityConstants.URL_ENCODING_LATIN1);
		this.logType = activityLog.getActivityType().getName();
		this.dateTime = activityLog.getActivityGmt();
		this.timeZone = timeZone;		
		this.activityMessage = URLDecoder.decode(activityMessage, WebSecurityConstants.URL_ENCODING_LATIN1);
		this.relativeDateTime = DateUtil.getRelativeTimeString(activityLog.getActivityGmt(), timeZone);
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLogType() {
		return logType;
	}
	public void setLogType(String logType) {
		this.logType = logType;
	}

	public Date getDateTime() {
		return dateTime;
	}
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	
	public String getRelativeDateTime() {
		return relativeDateTime;
	}
	public void setRelativeDateTime(String relativeDateTime) {
		this.relativeDateTime = relativeDateTime;
	}
	
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getActivityMessage() {
		return activityMessage;
	}

	public void setActivityMessage(String activityMessage) {
		this.activityMessage = activityMessage;
	}		
}
