package com.digitalpaytech.dto.customeradmin;

import java.io.Serializable;
import java.util.Date;

import com.digitalpaytech.util.support.RelativeDateTime;

public class DeviceHistoryInfo implements Serializable {

    private static final long serialVersionUID = 1879543303450255563L;
    private String deviceUid;
    private String userName;
    private String firstName;
    private String lastName;
    private RelativeDateTime time;
    //private String time;
    private String result;
    public String getFirstName(){
        return firstName;
    }
    public void setFirstName(String firstName){
        this.firstName = firstName;
    }
    public String getLastName(){
        return lastName;
    }
    public void setLastName(String lastName){
        this.lastName = lastName;
    }
    public String getDeviceUid(){
        return deviceUid;
    }
    public void setDeviceUid(String deviceUid){
        this.deviceUid = deviceUid;
    }
    public String getUserName(){
        return this.userName;
    }
    public void setUserName(String userName){
        this.userName = userName;
    }
    public RelativeDateTime getTime(){
        return time;
    }
    public void setTime(RelativeDateTime time){
        this.time = time;
    }
    public String getResult(){
        return result;
    }
    public void setResult(String result){
        this.result = result;
    }
}
