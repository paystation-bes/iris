package com.digitalpaytech.dto.customeradmin;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

public class TransactionDetail implements Serializable {
	private static final long serialVersionUID = 2171006622363918624L;
	
	private String purchaseRandomId;
    private String paystationName;
    private String locationName;
    private String licencePlateNumber;
    private Integer spaceNumber;
    private Integer ticketNumber;
    private String purchaseDate;
    private String permitExpiryDate;
    private String paymentType;
    private String cardType;
    private String creditCardType;
    private String last4DigitsOfCardNumber;
    private String mobileNumber;    
    private String cardNumber;

	private String customCardType;
    private Boolean isCouponOffline;
    private String couponNumber;
    private String emailAddress;
    private Boolean isActive;
    private Boolean isCreditCard;
    private Boolean isPasscard;
    private String authorizationNumber;
    private Float amount;
    private Boolean isCancelled;
    private Boolean isTest;
    
    // T2 Flex <PERMITNUMBER>, <FACILITYNAME>, <ISDESTROYED>, <ISLOST>, <ISCUSTODY>, <ISRETURNED>, <ISTERMINATED>
    private boolean isFlex;
    private String permitNumber;
    private String facilityName;
    private boolean isDestroyed;
    private boolean isLost;
    private boolean isCustody;
    private boolean isReturned;
    private boolean isTerminated;
    
    private String accountName;
    
    @XStreamOmitField
    private Date purchaseDateGmt;
    @XStreamOmitField
    private Date permitExpiryDateGmt;
    
    @XStreamOmitField
    private Date expiryDate;
    @XStreamOmitField
    private Long permitId;
    
    public String getAuthorizationNumber() {
        return authorizationNumber;
    }
    
    public void setAuthorizationNumber(String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }
    
    public Float getAmount() {
        return amount;
    }
    
    public void setAmount(Float amount) {
        this.amount = new BigDecimal(amount).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP).floatValue();
    }
    
    public Boolean getIsActive() {
        return isActive;
    }
    
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }
    
    public Boolean getIsCreditCard() {
        return isCreditCard;
    }
    
    public void setIsCreditCard(Boolean isCreditCard) {
        this.isCreditCard = isCreditCard;
    }    
    
    public String getPurchaseRandomId() {
        return purchaseRandomId;
    }
    
    public void setPurchaseRandomId(String purchaseRandomId) {
        this.purchaseRandomId = purchaseRandomId;
    }
    
    public String getPaystationName() {
        return paystationName;
    }
    
    public void setPaystationName(String paystationName) {
        this.paystationName = paystationName;
    }
    
    public String getLocationName() {
        return locationName;
    }
    
    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }
    
    public Integer getTicketNumber() {
        return ticketNumber;
    }
    
    public void setTicketNumber(Integer ticketNumber) {
        this.ticketNumber = ticketNumber;
    }
    
    public Integer getSpaceNumber() {
        return spaceNumber;
    }
    
    public void setSpaceNumber(Integer spaceNumber) {
        this.spaceNumber = spaceNumber;
    }
    
    public String getPurchaseDate() {
        return purchaseDate;
    }
    
    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }
    
    public String getPermitExpiryDate() {
        return permitExpiryDate;
    }
    
    public void setPermitExpiryDate(String permitExpiryDate) {
        this.permitExpiryDate = permitExpiryDate;
    }
    
    public String getPaymentType() {
        return paymentType;
    }
    
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }
    
    public String getCardType() {
        return cardType;
    }
    
    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
    
    public String getLast4DigitsOfCardNumber() {
        return last4DigitsOfCardNumber;
    }
    
    public void setLast4DigitsOfCardNumber(String last4DigitsOfCardNumber) {
        this.last4DigitsOfCardNumber = last4DigitsOfCardNumber;
    }
    
    public String getEmailAddress() {
        return emailAddress;
    }
    
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    
    public String getMobileNumber() {
        return mobileNumber;
    }
    
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
    
    public Boolean isIsCouponOffline() {
    	return isCouponOffline;
    }
    
    public void setIsCouponOffline(Boolean isCouponOffline) {
    	this.isCouponOffline = isCouponOffline;
    }
    
    public String getCouponNumber() {
        return couponNumber;
    }
    
    public void setCouponNumber(String couponNumber) {
        this.couponNumber = couponNumber;
    }
    
    public String getCreditCardType() {
        return creditCardType;
    }
    
    public void setCreditCardType(String creditCardType) {
        this.creditCardType = creditCardType;
    }
    
    public String getCustomCardType() {
        return customCardType;
    }
    
    public void setCustomCardType(String customCardType) {
        this.customCardType = customCardType;
    }
    
    public String getLicencePlateNumber() {
        return licencePlateNumber;
    }
    
    public void setLicencePlateNumber(String licencePlateNumber) {
        this.licencePlateNumber = licencePlateNumber;
    }
    
    public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	
    public Boolean getIsCancelled() {
        return isCancelled;
    }
    
    public void setIsCancelled(Boolean isCancelled) {
        this.isCancelled = isCancelled;
    }

    public Boolean getIsTest() {
        return isTest;
    }
    
    public void setIsTest(Boolean isTest) {
        this.isTest = isTest;
    }

	public Boolean getIsPasscard() {
		return isPasscard;
	}

	public void setIsPasscard(Boolean isPasscard) {
		this.isPasscard = isPasscard;
	}
	
	public final Boolean getIsFlex() {
        return this.isFlex;
    }
    
    public final void setIsFlex(final boolean isFlex) {
        this.isFlex = isFlex;
    }
    
    public final void setIsFlex(final Boolean isFlex) {
        this.setIsFlex((isFlex == null) ? false : isFlex.booleanValue());
    }
    
    public final String getPermitNumber() {
        return this.permitNumber;
    }
    
    public final void setPermitNumber(final String permitNumber) {
        this.permitNumber = permitNumber;
    }
    
    public final String getFacilityName() {
        return this.facilityName;
    }
    
    public final void setFacilityName(final String facilityName) {
        this.facilityName = facilityName;
    }
    
    public final Boolean getIsDestroyed() {
        return this.isDestroyed;
    }
    
    public final void setIsDestroyed(final boolean isDestroyed) {
        this.isDestroyed = isDestroyed;
    }
    
    public final void setIsDestroyed(final Boolean isDestroyed) {
        this.setIsDestroyed((isDestroyed == null) ? false : isDestroyed.booleanValue());
    }
    
    public final Boolean getIsLost() {
        return this.isLost;
    }
    
    public final void setIsLost(final boolean isLost) {
        this.isLost = isLost;
    }
    
    public final void setIsLost(final Boolean isLost) {
        this.setIsLost((isLost == null) ? false : isLost.booleanValue());
    }
    
    public final Boolean getIsCustody() {
        return this.isCustody;
    }
    
    public final void setIsCustody(final boolean isCustody) {
        this.isCustody = isCustody;
    }
    
    public final void setIsCustody(final Boolean isCustody) {
        this.setIsCustody((isCustody == null) ? false : isCustody.booleanValue());
    }
    
    public final Boolean getIsReturned() {
        return this.isReturned;
    }
    
    public final void setIsReturned(final boolean isReturned) {
        this.isReturned = isReturned;
    }
    
    public final void setIsReturned(final Boolean isReturned) {
        this.setIsReturned((isReturned == null) ? false : isReturned.booleanValue());
    }
    
    public final Boolean getIsTerminated() {
        return this.isTerminated;
    }
    
    public final void setIsTerminated(final boolean isTerminated) {
        this.isTerminated = isTerminated;
    }
    
    public final void setIsTerminated(final Boolean isTerminated) {
        this.setIsTerminated((isTerminated == null) ? false : isTerminated.booleanValue());
    }
    
    public final String getAccountName() {
        return this.accountName;
    }

    public final void setAccountName(final String accountName) {
        this.accountName = accountName;
    }

    public final Date getExpiryDate() {
        return this.expiryDate;
    }
    
    public final void setExpiryDate(final Date expiryDate) {
        this.expiryDate = expiryDate;
    }
    
    public final Long getPermitId() {
        return this.permitId;
    }
    
    public final void setPermitId(final Long permitId) {
        this.permitId = permitId;
    }

    public Date getPurchaseDateGmt() {
        return purchaseDateGmt;
    }

    public void setPurchaseDateGmt(Date purchaseDateGmt) {
        this.purchaseDateGmt = purchaseDateGmt;
    }

    public Date getPermitExpiryDateGmt() {
        return permitExpiryDateGmt;
    }

    public void setPermitExpiryDateGmt(Date permitExpiryDateGmt) {
        this.permitExpiryDateGmt = permitExpiryDateGmt;
    }	

}
