package com.digitalpaytech.dto.customeradmin;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias(value = "payStationListTransactionReportList")
public class PayStationListCollectionReportList {

	@XStreamImplicit
	private List<PayStationListCollectionReport> collectionReports;
	
	public PayStationListCollectionReportList(){		
		collectionReports = new ArrayList<PayStationListCollectionReport>();
	}

	public List<PayStationListCollectionReport> getCollectionReports() {
		return collectionReports;
	}

	public void setCollectionReports(List<PayStationListCollectionReport> collectionReports) {
		this.collectionReports = collectionReports;
	}
	
	public void addCollectionReport(PayStationListCollectionReport report){
		this.collectionReports.add(report);
	}
	
}
