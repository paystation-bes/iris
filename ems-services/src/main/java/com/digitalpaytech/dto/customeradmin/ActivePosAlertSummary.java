package com.digitalpaytech.dto.customeradmin;

import java.io.Serializable;

public class ActivePosAlertSummary implements Serializable {
	private static final long serialVersionUID = -3223009238033775910L;
	
	private int highestSeverity;
	private int paperLevel;
	private int alertsCount;
	
	public ActivePosAlertSummary() {
		
	}

	public int getHighestSeverity() {
		return highestSeverity;
	}

	public void setHighestSeverity(int highestSeverity) {
		this.highestSeverity = highestSeverity;
	}

	public int getPaperLevel() {
		return paperLevel;
	}

	public void setPaperLevel(int paperLevel) {
		this.paperLevel = paperLevel;
	}

	public int getAlertsCount() {
		return alertsCount;
	}

	public void setAlertsCount(int alertsCount) {
		this.alertsCount = alertsCount;
	}
}
