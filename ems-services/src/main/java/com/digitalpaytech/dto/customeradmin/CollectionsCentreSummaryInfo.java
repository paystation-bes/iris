package com.digitalpaytech.dto.customeradmin;

import java.math.BigDecimal;

import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.support.RelativeDateTime;

public class CollectionsCentreSummaryInfo {
    private int coinCount;
    private int billCount;
    private int creditCount;
    
    private int coinCountSeverity;
    private int billCountSeverity;
    private int creditCountSeverity;
    
    private BigDecimal coinAmount;
    private BigDecimal billAmount;
    private BigDecimal creditAmount;
    private BigDecimal runningTotal;
    
    private int coinAmountSeverity;
    private int billAmountSeverity;
    private int creditAmountSeverity;
    private int runningTotalSeverity;
    private int overdueCollectionSeverity;
    private int payStationSeverity;
    
    private String posName;
    private String serial;
    private String lastSeen;
    private String locationName;
    private String locationRandomId;
    private String routeName;
    private String routeRandomId;
    
    private String posRandomId;
    
    private RelativeDateTime collectionDate;
    
    public CollectionsCentreSummaryInfo() {
    }
    
    public int getCoinCount() {
        return coinCount;
    }
    
    public void setCoinCount(int coinCount) {
        this.coinCount = coinCount;
    }
    
    public int getBillCount() {
        return billCount;
    }
    
    public void setBillCount(int billCount) {
        this.billCount = billCount;
    }
    
    public Integer getCreditCount() {
        return creditCount;
    }
    
    public void setCreditCount(Integer creditCount) {
        this.creditCount = creditCount;
    }
    
    public BigDecimal getCoinAmount() {
        return coinAmount;
    }
    
    public void setCoinAmount(BigDecimal coinAmount) {
        this.coinAmount = coinAmount;
    }
    
    public BigDecimal getBillAmount() {
        return billAmount;
    }
    
    public void setBillAmount(BigDecimal billAmount) {
        this.billAmount = billAmount;
    }
    
    public BigDecimal getCreditAmount() {
        return creditAmount;
    }
    
    public void setCreditAmount(BigDecimal creditAmount) {
        this.creditAmount = creditAmount;
    }
    
    public BigDecimal getRunningTotal() {
        return runningTotal;
    }
    
    public void setRunningTotal(BigDecimal runningTotal) {
        this.runningTotal = runningTotal;
    }
    
    public String getPosName() {
        return posName;
    }
    
    public void setPosName(String posName) {
        this.posName = posName;
    }
    
    public String getSerial() {
        return serial;
    }
    
    public void setSerial(String serial) {
        this.serial = serial;
    }
    
    public String getLastSeen() {
        return lastSeen;
    }
    
    public void setLastSeen(String lastSeen) {
        this.lastSeen = lastSeen;
    }
    
    public String getLocationName() {
        return locationName;
    }
    
    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }
    
    public String getRouteName() {
        return routeName;
    }
    
    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }
    
    public int getCoinCountSeverity() {
        return coinCountSeverity;
    }
    
    public void setCoinCountSeverity(int coinCountSeverity) {
        this.coinCountSeverity = coinCountSeverity;
    }
    
    public int getBillCountSeverity() {
        return billCountSeverity;
    }
    
    public void setBillCountSeverity(int billCountSeverity) {
        this.billCountSeverity = billCountSeverity;
    }
    
    public int getCreditCountSeverity() {
        return creditCountSeverity;
    }
    
    public void setCreditCountSeverity(int creditCountSeverity) {
        this.creditCountSeverity = creditCountSeverity;
    }
    
    public int getCoinAmountSeverity() {
        return coinAmountSeverity;
    }
    
    public void setCoinAmountSeverity(int coinAmountSeverity) {
        this.coinAmountSeverity = coinAmountSeverity;
    }
    
    public int getBillAmountSeverity() {
        return billAmountSeverity;
    }
    
    public void setBillAmountSeverity(int billAmountSeverity) {
        this.billAmountSeverity = billAmountSeverity;
    }
    
    public int getCreditAmountSeverity() {
        return creditAmountSeverity;
    }
    
    public void setCreditAmountSeverity(int creditAmountSeverity) {
        this.creditAmountSeverity = creditAmountSeverity;
    }
    
    public int getRunningTotalSeverity() {
        return runningTotalSeverity;
    }
    
    public void setRunningTotalSeverity(int runningTotalSeverity) {
        this.runningTotalSeverity = runningTotalSeverity;
    }
    
    public void setRunningTotalDetails(CollectionSummaryEntry collectionSummaryEntry, String timeZoneId) {
        
        this.coinCount = collectionSummaryEntry.getCoinCount();
        this.billCount = collectionSummaryEntry.getBillCount();
        this.creditCount = collectionSummaryEntry.getCardCount();
        
        BigDecimal hundred = new BigDecimal(100);
        
        this.coinAmount = new BigDecimal(collectionSummaryEntry.getCoinAmount()).divide(hundred).setScale(2, BigDecimal.ROUND_HALF_UP);
        this.billAmount = new BigDecimal(collectionSummaryEntry.getBillAmount()).divide(hundred).setScale(2, BigDecimal.ROUND_HALF_UP);
        this.creditAmount = new BigDecimal(collectionSummaryEntry.getCardAmount()).divide(hundred).setScale(2, BigDecimal.ROUND_HALF_UP);
        this.runningTotal = new BigDecimal(collectionSummaryEntry.getTotalAmount()).divide(hundred).setScale(2, BigDecimal.ROUND_HALF_UP);
        
        if (collectionSummaryEntry.getCollectionDate() != null) {
            this.collectionDate = new RelativeDateTime(collectionSummaryEntry.getCollectionDate(), timeZoneId);
        }
    }
    
    //TODO performance can be improved severity is already calculated just pass it to this method no need to calculate again.
    public void setSeverity(final PosEventCurrent posEventCurrent) {
        
        final int currentSeverity = posEventCurrent.getEventSeverityType().getId();
        switch (posEventCurrent.getCustomerAlertType().getAlertThresholdType().getId()) {
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_COUNT:
                this.coinCountSeverity = currentSeverity;
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_DOLLARS:
                this.coinAmountSeverity = currentSeverity;
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_COUNT:
                this.billCountSeverity = currentSeverity;
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_DOLLARS:
                this.billAmountSeverity = currentSeverity;
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_COUNT:
                this.creditCountSeverity = currentSeverity;
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_DOLLARS:
                this.creditAmountSeverity = currentSeverity;
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_RUNNING_TOTAL_DOLLAR:
                this.runningTotalSeverity = currentSeverity;
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_OVERDUE_COLLECTION:
                this.overdueCollectionSeverity = currentSeverity;
                break;
            default:
                break;
        }
        if (currentSeverity > this.payStationSeverity) {
            this.payStationSeverity = currentSeverity;
        }
    }
    
    public String getLocationRandomId() {
        return locationRandomId;
    }
    
    public void setLocationRandomId(String locationRandomId) {
        this.locationRandomId = locationRandomId;
    }
    
    public String getPosRandomId() {
        return posRandomId;
    }
    
    public void setPosRandomId(String posRandomId) {
        this.posRandomId = posRandomId;
    }
    
    public int getPayStationSeverity() {
        return payStationSeverity;
    }
    
    public void setPayStationSeverity(int payStationSeverity) {
        this.payStationSeverity = payStationSeverity;
    }
    
    public String getRouteRandomId() {
        return routeRandomId;
    }
    
    public void setRouteRandomId(String routeRandomId) {
        this.routeRandomId = routeRandomId;
    }
    
    public int getOverdueCollectionSeverity() {
        return overdueCollectionSeverity;
    }
    
    public void setOverdueCollectionSeverity(int overdueCollectionSeverity) {
        this.overdueCollectionSeverity = overdueCollectionSeverity;
    }
    
    public RelativeDateTime getCollectionDate() {
        return collectionDate;
    }
    
    public void setCollectionDate(RelativeDateTime collectionDate) {
        this.collectionDate = collectionDate;
    }
    
}
