package com.digitalpaytech.dto.customeradmin;

public class PointOfSaleValuePair {

	private String randomizedPointOfSaleId;
	private String pointOfSaleName;
	
	public PointOfSaleValuePair() {
		
	}
	
	public PointOfSaleValuePair(String randomizedPointOfSaleId, String pointOfSaleName) {
		this.randomizedPointOfSaleId = randomizedPointOfSaleId;
		this.pointOfSaleName = pointOfSaleName;
	}

	public String getRandomizedPointOfSaleId() {
		return randomizedPointOfSaleId;
	}

	public void setRandomizedPointOfSaleId(String randomizedPointOfSaleId) {
		this.randomizedPointOfSaleId = randomizedPointOfSaleId;
	}

	public String getPointOfSaleName() {
		return pointOfSaleName;
	}

	public void setPointOfSaleName(String pointOfSaleName) {
		this.pointOfSaleName = pointOfSaleName;
	}
}
