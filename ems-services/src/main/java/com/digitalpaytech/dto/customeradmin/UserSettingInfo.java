package com.digitalpaytech.dto.customeradmin;

public class UserSettingInfo {
    
    private String randomId;
    private String firstName;
    private String lastName;
    private boolean isActive;
    private boolean isReadOnly;
    private boolean isAlias;
    private String customerName;
    
    public UserSettingInfo(final String firstName, final String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    
    public UserSettingInfo(final String randomId, final String firstName, final String lastName) {
        this.randomId = randomId;
        this.firstName = firstName;
        this.lastName = lastName;
    }
    
    public UserSettingInfo(final String randomId, final String firstName, final String lastName, final boolean isActive, final boolean isReadOnly,
            final boolean isAlias) {
        this.randomId = randomId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.isActive = isActive;
        this.isReadOnly = isReadOnly;
        this.isAlias = isAlias;
    }
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final String getFirstName() {
        return this.firstName;
    }
    
    public final void setFirstName(final String firstName) {
        this.firstName = firstName;
    }
    
    public final String getLastName() {
        return this.lastName;
    }
    
    public final void setLastName(final String lastName) {
        this.lastName = lastName;
    }
    
    public final boolean getIsActive() {
        return this.isActive;
    }
    
    public final void setIsActive(final boolean isActive) {
        this.isActive = isActive;
    }
    
    public final boolean getIsReadOnly() {
        return this.isReadOnly;
    }
    
    public final void setIsReadOnly(final boolean isReadOnly) {
        this.isReadOnly = isReadOnly;
    }
    
    public final boolean getIsAlias() {
        return this.isAlias;
    }
    
    public final void setIsAlias(final boolean isAlias) {
        this.isAlias = isAlias;
    }
    
    public final String getCustomerName() {
        return this.customerName;
    }
    
    public final void setCustomerName(final String customerName) {
        this.customerName = customerName;
    }
}
