package com.digitalpaytech.dto.customeradmin;

import java.util.List;

public class TaxReceiptInfo {
    
    private List<TaxDetail> taxDetails;
    private short taxTotal;
    private boolean totalParking;
    
    public TaxReceiptInfo(final List<TaxDetail> taxDetails, final short taxTotal, final boolean totalParking) {
        super();
        this.taxDetails = taxDetails;
        this.taxTotal = taxTotal;
        this.totalParking = totalParking;
    }
    
    public List<TaxDetail> getTaxDetails() {
        return this.taxDetails;
    }
    
    public void setTaxDetails(final List<TaxDetail> taxDetails) {
        this.taxDetails = taxDetails;
    }
    
    public short getTaxTotal() {
        return this.taxTotal;
    }
    
    public void setTaxTotal(final short taxTotal) {
        this.taxTotal = taxTotal;
    }
    
    public boolean getTotalParking() {
        return this.totalParking;
    }
    
    public void setTotalParking(final boolean totalParking) {
        this.totalParking = totalParking;
    }
    
}
