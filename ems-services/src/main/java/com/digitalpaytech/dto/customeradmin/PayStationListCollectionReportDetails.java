package com.digitalpaytech.dto.customeradmin;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias(value = "payStationListCollectionReportDetails")
public class PayStationListCollectionReportDetails {

    private String locationName;
    private String payStationName;
    private String payStationSerial;
    private Integer typeId;
    private String type;
    private int reportNumber;
    private String collectionDate;
    private String previousCollection;
    private int permitCount;
    private String startDateLocal;
    private String endDateLocal;
    private String payStationSettingName;
    private int endTicketNumber;
    private int ticketSold;

    private Integer tube1type;
    private Integer tube2type;
    private Integer tube3type;
    private Integer tube4type;
    private Float tube1amount;
    private Float tube2amount;
    private Float tube3amount;
    private Float tube4amount;
    private Float tubeTotalAmount;
    private Float replenishedAmount;
    private Float overfillAmount;
    private Float acceptedFloatAmount;
    private Float changerDispensed;
    private Float testDispensedChanger;
    private Float hopper1type;
    private Float hopper2type;
    private Float hopper1current;
    private Float hopper2current;
    private Float hopperTotalAmount;
    private Float hopper1dispensed;
    private Float hopper2dispensed;
    private Float hopper1replenished;
    private Float hopper2replenished;
    private Float testDispensedHopper1;
    private Float testDispensedHopper2;

    private Float coinTotalAmount;
    private Float billTotalAmount;
    private Float cardTotalAmount;

    private Float coinAmount5;
    private Float coinAmount10;
    private Float coinAmount25;
    private Float coinAmount100;
    private Float coinAmount200;

    private Float billAmount1;
    private Float billAmount2;
    private Float billAmount5;
    private Float billAmount10;
    private Float billAmount20;
    private Float billAmount50;

    private Float amexAmount;
    private Float dinersAmount;
    private Float discoverAmount;
    private Float masterCardAmount;
    private Float visaAmount;
    private Float jcbAmount;
    private Float smartCardAmount;
    private Float smartCardRechargeAmount;
    private Float valueCardAmount;
    private Float cardAmountTotal;

    private Float excessPaymentAmount;
    private Float totalCollections;
    private Float changeIssuedAmount;
    private Float refundIssuedAmount;
    private Float revenue;

    private String collectionUserName;

    public PayStationListCollectionReportDetails(){		
    }

    public PayStationListCollectionReportDetails(String locationName, String payStationName, String payStationSerial, Integer typeId, String type,
            int reportNumber, String collectionDate, String previousCollection, int permitCount, Float coinTotalAmount, Float billTotalAmount,
            Float cardTotalAmount, Float coinAmount5, Float coinAmount10, Float coinAmount25, Float coinAmount100, Float coinAmount200,
            Float billAmount1, Float billAmount2, Float billAmount5, Float billAmount10, Float billAmount20, Float billAmount50, Float amexAmount,
            Float dinersAmount, Float discoverAmount, Float masterCardAmount, Float visaAmount, Float jcbAmount, Float smartCardAmount, Float smartCardRechargeAmount, 
            Float valueCardAmount, String startDateLocal, String endDateLocal, String payStationSettingName, int endTicketNumber, int ticketSold,
            Integer tube1type, Float tube1amount, Integer tube2type, Float tube2amount, Integer tube3type, Float tube3amount, Integer tube4type, Float tube4amount,
            Float tubeTotalAmount, Float replenishedAmount, Float overfillAmount, Float acceptedFloatAmount, Float changerDispensed, Float testDispensedChanger,
            Float hopper1type, Float hopper2type, Float hopper1current, Float hopper2current, Float hopperTotalAmount, Float hopper1dispensed, Float hopper2dispensed,
            Float hopper1replenished, Float hopper2replenished, Float testDispensedHopper1, Float testDispensedHopper2, Float cardAmountTotal,
            Float excessPaymentAmount, Float totalCollections, Float changeIssuedAmount, Float refundIssuedAmount, Float revenue) {
        super();
        this.locationName = locationName;
        this.payStationName = payStationName;
        this.payStationSerial = payStationSerial;
        this.typeId = typeId;
        this.type = type;
        this.reportNumber = reportNumber;
        this.collectionDate = collectionDate;
        this.previousCollection = previousCollection;
        this.permitCount = permitCount;
        this.coinTotalAmount = coinTotalAmount;
        this.billTotalAmount = billTotalAmount;
        this.cardTotalAmount = cardTotalAmount;
        this.coinAmount5 = coinAmount5;
        this.coinAmount10 = coinAmount10;
        this.coinAmount25 = coinAmount25;
        this.coinAmount100 = coinAmount100;
        this.coinAmount200 = coinAmount200;
        this.billAmount1 = billAmount1;
        this.billAmount2 = billAmount2;
        this.billAmount5 = billAmount5;
        this.billAmount10 = billAmount10;
        this.billAmount20 = billAmount20;
        this.billAmount50 = billAmount50;
        this.amexAmount = amexAmount;
        this.dinersAmount = dinersAmount;
        this.discoverAmount = discoverAmount;
        this.masterCardAmount = masterCardAmount;
        this.visaAmount = visaAmount;
        this.jcbAmount = jcbAmount;
        this.smartCardAmount = smartCardAmount;
        this.smartCardRechargeAmount = smartCardRechargeAmount;
        this.valueCardAmount = valueCardAmount;
        this.startDateLocal = startDateLocal;
        this.endDateLocal = endDateLocal;
        this.payStationSettingName = payStationSettingName;
        this.endTicketNumber = endTicketNumber;
        this.ticketSold = ticketSold;
        this.tube1type = tube1type;
        this.tube1amount = tube1amount;
        this.tube2type = tube2type;
        this.tube2amount = tube2amount;
        this.tube3type = tube3type;
        this.tube3amount = tube3amount;
        this.tube4type = tube4type;
        this.tube4amount = tube4amount;
        this.tubeTotalAmount = tubeTotalAmount;
        this.replenishedAmount = replenishedAmount;
        this.overfillAmount = overfillAmount;
        this.acceptedFloatAmount = acceptedFloatAmount;
        this.changerDispensed = changerDispensed;
        this.testDispensedChanger = testDispensedChanger;
        this.hopper1type = hopper1type;
        this.hopper2type = hopper2type;
        this.hopper1current = hopper1current;
        this.hopper2current = hopper2current;
        this.hopperTotalAmount = hopperTotalAmount;
        this.hopper1dispensed = hopper1dispensed;
        this.hopper2dispensed = hopper2dispensed;
        this.hopper1replenished = hopper1replenished;
        this.hopper2replenished = hopper2replenished;
        this.testDispensedHopper1 = testDispensedHopper1;
        this.testDispensedHopper2 = testDispensedHopper2;
        this.cardAmountTotal = cardAmountTotal;
        this.excessPaymentAmount = excessPaymentAmount;
        this.totalCollections = totalCollections;
        this.changeIssuedAmount = changeIssuedAmount;
        this.refundIssuedAmount = refundIssuedAmount;
        this.revenue = revenue;
        
    }

    public final String getLocationName() {
        return this.locationName;
    }
    
    public final void setLocationName(final String locationName) {
        this.locationName = locationName;
    }
    
    public final String getPayStationName() {
        return this.payStationName;
    }
    
    public final void setPayStationName(final String payStationName) {
        this.payStationName = payStationName;
    }
    
    public final String getPayStationSerial() {
        return this.payStationSerial;
    }
    
    public final void setPayStationSerial(final String payStationSerial) {
        this.payStationSerial = payStationSerial;
    }
    
    public final Integer getTypeId() {
        return this.typeId;
    }
    
    public final void setTypeId(final Integer typeId) {
        this.typeId = typeId;
    }
    
    public final String getType() {
        return this.type;
    }
    
    public final void setType(final String type) {
        this.type = type;
    }
    
    public final int getReportNumber() {
        return this.reportNumber;
    }
    
    public final void setReportNumber(final int reportNumber) {
        this.reportNumber = reportNumber;
    }
    
    public final String getCollectionDate() {
        return this.collectionDate;
    }
    
    public final void setCollectionDate(final String collectionDate) {
        this.collectionDate = collectionDate;
    }
    
    public final String getPreviousCollection() {
        return this.previousCollection;
    }
    
    public final void setPreviousCollection(final String previousCollection) {
        this.previousCollection = previousCollection;
    }
    
    public final int getPermitCount() {
        return this.permitCount;
    }
    
    public final void setPermitCount(final int permitCount) {
        this.permitCount = permitCount;
    }
    
    public final Float getCoinTotalAmount() {
        return this.coinTotalAmount;
    }
    
    public final void setCoinTotalAmount(final Float coinTotalAmount) {
        this.coinTotalAmount = coinTotalAmount;
    }
    
    public final Float getBillTotalAmount() {
        return this.billTotalAmount;
    }
    
    public final void setBillTotalAmount(final Float billTotalAmount) {
        this.billTotalAmount = billTotalAmount;
    }
    
    public final Float getCardTotalAmount() {
        return this.cardTotalAmount;
    }
    
    public final void setCardTotalAmount(final Float cardTotalAmount) {
        this.cardTotalAmount = cardTotalAmount;
    }
    
    public final Float getCoinAmount5() {
        return this.coinAmount5;
    }
    
    public final void setCoinAmount5(final Float coinAmount5) {
        this.coinAmount5 = coinAmount5;
    }
    
    public final Float getCoinAmount10() {
        return this.coinAmount10;
    }
    
    public final void setCoinAmount10(final Float coinAmount10) {
        this.coinAmount10 = coinAmount10;
    }
    
    public final Float getCoinAmount25() {
        return this.coinAmount25;
    }
    
    public final void setCoinAmount25(final Float coinAmount25) {
        this.coinAmount25 = coinAmount25;
    }
    
    public final Float getCoinAmount100() {
        return this.coinAmount100;
    }
    
    public final void setCoinAmount100(final Float coinAmount100) {
        this.coinAmount100 = coinAmount100;
    }
    
    public final Float getCoinAmount200() {
        return this.coinAmount200;
    }
    
    public final void setCoinAmount200(final Float coinAmount200) {
        this.coinAmount200 = coinAmount200;
    }
    
    public final Float getBillAmount1() {
        return this.billAmount1;
    }
    
    public final void setBillAmount1(final Float billAmount1) {
        this.billAmount1 = billAmount1;
    }
    
    public final Float getBillAmount2() {
        return this.billAmount2;
    }
    
    public final void setBillAmount2(final Float billAmount2) {
        this.billAmount2 = billAmount2;
    }
    
    public final Float getBillAmount5() {
        return this.billAmount5;
    }
    
    public final void setBillAmount5(final Float billAmount5) {
        this.billAmount5 = billAmount5;
    }
    
    public final Float getBillAmount10() {
        return this.billAmount10;
    }
    
    public final void setBillAmount10(final Float billAmount10) {
        this.billAmount10 = billAmount10;
    }
    
    public final Float getBillAmount20() {
        return this.billAmount20;
    }
    
    public final void setBillAmount20(final Float billAmount20) {
        this.billAmount20 = billAmount20;
    }
    
    public final Float getBillAmount50() {
        return this.billAmount50;
    }
    
    public final void setBillAmount50(final Float billAmount50) {
        this.billAmount50 = billAmount50;
    }
    
    public final Float getAmexAmount() {
        return this.amexAmount;
    }
    
    public final void setAmexAmount(final Float amexAmount) {
        this.amexAmount = amexAmount;
    }
    
    public final Float getDinersAmount() {
        return this.dinersAmount;
    }
    
    public final void setDinersAmount(final Float dinersAmount) {
        this.dinersAmount = dinersAmount;
    }
    
    public final Float getDiscoverAmount() {
        return this.discoverAmount;
    }
    
    public final void setDiscoverAmount(final Float discoverAmount) {
        this.discoverAmount = discoverAmount;
    }
    
    public final Float getMasterCardAmount() {
        return this.masterCardAmount;
    }
    
    public final void setMasterCardAmount(final Float masterCardAmount) {
        this.masterCardAmount = masterCardAmount;
    }
    
    public final Float getVisaAmount() {
        return this.visaAmount;
    }
    
    public final void setVisaAmount(final Float visaAmount) {
        this.visaAmount = visaAmount;
    }
    
    public final Float getJcbAmount() {
        return this.jcbAmount;
    }
    
    public final void setJcbAmount(final Float jcbAmount) {
        this.jcbAmount = jcbAmount;
    }
    
    public final Float getSmartCardAmount() {
        return this.smartCardAmount;
    }
    
    public final void setSmartCardAmount(final Float smartCardAmount) {
        this.smartCardAmount = smartCardAmount;
    }
    
    public final Float getSmartCardRechargeAmount() {
        return this.smartCardRechargeAmount;
    }
    
    public final void setSmartCardRechargeAmount(final Float smartCardRechargeAmount) {
        this.smartCardRechargeAmount = smartCardRechargeAmount;
    }
    
    public final Float getValueCardAmount() {
        return this.valueCardAmount;
    }
    
    public final void setValueCardAmount(final Float valueCardAmount) {
        this.valueCardAmount = valueCardAmount;
    }
    
    public final String getStartDateLocal() {
        return this.startDateLocal;
    }
    
    public final void setStartDateLocal(final String startDateLocal) {
        this.startDateLocal = startDateLocal;
    }
    
    public final String getEndDateLocal() {
        return this.endDateLocal;
    }
    
    public final void setEndDateLocal(final String endDateLocal) {
        this.endDateLocal = endDateLocal;
    }
    
    public final String getPayStationSettingName() {
        return this.payStationSettingName;
    }
    
    public final void setPayStationSettingName(final String payStationSettingName) {
        this.payStationSettingName = payStationSettingName;
    }
    
    public final int getEndTicketNumber() {
        return this.endTicketNumber;
    }
    
    public final void setEndTicketNumber(final int endTicketNumber) {
        this.endTicketNumber = endTicketNumber;
    }
    
    public final int getTicketSold() {
        return this.ticketSold;
    }
    
    public final void setTicketSold(final int ticketSold) {
        this.ticketSold = ticketSold;
    }
    
    public final Integer getTube1type() {
        return this.tube1type;
    }
    
    public final void setTube1type(final Integer tube1type) {
        this.tube1type = tube1type;
    }
    
    public final Float getTube1amount() {
        return this.tube1amount;
    }
    
    public final void setTube1amount(final Float tube1amount) {
        this.tube1amount = tube1amount;
    }
    
    public final Integer getTube2type() {
        return this.tube2type;
    }
    
    public final void setTube2type(final Integer tube2type) {
        this.tube2type = tube2type;
    }
    
    public final Float getTube2amount() {
        return this.tube2amount;
    }
    
    public final void setTube2amount(final Float tube2amount) {
        this.tube2amount = tube2amount;
    }
    
    public final Integer getTube3type() {
        return this.tube3type;
    }
    
    public final void setTube3type(final Integer tube3type) {
        this.tube3type = tube3type;
    }
    
    public final Float getTube3amount() {
        return this.tube3amount;
    }
    
    public final void setTube3amount(final Float tube3amount) {
        this.tube3amount = tube3amount;
    }
    
    public final Integer getTube4type() {
        return this.tube4type;
    }
    
    public final void setTube4type(final Integer tube4type) {
        this.tube4type = tube4type;
    }
    
    public final Float getTube4amount() {
        return this.tube4amount;
    }
    
    public final void setTube4amount(final Float tube4amount) {
        this.tube4amount = tube4amount;
    }
    
    public final Float getTubeTotalAmount() {
        return this.tubeTotalAmount;
    }
    
    public final void setTubeTotalAmount(final Float tubeTotalAmount) {
        this.tubeTotalAmount = tubeTotalAmount;
    }
    
    public final Float getReplenishedAmount() {
        return this.replenishedAmount;
    }
    
    public final void setReplenishedAmount(final Float replenishedAmount) {
        this.replenishedAmount = replenishedAmount;
    }
    
    public final Float getOverfillAmount() {
        return this.overfillAmount;
    }
    
    public final void setOverfillAmount(final Float overfillAmount) {
        this.overfillAmount = overfillAmount;
    }
    
    public final Float getAcceptedFloatAmount() {
        return this.acceptedFloatAmount;
    }
    
    public final void setAcceptedFloatAmount(final Float acceptedFloatAmount) {
        this.acceptedFloatAmount = acceptedFloatAmount;
    }
    
    public final Float getChangerDispensed() {
        return this.changerDispensed;
    }
    
    public final void setChangerDispensed(final Float changerDispensed) {
        this.changerDispensed = changerDispensed;
    }
    
    public final Float getTestDispensedChanger() {
        return this.testDispensedChanger;
    }
    
    public final void setTestDispensedChanger(final Float testDispensedChanger) {
        this.testDispensedChanger = testDispensedChanger;
    }
    
    public final Float getHopper1type() {
        return this.hopper1type;
    }
    
    public final void setHopper1type(final Float hopper1type) {
        this.hopper1type = hopper1type;
    }
    
    public final Float getHopper2type() {
        return this.hopper2type;
    }
    
    public final void setHopper2type(final Float hopper2type) {
        this.hopper2type = hopper2type;
    }
    
    public final Float getHopper1current() {
        return this.hopper1current;
    }
    
    public final void setHopper1current(final Float hopper1current) {
        this.hopper1current = hopper1current;
    }
    
    public final Float getHopper2current() {
        return this.hopper2current;
    }
    
    public final void setHopper2current(final Float hopper2current) {
        this.hopper2current = hopper2current;
    }
    
    public final Float getHopperTotalAmount() {
        return this.hopperTotalAmount;
    }
    
    public final void setHopperTotalAmount(final Float hopperTotalAmount) {
        this.hopperTotalAmount = hopperTotalAmount;
    }
    
    public final Float getHopper1dispensed() {
        return this.hopper1dispensed;
    }
    
    public final void setHopper1dispensed(final Float hopper1dispensed) {
        this.hopper1dispensed = hopper1dispensed;
    }
    
    public final Float getHopper2dispensed() {
        return this.hopper2dispensed;
    }
    
    public final void setHopper2dispensed(final Float hopper2dispensed) {
        this.hopper2dispensed = hopper2dispensed;
    }
    
    public final Float getHopper1replenished() {
        return this.hopper1replenished;
    }
    
    public final void setHopper1replenished(final Float hopper1replenished) {
        this.hopper1replenished = hopper1replenished;
    }
    
    public final Float getHopper2replenished() {
        return this.hopper2replenished;
    }
    
    public final void setHopper2replenished(final Float hopper2replenished) {
        this.hopper2replenished = hopper2replenished;
    }
    
    public final Float getTestDispensedHopper1() {
        return this.testDispensedHopper1;
    }
    
    public final void setTestDispensedHopper1(final Float testDispensedHopper1) {
        this.testDispensedHopper1 = testDispensedHopper1;
    }
    
    public final Float getTestDispensedHopper2() {
        return this.testDispensedHopper2;
    }
    
    public final void setTestDispensedHopper2(final Float testDispensedHopper2) {
        this.testDispensedHopper2 = testDispensedHopper2;
    }
    
    public final Float getCardAmountTotal() {
        return this.cardAmountTotal;
    }
    
    public final void setCardAmountTotal(final Float cardAmountTotal) {
        this.cardAmountTotal = cardAmountTotal;
    }
    
    public final Float getExcessPaymentAmount() {
        return this.excessPaymentAmount;
    }
    
    public final void setExcessPaymentAmount(final Float excessPaymentAmount) {
        this.excessPaymentAmount = excessPaymentAmount;
    }
    
    public final Float getTotalCollections() {
        return this.totalCollections;
    }
    
    public final void setTotalCollections(final Float totalCollections) {
        this.totalCollections = totalCollections;
    }
    
    public final Float getChangeIssuedAmount() {
        return this.changeIssuedAmount;
    }
    
    public final void setChangeIssuedAmount(final Float changeIssuedAmount) {
        this.changeIssuedAmount = changeIssuedAmount;
    }
    
    public final Float getRefundIssuedAmount() {
        return this.refundIssuedAmount;
    }
    
    public final void setRefundIssuedAmount(final Float refundIssuedAmount) {
        this.refundIssuedAmount = refundIssuedAmount;
    }
    
    public final Float getRevenue() {
        return this.revenue;
    }
    
    public final void setRevenue(final Float revenue) {
        this.revenue = revenue;
    }
    
    public final String getCollectionUserName() {
        return this.collectionUserName;
    }
    
    public final void setCollectionUserName(final String collectionUserName) {
        this.collectionUserName = collectionUserName;
    }
    
}
