package com.digitalpaytech.dto.customeradmin;

import java.io.Serializable;
import java.util.Date;

public class ConsumerSearchCriteria implements Serializable {
	private static final long serialVersionUID = 6246104097962497690L;

	private Integer customerId;
	
	private String firstName;
	private String lastName;
	private String fullName; // First Name concat Last Name
	private String emailAddress;
	
	private Integer page;
	private Integer itemsPerPage;
	private String[] orderColumn;
	private boolean orderDesc = false;
	
	private Date maxUpdatedTime;
	
	private Integer endConsumerId;
	
	public ConsumerSearchCriteria() {
		
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getItemsPerPage() {
		return itemsPerPage;
	}

	public void setItemsPerPage(Integer itemsPerPage) {
		this.itemsPerPage = itemsPerPage;
	}

	public String[] getOrderColumn() {
		return orderColumn;
	}

	public void setOrderColumn(String[] orderColumn) {
		this.orderColumn = orderColumn;
	}

	public boolean isOrderDesc() {
		return orderDesc;
	}

	public void setOrderDesc(boolean orderDesc) {
		this.orderDesc = orderDesc;
	}

	public Date getMaxUpdatedTime() {
		return maxUpdatedTime;
	}

	public void setMaxUpdatedTime(Date maxUpdatedTime) {
		this.maxUpdatedTime = maxUpdatedTime;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	public Integer getEndConsumerId() {
		return endConsumerId;
	}

	public void setEndConsumerId(Integer endConsumerId) {
		this.endConsumerId = endConsumerId;
	}
}
