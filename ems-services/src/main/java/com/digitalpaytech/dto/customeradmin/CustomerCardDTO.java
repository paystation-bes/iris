package com.digitalpaytech.dto.customeradmin;

import java.io.Serializable;

public class CustomerCardDTO implements Serializable {
    private static final long serialVersionUID = -5886179976300545044L;
    
    private String randomId;
    private String cardNumber;
    
    private String cardType;
    private String cardTypeRandomId;
    
    private String startValidDate;
    private String expireDate;
    
    private Boolean restricted;
    private Short gracePeriodMinutes;
    
    private String maxNumOfUses;
    private String comment;
    
    private String location;
    private String locationRandomId;
    
    private String remainingNumOfUses;
    
    private String consumerName;
	private String consumerRandomId;
	
    public CustomerCardDTO() {
        
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getStartValidDate() {
        return startValidDate;
    }

    public void setStartValidDate(String startValidDate) {
        this.startValidDate = startValidDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public Boolean getRestricted() {
        return restricted;
    }

    public void setRestricted(Boolean restricted) {
        this.restricted = restricted;
    }

    public Short getGracePeriodMinutes() {
        return gracePeriodMinutes;
    }

    public void setGracePeriodMinutes(Short gracePeriodMinutes) {
        this.gracePeriodMinutes = gracePeriodMinutes;
    }

	public String getRandomId() {
		return randomId;
	}

	public void setRandomId(String randomId) {
		this.randomId = randomId;
	}

	public String getCardTypeRandomId() {
		return cardTypeRandomId;
	}

	public void setCardTypeRandomId(String cardTypeRandomId) {
		this.cardTypeRandomId = cardTypeRandomId;
	}

	public String getLocationRandomId() {
		return locationRandomId;
	}

	public void setLocationRandomId(String locationRandomId) {
		this.locationRandomId = locationRandomId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getMaxNumOfUses() {
		return maxNumOfUses;
	}

	public void setMaxNumOfUses(String maxNumOfUses) {
		this.maxNumOfUses = maxNumOfUses;
	}

	public String getRemainingNumOfUses() {
		return remainingNumOfUses;
	}

	public void setRemainingNumOfUses(String remainingNumOfUses) {
		this.remainingNumOfUses = remainingNumOfUses;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getConsumerName() {
		return consumerName;
	}

	public void setConsumerName(String consumerName) {
		this.consumerName = consumerName;
	}

	public String getConsumerRandomId() {
		return consumerRandomId;
	}

	public void setConsumerRandomId(String consumerRandomId) {
		this.consumerRandomId = consumerRandomId;
	}
}
