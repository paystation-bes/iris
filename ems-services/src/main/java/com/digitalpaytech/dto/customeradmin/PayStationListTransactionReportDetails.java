package com.digitalpaytech.dto.customeradmin;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.util.WebCoreConstants;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias(value = "payStationListTransactionReportDetails")
@StoryAlias(value = PayStationListTransactionReportDetails.ALIAS, code = "transactionReportDetail")
public class PayStationListTransactionReportDetails {
    
    public static final String ALIAS = "pay station transaction report detail";
    public static final String ALIAS_CASH_PAID = "cash paid";
    public static final String ALIAS_EXCESS_PAYMENT = "excess payment";
    public static final String ALIAS_AMOUNT_REFUND_SLIP = "refund slip";
    public static final String ALIAS_PAYMENT_TYPE = "payment type";
    public static final String ALIAS_CHARGED_AMOUNT = "charged amount";
    public static final String ALIAS_CHANGE_DISPENSED = "change deispensed";
    public static final String ALIAS_LOCATION = "location";
    public static final String ALIAS_PAYSTATION_NAME = "pay station name";
    public static final String ALIAS_DATE_PURCHASED = "date purchased";
    public static final String ALIAS_CARD_PAID_AMOUNT = "card paid amount";
    
    private String locationName;
    private String payStationName;
    private String payStationSerial;
    private String transactionNumber;
    private String lotNumber;
    private int stallNumber;
    private String licensePlateNumber;
    private String transactionType;
    private int processorTransactionTypeId;
    private String paymentType;
    private int addTimeNumber;
    private String purchasedDate;
    private String monthlyStartDate;
    private String expiryDate;
    private Float chargedAmount;
    private Float cashPaid;
    private Float cardPaid;
    private Float smartCardPaid;
    private Float valueCardPaid;
    private Float excessPayment;
    private Float changeDispensed;
    private String refundSlip;
    private String cardType;
    private String cardNumber;
    private String authorizationNumber;
    private String referenceNumber;
    private String status;
    private String processingDate;
    private Float amountRefundedSlip;
    private Float amountRefundedCard;
    private String couponNumber;
    private String smartCardData;
    
    public PayStationListTransactionReportDetails() {
    }
    
    public PayStationListTransactionReportDetails(final String locationName, final String payStationName, final String payStationSerial,
            final String transactionNumber, final String lotNumber, final int stallNumber, final String licensePlateNumber, final String transactionType,
            final int processorTransactionTypeId, final String paymentType, final int addTimeNumber, final String purchasedDate, final String monthlyStartDate,
            final String expiryDate, final Float chargedAmount, final Float cashPaid, final Float cardPaid, final Float smartCardPaid,
            final Float valueCardPaid, final Float excessPayment, final Float changeDispensed, final String refundSlip, final String cardType,
            final String cardNumber, final String authorizationNumber, final String referenceNumber, final String status, final String processingDate,
            final Float amountRefundedCard, final Float amountRefundedSlip, final String couponNumber, final String smartCardData) {
        super();
        this.locationName = locationName;
        this.payStationName = payStationName;
        this.payStationSerial = payStationSerial;
        this.transactionNumber = transactionNumber;
        this.lotNumber = lotNumber;
        this.stallNumber = stallNumber;
        this.licensePlateNumber = licensePlateNumber;
        this.transactionType = transactionType;
        this.processorTransactionTypeId = processorTransactionTypeId;
        this.paymentType = paymentType;
        this.addTimeNumber = addTimeNumber;
        this.purchasedDate = purchasedDate;
        this.monthlyStartDate = monthlyStartDate;
        this.expiryDate = expiryDate;
        this.chargedAmount = chargedAmount;
        this.cashPaid = cashPaid;
        this.cardPaid = cardPaid;
        this.smartCardPaid = smartCardPaid;
        this.valueCardPaid = valueCardPaid;
        this.excessPayment = excessPayment;
        this.changeDispensed = changeDispensed;
        this.refundSlip = refundSlip;
        this.cardType = cardType;
        this.cardNumber = cardNumber;
        this.authorizationNumber = authorizationNumber;
        this.referenceNumber = referenceNumber;
        this.status = status;
        this.processingDate = processingDate;
        this.amountRefundedSlip = amountRefundedSlip;
        this.amountRefundedCard = amountRefundedCard;
        this.couponNumber = couponNumber;
        this.smartCardData = smartCardData;
    }
    
    @StoryAlias(ALIAS_LOCATION)
    public final String getLocationName() {
        return this.locationName;
    }
    
    public final void setLocationName(final String locationName) {
        this.locationName = locationName;
    }
    
    @StoryAlias(ALIAS_PAYSTATION_NAME)
    public final String getPayStationName() {
        return this.payStationName;
    }
    
    public final void setPayStationName(final String payStationName) {
        this.payStationName = payStationName;
    }
    
    public final String getPayStationSerial() {
        return this.payStationSerial;
    }
    
    public final void setPayStationSerial(final String payStationSerial) {
        this.payStationSerial = payStationSerial;
    }
    
    public final String getTransactionNumber() {
        return this.transactionNumber;
    }
    
    public final void setTransactionNumber(final String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }
    
    public final String getLotNumber() {
        return this.lotNumber;
    }
    
    public final void setLotNumber(final String lotNumber) {
        this.lotNumber = lotNumber;
    }
    
    public final int getStallNumber() {
        return this.stallNumber;
    }
    
    public final void setStallNumber(final int stallNumber) {
        this.stallNumber = stallNumber;
    }
    
    public final String getLicensePlateNumber() {
        return this.licensePlateNumber;
    }
    
    public final void setLicensePlateNumber(final String licensePlateNumber) {
        this.licensePlateNumber = licensePlateNumber;
    }
    
    public final String getTransactionType() {
        return this.transactionType;
    }
    
    public final void setTransactionType(final String transactionType) {
        this.transactionType = transactionType;
    }
    
    @StoryAlias(ALIAS_PAYMENT_TYPE)
    public final String getPaymentType() {
        return this.paymentType;
    }
    
    public final void setPaymentType(final String paymentType) {
        this.paymentType = paymentType;
    }
    
    public final int getAddTimeNumber() {
        return this.addTimeNumber;
    }
    
    public final void setAddTimeNumber(final int addTimeNumber) {
        this.addTimeNumber = addTimeNumber;
    }
    
    @StoryAlias(ALIAS_DATE_PURCHASED)
    public final String getPurchasedDate() {
        return this.purchasedDate;
    }
    
    public final void setPurchasedDate(final String purchasedDate) {
        this.purchasedDate = purchasedDate;
    }
    
    public final String getMonthlyStartDate() {
        return this.monthlyStartDate;
    }
    
    public final void setMonthlyStartDate(final String monthlyStartDate) {
        this.monthlyStartDate = monthlyStartDate;
    }
    
    public final String getExpiryDate() {
        return this.expiryDate;
    }
    
    public final void setExpiryDate(final String expiryDate) {
        this.expiryDate = expiryDate;
    }
    
    @StoryAlias(ALIAS_CHARGED_AMOUNT)
    public final Float getChargedAmount() {
        return this.chargedAmount;
    }
    
    public final void setChargedAmount(final Float chargedAmount) {
        this.chargedAmount = chargedAmount;
    }
    
    @StoryAlias(ALIAS_CASH_PAID)
    public final Float getCashPaid() {
        return this.cashPaid;
    }
    
    public final void setCashPaid(final Float cashPaid) {
        this.cashPaid = cashPaid;
    }
    
    @StoryAlias(ALIAS_CARD_PAID_AMOUNT)
    public final Float getCardPaid() {
        return this.cardPaid;
    }
    
    public final void setCardPaid(final Float cardPaid) {
        this.cardPaid = cardPaid;
    }
    
    public final Float getSmartCardPaid() {
        return this.smartCardPaid;
    }
    
    public final void setSmartCardPaid(final Float smartCardPaid) {
        this.smartCardPaid = smartCardPaid;
    }
    
    public final Float getValueCardPaid() {
        return this.valueCardPaid;
    }
    
    public final void setValueCardPaid(final Float valueCardPaid) {
        this.valueCardPaid = valueCardPaid;
    }
    
    @StoryAlias(ALIAS_EXCESS_PAYMENT)
    public final Float getExcessPayment() {
        return this.excessPayment;
    }
    
    public final void setExcessPayment(final Float excessPayment) {
        this.excessPayment = excessPayment;
    }
    
    @StoryAlias(ALIAS_CHANGE_DISPENSED)
    public final Float getChangeDispensed() {
        return this.changeDispensed;
    }
    
    public final void setChangeDispensed(final Float changeDispensed) {
        this.changeDispensed = changeDispensed;
    }
    
    public final String getRefundSlip() {
        return this.refundSlip;
    }
    
    public final void setRefundSlip(final String refundSlip) {
        this.refundSlip = refundSlip;
    }
    
    public final void setRefundSlip(final boolean refundSlip) {
        if (refundSlip) {
            this.refundSlip = WebCoreConstants.RESPONSE_TRUE;
        } else {
            this.refundSlip = WebCoreConstants.RESPONSE_FALSE;
        }
    }
    
    public final String getCardType() {
        return this.cardType;
    }
    
    public final void setCardType(final String cardType) {
        this.cardType = cardType;
    }
    
    public final String getCardNumber() {
        return this.cardNumber;
    }
    
    public final void setCardNumber(final String cardNumber) {
        this.cardNumber = cardNumber;
    }
    
    public final String getAuthorizationNumber() {
        return this.authorizationNumber;
    }
    
    public final void setAuthorizationNumber(final String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }
    
    public final String getReferenceNumber() {
        return this.referenceNumber;
    }
    
    public final void setReferenceNumber(final String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }
    
    public final String getStatus() {
        return this.status;
    }
    
    public final void setStatus(final String status) {
        this.status = status;
    }
    
    public final String getProcessingDate() {
        return this.processingDate;
    }
    
    public final void setProcessingDate(final String processingDate) {
        this.processingDate = processingDate;
    }
    
    public final Float getAmountRefundedCard() {
        return this.amountRefundedCard;
    }
    
    public final void setAmountRefundedCard(final Float amountRefundedCard) {
        this.amountRefundedCard = amountRefundedCard;
    }
    
    @StoryAlias(ALIAS_AMOUNT_REFUND_SLIP)
    public final Float getAmountRefundedSlip() {
        return this.amountRefundedSlip;
    }
    
    public final void setAmountRefundedSlip(final Float amountRefundedSlip) {
        this.amountRefundedSlip = amountRefundedSlip;
    }
    
    public final String getCouponNumber() {
        return this.couponNumber;
    }
    
    public final void setCouponNumber(final String couponNumber) {
        this.couponNumber = couponNumber;
    }
    
    public final String getSmartCardData() {
        return this.smartCardData;
    }
    
    public final void setSmartCardData(final String smartCardData) {
        this.smartCardData = smartCardData;
    }
    
    public final int getProcessorTransactionTypeId() {
        return this.processorTransactionTypeId;
    }
    
    public final void setProcessorTransactionTypeId(final int processorTransactionTypeId) {
        this.processorTransactionTypeId = processorTransactionTypeId;
    }
}
