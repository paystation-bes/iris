package com.digitalpaytech.dto.customeradmin;

import java.io.Serializable;
import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

public class TransactionReceiptSearchCriteria implements Serializable {
    private static final long serialVersionUID = -8272314638044988948L;
    
    private Integer customerId;
    private String licencePlateNumber;
    private String spaceNumber;
    private String couponNumber;
    private String cardNumber;
    private String mobileNumber;
    private String emailAddress;
    private Integer locationId;
    private Date startDateTime;
    private Date endDateTime;
    
    @XStreamOmitField
    private Integer page;
    
    @XStreamOmitField
    private Integer itemsPerPage;
    private Date maxLastUpdatedTime;
    
    public TransactionReceiptSearchCriteria() {
        
    }
    
    public final Integer getLocationId() {
        return this.locationId;
    }
    
    public final void setLocationId(final Integer locationId) {
        this.locationId = locationId;
    }
    
    public final String getLicencePlateNumber() {
        return this.licencePlateNumber;
    }
    
    public final void setLicencePlateNumber(final String licencePlateNumber) {
        this.licencePlateNumber = licencePlateNumber;
    }
    
    public final String getMobileNumber() {
        return this.mobileNumber;
    }
    
    public final void setMobileNumber(final String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
    
    public final String getEmailAddress() {
        return this.emailAddress;
    }
    
    public final void setEmailAddress(final String emailAddress) {
        this.emailAddress = emailAddress;
    }
    
    public final String getCardNumber() {
        return this.cardNumber;
    }
    
    public final void setCardNumber(final String cardNumber) {
        this.cardNumber = cardNumber;
    }
    
    public final Integer getPage() {
        return this.page;
    }
    
    public final void setPage(final Integer page) {
        this.page = page;
    }
    
    public final Integer getCustomerId() {
        return this.customerId;
    }
    
    public final void setCustomerId(final Integer customerId) {
        this.customerId = customerId;
    }
    
    public final Integer getItemsPerPage() {
        return this.itemsPerPage;
    }
    
    public final void setItemsPerPage(final Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }
    
    public final String getCouponNumber() {
        return this.couponNumber;
    }
    
    public final void setCouponNumber(final String couponNumber) {
        this.couponNumber = couponNumber;
    }
    
    public final String getSpaceNumber() {
        return this.spaceNumber;
    }
    
    public final void setSpaceNumber(final String spaceNumber) {
        this.spaceNumber = spaceNumber;
    }
    
    public final Date getStartDateTime() {
        return this.startDateTime;
    }
    
    public final void setStartDateTime(final Date startDateTime) {
        this.startDateTime = startDateTime;
    }
    
    public final Date getEndDateTime() {
        return this.endDateTime;
    }
    
    public final void setEndDateTime(final Date endDateTime) {
        this.endDateTime = endDateTime;
    }
    
    public final Date getMaxLastUpdatedTime() {
        return this.maxLastUpdatedTime;
    }
    
    public final void setMaxLastUpdatedTime(final Date maxLastUpdatedTime) {
        this.maxLastUpdatedTime = maxLastUpdatedTime;
    }
}
