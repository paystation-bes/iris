package com.digitalpaytech.dto.customeradmin;

import java.util.HashMap;
import java.util.Map;

import org.hibernate.transform.AliasToBeanResultTransformer;

import com.digitalpaytech.domain.Route;
import com.digitalpaytech.util.RandomKeyMapping;

public class LocationTreeRouteTransformer extends AliasToBeanResultTransformer {
	private static final long serialVersionUID = -55308432017368346L;
	
	private RandomKeyMapping keyMapping;
	private Map<Integer, LocationTree> organizationsMap;
	private Map<Integer, LocationTree> routesMap;
	
	private int idIdx = -1;
	private int nameIdx = -1;
	private int customerIdIdx = -1;
	
	public LocationTreeRouteTransformer(RandomKeyMapping keyMapping, Map<Integer, LocationTree> organizationsMap) {
		super(LocationTree.class);
		
		this.keyMapping = keyMapping;
		this.organizationsMap = organizationsMap;
		this.routesMap = new HashMap<Integer, LocationTree>();
	}
	
	@Override
	public Object transformTuple(Object[] tuple, String[] aliases) {
		if(this.idIdx < 0) {
			resolveIdx(tuple, aliases);
		}
		
		Integer id = (Integer) tuple[this.idIdx];
		LocationTree locTree = new LocationTree((Integer) tuple[this.idIdx]);
		locTree.setName((String) tuple[this.nameIdx]);
		locTree.setRandomId(keyMapping.getRandomString(Route.class, id));
		locTree.setRoute(true);
		
		Integer customerId = (Integer) tuple[this.customerIdIdx];
		if(customerId != null) {
			LocationTree org = this.organizationsMap.get(customerId);
			if(org != null) {
				org.addChild(locTree);
			}
		}
		
		this.routesMap.put(locTree.getId(), locTree);
		
		return locTree;
	}
	
	private void resolveIdx(Object[] tuple, String[] aliases) {
		int idx = aliases.length;
		while(--idx >= 0) {
			if(aliases[idx].equals("id")) {
				this.idIdx = idx;
			}
			else if(aliases[idx].equals("name")) {
				this.nameIdx = idx;
			}
			else if(aliases[idx].equals("customerId")) {
				this.customerIdIdx = idx;
			}
		}
	}

	public Map<Integer, LocationTree> getRoutesMap() {
		return routesMap;
	}
}
