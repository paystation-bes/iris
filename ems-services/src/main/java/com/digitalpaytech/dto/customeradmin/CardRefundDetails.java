package com.digitalpaytech.dto.customeradmin;

/**
 * 
 * 
 * @author Brian Kim
 * 
 */
public class CardRefundDetails {
    
    private Long purchaseId;
    private String paystationName;
    private String paystationSerialNumber;
    private String paystationSettingName;
    private int ticketNumber;
    private int spaceNumber;
    private String plateNumber;
    private String transactionType;
    private String paymentType;
    private String purchasedDate;
    private String expiryDate;
    private String chargedAmount;
    private String cashPaidAmount;
    private String cardPaidAmount;
    private String excessPaymentAmount;
    private String changeDispensedAmount;
    private String cardType;
    private String cardNumber;
    private String statusName;
    private String authorizationNumber;
    private String referenceNumber;
    private String settlementDate;
    private String permitType;
    private String rate;
    
    public Long getPurchaseId() {
        return purchaseId;
    }
    
    public void setPurchaseId(Long purchaseId) {
        this.purchaseId = purchaseId;
    }
    
    public String getPaystationSettingName() {
        return paystationSettingName;
    }
    
    public void setPaystationSettingName(String paystationSettingName) {
        this.paystationSettingName = paystationSettingName;
    }
    
    public int getSpaceNumber() {
        return spaceNumber;
    }
    
    public void setSpaceNumber(int spaceNumber) {
        this.spaceNumber = spaceNumber;
    }
    
    public String getPlateNumber() {
        return plateNumber;
    }
    
    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }
    
    public String getTransactionType() {
        return transactionType;
    }
    
    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }
    
    public String getPaymentType() {
        return paymentType;
    }
    
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }
    
    public String getChangeDispensedAmount() {
        return changeDispensedAmount;
    }
    
    public void setChangeDispensedAmount(String changeDispensedAmount) {
        this.changeDispensedAmount = changeDispensedAmount;
    }
    
    public String getPurchasedDate() {
        return purchasedDate;
    }
    
    public void setPurchasedDate(String purchasedDate) {
        this.purchasedDate = purchasedDate;
    }
    
    public String getExpiryDate() {
        return expiryDate;
    }
    
    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }
    
    public String getChargedAmount() {
        return chargedAmount;
    }
    
    public void setChargedAmount(String chargedAmount) {
        this.chargedAmount = chargedAmount;
    }
    
    public String getCashPaidAmount() {
        return cashPaidAmount;
    }
    
    public void setCashPaidAmount(String cashPaidAmount) {
        this.cashPaidAmount = cashPaidAmount;
    }
    
    public String getCardPaidAmount() {
        return cardPaidAmount;
    }
    
    public void setCardPaidAmount(String cardPaidAmount) {
        this.cardPaidAmount = cardPaidAmount;
    }
    
    public String getExcessPaymentAmount() {
        return excessPaymentAmount;
    }
    
    public void setExcessPaymentAmount(String excessPaymentAmount) {
        this.excessPaymentAmount = excessPaymentAmount;
    }
    
    public String getCardType() {
        return cardType;
    }
    
    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
    
    public String getCardNumber() {
        return cardNumber;
    }
    
    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
    
    public String getStatusName() {
        return statusName;
    }
    
    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
    
    public String getAuthorizationNumber() {
        return authorizationNumber;
    }
    
    public void setAuthorizationNumber(String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }
    
    public String getReferenceNumber() {
        return referenceNumber;
    }
    
    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }
    
    public String getSettlementDate() {
        return settlementDate;
    }
    
    public void setSettlementDate(String settlementDate) {
        this.settlementDate = settlementDate;
    }
    
    public String getPaystationName() {
        return paystationName;
    }
    
    public void setPaystationName(String paystationName) {
        this.paystationName = paystationName;
    }
    
    public int getTicketNumber() {
        return ticketNumber;
    }
    
    public void setTicketNumber(int ticketNumber) {
        this.ticketNumber = ticketNumber;
    }
    
    public String getPaystationSerialNumber() {
        return paystationSerialNumber;
    }
    
    public void setPaystationSerialNumber(String paystationSerialNumber) {
        this.paystationSerialNumber = paystationSerialNumber;
    }
    
    public String getPermitType() {
        return permitType;
    }
    
    public void setPermitType(String permitType) {
        this.permitType = permitType;
    }
    
    public String getRate() {
        return rate;
    }
    
    public void setRate(String rate) {
        this.rate = rate;
    }
}
