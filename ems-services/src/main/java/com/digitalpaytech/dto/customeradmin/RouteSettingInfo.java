package com.digitalpaytech.dto.customeradmin;

public class RouteSettingInfo {

	private String randomizedRouteId;
	private String routeName;
	private String type;
	private int label;
	
	public RouteSettingInfo() {
		
	}
	
	public RouteSettingInfo(String randomizedRouteId, String routeName,
			String type, int label) {
		this.randomizedRouteId = randomizedRouteId;
		this.routeName = routeName;
		this.type = type;
		this.label = label;
	}

	public String getRandomizedRouteId() {
		return randomizedRouteId;
	}

	public void setRandomizedRouteId(String randomizedRouteId) {
		this.randomizedRouteId = randomizedRouteId;
	}

	public String getRouteName() {
		return routeName;
	}

	public void setRouteName(String routeName) {
		this.routeName = routeName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getLabel() {
		return label;
	}

	public void setLabel(int label) {
		this.label = label;
	}
}
