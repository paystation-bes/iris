package com.digitalpaytech.dto.customeradmin;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class ReportDefinitionInfo {
    
    @XStreamImplicit(itemFieldName = "definitionInfoList")
    private List<DefinitionInfo> definitionList;
    
    public List<DefinitionInfo> getDefinitionList() {
        return definitionList;
    }
    
    public void setDefinitionList(List<DefinitionInfo> definitionList) {
        this.definitionList = definitionList;
    }
    
    public ReportDefinitionInfo() {
        definitionList = new ArrayList<DefinitionInfo>();
    }
    
    static public class DefinitionInfo {
        
        private String randomId;
        private String title;
        private String nextScheduledTime;
        private long nextScheduledMs;
        
        public DefinitionInfo() {
        }
        
        public DefinitionInfo(String randomId, String title, String nextScheduledTime, long nextScheduledMs) {
            this.randomId = randomId;
            this.title = title;
            this.nextScheduledTime = nextScheduledTime;
            this.nextScheduledMs = nextScheduledMs;
        }
        
        public String getRandomId() {
            return randomId;
        }
        
        public void setRandomId(String randomId) {
            this.randomId = randomId;
        }
        
        public String getTitle() {
            return title;
        }
        
        public void setTitle(String title) {
            this.title = title;
        }
        
        public String getNextScheduledTime() {
            return nextScheduledTime;
        }
        
        public void setNextScheduledTime(String nextScheduledTime) {
            this.nextScheduledTime = nextScheduledTime;
        }
        
        public long getNextScheduledMs() {
            return nextScheduledMs;
        }
        
        public void setNextScheduledMs(long nextScheduledMs) {
            this.nextScheduledMs = nextScheduledMs;
        }
    }
}
