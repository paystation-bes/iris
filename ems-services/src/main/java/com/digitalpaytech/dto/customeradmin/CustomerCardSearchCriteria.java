package com.digitalpaytech.dto.customeradmin;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class CustomerCardSearchCriteria implements Serializable {
    private static final long serialVersionUID = 4078094730712057795L;
    
    private Integer customerId;
    private List<String> cardNumber;
    private Integer cardType;
    private List<String> cardTypeName;
    
    private List<Integer> cardIds;
    
    private Integer ignoredConsumerId;
    private List<Integer> ignoredCardIds;
    
    private boolean unusedCard;
    private boolean usedCard;
    
    private Date validForDate;
    
    private Integer page;
    private String[] orderColumn;
    private boolean orderDesc = false;
    
    private Integer itemsPerPage;
    
    private Date maxUpdatedTime;
    
    private Integer endCustomerCardId;
    
	public CustomerCardSearchCriteria() {
        
    }

    public List<String> getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(List<String> cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Integer getCardType() {
        return cardType;
    }

    public void setCardType(Integer cardType) {
        this.cardType = cardType;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String[] getOrderColumn() {
        return orderColumn;
    }

    public void setOrderColumn(String[] orderColumn) {
        this.orderColumn = orderColumn;
    }

    public boolean isOrderDesc() {
        return orderDesc;
    }

    public void setOrderDesc(boolean orderDesc) {
        this.orderDesc = orderDesc;
    }

    public Integer getItemsPerPage() {
        return itemsPerPage;
    }

    public void setItemsPerPage(Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

	public List<Integer> getCardIds() {
		return cardIds;
	}

	public void setCardIds(List<Integer> cardIds) {
		this.cardIds = cardIds;
	}

	public List<Integer> getIgnoredCardIds() {
		return ignoredCardIds;
	}

	public void setIgnoredCardIds(List<Integer> ignoredCardIds) {
		this.ignoredCardIds = ignoredCardIds;
	}

	public Date getMaxUpdatedTime() {
		return maxUpdatedTime;
	}

	public void setMaxUpdatedTime(Date maxUpdatedTime) {
		this.maxUpdatedTime = maxUpdatedTime;
	}

	public boolean isUnusedCard() {
		return unusedCard;
	}

	public void setUnusedCard(boolean unusedCard) {
		this.unusedCard = unusedCard;
	}

	public boolean isUsedCard() {
		return usedCard;
	}

	public void setUsedCard(boolean usedCard) {
		this.usedCard = usedCard;
	}

	public List<String> getCardTypeName() {
		return cardTypeName;
	}

	public void setCardTypeName(List<String> cardTypeName) {
		this.cardTypeName = cardTypeName;
	}

	public Integer getIgnoredConsumerId() {
		return ignoredConsumerId;
	}

	public void setIgnoredConsumerId(Integer ignoredConsumerId) {
		this.ignoredConsumerId = ignoredConsumerId;
	}

	public Date getValidForDate() {
		return validForDate;
	}

	public void setValidForDate(Date validForDate) {
		this.validForDate = validForDate;
	}
	
	public Integer getEndCustomerCardId() {
		return endCustomerCardId;
	}

	public void setEndCustomerCardId(Integer endCustomerCardId) {
		this.endCustomerCardId = endCustomerCardId;
	}
}
