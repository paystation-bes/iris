package com.digitalpaytech.dto.customeradmin;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import com.digitalpaytech.dto.comparator.AlertDateTimeComparator;
import com.digitalpaytech.dto.comparator.AlertMessageComparator;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.support.RelativeDateTime;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class AlertInfo {

	@XStreamImplicit(itemFieldName="alerts")
	private List<AlertInfoEntry> alerts;
	
	public AlertInfo(){	
		alerts = new ArrayList<AlertInfoEntry>();
	}
	
	public List<AlertInfoEntry> getAlerts() {
		return alerts;
	}

	public void setAlerts(List<AlertInfoEntry> alerts) {
		this.alerts = alerts;
	}
	
	public List<AlertInfoEntry> sortAlerts(boolean ascending, String category){

		if(category.equals("Date")){//TODO: use some webcoreconstant for this
			AlertDateTimeComparator comparator = new AlertDateTimeComparator();
			if(ascending){
				Collections.sort(alerts, comparator);				
			}else{
				Collections.sort(alerts, Collections.reverseOrder(comparator));
			}
		}
		else if(category.equals("Alert")){//TODO: use some webcoreconstant for this
			AlertMessageComparator comparator = new AlertMessageComparator();
			if(ascending){
				Collections.sort(alerts, comparator);				
			}else{
				Collections.sort(alerts, Collections.reverseOrder(comparator));
			}
		}
		return alerts;
	}

	public void addAlert(Calendar date, String alertMessage, int severity, String module, String timeZone, Boolean isActive) {
		AlertInfoEntry alertInfo = new AlertInfoEntry();
		if (date != null) {
		    alertInfo.setDate(date.getTime());
		}
		
		alertInfo.setAlertMessage(alertMessage);
		alertInfo.setSeverity(severity);
		alertInfo.setModule(module);
		alertInfo.setIsActive(isActive);
		
		alertInfo.setAlertDate(new RelativeDateTime(date.getTime(), timeZone));
		
		this.alerts.add(alertInfo);
	}	
}
