package com.digitalpaytech.dto.customeradmin;

import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.support.WebObjectId;

public class CollectionsCentreMinimalInfo {
    
    private String amount;
    private Integer alertThresholdTypeId;
    private Integer collectionTypeId;
    private WebObjectId collectionRandomId;
    private String revenueType;
    private String collectionUserName;
    
    public CollectionsCentreMinimalInfo(final CollectionsCentreCollectionInfo collectionInfo) {
        //this.amount = collectionInfo.getAmount();
        this.amount = WebCoreUtil.formatBase100Value(collectionInfo.getAmount());
        this.alertThresholdTypeId = collectionInfo.getAlertThresholdTypeId();
        this.collectionTypeId = collectionInfo.getCollectionTypeId();
        this.collectionRandomId = collectionInfo.getCollectionRandomId();
        this.revenueType = collectionInfo.getRevenueType();
        this.collectionUserName = collectionInfo.getCollectionUserName();
    }
    
    public final String getAmount() {
        return this.amount;
    }
    
    public final void setAmount(final String amount) {
        this.amount = amount;
    }
    
    public final String getRevenueType() {
        return this.revenueType;
    }
    
    public final void setRevenueType(final String revenueType) {
        this.revenueType = revenueType;
    }
    
    public final Integer getAlertThresholdTypeId() {
        return this.alertThresholdTypeId;
    }
    
    public final void setAlertThresholdTypeId(final Integer alertThresholdTypeId) {
        this.alertThresholdTypeId = alertThresholdTypeId;
    }
    
    public final WebObjectId getCollectionRandomId() {
        return this.collectionRandomId;
    }
    
    public final void setCollectionRandomId(final WebObjectId collectionRandomId) {
        this.collectionRandomId = collectionRandomId;
    }
    
    public final Integer getCollectionTypeId() {
        return this.collectionTypeId;
    }
    
    public final void setCollectionTypeId(final Integer collectionTypeId) {
        this.collectionTypeId = collectionTypeId;
    }
    
    public final String getCollectionUserName() {
        return this.collectionUserName;
    }
    
    public final void setCollectionUserName(final String collectionUserName) {
        this.collectionUserName = collectionUserName;
    }
}
