package com.digitalpaytech.dto.customeradmin;

import java.io.Serializable;
import java.util.List;

import com.digitalpaytech.domain.Route;
import com.digitalpaytech.util.support.WebObjectId;

public class PaystationSummaryEntry implements Serializable {
    
    private static final long serialVersionUID = 5955353318849455549L;
    private WebObjectId posRandomId;
    private String pointOfSaleName;
    private String serial;
    
    private Integer severity;
    private WebObjectId locationRandomId;
    private String locationName;
    
    private List<Route> routes;
    
    public PaystationSummaryEntry() {
        
    }
    
    public final WebObjectId getPosRandomId() {
        return this.posRandomId;
    }
    
    public final void setPosRandomId(final WebObjectId posRandomId) {
        this.posRandomId = posRandomId;
    }
    
    public final String getSerial() {
        return this.serial;
    }
    
    public final void setSerial(final String serial) {
        this.serial = serial;
    }
    
    public final Integer getSeverity() {
        return this.severity;
    }
    
    public final void setSeverity(final Integer severity) {
        this.severity = severity;
    }
    
    public final WebObjectId getLocationRandomId() {
        return this.locationRandomId;
    }
    
    public final void setLocationRandomId(final WebObjectId locationRandomId) {
        this.locationRandomId = locationRandomId;
    }
    
    public final String getLocationName() {
        return this.locationName;
    }
    
    public final void setLocationName(final String locationName) {
        this.locationName = locationName;
    }
    
    public final String getPointOfSaleName() {
        return this.pointOfSaleName;
    }
    
    public final void setPointOfSaleName(final String pointOfSaleName) {
        this.pointOfSaleName = pointOfSaleName;
    }
    
    public final List<Route> getRoutes() {
        return this.routes;
    }
    
    public final void setRoutes(final List<Route> routes) {
        this.routes = routes;
    }
    
}
