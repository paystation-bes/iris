package com.digitalpaytech.dto.customeradmin;

import java.sql.Date;

public class AlertSummaryEntry extends PaystationSummaryEntry {
    
    private static final long serialVersionUID = -7037699497263996778L;
    private Date lastSeenGMT;
    private String lastSeenDate;
    private Integer lastSeenSeverity;
    
    private Byte paperLevel;
    private Byte batteryLevel;
    private Float batteryVoltage;
    
    public AlertSummaryEntry() {
        
    }
    
    public final Date getLastSeenGMT() {
        return this.lastSeenGMT;
    }
    
    public final void setLastSeenGMT(final Date lastSeenGMT) {
        this.lastSeenGMT = lastSeenGMT;
    }
    
    public final String getLastSeenDate() {
        return this.lastSeenDate;
    }
    
    public final void setLastSeenDate(final String lastSeenDate) {
        this.lastSeenDate = lastSeenDate;
    }
    
    public final Integer getLastSeenSeverity() {
        return this.lastSeenSeverity;
    }
    
    public final void setLastSeenSeverity(final Integer lastSeenSeverity) {
        this.lastSeenSeverity = lastSeenSeverity;
    }
    
    public final Byte getPaperLevel() {
        return this.paperLevel;
    }
    
    public final void setPaperLevel(final Byte paperLevel) {
        this.paperLevel = paperLevel;
    }
    
    public final Byte getBatteryLevel() {
        return this.batteryLevel;
    }
    
    public final void setBatteryLevel(final Byte batteryLevel) {
        this.batteryLevel = batteryLevel;
    }
    
    public final Float getBatteryVoltage() {
        return this.batteryVoltage;
    }
    
    public final void setBatteryVoltage(final Float batteryVoltage) {
        this.batteryVoltage = batteryVoltage;
    }
    
}
