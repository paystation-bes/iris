package com.digitalpaytech.dto.customeradmin;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias(value = "payStationListTransactionReportList")
public class PayStationListTransactionReportList {

	@XStreamImplicit
	private List<PayStationListTransactionReport> transactionReports;
	
	public PayStationListTransactionReportList(){
		transactionReports = new ArrayList<PayStationListTransactionReport>();
	}

	public List<PayStationListTransactionReport> getTransactionReports() {
		return transactionReports;
	}

	public void setTransactionReports(
			List<PayStationListTransactionReport> transactionReports) {
		this.transactionReports = transactionReports;
	}
	
	public void addTransactionReport(PayStationListTransactionReport report){
		this.transactionReports.add(report);
	}
	
	
}
