package com.digitalpaytech.dto.customeradmin;

import java.util.Date;

public class PosDownloadStatus {
    private String locationName;
    private String serialNumber;
    private String paystationName;
    private Date downloadDate;
    private String status;
    
    public PosDownloadStatus() {
    }

    public PosDownloadStatus(String locationName, String serialNumber, String paystationName, Date downloadDate, String status) {
        this.locationName = locationName;
        this.serialNumber = serialNumber;
        this.paystationName = paystationName;
        this.downloadDate = downloadDate;
        this.status = status;
    }

    public String getLocationName() {
        return locationName;
    }
    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }
    public String getSerialNumber() {
        return serialNumber;
    }
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
    public String getPaystationName() {
        return paystationName;
    }
    public void setPaystationName(String paystationName) {
        this.paystationName = paystationName;
    }
    public Date getDownloadDate() {
        return downloadDate;
    }
    public void setDownloadDate(Date downloadDate) {
        this.downloadDate = downloadDate;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
}
