package com.digitalpaytech.dto.customeradmin;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class ReportRepositoryInfo {
    
    @XStreamImplicit(itemFieldName = "repositoryInfoList")
    private List<RepositoryInfo> repositoryList;
    
    public List<RepositoryInfo> getRepositoryList() {
        return repositoryList;
    }
    
    public void setRepositoryList(List<RepositoryInfo> repositoryList) {
        this.repositoryList = repositoryList;
    }
    
    public ReportRepositoryInfo() {
        repositoryList = new ArrayList<RepositoryInfo>();
    }
    
    static public class RepositoryInfo {
        
        private String randomId;
        private String title;
        private String createdTime;
        private long createdMs;
        private int status;
        private boolean isToBeDeleted;
        private boolean isReadByUser;
        
        public RepositoryInfo() {
        }
        
        public String getRandomId() {
            return randomId;
        }
        
        public void setRandomId(String randomId) {
            this.randomId = randomId;
        }
        
        public String getTitle() {
            return title;
        }
        
        public void setTitle(String title) {
            this.title = title;
        }
        
        public String getCreatedTime() {
            return createdTime;
        }
        
        public void setCreatedTime(String createdTime) {
            this.createdTime = createdTime;
        }
        
        public long getCreatedMs() {
            return createdMs;
        }
        
        public void setCreatedMs(long createdMs) {
            this.createdMs = createdMs;
        }
        
        public int getStatus() {
            return status;
        }
        
        public void setStatus(int status) {
            this.status = status;
        }
        
        public boolean isToBeDeleted() {
            return isToBeDeleted;
        }
        
        public void setToBeDeleted(boolean isToBeDeleted) {
            this.isToBeDeleted = isToBeDeleted;
        }
        
        public boolean isReadByUser() {
            return isReadByUser;
        }
        
        public void setReadByUser(boolean isReadByUser) {
            this.isReadByUser = isReadByUser;
        }
    }
}
