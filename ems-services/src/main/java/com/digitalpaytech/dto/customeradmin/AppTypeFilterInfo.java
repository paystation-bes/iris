package com.digitalpaytech.dto.customeradmin;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class AppTypeFilterInfo {
    @XStreamImplicit(itemFieldName="appTypes")
    private List<AppType> appTypes;
    
    public AppTypeFilterInfo(){
        appTypes = new ArrayList<AppType>();
    }
    public List<AppType> getAppTypes(){
        return appTypes;
    }
    public void setAppTypes(List<AppType> appTypes){
        this.appTypes = appTypes;
    }
    public void addAppType(String name, String randomId, Integer licenseUsed, Integer licenseCount){
        appTypes.add(new AppType(name, randomId, licenseUsed == null ? 0 : licenseUsed, licenseCount == null ? 0 : licenseCount));
    }
    
    public class AppType{
        private String name;
        private String randomId;
        private int licenseUsed;
        private int licenseCount;
        public AppType(String name, String randomId, int licenseUsed, int licenseCount){
            this.name = name;
            this.randomId = randomId;
            this.licenseUsed = licenseUsed;
            this.licenseCount = licenseCount;
        }
        public int getLicenseUsed(){
            return licenseUsed;
        }
        public void setLicenseUsed(int licenseUsed){
            this.licenseUsed = licenseUsed;
        }
        public int getLicenseCount(){
            return licenseCount;
        }
        public void setLicenseCount(int licenseCount){
            this.licenseCount = licenseCount;
        }
        public String getName(){
            return name;
        }
        public void setName(String name){
            this.name = name;
        }
        public String getRandomId(){
            return randomId;
        }
        public void setRandomId(String randomId){
            this.randomId = randomId;
        }
    }
}
