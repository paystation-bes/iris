package com.digitalpaytech.dto.customeradmin;

import java.util.Collection;
import java.util.HashSet;

import com.digitalpaytech.util.support.WebObjectId;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias(value = "PaystationListInfo")
public class PaystationListInfo {

	private WebObjectId randomPosId;
	private String name;
    private String serialNumber;
	private String status;
	private WebObjectId randomLocationId;
	private boolean isPosActivated;
    private boolean isPosDecommissioned;
    private boolean isPosHidden;
    private Integer alertCount;
    private Integer alertSeverity;
    private Integer payStationType;
	
    @XStreamImplicit(itemFieldName="randomRouteId")
	private Collection<WebObjectId> randomRouteId;

	public PaystationListInfo() {
		this.randomRouteId = new HashSet<WebObjectId>();
	}
	
	public WebObjectId getRandomPosId() {
		return randomPosId;
	}

	public void setRandomPosId(WebObjectId randomPosId) {
		this.randomPosId = randomPosId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public WebObjectId getRandomLocationId() {
		return randomLocationId;
	}

	public void setRandomLocationId(WebObjectId randomLocationId) {
		this.randomLocationId = randomLocationId;
	}

	public Collection<WebObjectId> getRandomRouteId() {
		return randomRouteId;
	}

	public void addRandomRouteId(WebObjectId randomRouteId) {
		this.randomRouteId.add(randomRouteId);
	}

    public boolean getIsPosActivated() {
        return isPosActivated;
    }

    public void setIsPosActivated(boolean isPosActivated) {
        this.isPosActivated = isPosActivated;
    }
    
    public boolean getIsPosDecommissioned() {
        return isPosDecommissioned;
    }

    public void setIsPosDecommissioned(boolean isPosDecommissioned) {
        this.isPosDecommissioned = isPosDecommissioned;
    }

	public boolean getIsPosHidden() {
		return isPosHidden;
	}

	public void setIsPosHidden(boolean isPosHidden) {
		this.isPosHidden = isPosHidden;
	}
    
    public Integer getAlertCount() {
        return alertCount;
    }
    
    public void setAlertCount(Integer alertCount) {
        this.alertCount = alertCount;
    }
    
    public Integer getAlertSeverity() {
        return alertSeverity;
    }
    
    public void setAlertSeverity(Integer alertSeverity) {
        this.alertSeverity = alertSeverity;
    }
    
    public Integer getPayStationType() {
        return payStationType;
    }
    
    public void setPayStationType(Integer payStationType) {
        this.payStationType = payStationType;
    }
    
}
