package com.digitalpaytech.dto.customeradmin;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

public final class AlertCentrePayStationInfo {
    
    private String name;
    private String serial;
    private String lastSeen;
    private String location;
    private Integer payStationType;
    
    @XStreamImplicit(itemFieldName = "route")
    private List<String> route;
    
    @XStreamImplicit(itemFieldName = "activeAlerts")
    private List<AlertCentreAlertInfo> activeAlerts;
    
    @XStreamAlias("resolvedAlerts")
    private List<AlertCentreAlertInfo> resolvedAlerts;
    
    public AlertCentrePayStationInfo() {
        this.route = new ArrayList<String>();
        this.activeAlerts = new ArrayList<AlertCentreAlertInfo>();
        this.resolvedAlerts = new ArrayList<AlertCentreAlertInfo>();
    }
    
    public AlertCentrePayStationInfo(final String name, final String serial, final String lastSeen, final String location, final List<String> route,
            final List<AlertCentreAlertInfo> activeAlerts) {
        this.name = name;
        this.serial = serial;
        this.lastSeen = lastSeen;
        this.location = location;
        this.route = route;
        this.activeAlerts = activeAlerts;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getSerial() {
        return this.serial;
    }
    
    public void setSerial(final String serial) {
        this.serial = serial;
    }
    
    public String getLastSeen() {
        return this.lastSeen;
    }
    
    public void setLastSeen(final String lastSeen) {
        this.lastSeen = lastSeen;
    }
    
    public String getLocation() {
        return this.location;
    }
    
    public void setLocation(final String location) {
        this.location = location;
    }
    
    public List<String> getRoute() {
        return this.route;
    }
    
    public void setRoute(final List<String> route) {
        this.route = route;
    }
    
    public List<AlertCentreAlertInfo> getActiveAlerts() {
        return this.activeAlerts;
    }
    
    public void setActiveAlerts(final List<AlertCentreAlertInfo> activeAlerts) {
        this.activeAlerts = activeAlerts;
    }
    
    public Integer getPayStationType() {
        return this.payStationType;
    }
    
    public void setPayStationType(final Integer payStationType) {
        this.payStationType = payStationType;
    }

    public List<AlertCentreAlertInfo> getResolvedAlerts() {
        return resolvedAlerts;
    }

    public void setResolvedAlerts(List<AlertCentreAlertInfo> resolvedAlerts) {
        this.resolvedAlerts = resolvedAlerts;
    }
}
