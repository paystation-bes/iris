package com.digitalpaytech.dto.customeradmin;

import java.util.Collection;

import com.digitalpaytech.dto.RandomIdNameSetting;
import com.digitalpaytech.dto.WidgetMapInfo;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class RouteDetails {
	
	private String randomRouteId;
	private String name;
	private String randomTypeId;
	private String typeName;
	private WidgetMapInfo widgetMapInfo;
	
	@XStreamImplicit(itemFieldName="routeTypeList")
	private Collection<RandomIdNameSetting> routeTypeList;
	
	@XStreamImplicit(itemFieldName="locationPaystationList")
	private Collection<RouteInputFormInfo> locationPaystationList;
	
	@XStreamImplicit(itemFieldName="routePaystationList")
	private Collection<RouteInputFormInfo> routePaystationList;
	
	@XStreamImplicit(itemFieldName="allPaystationList")
	private Collection<PaystationListInfo> allPaystationList;
	
	public String getRandomRouteId() {
		return randomRouteId;
	}

	public void setRandomRouteId(String randomRouteId) {
		this.randomRouteId = randomRouteId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRandomTypeId() {
		return randomTypeId;
	}

	public void setRandomTypeId(String randomTypeId) {
		this.randomTypeId = randomTypeId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public WidgetMapInfo getWidgetMapInfo() {
		return widgetMapInfo;
	}

	public void setWidgetMapInfo(WidgetMapInfo widgetMapInfo) {
		this.widgetMapInfo = widgetMapInfo;
	}

	public Collection<RandomIdNameSetting> getRouteTypeList() {
		return routeTypeList;
	}

	public void setRouteTypeList(Collection<RandomIdNameSetting> routeTypeList) {
		this.routeTypeList = routeTypeList;
	}

	public Collection<RouteInputFormInfo> getLocationPaystationList() {
		return locationPaystationList;
	}

	public void setLocationPaystationList(
			Collection<RouteInputFormInfo> locationPaystationList) {
		this.locationPaystationList = locationPaystationList;
	}

	public Collection<RouteInputFormInfo> getRoutePaystationList() {
		return routePaystationList;
	}

	public void setRoutePaystationList(
			Collection<RouteInputFormInfo> routePaystationList) {
		this.routePaystationList = routePaystationList;
	}

	public Collection<PaystationListInfo> getAllPaystationList() {
		return allPaystationList;
	}

	public void setAllPaystationList(
			Collection<PaystationListInfo> allPaystationList) {
		this.allPaystationList = allPaystationList;
	}
}
