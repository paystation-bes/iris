package com.digitalpaytech.dto;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("reportFilterType")
public class ReportUIFilterType implements Serializable {
    
    private static final long serialVersionUID = -57676801347840301L;
    
    private int value;
    private String text;
    
    public ReportUIFilterType(final int value, final String text) {
        this.value = value;
        this.text = text;
    }
    
    public final int getValue() {
        return this.value;
    }
    
    public final void setValue(final int value) {
        this.value = value;
    }
    
    public final String getText() {
        return this.text;
    }
    
    public final void setText(final String text) {
        this.text = text;
    }
    
}
