package com.digitalpaytech.dto;

import com.digitalpaytech.util.crypto.CryptoConstants;

public enum HashAlgorithm {
    SHA1(CryptoConstants.SHA_1, 1),
    SHA256(CryptoConstants.SHA_256, 2),
    SHA256_Iris(CryptoConstants.SHA_256_IRIS, 5);    
    
    private String name;
    private int id;
    
    HashAlgorithm(final String name, final int id) {
        this.name = name;
        this.id = id;
    }
    
    public int getId() {
        return this.id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public static HashAlgorithm resolveById(final int id) {
        HashAlgorithm result = null;
        
        final HashAlgorithm[] all = values();
        int idx = all.length;
        while ((result == null) && (--idx >= 0)) {
            if (id == all[idx].getId()) {
                result = all[idx];
            }
        }
        
        if (result == null) {
            throw new IllegalArgumentException("Unknown HashAlgorithm's Id: " + id);
        }
        
        return result;
    }
}
