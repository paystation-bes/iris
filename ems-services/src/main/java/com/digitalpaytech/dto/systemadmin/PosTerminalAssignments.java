package com.digitalpaytech.dto.systemadmin;

import java.io.Serializable;
import java.util.UUID;

public final class PosTerminalAssignments implements Serializable {
    private static final long serialVersionUID = 1827892870908235607L;
    private Integer customerId;
    private String deviceSerialNumber;
    private String cardType;
    private UUID terminalToken;
    
    public Integer getCustomerId() {
        return this.customerId;
    }
    public void setCustomerId(final Integer customerId) {
        this.customerId = customerId;
    }
    public String getDeviceSerialNumber() {
        return this.deviceSerialNumber;
    }
    public void setDeviceSerialNumber(final String deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }
    public String getCardType() {
        return this.cardType;
    }
    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }
    public UUID getTerminalToken() {
        return this.terminalToken;
    }
    public void setTerminalToken(final UUID terminalToken) {
        this.terminalToken = terminalToken;
    }
}
