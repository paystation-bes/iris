package com.digitalpaytech.dto.systemadmin;

import com.digitalpaytech.util.crypto.CryptoConstants;

public class CryptoKeyInfo {
    private int keyIndex;
    private int keyType;
    
    private Integer cryptoKeyId;
    
    private String sha1;
    private String sha256;
    private String source;
    private int displayIndex;
    
    public CryptoKeyInfo() {
        
    }
    
    public int getKeyIndex() {
        return this.keyIndex;
    }
    
    public void setKeyIndex(final int keyIndex) {
        this.keyIndex = keyIndex;
    }
    
    public int getKeyType() {
        return this.keyType;
    }
    
    public void setKeyType(final int keyType) {
        this.keyType = keyType;
    }
    
    public Integer getCryptoKeyId() {
        return this.cryptoKeyId;
    }
    
    public void setCryptoKeyId(final Integer cryptoKeyId) {
        this.cryptoKeyId = cryptoKeyId;
    }
    
    public String getSha1() {
        return this.sha1;
    }
    
    public String getSha256() {
        return this.sha256;
    }
    
    public void setHash(final int hashAlgType, final String hash) {
        switch (hashAlgType) {
            case (int) CryptoConstants.HASH_ALGORITHM_TYPE_SHA1:
                this.sha1 = hash;
                break;
            case (int) CryptoConstants.HASH_ALGORITHM_TYPE_SHA256:
                this.sha256 = hash;
                break;
            default:
                throw new IllegalArgumentException("Unknown HashAlgorithmType: " + hashAlgType);
        }
    }
    
    public String getSource() {
        return this.source;
    }
    
    public void setSource(final String source) {
        this.source = source;
    }

    public int getDisplayIndex() {
        return this.displayIndex;
    }

    public void setDisplayIndex(final int displayIndex) {
        this.displayIndex = displayIndex;
    }
    
}
