package com.digitalpaytech.dto.systemadmin;

import java.util.ArrayList;
import java.util.List;

import com.digitalpaytech.dto.JsonKeyValuePair;
import com.digitalpaytech.dto.CardProcessorInfo;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * This class hold card processing queue status result to display in System Status page.
 * 
 * @author Brian Kim
 * 
 */
public class CardProcessingQueueStatus {
    
    @XStreamImplicit(itemFieldName = "cardRetryStatusEntries")
    private List<CardProcessorInfo> cardRetryStatusEntries;
    
    private int settlementQueueSize;
    private int storeForwardQueueSize;
    private int reversalQueueSize;
    
    public CardProcessingQueueStatus() {
        this.cardRetryStatusEntries = new ArrayList<CardProcessorInfo>();
    }
    
    public List<CardProcessorInfo> getCardRetryStatusEntries() {
        return cardRetryStatusEntries;
    }

    public void addCardRetryStatusEntries(CardProcessorInfo cardRetryStatusEntry) {
        this.cardRetryStatusEntries.add(cardRetryStatusEntry);
    }
    
    public void setCardRetryStatusEntries(List<CardProcessorInfo> cardRetryStatusEntries) {
    	this.cardRetryStatusEntries = cardRetryStatusEntries;
    }

    public int getSettlementQueueSize() {
        return settlementQueueSize;
    }
    
    public void setSettlementQueueSize(int settlementQueueSize) {
        this.settlementQueueSize = settlementQueueSize;
    }
    
    public int getStoreForwardQueueSize() {
        return storeForwardQueueSize;
    }
    
    public void setStoreForwardQueueSize(int storeForwardQueueSize) {
        this.storeForwardQueueSize = storeForwardQueueSize;
    }
    
    public int getReversalQueueSize() {
        return reversalQueueSize;
    }
    
    public void setReversalQueueSize(int reversalQueueSize) {
        this.reversalQueueSize = reversalQueueSize;
    }
    
}
