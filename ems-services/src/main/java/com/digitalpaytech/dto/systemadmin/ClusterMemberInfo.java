package com.digitalpaytech.dto.systemadmin;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias(value = "ClusterMemberInfo")
public class ClusterMemberInfo {
	private String name;
	private boolean isPrimary;

	public ClusterMemberInfo(String name, boolean isPrimary) {
		this.name = name;
		this.isPrimary = isPrimary;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isPrimary() {
		return isPrimary;
	}

	public void setIsPrimary(boolean isPrimary) {
		this.isPrimary = isPrimary;
	}
}
