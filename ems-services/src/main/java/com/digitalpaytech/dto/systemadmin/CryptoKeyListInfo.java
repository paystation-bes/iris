package com.digitalpaytech.dto.systemadmin;

import java.util.Collection;

public class CryptoKeyListInfo {
    
    private CryptoKeyInfo currentScalaKey;
    private CryptoKeyInfo futureScalaKey;
    private CryptoKeyInfo currentLinkKey;
    private CryptoKeyInfo futureLinkKey;
    private Collection<CryptoKeyInfo> previousKeys;
    
    public CryptoKeyInfo getCurrentScalaKey() {
        return this.currentScalaKey;
    }
    
    public void setCurrentScalaKey(final CryptoKeyInfo currentScalaKey) {
        this.currentScalaKey = currentScalaKey;
    }
    
    public CryptoKeyInfo getFutureScalaKey() {
        return this.futureScalaKey;
    }
    
    public void setFutureScalaKey(final CryptoKeyInfo futureScalaKey) {
        this.futureScalaKey = futureScalaKey;
    }
    
    public CryptoKeyInfo getCurrentLinkKey() {
        return this.currentLinkKey;
    }
    
    public void setCurrentLinkKey(final CryptoKeyInfo currentLinkKey) {
        this.currentLinkKey = currentLinkKey;
    }
    
    public CryptoKeyInfo getFutureLinkKey() {
        return this.futureLinkKey;
    }
    
    public void setFutureLinkKey(final CryptoKeyInfo futureLinkKey) {
        this.futureLinkKey = futureLinkKey;
    }
    
    public Collection<CryptoKeyInfo> getPreviousKeys() {
        return this.previousKeys;
    }
    
    public void setPreviousKeys(final Collection<CryptoKeyInfo> previousKeys) {
        this.previousKeys = previousKeys;
    }
    
}
