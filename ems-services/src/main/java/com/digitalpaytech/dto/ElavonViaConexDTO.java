package com.digitalpaytech.dto;

import java.util.Date;

import com.digitalpaytech.domain.ElavonRequestType;
import com.digitalpaytech.domain.ElavonTransactionDetail;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.TransactionSettlementStatusType;

public class ElavonViaConexDTO {
    private PreAuth preAuth;
    private ProcessorTransaction processorTransaction;
    private ElavonRequestType elavonRequestType;
    private String cardData;
    private TransactionSettlementStatusType transactionSettlementStatusType;
    private MerchantAccount merchantAccount;
    private PointOfSale pointOfSale;
    private ElavonTransactionDetail elavonTransactionDetail;
    
    public ElavonViaConexDTO(final PreAuth preAuth, 
                             final ProcessorTransaction processorTransaction, 
                             final ElavonRequestType elavonRequestType,
                             final String cardData,
                             final TransactionSettlementStatusType transactionSettlementStatusType) {
        this.preAuth = preAuth;
        this.processorTransaction = processorTransaction;
        this.elavonRequestType = elavonRequestType;
        this.cardData = cardData;
        this.transactionSettlementStatusType = transactionSettlementStatusType;
    }
    
    
    public ElavonViaConexDTO(final ElavonTransactionDetail elavonTransactionDetail,
                             final PreAuth preAuth, 
                             final ProcessorTransaction processorTransaction, 
                             final ElavonRequestType elavonRequestType,
                             final String cardData,
                             final TransactionSettlementStatusType transactionSettleMentStatusType) {
        this(preAuth, processorTransaction, elavonRequestType, cardData, transactionSettleMentStatusType);
        this.elavonTransactionDetail = elavonTransactionDetail;
    }    
    
    public final MerchantAccount getMerchantAccount() {
        if (this.preAuth != null) {
            return this.preAuth.getMerchantAccount();
        } else if (this.processorTransaction != null) {
            return this.processorTransaction.getMerchantAccount();
        }
        return this.merchantAccount;
    }

    public final PointOfSale getPointOfSale() {
        if (this.preAuth != null) {
            return this.preAuth.getPointOfSale();
        } else if (this.processorTransaction != null) {
            return this.processorTransaction.getPointOfSale();
        }
        return this.pointOfSale;
    }
    
    public final PreAuth getPreAuth() {
        return this.preAuth;
    }
    public final void setPreAuth(final PreAuth preAuth) {
        this.preAuth = preAuth;
    }
    public final ProcessorTransaction getProcessorTransaction() {
        return this.processorTransaction;
    }
    public final void setProcessorTransaction(final ProcessorTransaction processorTransaction) {
        this.processorTransaction = processorTransaction;
    }
    public final String getCardData() {
        return this.cardData;
    }
    public final void setCardData(final String cardData) {
        this.cardData = cardData;
    }
    public final ElavonRequestType getElavonRequestType() {
        return this.elavonRequestType;
    }
    public final void setElavonRequestType(final ElavonRequestType elavonRequestType) {
        this.elavonRequestType = elavonRequestType;
    }
    public final TransactionSettlementStatusType getTransactionSettlementStatusType() {
        return this.transactionSettlementStatusType;
    }
    public final void setTransactionSettlementStatusType(final TransactionSettlementStatusType transactionSettlementStatusType) {
        this.transactionSettlementStatusType = transactionSettlementStatusType;
    }
    public final void setMerchantAccount(final MerchantAccount merchantAccount) {
        this.merchantAccount = merchantAccount;
    }
    public final void setPointOfSale(final PointOfSale pointOfSale) {
        this.pointOfSale = pointOfSale;
    }
    public final ElavonTransactionDetail getElavonTransactionDetail() {
        return this.elavonTransactionDetail;
    }
    public void setElavonTransactionDetail(final ElavonTransactionDetail elavonTransactionDetail) {
        this.elavonTransactionDetail = elavonTransactionDetail;
    }


    public final Date getPurchasedDate() {
        if (this.processorTransaction != null) {
            return this.processorTransaction.getPurchasedDate();
        }        
        return null;
    }
    
    public final Integer getTicketNumber() {
        if (this.processorTransaction != null) {
            return this.processorTransaction.getTicketNumber();
        }
        
        return 0;
    }
}
