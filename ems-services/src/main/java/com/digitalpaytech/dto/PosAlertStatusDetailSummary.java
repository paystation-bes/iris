package com.digitalpaytech.dto;

import java.io.Serializable;

public class PosAlertStatusDetailSummary implements Serializable {
    
    private static final long serialVersionUID = 7868378339813813291L;
    
    private int totalMinor;
    private int totalMajor;
    private int totalCritical;
    private int severity;
    
    public final int getTotalMinor() {
        return this.totalMinor;
    }
    
    public final void setTotalMinor(final int totalMinor) {
        this.totalMinor = totalMinor;
    }
    
    public final int getTotalMajor() {
        return this.totalMajor;
    }
    
    public final void setTotalMajor(final int totalMajor) {
        this.totalMajor = totalMajor;
    }
    
    public final int getTotalCritical() {
        return this.totalCritical;
    }
    
    public final void setTotalCritical(final int totalCritical) {
        this.totalCritical = totalCritical;
    }
    
    public final int getSeverity() {
        return this.severity;
    }
    
    public final void setSeverity(final int severity) {
        this.severity = severity;
    }
    
}
