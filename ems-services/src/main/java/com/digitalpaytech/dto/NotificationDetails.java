package com.digitalpaytech.dto;

import java.sql.Timestamp;

public class NotificationDetails {
    
    private String title;
    private String message;
    private String messageUrl;
    private Timestamp beginGmt;
    private Timestamp endGmt;
    private String beginTime;
    private String endTime;
    
    public final String getTitle() {
        return this.title;
    }
    
    public final void setTitle(final String title) {
        this.title = title;
    }
    
    public final String getMessage() {
        return this.message;
    }
    
    public final void setMessage(final String message) {
        this.message = message;
    }
    
    public final String getMessageUrl() {
        return this.messageUrl;
    }
    
    public final void setMessageUrl(final String messageUrl) {
        this.messageUrl = messageUrl;
    }
    
    public final Timestamp getBeginGmt() {
        return this.beginGmt;
    }
    
    public final void setBeginGmt(final Timestamp beginGmt) {
        this.beginGmt = beginGmt;
    }
    
    public final Timestamp getEndGmt() {
        return this.endGmt;
    }
    
    public final void setEndGmt(final Timestamp endGmt) {
        this.endGmt = endGmt;
    }
    
    public final String getBeginTime() {
        return this.beginTime;
    }
    
    public final void setBeginTime(final String beginTime) {
        this.beginTime = beginTime;
    }
    
    public final String getEndTime() {
        return this.endTime;
    }
    
    public final void setEndTime(final String endTime) {
        this.endTime = endTime;
    }
}
