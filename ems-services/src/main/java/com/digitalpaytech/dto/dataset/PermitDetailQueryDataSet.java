package com.digitalpaytech.dto.dataset;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("QUERY_DATASET")
public class PermitDetailQueryDataSet implements Serializable {
    private static final long serialVersionUID = 2508698363220705251L;
    
    @XStreamImplicit(itemFieldName = "RECORD")
    private List<PermitDetailFlexRecord> permitDetailFlexRecords;
    
    public final List<PermitDetailFlexRecord> getPermitDetailFlexRecords() {
        return this.permitDetailFlexRecords;
    }
    
    public final void setPermitDetailFlexRecords(final List<PermitDetailFlexRecord> permitDetailFlexRecord) {
        this.permitDetailFlexRecords = permitDetailFlexRecord;
    }
    
    public static class PermitDetailFlexRecord implements Serializable {
        private static final long serialVersionUID = 2298190813520830009L;

        @XStreamAlias("PERMITID")
        private String permitId;
        
        @XStreamAlias("PERMITEFFECTIVESTARTINMINS")
        private Date permitEffectiveStartDate;
        
        @XStreamAlias("PERMITEFFECTIVEENDINMINS")
        private Date permitEffectiveEndDate;
        
        @XStreamAlias("PERMITNUMBER")
        private String permitNumber;
        
        @XStreamAlias("FACILITYNAME")
        private String facilityName;
        
        @XStreamAlias("ENTITYNAME")
        private String fullName;
        
        @XStreamAlias("LICENSEPLATE")
        private String vehiclePlateLicense;
        
        @XStreamAlias("AMOUNT")
        private String amount;
        
        @XStreamAlias("MINOFCREATEDATE")
        private Date creationDate;
        
        @XStreamAlias("PAYMENTTYPE")
        private String paymentType;
        
        @XStreamAlias("ISDEACTIVATED")
        private boolean isDeactivated;
        
        @XStreamAlias("ISDESTROYED")
        private boolean isDestroyed;
        
        @XStreamAlias("ISLOST")
        private boolean isLostStolen;
        
        @XStreamAlias("ISRETURNED")
        private boolean isReturned;
        
        @XStreamAlias("ISTERMINATED")
        private boolean isTerminated;
        
        public final String getPermitId() {
            return this.permitId;
        }
        
        public final void setPermitId(final String permitId) {
            this.permitId = permitId;
        }
        
        public final Date getPermitEffectiveStartDate() {
            return this.permitEffectiveStartDate;
        }
        
        public final void setPermitEffectiveStartDate(final Date permitEffectiveStartDate) {
            this.permitEffectiveStartDate = permitEffectiveStartDate;
        }
        
        public final Date getPermitEffectiveEndDate() {
            return this.permitEffectiveEndDate;
        }
        
        public final void setPermitEffectiveEndDate(final Date permitEffectiveEndDate) {
            this.permitEffectiveEndDate = permitEffectiveEndDate;
        }
        
        public final String getPermitNumber() {
            return this.permitNumber;
        }
        
        public final void setPermitNumber(final String permitNumber) {
            this.permitNumber = permitNumber;
        }
        
        public final String getFacilityName() {
            return this.facilityName;
        }
        
        public final void setFacilityName(final String facilityName) {
            this.facilityName = facilityName;
        }
        
        public final String getFullName() {
            return this.fullName;
        }
        
        public final void setFullName(final String fullName) {
            this.fullName = fullName;
        }
        
        public final String getVehiclePlateLicense() {
            return this.vehiclePlateLicense;
        }
        
        public final void setVehiclePlateLicense(final String vehiclePlateLicense) {
            this.vehiclePlateLicense = vehiclePlateLicense;
        }
        
        public final String getAmount() {
            return this.amount;
        }
        
        public final void setAmount(final String amount) {
            this.amount = amount;
        }
        
        public final Date getCreationDate() {
            return this.creationDate;
        }
        
        public final void setCreationDate(final Date creationDate) {
            this.creationDate = creationDate;
        }
        
        public final String getPaymentType() {
            return this.paymentType;
        }
        
        public final void setPaymentType(final String paymentType) {
            this.paymentType = paymentType;
        }
        
        public final boolean isDeactivated() {
            return this.isDeactivated;
        }
        
        public final void setIsDeactivated(final boolean isDeactivated) {
            this.isDeactivated = isDeactivated;
        }
        
        public final boolean isDestroyed() {
            return this.isDestroyed;
        }
        
        public final void setIsDestroyed(final boolean isDestroyed) {
            this.isDestroyed = isDestroyed;
        }
        
        public final boolean isLostStolen() {
            return this.isLostStolen;
        }
        
        public final void setIsLostStolen(final boolean isLostStolen) {
            this.isLostStolen = isLostStolen;
        }
        
        public final boolean isReturned() {
            return this.isReturned;
        }
        
        public final void setIsReturned(final boolean isReturned) {
            this.isReturned = isReturned;
        }
        
        public final boolean isTerminated() {
            return this.isTerminated;
        }
        
        public final void setIsTerminated(final boolean isTerminated) {
            this.isTerminated = isTerminated;
        }
    }
}
