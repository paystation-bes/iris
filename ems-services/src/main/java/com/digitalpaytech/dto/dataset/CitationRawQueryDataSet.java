package com.digitalpaytech.dto.dataset;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@XStreamAlias("QUERY_DATASET")
public class CitationRawQueryDataSet implements Serializable {
    private static final long serialVersionUID = 1791851086856709996L;
    
    @XStreamImplicit(itemFieldName = "RECORD")
    private List<CitationRawData> citationRawDataList;
    
    @XStreamOmitField
    private Integer callStatusId;
    
    public CitationRawQueryDataSet() {
        
    }
    
    public final void setCitationRawDataList(final List<CitationRawData> citationRawDataList) {
        this.citationRawDataList = citationRawDataList;
    }
    
    public final List<CitationRawData> getCitationRawDataList() {
        return this.citationRawDataList;
    }
    
    public final Integer getCallStatusId() {
        return this.callStatusId;
    }
    
    public final void setCallStatusId(final Integer callStatusId) {
        this.callStatusId = callStatusId;
    }
    
    public static class CitationRawData implements Serializable {
        private static final long serialVersionUID = -6500573035425775301L;
        
        @XStreamAlias("TICKETID")
        private String ticketId;
        
        @XStreamAlias("CITATIONID")
        private String citationId;
        
        @XStreamAlias("CITATIONTYPEID")
        private String citationType;
        
        @XStreamAlias("PROPERTYTYPEID")
        private String propertyType;
        
        @XStreamAlias("CITATIONISSUEDATE")
        private Date issueDate;
        
        @XStreamAlias("LONGITUDE")
        private String longitude;
        
        @XStreamAlias("LATITUDE")
        private String latitude;
        
        public final String getTicketId() {
            return this.ticketId;
        }
        
        public final void setTicketId(final String ticketId) {
            this.ticketId = ticketId;
        }
        
        public final String getCitationId() {
            return this.citationId;
        }
        
        public final void setCitationId(final String citationId) {
            this.citationId = citationId;
        }
        
        public final String getCitationType() {
            return this.citationType;
        }
        
        public final void setCitationType(final String citationType) {
            this.citationType = citationType;
        }
        
        public final String getPropertyType() {
            return this.propertyType;
        }
        
        public final void setPropertyType(final String propertyType) {
            this.propertyType = propertyType;
        }
        
        public final Date getIssueDate() {
            return this.issueDate;
        }
        
        public final void setIssueDate(final Date issueDate) {
            this.issueDate = issueDate;
        }
        
        public final String getLongitude() {
            return this.longitude;
        }
        
        public final void setLongitude(final String longitude) {
            this.longitude = longitude;
        }
        
        public final String getLatitude() {
            return this.latitude;
        }
        
        public final void setLatitude(final String latitude) {
            this.latitude = latitude;
        }
        
    }
    
}
