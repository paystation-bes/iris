package com.digitalpaytech.dto.dataset;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@XStreamAlias("QUERY_DATASET")
public class ContraventionTypeQueryDataSet implements Serializable {
    private static final long serialVersionUID = -8940859323734304117L;
    
    @XStreamImplicit(itemFieldName = "RECORD")
    private List<ViolationFlexRecord> violationFlexRecords;
    
    @XStreamOmitField
    private Integer callStatusId;
    
    public final List<ViolationFlexRecord> getViolationFlexRecords() {
        return this.violationFlexRecords;
    }
    
    public final void setViolationFlexRecords(final List<ViolationFlexRecord> violationFlexRecords) {
        this.violationFlexRecords = violationFlexRecords;
    }
    
    public final Integer getCallStatusId() {
        return this.callStatusId;
    }
    
    public final void setCallStatusId(final Integer callStatusId) {
        this.callStatusId = callStatusId;
    }
    
    public static class ViolationFlexRecord implements Serializable {
        private static final long serialVersionUID = -6156287535820278575L;
        
        @XStreamAlias("VIC_UID")
        private Integer violationUid;
        
        @XStreamAlias("VIC_DESCRIPTION")
        private String violationDescription;
        
        @XStreamAlias("VIC_IS_ACTIVE")
        private boolean active;
        
        @XStreamAlias("VIC_MODIFY_DATE")
        private Date modifyDate;
        
        public final Integer getViolationUid() {
            return this.violationUid;
        }
        
        public final void setViolationUid(final Integer violationUid) {
            this.violationUid = violationUid;
        }
        
        public final String getViolationDescription() {
            return this.violationDescription;
        }
        
        public final void setViolationDescription(final String violationDescription) {
            this.violationDescription = violationDescription;
        }
        
        public final boolean isActive() {
            return this.active;
        }
        
        public final void setActive(final boolean active) {
            this.active = active;
        }
        
        public final Date getModifyDate() {
            return this.modifyDate;
        }
        
        public final void setModifyDate(final Date modifyDate) {
            this.modifyDate = modifyDate;
        }
        
        //        public final ViolationDescription getViolationDescription() {
        //            return this.violationDescription;
        //        }
        //        
        //        public final void setViolationDescription(final ViolationDescription violationDescription) {
        //            this.violationDescription = violationDescription;
        //        }
        
    }
    
    //    public class ViolationDescription {
    //        @XStreamAlias("VIC_DESCRIPTION")
    //        private String violationDescription;
    //
    //        @XStreamAsAttribute
    //        @XStreamAlias("xml:space")
    //        private String space;
    //        
    //        public final String getViolationDescription() {
    //            return this.violationDescription;
    //        }
    //        
    //        public final void setViolationDescription(final String violationDescription) {
    //            this.violationDescription = violationDescription;
    //        }
    //        
    //        public final String getSpace() {
    //            return this.space;
    //        }
    //        
    //        public final void setSpace(final String space) {
    //            this.space = space;
    //        }
    //    }
}
