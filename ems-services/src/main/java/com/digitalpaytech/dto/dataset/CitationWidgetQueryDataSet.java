package com.digitalpaytech.dto.dataset;

import java.io.Serializable;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@XStreamAlias("QUERY_DATASET")
public class CitationWidgetQueryDataSet implements Serializable {
    private static final long serialVersionUID = 6649183376788494050L;
    
    @XStreamImplicit(itemFieldName = "RECORD")
    private List<CitationWidgetFlexRecord> citationWidgetFlexRecords;
    
    @XStreamOmitField
    private Integer callStatusId;
    
    public final List<CitationWidgetFlexRecord> getCitationWidgetFlexRecords() {
        return this.citationWidgetFlexRecords;
    }
    
    public final void setCitationWidgetFlexRecords(final List<CitationWidgetFlexRecord> citationWidgetFlexRecords) {
        this.citationWidgetFlexRecords = citationWidgetFlexRecords;
    }
    
    public final Integer getCallStatusId() {
        return this.callStatusId;
    }

    public final void setCallStatusId(final Integer callStatusId) {
        this.callStatusId = callStatusId;
    }

    public static class CitationWidgetFlexRecord implements Serializable {
        private static final long serialVersionUID = 6838069982336939195L;

        @XStreamAlias("COUNT")
        private Integer count;
        
        @XStreamAlias("CITATIONTYPEID")
        private Integer citationTypeId;
        
        public final Integer getCount() {
            return this.count;
        }
        
        public final void setCount(final Integer count) {
            this.count = count;
        }
        
        public final Integer getCitationTypeId() {
            return this.citationTypeId;
        }
        
        public final void setCitationTypeId(final Integer citationTypeId) {
            this.citationTypeId = citationTypeId;
        }
        
    }
}
