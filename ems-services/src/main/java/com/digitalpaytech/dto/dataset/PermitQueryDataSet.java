package com.digitalpaytech.dto.dataset;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("QUERY_DATASET")
public class PermitQueryDataSet implements Serializable {
    private static final long serialVersionUID = -2863028578847289959L;
    
    @XStreamImplicit(itemFieldName = "RECORD")
    private List<PermitFlexRecord> permitFlexRecords;
    
    public final List<PermitFlexRecord> getPermitFlexRecords() {
        return this.permitFlexRecords;
    }
    
    public final void setPermitFlexRecords(final List<PermitFlexRecord> flexRecords) {
        this.permitFlexRecords = flexRecords;
    }
    
    public static class PermitFlexRecord implements Serializable {
        private static final long serialVersionUID = -6576829688867603237L;

        @XStreamAlias("RNUMIRIS")
        private String rNnmIris;
        
        @XStreamAlias("PERMITID")
        private Integer permitId;
        
        @XStreamAlias("EFFECTIVESTARTDATE")
        private Date permitEffectiveStartDate;
        
        @XStreamAlias("EFFECTIVEENDDATE")
        private Date permitEffectiveEndDate;
        
        @XStreamAlias("PERMITNUMBER")
        private String permitNumber;
        
        @XStreamAlias("FACILITYNAME")
        private String facilityName;
        
        @XStreamAlias("LICENSEPLATE")
        private String vehiclePlateLicense;
        
        @XStreamAlias("ISACTIVE")
        private boolean isActive;
        
        @XStreamAlias("ISDEACTIVATED")
        private boolean isDeactivated;
        
        @XStreamAlias("ISDESTROYED")
        private boolean isDestroyed;
        
        @XStreamAlias("ISLOST")
        private boolean isLostStolen;
        
        @XStreamAlias("ISCUSTODY")
        private boolean isMisscust;
        
        @XStreamAlias("ISRETURNED")
        private boolean isReturned;
        
        @XStreamAlias("ISTERMINATED")
        private boolean isTerminated;
        
        @XStreamAlias("ENTITYNAME")
        private String accountName;
        
        @XStreamAlias("AMOUNT")
        private Float amount;
        
        @XStreamAlias("PAYMENTTYPE")
        private String paymentType;
        
        @XStreamAlias("MINOFCREATEDATE")
        private Date paymentDate;
        
        public final String getrNnmIris() {
            return this.rNnmIris;
        }
        
        public final void setrNnmIris(final String rNnmIris) {
            this.rNnmIris = rNnmIris;
        }
        
        public final Integer getPermitId() {
            return this.permitId;
        }
        
        public final void setPermitId(final Integer permitId) {
            this.permitId = permitId;
        }
        
        public final Date getPermitEffectiveStartDate() {
            return this.permitEffectiveStartDate;
        }
        
        public final void setPermitEffectiveStartDate(final Date permitEffectiveStartDate) {
            this.permitEffectiveStartDate = permitEffectiveStartDate;
        }
        
        public final Date getPermitEffectiveEndDate() {
            return this.permitEffectiveEndDate;
        }
        
        public final void setPermitEffectiveEndDate(final Date permitEffectiveEndDate) {
            this.permitEffectiveEndDate = permitEffectiveEndDate;
        }
        
        public final String getPermitNumber() {
            return this.permitNumber;
        }
        
        public final void setPermitNumber(final String permitNumber) {
            this.permitNumber = permitNumber;
        }
        
        public final String getFacilityName() {
            return this.facilityName;
        }
        
        public final void setFacilityName(final String facilityName) {
            this.facilityName = facilityName;
        }
        
        public final String getVehiclePlateLicense() {
            return this.vehiclePlateLicense;
        }
        
        public final void setVehiclePlateLicense(final String plateLicense) {
            this.vehiclePlateLicense = plateLicense;
        }
        
        public final boolean isActive() {
            return this.isActive;
        }

        public final void setActive(boolean isActive) {
            this.isActive = isActive;
        }

        public final boolean isDeactivated() {
            return this.isDeactivated;
        }
        
        public final void setIsDeactivated(final boolean isDeactivated) {
            this.isDeactivated = isDeactivated;
        }
        
        public final boolean isDestroyed() {
            return this.isDestroyed;
        }
        
        public final void setIsDestroyed(final boolean isDestroyed) {
            this.isDestroyed = isDestroyed;
        }
        
        public final boolean isLostStolen() {
            return this.isLostStolen;
        }
        
        public final void setIsLostStolen(final boolean isLostStolen) {
            this.isLostStolen = isLostStolen;
        }
        
        public final boolean isMisscust() {
            return this.isMisscust;
        }
        
        public final void setIsMisscust(final boolean isMisscust) {
            this.isMisscust = isMisscust;
        }
        
        public final boolean isReturned() {
            return this.isReturned;
        }
        
        public final void setIsReturned(final boolean isReturned) {
            this.isReturned = isReturned;
        }
        
        public final boolean isTerminated() {
            return this.isTerminated;
        }
        
        public final void setIsTerminated(final boolean isTerminated) {
            this.isTerminated = isTerminated;
        }

        public final String getAccountName() {
            return this.accountName;
        }

        public final void setAccountName(final String accountName) {
            this.accountName = accountName;
        }

        public final Float getAmount() {
            return this.amount;
        }

        public final void setAmount(final Float amount) {
            this.amount = amount;
        }

        public final String getPaymentType() {
            return this.paymentType;
        }

        public final void setPaymentType(final String paymentType) {
            this.paymentType = paymentType;
        }

        public final Date getPaymentDate() {
            return this.paymentDate;
        }

        public final void setPaymentDate(final Date paymentDate) {
            this.paymentDate = paymentDate;
        }
    }
}
