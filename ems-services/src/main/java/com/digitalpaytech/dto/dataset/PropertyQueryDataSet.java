package com.digitalpaytech.dto.dataset;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@XStreamAlias("QUERY_DATASET")
public class PropertyQueryDataSet implements Serializable {
    private static final long serialVersionUID = -2128442571593167308L;
    
    @XStreamImplicit(itemFieldName = "RECORD")
    private List<PropertyFlexRecord> propertyFlexRecords;
    
    @XStreamOmitField
    private Integer callStatusId;
    
    public final List<PropertyFlexRecord> getPropertyFlexRecords() {
        return this.propertyFlexRecords;
    }
    
    public final void setPropertyFlexRecords(final List<PropertyFlexRecord> flexRecords) {
        this.propertyFlexRecords = flexRecords;
    }
    
    public final Integer getCallStatusId() {
        return this.callStatusId;
    }

    public final void setCallStatusId(final Integer callStatusId) {
        this.callStatusId = callStatusId;
    }

    public static class PropertyFlexRecord implements Serializable {
        private static final long serialVersionUID = -2322038341476058152L;

        @XStreamAlias("PRO_UID")
        private Integer uid;
        
        @XStreamAlias("PRO_NAME")
        private String name;
        
        @XStreamAlias("PRO_CITY")
        private String city;
        
        @XStreamAlias("PRO_IS_ACTIVE")
        private boolean active;
        
        @XStreamAlias("PRO_MODIFY_DATE")
        private Date modifyDate;
                
        public final Integer getUid() {
            return this.uid;
        }

        public final void setUid(final Integer uid) {
            this.uid = uid;
        }

        public final String getName() {
            return this.name;
        }

        public final void setName(final String name) {
            this.name = name;
        }

        public final String getCity() {
            return this.city;
        }

        public final void setCity(final String city) {
            this.city = city;
        }

        public final boolean isActive() {
            return this.active;
        }

        public final void setActive(final boolean active) {
            this.active = active;
        }

        public final Date getModifyDate() {
            return this.modifyDate;
        }

        public final void setModifyDate(final Date modifyDate) {
            this.modifyDate = modifyDate;
        }
 
    }      
    
}
