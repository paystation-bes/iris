package com.digitalpaytech.dto.dataset;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@XStreamAlias("QUERY_DATASET")
public class FacilityQueryDataSet implements Serializable {
    private static final long serialVersionUID = 689456424030877764L;
    
    @XStreamImplicit(itemFieldName = "RECORD")
    private List<FacilityFlexRecord> facilityFlexRecords;
    
    @XStreamOmitField
    private Integer callStatusId;
    
    public final List<FacilityFlexRecord> getFacilityFlexRecords() {
        return this.facilityFlexRecords;
    }
    
    public final void setFacilityFlexRecords(final List<FacilityFlexRecord> flexRecords) {
        this.facilityFlexRecords = flexRecords;
    }
    
    public final Integer getCallStatusId() {
        return this.callStatusId;
    }
    
    public final void setCallStatusId(final Integer callStatusId) {
        this.callStatusId = callStatusId;
    }
    
    public static class FacilityFlexRecord implements Serializable {
        private static final long serialVersionUID = 7765312478973291269L;
        
        @XStreamAlias("FAC_UID")
        private Integer uid;
        
        @XStreamAlias("FAC_CODE")
        private String code;
        
        @XStreamAlias("FAC_DESCRIPTION")
        private String description;
        
        @XStreamAlias("FAC_IS_ACTIVE")
        private boolean active;
        
        @XStreamAlias("FAC_MODIFY_DATE")
        private Date modifyDate;
        
        public final Integer getUid() {
            return this.uid;
        }
        
        public final void setUid(final Integer uid) {
            this.uid = uid;
        }
        
        public final String getCode() {
            return this.code;
        }
        
        public final void setCode(final String code) {
            this.code = code;
        }
        
        public final String getDescription() {
            return this.description;
        }
        
        public final void setDescription(final String description) {
            this.description = description;
        }
        
        public final boolean isActive() {
            return this.active;
        }
        
        public final void setActive(final boolean active) {
            this.active = active;
        }
        
        public final Date getModifyDate() {
            return this.modifyDate;
        }
        
        public final void setModifyDate(final Date modifyDate) {
            this.modifyDate = modifyDate;
        }
        
    }
    
}
