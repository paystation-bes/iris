package com.digitalpaytech.dto.dataset;

import java.io.Serializable;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("QUERY_DATASET")
public class FlexInfoQueryDataSet implements Serializable {
    private static final long serialVersionUID = 4471493507620905363L;
    
    @XStreamImplicit(itemFieldName = "RECORD")
    private List<FlexInfo> flexInfoRecords;
    
    public FlexInfoQueryDataSet() {
        
    }
    
    public final List<FlexInfo> getFlexInfoRecords() {
        return this.flexInfoRecords;
    }

    public final void setFlexInfoRecords(final List<FlexInfo> flexInfoRecords) {
        this.flexInfoRecords = flexInfoRecords;
    }

    public static class FlexInfo implements Serializable {
        private static final long serialVersionUID = -6418837030520968706L;
        
        @XStreamAlias("TIMEZONE")
        private String timezone;
        
        public FlexInfo() {
            
        }

        public final String getTimezone() {
            return this.timezone;
        }

        public final void setTimezone(final String timezone) {
            this.timezone = timezone;
        }
    }
}
