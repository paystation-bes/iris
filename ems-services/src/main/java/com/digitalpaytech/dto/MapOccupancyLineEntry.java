package com.digitalpaytech.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class MapOccupancyLineEntry implements Serializable {
    
    private static final long serialVersionUID = 3199185975403896410L;
    
    private BigDecimal latitude;
    private BigDecimal longitude;
    
    public MapOccupancyLineEntry(final BigDecimal latitude, final BigDecimal longitude) {
        super();
        this.latitude = latitude;
        this.longitude = longitude;
    }
    
    public final BigDecimal getLatitude() {
        return this.latitude;
    }
    
    public final void setLatitude(final BigDecimal latitude) {
        this.latitude = latitude;
    }
    
    public final BigDecimal getLongitude() {
        return this.longitude;
    }
    
    public final void setLongitude(final BigDecimal longitude) {
        this.longitude = longitude;
    }
    
}
