package com.digitalpaytech.dto;

import java.io.Serializable;

public class JsonResponse<T> implements Serializable {
    
    private static final long serialVersionUID = -8148978125657005153L;
    
    private boolean success;
    private String message;
    private T data;
    
    public JsonResponse() {
        
    }
    
    public JsonResponse(final boolean success) {
        this.success = success;
    }
    
    public JsonResponse(final boolean success, final String message) {
        this.success = success;
        this.message = message;
    }
    
    public final boolean isSuccess() {
        return this.success;
    }
    
    public final void setSuccess(final boolean success) {
        this.success = success;
    }
    
    public final String getMessage() {
        return this.message;
    }
    
    public final void setMessage(final String message) {
        this.message = message;
    }
    
    public final T getData() {
        return this.data;
    }
    
    public final void setData(final T data) {
        this.data = data;
    }
}
