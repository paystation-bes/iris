package com.digitalpaytech.dto;

import java.util.Date;

public class NotificationListInfo {
    
    private String randomId;
    private String title;
    private Date beginGmt;
    
    public NotificationListInfo() {
        
    }
    
    public NotificationListInfo(final String randomId, final String title, final Date beginGmt) {
        this.randomId = randomId;
        this.title = title;
        this.beginGmt = beginGmt;
    }
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final String getTitle() {
        return this.title;
    }
    
    public final void setTitle(final String title) {
        this.title = title;
    }
    
    public final Date getBeginGmt() {
        return this.beginGmt;
    }
    
    public final void setBeginGmt(final Date beginGmt) {
        this.beginGmt = beginGmt;
    }
}
