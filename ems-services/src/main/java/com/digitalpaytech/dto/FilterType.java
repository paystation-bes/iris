package com.digitalpaytech.dto;

import com.digitalpaytech.util.WebCoreConstants;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("filterType")
public class FilterType extends SettingsType {
    private static final long serialVersionUID = 7929216270081710214L;
    
    public FilterType() {
        setStatus(WebCoreConstants.ACTIVE);
    }
}
