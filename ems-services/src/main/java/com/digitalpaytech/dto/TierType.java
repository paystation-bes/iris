package com.digitalpaytech.dto;

import com.digitalpaytech.util.WebCoreConstants;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("tierType")
public class TierType extends SettingsType implements Cloneable {
    
    private static final long serialVersionUID = -248577462380969383L;
    
    public TierType() {
        setStatus(WebCoreConstants.ACTIVE);
    }
    
    @Override
    public final Object clone() throws CloneNotSupportedException {
        final TierType tierType = (TierType) super.clone();
        return tierType;
    }
}
