package com.digitalpaytech.dto;

import java.io.Serializable;

public class AlertsCount implements Serializable {
    
    private static final long serialVersionUID = 3302111572602555593L;
    
    private byte minor;
    private byte major;
    private byte critical;
    
    public AlertsCount() {
        
    }
    
    public final byte getMinor() {
        return this.minor;
    }
    
    public final void setMinor(final byte minor) {
        this.minor = minor;
    }
    
    public final byte getMajor() {
        return this.major;
    }
    
    public final void setMajor(final byte major) {
        this.major = major;
    }
    
    public final byte getCritical() {
        return this.critical;
    }
    
    public final void setCritical(final byte critical) {
        this.critical = critical;
    }
}
