package com.digitalpaytech.dto;

import com.digitalpaytech.util.WebCoreConstants;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("rangeType")
public class RangeType extends SettingsType {
    private static final long serialVersionUID = -4651898057990712543L;
    
    public RangeType() {
        setStatus(WebCoreConstants.ACTIVE);
    }
}
