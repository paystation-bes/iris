package com.digitalpaytech.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.digitalpaytech.util.WebCoreConstants;

@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class WidgetMetricInfo implements Serializable {
    
    private static final long serialVersionUID = -9126843854923291322L;
    
    private int customerId;
    private Integer tier1TypeId;
    private Integer tier2TypeId;
    private Integer tier3TypeId;
    private String tier1TypeName;
    private String tier2TypeName;
    private String tier3TypeName;
    private boolean tier1TypeHidden;
    private boolean tier2TypeHidden;
    private boolean tier3TypeHidden;
    private Date startTime;
    private BigDecimal widgetMetricValue;
    private String jsonString;
    
    public WidgetMetricInfo() {
        super();
    }
    
    public WidgetMetricInfo(final int customerId, final int tier1TypeId, final int tier2TypeId, final int tier3TypeId, final String tier1TypeName,
            final String tier2TypeName, final String tier3TypeName, final BigDecimal widgetMetricValue, final Date startTime, final String jsonString) {
        this.customerId = customerId;
        this.tier1TypeId = tier1TypeId;
        this.tier2TypeId = tier2TypeId;
        this.tier3TypeId = tier3TypeId;
        this.tier1TypeName = tier1TypeName;
        this.tier2TypeName = tier2TypeName;
        this.tier3TypeName = tier3TypeName;
        this.startTime = startTime;
        this.widgetMetricValue = widgetMetricValue;
        this.jsonString = jsonString;
    }
    
    public final int getCustomerId() {
        return this.customerId;
    }
    
    public final void setCustomerId(final int customerId) {
        this.customerId = customerId;
    }
    
    public final Integer getTier1TypeId() {
        return this.tier1TypeId;
    }
    
    public final void setTier1TypeId(final Object tier1TypeId) {
        
        if (tier1TypeId instanceof Integer) {
            this.tier1TypeId = Integer.parseInt(tier1TypeId.toString());
        } else if (tier1TypeId instanceof java.sql.Date) {
            final DateFormat df = new SimpleDateFormat("dd", WebCoreConstants.DEFAULT_LOCALE);
            this.tier1TypeId = Integer.parseInt(df.format(tier1TypeId));
        }
    }
    
    public final Integer getTier2TypeId() {
        return this.tier2TypeId;
    }
    
    public final void setTier2TypeId(final Integer tier2TypeId) {
        this.tier2TypeId = tier2TypeId;
    }
    
    public final Integer getTier3TypeId() {
        return this.tier3TypeId;
    }
    
    public final void setTier3TypeId(final Integer tier3TypeId) {
        this.tier3TypeId = tier3TypeId;
    }
    
    public final String getTier1TypeName() {
        return this.tier1TypeName;
    }
    
    public final void setTier1TypeName(final Object tier1TypeName) {
        
        if (tier1TypeName instanceof java.sql.Date) {
            final DateFormat df = new SimpleDateFormat("MM/dd/yyyy", WebCoreConstants.DEFAULT_LOCALE);
            this.tier1TypeName = df.format(tier1TypeName);
        } else if (tier1TypeName instanceof String) {
            this.tier1TypeName = tier1TypeName.toString();
        } else if (tier1TypeName instanceof Integer) {
            this.tier1TypeName = tier1TypeName.toString();
        } else if (tier1TypeName instanceof Byte) {
            this.tier1TypeName = tier1TypeName.toString();
        }
    }
    
    public final String getTier2TypeName() {
        return this.tier2TypeName;
    }
    
    public final void setTier2TypeName(final String tier2TypeName) {
        this.tier2TypeName = tier2TypeName;
    }
    
    public final String getTier3TypeName() {
        return this.tier3TypeName;
    }
    
    public final void setTier3TypeName(final String tier3TypeName) {
        this.tier3TypeName = tier3TypeName;
    }
    
    public final Date getStartTime() {
        return this.startTime;
    }
    
    public final void setStartTime(final Date startTime) {
        this.startTime = startTime;
    }
    
    public final BigDecimal getWidgetMetricValue() {
        return this.widgetMetricValue;
    }
    
    public final void setWidgetMetricValue(final BigDecimal widgetMetricValue) {
        this.widgetMetricValue = widgetMetricValue;
    }
    
    public final boolean isTier1TypeHidden() {
        return this.tier1TypeHidden;
    }
    
    public final void setTier1TypeHidden(final boolean tier1TypeHidden) {
        this.tier1TypeHidden = tier1TypeHidden;
    }
    
    public final boolean isTier2TypeHidden() {
        return this.tier2TypeHidden;
    }
    
    public final void setTier2TypeHidden(final boolean tier2TypeHidden) {
        this.tier2TypeHidden = tier2TypeHidden;
    }
    
    public final boolean isTier3TypeHidden() {
        return this.tier3TypeHidden;
    }
    
    public final void setTier3TypeHidden(final boolean tier3TypeHidden) {
        this.tier3TypeHidden = tier3TypeHidden;
    }
    
    public final String getJsonString() {
        return this.jsonString;
    }
    
    public final void setJsonString(final String jsonString) {
        this.jsonString = jsonString;
    }
}
