package com.digitalpaytech.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.hibernate.transform.AliasToBeanResultTransformer;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.support.WebObjectId;

public class MapEntryDetailsTransformer extends AliasToBeanResultTransformer {
    
    private static final long serialVersionUID = 7129757682993667981L;
    
    private Map<String, Integer> aliasIdxMap;
    private TimeZone timeZone;
    
    public MapEntryDetailsTransformer(final TimeZone timeZone) {
        super(MapEntry.class);
        this.timeZone = timeZone;
    }
    
    @Override
    public final Object transformTuple(final Object[] tuple, final String[] aliases) {
        resolveIdx(tuple, aliases);
        
        final MapEntry result = new MapEntry();
        
        // Read General Info.
        final Integer posId = (Integer) tuple[this.aliasIdxMap.get("psId")];
        result.setRandomId(new WebObjectId(PointOfSale.class, posId));
        result.setName((String) tuple[this.aliasIdxMap.get("psName")]);
        result.setSerialNumber((String) tuple[this.aliasIdxMap.get("psSerialNumber")]);
        result.setPayStationType((Integer) tuple[this.aliasIdxMap.get("psType")]);
        result.setLocationName((String) tuple[this.aliasIdxMap.get("locationName")]);
        result.setLatitude((BigDecimal) tuple[this.aliasIdxMap.get("latitude")]);
        result.setLongitude((BigDecimal) tuple[this.aliasIdxMap.get("longitude")]);
        
        // Read Hardware Info.
        final Date dateBuffer = (Date) tuple[this.aliasIdxMap.get("lastSeen")];
        result.setLastSeen((dateBuffer == null) ? WebCoreConstants.UNKNOWN : DateUtil.getRelativeTimeString(dateBuffer, this.timeZone));
        
        Object buffer = tuple[this.aliasIdxMap.get("batteryVoltage")];
        result.setBatteryVoltage((buffer == null) ? "0" : buffer + " V");
        
        buffer = tuple[this.aliasIdxMap.get("batteryLevel")];
        result.setBatteryStatus((buffer == null) ? WebCoreConstants.LEVEL_NORMAL : ((Number) buffer).intValue());
        switch (result.getBatteryStatus()) {
            case WebCoreConstants.LEVEL_EMPTY:
                result.setBatteryStatus(WebCoreConstants.SEVERITY_CRITICAL);
                break;
            case WebCoreConstants.LEVEL_LOW:
                result.setBatteryStatus(WebCoreConstants.SEVERITY_MAJOR);
                break;
            default:
                result.setBatteryStatus(WebCoreConstants.SEVERITY_CLEAR);
                break;
        }
        
        buffer = tuple[this.aliasIdxMap.get("paperLevel")];
        result.setPaperStatus((buffer == null) ? WebCoreConstants.LEVEL_NORMAL : ((Number) buffer).intValue());
        switch (result.getPaperStatus()) {
            case WebCoreConstants.LEVEL_EMPTY:
                result.setPaperStatus(WebCoreConstants.SEVERITY_CRITICAL);
                break;
            case WebCoreConstants.LEVEL_LOW:
                result.setPaperStatus(WebCoreConstants.SEVERITY_MAJOR);
                break;
            default:
                result.setPaperStatus(WebCoreConstants.SEVERITY_CLEAR);
                break;
        }
        
        // Read Running Total.
        final RunningTotalInfo runningTotal = new RunningTotalInfo();
        result.setRunningTotal(runningTotal);
        
        Number numBuffer = (Number) tuple[this.aliasIdxMap.get("unsettledCardAmount")];
        final int cardAmount = (numBuffer == null) ? 0 : numBuffer.intValue();
        runningTotal.setCardAmount(WebCoreUtil.formatBase100Value(cardAmount));
        
        numBuffer = (Number) tuple[this.aliasIdxMap.get("cashAmount")];
        final int cashAmount = (numBuffer == null) ? 0 : numBuffer.intValue();
        runningTotal.setCashAmount(WebCoreUtil.formatBase100Value(cashAmount));
        
        numBuffer = (Number) tuple[this.aliasIdxMap.get("billAmount")];
        final int billAmount = (numBuffer == null) ? 0 : numBuffer.intValue();
        runningTotal.setBillAmount(WebCoreUtil.formatBase100Value(billAmount));
        
        numBuffer = (Number) tuple[this.aliasIdxMap.get("coinAmount")];
        final int coinAmount = (numBuffer == null) ? 0 : numBuffer.intValue();
        runningTotal.setCoinAmount(WebCoreUtil.formatBase100Value(coinAmount));
        
        runningTotal.setRunningTotal(WebCoreUtil.formatBase100Value(cashAmount + cardAmount));
        runningTotal.setIsLegacyCash((Boolean) tuple[this.aliasIdxMap.get("isLegacyCash")]);
        
        result.setSeverity((Integer) tuple[this.aliasIdxMap.get("alertSeverity")]);
        result.setAlertsCount((Byte) tuple[this.aliasIdxMap.get("alertCount")]);
        
        result.setAlertIcon(Math.max(result.getSeverity(), result.getPaperStatus()));
        
        return result;
    }
    
    private void resolveIdx(final Object[] tuple, final String[] aliases) {
        if (this.aliasIdxMap == null) {
            int idx = aliases.length;
            this.aliasIdxMap = new HashMap<String, Integer>(idx * 2, 0.75F);
            while (--idx >= 0) {
                this.aliasIdxMap.put(aliases[idx], idx);
            }
        }
    }
}
