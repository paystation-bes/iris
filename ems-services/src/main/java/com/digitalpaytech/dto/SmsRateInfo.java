package com.digitalpaytech.dto;

import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import com.digitalpaytech.domain.ExtensibleRate;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;

public class SmsRateInfo {
    private static final Logger LOGGER = Logger.getLogger(SmsRateInfo.class);
    
    private ExtensibleRate emsRate1;
    private ExtensibleRate emsRate2;
    private int maxAllowedTimeByPermission;
    private Date expiryDateInGMT;
    private boolean isMergedRate;
    private boolean isRate2Need = true;
    private boolean hasPermission1;
    private int maxExtendedTimeUnderRate1;
    // variable to show the max extension minute in req 4010
    private int maxExtensionTimeForFreeParking;
    private boolean isSecondPermissionNoParking;
    private boolean isNoParkingLongerThan24H;
    private String noParkingLocalEndTimeDisplay;
    private int freeParkingTime;
    private Date freeParkingEndDate;
    private int isTimeLimited;
    
    /**
     * @param extendedTimeinMinutes
     * @return true if rate1 is not null, AND input time is within allowed max time, AND input time is not
     *         smaller than minimum extend time of rate1
     */
    public final boolean isValidTime(final int extendedTimeinMinutes) {
        final int maxAllowedTimeByRate = getMaxAllowedTimeByRate();
        int minAllowedTime = 0;
        
        if (this.emsRate1 != null) {
            if (isSpecialExtentionForNextFreeParking()) {
                minAllowedTime = getMaxExtendedTimeUnderRate1();
            } else if (isSpecialExtentionForNextNoParking() || isSpecialExtentionForSingleRate()) {
                minAllowedTime = getMaxAllowedParkingTimeInMinutes();
            } else {
                minAllowedTime = this.emsRate1.getMinExtensionMinutes();
            }
        } else {
            if (this.emsRate2 != null) {
                minAllowedTime = this.emsRate2.getMinExtensionMinutes();
            }
        }
        
        return (this.emsRate1 != null && this.maxAllowedTimeByPermission >= extendedTimeinMinutes && extendedTimeinMinutes >= minAllowedTime && maxAllowedTimeByRate >= extendedTimeinMinutes)
               || (this.emsRate1 == null && this.emsRate2 != null && this.maxAllowedTimeByPermission >= extendedTimeinMinutes
                   && extendedTimeinMinutes >= minAllowedTime && maxAllowedTimeByRate >= extendedTimeinMinutes);
    }
    
    /**
     * return true if rate1 is null and allowed to park by permission
     */
    public final boolean isFreeParking() {
        return this.hasPermission1 && this.emsRate1 == null;
    }
    
    public final boolean isSecondRateFreeParking(final Date expiryDateLocal) {
        LOGGER.debug("++++ expiryDateLocal: " + expiryDateLocal);
        final Date newExpiryDate = getRoundDownDate(expiryDateLocal);
        
        return (this.emsRate2 == null)
               && (this.emsRate1 != null && this.maxAllowedTimeByPermission > (int) (this.emsRate1.getEndDateLocal().getTime() - newExpiryDate
                       .getTime()) / 60000);
    }
    
    public final boolean hasFirstRateFreeParking(final Date expiryDateLocal) {
        final Date newExpiryDate = getRoundDownDate(expiryDateLocal);
        final Date newExpiryDateInGMT = getRoundDownDate(this.expiryDateInGMT);
        this.maxExtendedTimeUnderRate1 = (int) (this.emsRate1.getEndDateGMT().getTime() - newExpiryDateInGMT.getTime()) / 60000;
        return (this.emsRate2 == null)
               && (this.emsRate1 != null && (this.maxExtendedTimeUnderRate1 > this.emsRate1.getMinExtensionMinutes()) && this.maxAllowedTimeByPermission > (int) (this.emsRate1
                       .getEndDateLocal().getTime() - newExpiryDate.getTime()) / 60000);
    }
    
    /**
     * return how long park can stay for free. all by permission if no rate2 or until start of rate2
     */
    public final int getAutoExtensionTimeInMinutes(final String customerTimeZone) {
        final Date newExpiryDateInGMT = getRoundDownDate(this.expiryDateInGMT);
        
        if (this.emsRate2 != null) {
            final Date startGMT = DateUtil.getDatetimeInGMT(this.emsRate2.getStartDateLocal(), customerTimeZone);
            return (int) (startGMT.getTime() - newExpiryDateInGMT.getTime()) / 60000;
        } else {
            return this.maxAllowedTimeByPermission;
        }
    }
    
    /**
     * return true if is free parking or max allowed time by permission is bigger than rate1 minimum required
     * time.
     */
    public final boolean isExtendable() {
        return this.isSpecialExtentionForNextFreeParking() || this.isSpecialExtentionForNextNoParking() || this.isSpecialExtentionForSingleRate()
               || (this.maxAllowedTimeByPermission > 0 && this.emsRate1 == null)
               || (this.emsRate1 != null && this.emsRate1.getMinExtensionMinutes() < this.maxAllowedTimeByPermission);
    }
    
    /**
     * return rate1 minimum extension time in minutes
     */
    public final int getMinAllowedParkingTimeInMinutes() {
        if (this.emsRate1 != null) {
            return this.emsRate1.getMinExtensionMinutes();
        } else {
            return 0;
        }
    }
    
    /**
     * return maximum allowed time based on both permission and rate
     */
    public final int getMaxAllowedParkingTimeInMinutes() {
        final int maxAllowedTimeByRate = getMaxAllowedTimeByRate();
        
        if (maxAllowedTimeByRate > this.maxAllowedTimeByPermission) {
            if (this.emsRate1 != null && this.emsRate2 == null) {
                return maxAllowedTimeByRate;
            }
            return this.maxAllowedTimeByPermission;
        } else {
            return maxAllowedTimeByRate;
        }
    }
    
    private int getMaxAllowedTimeByRate() {
        // set to max as default
        int maxAllowedTimeByRate = WebCoreConstants.DEFAULT_MAX_EXTENDABLE_TIME_IN_MINUTES;
        
        final Date newExpiryDateInGMT = getRoundDownDate(this.expiryDateInGMT);
        
        if (this.emsRate1 != null) {
            maxAllowedTimeByRate = (int) (this.emsRate1.getEndDateGMT().getTime() - newExpiryDateInGMT.getTime()) / 60000;
            if (this.emsRate2 != null) {
                maxAllowedTimeByRate = (int) (this.emsRate2.getEndDateGMT().getTime() - newExpiryDateInGMT.getTime()) / 60000;
            } else {
                if (this.isTimeLimited == 1) { // 1 = time limited in parkingpermissiontype table                    
                    maxAllowedTimeByRate = (int) (new Date(this.expiryDateInGMT.getTime() + (this.maxAllowedTimeByPermission * 1000 * 60)).getTime() - this.expiryDateInGMT
                            .getTime()) / 60000;
                } else {
                    maxAllowedTimeByRate = maxAllowedTimeByRate + this.freeParkingTime;
                }
            }
        } else {
            if (this.emsRate2 != null) {
                maxAllowedTimeByRate = (int) (this.emsRate2.getEndDateGMT().getTime() - newExpiryDateInGMT.getTime()) / 60000;
            } else {
                maxAllowedTimeByRate = (int) (new Date(this.expiryDateInGMT.getTime() + (this.maxAllowedTimeByPermission * 1000 * 60)).getTime() - this.expiryDateInGMT
                        .getTime()) / 60000;
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("++++ MaxAllowedTimeByRate: " + maxAllowedTimeByRate);
        }
        return maxAllowedTimeByRate;
    }
    
    /**
     * Get charge total in cents
     * 
     * @param extendTime
     * @param expiryDateGMT
     * @return
     */
    public final int getTotalChargeCent(final int extendTime) {
        if (this.emsRate1 == null) {
            return 0;
        }
        
        float total = (float) (extendTime * this.emsRate1.getRateAmount() / 60.0 + this.emsRate1.getServiceFeeAmount());
        LOGGER.debug("+++ charing in Rate1(in cents): " + total);
        final Date newExpiryDateInGMT = getRoundDownDate(this.expiryDateInGMT);
        
        final int rate1AllowedMinutes = (int) (this.emsRate1.getEndDateGMT().getTime() - newExpiryDateInGMT.getTime()) / 60000;
        LOGGER.debug("+++ rate1AllowedMinutes: " + rate1AllowedMinutes);
        if (extendTime > rate1AllowedMinutes) {
            if (this.emsRate2 != null) {
                final float total1 = (float) (rate1AllowedMinutes * this.emsRate1.getRateAmount() / 60.0 + this.emsRate1.getServiceFeeAmount());
                LOGGER.debug("+++ recalculated charging in Rate1(in cents): " + total1);
                final float total2 = (float) ((extendTime - rate1AllowedMinutes) * this.emsRate2.getRateAmount() / 60.0);
                LOGGER.debug("+++ calculated charging in Rate2(in cents): " + total2);
                total = total1 + total2;
            } else {
                total = (float) (rate1AllowedMinutes * this.emsRate1.getRateAmount() / 60.0 + this.emsRate1.getServiceFeeAmount());
                LOGGER.debug("+++ recalculated charging in Rate1(in cents): " + total);
            }
        }
        LOGGER.info("+++ totalCharge(in cents): " + total);
        LOGGER.info("+++ Math.round(totalCharge)(in cents): " + Math.round(total));
        return Math.round(total);
    }
    
    public final ExtensibleRate getEmsRate1() {
        return this.emsRate1;
    }
    
    public final void setEmsRate1(final ExtensibleRate emsRate1) {
        this.emsRate1 = emsRate1;
    }
    
    public final ExtensibleRate getEmsRate2() {
        return this.emsRate2;
    }
    
    public final void setEmsRate2(final ExtensibleRate emsRate2) {
        this.emsRate2 = emsRate2;
    }
    
    public final int getMaxAllowedTimeByPermission() {
        return this.maxAllowedTimeByPermission;
    }
    
    public final void setMaxAllowedTimeByPermission(final int maxAllowedTimeByPermission) {
        this.maxAllowedTimeByPermission = maxAllowedTimeByPermission;
    }
    
    public final Date getExpiryDateGMT() {
        return this.expiryDateInGMT;
    }
    
    public final void setExpiryDateGMT(final Date expiryDateGMT) {
        this.expiryDateInGMT = expiryDateGMT;
    }
    
    public final void setFreeParkingTime(final int freeParkingTime) {
        this.freeParkingTime = freeParkingTime;
    }
    
    public final void setFreeParkingEndDate(final Date freeParkingEndDate) {
        this.freeParkingEndDate = freeParkingEndDate;
    }
    
    public final Date getFreeParkingEndDate() {
        return this.freeParkingEndDate;
    }
    
    public final void setIsTimeLimited(final int isTimeLimited) {
        this.isTimeLimited = isTimeLimited;
    }
    
    public final void setIsMergedRate(final boolean isMergedRate) {
        this.isMergedRate = isMergedRate;
    }
    
    public final boolean getIsMergedRate() {
        return this.isMergedRate;
    }
    
    public final void setIsRate2Need(final boolean isRate2Need) {
        this.isRate2Need = isRate2Need;
    }
    
    public final boolean getIsRate2Need() {
        return this.isRate2Need && !this.isMergedRate;
    }
    
    /**
     * It only happens on the following special case: <br/>
     * 
     * req: 4011 <br/>
     * 
     * single value + free parking
     * 
     * @return
     */
    public final boolean isSpecialExtentionForNextFreeParking() {
        boolean isSpecialExtention = false;
        if (this.emsRate1 != null && this.emsRate2 == null && !this.getIsSecondPermissionNoParking()) {
            final Date newExpiryDateInGMT = getRoundDownDate(this.expiryDateInGMT);
            
            isSpecialExtention = this.hasPermission1 && this.maxAllowedTimeByPermission > 0;
            this.maxExtendedTimeUnderRate1 = (int) (this.emsRate1.getEndDateGMT().getTime() - newExpiryDateInGMT.getTime()) / 60000;
            isSpecialExtention = isSpecialExtention && (this.maxAllowedTimeByPermission > getMaxExtendedTimeUnderRate1());
            isSpecialExtention = isSpecialExtention && (this.maxExtendedTimeUnderRate1 <= this.emsRate1.getMinExtensionMinutes());
            LOGGER.debug("+++ maxExtendedTimeUnderRate1: " + this.maxExtendedTimeUnderRate1);
            LOGGER.debug("+++ emsRate1.getMinExtensionMinutes(): " + this.emsRate1.getMinExtensionMinutes());
            LOGGER.debug("+++ isSpecialExtention: " + isSpecialExtention);
        }
        return isSpecialExtention;
    }
    
    /**
     * It only happens on the following special case: <br/>
     * 
     * req: 4011 <br/>
     * 
     * single value + 2nd NO PARKING <br/>
     * 
     * @return
     */
    public final boolean isSpecialExtentionForNextNoParking() {
        boolean isSpecialExtention = false;
        if (this.emsRate1 != null) {
            final Date newExpiryDateInGMT = getRoundDownDate(this.expiryDateInGMT);
            this.maxExtendedTimeUnderRate1 = (int) (this.emsRate1.getEndDateGMT().getTime() - newExpiryDateInGMT.getTime()) / 60000;
            
            if ((this.emsRate1.getMinExtensionMinutes() >= this.getMaxAllowedParkingTimeInMinutes()) && this.getIsSecondPermissionNoParking()
                && this.maxAllowedTimeByPermission > 0) {
                isSpecialExtention = true;
            }
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("+++ getMaxAllowedParkingTimeInMinutes(): " + getMaxAllowedParkingTimeInMinutes());
                LOGGER.debug("+++ emsRate1.getMinExtensionMinutes(): " + this.emsRate1.getMinExtensionMinutes());
                LOGGER.debug("+++ getIsSecondPermissionNoParking(): " + getIsSecondPermissionNoParking());
            }
        }
        return isSpecialExtention;
    }
    
    /**
     * It only happens on the following special case: <br/>
     * 
     * req: 4011 <br/>
     * 
     * single value + 1 rates
     * 
     * @return
     */
    public final boolean isSpecialExtentionForSingleRate() {
        boolean isSpecialExtention = false;
        if (this.emsRate1 != null) {
            final Date newExpiryDateInGMT = getRoundDownDate(this.expiryDateInGMT);
            this.maxExtendedTimeUnderRate1 = (int) (this.emsRate1.getEndDateGMT().getTime() - newExpiryDateInGMT.getTime()) / 60000;
            
            if ((this.emsRate1.getMinExtensionMinutes() >= this.getMaxAllowedParkingTimeInMinutes()) && !this.getIsSecondPermissionNoParking()
                && this.maxAllowedTimeByPermission > 0) {
                isSpecialExtention = true;
            }
            LOGGER.debug("+++ getMaxAllowedParkingTimeInMinutes(): " + getMaxAllowedParkingTimeInMinutes());
            LOGGER.debug("+++ emsRate1.getMinExtensionMinutes(): " + this.emsRate1.getMinExtensionMinutes());
            LOGGER.debug("+++ emsRate2(): " + this.emsRate2);
        }
        return isSpecialExtention;
    }
    
    public final int getMaxExtendedTimeUnderRate1() {
        return this.maxExtendedTimeUnderRate1;
    }
    
    public final void setHasPermission1(final boolean hasPermission1) {
        this.hasPermission1 = hasPermission1;
    }
    
    public final int getMaxExtensionTimeForFreeParking() {
        return this.maxExtensionTimeForFreeParking;
    }
    
    public final void setMaxExtensionTimeForFreeParking(final int maxExtensionTimeForFreeParking) {
        this.maxExtensionTimeForFreeParking = maxExtensionTimeForFreeParking;
    }
    
    public final String getNoParkingLocalEndTimeDisplay() {
        return this.noParkingLocalEndTimeDisplay;
    }
    
    public final void setNoParkingLocalEndTimeDisplay(final String noParkingLocalEndTimeDisplay) {
        this.noParkingLocalEndTimeDisplay = noParkingLocalEndTimeDisplay;
    }
    
    public final void setIsNoParkingLongerThan24H(final Date dateLocal, final String customerTimeZone) {
        final Date newExpiryDateInGMT = getRoundDownDate(this.expiryDateInGMT);
        final Date expiryDateLocal = DateUtil.changeTimeZone(newExpiryDateInGMT, customerTimeZone);
        final Calendar cal = Calendar.getInstance();
        cal.setTime(expiryDateLocal);
        cal.add(Calendar.MINUTE, WebCoreConstants.DEFAULT_MAX_EXTENDABLE_TIME_IN_MINUTES);
        this.isNoParkingLongerThan24H = cal.getTime().getTime() < dateLocal.getTime();
    }
    
    public final boolean getIsNoParkingLongerThan24H() {
        return this.isNoParkingLongerThan24H;
    }
    
    public final boolean getIsSecondPermissionNoParking() {
        return this.isSecondPermissionNoParking;
    }
    
    public final void setIsSecondPermissionNoParking(final boolean isSecondPermissionNoParking) {
        this.isSecondPermissionNoParking = isSecondPermissionNoParking;
    }
    
    public final boolean getHasPermission1() {
        return this.hasPermission1;
    }
    
    public final String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("**** SmsRateInfo ****\n");
        sb.append("maxAllowedTimeByPermission: ").append(this.maxAllowedTimeByPermission).append("\n").append("getMaxAllowedParkingTimeInMinutes: ")
                .append(this.getMaxAllowedParkingTimeInMinutes()).append("\n").append("getMinAllowedParkingTimeInMinutes: ")
                .append(this.getMinAllowedParkingTimeInMinutes()).append("\n").append("getMaxAllowedTimeByRate: ")
                .append(this.getMaxAllowedTimeByRate()).append("\n").append("maxExtendedTimeUnderRate1: ").append(this.maxExtendedTimeUnderRate1)
                .append("\n").append(this.emsRate1 == null ? "rate1: null" : "rate1: " + this.emsRate1).append("\n")
                .append(this.emsRate2 == null ? "rate2: null" : "rate2: " + this.emsRate2).append("\n").append("hasPermission1 : ")
                .append(this.hasPermission1).append("\n").append("isMergedRate : ").append(this.isMergedRate).append("\n").append("isRate2Need : ")
                .append(getIsRate2Need()).append("\n").append("isExtendable(): ").append(isExtendable()).append("\n").append("isFreeParking(): ")
                .append(isFreeParking()).append("\n").append("maxExtensionTimeForFreeParking: ").append(this.maxExtensionTimeForFreeParking)
                .append("\n").append("isSecondPermissionNoParking: ").append(this.isSecondPermissionNoParking).append("\n")
                .append("isNoParkingLongerThan24H: ").append(this.isNoParkingLongerThan24H).append("\n").append("noParkingLocalEndTimeDisplay: ")
                .append(this.noParkingLocalEndTimeDisplay).append("\n").append("isTimeLimited: ").append(this.isTimeLimited).append("\n")
                .append("isSpecialExtentionForNextFreeParking(): ").append(isSpecialExtentionForNextFreeParking()).append("\n")
                .append("isSpecialExtentionForNextNoParking(): ").append(isSpecialExtentionForNextNoParking()).append("\n")
                .append("isSpecialExtentionForSingleRate(): ").append(isSpecialExtentionForSingleRate()).append("\n");
        
        return sb.toString();
    }
    
    private Date getRoundDownDate(final Date date) {
        final Calendar cal = Calendar.getInstance();
        long timeMs = date.getTime();
        timeMs = timeMs / 1000 * 1000;
        date.setTime(timeMs);
        cal.setTime(date);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }
}
