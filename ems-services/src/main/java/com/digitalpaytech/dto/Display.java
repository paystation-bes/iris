package com.digitalpaytech.dto;

import com.digitalpaytech.util.StandardConstants;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("display")
public class Display {
    
    @XStreamAsAttribute
    private String type;
    
    public Display() {
    }
    
    public Display(final String type) {
        this.type = type;
    }
    
    public final boolean isCorrectType(final String displayType) {
        if (displayType == null) {
            return false;
        }
        
        if (type.trim().indexOf(StandardConstants.STRING_COMMA) == -1 && type.equalsIgnoreCase(displayType.trim())) {
            return true;
        }
        
        final String[] typeArray = type.split(",");
        for (String type : typeArray) {
            if (type.trim().equalsIgnoreCase(displayType.trim())) {
                return true;
            }
        }
        return false;
    }
    
    public final String getType() {
        return this.type;
    }
    
    public final void setType(final String type) {
        this.type = type;
    }
}
