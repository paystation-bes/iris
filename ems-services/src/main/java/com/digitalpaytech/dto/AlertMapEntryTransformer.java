package com.digitalpaytech.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.ResultTransformerTemplate;
import com.digitalpaytech.util.support.ValueProxy;
import com.digitalpaytech.util.support.WebObjectId;

public class AlertMapEntryTransformer extends ResultTransformerTemplate<AlertMapEntry> {
    public static final String ATTR_POS_ID = "psId";
    public static final String ATTR_SERIAL_NUMBER = "psSerialNumber";
    public static final String ATTR_PAY_STATION_TYPE = "psType";
    public static final String ATTR_POS_NAME = "pointOfSaleName";
    public static final String ATTR_LATITUDE = "Latitude";
    public static final String ATTR_LONGITUDE = "Longitude";
    
    public static final String ATTR_SUM = "sum";
    public static final String ATTR_MAX_SEVERITY = "maxSeverity";
    public static final String ATTR_MIN_ALERT_TIME = "minAlertGmt";
    
    private static final long serialVersionUID = -8954103282662087048L;
    
    private List<ValueProxy<StringBuilder, Integer>> posRouteNamesProxy = new ArrayList<ValueProxy<StringBuilder, Integer>>();
    
    private Map<Integer, AlertMapEntry> entriesHash;
    
    public AlertMapEntryTransformer() {
        this(null);
    }
    
    public AlertMapEntryTransformer(final Map<Integer, AlertMapEntry> entriesHash) {
        super(AlertMapEntry.class, (MessageHelper) null, TimeZone.getTimeZone("GMT"));
        
        this.entriesHash = entriesHash;
    }
    
    @Override
    public final AlertMapEntry createInstance(final Object[] tuple) {
        final AlertMapEntry result = new AlertMapEntry();
        
        final Integer posId = getAttrInt(tuple, ATTR_POS_ID);
        result.setPosRandomId(new WebObjectId(PointOfSale.class, posId));
        result.setSerial((String) getAttr(tuple, ATTR_SERIAL_NUMBER));
        result.setPayStationType(getAttrInt(tuple, ATTR_PAY_STATION_TYPE));
        result.setPointOfSaleName((String) getAttr(tuple, ATTR_POS_NAME));
        result.setLatitude(getAttrBigDecimal(tuple, ATTR_LATITUDE));
        result.setLongitude(getAttrBigDecimal(tuple, ATTR_LONGITUDE));
        result.setRoute(new ValueProxy<StringBuilder, Integer>(posId, new StringBuilder()));
        result.setNumberOfAlerts(getAttrInt(tuple, ATTR_SUM));
        result.setSeverity(getAttrInt(tuple, ATTR_MAX_SEVERITY));
        result.setAlertGMT(getAttrSQLDate(tuple, ATTR_MIN_ALERT_TIME));
        
        this.posRouteNamesProxy.add(result.getRoute());
        
        if (this.entriesHash != null) {
            this.entriesHash.put(posId, result);
        }
        
        return result;
    }
    
    public final List<ValueProxy<StringBuilder, Integer>> getPosRouteNamesProxy() {
        return this.posRouteNamesProxy;
    }
}
