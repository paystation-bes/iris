package com.digitalpaytech.dto;

import java.io.Serializable;

public class RunningTotalInfo implements Serializable {
    
    private static final long serialVersionUID = -3388194061553198881L;

    private String coinAmount;
    private String billAmount;
    private String cardAmount;
    private String cashAmount;
    
    private String runningTotal;
    
    private String coinCount;
    private String billCount;
    private String cardCount;
    private Boolean isLegacyCash;
    
    public RunningTotalInfo() {
    }
    
    public RunningTotalInfo(final String coinAmount, final String billAmount, final String cardAmount, final String cashAmount, final String runningTotal,
            final String coinCount, final String billCount, final String cardCount, final Boolean isLegacyCash) {
        super();
        this.coinAmount = coinAmount;
        this.billAmount = billAmount;
        this.cardAmount = cardAmount;
        this.cashAmount = cashAmount;
        this.runningTotal = runningTotal;
        this.coinCount = coinCount;
        this.billCount = billCount;
        this.cardCount = cardCount;
        this.isLegacyCash = isLegacyCash;
    }
    
    public final String getCoinAmount() {
        return this.coinAmount;
    }
    
    public final void setCoinAmount(final String coinAmount) {
        this.coinAmount = coinAmount;
    }
    
    public final String getBillAmount() {
        return this.billAmount;
    }
    
    public final void setBillAmount(final String billAmount) {
        this.billAmount = billAmount;
    }
    
    public final String getCardAmount() {
        return this.cardAmount;
    }
    
    public final void setCardAmount(final String cardAmount) {
        this.cardAmount = cardAmount;
    }
    
    public final String getCashAmount() {
        return this.cashAmount;
    }
    
    public final void setCashAmount(final String cashAmount) {
        this.cashAmount = cashAmount;
    }
    
    public final String getRunningTotal() {
        return this.runningTotal;
    }
    
    public final void setRunningTotal(final String runningTotal) {
        this.runningTotal = runningTotal;
    }
    
    public final String getCoinCount() {
        return this.coinCount;
    }
    
    public final void setCoinCount(final String coinCount) {
        this.coinCount = coinCount;
    }
    
    public final String getBillCount() {
        return this.billCount;
    }
    
    public final void setBillCount(final String billCount) {
        this.billCount = billCount;
    }
    
    public final String getCardCount() {
        return this.cardCount;
    }
    
    public final void setCardCount(final String cardCount) {
        this.cardCount = cardCount;
    }
    
    public final Boolean getIsLegacyCash() {
        return this.isLegacyCash;
    }
    
    public final void setIsLegacyCash(final Boolean isLegacyCash) {
        this.isLegacyCash = isLegacyCash;
    }
    
}
