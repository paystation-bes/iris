package com.digitalpaytech.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class WidgetDataListEntry implements Serializable {
    
    private static final long serialVersionUID = -4829236622099128054L;
    @XStreamImplicit(itemFieldName = "row")
    private List<String> entry;
    
    public WidgetDataListEntry() {
        this.entry = new ArrayList<String>();
    }
    
    public WidgetDataListEntry(final ArrayList<String> entry) {
        this.entry = entry;
    }
    
    public final List<String> getEntry() {
        return this.entry;
    }
    
    public final void setEntry(final List<String> entry) {
        this.entry = entry;
    }
}
