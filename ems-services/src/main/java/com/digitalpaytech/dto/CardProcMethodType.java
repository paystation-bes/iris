package com.digitalpaytech.dto;

import com.digitalpaytech.util.WebCoreConstants;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("cardProcMethodType")
public class CardProcMethodType extends SettingsType {
    
    private static final long serialVersionUID = -4830848972755991079L;
    
    private String orgId;
    
    public CardProcMethodType() {
        setStatus(WebCoreConstants.ACTIVE);
    }
    
    public final String getOrgId() {
        return this.orgId;
    }
    
    public final void setOrgId(final String orgId) {
        this.orgId = orgId;
    }
}
