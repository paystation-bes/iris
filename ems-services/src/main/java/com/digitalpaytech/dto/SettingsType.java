package com.digitalpaytech.dto;

import java.io.Serializable;

public abstract class SettingsType implements Serializable {
    
    private static final long serialVersionUID = 64599333009879956L;

    private String name;
    private String status;
    private String randomId;
    private String label;
    
    public final String getName() {
        return this.name;
    }
    
    public final void setName(final String name) {
        this.name = name;
    }
    
    public final String getStatus() {
        return this.status;
    }
    
    public final void setStatus(final String status) {
        this.status = status;
    }
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final String getLabel() {
        return this.label;
    }
    
    public final void setLabel(final String label) {
        this.label = label;
    }
}
