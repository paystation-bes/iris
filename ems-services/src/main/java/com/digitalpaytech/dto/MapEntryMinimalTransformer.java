package com.digitalpaytech.dto;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.transform.AliasToBeanResultTransformer;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.support.WebObjectId;

public class MapEntryMinimalTransformer extends AliasToBeanResultTransformer {
    
    private static final long serialVersionUID = 8252041365743552456L;
    
    private Map<String, Integer> aliasIdxMap;
    
    public MapEntryMinimalTransformer() {
        super(MapEntry.class);
    }
    
    @Override
    public final Object transformTuple(final Object[] tuple, final String[] aliases) {
        resolveIdx(tuple, aliases);
        
        final MapEntry result = new MapEntry();
        
        // Read General Info.
        final Integer id = (Integer) tuple[this.aliasIdxMap.get("psId")];
        result.setRandomId(new WebObjectId(PointOfSale.class, id));
        result.setName((String) tuple[this.aliasIdxMap.get("psName")]);
        result.setSerialNumber((String) tuple[this.aliasIdxMap.get("psSerialNumber")]);
        result.setPayStationType((Integer) tuple[this.aliasIdxMap.get("psType")]);
        result.setLatitude((BigDecimal) tuple[this.aliasIdxMap.get("latitude")]);
        result.setLongitude((BigDecimal) tuple[this.aliasIdxMap.get("longitude")]);
        
        final Object buffer = tuple[this.aliasIdxMap.get("paperLevel")];
        result.setPaperStatus((buffer == null) ? WebCoreConstants.LEVEL_NORMAL : ((Number) buffer).intValue());
        switch (result.getPaperStatus()) {
            case WebCoreConstants.LEVEL_EMPTY:
                result.setPaperStatus(WebCoreConstants.SEVERITY_CRITICAL);
                break;
            case WebCoreConstants.LEVEL_LOW:
                result.setPaperStatus(WebCoreConstants.SEVERITY_MAJOR);
                break;
            default:
                result.setPaperStatus(WebCoreConstants.SEVERITY_CLEAR);
                break;
        }
        
        result.setSeverity((Integer) tuple[this.aliasIdxMap.get("alertSeverity")]);
        result.setAlertsCount((Byte) tuple[this.aliasIdxMap.get("alertCount")]);
        
        result.setAlertIcon(Math.max(result.getSeverity(), result.getPaperStatus()));
        
        return result;
    }
    
    private void resolveIdx(final Object[] tuple, final String[] aliases) {
        if (this.aliasIdxMap == null) {
            int idx = aliases.length;
            this.aliasIdxMap = new HashMap<String, Integer>(idx * 2, 0.75F);
            while (--idx >= 0) {
                this.aliasIdxMap.put(aliases[idx], idx);
            }
        }
    }
}
