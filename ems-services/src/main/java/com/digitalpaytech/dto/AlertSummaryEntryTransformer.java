package com.digitalpaytech.dto;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.transform.AliasToBeanResultTransformer;

import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.dto.customeradmin.AlertSummaryEntry;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.support.WebObjectId;

public class AlertSummaryEntryTransformer extends AliasToBeanResultTransformer {
    
    /**
     * 
     */
    private static final long serialVersionUID = -5758461080871666L;
    
    private Map<String, Integer> aliasIdxMap;
    private Map<Byte, Byte> levelMap;
    
    private Map<Integer, AlertSummaryEntry> entriesHash;
    
    public AlertSummaryEntryTransformer() {
        super(AlertSummaryEntry.class);
        createLevelMap();
    }
    
    public AlertSummaryEntryTransformer(final Map<Integer, AlertSummaryEntry> entriesHash) {
        super(AlertSummaryEntry.class);
        createLevelMap();
        
        this.entriesHash = entriesHash;
    }
    
    private void createLevelMap() {
        this.levelMap = new HashMap<Byte, Byte>();
        this.levelMap.put((byte) WebCoreConstants.LEVEL_EMPTY, (byte) WebCoreConstants.SEVERITY_CRITICAL);
        this.levelMap.put((byte) WebCoreConstants.LEVEL_FULL, (byte) WebCoreConstants.SEVERITY_CLEAR);
        this.levelMap.put((byte) WebCoreConstants.LEVEL_JAM, (byte) WebCoreConstants.LEVEL_JAM);
        this.levelMap.put((byte) WebCoreConstants.LEVEL_LOW, (byte) WebCoreConstants.SEVERITY_MAJOR);
        this.levelMap.put((byte) WebCoreConstants.LEVEL_NORMAL, (byte) WebCoreConstants.SEVERITY_CLEAR);
    }
    
    @Override
    public final Object transformTuple(final Object[] tuple, final String[] aliases) {
        resolveIdx(tuple, aliases);
        
        final AlertSummaryEntry result = new AlertSummaryEntry();
        
        final Integer posId = (Integer) tuple[this.aliasIdxMap.get("posId")];
        result.setPosRandomId(new WebObjectId(PointOfSale.class, posId));
        final Integer locationId = (Integer) tuple[this.aliasIdxMap.get("locationId")];
        result.setLocationRandomId(new WebObjectId(Location.class, locationId));
        
        result.setSerial((String) tuple[this.aliasIdxMap.get("psSerialNumber")]);
        result.setPointOfSaleName((String) tuple[this.aliasIdxMap.get("pointOfSaleName")]);
        result.setLocationName((String) tuple[this.aliasIdxMap.get("locationName")]);
        final Byte paperLevel = (Byte) tuple[this.aliasIdxMap.get("paperLevel")];
        final Byte printerStatus = (Byte) tuple[this.aliasIdxMap.get("printerStatus")];
        result.setPaperLevel(this.levelMap.get(printerStatus == WebCoreConstants.LEVEL_JAM ? printerStatus : paperLevel));
        result.setBatteryLevel(this.levelMap.get((Byte) tuple[this.aliasIdxMap.get("batteryLevel")]));
        result.setBatteryVoltage((Float) tuple[this.aliasIdxMap.get("batteryVoltage")]);
        result.setLastSeenGMT(new Date(((Timestamp) tuple[this.aliasIdxMap.get("lastSeenGMT")]).getTime()));
        
        final int payStationCritical = defaultIfNull(tuple, "payStationCritical", 0);
        final int payStationMajor = defaultIfNull(tuple, "payStationMajor", 0);
        final int payStationMinor = defaultIfNull(tuple, "payStationMinor", 0);
        if (payStationCritical > 0) {
            result.setSeverity(WebCoreConstants.SEVERITY_CRITICAL);
        } else if (payStationMajor > 0) {
            result.setSeverity(WebCoreConstants.SEVERITY_MAJOR);
        } else if (payStationMinor > 0) {
            result.setSeverity(WebCoreConstants.SEVERITY_MINOR);
        } else {
            result.setSeverity(WebCoreConstants.SEVERITY_CLEAR);
        }
        
        final int communicationCritical = defaultIfNull(tuple, "communicationCritical", 0);
        final int communicationMajor = defaultIfNull(tuple, "communicationMajor", 0);
        final int communicationMinor = defaultIfNull(tuple, "communicationMinor", 0);
        if (communicationCritical > 0) {
            result.setLastSeenSeverity(WebCoreConstants.SEVERITY_CRITICAL);
        } else if (communicationMajor > 0) {
            result.setLastSeenSeverity(WebCoreConstants.SEVERITY_MAJOR);
        } else if (communicationMinor > 0) {
            result.setLastSeenSeverity(WebCoreConstants.SEVERITY_MINOR);
        } else {
            result.setLastSeenSeverity(WebCoreConstants.SEVERITY_CLEAR);
        }
        
        if (result.getLastSeenSeverity() > result.getSeverity()) {
            result.setSeverity(result.getLastSeenSeverity());
        }
        
        if (this.entriesHash != null) {
            this.entriesHash.put(posId, result);
        }
        
        return result;
    }
    
    private void resolveIdx(final Object[] tuple, final String[] aliases) {
        if (this.aliasIdxMap == null) {
            int idx = aliases.length;
            this.aliasIdxMap = new HashMap<String, Integer>(idx * 2, 0.75F);
            while (--idx >= 0) {
                this.aliasIdxMap.put(aliases[idx], idx);
            }
        }
    }
    
    private int defaultIfNull(final Object[] tuple, final String propertyName, final int defaultValue) {
        int result = defaultValue;
        
        final Integer idx = this.aliasIdxMap.get(propertyName);
        if (idx != null) {
            if (tuple[idx] != null) {
                result = ((Number) tuple[idx]).intValue();
            } else {
                result = 0;
            }
        }
        
        return result;
    }
    
}
