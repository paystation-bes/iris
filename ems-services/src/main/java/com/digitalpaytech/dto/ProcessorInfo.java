package com.digitalpaytech.dto;

import java.io.Serializable;

import com.digitalpaytech.domain.Processor;

public class ProcessorInfo implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -8901997679054894984L;
    private Integer id;
    private String name;
    private String label;
    private boolean gateway;
    private boolean forValueCard;
    private boolean emv;
    private String alias;
    private String formName;
    private boolean isLink;
    private boolean isActive;
    
    public ProcessorInfo(final Processor processor, final String label, final String formName, final String alias, final boolean isLink) {
        this.id = processor.getId();
        this.name = processor.getName().trim();
        this.gateway = processor.isIsGateway();
        this.forValueCard = processor.isIsForValueCard();
        this.emv = processor.getIsEMV();
        this.isLink = processor.getIsLink();
        
        this.label = (label == null) ? "" : label;
        this.alias = (alias == null) ? "" : alias.trim();
        this.formName = (formName == null) ? "" : formName.trim();
        this.isLink = isLink;
        this.isActive = processor.getIsActive();
    }
    
    public final Integer getId() {
        return this.id;
    }
    
    public final String getName() {
        return this.name;
    }
    
    public final String getLabel() {
        return this.label;
    }
    
    public final boolean isGateway() {
        return this.gateway;
    }
    
    public final boolean isForValueCard() {
        return this.forValueCard;
    }
    
    public final boolean isEMV() {
        return this.emv;
    }
    
    public final String getFormName() {
        return this.formName;
    }
    
    public final String getAlias() {
        return this.alias;
    }
    
    public boolean isLink() {
        return this.isLink;
    }
    
    public void setLink(final boolean isLink) {
        this.isLink = isLink;
    }

	public boolean isActive() {
		return this.isActive;
	}

	public void setActive(final boolean isActive) {
		this.isActive = isActive;
	}
    
}
