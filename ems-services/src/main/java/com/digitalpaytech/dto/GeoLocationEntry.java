package com.digitalpaytech.dto;

import java.io.Serializable;

import com.digitalpaytech.util.support.WebObjectId;

public class GeoLocationEntry implements Serializable {
    
    private static final long serialVersionUID = -4449395421285600927L;
    private WebObjectId randomId;
    private String jsonString;
    private String locationName;
    private Float paidOccupancy;
    private Float physicalOccupancy;
    
    public final WebObjectId getRandomId() {
        return this.randomId;
    }
    
    public final String getJsonString() {
        return this.jsonString;
    }
    
    public final String getLocationName() {
        return this.locationName;
    }
    
    public final Float getPaidOccupancy() {
        return this.paidOccupancy;
    }
    
    public final Float getPhysicalOccupancy() {
        return this.physicalOccupancy;
    }
    
    public final void setRandomId(final WebObjectId randomId) {
        this.randomId = randomId;
    }
    
    public final void setJsonString(final String jsonString) {
        this.jsonString = jsonString;
    }
    
    public final void setLocationName(final String locationName) {
        this.locationName = locationName;
    }
    
    public final void setPaidOccupancy(final Float paidOccupancy) {
        this.paidOccupancy = paidOccupancy;
    }
    
    public final void setPhysicalOccupancy(final Float physicalOccupancy) {
        this.physicalOccupancy = physicalOccupancy;
    }
    
}
