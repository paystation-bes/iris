package com.digitalpaytech.dto.mobile.rest;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "status")
@XmlAccessorType(XmlAccessType.FIELD)
public class CollectStatusDTO implements MobileDTO {
	@XmlElement(name = "routes")
	List<CollectRouteStatusDTO> routes;
	
	@XmlElement(name = "timestamp")
	Date timeStamp;
	
	public CollectStatusDTO(){
		routes = new LinkedList<CollectRouteStatusDTO>();
	}
	
	public List<CollectRouteStatusDTO> getRoutes(){
		return routes;
	}
	public void setRoutes(List<CollectRouteStatusDTO> routes){
		this.routes = routes;
	}
	public void addRoute(CollectRouteStatusDTO route){
		routes.add(route);
	}
	
	public Date getTimeStamp(){
	    return timeStamp;
	}
	public void setTimeStamp(Date timeStamp){
	    this.timeStamp = timeStamp;
	}
}
