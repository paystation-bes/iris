package com.digitalpaytech.dto.mobile.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "route")
@XmlAccessorType(XmlAccessType.FIELD)
public class CollectRouteSummaryDTO {
	@XmlElement(name = "name")
	private String name;
	
	@XmlElement(name = "id")
	private int id;
	
	@XmlElement(name = "usercount")
	private int userCount;
	
	@XmlElement(name = "paystationswithcollectionalerts")
	private int paystationsWithCollectionAlerts;
	
	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name = name;
	}
	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}
	
    public int getUserCount() {
        return userCount;
    }
    public void setUserCount(int userCount) {
        this.userCount = userCount;
    }
    public void incrementUserCount(int incrementBy) {
        userCount += incrementBy;
    }
    
    public int getPaystationsWithCollectionAlerts(){
        return paystationsWithCollectionAlerts;
    }
    public void setPaystationsWithCollectionAlerts(int paystationsWithCollectionAlerts){
        this.paystationsWithCollectionAlerts = paystationsWithCollectionAlerts;
    }
}
