package com.digitalpaytech.dto.mobile.rest;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
public class MobileSecretDTO implements MobileDTO{
	@XmlElement(name = "secret")
	private String secret;
	
	public MobileSecretDTO(){
		
	}
	public MobileSecretDTO(String secret){
		this.secret = secret;
	}
	
	public String getSecret(){
		return secret;
	}
	public void setSecret(String secret){
		this.secret = secret;
	}
}
