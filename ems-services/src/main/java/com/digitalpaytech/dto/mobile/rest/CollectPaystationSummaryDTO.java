package com.digitalpaytech.dto.mobile.rest;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "paystation")
@XmlAccessorType(XmlAccessType.FIELD)
public class CollectPaystationSummaryDTO {
	@XmlElement(name = "name")
	private String name;
	
	@XmlElement(name = "id")
	private int id;
	
	@XmlElement(name = "routes")
	private List<Integer> routes;
	
	@XmlElement(name = "lat")
	private BigDecimal latitude;
	
	@XmlElement(name = "lon")
	private BigDecimal longitude;
	
	@XmlElement(name = "location")
	private String location;
	
	@XmlElement(name = "serialnumber")
	private String serialNumber;
	
	@XmlElement(name = "paystationtype")
	private int paystationType;
	
	private Date lastCollected;
	private Date lastCollectedBill;
	private Date lastCollectedCoin;
	private Date lastCollectedCard;
	
	
	public Date getLastCollectedCard(){
	    return lastCollectedCard;
	}
	public void setLastCollectedCard(Date lastCollectedCard){
	    this.lastCollectedCard = lastCollectedCard;
	}
	public Date getLastCollectedCoin(){
	    return lastCollectedCoin;
	}
	public void setLastCollectedCoin(Date lastCollectedCoin){
	    this.lastCollectedCoin = lastCollectedCoin;
	}
	public Date getLastCollectedBill(){
	    return lastCollectedBill;
	}
	public void setLastCollectedBill(Date lastCollectedBill){
	    this.lastCollectedBill = lastCollectedBill;
	}
	public Date getLastCollected(){
	    return lastCollected;
	}
	public void setLastCollected(Date lastCollected){
	    this.lastCollected = lastCollected;
	}
	
	public int getPaystationType(){
	    return paystationType;
	}
	public void setPaystationType(int paystationType){
	    this.paystationType = paystationType;
	}
	
	public String getSerialNumber(){
	    return serialNumber;
	}
	public void setSerialNumber(String serialNumber){
	    this.serialNumber = serialNumber;
	}
	
	public String getLocation(){
	    return location;
	}
	public void setLocation(String location){
	    this.location = location;
	}
	
	public CollectPaystationSummaryDTO(){
		routes = new LinkedList<Integer>();
	}
	
	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name = name;
	}
	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}
	public List<Integer> getRoutes(){
		return routes;
	}
	public void setRoutes(List<Integer> routes){
		this.routes = routes;
	}
	public void addRoute(Integer route){
		routes.add(route);
	}
	public BigDecimal getLongitude(){
		return longitude;
	}
	public void setLongitude(BigDecimal longitude){
		this.longitude = longitude;
	}
	public BigDecimal getLatitude(){
		return latitude;
	}
	public void setLatitude(BigDecimal latitude){
		this.latitude = latitude;
	}
	
}
