package com.digitalpaytech.dto.mobile.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "collected")
@XmlAccessorType(XmlAccessType.FIELD)
public class CollectCollectedDTO implements MobileDTO {
    
    @XmlElement(name = "status")
    private String status;
    
    public CollectCollectedDTO() {
    }
    
    public final String getStatus() {
        return this.status;
    }
    
    public final void setStatus(final String status) {
        this.status = status;
    }
    
}
