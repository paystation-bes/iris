package com.digitalpaytech.dto.mobile.rest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.transform.AliasToBeanResultTransformer;

public class CollectPaystationSummaryTransformer extends AliasToBeanResultTransformer {
    private static final long serialVersionUID = -3451381928385821781L;
    
    private Map<Integer, CollectPaystationSummaryDTO> result;
    
    private int posNameIdx = -1;
    private int posIdIdx = -1;
    private int routeIdIdx = -1;
    private int latitudeIdx = -1;
    private int longitudeIdx = -1;
    private int locationNameIdx = -1;
    private int parentLocationNameIdx = -1;
    private int serialNumberIdx = -1;
    private int paystationTypeIdx = -1;
    private int lastCollectedIdx = -1;
    private int lastCollectedBillIdx = -1;
    private int lastCollectedCoinIdx = -1;
    private int lastCollectedCardIdx = -1;
    
    public CollectPaystationSummaryTransformer() {
        this(new HashMap<Integer, CollectPaystationSummaryDTO>());
    }
    
    public CollectPaystationSummaryTransformer(final Map<Integer, CollectPaystationSummaryDTO> result) {
        super(CollectPaystationSummaryDTO.class);
        this.result = result;
    }
    
    @Override
    public final Object transformTuple(final Object[] tuple, final String[] aliases) {
        if (this.posIdIdx < 0) {
            resolveIdx(tuple, aliases);
        }
        final Integer posId = (Integer) tuple[this.posIdIdx];
        CollectPaystationSummaryDTO summ = null;
        if (this.result.containsKey(posId)) {
            summ = this.result.get(posId);
            summ.getRoutes().add(((Number) tuple[this.routeIdIdx]).intValue());
        } else {
            summ = new CollectPaystationSummaryDTO();
            summ.setName((String) tuple[this.posNameIdx]);
            summ.setId(posId);
            summ.setRoutes(new ArrayList<Integer>());
            summ.getRoutes().add(((Number) tuple[this.routeIdIdx]).intValue());
            summ.setLatitude((BigDecimal) tuple[this.latitudeIdx]);
            summ.setLongitude((BigDecimal) tuple[this.longitudeIdx]);
            
            String locationName = (String) tuple[this.locationNameIdx];
            if (tuple[this.parentLocationNameIdx] != null) {
                locationName = locationName + " (" + ((String) tuple[this.parentLocationNameIdx]) + ")";
            }
            summ.setLocation(locationName);
            summ.setSerialNumber((String) tuple[this.serialNumberIdx]);
            summ.setPaystationType(((Number) tuple[this.paystationTypeIdx]).intValue());
            summ.setLastCollected((Date) tuple[this.lastCollectedIdx]);
            summ.setLastCollectedBill((Date) tuple[this.lastCollectedBillIdx]);
            summ.setLastCollectedCoin((Date) tuple[this.lastCollectedCoinIdx]);
            summ.setLastCollectedCard((Date) tuple[this.lastCollectedCardIdx]);
            
            this.result.put(posId, summ);
            
        }
        
        return summ;
    }
    
    private void resolveIdx(final Object[] tuple, final String[] aliases) {
        int idx = aliases.length;
        while (--idx >= 0) {
            if ("Name".equals(aliases[idx])) {
                this.posNameIdx = idx;
            } else if ("Id".equals(aliases[idx])) {
                this.posIdIdx = idx;
            } else if ("RouteId".equals(aliases[idx])) {
                this.routeIdIdx = idx;
            } else if ("Latitude".equals(aliases[idx])) {
                this.latitudeIdx = idx;
            } else if ("Longitude".equals(aliases[idx])) {
                this.longitudeIdx = idx;
            } else if ("Location".equals(aliases[idx])) {
                this.locationNameIdx = idx;
            } else if ("ParentLocation".equals(aliases[idx])) {
                this.parentLocationNameIdx = idx;
            } else if ("SerialNumber".equals(aliases[idx])) {
                this.serialNumberIdx = idx;
            } else if ("PaystationType".equals(aliases[idx])) {
                this.paystationTypeIdx = idx;
            } else if ("LastCollected".equals(aliases[idx])) {
                this.lastCollectedIdx = idx;
            } else if ("LastCollectedBill".equals(aliases[idx])) {
                this.lastCollectedBillIdx = idx;
            } else if ("LastCollectedCoin".equals(aliases[idx])) {
                this.lastCollectedCoinIdx = idx;
            } else if ("LastCollectedCard".equals(aliases[idx])) {
                this.lastCollectedCardIdx = idx;
            }
        }
    }
    
    public final Map<Integer, CollectPaystationSummaryDTO> getResult() {
        return this.result;
    }
}
