package com.digitalpaytech.dto.mobile.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "token")
@XmlAccessorType(XmlAccessType.FIELD)
public class MobileTokenDTO implements MobileDTO{
	@XmlElement(name = "token")
	private String sessionToken;
	
	public MobileTokenDTO(String sessionToken){
		this.sessionToken = sessionToken;
	}
	public String getSessionToken(){
		return sessionToken;
	}
	public void setSessionToken(String sessionToken){
		this.sessionToken = sessionToken;
	}
}
