package com.digitalpaytech.dto.mobile.rest;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang.StringUtils;

import com.digitalpaytech.util.RestCoreConstants;

public class MobileCollectStatusQueryParamList {
	private String sessionToken;
	private String latitude;
	private String longitude;
	private String route;
	private String lastUpdate;
	private String signature;
	private String signatureVersion;
	private String timestamp;
	private String active;
	public String getActive(){
	    return active;
	}
	public void setActive(String active){
	    this.active = active;
	}
	
	public String getSessionToken(){
		return sessionToken;
	}
	public void setSessionToken(String sessionToken){
		this.sessionToken = sessionToken;
	}
	
	public String getLatitude(){
		return latitude;
	}
	public void setLatitude(String latitude){
		this.latitude = latitude;
	}
	
	public String getLongitude(){
		return longitude;
	}
	public void setLongitude(String longitude){
		this.longitude = longitude;
	}
	
	public String getRoute(){
		return route;
	}
	public void setRoute(String route){
		this.route = route;
	}
	
	public String getLastUpdate(){
		return lastUpdate;
	}
	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}
	
	public String getSignature(){
		return signature;
	}
	public void setSignature(String signature){
		this.signature = signature;
	}
	
	public String getSignatureVersion(){
		return signatureVersion;
	}
	public void setSignatureVersion(String signatureVersion){
		this.signatureVersion = signatureVersion;
	}
	
	public String getTimestamp(){
	    return timestamp;
	}
	public void setTimestamp(String timestamp){
	    this.timestamp = timestamp;
	}
	
	@Override
	public String toString(){
		StringBuilder str = new StringBuilder();
		str.append(this.getClass().getName());
		str.append(" {sessiontoken=").append(this.sessionToken);
        if(!StringUtils.isBlank(latitude)){
            str.append(", lat=").append(this.latitude);
            str.append(", lon=").append(this.longitude);
        }
		if(!StringUtils.isBlank(route)){
		    str.append(", route=").append(this.route);
		}
		if(!StringUtils.isBlank(lastUpdate)){
		    str.append(", lastupdate=").append(this.lastUpdate);
		}
		str.append(", active=").append(this.active);
		str.append(", timestamp=").append(this.timestamp);
		str.append(", signature=").append(this.signature);
		str.append(", signatureversion=").append(this.signatureVersion);
		str.append("}\n");
		str.append(super.toString());
		return str.toString();
	}
	
	public String getFormatParamString() throws UnsupportedEncodingException{
		StringBuilder str = new StringBuilder();
		str.append("sessiontoken").append(RestCoreConstants.EQUAL_SIGN)
			.append(URLEncoder.encode(sessionToken, RestCoreConstants.HTTP_REST_ENCODE_CHARSET))
			.append(RestCoreConstants.AMPERSAND);
		if(!StringUtils.isBlank(latitude)){
		    str.append("lat").append(RestCoreConstants.EQUAL_SIGN)
                .append(URLEncoder.encode(latitude, RestCoreConstants.HTTP_REST_ENCODE_CHARSET))
                .append(RestCoreConstants.AMPERSAND)
            .append("lon").append(RestCoreConstants.EQUAL_SIGN)
                .append(URLEncoder.encode(longitude, RestCoreConstants.HTTP_REST_ENCODE_CHARSET))
                .append(RestCoreConstants.AMPERSAND);
		}
		if(!StringUtils.isBlank(route)){
		    str.append("route").append(RestCoreConstants.EQUAL_SIGN)
            .append(URLEncoder.encode(route, RestCoreConstants.HTTP_REST_ENCODE_CHARSET))
            .append(RestCoreConstants.AMPERSAND);
		}
		if(!StringUtils.isBlank(lastUpdate)){
		    str.append("lastupdate").append(RestCoreConstants.EQUAL_SIGN)
            .append(URLEncoder.encode(lastUpdate, RestCoreConstants.HTTP_REST_ENCODE_CHARSET))
            .append(RestCoreConstants.AMPERSAND);
		}
		str.append("active").append(RestCoreConstants.EQUAL_SIGN)
		    .append(URLEncoder.encode(active, RestCoreConstants.HTTP_REST_ENCODE_CHARSET))
		    .append(RestCoreConstants.AMPERSAND);
		str.append("timestamp").append(RestCoreConstants.EQUAL_SIGN)
		    .append(URLEncoder.encode(timestamp, RestCoreConstants.HTTP_REST_ENCODE_CHARSET))
		    .append(RestCoreConstants.AMPERSAND);
		str.append("signatureversion").append(RestCoreConstants.EQUAL_SIGN)
			.append(URLEncoder.encode(signatureVersion, RestCoreConstants.HTTP_REST_ENCODE_CHARSET));
		return str.toString();
	}
}
