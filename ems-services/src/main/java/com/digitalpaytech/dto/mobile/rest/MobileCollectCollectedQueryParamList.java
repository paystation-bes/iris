package com.digitalpaytech.dto.mobile.rest;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang.StringUtils;

import com.digitalpaytech.util.MobileConstants;
import com.digitalpaytech.util.RestCoreConstants;

public class MobileCollectCollectedQueryParamList {
    private String sessionToken;
    private String latitude;
    private String longitude;
    private Integer collectionTypeId;
    private String mobileEndGmt;
    private String mobileStartGmt;
    private String serialNumber;
    private String signature;
    private String signatureVersion;
    private String timestamp;
    
    public final String getSessionToken() {
        return this.sessionToken;
    }
    
    public final void setSessionToken(final String sessionToken) {
        this.sessionToken = sessionToken;
    }
    
    public final String getLatitude() {
        return this.latitude;
    }
    
    public final void setLatitude(final String latitude) {
        this.latitude = latitude;
    }
    
    public final String getLongitude() {
        return this.longitude;
    }
    
    public final void setLongitude(final String longitude) {
        this.longitude = longitude;
    }
    
    public final Integer getCollectionTypeId() {
        return this.collectionTypeId;
    }
    
    public final void setCollectionTypeId(final Integer collectionTypeId) {
        this.collectionTypeId = collectionTypeId;
    }
    
    public final String getMobileEndGmt() {
        return this.mobileEndGmt;
    }
    
    public final void setMobileEndGmt(final String mobileEndGmt) {
        this.mobileEndGmt = mobileEndGmt;
    }
    
    public final String getMobileStartGmt() {
        return this.mobileStartGmt;
    }
    
    public final void setMobileStartGmt(final String mobileStartGmt) {
        this.mobileStartGmt = mobileStartGmt;
    }
    
    public final String getSerialNumber() {
        return this.serialNumber;
    }
    
    public final void setSerialNumber(final String serialNumber) {
        this.serialNumber = serialNumber;
    }
    
    public final String getSignature() {
        return this.signature;
    }
    
    public final void setSignature(final String signature) {
        this.signature = signature;
    }
    
    public final String getSignatureVersion() {
        return this.signatureVersion;
    }
    
    public final void setSignatureVersion(final String signatureVersion) {
        this.signatureVersion = signatureVersion;
    }
    
    public final String getTimestamp() {
        return this.timestamp;
    }
    
    public final void setTimestamp(final String timestamp) {
        this.timestamp = timestamp;
    }
    
    @Override
    public final String toString() {
        final StringBuilder str = new StringBuilder();
        str.append(this.getClass().getName());
        str.append(" {sessiontoken=").append(this.sessionToken);
        if (!StringUtils.isBlank(this.latitude)) {
            str.append(", lat=").append(this.latitude);
            str.append(", lon=").append(this.longitude);
        }
        str.append(", timestamp=").append(this.timestamp);
        str.append(", serialnumber=").append(this.serialNumber);
        str.append(", mobileEndGmt=").append(this.mobileEndGmt);
        str.append(", mobileStartGmt=").append(this.mobileStartGmt);
        str.append(", signature=").append(this.signature);
        str.append(", signatureversion=").append(this.signatureVersion);
        str.append("}\n");
        str.append(super.toString());
        return str.toString();
    }
    
    public String getFormatParamString() throws UnsupportedEncodingException {
        StringBuilder str = new StringBuilder();
        str.append(MobileConstants.PARAM_NAME_COLLECTION_TYPE).append(RestCoreConstants.EQUAL_SIGN)
                .append(URLEncoder.encode(String.valueOf(this.collectionTypeId), RestCoreConstants.HTTP_REST_ENCODE_CHARSET))
                .append(RestCoreConstants.AMPERSAND);
        if (!StringUtils.isBlank(latitude)) {
            str.append(MobileConstants.PARAM_NAME_LAT).append(RestCoreConstants.EQUAL_SIGN)
                    .append(URLEncoder.encode(latitude, RestCoreConstants.HTTP_REST_ENCODE_CHARSET)).append(RestCoreConstants.AMPERSAND)
                    .append(MobileConstants.PARAM_NAME_LONG).append(RestCoreConstants.EQUAL_SIGN)
                    .append(URLEncoder.encode(longitude, RestCoreConstants.HTTP_REST_ENCODE_CHARSET)).append(RestCoreConstants.AMPERSAND);
        }
        str.append(MobileConstants.PARAM_NAME_MOBILE_END_GMT).append(RestCoreConstants.EQUAL_SIGN)
                .append(URLEncoder.encode(this.mobileEndGmt, RestCoreConstants.HTTP_REST_ENCODE_CHARSET)).append(RestCoreConstants.AMPERSAND);
        str.append(MobileConstants.PARAM_NAME_MOBILE_START_GMT).append(RestCoreConstants.EQUAL_SIGN)
                .append(URLEncoder.encode(this.mobileStartGmt, RestCoreConstants.HTTP_REST_ENCODE_CHARSET)).append(RestCoreConstants.AMPERSAND);
        str.append(MobileConstants.PARAM_NAME_SERIAL_NUMBER).append(RestCoreConstants.EQUAL_SIGN)
                .append(URLEncoder.encode(this.serialNumber, RestCoreConstants.HTTP_REST_ENCODE_CHARSET)).append(RestCoreConstants.AMPERSAND);
        str.append(MobileConstants.PARAM_NAME_SESSION_TOKEN).append(RestCoreConstants.EQUAL_SIGN)
                .append(URLEncoder.encode(this.sessionToken, RestCoreConstants.HTTP_REST_ENCODE_CHARSET)).append(RestCoreConstants.AMPERSAND);
        str.append(MobileConstants.PARAM_NAME_SIGNATURE_VERSION).append(RestCoreConstants.EQUAL_SIGN)
                .append(URLEncoder.encode(signatureVersion, RestCoreConstants.HTTP_REST_ENCODE_CHARSET)).append(RestCoreConstants.AMPERSAND);
        str.append(MobileConstants.PARAM_NAME_TIMESTAMP).append(RestCoreConstants.EQUAL_SIGN)
                .append(URLEncoder.encode(timestamp, RestCoreConstants.HTTP_REST_ENCODE_CHARSET));
        return str.toString();
    }
}
