package com.digitalpaytech.dto.mobile.rest;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang.StringUtils;

import com.digitalpaytech.util.RestCoreConstants;

public class MobileLoginParamList {
	private String deviceUid;
	private String applicationId;
	private String userName;
	private String password;
	private String latitude;
	private String longitude;
	private String signature;
	private String signatureVersion;
	private String timestamp;
	
	public String getDeviceId(){
		return deviceUid;
	}
	public void setDeviceUid(String deviceUid){
		this.deviceUid = deviceUid;
	}
	
	public String getApplicationId(){
		return applicationId;
	}
	public void setApplicationId(String applicationId){
		this.applicationId = applicationId;
	}
	
	public String getUserName(){
		return userName;
	}
	public void setUserName(String userName){
		this.userName = userName;
	}
	
	public String getPassword(){
		return password;
	}
	public void setPassword(String password){
		this.password = password;
	}
	
	public String getLatitude(){
	    return latitude;
	}
	public void setLatitude(String latitude){
	    this.latitude = latitude;
	}
	
	public String getLongitude(){
	    return longitude;
	}
	public void setLongitude(String longitude){
	    this.longitude = longitude;
	}
	
	public String getSignature(){
		return signature;
	}
	public void setSignature(String signature){
		this.signature = signature;
	}
	
	public String getSignatureVersion(){
		return signatureVersion;
	}
	public void setSignatureVersion(String signatureVersion){
		this.signatureVersion = signatureVersion;
	}
	
	public String getTimestamp(){
	    return timestamp;
	}
	public void setTimestamp(String timestamp){
	    this.timestamp = timestamp;
	}
	
	@Override
	public String toString(){
		StringBuilder str = new StringBuilder();
		str.append(this.getClass().getName());
		str.append(" {deviceUid=").append(this.deviceUid);
		str.append(", applicationid=").append(this.applicationId);
		str.append(", username=").append(this.userName);
		str.append(", password=").append(this.password);
        if(!StringUtils.isBlank(latitude)){
            str.append(", lat=").append(this.latitude);
            str.append(", lon=").append(this.longitude);
        }
		str.append(", timestamp=").append(this.timestamp);
		str.append(", signature=").append(this.signature);
		str.append(", signatureversion=").append(this.signatureVersion);
		str.append("}\n");
		str.append(super.toString());
		return str.toString();
	}
	
	public String getFormatParamString() throws UnsupportedEncodingException{
		StringBuilder str = new StringBuilder();
		str.append("device").append(RestCoreConstants.EQUAL_SIGN)
			.append(URLEncoder.encode(deviceUid, RestCoreConstants.HTTP_REST_ENCODE_CHARSET))
			.append(RestCoreConstants.AMPERSAND)
		.append("application").append(RestCoreConstants.EQUAL_SIGN)
			.append(URLEncoder.encode(applicationId, RestCoreConstants.HTTP_REST_ENCODE_CHARSET))
			.append(RestCoreConstants.AMPERSAND)
		.append("j_username").append(RestCoreConstants.EQUAL_SIGN)
			.append(URLEncoder.encode(userName, RestCoreConstants.HTTP_REST_ENCODE_CHARSET))
			.append(RestCoreConstants.AMPERSAND)
		.append("j_password").append(RestCoreConstants.EQUAL_SIGN)
			.append(URLEncoder.encode(password, RestCoreConstants.HTTP_REST_ENCODE_CHARSET))
			.append(RestCoreConstants.AMPERSAND);
		if(!StringUtils.isBlank(latitude)){
            str.append("lat").append(RestCoreConstants.EQUAL_SIGN)
                .append(URLEncoder.encode(latitude, RestCoreConstants.HTTP_REST_ENCODE_CHARSET))
                .append(RestCoreConstants.AMPERSAND)
            .append("lon").append(RestCoreConstants.EQUAL_SIGN)
                .append(URLEncoder.encode(longitude, RestCoreConstants.HTTP_REST_ENCODE_CHARSET))
                .append(RestCoreConstants.AMPERSAND);
        }
		str.append("timestamp").append(RestCoreConstants.EQUAL_SIGN)
		    .append(URLEncoder.encode(timestamp, RestCoreConstants.HTTP_REST_ENCODE_CHARSET))
		    .append(RestCoreConstants.AMPERSAND)
		.append("signatureversion").append(RestCoreConstants.EQUAL_SIGN)
			.append(URLEncoder.encode(signatureVersion, RestCoreConstants.HTTP_REST_ENCODE_CHARSET));
		
		return str.toString();
	}
}
