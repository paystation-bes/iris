package com.digitalpaytech.dto.mobile.rest;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.digitalpaytech.util.RestCoreConstants;

public class MobileProvisionParamList {
	//TODO is this class even used?  other query param list classes are used to verify the signature,
	// but provisioning the device (the purpose of the method which builds this query) implies that 
	// the secret key is not yet known by the mobile device.  
	//TODO possibly delete this class
	private String deviceUid;
	private String applicationId;
	private String friendlyName;
	private String userName;
	private String password;
	private String timestamp;
	
	public String getDeviceUid(){
		return deviceUid;
	}
	public void setDeviceUid(String deviceUid){
		this.deviceUid = deviceUid;
	}
	
	public String getApplicationId(){
		return applicationId;
	}
	public void setApplicationId(String applicationId){
		this.applicationId = applicationId;
	}
	
	public String getFriendlyName(){
		return friendlyName;
	}
	public void setFriendlyName(String friendlyName){
		this.friendlyName = friendlyName;
	}
	
	public String getUserName(){
		return userName;
	}
	public void setUserName(String userName){
		this.userName = userName;
	}
	
	public String getPassword(){
		return password;
	}
	public void setPassword(String password){
		this.password = password;
	}
	
	public String getTimestamp(){
	    return timestamp;
	}
	public void setTimestamp(String timestamp){
	    this.timestamp = timestamp;
	}
	
	@Override
	public String toString(){
		StringBuilder str = new StringBuilder();
		str.append(this.getClass().getName());
		str.append(" {deviceid=").append(this.deviceUid);
		str.append(", applicationid=").append(this.applicationId);
		str.append(", friendlyname=").append(this.friendlyName);
		str.append(", username=").append(this.userName);
		str.append(", password=").append(this.password);
		str.append(", timestamp=").append(this.timestamp);
		str.append("}\n");
		str.append(super.toString());
		return str.toString();
	}
	
	public String getFormatParamString() throws UnsupportedEncodingException{
		StringBuilder str = new StringBuilder();
		str.append("device").append(RestCoreConstants.EQUAL_SIGN)
			.append(URLEncoder.encode(deviceUid, RestCoreConstants.HTTP_REST_ENCODE_CHARSET))
			.append(RestCoreConstants.AMPERSAND)
		.append("application").append(RestCoreConstants.EQUAL_SIGN)
			.append(URLEncoder.encode(applicationId, RestCoreConstants.HTTP_REST_ENCODE_CHARSET))
			.append(RestCoreConstants.AMPERSAND)
		.append("friendlyname").append(RestCoreConstants.EQUAL_SIGN)
			.append(URLEncoder.encode(friendlyName, RestCoreConstants.HTTP_REST_ENCODE_CHARSET))
			.append(RestCoreConstants.AMPERSAND)
		.append("username").append(RestCoreConstants.EQUAL_SIGN)
			.append(URLEncoder.encode(userName, RestCoreConstants.HTTP_REST_ENCODE_CHARSET))
			.append(RestCoreConstants.AMPERSAND)
		.append("password").append(RestCoreConstants.EQUAL_SIGN)
			.append(URLEncoder.encode(password, RestCoreConstants.HTTP_REST_ENCODE_CHARSET))
			.append(RestCoreConstants.AMPERSAND)
		.append("timestamp").append(RestCoreConstants.EQUAL_SIGN)
		    .append(URLEncoder.encode(timestamp, RestCoreConstants.HTTP_REST_ENCODE_CHARSET));
		
		return str.toString();
	}
}
