package com.digitalpaytech.dto.mobile.rest;
/**
 * deliberately empty interface.  Used only to indicate a type of object which can be
 * returned by Mobile API as the root element of a JSON response.  
 * Implementers of this interface are expected to have XmlAccessorType annotations 
 * (javax.xml.bind.annotation.XmlAccessorType) on important fields, 
 * but it is not practical to enforce this restriction.
 * @author danielm
 */
public interface MobileDTO {
    
}
