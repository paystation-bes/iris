package com.digitalpaytech.dto.mobile.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Error")
@XmlAccessorType(XmlAccessType.FIELD)
public class MobileExceptionDTO implements MobileDTO {
	@XmlElement(name = "error")
	private String error;
	public MobileExceptionDTO(String error){
	    if(error == null){
	        this.error = "";
	    }else{
	        this.error = error;  
	    }
	}
	public MobileExceptionDTO(){
		error = "";
	}
	public String getError(){
		return error;
	}
	public void setError(String error){
		this.error = error;
	}
}
