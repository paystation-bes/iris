package com.digitalpaytech.dto.mobile.rest;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "paystation")
@XmlAccessorType(XmlAccessType.FIELD)
public class CollectPaystationStatusDTO {
	@XmlElement(name = "id")
	private int id;
	
	@XmlElement(name = "name")
	private String name;
	
	@XmlElement(name = "serialnumber")
	private String serialNumber;
	
	@XmlElement(name = "createdAlertTypes")
	private List<CollectionType> createdAlertTypes;
	
	@XmlElement(name = "clearedAlertTypes")
	private List<CollectionType> clearedAlertTypes;
	
	@XmlElement(name = "heartbeat")
	private Date heartBeat;
	
	private Date lastCoinCollection;
	private Date lastBillCollection;
	private Date lastCardCollection;
	private Date lastCollection;
	
	public Date getLastCoinCollection(){
		return lastCoinCollection;
	}
	public void setLastCoinCollection(Date lastCoinCollection){
		this.lastCoinCollection = lastCoinCollection;
	}
	public Date getLastBillCollection(){
		return lastBillCollection;
	}
	public void setLastBillCollection(Date lastBillCollection){
		this.lastBillCollection = lastBillCollection;
	}
	public Date getLastCardCollection(){
		return lastCardCollection;
	}
	public void setLastCardCollection(Date lastCardCollection){
		this.lastCardCollection = lastCardCollection;
	}
	public Date getLastCollection(){
		return lastCollection;
	}
	public void setLastCollection(Date lastCollection){
		this.lastCollection =lastCollection;
	}
	
	public CollectPaystationStatusDTO(){
		createdAlertTypes = new LinkedList<CollectionType>();
		clearedAlertTypes = new LinkedList<CollectionType>();
	}
	
	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}
	public String getName(){
	    return name;
	}
	public void setName(String name){
	    this.name = name;
	}
	public String getSerialNumber(){
	    return serialNumber;
	}
	public void setSerialNumber(String serialNumber){
	    this.serialNumber = serialNumber;
	}
	public List<CollectionType> getCreatedAlertTypes(){
		return createdAlertTypes;
	}
	public void addCreatedAlertType(String name, Date timestamp, int severity){
	    createdAlertTypes.add(new CollectionType(name, timestamp, severity));
	}
	public void addClearedAlertType(String name, Date timestamp){
	    clearedAlertTypes.add(new CollectionType(name, timestamp, 0));
	}
	public void setCreatedAlertTypes(List<CollectionType> createdAlertTypes){
		this.createdAlertTypes = createdAlertTypes;
	}
	
	public List<CollectionType> getClearedAlertTypes(){
	    return clearedAlertTypes;
	}
	public void setClearedAlertTypes(List<CollectionType> clearedAlertTypes){
	    this.clearedAlertTypes = clearedAlertTypes;
	}
	
	public Date getHeartBeat(){
	    return heartBeat;
	}
	public void setHeartBeat(Date heartBeat){
	    this.heartBeat = heartBeat;
	}
	
	@XmlRootElement(name = "collectiontype")
	@XmlAccessorType(XmlAccessType.FIELD)
	private class CollectionType{
		@XmlElement(name = "name")
		private String name;
		
		@XmlElement(name = "timestamp")
		private Date timestamp;
		
		@XmlElement(name = "severity")
		private int severity;
		
		public CollectionType(String name, Date timestamp, int severity){
			this.name = name;
			this.timestamp = timestamp;
			this.severity = severity;
		}
		
		public String getName(){
			return name;
		}
		public void setName(String name){
			this.name = name;
		}
		
		public Date getTimestamp(){
		    return timestamp;
		}
		public void setTimestamp(Date timestamp){
			this.timestamp = timestamp;
		}
		
		public int getSeverity(){
		    return severity;
		}
		public void setSeverity(int severity){
		    this.severity = severity;
		}
		
	}
}
