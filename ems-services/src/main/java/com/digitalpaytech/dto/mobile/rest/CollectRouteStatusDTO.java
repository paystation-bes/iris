package com.digitalpaytech.dto.mobile.rest;

import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "route")
@XmlAccessorType(XmlAccessType.FIELD)
public class CollectRouteStatusDTO {
	@XmlElement(name = "id")
	private int id;
	
	@XmlElement(name = "name")
    private String name;
	
	@XmlElement(name = "usercount")
	private int userCount;
	
	@XmlElement(name = "paystations")
	private List<CollectPaystationStatusDTO> paystations;
	
	public CollectRouteStatusDTO(){
		paystations = new LinkedList<CollectPaystationStatusDTO>();
	}
	
	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}
	public String getName(){
	    return name;
	}
	public void setName(String name){
	    this.name = name;
	}
	public int getUserCount(){
		return userCount;
	}
	public void setUserCount(int userCount){
		this.userCount = userCount;
	}
	public void incrementUserCount(int incrementBy){
		userCount += incrementBy;
	}
	
	public List<CollectPaystationStatusDTO> getPaystations(){
		return paystations;
	}
	public void setPaystations(List<CollectPaystationStatusDTO> paystations){
		this.paystations = paystations;
	}
	public void addPaystation(CollectPaystationStatusDTO paystation){
		paystations.add(paystation);
	}
	
	
}
