package com.digitalpaytech.dto.mobile.rest;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "summary")
@XmlAccessorType(XmlAccessType.FIELD)
public class CollectSummaryDTO implements MobileDTO {
	@XmlElement(name = "routes")
	List<CollectRouteSummaryDTO> routes;
	
	@XmlElement(name = "paystations")
	List<CollectPaystationSummaryDTO> paystations;
	
	public CollectSummaryDTO(){
		routes = new LinkedList<CollectRouteSummaryDTO>();
		paystations = new LinkedList<CollectPaystationSummaryDTO>();
	}
	
	public List<CollectRouteSummaryDTO> getRoutes(){
		return routes;
	}
	public void setRoutes(List<CollectRouteSummaryDTO> routes){
		this.routes = routes;
	}
	public void addRoute(CollectRouteSummaryDTO route){
		routes.add(route);
	}
	
	public List<CollectPaystationSummaryDTO> getPaystations(){
		return paystations;
	}
	public void setPaystations(List<CollectPaystationSummaryDTO> paystations){
		this.paystations = paystations;
	}
	public void addPaystation(CollectPaystationSummaryDTO paystation){
		paystations.add(paystation);
	}
}