package com.digitalpaytech.dto;

import com.digitalpaytech.util.WebCoreConstants;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("rateType")
public class RateType extends SettingsType {
    
    private static final long serialVersionUID = -4266325358194149693L;
    
    private String orgId;
    
    public RateType() {
        setStatus(WebCoreConstants.ACTIVE);
    }
    
    public final String getOrgId() {
        return this.orgId;
    }
    
    public final void setOrgId(final String orgId) {
        this.orgId = orgId;
    }
}
