package com.digitalpaytech.dto;

import java.util.Date;

public class PermitInfo {
    private Integer customerId;
    private Integer locationId; 
    private String licencePlate; 
    private Date startDate; 
    private Date endDate;
    private Date maxLastUpdatedTime;
    private int startRow; 
    private int endRow;
    
    public PermitInfo() {
    }

    public PermitInfo(Integer customerId, String licencePlate, Date startDate, Date endDate, Date maxLastUpdatedTime, int startRow, int endRow,
                        Integer locationId) {
        this.customerId = customerId;
        this.licencePlate = licencePlate;
        this.startDate = startDate;
        this.endDate = endDate;
        this.maxLastUpdatedTime = maxLastUpdatedTime;
        this.startRow = startRow;
        this.endRow = endRow;
        this.locationId = locationId;
    }
    
    public Integer getCustomerId() {
        return customerId;
    }
    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }
    public String getLicencePlate() {
        return licencePlate;
    }
    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }
    public Date getStartDate() {
        return startDate;
    }
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    public Date getEndDate() {
        return endDate;
    }
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    public Date getMaxLastUpdatedTime() {
        return this.maxLastUpdatedTime;
    }
    public void setMaxLastUpdatedTime(Date maxLastUpdatedTime) {
        this.maxLastUpdatedTime = maxLastUpdatedTime;
    }
    public int getStartRow() {
        return startRow;
    }
    public void setStartRow(int startRow) {
        this.startRow = startRow;
    }
    public int getEndRow() {
        return endRow;
    }
    public void setEndRow(int endRow) {
        this.endRow = endRow;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }
}
