package com.digitalpaytech.dto.comparator;

import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;

import com.digitalpaytech.dto.customeradmin.AlertCentreAlertInfo;

public class AlertCentreAlertDateComparator implements Comparator<AlertCentreAlertInfo>{

	@Override
	public int compare(AlertCentreAlertInfo o1, AlertCentreAlertInfo o2) {
		
		Date date1 = o1.getDate();
		Date date2 = o2.getDate();
		
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date1);
		cal1.set(Calendar.SECOND, 0);

		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(date2);
		cal2.set(Calendar.SECOND, 0);
		
		return cal1.compareTo(cal2);	
	}

}
