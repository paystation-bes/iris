package com.digitalpaytech.dto.comparator;

import java.util.Comparator;

import com.digitalpaytech.dto.customeradmin.AlertCentreAlertInfo;

public class AlertCentreAlertPayStationComparator implements Comparator<AlertCentreAlertInfo>{

	@Override
	public int compare(AlertCentreAlertInfo o1, AlertCentreAlertInfo o2) {

		return o1.getModule().compareTo(o2.getModule());	
	}

}
