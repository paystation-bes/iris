package com.digitalpaytech.dto.comparator;

import java.util.Comparator;

import com.digitalpaytech.dto.customeradmin.AlertCentreAlertInfo;

public class AlertCentreAlertSeverityComparator implements Comparator<AlertCentreAlertInfo>{

	@Override
	public int compare(AlertCentreAlertInfo o1, AlertCentreAlertInfo o2) {

		return o1.getSeverity() - (o2.getSeverity());
				
	}
}
