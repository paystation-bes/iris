package com.digitalpaytech.dto.comparator;

import java.util.Comparator;

import com.digitalpaytech.dto.WidgetSeries;

public class WidgetSeriesComparator implements Comparator<WidgetSeries> {
    
    @Override
    public int compare(WidgetSeries entry1, WidgetSeries entry2) {
        
        String name1 = entry1.getSeriesName();
        String name2 = entry2.getSeriesName();
        
        return name1.toLowerCase().compareTo(name2.toLowerCase());
    }
}
