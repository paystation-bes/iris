package com.digitalpaytech.dto.comparator;

import java.util.Comparator;

import com.digitalpaytech.domain.CustomerCard;

public class CustomerCardComparator implements Comparator<CustomerCard> {
	@Override
	public int compare(CustomerCard card1, CustomerCard card2) {
		int result = card1.getCardNumber().compareTo(card2.getCardNumber());
		if(result == 0) {
			if(card1.getCustomerCardType() != null) {
				if(card2.getCustomerCardType() != null) {
					result = card1.getCustomerCardType().getName().compareTo(card2.getCustomerCardType().getName());
				}
				else {
					result = -1;
				}
			}
			else {
				if(card2.getCustomerCardType() == null) {
					result = 0;
				}
				else {
					result = 1;
				}
			}
		}
		
		return result;
	}
}
