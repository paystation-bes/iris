package com.digitalpaytech.dto.comparator;

import java.util.Comparator;

import com.digitalpaytech.dto.customeradmin.ActivityLogEntry;

public class UserNameComparator implements Comparator<ActivityLogEntry>{

	@Override
	public int compare(ActivityLogEntry entry1, ActivityLogEntry entry2) {
		
		String userName1 = entry1.getUserName();
		String userName2 = entry2.getUserName();
		
		return userName1.compareToIgnoreCase(userName2);
	}

}
