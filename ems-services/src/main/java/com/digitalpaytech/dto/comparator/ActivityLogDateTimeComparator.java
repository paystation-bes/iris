package com.digitalpaytech.dto.comparator;

import java.util.Comparator;
import java.util.Date;

import com.digitalpaytech.dto.customeradmin.ActivityLogEntry;

public class ActivityLogDateTimeComparator implements Comparator<ActivityLogEntry>{

	@Override
	public int compare(ActivityLogEntry entry1, ActivityLogEntry entry2) {
		
		Date date1 = entry1.getDateTime();
		Date date2 = entry2.getDateTime();
		
		return date1.compareTo(date2);
	}	
}
