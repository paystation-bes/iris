package com.digitalpaytech.dto.comparator;

import java.util.Comparator;

import com.digitalpaytech.dto.customeradmin.CollectionsCentreCollectionInfo;


public class CollectionsCentrePayStationComparator implements Comparator<CollectionsCentreCollectionInfo>{

	@Override
	public int compare(CollectionsCentreCollectionInfo o1, CollectionsCentreCollectionInfo o2) {

		return o1.getPointOfSaleName().compareTo(o2.getPointOfSaleName());	
	}


}
