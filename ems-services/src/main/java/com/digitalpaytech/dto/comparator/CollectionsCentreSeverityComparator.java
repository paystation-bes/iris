package com.digitalpaytech.dto.comparator;

import java.util.Comparator;

import com.digitalpaytech.dto.customeradmin.CollectionsCentreCollectionInfo;

public class CollectionsCentreSeverityComparator implements Comparator<CollectionsCentreCollectionInfo>{

	@Override
	public int compare(CollectionsCentreCollectionInfo o1, CollectionsCentreCollectionInfo o2) {

		return o1.getSeverity() - (o2.getSeverity());
	}
}
