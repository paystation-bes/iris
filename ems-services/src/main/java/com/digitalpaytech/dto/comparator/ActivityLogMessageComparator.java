package com.digitalpaytech.dto.comparator;

import java.util.Comparator;

import com.digitalpaytech.dto.customeradmin.ActivityLogEntry;

public class ActivityLogMessageComparator implements Comparator<ActivityLogEntry> {

	@Override
	public int compare(ActivityLogEntry entry1, ActivityLogEntry entry2) {

		String message1 = entry1.getActivityMessage();
		String message2 = entry2.getActivityMessage();
		
		return message1.compareToIgnoreCase(message2);
	}

}
