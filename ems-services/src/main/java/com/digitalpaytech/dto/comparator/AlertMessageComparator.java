package com.digitalpaytech.dto.comparator;

import java.util.Comparator;

import com.digitalpaytech.dto.customeradmin.AlertInfoEntry;

public class AlertMessageComparator implements Comparator<AlertInfoEntry>{

	@Override
	public int compare(AlertInfoEntry entry1, AlertInfoEntry entry2) {
		
		String alert1 = entry1.getAlertMessage();
		String alert2 = entry2.getAlertMessage();
		
		return alert1.compareTo(alert2);
	}	

}
