package com.digitalpaytech.dto.comparator;

import java.util.Comparator;

import com.digitalpaytech.domain.Coupon;

public class CouponComparator implements Comparator<Coupon> {
    
    @Override
    public final int compare(final Coupon c1, final Coupon c2) {
        int result = c1.getCoupon().length() - c2.getCoupon().length();
        if (result == 0) {
            result = c1.getCoupon().compareTo(c2.getCoupon());
        }
        
        return result;
    }
    
}
