package com.digitalpaytech.dto.comparator;

import java.util.Comparator;
import java.util.Date;

import com.digitalpaytech.dto.customeradmin.AlertInfoEntry;

public class AlertDateTimeComparator implements Comparator<AlertInfoEntry> {
    
    @Override
    public int compare(AlertInfoEntry entry1, AlertInfoEntry entry2) {
        
        Date date1 = entry1.getDate();
        Date date2 = entry2.getDate();
        
        return date1.compareTo(date2);
    }
    
}
