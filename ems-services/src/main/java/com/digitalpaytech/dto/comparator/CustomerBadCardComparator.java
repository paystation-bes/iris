package com.digitalpaytech.dto.comparator;

import java.util.Comparator;

import com.digitalpaytech.domain.CustomerBadCard;

public class CustomerBadCardComparator implements Comparator<CustomerBadCard> {
	@Override
	public int compare(CustomerBadCard card1, CustomerBadCard card2) {
		int result = card1.getCardNumberOrHash().compareTo(card2.getCardNumberOrHash());
		if(result == 0) {
			result = card1.getCustomerCardType().getName().compareTo(card2.getCustomerCardType().getName());
			if(result == 0) {
				if(card1.getLast4digitsOfCardNumber() == null) {
					if(card2.getLast4digitsOfCardNumber() != null) {
						result = -1;
					}
				}
				else {
					if(card2.getLast4digitsOfCardNumber() == null) {
						result = 1;
					}
					else {
						result = card1.getLast4digitsOfCardNumber().compareTo(card2.getLast4digitsOfCardNumber());
					}
				}
			}
		}
		
		return result;
	}
}
