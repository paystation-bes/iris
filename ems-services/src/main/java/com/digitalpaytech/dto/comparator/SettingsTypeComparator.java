package com.digitalpaytech.dto.comparator;

import java.util.Comparator;

import com.digitalpaytech.dto.SettingsType;

public class SettingsTypeComparator implements Comparator<SettingsType>{

	@Override
	public int compare(SettingsType o1, SettingsType o2) {

		if(o1.getName().equals("N/A")){
			return -1;
		}
		else if(o2.getName().equals("N/A")){
			return 1;
		}
		return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());	
	}

}
