package com.digitalpaytech.dto.comparator;

import java.util.Comparator;

import com.digitalpaytech.dto.customeradmin.LocationTree;

public class LocationTreeComparator  implements Comparator<LocationTree> {

    @Override
    public int compare(LocationTree locationTree1, LocationTree locationTree2) {

        String name1 = locationTree1.getName();
        String name2 = locationTree2.getName();
        if (locationTree1.isUnassigned()) {
            return 1;
        } else if (locationTree2.isUnassigned()) {
            return -1;
        } 
        return name1.toLowerCase().compareTo(name2.toLowerCase());
    }
    
}
