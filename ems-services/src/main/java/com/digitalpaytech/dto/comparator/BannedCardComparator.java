package com.digitalpaytech.dto.comparator;

import java.util.Comparator;

import com.digitalpaytech.dto.customeradmin.BannedCard;

public class BannedCardComparator implements Comparator<BannedCard> {
    private static final int EQUAL = 0;
    private static final int BEFORE = -1;
    private static final int AFTER = 1;
    
    @Override
    public int compare(final BannedCard card1, final BannedCard card2) {
        
        int result = card1.getCardType().compareTo(card2.getCardType());
        if (result == EQUAL) {
            if (card1.getLast4DigitsOfCardNumber() == null) {
                if (card2.getLast4DigitsOfCardNumber() != null) {
                    result = BEFORE;
                }
            } else {
                if (card2.getLast4DigitsOfCardNumber() == null) {
                    result = AFTER;
                } else {
                    result = card1.getLast4DigitsOfCardNumber().compareTo(card2.getLast4DigitsOfCardNumber());
                    
                    if (result == EQUAL) {
                        result = card1.getCardNumber().compareTo(card2.getCardNumber());
                    }
                }
            }
        }
        
        return result;
    }
}
