package com.digitalpaytech.dto;

import java.io.Serializable;
import java.util.Date;

public class FlexLocationFacilitySearchCriteria implements Serializable {
    private static final long serialVersionUID = -2763046855009724876L;    
    private Integer customerId;
    private Integer locationId;
    private Integer flexLocationFacilityId;
    
    // this is the location id for the Location detail page 
    // that we are loading the list into
    private Integer selectedLocationId;
    
    private Integer page;
    private Integer itemsPerPage;
    
    
    private Date maxUpdatedTime;
    
    public final Integer getPage() {
        return this.page;
    }

    public final void setPage(final Integer page) {
        this.page = page;
    }

    public final Integer getItemsPerPage() {
        return this.itemsPerPage;
    }

    public final void setItemsPerPage(final Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

    public final Integer getCustomerId() {
        return this.customerId;
    }

    public final void setCustomerId(final Integer customerId) {
        this.customerId = customerId;
    }

    public final Integer getLocationId() {
        return this.locationId;
    }

    public final void setLocationId(final Integer locationId) {
        this.locationId = locationId;
    }

    public final Integer getSelectedLocationId() {
        return this.selectedLocationId;
    }

    public final void setSelectedLocationId(final Integer selectedLocationId) {
        this.selectedLocationId = selectedLocationId;
    }

    public final Integer getFlexLocationFacilityId() {
        return this.flexLocationFacilityId;
    }

    public final void setFlexLocationFacilityId(final Integer flexLocationFacilityId) {
        this.flexLocationFacilityId = flexLocationFacilityId;
    }

    public final Date getMaxUpdatedTime() {
        return this.maxUpdatedTime;
    }

    public final void setMaxUpdatedTime(final Date maxUpdatedTime) {
        this.maxUpdatedTime = maxUpdatedTime;
    }    
}
