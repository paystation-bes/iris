package com.digitalpaytech.dto;

import java.io.Serializable;
import java.util.Collection;

import com.digitalpaytech.util.WebCoreConstants;

public class CustomerMigrationSearchCriteria implements Serializable {
    
    private static final long serialVersionUID = -5793267139019916166L;
    private Integer page = 0;
    private Integer itemsPerPage = WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT;
    private Integer targetId;
    
    private String exclusiveOrderBy;
    private String exclusiveOrderDir;
    
    private Collection<Byte> customerMigrationStatusTypeIds;
    
    public CustomerMigrationSearchCriteria() {
        
    }
    
    public final Integer getPage() {
        return this.page;
    }
    
    public final void setPage(final Integer page) {
        this.page = page;
    }
    
    public final Integer getItemsPerPage() {
        return this.itemsPerPage;
    }
    
    public final void setItemsPerPage(final Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }
    
    public final Integer getTargetId() {
        return this.targetId;
    }
    
    public final void setTargetId(final Integer targetId) {
        this.targetId = targetId;
    }
    
    public final String getExclusiveOrderBy() {
        return this.exclusiveOrderBy;
    }
    
    public final void setExclusiveOrderBy(final String exclusiveOrderBy) {
        this.exclusiveOrderBy = exclusiveOrderBy;
    }
    
    public final String getExclusiveOrderDir() {
        return this.exclusiveOrderDir;
    }
    
    public final void setExclusiveOrderDir(final String exclusiveOrderDir) {
        this.exclusiveOrderDir = exclusiveOrderDir;
    }
    
    public final Collection<Byte> getCustomerMigrationStatusTypeIds() {
        return this.customerMigrationStatusTypeIds;
    }
    
    public final void setCustomerMigrationStatusTypeIds(final Collection<Byte> customerMigrationStatusTypeIds) {
        this.customerMigrationStatusTypeIds = customerMigrationStatusTypeIds;
    }
    
}
