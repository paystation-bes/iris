package com.digitalpaytech.dto;

import java.io.Serializable;

import com.digitalpaytech.service.EncryptionService;

public class EncryptionInfo implements Serializable {
    
    private static final long serialVersionUID = -9163995026123772234L;
    private String operationMode;
    private String keyPresent;
    private String provider;
    private String cardSerialNumber;
    
    public EncryptionInfo() {
        this.operationMode = EncryptionService.PRODUCTION;
        this.keyPresent = "No";
        this.provider = "";
        this.cardSerialNumber = "N/A";
    }
    
    public final void setOperationMode(final String operationMode) {
        this.operationMode = operationMode;
    }
    
    public final String getOperationMode() {
        return this.operationMode;
    }
    
    public final void setKeyPresent(final String keyPresent) {
        this.keyPresent = keyPresent;
    }
    
    public final String getKeyPresent() {
        return this.keyPresent;
    }
    
    public final void setProvider(final String provider) {
        this.provider = provider;
    }
    
    public final String getProvider() {
        return this.provider;
    }
    
    public final void setCardSerialNumber(final String cardSerialNumber) {
        this.cardSerialNumber = cardSerialNumber;
    }
    
    public final String getCardSerialNumber() {
        return this.cardSerialNumber;
    }
}
