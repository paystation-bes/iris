package com.digitalpaytech.dto;

import java.util.Date;

public class PointOfSaleAlertInfo {
    private Integer posId;
    private String posName;
    private String serialNumber;
    private Date heartBeat;
    private byte isDeleted;
    private Integer eventSeverity;
    private Date created;
    private Date cleared;    
    private Short alertTypeId;
    private Integer severity;
    private Boolean isActive;
    private Date lastCoinCollection;
    private Date lastBillCollection;
    private Date lastCardCollection;
    private Date lastCollection;
    
    public PointOfSaleAlertInfo() {
        this.lastCollection = null;
    }
    
    public final Date getLastCollection() {
        return this.lastCollection;
    }
    
    public final void setLastCollection(final Date lastCollection) {
        this.lastCollection = lastCollection;
    }
    
    public final Date getLastCardCollection() {
        return this.lastCardCollection;
    }
    
    public final void setLastCardCollection(final Date lastCardCollection) {
        if (lastCardCollection != null) {
            if (this.lastCollection == null || lastCardCollection.after(this.lastCollection)) {
                this.lastCollection = lastCardCollection;
            }
        }
        this.lastCardCollection = lastCardCollection;
    }
    
    public final Date getLastBillCollection() {
        return this.lastBillCollection;
    }
    
    public final void setLastBillCollection(final Date lastBillCollection) {
        if (lastBillCollection != null) {
            if (this.lastCollection == null || lastBillCollection.after(this.lastCollection)) {
                this.lastCollection = lastBillCollection;
            }
        }
        this.lastBillCollection = lastBillCollection;
    }
    
    public final Date getLastCoinCollection() {
        return this.lastCoinCollection;
    }
    
    public final void setLastCoinCollection(final Date lastCoinCollection) {
        if (lastCoinCollection != null) {
            if (this.lastCollection == null || lastCoinCollection.after(this.lastCollection)) {
                this.lastCollection = lastCoinCollection;
            }
        }
        this.lastCoinCollection = lastCoinCollection;
    }
    
    public final Integer getPosId() {
        return this.posId;
    }
    
    public final void setPosId(final Integer posId) {
        this.posId = posId;
    }
    
    public final String getPosName() {
        return this.posName;
    }
    
    public final void setPosName(final String posName) {
        this.posName = posName;
    }
    
    public final String getSerialNumber() {
        return this.serialNumber;
    }
    
    public final void setSerialNumber(final String serialNumber) {
        this.serialNumber = serialNumber;
    }
    
    public final Date getHeartBeat() {
        return this.heartBeat;
    }
    
    public final void setHeartBeat(final Date heartBeat) {
        this.heartBeat = heartBeat;
    }
    
    public final byte getIsDeleted() {
        return this.isDeleted;
    }
    
    public final void setIsDeleted(final byte isDeleted) {
        this.isDeleted = isDeleted;
    }
    
    public final Integer getEventSeverity() {
        return this.eventSeverity;
    }
    
    public final void setEventSeverity(final Integer eventSeverity) {
        this.eventSeverity = eventSeverity;
    }
    
    public final Date getCreated() {
        return this.created;
    }
    
    public final void setCreated(final Date created) {
        this.created = created;
    }
    
    public final Date getCleared() {
        return this.cleared;
    }
    
    public final void setCleared(final Date cleared) {
        this.cleared = cleared;
    }    
    
    public final Boolean getIsActive() {
        return this.isActive;
    }
    
    public final void setIsActive(final Boolean isActive) {
        this.isActive = isActive;
    }

    public Short getAlertTypeId() {
        return alertTypeId;
    }

    public void setAlertTypeId(Short alertTypeId) {
        this.alertTypeId = alertTypeId;
    }

    public Integer getSeverity() {
        return severity;
    }

    public void setSeverity(Integer severity) {
        this.severity = severity;
    }
}
