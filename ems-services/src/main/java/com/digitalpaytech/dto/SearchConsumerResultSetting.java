package com.digitalpaytech.dto;

public class SearchConsumerResultSetting {
    private String consumerRandomId;
    private String firstName;
    private String lastName;
    private String email;
    
    public SearchConsumerResultSetting() {
    }
    
    public SearchConsumerResultSetting(final String consumerRandomId, final String firstName, final String lastName, final String email) {
        this.consumerRandomId = consumerRandomId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }
    
    public final String getConsumerRandomId() {
        return this.consumerRandomId;
    }
    
    public final void setConsumerRandomId(final String consumerRandomId) {
        this.consumerRandomId = consumerRandomId;
    }
    
    public final String getEmail() {
        return this.email;
    }
    
    public final void setEmail(final String email) {
        this.email = email;
    }
    
    public final String getFirstName() {
        return this.firstName;
    }
    
    public final void setFirstName(final String firstName) {
        this.firstName = firstName;
    }
    
    public final String getLastName() {
        return this.lastName;
    }
    
    public final void setLastName(final String lastName) {
        this.lastName = lastName;
    }
}
