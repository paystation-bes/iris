package com.digitalpaytech.dto;

public class JsonKeyValuePair {
    private String key;
    private String value;
    
    public JsonKeyValuePair() {
        
    }
    
    public JsonKeyValuePair(final String key, final String value) {
        this.key = key;
        this.value = value;
    }
    
    public final String getKey() {
        return this.key;
    }
    
    public final void setKey(final String key) {
        this.key = key;
    }
    
    public final String getValue() {
        return this.value;
    }
    
    public final void setValue(final String value) {
        this.value = value;
    }
}
