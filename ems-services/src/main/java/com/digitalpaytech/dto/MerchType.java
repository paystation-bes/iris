package com.digitalpaytech.dto;

import com.digitalpaytech.util.WebCoreConstants;

public class MerchType extends SettingsType {
    private static final long serialVersionUID = 2023302974946311377L;
    
    public MerchType() {
        setStatus(WebCoreConstants.ACTIVE);
    }
}
