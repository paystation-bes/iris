package com.digitalpaytech.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.comparators.ComparatorChain;

import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.dto.comparator.WidgetSeriesComparator;
import com.digitalpaytech.util.WidgetConstants;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

public class WidgetData implements Serializable {
    
    private static final long serialVersionUID = -1162580317393641540L;
    
    @XStreamOmitField
    private Map<String, WidgetSeries> seriesMap;
    
    @XStreamImplicit(itemFieldName = "seriesList")
    private List<WidgetSeries> seriesList;
    @XStreamImplicit(itemFieldName = "tier1Names")
    private List<String> tier1Names;
    @XStreamImplicit(itemFieldName = "trendValue")
    //may be single value for specified trend value, or list for trend by location
    private List<String> trendValue;
    @XStreamImplicit(itemFieldName = "listData")
    private List<WidgetDataListEntry> listData;
    @XStreamImplicit(itemFieldName = "listHeaders")
    private List<String> listHeaders;
    @XStreamImplicit(itemFieldName = "listHeadersSortBy")
    private List<Boolean> listHeadersSortBy;
    private String sortByLabel;
    
    private WidgetMapInfo widgetMapInfo;
    
    private String singleValue;
    private String widgetName;
    private String timeInterval;
    private long startTime;
    private int chartType;
    private int metricTypeId;
    private int rangeTypeId;
    private boolean isEmpty;
    private boolean isZeroValues;
    private boolean isLimitExceeded;
    
    private String randomId;
    
    public WidgetData() {
        this.isZeroValues = true;
        this.widgetMapInfo = new WidgetMapInfo();
        this.seriesMap = new HashMap<String, WidgetSeries>();
        this.seriesList = new ArrayList<WidgetSeries>();
        this.tier1Names = new ArrayList<String>();
        this.trendValue = new ArrayList<String>();
        this.listHeaders = new ArrayList<String>();
        this.listHeadersSortBy = new ArrayList<Boolean>();
    }
    
    public WidgetData(final Map<String, WidgetSeries> seriesMap, final List<String> tier1Names, final int chartType, final String timeInterval,
            final long startTime, final String singleValue, final List<String> trendValue) {
        this.seriesMap = seriesMap;
        this.tier1Names = tier1Names;
        this.chartType = chartType;
        this.timeInterval = timeInterval;
        this.startTime = startTime;
        this.singleValue = singleValue;
    }
    
    // Creates an array used for list type widgets, an array for list type headings, 
    // a boolean array for which header is sorted as default, and a label for descending or ascending column ("asc or desc, where list type = 1")
    public final void setListData(final Widget widget) {
        this.listData = new ArrayList<WidgetDataListEntry>();
        for (int n = 0; n < this.tier1Names.size(); n++) {
            this.listData.add(new WidgetDataListEntry());
            this.listData.get(n).getEntry().add(this.tier1Names.get(n));
            for (int i = 0; i < this.seriesList.size(); i++) {
                this.listData.get(n).getEntry().add(this.seriesList.get(i).getSeriesData().get(n));
            }
        }
        this.listHeaders.add(widget.getWidgetTierTypeByWidgetTier1Type().getName());
        for (int i = 0; i < this.seriesList.size(); i++) {
            this.listHeaders.add(this.seriesList.get(i).getSeriesName());
        }
        if (widget.getWidgetChartType().getId() == WidgetConstants.CHART_TYPE_LIST) {
            // Revenue
            if (widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_REVENUE) {
                if (widget.getWidgetFilterType().getId() == WidgetConstants.FILTER_TYPE_TOP) {
                    this.listHeadersSortBy.add(false);
                    this.listHeadersSortBy.add(true);
                    this.sortByLabel = "desc:1";
                } else if (widget.getWidgetFilterType().getId() == WidgetConstants.FILTER_TYPE_BOTTOM) {
                    this.listHeadersSortBy.add(false);
                    this.listHeadersSortBy.add(true);
                    this.sortByLabel = "asc:1";
                } else {
                    this.listHeadersSortBy.add(true);
                    this.listHeadersSortBy.add(false);
                    this.sortByLabel = "asc:0";
                }
                // Map
            } else if (widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_ALERT_STATUS) {
                // DO NOTHING.
            } else {
                this.listHeadersSortBy.add(true);
                this.listHeadersSortBy.add(false);
                this.sortByLabel = "asc:0";
            }
        }
    }
    
    public final void setListHeadersSortBy(final List<Boolean> listHeadersSortBy) {
        this.listHeadersSortBy = listHeadersSortBy;
    }
    
    public final List<Boolean> setListHeadersSortBy() {
        return this.listHeadersSortBy;
    }
    
    public final void setSortByLabel(final String sortByLabel) {
        this.sortByLabel = sortByLabel;
    }
    
    public final String getSortByLabel() {
        return this.sortByLabel;
    }
    
    public final List<WidgetDataListEntry> getListData() {
        return this.listData;
    }
    
    public final WidgetMapInfo getWidgetMapInfo() {
        return this.widgetMapInfo;
    }
    
    public final void setWidgetMapInfo(final WidgetMapInfo widgetMapInfo) {
        this.widgetMapInfo = widgetMapInfo;
    }
    
    public final void setSeriesDataFromMap() {
        this.seriesList = new ArrayList<WidgetSeries>(this.seriesMap.values());
        if (this.seriesList != null) {
            final ComparatorChain chain = new ComparatorChain();
            
            chain.addComparator(new WidgetSeriesComparator(), false);
            
            Collections.sort(this.seriesList, chain);
        }
    }
    
    public final Map<String, WidgetSeries> getSeriesMap() {
        return this.seriesMap;
    }
    
    public final void setSeriesMap(final Map<String, WidgetSeries> seriesMap) {
        this.seriesMap = seriesMap;
    }
    
    public final void addToSeriesMap(final WidgetSeries widgetSeries, final String stackName, final String seriesName) {
        final String key = stackName + "_" + seriesName;
        this.seriesMap.put(key, widgetSeries);
    }
    
    public final List<String> getTier1Names() {
        return this.tier1Names;
    }
    
    public final void setTier1Names(final List<String> tier1Names) {
        this.tier1Names = tier1Names;
    }
    
    public final void addTier1Names(final String tier1Name) {
        this.tier1Names.add(tier1Name);
    }
    
    public final String getTimeInterval() {
        return this.timeInterval;
    }
    
    public final void setTimeInterval(final String timeInterval) {
        this.timeInterval = timeInterval;
    }
    
    public final long getStartTime() {
        return this.startTime;
    }
    
    public final void setStartTime(final long startTime) {
        this.startTime = startTime;
    }
    
    public final int getChartType() {
        return this.chartType;
    }
    
    public final void setChartType(final int chartType) {
        this.chartType = chartType;
    }
    
    public final String getSingleValue() {
        return this.singleValue;
    }
    
    public final void setSingleValue(final String singleValue) {
        this.singleValue = singleValue;
    }
    
    public final String getWidgetName() {
        return this.widgetName;
    }
    
    public final void setWidgetName(final String widgetName) {
        this.widgetName = widgetName;
    }
    
    public final List<String> getTrendValue() {
        return this.trendValue;
    }
    
    public final void setTrendValue(final List<String> trendValue) {
        this.trendValue = trendValue;
    }
    
    public final void addTrendValue(final String newTrendValue) {
        this.trendValue.add(newTrendValue);
    }
    
    public final int getMetricTypeId() {
        return this.metricTypeId;
    }
    
    public final void setMetricTypeId(final int metricTypeId) {
        this.metricTypeId = metricTypeId;
    }
    
    public final int getRangeTypeId() {
        return this.rangeTypeId;
    }
    
    public final void setRangeTypeId(final int rangeTypeId) {
        this.rangeTypeId = rangeTypeId;
    }
    
    public final boolean isEmpty() {
        return this.isEmpty;
    }
    
    public final void setIsEmpty(final boolean isEmpty) {
        this.isEmpty = isEmpty;
    }
    
    public final boolean isZeroValues() {
        return this.isZeroValues;
    }
    
    public final void setIsZeroValues(final boolean isZeroValues) {
        this.isZeroValues = isZeroValues;
    }
    
    public final boolean isLimitExceeded() {
        return this.isLimitExceeded;
    }
    
    public final void setIsLimitExceeded(final boolean isLimitExceeded) {
        this.isLimitExceeded = isLimitExceeded;
    }
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final List<WidgetSeries> getSeriesList() {
        return this.seriesList;
    }
}
