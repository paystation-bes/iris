package com.digitalpaytech.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BadCardFileInfo implements Serializable {
    private static final long serialVersionUID = -3022703434987420852L;

    @JsonProperty
    private String customerId;

    @JsonProperty
    private String fileCreationUTC;
    
    @JsonProperty
    private String hash;
    
    public BadCardFileInfo() {
    }

    public final String getCustomerId() {
        return this.customerId;
    }

    public final void setCustomerId(final String customerId) {
        this.customerId = customerId;
    }

    public final String getFileCreationUTC() {
        return this.fileCreationUTC;
    }

    public final void setFileCreationUTC(final String fileCreationUTC) {
        this.fileCreationUTC = fileCreationUTC;
    }

    public final String getHash() {
        return this.hash;
    }

    public final void setHash(final String hash) {
        this.hash = hash;
    }
}
