package com.digitalpaytech.dto.rest;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Coupons")
@XmlAccessorType(XmlAccessType.FIELD)
public class RESTCoupons {
    @XmlElement(name = "Coupon")
    private List<RESTCoupon> coupons;
    
    public List<RESTCoupon> getCoupons() {
        return coupons;
    }
    
    public void setCoupons(List<RESTCoupon> coupons) {
        this.coupons = coupons;
    }
}
