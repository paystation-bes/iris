package com.digitalpaytech.dto.rest.paystation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "TransactionResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaystationTransactionResponse extends PaystationResponse {
    @XmlElement(name = "PurchaseId")
    private Long purchaseId;
    @XmlElement(name = "AuthorizationId")
    private String authorizationId;
    
    public Long getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(Long purchaseId) {
        this.purchaseId = purchaseId;
    }

    public String getAuthorizationId() {
        return authorizationId;
    }

    public void setAuthorizationId(String authorizationId) {
        this.authorizationId = authorizationId;
    }

    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(this.getClass().getName());
        str.append(", PurchaseId=").append(this.purchaseId);
        str.append(", AuthorizationId=").append(this.authorizationId);
        str.append(super.toString());
        str.append("}");
        return str.toString();
    }
}
