package com.digitalpaytech.dto.rest.paystation;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.digitalpaytech.util.PaystationConstants;

public class PaystationQueryParamList {
    private String serialNumber;
    private String signature;
    private String signatureVersion;
    private String version;
    private String timestamp;
    
    public String getSerialNumber() {
        return serialNumber;
    }
    
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
    
    public String getSignature() {
        return signature;
    }
    
    public void setSignature(String signature) {
        this.signature = signature;
    }
    
    public String getSignatureVersion() {
        return signatureVersion;
    }
    
    public void setSignatureVersion(String signatureVersion) {
        this.signatureVersion = signatureVersion;
    }
    
    public String getVersion() {
        return version;
    }
    
    public void setVersion(String version) {
        this.version = version;
    }
    
    public String getTimestamp() {
        return timestamp;
    }
    
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
    
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(this.getClass().getName());
        str.append(" {SerialNumber=").append(this.serialNumber);
        str.append(", Signature=").append(this.signature);
        str.append(", SignatureVersion=").append(this.signatureVersion);
        str.append(", Timestamp=").append(this.timestamp);
        str.append(", Version=").append(this.version);
        str.append("}");
        return str.toString();
    }
    
    public String getFormatParamString() throws UnsupportedEncodingException {
        StringBuilder str = new StringBuilder();
        str.append("SerialNumber").append(PaystationConstants.EQUAL_SIGN)
                .append(URLEncoder.encode(this.serialNumber.trim(), PaystationConstants.HTTP_REST_ENCODE_CHARSET));
        str.append(PaystationConstants.AMPERSAND);
        str.append("SignatureVersion").append(PaystationConstants.EQUAL_SIGN)
                .append(URLEncoder.encode(this.signatureVersion.trim(), PaystationConstants.HTTP_REST_ENCODE_CHARSET));
        str.append(PaystationConstants.AMPERSAND);
        str.append("Timestamp").append(PaystationConstants.EQUAL_SIGN)
                .append(URLEncoder.encode(this.timestamp.trim(), PaystationConstants.HTTP_REST_ENCODE_CHARSET));
        str.append(PaystationConstants.AMPERSAND);
        str.append("Version").append(PaystationConstants.EQUAL_SIGN)
                .append(URLEncoder.encode(this.version.trim(), PaystationConstants.HTTP_REST_ENCODE_CHARSET));
        return str.toString();
    }
}
