package com.digitalpaytech.dto.rest;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang.StringUtils;

import com.digitalpaytech.util.RestCoreConstants;

public class RESTCouponQueryParamList extends RESTQueryParamList {
    private String token;
    private String couponCodes;
    private String couponCode;
    
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(this.getClass().getName());
        str.append(super.toString());
        str.append(" {Token=").append(this.token);
        return str.toString();
    }
    
    /*
     * Re-build the url query string without 'Signature'. It has 'SignatureVersion', 'Timestamp', 'Token' and 'CouponCodes' if exists.
     */
    @Override
    public String getFormatParamString() throws UnsupportedEncodingException {
        StringBuilder str = new StringBuilder();
        
        if (StringUtils.isNotBlank(couponCodes)) {
            str.append("CouponCodes").append(RestCoreConstants.EQUAL_SIGN).append(URLEncoder.encode(this.couponCodes, RestCoreConstants.HTTP_REST_ENCODE_CHARSET));
            str.append(RestCoreConstants.AMPERSAND);
        }
        str.append("SignatureVersion").append(RestCoreConstants.EQUAL_SIGN)
                .append(URLEncoder.encode(this.getSignatureVersion().trim(), RestCoreConstants.HTTP_REST_ENCODE_CHARSET));
        str.append(RestCoreConstants.AMPERSAND).append("Timestamp").append(RestCoreConstants.EQUAL_SIGN)
                .append(URLEncoder.encode(this.getTimestamp().trim(), RestCoreConstants.HTTP_REST_ENCODE_CHARSET));
        str.append(RestCoreConstants.AMPERSAND).append("Token").append(RestCoreConstants.EQUAL_SIGN)
                .append(URLEncoder.encode(this.token.trim(), RestCoreConstants.HTTP_REST_ENCODE_CHARSET));
        return str.toString();
    }
    
    public String getToken() {
        return token;
    }
    
    public void setToken(String token) {
        this.token = token;
    }
    
    public String getCouponCodes() {
        return couponCodes;
    }
    
    public void setCouponCodes(String couponCodes) {
        this.couponCodes = couponCodes;
    }
    public String getCouponCode() {
        return couponCode;
    }
    
    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }    
}
