package com.digitalpaytech.dto.rest.paystation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "LotSettingNotification")
@XmlAccessorType(XmlAccessType.FIELD)
public class LotSettingNotification {
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(", LotSettingNotification=true");
        return str.toString();
    }
}
