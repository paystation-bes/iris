package com.digitalpaytech.dto.rest;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Payment")
@XmlAccessorType(XmlAccessType.FIELD)
public class RESTPayment {
    @XmlAttribute(name = "Type")
    private String type;
    @XmlElement(name = "Amount")
    private BigDecimal amount;
    @XmlElement(name = "CardType")
    private String cardType;
    @XmlElement(name = "Last4DigitsOfCard")
    private String last4DigitsOfCard;
    @XmlElement(name = "CardAuthorizationId")
    private String cardAuthorizationId;
    @XmlElement(name = "PaymentDate")
    private String paymentDate;
    
    public String getType() {
        return this.type;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
    public BigDecimal getAmount() {
        return this.amount;
    }
    
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    
    public String getCardType() {
        return this.cardType;
    }
    
    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
    
    public String getLast4DigitsOfCard() {
        return this.last4DigitsOfCard;
    }
    
    public void setLast4DigitsOfCard(String last4DigitsOfCard) {
        this.last4DigitsOfCard = last4DigitsOfCard;
    }
    
    public String getCardAuthorizationId() {
        return this.cardAuthorizationId;
    }
    
    public void setCardAuthorizationId(String cardAuthorizationId) {
        this.cardAuthorizationId = cardAuthorizationId;
    }
    
    public String getPaymentDate() {
        return this.paymentDate;
    }
    
    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }
    
    public String toString() {
        final StringBuilder str = new StringBuilder();
        str.append(this.getClass().getName());
        str.append(" {Type=").append(this.type);
        str.append(", Amount=").append(this.amount.setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());
        str.append(", CardType=").append(this.cardType);
        str.append(", Last4DigitsOfCard=").append(this.last4DigitsOfCard);
        str.append(", CardAuthorizationId=").append(this.cardAuthorizationId);
        str.append(", PaymentDate=").append(this.paymentDate);
        str.append("}");
        return str.toString();
    }
}
