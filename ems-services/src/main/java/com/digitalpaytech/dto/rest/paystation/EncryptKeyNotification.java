package com.digitalpaytech.dto.rest.paystation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "EncryptKeyNotification")
@XmlAccessorType(XmlAccessType.FIELD)
public class EncryptKeyNotification {
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(", EncryptKeyNotification=true");
        return str.toString();
    }
}
