package com.digitalpaytech.dto.rest.paystation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class PaystationResponse {
    
    @XmlAttribute(name = "MessageNumber")
    private String messageNumber;
    
    @XmlElement(name = "ResponseMessage")
    private String responseMessage;
    
    @XmlElement(name = "LotSettingNotification")
    private LotSettingNotification lotSettingNotification;
    
    @XmlElement(name = "EncryptKeyNotification")
    private EncryptKeyNotification encryptKeyNotification;
    
    @XmlElement(name = "TimestampNotification")
    private TimestampNotification timestampNotification;
    
    @XmlElement(name = "EMSDomainNotification")
    private EMSDomainNotification emsDomainNotification;
    
    @XmlElement(name = "RootCertificateNotification")
    private RootCertificateNotification rootCertificateNotification;
    
    public String getMessageNumber() {
        return messageNumber;
    }
    
    public void setMessageNumber(String messageNumber) {
        this.messageNumber = messageNumber;
    }
    
    public String getResponseMessage() {
        return responseMessage;
    }
    
    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
    
    public LotSettingNotification getLotSettingNotification() {
        return lotSettingNotification;
    }
    
    public void setLotSettingNotification(LotSettingNotification lotSettingNotification) {
        this.lotSettingNotification = lotSettingNotification;
    }
    
    public EncryptKeyNotification getEncryptKeyNotification() {
        return encryptKeyNotification;
    }
    
    public void setEncryptKeyNotification(EncryptKeyNotification encryptKeyNotification) {
        this.encryptKeyNotification = encryptKeyNotification;
    }
    
    public TimestampNotification getTimestampNotification() {
        return timestampNotification;
    }
    
    public void setTimestampNotification(TimestampNotification timestampNotification) {
        this.timestampNotification = timestampNotification;
    }
    
    public EMSDomainNotification getEmsDomainNotification() {
        return emsDomainNotification;
    }
    
    public void setEmsDomainNotification(EMSDomainNotification emsDomainNotification) {
        this.emsDomainNotification = emsDomainNotification;
    }
    
    public final RootCertificateNotification getRootCertificateNotification() {
        return this.rootCertificateNotification;
    }
    
    public final void setRootCertificateNotification(final RootCertificateNotification rootCertificateNotification) {
        this.rootCertificateNotification = rootCertificateNotification;
    }
    
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(", ResponseMessage=").append(this.responseMessage);
        if (this.lotSettingNotification != null) {
            this.lotSettingNotification.toString();
        }
        if (this.encryptKeyNotification != null) {
            this.encryptKeyNotification.toString();
        }
        if (this.emsDomainNotification != null) {
            this.emsDomainNotification.toString();
        }
        if (this.rootCertificateNotification != null) {
            this.rootCertificateNotification.toString();
        }
        
        return str.toString();
    }
}
