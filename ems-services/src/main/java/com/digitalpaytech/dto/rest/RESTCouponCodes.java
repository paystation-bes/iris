package com.digitalpaytech.dto.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "CouponCodes")
@XmlAccessorType(XmlAccessType.FIELD)
public class RESTCouponCodes {
    @XmlElement(name = "CouponCode")
    private List<String> couponCodes;
    
    public List<String> getCouponCodes() {
        return couponCodes;
    }
    
    public void setCouponCodes(List<String> couponCodes) {
        this.couponCodes = couponCodes;
    }
}
