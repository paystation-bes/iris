package com.digitalpaytech.dto.rest;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Regions")
@XmlAccessorType(XmlAccessType.FIELD)
public class RESTRegions
{
	@XmlElement(name = "RegionName")
	private List<String> regionName;

	public List<String> getRegionName()
	{
		return regionName;
	}

	public void setRegionName(List<String> regionName)
	{
		this.regionName = regionName;
	}
	
	public String toString()
	{
		StringBuilder str = new StringBuilder();
		str.append(this.getClass().getName());
		str.append(", regionName=");
		for (String name : regionName)
		{
			str.append("(").append(name).append(")");
		}
		str.append("}");
		return str.toString();
	}
}
