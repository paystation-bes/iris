package com.digitalpaytech.dto.rest;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Permit")
@XmlAccessorType(XmlAccessType.FIELD)
public class RESTPermit {
    @XmlElement(name = "PermitNumber")
    private String permitNumber;
    @XmlElement(name = "PurchasedDate")
    private String purchasedDate;
    @XmlElement(name = "StallNumber")
    private String stallNumber;
    @XmlElement(name = "PlateNumber")
    private String plateNumber;
    @XmlElement(name = "RegionName")
    private String regionName;
    @XmlElement(name = "RateName")
    private String rateName;
    @XmlElement(name = "ExpiryDate")
    private String expiryDate;
    @XmlElement(name = "PermitAmount")
    private BigDecimal permitAmount;
    @XmlElement(name = "Payments")
    private RESTPayments payments;
    
    public String getPermitNumber() {
        return permitNumber;
    }
    
    public void setPermitNumber(String permitNumber) {
        this.permitNumber = permitNumber;
    }
    
    public String getPurchasedDate() {
        return purchasedDate;
    }
    
    public void setPurchasedDate(String purchasedDate) {
        this.purchasedDate = purchasedDate;
    }
    
    public String getStallNumber() {
        return stallNumber;
    }
    
    public void setStallNumber(String stallNumber) {
        this.stallNumber = stallNumber;
    }
    
    public String getPlateNumber() {
        return plateNumber;
    }
    
    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }
    
    public String getRegionName() {
        return regionName;
    }
    
    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }
    
    public String getRateName() {
        return rateName;
    }
    
    public void setRateName(String rateName) {
        this.rateName = rateName;
    }
    
    public String getExpiryDate() {
        return expiryDate;
    }
    
    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }
    
    public BigDecimal getPermitAmount() {
        return permitAmount;
    }
    
    public void setPermitAmount(BigDecimal permitAmount) {
        this.permitAmount = permitAmount;
    }
    
    public RESTPayments getPayments() {
        return payments;
    }
    
    public void setPayments(RESTPayments payments) {
        this.payments = payments;
    }
    
    @Override
    public final String toString() {
        final StringBuilder str = new StringBuilder();
        str.append(this.getClass().getName());
        str.append(" {PermitNumber=").append(this.permitNumber);
        str.append(", PurchasedDate=").append(this.purchasedDate);
        str.append(", PlateNumber=").append(this.plateNumber);
        str.append(", RegionName=").append(this.regionName);
        str.append(", RateName=").append(this.rateName);
        str.append(", ExpiryDate=").append(this.expiryDate);
        str.append(", PermitAmount=").append(this.permitAmount.setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());
        str.append(", Payments=").append(this.payments.toString());
        str.append("}");
        return str.toString();
    }
}
