package com.digitalpaytech.dto.rest.paystation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Token")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaystationToken extends PaystationRequest {
    @XmlElement(name = "MacAddress")
    private String macAddress;
    
    public String getMacAddress() {
        return macAddress;
    }
    
    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }
    
    public PaystationToken() {}
    
    public PaystationToken(String serialNumber, String macAddress) {
        super(serialNumber);
        this.macAddress = macAddress;
    }
    
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(this.getClass().getName());
        str.append(super.toString());
        str.append(", MacAddress=").append(this.macAddress);
        str.append("}");
        return str.toString();
    }
}
