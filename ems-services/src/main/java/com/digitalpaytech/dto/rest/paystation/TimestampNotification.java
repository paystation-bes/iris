package com.digitalpaytech.dto.rest.paystation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "TimestampNotification")
@XmlAccessorType(XmlAccessType.FIELD)
public class TimestampNotification {
    @XmlAttribute(name = "timestamp")
    private String timestamp;
    
    public TimestampNotification() {
    }
    
    public TimestampNotification(String timestamp) {
        super();
        this.timestamp = timestamp;
    }
    
    public String getTimestamp() {
        return timestamp;
    }
    
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
    
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(", EncryptKeyNotification timestamp=").append(this.timestamp);
        return str.toString();
    }
}
