package com.digitalpaytech.dto.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Token")
@XmlAccessorType(XmlAccessType.FIELD)
public class RESTToken
{
	private String account;
	@XmlElement(name = "SessionToken")
	private String sessionToken;
	
	public void setAccount(String account)
	{
		this.account = account;
	}

	public void setSessionToken(String sessionToken)
	{
		this.sessionToken = sessionToken;
	}

	public String getAccount()
	{
		return account;
	}

	public String getSessionToken()
	{
		return sessionToken;
	}

	public String toString()
	{
		StringBuilder str = new StringBuilder();
		str.append(this.getClass().getName());
		str.append(", Account=").append(this.account);
		str.append(", SessionToken=").append(this.sessionToken);
		str.append("}");
		return str.toString();
	}
}
