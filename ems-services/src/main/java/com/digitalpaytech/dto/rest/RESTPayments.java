package com.digitalpaytech.dto.rest;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Payments")
@XmlAccessorType(XmlAccessType.FIELD)
public class RESTPayments {
    @XmlElement(name = "Payment")
    private List<RESTPayment> payment;
    
    public List<RESTPayment> getPayment() {
        return this.payment;
    }
    
    public void setPayment(List<RESTPayment> payment) {
        this.payment = payment;
    }
    
    @Override
    public final String toString() {
        final StringBuilder str = new StringBuilder();
        str.append(this.getClass().getName()).append("{");
        for (RESTPayment p : this.payment) {
            str.append(p.toString());
        }
        str.append("}");
        return str.toString();
    }
}
