package com.digitalpaytech.dto.rest.paystation;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlTransient
public class PaystationRequest {
    
    @XmlAttribute(name = "MessageNumber")
    private String messageNumber;
    
    @XmlElement(name = "PaystationCommAddress")
    private String paystationCommAddress;
    
    public PaystationRequest() {
    }
    
    public PaystationRequest(String paystationCommAddress) {
        this.paystationCommAddress = paystationCommAddress;
    }
    
    public String getMessageNumber() {
        return messageNumber;
    }
    
    public void setMessageNumber(String messageNumber) {
        this.messageNumber = messageNumber;
    }
    
    public String getPaystationCommAddress() {
        return paystationCommAddress;
    }
    
    public void setPaystationCommAddress(String paystationCommAddress) {
        this.paystationCommAddress = paystationCommAddress;
    }
    
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(", MessageNumber=").append(this.messageNumber);
        str.append(", PaystationCommAddress=").append(this.paystationCommAddress);
        return str.toString();
    }
}
