package com.digitalpaytech.dto.rest.paystation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RootCertificateNotification")
@XmlAccessorType(XmlAccessType.FIELD)
public class RootCertificateNotification {
    
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(", RootCertificateNotification=true");
        return str.toString();
    }
}