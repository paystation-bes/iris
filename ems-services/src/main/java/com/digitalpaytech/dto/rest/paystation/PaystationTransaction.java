package com.digitalpaytech.dto.rest.paystation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Transaction")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaystationTransaction extends PaystationRequest {
    @XmlElement(name = "Number")
    private String number = "0";
    @XmlElement(name = "LotNumber")
    private String lotNumber;
    @XmlElement(name = "StallNumber")
    private String stallNumber = "0";
    @XmlElement(name = "AddTimeNum")
    private String addTimeNum = "0";
    @XmlElement(name = "Type")
    private String type;
    @XmlElement(name = "PurchasedDate")
    private String purchasedDate;
    @XmlElement(name = "Timestamp")
    private String timestamp;
    @XmlElement(name = "ParkingTimePurchased")
    private String parkingTimePurchased;
    @XmlElement(name = "OriginalAmount")
    private String originalAmount = "0";
    @XmlElement(name = "ChargedAmount")
    private String chargedAmount = "0";
    @XmlElement(name = "CashPaid")
    private String cashPaid = "0";
    @XmlElement(name = "CardPaid")
    private String cardPaid = "0";
    @XmlElement(name = "PaidByRFID")
    private String paidByRFID;
    @XmlElement(name = "ChangeDispensed")
    private String ChangeDispensed = "0";
    @XmlElement(name = "IsRefundSlip")
    private String isRefundSlip;
    @XmlElement(name = "CardData")
    private String cardData;
    @XmlElement(name = "SmartCardData")
    private String smartCardData;
    @XmlElement(name = "SmartCardPaid")
    private String smartCardPaid = "0";
    @XmlElement(name = "CouponNumber")
    private String couponNumber;

    @XmlElement(name = "MobileNumber")
    private String mobileNumber;
    @XmlElement(name = "CreditCardData")
    private String creditCardData;
    @XmlElement(name = "NumberBillsAccepted")
    private String numberBillsAccepted = "0";
    @XmlElement(name = "NumberCoinsAccepted")
    private String numberCoinsAccepted = "0";
    
    @XmlElement(name = "HopperDispensed")
    private String hopperDispensed;
    @XmlElement(name = "ExpiryDate")
    private String ExpiryDate;
    @XmlElement(name = "Tax1Rate")
    private String tax1Rate;
    @XmlElement(name = "Tax1Value")
    private String tax1Value;
    @XmlElement(name = "Tax1Name")
    private String tax1Name;
    @XmlElement(name = "Tax2Rate")
    private String tax2Rate;
    @XmlElement(name = "Tax2Value")
    private String tax2Value;
    @XmlElement(name = "Tax2Name")
    private String tax2Name;
    @XmlElement(name = "SmartCardType")
    private String SmartCardType;
    @XmlElement(name = "SmartCardMerchantInfo")
    private String smartCardMerchantInfo;
    @XmlElement(name = "SmartCardSettlementInfo")
    private String smartCardSettlementInfo;
    @XmlElement(name = "RateName")
    private String rateName;
    @XmlElement(name = "RateId")
    private String rateId;
    @XmlElement(name = "RateValue")
    private String rateValue = "0";
    @XmlElement(name = "TaxType")
    private String taxType;
    @XmlElement(name = "LicensePlateNo")
    private String licensePlateNo;

    @XmlElement(name = "CoinCol")
    private String coinCol;
    @XmlElement(name = "BillCol")
    private String billCol;
    @XmlElement(name = "CardAmountOther")
    private String cardAmountOther = "0";
    @XmlElement(name = "CardAmountSC")
    private String cardAmountSC = "0";
    @XmlElement(name = "CardAmountVisa")
    private String cardAmountVisa = "0";
    @XmlElement(name = "CardAmountMC")
    private String CardAmountMC = "0";
    @XmlElement(name = "CardAmountAmex")
    private String cardAmountAmex = "0";
    @XmlElement(name = "CardAmountDiners")
    private String cardAmountDiners = "0";
    @XmlElement(name = "CardAmountDiscover")
    private String cardAmountDiscover = "0";

    @XmlElement(name = "StoreAndForward")
    private String storeAndForward;
    
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
    
    public String getLotNumber() {
        return lotNumber;
    }
    
    public void setLotnumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }
    
    public String getStallNumber() {
        return stallNumber;
    }
    
    public void setStallNumber(String stallNumber) {
        this.stallNumber = stallNumber;
    }
    
    public String getAddTimeNum() {
        return addTimeNum;
    }
    
    public void setAddTimeNum(String addTimeNum) {
        this.addTimeNum = addTimeNum;
    }
    
    public String getType() {
        return type;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
    public String getPurchasedDate() {
        return purchasedDate;
    }
    
    public void setPurchasedDate(String purchasedDate) {
        this.purchasedDate = purchasedDate;
    }
    
    public String getTimestamp() {
        return timestamp;
    }
    
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
    
    public String getParkingTimePurchased() {
        return parkingTimePurchased;
    }
    
    public void setParkingTimePurchased(String parkingTimePurchased) {
        this.parkingTimePurchased = parkingTimePurchased;
    }
    
    public String getOriginalAmount() {
        return originalAmount;
    }
    
    public void setOriginalAmount(String originalAmount) {
        this.originalAmount = originalAmount;
    }
    
    public String getChargedAmount() {
        return chargedAmount;
    }
    
    public void setChargedAmount(String chargedAmount) {
        this.chargedAmount = chargedAmount;
    }
    
    public String getCashPaid() {
        return cashPaid;
    }
    
    public void setCashPaid(String cashPaid) {
        this.cashPaid = cashPaid;
    }
    
    public String getCardPaid() {
        return cardPaid;
    }
    
    public void setCardPaid(String cardPaid) {
        this.cardPaid = cardPaid;
    }
    
    public String getPaidByRFID() {
        return paidByRFID;
    }
    
    public void setPaidByRFID(String paidByRFID) {
        this.paidByRFID = paidByRFID;
    }
    
    public String getChangeDispensed() {
        return ChangeDispensed;
    }
    
    public void setChangeDispensed(String changeDispensed) {
        ChangeDispensed = changeDispensed;
    }
    
    public String getIsRefundSlip() {
        return isRefundSlip;
    }
    
    public void setIsRefundSlip(String isRefundSlip) {
        this.isRefundSlip = isRefundSlip;
    }
    
    public String getCardData() {
        return cardData;
    }
    
    public void setCardData(String cardData) {
        this.cardData = cardData;
    }
    
    public String getSmartCardData() {
        return smartCardData;
    }
    
    public void setSmartCardData(String smartCardData) {
        this.smartCardData = smartCardData;
    }
    
    public String getSmartCardPaid() {
        return smartCardPaid;
    }
    
    public void setSmartCardPaid(String smartCardPaid) {
        this.smartCardPaid = smartCardPaid;
    }
    
    public String getCouponNumber() {
        return couponNumber;
    }
    
    public void setCouponNumber(String couponNumber) {
        this.couponNumber = couponNumber;
    }
    
    public String getMobileNumber() {
        return mobileNumber;
    }
    
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getCreditCardData() {
        return creditCardData;
    }
    
    public void setCreditCardData(String creditCardData) {
        this.creditCardData = creditCardData;
    }

    public String getNumberBillsAccepted() {
        return numberBillsAccepted;
    }
    
    public void setNumberBillsAccepted(String numberBillsAccepted) {
        this.numberBillsAccepted = numberBillsAccepted;
    }
    
    public String getNumberCoinsAccepted() {
        return numberCoinsAccepted;
    }
    
    public void setNumberCoinsAccepted(String numberCoinsAccepted) {
        this.numberCoinsAccepted = numberCoinsAccepted;
    }
    
    public String getHopperDispensed() {
        return hopperDispensed;
    }
    
    public void setHopperDispensed(String hopperDispensed) {
        this.hopperDispensed = hopperDispensed;
    }
    
    public String getExpiryDate() {
        return ExpiryDate;
    }
    
    public void setExpiryDate(String expiryDate) {
        ExpiryDate = expiryDate;
    }
    
    public String getTax1Rate() {
        return tax1Rate;
    }
    
    public void setTax1Rate(String tax1Rate) {
        this.tax1Rate = tax1Rate;
    }
    
    public String getTax1Value() {
        return tax1Value;
    }
    
    public void setTax1Value(String tax1Value) {
        this.tax1Value = tax1Value;
    }
    
    public String getTax1Name() {
        return tax1Name;
    }
    
    public void setTax1Name(String tax1Name) {
        this.tax1Name = tax1Name;
    }
    
    public String getTax2Rate() {
        return tax2Rate;
    }
    
    public void setTax2Rate(String tax2Rate) {
        this.tax2Rate = tax2Rate;
    }
    
    public String getTax2Value() {
        return tax2Value;
    }
    
    public void setTax2Value(String tax2Value) {
        this.tax2Value = tax2Value;
    }
    
    public String getTax2Name() {
        return tax2Name;
    }
    
    public void setTax2Name(String tax2Name) {
        this.tax2Name = tax2Name;
    }
    
    public String getSmartCardType() {
        return SmartCardType;
    }
    
    public void setSmartCardType(String smartCardType) {
        SmartCardType = smartCardType;
    }
    
    public String getSmartCardMerchantInfo() {
        return smartCardMerchantInfo;
    }
    
    public void setSmartCardMerchantInfo(String smartCardMerchantInfo) {
        this.smartCardMerchantInfo = smartCardMerchantInfo;
    }
    
    public String getSmartCardSettlementInfo() {
        return smartCardSettlementInfo;
    }
    
    public void setSmartCardSettlementInfo(String smartCardSettlementInfo) {
        this.smartCardSettlementInfo = smartCardSettlementInfo;
    }
    
    public String getRateName() {
        return rateName;
    }
    
    public void setRateName(String rateName) {
        this.rateName = rateName;
    }
    
    public String getRateId() {
        return rateId;
    }
    
    public void setRateId(String rateId) {
        this.rateId = rateId;
    }
    
    public String getRateValue() {
        return rateValue;
    }
    
    public void setRateValue(String rateValue) {
        this.rateValue = rateValue;
    }
    
    public String getTaxType() {
        return taxType;
    }
    
    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }
    
    public String getLicensePlateNo() {
        return licensePlateNo;
    }
    
    public void setLicensePlateNo(String licensePlateNo) {
        this.licensePlateNo = licensePlateNo;
    }
    
    public String getCoinCol() {
        return coinCol;
    }
    
    public void setCoinCol(String coinCol) {
        this.coinCol = coinCol;
    }
    
    public String getBillCol() {
        return billCol;
    }
    
    public void setBillCol(String billCol) {
        this.billCol = billCol;
    }
    
    public String getCardAmountOther() {
        return cardAmountOther;
    }
    
    public void setCardAmountOther(String cardAmountOther) {
        this.cardAmountOther = cardAmountOther;
    }
    
    public String getCardAmountSC() {
        return cardAmountSC;
    }
    
    public void setCardAmountSC(String cardAmountSC) {
        this.cardAmountSC = cardAmountSC;
    }
    
    public String getCardAmountVisa() {
        return cardAmountVisa;
    }
    
    public void setCardAmountVisa(String cardAmountVisa) {
        this.cardAmountVisa = cardAmountVisa;
    }
    
    public String getCardAmountMC() {
        return CardAmountMC;
    }
    
    public void setCardAmountMC(String cardAmountMC) {
        CardAmountMC = cardAmountMC;
    }
    
    public String getCardAmountAmex() {
        return cardAmountAmex;
    }
    
    public void setCardAmountAmex(String cardAmountAmex) {
        this.cardAmountAmex = cardAmountAmex;
    }
    
    public String getCardAmountDiners() {
        return cardAmountDiners;
    }
    
    public void setCardAmountDiners(String cardAmountDiners) {
        this.cardAmountDiners = cardAmountDiners;
    }
    
    public String getCardAmountDiscover() {
        return cardAmountDiscover;
    }
    
    public void setCardAmountDiscover(String cardAmountDiscover) {
        this.cardAmountDiscover = cardAmountDiscover;
    }

    public String getStoreAndForward() {
        return storeAndForward;
    }

    public void setStoreAndForward(String storeAndForward) {
        this.storeAndForward = storeAndForward;
    }    
}
