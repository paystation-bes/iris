package com.digitalpaytech.dto.rest;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.digitalpaytech.util.RestCoreConstants;

public class RESTRegionQueryParamList extends RESTQueryParamList
{
	private String token;

	public String getToken()
	{
		return token;
	}

	public void setToken(String token)
	{
		this.token = token;
	}
	
	public String toString()
	{
		StringBuilder str = new StringBuilder();
		str.append(this.getClass().getName());
		str.append(" {Token=").append(this.token);
		str.append("}");
		str.append(super.toString());
		return str.toString();
	}

	@Override
	public String getFormatParamString() throws UnsupportedEncodingException
	{
		StringBuilder str = new StringBuilder();
		str.append("SignatureVersion").append(RestCoreConstants.EQUAL_SIGN).append(
				URLEncoder.encode(this.getSignatureVersion().trim(), RestCoreConstants.HTTP_REST_ENCODE_CHARSET));
		str.append(RestCoreConstants.AMPERSAND).append("Timestamp").append(RestCoreConstants.EQUAL_SIGN).append(
				URLEncoder.encode(this.getTimestamp().trim(), RestCoreConstants.HTTP_REST_ENCODE_CHARSET));
		if (token != null)
		{
			str.append(RestCoreConstants.AMPERSAND).append("Token").append(RestCoreConstants.EQUAL_SIGN).append(
					URLEncoder.encode(this.token.trim(), RestCoreConstants.HTTP_REST_ENCODE_CHARSET));
		}
		return str.toString();
	}
}
