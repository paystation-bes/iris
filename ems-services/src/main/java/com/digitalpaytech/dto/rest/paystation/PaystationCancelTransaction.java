package com.digitalpaytech.dto.rest.paystation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "CancelTransaction")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaystationCancelTransaction extends PaystationRequest {
    @XmlElement(name = "Number")
    private String number;
    @XmlElement(name = "PurchasedDate")
    private String purchasedDate;
    @XmlElement(name = "PurchaseId")
    private String purchaseId;
    @XmlElement(name = "EscrowAllReturned")
    private String escrowAllReturned = "0";
    @XmlElement(name = "ChangeDispensed")
    private String changeDispensed = "0";
    @XmlElement(name = "IsRefundSlip")
    private String isRefundSlip;
    
    public final String getNumber() {
        return this.number;
    }
    
    public final void setNumber(final String number) {
        this.number = number;
    }
    
    public final String getPurchasedDate() {
        return this.purchasedDate;
    }
    
    public final void setPurchasedDate(final String purchasedDate) {
        this.purchasedDate = purchasedDate;
    }
    
    public final String getPurchaseId() {
        return this.purchaseId;
    }
    
    public final void setPurchaseId(final String purchaseId) {
        this.purchaseId = purchaseId;
    }
    
    public final String getEscrowAllReturned() {
        return this.escrowAllReturned;
    }
    
    public final void setEscrowAllReturned(final String escrowAllReturned) {
        this.escrowAllReturned = escrowAllReturned;
    }
    
    public final String getChangeDispensed() {
        return this.changeDispensed;
    }
    
    public final void setChangeDispensed(final String changeDispensed) {
        this.changeDispensed = changeDispensed;
    }
    
    public final String getIsRefundSlip() {
        return this.isRefundSlip;
    }
    
    public final void setIsRefundSlip(final String isRefundSlip) {
        this.isRefundSlip = isRefundSlip;
    }
}
