package com.digitalpaytech.dto.rest;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.digitalpaytech.util.RestCoreConstants;

public class RESTTokenQueryParamList extends RESTQueryParamList
{
	private String account;
	private String token;
	
	public String getAccount()
	{
		return account;
	}

	public void setAccount(String account)
	{
		this.account = account;
	}
	
	public String getToken()
	{
		return token;
	}

	public void setToken(String token)
	{
		this.token = token;
	}

	public String toString()
	{
		StringBuilder str = new StringBuilder();
		str.append(this.getClass().getName());
		str.append(" {Account=").append(this.account);
		str.append(" {Token=").append(this.token);
		str.append("}\n");
		str.append(super.toString());
		return str.toString();
	}
	
	public String getFormatParamString() throws UnsupportedEncodingException 
	{
		StringBuilder str = new StringBuilder();
		if (account != null)
		{
			str.append("Account").append(RestCoreConstants.EQUAL_SIGN).append(
					URLEncoder.encode(this.getAccount().trim(), RestCoreConstants.HTTP_REST_ENCODE_CHARSET));
			str.append(RestCoreConstants.AMPERSAND);
		}				
		str.append("SignatureVersion").append(RestCoreConstants.EQUAL_SIGN).append(
				URLEncoder.encode(this.getSignatureVersion().trim(), RestCoreConstants.HTTP_REST_ENCODE_CHARSET));
		str.append(RestCoreConstants.AMPERSAND).append("Timestamp").append(RestCoreConstants.EQUAL_SIGN).append(
				URLEncoder.encode(this.getTimestamp().trim(), RestCoreConstants.HTTP_REST_ENCODE_CHARSET));
		if (token != null)
		{
			str.append(RestCoreConstants.AMPERSAND).append("Token").append(RestCoreConstants.EQUAL_SIGN).append(
					URLEncoder.encode(this.getToken().trim(), RestCoreConstants.HTTP_REST_ENCODE_CHARSET));
		}
		return str.toString();
	}
}
