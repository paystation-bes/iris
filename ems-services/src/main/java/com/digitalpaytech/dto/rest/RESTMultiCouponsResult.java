package com.digitalpaytech.dto.rest;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Results")
@XmlAccessorType(XmlAccessType.FIELD)
public class RESTMultiCouponsResult {
	
	@XmlElementWrapper(name = "Created")
	@XmlElement(name = "CouponCode")
    private List<String> couponCodes;	
	
    public List<String> getCouponCodes() {
        return couponCodes;
    }
    
    public void setCouponCodes(List<String> couponCodes) {
        this.couponCodes = couponCodes;
    }
    
	@XmlElementWrapper(name = "Failed")
    @XmlElement(name = "CouponCode")
    private List<String> failedCouponCodes;
    
    public List<String> getFailedCouponCodes() {
		return failedCouponCodes;
	}

	public void setFailedCouponCodes(List<String> failedCouponCodes) {
		this.failedCouponCodes = failedCouponCodes;
	}
}
