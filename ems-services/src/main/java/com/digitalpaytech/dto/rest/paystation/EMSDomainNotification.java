package com.digitalpaytech.dto.rest.paystation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "EMSDomainNotification")
@XmlAccessorType(XmlAccessType.FIELD)
public class EMSDomainNotification {
    
    @XmlAttribute(name = "url")
    private String url;
    
    @XmlAttribute(name = "ip")
    private String ip;
    
    @XmlAttribute(name = "connIp")
    private String connIp;
    
    public EMSDomainNotification() {
    }
    
    public EMSDomainNotification(String url, String ip, String connIp) {
        super();
        this.url = url;
        this.ip = ip;
        this.connIp = connIp;
    }
    
    public String getUrl() {
        return url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }
    
    public String getIp() {
        return ip;
    }
    
    public void setIp(String ip) {
        this.ip = ip;
    }
    
    public String getConnIp() {
        return connIp;
    }
    
    public void setConnIp(String connIp) {
        this.connIp = connIp;
    }
    
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(", EncryptKeyNotification url=").append(this.url);
        str.append(" ip=").append(this.ip);
        str.append(" connIp=").append(this.connIp);
        return str.toString();
    }
    
}
