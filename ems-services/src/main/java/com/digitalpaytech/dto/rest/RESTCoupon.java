package com.digitalpaytech.dto.rest;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Coupon")
@XmlAccessorType(XmlAccessType.FIELD)
public class RESTCoupon {
    @XmlElement(name = "CouponCode")
    private String couponCode;
    
    @XmlElement(name = "Description")
    private String description;
    
    @XmlElement(name = "DiscountType")
    private String discountType;
    
    @XmlElement(name = "DiscountAmt")
    private BigDecimal discountAmt;
    
    @XmlElement(name = "DiscountPercent")
    private BigDecimal discountPercent;
    
    @XmlElement(name = "StartDate")
    private String startDate;
    
    @XmlElement(name = "EndDate")
    private String endDate;
    
    @XmlElement(name = "Uses")
    private String uses;
    
    @XmlElement(name = "Location")
    private String location;
    
    @XmlElement(name = "OperatingMode")
    private String operatingMode;
    
    @XmlElement(name = "SpaceRange")
    private String spaceRange;
    
    @XmlElement(name = "DailySingleUse")
    private String validForNumOfDay;
    
    public String getCouponCode() {
        return couponCode;
    }
    
    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDiscountType() {
        return discountType;
    }
    
    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }
    
    public BigDecimal getDiscountAmt() {
        return discountAmt;
    }
    
    public void setDiscountAmt(BigDecimal discountAmt) {
        this.discountAmt = discountAmt;
    }
    
    public BigDecimal getDiscountPercent() {
        return discountPercent;
    }
    
    public void setDiscountPercent(BigDecimal discountPercent) {
        this.discountPercent = discountPercent;
    }
    
    public String getStartDate() {
        return startDate;
    }
    
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
    
    public String getEndDate() {
        return endDate;
    }
    
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    
    public String getUses() {
        return uses;
    }
    
    public void setUses(String uses) {
        this.uses = uses;
    }
    
    public String getLocation() {
        return location;
    }
    
    public void setLocation(String location) {
        this.location = location;
    }
    
    public String getOperatingMode() {
        return operatingMode;
    }
    
    public void setOperatingMode(String operatingMode) {
        this.operatingMode = operatingMode;
    }
    
    public String getSpaceRange() {
        return spaceRange;
    }
    
    public void setSpaceRange(String spaceRange) {
        this.spaceRange = spaceRange;
    }
    
    public String getValidForNumOfDay() {
        return validForNumOfDay;
    }
    
    public void setValidForNumOfDay(String validForNumOfDay) {
        this.validForNumOfDay = validForNumOfDay;
    }
}
