package com.digitalpaytech.dto.rest;

import java.io.UnsupportedEncodingException;

public abstract class RESTQueryParamList
{
	private String timestamp;
	private String signature;
	private String signatureVersion;
	
	public String getTimestamp()
	{
		return timestamp;
	}

	public void setTimestamp(String timestamp)
	{
		this.timestamp = timestamp;
	}

	public String getSignature()
	{
		return signature;
	}

	public void setSignature(String signature)
	{
		this.signature = signature;
	}

	public String getSignatureVersion()
	{
		return signatureVersion;
	}

	public void setSignatureVersion(String signatureVersion)
	{
		this.signatureVersion = signatureVersion;
	}
	
	public String toString()
	{
		StringBuilder str = new StringBuilder();
		str.append(this.getClass().getName());
		str.append(" {Signature=").append(this.signature);
		str.append(" {SignatureVersion=").append(this.signatureVersion);
		str.append(" {Timestamp=").append(this.timestamp);
		str.append("}");
		return str.toString();
	}
	
	public abstract String getFormatParamString() throws UnsupportedEncodingException;
}
