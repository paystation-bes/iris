package com.digitalpaytech.dto.rest.paystation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "HeartBeat")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaystationHeartBeat extends PaystationRequest {
    
    public PaystationHeartBeat() {
    }
    
    public PaystationHeartBeat(String serialNumber) {
        super(serialNumber);
    }
    
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(this.getClass().getName());
        str.append(super.toString());
        str.append("}");
        return str.toString();
    }
}
