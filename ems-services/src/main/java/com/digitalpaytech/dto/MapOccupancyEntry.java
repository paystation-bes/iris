package com.digitalpaytech.dto;

import java.io.Serializable;
import java.util.List;

public class MapOccupancyEntry implements Serializable {
    
    private static final long serialVersionUID = 6791600661191493835L;

    private float paidOccupancy;
    private float realOccupancy;
    private String randomId;
    private List<MapOccupancyLineEntry> geoLocationList;
    
    private transient Integer locationId;
    
    public final float getPaidOccupancy() {
        return this.paidOccupancy;
    }
    
    public final void setPaidOccupancy(final float paidOccupancy) {
        this.paidOccupancy = paidOccupancy;
    }
    
    public final float getRealOccupancy() {
        return this.realOccupancy;
    }
    
    public final void setRealOccupancy(final float realOccupancy) {
        this.realOccupancy = realOccupancy;
    }
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final List<MapOccupancyLineEntry> getGeoLocationList() {
        return this.geoLocationList;
    }
    
    public final void setGeoLocationList(final List<MapOccupancyLineEntry> geoLocationList) {
        this.geoLocationList = geoLocationList;
    }
    
    public final Integer getLocationId() {
        return this.locationId;
    }
    
    public final void setLocationId(final Integer locationId) {
        this.locationId = locationId;
    }
}
