package com.digitalpaytech.dto;

import com.digitalpaytech.domain.Customer;

public class BadCardData {
    
    private String id;
    private String cardNumber;
    private String expiry;
    private Integer customerCardTypeId;
    private String parentCardType;
    private String comment;
    private Customer customer;
    private String source;
    
    public BadCardData() {
        super();
    }
    
    public BadCardData(final String cardNumber, final String expiry, final Integer customerCardTypeId, final String comment, final Customer customer,
            final String source) {
        super();
        this.cardNumber = cardNumber;
        this.expiry = expiry;
        this.customerCardTypeId = customerCardTypeId;
        this.comment = comment;
        this.customer = customer;
        this.source = source;
    }
    
    public BadCardData(final String cardNumber, final String expiry, final String parentCardType, final String comment, final Customer customer,
            final String source) {
        super();
        this.cardNumber = cardNumber;
        this.expiry = expiry;
        this.parentCardType = parentCardType;
        this.comment = comment;
        this.customer = customer;
        this.source = source;
    }
    
    public String getId() {
        return this.id;
    }
    
    public void setId(final String id) {
        this.id = id;
    }
    
    public String getCardNumber() {
        return this.cardNumber;
    }
    
    public void setCardNumber(final String cardNumber) {
        this.cardNumber = cardNumber;
    }
    
    public String getExpiry() {
        return this.expiry;
    }
    
    public void setExpiry(final String expiry) {
        this.expiry = expiry;
    }
    
    public String getComment() {
        return this.comment;
    }
    
    public void setComment(final String comment) {
        this.comment = comment;
    }
    
    public Customer getCustomer() {
        return this.customer;
    }
    
    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }
    
    public String getParentCardType() {
        return this.parentCardType;
    }
    
    public void setParentCardType(final String parentCardType) {
        this.parentCardType = parentCardType;
    }
    
    public String getSource() {
        return this.source;
    }
    
    public void setSource(final String source) {
        this.source = source;
    }
    
    public Integer getCustomerCardTypeId() {
        return this.customerCardTypeId;
    }
    
    public void setCustomerCardTypeId(final Integer customerCardTypeId) {
        this.customerCardTypeId = customerCardTypeId;
    }
    
}
