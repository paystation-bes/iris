package com.digitalpaytech.dto;

import java.util.Iterator;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("tier2")
public class Tier2 {
    
    @XStreamAsAttribute
    private String type;
    
    @XStreamImplicit(itemFieldName = "tier3")
    private List<Tier3> tier3s;
    
    public Tier2() {
    }
    
    public Tier2(final String type, final List<Tier3> tier3s) {
        this.type = type;
        this.tier3s = tier3s;
    }
    
    /**
     * Finds Tier3 object that matches with input tier3 value.
     * 
     * @param tier3
     *            String tier 3 value, e.g. none (empty string, ""), Rate, TransactionType or RevenueType.
     * @return Tier3 Returns 'null' if couldn't find suitable Tier3 data, or input tier3 is null.
     */
    public final Tier3 getTier3(final String tier3) {
        Tier3 tier3Obj;
        if (this.tier3s != null) {
            final Iterator<Tier3> iter = this.tier3s.iterator();
            while (iter.hasNext()) {
                tier3Obj = iter.next();
                if (tier3 != null && tier3.trim().length() != 0 && tier3.equalsIgnoreCase(tier3Obj.getType())) {
                    return tier3Obj;
                }
            }
        }
        return null;
    }
    
    @Override
    public final String toString() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("Tier2 type: ").append(this.type).append(", Tier3 types: ");
        // tier3s could be null if there value is blank (empty) or no tier 3 data.
        if (this.tier3s != null) {
            final Iterator<Tier3> iter = this.tier3s.iterator();
            while (iter.hasNext()) {
                bdr.append(iter.next()).append(", ");
            }
        } else {
            bdr.append("none");
        }
        return bdr.toString();
    }
    
    public final List<Tier3> getTier3s() {
        return this.tier3s;
    }
    
    public final void setTier3s(final List<Tier3> tier3s) {
        this.tier3s = tier3s;
    }
    
    public final String getType() {
        return this.type;
    }
    
    public final void setType(final String type) {
        this.type = type;
    }
}
