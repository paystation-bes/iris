package com.digitalpaytech.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.digitalpaytech.util.support.RelativeDateTime;

public class MobileTokenMapEntry implements Serializable, Comparable<MobileTokenMapEntry> {
    
    private static final long serialVersionUID = 3340766094302444975L;
    private String userName;
    private String firstName;
    private String lastName;
    private String status;
    private int statusId;
    private BigDecimal latitude;
    private BigDecimal longitude;
    private String assignedRouteName;
    private String randomId;
    private String userRandomId;
    private RelativeDateTime lastCommunicated;
    private RelativeDateTime loggedInSince;
    private String deviceName;
    private String deviceUid;
    
    public final String getDeviceName() {
        return this.deviceName;
    }
    
    public final void setDeviceName(final String deviceName) {
        this.deviceName = deviceName;
    }
    
    public final String getDeviceUid() {
        return this.deviceUid;
    }
    
    public final void setDeviceUid(final String deviceUid) {
        this.deviceUid = deviceUid;
    }
    
    public final String getUserRandomId() {
        return this.userRandomId;
    }
    
    public final void setUserRandomId(final String userRandomId) {
        this.userRandomId = userRandomId;
    }
    
    public final RelativeDateTime getLoggedInSince() {
        return this.loggedInSince;
    }
    
    public final void setLoggedInSince(final RelativeDateTime loggedInSince) {
        this.loggedInSince = loggedInSince;
    }
    
    public final String getFirstName() {
        return this.firstName;
    }
    
    public final void setFirstName(final String firstName) {
        this.firstName = firstName;
    }
    
    public final String getLastName() {
        return this.lastName;
    }
    
    public final void setLastName(final String lastName) {
        this.lastName = lastName;
    }
    
    public final String getUserName() {
        return this.userName;
    }
    
    public final void setUserName(final String userName) {
        this.userName = userName;
    }
    
    public final String getStatus() {
        return this.status;
    }
    
    public final void setStatus(final String status) {
        this.status = status;
    }
    
    public final int getStatusId() {
        return this.statusId;
    }
    
    public final void setStatusId(final int statusId) {
        this.statusId = statusId;
    }
    
    public final BigDecimal getLatitude() {
        return this.latitude;
    }
    
    public final void setLatitude(final BigDecimal latitude) {
        this.latitude = latitude;
    }
    
    public final BigDecimal getLongitude() {
        return this.longitude;
    }
    
    public final void setLongitude(final BigDecimal longitude) {
        this.longitude = longitude;
    }
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final String getAssignedRouteName() {
        return this.assignedRouteName;
    }
    
    public final void setAssignedRouteName(final String assignedRouteName) {
        this.assignedRouteName = assignedRouteName;
    }
    
    public final RelativeDateTime getLastCommunicated() {
        return this.lastCommunicated;
    }
    
    public final void setLastCommunicated(final RelativeDateTime lastCommunicated) {
        this.lastCommunicated = lastCommunicated;
    }
    
    /**
     * Compares based on Status, UserName.
     * Throws a NPE if status or username is null for either object.
     */
    @Override
    public final int compareTo(final MobileTokenMapEntry other) {
        final int statusComparison = this.status.compareTo(other.status);
        if (statusComparison != 0) {
            return statusComparison;
        }
        return this.userName.compareTo(other.userName);
    }
}
