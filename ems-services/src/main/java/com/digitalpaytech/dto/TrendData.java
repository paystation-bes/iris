package com.digitalpaytech.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class TrendData implements Serializable {
    
    private static final long serialVersionUID = -4387907122890152766L;
    
    @XStreamImplicit(itemFieldName = "dateTimeStampList")
    private List<Long> dateTimeStampList;
    @XStreamImplicit(itemFieldName = "dataPointList")
    private List<String> dataPointList;
    @XStreamImplicit(itemFieldName = "targetValueList")
    private List<String> targetValueList;
    private String unitType;
    
    public TrendData() {
        this.dateTimeStampList = new ArrayList<Long>();
        this.dataPointList = new ArrayList<String>();
        this.targetValueList = new ArrayList<String>();
    }
    
    public TrendData(final List<Long> dateTimeStampList, final List<String> dataPointList, final List<String> targetValueList, final String unitType) {
        this.dateTimeStampList = dateTimeStampList;
        this.dataPointList = dataPointList;
        this.targetValueList = targetValueList;
        this.unitType = unitType;
    }
    
    public final List<Long> getDateTimeStampList() {
        return this.dateTimeStampList;
    }
    
    public final void setDateTimeStampList(final List<Long> dateTimeStampList) {
        this.dateTimeStampList = dateTimeStampList;
    }
    
    public final List<String> getDataPointList() {
        return this.dataPointList;
    }
    
    public final void setDataPointList(final List<String> dataPointList) {
        this.dataPointList = dataPointList;
    }
    
    public final List<String> getTargetValueList() {
        return this.targetValueList;
    }
    
    public final void setTargetValueList(final List<String> targetValueList) {
        this.targetValueList = targetValueList;
    }
    
    public final String getUnitType() {
        return this.unitType;
    }
    
    public final void setUnitType(final String unitType) {
        this.unitType = unitType;
    }
}
