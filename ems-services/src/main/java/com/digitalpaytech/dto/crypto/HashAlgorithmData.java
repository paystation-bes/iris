package com.digitalpaytech.dto.crypto;

import com.digitalpaytech.service.crypto.CryptoAlgorithm;
import com.digitalpaytech.domain.HashAlgorithmType;

public class HashAlgorithmData {
    private HashAlgorithmType hashAlgorithmType;
    private CryptoAlgorithm cryptoAlgorithm;

    public HashAlgorithmData() {
    }
    
    public HashAlgorithmData(HashAlgorithmType hashAlgorithmType, CryptoAlgorithm cryptoAlgorithm) {
        this.hashAlgorithmType = hashAlgorithmType;
        this.cryptoAlgorithm = cryptoAlgorithm;
    }
    
    public byte[][] getSalts() {
        if (cryptoAlgorithm == null) {
            return null;
        }
        return this.cryptoAlgorithm.getSalts();
    }
    
    public String getHashAlgorithm() {
        if (cryptoAlgorithm == null) {
            return null;
        }        
        return this.cryptoAlgorithm.getHashAlgorithm();
    }
    
    public Boolean isForSigning() {
        if (hashAlgorithmType == null) {
            return null;
        }
        return this.hashAlgorithmType.isIsForSigning();
    }
    
    public Boolean isDeleted() {
        if (hashAlgorithmType == null) {
            return null;
        }        
        return this.hashAlgorithmType.isIsDeleted();
    }
    
    public Integer getSignatureHashAlgorithmTypeId() {
        if (hashAlgorithmType == null) {
            return null;
        }
        return this.hashAlgorithmType.getSignatureId();
    }
    
    public HashAlgorithmType getHashAlgorithmType() {
        return hashAlgorithmType;
    }
    public void setHashAlgorithmType(HashAlgorithmType hashAlgorithmType) {
        this.hashAlgorithmType = hashAlgorithmType;
    }
    public CryptoAlgorithm getCryptoAlgorithm() {
        return cryptoAlgorithm;
    }
    public void setCryptoAlgorithm(CryptoAlgorithm cryptoAlgorithm) {
        this.cryptoAlgorithm = cryptoAlgorithm;
    }
}
