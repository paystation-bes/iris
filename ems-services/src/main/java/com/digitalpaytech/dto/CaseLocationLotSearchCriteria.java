package com.digitalpaytech.dto;

import java.io.Serializable;
import java.util.Date;

public class CaseLocationLotSearchCriteria implements Serializable {
    private static final long serialVersionUID = 5307805811893851311L;
    private Integer customerId;
    private Integer locationId;
    private Integer caseLocationLotId;
    
    private Integer page;
    private Integer itemsPerPage;
    
    private Date maxUpdatedTime;
    
    public final Integer getPage() {
        return this.page;
    }
    
    public final void setPage(final Integer page) {
        this.page = page;
    }
    
    public final Integer getItemsPerPage() {
        return this.itemsPerPage;
    }
    
    public final void setItemsPerPage(final Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }
    
    public final Integer getCustomerId() {
        return this.customerId;
    }
    
    public final void setCustomerId(final Integer customerId) {
        this.customerId = customerId;
    }
    
    public final Integer getLocationId() {
        return this.locationId;
    }
    
    public final void setLocationId(final Integer locationId) {
        this.locationId = locationId;
    }
    
    public final Integer getCaseLocationLotId() {
        return this.caseLocationLotId;
    }
    
    public final void setCaseLocationLotId(final Integer caseLocationLotId) {
        this.caseLocationLotId = caseLocationLotId;
    }
    
    public final Date getMaxUpdatedTime() {
        return this.maxUpdatedTime;
    }
    
    public final void setMaxUpdatedTime(final Date maxUpdatedTime) {
        this.maxUpdatedTime = maxUpdatedTime;
    }
}
