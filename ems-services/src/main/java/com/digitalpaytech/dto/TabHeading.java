package com.digitalpaytech.dto;

import com.digitalpaytech.domain.Tab;

public class TabHeading implements java.io.Serializable {
    
    private static final long serialVersionUID = 4979039287102608018L;
    private String id;
    private Integer originalId;
    private String name;
    /** Boolean value in String to identify if user has 'currentTab' cookie value. Default is 'false'. */
    private String current = Boolean.toString(false);
    
    public TabHeading() {
    }
    
    public TabHeading(final Tab tab) {
        this.id = tab.getRandomId();
        this.name = tab.getName();
        if (tab.getId() != null) {
            this.originalId = tab.getId();
        }
    }
    
    public final String getId() {
        return this.id;
    }
    
    public final void setId(final String randomId) {
        this.id = randomId;
    }
    
    public final String getName() {
        return this.name;
    }
    
    public final void setName(final String name) {
        this.name = name;
    }
    
    public final String getCurrent() {
        return this.current;
    }
    
    public final void setCurrent(final String current) {
        this.current = current;
    }
    
    public final Integer getOriginalId() {
        return this.originalId;
    }
    
    public final void setOriginalId(final Integer originalId) {
        this.originalId = originalId;
    }
}
