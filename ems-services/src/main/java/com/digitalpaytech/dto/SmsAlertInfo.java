package com.digitalpaytech.dto;

import java.io.Serializable;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;

@StoryAlias(SmsAlertInfo.ALIAS)
public class SmsAlertInfo implements Serializable {
    public static final String ALIAS = "SMS alert info";
    public static final String ALIAS_MOBILE_NUMBER = "mobile number";
    
    private static final long serialVersionUID = -4504981349191215951L;
    
    private static final String FMT_TO_STRING = "[{0}.{1}.{2}.{3}]";
    
    private Integer smsAlertId;
    private Integer mobileNumberId;
    private Long originalPermitId;
    private String mobileNumber;
    private Date expiryDate;
    private Integer customerId;
    private Integer locationId;
    private int ticketNumber;
    private Integer pointOfSaleId;
    private int last4Digit;
    private Date purchasedDate;
    private Date originalPurchasedDate;
    private String timeZone;
    private Integer licencePlateId;
    private String plateNumber;
    private int stallNumber;
    private boolean isAutoExtended;
    private SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
    
    public SmsAlertInfo() {
        
    }
    
    @StoryAlias("sms alert id")
    public final Integer getSmsAlertId() {
        return this.smsAlertId;
    }
    
    public final void setSmsAlertId(final Integer smsAlertId) {
        this.smsAlertId = smsAlertId;
    }
    
    public final Integer getMobileNumberId() {
        return this.mobileNumberId;
    }
    
    public final void setMobileNumberId(final Integer mobileNumberId) {
        this.mobileNumberId = mobileNumberId;
    }
    
    public final void setMobileNumber(final String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
    
    @StoryAlias(value = ALIAS_MOBILE_NUMBER)
    public final String getMobileNumber() {
        return this.mobileNumber;
    }
    
    @StoryAlias(value = "original permit id")
    public final Long getOriginalPermitId() {
        return this.originalPermitId;
    }
    
    public final void setOriginalPermitId(final Long originalPermitId) {
        this.originalPermitId = originalPermitId;
    }
    
    public final void setExpiryDate(final Date expiryDate) {
        this.expiryDate = expiryDate;
    }
    
    @StoryAlias(value = "expiry date")
    public final Date getExpiryDate() {
        return this.expiryDate;
    }
    
    @StoryAlias("customer id")
    public final Integer getCustomerId() {
        return this.customerId;
    }
    
    public final void setCustomerId(final Integer customerId) {
        this.customerId = customerId;
    }
    
    public final void setPurchasedDate(final Date purchasedDate) {
        this.purchasedDate = purchasedDate;
    }
    
    public final Date getPurchasedDate() {
        return this.purchasedDate;
    }
    
    public final void setOriginalPurchasedDate(final Date originalPurchasedDate) {
        this.originalPurchasedDate = originalPurchasedDate;
    }
    
    public final Date getOriginalPurchasedDate() {
        return this.originalPurchasedDate;
    }
    
    @StoryAlias(value = "location id")
    public final Integer getLocationId() {
        return this.locationId;
    }
    
    public final void setLocationId(final Integer locationId) {
        this.locationId = locationId;
    }
    
    public final Integer getPointOfSaleId() {
        return this.pointOfSaleId;
    }
    
    public final void setPointOfSaleId(final Integer pointOfSaleId) {
        this.pointOfSaleId = pointOfSaleId;
    }
    
    public final void setTicketNumber(final int ticketNumber) {
        this.ticketNumber = ticketNumber;
    }
    
    public final int getTicketNumber() {
        return this.ticketNumber;
    }
    
    public final void setLast4Digit(final int last4Digit) {
        this.last4Digit = last4Digit;
    }
    
    public final int getLast4Digit() {
        return this.last4Digit;
    }
    
    public final void setTimeZone(final String timeZone) {
        this.timeZone = timeZone;
    }
    
    @StoryAlias(value = "time zone")
    public final String getTimeZone() {
        if (this.timeZone == null || this.timeZone.trim().length() == 0) {
            this.timeZone = WebCoreConstants.GMT;
        }
        return this.timeZone;
    }
    
    public final void setPlateNumber(final String plateNumber) {
        this.plateNumber = plateNumber;
    }
    
    public final String getPlateNumber() {
        return this.plateNumber;
    }
    
    public final void setStallNumber(final int stallNumber) {
        this.stallNumber = stallNumber;
    }
    
    public final int getStallNumber() {
        return this.stallNumber;
    }
    
    public final String getExpiryDateDisplayTime() {
        if (this.timeZone == null || this.timeZone.trim().length() == 0) {
            this.timeZone = WebCoreConstants.GMT;
        }
        this.sdf.setTimeZone(TimeZone.getTimeZone(this.timeZone));
        return this.sdf.format(this.expiryDate);
    }
    
    public final String getExpiryDateDisplayTime(final Date newExpiryGMTDate) {
        if (this.timeZone == null || this.timeZone.trim().length() == 0) {
            this.timeZone = WebCoreConstants.GMT;
        }
        this.sdf.setTimeZone(TimeZone.getTimeZone(this.timeZone));
        return this.sdf.format(newExpiryGMTDate);
    }
    
    public final Date getExpiryDateLocal() {
        if (this.timeZone == null || this.timeZone.trim().length() == 0) {
            this.timeZone = WebCoreConstants.GMT;
        }
        final Date expiryDateLocal = DateUtil.changeTimeZone(this.expiryDate, this.timeZone);
        return expiryDateLocal;
    }
    
    public final boolean getIsAutoExtended() {
        return this.isAutoExtended;
    }
    
    public final void setIsAutoExtended(final boolean isAutoExtended) {
        this.isAutoExtended = isAutoExtended;
    }
    
    public final Integer getLicencePlateId() {
        return this.licencePlateId;
    }
    
    public final void setLicencePlateId(final Integer licencePlateId) {
        this.licencePlateId = licencePlateId;
    }
    
    public final String toString() {
        return MessageFormat.format(FMT_TO_STRING, this.mobileNumber, this.pointOfSaleId, this.purchasedDate, this.ticketNumber);
    }
}
