package com.digitalpaytech.dto;

import com.digitalpaytech.domain.Widget;

public class WidgetMasterListInfo {
    
    private String id;
    private String name;
    private String description;
    private int metricType;
    
    public WidgetMasterListInfo() {
    }
    
    public WidgetMasterListInfo(final String randomId, final String name, final String description, final int metricType) {
        this.id = randomId;
        this.name = name;
        this.description = description;
        this.metricType = metricType;
    }
    
    public WidgetMasterListInfo(final Widget widget) {
        this.name = widget.getName();
        this.description = widget.getDescription();
        this.metricType = widget.getWidgetMetricType().getId();
        
        if (this.description == null) {
            this.description = "Description not found";
        }
    }
    
    public final String getId() {
        return this.id;
    }
    
    public final void setId(final String id) {
        this.id = id;
    }
    
    public final String getName() {
        return this.name;
    }
    
    public final void setName(final String name) {
        this.name = name;
    }
    
    public final String getDescription() {
        return this.description;
    }
    
    public final void setDescription(final String description) {
        this.description = description;
    }
    
    public final int getMetricType() {
        return this.metricType;
    }
    
    public final void setMetricType(final int metricType) {
        this.metricType = metricType;
    }
    
}
