package com.digitalpaytech.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import com.digitalpaytech.util.WebCoreConstants;

public class AlertSearchCriteria implements Serializable {
    private static final long serialVersionUID = -3117721779660980251L;
    
    private Boolean isAlertCentre;
    private Boolean active;
    private boolean findAll;
    
    private Integer customerId;
    private Integer pointOfSaleId;
    private Integer locationId;
    private Integer routeId;
    
    private Integer severityId;
    private Integer eventDeviceId;
    
    private Date minClearedTime;
    private Date maxClearedTime;
    
    private Integer page = 0;
    private Integer itemsPerPage = WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT;
    private Integer targetId;
    
    private Collection<Byte> alertClassTypeIds;
    private Collection<Integer> alertThresholdTypeIds;
    
    private Date currentDataTime;
    private Date minTimeWithData;
    private long millisecsPerPage;
    
    private boolean onlyPayStation;
    
    public AlertSearchCriteria() {
        
    }
    
    public final Integer getSeverityId() {
        return this.severityId;
    }
    
    public final void setSeverityId(final Integer severityId) {
        this.severityId = severityId;
    }
    
    public final Integer getEventDeviceId() {
        return this.eventDeviceId;
    }
    
    public final void setEventDeviceId(final Integer eventDeviceId) {
        this.eventDeviceId = eventDeviceId;
    }
    
    public final Integer getPointOfSaleId() {
        return this.pointOfSaleId;
    }
    
    public final void setPointOfSaleId(final Integer pointOfSaleId) {
        this.pointOfSaleId = pointOfSaleId;
    }
    
    public final Integer getLocationId() {
        return this.locationId;
    }
    
    public final void setLocationId(final Integer locationId) {
        this.locationId = locationId;
    }
    
    public final Integer getRouteId() {
        return this.routeId;
    }
    
    public final void setRouteId(final Integer routeId) {
        this.routeId = routeId;
    }
    
    public final Integer getCustomerId() {
        return this.customerId;
    }
    
    public final void setCustomerId(final Integer customerId) {
        this.customerId = customerId;
    }
    
    public final Collection<Byte> getAlertClassTypeIds() {
        return this.alertClassTypeIds;
    }
    
    public final void setAlertClassTypeIds(final Collection<Byte> alertClassTypeIds) {
        this.alertClassTypeIds = alertClassTypeIds;
    }
    
    public final Boolean isActive() {
        return this.active;
    }
    
    public final void setActive(final Boolean active) {
        this.active = active;
    }
    
    public final boolean isFindAll() {
        return this.findAll;
    }
    
    public final void setFindAll(final boolean findAll) {
        this.findAll = findAll;
    }
    
    public final Integer getPage() {
        return this.page;
    }
    
    public final void setPage(final Integer page) {
        this.page = page;
    }
    
    public final Integer getItemsPerPage() {
        return this.itemsPerPage;
    }
    
    public final void setItemsPerPage(final Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }
    
    public final Integer getTargetId() {
        return this.targetId;
    }
    
    public final void setTargetId(final Integer targetId) {
        this.targetId = targetId;
    }
    
    public final Date getMinClearedTime() {
        return this.minClearedTime;
    }
    
    public final void setMinClearedTime(final Date minClearedTime) {
        this.minClearedTime = minClearedTime;
    }
    
    public final Date getMaxClearedTime() {
        return this.maxClearedTime;
    }
    
    public final void setMaxClearedTime(final Date maxClearedTime) {
        this.maxClearedTime = maxClearedTime;
    }
    
    public final Collection<Integer> getAlertThresholdTypeIds() {
        return this.alertThresholdTypeIds;
    }
    
    public final void setAlertThresholdTypeIds(final Collection<Integer> alertThresholdTypeIds) {
        this.alertThresholdTypeIds = alertThresholdTypeIds;
    }
    
    public final Boolean getIsAlertCentre() {
        return this.isAlertCentre;
    }
    
    public final void setIsAlertCentre(final Boolean isAlertCentre) {
        this.isAlertCentre = isAlertCentre;
    }

    public final Date getMinTimeWithData() {
        return this.minTimeWithData;
    }

    public final void setMinTimeWithData(final Date minTimeWithData) {
        this.minTimeWithData = minTimeWithData;
    }

    public final long getMillisecsPerPage() {
        return this.millisecsPerPage;
    }

    public final void setMillisecsPerPage(final long millisecsPerPage) {
        this.millisecsPerPage = millisecsPerPage;
    }

    public final Date getCurrentDataTime() {
        return this.currentDataTime;
    }

    public final void setCurrentDataTime(final Date currentDataTime) {
        this.currentDataTime = currentDataTime;
    }

    public final boolean isOnlyPayStation() {
        return this.onlyPayStation;
    }

    public final void setOnlyPayStation(final boolean onlyPayStation) {
        this.onlyPayStation = onlyPayStation;
    }
}
