package com.digitalpaytech.dto;

import java.io.Serializable;

import com.digitalpaytech.util.support.WebObjectId;

public class PayStationSelectorDTO implements Serializable {
    
    private static final long serialVersionUID = -1341805239926676378L;
    private WebObjectId randomId;
    private String name;
    private String serial;
    private String status;
    
    public PayStationSelectorDTO() {
        
    }
    
    public PayStationSelectorDTO(final WebObjectId randomId, final String name, final String serial) {
        this.randomId = randomId;
        this.name = name;
        this.serial = serial;
    }
    
    public final WebObjectId getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final WebObjectId randomId) {
        this.randomId = randomId;
    }
    
    public final String getName() {
        return this.name;
    }
    
    public final void setName(final String name) {
        this.name = name;
    }
    
    public final String getSerial() {
        return this.serial;
    }
    
    public final void setSerial(final String serial) {
        this.serial = serial;
    }
    
    public final String getStatus() {
        return this.status;
    }
    
    public final void setStatus(final String status) {
        this.status = status;
    }
}
