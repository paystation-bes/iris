package com.digitalpaytech.dto;

import java.io.Serializable;

public class PlateCheck implements Serializable {
    
    private static final long serialVersionUID = 5151495161608457084L;
    
    private String licensePlateNo;
    private String locationName;
    private String localDate;
    private Integer customerId;
    private boolean isPreferredByLocation;
    private boolean isLimitedByLocation;
    
    public PlateCheck() {
        super();
    }
    
    public PlateCheck(final String licensePlateNo, final String locationName, final String localDate, final Integer customerId,
            final boolean isPreferredByLocation, final boolean isLimitedByLocation) {
        super();
        this.licensePlateNo = licensePlateNo;
        this.locationName = locationName;
        this.localDate = localDate;
        this.customerId = customerId;
        this.isPreferredByLocation = isPreferredByLocation;
        this.isLimitedByLocation = isLimitedByLocation;
    }
    
    public final String getLicensePlateNo() {
        return this.licensePlateNo;
    }
    
    public final void setLicensePlateNo(final String licensePlateNo) {
        this.licensePlateNo = licensePlateNo;
    }
    
    public final String getLocationName() {
        return this.locationName;
    }
    
    public final void setLocationName(final String locationName) {
        this.locationName = locationName;
    }
    
    public final String getLocalDate() {
        return this.localDate;
    }
    
    public final void setLocalDate(final String localDate) {
        this.localDate = localDate;
    }
    
    public final Integer getCustomerId() {
        return this.customerId;
    }
    
    public final void setCustomerId(final Integer customerId) {
        this.customerId = customerId;
    }
    
    public final boolean getIsLimitedByLocation() {
        return isLimitedByLocation;
    }
    
    public final void setIsLimitedByLocation(final boolean isLimitedByLocation) {
        this.isLimitedByLocation = isLimitedByLocation;
    }
    
    public final boolean getIsPreferredByLocation() {
        return isPreferredByLocation;
    }
    
    public final void setIsPreferredByLocation(final boolean isPreferredByLocation) {
        this.isPreferredByLocation = isPreferredByLocation;
    }
}
