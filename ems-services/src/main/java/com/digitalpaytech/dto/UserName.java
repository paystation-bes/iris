package com.digitalpaytech.dto;

public class UserName {
    
    private String firstName;
    private String lastName;
    private String customerName;
    
    public UserName() {
        
    }
    
    public UserName(final String firstName, final String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    
    public UserName(final String firstName, final String lastName, final String customerName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.customerName = customerName;
    }
    
    public final String getFirstName() {
        return this.firstName;
    }
    
    public final void setFirstName(final String firstName) {
        this.firstName = firstName;
    }
    
    public final String getLastName() {
        return this.lastName;
    }
    
    public final void setLastName(final String lastName) {
        this.lastName = lastName;
    }
    
    public final String getCustomerName() {
        return this.customerName;
    }
    
    public final void setCustomerName(final String customerName) {
        this.customerName = customerName;
    }
    
}
