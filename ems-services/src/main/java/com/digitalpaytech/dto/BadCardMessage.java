package com.digitalpaytech.dto;

import java.io.Serializable;
import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BadCardMessage implements Serializable {
    private static final long serialVersionUID = 3690466259182723891L;
    
    @JsonProperty
    private String id;
    
    private String sha1CardHash;
    
    @JsonProperty
    private String sha256CardHash;
    
    @JsonProperty
    private String hashType;
    
    @JsonProperty
    private String last4Digits;
    
    @JsonProperty
    private String cardExpiry;
    
    @JsonProperty
    private String cardType;
    
    @JsonProperty
    private String cardParentType;
    
    @JsonProperty
    private Integer customerId;
    
    @JsonProperty
    private String source;
    
    @JsonProperty
    private Instant addedUTC;
    
    @JsonProperty
    private String comment;
    
    public BadCardMessage() {
        // Default constructor
    }
    
    public String getId() {
        return this.id;
    }
    
    public void setId(final String id) {
        this.id = id;
    }
    
    public String getSha1CardHash() {
        return this.sha1CardHash;
    }
    
    public void setSha1CardHash(final String sha1CardHash) {
        this.sha1CardHash = sha1CardHash;
    }
    
    public String getSha256CardHash() {
        return this.sha256CardHash;
    }
    
    public void setSha256CardHash(final String sha256CardHash) {
        this.sha256CardHash = sha256CardHash;
    }
    
    public String getLast4Digits() {
        return this.last4Digits;
    }
    
    public void setLast4Digits(final String last4Digits) {
        this.last4Digits = last4Digits;
    }
    
    public String getCardExpiry() {
        return this.cardExpiry;
    }
    
    public void setCardExpiry(final String cardExpiry) {
        this.cardExpiry = cardExpiry;
    }
    
    public String getCardType() {
        return this.cardType;
    }
    
    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }
    
    public String getCardParentType() {
        return this.cardParentType;
    }
    
    public void setCardParentType(final String cardParentType) {
        this.cardParentType = cardParentType;
    }
    
    public Integer getCustomerId() {
        return this.customerId;
    }
    
    public void setCustomerId(final Integer customerId) {
        this.customerId = customerId;
    }
    
    public String getSource() {
        return this.source;
    }
    
    public void setSource(final String source) {
        this.source = source;
    }
    
    public String getComment() {
        return this.comment;
    }
    
    public void setComment(final String comment) {
        this.comment = comment;
    }
    
    @JsonProperty("addedUTC")
    public final String getAddedUTC() {
        return this.addedUTC.toString();
    }
    
    public final Instant getAddedUTCInstant() {
        return this.addedUTC;
    }
    
    @JsonProperty("addedUTC")
    public final void setAddedUTC(final String addedUTCString) {
        this.addedUTC = Instant.parse(addedUTCString);
    }
    
    public final void setAddedUTCInstant(final Instant addedUTCInstant) {
        this.addedUTC = addedUTCInstant;
    }
    
    public String getHashType() {
        return this.hashType;
    }
    
    public void setHashType(final String hashType) {
        this.hashType = hashType;
    }
    
}
