package com.digitalpaytech.dto;

import java.io.Serializable;

import com.digitalpaytech.util.support.WebObjectId;

public class FlexLocationFacilitySelectorDTO implements Serializable {

    private static final long serialVersionUID = -1728492057557663384L;
    
    private WebObjectId randomId;
    private String facCode;
    private String facDescription;
    private String status;
    
    public WebObjectId getRandomId() {
        return randomId;
    }
    public void setRandomId(WebObjectId randomId) {
        this.randomId = randomId;
    }
    public String getFacCode() {
        return facCode;
    }
    public void setFacCode(String facCode) {
        this.facCode = facCode;
    }
    public String getFacDescription() {
        return facDescription;
    }
    public void setFacDescription(String facDescription) {
        this.facDescription = facDescription;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    
}
