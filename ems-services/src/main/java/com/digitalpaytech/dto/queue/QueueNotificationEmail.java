package com.digitalpaytech.dto.queue;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;

import com.digitalpaytech.annotation.StoryAlias;

@StoryAlias(QueueNotificationEmail.ALIAS)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QueueNotificationEmail implements Serializable {
    public static final String ALIAS = "Queue Notification Email";
    public static final String SUBJECT_ALIAS = "subject";
    public static final String CONTENT_ALIAS = "content";
    public static final String ADDRESS_ALIAS = "address";
    private static final long serialVersionUID = -1754587593207665259L;
    private String subject;
    private String content;
    private String toAddress;
    
    @StoryAlias(SUBJECT_ALIAS)
    public final String getSubject() {
        return this.subject;
    }
    
    public final void setSubject(final String subject) {
        this.subject = subject;
    }
    
    @StoryAlias(CONTENT_ALIAS)
    public final String getContent() {
        return this.content;
    }
    
    public final void setContent(final String content) {
        this.content = content;
    }
    
    @StoryAlias(ADDRESS_ALIAS)
    public final String getToAddress() {
        return this.toAddress;
    }
    
    public final void setToAddress(final String toAddress) {
        this.toAddress = toAddress;
    }
    
}
