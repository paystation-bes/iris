package com.digitalpaytech.dto.queue;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.digitalpaytech.annotation.StorageType;
import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.annotation.StoryLookup;
import com.digitalpaytech.annotation.StoryTransformer;
import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.EventActionType;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.EventType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.util.StableDateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

@StoryAlias(QueueEvent.ALIAS)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QueueEvent implements Serializable {
    public static final String ALIAS = "queue event";
    public static final String ALIAS_ACTIVE = "active";
    public static final String ALIAS_SEVERITY = "severity";
    public static final String ALIAS_ALERT_TYPE = "customer alert type";
    public static final String ALIAS_NOTIFICATION = "notification";
    private static final long serialVersionUID = 2612918327464322680L;
    private int pointOfSaleId;
    private Integer customerAlertTypeId;
    private byte eventTypeId;
    private int eventSeverityTypeId;
    private Integer eventActionTypeId;
    private String alertInfo;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StableDateUtil.UTC_TRANSACTION_DATE_TIME_FORMAT, timezone = StableDateUtil.UTC)
    private Date timestampGMT;
    private boolean isActive;
    private boolean isNotification;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StableDateUtil.UTC_TRANSACTION_DATE_TIME_FORMAT, timezone = StableDateUtil.UTC)
    private Date createdGMT;
    private int lastModifiedByUserId;
    
    @StoryLookup(type = PointOfSale.ALIAS, searchAttribute = PointOfSale.ALIAS_NAME, returnAttribute = PointOfSale.ALIAS_ID, storage = StorageType.DB)
    @StoryAlias(value = PointOfSale.ALIAS)
    public final int getPointOfSaleId() {
        return this.pointOfSaleId;
    }
    
    public final void setPointOfSaleId(final int pointOfSaleId) {
        this.pointOfSaleId = pointOfSaleId;
    }
    
    @StoryAlias(CustomerAlertType.ALIAS)
    @StoryLookup(type = CustomerAlertType.ALIAS, returnAttribute = CustomerAlertType.ALIAS_ID, searchAttribute = CustomerAlertType.ALIAS_NAME, storage = StorageType.DB)
    public final Integer getCustomerAlertTypeId() {
        return this.customerAlertTypeId;
    }
    
    public final void setCustomerAlertTypeId(final Integer customerAlertTypeId) {
        this.customerAlertTypeId = customerAlertTypeId;
    }
    
    @StoryAlias(value = EventType.ALIAS)
    @StoryTransformer("eventType")
    @StoryLookup(type = EventType.ALIAS, searchAttribute = EventType.ALIAS_NAME, returnAttribute = EventType.ALIAS_ID)
    public final byte getEventTypeId() {
        return this.eventTypeId;
    }
    
    public final void setEventTypeId(final byte eventTypeId) {
        this.eventTypeId = eventTypeId;
    }
    
    @StoryLookup(type = EventSeverityType.ALIAS, searchAttribute = EventSeverityType.ALIAS_NAME, returnAttribute = EventSeverityType.ALIAS_ID)
    @StoryAlias(value = EventSeverityType.ALIAS)
    public final int getEventSeverityTypeId() {
        return this.eventSeverityTypeId;
    }
    
    public final void setEventSeverityTypeId(final int eventSeverityTypeId) {
        this.eventSeverityTypeId = eventSeverityTypeId;
    }
    
    @StoryLookup(type = EventActionType.ALIAS, searchAttribute = EventActionType.ALIAS_NAME, returnAttribute = EventActionType.ALIAS_ID)
    @StoryAlias(value = EventActionType.ALIAS)
    public final Integer getEventActionTypeId() {
        return this.eventActionTypeId;
    }
    
    public final void setEventActionTypeId(final Integer eventActionTypeId) {
        this.eventActionTypeId = eventActionTypeId;
    }
    
    public final String getAlertInfo() {
        return this.alertInfo;
    }
    
    public final void setAlertInfo(final String alertInfo) {
        this.alertInfo = alertInfo;
    }
    
    @StoryAlias("Event Time")
    public final Date getTimestampGMT() {
        return this.timestampGMT;
    }
    
    public final void setTimestampGMT(final Date timestampGMT) {
        this.timestampGMT = timestampGMT;
    }
    
    @StoryAlias(ALIAS_ACTIVE)
    public final boolean getIsActive() {
        return this.isActive;
    }
    
    public final void setIsActive(final boolean isActive) {
        this.isActive = isActive;
    }
    
    @StoryAlias(ALIAS_NOTIFICATION)
    public final boolean getIsNotification() {
        return this.isNotification;
    }
    
    public final void setIsNotification(final boolean isNotification) {
        this.isNotification = isNotification;
    }
    
    public final Date getCreatedGMT() {
        return this.createdGMT;
    }
    
    public final void setCreatedGMT(final Date createdGMT) {
        this.createdGMT = createdGMT;
    }
    
    public final int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }
    
    public final void setLastModifiedByUserId(final int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("pointOfSaleId", pointOfSaleId).append("customerAlertTypeId", customerAlertTypeId)
                .append("eventTypeId", eventTypeId).append("eventSeverityTypeId", eventSeverityTypeId).append("eventActionTypeId", eventActionTypeId)
                .append("alertInfo", alertInfo).append("timestampGMT", timestampGMT).append("isActive", isActive)
                .append("isNotification", isNotification).append("createdGMT", createdGMT).append("lastModifiedByUserId", lastModifiedByUserId)
                .toString();
    }
    
    public final static QueueEvent newInstance(final QueueEvent source) {
        synchronized (source) {
            QueueEvent newQueueEvent = new QueueEvent();
            
            newQueueEvent.setPointOfSaleId(source.getPointOfSaleId());
            newQueueEvent.setCustomerAlertTypeId(source.getCustomerAlertTypeId());
            newQueueEvent.setEventTypeId(source.getEventTypeId());
            newQueueEvent.setEventSeverityTypeId(source.getEventSeverityTypeId());
            newQueueEvent.setEventActionTypeId(source.getEventActionTypeId());
            newQueueEvent.setAlertInfo(source.getAlertInfo());
            newQueueEvent.setTimestampGMT(new Date(source.getTimestampGMT().getTime()));
            newQueueEvent.setIsActive(source.getIsActive());
            newQueueEvent.setIsNotification(source.getIsNotification());
            newQueueEvent.setCreatedGMT(new Date(source.getCreatedGMT().getTime()));
            newQueueEvent.setLastModifiedByUserId(source.getLastModifiedByUserId());
            
            return newQueueEvent;
        }
    }
}