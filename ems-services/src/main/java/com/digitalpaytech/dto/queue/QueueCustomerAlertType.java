package com.digitalpaytech.dto.queue;

import java.io.Serializable;
import java.util.List;

import com.digitalpaytech.annotation.StorageType;
import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.annotation.StoryLookup;
import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.PointOfSale;

import com.fasterxml.jackson.annotation.JsonInclude;

@StoryAlias(QueueCustomerAlertType.ALIAS)
@JsonInclude(JsonInclude.Include.NON_NULL)
public final class QueueCustomerAlertType implements Serializable {
    public static final String ALIAS_CUSTOMER_ALERTS = "customer alerts";
    public static final String ALIAS_POINT_OF_SALE_IDS = "pay station";
    public static final String ALIAS_TYPE = "Type";
    public static final String ALIAS = "User Defined Alert Trigger";
    private static final long serialVersionUID = 622266562350163486L;
    
    public enum Type {
        ADDED, REMOVED, MODIFIED, COMMUNICATION
    }
    
    private Type action;
    private Integer limit;
    private List<Integer> pointOfSaleIds;
    private List<Integer> customerAlertTypeIds;
    
    public QueueCustomerAlertType() {
    
    }
    
    public QueueCustomerAlertType(final Type action, final List<Integer> pointOfSaleIds, final List<Integer> customerAlertTypeIds) {
        this.action = action;
        this.pointOfSaleIds = pointOfSaleIds;
        this.customerAlertTypeIds = customerAlertTypeIds;
    }
    
    public QueueCustomerAlertType(final Type action, final Integer limit) {
        this.action = action;
        this.limit = limit;
    }
    
    public QueueCustomerAlertType(final QueueCustomerAlertType other) {
        this.action = other.action;
        this.limit = other.limit;
        this.pointOfSaleIds = other.pointOfSaleIds;
        this.customerAlertTypeIds = other.customerAlertTypeIds;
    }
    
    @StoryAlias(ALIAS_TYPE)
    public Type getAction() {
        return this.action;
    }
    
    public void setAction(final Type action) {
        this.action = action;
    }
    
    @StoryAlias(value = "limit", defaultData = "100")
    public Integer getLimit() {
        return this.limit;
    }
    
    public void setLimit(final Integer limit) {
        this.limit = limit;
    }
    
    @StoryAlias(ALIAS_POINT_OF_SALE_IDS)
    @StoryLookup(type = PointOfSale.ALIAS, returnAttribute = PointOfSale.ALIAS_ID, searchAttribute = PointOfSale.ALIAS_NAME, storage = StorageType.DB)
    public List<Integer> getPointOfSaleIds() {
        return this.pointOfSaleIds;
    }
    
    public void setPointOfSaleIds(final List<Integer> pointOfSaleIds) {
        this.pointOfSaleIds = pointOfSaleIds;
    }
    
    @StoryAlias(ALIAS_CUSTOMER_ALERTS)
    @StoryLookup(type = CustomerAlertType.ALIAS, returnAttribute = CustomerAlertType.ALIAS_ID, searchAttribute = CustomerAlertType.ALIAS_NAME, storage = StorageType.DB)
    public List<Integer> getCustomerAlertTypeIds() {
        return this.customerAlertTypeIds;
    }
    
    public void setCustomerAlertTypeIds(final List<Integer> customerAlertTypeIds) {
        this.customerAlertTypeIds = customerAlertTypeIds;
    }
}
