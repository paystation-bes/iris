package com.digitalpaytech.dto;

import com.digitalpaytech.util.WebCoreConstants;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("citatType")
public class CitatType extends SettingsType {
    private static final long serialVersionUID = 755971162128631944L;
    private String orgId;
    
    public CitatType() {
        setStatus(WebCoreConstants.ACTIVE);
    }
    
    public final String getOrgId() {
        return this.orgId;
    }
    
    public final void setOrgId(final String orgId) {
        this.orgId = orgId;
    }
}
