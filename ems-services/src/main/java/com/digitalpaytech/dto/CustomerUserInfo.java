package com.digitalpaytech.dto;

import java.io.Serializable;
import java.util.Comparator;

import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.WebCoreConstants;

public class CustomerUserInfo implements Serializable {
    
    private static final long serialVersionUID = 2239347111271763491L;
    private transient Integer userAccountId;
    private String randomId;
    private String customerName;
    private boolean isParent;
    
    public CustomerUserInfo() {
        
    }
    
    public CustomerUserInfo(final UserAccount userAccount, final RandomKeyMapping keyMapping) {
        this.userAccountId = userAccount.getId();
        this.randomId = keyMapping.getRandomString(userAccount, WebCoreConstants.ID_LOOK_UP_NAME);
        this.customerName = userAccount.getCustomer().getName();
        this.isParent = userAccount.getCustomer().isIsParent();
        
    }
    
    public final Integer getUserAccountId() {
        return this.userAccountId;
    }
    
    public final void setUserAccountId(final Integer userAccountId) {
        this.userAccountId = userAccountId;
    }
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final String getCustomerName() {
        return this.customerName;
    }
    
    public final void setCustomerName(final String customerName) {
        this.customerName = customerName;
    }
    
    public final boolean getIsParent() {
        return this.isParent;
    }
    
    public final void setIsParent(final boolean isParent) {
        this.isParent = isParent;
    }
    
    public static class CustomerNameComparator implements Comparator<CustomerUserInfo> {
        @Override
        public final int compare(final CustomerUserInfo o1, final CustomerUserInfo o2) {
            final String s1 = o1.getCustomerName().toLowerCase().replaceAll("\\s+","");
            final String s2 = o2.getCustomerName().toLowerCase().replaceAll("\\s+","");
            return s1.compareTo(s2);
        }
    }    
    
}
