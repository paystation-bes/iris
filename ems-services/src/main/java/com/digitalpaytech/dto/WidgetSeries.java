package com.digitalpaytech.dto;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class WidgetSeries implements java.io.Serializable {
    
    /*
     * Number of widget tiers
     * | 1 | 2 | 3 |
     * x - axis | Tier 1 | Tier 1 | Tier 1 |
     * Series | / | Tier 2 | Tier 3 |
     * Stack | / | / | Tier 2 |
     */
    private static final long serialVersionUID = -5050012655339894449L;
    
    // Series names are multiple lines in a chart, or 'stacked' columns
    private String seriesName;
    // Stacks are for column names in a single x-axis interval
    private String stackName;
    
    @XStreamImplicit(itemFieldName = "seriesData")
    private List<String> seriesData;
    
    public WidgetSeries() {
        this.seriesData = new ArrayList<String>();
    }
    
    public WidgetSeries(final String seriesName, final String stackName, final List<String> seriesData) {
        this.seriesName = seriesName;
        this.stackName = stackName;
        this.seriesData = seriesData;
    }
    
    public final String getSeriesName() {
        return this.seriesName;
    }
    
    public final void setSeriesName(final String seriesName) {
        this.seriesName = seriesName;
    }
    
    public final String getStackName() {
        return this.stackName;
    }
    
    public final void setStackName(final String stackName) {
        this.stackName = stackName;
    }
    
    public final List<String> getSeriesData() {
        return this.seriesData;
    }
    
    public final void setSeriesData(final List<String> seriesData) {
        this.seriesData = seriesData;
    }
    
    public final void insertSeriesData(final int index, final String data) {
        if (this.seriesData.size() <= index) {
            return;
        }
        this.seriesData.set(index, data);
    }
    
    public final void addSeriesData(final String data) {
        this.seriesData.add(data);
    }
    
    public final void addAll(final WidgetSeries widgetSeries) {
        this.seriesData.addAll(widgetSeries.getSeriesData());
    }
    
}
