package com.digitalpaytech.dto;

import java.util.Iterator;
import java.util.List;

import org.hibernate.transform.AliasToBeanResultTransformer;

import com.digitalpaytech.util.RandomKeyMapping;

public class FilterDTOTransformer extends AliasToBeanResultTransformer {
    
    private static final long serialVersionUID = 4012143033654381110L;
    
    private RandomKeyMapping keyMapping;
    private Class<?> persistentClass;
    private List<FilterDTO> result;
    private Filter filter;
    
    private int labelIdx = -1;
    private int valueIdx = -1;
    private int descIdx = -1;
    private int extraDataIdx = -1;
    
    public FilterDTOTransformer(final RandomKeyMapping keyMapping, final Class<?> persistentClass) {
        this(keyMapping, persistentClass, (List<FilterDTO>) null);
    }
    
    public FilterDTOTransformer(final RandomKeyMapping keyMapping, final Class<?> persistentClass, final List<FilterDTO> result) {
        this(keyMapping, persistentClass, result, (Filter) null);
    }
    
    public FilterDTOTransformer(final RandomKeyMapping keyMapping, final Class<?> persistentClass, final List<FilterDTO> result, final Filter filter) {
        super(FilterDTO.class);
        this.keyMapping = keyMapping;
        this.persistentClass = persistentClass;
        this.result = result;
        
        this.filter = filter;
    }
    
    @Override
    public final Object transformTuple(final Object[] tuple, final String[] aliases) {
        if (this.labelIdx < 0) {
            resolveIdx(tuple, aliases);
        }
        
        FilterDTO dto = new FilterDTO();
        dto.setLabel((String) tuple[this.labelIdx]);
        dto.setId((Number) tuple[this.valueIdx]);
        dto.setValue(this.keyMapping.getRandomString(this.persistentClass, dto.getId()));
        if (this.descIdx >= 0) {
            dto.setDesc((String) tuple[this.descIdx]);
        }
        
        if (this.extraDataIdx >= 0 && tuple[this.extraDataIdx] instanceof String) {
            dto.setExtraData((String) tuple[this.extraDataIdx]);
        } else if (this.extraDataIdx >= 0 && tuple[this.extraDataIdx] instanceof Boolean) {
            final boolean b = ((Boolean) tuple[this.extraDataIdx]).booleanValue();
            if (b) {
                dto.setExtraData("1");
            } else {
                dto.setExtraData("0");
            }
        }
        
        if (this.result != null) {
            if ((this.filter == null) || (this.filter.filter(dto))) {
                this.result.add(dto);
            }
            
            dto = null;
        }
        
        return dto;
    }
    
    private void resolveIdx(final Object[] tuple, final String[] aliases) {
        int idx = aliases.length;
        while (--idx >= 0) {
            if (aliases[idx].equals("label")) {
                this.labelIdx = idx;
            } else if (aliases[idx].equals("value")) {
                this.valueIdx = idx;
            } else if (aliases[idx].equals("desc")) {
                this.descIdx = idx;
            } else if (aliases[idx].equals("extraData")) {
                this.extraDataIdx = idx;
            }
        }
    }
    
    @SuppressWarnings("rawtypes")
    @Override
    public final List transformList(final List collection) {
        List result;
        if (this.result != null) {
            result = this.result;
        } else {
            result = super.transformList(collection);
            if (this.filter != null) {
                @SuppressWarnings("unchecked")
                final Iterator<FilterDTO> itr = result.iterator();
                while (itr.hasNext()) {
                    final FilterDTO dto = itr.next();
                    if (!this.filter.filter(dto)) {
                        itr.remove();
                    }
                }
            }
        }
        
        return result;
    }
    
    public interface Filter {
        boolean filter(FilterDTO dto);
    }
    
}
