package com.digitalpaytech.dto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("tier1")
public class Tier1 {
    
    @XStreamAsAttribute
    private String type;
    
    @XStreamImplicit(itemFieldName = "tier2")
    private List<Tier2> tier2s;
    
    public Tier1() {
    }
    
    public Tier1(final String type, final List<Tier2> tier2s) {
        this.type = type;
        this.tier2s = tier2s;
    }
    
    /**
     * Finds Tier2 objects that match with input tier2 value.
     * e.g. value is 'Location' and there are 4 Tier2 object with same range & tier 1 values.
     * 
     * @param tier2
     *            String tier 2 value, e.g. none (empty string, ""), Parent Organization, Organization, Location, Route, Rate, TransactionType or RevenueType.
     * @return List<Tier2> Returns empty list if couldn't find suitable Tier2 data, or input tier2 is null.
     */
    public final List<Tier2> getTier2s(final String tier2) {
        final List<Tier2> tier2ObjsList = new ArrayList<Tier2>();
        if (this.tier2s != null) {
            final Iterator<Tier2> iter = this.tier2s.iterator();
            while (iter.hasNext()) {
                final Tier2 tier2Obj = iter.next();
                if (tier2 != null && tier2.trim().length() != 0 && tier2.equalsIgnoreCase(tier2Obj.getType())) {
                    tier2ObjsList.add(tier2Obj);
                }
            }
        }
        return tier2ObjsList;
    }
    
    @Override
    public final String toString() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("Tier1 type: ").append(this.type).append(", Tier2 types: ");
        Tier2 tier2;
        String s;
        // tier2s could be null if there value is blank (empty) or no tier 2 data.
        if (this.tier2s != null) {
            final Iterator<Tier2> iter = this.tier2s.iterator();
            while (iter.hasNext()) {
                tier2 = iter.next();
                if (tier2 == null) {
                    s = "none";
                } else {
                    s = tier2.toString();
                }
                bdr.append(s).append(", ");
            }
        } else {
            bdr.append("none");
        }
        return bdr.toString();
    }
    
    public final String getType() {
        return this.type;
    }
    
    public final void setType(final String type) {
        this.type = type;
    }
    
    public final List<Tier2> getTier2s() {
        return this.tier2s;
    }
    
    public final void setTier2s(final List<Tier2> tier2s) {
        this.tier2s = tier2s;
    }
}
