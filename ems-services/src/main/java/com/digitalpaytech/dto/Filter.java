package com.digitalpaytech.dto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("filter")
public class Filter {
    
    @XStreamAsAttribute
    private String type;
    
    @XStreamImplicit(itemFieldName = "tier1")
    private List<Tier1> tier1s;
    
    public Filter() {
    }
    
    public Filter(final String type, final List<Tier1> tier1s) {
        this.type = type;
        this.tier1s = tier1s;
    }
    
    /**
     * Finds Tier1 objects that match with input tier1 value.
     * e.g. value is 'Hour' and there are 8 Tier1 object with same tier 1 value.
     * 
     * @param tier1
     *            String tier 1 value, e.g. none (empty string, ""), Hour, Rate, Parent Organization, Organization, Location, Route, TransactionType or
     *            RevenueType.
     * @return List<Tier1> Returns empty list if couldn't find suitable Tier1 data, or input tier1 is null.
     */
    public final List<Tier1> getTier1s(final String tier1) {
        final List<Tier1> tier1ObjsList = new ArrayList<Tier1>();
        final Iterator<Tier1> iter = this.tier1s.iterator();
        while (iter.hasNext()) {
            final Tier1 tier1Obj = iter.next();
            // Input tier 1 selection is empty (all) & metric tier 1 allows empty (all).
            if (tier1 != null && tier1.trim().length() == 0 && tier1Obj.getType().trim().length() == 0) {
                tier1ObjsList.add(tier1Obj);
                
                // Input tier 1 selection not is empty, tier 1 value equals to tier1Obj in metric.
            } else if (tier1 != null && tier1.trim().length() != 0 && tier1.equalsIgnoreCase(tier1Obj.getType())) {
                tier1ObjsList.add(tier1Obj);
            }
        }
        return tier1ObjsList;
    }
    
    @Override
    public final String toString() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("Range type: ").append(this.type).append(", Tier1 types: ");
        Tier1 tier1;
        String s;
        final Iterator<Tier1> iter = this.tier1s.iterator();
        while (iter.hasNext()) {
            tier1 = iter.next();
            if (tier1 == null) {
                s = "null";
            } else {
                s = tier1.toString();
            }
            bdr.append(s).append(", ");
        }
        return bdr.toString();
    }
    
    public final String getType() {
        return this.type;
    }
    
    public final void setType(final String type) {
        this.type = type;
    }
    
    public final List<Tier1> getTier1s() {
        return this.tier1s;
    }
    
    public final void setTier1s(final List<Tier1> tier1s) {
        this.tier1s = tier1s;
    }
}
