package com.digitalpaytech.dto;

import java.util.Iterator;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("tier3")
public class Tier3 {
    
    @XStreamAsAttribute
    private String type;
    
    @XStreamImplicit(itemFieldName = "display")
    private List<Display> displays;
    
    public Tier3() {
    }
    
    public Tier3(final String type, final List<Display> displays) {
        this.type = type;
        this.displays = displays;
    }
    
    @Override
    public final String toString() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("Tier 3 type: ").append(this.type);
        return bdr.toString();
    }
    
    public final Display getDisplay(final String newType) {
        Display display;
        final Iterator<Display> iter = this.displays.iterator();
        while (iter.hasNext()) {
            display = iter.next();
            if (display.isCorrectType(newType)) {
                return display;
            }
        }
        return null;
    }
    
    public final String getType() {
        return this.type;
    }
    
    public final void setType(final String type) {
        this.type = type;
    }
    
    public final List<Display> getDisplays() {
        return this.displays;
    }
    
    public final void setDisplays(final List<Display> displays) {
        this.displays = displays;
    }
    
}
