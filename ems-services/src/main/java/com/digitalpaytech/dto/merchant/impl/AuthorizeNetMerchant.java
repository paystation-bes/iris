package com.digitalpaytech.dto.merchant.impl;

import java.util.SortedMap;
import java.util.TreeMap;

import org.springframework.stereotype.Component;

import com.digitalpaytech.client.dto.merchant.ForTransactionResponse;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.dto.merchant.MerchantDetails;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.MessageHelper;

@Component("authorizeNetMerchant")
public class AuthorizeNetMerchant implements MerchantDetails {
    private static final String MERCHANT_LOGIN_ID_KEY = "merchant.authorize.net.loginId";
    private static final String MERCHANT_PROCESSOR_KEY = "merchant.authorize.net.processor";
    private static final String MERCHANT_TRANSACTION_KEY = "merchant.authorize.net.transactionKey";
    
    
    @Override
    public final int getProcessorId() {
        return CardProcessingConstants.PROCESSOR_ID_AUTHORIZE_NET;
    }

    @Override
    public final String getProcessorType() {
        return "processor.authorize.net.link";
    }
    
    @Override
    public final SortedMap<String, String> buildTerminalDetails(final MessageHelper messageHelper, final MerchantAccount merchantAccount,
        final SortedMap<String, String> merchantDetails) {
        return new TreeMap<>();
    }
    
    @Override
    public final SortedMap<String, String> buildMerchantDetails(final MessageHelper messageHelper, final MerchantAccount merchantAccount) {
        final SortedMap<String, String> details = new TreeMap<>();
        details.put(messageHelper.getMessage(MERCHANT_LOGIN_ID_KEY), merchantAccount.getField1());
        details.put(messageHelper.getMessage(MERCHANT_PROCESSOR_KEY), merchantAccount.getField2());
        details.put(messageHelper.getMessage(MERCHANT_TRANSACTION_KEY), merchantAccount.getField3());
        return details;
    }
    
    @Override
    public final MerchantAccount buildMerchantAccount(final MessageHelper messageHelper,
                                                      final MerchantAccount merchantAccount, 
                                                      final ForTransactionResponse forTransactionResp) {
        merchantAccount.setField1(forTransactionResp.getMerchantConfiguration().get(messageHelper.getMessage(MERCHANT_LOGIN_ID_KEY)));
        merchantAccount.setField2(forTransactionResp.getMerchantConfiguration().get(messageHelper.getMessage(MERCHANT_PROCESSOR_KEY)));
        merchantAccount.setField3(forTransactionResp.getMerchantConfiguration().get(messageHelper.getMessage(MERCHANT_TRANSACTION_KEY)));
        merchantAccount.setTimeZone(forTransactionResp.getTimeZone());
        if (forTransactionResp.getCloseQuarterOfDay() != null) {
            merchantAccount.setCloseQuarterOfDay(forTransactionResp.getCloseQuarterOfDay().byteValue());
        }
        return merchantAccount;
    }
}
