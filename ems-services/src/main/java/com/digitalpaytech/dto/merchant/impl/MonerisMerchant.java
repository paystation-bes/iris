package com.digitalpaytech.dto.merchant.impl;

import java.util.SortedMap;
import java.util.TreeMap;

import org.springframework.stereotype.Component;

import com.digitalpaytech.client.dto.merchant.ForTransactionResponse;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.dto.merchant.MerchantDetails;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.MessageHelper;

@Component("monerisMerchant")
public class MonerisMerchant implements MerchantDetails {
    private static final String MERCHANT_STORE_ID_KEY = "merchant.moneris.storeId";
    private static final String MERCHANT_API_TOKEN_KEY = "merchant.moneris.apiToken";
    
    @Override
    public final int getProcessorId() {
        return CardProcessingConstants.PROCESSOR_ID_MONERIS;
    }

    @Override
    public final String getProcessorType() {
        return "processor.moneris.link";
    }
    
    @Override
    public final SortedMap<String, String> buildTerminalDetails(final MessageHelper messageHelper, final MerchantAccount merchantAccount,
        final SortedMap<String, String> merchantDetails) {
        return new TreeMap<>();
    }
    
    @Override
    public final SortedMap<String, String> buildMerchantDetails(final MessageHelper messageHelper, final MerchantAccount merchantAccount) {
        final SortedMap<String, String> details = new TreeMap<>();
        details.put(messageHelper.getMessage(MERCHANT_STORE_ID_KEY), merchantAccount.getField1());
        details.put(messageHelper.getMessage(MERCHANT_API_TOKEN_KEY), merchantAccount.getField2());
        return details;
    }
    
    @Override
    public final MerchantAccount buildMerchantAccount(final MessageHelper messageHelper,
                                                      final MerchantAccount merchantAccount, 
                                                      final ForTransactionResponse forTransactionResp) {
        merchantAccount.setField1(forTransactionResp.getMerchantConfiguration().get(messageHelper.getMessage(MERCHANT_STORE_ID_KEY)));
        merchantAccount.setField2(forTransactionResp.getMerchantConfiguration().get(messageHelper.getMessage(MERCHANT_API_TOKEN_KEY)));
        merchantAccount.setTimeZone(forTransactionResp.getTimeZone());
        if (forTransactionResp.getCloseQuarterOfDay() != null) {
            merchantAccount.setCloseQuarterOfDay(forTransactionResp.getCloseQuarterOfDay().byteValue());
        }
        return merchantAccount;
    }
}
