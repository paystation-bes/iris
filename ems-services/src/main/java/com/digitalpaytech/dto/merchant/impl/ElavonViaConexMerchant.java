package com.digitalpaytech.dto.merchant.impl;

import java.util.SortedMap;
import java.util.TreeMap;

import org.springframework.stereotype.Component;

import com.digitalpaytech.client.dto.merchant.ForTransactionResponse;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.dto.merchant.MerchantDetails;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.MessageHelper;

@Component("elavonViaConexMerchant")
public class ElavonViaConexMerchant implements MerchantDetails {
    private static final String MERCHANT_ID_KEY = "merchant.elavon.via.conex.merchantId";
    private static final String MERCHANT_CLOSE_BATCH_SIZE_KEY = "merchant.elavon.via.conex.close.batch.size";
    
    @Override
    public final int getProcessorId() {
        return CardProcessingConstants.PROCESSOR_ID_ELAVON_VIACONEX;
    }
    
    @Override
    public final String getProcessorType() {
        return "processor.elavon.viaconex.link";
    }
    
    @Override
    public final SortedMap<String, String> buildTerminalDetails(final MessageHelper messageHelper, final MerchantAccount merchantAccount,
        final SortedMap<String, String> merchantDetails) {
        final SortedMap<String, String> details = new TreeMap<>();
        details.put(messageHelper.getMessage(MerchantDetails.MERCHANT_TERMINAL_ID_NAME_KEY), merchantAccount.getField1());
        return details;
    }
    
    @Override
    public final SortedMap<String, String> buildMerchantDetails(final MessageHelper messageHelper, final MerchantAccount merchantAccount) {
        final SortedMap<String, String> details = new TreeMap<>();
        details.put(messageHelper.getMessage(MERCHANT_ID_KEY), merchantAccount.getField2());
        details.put(messageHelper.getMessage(MERCHANT_CLOSE_BATCH_SIZE_KEY), merchantAccount.getField3());
        return details;
    }

    @Override
    public final MerchantAccount buildMerchantAccount(final MessageHelper messageHelper,
                                                      final MerchantAccount merchantAccount, 
                                                      final ForTransactionResponse forTransactionResp) {
        merchantAccount.setField1(forTransactionResp.getTerminalConfiguration().get(messageHelper.getMessage(MerchantDetails.MERCHANT_TERMINAL_ID_NAME_KEY)));
        merchantAccount.setField2(forTransactionResp.getMerchantConfiguration().get(messageHelper.getMessage(MERCHANT_ID_KEY)));
        merchantAccount.setField3(forTransactionResp.getMerchantConfiguration().get(messageHelper.getMessage(MERCHANT_CLOSE_BATCH_SIZE_KEY)));
        merchantAccount.setTimeZone(forTransactionResp.getTimeZone());
        if (forTransactionResp.getCloseQuarterOfDay() != null) {
            merchantAccount.setCloseQuarterOfDay(forTransactionResp.getCloseQuarterOfDay().byteValue());
        }
        return merchantAccount;
    }
}
