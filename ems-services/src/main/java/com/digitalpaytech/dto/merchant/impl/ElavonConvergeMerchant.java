package com.digitalpaytech.dto.merchant.impl;

import java.util.SortedMap;
import java.util.TreeMap;

import org.springframework.stereotype.Component;

import com.digitalpaytech.client.dto.merchant.ForTransactionResponse;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.dto.merchant.MerchantDetails;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.MessageHelper;

@Component("elavonConvergeMerchant")
public class ElavonConvergeMerchant implements MerchantDetails {
    private static final String MERCHANT_ID_KEY = "merchant.elavon.converge.merchantId";
    private static final String MERCHANT_USER_ID_KEY = "merchant.elavon.converge.userId";
    private static final String MERCHANT_PIN_KEY = "merchant.elavon.converge.pin";
    
    @Override
    public final int getProcessorId() {
        return CardProcessingConstants.PROCESSOR_ID_ELAVON;
    }

    @Override
    public final String getProcessorType() {
        return "processor.elavon.link";
    }
                    
    @Override
    public final SortedMap<String, String> buildTerminalDetails(final MessageHelper messageHelper, final MerchantAccount merchantAccount,
        final SortedMap<String, String> merchantDetails) {
        return new TreeMap<>();
    }
    
    @Override
    public final SortedMap<String, String> buildMerchantDetails(final MessageHelper messageHelper, final MerchantAccount merchantAccount) {
        final SortedMap<String, String> details = new TreeMap<>();
        details.put(messageHelper.getMessage(MERCHANT_ID_KEY), merchantAccount.getField1());
        details.put(messageHelper.getMessage(MERCHANT_USER_ID_KEY), merchantAccount.getField2());
        details.put(messageHelper.getMessage(MERCHANT_PIN_KEY), merchantAccount.getField3());
        return details;
    }
    
    @Override
    public final MerchantAccount buildMerchantAccount(final MessageHelper messageHelper,
                                                      final MerchantAccount merchantAccount, 
                                                      final ForTransactionResponse forTransactionResp) {
        merchantAccount.setField1(forTransactionResp.getMerchantConfiguration().get(messageHelper.getMessage(MERCHANT_ID_KEY)));
        merchantAccount.setField2(forTransactionResp.getMerchantConfiguration().get(messageHelper.getMessage(MERCHANT_USER_ID_KEY)));
        merchantAccount.setField3(forTransactionResp.getMerchantConfiguration().get(messageHelper.getMessage(MERCHANT_PIN_KEY)));
        merchantAccount.setTimeZone(forTransactionResp.getTimeZone());
        if (forTransactionResp.getCloseQuarterOfDay() != null) {
            merchantAccount.setCloseQuarterOfDay(forTransactionResp.getCloseQuarterOfDay().byteValue());
        }
        return merchantAccount;
    }
}
