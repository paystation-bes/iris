package com.digitalpaytech.dto.merchant.impl;

import java.nio.charset.Charset;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.client.dto.merchant.ForTransactionResponse;
import com.digitalpaytech.client.PaymentClient;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.dto.merchant.MerchantDetails;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.ribbon.ClientFactory;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CurrencyUtil;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.json.JSON;
import com.fasterxml.jackson.core.type.TypeReference;
import com.netflix.hystrix.exception.HystrixRuntimeException;

@Component("creditcallMerchant")
public class CreditcallMerchant implements MerchantDetails {
    
    private static final String MERCHANT_CREDITCALL_CURRENCY_CODE = "merchant.creditcall.currency.code";
    private static final String MERCHANT_CREDITCALL_MERCHANT_PROCESSOR_KEY = "merchant.creditcall.merchantProcessor";
    private static final String MERCHANT_CREDITCALL_TRANSACTION_KEY = "merchant.creditcall.transactionKey";
    private static final String MERCHANT_CREDITCALL_MERCHANT_ID_KEY = "merchant.creditcall.merchantId";
    private static final String MERCHANT_CREDITCALL_CURRENCY = "merchant.creditcall.currency";
    
    @Autowired
    private ClientFactory clientFactory;
    
    private PaymentClient paymentClient;
    
    private JSON json;
    
    @PostConstruct
    public void init() {
        this.json = new JSON();
        this.paymentClient = this.clientFactory.from(PaymentClient.class);
    }
    
    @Override
    public final int getProcessorId() {
        return CardProcessingConstants.PROCESSOR_ID_CREDITCALL;
    }

    @Override
    public final String getProcessorType() {
        return "processor.creditcall.link";
    }
    
    @Override
    public final SortedMap<String, String> buildTerminalDetails(final MessageHelper messageHelper, final MerchantAccount merchantAccount,
        final SortedMap<String, String> merchantDetails) {
        
        final SortedMap<String, String> details = new TreeMap<>();
        final String terminalIdTag = messageHelper.getMessage(MerchantDetails.MERCHANT_TERMINAL_ID_NAME_KEY);
        final String currencyTag = messageHelper.getMessage(MERCHANT_CREDITCALL_CURRENCY);
        final String transactionKeyTag = messageHelper.getMessage(MERCHANT_CREDITCALL_TRANSACTION_KEY);
        
        details.put(terminalIdTag, merchantDetails.get(terminalIdTag));
        details.put(transactionKeyTag, merchantDetails.get(transactionKeyTag));
        details.put(currencyTag, merchantDetails.get(currencyTag));
        
        // remove terminal details from merchant account map
        merchantDetails.remove(terminalIdTag);
        merchantDetails.remove(transactionKeyTag);
        
        return details;
    }
    
    @Override
    public final SortedMap<String, String> buildMerchantDetails(final MessageHelper messageHelper, final MerchantAccount merchantAccount)
        throws InvalidDataException {
        try {
            final String terminalInfoJson =
                    this.paymentClient.grabTerminalInfo(merchantAccount.getTerminalToken()).execute().toString(Charset.defaultCharset());
            
            final SortedMap<String, String> details = this.json.deserialize(terminalInfoJson, new TypeReference<SortedMap<String, String>>() {
            });
            
            final String currencyTag = messageHelper.getMessage(MERCHANT_CREDITCALL_CURRENCY);
            final String currencyCodeTag = messageHelper.getMessage(MERCHANT_CREDITCALL_CURRENCY_CODE);
            details.put(currencyCodeTag, String.valueOf(CurrencyUtil.getCurrencyNumber(details.get(currencyTag))));
            
            return details;
        } catch (JsonException | HystrixRuntimeException e) {
            throw new InvalidDataException("Failed to get Credit Call Merchant Account details from payment server");
        }
        
    }

    @Override
    public final MerchantAccount buildMerchantAccount(final MessageHelper messageHelper,
                                                      final MerchantAccount merchantAccount, 
                                                      final ForTransactionResponse forTransactionResp) {
        merchantAccount.setField1(forTransactionResp.getMerchantConfiguration().get(messageHelper.getMessage(MERCHANT_CREDITCALL_MERCHANT_PROCESSOR_KEY)));
        merchantAccount.setField2(forTransactionResp.getMerchantConfiguration().get(messageHelper.getMessage(MERCHANT_CREDITCALL_MERCHANT_ID_KEY)));
        merchantAccount.setField3(forTransactionResp.getTerminalConfiguration().get(messageHelper.getMessage(MerchantDetails.MERCHANT_TERMINAL_ID_NAME_KEY)));
        merchantAccount.setField4(forTransactionResp.getTerminalConfiguration().get(messageHelper.getMessage(MERCHANT_CREDITCALL_TRANSACTION_KEY)));
        merchantAccount.setField5(forTransactionResp.getTerminalConfiguration().get(messageHelper.getMessage(MERCHANT_CREDITCALL_CURRENCY)));
        merchantAccount.setTimeZone(forTransactionResp.getTimeZone());
        if (forTransactionResp.getCloseQuarterOfDay() != null) {
            merchantAccount.setCloseQuarterOfDay(forTransactionResp.getCloseQuarterOfDay().byteValue());
        }
        return merchantAccount;
    }
}
