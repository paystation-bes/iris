package com.digitalpaytech.dto.merchant.impl;

import java.util.SortedMap;
import java.util.TreeMap;

import org.springframework.stereotype.Component;

import com.digitalpaytech.client.dto.merchant.ForTransactionResponse;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.dto.merchant.MerchantDetails;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.MessageHelper;

@Component("firstDataNashvilleMerchant")
public class FirstDataNashvilleMerchant implements MerchantDetails {
    private static final String MERCHANT_ID_KEY = "merchant.fdn.merchantId";
    private static final String DATAWIRE_ID_KEY  = "merchant.fdn.datawireId";
    private static final String URL_LIST_KEY = "merchant.fdn.urlList";
    private static final String ZIP_CODE_KEY = "merchant.fdn.zipCode";
    private static final String MERCHANT_CATEGORY_KEY = "merchant.fdn.merchantCategory";

    @Override
    public final int getProcessorId() {
        return CardProcessingConstants.PROCESSOR_ID_FIRST_DATA_NASHVILLE;
    }
    
    @Override
    public final String getProcessorType() {
        return "processor.firstDataNashville.link";
    }
    
    @Override
    public final SortedMap<String, String> buildTerminalDetails(final MessageHelper messageHelper, final MerchantAccount merchantAccount,
        final SortedMap<String, String> merchantDetails) {
        final SortedMap<String, String> details = new TreeMap<>();
        details.put(messageHelper.getMessage(MerchantDetails.MERCHANT_TERMINAL_ID_NAME_KEY), merchantAccount.getField2());
        return details;
    }
    
    @Override
    public final SortedMap<String, String> buildMerchantDetails(final MessageHelper messageHelper, final MerchantAccount merchantAccount) {
        final SortedMap<String, String> details = new TreeMap<>();
        details.put(messageHelper.getMessage(MERCHANT_ID_KEY), merchantAccount.getField1());
        details.put(messageHelper.getMessage(DATAWIRE_ID_KEY), merchantAccount.getField3());
        details.put(messageHelper.getMessage(URL_LIST_KEY), merchantAccount.getField4());
        details.put(messageHelper.getMessage(ZIP_CODE_KEY), merchantAccount.getField5());
        details.put(messageHelper.getMessage(MERCHANT_CATEGORY_KEY), merchantAccount.getField6());
        return details;
    }
    
    @Override
    public final MerchantAccount buildMerchantAccount(final MessageHelper messageHelper,
                                                      final MerchantAccount merchantAccount, 
                                                      final ForTransactionResponse forTransactionResp) {
        merchantAccount.setField1(forTransactionResp.getMerchantConfiguration().get(messageHelper.getMessage(MERCHANT_ID_KEY)));
        merchantAccount.setField2(forTransactionResp.getTerminalConfiguration().get(messageHelper.getMessage(MerchantDetails.MERCHANT_TERMINAL_ID_NAME_KEY)));
        merchantAccount.setField3(forTransactionResp.getMerchantConfiguration().get(messageHelper.getMessage(DATAWIRE_ID_KEY)));
        merchantAccount.setField4(forTransactionResp.getMerchantConfiguration().get(messageHelper.getMessage(URL_LIST_KEY)));
        merchantAccount.setField5(forTransactionResp.getMerchantConfiguration().get(messageHelper.getMessage(ZIP_CODE_KEY)));
        merchantAccount.setField6(forTransactionResp.getMerchantConfiguration().get(messageHelper.getMessage(MERCHANT_CATEGORY_KEY)));
        merchantAccount.setTimeZone(forTransactionResp.getTimeZone());
        if (forTransactionResp.getCloseQuarterOfDay() != null) {
            merchantAccount.setCloseQuarterOfDay(forTransactionResp.getCloseQuarterOfDay().byteValue());
        }
        return merchantAccount;
    }
}
