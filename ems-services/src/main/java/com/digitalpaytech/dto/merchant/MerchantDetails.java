package com.digitalpaytech.dto.merchant;

import java.util.SortedMap;
import com.digitalpaytech.client.dto.merchant.ForTransactionResponse;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.util.MessageHelper;

public interface MerchantDetails {
    String MERCHANT_TERMINAL_ID_NAME_KEY = "merchant.terminal.id.name";
    
    int getProcessorId();
    
    String getProcessorType();
    
    SortedMap<String, String> buildMerchantDetails(MessageHelper messageHelper, MerchantAccount merchantAccount) throws InvalidDataException;
    
    SortedMap<String, String> buildTerminalDetails(MessageHelper messageHelper, MerchantAccount merchantAccount,
        SortedMap<String, String> merchantDetails);
    
    MerchantAccount buildMerchantAccount(MessageHelper messageHelper, MerchantAccount merchantAccount, ForTransactionResponse forTransactionResponse);
}
