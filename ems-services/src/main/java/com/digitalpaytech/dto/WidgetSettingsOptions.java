package com.digitalpaytech.dto;

import java.io.Serializable;
import java.util.List;

import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WidgetConstants;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class WidgetSettingsOptions implements Serializable {
    
    private static final long serialVersionUID = -5613364262042235718L;

    @XStreamImplicit(itemFieldName = "metricTypes")
    private List<MetricType> metricTypes;
    
    @XStreamImplicit(itemFieldName = "tier1Types")
    private List<TierType> tier1Types;
    
    @XStreamImplicit(itemFieldName = "tier2Types")
    private List<TierType> tier2Types;
    
    @XStreamImplicit(itemFieldName = "tier3Types")
    private List<TierType> tier3Types;
    
    @XStreamImplicit(itemFieldName = "locationTypes")
    private List<LocationType> locationTypes;
    
    @XStreamImplicit(itemFieldName = "parentLocationTypes")
    private List<LocationType> parentLocationTypes;
    
    @XStreamImplicit(itemFieldName = "routeTypes")
    private List<RouteType> routeTypes;
    
    @XStreamImplicit(itemFieldName = "revenueTypes")
    private List<RevType> revenueTypes;
    
    @XStreamImplicit(itemFieldName = "transactionTypes")
    private List<TxnType> transactionTypes;
    
    @XStreamImplicit(itemFieldName = "cardProcessMethodTypes")
    private List<CardProcMethodType> cardProcessMethodTypes;
    
    @XStreamImplicit(itemFieldName = "filterTypes")
    private List<FilterType> filterTypes;
    
    @XStreamImplicit(itemFieldName = "displayTypes")
    private List<DisplayType> displayTypes;
    
    @XStreamImplicit(itemFieldName = "rateTypes")
    private List<RateType> rateTypes;
    
    @XStreamImplicit(itemFieldName = "rangeTypes")
    private List<RangeType> rangeTypes;
    
    @XStreamImplicit(itemFieldName = "organizationTypes")
    private List<OrganizationType> organizationTypes;
    
    @XStreamImplicit(itemFieldName = "collectionTypes")
    private List<CollType> collTypes;
    
    @XStreamImplicit(itemFieldName = "merchantTypes")
    private List<MerchType> merchTypes;
    
    @XStreamImplicit(itemFieldName = "alertTypes")
    private List<AlertWidgetType> alertTypes;
    
    @XStreamImplicit(itemFieldName = "citatTypes")
    private List<CitatType> citatTypes;
    
    // for top/bottom N value
    private String length;
    private String widgetName;
    private String targetValue;
    
    public WidgetSettingsOptions() {
        reset();
    }
    
    public final void reset() {
        this.metricTypes = null;
        this.tier1Types = null;
        this.tier2Types = null;
        this.tier3Types = null;
        this.locationTypes = null;
        this.parentLocationTypes = null;
        this.routeTypes = null;
        this.revenueTypes = null;
        this.transactionTypes = null;
        this.cardProcessMethodTypes = null;
        this.filterTypes = null;
        this.displayTypes = null;
        this.rateTypes = null;
        this.rangeTypes = null;
        this.organizationTypes = null;
        this.collTypes = null;
        this.merchTypes = null;
        this.alertTypes = null;
        this.citatTypes = null;
        this.widgetName = "";
        this.targetValue = "";
        this.length = "";
    }
    
    public final void setMetricType(final String metricTypeRandomId) {
        for (MetricType metricType : this.metricTypes) {
            if (metricType.getRandomId().equals(metricTypeRandomId)) {
                metricType.setStatus(WebCoreConstants.SELECTED);
                break;
            }
        }
    }
    
    public final void setTier1Type(final String tierTypeRandomId) {
        for (TierType tierType : this.tier1Types) {
            if (tierType.getRandomId().equals(tierTypeRandomId)) {
                tierType.setStatus(WebCoreConstants.SELECTED);
                break;
            }
        }
    }
    
    public final void setTier2Type(final String tierTypeRandomId) {
        for (TierType tierType : this.tier2Types) {
            if (tierType.getRandomId().equals(tierTypeRandomId)) {
                tierType.setStatus(WebCoreConstants.SELECTED);
                break;
            }
        }
    }
    
    public final void setTier3Type(final String tierTypeRandomId) {
        for (TierType tierType : this.tier3Types) {
            if (tierType.getRandomId().equals(tierTypeRandomId)) {
                tierType.setStatus(WebCoreConstants.SELECTED);
                break;
            }
        }
    }
    
    public final void setFilterType(final String filterTypeRandomId) {
        for (FilterType filterType : this.filterTypes) {
            if (filterType.getRandomId().equals(filterTypeRandomId)) {
                filterType.setStatus(WebCoreConstants.SELECTED);
                break;
            }
        }
    }
    
    public final void setChartType(final String chartTypeRandomId) {
        for (DisplayType chartType : this.displayTypes) {
            if (chartType.getRandomId().equals(chartTypeRandomId)) {
                chartType.setStatus(WebCoreConstants.SELECTED);
                break;
            }
        }
    }
    
    public final void setRangeType(final String rangeTypeRandomId) {
        for (RangeType rangeType : this.rangeTypes) {
            if (rangeType.getRandomId().equals(rangeTypeRandomId)) {
                rangeType.setStatus(WebCoreConstants.SELECTED);
                break;
            }
        }
    }
    
    public final List<MetricType> getMetricTypes() {
        return this.metricTypes;
    }
    
    public final void setMetricTypes(final List<MetricType> metricTypes) {
        this.metricTypes = metricTypes;
    }
    
    public final List<TierType> getTier1Types() {
        return this.tier1Types;
    }
    
    public final void setTier1Types(final List<TierType> tier1Types) {
        this.tier1Types = tier1Types;
    }
    
    public final List<TierType> getTier2Types() {
        return this.tier2Types;
    }
    
    public final void setTier2Types(final List<TierType> tier2Types) {
        this.tier2Types = tier2Types;
    }
    
    public final List<TierType> getTier3Types() {
        return this.tier3Types;
    }
    
    public final void setTier3Types(final List<TierType> tier3Types) {
        this.tier3Types = tier3Types;
    }
    
    public final List<RevType> getRevenueTypes() {
        return this.revenueTypes;
    }
    
    public final void setRevenueTypes(final List<RevType> revenueTypes) {
        this.revenueTypes = revenueTypes;
    }
    
    public final List<TxnType> getTransactionTypes() {
        return this.transactionTypes;
    }
    
    public final void setTransactionTypes(final List<TxnType> transactionTypes) {
        this.transactionTypes = transactionTypes;
    }
    
    public final List<CardProcMethodType> getCardProcessMethodTypes() {
        return this.cardProcessMethodTypes;
    }
    
    public final void setCardProcessMethodTypes(final List<CardProcMethodType> cardProcMethodTypes) {
        this.cardProcessMethodTypes = cardProcMethodTypes;
    }
    
    public final List<LocationType> getLocationTypes() {
        return this.locationTypes;
    }
    
    public final void setLocationTypes(final List<LocationType> locationTypes) {
        this.locationTypes = locationTypes;
    }
    
    public final List<LocationType> getParentLocationTypes() {
        return this.parentLocationTypes;
    }
    
    public final void setParentLocationTypes(final List<LocationType> parentLocationTypes) {
        this.parentLocationTypes = parentLocationTypes;
    }
    
    public final List<RouteType> getRouteTypes() {
        return this.routeTypes;
    }
    
    public final void setRouteTypes(final List<RouteType> routeTypes) {
        this.routeTypes = routeTypes;
    }
    
    public final List<FilterType> getFilterTypes() {
        return this.filterTypes;
    }
    
    public final void setFilterTypes(final List<FilterType> filterTypes) {
        this.filterTypes = filterTypes;
    }
    
    public final String getWidgetName() {
        return this.widgetName;
    }
    
    public final void setWidgetName(final String widgetName) {
        this.widgetName = widgetName;
    }
    
    public final List<DisplayType> getDisplayTypes() {
        return this.displayTypes;
    }
    
    public final void setDisplayTypes(final List<DisplayType> displayTypes) {
        this.displayTypes = displayTypes;
    }
    
    public final String getTargetValue() {
        return this.targetValue;
    }
    
    public final void setTargetValue(final String targetValue) {
        this.targetValue = targetValue;
    }
    
    public final List<RateType> getRateTypes() {
        return this.rateTypes;
    }
    
    public final void setRateTypes(final List<RateType> rateTypes) {
        this.rateTypes = rateTypes;
    }
    
    public final List<RangeType> getRangeTypes() {
        return this.rangeTypes;
    }
    
    public final void setRangeTypes(final List<RangeType> rangeTypes) {
        this.rangeTypes = rangeTypes;
    }
    
    public final String getLength() {
        return this.length;
    }
    
    public final void setLength(final String length) {
        this.length = length;
    }
    
    public final List<OrganizationType> getOrganizationTypes() {
        return this.organizationTypes;
    }
    
    public final void setOrganizationTypes(final List<OrganizationType> organizationTypes) {
        this.organizationTypes = organizationTypes;
    }
    
    public final List<CollType> getCollTypes() {
        return this.collTypes;
    }
    
    public final void setCollTypes(final List<CollType> collTypes) {
        this.collTypes = collTypes;
    }
    
    public final List<MerchType> getMerchTypes() {
        return this.merchTypes;
    }
    
    public final void setMerchTypes(final List<MerchType> merchTypes) {
        this.merchTypes = merchTypes;
    }
    
    public final List<AlertWidgetType> getAlertTypes() {
        return this.alertTypes;
    }
    
    public final void setAlertTypes(final List<AlertWidgetType> alertTypes) {
        this.alertTypes = alertTypes;
    }
    
    public final List<CitatType> getCitatTypes() {
        return this.citatTypes;
    }
    
    public final void setCitatTypes(final List<CitatType> citatTypes) {
        this.citatTypes = citatTypes;
    }
    
    public final List<? extends SettingsType> getSettingsForTierType(final int type) {
        List<? extends SettingsType> result = null;
        switch (type) {
            case WidgetConstants.TIER_TYPE_ORG: 
                result = this.organizationTypes;
                break;
            case WidgetConstants.TIER_TYPE_MERCHANT_ACCOUNT: 
                result = this.merchTypes;
                break;
            case WidgetConstants.TIER_TYPE_REVENUE_TYPE: 
                result = this.revenueTypes;
                break;
            case WidgetConstants.TIER_TYPE_ROUTE: 
                result = this.routeTypes;
                break;
            case WidgetConstants.TIER_TYPE_LOCATION: 
                result = this.locationTypes;
                break;
            case WidgetConstants.TIER_TYPE_PARENT_LOCATION: 
                result = this.parentLocationTypes;
                break;
            case WidgetConstants.TIER_TYPE_RATE: 
                result = this.rateTypes;
                break;
            case WidgetConstants.TIER_TYPE_CITATION_TYPE:
                result = this.citatTypes;
                break;
            case WidgetConstants.TIER_TYPE_TRANSACTION_TYPE: 
                result = this.transactionTypes;
                break;
            case WidgetConstants.TIER_TYPE_COLLECTION_TYPE: 
                result = this.collTypes;
                break;
            case WidgetConstants.TIER_TYPE_ALERT_TYPE: 
                result = this.alertTypes;
                break;
            case WidgetConstants.TIER_TYPE_CARD_PROCESS_METHOD_TYPE: 
                result = this.cardProcessMethodTypes;
                break;
            default:
                break;
        }
        
        return result;
    }
}
