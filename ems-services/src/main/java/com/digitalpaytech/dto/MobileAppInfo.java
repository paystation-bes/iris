package com.digitalpaytech.dto;

import java.io.Serializable;

public class MobileAppInfo implements Serializable {
    
    private static final long serialVersionUID = -2603007583078380569L;
    
    private String appName;
    private String randomId;
    private int statusId;
    private String status;
    
    public MobileAppInfo(final String appName, final String randomId) {
        this.appName = appName;
        this.randomId = randomId;
    }
    
    public final int getStatusId() {
        return this.statusId;
    }
    
    public final void setStatusId(final int statusId) {
        this.statusId = statusId;
    }
    
    public final String getStatus() {
        return this.status;
    }
    
    public final void setStatus(final String status) {
        this.status = status;
    }
    
    public final String getAppName() {
        return this.appName;
    }
    
    public final void setAppName(final String appName) {
        this.appName = appName;
    }
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
}
