package com.digitalpaytech.dto.cps;

import java.util.Date;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.Purchase;

public class CPSTransactionDto {
    private PointOfSale pointOfSale;
    private Purchase purchase;
    private ProcessorTransaction processorTransaction;
    private ProcessorTransaction refundExtensionProcessorTransaction;
    private MerchantAccount merchantAccount;
    private Date purchasedDate;
    private Date processingDate;
    private int amount;
    private String cardType;
    private short last4digitsOfCardNumber;
    private String authorizationNumber;
    private String referenceNumber;
    private String bankingMessage;
    private String chargeToken;
    
    public CPSTransactionDto(final MerchantAccount merchantAccount) {
        this.merchantAccount = merchantAccount;
    }
    
    public CPSTransactionDto(final MerchantAccount merchantAccount, final String bankingMessage) {
        this.merchantAccount = merchantAccount;
        this.bankingMessage = bankingMessage;
    }
    
    public CPSTransactionDto(final ProcessorTransaction processorTransaction, final ProcessorTransaction refundExtensionProcessorTransaction,
            final MerchantAccount merchantAccount) {
        this.processorTransaction = processorTransaction;
        this.refundExtensionProcessorTransaction = refundExtensionProcessorTransaction;
        this.merchantAccount = merchantAccount;
    }
    
    public CPSTransactionDto(final PointOfSale pointOfSale, final Purchase purchase, final ProcessorTransaction processorTransaction, 
        final MerchantAccount merchantAccount, final Date purchasedDate, final Date processingDate, final int amount, 
        final String cardType, final short last4digitsOfCardNumber, final String authorizationNumber, final String referenceNumber) {
        
        this.pointOfSale = pointOfSale;
        this.purchase = purchase;
        this.processorTransaction = processorTransaction;
        this.merchantAccount = merchantAccount;
        this.purchasedDate = purchasedDate;
        this.processingDate = processingDate;
        this.amount = amount;
        this.cardType = cardType;
        this.last4digitsOfCardNumber = last4digitsOfCardNumber;
        this.authorizationNumber = authorizationNumber;
        this.referenceNumber = referenceNumber;
    }

    public final PointOfSale getPointOfSale() {
        return this.pointOfSale;
    }

    public final Purchase getPurchase() {
        return this.purchase;
    }

    public final ProcessorTransaction getProcessorTransaction() {
        return this.processorTransaction;
    }

    public final MerchantAccount getMerchantAccount() {
        return this.merchantAccount;
    }

    public final Date getPurchasedDate() {
        return this.purchasedDate;
    }

    public final Date getProcessingDate() {
        return this.processingDate;
    }

    public final int getAmount() {
        return this.amount;
    }

    public final String getCardType() {
        return this.cardType;
    }

    public final short getLast4digitsOfCardNumber() {
        return this.last4digitsOfCardNumber;
    }

    public final String getAuthorizationNumber() {
        return this.authorizationNumber;
    }

    public final String getReferenceNumber() {
        return this.referenceNumber;
    }

    public final void setReferenceNumber(final String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public final String getBankingMessage() {
        return this.bankingMessage;
    }

    public final void setBankingMessage(final String bankingMessage) {
        this.bankingMessage = bankingMessage;
    }

    public final ProcessorTransaction getExtensionRefundProcessorTransaction() {
        return this.refundExtensionProcessorTransaction;
    }

    public final String getChargeToken() {
        return this.chargeToken;
    }

    public final void setChargeToken(final String transactionCardToken) {
        this.chargeToken = transactionCardToken;
    }
}
