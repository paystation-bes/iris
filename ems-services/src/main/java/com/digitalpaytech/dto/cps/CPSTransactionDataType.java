package com.digitalpaytech.dto.cps;

import com.digitalpaytech.domain.MerchantAccount;

public interface CPSTransactionDataType {
    MerchantAccount getMerchantAccount();
    int getProcessorId();
    CPSTransactionDto getCPSTransactionDto();
}
