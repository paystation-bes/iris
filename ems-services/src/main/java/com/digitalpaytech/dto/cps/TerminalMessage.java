package com.digitalpaytech.dto.cps;

import org.apache.commons.lang.builder.ToStringBuilder;

public class TerminalMessage {
    
    // UNIFI Id
    private Integer customerId;
    private String terminalToken;
    private String terminalName;
    private Boolean isValidated;
    private Boolean isLocked;
    private Boolean isDeleted;
    private Boolean isEnabled;
    private String merchantName;
    private String timeZone;
    private String quarterOfDay;
    private String processorType;
    
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("customerId", customerId).append("terminalToken", terminalToken).append("terminalName", terminalName)
                .append("isValidated", isValidated).append("isLocked", isLocked).append("isDeleted", isDeleted).append("isEnabled", isEnabled)
                .append("merchantName", merchantName).append("timeZone", timeZone).append("quarterOfDay", quarterOfDay)
                .append("processorType", processorType).toString();
    }

    public final String getTerminalToken() {
        return this.terminalToken;
    }
    
    public final void setTerminalToken(final String terminalToken) {
        this.terminalToken = terminalToken;
    }
    
    public final String getTerminalName() {
        return this.terminalName;
    }
    
    public final void setTerminalName(final String terminalName) {
        this.terminalName = terminalName;
    }
    
    public final Boolean getIsValidated() {
        return this.isValidated;
    }
    
    public final void setIsValidated(final Boolean isValidated) {
        this.isValidated = isValidated;
    }
    
    public final Boolean getIsLocked() {
        return this.isLocked;
    }
    
    public final void setIsLocked(final Boolean isLocked) {
        this.isLocked = isLocked;
    }
    
    public final Boolean getIsDeleted() {
        return this.isDeleted;
    }
    
    public final void setIsDeleted(final Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
    
    public final Boolean getIsEnabled() {
        return this.isEnabled;
    }
    
    public final void setIsEnabled(final Boolean isEnabled) {
        this.isEnabled = isEnabled;
    }
    
    public final String getMerchantName() {
        return this.merchantName;
    }
    
    public final void setMerchantName(final String merchantName) {
        this.merchantName = merchantName;
    }
    
    public final String getTimeZone() {
        return this.timeZone;
    }
    
    public final void setTimeZone(final String timeZone) {
        this.timeZone = timeZone;
    }
    
    public final String getQuarterOfDay() {
        return this.quarterOfDay;
    }
    
    public final void setQuarterOfDay(final String quarterOfDay) {
        this.quarterOfDay = quarterOfDay;
    }
    
    public final String getProcessorType() {
        return this.processorType;
    }
    
    public final void setProcessorType(final String processor) {
        this.processorType = processor;
    }
    
    public final Integer getCustomerId() {
        return this.customerId;
    }
    
    public final void setCustomerId(final Integer customerId) {
        this.customerId = customerId;
    }
    
}
