package com.digitalpaytech.dto.cps;

public class SnFChargeResultMessage {
   
    private String chargeToken;
    private String deviceSerialNumber;
    private String terminalToken;
    private String amount;
    private String authorizationNumber;
    private String cardType;
    private String last4Digits;
    private String referenceNumber;
    private String processorTransactionId;
    private String processedDate;
    
    public final String getChargeToken() {
        return this.chargeToken;
    }
    
    public final void setChargeToken(final String chargeToken) {
        this.chargeToken = chargeToken;
    }
    
    public final String getDeviceSerialNumber() {
        return this.deviceSerialNumber;
    }
    
    public final void setDeviceSerialNumber(final String deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }
    
    public final String getTerminalToken() {
        return this.terminalToken;
    }
    
    public final void setTerminalToken(final String terminalToken) {
        this.terminalToken = terminalToken;
    }
    
    public final String getAmount() {
        return this.amount;
    }
    
    public final void setAmount(final String amount) {
        this.amount = amount;
    }
    
    public final String getAuthorizationNumber() {
        return this.authorizationNumber;
    }
    
    public final void setAuthorizationNumber(final String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }
    
    public final String getCardType() {
        return this.cardType;
    }
    
    public final void setCardType(final String cardType) {
        this.cardType = cardType;
    }
    
    public final String getLast4Digits() {
        return this.last4Digits;
    }
    
    public final void setLast4Digits(final String last4Digits) {
        this.last4Digits = last4Digits;
    }
    
    public final String getReferenceNumber() {
        return this.referenceNumber;
    }
    
    public final void setReferenceNumber(final String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }
    
    public final String getProcessorTransactionId() {
        return this.processorTransactionId;
    }
    
    public final void setProcessorTransactionId(final String processorTransactionId) {
        this.processorTransactionId = processorTransactionId;
    }
    
    public final String getProcessedDate() {
        return this.processedDate;
    }
    
    public final void setProcessedDate(final String processedDate) {
        this.processedDate = processedDate;
    }
    
    @Override
    public String toString() {
        return "SnFChargeResultMessage [chargeToken=" + chargeToken + ", deviceSerialNumber=" + deviceSerialNumber + ", terminalToken=" + terminalToken + ", amount="
               + amount + ", authorizationNumber=" + authorizationNumber + ", cardType=" + cardType + ", last4Digits=" + last4Digits
               + ", referenceNumber=" + referenceNumber + ", processorTransactionId=" + processorTransactionId + ", processedDate=" + processedDate
               + "]";
    }
    
}
