package com.digitalpaytech.dto.cps;

import java.util.Map;
import java.util.HashMap;
import java.util.UUID;

public class Tokens {
    public enum TokenTypes {
        CARD_TOKEN, CHARGE_TOKEN, MERCHANT_TOKEN, REFUND_TOKEN, TERMINAL_TOKEN, 
    }
    
    private Map<TokenTypes, UUID> tokensMap;
    
    public Tokens() {
        this.tokensMap = new HashMap<>();
    }
    
    public Tokens(final Map<TokenTypes, UUID> tokensMap) {
        this.tokensMap = tokensMap;
    }
      
    public final void addToken(final TokenTypes tokenType, final UUID token) {
        this.tokensMap.put(tokenType, token);
    }
    
    public final UUID getToken(final TokenTypes tokenType) {
        return this.tokensMap.get(tokenType);
    }
    
    public final int count() {
        return this.tokensMap.size();
    }
}
