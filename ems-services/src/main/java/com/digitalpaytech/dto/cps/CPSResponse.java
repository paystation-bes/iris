package com.digitalpaytech.dto.cps;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.dto.EmbeddedTxObject;
import com.digitalpaytech.dto.paystation.CardEaseData;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@StoryAlias(CardEaseData.ALIAS)
public class CPSResponse implements EmbeddedTxObject {
    
    public static final String ALIAS = "cps response";
    
    @JsonProperty("processedDate")
    private String processedDate;
    
    @JsonProperty("cardType")
    private String cardType;
    
    @JsonProperty("last4Digits")
    private String last4Digits;
    
    @JsonProperty("authorizationNumber")
    private String authorizationNumber;
    
    @JsonProperty("referenceNumber")
    private String referenceNumber;
    
    @JsonProperty("processorTransactionId")
    private String processorTransactionId;
    
    @JsonProperty("terminalToken")
    private String terminalToken;
    
    @JsonProperty("chargeToken")
    private String chargeToken;
    
    @JsonProperty("cardToken")
    private String cardToken;

    @JsonProperty("externalToken")
    private String externalToken;

    @JsonProperty("refundToken")
    private String refundToken;

    @JsonProperty("optionalData")
    private Map<String, Object> optionalData;
    
    public CPSResponse() {
        super();
    }
    
    public CPSResponse(final String processedDate, final String cardType, final String last4Digits, final String authorizationNumber,
            final String referenceNumber, final String processorTransactionId, final String terminalToken, final String chargeToken,
            final Map<String, Object> optionalData) {
        this.processedDate = processedDate;
        this.cardType = cardType;
        this.last4Digits = last4Digits;
        this.authorizationNumber = authorizationNumber;
        this.referenceNumber = referenceNumber;
        this.processorTransactionId = processorTransactionId;
        this.terminalToken = terminalToken;
        this.chargeToken = chargeToken;
        this.optionalData = optionalData;
    }
    
    @Override
    public final boolean isCPSStoreForward() {
        return StringUtils.isBlank(this.processedDate) && StringUtils.isNotBlank(this.terminalToken) && StringUtils.isNotBlank(this.chargeToken);
    }
    
    public final String getProcessedDate() {
        return this.processedDate;
    }
    
    public final void setProcessedDate(final String processedDate) {
        this.processedDate = processedDate;
    }
    
    public final String getCardType() {
        return this.cardType;
    }
    
    public final void setCardType(final String cardType) {
        this.cardType = cardType;
    }
    
    public final String getLast4Digits() {
        return this.last4Digits;
    }
    
    public final void setLast4Digits(final String last4Digits) {
        this.last4Digits = last4Digits;
    }
    
    public final String getAuthorizationNumber() {
        return this.authorizationNumber;
    }
    
    public final void setAuthorizationNumber(final String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }
    
    public final String getReferenceNumber() {
        return this.referenceNumber;
    }
    
    public final void setReferenceNumber(final String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }
    
    public final String getProcessorTransactionId() {
        return this.processorTransactionId;
    }
    
    public final void setProcessorTransactionId(final String processorTransactionId) {
        this.processorTransactionId = processorTransactionId;
    }
    
    public final String getChargeToken() {
        return this.chargeToken;
    }
    
    public final void setChargeToken(final String chargeToken) {
        this.chargeToken = chargeToken;
    }
    
    public final String getCardToken() {
        return this.cardToken;
    }
    
    public final void setCardToken(final String cardToken) {
        this.cardToken = cardToken;
    }
    
    public final String getExternalToken() {
        return this.externalToken;
    }
    
    public final void setExternalToken(final String externalToken) {
        this.externalToken = externalToken;
    }
    
    public final String getRefundToken() {
        return this.refundToken;
    }
    
    public final void setRefundToken(final String refundToken) {
        this.refundToken = refundToken;
    }
    
    public final String getTerminalToken() {
        return this.terminalToken;
    }
    
    public final void setTerminalToken(final String terminalToken) {
        this.terminalToken = terminalToken;
    }
    
    public Map<String, Object> getOptionalData() {
        return this.optionalData;
    }
    
    public void setOptionalData(final Map<String, Object> optionalData) {
        this.optionalData = optionalData;
    }

    @Override
    public String toString() {
        return "CPSResponse [processedDate=" + processedDate + ", cardType=" + cardType + ", last4Digits=" + last4Digits + ", authorizationNumber="
               + authorizationNumber + ", referenceNumber=" + referenceNumber + ", processorTransactionId=" + processorTransactionId
               + ", terminalToken=" + terminalToken + ", chargeToken=" + chargeToken + ", cardToken=" + cardToken + ", externalToken=" + externalToken
               + ", refundToken=" + refundToken + ", optionalData=" + optionalData + "]";
    }
    
}
