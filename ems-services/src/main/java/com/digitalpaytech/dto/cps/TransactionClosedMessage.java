package com.digitalpaytech.dto.cps;

import org.apache.commons.lang.builder.ToStringBuilder;

public class TransactionClosedMessage {
    private int pointOfSaleId;
    private int processorTransactionId;
    private int ticketNumber;
    private int transactionSettlementStatusTypeId;
    private String purchasedDate;
    private String chargeToken;
    

    public TransactionClosedMessage() {
    }
    
    public TransactionClosedMessage(final int pointOfSaleId, 
                                    final int processorTransactionId, 
                                    final int ticketNumber, 
                                    final int transactionSettlementStatusTypeId,
                                    final String purchasedDate,
                                    final String chargeToken) {
        this.pointOfSaleId = pointOfSaleId;
        this.processorTransactionId = processorTransactionId;
        this.ticketNumber = ticketNumber;
        this.transactionSettlementStatusTypeId = transactionSettlementStatusTypeId;
        this.purchasedDate = purchasedDate;
        this.chargeToken = chargeToken;
    }
    public final int getPointOfSaleId() {
        return this.pointOfSaleId;
    }
    public final void setPointOfSaleId(final int pointOfSaleId) {
        this.pointOfSaleId = pointOfSaleId;
    }
    public final int getProcessorTransactionId() {
        return this.processorTransactionId;
    }
    public final void setProcessorTransactionId(final int processorTransactionId) {
        this.processorTransactionId = processorTransactionId;
    }
    public final int getTicketNumber() {
        return this.ticketNumber;
    }
    public final void setTicketNumber(final int ticketNumber) {
        this.ticketNumber = ticketNumber;
    }
    public final int getTransactionSettlementStatusTypeId() {
        return this.transactionSettlementStatusTypeId;
    }
    public final void setTransactionSettlementStatusTypeId(final int transactionSettlementStatusTypeId) {
        this.transactionSettlementStatusTypeId = transactionSettlementStatusTypeId;
    }
    public final String getPurchasedDate() {
        return this.purchasedDate;
    }
    public final void setPurchasedDate(final String purchasedDate) {
        this.purchasedDate = purchasedDate;
    }
    public final String getChargeToken() {
        return this.chargeToken;
    }
    public final void setChargeToken(final String chargeToken) {
        this.chargeToken = chargeToken;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("pointOfSaleId", pointOfSaleId).append("processorTransactionId", processorTransactionId)
                .append("ticketNumber", ticketNumber).append("transactionSettlementStatusTypeId", transactionSettlementStatusTypeId)
                .append("purchasedDate", purchasedDate).append("chargeToken", chargeToken).toString();
    }
}
