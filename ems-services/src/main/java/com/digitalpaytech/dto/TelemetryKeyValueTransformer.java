package com.digitalpaytech.dto;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.hibernate.transform.ResultTransformer;

import com.digitalpaytech.util.KeyValuePairString;
import com.digitalpaytech.util.MessageHelper;

public class TelemetryKeyValueTransformer implements ResultTransformer {
    private static final long serialVersionUID = 5248119141898914726L;

    private Set<KeyValuePairString> result = new TreeSet<KeyValuePairString>(new Comparator<KeyValuePairString>() {
        @Override
        public final int compare(final KeyValuePairString o1, final KeyValuePairString o2) {
            return o1.getKey().compareTo(o2.getKey());
        }
    });
    
    private MessageHelper message;
    
    private int keyIdx = -1;
    private int keyNameIdx = -1;
    private int valIdx = -1;
    
    public TelemetryKeyValueTransformer(final MessageHelper messageHelper) {
        this.message = messageHelper;
    }

    @Override
    public final Object transformTuple(final Object[] tuple, final String[] aliases) {
        resolveIdx(tuple, aliases);
        
        final String key = this.message.getMessageWithDefault((String) tuple[this.keyIdx], (String) tuple[this.keyNameIdx]);
        final String val = (String) tuple[this.valIdx];
        
        this.result.add(new KeyValuePairString(key, val));
        
        return null;
    }

    @Override
    public final List transformList(final List collection) {
        return new ArrayList<KeyValuePairString>(this.result);
    }
    
    private void resolveIdx(final Object[] tuple, final String[] aliases) {
        if (this.keyIdx < 0) {
            int idx = aliases.length;
            while (--idx >= 0) {
                switch (aliases[idx]) {
                    case "key":
                        this.keyIdx = idx;
                        break;
                    case "value":
                        this.valIdx = idx;
                        break;
                    case "keyName":
                        this.keyNameIdx = idx;
                        break;
                    default:
                        // DO NOTHING.
                }
            }
        }
    }
}
