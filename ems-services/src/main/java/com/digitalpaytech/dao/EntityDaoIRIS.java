package com.digitalpaytech.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

/**
 * Provide a convenience Entity Manager interface. This interface will eventually acts as the whole DAO layer.
 */
public interface EntityDaoIRIS {
    
    SQLQuery createSQLQuery(String queryStr);
    
    Session getCurrentSession();
    
    void delete(Object entity);
    
    void evict(Object obj);
    
    List findByNamedQuery(String queryName, String[] names, Object[] values, int startRow, int howManyRows);
    
    Object merge(Object entity);
    
    Serializable save(Object entity);
    
    void update(Object entity);
    
    <T> T get(Class<T> entityClass, Serializable id);
    
    List findByNamedQueryAndNamedParam(String queryName, String paramName, Object value);
    
    List findByNamedQueryAndNamedParam(String queryName, String[] paramNames, Object[] values, boolean cacheable);
    
    Object findUniqueByNamedQueryAndNamedParam(String queryName, String[] paramNames, Object[] values, boolean cacheable);
    
}
