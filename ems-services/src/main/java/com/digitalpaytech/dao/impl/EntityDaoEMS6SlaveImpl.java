package com.digitalpaytech.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.digitalpaytech.dao.EntityDaoEMS6Slave;

@Repository
public class EntityDaoEMS6SlaveImpl implements EntityDaoEMS6Slave {
    
    @Autowired
    @Qualifier("sessionFactoryMigrationEMS6Slave")
    private SessionFactory sessionFactory;
    
    public final SQLQuery createSQLQuery(final String queryStr) {
        return getCurrentSession().createSQLQuery(queryStr);
    }
    
    public final Session getCurrentSession() {
        return this.sessionFactory.getCurrentSession();
    }

    
}
