package com.digitalpaytech.dao.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.digitalpaytech.dao.EntityDaoIRIS;

@Repository
public class EntityDaoIRISImpl implements EntityDaoIRIS {
    
    @Autowired
    @Qualifier("sessionFactoryMigrationIRIS")
    private SessionFactory sessionFactory;
    
    public final SQLQuery createSQLQuery(final String queryStr) {
        return getCurrentSession().createSQLQuery(queryStr);
    }
    
    public final Session getCurrentSession() {
        return this.sessionFactory.getCurrentSession();
    }
    
    @Override
    public final void delete(final Object entity) {
        getCurrentSession().delete(entity);
    }
    
    @Override
    public final void evict(final Object obj) {
        getCurrentSession().evict(obj);
    }
    
    @Override
    public final List findByNamedQuery(final String queryName, final String[] names, final Object[] values, final int startRow, final int howManyRows) {
        final Query queryObject = getCurrentSession().getNamedQuery(queryName);
        queryObject.setCacheable(false);
        if (values != null) {
            for (int i = 0; i < values.length; i++) {
                if (values[i] instanceof Collection) {
                    queryObject.setParameterList(names[i], (Collection) values[i]);
                } else {
                    queryObject.setParameter(names[i], values[i]);
                }
            }
        }
        if (startRow >= 0) {
            queryObject.setFirstResult(startRow);
        }
        if (howManyRows > 0) {
            queryObject.setMaxResults(howManyRows);
        }
        return queryObject.list();
    }
    
    @Override
    public final Object merge(final Object entity) {
        return getCurrentSession().merge(entity);
    }
    
    @Override
    public final Serializable save(final Object entity) {
        return getCurrentSession().save(entity);
    }
    
    @Override
    public final void update(final Object entity) {
        getCurrentSession().update(entity);
    }
    
    @Override
    public final <T> T get(final Class<T> entityClass, final Serializable id) {
        return (T) getCurrentSession().get(entityClass, id);
    }
    
    @Override
    public final List findByNamedQueryAndNamedParam(final String queryName, final String paramName, final Object value) {
        return getCurrentSession().getNamedQuery(queryName).setParameter(paramName, value).list();
    }
    
    @Override
    public final List findByNamedQueryAndNamedParam(final String queryName, final String[] paramNames, final Object[] values, final boolean cacheable) {
        if (paramNames.length != values.length) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("paramName length is ").append(paramNames.length).append(", but values length is ").append(values.length);
            throw new HibernateException(bdr.toString());
        }
        
        final Query q = getCurrentSession().getNamedQuery(queryName).setCacheable(cacheable);
        for (int i = 0; i < paramNames.length; i++) {
            if (values[i] instanceof Collection) {
                q.setParameterList(paramNames[i], (Collection<?>) values[i]);
            } else {
                q.setParameter(paramNames[i], values[i]);
            }
        }
        return q.list();
        
    }

    
    @Override
    public final Object findUniqueByNamedQueryAndNamedParam(final String queryName, final String[] paramNames, final Object[] values, final boolean cacheable) {
        if (paramNames.length != values.length) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("paramName length is ").append(paramNames.length).append(", but values length is ").append(values.length);
            throw new HibernateException(bdr.toString());
        }
        
        final Query q = getCurrentSession().getNamedQuery(queryName).setCacheable(cacheable);
        for (int i = 0; i < paramNames.length; i++) {
            q.setParameter(paramNames[i], values[i]);
        }
        return q.uniqueResult();
    }
    
}
