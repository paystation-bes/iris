package com.digitalpaytech.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.digitalpaytech.dao.EntityDaoReport;

@Repository
public class EntityDaoReportImpl implements EntityDaoReport {
    @Autowired
    @Qualifier("reportQuerySessionFactory")
    private SessionFactory sessionFactory;

    @Override
    public Session getCurrentSession() {
        return this.sessionFactory.getCurrentSession();
    }
    
}
