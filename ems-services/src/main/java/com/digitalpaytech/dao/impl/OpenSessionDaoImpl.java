package com.digitalpaytech.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.digitalpaytech.dao.OpenSessionDao;

@Repository
public class OpenSessionDaoImpl implements OpenSessionDao {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    public final List findByNamedQuery(final String queryName) {
        Session session = null;
        try {
            session = openSession();
            return session.getNamedQuery(queryName).list();
        } catch (HibernateException e) {
            throw e;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
    
    public final Serializable save(final Object entity) {
        Session session = null;
        try {
            session = openSession();
            return session.save(entity);
        } catch (HibernateException e) {
            throw e;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
    
    public final void update(final Object entity) {
        Session session = null;
        try {
            session = openSession();
            session.update(entity);
        } catch (HibernateException e) {
            throw e;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
    
    public final Object findUniqueByNamedQueryAndNamedParam(final String queryName, final String paramName, final Object value) {
        Session session = null;
        try {
            session = openSession();
            return session.getNamedQuery(queryName).setParameter(paramName, value).uniqueResult();
        } catch (HibernateException e) {
            throw e;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
    
    public final <T> List<T> loadAll(final Class<T> entityClass) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("from ").append(entityClass.getName());
        Session session = null;
        try {
            session = openSession();
            return session.createQuery(bdr.toString()).list();
        } catch (HibernateException e) {
            throw e;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
    
    private Session openSession() {
        return this.sessionFactory.openSession();
    }
    
    public final <T> List<T> findByNamedQueryAndNamedParam(final String queryName, final String[] paramNames, final Object[] values,
        final boolean cacheable) {
        Session session = null;
        try {
            session = openSession();
            
            if (paramNames.length != values.length) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append("paramName length is ").append(paramNames.length).append(", but values length is ").append(values.length);
                throw new HibernateException(bdr.toString());
            }
            
            final Query q = session.getNamedQuery(queryName).setCacheable(cacheable);
            for (int i = 0; i < paramNames.length; i++) {
                q.setParameter(paramNames[i], values[i]);
            }
            return q.list();
        } catch (HibernateException e) {
            throw e;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
}
