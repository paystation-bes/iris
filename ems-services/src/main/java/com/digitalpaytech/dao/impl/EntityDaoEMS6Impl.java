package com.digitalpaytech.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.digitalpaytech.dao.EntityDaoEMS6;

@Repository
public class EntityDaoEMS6Impl implements EntityDaoEMS6 {
    
    @Autowired
    @Qualifier("sessionFactoryMigrationEMS6")
    private SessionFactory sessionFactory;
    
    public final SQLQuery createSQLQuery(final String queryStr) {
        return getCurrentSession().createSQLQuery(queryStr);
    }
    
    public final Session getCurrentSession() {
        return this.sessionFactory.getCurrentSession();
    }

    
}
