package com.digitalpaytech.dao.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Cache;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.digitalpaytech.dao.EntityDao;

@Repository
public class EntityDaoImpl implements EntityDao {
    
    @Autowired
    @Qualifier("sessionFactory")
    private SessionFactory sessionFactory;
    
    public final void delete(final Object entity) {
        getCurrentSession().delete(entity);
    }
    
    public final List findByNamedQuery(final String queryName) {
        return getCurrentSession().getNamedQuery(queryName).list();
    }
    
    public final List findByNamedQuery(final String queryName, final boolean cacheable) {
        return getCurrentSession().getNamedQuery(queryName).setCacheable(cacheable).list();
    }
    
    public final Object findUniqueByNamedQueryWithLock(final String queryName, final String lockedAlias) {
        final List list = getCurrentSession().getNamedQuery(queryName).setLockMode(lockedAlias, LockMode.PESSIMISTIC_WRITE).setMaxResults(1).list();
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }
    
    public final List findByNamedQueryWithLock(final String queryName, final String lockedAlias) {
        return getCurrentSession().getNamedQuery(queryName).setLockMode(lockedAlias, LockMode.PESSIMISTIC_WRITE).list();
    }
    
    public final Object findUniqueByNamedQueryAndNamedParamWithLock(final String queryName, final String paramName, final Object value,
        final String lockedAlias) {
        final List list = getCurrentSession().getNamedQuery(queryName).setParameter(paramName, value)
                .setLockMode(lockedAlias, LockMode.PESSIMISTIC_WRITE).setMaxResults(1).list();
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        return null;
        
    }
    
    public final List findByNamedQueryAndNamedParamWithLock(final String queryName, final String paramName, final Object value,
        final String lockedAlias) {
        return getCurrentSession().getNamedQuery(queryName).setParameter(paramName, value).setLockMode(lockedAlias, LockMode.PESSIMISTIC_WRITE)
                .list();
    }
    
    public final List findByNamedQueryAndNamedParam(final String queryName, final String paramName, final Object value) {
        return getCurrentSession().getNamedQuery(queryName).setParameter(paramName, value).list();
    }
    
    public List findByNamedQueryAndNamedParam(final String queryName, final String[] paramNames, final Object[] values) {
        return findByNamedQueryAndNamedParam(queryName, paramNames, values, false);
    }
    
    @Override
    public final List findByNamedQueryAndNamedParam(final String queryName, final String[] paramNames, final Object[] values,
        final boolean cacheable) {
        if (paramNames.length != values.length) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("paramName length is ").append(paramNames.length).append(", but values length is ").append(values.length);
            throw new HibernateException(bdr.toString());
        }
        
        final Query q = getCurrentSession().getNamedQuery(queryName).setCacheable(cacheable);
        for (int i = 0; i < paramNames.length; i++) {
            if (values[i] instanceof Collection) {
                q.setParameterList(paramNames[i], (Collection<?>) values[i]);
            } else {
                q.setParameter(paramNames[i], values[i]);
            }
        }
        return q.list();
        
    }
    
    public List findByNamedQueryAndNamedParam(final String queryName, final String paramName, final Object value, final boolean cacheable) {
        return getCurrentSession().getNamedQuery(queryName).setCacheable(cacheable).setParameter(paramName, value).list();
    }
    
    public final Object findUniqueByNamedQueryAndNamedParam(final String queryName, final String paramName, final Object value) {
        return getCurrentSession().getNamedQuery(queryName).setParameter(paramName, value).uniqueResult();
    }
    
    public final Object findUniqueByNamedQueryAndNamedParam(final String queryName, final String paramName, final Object value,
        final boolean cacheable) {
        return getCurrentSession().getNamedQuery(queryName).setCacheable(cacheable).setParameter(paramName, value).uniqueResult();
    }
    
    public final Object findUniqueByNamedQueryAndNamedParam(final String queryName, final String[] paramNames, final Object[] values,
        final boolean cacheable) {
        if (paramNames.length != values.length) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("paramName length is ").append(paramNames.length).append(", but values length is ").append(values.length);
            throw new HibernateException(bdr.toString());
        }
        
        final Query q = getCurrentSession().getNamedQuery(queryName).setCacheable(cacheable);
        for (int i = 0; i < paramNames.length; i++) {
            q.setParameter(paramNames[i], values[i]);
        }
        return q.uniqueResult();
    }
    
    @Override
    public final List findByNamedQueryAndNamedParam(final String queryName, final String[] paramNames, final Object[] values,
        final int startingResult, final int maxResult, final boolean cacheable) {
        if (paramNames.length != values.length) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("paramName length is ").append(paramNames.length).append(", but values length is ").append(values.length);
            throw new HibernateException(bdr.toString());
        }
        
        final Query q = getCurrentSession().getNamedQuery(queryName).setCacheable(cacheable);
        for (int i = 0; i < paramNames.length; i++) {
            if (values[i] instanceof Collection) {
                q.setParameterList(paramNames[i], (Collection<?>) values[i]);
            } else {
                q.setParameter(paramNames[i], values[i]);
            }
        }
        q.setFirstResult(startingResult);
        q.setMaxResults(maxResult);
        return q.list();
        
    }
    
    public final List findByNamedQueryAndNamedParamWithLimit(final String queryName, final String[] paramNames, final Object[] values,
        final boolean cacheable, final int limit) {
        if (paramNames.length != values.length) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("paramName length is ").append(paramNames.length).append(", but values length is ").append(values.length);
            throw new HibernateException(bdr.toString());
        }
        
        final Query q = getCurrentSession().getNamedQuery(queryName).setCacheable(cacheable);
        for (int i = 0; i < paramNames.length; i++) {
            if (values[i] instanceof Collection) {
                q.setParameterList(paramNames[i], (Collection<?>) values[i]);
            } else {
                q.setParameter(paramNames[i], values[i]);
            }
        }
        q.setMaxResults(limit);
        return q.list();
        
    }
    
    @Override
    public final void deleteByNamedQueryAndNamedParams(final String queryName, final String[] paramNames, final Object[] values) {
        if (paramNames.length != values.length) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("paramName length is ").append(paramNames.length).append(", but values length is ").append(values.length);
            throw new HibernateException(bdr.toString());
        }
        
        final Query q = getCurrentSession().getNamedQuery(queryName);
        
        for (int i = 0; i < paramNames.length; i++) {
            if (values[i] instanceof Collection) {
                q.setParameterList(paramNames[i], (Collection<?>) values[i]);
            } else {
                q.setParameter(paramNames[i], values[i]);
            }
        }
        
        q.executeUpdate();
        
    }
    
    public final void flush() {
        getCurrentSession().flush();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final <T> T get(final Class<T> entityClass, final Serializable id) {
        return (T) getCurrentSession().get(entityClass, id);
    }
    
    /**
     * @param lockOptions
     *            LockOptions.NONE Tries to get object from cache, if not present read it from the database. It does not create a database lock.
     *            LockOptions.READ Bypasses cache and reads object from the database.
     *            LockOptions.UPGRADE Bypasses cache and creates a lock using select for update statement. This might wait for a database timeout. By default
     *            the lock request will wait for ever.
     */
    @SuppressWarnings("unchecked")
    @Override
    public final <T> T get(final Class<T> entityClass, final Serializable id, final LockMode lockMode) {
        return (T) getCurrentSession().get(entityClass, id, new LockOptions(lockMode));
    }
    
    public final Query getNamedQuery(final String queryName) {
        return getCurrentSession().getNamedQuery(queryName);
    }
    
    public final <T> List<T> loadAll(final Class<T> entityClass) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("from ").append(entityClass.getName());
        final Query q = getCurrentSession().createQuery(bdr.toString());
        return q.list();
    }
    
    public final <T> List<T> loadWithLimit(final Class<T> entityClass, final int limit, final boolean isCacheable) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("from ").append(entityClass.getName());
        final Query q = getCurrentSession().createQuery(bdr.toString()).setMaxResults(limit).setCacheable(isCacheable);
        return q.list();
    }
    
    public final Serializable save(final Object entity) {
        return getCurrentSession().save(entity);
    }
    
    public final void save(final String entityName, final Object entity) {
        getCurrentSession().save(entityName, entity);
    }
    
    public final void saveOrUpdate(final Object entity) {
        getCurrentSession().saveOrUpdate(entity);
    }
    
    /**
     * There is no way to save the collection at one shot in session.
     */
    public final void saveOrUpdateAll(final Collection entities) {
        for (Object obj : entities) {
            saveOrUpdate(obj);
        }
    }
    
    public final void update(final Object entity) {
        getCurrentSession().update(entity);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final <T> T merge(final T entity) {
        return (T) getCurrentSession().merge(entity);
    }
    
    public final SQLQuery createSQLQuery(final String queryStr) {
        return getCurrentSession().createSQLQuery(queryStr);
    }
    
    public final void evict(final Object obj) {
        getCurrentSession().evict(obj);
    }
    
    public final List findPageResultByNamedQuery(final String queryName, final String[] paramNames, final Object[] values, final boolean isCacheable,
        final int page, final int pageSize) {
        final Query queryObject = getCurrentSession().getNamedQuery(queryName);
        queryObject.setCacheable(isCacheable);
        if (values != null) {
            for (int i = 0; i < values.length; i++) {
                if (values[i] instanceof Collection) {
                    queryObject.setParameterList(paramNames[i], (Collection) values[i]);
                } else {
                    queryObject.setParameter(paramNames[i], values[i]);
                }
            }
        }
        if ((pageSize != 0) && (page > 0)) {
            queryObject.setFirstResult((page - 1) * pageSize);
            queryObject.setMaxResults(pageSize);
        }
        return queryObject.list();
    }
    
    public final int findTotalResultByNamedQuery(final String queryName, final String[] names, final Object[] values) {
        final Query q = getCurrentSession().getNamedQuery(queryName);
        if (values != null) {
            for (int i = 0; i < values.length; i++) {
                if (values[i] instanceof Collection) {
                    q.setParameterList(names[i], (Collection) values[i]);
                } else {
                    q.setParameter(names[i], values[i]);
                }
                
            }
        }
        final Iterator it = q.list().iterator();
        
        int num_records = 0;
        while (it.hasNext()) {
            num_records += ((Number) it.next()).intValue();
        }
        
        return (num_records);
    }
    
    public final int findTotalResultByNamedQueryWithLock(final String queryName, final String[] names, final Object[] values, final String alias) {
        final Query q = getCurrentSession().getNamedQuery(queryName).setLockMode(alias, LockMode.PESSIMISTIC_WRITE);
        if (values != null) {
            for (int i = 0; i < values.length; i++) {
                if (values[i] instanceof Collection) {
                    q.setParameterList(names[i], (Collection) values[i]);
                } else {
                    q.setParameter(names[i], values[i]);
                }
                
            }
        }
        final Iterator it = q.list().iterator();
        
        int num_records = 0;
        while (it.hasNext()) {
            num_records += ((Number) it.next()).intValue();
        }
        
        return (num_records);
    }
    
    public final int deleteAllByNamedQuery(final String queryName, final Object[] values) {
        final Query deleteObject = getCurrentSession().getNamedQuery(queryName);
        if (values != null) {
            for (int i = 0; i < values.length; i++) {
                deleteObject.setParameter(i, values[i]);
            }
        }
        return deleteObject.executeUpdate();
        
    }
    
    public final List findByNamedQuery(final String queryName, final boolean isCacheable, final int startRow, final int howManyRows) {
        final Query queryObject = getCurrentSession().getNamedQuery(queryName);
        queryObject.setCacheable(isCacheable);
        if (startRow >= 0) {
            queryObject.setFirstResult(startRow);
        }
        if (howManyRows > 0) {
            queryObject.setMaxResults(howManyRows);
        }
        return queryObject.list();
    }
    
    public final List findByNamedQuery(final String queryName, final Object[] values, final int startRow, final int howManyRows) {
        final Query queryObject = getCurrentSession().getNamedQuery(queryName);
        queryObject.setCacheable(true);
        if (values != null) {
            for (int i = 0; i < values.length; i++) {
                queryObject.setParameter(i, values[i]);
            }
        }
        if (startRow >= 0) {
            queryObject.setFirstResult(startRow);
        }
        if (howManyRows > 0) {
            queryObject.setMaxResults(howManyRows);
        }
        return queryObject.list();
    }
    
    public final List findByNamedQuery(final String queryName, final String[] names, final Object[] values, final int startRow,
        final int howManyRows) {
        final Query queryObject = getCurrentSession().getNamedQuery(queryName);
        queryObject.setCacheable(false);
        if (values != null) {
            for (int i = 0; i < values.length; i++) {
                if (values[i] instanceof Collection) {
                    queryObject.setParameterList(names[i], (Collection) values[i]);
                } else {
                    queryObject.setParameter(names[i], values[i]);
                }
            }
        }
        if (startRow >= 0) {
            queryObject.setFirstResult(startRow);
        }
        if (howManyRows > 0) {
            queryObject.setMaxResults(howManyRows);
        }
        return queryObject.list();
    }
    
    public final int updateByNamedQuery(final String queryName, final String[] names, final Object[] values) {
        final Query updateObject = getCurrentSession().getNamedQuery(queryName);
        if (values != null) {
            for (int i = 0; i < values.length; i++) {
                if (values[i] instanceof Collection) {
                    updateObject.setParameterList(names[i], (Collection) values[i]);
                } else {
                    updateObject.setParameter(names[i], values[i]);
                }
            }
        }
        return updateObject.executeUpdate();
    }
    
    public final Session getCurrentSession() {
        return this.sessionFactory.getCurrentSession();
    }
    
    public final Criteria createCriteria(final Class clazz) {
        return getCurrentSession().createCriteria(clazz);
    }
    
    @Override
    public final void clearNamedQuery(final String cacheRegion) {
        final Cache currentCache = this.sessionFactory.getCache();
        currentCache.evictQueryRegion(cacheRegion);
    }
    
    @Override
    public final void clearCollection(final String cacheRegion) {
        final Cache currentCache = this.sessionFactory.getCache();
        currentCache.evictCollectionRegion(cacheRegion);
    }
    
    @Override
    public final void clearEntity(final String entityName) {
        final Cache currentCache = this.sessionFactory.getCache();
        currentCache.evictEntityRegion(entityName);
    }
    
    @Override
    public final boolean clearFromCache(final Class<?> entityClass, final Serializable identifier) {
        boolean cleared = false;
        
        final Cache cache = this.sessionFactory.getCache();
        if (cache.containsEntity(entityClass, identifier)) {
            cache.evictEntity(entityClass, identifier);
            cleared = true;
        }
        
        return cleared;
    }
    
    @Override
    public final boolean clearFromCache(final Object entity) {
        boolean cleared = false;
        
        final Session session = this.sessionFactory.getCurrentSession();
        final String entityName = session.getEntityName(entity);
        final Serializable identifier = session.getIdentifier(entity);
        
        session.evict(entity);
        
        final Cache cache = this.sessionFactory.getCache();
        if (cache.containsEntity(entityName, identifier)) {
            cache.evictEntity(entityName, identifier);
            cleared = true;
        }
        
        return cleared;
    }
    
    @Override
    public final void clearFromCache(final Query query) {
        final List<?> entitiesList = query.setCacheable(false).list();
        for (Object buffer : entitiesList) {
            if (buffer instanceof Object[]) {
                final Object[] bufferArray = (Object[]) buffer;
                int idx = bufferArray.length;
                while (--idx >= 0) {
                    clearFromCache(bufferArray[idx]);
                }
            } else {
                clearFromCache(buffer);
            }
        }
    }
    
    @Override
    public final void bringToCache(final Object entity) {
        final Session session = this.sessionFactory.getCurrentSession();
        session.evict(entity);
        session.update(entity);
    }
    
    @Override
    public final void bringToCache(final Query query) {
        final List<?> entitiesList = query.setCacheable(false).setCacheMode(CacheMode.REFRESH).list();
        for (Object buffer : entitiesList) {
            if (buffer instanceof Object[]) {
                final Object[] bufferArray = (Object[]) buffer;
                int idx = bufferArray.length;
                while (--idx >= 0) {
                    bringToCache(bufferArray[idx]);
                }
            } else {
                bringToCache(buffer);
            }
        }
    }
    
    @Override
    public final Criterion nullableAnd(final Criterion mainCondition, final Criterion additionalCondition) {
        Criterion result = null;
        if (mainCondition == null) {
            result = additionalCondition;
        } else if (additionalCondition == null) {
            result = mainCondition;
        } else {
            result = Restrictions.and(mainCondition, additionalCondition);
        }
        
        return result;
    }
    
    @Override
    public final Criterion nullableOr(final Criterion mainCondition, final Criterion additionalCondition) {
        Criterion result = null;
        if (mainCondition == null) {
            result = additionalCondition;
        } else if (additionalCondition == null) {
            result = mainCondition;
        } else {
            result = Restrictions.or(mainCondition, additionalCondition);
        }
        
        return result;
    }
    
    @Override
    public void initialize(final Object proxy) {
        Hibernate.initialize(proxy);
    }
    
}
