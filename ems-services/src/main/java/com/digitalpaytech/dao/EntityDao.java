package com.digitalpaytech.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.springframework.dao.DataAccessException;

/**
 * Provide a convenience Entity Manager interface. This interface will eventually acts as the whole DAO layer.
 */
public interface EntityDao {
    
    /**
     * Create a simple SQL Query
     * 
     * @param queryStr
     * @return
     */
    SQLQuery createSQLQuery(String queryStr);
    
    /**
     * Create a simple HQL Query
     * 
     * @param queryStr
     * @return
     */
    //	public Query createQuery(String queryStr);
    
    /**
     * Delete the given persistent instance.
     * 
     * @param entity
     *            the persistent instance to delete
     * @throws org.springframework.dao.DataAccessException
     *             in case of Hibernate errors
     * @see net.sf.hibernate.Session#delete(Object)
     */
    void delete(Object entity);
    
    /**
     * Evict the given persistent instance.
     * 
     * @param entity
     *            the persistent instance to evict
     * @throws org.springframework.dao.DataAccessException
     *             in case of Hibernate errors
     * @see net.sf.hibernate.Session#delete(Object)
     */
    void evict(Object obj);
    
    /**
     * Execute a named query for persistent instances. A named query is defined in a Hibernate mapping file.
     * 
     * @param queryName
     *            the name of a Hibernate query in a mapping file
     * @return a List containing 0 or more persistent instances
     * @throws org.springframework.dao.DataAccessException
     *             in case of Hibernate errors
     * @see net.sf.hibernate.Session#find(String)
     * @see net.sf.hibernate.Session#getNamedQuery(String)
     */
    List findByNamedQuery(String queryName);
    
    List findByNamedQuery(String queryName, boolean cacheable);
    
    /**
     * Execute a named query for persistent instances with SELECT FOR UPDATE and lock the table for update
     * 
     * @param queryName
     *            the name of a Hibernate query in a mapping file
     * @param lockedAlias
     *            the alias of the Table in the SQL
     * @return a List containing 0 or more persistent instances
     * @throws org.springframework.dao.DataAccessException
     *             in case of Hibernate errors
     * @see net.sf.hibernate.Session#find(String)
     * @see net.sf.hibernate.Session#getNamedQuery(String)
     */
    List findByNamedQueryWithLock(String queryName, String lockedAlias);
    
    Object findUniqueByNamedQueryWithLock(String queryName, String lockedAlias);
    
    List findByNamedQueryAndNamedParamWithLock(String queryName, String paramName, Object value, String lockedAlias);
    
    Object findUniqueByNamedQueryAndNamedParamWithLock(String queryName, String paramName, Object value, String lockedAlias);
    
    /**
     * Execute a named query for persistent instances, binding one value to a ":" named parameter in the query
     * string. A named query is defined in a Hibernate mapping file.
     * 
     * @param queryName
     *            the name of a Hibernate query in a mapping file
     * @param paramName
     *            the name of parameter
     * @param value
     *            the value of the parameter
     * @return a List containing 0 or more persistent instances
     * @throws org.springframework.dao.DataAccessException
     *             in case of Hibernate errors
     * @see net.sf.hibernate.Session#find(String, Object, net.sf.hibernate.type.Type)
     * @see net.sf.hibernate.Session#getNamedQuery(String)
     */
    List findByNamedQueryAndNamedParam(String queryName, String paramName, Object value);
    
    List findByNamedQueryAndNamedParam(String queryName, String paramName, Object value, boolean cacheable);
    
    List findByNamedQueryAndNamedParam(String queryName, String[] paramNames, Object[] values, int startingResult, int maxResult, boolean cacheable);
    
    /**
     * Execute a named query for persistent instances, binding a number of values to ":" named parameters in
     * the query string. A named query is defined in a Hibernate mapping file.
     * 
     * @param queryName
     *            the name of a Hibernate query in a mapping file
     * @param paramNames
     *            the names of the parameters
     * @param values
     *            the values of the parameters
     * @return a List containing 0 or more persistent instances
     * @throws org.springframework.dao.DataAccessException
     *             in case of Hibernate errors
     * @see net.sf.hibernate.Session#find(String, Object[], net.sf.hibernate.type.Type[])
     * @see net.sf.hibernate.Session#getNamedQuery(String)
     */
    List findByNamedQueryAndNamedParam(String queryName, String[] paramNames, Object[] values);
    
    List findByNamedQueryAndNamedParam(String queryName, String[] paramNames, Object[] values, boolean cacheable);
    
    List findByNamedQueryAndNamedParamWithLimit(String queryName, String[] paramNames, Object[] values, boolean cacheable, int limit);
    
    Object findUniqueByNamedQueryAndNamedParam(String queryName, String paramName, Object value);
    
    Object findUniqueByNamedQueryAndNamedParam(String queryName, String paramName, Object value, boolean cacheable);
    
    Object findUniqueByNamedQueryAndNamedParam(String queryName, String[] paramNames, Object[] values, boolean cacheable);
    
    /**
     * Flush all pending saves, updates and deletes to the database.
     * <p>
     * Only invoke this for selective eager flushing, for example when JDBC code needs to see certain changes within the same transaction. Else, it's preferable
     * to rely on auto-flushing at transaction completion.
     * 
     * @throws org.springframework.dao.DataAccessException
     *             in case of Hibernate errors
     * @see net.sf.hibernate.Session#flush
     */
    void flush();
    
    /**
     * Return the persistent instance of the given entity class with the given identifier, or <code>null</code> if not found.
     * 
     * @param entityClass
     *            a persistent class
     * @param id
     *            an identifier of the persistent instance
     * @return the persistent instance, or <code>null</code> if not found
     * @throws org.springframework.dao.DataAccessException
     *             in case of Hibernate errors
     * @see net.sf.hibernate.Session#get(Class, java.io.Serializable)
     */
    <T> T get(Class<T> entityClass, Serializable id);
    
    /**
     * @param lockOptions
     *            LockOptions.NONE Tries to get object from cache, if not present read it from the database. It does not create a database lock.
     *            LockOptions.READ Bypasses cache and reads object from the database.
     *            LockOptions.UPGRADE Bypasses cache and creates a lock using select for update statement. This might wait for a database timeout. By default
     *            the lock request will wait for ever.
     */
    <T> T get(Class<T> entityClass, Serializable id, LockMode lockMode);
    
    /**
     * Return a instance of a named query
     * 
     * @param queryName
     * @return
     * @throws DataAccessException
     */
    Query getNamedQuery(String queryName);
    
    /**
     * Return the persistent instance of the given entity class with the given identifier, throwing an
     * exception if not found.
     * 
     * @param entityClass
     *            a persistent class
     * @param id
     *            an identifier of the persistent instance
     * @return the persistent instance
     * @throws org.springframework.orm.ObjectRetrievalFailureException
     *             if not found
     * @throws org.springframework.dao.DataAccessException
     *             in case of Hibernate errors
     * @see net.sf.hibernate.Session#load(Class, java.io.Serializable)
     */
    //	Object load(Class entityClass, Serializable id);
    
    /**
     * Return all persistent instances of the given entity class. Note: Use queries or criteria for retrieving
     * a specific subset.
     * 
     * @param entityClass
     *            a persistent class
     * @return a List containing 0 or more persistent instances
     * @throws org.springframework.dao.DataAccessException
     *             if there is a Hibernate error
     * @see org.hibernate.Session#createCriteria
     */
    <T> List<T> loadAll(Class<T> entityClass);
    
    /**
     * Return "limit" number of rows persistent instances of the given entity class. Note: Use queries or criteria for retrieving
     * a specific subset.
     * 
     * @param entityClass
     *            a persistent class
     * @param limit
     *            number of rows
     * @return a List containing 0 or more persistent instances
     * @throws org.springframework.dao.DataAccessException
     *             if there is a Hibernate error
     * @see org.hibernate.Session#createCriteria
     */
    <T> List<T> loadWithLimit(Class<T> entityClass, int limit, boolean isCacheable);
    
    /**
     * Copy the state of the given object onto the persistent object with the same identifier. Follows JSR-220
     * semantics.
     * <p>
     * Similar to <code>saveOrUpdate</code>, but never associates the given object with the current Hibernate Session. In case of a new entity, the state will
     * be copied over as well.
     * <p>
     * Note that <code>merge</code> will <i>not</i> update the identifiers in the passed-in object graph (in contrast to TopLink)! Consider registering Spring's
     * IdTransferringMergeEventListener if you'd like to have newly assigned ids transferred to the original object graph too.
     * 
     * @param entity
     *            the object to merge with the corresponding persistence instance
     * @return the updated, registered persistent instance
     * @throws org.springframework.dao.DataAccessException
     *             in case of Hibernate errors
     * @see org.hibernate.Session#merge(Object)
     * @see #saveOrUpdate
     * @see org.springframework.orm.hibernate4.support.IdTransferringMergeEventListener
     */
    <T> T merge(T entity);
    
    /**
     * Copy the state of the given object onto the persistent object with the same identifier. Follows JSR-220
     * semantics.
     * <p>
     * Similar to <code>saveOrUpdate</code>, but never associates the given object with the current Hibernate Session. In case of a new entity, the state will
     * be copied over as well.
     * <p>
     * Note that <code>merge</code> will <i>not</i> update the identifiers in the passed-in object graph (in contrast to TopLink)! Consider registering Spring's
     * IdTransferringMergeEventListener if you'd like to have newly assigned ids transferred to the original object graph too.
     * 
     * @param entityName
     *            the name of a persistent entity
     * @param entity
     *            the object to merge with the corresponding persistence instance
     * @return the updated, registered persistent instance
     * @throws org.springframework.dao.DataAccessException
     *             in case of Hibernate errors
     * @see org.hibernate.Session#merge(Object)
     * @see #saveOrUpdate
     */
    //	Object merge(String entityName, Object entity);
    
    /**
     * Persist the given transient instance.
     * 
     * @param entity
     *            the transient instance to persist
     * @return the generated identifier
     * @throws org.springframework.dao.DataAccessException
     *             in case of Hibernate errors
     * @see net.sf.hibernate.Session#save(Object)
     */
    Serializable save(Object entity);
    
    /**
     * Persist the given transient instance.
     * 
     * @param entityName
     *            the name of a persistent entity
     * @param entity
     *            the transient instance to persist
     * @throws org.springframework.dao.DataAccessException
     *             in case of Hibernate errors
     * @see net.sf.hibernate.Session#save(String, Object)
     */
    //	void save(String entityName, Object entity);
    
    /**
     * Save or update the given persistent instance, according to its id (matching the configured
     * "unsaved-value"?).
     * 
     * @param entity
     *            the persistent instance to save or update (to be associated with the Hibernate Session)
     * @throws DataAccessException
     *             in case of Hibernate errors
     * @see net.sf.hibernate.Session#saveOrUpdate(Object)
     */
    void saveOrUpdate(Object entity);
    
    /**
     * Save or update all given persistent instances, according to its id.
     * 
     * @param entities
     *            the persistent instances to save or update (to be associated with the Hibernate Session)
     * @throws DataAccessException
     *             in case of Hibernate errors
     * @see net.sf.hibernate.Session#saveOrUpdate(Object)
     */
    void saveOrUpdateAll(Collection entities);
    
    /**
     * Update the given persistent instance.
     * 
     * @param entity
     *            the persistent instance to update
     * @throws org.springframework.dao.DataAccessException
     *             in case of Hibernate errors
     * @see net.sf.hibernate.Session#update(Object)
     */
    void update(Object entity);
    
    /**
     * Find paged result by named query
     * 
     * @param queryName
     *            The query name
     * @param values
     *            parameter values
     * @param page
     * @param pageSize
     *            set to 0 to get all results
     * @return
     * @throws DataAccessException
     */
    //	public List findPageResultByNamedQuery(String queryName, Object[] values, int page, int pageSize);
    
    List findPageResultByNamedQuery(String queryName, String[] paramNames, Object[] values, boolean isCacheable, int page, int pageSize);
    
    /**
     * Find the number of records by named query
     * 
     * @param queryName
     *            query name
     * @param values
     *            parameter values
     * @return number of records found
     */
    //	public int findTotalResultByNamedQuery(String queryName, Object[] values);
    
    /**
     * Find the number of records by named query
     * 
     * @param queryName
     *            query name
     * @param names
     *            parameter names
     * @param values
     *            parameter values
     * @return number of records found
     */
    int findTotalResultByNamedQuery(String queryName, String[] names, Object[] values);
    
    /**
     * Find the number of records by named query
     * 
     * @param queryName
     *            query name
     * @param names
     *            parameter names
     * @param values
     *            parameter values
     * @param alias
     *            alias of the table to be locked
     * @return number of records found
     */
    int findTotalResultByNamedQueryWithLock(String queryName, String[] names, Object[] values, String alias);
    
    /**
     * Delete all records by named query
     * 
     * @param queryName
     *            query name
     * @param values
     *            parameter values
     * @return number of records updated
     * @throws DataAccessException
     */
    int deleteAllByNamedQuery(String queryName, Object[] values);
    
    /**
     * Find paged result by named query
     * 
     * @param queryName
     *            query name
     * @param startRow
     * @param howManyRows
     *            set to 0 to get all results
     * @return
     */
    List findByNamedQuery(String queryName, boolean isCacheable, int startRow, int howManyRows);
    
    /**
     * Find paged result by named query
     * 
     * @param queryName
     *            query name
     * @param values
     *            parameter values
     * @param startRow
     * @param howManyRows
     *            set to 0 to get all results
     * @return
     */
    List findByNamedQuery(String queryName, Object[] values, int startRow, int howManyRows);
    
    //	public List findAndEvictByNamedQuery(String queryName, Object[] values, int startRow, int howManyRows);
    
    /**
     * Find paged result by named query
     * 
     * @param queryName
     *            query name
     * @param names
     *            parameter names
     * @param values
     *            parameter values
     * @param startRow
     * @param howManyRows
     *            set to 0 to get all results
     * @return
     */
    List findByNamedQuery(String queryName, String[] names, Object[] values, int startRow, int howManyRows);
    
    /**
     * Find result by SQL String
     * 
     * @param SQLString
     * @param names
     *            parameter names
     * @param values
     *            parameter values
     * @param startRow
     * @param howManyRows
     *            set to 0 to get all results
     * @return
     */
    //	public Collection findBySQLString(String SQLString, String[] names, Object[] values, int startRow,
    //			int howManyRows);
    
    //	public Collection findBySQLString(String SQLString, Object[] values, int startRow, int howManyRows);
    
    //	public Collection findPageResultBySQLString(String SQLString, Object[] values, int page, int pageSize);
    
    //	public Collection findPageResultByQuery(String queryString, Object[] values, int page, int pageSize);
    
    //	public Collection findByQuery(String queryString, Object[] values, int startRow, int howManyRows);
    
    //	public Collection findByQuery(String queryString, String[] names, Object[] values, int startRow,
    //			int howManyRows);
    
    /**
     * Find the number of records by SQL String
     * 
     * @param SQLString
     *            SQL Strring
     * @param names
     *            parameter names
     * @param values
     *            parameter values
     * @return number of records found
     */
    //	public int findTotalResultBySQLString(String SQLString, String[] names, Object[] values);
    
    //	public int findTotalResultBySQLString(String SQLString, Object[] values);
    
    //	public int findTotalResultByQuery(String queryString, Object[] values);
    
    /**
     * Delete a record by primary key.
     * 
     * @param entityClass
     * @param id
     * @throws DataAccessException
     */
    //	public void deleteByPrimaryKey(Class entityClass, Serializable id);
    
    /**
     * Update table by named query.
     * 
     * @param queryName
     *            query name
     * @param names
     *            parameter names
     * @param values
     *            parameter values
     * @return number of records updated
     * @throws DataAccessException
     */
    int updateByNamedQuery(final String queryName, final String[] names, final Object[] values);
    
    //	public long findLongResultByNamedQuery(String queryName, String paramName, Object value);
    
    Session getCurrentSession();
    
    Criteria createCriteria(Class clazz);
    
    boolean clearFromCache(Class<?> entityClass, Serializable id);
    
    /**
     * Remove entity Object from Session's cache and 2nd level cache. Note that this method does not touch Query's cache.
     * 
     * @param entity
     *            Object to be remove from cache.
     * @return true if the entity Object was cached (and, of course, removed).
     */
    boolean clearFromCache(Object entity);
    
    /**
     * Use the specified Hibernate's {@link Query} to determine the Objects need to be clear.
     * This method simply execute the Query and use the result returned from the Query to call {@link #clearFromCache(Object)}.
     * 
     * Note that, to use one Query to select multiple entity type, you can just use Query that return Object[].
     * 
     * @param query
     *            Query to retrieve the Objects that need to be clear from cache.
     */
    void clearFromCache(Query query);
    
    /**
     * Bring the stale entity into Hibernate. This will refresh Session's cache, 2nd level cache, and Query's cache.
     * Normally when an entity Object is insert with out Hibernate's awareness, the cached query will not select this new entity Object.
     * This method simply send update to Hibernate's session to trigger the Query's cache to be refreshed.
     * 
     * @param entity
     *            entity Object to be added to cache.
     */
    void bringToCache(Object entity);
    
    /**
     * Use the specified Hibernate's {@link Query} to determine the Objects need to be bring into cache.
     * This method simply execute the Query and use the result returned from the Query to call {@link #bringToCache(Object)}.
     * 
     * Note that, to use one Query to select multiple entity type, you can just use Query that return Object[].
     * 
     * @param query
     *            Query to retrieve the Objects that need to be added to cache.
     */
    void bringToCache(Query query);
    
    /**
     * Clear 2nd level cached version of namedQuery.
     * 
     */
    void clearNamedQuery(String cacheRegion);
    
    /**
     * Clear 2nd level cached version of collection.
     * 
     */
    void clearCollection(String cacheRegion);
    
    void clearEntity(String entityName);
    
    Criterion nullableAnd(Criterion mainCondition, Criterion additionalCondition);
    
    Criterion nullableOr(Criterion mainCondition, Criterion additionalCondition);
    
    void initialize(Object proxy);
    
    void deleteByNamedQueryAndNamedParams(String queryName, String[] paramNames, Object[] values);
}
