package com.digitalpaytech.dao;

import java.io.Serializable;
import java.util.List;

public interface OpenSessionDao {
    
    List findByNamedQuery(String queryName);
    
    Object findUniqueByNamedQueryAndNamedParam(String queryName, String paramName, Object value);
    
    Serializable save(Object entity);
    
    void update(Object entity);
    
    <T> List<T> loadAll(Class<T> entityClass);
    
    <T> List<T> findByNamedQueryAndNamedParam(String queryName, String[] paramNames, Object[] values, boolean cacheable);
}
