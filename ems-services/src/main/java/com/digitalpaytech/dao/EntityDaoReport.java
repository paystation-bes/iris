package com.digitalpaytech.dao;

import org.hibernate.Session;

public interface EntityDaoReport {
    Session getCurrentSession();
}
