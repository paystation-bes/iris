package com.digitalpaytech.dao;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

/**
 * Provide a convenience Entity Manager interface. This interface will eventually acts as the whole DAO layer.
 */
public interface EntityDaoEMS6 {
    
    /**
     * Create a simple SQL Query
     * 
     * @param queryStr
     * @return
     */
    SQLQuery createSQLQuery(String queryStr);
    
    Session getCurrentSession();
}
