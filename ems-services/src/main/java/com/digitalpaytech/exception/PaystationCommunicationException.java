package com.digitalpaytech.exception;

import com.digitalpaytech.dto.rest.paystation.EMSDomainNotification;
import com.digitalpaytech.dto.rest.paystation.TimestampNotification;

public class PaystationCommunicationException extends ApplicationException {
    
    private static final long serialVersionUID = 2747007380290307570L;
    private int status;
    private EMSDomainNotification emsDomainNotification;
    private TimestampNotification timestampNotification;
    
    /**
     * Constructor for PaystationCommunicationException.
     */
    public PaystationCommunicationException() {
        super();
    }
    
    /**
     * Constructor for PaystationCommunicationException.
     */
    public PaystationCommunicationException(int status) {
        super();
        this.status = status;
    }
    
    /**
     * Constructor for PaystationCommunicationException.
     */
    public PaystationCommunicationException(int status, EMSDomainNotification emsDomainNotification) {
        super();
        this.status = status;
        this.emsDomainNotification = emsDomainNotification;
    }

    /**
     * Constructor for PaystationCommunicationException.
     */
    public PaystationCommunicationException(int status, TimestampNotification timestampNotification) {
        super();
        this.status = status;
        this.timestampNotification = timestampNotification;
    }

    /**
     * Constructor for PaystationCommunicationException.
     * 
     * @param message
     */
    public PaystationCommunicationException(String message) {
        super(message);
    }
    
    /**
     * Constructor for PaystationCommunicationException.
     * 
     * @param message
     * @param cause
     */
    public PaystationCommunicationException(String message, Throwable cause) {
        super(message, cause);
    }
    
    /**
     * Constructor for PaystationCommunicationException.
     * 
     * @param cause
     */
    public PaystationCommunicationException(Throwable cause) {
        super(cause);
    }
    
    public int getStatus() {
        return status;
    }
    
    public void setStatus(int status) {
        this.status = status;
    }
    
    public EMSDomainNotification getEmsDomainNotification() {
        return emsDomainNotification;
    }
    
    public void setEmsDomainNotification(EMSDomainNotification emsDomainNotification) {
        this.emsDomainNotification = emsDomainNotification;
    }

    public TimestampNotification getTimestampNotification() {
        return timestampNotification;
    }
    
    public void setTimestampNotification(TimestampNotification timestampNotification) {
        this.timestampNotification = timestampNotification;
    }

}
