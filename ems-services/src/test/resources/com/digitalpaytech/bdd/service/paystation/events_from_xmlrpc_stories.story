Scenario: Testing the Events comming from the paystation

Given the InMemoryDataBase is reset
And there exists a Pay Station where the 
name is testpaystation
pay station type is LUKE II
When the testpaystation sends <type>, <action>, <information> at <timestamp>
Then the created event has the following <eventactiontype>, <eventseverity>, <active>, <queuetype> and <eventtype>

Examples:
|type|action|information|timestamp|severity|eventactiontype|eventseverity|active|queuetype|eventtype|
|Battery|Normal|null|now|null|Normal|Clear|FALSE|Recent & Historical|Battery Level|
|Battery|Low|null|now|null|Low|Major|True|Recent & Historical|Battery Level|
|Battery2|Normal|null|now|null|Normal|Clear|FALSE|Recent & Historical|Battery Level|
|Battery2|Low|null|now|null|Low|Major|True|Recent & Historical|Battery Level|
|BillAcceptor|NotPresent|null|now|null|null|Clear|TRUE|null|Bill Acceptor Present|Bill Acceptor Present|
|BillAcceptor|Jam|null|now|null|null|Major|TRUE|Recent & Historical|Bill Acceptor Jam|
|BillAcceptor|UnableToStack|null|now|null|Alerted|Major|TRUE|Recent & Historical|Bill Stacker Unable To Stack|
|BillAcceptor|Present|null|now|null|null|Clear|FALSE|null|Bill Acceptor Present|
|BillAcceptor|JamCleared|null|now|null|null|Clear|FALSE|Recent & Historical|Bill Acceptor Jam|
|BillAcceptor|UnableToStackCleared|null|now|null|Cleared|Clear|FALSE|Recent & Historical|Bill Stacker Unable To Stack|
|BillStacker|NotPresent|null|now|null|null|Clear|TRUE|null|Bill Stacker Present|Bill Stacker Present|
|BillStacker|Full|null|now|null|Full|Major|TRUE|Recent & Historical|Bill Stacker Level|
|BillStacker|Present|null|now|null|null|Clear|FALSE|null|Bill Stacker Present|
|BillStacker|FullCleared|null|now|null|Cleared|Clear|FALSE|Recent & Historical|Bill Stacker Level|
|BillStacker|Removed|null|now|null|Alerted|Minor|TRUE|Recent & Historical|Bill Canister Removed|
|BillStacker|Inserted|null|now|null|Cleared|Clear|FALSE|Recent & Historical|Bill Canister Removed|
|CardReader|NotPresent|null|now|null|null|Major|TRUE|Recent & Historical|Card Reader Present|
|CardReader|Present|null|now|null|null|Clear|FALSE|Recent & Historical|Card Reader Present|
|CoinAcceptor|NotPresent|null|now|null|null|Clear|TRUE|null|Coin Acceptor Present|
|CoinAcceptor|Jam|null|now|null|Alerted|Major|TRUE|Recent & Historical|Coin Acceptor Jam|
|CoinAcceptor|Present|null|now|null|null|Clear|FALSE|null|Coin Acceptor Present|
|CoinAcceptor|JamCleared|null|now|null|Cleared|Clear|FALSE|Recent & Historical|Coin Acceptor Jam|
|CoinEscrow|NotPresent|null|now|null|null|Clear|TRUE|null|Coin Escrow Present|
|CoinEscrow|Jam|null|now|null|null|Major|TRUE|Recent & Historical|Coin Escrow Jam|
|CoinEscrow|Present|null|now|null|null|Clear|FALSE|null|Coin Escrow Present|
|CoinEscrow|JamCleared|null|now|null|null|Clear|FALSE|Recent & Historical|Coin Escrow Jam|
|CoinCanister|Present|null|now|null|null|Clear|FALSE|null|Coin Canister Present|
|CoinCanister|NotPresent|null|now|null|null|Clear|TRUE|null|Coin Canister Present|
|CoinCanister|Removed|null|now|null|null|Minor|TRUE|Recent & Historical|Coin Canister Removed|
|CoinCanister|Inserted|null|now|null|null|Clear|FALSE|Recent & Historical|Coin Canister Removed|
|Door|DoorOpened|null|now|null|null|Minor|TRUE|Recent & Historical|Door Open|
|Paystation|LowPowerShutdownOn|null|now|null|null|Critical|TRUE|Recent & Historical|Pay Station Battery Depleted|
|Paystation|ServiceModeEnabled|null|now|null|null|Minor|TRUE|Recent & Historical|Pay Station In Service Mode|
|ShockAlarm|ShockOn|null|now|null|null|Critical|TRUE|Recent & Historical|Shock Alarm|
|Paystation|PublicKeyUpdate|0|now|null|null|Clear|TRUE|null|Pay Station Encryption Key Updated|
|Paystation|CoinChangerLow|null|now|null|Low|Minor|TRUE|Recent & Historical|Coin Changer Level|
|Paystation|CoinChangerHigh|null|now|null|High|Minor|TRUE|Recent & Historical|Coin Changer Level|
|Paystation|CoinChangerJam|null|now|null|null|Minor|TRUE|Recent & Historical|Coin Changer Jam|
|Paystation|CoinChangerJamCleared|null|now|null|null|Clear|FALSE|Recent & Historical|Coin Changer Jam|
|Door|DoorClosed|null|now|null|null|Clear|FALSE|Recent & Historical|Door Open|
|Paystation|ServiceModeDisabled|null|now|null|null|Clear|FALSE|Recent & Historical|Pay Station In Service Mode|
|Paystation|LowPowerShutdownOff|null|now|null|null|Clear|FALSE|Recent & Historical|Pay Station Battery Depleted|
|ShockAlarm|ShockOff|null|now|null|null|Clear|FALSE|Recent & Historical|Shock Alarm|
|Paystation|PublicKeyUpdate|TRUE|now|null|null|Clear|TRUE|null|Pay Station Encryption Key Updated|
|Paystation|Upgrade|null|now|null|null|Clear|TRUE|null|Pay Station Software Updated|
|Printer|NotPresent|null|now|null|null|Critical|TRUE|Recent & Historical|Printer Present|
|Printer|CutterError|null|now|null|null|Minor|TRUE|Recent & Historical|Printer Cutter Fault|
|Printer|HeadError|null|now|null|null|Minor|TRUE|Recent & Historical|Printer Head Fault|
|Printer|LeverDisengaged|null|now|null|null|Major|TRUE|Recent & Historical|Printer Level Disengaged|
|Printer|PaperLow|null|now|null|Low|Major|TRUE|Recent & Historical|Paper Status|
|Printer|PaperOut|null|now|null|Empty|Critical|TRUE|Recent & Historical|Paper Status|
|Printer|PaperJam|null|now|null|null|Critical|TRUE|Recent & Historical|Printer Paper Jam|
|Printer|TemperatureError|null|now|null|null|Minor|TRUE|Recent & Historical|Printer Temperature Fault|
|Printer|VoltageError|null|now|null|null|Minor|TRUE|Recent & Historical|Printer Voltage Fault|
|Printer|Present|null|now|null|null|Clear|FALSE|Recent & Historical|Printer Present|
|Printer|CutterErrorCleared|null|now|null|null|Clear|FALSE|Recent & Historical|Printer Cutter Fault|
|Printer|HeadErrorCleared|null|now|null|null|Clear|FALSE|Recent & Historical|Printer Head Fault|
|Printer|LeverDisengagedCleared|null|now|null|null|Clear|FALSE|Recent & Historical|Printer Level Disengaged|
|Printer|PaperOutCleared|null|now|null|Cleared|Clear|FALSE|Recent & Historical|Paper Status|
|Printer|PaperJamCleared|null|now|null|null|Clear|FALSE|Recent & Historical|Printer Paper Jam|
|Printer|PaperNormal|null|now|null|Cleared|Clear|FALSE|Recent & Historical|Paper Status|
|Printer|TemperatureErrorCleared|null|now|null|null|Clear|FALSE|Recent & Historical|Printer Temperature Fault|
|Printer|VoltageErrorCleared|null|now|null|null|Clear|FALSE|Recent & Historical|Printer Voltage Fault|
|RfidCardReader|NotPresent|null|now|null|null|clear|TRUE|null|RFID Card Reader Present|
|RfidCardReader|Present|null|now|null|null|clear|FALSE|null|RFID Card Reader Present|
|DoorLower|DoorOpened|null|now|null|null|Minor|TRUE|Recent & Historical|Pedestal Door Open|
|DoorLower|DoorClosed|null|now|null|null|Clear|FALSE|Recent & Historical|Pedestal Door Open|
|DoorUpper|DoorOpened|null|now|null|null|Minor|TRUE|Recent & Historical|Maintenance Door Open|
|DoorUpper|DoorClosed|null|now|null|null|Clear|FALSE|Recent & Historical|Maintenance Door Open|
|PCM|Upgrade|null|now|null|null|Clear|TRUE|null|PCM Upgraded|
|PCM|WirelessSignalStrength|null|now|null|null|Clear|TRUE|null|PCM - Wireless Signal Strength|
|DoorMaintenance|DoorOpened|null|now|null|null|Minor|TRUE|Recent & Historical|eventType.maintenancedoor.open|
|DoorMaintenance|DoorClosed|null|now|null|null|Clear|FALSE|Recent & Historical|eventType.maintenancedoor.open|
|DoorCashVault|DoorOpened|null|now|null|null|Minor|TRUE|Recent & Historical|Cash Vault Door Open|
|DoorCashVault|DoorClosed|null|now|null|null|Clear|FALSE|Recent & Historical|Cash Vault Door Open|
|Paystation|TicketNotTaken|null|now|null|null|Major|TRUE|Recent & Historical|Ticket Not Taken|
|Paystation|TicketNotTakenCleared|null|now|null|null|Clear|FALSE|Recent & Historical|Ticket Not Taken|
|Printer|TicketDidNotPrint|null|now|null|null|Critical|TRUE|Recent & Historical|Ticket Did Not Print|
|Printer|TicketDidNotPrintCleared|null|now|null|null|Clear|FALSE|Recent & Historical|Ticket Did Not Print|
|Printer|PrintingFailure|null|now|null|null|Critical|TRUE|Recent & Historical|Printing Failure|
|Printer|PrintingFailureCleared|null|now|null|null|Clear|FALSE|Recent & Historical|Printing Failure|
|Paystation|UIDChanged|null|now|null|null|Minor|FALSE|null|Pay Station - Mac Address Updated|
|Paystation|DuplicateSerialNumber|null|now|null|null|Critical|TRUE|Recent & Historical|Duplicate SerialNumber|
|Paystation|DuplicateSerialNumberCleared|null|now|null|null|Clear|FALSE|Recent & Historical|Duplicate SerialNumber|
|Paystation|ClockChanged|null|now|null|null|Minor|FALSE|null|Pay Station - Clock Reset Requested|

Scenario: Testing unsupported events are ignored by Luke II
Given the InMemoryDataBase is reset
And there exists a Pay Station where the
name is testpaystation
pay station type is LUKE II
When the testpaystation sends <type>, <action>, <information> at <timestamp>
Then the event is ignored

Examples:
|type|action|information|timestamp|
|CoinChanger|Empty|null|now|
|CoinChanger|Low|null|now|
|CoinChanger|Jam|null|now|
|CoinChanger|JamCleared|null|now|
|CoinChanger|Present|null|now|
|CoinChanger|Normal|null|now|
|CoinChanger|NotPresent|null|now|
|CoinHopper1|NotPresent|null|now|
|CoinHopper1|Empty|null|now|
|CoinHopper1|EmptyCleared|null|now|
|CoinHopper1|Present|null|now|
|CoinHopper2|NotPresent|null|now|
|CoinHopper2|Empty|null|now|
|CoinHopper2|Present|null|now|
|CoinHopper2|EmptyCleared|null|now|

Scenario: Testing unsupported events are ignored by Radius LUKE
Given the InMemoryDataBase is reset
And there exists a Pay Station where the
name is testpaystation
pay station type is Radius LUKE
When the testpaystation sends <type>, <action>, <information> at <timestamp>
Then the event is ignored

Examples:
|type|action|information|timestamp|
|CoinEscrow|NotPresent|null|now|
|CoinEscrow|Present|null|now|
|CoinEscrow|Jam|null|now|
|CoinEscrow|JamCleared|null|now|
|CoinHopper1|NotPresent|null|now|
|CoinHopper1|Empty|null|now|
|CoinHopper1|EmptyCleared|null|now|
|CoinHopper1|Present|null|now|
|CoinHopper2|NotPresent|null|now|
|CoinHopper2|Empty|null|now|
|CoinHopper2|Present|null|now|
|CoinHopper2|EmptyCleared|null|now|

Scenario: Testing unsupported events are ignored by SHELBY
Given the InMemoryDataBase is reset
And there exists a Pay Station where the
name is testpaystation
pay station type is SHELBY
When the testpaystation sends <type>, <action>, <information> at <timestamp>
Then the event is ignored

Examples:
|type|action|information|timestamp|
|CoinEscrow|NotPresent|null|now|
|CoinEscrow|Present|null|now|
|CoinEscrow|Jam|null|now|
|CoinEscrow|JamCleared|null|now|

Scenario: Testing unsupported events are ignored by V1LUKE
Given the InMemoryDataBase is reset
And there exists a Pay Station where the
name is testpaystation
pay station type is V1 LUKE
When the testpaystation sends <type>, <action>, <information> at <timestamp>
Then the event is ignored

Examples:
|type|action|information|timestamp|
|CoinEscrow|NotPresent|null|now|
|CoinEscrow|Present|null|now|
|CoinEscrow|Jam|null|now|
|CoinEscrow|JamCleared|null|now|
|CoinHopper1|NotPresent|null|now|
|CoinHopper1|Empty|null|now|
|CoinHopper1|EmptyCleared|null|now|
|CoinHopper1|Present|null|now|
|CoinHopper2|NotPresent|null|now|
|CoinHopper2|Empty|null|now|
|CoinHopper2|Present|null|now|
|CoinHopper2|EmptyCleared|null|now|