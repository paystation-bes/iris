package com.digitalpaytech.bdd.services;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.domain.TransactionFileUpload;

public final class TransactionFileUploadServiceMockImpl {
    
    private TransactionFileUploadServiceMockImpl() {
    }
    
    public static Answer<TransactionFileUpload> updateTransactionFileUpload() {
        return new Answer<TransactionFileUpload>() {
            @Override
            public TransactionFileUpload answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final TransactionFileUpload file = (TransactionFileUpload) args[0];
                TestDBMap.addToMemory(file.getId(), file);
                return null;
            }
        };
    }
    
    public static Answer<TransactionFileUpload> findTransactionFileUpload() {
        return new Answer<TransactionFileUpload>() {
            @Override
            public TransactionFileUpload answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final String fileName = (String) args[1];
                return (TransactionFileUpload) TestDBMap.findObjectByFieldFromMemory(fileName, TransactionFileUpload.ALIAS_FILE_NAME,
                                                                                     TransactionFileUpload.class);
            }
        };
    }
}
