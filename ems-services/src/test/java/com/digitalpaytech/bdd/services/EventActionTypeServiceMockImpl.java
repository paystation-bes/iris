package com.digitalpaytech.bdd.services;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Map;
import java.util.HashMap;

import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.domain.EventActionType;

public class EventActionTypeServiceMockImpl {
    private static final Map<Integer, EventActionType> eventActionTypesMap; 
    static {
        eventActionTypesMap = new HashMap<>();
        eventActionTypesMap.put(Integer.valueOf(0), new EventActionType(0, ""));
        eventActionTypesMap.put(Integer.valueOf(1), new EventActionType(1, "Alerted"));
        eventActionTypesMap.put(Integer.valueOf(2), new EventActionType(2, "Cleared"));
        eventActionTypesMap.put(Integer.valueOf(3), new EventActionType(3, "Full"));
        eventActionTypesMap.put(Integer.valueOf(4), new EventActionType(4, "Normal"));
        eventActionTypesMap.put(Integer.valueOf(5), new EventActionType(5, "Low"));
        eventActionTypesMap.put(Integer.valueOf(6), new EventActionType(6, "Empty"));
        eventActionTypesMap.put(Integer.valueOf(7), new EventActionType(7, "High"));
        eventActionTypesMap.put(Integer.valueOf(8), new EventActionType(8, "Info"));
        eventActionTypesMap.put(Integer.valueOf(9), new EventActionType(9, "Violated"));
        eventActionTypesMap.put(Integer.valueOf(10), new EventActionType(10, "Tampered"));
        eventActionTypesMap.put(Integer.valueOf(11), new EventActionType(11, "Fault"));
    }
    
    private EventActionTypeServiceMockImpl() {
    
    }
    
    @Deprecated
    public static Answer<EventActionType> findEventActionType() {
        return new Answer<EventActionType>() {
            
            @Override
            public EventActionType answer(final InvocationOnMock invocation) throws Throwable {
                final int eventActionTypeId = (int) invocation.getArguments()[0];
                return TestLookupTables.getEventActionList().get(eventActionTypeId);
            }
        };
    }

    public static Answer<EventActionType> findEventActionTypeMock() {
        return new Answer<EventActionType>() {
            @Override
            public EventActionType answer(final InvocationOnMock invocation) throws Throwable {
                final int eventActionTypeId = (int) invocation.getArguments()[0];
                return EventActionTypeServiceMockImpl.eventActionTypesMap.get(eventActionTypeId);
            }
        };
    }
}
