package com.digitalpaytech.bdd.services;

import java.util.List;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.EntityDB;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.domain.SubscriptionType;

@SuppressWarnings({"checkstyle:illegalthrows"})
public final class SubscriptionTypeServiceMockImpl {
    
    private SubscriptionTypeServiceMockImpl() {
    }
    
    public static Answer<List<SubscriptionType>> loadAll() {
        return new Answer<List<SubscriptionType>>() {
            
            @Override
            public List<SubscriptionType> answer(final InvocationOnMock invocation) throws Throwable {
                final EntityDB db = TestContext.getInstance().getDatabase();
                return db.find(SubscriptionType.class);
            }
            
        };
    }
}
