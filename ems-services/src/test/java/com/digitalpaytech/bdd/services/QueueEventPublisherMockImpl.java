package com.digitalpaytech.bdd.services;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.bdd.util.TestEventQueue;
import com.digitalpaytech.dto.queue.QueueEvent;

public final class QueueEventPublisherMockImpl {
    private QueueEventPublisherMockImpl() {
    }
    
    public static Answer<Integer> offer(final byte queueType) {
        return new Answer<Integer>() {
            
           
            @Override
            public Integer answer(final InvocationOnMock invocation) throws Throwable {
 
                final Object[] args = invocation.getArguments();
                final Object queueEvent = args[0];
                
                final TestEventQueue testEventQueue = TestDBMap.getTestEventQueueFromMem();
                if (queueEvent instanceof QueueEvent) {
                    testEventQueue.offerToQueue(copyQueueEvent((QueueEvent) queueEvent), queueType);
                } else {
                    testEventQueue.offerToQueue(queueEvent, queueType);
                }
                return null;
            }
            
            
            final QueueEvent copyQueueEvent(final QueueEvent queueEvent) {
                
                final QueueEvent returnEvent = new QueueEvent();
                
                returnEvent.setPointOfSaleId(queueEvent.getPointOfSaleId());
                returnEvent.setCustomerAlertTypeId(queueEvent.getCustomerAlertTypeId());
                returnEvent.setEventTypeId(queueEvent.getEventTypeId());
                returnEvent.setEventSeverityTypeId(queueEvent.getEventSeverityTypeId());
                returnEvent.setEventActionTypeId(queueEvent.getEventActionTypeId());
                returnEvent.setAlertInfo(queueEvent.getAlertInfo());
                returnEvent.setTimestampGMT(queueEvent.getTimestampGMT());
                returnEvent.setIsActive(queueEvent.getIsActive());
                returnEvent.setIsNotification(queueEvent.getIsNotification());
                returnEvent.setCreatedGMT(queueEvent.getCreatedGMT());
                returnEvent.setLastModifiedByUserId(queueEvent.getLastModifiedByUserId());
                
                return returnEvent;                
            }            
            
        };
        
    }
}
