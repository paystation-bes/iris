package com.digitalpaytech.bdd.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.domain.Customer;

@SuppressWarnings("checkstyle:illegalthrows")
public final class CustomerServiceMockImpl {
    
    private CustomerServiceMockImpl() {
    
    }
    
    public static Object findAllParentCustomersAnswer() {
        return null;
    }
    
    public static Answer<Customer> findCustomer() {
        return new Answer<Customer>() {
            @Override
            public Customer answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                Customer customer = (Customer) TestDBMap.findObjectByIdFromMemory((Integer) args[0], Customer.class);
                if (customer == null) {
                    final List<Customer> apList = TestContext.getInstance().getDatabase().find(Customer.class);                    
                    if (!apList.isEmpty()) {
                        customer = apList.get(0);
                    }                    
                }
                customer.setIsMigrated(true);
                return customer;
            }
        };
    }
    
    public static Answer<List<Customer>> findAllParentAndChildCustomersBySearchKeyword() {
        return new Answer<List<Customer>>() {
            @SuppressWarnings("unchecked")
            @Override
            public List<Customer> answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final String searchString = (String) args[0];
                return TestContext.getInstance().getDatabase().find(Customer.class, "name", searchString);
            }
        };
    }
    
    public static Answer<List<Customer>> findAllChildCustomers() {
        return new Answer<List<Customer>>() {
            @SuppressWarnings("unchecked")
            @Override
            public List<Customer> answer(final InvocationOnMock invocation) throws Throwable {
                final List<Customer> childCustomers = new ArrayList<Customer>();
                final Object[] args = invocation.getArguments();
                final Integer parentId = (Integer) args[0];
                for (Customer customer : (Collection<Customer>) TestDBMap.getTypeListFromMemory(Customer.class)) {
                    final Customer parentCustomer = customer.getParentCustomer();
                    final Integer pId = parentCustomer == null ? null : parentCustomer.getId();
                    if (pId != null && pId.equals(parentId)) {
                        childCustomers.add(customer);
                    }
                }
                return childCustomers;
            }
        };
    }
}
