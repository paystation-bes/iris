package com.digitalpaytech.bdd.services;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.domain.EventSeverityType;

public class EventSeverityTypeServiceMockImpl {
    @Deprecated
    public static Answer<EventSeverityType> findEventSeverityType() {
        return new Answer<EventSeverityType>() {
            
            @Override
            public EventSeverityType answer(final InvocationOnMock invocation) throws Throwable {
                final int eventSeverityTypeId = (int) invocation.getArguments()[0];
                return TestLookupTables.getEventSeverityList().get(eventSeverityTypeId);
            }
        };
    }
    
    public static Answer<EventSeverityType> findEventSeverityTypeMock() {
        return new Answer<EventSeverityType>() {
            @Override
            public EventSeverityType answer(final InvocationOnMock invocation) throws Throwable {
                final EventSeverityType type = new EventSeverityType();
                type.setName("Major");
                type.setId(2);
                return type;
            }
        };
    }
    
}
