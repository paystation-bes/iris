package com.digitalpaytech.bdd.services;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.util.StandardConstants;

@SuppressWarnings({"checkstyle:illegalthrows"})
public final class SystemAdminServiceMockImpl {
    
    private SystemAdminServiceMockImpl() {
    
    }
    
    public static Answer<Integer> createCustomer() {
        return new Answer<Integer>() {
            @Override
            public Integer answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final Customer customer = (Customer) args[0];
                final UserAccount userAccount = (UserAccount) args[1];
                userAccount.setId((int)(Math.random()*100));
                final String timeZone = (String) args[2];
                customer.setTimezoneId(TestLookupTables.findMatchingTZ(timeZone));
                customer.setId(new Double(Math.random() * StandardConstants.CONSTANT_100).intValue());
                if (customer.getParentCustomer() != null) {
                    customer.setParentCustomer((Customer) TestDBMap.findObjectByIdFromMemory(customer.getParentCustomer().getId(), Customer.class));
                }
                TestDBMap.addToMemory(customer.getId(), customer);
                TestDBMap.addToMemory(userAccount.getId(), userAccount);
                return customer.getId();
            }
        };
    }
}
