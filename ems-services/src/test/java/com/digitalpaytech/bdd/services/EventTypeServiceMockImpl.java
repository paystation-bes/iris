package com.digitalpaytech.bdd.services;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.domain.EventDeviceType;
import com.digitalpaytech.domain.EventType;

public class EventTypeServiceMockImpl {
    @Deprecated
    public static Answer<EventType> findEventTypeById() {
        return new Answer<EventType>() {
            
            @Override
            public EventType answer(final InvocationOnMock invocation) throws Throwable {
                return TestContext.getInstance().getDatabase().findById(EventType.class, invocation.getArgumentAt(0, Byte.class));
            }
        };
    }
    
    public static Answer<EventType> findEventTypeByIdMock() {
        return new Answer<EventType>() {
            @Override
            public EventType answer(final InvocationOnMock invocation) throws Throwable {
                final Byte b = invocation.getArgumentAt(0, Byte.class);
                final EventType type = new EventType();
                type.setDescription("Coin Changer Jam");
                type.setEventDeviceType(new EventDeviceType(6, "eventdevicetype.coinchanger", "CoinChanger"));
                return type;
            }
        };
    }
}
