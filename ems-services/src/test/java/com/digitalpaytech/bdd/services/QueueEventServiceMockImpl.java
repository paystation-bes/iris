package com.digitalpaytech.bdd.services;

import java.util.ArrayList;
import java.util.Date;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.bdd.util.TestEventQueue;
import com.digitalpaytech.domain.EventType;
import com.digitalpaytech.dto.queue.QueueEvent;
import com.digitalpaytech.util.WebCoreConstants;

public final class QueueEventServiceMockImpl {
    
    private QueueEventServiceMockImpl() {
    
    }
    
    public static Answer<Void> addQueueEventToQueue() {
        return new SomeAnswer() {
            public QueueEvent retrieveQueueEvent(final InvocationOnMock invocation) {
                QueueEvent qe = new QueueEvent();
                qe.setPointOfSaleId(invocation.getArgumentAt(0, Integer.class));
                qe.setCustomerAlertTypeId(invocation.getArgumentAt(2, Integer.class));
                qe.setEventTypeId(invocation.getArgumentAt(3, EventType.class).getId());
                qe.setEventSeverityTypeId(invocation.getArgumentAt(4, Integer.class));
                qe.setEventActionTypeId(invocation.getArgumentAt(5, Integer.class));
                qe.setAlertInfo(invocation.getArgumentAt(7, String.class));
                qe.setTimestampGMT(invocation.getArgumentAt(6, Date.class));
                qe.setIsActive(invocation.getArgumentAt(8, Boolean.class));
                qe.setIsNotification(invocation.getArgumentAt(9, Boolean.class));
                qe.setCreatedGMT(invocation.getArgumentAt(6, Date.class));
                qe.setLastModifiedByUserId(1);
                return qe;
            }
            
            public byte retrieveQueueType(final InvocationOnMock invocation) {
                return invocation.getArgumentAt(11, Byte.class);
            }
        };
    }
    
    public static Answer<Void> addItemToQueue() {
        return new SomeAnswer() {
            public QueueEvent retrieveQueueEvent(final InvocationOnMock invocation) {
                return invocation.getArgumentAt(0, QueueEvent.class);
            }
            
            public byte retrieveQueueType(final InvocationOnMock invocation) {
                return invocation.getArgumentAt(1, Byte.class);
            }
        };
    }
    
    static abstract class SomeAnswer implements Answer<Void> {
        public abstract QueueEvent retrieveQueueEvent(final InvocationOnMock invocation);
        
        public abstract byte retrieveQueueType(final InvocationOnMock invocation);
        
        @Override
        public Void answer(final InvocationOnMock invocation) throws Throwable {
            final String recentEventQueue = "recentEventQueue";
            final String historicalEventQueue = "historicalEventList";
            final Object[] args = invocation.getArguments();
            
            final QueueEvent queueEvent = retrieveQueueEvent(invocation);
            final byte queueType = retrieveQueueType(invocation);
            final ArrayList<QueueEvent> recentEventList = new ArrayList<QueueEvent>();
            final ArrayList<QueueEvent> historicalEventList = new ArrayList<QueueEvent>();
            
            final TestEventQueue teq = TestDBMap.getTestEventQueueFromMem();
            
            if ((queueType & WebCoreConstants.QUEUE_TYPE_RECENT_EVENT) == WebCoreConstants.QUEUE_TYPE_RECENT_EVENT) {
                recentEventList.add(queueEvent);
                teq.getQueue(WebCoreConstants.QUEUE_TYPE_RECENT_EVENT).add(queueEvent);
            }
            if ((queueType & WebCoreConstants.QUEUE_TYPE_HISTORICAL_EVENT) == WebCoreConstants.QUEUE_TYPE_HISTORICAL_EVENT) {
                // If severity is 1 and is a user defined alert it means a critical alert is cleared and reduced to minor
                // History and current status should be different. (current active with severity 1, history cleared with severity 0
                if (queueEvent.getCustomerAlertTypeId() != null && queueEvent.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_MINOR) {
                    queueEvent.setEventSeverityTypeId(WebCoreConstants.SEVERITY_CLEAR);
                    queueEvent.setIsActive(false);
                }
                
                final QueueEvent historicalQE = copyQueueEvent(queueEvent);
                historicalEventList.add(historicalQE);
                teq.getQueue(WebCoreConstants.QUEUE_TYPE_HISTORICAL_EVENT).add(historicalQE);
            }
            
            TestDBMap.addToMemory(recentEventQueue, recentEventList);
            TestDBMap.addToMemory(historicalEventQueue, historicalEventList);
            return null;
        }
        
        final QueueEvent copyQueueEvent(final QueueEvent queueEvent) {
            
            final QueueEvent returnEvent = new QueueEvent();
            
            returnEvent.setPointOfSaleId(queueEvent.getPointOfSaleId());
            returnEvent.setCustomerAlertTypeId(queueEvent.getCustomerAlertTypeId());
            returnEvent.setEventTypeId(queueEvent.getEventTypeId());
            returnEvent.setEventSeverityTypeId(queueEvent.getEventSeverityTypeId());
            returnEvent.setEventActionTypeId(queueEvent.getEventActionTypeId());
            returnEvent.setAlertInfo(queueEvent.getAlertInfo());
            returnEvent.setTimestampGMT(queueEvent.getTimestampGMT());
            returnEvent.setIsActive(queueEvent.getIsActive());
            returnEvent.setIsNotification(queueEvent.getIsNotification());
            returnEvent.setCreatedGMT(queueEvent.getCreatedGMT());
            returnEvent.setLastModifiedByUserId(queueEvent.getLastModifiedByUserId());
            
            return returnEvent;                
        }   
}
    
}

