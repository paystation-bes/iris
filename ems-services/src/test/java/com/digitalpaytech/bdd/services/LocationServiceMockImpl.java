package com.digitalpaytech.bdd.services;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.util.EntityMap;

@SuppressWarnings("checkstyle:illegalthrows")
public final class LocationServiceMockImpl {
    
    private LocationServiceMockImpl() {
        
    }
     
    @Deprecated
    public static Answer<Location> findLocationById() {
        return new Answer<Location>() {
            
            @Override
            public Location answer(final InvocationOnMock invocation) throws Throwable {
                final int locationId = (int) invocation.getArguments()[0];
                return (Location) TestDBMap.findObjectByIdFromMemory(locationId, Location.class);
            }
            
        };
    }
    
    public static Answer<Location> findLocationByIdMock() {
        return new Answer<Location>() {
            @Override
            public Location answer(final InvocationOnMock invocation) throws Throwable {
                return EntityMap.INSTANCE.findDefaultLocation();
            }
        };
    }
}
