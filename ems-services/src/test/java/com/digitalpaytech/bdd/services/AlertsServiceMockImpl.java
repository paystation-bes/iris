package com.digitalpaytech.bdd.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.TestConstants;
import com.digitalpaytech.domain.AlertClassType;
import com.digitalpaytech.domain.AlertThresholdType;
import com.digitalpaytech.domain.AlertType;
import com.digitalpaytech.util.WebCoreConstants;

@SuppressWarnings("checkstyle:illegalthrows")
public final class AlertsServiceMockImpl {
    private static final String STRING_UNSETTLED_CREDIT_CARDS = "Unsettled Credit Cards";
    private static final String STRING_PAY_STATION_ALERT = "Pay Station Alert";
    private static final String STRING_BILL_STACKER = "Bill Stacker";
    private static final String STRING_COLLECTION = "Collection";
    
    private AlertsServiceMockImpl() {
    
    }
    
    public static Answer<List<AlertThresholdType>> findAlertThresholdTypeByAlertClassTypeId() {
        return new Answer<List<AlertThresholdType>>() {
            @Override
            public List<AlertThresholdType> answer(final InvocationOnMock invocation) throws Throwable {
                final AlertClassType lastSeenACT =
                        new AlertClassType(WebCoreConstants.ALERT_CLASS_TYPE_COMMUNICATION, "Communication", new Date(), 1);
                final AlertClassType collectionACT =
                        new AlertClassType(WebCoreConstants.ALERT_CLASS_TYPE_COLLECTION, STRING_COLLECTION, new Date(), 1);
                final AlertClassType payStationACT =
                        new AlertClassType(WebCoreConstants.ALERT_CLASS_TYPE_PAY_STATION_ALERT, STRING_PAY_STATION_ALERT, new Date(), 1);
                        
                final AlertType lastSeenAT =
                        new AlertType(WebCoreConstants.ALERTTYPE_ID_LASTSEENINTERVAL, lastSeenACT, "Last Seen Interval", true, new Date(), 1);
                        
                final AlertType runningTotalAt =
                        new AlertType(WebCoreConstants.ALERTTYPE_ID_RUNNINGTOTAL, collectionACT, "Running Total", true, new Date(), 1);
                final AlertType coinCanAT =
                        new AlertType(WebCoreConstants.ALERTTYPE_ID_COINCANNISTER, collectionACT, TestConstants.COIN_CAN, true, new Date(), 1);
                final AlertType billCanAt =
                        new AlertType(WebCoreConstants.ALERTTYPE_ID_BILLSTACKER, collectionACT, STRING_BILL_STACKER, true, new Date(), 1);
                final AlertType unsettledCardAT = new AlertType(WebCoreConstants.ALERTTYPE_ID_UNSETTLEDCREDITCARD, collectionACT,
                        STRING_UNSETTLED_CREDIT_CARDS, true, new Date(), 1);
                final AlertType payStationAT =
                        new AlertType(WebCoreConstants.ALERTTYPE_ID_PAYSTATIONALERT, payStationACT, STRING_PAY_STATION_ALERT, true, new Date(), 1);
                final AlertType overdueCollectionAT = new AlertType(WebCoreConstants.ALERTTYPE_ID_LASTSEENCOLLECTION, collectionACT,
                        "Last Collection Interval", true, new Date(), 1);
                final List<AlertThresholdType> allThresholdTypes = new ArrayList<AlertThresholdType>();
                
                allThresholdTypes.add(new AlertThresholdType(1, lastSeenAT, "Last Seen Interval Hour", new Date(), 1));
                
                allThresholdTypes.add(new AlertThresholdType(2, runningTotalAt, "Running Total Dollar", new Date(), 1));
                
                allThresholdTypes.add(new AlertThresholdType(WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_COUNT, coinCanAT,
                        TestConstants.COIN_CAN_COUNT, new Date(), 1));
                        
                allThresholdTypes.add(new AlertThresholdType(WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_DOLLARS, coinCanAT,
                        "Coin Canister Dollar", new Date(), 1));
                        
                allThresholdTypes.add(new AlertThresholdType(WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_COUNT, billCanAt,
                        "Bill Stacker Count", new Date(), 1));
                        
                allThresholdTypes.add(new AlertThresholdType(WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_DOLLARS, billCanAt,
                        "Bill Stacker Dollar", new Date(), 1));
                        
                allThresholdTypes.add(new AlertThresholdType(WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_COUNT, unsettledCardAT,
                        "Unsettled Credit Card Count", new Date(), 1));
                        
                allThresholdTypes.add(new AlertThresholdType(WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_DOLLARS, unsettledCardAT,
                        "Unsettled Credit Card Dollar", new Date(), 1));
                        
                allThresholdTypes.add(new AlertThresholdType(WebCoreConstants.ALERT_THRESHOLD_TYPE_PAYSTATION, payStationAT, STRING_PAY_STATION_ALERT,
                        new Date(), 1));
                        
                allThresholdTypes.add(new AlertThresholdType(WebCoreConstants.ALERT_THRESHOLD_TYPE_OVERDUE_COLLECTION, overdueCollectionAT,
                        "Collection Overdue", new Date(), 1));
                        
                final Object[] args = invocation.getArguments();
                final byte alertClassTypeId = (byte) args[0];
                final List<AlertThresholdType> returnList = new ArrayList<AlertThresholdType>();
                
                for (AlertThresholdType thresholdType : allThresholdTypes) {
                    if (thresholdType.getAlertType().getAlertClassType().getId() == alertClassTypeId) {
                        returnList.add(thresholdType);
                    }
                }
                
                return returnList;
            }
        };
    }
}
