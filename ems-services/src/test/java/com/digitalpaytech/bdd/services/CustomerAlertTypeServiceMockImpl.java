package com.digitalpaytech.bdd.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.EntityDB;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerAlertEmail;
import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.util.EntityMap;

public final class CustomerAlertTypeServiceMockImpl {
    
    private CustomerAlertTypeServiceMockImpl() {
    }
    
    @SuppressWarnings("checkstyle:magicnumber")
    public static Answer<CustomerAlertType> findDefaultCustomerAlertTypeByCustomerIdAndAlertThresholdTypeId() {
        return new Answer<CustomerAlertType>() {
            @SuppressWarnings("unchecked")
            @Override
            public CustomerAlertType answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final Customer customer = (Customer) TestDBMap.findObjectByIdFromMemory(args[0], Customer.class);
                final Collection<CustomerAlertType> catList =
                        (Collection<CustomerAlertType>) TestDBMap.getTypeListFromMemory(CustomerAlertType.class);
                final Integer alertThresholdId = (Integer) args[1];
                final Iterator<CustomerAlertType> itr = catList.iterator();
                
                while (itr.hasNext()) {
                    final CustomerAlertType cat = itr.next();
                    if (cat.getAlertThresholdType().getId() == alertThresholdId && cat.isIsDefault()
                        && cat.getCustomer().getId() == customer.getId()) {
                        return cat;
                    }
                    
                }
                return null;
            }
        };
    }
    
    @Deprecated
    public static Answer<List<CustomerAlertType>> findCustomerAlertTypeByIdList() {
        return new Answer<List<CustomerAlertType>>() {
            @Override
            public List<CustomerAlertType> answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                @SuppressWarnings("unchecked")
                final List<Integer> idList = (List<Integer>) args[0];
                final List<CustomerAlertType> catList = new ArrayList<CustomerAlertType>();
                final EntityDB db = TestContext.getInstance().getDatabase();
                for (Integer id : idList) {
                    catList.add(db.findById(CustomerAlertType.class, id));
                }
                return catList;
            }
        };
    }
    
    public static Answer<List<CustomerAlertType>> findCustomerAlertTypeByIdListMock() {
        return new Answer<List<CustomerAlertType>>() {
            @Override
            public List<CustomerAlertType> answer(final InvocationOnMock invocation) throws Throwable {
                return EntityMap.INSTANCE.findDefaultCustomerAlertTypes();
            }
        };
    }
    
    @Deprecated
    public static Answer<CustomerAlertType> findCustomerAlertTypeById() {
        return new Answer<CustomerAlertType>() {
            @Override
            public CustomerAlertType answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final Integer id = (Integer) args[0];
                final EntityDB db = TestContext.getInstance().getDatabase();
                final CustomerAlertType cat = db.findById(CustomerAlertType.class, id);
                return cat;
            }
        };
    }

    public static Answer<CustomerAlertType> findCustomerAlertTypeByIdMock() {
        return new Answer<CustomerAlertType>() {
            @Override
            public CustomerAlertType answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final Integer id = (Integer) args[0];
                final List<CustomerAlertType> types = EntityMap.INSTANCE.findDefaultCustomerAlertTypes();
                if (id == 1 || id == 2) {
                    return types.get(0);
                } else {
                    final CustomerAlertType t = types.get(2);
                    t.getAlertThresholdType().setId(5);
                    return t;
                }
            }
        };
    }
                
    @Deprecated
    public static Answer<Collection<CustomerAlertType>> findCustomerAlertTypesByCustomerIdAlertThresholdTypeIdAndIsActive() {
        return new Answer<Collection<CustomerAlertType>>() {
            @SuppressWarnings("unchecked")
            @Override
            public Collection<CustomerAlertType> answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final int alertThresholdTypeId = ((Integer) args[1]).intValue();
                final List<CustomerAlertType> catList = new ArrayList<CustomerAlertType>();
                
                final List<CustomerAlertType> allCatList = TestContext.getInstance().getDatabase().find(CustomerAlertType.class, "customer.id", args[0]);
                for (CustomerAlertType customerAlertType : allCatList) {
                    if (customerAlertType.getAlertThresholdType() != null
                        && alertThresholdTypeId == customerAlertType.getAlertThresholdType().getId()) {
                        catList.add(customerAlertType);
                    }
                    
                }
                return catList;
            }
        };
    }
    
    public static Answer<Collection<CustomerAlertType>> findCustomerAlertTypesByCustomerIdAlertThresholdTypeIdAndIsActiveMock() {
        return new Answer<Collection<CustomerAlertType>>() {
            @SuppressWarnings("unchecked")
            @Override
            public Collection<CustomerAlertType> answer(final InvocationOnMock invocation) throws Throwable {
                return EntityMap.INSTANCE.findDefaultCustomerAlertTypes();
            }
        };
    }
    
    @Deprecated
    public static Answer<List<CustomerAlertEmail>> findCustomerAlertEmailByCustomerAlertTypeId() {
        return new Answer<List<CustomerAlertEmail>>() {
            @Override
            public List<CustomerAlertEmail> answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final Integer id = (Integer) args[0];
                final EntityDB db = TestContext.getInstance().getDatabase();
                final CustomerAlertType cat = db.findById(CustomerAlertType.class, id);
                return new ArrayList<CustomerAlertEmail>(cat.getCustomerAlertEmails());
            }
        };
        
    }
    
    public static Answer<List<CustomerAlertEmail>> findCustomerAlertEmailByCustomerAlertTypeIdMock() {
        return new Answer<List<CustomerAlertEmail>>() {
            @Override
            public List<CustomerAlertEmail> answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final Integer id = (Integer) args[0];
                final List<CustomerAlertEmail> emails = new ArrayList<>();
                final List<CustomerAlertType> list = EntityMap.INSTANCE.findDefaultCustomerAlertTypes();
                for (CustomerAlertType custAlert : list) {
                    if (custAlert.getId() == id) {
                        emails.addAll(new ArrayList<CustomerAlertEmail>(custAlert.getCustomerAlertEmails()));
                    }
                }
                return emails;
            }
        };
    }
}
