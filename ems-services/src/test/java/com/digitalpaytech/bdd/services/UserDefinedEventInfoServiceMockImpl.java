package com.digitalpaytech.bdd.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.EntityDB;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.data.UserDefinedEventInfo;
import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.EventType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosBalance;
import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.domain.PosHeartbeat;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.EntityMap;

@SuppressWarnings({ "PMD.GodClass" })
public final class UserDefinedEventInfoServiceMockImpl {
    
    private static final String CLEAR_STRING = "clear";
    
    private UserDefinedEventInfoServiceMockImpl() {
    }
    
    @Deprecated
    public static Answer<List<UserDefinedEventInfo>> findEventsByNamedQueryAndPointOfSaleIdList() {
        return new Answer<List<UserDefinedEventInfo>>() {
            @SuppressWarnings("unchecked")
            @Override
            public List<UserDefinedEventInfo> answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final String query = (String) args[0];
                final List<Integer> posIdList = (List<Integer>) args[1];
                final List<UserDefinedEventInfo> infoList = new ArrayList<UserDefinedEventInfo>();
                final Collection<PosEventCurrent> pecList = (Collection<PosEventCurrent>) TestDBMap.getTypeListFromMemory(PosEventCurrent.class);
                final Iterator<PosEventCurrent> itr = pecList.iterator();
                final EntityDB db = TestContext.getInstance().getDatabase();
                while (itr.hasNext()) {
                    final PosEventCurrent pec = itr.next();
                    if (pec.getEventActionType() == null) {
                        pec.setEventType(TestContext.getInstance().getDatabase().findById(EventType.class, WebCoreConstants.EVENT_TYPE_CUSTOMERDEFINED_ALERT));
                    }
                    final CustomerAlertType cat = pec.getCustomerAlertType();
                    if (query.contains(String.valueOf(cat.getAlertThresholdType().getId())) && posIdList.contains(pec.getPointOfSale().getId())) {
                        final PosBalance posBal = db.findById(PosBalance.class, pec.getPointOfSale().getId());
                        final PosHeartbeat posHeartbeat = db.findById(PosHeartbeat.class, pec.getPointOfSale().getId());
                        if (posBal != null || posHeartbeat != null) {
                            addToInfoList(cat, posBal, posHeartbeat, query, pec, infoList);
                        }
                    }
                }
                return infoList;
            }
        };
    }
    
    public static Answer<List<UserDefinedEventInfo>> findEventsByNamedQueryAndPointOfSaleIdListMock() {
        return new Answer<List<UserDefinedEventInfo>>() {
            @SuppressWarnings("unchecked")
            @Override
            public List<UserDefinedEventInfo> answer(final InvocationOnMock invocation) throws Throwable {
                final List<UserDefinedEventInfo> infoList = new ArrayList<UserDefinedEventInfo>();
                final PosEventCurrent pec = new PosEventCurrent();
                pec.setEventType(new EventType(WebCoreConstants.EVENT_TYPE_CUSTOMERDEFINED_ALERT));
                final CustomerAlertType cat = EntityMap.INSTANCE.findDefaultCustomerAlertTypes().get(0);
                final PosBalance posBal = new PosBalance();
                posBal.setPointOfSale(EntityMap.INSTANCE.findDefaultPointOfSales(2).get(1));
                posBal.setBillCount((short) 0);
                posBal.setCoinCount((short) 2001);
                
                final GregorianCalendar cal = new GregorianCalendar();
                cal.add(Calendar.MINUTE, -5);
                final PosHeartbeat posHeartbeat = new PosHeartbeat();
                posHeartbeat.setPointOfSale(EntityMap.INSTANCE.findDefaultPointOfSale());
                posHeartbeat.setLastHeartbeatGmt(cal.getTime());
                addToInfoList(cat, posBal, posHeartbeat, CLEAR_STRING, pec, infoList);
                return infoList;
            }
        };
    }


    @Deprecated
    public static Answer<List<UserDefinedEventInfo>> findEventsByNamedQueryAndPointOfSaleIdListAndCustomerAlertTypeId() {
        return new Answer<List<UserDefinedEventInfo>>() {
            @SuppressWarnings("unchecked")
            @Override
            public List<UserDefinedEventInfo> answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final String query = (String) args[0];
                final Integer catId = (Integer) args[2];
                final List<UserDefinedEventInfo> infoList = new ArrayList<UserDefinedEventInfo>();
                final CustomerAlertType cat = (CustomerAlertType) TestDBMap.findObjectByIdFromMemory(catId, CustomerAlertType.class);
                final Collection<PosEventCurrent> pecList = (Collection<PosEventCurrent>) TestDBMap.getTypeListFromMemory(PosEventCurrent.class);
                final Iterator<PosEventCurrent> itr = pecList.iterator();
                final EntityDB db = TestContext.getInstance().getDatabase();
                while (itr.hasNext()) {
                    final PosEventCurrent pec = itr.next();
                    if (pec.getEventActionType() == null) {
                        pec.setEventType(TestContext.getInstance().getDatabase().findById(EventType.class, WebCoreConstants.EVENT_TYPE_CUSTOMERDEFINED_ALERT));
                    }
                    if (pec.getCustomerAlertType().getId() == catId) {
                        final PosBalance posBal = db.findById(PosBalance.class, pec.getPointOfSale().getId());
                        final PosHeartbeat posHeartbeat = db.findById(PosHeartbeat.class, pec.getPointOfSale().getId());
                        if (posBal != null || posHeartbeat != null) {
                            addToInfoList(cat, posBal, posHeartbeat, query, pec, infoList);
                            
                        }
                    }
                }
                return infoList;
            }
        };
    }
    
    public static Answer<List<UserDefinedEventInfo>> findEventsByNamedQueryAndPointOfSaleIdListAndCustomerAlertTypeIdMock() {
        return new Answer<List<UserDefinedEventInfo>>() {
            @SuppressWarnings("unchecked")
            @Override
            public List<UserDefinedEventInfo> answer(final InvocationOnMock invocation) throws Throwable {
                final List<UserDefinedEventInfo> infoList = new ArrayList<UserDefinedEventInfo>();
                final PosEventCurrent pec = new PosEventCurrent();
                pec.setEventType(new EventType(WebCoreConstants.EVENT_TYPE_CUSTOMERDEFINED_ALERT));
                pec.setPointOfSale(new PointOfSale(1));
                final CustomerAlertType cat = EntityMap.INSTANCE.findDefaultCustomerAlertTypes().get(0);
                final PosBalance posBal = new PosBalance();
                posBal.setPointOfSale(EntityMap.INSTANCE.findDefaultPointOfSales(2).get(0));
                posBal.setBillCount((short) 0);
                posBal.setCoinCount((short) 80);
                
                final GregorianCalendar cal = new GregorianCalendar();
                cal.add(Calendar.MINUTE, -5);
                final PosHeartbeat posHeartbeat = new PosHeartbeat();
                posHeartbeat.setPointOfSale(EntityMap.INSTANCE.findDefaultPointOfSale());
                posHeartbeat.setLastHeartbeatGmt(cal.getTime());
                addToInfoList(cat, posBal, posHeartbeat, CLEAR_STRING, pec, infoList);
                return infoList;
            }
        };
    }

    @Deprecated
    @SuppressWarnings("unchecked")
    public static Answer<List<UserDefinedEventInfo>> findEventsClassTypeAndLimit() {
        return new Answer<List<UserDefinedEventInfo>>() {
            @Override
            public List<UserDefinedEventInfo> answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final Class<?> classType = (Class<?>) args[0];
                final String className = classType.getSimpleName();
                
                final Collection<PosEventCurrent> pecList = (Collection<PosEventCurrent>) TestDBMap.getTypeListFromMemory(PosEventCurrent.class);
                final List<UserDefinedEventInfo> infoList = new ArrayList<UserDefinedEventInfo>();
                final Iterator<PosEventCurrent> itr = pecList.iterator();
                final EntityDB db = TestContext.getInstance().getDatabase();
                while (itr.hasNext()) {
                    final PosEventCurrent pec = itr.next();
                    if (pec.getCustomerAlertType().getAlertThresholdType().getId() == 1) {
                        final PosHeartbeat posHeartbeat = db.findById(PosHeartbeat.class, pec.getPointOfSale().getId());
                        
                        if (posHeartbeat != null) {
                            addToInfoList(pec.getCustomerAlertType(), null, posHeartbeat, className, pec, infoList);
                        }
                    }
                }
                return infoList;
            }
        };
    }

    @SuppressWarnings("checkstyle:cyclomaticcomplexity")
    private static void addToInfoList(final CustomerAlertType cat, final PosBalance posBal, final PosHeartbeat posHeartbeat, final String query,
        final PosEventCurrent pec, final List<UserDefinedEventInfo> infoList) {
        final boolean isClear = query.toLowerCase(WebCoreConstants.DEFAULT_LOCALE).contains(CLEAR_STRING);
        if (cat.getAlertThresholdType().getId() == 1) {
            final boolean isStatusActive = posHeartbeat.getLastHeartbeatGmt().before(new Date(DateUtil.getCurrentGmtDate().getTime()
                                                                                              - StandardConstants.HOURS_IN_MILLIS_1
                                                                                              * new Float(cat.getThreshold()).intValue()));
            final UserDefinedEventInfo udei = buildUdeiForCommunication(cat, pec, isStatusActive);
            if (isClear && !isStatusActive || !isClear && isStatusActive) {
                infoList.add(udei);
            }
        } else {
            final Float target = getEventTarget(cat, posBal);
            final UserDefinedEventInfo udei = buildUdei(target / cat.getThreshold(), cat, pec);
            final boolean clearConditions = pec.isIsActive()
                                            && (target <= cat.getThreshold() * WebCoreConstants.ALERT_THRESHOLD_0_75 || pec.getEventSeverityType()
                                                    .getId() >= WebCoreConstants.SEVERITY_CRITICAL && target <= cat.getThreshold());
            final boolean triggerConditions = !pec.isIsActive() && target > cat.getThreshold() * WebCoreConstants.ALERT_THRESHOLD_0_75
                                              || pec.isIsActive() && pec.getEventSeverityType().getId() < WebCoreConstants.SEVERITY_CRITICAL
                                              && target > cat.getThreshold();
            
            if (isClear && clearConditions || !isClear && triggerConditions) {
                infoList.add(udei);
            }
        }
    }
    
    private static Float getEventTarget(final CustomerAlertType cat, final PosBalance posBal) {
        Float target = 0F;
        switch (cat.getAlertThresholdType().getId()) {
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_COUNT:
                target = Float.valueOf(posBal.getCoinCount());
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_DOLLARS:
                target = Float.valueOf(posBal.getCoinAmount());
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_COUNT:
                target = Float.valueOf(posBal.getBillCount());
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_DOLLARS:
                target = Float.valueOf(posBal.getBillAmount());
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_RUNNING_TOTAL_DOLLAR:
                target = Float.valueOf(posBal.getTotalAmount());
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_COUNT:
                target = Float.valueOf(posBal.getUnsettledCreditCardCount());
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_DOLLARS:
                target = Float.valueOf(posBal.getUnsettledCreditCardAmount());
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_OVERDUE_COLLECTION:
                target = Float.valueOf(posBal.getLastCollectionGmt().getTime());
                break;
            default:
                break;
        }
        return target;
    }
    
    private static UserDefinedEventInfo buildUdei(final Float severity, final CustomerAlertType cat, final PosEventCurrent pec) {
        final UserDefinedEventInfo udei = new UserDefinedEventInfo();
        if (severity > 1) {
            udei.setGapToCritical(-1);
            udei.setGapToMajor(-1);
            udei.setGapToMinor(-1);
        } else if (severity > WebCoreConstants.ALERT_THRESHOLD_0_75) {
            udei.setGapToCritical(0);
            udei.setGapToMajor(0);
            udei.setGapToMinor(-1);
        } else {
            udei.setGapToCritical(0);
            udei.setGapToMajor(0);
            udei.setGapToMinor(0);
        }
        udei.setPosEventCurrent(pec);
        udei.setCustomerAlertType(cat);
        udei.setPointOfSaleId(pec.getPointOfSale().getId());
        return udei;
        
    }
    
    private static UserDefinedEventInfo buildUdeiForCommunication(final CustomerAlertType cat, final PosEventCurrent pec, final boolean isActive) {
        final UserDefinedEventInfo udei = new UserDefinedEventInfo();
        if (isActive) {
            udei.setGapToCritical(0);
            udei.setGapToMajor(-1);
            udei.setGapToMinor(0);
        } else {
            udei.setGapToCritical(0);
            udei.setGapToMajor(0);
            udei.setGapToMinor(0);
        }
        udei.setPosEventCurrent(pec);
        udei.setCustomerAlertType(cat);
        udei.setPointOfSaleId(pec.getPointOfSale().getId());
        return udei;
        
    }
    
}
