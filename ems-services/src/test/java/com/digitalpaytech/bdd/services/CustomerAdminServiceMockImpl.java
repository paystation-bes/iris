package com.digitalpaytech.bdd.services;

import java.util.Date;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerPropertyType;
import com.digitalpaytech.util.WebCoreConstants;

@SuppressWarnings({"checkstyle:illegalthrows"})
public final class CustomerAdminServiceMockImpl {
    
    private CustomerAdminServiceMockImpl() {
    
    }
    
    public static Answer<CustomerProperty> createDummyCustomerProperty() {
        return new Answer<CustomerProperty>() {
            @Override
            public CustomerProperty answer(final InvocationOnMock invocation) throws Throwable {
                return new CustomerProperty(new CustomerPropertyType(), new Customer(), WebCoreConstants.SYSTEM_ADMIN_UI_TIMEZONE, new Date(), 0);
            }
        };
    }
    
}
