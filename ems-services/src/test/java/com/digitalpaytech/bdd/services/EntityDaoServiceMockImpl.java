package com.digitalpaytech.bdd.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.hibernate.SQLQuery;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.EntityDB;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.domain.EventDeviceType;
import com.digitalpaytech.domain.PosEventCurrent;

@SuppressWarnings({ "checkstyle:illegalthrows" })
public final class EntityDaoServiceMockImpl {
    
    private EntityDaoServiceMockImpl() {
    }
    
    public static Answer<SQLQuery> switchSQLWithMock(final SQLQuery sqlQuery) {
        return new Answer<SQLQuery>() {
            @Override
            public SQLQuery answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final ControllerFieldsWrapper controllerFieldsWrapper = TestDBMap.getControllerFieldsWrapperFromMem();
                final List<String> dataList = controllerFieldsWrapper.getDataList();
                dataList.add((String) args[0]);
                controllerFieldsWrapper.setDataList(dataList);
                TestDBMap.saveControllerFieldsWrapperToMem(controllerFieldsWrapper);
                return sqlQuery;
            }
        };
    }
    
    public static Answer<List<EventDeviceType>> loadAll(final Class<?> className) {
        if ("EventDeviceType".equals(className.getSimpleName())) {
            return new Answer<List<EventDeviceType>>() {
                
                @Override
                public List<EventDeviceType> answer(final InvocationOnMock invocation) throws Throwable {
                    EntityDB db = TestContext.getInstance().getDatabase();
                    return db.find(EventDeviceType.class);
                }
                
            };
        }
        return null;
    }
    
    public static Answer<Integer> delete() {
        return new Answer<Integer>() {
            @SuppressWarnings("unchecked")
            @Override
            public Integer answer(final InvocationOnMock invocation) throws Throwable {
                final PosEventCurrent pec = (PosEventCurrent) invocation.getArguments()[0];
                PosEventCurrent pecToDelete = null;
                final Collection<PosEventCurrent> pecList = (Collection<PosEventCurrent>) TestDBMap.getTypeListFromMemory(PosEventCurrent.class);
                final Iterator<PosEventCurrent> pecItr = pecList.iterator();
                while (pecItr.hasNext()) {
                    final PosEventCurrent curPec = pecItr.next();
                    final Boolean posMatch = curPec.getPointOfSale().getId() == pec.getPointOfSale().getId();
                    final Boolean catMatch = curPec.getCustomerAlertType() == pec.getCustomerAlertType();
                    if (posMatch && catMatch) {
                        pecToDelete = curPec;
                        break;
                    }
                }
                TestDBMap.deleteFromMemory(pecToDelete.getId(), PosEventCurrent.class);
                return null;
            }
        };
    }
    
    public static Answer<List<PosEventCurrent>> scrollAlerts() {
        return new Answer<List<PosEventCurrent>>() {
            @SuppressWarnings("unchecked")
            @Override
            public List<PosEventCurrent> answer(final InvocationOnMock invocation) throws Throwable {
                final List<PosEventCurrent> candiateList = new ArrayList<PosEventCurrent>();
                final Object[] args = invocation.getArguments();
                final Object[] hibArgs = (Object[]) args[2];
                final Collection<Integer> posIds = (Collection<Integer>) hibArgs[0];
                final Collection<Integer> catIds = (Collection<Integer>) hibArgs[1];
                final Collection<PosEventCurrent> pecList = (Collection<PosEventCurrent>) TestDBMap.getTypeListFromMemory(PosEventCurrent.class);
                final Iterator<PosEventCurrent> pecItr = pecList.iterator();
                while (pecItr.hasNext()) {
                    final PosEventCurrent pec = pecItr.next();
                    if (posIds.contains(pec.getPointOfSale().getId()) && catIds.contains(pec.getCustomerAlertType().getId())) {
                        candiateList.add(pec);
                    }
                    
                }
                return candiateList;
                
            }
        };
    }
    
}
