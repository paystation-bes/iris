package com.digitalpaytech.bdd.services;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.domain.ServiceAgreement;

public final class ServiceAgreementServiceMockImpl {
    private ServiceAgreementServiceMockImpl() {
    }
    
    public static Answer<ServiceAgreement> findLatestServiceAgreementByCustomerId(final int customerId) {
        return new Answer<ServiceAgreement>() {
            @Override
            public ServiceAgreement answer(final InvocationOnMock invocation) throws Throwable {
                return new ServiceAgreement();
            }
        };
    }
    
}
