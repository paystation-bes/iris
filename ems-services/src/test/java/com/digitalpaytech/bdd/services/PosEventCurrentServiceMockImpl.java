package com.digitalpaytech.bdd.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.EntityDB;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.bdd.util.expression.StoryObjectBuilder;
import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.EventType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.util.EntityMap;
import com.digitalpaytech.util.ObjectUtils;
import com.digitalpaytech.bdd.services.PosEventCurrentServiceMockImpl;

public final class PosEventCurrentServiceMockImpl {
    private PosEventCurrentServiceMockImpl() {
    }
    
    public static Answer<PosEventCurrent> findPosEventCurrentByPointOfSaleIdAndEventTypeIdAndCustomerAlertTypeId() {
        return new Answer<PosEventCurrent>() {
            @Override
            public PosEventCurrent answer(final InvocationOnMock invocation) throws Throwable {
                final Integer posId = invocation.getArgumentAt(0, Integer.class);
                final byte eventTypeId = invocation.getArgumentAt(1, byte.class);
                final Integer customerAlertTypeId = invocation.getArgumentAt(2, Integer.class);
                
                PosEventCurrent result = null;
                PosEventCurrent candidate;
                @SuppressWarnings("unchecked")
                final Collection<PosEventCurrent> pecList = (Collection<PosEventCurrent>) TestDBMap.getTypeListFromMemory(PosEventCurrent.class);
                if (pecList != null && !pecList.isEmpty()) {
                    final Iterator<PosEventCurrent> pecItr = pecList.iterator();
                    while (result == null && pecItr.hasNext()) {
                        candidate = pecItr.next();
                        if (posId.equals(candidate.getPointOfSale().getId())
                            && eventTypeId == candidate.getEventType().getId()
                            && (candidate.getCustomerAlertType() == null || ObjectUtils.nullableEquals(customerAlertTypeId, candidate
                                    .getCustomerAlertType().getId()))) {
                            result = candidate;
                        }
                    }
                }
                
                return result;
            }
        };
    }
    
    @Deprecated
    public static Answer<List<PosEventCurrent>> findActiveDefaultPosEventCurrentByPosIdListAndAttIdList() {
        return new Answer<List<PosEventCurrent>>() {
            @SuppressWarnings("unchecked")
            @Override
            public List<PosEventCurrent> answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final List<Integer> posIdList = (List<Integer>) args[0];
                final List<Integer> alertThresholdTypeIdList = (List<Integer>) args[1];
                final List<PosEventCurrent> candidateList = new ArrayList<PosEventCurrent>();
                final Collection<PosEventCurrent> pecList = (Collection<PosEventCurrent>) TestDBMap.getTypeListFromMemory(PosEventCurrent.class);
                for (PosEventCurrent pec : pecList) {
                    if (pec.isIsActive() && pec.getCustomerAlertType().isIsDefault()
                        && alertThresholdTypeIdList.contains(pec.getCustomerAlertType().getAlertThresholdType().getId())
                        && posIdList.contains(pec.getPointOfSale().getId())) {
                        candidateList.add(pec);
                    }
                    
                }
                return candidateList;
            }
        };
        
    }
    
    public static Answer<List<PosEventCurrent>> findActivePosEventCurrentByPosIdListAndAttIdListMock() {
        return new Answer<List<PosEventCurrent>>() {
            @SuppressWarnings("unchecked")
            @Override
            public List<PosEventCurrent> answer(final InvocationOnMock invocation) throws Throwable {
                final List<Integer> alertThresholdTypeIdList = new ArrayList<>();
                alertThresholdTypeIdList.add(100);
                alertThresholdTypeIdList.add(2000);
                
                final PosEventCurrent curr1 = new PosEventCurrent();
//                curr1.setAlertGmt(alertGmt);
//                curr1.setAlertInfo(alertInfo);
                
                final CustomerAlertType cat1 = EntityMap.INSTANCE.findDefaultCustomerAlertTypes().get(0);
                final PointOfSale pos = EntityMap.INSTANCE.findDefaultPointOfSale();
                curr1.setCustomerAlertType(cat1);
                curr1.setEventSeverityType(new EventSeverityType(3, "Critical"));
//                curr1.setEventType(eventType);
//              curr1.setEventActionType(eventActionType);                
                curr1.setIsActive(true);
                curr1.setPointOfSale(pos);
                curr1.setIsHidden(false);
                
                final List<PosEventCurrent> candidateList = new ArrayList<PosEventCurrent>();
                candidateList.add(curr1);
                return candidateList;
            }
        };
    }

    @Deprecated
    public static Answer<List<PosEventCurrent>> findActivePosEventCurrentByTimestamp() {
        return new Answer<List<PosEventCurrent>>() {
            @SuppressWarnings("unchecked")
            @Override
            public List<PosEventCurrent> answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final Date yesterday = (Date) args[0];
                final List<PosEventCurrent> candidateList = new ArrayList<PosEventCurrent>();
                final Collection<PosEventCurrent> pecList = (Collection<PosEventCurrent>) TestDBMap.getTypeListFromMemory(PosEventCurrent.class);
                for (PosEventCurrent pec : pecList) {
                    if (pec.getAlertGmt().before(yesterday) && pec.getClearedGmt() == null) {
                        candidateList.add(pec);
                    }
                    
                }
                return candidateList;
            }
        };
        
    }

    
    public static Answer<List<PosEventCurrent>> findActivePosEventCurrentByTimestampMock() {
        return new Answer<List<PosEventCurrent>>() {
            @SuppressWarnings("unchecked")
            @Override
            public List<PosEventCurrent> answer(final InvocationOnMock invocation) throws Throwable {
                final GregorianCalendar cal = new GregorianCalendar();
                cal.add(Calendar.HOUR, -25);

                final List<PosEventCurrent> candidateList = new ArrayList<PosEventCurrent>();
                candidateList.add(createPosEventCurrent(cal.getTime(), "alert info 1", EntityMap.INSTANCE.findDefaultPointOfSale()));
                
                cal.add(Calendar.HOUR, -5);
                candidateList.add(createPosEventCurrent(cal.getTime(), "alert info 2", EntityMap.INSTANCE.findDefaultPointOfSales(2).get(1)));
                
                cal.add(Calendar.MINUTE, -30);
                candidateList.add(createPosEventCurrent(cal.getTime(), "alert info 3", EntityMap.INSTANCE.findDefaultPointOfSales(3).get(2)));
                
                return candidateList;
            }
        };
    }

    private static PosEventCurrent createPosEventCurrent(final Date alertGmt, final String alertInfo, final PointOfSale pos) {
        final List<Integer> alertThresholdTypeIdList = new ArrayList<>();
        alertThresholdTypeIdList.add(300);
        alertThresholdTypeIdList.add(700);
        final PosEventCurrent curr = new PosEventCurrent();
        curr.setAlertGmt(alertGmt);
        curr.setAlertInfo(alertInfo);
        final CustomerAlertType cat1 = EntityMap.INSTANCE.findDefaultCustomerAlertTypes().get(0);
        curr.setCustomerAlertType(cat1);
        curr.setEventSeverityType(new EventSeverityType(3, "Critical"));
        final EventType et = new EventType((byte) 4);
        et.setIsNotification(true);
        curr.setEventType(et);
//      curr.setEventActionType(eventActionType);                
        curr.setIsActive(true);
        curr.setPointOfSale(pos);
        curr.setIsHidden(false);
        return curr;
    }
    
    
    @Deprecated
    public static Answer<List<PosEventCurrent>> savePosEventCurrent() {
        return new Answer<List<PosEventCurrent>>() {
            @Override
            public List<PosEventCurrent> answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final PosEventCurrent pec = (PosEventCurrent) args[0];
                final EntityDB db = TestContext.getInstance().getDatabase();
                pec.setPointOfSale(db.findById(PointOfSale.class, pec.getPointOfSale().getId()));
                final StoryObjectBuilder objBuilder = TestLookupTables.getObjectBuilder();
                final Object id = objBuilder.generateId(pec);
                TestDBMap.addToMemory(id, pec);
                return null;
            }
        };
    }
    
    public static Answer<List<PosEventCurrent>> savePosEventCurrentMock() {
        return new Answer<List<PosEventCurrent>>() {
            @Override
            public List<PosEventCurrent> answer(final InvocationOnMock invocation) throws Throwable {
                return null;
            }
        };
    }
    
    public static Answer<Void> updatePosEventCurrent() {
        return new Answer<Void>() {
            @Override
            public Void answer(final InvocationOnMock invocation) throws Throwable {
                final PosEventCurrent pec = invocation.getArgumentAt(0, PosEventCurrent.class);
                
                final PosEventCurrent dbPec = TestDBMap.findObjectByIdFromMemory(pec.getId(), PosEventCurrent.class);
                PropertyUtils.copyProperties(dbPec, pec);
                
                return null;
            }
        };
    }
    
    @Deprecated
    public static Answer<List<Long>> findDefaultPosEventCurrentsToDisable() {
        return new Answer<List<Long>>() {
            @Override
            public List<Long> answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final List<Integer> posIdList = (List<Integer>) args[0];
                final List<Long> candidateList = new ArrayList<Long>();
                final Collection<PosEventCurrent> pecList = (Collection<PosEventCurrent>) TestDBMap.getTypeListFromMemory(PosEventCurrent.class);
                for (PosEventCurrent pec : pecList) {
                    if (pec.getCustomerAlertType().isIsDefault() && !pec.isIsHidden() && posIdList.contains(pec.getPointOfSale().getId())) {
                        candidateList.add(pec.getId());
                    }
                }
                return candidateList;
            }
        };
    }
    
    public static Answer<List<Long>> findPosEventCurrentsToDisableMock() {
        return new Answer<List<Long>>() {
            @Override
            public List<Long> answer(final InvocationOnMock invocation) throws Throwable {
                return new ArrayList<>();
            }
        };
    }
    
    @Deprecated
    public static Answer<List<Long>> findDefaultPosEventCurrentsToEnable() {
        return new Answer<List<Long>>() {
            @Override
            public List<Long> answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final List<Integer> posIdList = (List<Integer>) args[0];
                final List<Long> candidateList = new ArrayList<Long>();
                final Collection<PosEventCurrent> pecList = (Collection<PosEventCurrent>) TestDBMap.getTypeListFromMemory(PosEventCurrent.class);
                for (PosEventCurrent pec : pecList) {
                    if (pec.getCustomerAlertType().isIsDefault() && pec.isIsHidden() && posIdList.contains(pec.getPointOfSale().getId())) {
                        candidateList.add(pec.getId());
                    }
                    
                }
                return candidateList;
            }
        };
    }

    public static Answer<List<Long>> findPosEventCurrentsToEnableMock() {
        return new Answer<List<Long>>() {
            @Override
            public List<Long> answer(final InvocationOnMock invocation) throws Throwable {
                return new ArrayList<>();
            }
        };
    }
    
    @Deprecated
    public static Answer<List<PosEventCurrent>> findPosEventCurrentsByIdList() {
        return new Answer<List<PosEventCurrent>>() {
            @Override
            public List<PosEventCurrent> answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final List<Long> pecIdList = (List<Long>) args[0];
                final List<PosEventCurrent> candidateList = new ArrayList<PosEventCurrent>();
                final Collection<PosEventCurrent> pecList = (Collection<PosEventCurrent>) TestDBMap.getTypeListFromMemory(PosEventCurrent.class);
                for (PosEventCurrent pec : pecList) {
                    if (pecIdList.contains(pec.getId())) {
                        candidateList.add(pec);
                    }
                    
                }
                return candidateList;
            }
        };
    }
    
    public static Answer<List<PosEventCurrent>> findPosEventCurrentsByIdListMock() {
        return new Answer<List<PosEventCurrent>>() {
            @Override
            public List<PosEventCurrent> answer(final InvocationOnMock invocation) throws Throwable {
                return new ArrayList<>();
            }
        };
    }
}
