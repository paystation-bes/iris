package com.digitalpaytech.bdd.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.RoutePOS;
import com.digitalpaytech.util.EntityMap;

public final class RouteServiceMockImpl {
    
    private RouteServiceMockImpl() {
    }
    
    @Deprecated
    public static Answer<List<Route>> findRoutesByPointOfSaleId() {
        return new Answer<List<Route>>() {
            @Override
            public List<Route> answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                @SuppressWarnings("unchecked")
                final int posId = ((Integer) args[0]).intValue();
                final List<Route> routeList = new ArrayList<Route>();
                final Collection<Route> routes = (Collection<Route>) TestDBMap.getTypeListFromMemory(Route.class);
                if (routes != null) {
                    for (Route route : routes) {
                        final Set<RoutePOS> routePOSes = route.getRoutePOSes();
                        for (RoutePOS routePOS : routePOSes) {
                            if (routePOS.getPointOfSale().getId().intValue() == posId) {
                                routeList.add(route);
                                break;
                            }
                        }
                    }
                }
                return routeList;
            }
        };
    }
    
    public static Answer<List<Route>> findRoutesByPointOfSaleIdMock() {
        return new Answer<List<Route>>() {
            @Override
            public List<Route> answer(final InvocationOnMock invocation) throws Throwable {
                return EntityMap.INSTANCE.findDefaultRoutes();
            }
        };
    }
}
