package com.digitalpaytech.bdd.services;

import java.util.Date;
import java.util.List;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.EntityDB;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.MerchantPOS;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosSensorState;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.PosStatus;
import com.digitalpaytech.util.EntityMap;

@SuppressWarnings("checkstyle:illegalthrows")
public final class PointOfSaleServiceMockImpl {
    
    private PointOfSaleServiceMockImpl() {
        
    }
    
    @Deprecated
    public static Answer<PosServiceState> findPosServiceStateByPointOfSaleId() {
        return new Answer<PosServiceState>() {
            @Override
            public PosServiceState answer(final InvocationOnMock invocation) throws Throwable {
                final int pointOfSaleId = (int) invocation.getArguments()[0];
                PosServiceState posss = (PosServiceState) TestDBMap.findObjectByIdFromMemory(pointOfSaleId, PosServiceState.class);
                if (posss == null) {
                    final PointOfSale pointOfSale = (PointOfSale) TestDBMap.findObjectByIdFromMemory(pointOfSaleId, PointOfSale.class);
                    if (pointOfSale == null) {
                        return null;
                    }
                    posss = new PosServiceState();
                    posss.setPointOfSale(pointOfSale);
                    TestContext.getInstance().getDatabase().save(posss);
                }
                return posss;
            }
        };
    }

    public static Answer<PosServiceState> findPosServiceStateByPointOfSaleIdMock() {
        return new Answer<PosServiceState>() {
            @Override
            public PosServiceState answer(final InvocationOnMock invocation) throws Throwable {
                return EntityMap.INSTANCE.findDefaultPointOfSale().getPosServiceState();
            }
        };
    }
    
    @Deprecated
    public static Answer<PointOfSale> findPointOfSaleById() {
        return new Answer<PointOfSale>() {
            
            @Override
            public PointOfSale answer(final InvocationOnMock invocation) throws Throwable {
                final int pointOfSaleId = (int) invocation.getArguments()[0];
                final EntityDB db = TestContext.getInstance().getDatabase();
                return db.findById(PointOfSale.class, pointOfSaleId);
            }
            
        };
    }
    
    public static Answer<PointOfSale> findPointOfSaleByIdMock() {
        return new Answer<PointOfSale>() {
            @Override
            public PointOfSale answer(final InvocationOnMock invocation) throws Throwable {
                return EntityMap.INSTANCE.findDefaultPointOfSale();
            }
        };
    }
    
    public static Answer<PointOfSale> findPointOfSaleBySerialNumber() {
        return new Answer<PointOfSale>() {
            
            @Override
            public PointOfSale answer(final InvocationOnMock invocation) throws Throwable {
                final String serialNumber = (String) invocation.getArguments()[0];
                final EntityDB db = TestContext.getInstance().getDatabase();
                final PointOfSale pos = db.findOne(PointOfSale.class, "serialNumber", serialNumber);
                final PosStatus pstat = new PosStatus();
                pstat.setIsLocked(false);
                pos.setPosStatus(pstat);
                final Customer cust = new Customer();
                cust.setId(1);
                cust.setUnifiId(1);
                pos.setCustomer(cust);
                return pos;
            }
            
        };
    }
    
    public static Answer<List<MerchantPOS>> findMerchPOSByPOSIdNoDeleted() {
        return new Answer<List<MerchantPOS>>() {
            
            @Override
            public List<MerchantPOS> answer(final InvocationOnMock invocation) throws Throwable {
                final EntityDB db = TestContext.getInstance().getDatabase();
                return db.find(MerchantPOS.class);
            }
        };
    }
    
    public static Answer<Paystation> findPayStationByPointOfSaleId() {
        return new Answer<Paystation>() {
            @Override
            public Paystation answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final int pointOfSaleId = (int) args[0];
                final EntityDB db = TestContext.getInstance().getDatabase();
                return db.findById(PointOfSale.class, pointOfSaleId).getPaystation();
            }
        };
    }
    
    public static Answer<PosSensorState> findPosSensorStateByPointOfSaleId() {
        return new Answer<PosSensorState>() {
            
            @Override
            public PosSensorState answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final int pointOfSaleId = (int) args[0];
                final EntityDB db = TestContext.getInstance().getDatabase();
                final PointOfSale pointOfSale = db.findById(PointOfSale.class, pointOfSaleId);
                return new PosSensorState(pointOfSale, (byte) 1, (byte) 1, (byte) 1, (byte) 1, new Date());
            }
            
        };
    }
}
