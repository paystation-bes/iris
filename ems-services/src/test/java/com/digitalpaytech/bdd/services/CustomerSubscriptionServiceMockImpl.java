package com.digitalpaytech.bdd.services;

import java.util.ArrayList;
import java.util.List;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerSubscription;

@SuppressWarnings({"checkstyle:illegalthrows"})
public final class CustomerSubscriptionServiceMockImpl {
    
    private CustomerSubscriptionServiceMockImpl() {
    
    }
    
    public static Answer<List<CustomerSubscription>> findCustomerSubscriptionsByCustomerId() {
        return new Answer<List<CustomerSubscription>>() {
            @Override
            public List<CustomerSubscription> answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final List<CustomerSubscription> customerSubscriptionList = new ArrayList<CustomerSubscription>();
                final Customer customer = (Customer) TestDBMap.findObjectByIdFromMemory((Integer) args[0], Customer.class);
                for (CustomerSubscription subscription : customer.getCustomerSubscriptions()) {
                    customerSubscriptionList.add(subscription);
                }
                return customerSubscriptionList;
            }
        };
    }
}
