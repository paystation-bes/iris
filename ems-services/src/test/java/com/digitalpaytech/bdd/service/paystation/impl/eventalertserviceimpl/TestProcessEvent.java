package com.digitalpaytech.bdd.service.paystation.impl.eventalertserviceimpl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

import org.junit.Assert;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.services.EntityDaoServiceMockImpl;
import com.digitalpaytech.bdd.services.PointOfSaleServiceMockImpl;
import com.digitalpaytech.bdd.services.QueueEventPublisherMockImpl;
import com.digitalpaytech.bdd.util.Cache;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.bdd.util.TestEventQueue;
import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.EventDefinition;
import com.digitalpaytech.domain.EventDeviceType;
import com.digitalpaytech.domain.EventException;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.dto.paystation.EventData;
import com.digitalpaytech.dto.queue.QueueEvent;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.service.EventTypeService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.impl.EventExceptionServiceImpl;
import com.digitalpaytech.service.impl.PosEventDelayedServiceImpl;
import com.digitalpaytech.service.paystation.PosServiceStateService;
import com.digitalpaytech.service.paystation.impl.EventAlertServiceImpl;
import com.digitalpaytech.service.queue.impl.KafkaEventServiceImpl;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.kafka.InvalidTopicTypeException;
import com.digitalpaytech.util.kafka.impl.LinkMessageProducer;
import com.digitalpaytech.util.queue.QueuePublisher;

@SuppressWarnings({ "PMD.AvoidUsingShortType", "PMD.BeanMembersShouldSerialize", "PMD.ExcessiveImports", "PMD.UnusedPrivateField" })
public class TestProcessEvent implements JBehaveTestHandler {
    
    private static final List<EventSeverityType> EVENT_SEVERITY_TYPE = TestLookupTables.getEventSeverityList();
    private static final List<EventDefinition> EVENT_DEFINITION_LIST = TestLookupTables.getEventDefinationList();
    private static final Map<String, String> EVENT_TYPE_MAPPING = TestLookupTables.getEventTypeMapping();
    
    private static final String HISTORICAL_STRING = "Historical";
    private static final String RECENT_AND_HISTORICAL_STRING = "Recent & Historical";
    private static final String RECENT_EVENT_QUEUE = "RECENT_EVENT_QUEUE";
    private static final String HISTORICAL_EVENT_QUEUE = "historicalEventList";
    
    @Mock
    private PointOfSaleService pointOfSaleService;
    
    @Mock
    private PosServiceStateService posServiceStateService;
    
    @Mock
    private EventTypeService eventTypeService;
    
    @Mock
    private EntityDao entityDao;
    
    @Mock
    private QueuePublisher<QueueEvent> recentEventsQueuePublisher;
    
    @Mock
    private QueuePublisher<QueueEvent> historicalEventsQueuePublisher;
    
    @Mock
    private QueuePublisher<QueueEvent> processingEventsQueuePublisher;
    
    @InjectMocks
    private EventAlertServiceImpl eventAlertService;
    
    @InjectMocks
    private KafkaEventServiceImpl queueEventService;
    
    @InjectMocks
    private PosEventDelayedServiceImpl posEventDelayedService;
    
    private final void transmitEvent(final String posName) {
        MockitoAnnotations.initMocks(this);
        implementMocks();
        this.eventAlertService.loadPositiveEventActions();
        
        final LinkMessageProducer producer = new LinkMessageProducer() {
            @Override
            public <T> Future<Boolean> sendWithByteArray(final String topicType, final String key, final T data)
                    throws InvalidTopicTypeException, JsonException {
                return new CompletableFuture<Boolean>();
            }
        };
        this.queueEventService.setProcessingEventsProducer(producer);
        this.eventAlertService.setQueueEventService(this.queueEventService);
        final PointOfSale pointOfSale = (PointOfSale) TestDBMap.findObjectByFieldFromMemory(posName, "name", PointOfSale.class);
        @SuppressWarnings("unchecked")
        final Cache cache = TestContext.getInstance().getCache();
        final Map<Object, EventData> event = cache.map(EventData.class);
        if (event.size() != 1) {
            Assert.fail("The number of event objects found are" + event.size() + ", there should be only one to transmit.");
        }
        final Map.Entry<Object, EventData> buffer = event.entrySet().iterator().next();
        final EventData eventToTransmit = buffer.getValue();
        cache.remove(EventData.class, buffer.getKey());
        try {
            this.eventAlertService.processEvent(pointOfSale, eventToTransmit);
        } catch (InvalidDataException e) {
            Assert.fail(e.getStackTrace().toString());
        }
    }
    
    private final void assertEvent() {
        final Map<String, Object> objectMap = TestDBMap.getObjectMapFromMem();
        final String eventActionType = (String) objectMap.get("eventactiontype");
        final String eventSeverity = (String) objectMap.get("eventseverity");
        final String active = (String) objectMap.get("active");
        final String queueType = (String) objectMap.get("queuetype");
        final String eventTypeString = (String) objectMap.get("eventtype");
        
        if (RECENT_AND_HISTORICAL_STRING.equalsIgnoreCase(queueType.trim())) {
            this.assertQueueAndEventType(eventActionType, eventSeverity, active, TestProcessEvent.RECENT_EVENT_QUEUE, eventTypeString);
            this.assertQueueAndEventType(eventActionType, eventSeverity, active, TestProcessEvent.HISTORICAL_EVENT_QUEUE, eventTypeString);
        } else if (HISTORICAL_STRING.equalsIgnoreCase(queueType.trim())) {
            this.assertQueueAndEventType(eventActionType, eventSeverity, active, TestProcessEvent.HISTORICAL_EVENT_QUEUE, eventTypeString);
            this.assertEmptyQueue(TestProcessEvent.RECENT_EVENT_QUEUE);
        } else if (StandardConstants.STRING_NULL.equalsIgnoreCase(queueType.trim())) {
            this.assertEmptyQueue(TestProcessEvent.RECENT_EVENT_QUEUE);
            this.assertEmptyQueue(TestProcessEvent.HISTORICAL_EVENT_QUEUE);
        } else {
            Assert.fail();
        }
        
        junit.framework.Assert.assertTrue(true);
    }
    
    @SuppressWarnings({ "PMD.UseObjectForClearerAPI", "checkstyle:cyclomaticcomplexity" })
    private void assertQueueAndEventType(final String eventActionType, final String eventSeverity, final String active, final String queueName,
        final String eventTypeString) {
        final TestEventQueue eventQueues = TestDBMap.getTestEventQueueFromMem();
        final Queue<Object> eventQueue;
        if (TestProcessEvent.RECENT_EVENT_QUEUE.equalsIgnoreCase(queueName)) {
            eventQueue = eventQueues.getQueue(WebCoreConstants.QUEUE_TYPE_RECENT_EVENT);
        } else if (TestProcessEvent.HISTORICAL_EVENT_QUEUE.equalsIgnoreCase(queueName)) {
            eventQueue = eventQueues.getQueue(WebCoreConstants.QUEUE_TYPE_HISTORICAL_EVENT);
        } else {
            Assert.fail();
            return;
        }
        if (eventQueue.size() != 1) {
            Assert.fail();
        }
        final QueueEvent event = (QueueEvent) eventQueue.remove();
        
        if (eventActionType.equalsIgnoreCase(WebCoreConstants.BLANK) && event.getEventActionTypeId() != null
            || !EVENT_SEVERITY_TYPE.get(event.getEventSeverityTypeId()).getName().equalsIgnoreCase(eventSeverity.trim())
            || event.getIsActive() != Boolean.parseBoolean(active.trim())) {
            Assert.fail();
        }
        
        final byte eventTypeId = event.getEventTypeId();
        for (EventDefinition e : EVENT_DEFINITION_LIST) {
            if (EVENT_TYPE_MAPPING.get(eventTypeString.trim()).equalsIgnoreCase(e.getEventType().getDescription())
                && e.getEventType().getId() == eventTypeId) {
                return;
            }
        }
        Assert.fail("Could not assert the EventType.");
    }
    
    private final void assertEventIgnored() {
        this.assertEmptyQueue(TestProcessEvent.RECENT_EVENT_QUEUE);
        this.assertEmptyQueue(TestProcessEvent.HISTORICAL_EVENT_QUEUE);
    }
    
    private void assertEmptyQueue(final String queueName) {
        @SuppressWarnings("unchecked")
        final List<QueueEvent> eventQueue = (List<QueueEvent>) TestDBMap.findObjectByIdFromMemory(queueName, List.class);
        
        if (!(eventQueue == null || eventQueue.isEmpty())) {
            Assert.fail();
        }
    }
    
    private void implementMocks() {
        
        this.queueEventService.setEventExceptionService(new EventExceptionServiceImpl() {
            
            private Map<String, EventException> eventExceptionMap;
            
            public boolean isException(final Integer paystationTypeId, final Integer eventDeviceTypeId) {
                if (loadAll() != null) {
                    return loadAll().containsKey(new StringBuilder(String.valueOf(paystationTypeId)).append(StandardConstants.STRING_UNDERSCORE)
                            .append(eventDeviceTypeId).toString());
                }
                return false;
            }
            
            public Map<String, EventException> loadAll() {
                if (this.eventExceptionMap == null) {
                    final List<EventException> eventExceptionList = TestLookupTables.getEventExceptionList();
                    if (eventExceptionList != null) {
                        this.eventExceptionMap = new HashMap<String, EventException>();
                        for (EventException eventException : eventExceptionList) {
                            this.eventExceptionMap.put(new StringBuilder(String.valueOf(eventException.getPaystationType().getId()))
                                    .append(StandardConstants.STRING_UNDERSCORE).append(eventException.getEventDeviceType().getId()).toString(),
                                                       eventException);
                        }
                    }
                }
                return this.eventExceptionMap;
            }
        });
        
        this.eventAlertService.setQueueEventService(this.queueEventService);
        Mockito.when(this.pointOfSaleService.findPayStationByPointOfSaleId(Mockito.anyInt()))
                .thenAnswer(PointOfSaleServiceMockImpl.findPayStationByPointOfSaleId());
        Mockito.when(this.entityDao.loadAll(EventDeviceType.class)).thenAnswer(EntityDaoServiceMockImpl.loadAll(EventDeviceType.class));
        Mockito.when(this.entityDao.findByNamedQuery(Mockito.anyString())).thenAnswer(new Answer<List<EventDefinition>>() {
            
            @Override
            public List<EventDefinition> answer(final InvocationOnMock invocation) throws Throwable {
                return TestLookupTables.getEventDefinationList();
            }
            
        });
        
        Mockito.when(this.pointOfSaleService.findPosServiceStateByPointOfSaleId(Mockito.anyInt())).thenAnswer(new Answer<PosServiceState>() {
            
            @Override
            public PosServiceState answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final int pointOfSaleId = (int) args[0];
                
                final PointOfSale pointOfSale = TestContext.getInstance().getDatabase().findById(PointOfSale.class, pointOfSaleId);
                
                return new PosServiceState(pointOfSale, (short) 1, (byte) 1, 1, (short) 1, (byte) 1, (short) 1, (byte) 1, (short) 1, (byte) 1, 1,
                     false, false, false, false, false, (byte) 1, false, false, false, false, false, false, false, false, false, false, false, new Date());
            }
        });
        
        Mockito.when(this.pointOfSaleService.findPosSensorStateByPointOfSaleId(Mockito.anyInt()))
                .thenAnswer(PointOfSaleServiceMockImpl.findPosSensorStateByPointOfSaleId());
        
        Mockito.doAnswer(QueueEventPublisherMockImpl.offer(WebCoreConstants.QUEUE_TYPE_RECENT_EVENT)).when(this.recentEventsQueuePublisher)
                .offer(Mockito.any(QueueEvent.class));
        Mockito.doAnswer(QueueEventPublisherMockImpl.offer(WebCoreConstants.QUEUE_TYPE_HISTORICAL_EVENT)).when(this.historicalEventsQueuePublisher)
                .offer(Mockito.any(QueueEvent.class));
        Mockito.doAnswer(QueueEventPublisherMockImpl.offer(WebCoreConstants.QUEUE_TYPE_PROCESSING_EVENT)).when(this.processingEventsQueuePublisher)
                .offer(Mockito.any(QueueEvent.class));
    }
    
    @Override
    public Object performAction(Object... objects) {
        transmitEvent((String) objects[0]);
        return null;
    }
    
    @Override
    public Object assertSuccess(Object... objects) {
        assertEvent();
        return null;
    }
    
    @Override
    public Object assertFailure(Object... objects) {
        // TODO Auto-generated method stub
        return null;
    }
}
