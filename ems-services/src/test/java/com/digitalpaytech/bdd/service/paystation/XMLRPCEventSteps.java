package com.digitalpaytech.bdd.service.paystation;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.jbehave.core.annotations.AfterStory;
import org.jbehave.core.annotations.BeforeStory;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import com.digitalpaytech.bdd.util.AbstractSteps;
import com.digitalpaytech.bdd.util.JBehaveTestHandlers;
import com.digitalpaytech.bdd.util.ObjectConstructionException;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.bdd.util.TestEventQueue;
import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.domain.PaystationType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.dto.paystation.EventData;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;

@SuppressWarnings({ "PMD.UseObjectForClearerAPI", "PMD.UseConcurrentHashMap", })
public final class XMLRPCEventSteps extends AbstractSteps {
    
    private static final String ACTION_TRANSMIT = "transmit";
    private static final String OBJECT_NAME = "paystationevent";
    private static final String ACTION_ASSERT = "assert";
    private static final String ACTION_IGNORED = "ignore";
    private static final String TEST_PAYSTATION_NAME = "testpaystation";
    
    public XMLRPCEventSteps(final JBehaveTestHandlers testHandlers) {
        super(testHandlers);
    }
    
    @BeforeStory
    public final void setup() {
        TestLookupTables.getExpressionParser().register(getStoryObjectTypes());
    }
    
    protected final Class<?>[] getStoryObjectTypes() {
        return new Class[] { PointOfSale.class, PaystationType.class };
    }
    
    @When("the testpaystation sends <type>, <action>, <information> at <timestamp>")
    public final void sendEvent(@Named("type") final String type, @Named("action") final String action,
        @Named("information") final String information, @Named("timestamp") final String timestamp) throws ObjectConstructionException {
        final PointOfSale pointOfSale = (PointOfSale) TestDBMap.findObjectByFieldFromMemory(TEST_PAYSTATION_NAME, "name", PointOfSale.class);
        final EventData event = new EventData();
        event.setType(type);
        event.setAction(action);
        event.setInformation(information);
        event.setTimeStamp(new Date());
        event.setPointOfSaleId(pointOfSale.getId());
        TestDBMap.addToMemory(new Double(Math.random() * StandardConstants.CONSTANT_100).intValue(), event);
        testHandlers.invokePerformMethod(XMLRPCEventSteps.OBJECT_NAME, XMLRPCEventSteps.ACTION_TRANSMIT, TEST_PAYSTATION_NAME);
    }
    
    /**
     * 
     * Sends a Pay station event.
     * 
     * @.example the *testpaystation* sends *Printer* event *Paper Out*
     * 
     * @param posName
     *            Name of the Pay station for which the event is sent
     * @param type
     *            Type entity for which the event is being sent i.e. Bill Acceptor, Printer
     * @param action
     *            The event for the entity i.e. Paper Out, Paper Jam, Door Open
     */
    @When("the $posName sends $type event $action")
    public final void sendExistingEvent(final String posName, final String type, final String action) throws ObjectConstructionException {
        testHandlers.invokePerformMethod(XMLRPCEventSteps.OBJECT_NAME, XMLRPCEventSteps.ACTION_TRANSMIT, posName);
    }
    
    @Given("the InMemoryDataBase is reset")
    public final void resetTheDatabase() {
        TestContext.getInstance().close();
        this.testHandlers.clearTestHandlers();
        
        this.testHandlers.initializeTestHandlers();
        final TestEventQueue testEventQueue = new TestEventQueue();
        TestDBMap.saveTestEventQueueToMem(testEventQueue);
    }
    
    @Then("the created event has the following <eventactiontype>, <eventseverity>, <active>, <queuetype> and <eventtype>")
    public final void assertEventCreated(@Named("eventactiontype") final String eventActionType, @Named("eventseverity") final String eventSeverity,
        @Named("active") final String active, @Named("queuetype") final String queueType, @Named("eventtype") final String eventType)
            throws ObjectConstructionException {
        //If the queue type is recent & historical we assert both the queues in the memory database
        final Map<String, Object> objectMap = new HashMap<String, Object>();
        objectMap.put("eventactiontype", "null".equalsIgnoreCase(eventActionType) ? WebCoreConstants.BLANK : eventActionType);
        objectMap.put("eventseverity", eventSeverity);
        objectMap.put("active", active);
        objectMap.put("queuetype", queueType);
        objectMap.put("eventtype", eventType);
        TestDBMap.saveObjectMapToMem(objectMap);
        testHandlers.invokeAssertSuccessMethod(XMLRPCEventSteps.OBJECT_NAME, XMLRPCEventSteps.ACTION_ASSERT);
    }
    
    /**
     * 
     * Asserts to insure event is not processed and is ignored.
     * 
     * @.example the event is ignored
     *           
     */
    @Then("the event is ignored")
    public final void assertEventIgnored() throws ObjectConstructionException {
        testHandlers.invokeAssertFailureMethod(XMLRPCEventSteps.OBJECT_NAME, XMLRPCEventSteps.ACTION_IGNORED);
    }
    
}
