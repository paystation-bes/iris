package com.digitalpaytech.bdd.service.paystation;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.service.paystation.impl.eventalertserviceimpl.TestProcessEvent;
import com.digitalpaytech.bdd.util.AbstractSteps;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

// Because we're using Kafka for events, these test stories wouldn't work anymore.
@org.junit.Ignore
@RunWith(JUnitReportingRunner.class)
public class EventsFromXmlrpcStories extends AbstractStories {
    
    public EventsFromXmlrpcStories() {
        super();
        this.testHandlers.registerTestHandler("paystationeventtransmit", TestProcessEvent.class);
        this.testHandlers.registerTestHandler("paystationeventassert", TestProcessEvent.class);
        this.testHandlers.registerTestHandler("paystationeventignore", TestProcessEvent.class);
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }
    
    // Here we specify the steps classes
    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new XMLRPCEventSteps(this.testHandlers));
    }
}