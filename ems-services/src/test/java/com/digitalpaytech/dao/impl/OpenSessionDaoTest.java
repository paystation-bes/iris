package com.digitalpaytech.dao.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.hibernate.Query;

import com.digitalpaytech.bdd.util.EntityDB;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.dao.OpenSessionDao;

public final class OpenSessionDaoTest implements OpenSessionDao {
	private EntityDB db;

	public OpenSessionDaoTest() {
		this.db = TestContext.getInstance().getDatabase();
	}

	@Override
	public List findByNamedQuery(final String queryName) {
		return this.db.session().getNamedQuery(queryName).list();
	}

	@Override
	public Object findUniqueByNamedQueryAndNamedParam(final String queryName,
			final String paramName, final Object value) {
		return this.db.session().getNamedQuery(queryName)
				.setParameter(paramName, value).uniqueResult();
	}

	@Override
	public Serializable save(final Object entity) {
		return this.db.session().save(entity);
	}

	@Override
	public void update(final Object entity) {
		this.db.session().update(entity);
	}

	@Override
	public <T> List<T> loadAll(final Class<T> entityClass) {
		return this.db.find(entityClass);
	}

	@Override
	public <T> List<T> findByNamedQueryAndNamedParam(final String queryName,
			final String[] paramNames, final Object[] values, final boolean cacheable) {
		final Query query = this.db.session().getNamedQuery(queryName);
		query.setCacheable(cacheable);
		int i = paramNames.length;
		while (--i >= 0) {
			if (values[i] instanceof Collection) {
				query.setParameterList(paramNames[i], (Collection) values[i]);
			} else {
				query.setParameter(paramNames[i], values[i]);
			}
		}

		return query.list();
	}

}
