package com.digitalpaytech.dao.impl;

import org.hibernate.Session;

import com.digitalpaytech.bdd.util.EntityDB;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.dao.EntityDaoReport;

public class EntityDaoReportTest implements EntityDaoReport {
    private EntityDB db;
    
    public EntityDaoReportTest() {
        this.db = TestContext.getInstance().getDatabase();
    }
    
    @Override
    public Session getCurrentSession() {
        return this.db.session();
    }
    
}
