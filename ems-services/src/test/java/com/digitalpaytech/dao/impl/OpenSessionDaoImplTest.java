package com.digitalpaytech.dao.impl;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.OpenSessionDao;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations={"classpath:ems-test.xml", "classpath:ems-security.xml"})
//@Transactional
public class OpenSessionDaoImplTest {

	@Autowired
	private OpenSessionDao openSessionDao;
	
	@Test
//	@Transactional(propagation=Propagation.SUPPORTS)
	public void test_findByNamedQuery() {
//		Iterator iterator = openSessionDao.findByNamedQuery(
//				"EmsProperties.findAll").iterator();
//		
//		assertNotNull(iterator);
	}

	public void test_findUniqueByNamedQueryAndNamedParam() {
		Object obj = openSessionDao.findUniqueByNamedQueryAndNamedParam("EmsProperties.findByName", "name", "ServerUrl");
		assertNotNull(obj);
	}
}
