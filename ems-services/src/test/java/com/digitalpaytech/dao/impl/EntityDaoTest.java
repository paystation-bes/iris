package com.digitalpaytech.dao.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.digitalpaytech.bdd.util.EntityDB;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.dao.EntityDao;

@SuppressWarnings({ "rawtypes", "PMD.ExcessivePublicCount", "PMD.TooManyMethods", "PMD.UseVarargs", "unchecked" })
public final class EntityDaoTest implements EntityDao {
    private final EntityDB db;
    
    public EntityDaoTest() {
        this.db = TestContext.getInstance().getDatabase();
    }
    
    public void delete(final Object entity) {
        this.db.session().delete(entity);
    }
    
    public List findByNamedQuery(final String queryName) {
        return this.db.session().getNamedQuery(queryName).list();
    }
    
    public List findByNamedQuery(final String queryName, final boolean cacheable) {
        return this.db.session().getNamedQuery(queryName).setCacheable(cacheable).list();
    }
    
    public List findByNamedQuery(final String queryName, final boolean isCacheable, final int startRow, final int howManyRows) {
        final Query query = this.db.session().getNamedQuery(queryName);
        query.setCacheable(isCacheable);
        query.setFirstResult(startRow);
        query.setMaxResults(howManyRows);
        
        return query.list();
    }
    
    public List findByNamedQuery(final String queryName, final Object[] values, final int startRow, final int howManyRows) {
        final Query query = this.db.session().getNamedQuery(queryName);
        query.setFirstResult(startRow);
        query.setMaxResults(howManyRows);
        
        int i = values.length;
        while (--i >= 0) {
            query.setParameter(i, values[i]);
        }
        
        return query.list();
    }
    
    public List findByNamedQuery(final String queryName, final String[] names, final Object[] values, final int startRow, final int howManyRows) {
        final Query query = this.db.session().getNamedQuery(queryName);
        query.setFirstResult(startRow);
        query.setMaxResults(howManyRows);
        
        int i = names.length;
        while (--i >= 0) {
            if (values[i] instanceof Collection) {
                query.setParameterList(names[i], (Collection) values[i]);
            } else {
                query.setParameter(names[i], values[i]);
            }
        }
        
        return query.list();
    }
    
    public List findByNamedQueryWithLock(final String queryName, final String lockedAlias) {
        return this.db.session().getNamedQuery(queryName).setLockMode(lockedAlias, LockMode.PESSIMISTIC_WRITE).list();
    }
    
    public List findByNamedQueryAndNamedParamWithLock(final String queryName, final String paramName, final Object value, final String lockedAlias) {
        return this.db.session().getNamedQuery(queryName).setLockMode(lockedAlias, LockMode.PESSIMISTIC_WRITE).setParameter(paramName, value).list();
    }
    
    public List findByNamedQueryAndNamedParam(final String queryName, final String paramName, final Object value) {
        return this.db.session().getNamedQuery(queryName).setParameter(paramName, value).list();
    }
    
    public List findByNamedQueryAndNamedParam(final String queryName, final String paramName, final Object value, final boolean cacheable) {
        return this.db.session().getNamedQuery(queryName).setCacheable(cacheable).setParameter(paramName, value).list();
    }
    
    public List findByNamedQueryAndNamedParam(final String queryName, final String[] paramNames, final Object[] values) {
        final Query query = this.db.session().getNamedQuery(queryName);
        int i = paramNames.length;
        while (--i >= 0) {
            if (values[i] instanceof Collection) {
                query.setParameterList(paramNames[i], (Collection) values[i]);
            } else {
                query.setParameter(paramNames[i], values[i]);
            }
        }
        
        return query.list();
    }
    
    public List findByNamedQueryAndNamedParam(final String queryName, final String[] paramNames, final Object[] values, final boolean cacheable) {
        final Query query = this.db.session().getNamedQuery(queryName);
        query.setCacheable(cacheable);
        int i = paramNames.length;
        while (--i >= 0) {
            if (values[i] instanceof Collection) {
                query.setParameterList(paramNames[i], (Collection) values[i]);
            } else {
                query.setParameter(paramNames[i], values[i]);
            }
        }
        
        return query.list();
    }
    
    public List findByNamedQueryAndNamedParam(final String queryName, final String[] paramNames, final Object[] values, final int startingResult,
        final int maxResult, final boolean cacheable) {
        final Query query = this.db.session().getNamedQuery(queryName);
        query.setCacheable(cacheable);
        query.setFirstResult(startingResult);
        query.setMaxResults(maxResult);
        
        int i = paramNames.length;
        while (--i >= 0) {
            if (values[i] instanceof Collection) {
                query.setParameterList(paramNames[i], (Collection) values[i]);
            } else {
                query.setParameter(paramNames[i], values[i]);
            }
        }
        
        return query.list();
    }
    
    public List findByNamedQueryAndNamedParamWithLimit(final String queryName, final String[] paramNames, final Object[] values,
        final boolean cacheable, final int limit) {
        final Query query = this.db.session().getNamedQuery(queryName);
        query.setCacheable(cacheable);
        query.setMaxResults(limit);
        
        int i = paramNames.length;
        while (--i >= 0) {
            if (values[i] instanceof Collection) {
                query.setParameterList(paramNames[i], (Collection) values[i]);
            } else {
                query.setParameter(paramNames[i], values[i]);
            }
        }
        
        return query.list();
    }
    
    public SQLQuery createSQLQuery(final String queryStr) {
        return this.db.session().createSQLQuery(queryStr);
    }
    
    public void evict(final Object obj) {
        this.db.session().evict(obj);
    }
    
    public int deleteAllByNamedQuery(final String queryName, final Object[] values) {
        final Query query = this.db.session().getNamedQuery(queryName);
        
        int i = values.length;
        while (--i >= 0) {
            query.setParameter(i, values[i]);
        }
        
        return query.executeUpdate();
    }
    
    public Criteria createCriteria(final Class clazz) {
        return this.db.session().createCriteria(clazz);
    }
    
    public void clearNamedQuery(final String cacheRegion) {
        // No need to worry about this cause we don't have cache!
    }
    
    public void clearCollection(final String cacheRegion) {
        // No need to worry about this cause we don't have cache!
    }
    
    public void clearEntity(final String entityName) {
        // No need to worry about this cause we don't have cache!
    }
    
    public boolean clearFromCache(final Class<?> entityClass, final Serializable identifier) {
        // No need to worry about this cause we don't have cache!
        return true;
    }
    
    public boolean clearFromCache(final Object entity) {
        // No need to worry about this cause we don't have cache!
        return true;
    }
    
    public void clearFromCache(final Query query) {
        // No need to worry about this cause we don't have cache!
    }
    
    public void bringToCache(final Object entity) {
        // No need to worry about this cause we don't have cache!
    }
    
    public void bringToCache(final Query query) {
        // No need to worry about this cause we don't have cache!
    }
    
    public boolean equals(final Object obj) {
        return this.db.session().equals(obj);
    }
    
    public Object findUniqueByNamedQueryWithLock(final String queryName, final String lockedAlias) {
        final Query query = this.db.session().getNamedQuery(queryName);
        query.setLockMode(lockedAlias, LockMode.PESSIMISTIC_WRITE);
        
        return query.uniqueResult();
    }
    
    public Object findUniqueByNamedQueryAndNamedParamWithLock(final String queryName, final String paramName, final Object value,
        final String lockedAlias) {
        final Query query = this.db.session().getNamedQuery(queryName);
        query.setLockMode(lockedAlias, LockMode.PESSIMISTIC_WRITE);
        query.setParameter(paramName, value);
        
        return query.uniqueResult();
    }
    
    public Object findUniqueByNamedQueryAndNamedParam(final String queryName, final String paramName, final Object value) {
        final Query query = this.db.session().getNamedQuery(queryName);
        query.setParameter(paramName, value);
        
        return query.uniqueResult();
    }
    
    public Object findUniqueByNamedQueryAndNamedParam(final String queryName, final String paramName, final Object value, final boolean cacheable) {
        final Query query = this.db.session().getNamedQuery(queryName);
        query.setCacheable(cacheable);
        query.setParameter(paramName, value);
        
        return query.uniqueResult();
    }
    
    public Object findUniqueByNamedQueryAndNamedParam(final String queryName, final String[] paramNames, final Object[] values,
        final boolean cacheable) {
        final Query query = this.db.session().getNamedQuery(queryName);
        query.setCacheable(cacheable);
        
        int i = paramNames.length;
        while (--i >= 0) {
            if (values[i] instanceof Collection) {
                query.setParameterList(paramNames[i], (Collection) values[i]);
            } else {
                query.setParameter(paramNames[i], values[i]);
            }
        }
        
        return query.uniqueResult();
    }
    
    public void flush() {
        this.db.session().flush();
    }
    
    public <T> T get(final Class<T> entityClass, final Serializable id) {
        return (T) this.db.session().get(entityClass, id);
    }
    
    public <T> T get(final Class<T> entityClass, final Serializable id, final LockMode lockMode) {
        return (T) this.db.session().get(entityClass, id, lockMode);
    }
    
    public Query getNamedQuery(final String queryName) {
        return this.db.session().getNamedQuery(queryName);
    }
    
    public <T> List<T> loadAll(final Class<T> entityClass) {
        return this.db.find(entityClass);
    }
    
    public <T> List<T> loadWithLimit(final Class<T> entityClass, final int limit, final boolean isCacheable) {
        return this.db.session().createCriteria(entityClass).setCacheable(isCacheable).setMaxResults(limit).list();
    }
    
    public Serializable save(final Object entity) {
        return this.db.session().save(entity);
    }
    
    public void save(final String entityName, final Object entity) {
        this.db.session().save(entityName, entity);
    }
    
    public void saveOrUpdate(final Object entity) {
        this.db.session().saveOrUpdate(entity);
    }
    
    public void saveOrUpdateAll(final Collection entities) {
        for (Object obj : entities) {
            saveOrUpdate(obj);
        }
    }
    
    public void update(final Object entity) {
        this.db.session().update(entity);
    }
    
    public <T> T merge(final T entity) {
        return (T) this.db.session().merge(entity);
    }
    
    public List findPageResultByNamedQuery(final String queryName, final String[] paramNames, final Object[] values, final boolean isCacheable,
        final int page, final int pageSize) {
        final Query queryObject = getCurrentSession().getNamedQuery(queryName);
        queryObject.setCacheable(isCacheable);
        if (values != null) {
            for (int i = 0; i < values.length; i++) {
                if (values[i] instanceof Collection) {
                    queryObject.setParameterList(paramNames[i], (Collection) values[i]);
                } else {
                    queryObject.setParameter(paramNames[i], values[i]);
                }
            }
        }
        if ((pageSize != 0) && (page > 0)) {
            queryObject.setFirstResult((page - 1) * pageSize);
            queryObject.setMaxResults(pageSize);
        }
        return queryObject.list();
    }
    
    public int findTotalResultByNamedQuery(final String queryName, final String[] names, final Object[] values) {
        return findTotalResultByNamedQueryWithLock(queryName, names, values, (String) null);
    }
    
    public int findTotalResultByNamedQueryWithLock(final String queryName, final String[] names, final Object[] values, final String alias) {
        final Query query = this.db.session().getNamedQuery(queryName);
        if (alias != null) {
            query.setLockMode(alias, LockMode.PESSIMISTIC_WRITE);
        }
        
        int i = names.length;
        while (--i >= 0) {
            if (values[i] instanceof Collection) {
                query.setParameterList(names[i], (Collection) values[i]);
            } else {
                query.setParameter(names[i], values[i]);
            }
        }
        
        final Iterator it = query.list().iterator();
        
        int num_records = 0;
        while (it.hasNext()) {
            num_records += ((Number) it.next()).intValue();
        }
        
        return num_records;
    }
    
    public int updateByNamedQuery(final String queryName, final String[] names, final Object[] values) {
        final Query query = this.db.session().getNamedQuery(queryName);
        if (values != null) {
            int i = values.length;
            while (--i >= 0) {
                query.setParameter(names[i], values[i]);
            }
        }
        return query.executeUpdate();
    }
    
    public Session getCurrentSession() {
        return this.db.session();
    }
    
    public int hashCode() {
        return this.db.session().hashCode();
    }
    
    public Criterion nullableAnd(final Criterion mainCondition, final Criterion additionalCondition) {
        Criterion result = null;
        if (mainCondition == null) {
            result = additionalCondition;
        } else if (additionalCondition == null) {
            result = mainCondition;
        } else {
            result = Restrictions.and(mainCondition, additionalCondition);
        }
        
        return result;
    }
    
    public Criterion nullableOr(final Criterion mainCondition, final Criterion additionalCondition) {
        Criterion result = null;
        if (mainCondition == null) {
            result = additionalCondition;
        } else if (additionalCondition == null) {
            result = mainCondition;
        } else {
            result = Restrictions.or(mainCondition, additionalCondition);
        }
        
        return result;
    }
    
    public void initialize(final Object proxy) {
        Hibernate.initialize(proxy);
    }
    
    public void clear() {
        this.db.clear();
    }
    
    public String toString() {
        return this.db.session().toString();
    }
    
    @Override
    public void deleteByNamedQueryAndNamedParams(String queryName, String[] paramNames, Object[] values) {
        // TODO Auto-generated method stub
        
    }
    
}
