package com.digitalpaytech.dao.impl;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import static org.junit.Assert.*;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Section;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.Tab;

@org.junit.Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:ems-test.xml"})
@Transactional
public class CriteriaDaoImplTest {

	private static Logger log = Logger.getLogger(CriteriaDaoImplTest.class);
	
	@Autowired
	private EntityDao entityDao;
	
	
	@Transactional(propagation=Propagation.SUPPORTS)
	@Test
	public void findTabByIdEager() {
		
		Tab tab = findTabWithCriteria(4, true);
		if (tab == null) {
			tab = findTabWithCriteria(4, false);
		}
		
		log.debug("ttttttttt tab: " + tab);
		log.debug("ttttttttt tab name: " + tab.getName() + ", section num: " + tab.getSections().size());
		Iterator<Section> secIter = tab.getSections().iterator();
		while (secIter.hasNext()) {
			Section section = secIter.next();
			log.debug("sssssssssssss sec layout id / order: " + section.getLayoutId() + "/" + section.getOrderNumber());
			
			Iterator<Widget> wiIter = section.getWidgets().iterator();
			while (wiIter.hasNext()) {
				Widget wi = wiIter.next();
				log.debug("wwwwwwwwwwww widget/colId/orderNum : " + wi.getName() + "/" + wi.getColumnId() + "/" + wi.getOrderNumber());
			}
		}
		
		assertNotNull(tab);
	}

	
	//--------------------------------------------------------------------
	// COPIED from DashboardServiceImpl
	//--------------------------------------------------------------------
	private Tab findTabWithCriteria(int id, boolean searchWidgetsFlag) {
		
		Criteria crit = entityDao.createCriteria(Tab.class);
		// Prepares Section table information.
		crit = crit.createAlias("sections", "tabSections");

		if (searchWidgetsFlag) {
    		// Prepares Widget table information.
    		crit = crit.createAlias("sections.widgets", "sectionWidgets");
		}
		
		// Sets retrieving Section in eager where tab id equals input id and order by 'orderNumber' ascendantly.
		crit = crit.add(Restrictions.eq("id", id)).setFetchMode("sections", FetchMode.JOIN).addOrder(Order.asc("tabSections.orderNumber"));
		
		if (searchWidgetsFlag) {
    		// Sets retrieving  Widget in eager and order by 'columnId' and 'orderNumber' ascendantly.
    		crit = crit.setFetchMode("sectionWidgets", FetchMode.JOIN).addOrder(Order.asc("sectionWidgets.columnId")).addOrder(Order.asc("sectionWidgets.orderNumber"));
		}
		
		Tab tab = null;
		List<Tab> list = crit.setCacheable(true).list();
		if (list != null && !list.isEmpty()) {
			tab = (Tab) crit.setCacheable(true).list().get(0);			
		}
		return tab;
	}
}
