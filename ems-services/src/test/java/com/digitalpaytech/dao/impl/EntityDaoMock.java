package com.digitalpaytech.dao.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CreditCardType;

public class EntityDaoMock implements EntityDao {
    private List<CreditCardType> creditCardTypes;
    
    @Override
    public <T> List<T> loadAll(Class<T> entityClass) {
        if (entityClass.getTypeName().equals(CreditCardType.class.getName())) {
            return (List<T>) this.creditCardTypes;
        }
        throw new IllegalStateException(entityClass.getTypeName() + " is not supported");
    }

    @Override
    public void evict(Object obj) {
        // Do nothing
    }

    public void setCreditCardTypes(final List<CreditCardType> creditCardTypes) {
        this.creditCardTypes = creditCardTypes;
    }
    
    @Override
    public SQLQuery createSQLQuery(String queryStr) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void delete(Object entity) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public List findByNamedQuery(String queryName) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List findByNamedQuery(String queryName, boolean cacheable) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List findByNamedQueryWithLock(String queryName, String lockedAlias) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object findUniqueByNamedQueryWithLock(String queryName, String lockedAlias) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List findByNamedQueryAndNamedParamWithLock(String queryName, String paramName, Object value, String lockedAlias) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object findUniqueByNamedQueryAndNamedParamWithLock(String queryName, String paramName, Object value, String lockedAlias) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List findByNamedQueryAndNamedParam(String queryName, String paramName, Object value) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List findByNamedQueryAndNamedParam(String queryName, String paramName, Object value, boolean cacheable) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List findByNamedQueryAndNamedParam(String queryName, String[] paramNames, Object[] values, int startingResult, int maxResult,
        boolean cacheable) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List findByNamedQueryAndNamedParam(String queryName, String[] paramNames, Object[] values) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List findByNamedQueryAndNamedParam(String queryName, String[] paramNames, Object[] values, boolean cacheable) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List findByNamedQueryAndNamedParamWithLimit(String queryName, String[] paramNames, Object[] values, boolean cacheable, int limit) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object findUniqueByNamedQueryAndNamedParam(String queryName, String paramName, Object value) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object findUniqueByNamedQueryAndNamedParam(String queryName, String paramName, Object value, boolean cacheable) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object findUniqueByNamedQueryAndNamedParam(String queryName, String[] paramNames, Object[] values, boolean cacheable) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void flush() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public <T> T get(Class<T> entityClass, Serializable id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> T get(Class<T> entityClass, Serializable id, LockMode lockMode) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Query getNamedQuery(String queryName) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> List<T> loadWithLimit(Class<T> entityClass, int limit, boolean isCacheable) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> T merge(T entity) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Serializable save(Object entity) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void saveOrUpdate(Object entity) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void saveOrUpdateAll(Collection entities) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void update(Object entity) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public List findPageResultByNamedQuery(String queryName, String[] paramNames, Object[] values, boolean isCacheable, int page, int pageSize) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int findTotalResultByNamedQuery(String queryName, String[] names, Object[] values) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int findTotalResultByNamedQueryWithLock(String queryName, String[] names, Object[] values, String alias) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int deleteAllByNamedQuery(String queryName, Object[] values) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public List findByNamedQuery(String queryName, boolean isCacheable, int startRow, int howManyRows) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List findByNamedQuery(String queryName, Object[] values, int startRow, int howManyRows) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List findByNamedQuery(String queryName, String[] names, Object[] values, int startRow, int howManyRows) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int updateByNamedQuery(String queryName, String[] names, Object[] values) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public Session getCurrentSession() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Criteria createCriteria(Class clazz) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean clearFromCache(Class<?> entityClass, Serializable id) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean clearFromCache(Object entity) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void clearFromCache(Query query) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void bringToCache(Object entity) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void bringToCache(Query query) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void clearNamedQuery(String cacheRegion) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void clearCollection(String cacheRegion) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void clearEntity(String entityName) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public Criterion nullableAnd(Criterion mainCondition, Criterion additionalCondition) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Criterion nullableOr(Criterion mainCondition, Criterion additionalCondition) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void initialize(Object proxy) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void deleteByNamedQueryAndNamedParams(String queryName, String[] paramNames, Object[] values) {
        // TODO Auto-generated method stub
        
    }

}
