package com.digitalpaytech.util;

import org.junit.*;
import static org.junit.Assert.*;
import java.util.*;
import org.apache.log4j.Logger;

import com.digitalpaytech.domain.Coupon;
import com.digitalpaytech.dto.paystation.TransactionDto;

public class CouponUtilTest {
    private static Logger log = Logger.getLogger(CouponUtilTest.class);
    
    @Test
    public void isFromDayBefore() {
        GregorianCalendar cal = new GregorianCalendar();
        
        log.info(cal.getTime());
        boolean b = CouponUtil.isFromDayBefore(cal.getTime(), "GMT");
        assertFalse(b);

        
        cal.add(Calendar.DATE, -1);
        log.info(cal.getTime());
        b = CouponUtil.isFromDayBefore(cal.getTime(),  "GMT");
        assertTrue(b);
    }
    
    @Test
    public void isDiscountValueChanged() {
        Coupon c = new Coupon();
        c.setPercentDiscount((short) 10);
        
        TransactionDto dto = new TransactionDto();
        dto.setCouponPercent("10");
        
        assertFalse(CouponUtil.isDiscountValueChanged(c, dto));
        //------------------------------------------------
        
        // coupon percent discount is 10
        dto.setCouponPercent("25");
        assertTrue(CouponUtil.isDiscountValueChanged(c, dto));
        
        dto.setCouponPercent(null);
        assertTrue(CouponUtil.isDiscountValueChanged(c, dto));
        
        dto.setCouponPercent("0");
        assertTrue(CouponUtil.isDiscountValueChanged(c, dto));
        //------------------------------------------------
        
        // coupon dollar discount is 5.25
        c.setPercentDiscount((short) 0);
        c.setDollarDiscountAmount(525);
        dto.setCouponAmount("5.25");

        assertFalse(CouponUtil.isDiscountValueChanged(c, dto));
        
        dto.setCouponAmount("3.75");
        assertTrue(CouponUtil.isDiscountValueChanged(c, dto));
        
        dto.setCouponAmount(null);
        assertTrue(CouponUtil.isDiscountValueChanged(c, dto));
        
        dto.setCouponAmount("0");
        assertTrue(CouponUtil.isDiscountValueChanged(c, dto));
    }
    
    @Test
    public void isDiscountValueChangedEms() {
        Coupon c = new Coupon();
        c.setPercentDiscount((short) 10);
        assertFalse(CouponUtil.isDiscountValueChanged(c, "10", null));
        assertTrue(CouponUtil.isDiscountValueChanged(c, "25", ""));
        assertTrue(CouponUtil.isDiscountValueChanged(c, null, ""));
        assertTrue(CouponUtil.isDiscountValueChanged(c, "0", null));
        
        c.setPercentDiscount((short) 0);
        c.setDollarDiscountAmount(525);
        assertFalse(CouponUtil.isDiscountValueChanged(c, "", "5.25"));
        assertTrue(CouponUtil.isDiscountValueChanged(c, null, "3.75"));
        assertTrue(CouponUtil.isDiscountValueChanged(c, null, null));
        assertTrue(CouponUtil.isDiscountValueChanged(c, "25", null));
        assertTrue(CouponUtil.isDiscountValueChanged(c, "", "0"));
    }
    
    
    @Test
    public void isDiscountValueChangedPercentDollar() {
        Coupon c = new Coupon();
        c.setPercentDiscount((short) 10);
        assertFalse(CouponUtil.isDiscountValueChanged(c, (short) 10, 0));
        assertTrue(CouponUtil.isDiscountValueChanged(c, (short) 25, 0));
        assertTrue(CouponUtil.isDiscountValueChanged(c, (short) 0, 0));
        
        c.setPercentDiscount((short) 0);
        c.setDollarDiscountAmount(525);
        assertFalse(CouponUtil.isDiscountValueChanged(c, (short) 0, 525));
        assertTrue(CouponUtil.isDiscountValueChanged(c, (short) 0, 375));
        assertTrue(CouponUtil.isDiscountValueChanged(c, (short) 0, (short) 0));
    }
}
