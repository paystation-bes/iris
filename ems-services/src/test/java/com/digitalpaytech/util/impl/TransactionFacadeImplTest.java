package com.digitalpaytech.util.impl;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;
import org.apache.log4j.Logger;
import org.apache.commons.lang.StringUtils;

import java.util.*;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.PaymentType;
import com.digitalpaytech.domain.PermitIssueType;
import com.digitalpaytech.domain.PermitType;
import com.digitalpaytech.domain.TransactionType;
import com.digitalpaytech.domain.UnifiedRate;
import com.digitalpaytech.domain.MobileNumber;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.impl.CustomerCardTypeServiceImpl;
import com.digitalpaytech.service.PaymentTypeService;
import com.digitalpaytech.service.impl.PaymentTypeServiceImpl;
import com.digitalpaytech.service.TransactionTypeService;
import com.digitalpaytech.service.impl.TransactionTypeServiceImpl;
import com.digitalpaytech.service.UnifiedRateService;
import com.digitalpaytech.service.impl.UnifiedRateServiceImpl;
import com.digitalpaytech.service.PermitTypeService;
import com.digitalpaytech.service.impl.PermitTypeServiceImpl;
import com.digitalpaytech.service.PermitIssueTypeService;
import com.digitalpaytech.service.impl.PermitIssueTypeServiceImpl;
import com.digitalpaytech.service.MobileNumberService;
import com.digitalpaytech.service.impl.MobileNumberServiceImpl;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.impl.TransactionFacadeImpl;

public class TransactionFacadeImplTest {
    private static Logger log = Logger.getLogger(TransactionFacadeImplTest.class);
    
    private CustomerCardTypeService customerCardTypeService;
    private PaymentTypeService paymentTypeService;
    private TransactionTypeService transactionTypeService;
    private UnifiedRateService unifiedRateService;
    private PermitTypeService permitTypeService;
    private PermitIssueTypeService permitIssueTypeService;
    private MobileNumberService mobileNumberService;
    private TransactionFacadeImpl transactionHelper;
    
    
    @Before
    public void setUp() {
        customerCardTypeService = new CustomerCardTypeServiceImpl() {           
            @Override
            public CustomerCardType getCardTypeByTrack2Data(int customerId, String track2Data) {
                CustomerCardType custCardType;
                List<CustomerCardType> list = createCustomerCardTypes();
                Iterator<CustomerCardType> iter = list.iterator();
                while (iter.hasNext()) {
                    custCardType = iter.next();
                    if (track2Data != null && StringUtils.isNotBlank(custCardType.getTrack2pattern()) &&
                        custCardType.getCustomer().getId() == customerId && custCardType.getTrack2pattern().equals(track2Data)) {
                        return custCardType;
                    }
                }
                return null;
            }
        };

        paymentTypeService = new PaymentTypeServiceImpl() {
            @Override
            public List<PaymentType> loadAll() {
                return createPaymentTypes();
            }

            @Override
            public Map<Byte, PaymentType> getPaymentTypesMap() {
                List<PaymentType> list = loadAll();
                Map<Byte, PaymentType> paymentTypesMap = new HashMap<Byte, PaymentType>();
                paymentTypesMap = new HashMap<Byte, PaymentType>(list.size());
                Iterator<PaymentType> iter = list.iterator();
                while (iter.hasNext()) {
                    PaymentType paymentType = iter.next();
                    paymentTypesMap.put(paymentType.getId(), paymentType);
                }
                return paymentTypesMap;
            }
        };
        
        transactionTypeService = new TransactionTypeServiceImpl() {
            @Override
            public TransactionType findTransactionType(String name) {
                Map<String, TransactionType> map = createTransactionTypeNamesMap();
                return map.get(name.toLowerCase());
            }
        };
        
        unifiedRateService = new UnifiedRateServiceImpl() {
            @Override
            public UnifiedRate findRateByNameAndCustomerId(String unifiedRateName, Integer customerId) {
                Customer cust = new Customer();
                if (unifiedRateName.equals("Unknown") && customerId == 3) {
                    cust.setId(3);
                    return new UnifiedRate(cust, "Unknown", new Date(), 1);
                } else if (unifiedRateName.equals("Unknown") && customerId == 5) {
                    cust.setId(5);
                    return new UnifiedRate(cust, "Unknown", new Date(), 1);
                } else if (unifiedRateName.equals("Morning") && customerId == 3) {
                    cust.setId(3);
                    return new UnifiedRate(cust, "Morning", new Date(), 1);
                } else if (unifiedRateName.equals("Free Day") && customerId == 5) {
                    cust.setId(5);
                    return new UnifiedRate(cust, "Free Day", new Date(), 1);
                } else {
                    return null;
                }
            }
        };
        
        permitTypeService = new PermitTypeServiceImpl() {
            @Override
            public PermitType findPermitType(String name) {
                Map<String, PermitType> map = createPermitTypeNamesMap();
                return map.get(name.toLowerCase());
            }
        };
        
        permitIssueTypeService = new PermitIssueTypeServiceImpl() {
            @Override
            public PermitIssueType findPermitIssueType(String licensePlate, String stallNumber) {
                if (StringUtils.isNotBlank(licensePlate) && StringUtils.isNotBlank(stallNumber)) {
                    StringBuilder bdr = new StringBuilder();
                    bdr.append("PermitIssueTypeServiceImpl, findPermitIssueType, cannot have both licensePlate and stallNumber, licensePlate: ").append(licensePlate);
                    bdr.append(", stallNumber: ").append(stallNumber);
                    throw new RuntimeException(bdr.toString());
                }
                
                if (StringUtils.isNotBlank(licensePlate)) {
                    return new PermitIssueType(2, "Pay by Plate", new Date(), 1);
                } else if (StringUtils.isNotBlank(stallNumber)) {
                    return new PermitIssueType(3, "Pay by Space", new Date(), 1);
                } else {
                    return new PermitIssueType(1, "Pay & Display", new Date(), 1);
                }
            }
            
        };
        
        mobileNumberService = new MobileNumberServiceImpl() {
            @Override
            public MobileNumber findMobileNumber(String number) {
                if (StringUtils.isBlank(number)) {
                    return null;
                }
                if (number.equals("6041111111")) {
                    return new MobileNumber("6041111111", new Date());
                } else {
                    return null;
                }
            }
        };
        
        transactionHelper = new TransactionFacadeImpl();
        transactionHelper.setCustomerCardTypeService(customerCardTypeService);
        transactionHelper.setPaymentTypeService(paymentTypeService);
        transactionHelper.setTransactionTypeService(transactionTypeService);
        transactionHelper.setUnifiedRateService(unifiedRateService);
        transactionHelper.setPermitTypeService(permitTypeService);
        transactionHelper.setPermitIssueTypeService(permitIssueTypeService);
        transactionHelper.setMobileNumberService(mobileNumberService);
    }
    
    @Test
    public void findPermitIssueType() {
        assertEquals("Pay & Display", transactionHelper.findPermitIssueType("", null).getName());
        assertEquals("Pay by Plate", transactionHelper.findPermitIssueType("ABC123", null).getName());
        assertEquals("Pay by Space", transactionHelper.findPermitIssueType("", "2").getName());
    }

    @Test (expected = RuntimeException.class)
    public void findPermitIssueTypeWithLicenseAndStallNumber() {
        assertEquals("Pay & Display", transactionHelper.findPermitIssueType("ABC123", "2").getName());
    }    
    
    @Test
    public void findMobileNumber() {
        assertEquals("6041111111", transactionHelper.findMobileNumber("6041111111").getNumber());
        assertNull(transactionHelper.findMobileNumber(""));
        assertNull(transactionHelper.findMobileNumber(null));
        assertNull(transactionHelper.findMobileNumber("6049999999"));
        assertNull(transactionHelper.findMobileNumber("TEST"));
    }
    
    
    @Test
    public void findTransactionType() {
        assertEquals("Permit Extension", transactionHelper.findTransactionType("AddTime").getDescription());
        assertEquals("Permit", transactionHelper.findTransactionType("Regular").getDescription());
        assertEquals("Smart Card Recharge", transactionHelper.findTransactionType("SCRecharge").getDescription());
        assertEquals("Cancelled Purchase", transactionHelper.findTransactionType("Cancelled").getDescription());
        assertEquals("Test Purchase", transactionHelper.findTransactionType("Test").getDescription());
        assertEquals("Monthly Permit", transactionHelper.findTransactionType("Monthly").getDescription());
        assertEquals("Extend by Phone Permit Extension", transactionHelper.findTransactionType("AddTime EbP").getDescription());
        assertEquals("Extend by Phone Permit", transactionHelper.findTransactionType("Regular EbP").getDescription());
        assertNull(transactionHelper.findTransactionType("EbP"));
    }
    
    @Test
    public void findUnifiedRate() {
        assertEquals("Unknown", transactionHelper.findUnifiedRate(3, "Unknown").getName());
        assertEquals("Unknown", transactionHelper.findUnifiedRate(5, "Unknown").getName());
        assertEquals("Free Day", transactionHelper.findUnifiedRate(5, "Free Day").getName());
        assertNull(transactionHelper.findUnifiedRate(1, "Free"));
    }
    
    
    @Test
    public void findPermitType() { 
        assertEquals(5, transactionHelper.findPermitType("Monthly").getId());
        assertEquals("N/A", transactionHelper.findPermitType("N/A").getName());
        assertEquals("Regular", transactionHelper.findPermitType("Regular").getName());
        assertEquals(2, transactionHelper.findPermitType("Addtime").getId());
        assertEquals("Regular Extend by Phone", transactionHelper.findPermitType("Regular Extend by Phone").getName());
        assertEquals(4, transactionHelper.findPermitType("Addtime Extend by Phone").getId());
        assertNull(transactionHelper.findPermitType("Cancelled"));
        assertNull(transactionHelper.findPermitType("Test"));
        assertNull(transactionHelper.findPermitType("SCRecharge"));
    }
    

    
    private List<PaymentType> createPaymentTypes() {
        byte ca = 1;
        PaymentType cash = new PaymentType(ca, "Cash", "Cash Only", new Date(), 1);
        
        byte cc = 2;
        PaymentType credit = new PaymentType(cc, "Credit Card", "Credit Card Only", new Date(), 1);
        
        byte sc = 4;
        PaymentType smart = new PaymentType(sc, "Smart Card", "Smart Card Only", new Date(), 1);

        byte caCc = 5;
        PaymentType cashCc = new PaymentType(caCc, "Cash/CC", "Cash and Credit Card", new Date(), 1);
        
        byte caSc = 6;
        PaymentType cashSc = new PaymentType(caSc, "Cash/SC", "Cash and Smart Card", new Date(), 1);
        
        byte vc = 7;
        PaymentType value = new PaymentType(vc, "Value Card", "Value Card Only", new Date(), 1);

        byte caVc = 9;
        PaymentType caValue = new PaymentType(caVc, "Cash/Value", "Cash and Value Card", new Date(), 1);
        
        byte unk = 10;
        PaymentType unkn = new PaymentType(unk, WebCoreConstants.UNKNOWN, "Unknown Payment Type", new Date(), 1);
        
        List<PaymentType> list = new ArrayList<PaymentType>();
        list.add(cash);
        list.add(credit);
        list.add(smart);
        list.add(cashCc);
        list.add(value);
        list.add(caValue);
        list.add(unkn);
        list.add(cashSc);
        return list;
    }

    private List<CustomerCardType> createCustomerCardTypes() {
        Customer cust = new Customer();
        cust.setId(3);

        String name0 = "N/A";
        CustomerCardType custCardType0 = new CustomerCardType();
        custCardType0.setCustomer(cust);
        custCardType0.setCardType(createCardType(0, name0));
        custCardType0.setName(name0);
        custCardType0.setTrack2pattern(null);
        
        String name1 = "Credit Card";
        CustomerCardType custCardType1 = new CustomerCardType();
        custCardType1.setCustomer(cust);
        custCardType1.setCardType(createCardType(1, name1));
        custCardType1.setName(name1);
        custCardType1.setTrack2pattern(name1);
        
        String name2 = "Smart Card";
        CustomerCardType custCardType2 = new CustomerCardType();
        custCardType2.setCustomer(cust);
        custCardType2.setCardType(createCardType(2, name2));
        custCardType2.setName(name2);
        custCardType2.setTrack2pattern(name2);
        
        String name3 = "Value Card";
        CustomerCardType custCardType3 = new CustomerCardType();
        custCardType3.setCustomer(cust);
        custCardType3.setCardType(createCardType(3, name3));
        custCardType3.setName(name3);
        custCardType3.setTrack2pattern(name3);

        
        String name4 = "Cash/CC";
        CustomerCardType custCardType4 = new CustomerCardType();
        custCardType4.setCustomer(cust);
        custCardType4.setCardType(createCardType(4, name4));
        custCardType4.setName(name4);
        custCardType4.setTrack2pattern(name4);

        String name5 = "Cash/SC";
        CustomerCardType custCardType5 = new CustomerCardType();
        custCardType5.setCustomer(cust);
        custCardType5.setCardType(createCardType(5, name5));
        custCardType5.setName(name5);
        custCardType5.setTrack2pattern(name5);

        String name6 = "Cash/Value";
        CustomerCardType custCardType6 = new CustomerCardType();
        custCardType6.setCustomer(cust);
        custCardType6.setCardType(createCardType(7, name6));
        custCardType6.setName(name6);
        custCardType6.setTrack2pattern(name6);
        
        String name7 = WebCoreConstants.UNKNOWN;
        CustomerCardType custCardType7 = new CustomerCardType();
        custCardType7.setCustomer(cust);
        custCardType7.setCardType(createCardType(8, name7));
        custCardType7.setName(name7);
        custCardType7.setTrack2pattern(name7);
         
        
        List<CustomerCardType> list = new ArrayList<CustomerCardType>();
        list.add(custCardType0);
        list.add(custCardType1);
        list.add(custCardType2);
        list.add(custCardType3);
        list.add(custCardType4);
        list.add(custCardType5);
        list.add(custCardType6);
        list.add(custCardType7);
        return list;
    }
        
    private CardType createCardType(int id, String name) {
        return new CardType(id, name, new Date(), 1);
    }
    
    
    private TransactionType createTransactionType(Integer id, String name, String desc) {
        return new TransactionType(id, name, desc, new Date(), 1);
    }
    
    
    private Map<String, TransactionType> createTransactionTypeNamesMap() {
        Map<String, TransactionType> map = new HashMap<String, TransactionType>();
        map.put("addtime", createTransactionType(1, "AddTime", "Permit Extension"));
        map.put("regular", createTransactionType(2, "Regular", "Permit"));
        map.put("screcharge", createTransactionType(4, "SCRecharge", "Smart Card Recharge"));
        map.put("cancelled", createTransactionType(5, "Cancelled", "Cancelled Purchase"));
        map.put("test", createTransactionType(6, "Test", "Test Purchase"));
        map.put("monthly", createTransactionType(12, "Monthly", "Monthly Permit"));
        map.put("addtime ebp", createTransactionType(16, "AddTime EbP", "Extend by Phone Permit Extension"));
        map.put("regular ebp", createTransactionType(17, "Regular EbP", "Extend by Phone Permit"));
        return map;
    }
    
    private Map<String, PermitType> createPermitTypeNamesMap() {
        Map<String, PermitType> map = new HashMap<String, PermitType>();
        map.put("n/a", new PermitType(0, "N/A", new Date(), 1));
        map.put("regular", new PermitType(1, "Regular", new Date(), 1));
        map.put("addtime", new PermitType(2, "Addtime", new Date(), 1));
        map.put("regular extend by phone", new PermitType(3, "Regular Extend by Phone", new Date(), 1));
        map.put("addtime extend by phone", new PermitType(4, "Addtime Extend by Phone", new Date(), 1));
        map.put("monthly", new PermitType(5, "Monthly", new Date(), 1));
        return map;
    }
}
