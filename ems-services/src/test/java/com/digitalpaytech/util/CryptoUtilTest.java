package com.digitalpaytech.util;

import org.junit.Test;
import static org.junit.Assert.*;

public class CryptoUtilTest {

    @Test
    public void isDecryptedCardData() {
        String cardData = "4242424242424242";
        assertTrue(CryptoUtil.isDecryptedCardData(cardData));
        
        cardData = "4242424242424242=1901";
        assertTrue(CryptoUtil.isDecryptedCardData(cardData));
        
        cardData = "4242424242424242=1901123123123123";
        assertTrue(CryptoUtil.isDecryptedCardData(cardData));
        
        cardData = "424242Efsb4242424242=1901123123123123EF";
        assertFalse(CryptoUtil.isDecryptedCardData(cardData));
        
        cardData = "002002ABC";
        assertFalse(CryptoUtil.isDecryptedCardData(cardData));
        
        cardData = null;
        assertFalse(CryptoUtil.isDecryptedCardData(cardData));
        
        cardData = "";
        assertFalse(CryptoUtil.isDecryptedCardData(cardData));
    }
}
