package com.digitalpaytech.util;

import static org.junit.Assert.*;

import java.util.List;
import java.util.ArrayList;
import org.junit.Test;
import org.apache.log4j.Logger;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.RevenueType;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.TransactionType;
import com.digitalpaytech.domain.UnifiedRate;
import com.digitalpaytech.domain.WidgetChartType;
import com.digitalpaytech.domain.WidgetMetricType;
import com.digitalpaytech.domain.WidgetRangeType;
import com.digitalpaytech.domain.WidgetTierType;
import com.digitalpaytech.domain.WidgetFilterType;
import com.digitalpaytech.dto.DisplayType;
import com.digitalpaytech.dto.LocationType;
import com.digitalpaytech.dto.MetricType;
import com.digitalpaytech.dto.RangeType;
import com.digitalpaytech.dto.RevType;
import com.digitalpaytech.dto.RouteType;
import com.digitalpaytech.dto.TierType;
import com.digitalpaytech.dto.TxnType;
import com.digitalpaytech.dto.FilterType;
import com.digitalpaytech.dto.RateType;

public class WebWidgetUtilTest {
	private static Logger log = Logger.getLogger(WebWidgetUtilTest.class);
	
	@Test
	public void copyObject() {
		MetricType obj = new MetricType();
		obj.setName("Original");
		
		MetricType dest = new MetricType();
		assertNull(dest.getName());
		
		WebWidgetUtil.copyObject(dest, obj);
		assertNotNull(dest.getName());
		assertEquals("Original", dest.getName());
	}
	

	@Test
	public void copyToLocationTypes() {
		Customer c = new Customer();
		c.setId(1);
		
	    Location lo1 = new Location();
		lo1.setIsDeleted(false);
		lo1.setIsUnassigned(false);
		lo1.setName("LOC 1");
		lo1.setNumberOfSpaces(50);
		lo1.setCustomer(c);
		
		Location lo2 = new Location();
		lo2.setIsDeleted(false);
		lo2.setIsUnassigned(true);
		lo2.setName("2 LOC 2");
		lo2.setNumberOfSpaces(10);
		lo2.setCustomer(c);
		
		Location lo3 = new Location();
		lo3.setIsDeleted(true);
		lo3.setIsUnassigned(true);
		lo3.setName("3 LOC 3");
		lo3.setNumberOfSpaces(300);
		lo3.setCustomer(c);
		
		List<Location> list = new ArrayList<Location>();
		list.add(lo1);
		list.add(lo2);
		list.add(lo3);
		
		List<LocationType> newList = WebWidgetUtil.copyToLocationTypes(list);
		assertNotNull(newList);
		assertEquals(3, newList.size());
		
		//------------------ 1-----------------------------
		LocationType type = newList.get(0);
		assertEquals("LOC 1", type.getName());
		
		//------------------ 2-----------------------------
		type = newList.get(1);
		assertEquals("2 LOC 2", type.getName());

		//------------------ 3-----------------------------
		type = newList.get(2);
		assertEquals("3 LOC 3", type.getName());
	}

	@Test
	public void copyToMetricTypes() {
		WidgetMetricType type1 = new WidgetMetricType();
		type1.setName("METRIC TYPE 1");
		
		WidgetMetricType type2 = new WidgetMetricType();
		type2.setName("2222 METRIC TYPE 222");
		
		List<WidgetMetricType> list = new ArrayList<WidgetMetricType>();
		list.add(type1);
		list.add(type2);
		
		List<MetricType> newList = WebWidgetUtil.copyToMetricTypes(list);
		assertNotNull(newList);
		assertEquals(2, newList.size());
		
		MetricType mt = newList.get(1);
		assertEquals("2222 METRIC TYPE 222", mt.getName());
	}
	
	@Test
	public void copyToTierTypes() {
		WidgetTierType type1 = new WidgetTierType();
		type1.setName("111 WidgetTierType 111");
		type1.setId(3);
		
		WidgetTierType type2 = new WidgetTierType();
		type2.setName("2 WidgetTierType 2");
		type2.setId(55);
		
		WidgetTierType type3 = new WidgetTierType();
		type3.setName("WidgetTierType 3");
		type3.setId(12);
		
		WidgetTierType type4 = new WidgetTierType();
		type4.setName("4 WidgetTierType");
		type4.setId(9);
		
		List<WidgetTierType> list = new ArrayList<WidgetTierType>();
		list.add(type1);
		list.add(type2);
		list.add(type3);
		list.add(type4);

		List<TierType> newList = WebWidgetUtil.copyToTierTypes(list, false);
		assertNotNull(newList);
		assertEquals(4, newList.size());
		
		// 3rd obj
		TierType tt = newList.get(2);
		assertNotNull(tt);
		assertEquals("WidgetTierType 3", tt.getName());
		assertEquals(WebCoreConstants.ACTIVE, tt.getStatus());
		
		// 1st obj
		tt = newList.get(0);
		assertNotNull(tt);
		assertEquals(WebCoreConstants.ACTIVE, tt.getStatus());

		// 4th obj
		tt = newList.get(3);
		assertNotNull(tt);
		assertEquals("4 WidgetTierType", tt.getName());
		assertEquals(WebCoreConstants.ACTIVE, tt.getStatus());

		// 2nd obj
		tt = newList.get(1);
		assertNotNull(tt);
		assertEquals(WebCoreConstants.ACTIVE, tt.getStatus());
		
		// -------------------------------------------------------------------------
//		WidgetTierType typeP = new WidgetTierType();
//		typeP.setName("Parent WidgetTierType Parent");
//		typeP.setId(102);
//		
//		list = new ArrayList<WidgetTierType>();
//		list.add(typeP);
//		
//		newList = WebWidgetUtil.copyToTierTypes(list, true);
//		tt = newList.get(0);
//		assertNotNull(tt);
//		assertEquals(WebCoreConstants.IN_ACTIVE, tt.getStatus());
	}
	
	@Test
	public void copyToRevTypes() {
		RevenueType rev1 = new RevenueType();
		rev1.setLevel((byte)3);
		rev1.setName("RevenueType 1");
		
		RevenueType rev2 = new RevenueType();
		rev2.setLevel((byte)23);
		rev2.setName("2 RevenueType 2");
		
		List<RevenueType> list = new ArrayList<RevenueType>();
		list.add(rev1);
		list.add(rev2);
		
		List<RevType> newList = WebWidgetUtil.copyToRevTypes(list);
		assertNotNull(newList);
		assertEquals(2, newList.size());
		assertEquals(23, newList.get(1).getLevel());
	}
	
	@Test
	public void copyToRouteTypes() {
		Customer c = new Customer();
		c.setId(1);
	    Route r1 = new Route();
		r1.setName("F Route");
		r1.setCustomer(c);
		
		Route r2 = new Route();
		r2.setName("G Route");
		r2.setCustomer(c);
		
		List<Route> list = new ArrayList<Route>();
		list.add(r1);
		list.add(r2);
		
		List<RouteType> newList = WebWidgetUtil.copyToRouteTypes(list);
		assertNotNull(newList);
		assertEquals(2, newList.size());
		assertEquals("F Route", newList.get(0).getName());
		assertEquals("G Route", newList.get(1).getName());
	}

	@Test
	public void copyToTxnTypes() {
		TransactionType tt1 = new TransactionType();
		tt1.setName("TRANS 11");
		
		TransactionType tt2 = new TransactionType();
		tt2.setName("TRANS 22");
		
		List<TransactionType> list = new ArrayList<TransactionType>();
		list.add(tt1);
		list.add(tt2);

		List<TxnType> newList = WebWidgetUtil.copyToTxnTypes(list);
		assertNotNull(newList);
		assertEquals(2, newList.size());
		assertEquals("TRANS 22", newList.get(1).getName());
	}
	
	@Test
	public void copyToFilterTypes() {
		WidgetFilterType wt1 = new WidgetFilterType();
		wt1.setName("WidgetFilterType 111");

		WidgetFilterType wt2 = new WidgetFilterType();
		wt2.setName("2 WidgetFilterType 222");
		
		List<WidgetFilterType> list = new ArrayList<WidgetFilterType>();
		list.add(wt1);
		list.add(wt2);
		
		List<FilterType> newList = WebWidgetUtil.copyToFilterTypes(list);
		assertNotNull(newList);
		assertEquals(2, newList.size());
		assertEquals("WidgetFilterType 111", newList.get(0).getName());
		assertEquals("2 WidgetFilterType 222", newList.get(1).getName());
	}
	
	@Test
	public void copyToDisplayTypes() {
		WidgetChartType wt1 = new WidgetChartType();
		wt1.setName("111 WidgetChartType 111");

		WidgetChartType wt2 = new WidgetChartType();
		wt2.setName("222 WidgetChartType 222");
		
		List<WidgetChartType> list = new ArrayList<WidgetChartType>();
		list.add(wt1);
		list.add(wt2);
		
		List<DisplayType> newList = WebWidgetUtil.copyToDisplayTypes(list);
		assertNotNull(newList);
		assertEquals(2, newList.size());
		assertEquals("111 WidgetChartType 111", newList.get(0).getName());
		assertEquals("222 WidgetChartType 222", newList.get(1).getName());
	}	
    
	@Test
	public void copyToRateTypes() {
	    Customer c = new Customer();
        c.setId(1);
	    UnifiedRate r1 = new UnifiedRate();
		r1.setName("Afternoon");
		r1.setCustomer(c);
		
		UnifiedRate r2 = new UnifiedRate();
		r2.setName("Early Bird");
		r2.setCustomer(c);
		
		List<UnifiedRate> list = new ArrayList<UnifiedRate>();
		list.add(r1);
		list.add(r2);
		
		List<RateType> newList = WebWidgetUtil.copyToRateTypes(list);
		assertNotNull(newList);
		assertEquals(2, newList.size());
		assertEquals("Afternoon", newList.get(0).getName());
		assertEquals("Early Bird", newList.get(1).getName());
	}
	
	@Test
	public void copyToRangeTypes() {
		WidgetRangeType wrt1 = new WidgetRangeType();
		wrt1.setRandomId("RandomId1");
		wrt1.setName("Now");
		
		WidgetRangeType wrt2 = new WidgetRangeType();
		wrt2.setRandomId("RandomId5");
		wrt2.setName("Last 30 Days");
		
		WidgetRangeType wrt3 = new WidgetRangeType();
		wrt3.setRandomId("RandomId7");
		wrt3.setName("Last 12 Months");
		
		List<WidgetRangeType> list = new ArrayList<WidgetRangeType>();
		list.add(wrt1);
		list.add(wrt2);
		list.add(wrt3);
		
		List<RangeType> newList = WebWidgetUtil.copyToRangeTypes(list);
		assertNotNull(newList);
		assertEquals(3, newList.size());
		assertEquals("Now", newList.get(0).getName());
		assertEquals("RandomId1", newList.get(0).getRandomId());
		assertEquals("Last 30 Days", newList.get(1).getName());
		assertEquals("RandomId7", newList.get(2).getRandomId());
		assertEquals("Last 12 Months", newList.get(2).getName());
	}
}
