package com.digitalpaytech.util;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.AbstractMap;
import org.apache.commons.lang.StringUtils;

import com.digitalpaytech.domain.AlertThresholdType;
import com.digitalpaytech.domain.AlertType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerAlertEmail;
import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.CustomerEmail;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.RoutePOS;
import com.digitalpaytech.util.StandardConstants;

public enum EntityMap {
    INSTANCE;    
    private final Map<String, Object> objectMap = new HashMap<>();

    /**
     * 
     * @param key e.g. "PointOfSale_1" - class name, underscore, object id
     * @return
     */
    public final List<PointOfSale> findDefaultPointOfSales(final int count) {
        final List<PointOfSale> list = new ArrayList<>(count);
        for (int i = StandardConstants.CONSTANT_1; i < count + StandardConstants.CONSTANT_1; i++) {
            final String key = createKey(PointOfSale.class, i);
            if (this.objectMap.get(key) == null) {
                createDefaultPointOfSales(i);
            }
            list.add((PointOfSale) this.objectMap.get(key));
        }
        return list;
    }
    
    public final PointOfSale findDefaultPointOfSale() {
        final List<PointOfSale> list = findDefaultPointOfSales(StandardConstants.CONSTANT_1);
        return list.get(0);   
    }
    
    public final List<CustomerAlertType> findDefaultCustomerAlertTypes() {
        final String key = createKey(CustomerAlertType.class, StandardConstants.CONSTANT_1);
        if (this.objectMap.get(key) == null) {
            createPayStationAlertThresholdTypes();
            createDefaultCustomerAlertTypes();
        }
        final List<CustomerAlertType> list = new ArrayList<>(3);
        list.add((CustomerAlertType) this.objectMap.get(createKey(CustomerAlertType.class, StandardConstants.CONSTANT_1)));
        list.add((CustomerAlertType) this.objectMap.get(createKey(CustomerAlertType.class, StandardConstants.CONSTANT_2)));
        list.add((CustomerAlertType) this.objectMap.get(createKey(CustomerAlertType.class, StandardConstants.CONSTANT_3)));
        return list;
    }
    
    public final List<Route> findDefaultRoutes() {
        final List<Route> list = new ArrayList<>(2);
        list.add((Route) this.objectMap.get(createKey(Route.class, StandardConstants.CONSTANT_1)));
        list.add((Route) this.objectMap.get(createKey(Route.class, StandardConstants.CONSTANT_2)));
        return list;
    }
    
    public final Location findDefaultLocation() {
        final String key = createKey(Location.class, StandardConstants.CONSTANT_1);
        if (this.objectMap.get(key) == null) {
            createDefaultLocation();
        }
        return (Location) this.objectMap.get(createDefaultKey(Location.class));
    }
    
    private void createDefaultPointOfSales(final int count) {
        createDefaultCustomer();
        createDefaultLocation();
        createDefaultRoutes();
        
        for (int i = 1; i < count + 1; i++) {
            createPaystation(i);
        
            final PointOfSale pos = new PointOfSale();
            final RoutePOS rpos1 = new RoutePOS();
            rpos1.setId(Long.valueOf(StandardConstants.STRING_ONE));
            rpos1.setPointOfSale(pos);
            rpos1.setRoute((Route) this.objectMap.get(createKey(Route.class, i)));
        
            final RoutePOS rpos2 = new RoutePOS();
            rpos2.setId(Long.valueOf("2"));
            rpos2.setPointOfSale(pos);
            rpos2.setRoute((Route) this.objectMap.get(createKey(Route.class, i + 1)));

            final Set<RoutePOS> rposSet = new HashSet<>();
            rposSet.add(rpos1);
            rposSet.add(rpos2);
        
            pos.setId(i);
            pos.setIsLinux(false);
            pos.setName("PointOfSale_" + i);
            pos.setCustomer((Customer) this.objectMap.get(createDefaultKey(Customer.class)));
            pos.setLocation((Location) this.objectMap.get(createDefaultKey(Location.class)));
            pos.setPaystation((Paystation) this.objectMap.get(createDefaultKey(Paystation.class)));
            pos.setRoutePOSes(rposSet);

            final PosServiceState pss = new PosServiceState();
            pss.setPointOfSaleId(pos.getId());
            pos.setPosServiceState(pss);
            this.objectMap.put(createDefaultKey(PointOfSale.class, i), pos);
        }
    }
    
    private void createDefaultCustomer() {
        final Customer cust = new Customer();
        cust.setId(StandardConstants.CONSTANT_1);
        cust.setName("Mackenzie Parking");
        this.objectMap.put(createDefaultKey(Customer.class), cust);
    }
    
    private void createPaystation(final int count) {
        final Paystation ps = new Paystation();
        ps.setId(count);
        this.objectMap.put(createDefaultKey(Paystation.class), ps);
    }
    
    private void createDefaultLocation() {
        final Location loc = new Location();
        loc.setId(StandardConstants.CONSTANT_1);
        loc.setName("ThisIsALocation");
        this.objectMap.put(createDefaultKey(Location.class), loc);
    }
    
    private void createDefaultRoutes() {
        final Route route1 = new Route();
        route1.setId(StandardConstants.CONSTANT_1);
        route1.setName("hasPaystation");
        this.objectMap.put(createKey(Route.class, StandardConstants.CONSTANT_1), route1);
        
        final Route route2 = new Route();
        route2.setId(StandardConstants.CONSTANT_2);
        route2.setName("emptyRoute");
        this.objectMap.put(createKey(Route.class, StandardConstants.CONSTANT_2), route2);
    }
    
    private void createPayStationAlertThresholdTypes() {
        final AlertThresholdType attype1 = new AlertThresholdType();
        final AlertType ty1 = new AlertType();
        ty1.setId((short) 6);
        ty1.setName("Pay Station Alert");
        attype1.setAlertType(ty1);
        attype1.setId(12);
        this.objectMap.put(createKey(AlertThresholdType.class, StandardConstants.CONSTANT_1), attype1);
        
        final AlertThresholdType attype2 = new AlertThresholdType();
        final AlertType ty2 = new AlertType();
        ty2.setId((short) 6);
        ty2.setName("Pay Station Alert");
        attype2.setAlertType(ty2);
        attype2.setId(12);
        this.objectMap.put(createKey(AlertThresholdType.class, StandardConstants.CONSTANT_2), attype2);
        
        final AlertThresholdType attype3 = new AlertThresholdType();
        final AlertType ty3 = new AlertType();
        ty3.setId((short) 6);
        ty3.setName("Pay Station Alert");
        attype3.setAlertType(ty3);
        attype3.setId(12);
        this.objectMap.put(createKey(AlertThresholdType.class, StandardConstants.CONSTANT_3), attype3);        
    }
    
    private void createDefaultCustomerAlertTypes() {
        if (this.objectMap.get(createKey(AlertThresholdType.class, StandardConstants.CONSTANT_1)) == null) {
            createPayStationAlertThresholdTypes();
        }
                           
        final Customer customer = (Customer) this.objectMap.get(createDefaultKey(Customer.class));
        final CustomerAlertType customerAlertType1 = new CustomerAlertType();
        customerAlertType1.setAlertThresholdType((AlertThresholdType) this.objectMap.get(createKey(AlertThresholdType.class, StandardConstants.CONSTANT_1)));
        customerAlertType1.setCustomer(customer);
        final CustomerAlertEmail alertEmail1 = new CustomerAlertEmail();
        alertEmail1.setCustomerEmail(new CustomerEmail(customer, "includes@test.com"));
        final Set<CustomerAlertEmail> eSet1 = new HashSet<>();
        eSet1.add(alertEmail1);
        customerAlertType1.setCustomerAlertEmails(eSet1);
        customerAlertType1.setId(StandardConstants.CONSTANT_1);
        customerAlertType1.setIsActive(true);
        customerAlertType1.setLocation((Location) this.objectMap.get(createKey(Location.class, StandardConstants.CONSTANT_1)));
        customerAlertType1.setName("withPaystation");
        customerAlertType1.setId(12);
        customerAlertType1.setRoute((Route) this.objectMap.get(createKey(Route.class, StandardConstants.CONSTANT_1)));
        customerAlertType1.setAlertThresholdType((AlertThresholdType) this.objectMap.get(createKey(AlertThresholdType.class, StandardConstants.CONSTANT_1)));
        this.objectMap.put(createKey(CustomerAlertType.class, StandardConstants.CONSTANT_1), customerAlertType1);
        
        final CustomerAlertType customerAlertType2 = new CustomerAlertType();
        customerAlertType2.setAlertThresholdType((AlertThresholdType) this.objectMap.get(createKey(AlertThresholdType.class, StandardConstants.CONSTANT_2)));
        customerAlertType2.setCustomer(customer);
        final CustomerAlertEmail alertEmail2 = new CustomerAlertEmail();
        alertEmail2.setCustomerEmail(new CustomerEmail(customer, "notincludes@test.com"));
        final Set<CustomerAlertEmail> eSet2 = new HashSet<>();
        eSet1.add(alertEmail2);
        customerAlertType2.setCustomerAlertEmails(eSet2);
        customerAlertType2.setId(StandardConstants.CONSTANT_2);
        customerAlertType2.setIsActive(true);
        customerAlertType2.setLocation((Location) this.objectMap.get(createKey(Location.class, StandardConstants.CONSTANT_2)));
        customerAlertType2.setName("noPaystation");
        customerAlertType2.setId(12);
        customerAlertType2.setRoute((Route) this.objectMap.get(createKey(Route.class, StandardConstants.CONSTANT_2)));
        customerAlertType2.setAlertThresholdType((AlertThresholdType) this.objectMap.get(createKey(AlertThresholdType.class, StandardConstants.CONSTANT_2)));
        this.objectMap.put(createKey(CustomerAlertType.class, StandardConstants.CONSTANT_2), customerAlertType2);
        
        final CustomerAlertType customerAlertType3 = new CustomerAlertType();
        customerAlertType3.setAlertThresholdType((AlertThresholdType) this.objectMap.get(createKey(AlertThresholdType.class, StandardConstants.CONSTANT_3)));
        customerAlertType3.setCustomer(customer);
        customerAlertType3.setId(StandardConstants.CONSTANT_3);
        customerAlertType3.setIsActive(true);
        customerAlertType3.setName("allPaystations");
        customerAlertType3.setId(12);
        customerAlertType3.setAlertThresholdType((AlertThresholdType) this.objectMap.get(createKey(AlertThresholdType.class, StandardConstants.CONSTANT_3)));
        this.objectMap.put(createKey(CustomerAlertType.class, StandardConstants.CONSTANT_3), customerAlertType3);
    }
    
    public String createDefaultKey(final Class<?> clazz) {
        return createKey(clazz, StandardConstants.CONSTANT_1);
    }
    
    public String createDefaultKey(final Class<?> clazz, final Integer count) {
        return createKey(clazz, count);
    }
    
    public String createKey(final Class<?> clazz, final Integer id) {
        return clazz.getSimpleName() + StandardConstants.CHAR_UNDERSCORE + id;
    }
}
