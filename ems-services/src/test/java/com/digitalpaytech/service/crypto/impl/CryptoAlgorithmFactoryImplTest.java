package com.digitalpaytech.service.crypto.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.digitalpaytech.domain.HashAlgorithmType;
import com.digitalpaytech.dto.crypto.HashAlgorithmData;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.HashAlgorithmTypeService;
import com.digitalpaytech.service.crypto.CryptoAlgorithm;
import com.digitalpaytech.service.impl.EmsPropertiesServiceImpl;
import com.digitalpaytech.service.impl.HashAlgorithmTypeServiceImpl;
import com.digitalpaytech.util.WebSecurityConstants;

public class CryptoAlgorithmFactoryImplTest {
    private static Logger log = Logger.getLogger(CryptoAlgorithmFactoryImplTest.class);
    private HashAlgorithmTypeService hashAlgorithmTypeService;
    private EmsPropertiesService emsPropertiesService;
    
    @Test
    public void getShaHash() {
        CryptoAlgorithmFactoryImpl fac = new CryptoAlgorithmFactoryImpl();
        fac.setHashAlgorithmTypeService(hashAlgorithmTypeService);

        byte[] data = "CryptoAlgorithmFactoryImplTest".getBytes();
        int hashAlgorithmTypeId = 1; // SHA-1
        int hashType = -1; // Total of 6 byte array variables.
        
        String hash = null;
        try {
            hash = fac.getShaHash(data, hashType, hashAlgorithmTypeId);
        } catch (CryptoException ce) {
            // Pass
            ce.printStackTrace();
        } catch (Exception e) {
            // Fail
            e.printStackTrace();
            fail("should not throw Exception");
        }
        
        hashType = 7;
        try {
            hash = fac.getShaHash(data, hashType, hashAlgorithmTypeId);
        } catch (CryptoException ce) {
            // Pass
            ce.printStackTrace();
        } catch (Exception e) {
            // Fail
            e.printStackTrace();
            fail("should not throw Exception");
        }
        //---------------------------------------------------------------
        
        // SALT_BAD_CARD = 0
        hashType = 0;
        try {
            hash = fac.getShaHash(data, hashType, hashAlgorithmTypeId);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);
            
        } catch (Exception e) {
            e.printStackTrace();
            fail("should not throw Exception");
        }
        
        //---------------------------------------------------------------
        // SALT_PUBLIC_ENCRYPTION_KEY = 1
        hashType = 1;
        try {
            hash = fac.getShaHash(data, hashType, hashAlgorithmTypeId);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);
            
        } catch (Exception e) {
            e.printStackTrace();
            fail("should not throw Exception");
        }

        //---------------------------------------------------------------
        // SALT_CREDIT_CARD_PROCESSING = 2
        hashType = 2;
        // SHA-256 = hashAlgorithmTypeId 2
        hashAlgorithmTypeId = 2;
        try {
            hash = fac.getShaHash(data, hashType, hashAlgorithmTypeId);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);
            
        } catch (Exception e) {
            e.printStackTrace();
            fail("should not throw Exception");
        }

        //---------------------------------------------------------------
        // SALT_FILE_UPLOAD = 3
        hashType = 3;
        try {
            hash = fac.getShaHash(data, hashType, hashAlgorithmTypeId);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);
            
        } catch (Exception e) {
            e.printStackTrace();
            fail("should not throw Exception");
        }

        //---------------------------------------------------------------
        // SALT_IMPORT_ERROR = 4
        hashType = 4;
        try {
            hash = fac.getShaHash(data, hashType, hashAlgorithmTypeId);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);
            
        } catch (Exception e) {
            e.printStackTrace();
            fail("should not throw Exception");
        }

        //---------------------------------------------------------------
        // SALT_TRANSACTION_UPLOAD = 5
        hashType = 5;
        // SHA-1 = hashAlgorithmTypeId 2
        hashAlgorithmTypeId = 1;
        try {
            hash = fac.getShaHash(data, hashType, hashAlgorithmTypeId);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);
            
        } catch (Exception e) {
            e.printStackTrace();
            fail("should not throw Exception");
        }
    }
    
    
    @Test
    public void getShaHashWithCharSet() {
        CryptoAlgorithmFactoryImpl fac = new CryptoAlgorithmFactoryImpl();
        fac.setHashAlgorithmTypeService(hashAlgorithmTypeService);

        byte[] data = "CryptoAlgorithmFactoryImplTest".getBytes();
        int hashAlgorithmTypeId = 2; // SHA-256
        
        String hash = null;
        // SALT_BAD_CARD = 0
        int hashType = 0;
        try {
            hash = fac.getShaHash(data, hashType, WebSecurityConstants.URL_ENCODING_LATIN1, hashAlgorithmTypeId);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);
            
        } catch (Exception e) {
            e.printStackTrace();
            fail("should not throw Exception");
        }
        
        //---------------------------------------------------------------
        // SALT_PUBLIC_ENCRYPTION_KEY = 1
        hashType = 1;
        try {
            hash = fac.getShaHash(data, hashType, WebSecurityConstants.URL_ENCODING_LATIN1, hashAlgorithmTypeId);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);
            
        } catch (Exception e) {
            e.printStackTrace();
            fail("should not throw Exception");
        }

        //---------------------------------------------------------------
        // SALT_CREDIT_CARD_PROCESSING = 2
        hashType = 2;
        try {
            hash = fac.getShaHash(data, hashType, WebSecurityConstants.URL_ENCODING_LATIN1, hashAlgorithmTypeId);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);
            
        } catch (Exception e) {
            e.printStackTrace();
            fail("should not throw Exception");
        }

        //---------------------------------------------------------------
        // SALT_FILE_UPLOAD = 3
        hashType = 3;
        try {
            hash = fac.getShaHash(data, hashType, WebSecurityConstants.URL_ENCODING_LATIN1, hashAlgorithmTypeId);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);
            
        } catch (Exception e) {
            e.printStackTrace();
            fail("should not throw Exception");
        }

        //---------------------------------------------------------------
        // SALT_IMPORT_ERROR = 4
        hashType = 4;
        try {
            hash = fac.getShaHash(data, hashType, WebSecurityConstants.URL_ENCODING_LATIN1, hashAlgorithmTypeId);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);
            
        } catch (Exception e) {
            e.printStackTrace();
            fail("should not throw Exception");
        }

        //---------------------------------------------------------------
        // SALT_TRANSACTION_UPLOAD = 5
        hashType = 5;
        try {
            hash = fac.getShaHash(data, hashType, WebSecurityConstants.URL_ENCODING_LATIN1, hashAlgorithmTypeId);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);
            
        } catch (Exception e) {
            e.printStackTrace();
            fail("should not throw Exception");
        }
        
    }    
    

    
    @Test
    public void getShaHashWithFile() {
        CryptoAlgorithmFactoryImpl fac = new CryptoAlgorithmFactoryImpl();
        fac.setHashAlgorithmTypeService(hashAlgorithmTypeService);
        
        int hashAlgorithmTypeId = 2; // SHA-256
        File file = null;
        try {
            file = createFile();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
        
        String hash = null;
        // SALT_BAD_CARD = 0
        int hashType = 0;
        try {
            hash = fac.getShaHash(file, hashType, hashAlgorithmTypeId);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);
            
        } catch (Exception e) {
            e.printStackTrace();
            fail("should not throw Exception");
        }
        
        //---------------------------------------------------------------
        // SALT_PUBLIC_ENCRYPTION_KEY = 1
        hashType = 1;
        try {
            hash = fac.getShaHash(file, hashType, hashAlgorithmTypeId);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);
            
        } catch (Exception e) {
            e.printStackTrace();
            fail("should not throw Exception");
        }

        //---------------------------------------------------------------
        // SALT_CREDIT_CARD_PROCESSING = 2
        hashType = 2;
        try {
            hash = fac.getShaHash(file, hashType, hashAlgorithmTypeId);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);
            
        } catch (Exception e) {
            e.printStackTrace();
            fail("should not throw Exception");
        }

        //---------------------------------------------------------------
        // SALT_FILE_UPLOAD = 3
        hashType = 3;
        try {
            hash = fac.getShaHash(file, hashType, hashAlgorithmTypeId);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);
            
        } catch (Exception e) {
            e.printStackTrace();
            fail("should not throw Exception");
        }

        //---------------------------------------------------------------
        // SALT_IMPORT_ERROR = 4
        hashType = 4;
        try {
            hash = fac.getShaHash(file, hashType, hashAlgorithmTypeId);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);
            
        } catch (Exception e) {
            e.printStackTrace();
            fail("should not throw Exception");
        }

        //---------------------------------------------------------------
        // SALT_TRANSACTION_UPLOAD = 5
        hashType = 5;
        try {
            hash = fac.getShaHash(file, hashType, hashAlgorithmTypeId);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);
            
        } catch (Exception e) {
            e.printStackTrace();
            fail("should not throw Exception");
        }
        
    }    
    

    
    @Test
    public void getShaHashWithRawString() {
        CryptoAlgorithmFactoryImpl fac = new CryptoAlgorithmFactoryImpl();
        fac.setHashAlgorithmTypeService(hashAlgorithmTypeService);
        
        int hashAlgorithmTypeId = 2; // SHA-256
        String hash = null;
        String s = "This is a test";
        
        
        // SALT_BAD_CARD = 0
        int hashType = 0;
        try {
            hash = fac.getShaHash(s, hashType, hashAlgorithmTypeId);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);
            
        } catch (Exception e) {
            e.printStackTrace();
            fail("should not throw Exception");
        }
        
        //---------------------------------------------------------------
        // SALT_PUBLIC_ENCRYPTION_KEY = 1
        hashType = 1;
        try {
            hash = fac.getShaHash(s, hashType, hashAlgorithmTypeId);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);
            
        } catch (Exception e) {
            e.printStackTrace();
            fail("should not throw Exception");
        }

        //---------------------------------------------------------------
        // SALT_CREDIT_CARD_PROCESSING = 2
        hashType = 2;
        try {
            hash = fac.getShaHash(s, hashType, hashAlgorithmTypeId);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);
            
        } catch (Exception e) {
            e.printStackTrace();
            fail("should not throw Exception");
        }

        //---------------------------------------------------------------
        // SALT_FILE_UPLOAD = 3
        hashType = 3;
        try {
            hash = fac.getShaHash(s, hashType, hashAlgorithmTypeId);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);
            
        } catch (Exception e) {
            e.printStackTrace();
            fail("should not throw Exception");
        }

        //---------------------------------------------------------------
        // SALT_IMPORT_ERROR = 4
        hashType = 4;
        try {
            hash = fac.getShaHash(s, hashType, hashAlgorithmTypeId);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);
            
        } catch (Exception e) {
            e.printStackTrace();
            fail("should not throw Exception");
        }

        //---------------------------------------------------------------
        // SALT_TRANSACTION_UPLOAD = 5
        hashType = 5;
        try {
            hash = fac.getShaHash(s, hashType, hashAlgorithmTypeId);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);
            
        } catch (Exception e) {
            e.printStackTrace();
            fail("should not throw Exception");
        }
        
    }    
    
    @Test
    public void getSha1Hash() {
        CryptoAlgorithmFactoryImpl fac = new CryptoAlgorithmFactoryImpl();
        fac.setHashAlgorithmTypeService(hashAlgorithmTypeService);
        fac.setEmsPropertiesService(emsPropertiesService);
        
        //public String getSha1Hash(byte[] data, int hashType) throws CryptoException {
        String data = "This is test data";
        // SALT_BAD_CARD = 0
        int hashType = 0;

        String hash = null;
        try {
            hash = fac.getSha1Hash(data.getBytes(), hashType);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);            
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    
    @Test
    public void getSha1HashWithCharSet() {
        CryptoAlgorithmFactoryImpl fac = new CryptoAlgorithmFactoryImpl();
        fac.setHashAlgorithmTypeService(hashAlgorithmTypeService);
        fac.setEmsPropertiesService(emsPropertiesService);
        
        //public String getSha1Hash(byte[] data, int hashType) throws CryptoException {
        String data = "This is test data";
        // SALT_BAD_CARD = 0
        int hashType = 0;

        String hash = null;
        try {
            hash = fac.getSha1Hash(data.getBytes(), hashType, WebSecurityConstants.URL_ENCODING_LATIN1);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);            
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    } 

    
    @Test
    public void getSha1HashWithFile() {
        CryptoAlgorithmFactoryImpl fac = new CryptoAlgorithmFactoryImpl();
        fac.setHashAlgorithmTypeService(hashAlgorithmTypeService);
        fac.setEmsPropertiesService(emsPropertiesService);
        
        // SALT_BAD_CARD = 0
        int hashType = 0;

        File file = null;
        try {
            file = createFile();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
        
        
        String hash = null;
        try {
            hash = fac.getSha1Hash(file, hashType);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);            
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }    
    
    
    @Test
    public void getSha1HashWithRawString() {
        CryptoAlgorithmFactoryImpl fac = new CryptoAlgorithmFactoryImpl();
        fac.setHashAlgorithmTypeService(hashAlgorithmTypeService);
        fac.setEmsPropertiesService(emsPropertiesService);
        
        // SALT_BAD_CARD = 0
        int hashType = 0;

        String s = "THIS is a test.";
        String hash = null;
        try {
            hash = fac.getSha1Hash(s, hashType);
            assertNotNull(hash);
            assertTrue(hash.length() > 0);            
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }     
    
    @Before
    public void before() {
        hashAlgorithmTypeService = new HashAlgorithmTypeServiceImpl() {
            @Override
            public HashAlgorithmData findForMessageDigest(Integer hashAlgorithmTypeId) {
                HashAlgorithmType t = null;
                CryptoAlgorithm alg = null;
                if (hashAlgorithmTypeId == 1) {
                    // SHA-1
                    t = new HashAlgorithmType();
                    t.setId(1);
                    t.setName("SHA-1");
                    t.setIsDeleted(false);
                    t.setClassName("Sha1");
                    t.setSignatureId(3);
                    t.setIsForSigning(false);
                    
                    alg = new Sha1AlgorithmImpl();
                    
                } else if (hashAlgorithmTypeId == 2) {
                    // SHA-256
                    t = new HashAlgorithmType();
                    t.setId(2);
                    t.setName("SHA-256");
                    t.setIsDeleted(false);
                    t.setClassName("Sha256");
                    t.setSignatureId(4);
                    t.setIsForSigning(false);
                    
                    alg = new Sha256AlgorithmImpl();
                
                } else if (hashAlgorithmTypeId == 2) {
                    // SHA1withRSA
                    t = new HashAlgorithmType();
                    t.setId(3);
                    t.setName("SHA1withRSA");
                    t.setIsDeleted(false);
                    t.setClassName(null);
                    t.setIsForSigning(false);                    
                
                } else if (hashAlgorithmTypeId == 2) {
                    // SHA256withRSA
                    t = new HashAlgorithmType();
                    t.setId(4);
                    t.setName("SHA256withRSA");
                    t.setIsDeleted(false);
                    t.setClassName("");
                    t.setIsForSigning(false);                    
                }
                
                HashAlgorithmData data = new HashAlgorithmData(t, alg);
                return data;
            }
        };
        
        emsPropertiesService = new EmsPropertiesServiceImpl() {
            @Override
            public int getPropertyValueAsIntQuiet(String propertyName, int defaultValue) {
                return 1;   // SHA-1
            }
        };
    }
    
    
    private File createFile() throws IOException {
        String data = "This is a test";
        File testFile = new File("test.txt");
        FileUtils.writeByteArrayToFile(testFile, data.getBytes());
        return testFile;
    }
    
    @After
    public void deleteTestFile() {
        File testFile = new File("test.txt");
        if (testFile.exists()) {
            try {
                testFile.delete();
            } catch (Exception e) {
                log.error("Cannot delete test.txt, ", e);
            }
        }
    }

}
