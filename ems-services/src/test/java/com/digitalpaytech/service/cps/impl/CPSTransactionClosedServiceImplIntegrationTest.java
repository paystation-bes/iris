package com.digitalpaytech.service.cps.impl;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.apache.log4j.Logger;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Calendar;
import com.digitalpaytech.dto.cps.TransactionClosedMessage;
import com.digitalpaytech.domain.CPSData;
import com.digitalpaytech.domain.ElavonTransactionDetail;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.ProcessorTransactionType;
import com.digitalpaytech.service.ProcessorTransactionService;
import com.digitalpaytech.service.cps.CPSDataService;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.exception.ApplicationException;

public class CPSTransactionClosedServiceImplIntegrationTest {
    private static final Logger LOG = Logger.getLogger(CPSTransactionClosedServiceImplIntegrationTest.class);
    
    @Mock
    private ProcessorTransactionService processorTransactionService;
    
    @Mock
    private CPSDataService cpsDataService;
    
    @InjectMocks
    private CPSTransactionClosedServiceImpl cpsTransactionClosedService;
    
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(this.processorTransactionService.findProcessorTransactionById(anyLong(), anyBoolean())).thenAnswer(new Answer<ProcessorTransaction>() {
            @Override
            public ProcessorTransaction answer(final InvocationOnMock invocation) throws Throwable {
                return createProcessorTransaction();
            }
        });
        
        when(this.cpsDataService.findCPSDataByChargeTokenAndRefundTokenIsNull(anyString())).thenAnswer(new Answer<CPSData>() {
            @Override
            public CPSData answer(final InvocationOnMock invocation) throws Throwable {
                final CPSData cpsData = new CPSData();
                cpsData.setChargeToken("2f73e65b-b7d7-4ac2-891b-1c9389d8b6a8");
                cpsData.setProcessorTransaction(createProcessorTransaction());
                return cpsData;
            }
        });
    }
    
    private ProcessorTransaction createProcessorTransaction() {
        final ProcessorTransaction pt = new ProcessorTransaction();
        pt.setId(new Long(117));
        pt.setAmount(100);
        pt.setAuthorizationNumber("L2YP6J:197");
        final PointOfSale pos = new PointOfSale(1);
        pt.setPointOfSale(pos);
        pt.setPurchasedDate(new Date());
        pt.setTicketNumber(777);
        pt.setProcessorTransactionType(new ProcessorTransactionType(2));
        final MerchantAccount ma = new MerchantAccount(56);
        pt.setMerchantAccount(ma);
        pt.setCardType(WebCoreConstants.NOT_APPLICABLE);
        pt.setLast4digitsOfCardNumber((short) 1235);
        pt.setProcessingDate(new Date());
        pt.setProcessorTransactionId(WebCoreConstants.NOT_APPLICABLE);
        pt.setReferenceNumber(WebCoreConstants.NOT_APPLICABLE);
        pt.setIsApproved(true);
        return pt;
    }
    
    @Test
    public void create() {
        /*
         * {
         *    "pointOfSaleId": 1,
         *    "processorTransactionId": 117,
         *    "ticketNumber": 0,
         *    "transactionSettlementStatusTypeId": 2,
         *    "purchasedDate": "2018-05-31T16:21:38.000+0000"
         * }
         */
        final TransactionClosedMessage msg = new TransactionClosedMessage(1, 117, 0, 2, "2018-05-31T16:21:38.000+0000", "2f73e65b-b7d7-4ac2-891b-1c9389d8b6a8");
        try {
            final ElavonTransactionDetail etd = this.cpsTransactionClosedService.create(msg);
            assertEquals(1, etd.getPointOfSale().getId().intValue());
            assertEquals(117, etd.getProcessorTransaction().getId().intValue());
            assertEquals(2, etd.getTransactionSettlementStatusType().getId().intValue());
            
            final GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(etd.getPurchasedDate());
            assertEquals(2018, cal.get(Calendar.YEAR));
            assertEquals(5, cal.get(Calendar.MONTH) + 1);
            assertEquals(31, cal.get(Calendar.DATE));
            assertEquals(16, cal.get(Calendar.HOUR_OF_DAY));
            assertEquals(21, cal.get(Calendar.MINUTE));
            assertEquals(38, cal.get(Calendar.SECOND));
            
            if (etd.getTicketNumber() == 0) {
                fail("Ticket number should be 777, not 0");
            }
            
        } catch (ApplicationException ae) {
            fail(ae.getMessage());
        }
    }
}
