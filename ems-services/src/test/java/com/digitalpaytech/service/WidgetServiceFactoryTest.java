package com.digitalpaytech.service;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.exception.WebWidgetServiceException;
import com.digitalpaytech.service.WebWidgetService;

@org.junit.Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:ems-test.xml","classpath:ems-security.xml"})
public class WidgetServiceFactoryTest {
    
    @Test
    public void getInstance(){
        
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath*:ems-test.xml","classpath:ems-security.xml");
        WebWidgetServiceFactory fac = (WebWidgetServiceFactory) ctx.getBean("webWidgetServiceFactory");
                
        assertNotNull(fac); 
    }
    
    @Test(expected=WebWidgetServiceException.class)
    public void getWebWidgetServiceNoServiceType(){
        
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath*:ems-test.xml","classpath:ems-security.xml");
        WebWidgetServiceFactory fac = (WebWidgetServiceFactory) ctx.getBean("webWidgetServiceFactory");
                
        assertNotNull(fac); 
        
        //WebWidgetServiceException - no such services exist
        fac.getWebWidgetService(0);
        fac.getWebWidgetService(12);
    }       
    
    @Test
    public void getWebWidgetService() {
        
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath*:ems-test.xml","classpath:ems-security.xml");
        WebWidgetServiceFactory fac = (WebWidgetServiceFactory) ctx.getBean("webWidgetServiceFactory");
                    
        // WebWidgetRevenueServiceImpl
        WebWidgetService serv = fac.getWebWidgetService(1);
        assertNotNull(serv);
        assertTrue(serv.toString().contains("com.digitalpaytech.service.impl.WebWidgetRevenueServiceImpl"));
        
        // WebWidgetActiveAlertsServiceImpl
        serv = fac.getWebWidgetService(2);
        assertTrue(serv.toString().contains("com.digitalpaytech.service.impl.WebWidgetCollectionsServiceImpl"));
        
        // WebWidgetCollectionsServiceImpl
        serv = fac.getWebWidgetService(3);
        assertTrue(serv.toString().contains("com.digitalpaytech.service.impl.WebWidgetSettledCardServiceImpl"));

        // WebWidgetSettledCardServiceImpl
        serv = fac.getWebWidgetService(4);
        assertTrue(serv.toString().contains("com.digitalpaytech.service.impl.WebWidgetPurchasesServiceImpl"));

        // WebWidgetPurchasesServiceImpl
        serv = fac.getWebWidgetService(5);
        assertTrue(serv.toString().contains("com.digitalpaytech.service.impl.WebWidgetPaidOccupancyServiceImpl"));

        // WebWidgetPaidOccupancyServiceImpl
        serv = fac.getWebWidgetService(6);
        assertTrue(serv.toString().contains("com.digitalpaytech.service.impl.WebWidgetTurnoverServiceImpl"));

        // WebWidgetTurnoverServiceImpl
        serv = fac.getWebWidgetService(7);
        assertTrue(serv.toString().contains("com.digitalpaytech.service.impl.WebWidgetUtilizationServiceImpl"));

        // WebWidgetUtilizationServiceImpl
        serv = fac.getWebWidgetService(8);
        assertTrue(serv.toString().contains("com.digitalpaytech.service.impl.WebWidgetDurationServiceImpl"));

        // WebWidgetDurationServiceImpl
        serv = fac.getWebWidgetService(9);
        assertTrue(serv.toString().contains("com.digitalpaytech.service.impl.WebWidgetActiveAlertsServiceImpl"));

        // WebWidgetAlertStatusServiceImpl
        serv = fac.getWebWidgetService(10);
        assertTrue(serv.toString().contains("com.digitalpaytech.service.impl.WebWidgetAlertStatusServiceImpl"));

        // WebWidgetAlertStatusServiceImpl
        serv = fac.getWebWidgetService(11);
        assertTrue(serv.toString().contains("com.digitalpaytech.service.impl.WebWidgetMapServiceImpl"));
    }   
}
