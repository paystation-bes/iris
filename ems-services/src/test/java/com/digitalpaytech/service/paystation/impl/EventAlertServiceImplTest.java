package com.digitalpaytech.service.paystation.impl;

import org.junit.Test;
import static org.junit.Assert.*;
import org.apache.log4j.Logger;
import java.util.Iterator;

public class EventAlertServiceImplTest {

    private static Logger log = Logger.getLogger(EventAlertServiceImplTest.class);
    
    @Test
    public void loadPositiveEventActions() {
        EventAlertServiceImpl eventServ = null;
        try {
            eventServ = new EventAlertServiceImpl();
            eventServ.loadPositiveEventActions();
        } catch (Exception e) {
            fail(e.getMessage());
        }
        
        log.debug("size: " + eventServ.getPositiveActions().size());
        assertEquals(33, eventServ.getPositiveActions().size());
        
        String s;
        Iterator<String> iter = eventServ.getPositiveActions().iterator();
        while (iter.hasNext()) {
            s = iter.next();
            log.debug(s);
        }
    }
}
