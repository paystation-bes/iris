package com.digitalpaytech.service.processor.impl;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.beanstream.requests.CardPaymentRequest;
import com.beanstream.responses.PaymentResponse;
import com.digitalpaytech.bdd.util.TestBeanstreamException;

public final class TestTDMerchantCommunicationServiceMockImpl {
    
    protected static final Double AMOUNT_PROCESS_CHARGE_APPROVED = 4.00;
    protected static final Double AMOUNT_PROCESS_CHARGE_DECLINED = 4.01;
    protected static final String APPROVED_PROCESS_CHARGE_NUMBER = "333333";
    
    protected static final Double AMOUNT_POST_AUTH_APPROVED = 2.00;
    protected static final Double AMOUNT_POST_AUTH_DECLINED = 3.00;
    protected static final String APPROVED_POST_AUTH_NUMBER = "222222";
    
    private static final int RESPONSE_CODE_NON_USER_ERROR = 50;
    private static final String CARD_NOT_SUPPORTED = "Card Not Supported";
    private static final String DECLINED = "Declined";
    private static final String PC_DECLINED_MSG = "Failed to charge";
    private static final int DENIED_RESPONSE_CODE = 92;
    private static final String APPROVED_ID = "12345678";
    private static final String APPROVED_AUTH_CODE = "TEST";
    private static final String APPROVED_STRING = "1";
    
    private TestTDMerchantCommunicationServiceMockImpl() {
    }
    
    public static Answer<PaymentResponse> sendPreAuthRequest() {
        return new Answer<PaymentResponse>() {
            
            @Override
            public PaymentResponse answer(final InvocationOnMock invocation) throws Throwable {
                final CardPaymentRequest request = (CardPaymentRequest) invocation.getArguments()[1];
                if ("4030000010001234".equals(request.getCard().getNumber()) || "2223000010262100".equals(request.getCard().getNumber())) {
                    final PaymentResponse response = new PaymentResponse();
                    response.approved = APPROVED_STRING;
                    response.authCode = APPROVED_AUTH_CODE;
                    response.id = APPROVED_ID;
                    response.messageId = APPROVED_STRING;
                    return response;
                } else if ("4003050500040005".equals(request.getCard().getNumber())) {
                    final TestBeanstreamException testException = new TestBeanstreamException(DENIED_RESPONSE_CODE, 1, DECLINED, 400, true);
                    throw testException;
                } else {
                    final TestBeanstreamException testException =
                            new TestBeanstreamException(RESPONSE_CODE_NON_USER_ERROR, 1, CARD_NOT_SUPPORTED, 400, false);
                    throw testException;
                }
            }
        };
    }
    
    public static Answer<PaymentResponse> sendPostAuthRequest() {
        return new Answer<PaymentResponse>() {
            
            @Override
            public PaymentResponse answer(final InvocationOnMock invocation) throws Throwable {
                final Double chargeAmount = (Double) invocation.getArguments()[2];
                if (chargeAmount.equals(AMOUNT_POST_AUTH_APPROVED)) {
                    final PaymentResponse response = new PaymentResponse();
                    response.approved = APPROVED_STRING;
                    response.id = APPROVED_POST_AUTH_NUMBER;
                    response.authCode = APPROVED_AUTH_CODE;
                    return response;
                } else if (AMOUNT_POST_AUTH_DECLINED.equals(chargeAmount)) {
                    final TestBeanstreamException testException = new TestBeanstreamException(DENIED_RESPONSE_CODE, 1, DECLINED, 400, true);
                    throw testException;
                } else {
                    return new PaymentResponse();
                }
                
            }
        };
    }
    
    public static Answer<PaymentResponse> makePayment() {
        return new Answer<PaymentResponse>() {
            
            @Override
            public PaymentResponse answer(final InvocationOnMock invocation) throws Throwable {
                
                final CardPaymentRequest request = (CardPaymentRequest) invocation.getArguments()[1];
                final Double chargeAmount = Double.parseDouble(request.getAmount());
                if (chargeAmount.equals(AMOUNT_PROCESS_CHARGE_APPROVED)) {
                    final PaymentResponse response = new PaymentResponse();
                    response.approved = APPROVED_STRING;
                    response.id = APPROVED_PROCESS_CHARGE_NUMBER;
                    response.authCode = APPROVED_AUTH_CODE;
                    return response;
                } else if (AMOUNT_PROCESS_CHARGE_DECLINED.equals(chargeAmount)) {
                    final TestBeanstreamException testException = new TestBeanstreamException(DENIED_RESPONSE_CODE, 1, PC_DECLINED_MSG, 402, true);
                    throw testException;
                } else {
                    return new PaymentResponse();
                }
                
            }
        };
    }
    
}
