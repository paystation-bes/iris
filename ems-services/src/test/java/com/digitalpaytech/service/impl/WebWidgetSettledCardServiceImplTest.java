package com.digitalpaytech.service.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.WidgetChartType;
import com.digitalpaytech.domain.WidgetFilterType;
import com.digitalpaytech.domain.WidgetMetricType;
import com.digitalpaytech.domain.WidgetRangeType;
import com.digitalpaytech.domain.WidgetTierType;
import com.digitalpaytech.dto.WidgetData;
import com.digitalpaytech.service.WebWidgetHelperService;
import com.digitalpaytech.util.WidgetConstants;

@Ignore
public class WebWidgetSettledCardServiceImplTest {

//  private Widget widget;
//  private WebWidgetTotalSettledImpl impl;
//    private UserAccount userAccount;
//
//  @Autowired  
//  protected WebWidgetHelperService webWidgetHelperService;    
//  
//  protected void setWebWidgetHelperService(WebWidgetHelperService webWidgetHelperService) {
//      this.webWidgetHelperService = webWidgetHelperService;
//  }
//  
//  @Before
//  public void setUp() {       
//      
//      impl = new WebWidgetTotalSettledImpl();
//        impl.setWebWidgetHelperService(new WebWidgetHelperServiceImpl());
//      widget = new Widget();
//      WidgetTierType type1 = new WidgetTierType();
//      type1.setId(0);
//      WidgetTierType type2 = new WidgetTierType();
//      type2.setId(0);
//      WidgetTierType type3 = new WidgetTierType();
//      type3.setId(0);
//      WidgetRangeType rangeType = new WidgetRangeType();
//      WidgetMetricType widgetMetricType = new WidgetMetricType();
//      WidgetFilterType widgetFilterType = new WidgetFilterType();
//      WidgetChartType widgetChartType = new WidgetChartType();
//      
//        userAccount = new UserAccount();
//        Customer customer = new Customer();
//        customer.setId(3);
//        Customer parentCustomer = new Customer();
//        parentCustomer.setId(2);
//        customer.setParentCustomer(parentCustomer);
//        userAccount.setCustomer(customer);
//
//      widget.setWidgetTierTypeByWidgetTier1Type(type1);
//      widget.setWidgetTierTypeByWidgetTier2Type(type2);
//      widget.setWidgetTierTypeByWidgetTier3Type(type3);
//      widget.setWidgetRangeType(rangeType);
//      widget.setWidgetMetricType(widgetMetricType);
//      widget.setWidgetFilterType(widgetFilterType);
//      widget.setWidgetChartType(widgetChartType);
//      widget.setName("test widget");
//  }
//  
//  @After
//  public void cleanUp() {
//      impl = null;
//      widget = null;
//  }         
//  
//
//  @Test
//  public void test_appendColumns() {
//      String result = impl.appendColumns(widget);     
//      assertEquals(result, "");
//  }   
//  @Test
//  public void test_appendColumns_tier1_merchant_account() {
//      widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_MERCHANT_ACCOUNT);
//      String result = impl.appendColumns(widget);     
//      assertEquals(result, "");
//  }   
//
//  
//  @Test
//  public void test_appendTables() {
//      String result = impl.appendTables(widget, userAccount); 
//      assertEquals(result, "");
//  }   
//  @Test
//  public void test_appendTables_tier1_merchant_account() {
//      widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_MERCHANT_ACCOUNT);
//      String result = impl.appendTables(widget, userAccount); 
//      assertEquals(result, "");
//  }
//
//
//  
//  @Test
//  public void test_appendWhere_customerId_null() {
//      String result = impl.appendWhere(widget, userAccount);  
//      assertEquals(result, "");
//  }                          
//  @Test
//  public void test_appendWhere_customerId_not_null() {
//      widget.setCustomerId(2);
//      String result = impl.appendWhere(widget, userAccount);  
//      assertEquals(result, "");
//  }
//  @Test
//  public void test_appendWhere_tier1_merchant_account() {
//      widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_MERCHANT_ACCOUNT);
//      String result = impl.appendWhere(widget, userAccount);  
//      assertEquals(result, "");
//  }
//  
//  
//  @Test
//  public void test_appendGroupBy() {
//      String result = impl.appendGroupBy(widget);
//      assertEquals(result, "");
//  }
//  @Test
//  public void test_appendGroupBy_tier1_merchant_account() {
//      widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_MERCHANT_ACCOUNT);
//      String result = impl.appendGroupBy(widget); 
//      assertEquals(result, "");
//  }
//
//  
//  @Test
//  public void test_appendOrderBy() {
//      String result = impl.appendOrderBy(widget);
//      assertEquals(result, "");
//  }
//  @Test
//  public void test_appendOrderBy_tier1_merchant_account() {
//      widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_MERCHANT_ACCOUNT);
//      String result = impl.appendOrderBy(widget); 
//      assertEquals(result, "");
//  }
//
//  
//  @Test
//  public void test_appendLimit_filter_type_all() {
//      widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_ALL);    
//      String result = impl.appendLimit(widget,"3000");
//      assertEquals(result, " LIMIT 3000; ");
//  }
//  @Test
//  public void test_appendLimit_filter_type_subset() {
//      widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_SUBSET); 
//      String result = impl.appendLimit(widget,"3000");
//      assertEquals(result, " LIMIT 3000; ");
//  }
//  
//
//  @Test
//  public void test_appendSubsetWhere_tier1_merchant_account() {
//      widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_MERCHANT_ACCOUNT);
//      widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_SUBSET);
//      widget.setIsSubsetTier1(true);
//      String result = impl.appendSubsetWhere(widget); 
//      assertEquals(result, "");
//  }
}
