package com.digitalpaytech.service.impl;

import static org.junit.Assert.*;

import java.util.Properties;

import org.easymock.EasyMock;
import org.junit.Test;

import com.digitalpaytech.dao.OpenSessionDao;
import com.digitalpaytech.domain.EmsProperties;

public class EmsPropertiesServiceImplTest {

    @Test
    public void test_getPropertyValue_from_Properties() {
        EmsPropertiesServiceImpl emsPropertiesServiceImpl = new EmsPropertiesServiceImpl();
        Properties properties = new Properties();
        properties.clear();
        properties.put("MaximumLoginAttempt", "10");
        properties.put("MaximumLoginLockUpMinutes", "30");
        emsPropertiesServiceImpl.setProperties(properties);
        
        String result = emsPropertiesServiceImpl.getPropertyValue(
                "MaximumLoginAttempt", "2", false);
        
        assertNotNull(result);
        assertEquals(result, "10");
        
        String result2 = emsPropertiesServiceImpl.getPropertyValue(
                "MaximumLoginLockUpMinutes", "2", false);
        
        assertNotNull(result2);
        assertEquals(result2, "30");
    }

    @Test
    public void test_getPropertyValue_from_dao() {
        EmsPropertiesServiceImpl emsPropertiesServiceImpl = new EmsPropertiesServiceImpl();
        EmsProperties emsProperties = new EmsProperties("MaximumLoginAttempt", "20");
        
        OpenSessionDao entityDao = EasyMock.createMock(OpenSessionDao.class);
        EasyMock.expect(entityDao.findUniqueByNamedQueryAndNamedParam("EmsProperties.findByName", "name", "MaximumLoginAttempt")).andReturn(emsProperties);
        EasyMock.replay(entityDao);
        
        emsPropertiesServiceImpl.setOpenSessionDao(entityDao);
        String result = emsPropertiesServiceImpl.getPropertyValue(
                "MaximumLoginAttempt", "2", true);
        
        assertNotNull(result);
        assertEquals(result, "20");
        
    }
    
    @Test
    public void test_getPropertyValue_default_return() {
        EmsPropertiesServiceImpl emsPropertiesServiceImpl = new EmsPropertiesServiceImpl();
        EmsProperties emsProperties = new EmsProperties();
        
        OpenSessionDao entityDao = EasyMock.createMock(OpenSessionDao.class);
        EasyMock.expect(entityDao.findUniqueByNamedQueryAndNamedParam("EmsProperties.findByName", "name", "MaximumLoginAttempt")).andReturn(emsProperties);
        EasyMock.replay(entityDao);
        
        emsPropertiesServiceImpl.setOpenSessionDao(entityDao);
        String result = emsPropertiesServiceImpl.getPropertyValue(
                "MaximumLoginAttempt", "2", true);
        
        assertNotNull(result);
        assertEquals(result, "2");
    }
    
    @Test
    public void test_getPropertyValue_properties_null() {
        EmsPropertiesServiceImpl emsPropertiesServiceImpl = new EmsPropertiesServiceImpl();
        EmsProperties emsProperties = null;
        
        OpenSessionDao entityDao = EasyMock.createMock(OpenSessionDao.class);
        EasyMock.expect(entityDao.findUniqueByNamedQueryAndNamedParam("EmsProperties.findByName", "name", "MaximumLoginAttempt")).andReturn(emsProperties);
        EasyMock.replay(entityDao);
        
        emsPropertiesServiceImpl.setOpenSessionDao(entityDao);
        String result = emsPropertiesServiceImpl.getPropertyValue(
                "MaximumLoginAttempt", "2", true);
        
        assertNotNull(result);
        assertEquals(result, "2");
    }
}
