package com.digitalpaytech.service.impl;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.WidgetChartType;
import com.digitalpaytech.domain.WidgetFilterType;
import com.digitalpaytech.domain.WidgetMetricType;
import com.digitalpaytech.domain.WidgetRangeType;
import com.digitalpaytech.domain.WidgetTierType;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WidgetConstants;

public class WebWidgetPurchasesServiceImplTest {
    
//    private Widget widget;
//    private UserAccount userAccount;
//    private WebWidgetPurchasesServiceImpl impl;
//    
//    @Before
//    public void setUp() {
//        
//        userAccount = new UserAccount();
//        Customer customer = new Customer();
//        Customer parentCustomer = new Customer();
//        
//        parentCustomer.setId(2);
//        
//        customer.setParentCustomer(parentCustomer);
//        customer.setId(2);
//        userAccount.setCustomer(customer);
//        
//        impl = new WebWidgetPurchasesServiceImpl();
//        
//        impl.setWebWidgetHelperService(new WebWidgetHelperServiceImpl() {
//            public String getCustomerTimeZoneByCustomerId(int customerId) {
//                String timeZone = WebCoreConstants.GMT;
//                return timeZone;
//            }
//        });
//        
//        widget = new Widget();
//        WidgetTierType type1 = new WidgetTierType();
//        type1.setId(0);
//        WidgetTierType type2 = new WidgetTierType();
//        type2.setId(0);
//        WidgetTierType type3 = new WidgetTierType();
//        type3.setId(0);
//        WidgetRangeType rangeType = new WidgetRangeType();
//        WidgetMetricType widgetMetricType = new WidgetMetricType();
//        WidgetFilterType widgetFilterType = new WidgetFilterType();
//        WidgetChartType widgetChartType = new WidgetChartType();
//        
//        widget.setWidgetTierTypeByWidgetTier1Type(type1);
//        widget.setWidgetTierTypeByWidgetTier2Type(type2);
//        widget.setWidgetTierTypeByWidgetTier3Type(type3);
//        widget.setWidgetRangeType(rangeType);
//        widget.setWidgetMetricType(widgetMetricType);
//        widget.setWidgetFilterType(widgetFilterType);
//        widget.setWidgetChartType(widgetChartType);
//        widget.setName("test widget");
//    }
//    
//    @After
//    public void cleanUp() {
//        impl = null;
//        widget = null;
//    }
//    
//    @Test
//    public void test_appendColumns_tier1_hour() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_HOUR);
//        String result = impl.appendColumns(widget, WidgetConstants.TABLE_DEPTH_TYPE_TOTAL);
//        assertEquals(result,
//                     "SELECT IFNULL(pppt.CustomerId, 0) AS CustomerId , t.DayOfYear AS Tier1TypeId, (SELECT MIN(t2.DateTime) FROM Time t2 where t2.Date = t.Date AND t2.HourOfDay = t.HourOfDay) AS Tier1TypeName, t.DateTime AS StartTime , IFNULL(SUM(pppt.TotalCount), 0) AS WidgetMetricValue ");
//    }
//    
//    @Test
//    public void test_appendColumns_tier1_day() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_DAY);
//        String result = impl.appendColumns(widget, WidgetConstants.TABLE_DEPTH_TYPE_TOTAL);
//        assertEquals(result,
//                     "SELECT IFNULL(pppt.CustomerId, 0) AS CustomerId , t.DayOfYear AS Tier1TypeId, t.Date AS Tier1TypeName, t.Date AS StartTime , IFNULL(SUM(pppt.TotalCount), 0) AS WidgetMetricValue ");
//    }
//    
//    @Test
//    public void test_appendColumns_tier1_month() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_MONTH);
//        String result = impl.appendColumns(widget, WidgetConstants.TABLE_DEPTH_TYPE_TOTAL);
//        assertEquals(result,
//                     "SELECT IFNULL(pppt.CustomerId, 0) AS CustomerId , t.Month AS Tier1TypeId, CONCAT(t.MonthNameAbbrev,' ', t.Year) AS Tier1TypeName , IFNULL(SUM(pppt.TotalCount), 0) AS WidgetMetricValue ");
//    }
//    
//    @Test
//    public void test_appendColumns_tier1_organization() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_ORG);
//        String result = impl.appendColumns(widget, WidgetConstants.TABLE_DEPTH_TYPE_TOTAL);
//        assertEquals(result,
//                     "SELECT IFNULL(pppt.CustomerId, 0) AS CustomerId , c.Id AS Tier1TypeId, c.Name AS Tier1TypeName , IFNULL(SUM(pppt.TotalCount), 0) AS WidgetMetricValue ");
//    }
//    
//    @Test
//    public void test_appendColumns_tier1_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendColumns(widget, WidgetConstants.TABLE_DEPTH_TYPE_DETAIL);
//        assertEquals(result,
//                     "SELECT IFNULL(pppt.CustomerId, 0) AS CustomerId , l.Id AS Tier1TypeId, l.Name AS Tier1TypeName , IFNULL(SUM(pppd.TotalCount), 0) AS WidgetMetricValue ");
//    }
//    
//    @Test
//    public void test_appendColumns_tier1_route() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//        String result = impl.appendColumns(widget, WidgetConstants.TABLE_DEPTH_TYPE_DETAIL);
//        assertEquals(result,
//                     "SELECT IFNULL(pppt.CustomerId, 0) AS CustomerId , ro.Id AS Tier1TypeId, ro.Name AS Tier1TypeName , IFNULL(SUM(pppd.TotalCount), 0) AS WidgetMetricValue ");
//    }
//    
//    @Test
//    public void test_appendColumns_tier1_rate() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_RATE);
//        String result = impl.appendColumns(widget, WidgetConstants.TABLE_DEPTH_TYPE_DETAIL);
//        assertEquals(result,
//                     "SELECT IFNULL(pppt.CustomerId, 0) AS CustomerId , ra.Id AS Tier1TypeId, ra.Name AS Tier1TypeName , IFNULL(SUM(pppd.TotalCount), 0) AS WidgetMetricValue ");
//    }
//    
//    @Test
//    public void test_appendColumns_tier1_revenueType() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_REVENUE_TYPE);
//        String result = impl.appendColumns(widget, WidgetConstants.TABLE_DEPTH_TYPE_REVENUE);
//        assertEquals(result,
//                     "SELECT IFNULL(pppt.CustomerId, 0) AS CustomerId , rt.Id AS Tier1TypeId, rt.Name AS Tier1TypeName , IFNULL(SUM(pppr.TotalCount), 0) AS WidgetMetricValue ");
//    }
//    
//    @Test
//    public void test_appendColumns_tier1_hour_tier2_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_HOUR);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendColumns(widget, WidgetConstants.TABLE_DEPTH_TYPE_DETAIL);
//        assertEquals(result,
//                     "SELECT IFNULL(pppt.CustomerId, 0) AS CustomerId , t.DayOfYear AS Tier1TypeId, (SELECT MIN(t2.DateTime) FROM Time t2 where t2.Date = t.Date AND t2.HourOfDay = t.HourOfDay) AS Tier1TypeName, t.DateTime AS StartTime , l.Id AS Tier2TypeId, l.Name AS Tier2TypeName , IFNULL(SUM(pppd.TotalCount), 0) AS WidgetMetricValue ");
//    }
//    
//    @Test
//    public void test_appendColumns_tier1_day_tier2_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_DAY);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendColumns(widget, WidgetConstants.TABLE_DEPTH_TYPE_DETAIL);
//        assertEquals(result,
//                     "SELECT IFNULL(pppt.CustomerId, 0) AS CustomerId , t.DayOfYear AS Tier1TypeId, t.Date AS Tier1TypeName, t.Date AS StartTime , l.Id AS Tier2TypeId, l.Name AS Tier2TypeName , IFNULL(SUM(pppd.TotalCount), 0) AS WidgetMetricValue ");
//    }
//    
//    @Test
//    public void test_appendColumns_tier1_month_tier2_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_MONTH);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendColumns(widget, WidgetConstants.TABLE_DEPTH_TYPE_DETAIL);
//        assertEquals(result,
//                     "SELECT IFNULL(pppt.CustomerId, 0) AS CustomerId , t.Month AS Tier1TypeId, CONCAT(t.MonthNameAbbrev,' ', t.Year) AS Tier1TypeName , l.Id AS Tier2TypeId, l.Name AS Tier2TypeName , IFNULL(SUM(pppd.TotalCount), 0) AS WidgetMetricValue ");
//    }
//    
//    @Test
//    public void test_appendColumns_tier1_hour_tier2_route() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_HOUR);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//        String result = impl.appendColumns(widget, WidgetConstants.TABLE_DEPTH_TYPE_DETAIL);
//        
//        assertEquals(result,
//                     "SELECT IFNULL(pppt.CustomerId, 0) AS CustomerId , t.DayOfYear AS Tier1TypeId, (SELECT MIN(t2.DateTime) FROM Time t2 where t2.Date = t.Date AND t2.HourOfDay = t.HourOfDay) AS Tier1TypeName, t.DateTime AS StartTime , ro.Id AS Tier2TypeId, ro.Name AS Tier2TypeName , IFNULL(SUM(pppd.TotalCount), 0) AS WidgetMetricValue ");
//    }
//    
//    @Test
//    public void test_appendColumns_tier1_hour_tier2_route_tier3_rate() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_HOUR);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendColumns(widget, WidgetConstants.TABLE_DEPTH_TYPE_DETAIL);
//        
//        assertEquals(result,
//                     "SELECT IFNULL(pppt.CustomerId, 0) AS CustomerId , t.DayOfYear AS Tier1TypeId, (SELECT MIN(t2.DateTime) FROM Time t2 where t2.Date = t.Date AND t2.HourOfDay = t.HourOfDay) AS Tier1TypeName, t.DateTime AS StartTime , ro.Id AS Tier2TypeId, ro.Name AS Tier2TypeName , l.Id AS Tier3TypeId, l.Name AS Tier3TypeName , IFNULL(SUM(pppd.TotalCount), 0) AS WidgetMetricValue ");
//    }
//    
//    @Test
//    public void test_appendColumns_tier1_hour_tier2_route_tier3_revenueType() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_HOUR);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_REVENUE_TYPE);
//        String result = impl.appendColumns(widget, WidgetConstants.TABLE_DEPTH_TYPE_REVENUE);
//        
//        assertEquals(result,
//                     "SELECT IFNULL(pppt.CustomerId, 0) AS CustomerId , t.DayOfYear AS Tier1TypeId, (SELECT MIN(t2.DateTime) FROM Time t2 where t2.Date = t.Date AND t2.HourOfDay = t.HourOfDay) AS Tier1TypeName, t.DateTime AS StartTime , ro.Id AS Tier2TypeId, ro.Name AS Tier2TypeName , rt.Id AS Tier3TypeId, rt.Name AS Tier3TypeName , IFNULL(SUM(pppr.TotalCount), 0) AS WidgetMetricValue ");
//    }
//    
//    @Test
//    public void test_appendColumns_tier1_hour_tier2_route_tier3_route() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_HOUR);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_REVENUE_TYPE);
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//        String result = impl.appendColumns(widget, WidgetConstants.TABLE_DEPTH_TYPE_REVENUE);
//        
//        assertEquals(result,
//                     "SELECT IFNULL(pppt.CustomerId, 0) AS CustomerId , t.DayOfYear AS Tier1TypeId, (SELECT MIN(t2.DateTime) FROM Time t2 where t2.Date = t.Date AND t2.HourOfDay = t.HourOfDay) AS Tier1TypeName, t.DateTime AS StartTime , rt.Id AS Tier2TypeId, rt.Name AS Tier2TypeName , ro.Id AS Tier3TypeId, ro.Name AS Tier3TypeName , IFNULL(SUM(pppr.TotalCount), 0) AS WidgetMetricValue ");
//    }
//    
//    @Test
//    public void test_appendColumns_tier1_month_tier2_rate() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_MONTH);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_RATE);
//        String result = impl.appendColumns(widget, WidgetConstants.TABLE_DEPTH_TYPE_DETAIL);
//        
//        assertEquals(result,
//                     "SELECT IFNULL(pppt.CustomerId, 0) AS CustomerId , t.Month AS Tier1TypeId, CONCAT(t.MonthNameAbbrev,' ', t.Year) AS Tier1TypeName , ra.Id AS Tier2TypeId, ra.Name AS Tier2TypeName , IFNULL(SUM(pppd.TotalCount), 0) AS WidgetMetricValue ");
//    }
//    
//    @Test
//    public void test_appendColumns_tier2_location_tier3_revenueType() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_NA);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_REVENUE_TYPE);
//        String result = impl.appendColumns(widget, WidgetConstants.TABLE_DEPTH_TYPE_REVENUE);
//        assertEquals(result,
//                     "SELECT IFNULL(pppt.CustomerId, 0) AS CustomerId , l.Id AS Tier2TypeId, l.Name AS Tier2TypeName , rt.Id AS Tier3TypeId, rt.Name AS Tier3TypeName , IFNULL(SUM(pppr.TotalCount), 0) AS WidgetMetricValue ");
//    }
//    
//    @Test
//    public void test_appendTables() {
//        String result = impl.appendTables(widget, WidgetConstants.TABLE_DEPTH_TYPE_TOTAL, WidgetConstants.TABLE_RANGE_TYPE_HOUR);
//        assertEquals(result, "FROM PPPTotalHour pppt INNER JOIN Time t ON pppt.TimeIdLocal = t.Id INNER JOIN Customer c ON c.Id = pppt.CustomerId ");
//    }
//    
//    @Test
//    public void test_appendTables_tier1_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendTables(widget, WidgetConstants.TABLE_DEPTH_TYPE_DETAIL, WidgetConstants.TABLE_RANGE_TYPE_HOUR);
//        assertEquals(result,
//                     "FROM PPPTotalHour pppt INNER JOIN PPPDetailHour pppd ON pppd.PPPTotalHourId = pppt.Id INNER JOIN Time t ON pppt.TimeIdLocal = t.Id INNER JOIN Location l ON l.Id = pppd.PurchaseLocationId ");
//    }
//    
//    @Test
//    public void test_appendTables_tier1_location_tier2_revenueType() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_REVENUE_TYPE);
//        String result = impl.appendTables(widget, WidgetConstants.TABLE_DEPTH_TYPE_DETAIL, WidgetConstants.TABLE_RANGE_TYPE_HOUR);
//        assertEquals(result,
//                     "FROM PPPTotalHour pppt INNER JOIN PPPDetailHour pppd ON pppd.PPPTotalHourId = pppt.Id INNER JOIN Time t ON pppt.TimeIdLocal = t.Id INNER JOIN Location l ON l.Id = pppd.PurchaseLocationId INNER JOIN RevenueType rt ON rt.Id = pppr.RevenueTypeId ");
//    }
//    
//    @Test
//    public void test_appendTables_tier2_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_DAY); //Tier 1 is needed to fill in FROM statement
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendTables(widget, WidgetConstants.TABLE_DEPTH_TYPE_DETAIL, WidgetConstants.TABLE_RANGE_TYPE_MONTH);
//        assertEquals(result,
//                     "FROM PPPTotalMonth pppt INNER JOIN PPPDetailMonth pppd ON pppd.PPPTotalMonthId = pppt.Id INNER JOIN Time t ON pppt.TimeIdLocal = t.Id INNER JOIN Location l ON l.Id = pppd.PurchaseLocationId ");
//    }
//    
//    @Test
//    public void test_appendTables_tier2_location_tier3_revenueType() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_DAY); //Tier 1 is needed to fill in FROM statement
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_REVENUE_TYPE);
//        String result = impl.appendTables(widget, WidgetConstants.TABLE_DEPTH_TYPE_REVENUE, WidgetConstants.TABLE_RANGE_TYPE_MONTH);
//        assertEquals(result,
//                     "FROM PPPTotalMonth pppt INNER JOIN PPPDetailMonth pppd ON pppd.PPPTotalMonthId = pppt.Id INNER JOIN PPPRevenueMonth pppr ON pppr.PPPDetailMonthId = pppd.Id INNER JOIN Time t ON pppt.TimeIdLocal = t.Id INNER JOIN Location l ON l.Id = pppd.PurchaseLocationId INNER JOIN RevenueType rt ON rt.Id = pppr.RevenueTypeId ");
//    }
//    
//    @Test
//    public void test_appendWhere_customerId_null() {
//        userAccount.getCustomer().setIsParent(true);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result, "WHERE pppt.CustomerId IN (:customerId) ");
//    }
//    
//    @Test
//    public void test_appendWhere_customerId_not_null() {
//        widget.setCustomerId(2);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result, "WHERE pppt.CustomerId = :customerId ");
//    }
//    
//    @Test
//    public void test_appendWhere_tier1_type_route() {
//        widget.setCustomerId(2);
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result, "WHERE pppt.CustomerId = :customerId ");
//    }
//    
//    @Test
//    public void test_appendWhere_tier2_type_route() {
//        widget.setCustomerId(2);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result, "WHERE pppt.CustomerId = :customerId ");
//    }
//    
//    @Test
//    public void test_appendWhere_tier3_type_route() {
//        widget.setCustomerId(2);
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result, "WHERE pppt.CustomerId = :customerId ");
//    }
//    
//    @Test
//    public void test_appendWhere_widget_rangeType_1() {
//        widget.setCustomerId(2);
//        widget.getWidgetRangeType().setId(WidgetConstants.RANGE_TYPE_TODAY);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result, "WHERE pppt.CustomerId = :customerId AND t.Date = DATE(:currentLocalTime) AND t.DateTime <= :currentLocalTime ");
//    }
//    
//    @Test
//    public void test_appendWhere_widget_rangeType_2() {
//        widget.setCustomerId(2);
//        widget.getWidgetRangeType().setId(WidgetConstants.RANGE_TYPE_YESTERDAY);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result, "WHERE pppt.CustomerId = :customerId AND t.Date = DATE(DATE_SUB(:currentLocalTime, INTERVAL 1 DAY)) ");
//    }
//    
//    @Test
//    public void test_appendWhere_widget_rangeType_3() {
//        widget.setCustomerId(2);
//        widget.getWidgetRangeType().setId(WidgetConstants.RANGE_TYPE_LAST_24HOURS);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result,
//                     "WHERE pppt.CustomerId = :customerId AND t.Id >= (SELECT MIN(t3.Id) FROM `Time` t3 WHERE t3.HourOfDay = HOUR(DATE_SUB(:currentLocalTime, INTERVAL 24 HOUR)) AND t3.Date = DATE(DATE_SUB(:currentLocalTime, INTERVAL 24 HOUR))) AND t.Id <  (SELECT MIN(t4.Id) FROM `Time` t4 WHERE t4.HourOfDay = HOUR(:currentLocalTime) AND t4.Date = DATE(:currentLocalTime)) ");
//    }
//    
//    @Test
//    public void test_appendWhere_widget_rangeType_4() {
//        widget.setCustomerId(2);
//        widget.getWidgetRangeType().setId(WidgetConstants.RANGE_TYPE_LAST_7DAYS);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result,
//                     "WHERE pppt.CustomerId = :customerId AND t.Id >= (SELECT t3.Id FROM `Time` t3 WHERE t3.Date = DATE(DATE_SUB(:currentLocalTime, INTERVAL 7 DAY)) AND t3.QuarterOfDay = 0) AND t.Id <  (SELECT t4.Id FROM `Time` t4 WHERE t4.Date = DATE(:currentLocalTime) AND t4.QuarterOfDay = 0) AND t.QuarterOfDay = 0 ");
//    }
//    
//    @Test
//    public void test_appendWhere_widget_rangeType_5() {
//        widget.setCustomerId(2);
//        widget.getWidgetRangeType().setId(WidgetConstants.RANGE_TYPE_LAST_30DAYS);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result,
//                     "WHERE pppt.CustomerId = :customerId AND t.Id >= (SELECT t3.Id FROM `Time` t3 WHERE t3.Date = DATE(DATE_SUB(:currentLocalTime, INTERVAL 30 DAY)) AND t3.QuarterOfDay = 0) AND t.Id <  (SELECT t4.Id FROM `Time` t4 WHERE t4.Date = DATE(:currentLocalTime) AND t4.QuarterOfDay = 0) AND t.QuarterOfDay = 0 ");
//    }
//    
//    @Test
//    public void test_appendWhere_widget_rangeType_6() {
//        widget.setCustomerId(2);
//        widget.getWidgetRangeType().setId(WidgetConstants.RANGE_TYPE_LAST_12MONTHS);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result,
//                     "WHERE pppt.CustomerId = :customerId AND t.Id >= (SELECT MIN(t3.Id) FROM `Time` t3 WHERE t3.Year = YEAR(DATE_SUB(:currentLocalTime, INTERVAL 12 MONTH)) AND t3.Month = MONTH(DATE_SUB(:currentLocalTime, INTERVAL 12 MONTH)) AND t3.QuarterOfDay = 0 AND t3.DayOfMonth = 1) AND t.Id <  (SELECT MIN(t4.Id) FROM `Time` t4 WHERE t4.Year = YEAR(:currentLocalTime) AND t4.Month = MONTH(:currentLocalTime) AND t4.DateTime <= :currentLocalTime AND t4.QuarterOfDay = 0 AND t4.DayOfMonth = 1) AND t.QuarterOfDay = 0 AND t.DayOfMonth = 1 ");
//    }
//    
//    @Test
//    public void test_appendWhere_widget_rangeType_7() {
//        widget.setCustomerId(2);
//        widget.getWidgetRangeType().setId(WidgetConstants.RANGE_TYPE_THIS_WEEK);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result,
//                     "WHERE pppt.CustomerId = :customerId AND t.Date >= DATE(DATE_SUB(:currentLocalTime, INTERVAL 1 WEEK)) AND t.WeekOfYear = WEEK(:currentLocalTime,2) AND t.DateTime <= :currentLocalTime AND t.QuarterOfDay = 0 ");
//    }
//    
//    @Test
//    public void test_appendWhere_widget_rangeType_8() {
//        widget.setCustomerId(2);
//        widget.getWidgetRangeType().setId(WidgetConstants.RANGE_TYPE_THIS_MONTH);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result,
//                     "WHERE pppt.CustomerId = :customerId AND t.Year = YEAR(:currentLocalTime) AND t.Month = MONTH(:currentLocalTime) AND t.DateTime <= :currentLocalTime AND t.QuarterOfDay = 0 AND t.DayOfMonth = 1 ");
//    }
//    
//    @Test
//    public void test_appendWhere_widget_rangeType_9() {
//        widget.setCustomerId(2);
//        widget.getWidgetRangeType().setId(WidgetConstants.RANGE_TYPE_THIS_YEAR);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result, "WHERE pppt.CustomerId = :customerId AND t.Year = YEAR(:currentLocalTime) AND t.DateTime <= :currentLocalTime AND t.QuarterOfDay = 0 AND t.DayOfMonth = 1 ");
//    }
//    
//    @Test
//    public void test_appendWhere_widget_rangeType_10() {
//        widget.setCustomerId(2);
//        widget.getWidgetRangeType().setId(WidgetConstants.RANGE_TYPE_LAST_WEEK);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result,
//                     "WHERE pppt.CustomerId = :customerId AND t.Date >= DATE(DATE_SUB(:currentLocalTime, INTERVAL 2 WEEK)) AND t.WeekOfYear = WEEK(DATE_SUB(:currentLocalTime, INTERVAL 1 WEEK), 2) AND t.QuarterOfDay = 0 ");
//    }
//    
//    @Test
//    public void test_appendWhere_widget_rangeType_11() {
//        widget.setCustomerId(2);
//        widget.getWidgetRangeType().setId(WidgetConstants.RANGE_TYPE_LAST_MONTH);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result,
//                     "WHERE pppt.CustomerId = :customerId AND t.Year = YEAR(DATE_SUB(:currentLocalTime, INTERVAL 1 MONTH)) AND t.Month = MONTH(DATE_SUB(:currentLocalTime, INTERVAL 1 MONTH)) AND t.QuarterOfDay = 0 AND t.DayOfMonth = 1 ");
//    }
//    
//    @Test
//    public void test_appendWhere_widget_rangeType_12() {
//        widget.setCustomerId(2);
//        widget.getWidgetRangeType().setId(WidgetConstants.RANGE_TYPE_LAST_YEAR);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result, "WHERE pppt.CustomerId = :customerId AND t.Year = YEAR(DATE_SUB(:currentLocalTime, INTERVAL 1 YEAR)) AND t.QuarterOfDay = 0 AND t.DayOfMonth = 1 ");
//    }
//    
//    @Test
//    public void test_appendGroupBy() {
//        String result = impl.appendGroupBy(widget);
//        assertEquals(result, "");
//    }
//    
//    @Test
//    public void test_appendGroupBy_tier1_2() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_HOUR);
//        String result = impl.appendGroupBy(widget);
//        assertEquals(result, " GROUP BY t.Date, t.HourOfDay");
//    }
//    
//    @Test
//    public void test_appendGroupBy_tier1_3() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_DAY);
//        String result = impl.appendGroupBy(widget);
//        assertEquals(result, " GROUP BY t.Date");
//    }
//    
//    @Test
//    public void test_appendGroupBy_tier1_5() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_MONTH);
//        String result = impl.appendGroupBy(widget);
//        assertEquals(result, " GROUP BY t.Year, t.Month");
//    }
//    
//    @Test
//    public void test_appendGroupBy_tier1_2_tier2_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_HOUR);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendGroupBy(widget);
//        assertEquals(result, " GROUP BY t.Date, t.HourOfDay, l.Id");
//    }
//    
//    @Test
//    public void test_appendGroupBy_tier1_3_tier2_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_DAY);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendGroupBy(widget);
//        assertEquals(result, " GROUP BY t.Date, l.Id");
//    }
//    
//    @Test
//    public void test_appendGroupBy_tier1_5_tier2_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_MONTH);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendGroupBy(widget);
//        assertEquals(result, " GROUP BY t.Year, t.Month, l.Id");
//    }
//    
//    @Test
//    public void test_appendGroupBy_tier1_2_tier2_location_tier3_revenueType() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_HOUR);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_REVENUE_TYPE);
//        String result = impl.appendGroupBy(widget);
//        assertEquals(result, " GROUP BY t.Date, t.HourOfDay, l.Id, rt.Id");
//    }
//    
//    @Test
//    public void test_appendGroupBy_tier1_3_tier2_location_tier3_revenueType() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_DAY);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_REVENUE_TYPE);
//        String result = impl.appendGroupBy(widget);
//        assertEquals(result, " GROUP BY t.Date, l.Id, rt.Id");
//    }
//    
//    @Test
//    public void test_appendGroupBy_tier1_5_tier2_location_tier3_revenueType() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_MONTH);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_REVENUE_TYPE);
//        String result = impl.appendGroupBy(widget);
//        assertEquals(result, " GROUP BY t.Year, t.Month, l.Id, rt.Id");
//    }
//    
//    @Test
//    public void test_appendGroupBy_tier1_2_tier2_location_tier3_route() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_HOUR);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//        String result = impl.appendGroupBy(widget);
//        assertEquals(result, " GROUP BY t.Date, t.HourOfDay, l.Id, ro.Id");
//    }
//    
//    @Test
//    public void test_appendGroupBy_tier1_3_tier2_location_tier3_route() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_DAY);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//        String result = impl.appendGroupBy(widget);
//        assertEquals(result, " GROUP BY t.Date, l.Id, ro.Id");
//    }
//    
//    @Test
//    public void test_appendGroupBy_tier1_5_tier2_location_tier3_route() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_MONTH);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//        String result = impl.appendGroupBy(widget);
//        assertEquals(result, " GROUP BY t.Year, t.Month, l.Id, ro.Id");
//    }
//    
//    @Test
//    public void test_appendGroupBy_tier2_location_tier3_revenueType() {
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_REVENUE_TYPE);
//        String result = impl.appendGroupBy(widget);
//        assertEquals(result, " GROUP BY l.Id, rt.Id");
//    }
//    
//    @Test
//    public void test_appendGroupBy_tier2_location_tier3_route() {
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//        String result = impl.appendGroupBy(widget);
//        assertEquals(result, " GROUP BY l.Id, ro.Id");
//    }
//    
//    @Test
//    public void test_appendGroupBy_tier3_revenueType() {
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_REVENUE_TYPE);
//        String result = impl.appendGroupBy(widget);
//        assertEquals(result, " GROUP BY rt.Id");
//    }
//    
//    @Test
//    public void test_appendGroupBy_tier3_route() {
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//        String result = impl.appendGroupBy(widget);
//        assertEquals(result, " GROUP BY ro.Id");
//    }
//    
//    @Test
//    public void test_appendGroupBy_tier3_location() {
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendGroupBy(widget);
//        assertEquals(result, " GROUP BY l.Id");
//    }
//    
//    @Test
//    public void test_appendOrderBy() {
//        String result = impl.appendOrderBy(widget);
//        assertEquals(result, "");
//    }
//    
//    @Test
//    public void test_appendOrderBy_tier1_2() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_HOUR);
//        String result = impl.appendOrderBy(widget);
//        assertEquals(result, " ORDER BY t.Date, t.HourOfDay");
//    }
//    
//    @Test
//    public void test_appendOrderBy_tier1_3() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_DAY);
//        String result = impl.appendOrderBy(widget);
//        assertEquals(result, " ORDER BY t.Date");
//    }
//    
//    @Test
//    public void test_appendOrderBy_tier1_5() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_MONTH);
//        String result = impl.appendOrderBy(widget);
//        assertEquals(result, " ORDER BY t.Year, t.Month");
//    }
//    
//    @Test
//    public void test_appendOrderBy_tier1_2_tier2_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_HOUR);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendOrderBy(widget);
//        assertEquals(result, " ORDER BY t.Date, t.HourOfDay, l.Name");
//    }
//    
//    @Test
//    public void test_appendOrderBy_tier1_3_tier2_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_DAY);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendOrderBy(widget);
//        assertEquals(result, " ORDER BY t.Date, l.Name");
//    }
//    
//    @Test
//    public void test_appendOrderBy_tier1_5_tier2_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_MONTH);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendOrderBy(widget);
//        assertEquals(result, " ORDER BY t.Year, t.Month, l.Name");
//    }
//    
//    @Test
//    public void test_appendOrderBy_tier1_2_tier2_location_tier3_revenueType() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_HOUR);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_REVENUE_TYPE);
//        String result = impl.appendOrderBy(widget);
//        assertEquals(result, " ORDER BY t.Date, t.HourOfDay, l.Name, rt.Name");
//    }
//    
//    @Test
//    public void test_appendOrderBy_tier1_3_tier2_location_tier3_revenueType() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_DAY);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_REVENUE_TYPE);
//        String result = impl.appendOrderBy(widget);
//        assertEquals(result, " ORDER BY t.Date, l.Name, rt.Name");
//    }
//    
//    @Test
//    public void test_appendOrderBy_tier1_5_tier2_location_tier3_revenueType() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_MONTH);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_REVENUE_TYPE);
//        String result = impl.appendOrderBy(widget);
//        assertEquals(result, " ORDER BY t.Year, t.Month, l.Name, rt.Name");
//    }
//    
//    @Test
//    public void test_appendOrderBy_tier1_2_tier2_location_tier3_route() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_HOUR);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//        String result = impl.appendOrderBy(widget);
//        assertEquals(result, " ORDER BY t.Date, t.HourOfDay, l.Name, ro.Name");
//    }
//    
//    @Test
//    public void test_appendOrderBy_tier1_3_tier2_location_tier3_route() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_DAY);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//        String result = impl.appendOrderBy(widget);
//        assertEquals(result, " ORDER BY t.Date, l.Name, ro.Name");
//    }
//    
//    @Test
//    public void test_appendOrderBy_tier1_5_tier2_location_tier3_route() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_MONTH);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//        String result = impl.appendOrderBy(widget);
//        assertEquals(result, " ORDER BY t.Year, t.Month, l.Name, ro.Name");
//    }
//    
//    @Test
//    public void test_appendOrderBy_tier2_location_tier3_revenueType() {
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_REVENUE_TYPE);
//        String result = impl.appendOrderBy(widget);
//        assertEquals(result, " ORDER BY l.Name, rt.Name");
//    }
//    
//    @Test
//    public void test_appendOrderBy_tier2_location_tier3_route() {
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//        String result = impl.appendOrderBy(widget);
//        assertEquals(result, " ORDER BY l.Name, ro.Name");
//    }
//    
//    @Test
//    public void test_appendOrderBy_tier3_revenueType() {
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_REVENUE_TYPE);
//        String result = impl.appendOrderBy(widget);
//        assertEquals(result, " ORDER BY rt.Name");
//    }
//    
//    @Test
//    public void test_appendOrderBy_tier3_route() {
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//        String result = impl.appendOrderBy(widget);
//        assertEquals(result, " ORDER BY ro.Name");
//    }
//    
//    @Test
//    public void test_appendLimit_filter_type_all() {
//        widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_ALL);
//        String result = impl.appendLimit(widget, "3000");
//        assertEquals(result, " LIMIT 3000; ");
//    }
//    
//    @Test
//    public void test_appendLimit_filter_type_subset() {
//        widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_SUBSET);
//        String result = impl.appendLimit(widget, "3000");
//        assertEquals(result, " LIMIT 3000; ");
//    }
//    
//    @Test
//    public void test_appendSubsetWhere_tier1_organization() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_ORG);
//        widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_SUBSET);
//        widget.setIsSubsetTier1(true);
//        String result = impl.appendSubsetWhere(widget, userAccount);
//        assertEquals(result, "AND (c.Id in null) ");
//    }
//    
//    @Test
//    public void test_appendSubsetWhere_tier1_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_SUBSET);
//        widget.setIsSubsetTier1(true);
//        String result = impl.appendSubsetWhere(widget, userAccount);
//        assertEquals(result, "AND (l.Id in null) ");
//    }
//    
//    @Test
//    public void test_appendSubsetWhere_tier1_route() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//        widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_SUBSET);
//        widget.setIsSubsetTier1(true);
//        String result = impl.appendSubsetWhere(widget, userAccount);
//        assertEquals(result, "AND (ro.Id in null OR ro.Id is null) ");
//    }
//    
//    @Test
//    public void test_appendSubsetWhere_tier1_rate() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_RATE);
//        widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_SUBSET);
//        widget.setIsSubsetTier1(true);
//        String result = impl.appendSubsetWhere(widget, userAccount);
//        assertEquals(result, "AND (ra.Id in null) ");
//    }
//    
//    @Test
//    public void test_appendSubsetWhere_tier1_revenueType() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_REVENUE_TYPE);
//        widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_SUBSET);
//        widget.setIsSubsetTier1(true);
//        String result = impl.appendSubsetWhere(widget, userAccount);
//        assertEquals(result, "AND (rt.Id in null) ");
//    }
//    
//    @Test
//    public void test_appendSubsetWhere_tier2_organization() {
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_ORG);
//        widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_SUBSET);
//        widget.setIsSubsetTier2(true);
//        String result = impl.appendSubsetWhere(widget, userAccount);
//        assertEquals(result, "AND (c.Id in null) ");
//    }
//    
//    @Test
//    public void test_appendSubsetWhere_tier2_location() {
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_SUBSET);
//        widget.setIsSubsetTier2(true);
//        String result = impl.appendSubsetWhere(widget, userAccount);
//        assertEquals(result, "AND (l.Id in null) ");
//    }
//    
//    @Test
//    public void test_appendSubsetWhere_tier2_route() {
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//        widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_SUBSET);
//        widget.setIsSubsetTier2(true);
//        String result = impl.appendSubsetWhere(widget, userAccount);
//        assertEquals(result, "AND (ro.Id in null OR ro.Id is null) ");
//    }
//    
//    // TODO UNNECESSARY TEST resulting SQL syntax is wrong
//    //    @Test 
//    //    public void test_appendSubsetWhere_tier2_rate() {
//    //        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_RATE);
//    //        widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_SUBSET);
//    //        widget.setIsSubsetTier2(true);
//    //        String result = impl.appendSubsetWhere(widget, userAccount);
//    //        assertEquals(result, "AND (ra.Id in null OR ra.Id is null) ");
//    //    }
//    @Test
//    public void test_appendSubsetWhere_tier2_revenueType() {
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_REVENUE_TYPE);
//        widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_SUBSET);
//        widget.setIsSubsetTier2(true);
//        String result = impl.appendSubsetWhere(widget, userAccount);
//        assertEquals(result, "AND (rt.Id in null) ");
//    }
//    
//    @Test
//    public void test_appendSubsetWhere_tier3_location() {
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_SUBSET);
//        widget.setIsSubsetTier3(true);
//        String result = impl.appendSubsetWhere(widget, userAccount);
//        assertEquals(result, "AND (l.Id in null) ");
//    }
//    
//    @Test
//    public void test_appendSubsetWhere_tier3_revenueType() {
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_REVENUE_TYPE);
//        widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_SUBSET);
//        widget.setIsSubsetTier3(true);
//        String result = impl.appendSubsetWhere(widget, userAccount);
//        assertEquals(result, "AND (rt.Id in null) ");
//    }
//    
//    @Test
//    public void test_appendSubsetWhere_tier3_route() {
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//        widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_SUBSET);
//        widget.setIsSubsetTier3(true);
//        String result = impl.appendSubsetWhere(widget, userAccount);
//        assertEquals(result, "AND (ro.Id in null OR ro.Id is null) ");
//    }
//    
//    @Test
//    public void test_appendSubsetWhere_tier1_paystation_tier2_location_tier3_revenueType() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_ORG);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.getWidgetTierTypeByWidgetTier3Type().setId(WidgetConstants.TIER_TYPE_REVENUE_TYPE);
//        widget.setIsSubsetTier1(true);
//        widget.setIsSubsetTier2(true);
//        widget.setIsSubsetTier3(true);
//        widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_SUBSET);
//        String result = impl.appendSubsetWhere(widget, userAccount);
//        assertEquals(result, "AND (c.Id in null) AND (l.Id in null) AND (rt.Id in null) ");
//    }
}
