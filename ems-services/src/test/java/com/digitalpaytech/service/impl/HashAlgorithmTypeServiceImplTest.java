package com.digitalpaytech.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;

import com.digitalpaytech.domain.HashAlgorithmType;
import com.digitalpaytech.dto.crypto.HashAlgorithmData;


public class HashAlgorithmTypeServiceImplTest {
    private static Logger log = Logger.getLogger(HashAlgorithmTypeServiceImplTest.class);
    private HashAlgorithmTypeServiceImpl algTypeService = getHashAlgorithmTypeServiceImpl();
    

    @Test
    public void loadHashAlgorithmDataMap() {
        // Test data, override 'loadAll' method.
        algTypeService.setHashAlgorithmsTypes(algTypeService.loadAll());
        algTypeService.setHashAlgorithmNotSigningMap(new HashMap<Integer, HashAlgorithmData>());
        algTypeService.setHashAlgorithmNotSigningAllMap(new HashMap<Integer, HashAlgorithmData>());
        // ------------------------------------------
        
        try {
            algTypeService.loadHashAlgorithmNotSigningMaps();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
        Map<Integer, HashAlgorithmData> map = algTypeService.findHashAlgorithmNotSigningMap();
        assertNotNull(map);
        assertEquals(3, map.size());
        
        Integer id;
        Iterator<Integer> iter = map.keySet().iterator();
        while (iter.hasNext()) {
            id = iter.next();
            if (id == 1) {
                assertNotNull(map.get(id).getCryptoAlgorithm());
                assertEquals("com.digitalpaytech.service.crypto.impl.Sha1AlgorithmImpl", map.get(id).getCryptoAlgorithm().getClass().getName());
                assertEquals("SHA-1", map.get(id).getHashAlgorithm());
                assertEquals("AsGuw8M8ew/p1X3gUDU+iA", new String(map.get(id).getSalts()[0]));  // SALT_BAD_CARD
                assertEquals("kemd57fTC9NDqfrePlMwxG", new String(map.get(id).getSalts()[3]));  // SALT_FILE_UPLOAD
            
            } else if (id == 2 || id == 5) {
                assertNotNull(map.get(id).getCryptoAlgorithm());
                assertEquals("com.digitalpaytech.service.crypto.impl.Sha256AlgorithmImpl", map.get(id).getCryptoAlgorithm().getClass().getName());
                assertEquals("SHA-256", map.get(id).getHashAlgorithm());
                assertEquals("V!1+@!gJ)go-fyTW!fo#?e", new String(map.get(id).getSalts()[2]));  // SALT_CREDIT_CARD_PROCESSING
                assertEquals("TvjO23EFl#)fe*)Pw7@#5J", new String(map.get(id).getSalts()[5]));  // SALT_TRANSACTION_UPLOAD
            }
        }
    }
    

    
    @Test
    public void findForInternalKeyRotation() {
        // Test data, override 'loadAll' & 'reloadHashAlgorithmDataMap' method.
        algTypeService.setHashAlgorithmsTypes(algTypeService.loadAll());
        algTypeService.setHashAlgorithmNotSigningMap(new HashMap<Integer, HashAlgorithmData>());
        algTypeService.setHashAlgorithmNotSigningAllMap(new HashMap<Integer, HashAlgorithmData>());
        // ------------------------------------------

        try {
            algTypeService.reloadHashAlgorithmNotSigningMaps();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
        // ------------------------------------------
        
        Map<Integer, HashAlgorithmData> map = algTypeService.findForInternalKeyRotation();
        assertNotNull(map);
        if (map.size() == 0 || map.size() > 1) {
            fail("findForInternalKeyRotation returns 0 or more than 1 record, size: " + map.size());
        }
        assertEquals("SHA-1", map.get(1).getHashAlgorithm());
    }
    
    private HashAlgorithmTypeServiceImpl getHashAlgorithmTypeServiceImpl() {
        algTypeService = new HashAlgorithmTypeServiceImpl() {
            @Override
            public List<HashAlgorithmType> loadAll() {
                HashAlgorithmType t1 = new HashAlgorithmType();
                t1.setId(1);
                t1.setName("SHA-1");
                t1.setIsDeleted(false);
                t1.setIsForSigning(false);
                t1.setClassName("Sha1");
                t1.setSignatureId(3);
                
                HashAlgorithmType t2 = new HashAlgorithmType();
                t2.setId(2);
                t2.setName("SHA-256");
                t2.setIsDeleted(false);
                t2.setIsForSigning(false);
                t2.setClassName("Sha256");                
                t2.setSignatureId(4);
                
                HashAlgorithmType t3 = new HashAlgorithmType();
                t3.setId(3);
                t3.setName("SHA1withRSA");
                t3.setIsDeleted(false);
                t3.setIsForSigning(true);
                t3.setClassName(null);                
                t3.setSignatureId(0);
                
                HashAlgorithmType t4 = new HashAlgorithmType();
                t4.setId(4);
                t4.setName("SHA256withRSA");
                t4.setIsDeleted(false);
                t4.setIsForSigning(true);
                t4.setClassName(null);                
                t4.setSignatureId(0);                

                HashAlgorithmType t5 = new HashAlgorithmType();
                t5.setId(5);
                t5.setName("SHA-256-Iris");
                t5.setIsDeleted(false);
                t5.setIsForSigning(false);
                t5.setClassName("Sha256");                
                t5.setSignatureId(4);                
                
                List<HashAlgorithmType> list = new ArrayList<HashAlgorithmType>(2);
                list.add(t1);
                list.add(t2);
                list.add(t3);
                list.add(t4);
                list.add(t5);
                
                return list;
            }
        };
        return algTypeService;
    }
    
}
