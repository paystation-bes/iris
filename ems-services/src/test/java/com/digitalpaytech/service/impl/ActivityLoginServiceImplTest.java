package com.digitalpaytech.service.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.dao.impl.EntityDaoImpl;
import com.digitalpaytech.domain.ActivityLogin;
import com.digitalpaytech.domain.UserAccount;

public class ActivityLoginServiceImplTest {

    @Test
    public void test_findInvalidActivityLoginByLastModifiedGMT() {
        
        ActivityLoginServiceImpl service = new ActivityLoginServiceImpl();
        
        EntityDao dao = new EntityDaoImpl() {
            
            @Override
            public List findByNamedQueryAndNamedParam(String queryName, String paramName, Object value, boolean cacheable) {
                List<UserAccount> result = new ArrayList<UserAccount>();
                UserAccount account = new UserAccount();
                account.setId(1);
                result.add(account);
                return result;
            }
            
            @Override
            public List findByNamedQueryAndNamedParam(String queryName, String[] paramName, Object[] value) {
                List<ActivityLogin> lists = new ArrayList<ActivityLogin>();
                lists.add(new ActivityLogin());
                lists.add(new ActivityLogin());
                return lists;
                
            }
        };
        
        service.setEntityDao(dao);
        List<ActivityLogin> result = service.findInvalidActivityLoginByLastModifiedGMT("text", 10);
        
        assertNotNull(result);
        assertEquals(result.size(), 2);
    }

}
