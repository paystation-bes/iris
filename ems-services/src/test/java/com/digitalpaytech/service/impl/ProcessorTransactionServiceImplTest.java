package com.digitalpaytech.service.impl;

import org.junit.Test;
import static org.junit.Assert.*;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.ProcessorTransactionType;

public class ProcessorTransactionServiceImplTest {
    @Test
    public void isRefundableTransaction() {
        final ProcessorTransactionServiceImpl serv = new ProcessorTransactionServiceImpl();
        // 1    Auth PlaceHolder
        assertFalse(serv.isRefundableTransaction(create(1)));
        
        // 2   Real-Time
        assertTrue(serv.isRefundableTransaction(create(2)));

        // 3   Batch
        assertTrue(serv.isRefundableTransaction(create(3)));
        
        // 4   Refund
        assertFalse(serv.isRefundableTransaction(create(4)));
        
        // 5   Reversal
        assertFalse(serv.isRefundableTransaction(create(5)));
        
        // 6   Real-Time
        assertFalse(serv.isRefundableTransaction(create(6)));
        
        // 7   Batch
        assertFalse(serv.isRefundableTransaction(create(7)));
        
        // 8   Uncloseable
        assertFalse(serv.isRefundableTransaction(create(8)));
        
        // 9   3rd Party
        assertFalse(serv.isRefundableTransaction(create(9)));
        
        // 10  Batch
        assertTrue(serv.isRefundableTransaction(create(10)));
        
        // 11  Store & Fwd
        assertTrue(serv.isRefundableTransaction(create(11)));
        
        // 12  Batch
        assertFalse(serv.isRefundableTransaction(create(12)));
        
        // 13  Store & Fwd
        assertFalse(serv.isRefundableTransaction(create(13)));
        
        // 14  Encryption Error
        assertFalse(serv.isRefundableTransaction(create(14)));
        
        // 17  Real-Time
        assertTrue(serv.isRefundableTransaction(create(17)));
        
        // 18  Refund
        assertFalse(serv.isRefundableTransaction(create(18)));
        
        // 19  Real-Time
        assertFalse(serv.isRefundableTransaction(create(19)));
        
        // 20  Recoverable
        assertFalse(serv.isRefundableTransaction(create(20)));
        
        // 21  Real-Time
        assertTrue(serv.isRefundableTransaction(create(21)));
        
        // 22  Refund
        assertFalse(serv.isRefundableTransaction(create(22)));
        
        // 23  Real-Time
        assertFalse(serv.isRefundableTransaction(create(23)));
        
        // 24  Expired-Refund
        assertFalse(serv.isRefundableTransaction(create(24)));
        
        // 99  Cancel
        assertFalse(serv.isRefundableTransaction(create(99)));
    }
    
    private ProcessorTransaction create(int processorTransactionTypeId) {
        final ProcessorTransaction pt = new ProcessorTransaction();
        pt.setProcessorTransactionType(new ProcessorTransactionType(processorTransactionTypeId));
        return pt;
    }
}
