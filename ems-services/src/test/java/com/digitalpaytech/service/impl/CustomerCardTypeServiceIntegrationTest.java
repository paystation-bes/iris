package com.digitalpaytech.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.client.CardTypeClient;
import com.digitalpaytech.client.dto.ObjectResponse;
import com.digitalpaytech.client.dto.corecps.CardConfiguration;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.ribbon.mock.MockClientFactory;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.json.JSON;

@SuppressWarnings({ "checkstyle:magicnumber", "checkstyle:methodlength" })
public final class CustomerCardTypeServiceIntegrationTest {
    
    private final TestContext ctx = TestContext.getInstance();
    
    @SuppressWarnings("unused")
    private final EntityDaoTest entityDao = new EntityDaoTest();
    
    private String dummyEnd = "45678912=01234567890123456789";
    
    @InjectMocks
    private LinkCardTypeServiceImpl linkCardTypeService;
    
    @InjectMocks
    private MockClientFactory client;
    
    @InjectMocks
    private CardTypeServiceImpl cardTypeService;
    
    @InjectMocks
    private CustomerCardTypeServiceImpl customerCardTypeService;
    
    @InjectMocks
    private EntityServiceImpl entityService;
    
    private JSON json = new JSON();
    
    private String buildResponse() throws JsonException {
        final CardConfiguration cupConfig = new CardConfiguration();
        cupConfig.setCardType("UnionPay");
        cupConfig.setPatterns(new String[] { "62\\d{14}=\\d{7,20}", "62\\d{14}", "81([0-6][0-9]|7[0-1])\\d{12}=\\d{7,20}",
            "81([0-6][0-9]|7[0-1])\\d{12}" });
        
        final CardConfiguration jcbConfig = new CardConfiguration();
        jcbConfig.setCardType("JCB");
        jcbConfig.setPatterns(new String[] { "35\\d{14}=\\d{7,20}", "(2131|1800)\\d{11}=\\d{7,21}", "35\\d{14}", "(2131|1800)\\d{11}",
            "30(8[8-9]|9[0-4])\\d{12}=\\d{7,20}", "30(8[8-9]|9[0-4])\\d{12}", "3(09[6-9]|10[0-2])\\d{12}=\\d{7,20}", "3(09[6-9]|10[0-2])\\d{12}",
            "31(1[2-9]|20)\\d{12}=\\d{7,20}", "31(1[2-9]|20)\\d{12}", "315[8-9]\\d{12}=\\d{7,20}", "315[8-9]\\d{12}",
            "33(3[7-9]|4[0-9])\\d{12}=\\d{7,20}", "33(3[7-9]|4[0-9])\\d{12}" });
        
        return this.json.serialize(new ObjectResponse<>(null, Arrays.asList(cupConfig, jcbConfig)));
    }
    
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.client.prepareForSuccessRequest(CardTypeClient.class, this.buildResponse().getBytes());
        this.ctx.autowiresFromTestObject(this);
        
        this.linkCardTypeService.init();
        this.customerCardTypeService.init();
    }
    
    @After
    public void tearDown() throws Exception {
    }
    
    @Test
    public void testJCBNumbers() {
        assertCardNumberRangeMatchesPattern(30880000, 30949999, CardProcessingConstants.NAME_JCB);
        assertCardNumberRangeMatchesPattern(31120000, 31209999, CardProcessingConstants.NAME_JCB);
        assertCardNumberRangeMatchesPattern(31580000, 31599999, CardProcessingConstants.NAME_JCB);
        assertCardNumberRangeMatchesPattern(33370000, 33499999, CardProcessingConstants.NAME_JCB);
    }
    
    @Test
    public void testCUPNumbers() {
        assertCardNumberRangeMatchesPattern(81000000, 81000000, CardProcessingConstants.NAME_UNION_PAY);
        assertCardNumberRangeMatchesPattern(81100000, 81319999, CardProcessingConstants.NAME_UNION_PAY);
        assertCardNumberRangeMatchesPattern(81320000, 81519999, CardProcessingConstants.NAME_UNION_PAY);
        assertCardNumberRangeMatchesPattern(81520000, 81639999, CardProcessingConstants.NAME_UNION_PAY);
        assertCardNumberRangeMatchesPattern(81640000, 81719999, CardProcessingConstants.NAME_UNION_PAY);
        assertCardNumberRangeMatchesPattern(81640000, 81719999, CardProcessingConstants.NAME_UNION_PAY);
    }
    
    private void assertCardNumberRangeMatchesPattern(final int start, final int end, final String cardTypeName) {
        final List<Pattern> cardTypePatterns = this.customerCardTypeService.getCreditCustomerCardTypes().stream()
                .filter(cct -> cct.getName().equals(cardTypeName)).map(cct -> Pattern.compile(cct.getTrack2pattern())).collect(Collectors.toList());
        
        for (int i = start; i <= end; i++) {
            final String iString = String.valueOf(i);
            Assert.assertTrue(String.valueOf(i) + dummyEnd + " did not match any regex for " + cardTypeName,
                              cardTypePatterns.stream().anyMatch(pat -> pat.matcher(String.valueOf(iString) + dummyEnd).matches()));
        }
    }
    
}
