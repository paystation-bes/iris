package com.digitalpaytech.service.impl;

import org.junit.Before;
import org.junit.Test;
import java.util.*;

import com.digitalpaytech.util.crypto.CryptoConstants;
import com.digitalpaytech.util.json.JSON;
import com.digitalpaytech.util.json.JSONConfigurationBean;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;

public class CryptoServiceIsUseOldKeyTest {
    private static final Logger LOG = Logger.getLogger(CryptoServiceImplTest.class);
    private static final String SCALA_CC = CryptoConstants.IRIS_EXTERNAL_KEY_PREFIX + "0534242424242424242=202012999999999999999999";
    private static final String SCALA_CC_AMEX = CryptoConstants.IRIS_EXTERNAL_KEY_PREFIX + "04841111111111111=202012999999999999999999";
    private static final String LINK_CC = CryptoConstants.LINK_EXTERNAL_KEY_PREFIX + "0035454545454545454=202012111111111111111111";
    private MockCryptoServiceSupport support;
    private MockCryptoServiceSupport.MockEmsPropertiesService propService;
    private CryptoServiceImpl cryptoServiceImpl;
    
    @Before
    public void before() {
        support = new MockCryptoServiceSupport();
        support.setForIsUseOldKey(true);
        propService = support.new MockEmsPropertiesService();
        cryptoServiceImpl = new CryptoServiceImpl();
        cryptoServiceImpl.setEmsPropertiesService(propService);
        cryptoServiceImpl.setLinkCryptoClient(support.new MockLinkCryptoClient());
        cryptoServiceImpl.setCryptoClient(support.new MockCryptoClient());
        cryptoServiceImpl.setPointOfSaleService(support.new MockPointOfSaleService());
        cryptoServiceImpl.setMailerService(support.new MockMailerService());
        cryptoServiceImpl.setPosServiceStateService(support.new MockPosServiceStateService());
        cryptoServiceImpl.setJson(new JSON(new JSONConfigurationBean(true, false, false)));
    }
    
    @Test
    public void decryptData() {
        support.setNoKeyInfo(true);
        propService.setEncryptionMode("CryptoService");
        propService.setUseLinkCrypto("0");
        try {
            String data = cryptoServiceImpl.decryptData(SCALA_CC, 1, createDate(2018, 01, 14));
            assertNotNull(data);
            assertEquals(SCALA_CC, data);

            support.setNoKeyInfo(false);
            data = cryptoServiceImpl.decryptData(SCALA_CC_AMEX, 5, createDate(2018, 01, 14));
            assertNotNull(data);
            assertEquals(SCALA_CC_AMEX, data);
            
            
            support.setNoKeyInfo(false);
            propService.setUseLinkCrypto("1");
            data = cryptoServiceImpl.decryptData(LINK_CC, 200, createDate(2018, 01, 14));
            assertNotNull(data);
            assertEquals(LINK_CC, data);
        
        } catch (Exception e) {
            LOG.error(e);
            fail(e.getMessage());
        }
        
    }
    
    private Date createDate(int year, int month, int day) {
        GregorianCalendar cal = new GregorianCalendar(year, month, day);
        return cal.getTime();
    }
}
