package com.digitalpaytech.service.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.Future;

import javax.mail.Address;

import org.apache.commons.lang.BooleanUtils;

import com.digitalpaytech.client.CryptoClient;
import com.digitalpaytech.client.LinkCryptoClient;
import com.digitalpaytech.client.dto.RSAKeyInfo;
import com.digitalpaytech.clientcommunication.dto.MultipartFileWrapper;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerAgreement;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.CustomerURLReroute;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MerchantPOS;
import com.digitalpaytech.domain.ModemSetting;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PaystationType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosBalance;
import com.digitalpaytech.domain.PosDate;
import com.digitalpaytech.domain.PosDateType;
import com.digitalpaytech.domain.PosHeartbeat;
import com.digitalpaytech.domain.PosSensorState;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.PosStatus;
import com.digitalpaytech.dto.MapEntry;
import com.digitalpaytech.dto.PayStationSelectorDTO;
import com.digitalpaytech.dto.PointOfSaleAlertInfo;
import com.digitalpaytech.dto.PointOfSaleSearchCriteria;
import com.digitalpaytech.dto.customeradmin.LocationTree;
import com.digitalpaytech.dto.customeradmin.PaystationListInfo;
import com.digitalpaytech.dto.customeradmin.PlacementMapBordersInfo;
import com.digitalpaytech.dto.mobile.rest.CollectPaystationSummaryDTO;
import com.digitalpaytech.dto.paystation.PosServiceStateInitInfo;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.EncryptionService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.paystation.PosServiceStateService;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.crypto.CryptoConstants;
import com.digitalpaytech.util.json.JSON;
import com.netflix.ribbon.RequestWithMetaData;
import com.netflix.ribbon.RibbonRequest;

import io.netty.buffer.ByteBuf;
import rx.Observable;

public class MockCryptoServiceSupport {
    private static final String LINK_KEY_NUM = "005";
    private static final String SCALA_KEY_NUM = "075";
    private boolean forIsUseOldKey;
    private boolean noKeyInfo;
    private String isLinkCrypto = "0";
    
    
    public void setForIsUseOldKey(boolean forIsUseOldKey) {
        this.forIsUseOldKey = forIsUseOldKey;
    }
    public void setIsLinkCrypto(String isLinkCrypto) {
        this.isLinkCrypto = isLinkCrypto;
    }
    public void setNoKeyInfo(boolean noKeyInfo) {
        this.noKeyInfo = noKeyInfo;
    }
    
    class MockLinkCryptoClient implements LinkCryptoClient {
        private RibbonRequest createRibbonRequest(String data) {
            return createRibbonRequest(true, data);
        }

        private RibbonRequest createRibbonRequest(boolean hasTitle, String data) {
            RibbonRequest rrequest = new MockRibbonRequest();
            String title = hasTitle ? "MockLinkCryptoClient - "+ data : data;
            ((MockRibbonRequest) rrequest).setData(title);
            return rrequest;
        }        
        
        
        @Override
        public RibbonRequest<ByteBuf> encryptInternal(String plainText) {
            return createRibbonRequest("encryptInternal - "+ plainText);
        }

        @Override
        public RibbonRequest<ByteBuf> currentInternalKey() {
            return createRibbonRequest("currentInternalKey");
        }
        
        @Override
        public RibbonRequest<ByteBuf> decrypt(String encryptedText) {
            return createRibbonRequest(false, encryptedText);
        }
        
        @Override
        public RibbonRequest<ByteBuf> currentExternalKey() {
            if (forIsUseOldKey) {
                return createRibbonRequest(false, CryptoConstants.LINK_EXTERNAL_KEY_PREFIX + LINK_KEY_NUM);
            } else {
                return createRibbonRequest("currentExternalKey");
            }
        }
        
        @Override
        public RibbonRequest<ByteBuf> keyInfo(String keyAlias) {
            if (noKeyInfo) {
                return null;
            }
            
            RSAKeyInfo rsa = new RSAKeyInfo();
            rsa.setName("LINK");
            rsa.setCreateTime(new Date());
            JSON j = new JSON();
            try {
                return createRibbonRequest(false, j.serialize(rsa));
            } catch (JsonException e) {
                e.printStackTrace();
            }
            return null;
        }

        
        
        @Override
        public RibbonRequest<ByteBuf> export(String keyAlias, String hashAlgorithm) {
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        public RibbonRequest<ByteBuf> signFile(String algorithm, MultipartFileWrapper multipartFileWrapper) {
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        public RibbonRequest<ByteBuf> listExternalKeys() {
            // TODO Auto-generated method stub
            return null;
        }
    }
    
    
    class MockCryptoClient implements CryptoClient {
        private RibbonRequest createRibbonRequest(String data) {
            return createRibbonRequest(true, data);
        }
        
        private RibbonRequest createRibbonRequest(boolean hasTitle, String data) {
            RibbonRequest rrequest = new MockRibbonRequest();
            String title = hasTitle ? "MockCryptoClient - "+ data : data;
            ((MockRibbonRequest) rrequest).setData(title);
            return rrequest;
        }        
        
        @Override
        public RibbonRequest<ByteBuf> encrypt(String decrypted) {
            return createRibbonRequest("encrypt - "+ decrypted);
        }

        @Override
        public RibbonRequest<ByteBuf> currentInternalKey() {
            return createRibbonRequest("currentInternalKey");
        }

        @Override
        public RibbonRequest<ByteBuf> currentExternalKey() {
            if (forIsUseOldKey) {
                 return createRibbonRequest(false, CryptoConstants.IRIS_EXTERNAL_KEY_PREFIX + SCALA_KEY_NUM);
            } else {
                return createRibbonRequest("currentExternalKey");
            }
        }
        
        @Override
        public RibbonRequest<ByteBuf> keyInfo(String keyAlias) {
            if (noKeyInfo) {
                return null;
            }
            
            GregorianCalendar cal = new GregorianCalendar(2018, 9, 30);
            RSAKeyInfo rsa = new RSAKeyInfo();
            rsa.setName("Scala");
            rsa.setCreateTime(cal.getTime());
            JSON j = new JSON();
            try {
                return createRibbonRequest(false, j.serialize(rsa));
            } catch (JsonException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        public RibbonRequest<ByteBuf> decrypt(String encrypted) {
            return createRibbonRequest(false, encrypted);
        }

        
        
        @Override
        public RibbonRequest<ByteBuf> listExternalKey() {
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        public RibbonRequest<ByteBuf> rotate() {
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        public RibbonRequest<ByteBuf> export(String keyAlias, String hashAlgorithm) {
            // TODO Auto-generated method stub
            return null;
        }
    }

    
    class MockPosServiceStateService implements PosServiceStateService {

        @Override
        public PosServiceState findPosServiceStateById(Integer id, boolean isEvicted) {
            PosServiceState pss = new PosServiceState();
            if (id == 50) {
                pss.setIsSwitchLinkCrypto(true);
                pss.setIsUsingLinkCrypto(true);
            }
            return pss;
        }

        @Override
        public void updatePosServiceState(PosServiceState posServiceState) {
        }

        
        @Override
        public Collection<PosServiceState> findAllPosServiceStates(boolean isEvicted) {
            return null;
        }
        @Override
        public void processInitialization(Integer id, PosServiceStateInitInfo posServiceStateInitInfo) {
        }
        @Override
        public List<PosServiceState> findPosServiceStateByNextSettingsFileId(Integer nextSettingsFileId) {
            return null;
        }
        @Override
        public void checkforEMVMerchantAccount(MerchantAccount merchantAccount, List<Integer> posId) {
        }

        @Override
        public void updatePosServiceStates(Collection<PosServiceState> posServiceStates) {
            // TODO Auto-generated method stub
            
        }

        @Override
        public Collection<PosServiceState> findAllLinuxPosServiceStates() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Collection<PosServiceState> findLinuxPosServiceStatesByLinkCustomerId(Integer linkCustomerId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public PosServiceState findLinuxPosServiceStateBySerialNumber(String serialNumber) {
            // TODO Auto-generated method stub
            return null;
        }
        
    }
    
    class MockRibbonRequest implements RibbonRequest {
        private String data;
        
        public void setData(String data) {
            this.data = data;
        }
        
        @Override
        public Object execute() {
            ByteBuf byteBuf = new MockByteBuf(data);
            return byteBuf;
        }

        
        
        @Override
        public Observable observe() {
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        public Future queue() {
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        public Observable toObservable() {
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        public RequestWithMetaData withMetadata() {
            // TODO Auto-generated method stub
            return null;
        }
    }
    
    class MockMailerService implements MailerService {
        @Override
        public void sendAdminWarnAlert(String subject, String content, Exception exception) {
        }

        
        
        @Override
        public void sendMessage(String toAddresses, String content) {
            // TODO Auto-generated method stub
        }
        @Override
        public void sendMessage(String toAddresses, Object content, String type) {
            // TODO Auto-generated method stub
        }
        @Override
        public void sendMessage(String toAddresses, String ccAddresses, String bccAddresses, Object content, String type) {
            // TODO Auto-generated method stub
        }
        @Override
        public void sendMessage(Address fromAddress, String toAddresses, String subject, Object content, String type) {
            // TODO Auto-generated method stub
        }
        @Override
        public void sendMessage(String toAddresses, String subject, Object content, String type) {
            // TODO Auto-generated method stub
        }
        @Override
        public void sendMessage(Address fromAddress, String toAddresses, String ccAddresses, String bccAddresses, String subject, Object content,
            String type) {
            // TODO Auto-generated method stub
        }
        @Override
        public void sendMessage(Address fromAddress, String toAddresses, String ccAddresses, String bccAddresses, String subject, Object content,
            String type, byte[] attachment, String filename) {
            // TODO Auto-generated method stub
        }
        @Override
        public void sendMessage(String toAddresses, String subject, Object content, String type, byte[] attachment, String filename) {
            // TODO Auto-generated method stub
        }
        @Override
        public void sendAdminErrorAlert(String subject, String content, Exception exception) {
            // TODO Auto-generated method stub
        }
        @Override
        public void sendAdminAlert(String subject, String content, Exception exception) {
            // TODO Auto-generated method stub
        }
        @Override
        public void sendServiceAgreementAccepted(CustomerAgreement customerAgreement, String customerName) {
            // TODO Auto-generated method stub
        }
    }
    
    
    class MockEmsPropertiesService implements EmsPropertiesService {
        private String encryptionMode;
        private String useLinkCrypto;
        
        public void setEncryptionMode(String encryptionMode) {
            this.encryptionMode = encryptionMode;
        }
        public void setUseLinkCrypto(String useLinkCrypto) {
            this.useLinkCrypto = useLinkCrypto;
        }
        public String getUseLinkCrypto() {
            return this.useLinkCrypto;
        }
        
        @Override
        public String getPropertyValue(String propertyName, String defaultValue, boolean forceGet) {
            if (EmsPropertiesService.ENCRYPTION_MODE.equals(propertyName)) {
                return EncryptionService.CRYPTO_SERVER;
            } else {
                return defaultValue;
            }

        }

        @Override
        public boolean getPropertyValueAsBoolean(String propertyName, boolean defaultValue) {
            if (propertyName.equalsIgnoreCase("UseLinkCrypto")) {
                return BooleanUtils.toBoolean(Integer.parseInt(this.useLinkCrypto));
            }
            return false;
        }
        
        
        @Override
        public int getPropertyValueAsInt(String propertyName, int defaultValue) {
            // TODO Auto-generated method stub
            return 0;
        }
        @Override
        public String getPropertyValue(String propertyName, boolean forceGet) {
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        public String getPropertyValue(String propertyName) {
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        public String getPropertyValue(String propertyName, String defaultValue) {
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        public int getPropertyValueAsInt(String propertyName, int defaultValue, boolean forceGet) {
            // TODO Auto-generated method stub
            return 0;
        }
        @Override
        public long getPropertyValueAsLong(String propertyName, long defaultValue) {
            // TODO Auto-generated method stub
            return 0;
        }
        @Override
        public long getPropertyValueAsLong(String propertyName, long defaultValue, boolean forceGet) {
            // TODO Auto-generated method stub
            return 0;
        }
        @Override
        public int getPropertyValueAsIntQuiet(String propertyName, int defaultValue) {
            // TODO Auto-generated method stub
            return 0;
        }
        @Override
        public void findPropertiesFile() {
            // TODO Auto-generated method stub
            
        }
        @Override
        public boolean isWindowsOS() {
            // TODO Auto-generated method stub
            return false;
        }
        @Override
        public void updatePrimaryServer(String newPrimaryServer) {
            // TODO Auto-generated method stub
            
        }
        @Override
        public void updatePatchExecution(boolean execute) {
            // TODO Auto-generated method stub
            
        }
        @Override
        public void loadPropertiesFile() {
            // TODO Auto-generated method stub
            
        }
        @Override
        public boolean getPropertyValueAsBoolean(String propertyName, boolean defaultValue, boolean forceGet) {
            // TODO Auto-generated method stub
            return false;
        }
        @Override
        public <V> V getProperty(String key, Class<V> valueClass, V defaulValue) {
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        public String getIrisVersion() {
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        public String getIrisBuildNumber() {
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        public String getIrisBuildDate() {
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        public String getStaticContentPath() {
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        public String getStaticContentPatam() {
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        public String getIrisDeploymentMode() {
            // TODO Auto-generated method stub
            return null;
        }
    }
    
    class MockPointOfSaleService implements PointOfSaleService {
        @Override
        public PointOfSale findPointOfSaleBySerialNumber(String serialNumber) {
            PointOfSale pos = new PointOfSale();
            pos.setSerialNumber(serialNumber);
            Customer cust = new Customer();
            if (serialNumber.equals(CryptoServiceImplTest.LINK_PS)) {
                cust.setId(1);
                pos.setCustomer(cust);
            } else if (serialNumber.equals(CryptoServiceImplTest.SCALA_PS)) {
                cust.setId(55);
                pos.setCustomer(cust);
            } else {
                return null;
            }
            return pos;
        }

        @Override
        public PointOfSale findPointOfSaleById(Integer id) {
            Customer cust = new Customer();
            cust.setId(1);
            cust.setIsLinkCrypto(BooleanUtils.toBoolean(isLinkCrypto));
            PointOfSale p = new PointOfSale();
            p.setCustomer(cust);
            return p;
        }

        
        
        
        @Override
        public List<PointOfSale> findPointOfSalesByRouteId(int routeId) {
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        public List<PointOfSale> findPointOfSalesByLocationId(int locationId) {
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        public List<PointOfSale> findPointOfSalesByLocationIdOrderByName(int locationId) {
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        public List<PointOfSale> findPointOfSalesByCustomerId(int customerId) {
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        public List<PointOfSale> findPointOfSalesByCustomerId(int customerId, boolean all) {
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        public List<PointOfSale> findValidPointOfSaleByCustomerIdOrderByName(int customerId) {
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        public List<PointOfSale> findPointOfSaleAndPosStatusByCustomerIdNoCache(int customerId) {
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        public PointOfSale findPointOfSaleAndPosStatusBySerialNumber(String serialNumber) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public PointOfSale findTxablePointOfSaleBySerialNumber(String serialNumber) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<PointOfSale> findPointOfSaleBySerialNumberNoConditions(String serialNumber) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public PointOfSale findPointOfSaleByPaystationId(int payStationId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Collection<PointOfSale> findAllPointOfSalesOrderByName() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<Paystation> findPayStationsByLocationId(int locationId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<Paystation> findPayStationsByCustomerId(int customerId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Paystation findPayStationById(int payStationId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Paystation findPayStationByPointOfSaleId(int pointOfSaleId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public PosStatus findPointOfSaleStatusByPOSId(int pointOfSaleId, boolean useCache) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public PaystationType findPayStationTypeByPointOfSaleId(int pointOfSaleId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public PaystationType findPayStationTypeById(int payStationTypeId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public PosHeartbeat findPosHeartbeatByPointOfSaleId(int pointOfSaleId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public PosServiceState findPosServiceStateByPointOfSaleId(int pointOfSaleId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public PosSensorState findPosSensorStateByPointOfSaleId(int pointOfSaleId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<MerchantPOS> findMerchantPOSbyPointOfSaleId(int pointOfSaleId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<MerchantPOS> findMerchPOSByPOSIdNoDeleted(int pointOfSaleId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public PosBalance findPOSBalanceByPointOfSaleId(int pointOfSaleId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public PosBalance findPOSBalanceByPointOfSaleIdNoCache(int pointOfSaleId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Date findLastCollectionDateByPointOfSaleId(int pointOfSaleId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public ModemSetting findModemSettingByPointOfSaleid(int pointOfSaleId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public void saveOrUpdatePointOfSaleHeartbeat(PosHeartbeat posHeartbeat) {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void saveOrUpdatePOSDate(PosDate posDate) {
            // TODO Auto-generated method stub
            
        }

        @Override
        public PosDateType findPosDateTypeById(int posDateTypeId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<PointOfSale> findPointOfSalesBySettingsFileId(int settingsFileId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<PointOfSale> findPointOfSalesBySerialNumberAndCustomerId(int customerId, List<String> serialNumberList) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Collection<PointOfSale> findAllPointOfSalesBySerialNumberKeyword(String keyword) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<PointOfSale> findAllPointOfSalesBySerialNumberKeyword(String keyword, int customerId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<PointOfSale> findPointOfSalesBySerialNumberKeywordWithLimit(String keyword, int customerId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Collection<PointOfSale> findAllPointOfSalesByPosNameKeyword(String keyword) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<PointOfSale> findAllPointOfSalesByPosNameKeyword(String keyword, int customerId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<PointOfSale> findPointOfSalesByPosNameKeywordWithLimit(String keyword, int customerId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<PointOfSale> findVirtualPointOfSalesByCustomerId(int customerId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<PointOfSale> findNoneVirtualPointOfSalesByCustomerId(int customerId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public void update(PointOfSale pointOfSale) {
            // TODO Auto-generated method stub
            
        }

        @Override
        public List<PointOfSale> findCurrentPointOfSalesBySettingsFileIdForPayStationSettingsPage(Integer settingsFileId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<PointOfSale> findFuturePointOfSalesBySettingsFileIdForPayStationSettingsPage(Integer settingsFileId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<PointOfSale> findUnAssignedPointOfSalesForPayStationSettingsPage(Integer customerId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public PlacementMapBordersInfo findMapBordersByCustomerId(Integer customerId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<PointOfSale> findByCustomerIdAndLocationName(Integer customerId, String locationName) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<PointOfSale> findPointOfSaleAndPosStatusByCustomerId(int customerId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<PointOfSale> findPointOfSaleAndPosStatusByCustomerIdOrderByName(int customerId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public int countUnPlacedByCustomerIds(Collection<Integer> customerIds) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public int countUnPlacedByLocationIds(Collection<Integer> locationIds) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public int countUnPlacedByRouteIds(Collection<Integer> routeIds) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public List<MapEntry> findMapEntriesByCustomerIds(Collection<Integer> customerIds) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<MapEntry> findMapEntriesByLocationIds(Collection<Integer> locationIds) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<MapEntry> findMapEntriesByRouteIds(Collection<Integer> routeIds) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Map<Integer, MapEntry> findPointOfSaleMapEntryDetails(Collection<Integer> pointOfSaleIds, TimeZone timeZone) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Map<Integer, PointOfSale> findPosHashByCustomerIds(Collection<Integer> customerIds) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Map<Integer, PointOfSale> findPosHashByLocationIds(Collection<Integer> locationIds) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Map<Integer, PointOfSale> findPosHashByRouteIds(Collection<Integer> routeIds) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Map<Integer, PointOfSale> findPosHashByPosIds(Collection<Integer> posIds) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<Integer> findPosIdsByCustomerId(Integer customerId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<Integer> findAllIds(List<Integer> customerId, List<Integer> locationIds, List<Integer> routeIds) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<PointOfSale> findAllPointOfSales(List<Integer> customerId, List<Integer> locationIds, List<Integer> routeIds,
            List<Integer> pointOfSaleIds) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public PointOfSale findPointOfSaleBySerialNumberAndCustomerId(String serialNumber, Integer customerId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public boolean isPointOfSalesReadyForMigration(Customer customer, Set<String> versionSet, boolean isForMigration) {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public List<PointOfSale> findPointOfSaleNotReadyForMigration(Integer customerId, Set<String> versionSet) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<PayStationSelectorDTO> findPayStationSelector(PointOfSaleSearchCriteria criteria) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<PaystationListInfo> findPaystation(PointOfSaleSearchCriteria criteria) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public int findPayStationPage(Integer posId, PointOfSaleSearchCriteria criteria) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public void updatePosServicState(PosServiceState posServiceState) {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void updatePosSensorState(PosSensorState posSensorState) {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void updatePaystationActivityTime(PointOfSale pos, boolean isUpdated) {
            // TODO Auto-generated method stub
            
        }

        @Override
        public List<PointOfSale> getDetachedPointOfSaleByLowerCaseNames(int customerId, Collection<String> names, String locPosNameSeparator) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long countPointOfSalesRequiringCollections(Integer routeId, int severity) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public List<PointOfSaleAlertInfo> findActivePointOfSalesWithCollectionAlertsInfo(Integer customerId, Integer routeId, Date date,
            Integer severity, boolean onlyActive, boolean onlyInactive) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<CollectPaystationSummaryDTO> findValidAndActivatedPointOfSalesByCustomerIdAndRouteType(int customerId, int routeTypeId) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<PointOfSaleAlertInfo> findActivePointOfSalesWithRecentHeartbeatOrCollection(Integer customerId, Integer routeId, Date sinceDate) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public int findPayStationCountByCustomerId(Integer customerId) {
            // TODO Auto-generated method stub
            return 0;
        }
        @Override
        public List<PointOfSale> findLinuxPointOfSalesByCustomerId(Integer customerId) {
            // TODO Auto-generated method stub
            return null;
        }
		
		@Override
		public List<PointOfSale> findAllPointOfSalesByPosNameForCustomer(String name, Integer customerId) {
			// TODO Auto-generated method stub
			return null;
		}
        
    }
}
