package com.digitalpaytech.service.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Test;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.dao.OpenSessionDao;
import com.digitalpaytech.domain.ServiceAgreement;

public class ServiceAgreementServiceImplTest {

    @Test
    public void test_findLatestServiceAgreement() {
        ServiceAgreementServiceImpl service = new ServiceAgreementServiceImpl();
        List<ServiceAgreement> lists = new ArrayList<ServiceAgreement>();
        ServiceAgreement serviceAgreement = new ServiceAgreement();
        serviceAgreement.setId(new Short("2"));
        lists.add(serviceAgreement);

        OpenSessionDao entityDao = EasyMock.createMock(OpenSessionDao.class);
        EasyMock.expect(entityDao.findByNamedQuery("ServiceAgreement.findLatestAgreement"))
                .andReturn(lists);
        EasyMock.replay(entityDao);
        
        service.setOpenSessionDao(entityDao);
        ServiceAgreement result = service.findLatestServiceAgreement();
        
        assertNotNull(result);
        assertEquals(result.getId(), 2);
    }
    
    @Test
    public void test_findLatestServiceAgreement_empty() {
        ServiceAgreementServiceImpl service = new ServiceAgreementServiceImpl();
        List<ServiceAgreement> lists = new ArrayList<ServiceAgreement>();

        OpenSessionDao entityDao = EasyMock.createMock(OpenSessionDao.class);
        EasyMock.expect(entityDao.findByNamedQuery("ServiceAgreement.findLatestAgreement"))
                .andReturn(lists);
        EasyMock.replay(entityDao);
        
        service.setOpenSessionDao(entityDao);
        ServiceAgreement result = service.findLatestServiceAgreement();
        
        assertNull(result);
    }
    
    @Test
    public void test_findLatestServiceAgreementByCustomerId() {
        ServiceAgreementServiceImpl service = new ServiceAgreementServiceImpl();
        ServiceAgreement agreement = new ServiceAgreement();
        agreement.setId(3);

        EntityDao entityDao = EasyMock.createMock(EntityDao.class);
        EasyMock.expect(entityDao.findUniqueByNamedQueryAndNamedParam(
                "ServiceAgreement.findLatestAgreementByCustomerId", "customerId", 2, true))
                .andReturn(agreement);
        EasyMock.replay(entityDao);
        
        service.setEntityDao(entityDao);
        ServiceAgreement result = service.findLatestServiceAgreementByCustomerId(2);
        
        assertNotNull(result);
        assertEquals(result.getId(), 3);
    }
}
