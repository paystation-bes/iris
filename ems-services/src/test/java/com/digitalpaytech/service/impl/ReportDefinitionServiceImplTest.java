package com.digitalpaytech.service.impl;

import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.junit.Test;

import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerStatusType;
import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportRepeatType;
import com.digitalpaytech.domain.ReportStatusType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserStatusType;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.impl.ReportingUtilImpl;

public class ReportDefinitionServiceImplTest extends ReportingUtilImpl {
    
    private final EntityDaoTest entityDao = new EntityDaoTest();

    @Test
    public void isDateCalculatedCorrect() {
        ReportDefinition reportDefinition = new ReportDefinition();
        
        // initial date is 01/01/2012 12:30
        Calendar initialDate = Calendar.getInstance();
        initialDate.setTimeZone(TimeZone.getTimeZone("US/Pacific"));
        initialDate.set(Calendar.YEAR, 2012);
        initialDate.set(Calendar.DAY_OF_YEAR, 1);
        initialDate.set(Calendar.HOUR_OF_DAY, 12);
        initialDate.set(Calendar.MINUTE, 30);
        initialDate.set(Calendar.SECOND, 0);
        initialDate.set(Calendar.MILLISECOND, 0);
        reportDefinition.setScheduledToRunGmt(((Calendar) initialDate.clone()).getTime());
        
        // TEST 1 daily at 13:30
        reportDefinition.setReportRepeatType(new ReportRepeatType(ReportingConstants.REPORT_REPEAT_TYPE_DAILY, "Daily", new Date(), 0));
        reportDefinition.setReportRepeatTimeOfDay("13:30");
        // should be 2 JAN 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        Calendar compareDate = (Calendar) initialDate.clone();
        compareDate.set(Calendar.DAY_OF_YEAR, 2);
        compareDate.set(Calendar.HOUR_OF_DAY, 13);
        compareDate.set(Calendar.MINUTE, 30);
        
        System.out.println(reportDefinition.getScheduledToRunGmt().getTime());
        System.out.println(compareDate.getTimeInMillis());
        System.out.println(compareDate.getTime());
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        
        // TEST 2 weekly every Monday at 14:30
        reportDefinition.setReportRepeatType(new ReportRepeatType(ReportingConstants.REPORT_REPEAT_TYPE_WEEKLY, "Weekly", new Date(), 1));
        reportDefinition.setReportRepeatTimeOfDay("14:30");
        reportDefinition.setReportRepeatFrequency((byte) 1);
        reportDefinition.setReportRepeatDays("2");
        // should be 9 JAN 14:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_YEAR, 9);
        compareDate.set(Calendar.HOUR_OF_DAY, 14);
        compareDate.set(Calendar.MINUTE, 30);
        
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        
        // TEST 3 weekly every Monday, Thuesday and Wednesday at 11:30
        reportDefinition.setReportRepeatTimeOfDay("13:30");
        reportDefinition.setReportRepeatFrequency((byte) 1);
        reportDefinition.setReportRepeatDays("2,3,4");
        // should be 10 JAN 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.HOUR_OF_DAY, 13);
        compareDate.set(Calendar.MINUTE, 30);
        compareDate.set(Calendar.DAY_OF_YEAR, 10);
        
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        // should be 11 JAN 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_YEAR, 11);
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        // should be 16 JAN 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_YEAR, 16);
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        
        // TEST 4 bi-weekly every Thuesday and Thursday at 11:30
        reportDefinition.setReportRepeatFrequency((byte) 2);
        reportDefinition.setReportRepeatDays("3,5");
        // should be 17 JAN 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_YEAR, 17);
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        // should be 19 JAN 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_YEAR, 19);
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        // should be 31 JAN 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_YEAR, 31);
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        // should be 2 FEB 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_MONTH, 2);
        compareDate.set(Calendar.MONTH, 1);
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        
        // TEST 5 monthly every 31th day of the month at 11:30
        reportDefinition.setReportRepeatType(new ReportRepeatType(ReportingConstants.REPORT_REPEAT_TYPE_MONTHLY, "Monthly", new Date(), 1));
        reportDefinition.setReportRepeatFrequency((byte) 1);
        reportDefinition.setReportRepeatDays("31");
        // should be 31 MAR 13:30 - 1 MAR is skipped because normally changing the schedule would fix the date, the calculater
        // assumes that the schedule has not changed since last report. When the schedule is changed the date in the DB 
        // should be set to the first day of the new schedule
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_MONTH, 31);
        compareDate.set(Calendar.MONTH, 2);
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        // should be 1 MAY 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_MONTH, 1);
        compareDate.set(Calendar.MONTH, 4);
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        // should be 31 MAY 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_MONTH, 31);
        compareDate.set(Calendar.MONTH, 4);
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        
        // TEST 6 monthly every 15 and 30th day of the month at 11:30
        reportDefinition.setReportRepeatDays("15,31");
        // should be 15 JUN 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_MONTH, 15);
        compareDate.set(Calendar.MONTH, 5);
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        // should be 1 JUL 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_MONTH, 1);
        compareDate.set(Calendar.MONTH, 6);
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        // should be 15 JUL 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_MONTH, 15);
        compareDate.set(Calendar.MONTH, 6);
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        // should be 31 JUL 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_MONTH, 31);
        compareDate.set(Calendar.MONTH, 6);
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        
        // TEST 7 monthly every 31th day of the month at 11:30 - For testing February 2013
        reportDefinition.setReportRepeatType(new ReportRepeatType(ReportingConstants.REPORT_REPEAT_TYPE_MONTHLY, "Monthly", new Date(), 1));
        reportDefinition.setReportRepeatDays("31");
        // should be 31 AUG 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_MONTH, 31);
        compareDate.set(Calendar.MONTH, 7);
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        // should be 1 OCT 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_MONTH, 1);
        compareDate.set(Calendar.MONTH, 9);
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        // should be 31 OCT 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_MONTH, 31);
        compareDate.set(Calendar.MONTH, 9);
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        // should be 1 DEC 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_MONTH, 1);
        compareDate.set(Calendar.MONTH, 11);
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        // should be 31 DEC 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_MONTH, 31);
        compareDate.set(Calendar.MONTH, 11);
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        
        // TEST 8 monthly every 15 and 30th day of the month at 11:30
        reportDefinition.setReportRepeatDays("15,31");
        // should be 15 JAN 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_MONTH, 15);
        compareDate.set(Calendar.MONTH, 0);
        compareDate.set(Calendar.YEAR, 2013);
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        // should be 31 JAN 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_MONTH, 31);
        compareDate.set(Calendar.MONTH, 0);
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        // should be 15 FEB 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_MONTH, 15);
        compareDate.set(Calendar.MONTH, 1);
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        // should be 1 MAR 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_MONTH, 1);
        compareDate.set(Calendar.MONTH, 2);
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        // should be 15 MAR 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_MONTH, 15);
        compareDate.set(Calendar.MONTH, 2);
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        // should be 31 MAR 13:30
        calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), "US/Pacific", false);
        compareDate.set(Calendar.DAY_OF_MONTH, 31);
        compareDate.set(Calendar.MONTH, 2);
        assertTrue(reportDefinition.getScheduledToRunGmt().getTime() == compareDate.getTimeInMillis());
        
    }
    
//    @Test
//    public void DontShowReportsForDeletedCustomers() {
//        runCustomerStatusTypeTest(3, false);
//    }
    
//    @Test
//    public void DontShowReportsForDisabledCustomers() {
//        runCustomerStatusTypeTest(0, false);
//    }
    
    @Test
    public void ShowReportsForTrialCustomer() {
        runCustomerStatusTypeTest(2, true);
    }
    
    @Test
    public void ShowReportsForEnabledCustomer() {
        runCustomerStatusTypeTest(1, true);
    }

    private void runCustomerStatusTypeTest(int customerStatusTypeId, boolean shouldFind) {
        ReportStatusType rst = createReportStatusType();        
        UserStatusType ust = createUserStatusType();        
        UserAccount ua = createUserAccount(ust);        
        CustomerStatusType cst = createCustomerStatusType(customerStatusTypeId);        
        Customer c = createCustomer(cst);        
        createReportDefinition(rst, ua, c);               
        ReportDefinition reportDefinition = findReportDefinition();        
        if (shouldFind) {
            assertTrue(reportDefinition != null);            
            entityDao.delete(reportDefinition);
        } else {
            assertTrue(reportDefinition == null);            
        }
    }

    private ReportDefinition findReportDefinition() {
        ReportDefinition reportDefinition = (ReportDefinition) entityDao
                .findUniqueByNamedQueryAndNamedParamWithLock("ReportDefinition.findReadyReports", "currentDate", new Date(), "rd");
        return reportDefinition;
    }

    private void createReportDefinition(ReportStatusType rst, UserAccount ua, Customer c) {
        ReportDefinition r = new ReportDefinition();
        r.setReportStatusType(rst);
        r.setUserAccount(ua);
        r.setScheduledToRunGmt(new Date(System.currentTimeMillis() - 3600 * 1000));
        r.setCustomer(c);
        entityDao.save(r);
    }

    private Customer createCustomer(CustomerStatusType cst) {
        Customer c = new Customer();
        c.setCustomerStatusType(cst);
        entityDao.save(c);
        return c;
    }

    private CustomerStatusType createCustomerStatusType(int id) {
        CustomerStatusType cst = new CustomerStatusType();
        cst.setId(id);
        return cst;
    }

    private UserAccount createUserAccount(UserStatusType ust) {
        UserAccount ua = new UserAccount();
        ua.setUserStatusType(ust);
        entityDao.save(ua);
        return ua;
    }

    private UserStatusType createUserStatusType() {
        UserStatusType ust = new UserStatusType();
        ust.setId(1);
        return ust;
    }

    private ReportStatusType createReportStatusType() {
        ReportStatusType rst = new ReportStatusType();
        rst.setId(1);
        return rst;
    }    
}
