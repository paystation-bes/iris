package com.digitalpaytech.service.impl;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

//import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerPropertyType;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PaystationType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosBalance;
import com.digitalpaytech.domain.PosHeartbeat;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.WidgetChartType;
import com.digitalpaytech.domain.WidgetFilterType;
import com.digitalpaytech.domain.WidgetMetricType;
import com.digitalpaytech.domain.WidgetRangeType;
import com.digitalpaytech.domain.WidgetSubsetMember;
import com.digitalpaytech.domain.WidgetTierType;
import com.digitalpaytech.dto.MapEntry;
import com.digitalpaytech.dto.RunningTotalInfo;
import com.digitalpaytech.dto.WidgetMapInfo;
import com.digitalpaytech.dto.customeradmin.CustomerActivityLog;
//import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
//import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WidgetConstants;
import com.digitalpaytech.util.support.WebObjectId;
import com.digitalpaytech.util.support.WebObjectIdConverter;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;

// Commented out due to un-intuitive test-case !
public class WebWidgetMapServiceImplTest {
// 
//  private WebWidgetMapServiceImpl impl;
//  private UserAccount userAccount;
//  private Widget widget;
//  private XStream flatXstream;
//  private MockHttpServletRequest request;
//  private MockHttpSession session;
//  
//  @Before
//  public void setUp() {       
//      TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
//      
//      this.request = new MockHttpServletRequest();
//      session = new MockHttpSession();
//      this.request.setSession(session);
//      RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
//      
//      userAccount = new UserAccount();
//      Customer customer = new Customer();
//      customer.setId(3);
//      Customer parentCustomer = new Customer();       
//      parentCustomer.setId(2);        
//      customer.setParentCustomer(parentCustomer);
//      userAccount.setCustomer(customer);
//      
//      impl = new WebWidgetMapServiceImpl();
//      widget = new Widget();
//      WidgetTierType type1 = new WidgetTierType();
//      type1.setId(0);
//      WidgetTierType type2 = new WidgetTierType();
//      type2.setId(0);
//      WidgetTierType type3 = new WidgetTierType();
//      type3.setId(0);
//      WidgetRangeType rangeType = new WidgetRangeType();
//      WidgetMetricType widgetMetricType = new WidgetMetricType();
//      WidgetFilterType widgetFilterType = new WidgetFilterType();
//      WidgetChartType widgetChartType = new WidgetChartType();
//      
//      widget.setWidgetTierTypeByWidgetTier1Type(type1);
//      widget.setWidgetTierTypeByWidgetTier2Type(type2);
//      widget.setWidgetTierTypeByWidgetTier3Type(type3);
//      widget.setWidgetRangeType(rangeType);
//      widget.setWidgetMetricType(widgetMetricType);
//      widget.setWidgetFilterType(widgetFilterType);
//      widget.setWidgetChartType(widgetChartType);
//      widget.setName("test widget");
//      widget.setId(12345);
//      
//      flatXstream = new XStream(new JettisonMappedXmlDriver());
//      flatXstream.registerConverter(new WebObjectIdConverter());
//        flatXstream.processAnnotations(WidgetMapInfo.class);
//        
//        impl.setPointOfSaleService(new PointOfSaleServiceImpl(){
//            public PaystationType findPayStationTypeByPointOfSaleId(int pointOfSaleId){
//                PaystationType payStationType = new PaystationType();
//                payStationType.setId(2);
//                return payStationType;
//            }
//            public List<PointOfSale> findPlacedPointOfSaleByCustomerId(Integer customerId){              
//                return createPointOfSaleList();
//            }
//
//            public List<PointOfSale> findPlacedPointOfSaleByCustomerIdList(List<Integer> customerIds){              
//                return createPointOfSaleList();
//            }
//
//            public List<PointOfSale> findPlacedPointOfSaleByLocationIdList(List<Integer> locationIds){              
//                return createPointOfSaleList();
//            }
//
//            public List<PointOfSale> findPlacedPointOfSaleByRouteIdList(List<Integer> routeIds){              
//                return createPointOfSaleList();
//            }
//            
//            public PosHeartbeat findPosHeartbeatByPointOfSaleId(int pointOfSaleId){
//                PosHeartbeat heartbeat = new PosHeartbeat();
//                PointOfSale pos = new PointOfSale();
//                pos.setId(1);
//                heartbeat.setPointOfSale(pos);
//                heartbeat.setLastHeartbeatGmt(new Date());
//                return heartbeat;
//            }   
//            public EventSeverityType findGreatestActiveSeverityTypeByPointOfSaleId(int pointOfSaleId) {
//                EventSeverityType eventSeverityType = new EventSeverityType();
//                eventSeverityType.setId(0);
//                return eventSeverityType;
//            }
//            public PosServiceState findPosServiceStateByPointOfSaleId(int pointOfSaleId){
//                PosServiceState posServiceState = new PosServiceState();
//                posServiceState.setBattery1voltage((float) 0);
//                posServiceState.setPrinterPaperLevel((byte)0);
//                return posServiceState;
//            }
//            public PosBalance findPOSBalanceByPointOfSaleId(int pointOfSaleId){
//                PosBalance pointOfSaleBalance = new PosBalance();
//                pointOfSaleBalance.setBillAmount(1);
//                pointOfSaleBalance.setBillCount((short) 1);
//                pointOfSaleBalance.setCoinAmount(1);
//                pointOfSaleBalance.setCoinCount((short) 1);
//                pointOfSaleBalance.setUnsettledCreditCardAmount(1);
//                pointOfSaleBalance.setUnsettledCreditCardCount((short) 1);
//                pointOfSaleBalance.setTotalAmount(1);
//                return pointOfSaleBalance;
//            }
//            public Date findLastCollectionDateByPointOfSaleId(int pointOfSaleId){
//                return new Date();
//            }
//          @Override
//          @Transactional(propagation = Propagation.SUPPORTS)
//          public int countUnPlacedByCustomerIds(
//                  Collection<Integer> customerIds) {
//              return 0;
//          }
//          @Override
//          @Transactional(propagation = Propagation.SUPPORTS)
//          public int countUnPlacedByLocationIds(
//                  Collection<Integer> locationIds) {
//              return 0;
//          }
//          @Override
//          @Transactional(propagation = Propagation.SUPPORTS)
//          public int countUnPlacedByRouteIds(Collection<Integer> routeIds) {
//              return 0;
//          }
//          @Override
//          @Transactional(propagation = Propagation.SUPPORTS)
//          public List<MapEntry> findMapEntriesByCustomerIds(
//                  Collection<Integer> customerIds) {
//              return createMapEntryList();
//          }
//          @Override
//          @Transactional(propagation = Propagation.SUPPORTS)
//          public List<MapEntry> findMapEntriesByLocationIds(
//                  Collection<Integer> locationIds) {
//              return createMapEntryList();
//          }
//          @Override
//          @Transactional(propagation = Propagation.SUPPORTS)
//          public List<MapEntry> findMapEntriesByRouteIds(
//                  Collection<Integer> routeIds) {
//              return createMapEntryList();
//          }
//          @Override
//          @Transactional(propagation = Propagation.SUPPORTS)
//          public Map<Integer, MapEntry> findPointOfSaleMapEntryDetails(
//                  Collection<Integer> pointOfSaleIds, TimeZone timeZone) {
//              return new HashMap<Integer, MapEntry>(3);
//          }
//        });
//        impl.setCustomerAdminService(new CustomerAdminServiceImpl(){
//            public List<CustomerProperty> getCustomerPropertyByCustomerId(int customerId){
//                CustomerProperty property = new CustomerProperty();
//                CustomerPropertyType propertyType = new CustomerPropertyType();
//                propertyType.setId(WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
//                property.setCustomerPropertyType(propertyType);
//                property.setPropertyValue("GMT");
//                List<CustomerProperty> list = new ArrayList<CustomerProperty>();
//                list.add(property);
//                return list;            
//            }
//        });
//
//        
//        
//  }
//  
//  @Test
//  public void test_locationMapWidget() {
//      
//      widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//      widget.setIsSubsetTier1(false);
//              
//      String data = convertToJson(impl.getWidgetData(widget, userAccount).getWidgetMapInfo(), "widgetData", true);
//      assertTrue(data.startsWith("{\"widgetData\":{\"mapEntries\":[{\"payStationType\":2,\"severity\":0,\"name\":\"POS 1\",\"randomId\":\""));
//      assertTrue(data.contains(",\"latitude\":101,\"longitude\":-101,\"locationName\":\"loc1\",\"lastSeen\":\"just now\",\"batteryVoltage\":\"0.0 V\",\"paperStatus\":0,\"lastCollection\":"));
//      assertTrue(data.contains(",\"runningTotal\":{\"coinAmount\":0.01,\"billAmount\":0.01,\"cardAmount\":0.01}},{\"payStationType\":2,\"severity\":0,\"name\":\"POS 2\",\"randomId\":\""));
//      assertTrue(data.contains(",\"latitude\":102,\"longitude\":-102,\"locationName\":\"loc1\",\"lastSeen\":\"just now\",\"batteryVoltage\":\"0.0 V\",\"paperStatus\":0,\"lastCollection\":"));
//      assertTrue(data.endsWith(",\"runningTotal\":{\"coinAmount\":0.01,\"billAmount\":0.01,\"cardAmount\":0.01}}],\"missingPayStation\":false}}"));
//  }                               
//
//  @Test
//  public void test_subsetLocationMapWidget() {
//
//      Set<WidgetSubsetMember> susbetMembers = new HashSet<WidgetSubsetMember>();
//      WidgetSubsetMember susbetMember = new WidgetSubsetMember();
//      WidgetTierType widgetTierType = new WidgetTierType();
//      widgetTierType.setId(WidgetConstants.TIER_TYPE_LOCATION);
//      susbetMember.setWidgetTierType(widgetTierType);
//      susbetMember.setMemberId(1);
//      susbetMembers.add(susbetMember);
//      
//      widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//      widget.setIsSubsetTier1(true);
//      widget.setWidgetSubsetMembers(susbetMembers);
//      
//      String data = convertToJson(impl.getWidgetData(widget, userAccount).getWidgetMapInfo(), "widgetData", true);
//      
//      assertTrue(data.startsWith("{\"widgetData\":{\"mapEntries\":[{\"payStationType\":2,\"severity\":0,\"name\":\"POS 1\",\"randomId\":\""));
//      assertTrue(data.contains(",\"latitude\":101,\"longitude\":-101,\"locationName\":\"loc1\",\"lastSeen\":\"just now\",\"batteryVoltage\":\"0.0 V\",\"paperStatus\":0,\"lastCollection\":"));
//      assertTrue(data.contains(",\"runningTotal\":{\"coinAmount\":0.01,\"billAmount\":0.01,\"cardAmount\":0.01}},{\"payStationType\":2,\"severity\":0,\"name\":\"POS 2\",\"randomId\":\""));
//      assertTrue(data.contains(",\"latitude\":102,\"longitude\":-102,\"locationName\":\"loc1\",\"lastSeen\":\"just now\",\"batteryVoltage\":\"0.0 V\",\"paperStatus\":0,\"lastCollection\":"));
//      assertTrue(data.endsWith(",\"runningTotal\":{\"coinAmount\":0.01,\"billAmount\":0.01,\"cardAmount\":0.01}}],\"missingPayStation\":false}}"));
//  }   
//
//  @Test
//  public void test_organizationMapWidget() {
//      
//      impl.setDashboardService(new DashboardServiceImpl(){
//          public List<Customer> findAllChildCustomers(int parentCustomerId){
//              List<Customer> customers = new ArrayList<Customer>();
//              Customer customer = new Customer();
//              customer.setId(3);
//              return customers;
//          }
//      });
//      
//      widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//      widget.setIsSubsetTier1(false);
//      
//      String data = convertToJson(impl.getWidgetData(widget, userAccount).getWidgetMapInfo(), "widgetData", true);
//      
//      assertTrue(data.startsWith("{\"widgetData\":{\"mapEntries\":[{\"payStationType\":2,\"severity\":0,\"name\":\"POS 1\",\"randomId\":\""));
//      assertTrue(data.contains(",\"latitude\":101,\"longitude\":-101,\"locationName\":\"loc1\",\"lastSeen\":\"just now\",\"batteryVoltage\":\"0.0 V\",\"paperStatus\":0,\"lastCollection\":"));       
//      assertTrue(data.contains(",\"runningTotal\":{\"coinAmount\":0.01,\"billAmount\":0.01,\"cardAmount\":0.01}},{\"payStationType\":2,\"severity\":0,\"name\":\"POS 2\",\"randomId\":"));
//      assertTrue(data.contains(",\"latitude\":102,\"longitude\":-102,\"locationName\":\"loc1\",\"lastSeen\":\"just now\",\"batteryVoltage\":\"0.0 V\",\"paperStatus\":0,\"lastCollection\":"));
//      assertTrue(data.endsWith(",\"runningTotal\":{\"coinAmount\":0.01,\"billAmount\":0.01,\"cardAmount\":0.01}}],\"missingPayStation\":false}}"));
//  }   
//  
//  @Test
//  public void test_subsetOrganizationMapWidget() {
//
//      Set<WidgetSubsetMember> susbetMembers = new HashSet<WidgetSubsetMember>();
//      WidgetSubsetMember susbetMember = new WidgetSubsetMember();
//      WidgetTierType widgetTierType = new WidgetTierType();
//      widgetTierType.setId(WidgetConstants.TIER_TYPE_ORG);
//      susbetMember.setWidgetTierType(widgetTierType);
//      susbetMember.setMemberId(1);
//      susbetMembers.add(susbetMember);
//      
//      widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_ORG);
//      widget.setIsSubsetTier1(true);
//      widget.setWidgetSubsetMembers(susbetMembers);
//      
//      String data = convertToJson(impl.getWidgetData(widget, userAccount).getWidgetMapInfo(), "widgetData", true);
//      
//      assertTrue(data.startsWith("{\"widgetData\":{\"mapEntries\":[{\"payStationType\":2,\"severity\":0,\"name\":\"POS 1\",\"randomId\":\""));
//      assertTrue(data.contains(",\"latitude\":101,\"longitude\":-101,\"locationName\":\"loc1\",\"lastSeen\":\"just now\",\"batteryVoltage\":\"0.0 V\",\"paperStatus\":0,\"lastCollection\":"));
//      assertTrue(data.contains(",\"runningTotal\":{\"coinAmount\":0.01,\"billAmount\":0.01,\"cardAmount\":0.01}},{\"payStationType\":2,\"severity\":0,\"name\":\"POS 2\",\"randomId\":\""));
//      assertTrue(data.contains(",\"latitude\":102,\"longitude\":-102,\"locationName\":\"loc1\",\"lastSeen\":\"just now\",\"batteryVoltage\":\"0.0 V\",\"paperStatus\":0,\"lastCollection\":"));
//      assertTrue(data.endsWith(",\"runningTotal\":{\"coinAmount\":0.01,\"billAmount\":0.01,\"cardAmount\":0.01}}],\"missingPayStation\":false}}"));
//  }   
//  
//  @Test
//  public void test_routeMapWidget() {
//      
//      widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//      widget.setIsSubsetTier1(false);
//      
//      String data = convertToJson(impl.getWidgetData(widget, userAccount).getWidgetMapInfo(), "widgetData", true);
//      
//      assertTrue(data.startsWith("{\"widgetData\":{\"mapEntries\":[{\"payStationType\":2,\"severity\":0,\"name\":\"POS 1\",\"randomId\":\""));
//      assertTrue(data.contains(",\"latitude\":101,\"longitude\":-101,\"locationName\":\"loc1\",\"lastSeen\":\"just now\",\"batteryVoltage\":\"0.0 V\",\"paperStatus\":0,\"lastCollection\":"));
//      assertTrue(data.contains(",\"runningTotal\":{\"coinAmount\":0.01,\"billAmount\":0.01,\"cardAmount\":0.01}},{\"payStationType\":2,\"severity\":0,\"name\":\"POS 2\",\"randomId\":\""));
//      assertTrue(data.contains(",\"latitude\":102,\"longitude\":-102,\"locationName\":\"loc1\",\"lastSeen\":\"just now\",\"batteryVoltage\":\"0.0 V\",\"paperStatus\":0,\"lastCollection\":"));
//      assertTrue(data.endsWith(",\"runningTotal\":{\"coinAmount\":0.01,\"billAmount\":0.01,\"cardAmount\":0.01}}],\"missingPayStation\":false}}"));
//  }   
//  
//  @Test
//  public void test_subsetRouteMapWidget() {
//
//      Set<WidgetSubsetMember> susbetMembers = new HashSet<WidgetSubsetMember>();
//      WidgetSubsetMember susbetMember = new WidgetSubsetMember();
//      WidgetTierType widgetTierType = new WidgetTierType();
//      widgetTierType.setId(WidgetConstants.TIER_TYPE_ROUTE);
//      susbetMember.setWidgetTierType(widgetTierType);
//      susbetMember.setMemberId(1);
//      susbetMembers.add(susbetMember);
//      
//      widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//      widget.setIsSubsetTier1(true);
//      widget.setWidgetSubsetMembers(susbetMembers);
//      
//      String data = convertToJson(impl.getWidgetData(widget, userAccount).getWidgetMapInfo(), "widgetData", true);
//      
//      assertTrue(data.startsWith("{\"widgetData\":{\"mapEntries\":[{\"payStationType\":2,\"severity\":0,\"name\":\"POS 1\",\"randomId\":\""));
//      assertTrue(data.contains(",\"latitude\":101,\"longitude\":-101,\"locationName\":\"loc1\",\"lastSeen\":\"just now\",\"batteryVoltage\":\"0.0 V\",\"paperStatus\":0,\"lastCollection\":"));
//      assertTrue(data.contains(",\"runningTotal\":{\"coinAmount\":0.01,\"billAmount\":0.01,\"cardAmount\":0.01}},{\"payStationType\":2,\"severity\":0,\"name\":\"POS 2\",\"randomId\":\""));
//      assertTrue(data.contains(",\"latitude\":102,\"longitude\":-102,\"locationName\":\"loc1\",\"lastSeen\":\"just now\",\"batteryVoltage\":\"0.0 V\",\"paperStatus\":0,\"lastCollection\":"));
//      assertTrue(data.endsWith(",\"runningTotal\":{\"coinAmount\":0.01,\"billAmount\":0.01,\"cardAmount\":0.01}}],\"missingPayStation\":false}}"));
//  }   
//
//  private List<PointOfSale> createPointOfSaleList(){
//      List<PointOfSale> pointOfSales = new ArrayList<PointOfSale>();
//      
//      Paystation payStation = new Paystation();
//      PaystationType paystationType = new PaystationType();
//      paystationType.setId(WebCoreConstants.PAY_STATION_TYPE_SHELBY);
//      payStation.setPaystationType(paystationType);
//      
//      PointOfSale pos = new PointOfSale();
//      PointOfSale pos2 = new PointOfSale();
//      Location location = new Location();
//      location.setName("loc1");
//      
//      pos.setId(1);
//      pos.setName("POS 1");
//      pos.setLatitude(BigDecimal.valueOf(101));
//      pos.setLongitude(BigDecimal.valueOf(-101));
//      pos.setLocation(location);
//      pos.setPaystation(payStation);
//      pointOfSales.add(pos);
//
//      pos2.setId(2);
//      pos2.setName("POS 2");
//      pos2.setLatitude(BigDecimal.valueOf(102));
//      pos2.setLongitude(BigDecimal.valueOf(-102));
//      pos2.setLocation(location);
//        pos2.setPaystation(payStation);
//      pointOfSales.add(pos2);
//      
//      return pointOfSales;
//  }
//  
//  private List<MapEntry> createMapEntryList() {
//      List<MapEntry> result = new ArrayList<MapEntry>(2);
//      
//      Location location = new Location();
//      location.setName("loc1");
//      
//      MapEntry entry = new MapEntry();
//      entry.setRandomId(new WebObjectId(PointOfSale.class, 1));
//      entry.setName("POS 1");
//      entry.setLatitude(BigDecimal.valueOf(101));
//      entry.setLongitude(BigDecimal.valueOf(-101));
//      entry.setLocationName("loc1");
//      entry.setBatteryVoltage("0.0 V");
//      entry.setPaperStatus("0");
//      
//      RunningTotalInfo rti = new RunningTotalInfo();
//      rti.setBillAmount("1");
//      rti.setBillCount("1");
//      rti.setCardAmount("1");
//      rti.setCardCount("1");
//      rti.setCashAmount("1");
//      rti.setCoinAmount("1");
//      rti.setCoinCount("1");
//      entry.setRunningTotal(rti);
//      
//      result.add(entry);
//      
//      entry = new MapEntry();
//      entry.setRandomId(new WebObjectId(PointOfSale.class, 1));
//      entry.setName("POS 2");
//      entry.setLatitude(BigDecimal.valueOf(102));
//      entry.setLongitude(BigDecimal.valueOf(-102));
//      entry.setLocationName("loc1");
//      entry.setBatteryVoltage("0.0 V");
//      entry.setPaperStatus("0");
//      
//      rti = new RunningTotalInfo();
//      rti.setBillAmount("0.01");
//      rti.setBillCount("1");
//      rti.setCardAmount("0.01");
//      rti.setCardCount("1");
//      rti.setCashAmount("0.01");
//      rti.setCoinAmount("1");
//      rti.setCoinCount("0.01");
//      entry.setRunningTotal(rti);
//      
//      result.add(entry);
//      
//      return result;
//  }
//  
//  private String convertToJson(Object obj, String openTagName, boolean flatOut) {
//        XStream xstream;
//        xstream = flatXstream;        
//        xstream.setMode(XStream.NO_REFERENCES);
//        xstream.alias(openTagName, obj.getClass());             
//        String json = xstream.toXML(obj);        
//        return json;
//    }
}
