package com.digitalpaytech.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;

public class UserAccountServiceImplTest {
    
    private UserAccountServiceImpl serv;
    private UserAccount account;
    
    @Before
    public void setUp() {
        account = new UserAccount();
        account.setId(3);
        account.setUserName("momo uno @ park san diego . com");
        
        //-------------- 1st role, permission -----------------
        Role role1 = new Role();
        role1.setUserAccount(account);
        role1.setName("Child Admin");
        
        Permission pe1 = new Permission();
        pe1.setName("Reports Management");
        RolePermission rp1 = new RolePermission();
        rp1.setRole(role1);
        rp1.setPermission(pe1);
        
        Set<RolePermission> rpSet1 = new HashSet<RolePermission>();
        rpSet1.add(rp1);
        role1.setRolePermissions(rpSet1);
        
        UserRole ur1 = new UserRole();
        ur1.setRole(role1);
        ur1.setUserAccount(account);
        
        //-------------- 2nd role, permission -----------------     
        Role role2 = new Role();
        role2.setUserAccount(account);
        role2.setName("Basic");
        
        Permission pe2 = new Permission();
        pe2.setName("Taxes Reports");
        RolePermission rp2 = new RolePermission();
        rp2.setRole(role2);
        rp2.setPermission(pe2);
        
        Set<RolePermission> rpSet2 = new HashSet<RolePermission>();
        rpSet2.add(rp2);
        role2.setRolePermissions(rpSet2);
        
        UserRole ur2 = new UserRole();
        ur2.setRole(role2);
        ur2.setUserAccount(account);
        
        Set<UserRole> set = new HashSet<UserRole>();
        set.add(ur1);
        set.add(ur2);
        
        account.setUserRoles(set);
    }
    
    @Test
    public void findUserAccount() {
        
        EntityDao dao = EasyMock.createMock(EntityDao.class);
        EasyMock.expect(dao.get(UserAccount.class, 3)).andReturn(account);
        EasyMock.replay(dao);
        
        serv = new UserAccountServiceImpl();
        serv.setEntityDao(dao);
        UserAccount ua = serv.findUserAccount(3);
        
        assertNotNull(ua);
        assertNotNull(ua.getUserName());
        assertEquals("momo uno @ park san diego . com", ua.getUserName());
    }
    
    @Test
    public void findRoles() {
        EntityDao dao = EasyMock.createMock(EntityDao.class);
        EasyMock.expect(dao.get(UserAccount.class, 3)).andReturn(account);
        EasyMock.replay(dao);
        
        serv = new UserAccountServiceImpl();
        serv.setEntityDao(dao);
        List<Role> roles = serv.findRoles(3);
        assertNotNull(roles);
    }
    
    @Test
    public void findPermissions() {
        EntityDao dao = EasyMock.createMock(EntityDao.class);
        EasyMock.expect(dao.get(UserAccount.class, 3)).andReturn(account);
        EasyMock.replay(dao);
        
        serv = new UserAccountServiceImpl();
        serv.setEntityDao(dao);
        List<Permission> perms = serv.findPermissions(3);
        assertNotNull(perms);
    }
    
    @Test
    public void test_findUserAccount_ByUserName() {
        UserAccountServiceImpl service = new UserAccountServiceImpl();
        EntityDao dao = EasyMock.createMock(EntityDao.class);
        String userName = "testuser";
        List<UserAccount> lists = new ArrayList<UserAccount>();
        UserAccount account = new UserAccount();
        account.setId(1000);
        lists.add(account);
        EasyMock.expect(dao.findByNamedQueryAndNamedParam("UserAccount.findUndeletedUserAccountByUserName", "userName", userName, true)).andReturn(lists);
        EasyMock.replay(dao);
        
        service.setEntityDao(dao);
        UserAccount resultAccount = service.findUndeletedUserAccount(userName);
        
        assertNotNull(resultAccount);
        assertEquals(resultAccount.getId(), new Integer(1000));
    }
    
    @Test
    public void test_findUserAccount_ByUserName_empty() {
        UserAccountServiceImpl service = new UserAccountServiceImpl();
        EntityDao dao = EasyMock.createMock(EntityDao.class);
        String userName = "testuser";
        List<UserAccount> lists = new ArrayList<UserAccount>();
        EasyMock.expect(dao.findByNamedQueryAndNamedParam("UserAccount.findUndeletedUserAccountByUserName", "userName", userName, true)).andReturn(lists);
        EasyMock.replay(dao);
        
        service.setEntityDao(dao);
        UserAccount resultAccount = service.findUndeletedUserAccount(userName);
        
        assertNull(resultAccount);
    }
}
