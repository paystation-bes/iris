package com.digitalpaytech.service.impl;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.WidgetChartType;
import com.digitalpaytech.domain.WidgetFilterType;
import com.digitalpaytech.domain.WidgetMetricType;
import com.digitalpaytech.domain.WidgetRangeType;
import com.digitalpaytech.domain.WidgetTierType;
import com.digitalpaytech.service.WebWidgetHelperService;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WidgetConstants;

public class WebWidgetUtilizationServiceImplTest {
    
//    private Widget widget;
//    private WebWidgetUtilizationServiceImpl impl;
//    private UserAccount userAccount;
//    
//    @Autowired
//    protected WebWidgetHelperService webWidgetHelperService;
//    
//    protected void setWebWidgetHelperService(WebWidgetHelperService webWidgetHelperService) {
//        this.webWidgetHelperService = webWidgetHelperService;
//    }
//    
//    @Before
//    public void setUp() {
//        
//        userAccount = new UserAccount();
//        Customer customer = new Customer();
//        Customer parentCustomer = new Customer();
//        
//        parentCustomer.setId(2);
//        
//        customer.setParentCustomer(parentCustomer);
//        customer.setId(2);
//        userAccount.setCustomer(customer);
//        
//        impl = new WebWidgetUtilizationServiceImpl();
//        
//        impl.setWebWidgetHelperService(new WebWidgetHelperServiceImpl() {
//            public String getCustomerTimeZoneByCustomerId(int customerId) {
//                String timeZone = WebCoreConstants.GMT;
//                return timeZone;
//            }
//        });
//        
//        widget = new Widget();
//        WidgetTierType type1 = new WidgetTierType();
//        type1.setId(0);
//        WidgetTierType type2 = new WidgetTierType();
//        type2.setId(0);
//        WidgetTierType type3 = new WidgetTierType();
//        type3.setId(0);
//        WidgetRangeType rangeType = new WidgetRangeType();
//        WidgetMetricType widgetMetricType = new WidgetMetricType();
//        WidgetFilterType widgetFilterType = new WidgetFilterType();
//        WidgetChartType widgetChartType = new WidgetChartType();
//        
//        widget.setWidgetTierTypeByWidgetTier1Type(type1);
//        widget.setWidgetTierTypeByWidgetTier2Type(type2);
//        widget.setWidgetTierTypeByWidgetTier3Type(type3);
//        widget.setWidgetRangeType(rangeType);
//        widget.setWidgetMetricType(widgetMetricType);
//        widget.setWidgetFilterType(widgetFilterType);
//        widget.setWidgetChartType(widgetChartType);
//        widget.setName("test widget");
//    }
//    
//    @After
//    public void cleanUp() {
//        impl = null;
//        widget = null;
//    }
//    
//    @Test
//    public void test_appendColumns_tier1_day() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_DAY);
//        String result = impl.appendColumns(widget);
//        assertEquals(result,
//                     "SELECT IFNULL(u.CustomerId, 0) AS CustomerId , t.DayOfYear AS Tier1TypeId, t.Date AS Tier1TypeName, t.Date AS StartTime , SUM(u.TotalMinutesPurchased) / SUM(IF(u.MaxUtilizationMinutes = 0, 100, u.MaxUtilizationMinutes)) * 100 AS WidgetMetricValue ");
//    }
//
//    @Test
//    public void test_appendColumns_tier1_month() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_MONTH);
//        String result = impl.appendColumns(widget);
//        assertEquals(result,
//                     "SELECT IFNULL(u.CustomerId, 0) AS CustomerId , t.Month AS Tier1TypeId, CONCAT(t.MonthNameAbbrev,' ', t.Year) AS Tier1TypeName , SUM(u.TotalMinutesPurchased) / SUM(IF(u.MaxUtilizationMinutes = 0, 100, u.MaxUtilizationMinutes)) * 100 AS WidgetMetricValue ");
//    }
//
//    @Test
//    public void test_appendColumns_tier1_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendColumns(widget);
//        assertEquals(result,
//                     "SELECT IFNULL(u.CustomerId, 0) AS CustomerId , l.Id AS Tier1TypeId, l.Name AS Tier1TypeName , SUM(u.TotalMinutesPurchased) / SUM(IF(u.MaxUtilizationMinutes = 0, 100, u.MaxUtilizationMinutes)) * 100 AS WidgetMetricValue ");
//    }
//    
//    @Test
//    public void test_appendColumns_tier1_parent_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_PARENT_LOCATION);
//        String result = impl.appendColumns(widget);
//        assertEquals(result,
//                     "SELECT IFNULL(u.CustomerId, 0) AS CustomerId , l.ParentLocationId AS Tier1TypeId, (SELECT l3.Name FROM Location l3 WHERE l.ParentLocationId = l3.Id) AS Tier1TypeName , SUM(u.TotalMinutesPurchased) / SUM(IF(u.MaxUtilizationMinutes = 0, 100, u.MaxUtilizationMinutes)) * 100 AS WidgetMetricValue ");
//    }
//    
//    @Test
//    public void test_appendColumns_tier1_day_tier2_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_DAY);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendColumns(widget);
//        assertEquals(result,
//                     "SELECT IFNULL(u.CustomerId, 0) AS CustomerId , t.DayOfYear AS Tier1TypeId, t.Date AS Tier1TypeName, t.Date AS StartTime , l.Id AS Tier2TypeId, l.Name AS Tier2TypeName , SUM(u.TotalMinutesPurchased) / SUM(IF(u.MaxUtilizationMinutes = 0, 100, u.MaxUtilizationMinutes)) * 100 AS WidgetMetricValue ");
//    }
//
//    @Test
//    public void test_appendColumns_tier1_month_tier2_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_MONTH);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendColumns(widget);
//        assertEquals(result,
//                     "SELECT IFNULL(u.CustomerId, 0) AS CustomerId , t.Month AS Tier1TypeId, CONCAT(t.MonthNameAbbrev,' ', t.Year) AS Tier1TypeName , l.Id AS Tier2TypeId, l.Name AS Tier2TypeName , SUM(u.TotalMinutesPurchased) / SUM(IF(u.MaxUtilizationMinutes = 0, 100, u.MaxUtilizationMinutes)) * 100 AS WidgetMetricValue ");
//    }
//    
//
//    @Test
//    public void test_appendTables() {
//        String result = impl.appendTables(widget, userAccount, WidgetConstants.TABLE_RANGE_TYPE_MONTH);
//        assertEquals(result,
//                     "FROM UtilizationMonth u INNER JOIN Time t ON u.TimeIdLocal = t.Id INNER JOIN Location l ON l.Id = u.LocationId ");
//    }
//    
//    @Test
//    public void test_appendTables_tier1_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendTables(widget, userAccount, WidgetConstants.TABLE_RANGE_TYPE_MONTH);
//        assertEquals(result,
//                     "FROM UtilizationMonth u INNER JOIN Time t ON u.TimeIdLocal = t.Id INNER JOIN Location l ON l.Id = u.LocationId ");
//    }
//    
//    @Test
//    public void test_appendTables_tier1_month_tier2_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_MONTH);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendTables(widget, userAccount, WidgetConstants.TABLE_RANGE_TYPE_MONTH);
//        assertEquals(result,
//                     "FROM UtilizationMonth u INNER JOIN Time t ON u.TimeIdLocal = t.Id INNER JOIN Location l ON l.Id = u.LocationId ");
//    }
//    
//    @Test
//    public void test_appendTables_tier1_day_tier2_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_DAY);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendTables(widget, userAccount, WidgetConstants.TABLE_RANGE_TYPE_DAY);
//        assertEquals(result,
//                     "FROM UtilizationDay u INNER JOIN Time t ON u.TimeIdLocal = t.Id INNER JOIN Location l ON l.Id = u.LocationId ");
//    }
//    
//    @Test
//    public void test_appendWhere_customerId_null() {
//        userAccount.getCustomer().setIsParent(true);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result, "WHERE u.CustomerId IN (:customerId) AND l.IsUnassigned = 0 AND l.IsParent = 0 ");
//    }
//    
//    @Test
//    public void test_appendWhere_customerId_not_null() {
//        widget.setCustomerId(2);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result, "WHERE u.CustomerId = :customerId AND l.IsUnassigned = 0 AND l.IsParent = 0 ");
//    }
//    
//    @Test
//    public void test_appendWhere_tier1_type_location() {
//        widget.setCustomerId(2);
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result, "WHERE u.CustomerId = :customerId AND l.IsUnassigned = 0 AND l.IsParent = 0 AND l.IsDeleted = 0 ");
//    }
//    
//    @Test
//    public void test_appendWhere_widget_rangeType_1() {
//        widget.setCustomerId(2);
//        widget.getWidgetRangeType().setId(WidgetConstants.RANGE_TYPE_TODAY);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result,
//                     "WHERE u.CustomerId = :customerId AND l.IsUnassigned = 0 AND l.IsParent = 0 AND t.Date = DATE(:currentLocalTime) AND t.DateTime <= :currentLocalTime ");
//    }
//    
//    @Test
//    public void test_appendWhere_widget_rangeType_2() {
//        widget.setCustomerId(2);
//        widget.getWidgetRangeType().setId(WidgetConstants.RANGE_TYPE_YESTERDAY);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result,
//                     "WHERE u.CustomerId = :customerId AND l.IsUnassigned = 0 AND l.IsParent = 0 AND t.Date = DATE(DATE_SUB(:currentLocalTime, INTERVAL 1 DAY)) ");
//    }
//    
//    @Test
//    public void test_appendWhere_widget_rangeType_3() {
//        widget.setCustomerId(2);
//        widget.getWidgetRangeType().setId(WidgetConstants.RANGE_TYPE_LAST_12MONTHS);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result,
//                     "WHERE u.CustomerId = :customerId AND l.IsUnassigned = 0 AND l.IsParent = 0 AND t.Id >= (SELECT MIN(t3.Id) FROM `Time` t3 WHERE t3.Year = YEAR(DATE_SUB(:currentLocalTime, INTERVAL 12 MONTH)) AND t3.Month = MONTH(DATE_SUB(:currentLocalTime, INTERVAL 12 MONTH)) AND t3.QuarterOfDay = 0 AND t3.DayOfMonth = 1) AND t.Id <  (SELECT MIN(t4.Id) FROM `Time` t4 WHERE t4.Year = YEAR(:currentLocalTime) AND t4.Month = MONTH(:currentLocalTime) AND t4.DateTime <= :currentLocalTime AND t4.QuarterOfDay = 0 AND t4.DayOfMonth = 1) AND t.QuarterOfDay = 0 AND t.DayOfMonth = 1 ");
//        //"WHERE (l.CustomerId = :customerId OR l.CustomerId IS NULL) AND lo.IsOpen = 1 AND lo.QuarterHourId = t.QuarterOfDay AND l.IsUnassigned = 0 AND l.IsParent = 0 AND t.DateTime >= DATE_SUB(:currentLocalTime, INTERVAL 24 HOUR) AND t.DateTime < :currentLocalTime "
//    }
//    
//    @Test
//    public void test_appendWhere_widget_rangeType_4() {
//        widget.setCustomerId(2);
//        widget.getWidgetRangeType().setId(WidgetConstants.RANGE_TYPE_LAST_7DAYS);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result,
//                     "WHERE u.CustomerId = :customerId AND l.IsUnassigned = 0 AND l.IsParent = 0 AND t.Id >= (SELECT t3.Id FROM `Time` t3 WHERE t3.Date = DATE(DATE_SUB(:currentLocalTime, INTERVAL 7 DAY)) AND t3.QuarterOfDay = 0) AND t.Id <  (SELECT t4.Id FROM `Time` t4 WHERE t4.Date = DATE(:currentLocalTime) AND t4.QuarterOfDay = 0) AND t.QuarterOfDay = 0 ");
//        //"WHERE (l.CustomerId = :customerId OR l.CustomerId IS NULL) AND lo.IsOpen = 1 AND lo.QuarterHourId = t.QuarterOfDay AND l.IsUnassigned = 0 AND l.IsParent = 0 AND t.Date >= DATE(DATE_SUB(:currentLocalTime, INTERVAL 7 DAY)) AND t.Date < DATE(:currentLocalTime) "
//    }
//    
//    @Test
//    public void test_appendWhere_widget_rangeType_5() {
//        widget.setCustomerId(2);
//        widget.getWidgetRangeType().setId(WidgetConstants.RANGE_TYPE_LAST_30DAYS);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result,
//                     "WHERE u.CustomerId = :customerId AND l.IsUnassigned = 0 AND l.IsParent = 0 AND t.Id >= (SELECT t3.Id FROM `Time` t3 WHERE t3.Date = DATE(DATE_SUB(:currentLocalTime, INTERVAL 30 DAY)) AND t3.QuarterOfDay = 0) AND t.Id <  (SELECT t4.Id FROM `Time` t4 WHERE t4.Date = DATE(:currentLocalTime) AND t4.QuarterOfDay = 0) AND t.QuarterOfDay = 0 ");
//        //"WHERE (l.CustomerId = :customerId OR l.CustomerId IS NULL) AND lo.IsOpen = 1 AND lo.QuarterHourId = t.QuarterOfDay AND l.IsUnassigned = 0 AND l.IsParent = 0 AND t.Date >= DATE(DATE_SUB(:currentLocalTime, INTERVAL 30 DAY)) AND t.Date < DATE(:currentLocalTime) "
//    }
//    
//    @Test
//    public void test_appendWhere_widget_rangeType_7() {
//        widget.setCustomerId(2);
//        widget.getWidgetRangeType().setId(WidgetConstants.RANGE_TYPE_THIS_WEEK);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result,
//                     "WHERE u.CustomerId = :customerId AND l.IsUnassigned = 0 AND l.IsParent = 0 AND t.Date >= DATE(DATE_SUB(:currentLocalTime, INTERVAL 1 WEEK)) AND t.WeekOfYear = WEEK(:currentLocalTime,2) AND t.DateTime <= :currentLocalTime AND t.QuarterOfDay = 0 ");
//    }
//    
//    @Test
//    public void test_appendWhere_widget_rangeType_8() {
//        widget.setCustomerId(2);
//        widget.getWidgetRangeType().setId(WidgetConstants.RANGE_TYPE_THIS_MONTH);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result,
//                     "WHERE u.CustomerId = :customerId AND l.IsUnassigned = 0 AND l.IsParent = 0 AND t.Year = YEAR(:currentLocalTime) AND t.Month = MONTH(:currentLocalTime) AND t.DateTime <= :currentLocalTime AND t.QuarterOfDay = 0 AND t.DayOfMonth = 1 ");
//    }
//    
//    @Test
//    public void test_appendWhere_widget_rangeType_10() {
//        widget.setCustomerId(2);
//        widget.getWidgetRangeType().setId(WidgetConstants.RANGE_TYPE_LAST_WEEK);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result,
//                     "WHERE u.CustomerId = :customerId AND l.IsUnassigned = 0 AND l.IsParent = 0 AND t.Date >= DATE(DATE_SUB(:currentLocalTime, INTERVAL 2 WEEK)) AND t.WeekOfYear = WEEK(DATE_SUB(:currentLocalTime, INTERVAL 1 WEEK), 2) AND t.QuarterOfDay = 0 ");
//    }
//    
//    @Test
//    public void test_appendWhere_widget_rangeType_11() {
//        widget.setCustomerId(2);
//        widget.getWidgetRangeType().setId(WidgetConstants.RANGE_TYPE_LAST_MONTH);
//        String result = impl.appendWhere(widget, userAccount);
//        assertEquals(result,
//                     "WHERE u.CustomerId = :customerId AND l.IsUnassigned = 0 AND l.IsParent = 0 AND t.Year = YEAR(DATE_SUB(:currentLocalTime, INTERVAL 1 MONTH)) AND t.Month = MONTH(DATE_SUB(:currentLocalTime, INTERVAL 1 MONTH)) AND t.QuarterOfDay = 0 AND t.DayOfMonth = 1 ");
//    }
//    
//    @Test
//    public void test_appendGroupBy() {
//        String result = impl.appendGroupBy(widget);
//        assertEquals(result, " GROUP BY u.CustomerId");
//    }
//    
//    @Test
//    public void test_appendGroupBy_tier1_2() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_MONTH);
//        String result = impl.appendGroupBy(widget);
//        assertEquals(result, " GROUP BY u.CustomerId, t.Year, t.Month");
//    }
//    
//    @Test
//    public void test_appendGroupBy_tier1_3() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_DAY);
//        String result = impl.appendGroupBy(widget);
//        assertEquals(result, " GROUP BY u.CustomerId, t.Date");
//    }
//    
//    @Test
//    public void test_appendGroupBy_tier1_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendGroupBy(widget);
//        assertEquals(result, " GROUP BY u.CustomerId, l.Id");
//    }
//    
//    @Test
//    public void test_appendGroupBy_tier1_2_tier2_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_MONTH);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendGroupBy(widget);
//        assertEquals(result, " GROUP BY u.CustomerId, t.Year, t.Month, l.Id");
//    }
//    
//    @Test
//    public void test_appendGroupBy_tier1_3_tier2_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_DAY);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendGroupBy(widget);
//        assertEquals(result, " GROUP BY u.CustomerId, t.Date, l.Id");
//    }
//    
//    @Test
//    public void test_appendOrderBy() {
//        String result = impl.appendOrderBy(widget);
//        assertEquals(result, " ORDER BY u.CustomerId");
//    }
//    
//    @Test
//    public void test_appendOrderBy_tier1_2() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_MONTH);
//        String result = impl.appendOrderBy(widget);
//        assertEquals(result, " ORDER BY u.CustomerId, t.Year, t.Month");
//    }
//    
//    @Test
//    public void test_appendOrderBy_tier1_3() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_DAY);
//        String result = impl.appendOrderBy(widget);
//        assertEquals(result, " ORDER BY u.CustomerId, t.Date");
//    }
//    
//    @Test
//    public void test_appendOrderBy_tier1_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendOrderBy(widget);
//        assertEquals(result, " ORDER BY u.CustomerId, l.Name");
//    }
//    
//    @Test
//    public void test_appendOrderBy_tier1_2_tier2_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_MONTH);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendOrderBy(widget);
//        assertEquals(result, " ORDER BY u.CustomerId, t.Year, t.Month, l.Id");
//    }
//    
//    @Test
//    public void test_appendOrderBy_tier1_3_tier2_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_DAY);
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        String result = impl.appendOrderBy(widget);
//        assertEquals(result, " ORDER BY u.CustomerId, t.Date, l.Id");
//    }
//    
//    @Test
//    public void test_appendLimit_filter_type_all() {
//        widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_ALL);
//        String result = impl.appendLimit(widget, "3000");
//        assertEquals(result, " LIMIT 3000; ");
//    }
//    
//    @Test
//    public void test_appendLimit_filter_type_subset() {
//        widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_SUBSET);
//        String result = impl.appendLimit(widget, "3000");
//        assertEquals(result, " LIMIT 3000; ");
//    }
//    
//    @Test
//    public void test_appendSubsetWhere_tier1_location() {
//        widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_SUBSET);
//        widget.setIsSubsetTier1(true);
//        String result = impl.appendSubsetWhere(widget);
//        assertEquals(result, "AND (l.Id in null) ");
//    }
//    
//    @Test
//    public void test_appendSubsetWhere_tier2_location() {
//        widget.getWidgetTierTypeByWidgetTier2Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//        widget.setIsSubsetTier2(true);
//        widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_SUBSET);
//        String result = impl.appendSubsetWhere(widget);
//        assertEquals(result, "AND (l.Id in null) ");
//    }
}
