package com.digitalpaytech.service.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.dao.impl.EntityDaoImpl;
import com.digitalpaytech.domain.Notification;

public class NotificationServiceImplTest {

    @Test
    public void test_findNotificationByEffectiveDate() {
        
        EntityDao entityDao = new EntityDaoImpl() {
            @Override
            public List findByNamedQueryAndNamedParam(String queryName, String paramName, Object value, boolean cacheable) {
                List<Notification> result = new ArrayList<Notification>();
                Notification notification = new Notification();
                notification.setId(1);
                notification.setMessage("test notification");
                result.add(notification);
                
                return result;
                
            }
        };
        
        NotificationServiceImpl notificationService = new NotificationServiceImpl();
        notificationService.setEntityDao(entityDao);
        
        List<Notification> result = notificationService.findNotificationByEffectiveDate();
        
        assertNotNull(result);
        assertEquals(result.size(), 1);
        assertEquals(((Notification)result.get(0)).getMessage(), "test notification");
    }

}
