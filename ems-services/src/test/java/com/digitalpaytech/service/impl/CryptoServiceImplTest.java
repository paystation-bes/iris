package com.digitalpaytech.service.impl;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.util.json.JSON;
import com.digitalpaytech.util.json.JSONConfigurationBean;
import com.digitalpaytech.client.dto.RSAKeyInfo;

public class CryptoServiceImplTest {
    private static final Logger LOG = Logger.getLogger(CryptoServiceImplTest.class);
    static final String LINK_PS = "500050001234";
    static final String SCALA_PS = "200020009999";
    private CryptoServiceImpl cryptoServiceImpl;
    private MockCryptoServiceSupport support;
    private MockCryptoServiceSupport.MockEmsPropertiesService propService;
    
    @Before
    public void before() {
        support = new MockCryptoServiceSupport();
        propService = support.new MockEmsPropertiesService();
        cryptoServiceImpl = new CryptoServiceImpl();
        cryptoServiceImpl.setEmsPropertiesService(propService);
        cryptoServiceImpl.setLinkCryptoClient(support.new MockLinkCryptoClient());
        cryptoServiceImpl.setCryptoClient(support.new MockCryptoClient());
        cryptoServiceImpl.setPointOfSaleService(support.new MockPointOfSaleService());
        cryptoServiceImpl.setJson(new JSON(new JSONConfigurationBean(true, false, false)));
    }
    
    @Test
    public void encryptData() {
        try {
            propService.setEncryptionMode("CryptoService");
            propService.setUseLinkCrypto("0");
            
            String data = cryptoServiceImpl.encryptData("Scala");
            assertEquals("MockCryptoClient - encrypt - Scala", data);
            
            propService.setUseLinkCrypto("1");
            data = cryptoServiceImpl.encryptData("Link");
            assertEquals("MockLinkCryptoClient - encryptInternal - Link", data);
            
        } catch (CryptoException ce) {
            fail(ce.getMessage());
        }
    }
    
    @Test
    public void currentExternalKey() {
        try {
            propService.setEncryptionMode("CryptoService");
            propService.setUseLinkCrypto("0");
            
            String data = cryptoServiceImpl.currentExternalKey();
            assertEquals("MockCryptoClient - currentExternalKey", data);
            
            propService.setUseLinkCrypto("1");
            data = cryptoServiceImpl.currentExternalKey();
            assertEquals("MockLinkCryptoClient - currentExternalKey", data);

        } catch (CryptoException ce) {
            fail(ce.getMessage());
        }
    }
    
    @Test
    public void currentExternalKeyWithSerialNumber() {
        try {
            propService.setEncryptionMode("CryptoService");
            propService.setUseLinkCrypto("0");
            
            String data = cryptoServiceImpl.currentExternalKey(false, LINK_PS);
            assertEquals("MockCryptoClient - currentExternalKey", data);
            
            propService.setUseLinkCrypto("1");
            data = cryptoServiceImpl.currentExternalKey(true, SCALA_PS);
            assertEquals("MockLinkCryptoClient - currentExternalKey", data);            
            
        } catch (CryptoException ce) {
            fail(ce.getMessage());
        }
    }
    
    @Test
    public void currentInternalKey() {
        try {
            propService.setEncryptionMode("CryptoService");
            propService.setUseLinkCrypto("0");

            String data = cryptoServiceImpl.currentInternalKey();
            assertEquals("MockCryptoClient - currentInternalKey", data);
        
            propService.setUseLinkCrypto("1");
            data = cryptoServiceImpl.currentInternalKey();
            assertEquals("MockLinkCryptoClient - currentInternalKey", data);
            
        } catch (CryptoException ce) {
            fail(ce.getMessage());
        }
    }
    
    @Test
    public void retrieveKeyInfo() {
        try {
            propService.setEncryptionMode("CryptoService");
            propService.setUseLinkCrypto("0");
            
            RSAKeyInfo data = cryptoServiceImpl.retrieveKeyInfo("000121");
            assertEquals("Scala", data.getName());
            
            propService.setUseLinkCrypto("1");
            data = cryptoServiceImpl.retrieveKeyInfo("006001");
            assertEquals("LINK", data.getName());
            
        } catch (CryptoException ce) {
            fail(ce.getMessage());
        }
    }
}
