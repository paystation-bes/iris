package com.digitalpaytech.service.impl;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import org.junit.Test;
import static org.junit.Assert.*;

import com.digitalpaytech.domain.ExtensibleRate;
import com.digitalpaytech.domain.ExtensibleRateDayOfWeek;
import com.digitalpaytech.domain.DayOfWeek;
import com.digitalpaytech.domain.ParkingPermission;
import com.digitalpaytech.domain.ParkingPermissionDayOfWeek;
import com.digitalpaytech.service.LocationService;


public class LocationServiceImplTest {
    

    @Test
    public void getValidRatesByLocationId() {
        LocationService serv = new LocationServiceImpl() {
            @Override
            public List<ExtensibleRateDayOfWeek> getValidRatesDayOfWeekByLocationId(Integer locationId) {
                return createExtensibleRateDayOfWeeks();
            }
        };
        List<List<ExtensibleRate>> list = serv.getValidRatesByLocationId(2);
        assertNotNull(list);
        
        int numDays = 0;
        Iterator<List<ExtensibleRate>> iter = list.iterator();
        while (iter.hasNext()) {
            List<ExtensibleRate> ers = iter.next();
            assertNotNull(ers);
            
            if (ers.size() > 0) {
                numDays++;
            }
        }
        assertEquals(4, numDays);
    }
    
    @Test
    public void getValidParkingPermissionsByLocationId() {
        LocationService serv = new LocationServiceImpl() {
            @Override
            public List<ParkingPermissionDayOfWeek> getValidParkingPermissionsDayOfWeekByLocationId(Integer locationId) {
                return createParkingPermissionDayOfWeeks();
            }
        };
        List<List<ParkingPermission>> list = serv.getValidParkingPermissionsByLocationId(422);
        assertNotNull(list);
        
        int numDays = 0;
        Iterator<List<ParkingPermission>> iter = list.iterator();
        while (iter.hasNext()) {
            List<ParkingPermission> pps = iter.next();
            assertNotNull(pps);
            
            if (pps.size() > 0) {
                numDays++;
            }
        }
        assertEquals(3, numDays);
    }

    private List<ParkingPermissionDayOfWeek> createParkingPermissionDayOfWeeks() {
        List<ParkingPermissionDayOfWeek> list = new ArrayList<ParkingPermissionDayOfWeek>();
        ParkingPermissionDayOfWeek ppdow1 = new ParkingPermissionDayOfWeek();
        ppdow1.setParkingPermission(createParkingPermission("Test PP1", 0, true));
        ppdow1.setDayOfWeek(createDayOfWeek(2));
        list.add(ppdow1);
        
        ParkingPermissionDayOfWeek ppdow2 = new ParkingPermissionDayOfWeek();
        ppdow2.setParkingPermission(createParkingPermission("Test PP2", 2, false));
        ppdow2.setDayOfWeek(createDayOfWeek(5));
        list.add(ppdow2);

        ParkingPermissionDayOfWeek ppdow3 = new ParkingPermissionDayOfWeek();
        ppdow1.setParkingPermission(createParkingPermission("Test PP3", 4, false));
        ppdow3.setDayOfWeek(createDayOfWeek(6));
        list.add(ppdow3);
        return list;
    }
    
    private List<ExtensibleRateDayOfWeek> createExtensibleRateDayOfWeeks() {
        List<ExtensibleRateDayOfWeek> list = new ArrayList<ExtensibleRateDayOfWeek>();
        ExtensibleRateDayOfWeek erdow1 = new ExtensibleRateDayOfWeek();
        erdow1.setExtensibleRate(createExtensibleRate("Test ExtR 1", 8, 1, 3));
        erdow1.setDayOfWeek(createDayOfWeek(1));
        list.add(erdow1);
        
        ExtensibleRateDayOfWeek erdow2 = new ExtensibleRateDayOfWeek();
        erdow2.setExtensibleRate(createExtensibleRate("Test ExtR 1", 8, 1, 3));
        erdow2.setDayOfWeek(createDayOfWeek(2));
        list.add(erdow2);
        
        ExtensibleRateDayOfWeek erdow3 = new ExtensibleRateDayOfWeek();
        erdow3.setExtensibleRate(createExtensibleRate("Test ExtR 1", 8, 1, 3));
        erdow3.setDayOfWeek(createDayOfWeek(5));
        list.add(erdow3);
        
        ExtensibleRateDayOfWeek erdow4 = new ExtensibleRateDayOfWeek();
        erdow4.setExtensibleRate(createExtensibleRate("Test ExtR 7", 27, 3, 6));
        erdow4.setDayOfWeek(createDayOfWeek(7));
        list.add(erdow4);
        
        return list;
    }
    
    private ExtensibleRate createExtensibleRate(String name, int hourlyRate, int serviceFee, int minExt) {
        ExtensibleRate er = new ExtensibleRate();
        short shr = 5;
        short smin = 35;
        er.setBeginHourLocal(shr);
        er.setBeginMinuteLocal(smin);
        short ehr = 8;
        short emin = 0;
        er.setEndHourLocal(ehr);
        er.setEndMinuteLocal(emin);
        
        er.setName(name);
        er.setIsActive(true);
        er.setRateAmount(hourlyRate);
        er.setServiceFeeAmount(serviceFee);
        er.setMinExtensionMinutes(minExt);
        return er;
    }
    
    private ParkingPermission createParkingPermission(String name, int maxDuration, boolean isUnlimited) {
        ParkingPermission pp = new ParkingPermission();
        short shr = 7;
        short smin = 40;
        pp.setBeginHourLocal(shr);
        pp.setBeginMinuteLocal(smin);
        short ehr = 11;
        short emin = 10;
        pp.setEndHourLocal(ehr);
        pp.setEndMinuteLocal(emin);
        
        pp.setName(name);
        pp.setIsActive(true);
        pp.setIsLimitedOrUnlimited(isUnlimited);
        pp.setMaxDurationMinutes(maxDuration);
        return pp;
    }
    
    
    // SUN - 1, MON - 2, TUE - 3, WED - 4, THU - 5, FRI - 6, SAT - 7
    private DayOfWeek createDayOfWeek(int day) {
        DayOfWeek dow = new DayOfWeek();
        dow.setId(day);
        dow.setDayOfWeek(day);
        return dow;
    }
}
