package com.digitalpaytech.service.impl;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.WidgetChartType;
import com.digitalpaytech.domain.WidgetFilterType;
import com.digitalpaytech.domain.WidgetMetricType;
import com.digitalpaytech.domain.WidgetRangeType;
import com.digitalpaytech.domain.WidgetTierType;
import com.digitalpaytech.service.WebWidgetHelperService;
import com.digitalpaytech.util.WidgetConstants;

public class WebWidgetActiveAlertsServiceImplTest {

//  private Widget widget;
//  private WebWidgetActiveAlertsServiceImpl impl;
//  private UserAccount userAccount;
//
//  protected WebWidgetHelperService webWidgetHelperService;    
//  
//  protected void setWebWidgetHelperService(WebWidgetHelperService webWidgetHelperService) {
//      this.webWidgetHelperService = webWidgetHelperService;
//  }
//  
//  @Before
//  public void setUp() {       
//      
//      
//      userAccount = new UserAccount();
//      Customer customer = new Customer();
//      customer.setId(2);
//      userAccount.setCustomer(customer);
//      
//        setWebWidgetHelperService(new WebWidgetHelperServiceImpl());
//      impl = new WebWidgetActiveAlertsServiceImpl();
//      impl.setWebWidgetHelperService(this.webWidgetHelperService);
//      widget = new Widget();
//      WidgetTierType type1 = new WidgetTierType();
//      type1.setId(0);
//      WidgetTierType type2 = new WidgetTierType();
//      type2.setId(0);
//      WidgetTierType type3 = new WidgetTierType();
//      type3.setId(0);
//      WidgetRangeType rangeType = new WidgetRangeType();
//      WidgetMetricType widgetMetricType = new WidgetMetricType();
//      WidgetFilterType widgetFilterType = new WidgetFilterType();
//      WidgetChartType widgetChartType = new WidgetChartType();
//      
//      widget.setWidgetTierTypeByWidgetTier1Type(type1);
//      widget.setWidgetTierTypeByWidgetTier2Type(type2);
//      widget.setWidgetTierTypeByWidgetTier3Type(type3);
//      widget.setWidgetRangeType(rangeType);
//      widget.setWidgetMetricType(widgetMetricType);
//      widget.setWidgetFilterType(widgetFilterType);
//      widget.setWidgetChartType(widgetChartType);
//      widget.setName("test widget");
//      
//  }
//  
//  @After
//  public void cleanUp() {
//      impl = null;
//      widget = null;
//  }         
//
//  /*
//  @Test
//  public void test_appendColumns() {
//      String result = impl.appendColumns(widget);     
//      assertEquals(result, "SELECT c.Id AS CustomerId, COUNT(DISTINCT apa.PointOfSaleId) AS WidgetMetricValue ");
//  }   
//  @Test
//  public void test_appendColumns_tier1_location() {
//      widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//      String result = impl.appendColumns(widget); 
//      assertEquals(result, "SELECT c.Id AS CustomerId, l.Id AS Tier1TypeId, l.Name AS Tier1TypeName, COUNT(DISTINCT apa.PointOfSaleId) AS WidgetMetricValue ");
//  }   
//  @Test
//  public void test_appendColumns_tier1_route() {
//      widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//      String result = impl.appendColumns(widget); 
//      assertEquals(result, "SELECT c.Id AS CustomerId, r.Id AS Tier1TypeId, r.Name AS Tier1TypeName, COUNT(DISTINCT apa.PointOfSaleId) AS WidgetMetricValue ");
//  }   
//
//  
//
//  
//  @Test
//  public void test_appendWhere_customerId_null() {
//      String result = impl.appendWhere(widget, userAccount);
//      assertEquals(result, "WHERE c.Id = :customerId AND apa.IsActive = 1 AND poss.IsVisible = 1 AND poss.IsDecommissioned = 0 AND poss.IsDeleted = 0 ");
//  }                          
//  @Test
//  public void test_appendWhere_customerId_not_null() {
//      widget.setCustomerId(2);
//      String result = impl.appendWhere(widget, userAccount);
//      assertEquals(result, "WHERE c.Id = :customerId AND apa.IsActive = 1 AND poss.IsVisible = 1 AND poss.IsDecommissioned = 0 AND poss.IsDeleted = 0 ");
//  }
//  @Test
//  public void test_appendWhere_tier1_type_location() {
//      widget.setCustomerId(2);
//      widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//      String result = impl.appendWhere(widget, userAccount);
//      assertEquals(result, "WHERE c.Id = :customerId AND apa.IsActive = 1 AND poss.IsVisible = 1 AND poss.IsDecommissioned = 0 AND poss.IsDeleted = 0 AND l.IsUnassigned = 0 AND l.IsDeleted = 0 ");
//  }
//  @Test
//  public void test_appendWhere_tier1_type_route() {
//      widget.setCustomerId(2);
//      widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//      String result = impl.appendWhere(widget, userAccount);
//      assertEquals(result, "WHERE c.Id = :customerId AND apa.IsActive = 1 AND poss.IsVisible = 1 AND poss.IsDecommissioned = 0 AND poss.IsDeleted = 0 ");
//  }
//  
//  
//  @Test
//  public void test_appendGroupBy() {
//      String result = impl.appendGroupBy(widget);
//      assertEquals(result, "");
//  }
//  @Test
//  public void test_appendGroupBy_tier1_location() {
//      widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//      String result = impl.appendGroupBy(widget);
//      assertEquals(result, " GROUP BY l.Id");
//  }   
//  @Test
//  public void test_appendGroupBy_tier1_route() {
//      widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//      String result = impl.appendGroupBy(widget);
//      assertEquals(result, " GROUP BY r.Id");
//  }   
//  
//  
//  @Test
//  public void test_appendOrderBy() {
//      String result = impl.appendOrderBy(widget);
//      assertEquals(result, "");
//  }   
//  @Test
//  public void test_appendOrderBy_tier1_location() {
//      widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//      String result = impl.appendOrderBy(widget);
//      assertEquals(result, " ORDER BY l.Name");
//  }   
//  @Test
//  public void test_appendOrderBy_tier1_route() {
//      widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_RATE);
//      String result = impl.appendOrderBy(widget);
//      assertEquals(result, "");
//  }   
//
//  
//  @Test
//  public void test_appendLimit_filter_type_all() {
//      widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_ALL);    
//      String result = impl.appendLimit(widget,"3000");
//      assertEquals(result, " LIMIT 3000; ");
//  }
//  @Test
//  public void test_appendLimit_filter_type_subset() {
//      widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_SUBSET); 
//      String result = impl.appendLimit(widget,"3000");
//      assertEquals(result, " LIMIT 3000; ");
//  }
//  
//
//  @Test
//  public void test_appendSubsetWhere_tier1_location() {
//      widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_LOCATION);
//      widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_SUBSET);
//      widget.setIsSubsetTier1(true);
//      String result = impl.appendSubsetWhere(widget);
//      // '...in null OR...' it's 'null' because it will be replaced by actual id values.
//      assertEquals("AND (l.Id in null OR l.Id is null) ", result);
//  }
//  @Test
//  public void test_appendSubsetWhere_tier1_route() {
//      widget.getWidgetTierTypeByWidgetTier1Type().setId(WidgetConstants.TIER_TYPE_ROUTE);
//      widget.getWidgetFilterType().setId(WidgetConstants.FILTER_TYPE_SUBSET);
//      widget.setIsSubsetTier1(true);
//      String result = impl.appendSubsetWhere(widget);
//        // '...in null OR...' it's 'null' because it will be replaced by actual id values.
//      assertEquals("AND (r.Id in null OR r.Id is null) ", result);
//  }
//  */
}
