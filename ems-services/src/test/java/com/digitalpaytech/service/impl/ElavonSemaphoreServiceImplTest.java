/**
 * 
 */
package com.digitalpaytech.service.impl;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.support.atomic.RedisAtomicInteger;

/**
 * @author mikev
 *
 */
public class ElavonSemaphoreServiceImplTest {
    private static final Logger LOG = Logger.getLogger(ElavonSemaphoreServiceImplTest.class);
    
    static JedisConnectionFactory connectionFactory;
    static String redisKey = "testKey";
    
    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestRedisBeans.xml");
        connectionFactory = (JedisConnectionFactory) context.getBean("connectionFactory");
    }
    
    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        RedisAtomicInteger txnCount = new RedisAtomicInteger(redisKey, connectionFactory);
        txnCount.set(0);
    }
    
    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }
    
    /**
     * Test method for {@link com.digitalpaytech.service.impl.ElavonSemaphoreServiceImpl#bookConnection(java.lang.String)}.
     */
    @Ignore
    @Test
    public void testBookConnection() {
        RedisAtomicInteger txnCount = new RedisAtomicInteger(redisKey, connectionFactory);
        txnCount.set(1);
        
        ElavonSemaphoreServiceImpl service = new ElavonSemaphoreServiceImpl(connectionFactory);
        Boolean book = service.bookConnection(redisKey);
        assertEquals(false, book);
    }
    
    @Ignore
    @Test
    public void testBookConnectionSuccessfully() {
        ElavonSemaphoreServiceImpl service = new ElavonSemaphoreServiceImpl(connectionFactory);
        Boolean book = service.bookConnection(redisKey);
        assertEquals(true, book);
    }
    
    
    /**
     * Test method for {@link com.digitalpaytech.service.impl.ElavonSemaphoreServiceImpl#releaseConnection(java.lang.String)}.
     */
    @Ignore
    @Test
    public void testReleaseConnection() {
        ElavonSemaphoreServiceImpl service = new ElavonSemaphoreServiceImpl(connectionFactory);
        service.releaseConnection(redisKey);
        
        RedisAtomicInteger txnCount = new RedisAtomicInteger(redisKey, connectionFactory);
        assertEquals(0, txnCount.get());
    }
    
    @Ignore
    @Test
    public final void bookConnectionWithExpire() {
        final ElavonSemaphoreServiceImpl service = new ElavonSemaphoreServiceImpl(connectionFactory);
        final Boolean book = service.bookConnectionWithExpire(redisKey, 5000);
        LOG.debug("Result after setExpire(5000): " + book.booleanValue());
        assertNotNull(book);
    }
}
