package com.digitalpaytech.service.impl;

import org.junit.*;
import static org.junit.Assert.*;
import java.util.*;

import com.digitalpaytech.domain.CreditCardType;
import com.digitalpaytech.dao.impl.EntityDaoMock;

public class CreditCardTypeServiceImplTest {
    private EntityDaoMock entityDao;
    
    @Before
    public void setUp() {
        final List<CreditCardType> list = new ArrayList<>();
        list.add(new CreditCardType(0, "N/A", new Date(), 1));
        list.add(new CreditCardType(1, "Not Assigned", new Date(), 1));
        list.add(new CreditCardType(2, "VISA", new Date(), 1));
        list.add(new CreditCardType(3, "MasterCard", new Date(), 1));
        list.add(new CreditCardType(4, "AMEX", new Date(), 1));
        list.add(new CreditCardType(5, "Discover", new Date(), 1));
        list.add(new CreditCardType(6, "Diners Club", new Date(), 1));
        list.add(new CreditCardType(7, "CreditCard", new Date(), 1));
        list.add(new CreditCardType(8, "Other", new Date(), 1));
        list.add(new CreditCardType(9, "JCB", new Date(), 1));
        
        this.entityDao = new EntityDaoMock();
        this.entityDao.setCreditCardTypes(list);
    }
    
    @Test
    public void getCreditCardTypeByName() {
        final CreditCardTypeServiceImpl serv = new CreditCardTypeServiceImpl();
        serv.setEntityDao(this.entityDao);
        
        final CreditCardType mc = serv.getCreditCardTypeByName("MasterCard");
        assertNotNull(mc);
        assertEquals("MasterCard", mc.getName());
        
        final CreditCardType cc1 = serv.getCreditCardTypeByName("JCBB", true);
        assertNotNull(cc1);
        assertEquals("CreditCard", cc1.getName());

        final CreditCardType cc2 = serv.getCreditCardTypeByName(null, true);
        assertNotNull(cc2);
        assertEquals("CreditCard", cc2.getName());

        final CreditCardType cc3 = serv.getCreditCardTypeByName("", true);
        assertNotNull(cc3);
        assertEquals("CreditCard", cc3.getName());
        
        
        final CreditCardType viNull = serv.getCreditCardTypeByName("VI");
        assertNull(viNull);
        
        final CreditCardType diNull = serv.getCreditCardTypeByName("Dis", false);
        assertNull(diNull);
    }
}
