package com.digitalpaytech.service.impl;

import static org.junit.Assert.*;

import java.io.Serializable;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerAgreement;

public class CustomerServiceImplTest {

    private CustomerServiceImpl customerServiceImpl;
    
    @Before
    public void setUp() {
        customerServiceImpl = new CustomerServiceImpl();
    }
    
    @After
    public void cleanUp() {
        customerServiceImpl = null;
    }
    
    // NOT NECESSARY
    @Test
    public void test_findCustomer() {
//      Customer customer = new Customer();
//      customer.setId(1);
//      
//      EntityDao entityDao = EasyMock.createMock(EntityDao.class);
//      EasyMock.expect(entityDao.get(Customer.class, 1)).andReturn(customer);
//      EasyMock.replay(entityDao);
//      
//      customerServiceImpl.setEntityDao(entityDao);
//      Customer result = customerServiceImpl.findCustomer(1);
//      
//      assertNotNull(result);
//      assertEquals(result.getId().intValue(), 1);
//  }
//
//  @Test
//  public void test_findCustomerNull() {
//      Customer customer = null;
//      
//      EntityDao entityDao = EasyMock.createMock(EntityDao.class);
//      EasyMock.expect(entityDao.get(Customer.class, 1)).andReturn(customer);
//      EasyMock.replay(entityDao);
//      
//      customerServiceImpl.setEntityDao(entityDao);
//      Customer result = customerServiceImpl.findCustomer(1);
//      
//      assertNull(result);
//  }
//  
//  public void test_saveCustomerAgreement() {
//      CustomerAgreement agreement = new CustomerAgreement();
//      agreement.setId(1);
//      
//      EntityDao entityDao = EasyMock.createMock(EntityDao.class);
//      EasyMock.expect(entityDao.save(agreement)).andReturn(agreement);
//      EasyMock.replay(entityDao);
//      
//      customerServiceImpl.setEntityDao(entityDao);
//      Serializable result = customerServiceImpl.saveCustomerAgreement(agreement);
//      
        assertNotNull(1);
    }
}
