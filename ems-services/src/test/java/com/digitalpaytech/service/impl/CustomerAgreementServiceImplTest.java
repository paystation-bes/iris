package com.digitalpaytech.service.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Test;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CustomerAgreement;

public class CustomerAgreementServiceImplTest {

    @Test
    public void test_findCustomerAgreementByCustomerId() {
        CustomerAgreementServiceImpl customerAgreementService = new CustomerAgreementServiceImpl();
        List<CustomerAgreement> lists = new ArrayList<CustomerAgreement>();
        CustomerAgreement ca = new CustomerAgreement();
        ca.setId(5);
        lists.add(ca);
        
        EntityDao entityDao = EasyMock.createMock(EntityDao.class);
        EasyMock.expect(entityDao.findByNamedQueryAndNamedParam(
                        "CustomerAgreement.findCustomerAgreementByCustomerId",
                        "customerId", 1, true)).andReturn(lists);
        EasyMock.replay(entityDao);
        
        customerAgreementService.setEntityDao(entityDao);
        CustomerAgreement result = customerAgreementService.findCustomerAgreementByCustomerId(1);
        
        assertNotNull(result);
        assertEquals(result.getId().intValue(), 5);
    }

    @Test
    public void test_findCustomerAgreementByCustomerId_null() {
        CustomerAgreementServiceImpl customerAgreementService = new CustomerAgreementServiceImpl();
        List<CustomerAgreement> lists = new ArrayList<CustomerAgreement>();
        
        EntityDao entityDao = EasyMock.createMock(EntityDao.class);
        EasyMock.expect(entityDao.findByNamedQueryAndNamedParam(
                        "CustomerAgreement.findCustomerAgreementByCustomerId",
                        "customerId", 1, true)).andReturn(lists);
        EasyMock.replay(entityDao);
        
        customerAgreementService.setEntityDao(entityDao);
        CustomerAgreement result = customerAgreementService.findCustomerAgreementByCustomerId(1);
        
        assertNull(result);
    }
}
