package com.digitalpaytech.service.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Test;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Tab;
import com.digitalpaytech.domain.Section;
import com.digitalpaytech.domain.Widget;

public class DashboardServiceImplTest {

    @Test
    public void test_TabsByUserId() {
        DashboardServiceImpl dashboardService = new DashboardServiceImpl();
        List<Tab> resultList = new ArrayList<Tab>();
        Tab tab = new Tab();
        tab.setId(2);
        resultList.add(tab);
        
        EntityDao entityDao = EasyMock.createMock(EntityDao.class);
        EasyMock.expect(entityDao.findByNamedQueryAndNamedParam(
                "findTabsByUserAccountId", "id", 2, true)).andReturn(resultList);
        EasyMock.replay(entityDao);
        
        dashboardService.setEntityDao(entityDao);
        Collection<Tab> result = dashboardService.findTabsByUserAccountId(2);
        
        assertNotNull(result);
        assertEquals(result.size(), 1);
    }

    @org.junit.Ignore
    @Test
    public void findTabByIdEager() {
        Widget w = new Widget();
        w.setId(1);
        w.setColumnId(1);
        ArrayList<Widget> ws = new ArrayList<Widget>();
        ws.add(w);
        
        Section se = new Section();
        se.setId(1);
        se.setWidgets(ws);
        ArrayList<Section> ss = new ArrayList<Section>();
        ss.add(se);
        
        Tab tab = new Tab();
        tab.setId(2);
        tab.setSections(ss);

        
        DashboardServiceImpl dashboardService = new DashboardServiceImpl();
        Tab newTab = dashboardService.findTabByIdEager(2);
        
        assertNotNull(newTab);
        assertEquals(2, newTab.getId().intValue());
    }
}
