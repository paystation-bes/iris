package com.digitalpaytech.domain;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.*;

public class TabTest {

	@Test
	public void deleteSection() {
		Tab tab = createTabSection();
		assertEquals(2, tab.getSections().size());
		
		tab.deleteSection("TestSec1");
		assertEquals(1, tab.getSections().size());

		Section sec2 = tab.getSection("TestSec2");
		assertNotNull(sec2);
		assertEquals("TestSec2", sec2.getRandomId());
		
		tab.deleteSection("TestSec2");
		assertEquals(0, tab.getSections().size());
	}
	
	
	private Tab createTabSection() {
		Section sec1 = new Section();
		sec1.setRandomId("TestSec1");
		
		Section sec2 = new Section();
		sec2.setRandomId("TestSec2");
		
		List<Section> list = new ArrayList<Section>();
		list.add(sec1);
		list.add(sec2);
		
		Tab tab = new Tab();
		tab.setSections(list);
		tab.addSectionToMap(sec1);
		tab.addSectionToMap(sec2);
		
		return tab;
	}
}
