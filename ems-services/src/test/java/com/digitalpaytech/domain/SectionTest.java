package com.digitalpaytech.domain;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.*;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;

import com.digitalpaytech.data.SectionColumn;
import com.digitalpaytech.domain.Widget;

public class SectionTest {

	@Test
	public void prepareSectionColumns() {
		Section sec = new Section();
		sec.setWidgets(null);
		
		sec.prepareSectionColumns();
		assertNotNull(sec.createWidgetsInColumns());
		// 3 empty SectionColumn in the beginning
		assertEquals(3, sec.createWidgetsInColumns().size());
	}
	
	@Test
	public void getSectionColumn() {
		// Column 1
		Widget col1Wig1 = new Widget();
		col1Wig1.setColumnId(0);
		Widget col1Wig2 = new Widget();
		col1Wig2.setColumnId(0);
		
		// Column 2
		Widget col2Wig1 = new Widget();
		col2Wig1.setColumnId(1);
		
		ArrayList<Widget> list = new ArrayList<Widget>();
		list.add(col1Wig1);
		list.add(col1Wig2);
		list.add(col2Wig1);
		
		Section sec = new Section();		
		sec.setWidgets(list);
		
		assertEquals(2, sec.getSectionColumn("0").size());
		assertEquals(1, sec.getSectionColumn("1").size());
		assertEquals(0, sec.getSectionColumn("2").size());
		
	}
	
	@Test
	public void createWidgetsInColumns() {
		Section sec = new Section();
		sec.setWidgets(createWidgets());
		sec.prepareSectionColumns();
		List<List<Widget>> wigsInCols = sec.createWidgetsInColumns();
		
		assertEquals(2, wigsInCols.get(0).size());
		assertEquals(1, wigsInCols.get(1).size());
		assertEquals(3, wigsInCols.get(2).size());
	}
	
	private ArrayList<Widget> createWidgets() {
		// Column 0 - 2 widgets
		Widget col1Wig1 = new Widget();
		col1Wig1.setRandomId("col1Wig1");
		col1Wig1.setColumnId(0);
		Widget col1Wig2 = new Widget();
		col1Wig2.setColumnId(0);
		col1Wig2.setRandomId("col1Wig2");
		
		// Column 1 - 1 widget
		Widget col2Wig1 = new Widget();
		col2Wig1.setColumnId(1);
		col2Wig1.setRandomId("col2Wig1");
		
		// Column 2 - 3 widgets
		Widget col3Wig1 = new Widget();
		col3Wig1.setColumnId(2);
		col3Wig1.setRandomId("col3Wig1");
		Widget col3Wig2 = new Widget();
		col3Wig2.setColumnId(2);
		col3Wig2.setRandomId("col3Wig2");
		Widget col3Wig3 = new Widget();
		col3Wig3.setColumnId(2);
		col3Wig3.setRandomId("col3Wig3");
		
		ArrayList<Widget> list = new ArrayList<Widget>();
		list.add(col1Wig1);
		list.add(col1Wig2);		
		list.add(col2Wig1);
		list.add(col3Wig1);
		list.add(col3Wig2);
		list.add(col3Wig3);
		return list;
	}
	
	
	
	@Test
	public void removeSectionColumn() {
		Section sec = new Section();
		sec.setWidgets(createWidgets());
		sec.prepareSectionColumns();

		String id = "0";
		
		sec.removeSectionColumn(id);
		SectionColumn secCol = sec.getSectionColumn("0");
		assertNotNull(secCol);
		assertEquals(0, secCol.size());
		assertEquals(0, secCol.getWidgets().size());
		assertNotNull(sec.getSectionColumn("1"));
	}
	
	@Test
	public void removeSectionColumns() {
		Section sec = new Section();
		sec.setWidgets(createWidgets());
		sec.prepareSectionColumns();

		String[] ids = new String[]{"1", "2"};
		sec.removeSectionColumns(ids);
		assertNotNull(sec.getSectionColumn("2"));
		assertEquals(0, sec.getSectionColumn("1").size());
		assertEquals(0, sec.getSectionColumn("2").getWidgets().size());
	}
	

	@Test
	public void deleteWidget() {
		Section sec = new Section();
		List<Widget> ws = createWidgets();
		sec.setWidgetMap(createWidgetMap(ws));
		sec.setWidgets(ws);
		sec.prepareSectionColumns();
		List<Section> list = new ArrayList<Section>();
		list.add(sec);
		Tab tab = new Tab();
		tab.setSections(list);
		
		MockHttpServletRequest req = new MockHttpServletRequest();
		MockHttpSession ses = new MockHttpSession();
		req.setSession(ses);
		
		sec = tab.getSections().get(0);
		Widget w = sec.deleteWidget("col3Wig3");
		assertEquals("col3Wig3", w.getRandomId());
		assertEquals(5, sec.getWidgets().size());
		assertNull(sec.getWidgetMap().get("col3Wig3"));
	}
	
	
	private Map<String, Widget> createWidgetMap(List<Widget> widgets) {
		Map<String, Widget> map = new HashMap<String, Widget>(widgets.size());
		Iterator<Widget> iter = widgets.iterator();
		while (iter.hasNext()) {
			Widget w = iter.next();
			map.put(w.getRandomId(), w);
		}
		return map;
	}
}
