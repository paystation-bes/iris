package com.digitalpaytech.dto.customeradmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.digitalpaytech.domain.TimezoneVId;

public class GlobalConfigurationTest {
    
    @Test
    public void findTimeZone() {
        GlobalConfiguration cfg = new GlobalConfiguration();
        loadTimeZones(cfg);
        
        assertEquals("US/Pacific", cfg.findTimeZone("US/Pacific").getName());
        assertEquals("US/Central", cfg.findTimeZone("US/Central").getName());
        assertEquals("Canada/Eastern", cfg.findTimeZone("Canada/Eastern").getName());
        assertNull(cfg.findTimeZone("US/Mountain"));
    }
    
    private void loadTimeZones(GlobalConfiguration cfg) {
        TimezoneVId tz1 = new TimezoneVId();
        tz1.setId(1);
        tz1.setName("US/Pacific");
        
        TimezoneVId tz2 = new TimezoneVId();
        tz2.setId(2);
        tz2.setName("Canada/Eastern");
        
        TimezoneVId tz3 = new TimezoneVId();
        tz3.setId(3);
        tz3.setName("US/Central");
        
        List<TimezoneVId> tzs = new ArrayList<TimezoneVId>();
        tzs.add(tz3);
        tzs.add(tz1);
        tzs.add(tz2);
        
        cfg.setTimeZones(tzs);
    }
}
