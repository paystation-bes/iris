package com.digitalpaytech.dto.cps;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.UUID;

public class TokensTest {
    @Test
    public void getNullToken() {
        final Tokens tokens = new Tokens();
        assertNull(tokens.getToken(Tokens.TokenTypes.CARD_TOKEN));
        assertNull(tokens.getToken(Tokens.TokenTypes.CHARGE_TOKEN));
        assertNull(tokens.getToken(Tokens.TokenTypes.MERCHANT_TOKEN));
        assertNull(tokens.getToken(Tokens.TokenTypes.REFUND_TOKEN));
        assertNull(tokens.getToken(Tokens.TokenTypes.TERMINAL_TOKEN));
    }
    
    @Test
    public void addToken() {
        final Tokens tokens = new Tokens();
        tokens.addToken(Tokens.TokenTypes.CARD_TOKEN, UUID.randomUUID());
        assertNotNull(tokens.getToken(Tokens.TokenTypes.CARD_TOKEN));
        tokens.addToken(Tokens.TokenTypes.CHARGE_TOKEN, UUID.randomUUID());
        assertNotNull(tokens.getToken(Tokens.TokenTypes.CHARGE_TOKEN));
        tokens.addToken(Tokens.TokenTypes.MERCHANT_TOKEN, UUID.randomUUID());
        assertNotNull(tokens.getToken(Tokens.TokenTypes.MERCHANT_TOKEN));
        tokens.addToken(Tokens.TokenTypes.REFUND_TOKEN, UUID.randomUUID());
        assertNotNull(tokens.getToken(Tokens.TokenTypes.REFUND_TOKEN));
        tokens.addToken(Tokens.TokenTypes.TERMINAL_TOKEN, UUID.randomUUID());
        assertNotNull(tokens.getToken(Tokens.TokenTypes.TERMINAL_TOKEN));
    }
    
    @Test
    public void count() {
        final Tokens tokens = new Tokens();
        assertEquals(0, tokens.count());
        
        tokens.addToken(Tokens.TokenTypes.CHARGE_TOKEN, UUID.randomUUID());
        tokens.addToken(Tokens.TokenTypes.TERMINAL_TOKEN, UUID.randomUUID());
        assertEquals(2, tokens.count());
    }
    
    @Test
    public void addNullToken() {
        final Tokens tokens = new Tokens();
        tokens.addToken(Tokens.TokenTypes.MERCHANT_TOKEN, null);
        assertNull(tokens.getToken(Tokens.TokenTypes.MERCHANT_TOKEN));
    }
}
