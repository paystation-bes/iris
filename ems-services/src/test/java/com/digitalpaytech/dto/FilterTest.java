package com.digitalpaytech.dto;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.*;

public class FilterTest {
	
	@Test
	public void hasTier1() {
		Filter filter = new Filter();
		filter.setType("Today");
		filter.setTier1s(createTier1s(false));
		
		if (filter.getTier1s("Hour").isEmpty()) { 
			fail(); 
		}
		if (filter.getTier1s("Organization").isEmpty()) { 
			fail(); 
		}
		if (!filter.getTier1s("Now").isEmpty()) { 
			fail(); 
		}
		
		filter = new Filter();
		filter.setType("Last 24 Hours");
		filter.setTier1s(createTier1s(true));
		if (filter.getTier1s("").isEmpty()) { 
			fail(); 
		}
		if (!filter.getTier1s("Organization").isEmpty()) { 
			fail(); 
		}
	}
	
	
	private List<Tier1> createTier1s(boolean allFlag) {
		List<Tier1> list = new ArrayList<Tier1>();
		if (allFlag) {
    		Tier1 t1 = new Tier1();
    		t1.setType("");
    		list.add(t1);
		} else {
    		Tier1 t2 = new Tier1();
    		t2.setType("Hour");
    
    		Tier1 t3 = new Tier1();
    		t3.setType("Organization");
    		list.add(t2);
    		list.add(t3);
		}		
		return list;
	}
}
