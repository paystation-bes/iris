package com.digitalpaytech.dto;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.*;

public class Tier1Test {

	@Test
	public void getTier2() {
		Tier1 t1 = new Tier1();
		t1.setType("Hour");
		t1.setTier2s(createTier2s());
		
//		if (t1.getTier2s("Parent Organization").isEmpty()) {
//			fail();
//		}
		if (t1.getTier2s("TransactionType").isEmpty()) {
			fail();
		}
		if (t1.getTier2s("Route").isEmpty()) {
			fail();
		}
		
		
		if (!t1.getTier2s("Rate").isEmpty()) {
			fail();
		}
		if (!t1.getTier2s("").isEmpty()) {
			fail();
		}
	}
	
	
	private List<Tier2> createTier2s() {
		
		Tier2 t1 = new Tier2();
		t1.setType("Organization");

		Tier2 t2 = new Tier2();
		t2.setType("Route");
		
		Tier2 t3 = new Tier2();
		t3.setType("TransactionType");

		List<Tier2> list = new ArrayList<Tier2>();
		list.add(t1);
		list.add(t2);
		list.add(t3);
		return list;
	}
}
