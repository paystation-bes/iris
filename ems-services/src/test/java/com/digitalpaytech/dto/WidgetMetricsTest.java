package com.digitalpaytech.dto;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.*;

public class WidgetMetricsTest {
	
	@Test
	public void getMetric() {
		WidgetMetrics widgetMetrics = new WidgetMetrics();
		widgetMetrics.setMetrics(createMetrics());
		Metric metric = widgetMetrics.getMetric("Utilization");
		assertNotNull(metric);
		assertEquals("Utilization", metric.getName());
		
		metric = widgetMetrics.getMetric("Total Purchases");
		assertNotNull(metric);
		assertEquals("Total Purchases", metric.getName());
		
		metric = widgetMetrics.getMetric("Total Revenue");
		assertNotNull(metric);
		assertEquals("Total Revenue", metric.getName());
		
		metric = widgetMetrics.getMetric("Test");
		assertNull(metric);
	}
	
	private List<Metric> createMetrics() {
		Metric totalRevenue = new Metric();
		totalRevenue.setName("Total Revenue");
		
		Metric totalPurchases = new Metric();
		totalPurchases.setName("Total Purchases");
		
		Metric utilization = new Metric();
		utilization.setName("Utilization");

		List<Metric> list = new ArrayList<Metric>();
		list.add(totalRevenue);
		list.add(totalPurchases);
		list.add(utilization);
		
		return list;
	}
}
