package com.digitalpaytech.dto;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.*;

public class Tier2Test {

	@Test
	public void getTier2() {
		Tier2 t1 = new Tier2();
		t1.setType("Route");
		t1.setTier3s(createTier3s());
		
		assertNotNull(t1.getTier3("RevenueType"));
		assertNotNull(t1.getTier3("TransactionType"));
		assertNotNull(t1.getTier3("Rate"));
		
		assertNull(t1.getTier3(""));
	}
	
	
	private List<Tier3> createTier3s() {
		
		Tier3 t1 = new Tier3();
		t1.setType("TransactionType");

		Tier3 t2 = new Tier3();
		t2.setType("Rate");
		
		Tier3 t3 = new Tier3();
		t3.setType("RevenueType");

		List<Tier3> list = new ArrayList<Tier3>();
		list.add(t1);
		list.add(t2);
		list.add(t3);
		return list;
	}
}
