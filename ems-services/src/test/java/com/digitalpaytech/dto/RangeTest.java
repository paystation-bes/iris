package com.digitalpaytech.dto;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.*;

public class RangeTest {
	
	@Test
	public void isCorrectType() {
		Range range = new Range();
		range.setType("Today, Yesterday,        Last 24 Hours       ");
		assertTrue(range.isCorrectType("Last 24 Hours"));
		assertTrue(range.isCorrectType("Yesterday"));
		assertFalse(range.isCorrectType(" Last 30 Days"));
		
		range.setType("Today,Yesterday,Last 24 Hours");
		assertTrue(range.isCorrectType("    Last 24 Hours       "));
		assertFalse(range.isCorrectType(" Last 30 Days"));
	}

	@Test
	public void hasTier1() {
		Range range = new Range();
		range.setType("Today");
		range.setFilters(createFilters(false));
		
		if (range.getFilters("All").isEmpty()) { 
			fail(); 
		}
		if (range.getFilters("Top N").isEmpty()) { 
			fail(); 
		}
		if (!range.getFilters("").isEmpty()) { 
			fail(); 
		}
		
		range = new Range();
		range.setType("Last 24 Hours");
		range.setFilters(createFilters(true));
		if (range.getFilters("").isEmpty()) { 
			fail(); 
		}
		if (!range.getFilters("All").isEmpty()) { 
			fail(); 
		}
	}
	
	
	private List<Filter> createFilters(boolean allFlag) {
		List<Filter> list = new ArrayList<Filter>();
		if (allFlag) {
			Filter f = new Filter();
    		f.setType("");
    		list.add(f);
		} else {
			Filter f1 = new Filter();
    		f1.setType("All");
    
    		Filter f2 = new Filter();
    		f2.setType("Top N");

    		Filter f3 = new Filter();
    		f3.setType("Bottom N");
    		
    		list.add(f1);
    		list.add(f2);
    		list.add(f3);
		}		
		return list;
	}
}
