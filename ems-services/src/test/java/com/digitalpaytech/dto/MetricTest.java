package com.digitalpaytech.dto;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.*;

public class MetricTest {

	@Test
	public void getRange() {
		Metric metric = new Metric();
		metric.setRanges(createRanges());
		Range range = metric.getRange("now");
		assertNotNull(range);
		
		range = metric.getRange("This Week");
		assertNotNull(range);
		range = metric.getRange("Last week");
		assertNotNull(range);
		range = metric.getRange("Last Year");
		assertNotNull(range);
		range = metric.getRange("Last 7 Days");
		assertNotNull(range);		
		
		range = metric.getRange("Today");
		assertNull(range);
	}
	
	
	private List<Range> createRanges() {
		Range r1 = new Range();
		r1.setType("Now");
		Range r2 = new Range();
		r2.setType("Last 7 Days, Last 30 Days, Last 12 Months, This Week, This Month, Last week, Last Month, Last Year, This Year");

		List<Range> list = new ArrayList<Range>();
		list.add(r1);
		list.add(r2);
		return list;
	}
}
