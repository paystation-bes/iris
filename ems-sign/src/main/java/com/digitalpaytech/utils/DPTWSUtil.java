package com.digitalpaytech.utils;

import org.apache.log4j.Logger;

import com.digitalpaytech.ws.adapter.WSAdapter;
import com.digitalpaytech.ws.provision.ProvisionService;
import com.digitalpaytech.ws.signingservice.SigningService;
import com.digitalpaytech.ws.signingservicev2.SigningServiceV2;

public class DPTWSUtil {
    private final static Logger logger = Logger.getLogger(DPTWSUtil.class);
    
    private static String ss_ws_url = "https://cerberus.digitalpaytech.com/signingservice/services/SigningService";
    private static String ems_ws_url = "https://www.intellapay.com/services/EMS2Cerberus";
    private static String cerberus_ws_url = "https://cerberus.digitalpaytech.com/services/CerberusService";
    
    private static SigningService signingService = null;
    private static SigningServiceV2 signingServiceV2 = null;
    private static ProvisionService provisionService = null;
    
    private DPTWSUtil() {
        // Empty constructor
    }
    
    // synchronized so that while the signingservice is initializing, another thread doesn't call into this
    // method.
    public static synchronized SigningService getSigningService(String ssUrl, String userName, String password) {
        if (signingService == null) {
            if (ssUrl == null || ssUrl.length() == 0) {
                ssUrl = ss_ws_url;
            }
            if (userName == null || userName.length() == 0) {
                logger.error("!!! userName is null or empty. !!!");
                return null;
            }
            if (password == null || password.length() == 0) {
                logger.error("!!! password is null or empty. !!!");
                return null;
            }
            WSAdapter adapter = new WSAdapter();
            adapter.setServiceLocation(ssUrl);
            adapter.setUserName(userName);
            adapter.setPassword(password);
            signingService = adapter.getSigningService();
        }
        return signingService;
    }

    
    public static synchronized SigningServiceV2 getSigningServiceV2(String signingServiceV2Url, 
                                                                    String signingServiceV2UserName, 
                                                                    String signingServiceV2Password) {
        if (signingServiceV2 == null) {
            WSAdapter adapter = new WSAdapter();
            adapter.setServiceLocation(signingServiceV2Url);
            adapter.setUserName(signingServiceV2UserName);
            adapter.setPassword(signingServiceV2Password);
            signingServiceV2 = adapter.getSigningServiceV2();
        }
        return signingServiceV2;
    }    
    
    public static synchronized ProvisionService getProvisionService(String provisionServiceUrl, String userName, String password) {
        if (provisionService == null) {
            if (provisionServiceUrl == null || provisionServiceUrl.length() == 0) {
                logger.error("!!! provisionServiceUrl is null or empty. !!!");
                return null;
            }
            if (userName == null || userName.length() == 0) {
                logger.error("!!! userName is null or empty. !!!");
                return null;
            }
            if (password == null || password.length() == 0) {
                logger.error("!!! password is null or empty. !!!");
                return null;
            }
            WSAdapter adapter = new WSAdapter();
            adapter.setServiceLocation(provisionServiceUrl);
            adapter.setUserName(userName + ":" + userName);
            adapter.setPassword(password);
            provisionService = adapter.getProvisionService();
        }
        return provisionService;
    }
    
    public static void setSigningServerUrl(String ssUrl) {
        ss_ws_url = ssUrl;
    }
    
    public static void setEMSWSUrl(String emsWsUrl) {
        ems_ws_url = ems_ws_url;
    }
    
    public static void setCerberusWSUrl(String cerberusWsUrl) {
        cerberus_ws_url = cerberusWsUrl;
    }
    
}
