
package com.digitalpaytech.ws.signingservicev2;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.digitalpaytech.ws.signingservicev2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetSignStringRequest_QNAME = new QName("http://ws.digitalpaytech.com/SigningServiceV2/", "GetSignStringRequest");
    private final static QName _GetSignBytesResponse_QNAME = new QName("http://ws.digitalpaytech.com/SigningServiceV2/", "GetSignBytesResponse");
    private final static QName _GetSignBytesRequest_QNAME = new QName("http://ws.digitalpaytech.com/SigningServiceV2/", "GetSignBytesRequest");
    private final static QName _GetSignStringResponse_QNAME = new QName("http://ws.digitalpaytech.com/SigningServiceV2/", "GetSignStringResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.digitalpaytech.ws.signingservicev2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UnsupportedHashRequestFault }
     * 
     */
    public UnsupportedHashRequestFault createUnsupportedHashRequestFault() {
        return new UnsupportedHashRequestFault();
    }

    /**
     * Create an instance of {@link GetSignStringType }
     * 
     */
    public GetSignStringType createGetSignStringType() {
        return new GetSignStringType();
    }

    /**
     * Create an instance of {@link GetSignStringFault }
     * 
     */
    public GetSignStringFault createGetSignStringFault() {
        return new GetSignStringFault();
    }

    /**
     * Create an instance of {@link GetTypeSignFault }
     * 
     */
    public GetTypeSignFault createGetTypeSignFault() {
        return new GetTypeSignFault();
    }

    /**
     * Create an instance of {@link GetSignBytesType }
     * 
     */
    public GetSignBytesType createGetSignBytesType() {
        return new GetSignBytesType();
    }

    /**
     * Create an instance of {@link GetSignBytesFault }
     * 
     */
    public GetSignBytesFault createGetSignBytesFault() {
        return new GetSignBytesFault();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSignStringType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalpaytech.com/SigningServiceV2/", name = "GetSignStringRequest")
    public JAXBElement<GetSignStringType> createGetSignStringRequest(GetSignStringType value) {
        return new JAXBElement<GetSignStringType>(_GetSignStringRequest_QNAME, GetSignStringType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalpaytech.com/SigningServiceV2/", name = "GetSignBytesResponse")
    public JAXBElement<String> createGetSignBytesResponse(String value) {
        return new JAXBElement<String>(_GetSignBytesResponse_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSignBytesType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalpaytech.com/SigningServiceV2/", name = "GetSignBytesRequest")
    public JAXBElement<GetSignBytesType> createGetSignBytesRequest(GetSignBytesType value) {
        return new JAXBElement<GetSignBytesType>(_GetSignBytesRequest_QNAME, GetSignBytesType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalpaytech.com/SigningServiceV2/", name = "GetSignStringResponse")
    public JAXBElement<String> createGetSignStringResponse(String value) {
        return new JAXBElement<String>(_GetSignStringResponse_QNAME, String.class, null, value);
    }

}
