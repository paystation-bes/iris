
package com.digitalpaytech.ws.provision;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.dpt.ws.provision package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UpdatePaystationResponse_QNAME = new QName("http://ws.dpt.com/provision", "UpdatePaystationResponse");
    private final static QName _UpdateCustomerResponse_QNAME = new QName("http://ws.dpt.com/provision", "UpdateCustomerResponse");
    private final static QName _DeletePaystationResponse_QNAME = new QName("http://ws.dpt.com/provision", "DeletePaystationResponse");
    private final static QName _GetCustomerAgreementSignedResponse_QNAME = new QName("http://ws.dpt.com/provision", "GetCustomerAgreementSignedResponse");
    private final static QName _CreatePaystationResponse_QNAME = new QName("http://ws.dpt.com/provision", "CreatePaystationResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.dpt.ws.provision
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PaystationEventRequest }
     * 
     */
    public PaystationEventRequest createPaystationEventRequest() {
        return new PaystationEventRequest();
    }

    /**
     * Create an instance of {@link PaystationHardwareRequest }
     * 
     */
    public PaystationHardwareRequest createPaystationHardwareRequest() {
        return new PaystationHardwareRequest();
    }

    /**
     * Create an instance of {@link UpdateCustomerRequest }
     * 
     */
    public UpdateCustomerRequest createUpdateCustomerRequest() {
        return new UpdateCustomerRequest();
    }

    /**
     * Create an instance of {@link CustomerServiceType }
     * 
     */
    public CustomerServiceType createCustomerServiceType() {
        return new CustomerServiceType();
    }

    /**
     * Create an instance of {@link PaystationSoftwareRequest }
     * 
     */
    public PaystationSoftwareRequest createPaystationSoftwareRequest() {
        return new PaystationSoftwareRequest();
    }

    /**
     * Create an instance of {@link PaystationHardwareResponse }
     * 
     */
    public PaystationHardwareResponse createPaystationHardwareResponse() {
        return new PaystationHardwareResponse();
    }

    /**
     * Create an instance of {@link PaystationHardwareDetailType }
     * 
     */
    public PaystationHardwareDetailType createPaystationHardwareDetailType() {
        return new PaystationHardwareDetailType();
    }

    /**
     * Create an instance of {@link ProvisionServiceFault }
     * 
     */
    public ProvisionServiceFault createProvisionServiceFault() {
        return new ProvisionServiceFault();
    }

    /**
     * Create an instance of {@link PaystationEventHistoryRequest }
     * 
     */
    public PaystationEventHistoryRequest createPaystationEventHistoryRequest() {
        return new PaystationEventHistoryRequest();
    }

    /**
     * Create an instance of {@link GetCustomerAgreementDetailResponse }
     * 
     */
    public GetCustomerAgreementDetailResponse createGetCustomerAgreementDetailResponse() {
        return new GetCustomerAgreementDetailResponse();
    }

    /**
     * Create an instance of {@link CustomerAgreementDetailType }
     * 
     */
    public CustomerAgreementDetailType createCustomerAgreementDetailType() {
        return new CustomerAgreementDetailType();
    }

    /**
     * Create an instance of {@link DeletePaystationRequest }
     * 
     */
    public DeletePaystationRequest createDeletePaystationRequest() {
        return new DeletePaystationRequest();
    }

    /**
     * Create an instance of {@link CreatePaystationRequest }
     * 
     */
    public CreatePaystationRequest createCreatePaystationRequest() {
        return new CreatePaystationRequest();
    }

    /**
     * Create an instance of {@link PaystationBillingInfoRequest }
     * 
     */
    public PaystationBillingInfoRequest createPaystationBillingInfoRequest() {
        return new PaystationBillingInfoRequest();
    }

    /**
     * Create an instance of {@link GetCustomerAgreementSignedRequest }
     * 
     */
    public GetCustomerAgreementSignedRequest createGetCustomerAgreementSignedRequest() {
        return new GetCustomerAgreementSignedRequest();
    }

    /**
     * Create an instance of {@link UpdatePaystationRequest }
     * 
     */
    public UpdatePaystationRequest createUpdatePaystationRequest() {
        return new UpdatePaystationRequest();
    }

    /**
     * Create an instance of {@link PaystationSensorDataRequest }
     * 
     */
    public PaystationSensorDataRequest createPaystationSensorDataRequest() {
        return new PaystationSensorDataRequest();
    }

    /**
     * Create an instance of {@link GetCustomerAgreementDetailRequest }
     * 
     */
    public GetCustomerAgreementDetailRequest createGetCustomerAgreementDetailRequest() {
        return new GetCustomerAgreementDetailRequest();
    }

    /**
     * Create an instance of {@link PaystationBillingInfoResponse }
     * 
     */
    public PaystationBillingInfoResponse createPaystationBillingInfoResponse() {
        return new PaystationBillingInfoResponse();
    }

    /**
     * Create an instance of {@link PaystationBillingInfoType }
     * 
     */
    public PaystationBillingInfoType createPaystationBillingInfoType() {
        return new PaystationBillingInfoType();
    }

    /**
     * Create an instance of {@link PaystationEventHistoryResponse }
     * 
     */
    public PaystationEventHistoryResponse createPaystationEventHistoryResponse() {
        return new PaystationEventHistoryResponse();
    }

    /**
     * Create an instance of {@link PaystationEventHistoryDetailType }
     * 
     */
    public PaystationEventHistoryDetailType createPaystationEventHistoryDetailType() {
        return new PaystationEventHistoryDetailType();
    }

    /**
     * Create an instance of {@link PaystationEventResponse }
     * 
     */
    public PaystationEventResponse createPaystationEventResponse() {
        return new PaystationEventResponse();
    }

    /**
     * Create an instance of {@link PaystationEventDetailType }
     * 
     */
    public PaystationEventDetailType createPaystationEventDetailType() {
        return new PaystationEventDetailType();
    }

    /**
     * Create an instance of {@link PaystationSensorDataResponse }
     * 
     */
    public PaystationSensorDataResponse createPaystationSensorDataResponse() {
        return new PaystationSensorDataResponse();
    }

    /**
     * Create an instance of {@link PaystationSensorDataDetailType }
     * 
     */
    public PaystationSensorDataDetailType createPaystationSensorDataDetailType() {
        return new PaystationSensorDataDetailType();
    }

    /**
     * Create an instance of {@link PaystationSoftwareResponse }
     * 
     */
    public PaystationSoftwareResponse createPaystationSoftwareResponse() {
        return new PaystationSoftwareResponse();
    }

    /**
     * Create an instance of {@link PaystationSoftwareDetailType }
     * 
     */
    public PaystationSoftwareDetailType createPaystationSoftwareDetailType() {
        return new PaystationSoftwareDetailType();
    }

    /**
     * Create an instance of {@link ModemSettingType }
     * 
     */
    public ModemSettingType createModemSettingType() {
        return new ModemSettingType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.dpt.com/provision", name = "UpdatePaystationResponse")
    public JAXBElement<Boolean> createUpdatePaystationResponse(Boolean value) {
        return new JAXBElement<Boolean>(_UpdatePaystationResponse_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.dpt.com/provision", name = "UpdateCustomerResponse")
    public JAXBElement<Integer> createUpdateCustomerResponse(Integer value) {
        return new JAXBElement<Integer>(_UpdateCustomerResponse_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.dpt.com/provision", name = "DeletePaystationResponse")
    public JAXBElement<Boolean> createDeletePaystationResponse(Boolean value) {
        return new JAXBElement<Boolean>(_DeletePaystationResponse_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.dpt.com/provision", name = "GetCustomerAgreementSignedResponse")
    public JAXBElement<Boolean> createGetCustomerAgreementSignedResponse(Boolean value) {
        return new JAXBElement<Boolean>(_GetCustomerAgreementSignedResponse_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.dpt.com/provision", name = "CreatePaystationResponse")
    public JAXBElement<Integer> createCreatePaystationResponse(Integer value) {
        return new JAXBElement<Integer>(_CreatePaystationResponse_QNAME, Integer.class, null, value);
    }

}
