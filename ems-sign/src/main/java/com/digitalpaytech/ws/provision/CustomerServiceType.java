
package com.digitalpaytech.ws.provision;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerServiceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerServiceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="reportEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="textAlertEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ccProcessingEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="payByCellEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="couponsEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="valueCardsEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="webServicesEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="payByPhoneEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerServiceType", propOrder = {
    "reportEnabled",
    "textAlertEnabled",
    "ccProcessingEnabled",
    "payByCellEnabled",
    "couponsEnabled",
    "valueCardsEnabled",
    "webServicesEnabled",
    "payByPhoneEnabled"
})
public class CustomerServiceType {

    protected boolean reportEnabled;
    protected boolean textAlertEnabled;
    protected boolean ccProcessingEnabled;
    protected boolean payByCellEnabled;
    protected boolean couponsEnabled;
    protected boolean valueCardsEnabled;
    protected boolean webServicesEnabled;
    protected boolean payByPhoneEnabled;

    /**
     * Gets the value of the reportEnabled property.
     * 
     */
    public boolean isReportEnabled() {
        return reportEnabled;
    }

    /**
     * Sets the value of the reportEnabled property.
     * 
     */
    public void setReportEnabled(boolean value) {
        this.reportEnabled = value;
    }

    /**
     * Gets the value of the textAlertEnabled property.
     * 
     */
    public boolean isTextAlertEnabled() {
        return textAlertEnabled;
    }

    /**
     * Sets the value of the textAlertEnabled property.
     * 
     */
    public void setTextAlertEnabled(boolean value) {
        this.textAlertEnabled = value;
    }

    /**
     * Gets the value of the ccProcessingEnabled property.
     * 
     */
    public boolean isCcProcessingEnabled() {
        return ccProcessingEnabled;
    }

    /**
     * Sets the value of the ccProcessingEnabled property.
     * 
     */
    public void setCcProcessingEnabled(boolean value) {
        this.ccProcessingEnabled = value;
    }

    /**
     * Gets the value of the payByCellEnabled property.
     * 
     */
    public boolean isPayByCellEnabled() {
        return payByCellEnabled;
    }

    /**
     * Sets the value of the payByCellEnabled property.
     * 
     */
    public void setPayByCellEnabled(boolean value) {
        this.payByCellEnabled = value;
    }

    /**
     * Gets the value of the couponsEnabled property.
     * 
     */
    public boolean isCouponsEnabled() {
        return couponsEnabled;
    }

    /**
     * Sets the value of the couponsEnabled property.
     * 
     */
    public void setCouponsEnabled(boolean value) {
        this.couponsEnabled = value;
    }

    /**
     * Gets the value of the valueCardsEnabled property.
     * 
     */
    public boolean isValueCardsEnabled() {
        return valueCardsEnabled;
    }

    /**
     * Sets the value of the valueCardsEnabled property.
     * 
     */
    public void setValueCardsEnabled(boolean value) {
        this.valueCardsEnabled = value;
    }

    /**
     * Gets the value of the webServicesEnabled property.
     * 
     */
    public boolean isWebServicesEnabled() {
        return webServicesEnabled;
    }

    /**
     * Sets the value of the webServicesEnabled property.
     * 
     */
    public void setWebServicesEnabled(boolean value) {
        this.webServicesEnabled = value;
    }

    /**
     * Gets the value of the payByPhoneEnabled property.
     * 
     */
    public boolean isPayByPhoneEnabled() {
        return payByPhoneEnabled;
    }

    /**
     * Sets the value of the payByPhoneEnabled property.
     * 
     */
    public void setPayByPhoneEnabled(boolean value) {
        this.payByPhoneEnabled = value;
    }

}
