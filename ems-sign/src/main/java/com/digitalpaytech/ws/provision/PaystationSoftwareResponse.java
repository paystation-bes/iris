
package com.digitalpaytech.ws.provision;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="paystationSoftwareDetail" type="{http://ws.dpt.com/provision}PaystationSoftwareDetailType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "paystationSoftwareDetail"
})
@XmlRootElement(name = "PaystationSoftwareResponse")
public class PaystationSoftwareResponse {

    @XmlElement(required = true)
    protected PaystationSoftwareDetailType paystationSoftwareDetail;

    /**
     * Gets the value of the paystationSoftwareDetail property.
     * 
     * @return
     *     possible object is
     *     {@link PaystationSoftwareDetailType }
     *     
     */
    public PaystationSoftwareDetailType getPaystationSoftwareDetail() {
        return paystationSoftwareDetail;
    }

    /**
     * Sets the value of the paystationSoftwareDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaystationSoftwareDetailType }
     *     
     */
    public void setPaystationSoftwareDetail(PaystationSoftwareDetailType value) {
        this.paystationSoftwareDetail = value;
    }

}
