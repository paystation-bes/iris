
package com.digitalpaytech.ws.provision;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaystationSoftwareDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaystationSoftwareDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="paystatonName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="softwareVersion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pcmVersion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="firmwareVersion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="bspVersion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="region" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="setting" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="groups" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="merchantAccount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="customCardMerchantAccount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="parkingDataSharedBy" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaystationSoftwareDetailType", propOrder = {
    "paystatonName",
    "softwareVersion",
    "pcmVersion",
    "firmwareVersion",
    "bspVersion",
    "region",
    "setting",
    "groups",
    "merchantAccount",
    "customCardMerchantAccount",
    "parkingDataSharedBy"
})
public class PaystationSoftwareDetailType {

    @XmlElement(required = true)
    protected String paystatonName;
    @XmlElement(required = true)
    protected String softwareVersion;
    @XmlElement(required = true)
    protected String pcmVersion;
    @XmlElement(required = true)
    protected String firmwareVersion;
    @XmlElement(required = true)
    protected String bspVersion;
    @XmlElement(required = true)
    protected String region;
    @XmlElement(required = true)
    protected String setting;
    protected List<String> groups;
    @XmlElement(required = true)
    protected String merchantAccount;
    @XmlElement(required = true)
    protected String customCardMerchantAccount;
    @XmlElement(required = true)
    protected String parkingDataSharedBy;

    /**
     * Gets the value of the paystatonName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaystatonName() {
        return paystatonName;
    }

    /**
     * Sets the value of the paystatonName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaystatonName(String value) {
        this.paystatonName = value;
    }

    /**
     * Gets the value of the softwareVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoftwareVersion() {
        return softwareVersion;
    }

    /**
     * Sets the value of the softwareVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoftwareVersion(String value) {
        this.softwareVersion = value;
    }

    /**
     * Gets the value of the pcmVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPcmVersion() {
        return pcmVersion;
    }

    /**
     * Sets the value of the pcmVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPcmVersion(String value) {
        this.pcmVersion = value;
    }

    /**
     * Gets the value of the firmwareVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    /**
     * Sets the value of the firmwareVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirmwareVersion(String value) {
        this.firmwareVersion = value;
    }

    /**
     * Gets the value of the bspVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBspVersion() {
        return bspVersion;
    }

    /**
     * Sets the value of the bspVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBspVersion(String value) {
        this.bspVersion = value;
    }

    /**
     * Gets the value of the region property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegion() {
        return region;
    }

    /**
     * Sets the value of the region property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegion(String value) {
        this.region = value;
    }

    /**
     * Gets the value of the setting property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSetting() {
        return setting;
    }

    /**
     * Sets the value of the setting property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSetting(String value) {
        this.setting = value;
    }

    /**
     * Gets the value of the groups property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the groups property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroups().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getGroups() {
        if (groups == null) {
            groups = new ArrayList<String>();
        }
        return this.groups;
    }

    /**
     * Gets the value of the merchantAccount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMerchantAccount() {
        return merchantAccount;
    }

    /**
     * Sets the value of the merchantAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMerchantAccount(String value) {
        this.merchantAccount = value;
    }

    /**
     * Gets the value of the customCardMerchantAccount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomCardMerchantAccount() {
        return customCardMerchantAccount;
    }

    /**
     * Sets the value of the customCardMerchantAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomCardMerchantAccount(String value) {
        this.customCardMerchantAccount = value;
    }

    /**
     * Gets the value of the parkingDataSharedBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParkingDataSharedBy() {
        return parkingDataSharedBy;
    }

    /**
     * Sets the value of the parkingDataSharedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParkingDataSharedBy(String value) {
        this.parkingDataSharedBy = value;
    }

}
