
package com.digitalpaytech.ws.provision;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ServiceAgreementType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ServiceAgreementType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NCName">
 *     &lt;enumeration value="Electronic"/>
 *     &lt;enumeration value="Paper"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ServiceAgreementType")
@XmlEnum
public enum ServiceAgreementType {

    @XmlEnumValue("Electronic")
    ELECTRONIC("Electronic"),
    @XmlEnumValue("Paper")
    PAPER("Paper");
    private final String value;

    ServiceAgreementType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ServiceAgreementType fromValue(String v) {
        for (ServiceAgreementType c: ServiceAgreementType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
