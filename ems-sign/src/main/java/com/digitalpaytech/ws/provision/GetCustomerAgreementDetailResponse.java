
package com.digitalpaytech.ws.provision;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="customerAgreementDetail" type="{http://ws.dpt.com/provision}CustomerAgreementDetailType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "customerAgreementDetail"
})
@XmlRootElement(name = "GetCustomerAgreementDetailResponse")
public class GetCustomerAgreementDetailResponse {

    protected CustomerAgreementDetailType customerAgreementDetail;

    /**
     * Gets the value of the customerAgreementDetail property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerAgreementDetailType }
     *     
     */
    public CustomerAgreementDetailType getCustomerAgreementDetail() {
        return customerAgreementDetail;
    }

    /**
     * Sets the value of the customerAgreementDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerAgreementDetailType }
     *     
     */
    public void setCustomerAgreementDetail(CustomerAgreementDetailType value) {
        this.customerAgreementDetail = value;
    }

}
