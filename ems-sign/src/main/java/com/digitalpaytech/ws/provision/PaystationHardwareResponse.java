
package com.digitalpaytech.ws.provision;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="paystationHardwareDetail" type="{http://ws.dpt.com/provision}PaystationHardwareDetailType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "paystationHardwareDetail"
})
@XmlRootElement(name = "PaystationHardwareResponse")
public class PaystationHardwareResponse {

    @XmlElement(required = true)
    protected PaystationHardwareDetailType paystationHardwareDetail;

    /**
     * Gets the value of the paystationHardwareDetail property.
     * 
     * @return
     *     possible object is
     *     {@link PaystationHardwareDetailType }
     *     
     */
    public PaystationHardwareDetailType getPaystationHardwareDetail() {
        return paystationHardwareDetail;
    }

    /**
     * Sets the value of the paystationHardwareDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaystationHardwareDetailType }
     *     
     */
    public void setPaystationHardwareDetail(PaystationHardwareDetailType value) {
        this.paystationHardwareDetail = value;
    }

}
