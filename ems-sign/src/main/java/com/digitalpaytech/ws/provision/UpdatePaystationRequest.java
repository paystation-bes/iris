
package com.digitalpaytech.ws.provision;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userName" type="{http://www.w3.org/2001/XMLSchema}token"/>
 *         &lt;element name="token" type="{http://www.w3.org/2001/XMLSchema}token"/>
 *         &lt;element name="oldSerialNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="newSerialNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="emsCustomerId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="signature" type="{http://www.w3.org/2001/XMLSchema}token"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userName",
    "token",
    "oldSerialNumber",
    "newSerialNumber",
    "emsCustomerId",
    "signature"
})
@XmlRootElement(name = "UpdatePaystationRequest")
public class UpdatePaystationRequest {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userName;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String token;
    @XmlElement(required = true)
    protected String oldSerialNumber;
    @XmlElement(required = true)
    protected String newSerialNumber;
    protected int emsCustomerId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String signature;

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Gets the value of the oldSerialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOldSerialNumber() {
        return oldSerialNumber;
    }

    /**
     * Sets the value of the oldSerialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOldSerialNumber(String value) {
        this.oldSerialNumber = value;
    }

    /**
     * Gets the value of the newSerialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewSerialNumber() {
        return newSerialNumber;
    }

    /**
     * Sets the value of the newSerialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewSerialNumber(String value) {
        this.newSerialNumber = value;
    }

    /**
     * Gets the value of the emsCustomerId property.
     * 
     */
    public int getEmsCustomerId() {
        return emsCustomerId;
    }

    /**
     * Sets the value of the emsCustomerId property.
     * 
     */
    public void setEmsCustomerId(int value) {
        this.emsCustomerId = value;
    }

    /**
     * Gets the value of the signature property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSignature() {
        return signature;
    }

    /**
     * Sets the value of the signature property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSignature(String value) {
        this.signature = value;
    }

}
