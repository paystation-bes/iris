
package com.digitalpaytech.ws.provision;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="paystationBillingInfo" type="{http://ws.dpt.com/provision}PaystationBillingInfoType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "paystationBillingInfo"
})
@XmlRootElement(name = "PaystationBillingInfoResponse")
public class PaystationBillingInfoResponse {

    @XmlElement(required = true)
    protected PaystationBillingInfoType paystationBillingInfo;

    /**
     * Gets the value of the paystationBillingInfo property.
     * 
     * @return
     *     possible object is
     *     {@link PaystationBillingInfoType }
     *     
     */
    public PaystationBillingInfoType getPaystationBillingInfo() {
        return paystationBillingInfo;
    }

    /**
     * Sets the value of the paystationBillingInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaystationBillingInfoType }
     *     
     */
    public void setPaystationBillingInfo(PaystationBillingInfoType value) {
        this.paystationBillingInfo = value;
    }

}
