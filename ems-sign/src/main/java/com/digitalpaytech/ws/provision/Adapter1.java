
package com.digitalpaytech.ws.provision;

import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class Adapter1
    extends XmlAdapter<String, Date>
{


    public Date unmarshal(String value) {
        return (com.digitalpaytech.utils.DateTimeAdapter.parseDateTime(value));
    }

    public String marshal(Date value) {
        return (com.digitalpaytech.utils.DateTimeAdapter.printDateTime(value));
    }

}
