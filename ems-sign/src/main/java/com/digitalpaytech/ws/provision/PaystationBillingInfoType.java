
package com.digitalpaytech.ws.provision;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaystationBillingInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaystationBillingInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="initialActivity" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="latestActivity" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaystationBillingInfoType", propOrder = {
    "name",
    "initialActivity",
    "latestActivity"
})
public class PaystationBillingInfoType {

    @XmlElement(required = true)
    protected String name;
    protected long initialActivity;
    protected long latestActivity;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the initialActivity property.
     * 
     */
    public long getInitialActivity() {
        return initialActivity;
    }

    /**
     * Sets the value of the initialActivity property.
     * 
     */
    public void setInitialActivity(long value) {
        this.initialActivity = value;
    }

    /**
     * Gets the value of the latestActivity property.
     * 
     */
    public long getLatestActivity() {
        return latestActivity;
    }

    /**
     * Sets the value of the latestActivity property.
     * 
     */
    public void setLatestActivity(long value) {
        this.latestActivity = value;
    }

}
