
package com.digitalpaytech.ws.provision;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaystationHardwareDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaystationHardwareDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="serialNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="emsCustomerId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="modules" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="activationDate" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="lastSeen" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="timeZone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="modemSetting" type="{http://ws.dpt.com/provision}ModemSettingType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaystationHardwareDetailType", propOrder = {
    "serialNumber",
    "emsCustomerId",
    "modules",
    "activationDate",
    "lastSeen",
    "timeZone",
    "modemSetting"
})
public class PaystationHardwareDetailType {

    @XmlElement(required = true)
    protected String serialNumber;
    protected int emsCustomerId;
    protected List<String> modules;
    protected long activationDate;
    protected long lastSeen;
    @XmlElement(required = true)
    protected String timeZone;
    protected ModemSettingType modemSetting;

    /**
     * Gets the value of the serialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * Sets the value of the serialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerialNumber(String value) {
        this.serialNumber = value;
    }

    /**
     * Gets the value of the emsCustomerId property.
     * 
     */
    public int getEmsCustomerId() {
        return emsCustomerId;
    }

    /**
     * Sets the value of the emsCustomerId property.
     * 
     */
    public void setEmsCustomerId(int value) {
        this.emsCustomerId = value;
    }

    /**
     * Gets the value of the modules property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the modules property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getModules().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getModules() {
        if (modules == null) {
            modules = new ArrayList<String>();
        }
        return this.modules;
    }

    /**
     * Gets the value of the activationDate property.
     * 
     */
    public long getActivationDate() {
        return activationDate;
    }

    /**
     * Sets the value of the activationDate property.
     * 
     */
    public void setActivationDate(long value) {
        this.activationDate = value;
    }

    /**
     * Gets the value of the lastSeen property.
     * 
     */
    public long getLastSeen() {
        return lastSeen;
    }

    /**
     * Sets the value of the lastSeen property.
     * 
     */
    public void setLastSeen(long value) {
        this.lastSeen = value;
    }

    /**
     * Gets the value of the timeZone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeZone() {
        return timeZone;
    }

    /**
     * Sets the value of the timeZone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeZone(String value) {
        this.timeZone = value;
    }

    /**
     * Gets the value of the modemSetting property.
     * 
     * @return
     *     possible object is
     *     {@link ModemSettingType }
     *     
     */
    public ModemSettingType getModemSetting() {
        return modemSetting;
    }

    /**
     * Sets the value of the modemSetting property.
     * 
     * @param value
     *     allowed object is
     *     {@link ModemSettingType }
     *     
     */
    public void setModemSetting(ModemSettingType value) {
        this.modemSetting = value;
    }

}
