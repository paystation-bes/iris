/* 
 * Copyright (c) 1997 - 2009  DPT  All rights reserved.
 *
 * Created on Feb 20, 2009 by rexz
 * 
 * $Header:  $
 * 
 * $Log:  $ 
 *
 */
package com.digitalpaytech.ws.adapter;

import java.util.HashMap;
import java.util.Map;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.common.gzip.GZIPInInterceptor;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.handler.WSHandlerConstants;

import com.digitalpaytech.ws.handler.ClientPasswordHandler;
import com.digitalpaytech.ws.provision.ProvisionService;
import com.digitalpaytech.ws.signingservice.SigningService;
import com.digitalpaytech.ws.signingservicev2.SigningServiceV2;

/**
 * A Adaptor to retrieve EMS/Client Web Serivce
 * 
 * @author rexz
 * @since
 * 
 */
public class WSAdapter {
    
    private String userName = "";
    private String password = "";
    
    private String serviceLocation = "";
    
    public WSAdapter() {
    }
    
    public String getUserName() {
        return userName;
    }
    
    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getServiceLocation() {
        return serviceLocation;
    }
    
    public void setServiceLocation(String serviceLocation) {
        this.serviceLocation = serviceLocation;
    }
    
    protected void configureOutProperties(Map outProps) {
        outProps.put(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN);
        // Specify our username
        outProps.put(WSHandlerConstants.USER, userName);
        // Password type : plain text
        outProps.put(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT);
        ClientPasswordHandler psHandler = new ClientPasswordHandler();
        psHandler.setPassword(password);
        outProps.put(WSHandlerConstants.PW_CALLBACK_REF, psHandler);
    }
    
    public SigningService getSigningService() {
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(SigningService.class);
        factory.setAddress(serviceLocation);
        SigningService service = (SigningService) factory.create();
        Client client = ClientProxy.getClient(service);
        Endpoint cxfEndpoint = client.getEndpoint();
        Map outProps = new HashMap();
        WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor(outProps);
        cxfEndpoint.getOutInterceptors().add(wssOut);
        configureOutProperties(outProps);
        return service;
    }
    
    public SigningServiceV2 getSigningServiceV2() {
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(SigningServiceV2.class);
        factory.setAddress(serviceLocation);
        SigningServiceV2 service = (SigningServiceV2) factory.create();
        Client client = ClientProxy.getClient(service);
        Endpoint cxfEndpoint = client.getEndpoint();
        Map outProps = new HashMap();
        WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor(outProps);
        cxfEndpoint.getOutInterceptors().add(wssOut);
        configureOutProperties(outProps);
        return service;
    }
    
    //	public CerberusService getCerberusService()
    //	{
    //		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
    //		factory.setServiceClass(CerberusService.class);
    //		factory.setAddress(serviceLocation);
    //		CerberusService service = (CerberusService)factory.create();
    //		Client client = ClientProxy.getClient(service);
    //		Endpoint cxfEndpoint = client.getEndpoint();
    //		Map outProps = new HashMap();
    //		WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor(outProps);
    //		cxfEndpoint.getOutInterceptors().add(wssOut);
    //		configureOutProperties(outProps);
    //		return service;
    //	}
    //	
    //	public LicenseService getLicenseService()
    //	{
    //		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
    //		factory.setServiceClass(LicenseService.class);
    //		factory.setAddress(serviceLocation);
    //		LicenseService service = (LicenseService)factory.create();
    //		Client client = ClientProxy.getClient(service);
    //		Endpoint cxfEndpoint = client.getEndpoint();
    //		Map outProps = new HashMap();
    //		WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor(outProps);
    //		cxfEndpoint.getOutInterceptors().add(wssOut);
    //		configureOutProperties(outProps);
    //		Map inProps = new HashMap();
    //		GZIPInInterceptor gzipIn = new GZIPInInterceptor();
    //		cxfEndpoint.getInInterceptors().add(gzipIn);
    //		return service;
    //	}
    
    public ProvisionService getProvisionService() {
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(ProvisionService.class);
        factory.setAddress(serviceLocation);
        ProvisionService service = (ProvisionService) factory.create();
        Client client = ClientProxy.getClient(service);
        Endpoint cxfEndpoint = client.getEndpoint();
        Map outProps = new HashMap();
        WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor(outProps);
        cxfEndpoint.getOutInterceptors().add(wssOut);
        configureOutProperties(outProps);
        Map inProps = new HashMap();
        GZIPInInterceptor gzipIn = new GZIPInInterceptor();
        cxfEndpoint.getInInterceptors().add(gzipIn);
        return service;
    }
}
