package com.digitalpaytech.ws.handler;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.ws.security.WSPasswordCallback;

public class ClientPasswordHandler implements CallbackHandler
{

	private String password = " ";

	public ClientPasswordHandler()
	{
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException
	{
		WSPasswordCallback pc = (WSPasswordCallback)callbacks[0];
		pc.setPassword(password);
	}
}
