
/*
 * 
 */

package com.digitalpaytech.ws.signingservice;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;

/**
 * This class was generated by Apache CXF 2.1.3
 * Wed Feb 08 15:07:24 PST 2012
 * Generated source version: 2.1.3
 * 
 */


@WebServiceClient(name = "SigningService", 
                  wsdlLocation = "file:/C:/Projects/DPTWSFactory_trunk/src/wsdl/signingService.wsdl",
                  targetNamespace = "http://ws.digitalpioneer.com/SigningService/") 
public class SigningService_Service extends Service {

    public final static URL WSDL_LOCATION;
    public final static QName SERVICE = new QName("http://ws.digitalpioneer.com/SigningService/", "SigningService");
    public final static QName SigningServicehttpPort = new QName("http://ws.digitalpioneer.com/SigningService/", "SigningServicehttpPort");
    static {
        URL url = null;
        try {
            url = new URL("file:/C:/Projects/DPTWSFactory_trunk/src/wsdl/signingService.wsdl");
        } catch (MalformedURLException e) {
            System.err.println("Can not initialize the default wsdl from file:/C:/Projects/DPTWSFactory_trunk/src/wsdl/signingService.wsdl");
            // e.printStackTrace();
        }
        WSDL_LOCATION = url;
    }

    public SigningService_Service(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public SigningService_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public SigningService_Service() {
        super(WSDL_LOCATION, SERVICE);
    }

    /**
     * 
     * @return
     *     returns SigningService
     */
    @WebEndpoint(name = "SigningServicehttpPort")
    public SigningService getSigningServicehttpPort() {
        return super.getPort(SigningServicehttpPort, SigningService.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns SigningService
     */
    @WebEndpoint(name = "SigningServicehttpPort")
    public SigningService getSigningServicehttpPort(WebServiceFeature... features) {
        return super.getPort(SigningServicehttpPort, SigningService.class, features);
    }

}
