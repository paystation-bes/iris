package com.digitalpaytech.ws.signingservicev2;

import org.junit.Test;
import static org.junit.Assert.*;

public class SigningServiceV2ImplTest {
    @Test
    public void testSignatureAlgorithms() {
        assertEquals(5, SigningServiceV2Impl.SIGNATURE_ALGORITHMS.length);
        for (int i = 0; i < 3; i++) {
            assertNull(SigningServiceV2Impl.SIGNATURE_ALGORITHMS[i]);
        }
        assertEquals("SHA-1", SigningServiceV2Impl.SIGNATURE_ALGORITHMS[3]);
        assertEquals("SHA-256", SigningServiceV2Impl.SIGNATURE_ALGORITHMS[4]);
    }
}
