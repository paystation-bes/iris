package com.digitalpaytech.signer.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.server.signingservicev2.Signer;
import com.digitalpaytech.signer.signingservicev2.BouncyCastleSigner;

public class BouncyCastleSignerTest {
    
    protected static final String KEY_ALIAS = "DPTSIGNINGKEY";
    
    private static final String TEST_KEY_STORE_FILE = "src/test/resources/dptsigningtest.keystore";
    private static final String TEST_ZIP_FILE = "src/test/resources/original/Sample Zip File.zip";
    private static final String TEST_ZIP_FILE_TAMPERED = "src/test/resources/tampered/Sample Zip File.zip";
    private static final String SHA1WITHRSA = "SHA1WithRSA";
    private static final String SHA256WITHRSA = "SHA256WithRSA";
    private static final String RSA = "RSA";
    private static final int SIXTEEN = 16;
    private static final String DPT_TEST_SIGN_STRING = "This is DPT sign data test";
    private static final String DPT_TEST_SIGN_STRING_TAMPERED = "This is not DPT sign data test";
    
    protected char[] keyPassword = "testpassword".toCharArray();
    protected char[] keyStorePassword = "l;kja*dfj@aRsBXkqePr?9xCv[9ewq.8ds(UjOsafW2I1sdfZsdfk]laG".toCharArray();
    
    protected List<String> signAlgorithms;
    
    @org.junit.Before
    public final void setupBouncyCastleProvider() {
        Security.insertProviderAt(new org.bouncycastle.jce.provider.BouncyCastleProvider(), 1);
        this.signAlgorithms = new ArrayList<String>();
        this.signAlgorithms.add(SHA1WITHRSA);
        this.signAlgorithms.add(SHA256WITHRSA);
    }
    
    public final PublicKey getPublicKey(final BigInteger modulus, final BigInteger exponent) {
        final RSAPublicKeySpec spec = new RSAPublicKeySpec(modulus, exponent);
        KeyFactory factory = null;
        try {
            factory = KeyFactory.getInstance(RSA);
        } catch (NoSuchAlgorithmException e) {
            fail(e.getMessage());
        }
        PublicKey pub = null;
        try {
            pub = factory.generatePublic(spec);
        } catch (InvalidKeySpecException e) {
            fail(e.getMessage());
        }
        
        return pub;
    }
    
    private boolean verifySig(final String algoName, final PublicKey pub, final byte[] testSignData, final byte[] signedData) {
        
        Signature verifier = null;
        try {
            verifier = Signature.getInstance(algoName);
        } catch (NoSuchAlgorithmException e) {
            fail(e.getMessage());
        }
        try {
            verifier.initVerify(pub);
        } catch (InvalidKeyException e) {
            fail(e.getMessage());
        }
        try {
            verifier.update(testSignData);
        } catch (SignatureException e) {
            fail(e.getMessage());
        }
        boolean okay = false;
        try {
            okay = verifier.verify(signedData);
        } catch (SignatureException e) {
            fail(e.getMessage());
        }
        
        return okay;
    }
    
    @Test
    public final void testSHA1WITHRSAHash() {
        BigInteger modulus = null;
        BigInteger exponent = null;
        byte[] signedData = null;
        
        try {
            final Signer bouncyCastleSigner = new BouncyCastleSigner(TEST_KEY_STORE_FILE, SHA1WITHRSA, this.signAlgorithms);
            final String[] publicKey = bouncyCastleSigner.getNonEncodedPublicKey();
            signedData = bouncyCastleSigner.signData(DPT_TEST_SIGN_STRING.getBytes());
            
            modulus = new BigInteger(publicKey[0], SIXTEEN);
            exponent = new BigInteger(publicKey[1], SIXTEEN);
            
        } catch (CryptoException e) {
            fail(e.getMessage());
        }
        assertNotNull(signedData);
        
        final PublicKey pub = getPublicKey(modulus, exponent);
        final boolean result = verifySig(SHA1WITHRSA, pub, DPT_TEST_SIGN_STRING.getBytes(), signedData);
        assertTrue(result);
        
    }
    
    @Test
    public final void testSHA1WITHRSAHashTampered() {
        
        BigInteger modulus = null;
        BigInteger exponent = null;
        byte[] signedData = null;
        
        try {
            final Signer bouncyCastleSigner = new BouncyCastleSigner(TEST_KEY_STORE_FILE, SHA1WITHRSA, this.signAlgorithms);
            final String[] publicKey = bouncyCastleSigner.getNonEncodedPublicKey();
            signedData = bouncyCastleSigner.signData(DPT_TEST_SIGN_STRING.getBytes());
            
            modulus = new BigInteger(publicKey[0], SIXTEEN);
            exponent = new BigInteger(publicKey[1], SIXTEEN);
            
        } catch (CryptoException e) {
            fail(e.getMessage());
            
        }
        assertNotNull(signedData);
        // testSignData tampered
        
        final PublicKey pub = getPublicKey(modulus, exponent);
        final boolean result = verifySig(SHA1WITHRSA, pub, DPT_TEST_SIGN_STRING_TAMPERED.getBytes(), signedData);
        
        assertFalse(result);
        
    }
    
    @Test
    public final void testSHA256WITHRSAHash() {
        
        BigInteger modulus = null;
        BigInteger exponent = null;
        byte[] signedData = null;
        
        try {
            final Signer bouncyCastleSigner = new BouncyCastleSigner(TEST_KEY_STORE_FILE, SHA256WITHRSA, this.signAlgorithms);
            final String[] publicKey = bouncyCastleSigner.getNonEncodedPublicKey();
            signedData = bouncyCastleSigner.signData(DPT_TEST_SIGN_STRING.getBytes());
            modulus = new BigInteger(publicKey[0], SIXTEEN);
            exponent = new BigInteger(publicKey[1], SIXTEEN);
        } catch (CryptoException e) {
            fail(e.getMessage());
        }
        assertNotNull(signedData);
        
        final PublicKey pub = getPublicKey(modulus, exponent);
        final boolean result = verifySig(SHA256WITHRSA, pub, DPT_TEST_SIGN_STRING.getBytes(), signedData);
        
        assertTrue(result);
        
    }
    
    @Test
    public final void testSHA256WITHRSAHashTampered() {
        
        BigInteger modulus = null;
        BigInteger exponent = null;
        byte[] signedData = null;
        
        try {
            final Signer bouncyCastleSigner = new BouncyCastleSigner(TEST_KEY_STORE_FILE, SHA256WITHRSA, this.signAlgorithms);
            final String[] publicKey = bouncyCastleSigner.getNonEncodedPublicKey();
            signedData = bouncyCastleSigner.signData(DPT_TEST_SIGN_STRING.getBytes());
            
            modulus = new BigInteger(publicKey[0], SIXTEEN);
            exponent = new BigInteger(publicKey[1], SIXTEEN);
            
        } catch (CryptoException e) {
            fail(e.getMessage());
        }
        assertNotNull(signedData);
        
        final PublicKey pub = getPublicKey(modulus, exponent);
        final boolean result = verifySig(SHA256WITHRSA, pub, DPT_TEST_SIGN_STRING_TAMPERED.getBytes(), signedData);
        
        assertFalse(result);
        
    }
    
    @Test
    public final void testSignFileVerifyUsingSHA256WITHRSA() {
        
        BigInteger modulus = null;
        BigInteger exponent = null;
        byte[] signedData = null;
        File file = null;
        FileInputStream fileInputStream = null;
        
        file = new File(TEST_ZIP_FILE);
        final byte[] bFile = new byte[(int) file.length()];
        try {
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bFile);
        } catch (FileNotFoundException e) {
            fail(e.getMessage());
        } catch (IOException e) {
            fail(e.getMessage());
        }
        
        try {
            final Signer bouncyCastleSigner = new BouncyCastleSigner(TEST_KEY_STORE_FILE, SHA256WITHRSA, this.signAlgorithms);
            final String[] publicKey = bouncyCastleSigner.getNonEncodedPublicKey();
            signedData = bouncyCastleSigner.signData(bFile);
            
            modulus = new BigInteger(publicKey[0], SIXTEEN);
            exponent = new BigInteger(publicKey[1], SIXTEEN);
            
        } catch (CryptoException e) {
            fail(e.getMessage());
        }
        assertNotNull(signedData);
        
        final PublicKey pub = getPublicKey(modulus, exponent);
        final boolean result = verifySig(SHA256WITHRSA, pub, bFile, signedData);
        
        assertTrue(result);
        
    }
    
    @Test
    public final void testSignFileTamperedVerifyUsingSHA256WITHRSA() {
        
        BigInteger modulus = null;
        BigInteger exponent = null;
        byte[] signedData = null;
        File file = null;
        FileInputStream fileInputStream = null;
        
        file = new File(TEST_ZIP_FILE);
        byte[] bFile = new byte[(int) file.length()];
        try {
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bFile);
        } catch (FileNotFoundException e) {
            fail(e.getMessage());
        } catch (IOException e) {
            fail(e.getMessage());
        }
        
        try {
            final Signer bouncyCastleSigner = new BouncyCastleSigner(TEST_KEY_STORE_FILE, SHA256WITHRSA, this.signAlgorithms);
            final String[] publicKey = bouncyCastleSigner.getNonEncodedPublicKey();
            signedData = bouncyCastleSigner.signData(bFile);
            
            modulus = new BigInteger(publicKey[0], SIXTEEN);
            exponent = new BigInteger(publicKey[1], SIXTEEN);
            
        } catch (CryptoException e) {
            fail(e.getMessage());
        }
        assertNotNull(signedData);
        
        file = new File(TEST_ZIP_FILE_TAMPERED);
        bFile = new byte[(int) file.length()];
        try {
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bFile);
        } catch (FileNotFoundException e) {
            fail(e.getMessage());
        } catch (IOException e) {
            fail(e.getMessage());
        }
        final PublicKey pub = getPublicKey(modulus, exponent);
        final boolean result = verifySig(SHA256WITHRSA, pub, bFile, signedData);
        
        assertFalse(result);
        
    }
    
    @Test
    public final void testSignFileTamperedVerifyUsingSHA1WITHRSA() {
        
        BigInteger modulus = null;
        BigInteger exponent = null;
        byte[] signedData = null;
        File file = null;
        FileInputStream fileInputStream = null;
        
        file = new File(TEST_ZIP_FILE);
        byte[] bFile = new byte[(int) file.length()];
        try {
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bFile);
        } catch (FileNotFoundException e) {
            fail(e.getMessage());
        } catch (IOException e) {
            fail(e.getMessage());
        }
        
        try {
            final Signer bouncyCastleSigner = new BouncyCastleSigner(TEST_KEY_STORE_FILE, SHA1WITHRSA, this.signAlgorithms);
            final String[] publicKey = bouncyCastleSigner.getNonEncodedPublicKey();
            signedData = bouncyCastleSigner.signData(bFile);
            
            modulus = new BigInteger(publicKey[0], SIXTEEN);
            exponent = new BigInteger(publicKey[1], SIXTEEN);
            
        } catch (CryptoException e) {
            fail(e.getMessage());
        }
        assertNotNull(signedData);
        
        file = new File(TEST_ZIP_FILE_TAMPERED);
        bFile = new byte[(int) file.length()];
        try {
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bFile);
        } catch (FileNotFoundException e) {
            fail(e.getMessage());
        } catch (IOException e) {
            fail(e.getMessage());
        }
        
        final PublicKey pub = getPublicKey(modulus, exponent);
        final boolean result = verifySig(SHA1WITHRSA, pub, bFile, signedData);
        
        assertFalse(result);
        
    }
    
    @Test
    public final void testSignFileVerifyUsingSHA1WITHRSA() {
        
        BigInteger modulus = null;
        BigInteger exponent = null;
        byte[] signedData = null;
        File file = null;
        FileInputStream fileInputStream = null;
        
        file = new File(TEST_ZIP_FILE);
        final byte[] bFile = new byte[(int) file.length()];
        try {
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bFile);
        } catch (FileNotFoundException e) {
            fail(e.getMessage());
        } catch (IOException e) {
            fail(e.getMessage());
        }
        
        try {
            final Signer bouncyCastleSigner = new BouncyCastleSigner(TEST_KEY_STORE_FILE, SHA1WITHRSA, this.signAlgorithms);
            final String[] publicKey = bouncyCastleSigner.getNonEncodedPublicKey();
            signedData = bouncyCastleSigner.signData(bFile);
            
            modulus = new BigInteger(publicKey[0], SIXTEEN);
            exponent = new BigInteger(publicKey[1], SIXTEEN);
            
        } catch (CryptoException e) {
            fail(e.getMessage());
        }
        assertNotNull(signedData);
        
        final PublicKey pub = getPublicKey(modulus, exponent);
        final boolean result = verifySig(SHA1WITHRSA, pub, bFile, signedData);
        
        assertTrue(result);
        
    }
    
}
