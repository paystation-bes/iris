package com.digitalpaytech.ws.signingservicev2;

import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.jws.soap.SOAPBinding;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.client.SigningClient;
import com.digitalpaytech.domain.HashAlgorithmType;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.SigningLog;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.SystemException;
import com.digitalpaytech.ribbon.ClientFactory;
import com.digitalpaytech.server.signingservicev2.Signer;
import com.digitalpaytech.server.signingservicev2.SignerFactory;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.EncryptionService;
import com.digitalpaytech.service.HashAlgorithmTypeService;
import com.digitalpaytech.service.SigningLogService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.crypto.CryptoConstants;

/**
 * This Class is the Service implementation of the SigningServiceV2 SOAP Web
 * Service
 * 
 * 
 */
@Component("signingServiceV2")
@javax.jws.WebService(serviceName = "SigningServiceV2", portName = "SigningServicehttpPort", targetNamespace = "http://ws.digitalpaytech.com/SigningServiceV2/", endpointInterface = "com.digitalpaytech.ws.signingservicev2.SigningServiceV2")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.BARE)
public class SigningServiceV2Impl implements SigningServiceV2 {
    private static final Charset SIGNATURE_CHARSET = Charset.forName("UTF-8");
    static final String[] SIGNATURE_ALGORITHMS = { null, null, null, CryptoConstants.SHA_1, CryptoConstants.SHA_256 };    
    public static final String MESSAGE_DEPRECATED_HASH_TYPE = "The requested hash algorithm has been deprecated and no longer used by Iris";
    public static final String MESSAGE_UNSUPPORTED_HASH_TYPE = "Unsupported type request";
    public static final String MESSAGE_TYPE_MISSING = "Type parameter missing in incomming request";
    public static final String MESSAGE_SIGN_STRING_MISSING = "No sign string provided";
    public static final String MESSAGE_SIGN_BYTES_MISSING = "No sign bytes provided";
    public static final String MESSAGE_FAILURE = "Unable to generate Signature";
    public static final String MESSAGE_AUTHENTICATION_FAILURE = "Unable to authenticate user";
    public static final String MESSAGE_INSUFFICIENT_PRIVILEDGES = "User doesnot has privileges to access signing server";
    public static final String TEXT_FAILURE = "Failure";
    public static final String TEXT_INVALID_USER = "Invalid User";
    public static final String TEXT_INPUT_ERROR = "Input Error";
    
    public static final String ERROR_DEPRECATED_HASH_TYPE = "300";
    public static final String ERROR_UNSUPPORTED_HASH_TYPE = "301";
    public static final String ERROR_TYPE_MISSING = "302";
    public static final String ERROR_SIGN_STRING_MISSING = "303";
    public static final String ERROR_SIGN_BYTES_MISSING = "304";
    public static final String ERROR_FAILURE = "305";
    public static final String ERROR_AUTHENTICATION_FAILURE = "306";
    public static final String ERROR_INSUFFICIENT_PRIVILEDGES = "307";
    
    private static final Logger LOGGER = Logger.getLogger(SigningServiceV2Impl.class.getName());
    
    @Autowired
    private UserAccountService userAccountService;
    
    @Autowired
    private SigningLogService signingLogService;
    
    @Autowired
    private HashAlgorithmTypeService hashAlgoService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private ClientFactory clientFactory;
    
    private SigningClient signingClient;
    
    @PostConstruct
    public void init() {
        this.signingClient = this.clientFactory.from(SigningClient.class);
    }
    
    public final void setUserAccountService(final UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }
    
    public final HashAlgorithmTypeService getHashAlgoService() {
        return this.hashAlgoService;
    }
    
    public final void setHashAlgoService(final HashAlgorithmTypeService hashAlgoService) {
        this.hashAlgoService = hashAlgoService;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final void setSigningLogService(final SigningLogService signingLogService) {
        this.signingLogService = signingLogService;
    }
    
    public final EmsPropertiesService getEmsPropertiesService() {
        return this.emsPropertiesService;
    }
    
    protected final GetSignStringFault createSignStringFault(final String message, final String faultCode) {
        final GetSignStringFault fault = new GetSignStringFault();
        fault.setShortErrorMessage(message);
        fault.setErrCode(faultCode);
        return fault;
    }
    
    protected final GetSignBytesFault createSignBytesFault(final String message, final String faultCode) {
        final GetSignBytesFault fault = new GetSignBytesFault();
        fault.setShortErrorMessage(message);
        fault.setErrCode(faultCode);
        return fault;
    }
    
    protected final UnsupportedHashRequestFault createUnsupportedHashRequestFault(final String message, final String faultCode) {
        final UnsupportedHashRequestFault fault = new UnsupportedHashRequestFault();
        fault.setShortErrorMessage(message);
        fault.setErrCode(faultCode);
        return fault;
    }
    
    protected final void validateTypeInfo(final int hashType) throws GetUnsupportedHashRequestFaultMessage {
        if (useCryptoServer()) {
            if ((hashType < 1) || (hashType > SIGNATURE_ALGORITHMS.length)) {
                throw new GetUnsupportedHashRequestFaultMessage(TEXT_FAILURE, createUnsupportedHashRequestFault(MESSAGE_UNSUPPORTED_HASH_TYPE,
                                                                                                                ERROR_UNSUPPORTED_HASH_TYPE));
            }
        } else {
            final HashAlgorithmType algoType = this.hashAlgoService.findHashAlgorithmType(hashType);
            if (algoType == null) {
                throw new GetUnsupportedHashRequestFaultMessage(TEXT_FAILURE, createUnsupportedHashRequestFault(MESSAGE_UNSUPPORTED_HASH_TYPE,
                                                                                                                ERROR_UNSUPPORTED_HASH_TYPE));
            }
            if (algoType.isIsDeleted()) {
                throw new GetUnsupportedHashRequestFaultMessage(TEXT_FAILURE, createUnsupportedHashRequestFault(MESSAGE_DEPRECATED_HASH_TYPE,
                                                                                                                ERROR_DEPRECATED_HASH_TYPE));
            } else if (!algoType.isIsForSigning()) {
                throw new GetUnsupportedHashRequestFaultMessage(TEXT_FAILURE, createUnsupportedHashRequestFault(MESSAGE_UNSUPPORTED_HASH_TYPE,
                                                                                                                ERROR_UNSUPPORTED_HASH_TYPE));
            } else {
                return;
            }
        }
    }
    
    public final String getSignAlgoName(final int hashType) {
        return this.hashAlgoService.findAlgorithmName(hashType);
    }
    
    public final List<String> getSupportedSignAlorithms() {
        return this.hashAlgoService.findActiveSigningHashAlgorithmNames();
    }
    
    /*
     * (non-Javadoc)
     * @see
     * com.digitalpaytech.ws.signingservicev2.SigningServiceV2#getSignString
     * (com.digitalpaytech.ws.signingservicev2.GetSignStringType
     * getSignStringRequest )*
     */
    public final java.lang.String getSignString(final GetSignStringType getSignStringRequest) throws GetSignStringFaultMessage,
            GetUnsupportedHashRequestFaultMessage {
        String signedString = null;
        
        LOGGER.info("Executing operation getSignString");
        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        
        if (auth == null || auth.getName() == null || auth.getName().length() == 0) {
            return TEXT_INVALID_USER;
        }
        
        final WebUser webUser = (WebUser) auth.getPrincipal();
        
        if (!validateSigningPrivileges(webUser)) {
            LOGGER.error("com.digitalpaytech.ws.signingservicev2.SigningServiceV2Impl.getSignString(GetSignStringType): Access to signing server denied");
            throw new GetSignStringFaultMessage(TEXT_FAILURE, createSignStringFault(MESSAGE_INSUFFICIENT_PRIVILEDGES, ERROR_INSUFFICIENT_PRIVILEDGES));
        }
        
        if (getSignStringRequest.getType() == WebCoreConstants.HASH_TYPE_WS_MISSING) {
            throw new GetSignStringFaultMessage(TEXT_INPUT_ERROR, createSignStringFault(MESSAGE_TYPE_MISSING, ERROR_TYPE_MISSING));
        }
        if (WebCoreConstants.BLANK.equals(getSignStringRequest.getSignString()) || getSignStringRequest.getSignString() == null) {
            throw new GetSignStringFaultMessage(TEXT_INPUT_ERROR, createSignStringFault(MESSAGE_SIGN_STRING_MISSING, ERROR_SIGN_STRING_MISSING));
        }
        
        validateTypeInfo(getSignStringRequest.getType());
        
        if (useCryptoServer()) {
            signedString = this.signingClient.sign(formatSignatureAlgorithm(getSignStringRequest.getType())
                    , getSignStringRequest.getSignString().getBytes(SIGNATURE_CHARSET)).execute()
                            .toString(SIGNATURE_CHARSET);
        } else {
            byte[] signedData = null;
            Signer signer = null;
            
            final List<String> supportedAlgorithms = getSupportedSignAlorithms();
            final String signAlgorithmName = getSignAlgoName(getSignStringRequest.getType());
            
            if (supportedAlgorithms == null || supportedAlgorithms.size() == 0) {
                LOGGER.error("SigningServiceV2Impl.getSignString(GetSignStringType) : Unable to get supported alorithms list");
                throw new GetSignStringFaultMessage(TEXT_FAILURE, createSignStringFault(MESSAGE_FAILURE, ERROR_SIGN_BYTES_MISSING));
            }
            
            if (signAlgorithmName == null) {
                LOGGER.error("SigningServiceV2Impl.getSignString(GetSignStringType) : Unable to derive algorithm from type parameter");
                throw new GetSignStringFaultMessage(TEXT_FAILURE, createSignStringFault(MESSAGE_FAILURE, ERROR_FAILURE));
            }
            
            // After the incooming request has been validated we have to call the
            // factory to get the sha instance we were looking for
            try {
                signer = SignerFactory.getSigner(signAlgorithmName, supportedAlgorithms, this.emsPropertiesService);
            } catch (SystemException e) {
                throw new GetSignStringFaultMessage(TEXT_FAILURE, createSignStringFault(MESSAGE_FAILURE, ERROR_FAILURE));
            }
            
            try {
                signedData = signer.signData(getSignStringRequest.getSignString().getBytes());
            } catch (CryptoException e) {
                throw new GetSignStringFaultMessage(TEXT_FAILURE, createSignStringFault(MESSAGE_FAILURE, ERROR_FAILURE));
            }
            
            if (signedData == null || signedData.length == 0) {
                throw new GetSignStringFaultMessage(TEXT_FAILURE, createSignStringFault(MESSAGE_FAILURE, ERROR_FAILURE));
            }
            
            signedString = new String(Base64.encodeBase64(signedData));
        }
        
        logSignatureData(signedString, getSignStringRequest.getComments(), webUser);
        
        return signedString;
    }
    
    /*
     * (non-Javadoc)
     * @see
     * com.digitalpaytech.ws.signingservicev2.SigningServiceV2#getSignBytes(
     * com.digitalpaytech.ws.signingservicev2.GetSignBytesType
     * getSignBytesRequest )*
     */
    public final java.lang.String getSignBytes(final GetSignBytesType getSignBytesRequest) throws GetSignBytesFaultMessage,
            GetUnsupportedHashRequestFaultMessage {
        
        LOGGER.info("Executing operation getSignBytes");
        
        String signedString = null;
        
        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null || auth.getName() == null || auth.getName().length() == 0) {
            return TEXT_INVALID_USER;
        }
        
        final WebUser webUser = (WebUser) auth.getPrincipal();
        
        if (!validateSigningPrivileges(webUser)) {
            LOGGER.error("com.digitalpaytech.ws.signingservicev2.SigningServiceV2Impl.getSignBytes(GetSignBytesType): Access to signing server denied");
            throw new GetSignBytesFaultMessage(TEXT_FAILURE, createSignBytesFault(MESSAGE_INSUFFICIENT_PRIVILEDGES, ERROR_INSUFFICIENT_PRIVILEDGES));
        }
        
        if (getSignBytesRequest.getType() == WebCoreConstants.HASH_TYPE_WS_MISSING) {
            throw new GetSignBytesFaultMessage(TEXT_INPUT_ERROR, createSignBytesFault(MESSAGE_TYPE_MISSING, ERROR_TYPE_MISSING));
        }
        
        final byte[] rawData = getSignBytesRequest.getSignBytes();
        if (rawData == null || rawData.length == 0) {
            throw new GetSignBytesFaultMessage(TEXT_INPUT_ERROR, createSignBytesFault(MESSAGE_SIGN_BYTES_MISSING, ERROR_SIGN_BYTES_MISSING));
        }
        
        validateTypeInfo(getSignBytesRequest.getType());
        
        if (useCryptoServer()) {
            signedString = this.signingClient.sign(formatSignatureAlgorithm(getSignBytesRequest.getType())
                    , rawData).execute().toString(SIGNATURE_CHARSET);
        } else {
            Signer signer = null;
            byte[] signedData = null;
            
            final List<String> supportedAlgorithms = getSupportedSignAlorithms();
            final String signAlgorithmName = getSignAlgoName(getSignBytesRequest.getType());
            
            if (supportedAlgorithms == null || supportedAlgorithms.size() == 0) {
                LOGGER.error("com.digitalpaytech.ws.signingservicev2.SigningServiceV2Impl.getSignBytes(GetSignBytesType): Unable to get supported alorithms list");
                throw new GetSignBytesFaultMessage(TEXT_FAILURE, createSignBytesFault(MESSAGE_FAILURE, ERROR_FAILURE));
            }
            
            if (signAlgorithmName == null) {
                LOGGER.error("SigningServiceV2Impl.getSignBytes(GetSignBytesType): Unable to derive algorithm from type parameter");
                throw new GetSignBytesFaultMessage(TEXT_FAILURE, createSignBytesFault(MESSAGE_FAILURE, ERROR_FAILURE));
            }
            
            try {
                signer = SignerFactory.getSigner(getSignAlgoName(getSignBytesRequest.getType()), getSupportedSignAlorithms(), this.emsPropertiesService);
            } catch (SystemException e) {
                throw new GetSignBytesFaultMessage(TEXT_FAILURE, createSignBytesFault(MESSAGE_FAILURE, ERROR_FAILURE));
            }
            
            try {
                signedData = signer.signData(rawData);
            } catch (CryptoException e) {
                throw new GetSignBytesFaultMessage(TEXT_FAILURE, createSignBytesFault(MESSAGE_FAILURE, ERROR_FAILURE));
            }
            
            if (signedData == null || signedData.length == 0) {
                throw new GetSignBytesFaultMessage(TEXT_FAILURE, createSignBytesFault(MESSAGE_FAILURE, ERROR_FAILURE));
            }
            
            signedString = new String(Base64.encodeBase64(signedData));
        }
        
        logSignatureData(signedString, getSignBytesRequest.getComments(), webUser);
        
        return signedString;
        
    }
    
    private void logSignatureData(final String signedString, final String comments, final WebUser webUser) {
        
        final SigningLog logSignatureData = new SigningLog();
        logSignatureData.setSignature(signedString);
        logSignatureData.setComments(comments);
        logSignatureData.setSignDate(new Date());
        logSignatureData.setUserAccountId(webUser.getUserAccountId());
        logSignatureData.setUserAccountName(webUser.getUsername());
        
        this.signingLogService.recordSignatureData(logSignatureData);
    }
    
    private boolean validateSigningPrivileges(final WebUser webUser) {
        
        final Collection<RolePermission> permissions = this.userAccountService.findUserRoleAndPermission(webUser.getUserAccountId());
        if (permissions == null || permissions.size() == 0) {
            LOGGER.error("Unable to get user permissions");
            return false;
        }
        for (RolePermission permission : permissions) {
            if (permission.getPermission().getId() == WebSecurityConstants.PERMISSION_CREATE_SIGNATURE_SIGNINGSERVER) {
                return true;
            }
        }
        return false;
    }
    
    private boolean useCryptoServer() {
        return EncryptionService.CRYPTO_SERVER.equals(this.emsPropertiesService.getPropertyValue(EmsPropertiesService.ENCRYPTION_MODE, null, false));
    }
    
    private String formatSignatureAlgorithm(final int algorithmTypeId) {
        if (SIGNATURE_ALGORITHMS[algorithmTypeId] == null) {
            throw new IllegalArgumentException("Un-recognize hash algorithm: " + algorithmTypeId);
        }
        
        final StringBuilder result = new StringBuilder(SIGNATURE_ALGORITHMS[algorithmTypeId]);
        if (this.emsPropertiesService.getPropertyValueAsBoolean(EmsPropertiesService.ENABLE_SIGNING_PROXY, false)) {
            result.append(EncryptionService.SIGNING_PROXY_SUFFIX);
        }
        
        return result.toString();
    }
}
