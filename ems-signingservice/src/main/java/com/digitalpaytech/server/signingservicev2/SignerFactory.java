package com.digitalpaytech.server.signingservicev2;

import java.security.Security;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.commons.lang.StringUtils;

import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.SystemException;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.signer.signingservicev2.BouncyCastleSigner;
import com.digitalpaytech.signer.signingservicev2.NcipherSigner;
import com.digitalpaytech.util.CryptoUtil;

public final class SignerFactory {
    
    private static final Logger LOGGER = Logger.getLogger(SignerFactory.class);
    
    private static final String DEFAULT_CRYPTO_DIRECTORY = System.getProperty("catalina.home") + System.getProperty("file.separator") + "certs";
    
    private static final String SIGNING_CRYPTO_DIRECTORY = "CryptoDirectory";
    
    private static Signer signer;
    
    private static boolean securityProvidersInitialized;

    
    private SignerFactory() {
    }    
    
    private static synchronized void addSecurityProviders() {
        
        if (securityProvidersInitialized) {
            return;
        }
        
        int providerListLength = Security.getProviders().length;
        
        LOGGER.info("BC provider: " + Security.insertProviderAt(new org.bouncycastle.jce.provider.BouncyCastleProvider(), providerListLength++));
        
        securityProvidersInitialized = true;
    }
    
    // synchronized so that while the crypto is initializing, another thread
    // doesn't call into this method.
    public static synchronized Signer getSigner(final String hashTypeAlgo, final List<String> supportedSignAlgorithms,
                                                final EmsPropertiesService emsPropertiesService) throws SystemException {
        addSecurityProviders();
        String cryptoPath = null;
        
        if (signer == null || !signer.getAlgoType().equalsIgnoreCase(hashTypeAlgo)) {
            cryptoPath = emsPropertiesService.getPropertyValue(SIGNING_CRYPTO_DIRECTORY);
            if (cryptoPath == null || cryptoPath.length() == 0) {
                cryptoPath = DEFAULT_CRYPTO_DIRECTORY;
            }
            cryptoPath += "/dptsigning.keystore";
            
            // attempt to use nCipher keystore first
            try {
                signer = new NcipherSigner(cryptoPath, hashTypeAlgo, supportedSignAlgorithms);
                signer.setAlgoType(hashTypeAlgo);
                return signer;
            } catch (CryptoException e) {
                if (StringUtils.isNotBlank(CryptoUtil.getOperationMode()) 
                        && CryptoUtil.isHSMMode()) {
                    final String border = "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
                    LOGGER.error(border);
                    LOGGER.error("!!! Critical Error - HSM not correctly installed in Production Mode !!!");
                    LOGGER.error("!!!                        Shutting down IRIS                       !!!");
                    LOGGER.error(border);
                    LOGGER.error(e);
                    return null;
                } else {
                    LOGGER.info("nCipher Crypto Card not installed, trying Bouncy Castle.", e);
                }
            }
            
            try {
                signer = new BouncyCastleSigner(cryptoPath, hashTypeAlgo, supportedSignAlgorithms);
                signer.setAlgoType(hashTypeAlgo);
            } catch (CryptoException e) {
                throw new SystemException("Cannot create Crypto", e);
            }
        }
        return signer;
    }
}
