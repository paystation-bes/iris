package com.digitalpaytech.server.signingservicev2;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.text.MessageFormat;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import sun.security.rsa.RSAPublicKeyImpl;

import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.util.BouncyCastleCrypto;
import com.digitalpaytech.util.NcipherCrypto;

public abstract class Signer {
    
    public static final int CURR_PUBLIC_KEY_RSA_MODULUS = 0;
    public static final int CURR_PUBLIC_KEY_RSA_EXPONENT = 1;
    protected static final String KEY_ALIAS = "DPTSIGNINGKEY";
    protected static final String KEY_TYPE = "RSA";
    protected static final int KEY_SIZE = 4096;
    protected static final int BYTE_SIZE_ONE_KB = 1024;
    protected static final int SIXTEEN = 16;
    
    private static final Logger LOGGER = Logger.getLogger(Signer.class);
    
    private static final String FILE_SEPERATOR = "file.separator";
    
    private static final String DEFAULT_CRYPTO_DIRECTORY = System.getProperty("catalina.home") + System.getProperty(FILE_SEPERATOR) + "certs"
                                                           + System.getProperty(FILE_SEPERATOR);
    
    private static final String UNABLE_SIGN_DATA_ERROR = "Unable to sign data";
    private static final String UNABLE_VERIFY_DATA_ERROR = "Unable to verify data";
    private static final String UNABLE_EXTRACT_PUB_KEY_ERROR = "Unable to extract public key information";

    
    private static boolean securityProvidersInitialized;
    
    protected char[] keyPassword = "testpassword".toCharArray();
    protected char[] keyStorePassword = "l;kja*dfj@aRsBXkqePr?9xCv[9ewq.8ds(UjOsafW2I1sdfZsdfk]laG".toCharArray();
    
    protected Signature rsaSigner;
    protected Signature rsaVerifier;
    protected PrivateKey rsaPrivateKey;
    protected PublicKey rsaPublicKey;
    protected Certificate rsaCertificate;
    protected String algoType;
    
    private NcipherCrypto nCipher;
    private BouncyCastleCrypto bountryCastle;
    
    public final String getAlgoType() {
        return this.algoType;
    }
    
    public final void setAlgoType(final String algoType) {
        this.algoType = algoType;
    }
    
    public final NcipherCrypto getnCipher() {
        return this.nCipher;
    }
    
    public final void setnCipher(final NcipherCrypto nCipher) {
        this.nCipher = nCipher;
    }
    
    public final BouncyCastleCrypto getBountryCastle() {
        return this.bountryCastle;
    }
    
    public final void setBountryCastle(final BouncyCastleCrypto bountryCastle) {
        this.bountryCastle = bountryCastle;
    }
    
    /**
     * This Method gets the alias name which will be lookup name of the keys in
     * the Signing Keystore file. To maintain backward compatibility SHA1WithRSA
     * uses the existing KEY_ALIAS name. All future algorithms have their name
     * appended to the KEY_ALIAS for lookup logic
     * 
     * @param hashType
     * @return String Key Alias name for Keystore lookup
     */
    public final String getAliasName(final String hashType) {
        
        if ("SHA1WithRSA".equalsIgnoreCase(hashType)) {
            return KEY_ALIAS;
        } else {
            return KEY_ALIAS + hashType;
        }
    }
    
    public final byte[] signData(final byte[] data) throws CryptoException {
        if (data == null || data.length == 0) {
            return null;
        }
        
        try {
            byte[] signData = null;
            synchronized (this.rsaSigner) {
                this.rsaSigner.update(data);
                signData = this.rsaSigner.sign();
            }
            return signData;
        } catch (Exception e) {
            throw new CryptoException(UNABLE_SIGN_DATA_ERROR, e);
        }
    }
    
    public final byte[] signData(final InputStream appstream) throws CryptoException {
        if (appstream == null) {
            return null;
        }
        
        try {
            byte[] signData = null;
            synchronized (this.rsaSigner) {
                final BufferedInputStream bufin = new BufferedInputStream(appstream);
                final byte[] buffer = new byte[BYTE_SIZE_ONE_KB];
                int len;
                while (bufin.available() != 0) {
                    len = bufin.read(buffer);
                    this.rsaSigner.update(buffer, 0, len);
                }
                bufin.close();
                
                signData = this.rsaSigner.sign();
            }
            return signData;
        } catch (Exception e) {
            throw new CryptoException(UNABLE_SIGN_DATA_ERROR, e);
        }
    }
    
    public final boolean verifyData(final byte[] appData, final byte[] signData) throws CryptoException {
        if ((appData == null) || (appData.length == 0) || (signData == null) || (signData.length == 0)) {
            return false;
        }
        
        try {
            boolean verifies = false;
            
            synchronized (this.rsaVerifier) {
                this.rsaVerifier.update(appData);
                
                verifies = this.rsaVerifier.verify(signData);
            }
            return verifies;
        } catch (Exception e) {
            throw new CryptoException(UNABLE_VERIFY_DATA_ERROR, e);
        }
    }
    
    public final boolean verifyData(final InputStream appStream, final InputStream signStream) throws CryptoException {
        if ((appStream == null) || (signStream == null)) {
            return false;
        }
        
        try {
            boolean verifies = false;
            final byte[] sigToVerify = new byte[signStream.available()];
            signStream.read(sigToVerify);
            synchronized (this.rsaVerifier) {
                final BufferedInputStream bufin = new BufferedInputStream(appStream);
                final byte[] buffer = new byte[BYTE_SIZE_ONE_KB];
                int len;
                while (bufin.available() != 0) {
                    len = bufin.read(buffer);
                    this.rsaVerifier.update(buffer, 0, len);
                }
                bufin.close();
                
                verifies = this.rsaVerifier.verify(sigToVerify);
            }
            return verifies;
        } catch (Exception e) {
            throw new CryptoException(UNABLE_VERIFY_DATA_ERROR, e);
        }
    }
    
    /**
     * Retrieves the <b>current</b> Public Key used by the <i>external
     * source</i> CreditCard Data encryption routines. This key is used for
     * encryption by an external source before it transmits the CreditCard Data
     * to the EMS.
     * 
     * @return The <b>current</b> Public Key.
     * @throws CryptoException
     *             - if the key cannot be retrieved.
     */
    public final String[] getCurrentPublicKey() throws CryptoException {
        final String[] currentPublicKey = new String[2];
        try {
            final RSAPublicKeyImpl publicKey = new RSAPublicKeyImpl(this.rsaPublicKey.getEncoded());
            currentPublicKey[CURR_PUBLIC_KEY_RSA_MODULUS] = convertToBase64EncodedString(publicKey.getModulus().toString(SIXTEEN));
            currentPublicKey[CURR_PUBLIC_KEY_RSA_EXPONENT] = convertToBase64EncodedString(publicKey.getPublicExponent().toString(SIXTEEN));
        } catch (InvalidKeyException e) {
            throw new CryptoException(UNABLE_EXTRACT_PUB_KEY_ERROR, e);
        }
        
        return currentPublicKey;
    }
    
    public final String[] getNonEncodedPublicKey() throws CryptoException {
        final String[] currentPublicKey = new String[2];
        try {
            final RSAPublicKeyImpl publicKey = new RSAPublicKeyImpl(this.rsaPublicKey.getEncoded());
            currentPublicKey[CURR_PUBLIC_KEY_RSA_MODULUS] = publicKey.getModulus().toString(SIXTEEN);
            currentPublicKey[CURR_PUBLIC_KEY_RSA_EXPONENT] = publicKey.getPublicExponent().toString(SIXTEEN);
        } catch (InvalidKeyException e) {
            throw new CryptoException(UNABLE_EXTRACT_PUB_KEY_ERROR, e);
        }
        
        return currentPublicKey;
    }
    
    /**
     * Prints a certificate in a human readable format.
     */
    public final String printX509Cert() throws Exception {
        /*
         * out.println("Owner: " + cert.getSubjectDN().toString() + "\n" +
         * "Issuer: " + cert.getIssuerDN().toString() + "\n" + "Serial number: "
         * + cert.getSerialNumber().toString(SIXTEEN) + "\n" + "Valid from: " +
         * cert.getNotBefore().toString() + " until: " +
         * cert.getNotAfter().toString() + "\n" + "Certificate fingerprints:\n"
         * + "\t MD5:  " + getCertFingerPrint("MD5", cert) + "\n" + "\t SHA1: "
         * + getCertFingerPrint("SHA1", cert));
         */
        final X509Certificate cert = (X509Certificate) this.rsaCertificate;
        final MessageFormat form = new MessageFormat(
                "Owner: {0}\nIssuer: {1}\nSerial number: {2}\nValid from: {3} until: {4}\nCertificate fingerprints:\n\t MD5:  {5}\n\t SHA1: {6}");
        final Object[] source = { cert.getSubjectDN().toString(), cert.getIssuerDN().toString(), cert.getSerialNumber().toString(SIXTEEN),
            cert.getNotBefore().toString(), cert.getNotAfter().toString(), getCertFingerPrint("MD5"), getCertFingerPrint("SHA1"), };
        return form.format(source);
    }
    
    /**
     * Gets the requested finger print of the certificate.
     */
    public final String getCertFingerPrint(final String mdAlg) throws Exception {
        final byte[] encCertInfo = this.rsaCertificate.getEncoded();
        final MessageDigest md = MessageDigest.getInstance(mdAlg);
        final byte[] digest = md.digest(encCertInfo);
        return toHexString(digest);
    }
    
    public final byte[] getCertificate() throws Exception {
        return this.rsaCertificate.getEncoded();
    }
    
    /**
     * Converts a byte to hex digit and writes to the supplied buffer
     */
    protected final void byte2hex(final byte b, final StringBuffer buf) {
        final char[] hexChars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
        final int high = (b & 0xf0) >> 4;
        final int low = b & 0x0f;
        buf.append(hexChars[high]);
        buf.append(hexChars[low]);
    }
    
    /**
     * Converts a byte array to hex string
     */
    public final String toHexString(final byte[] block) {
        final StringBuffer buf = new StringBuffer();
        final int len = block.length;
        for (int i = 0; i < len; i++) {
            byte2hex(block[i], buf);
            if (i < len - 1) {
                buf.append(":");
            }
        }
        return buf.toString();
    }
    
    private String convertToBase64EncodedString(String baseSIXTEENString) {
        // If the hex string has an odd # of digits, it means there is a leading
        // '0' that is missing
        if (baseSIXTEENString.length() % 2 == 1) {
            baseSIXTEENString = "0" + baseSIXTEENString;
        }
        
        // Convert the hex string to a byte array
        final byte[] byteArray = new byte[baseSIXTEENString.length() / 2];
        int i = 0;
        
        for (i = 0; i < baseSIXTEENString.length() - 2; i = i + 2) {
            byteArray[i / 2] = (byte) Integer.parseInt(baseSIXTEENString.substring(i, i + 2), SIXTEEN);
        }
        byteArray[i / 2] = (byte) Integer.parseInt(baseSIXTEENString.substring(i), SIXTEEN);
        
        // Base 64 encode and encapsulate in a string
        return new String(Base64.encodeBase64(byteArray));
    }
}
