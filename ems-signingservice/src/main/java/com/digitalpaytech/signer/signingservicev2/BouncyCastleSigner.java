package com.digitalpaytech.signer.signingservicev2;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStore.PasswordProtection;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;

import javax.security.auth.x500.X500Principal;

import org.bouncycastle.x509.X509V1CertificateGenerator;

import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.server.signingservicev2.Signer;

public final class BouncyCastleSigner extends Signer {
    
    private static final String JCE_PROVIDER = "BC";
    private static final String KEYSTORE_TYPE = "JCEKS";
    private static final String KEYSTORE_PROVIDER = "SunJCE";
    private static final String CERTIFICATE_SIGNATURE = "CN=Digital Payment, OU=Digital Payment, O=Digital Payment, L=Burnaby, ST=BC, C=CA";
    private static final long EXPIRY_TIME = 30 * 24 * 60 * 60 * 1000;
    
    public BouncyCastleSigner(final String keyPath, final String hashType, final List<String> supportedSignAlgorithms) throws CryptoException {
        
        initializeBouncyCastleKeystore(keyPath, hashType, supportedSignAlgorithms);
        try {
            rsaSigner = Signature.getInstance(hashType, JCE_PROVIDER);
            rsaSigner.initSign(rsaPrivateKey);
            rsaVerifier = Signature.getInstance(hashType, JCE_PROVIDER);
            rsaVerifier.initVerify(rsaPublicKey);
        } catch (Exception e) {
            throw new CryptoException("Unable to initialize crypto", e);
        }
    }
    
    private void initializeBouncyCastleKeystore(final String keyPath, final String hashType, final List<String> supportedSignAlgorithms)
        throws CryptoException {
        PrivateKeyEntry rsaKeyEntry;
        KeyStore keyStore = null;
        File keyStoreFile = null;
        try {
            keyStore = KeyStore.getInstance(KEYSTORE_TYPE, KEYSTORE_PROVIDER);
            keyStoreFile = new File(keyPath);
            rsaKeyEntry = null;
            
            if (!keyStoreFile.exists()) {
                createBouncyKeyStore(keyStore, keyStoreFile, supportedSignAlgorithms);
            }
            
            keyStore.load(new FileInputStream(keyStoreFile), keyStorePassword);
            rsaKeyEntry = (PrivateKeyEntry) keyStore.getEntry(getAliasName(hashType), new PasswordProtection(keyPassword));
            
        } catch (Exception e) {
            throw new CryptoException(e);
        }
        
        // xxx RSA key manually created in production EMS for PABP demo
        if (rsaKeyEntry != null) {
            rsaPrivateKey = rsaKeyEntry.getPrivateKey();
            rsaCertificate = rsaKeyEntry.getCertificate();
            rsaPublicKey = rsaKeyEntry.getCertificate().getPublicKey();
        }
        
    }
    
    public X509Certificate generateV1Certificate(final KeyPair pair, final String signingAlgorithm) throws InvalidKeyException,
            NoSuchProviderException, NoSuchAlgorithmException, SignatureException, CertificateEncodingException, IllegalStateException {
        // generate the certificate
        final X509V1CertificateGenerator certGen = new X509V1CertificateGenerator();
        
        certGen.setSerialNumber(BigInteger.valueOf(System.currentTimeMillis()));
        certGen.setIssuerDN(new X500Principal(CERTIFICATE_SIGNATURE));
        certGen.setNotBefore(new Date(System.currentTimeMillis()));
        certGen.setNotAfter(new Date(System.currentTimeMillis() + EXPIRY_TIME));
        certGen.setSubjectDN(new X500Principal(CERTIFICATE_SIGNATURE));
        certGen.setPublicKey(pair.getPublic());
        certGen.setSignatureAlgorithm(signingAlgorithm);
        
        return certGen.generate(pair.getPrivate(), JCE_PROVIDER);
    }
    
    public synchronized void createBouncyKeyStore(final KeyStore keyStore, final File keystoreFile, final List<String> supportedSignAlgorithms)
        throws CryptoException {
        KeyPairGenerator keyGen = null;
        PrivateKeyEntry rsaKeyEntry = null;
        CertificateFactory fact = null;
        // read the certificate
        X509Certificate x509Cert = null;
        
        if (supportedSignAlgorithms == null || supportedSignAlgorithms.size() == 0) {
            return;
        }
        
        for (String algo : supportedSignAlgorithms) {
            try {
                keyGen = KeyPairGenerator.getInstance(KEY_TYPE, JCE_PROVIDER);
                keyGen.initialize(KEY_SIZE);
                final KeyPair keyPair = keyGen.generateKeyPair();
                final ByteArrayOutputStream bOut = new ByteArrayOutputStream();
                bOut.write(generateV1Certificate(keyPair, algo).getEncoded());
                final InputStream in = new ByteArrayInputStream(bOut.toByteArray());
                fact = CertificateFactory.getInstance("X.509", JCE_PROVIDER);
                x509Cert = (X509Certificate) fact.generateCertificate(in);
                final X509Certificate[] chain = new X509Certificate[1];
                chain[0] = x509Cert;
                keyStore.load(null, null);
                rsaKeyEntry = new PrivateKeyEntry(keyPair.getPrivate(), chain);
                keyStore.setEntry(getAliasName(algo), rsaKeyEntry, new PasswordProtection(keyPassword));
                keyStore.store(new FileOutputStream(keystoreFile), keyStorePassword);
            } catch (Exception e) {
                throw new CryptoException(e);
            }
            
        }
        
    }
    /*
     * // This should create Keys of all the types of signature // algorithms
     * supported in Iris
     * for (int i = 0; i < signAlgorithms.length; i++) { KeyPairGenerator kgen =
     * KeyPairGenerator.getInstance( KEY_TYPE, JCE_PROVIDER);
     * kgen.initialize(KEY_SIZE); KeyPair keyPair = kgen.generateKeyPair();
     * ByteArrayOutputStream bOut = new ByteArrayOutputStream(); // create the
     * input stream bOut.write(generateV1Certificate(keyPair, signAlgorithms[i])
     * .getEncoded());
     * InputStream in = new ByteArrayInputStream( bOut.toByteArray());
     * // create the certificate factory CertificateFactory fact =
     * CertificateFactory.getInstance( "X.509", "BC");
     * // read the certificate X509Certificate x509Cert = (X509Certificate) fact
     * .generateCertificate(in);
     * X509Certificate[] chain = new X509Certificate[1]; chain[0] = x509Cert;
     * keystore.load(null, null); rsa_key_entry = new
     * PrivateKeyEntry(keyPair.getPrivate(), chain); keystore.setEntry(KEY_ALIAS
     * + signAlgorithms[i], rsa_key_entry, new PasswordProtection(keyPassword));
     * keystore.store(new FileOutputStream(keystore_file), keyStorePassword);
     * }
     */
    
}
