package com.digitalpaytech.signer.signingservicev2;

import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.KeyStore.PasswordProtection;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.Signature;
import java.util.List;

import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.server.signingservicev2.Signer;

public final class NcipherSigner extends Signer {
    
    private static final String JCE_PROVIDER = "nCipherKM";
    private static final String KEYSTORE_TYPE = "nCipher.sworld";
    private static final String KEYSTORE_PROVIDER = "nCipherKM";
    
    public NcipherSigner(final String keyPath, final String signAlgorithm, final List<String> supportedSignAlgorithms) throws CryptoException {
        initializeNcipherKeystore(keyPath, signAlgorithm);
        try {
            rsaSigner = Signature.getInstance(signAlgorithm, JCE_PROVIDER);
            rsaSigner.initSign(rsaPrivateKey);
            rsaVerifier = Signature.getInstance(signAlgorithm, JCE_PROVIDER);
            rsaVerifier.initVerify(rsaPublicKey);
        } catch (Exception e) {
            throw new CryptoException("Unable to initialize crypto", e);
        }
    }
    
    private void initializeNcipherKeystore(final String keyPath, final String signAlgorithm) throws CryptoException {
        final File keystoreFile = new File(keyPath);
        PrivateKeyEntry rsaKeyEntry = null;
        
        // If keystore file already exists, load the data
        if (keystoreFile.exists()) {
            try {
                final KeyStore keystore = KeyStore.getInstance(KEYSTORE_TYPE, KEYSTORE_PROVIDER);
                keystore.load(new FileInputStream(keystoreFile), keyStorePassword);
                rsaKeyEntry = (PrivateKeyEntry) keystore.getEntry(getAliasName(signAlgorithm), new PasswordProtection(keyPassword));
            } catch (Exception e) {
                throw new CryptoException("Unable to initialize keystore.", e);
            }
        } else {
            throw new CryptoException("Keystore: " + keyPath + " does not exist.");
        }
        
        rsaPrivateKey = rsaKeyEntry.getPrivateKey();
        rsaCertificate = rsaKeyEntry.getCertificate();
        rsaPublicKey = rsaKeyEntry.getCertificate().getPublicKey();
    }
}
