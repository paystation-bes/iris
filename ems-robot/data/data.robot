*** Settings ***
# Includes the Selenium2Library

Library  Selenium2Library  run_on_failure=Nothing

***Variables***
# A Resource file that includes all pre-defined variables


${TEST_SERVER_URL}         		dev.digitalpaytech.com:8474
# ${TEST_SERVER_URL}         		qa3.digitalpaytech.com
${SERVICES_URL}            		https://${TEST_SERVER_URL}/services
${ADMIN_USERNAME}          		admin@oranj
${SYS_ADMIN_USERNAME}      		admin@qa1admin
${PARENT_USERNAME}         		qaAutomation1@parent
${CHILD_USERNAME_2}        		qaAutomation2@child
${CHILD_USERNAME_1}        		admin@qa1child
${PARENT_2_USERNAME}       		qaAutomation2@parent
${CHILD_USERNAME_3}        		admin@qa1child2
${CHILD_USERNAME_4}        		admin@qa2child
${CHILD_USERNAME_5}        		admin@qa2child2
${LIZA_USERNAME}           		liza@oranj
${NOAH_USERNAME}           		noah@oranj
${THOMAS_USERNAME}         		thomas@childuserEDIT
${FLEX_PARENT_NAME}        		oranjparentqa
${FLEX_CHILD_NAME}         		oranjchildqa
${PASSWORD}                		Password$1
${PASSWORD_2}              		Password$2
${DEFAULT_PASSWORD}        		password
${CHILD_ROLE}              		01SmokeTestChildRole
${PARENT_ROLE}             		01SmokeTestParentRole
${EDITED_CHILD_ROLE}       		01ChildRoleEDITED 
${EDITED_PARENT_ROLE}      		01ParentRoleEDITED
${SMOKE_EMAIL}             		SmokeTest@qaAutomation
${PAYSTATION_COMM_ADDRESS}  	500000070005
${BROWSER}        				firefox
${FF_PROFILE_DIR}				${CURDIR}/../conf/firefox_profile
${DELAY}          				0
${LOGIN_URL}      				https://${TEST_SERVER_URL}/
${SYSTEM_ADMIN_URL}    			https://${TEST_SERVER_URL}/systemAdmin/workspaceIndex.html
${CUSTOMER_ADMIN_URL}    		https://${TEST_SERVER_URL}/secure/dashboard/index.html

# Database 
${DB_API_MODULE_NAME}           pymysql

${DB_USERNAME}                  billyh
${DB_PASSWORD}                  fm349fi1
${DB_PORT}                      3306

# 7.5 Bugs Branch
${DB_NAME}                      IRISAPP-7.5-Bugs
${DB_HOST}                      172.30.5.65

# QA3
#${DB_NAME}                      ems7_5_QA
#${DB_HOST}                      172.30.5.91
