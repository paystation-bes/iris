*** Settings ***
Documentation     A data file containing reusable dictionary global variables for all known alerts that can be sent to a paystation, designed to be used with keywords for sending alerts

*** Variables ***
&{BILL_ACCEPTOR_JAM}                              type=BillAcceptor    action=Jam                       information=${EMPTY}
&{BILL_ACCEPTOR_JAM_CLEARED}                      type=BillAcceptor    action=JamCleared                information=${EMPTY}
&{BILL_ACCEPTOR_NOT_PRESENT}                      type=BillAcceptor    action=NotPresent                information=${EMPTY}
&{BILL_ACCEPTOR_PRESENT}                          type=BillAcceptor    action=Present                   information=${EMPTY}
&{BILL_ACCEPTOR_UNABLE_TO_STACK}                  type=BillAcceptor    action=UnableToStack             information=${EMPTY}
&{BILL_ACCEPTOR_UNABLE_TO_STACK_CLEARED}          type=BillAcceptor    action=UnableToStackCleared      information=${EMPTY}
&{BILL_CANISTER_REMOVED}                          type=BillCanister    action=Removed                   information=${EMPTY}
&{BILL_CANISTER_INSERTED}                         type=BillCanister    action=Inserted                  information=${EMPTY}
&{BILL_CANISTER_NOT_PRESENT}					  type=BillCanister    action=NotPresent                information=${EMPTY}
&{BILL_CANISTER_PRESENT}					      type=BillCanister    action=Present                   information=${EMPTY}
&{BILL_STACKER_BILLS_ACCEPTED}                    type=BillStacker     action=BillsAccepted             information=${EMPTY}
&{BILL_STACKER_FULL}                              type=BillStacker     action=Full                      information=${EMPTY}
&{BILL_STACKER_FULL_CLEARED}                      type=BillStacker     action=FullCleared               information=${EMPTY}
&{BILL_STACKER_LEVEL_NORMAL}                      type=BillStacker     action=Normal                    information=${EMPTY}
&{BILL_STACKER_NOT_PRESENT}                       type=BillStacker     action=NotPresent                information=${EMPTY}
&{BILL_STACKER_PRESENT}                           type=BillStacker     action=Present                   information=${EMPTY}
&{BILL_STACKER_RESET}                             type=BillStacker     action=Reset                     information=${EMPTY}
&{CARD_READER_NOT_PRESENT}                        type=CardReader      action=NotPresent                information=${EMPTY}
&{CARD_READER_PRESENT}                            type=CardReader      action=Present                   information=${EMPTY}
&{COIN_ACCEPTOR_COINS_ACCEPTED}                   type=CoinAcceptor    action=CoinsAccepted             information=${EMPTY}
&{COIN_ACCEPTOR_JAM}                              type=CoinAcceptor    action=Jam                       information=${EMPTY}
&{COIN_ACCEPTOR_JAM_CLEARED}                      type=CoinAcceptor    action=JamCleared                information=${EMPTY}
&{COIN_ACCEPTOR_NOT_PRESENT}                      type=CoinAcceptor    action=NotPresent                information=${EMPTY}
&{COIN_ACCEPTOR_PRESENT}                          type=CoinAcceptor    action=Present                   information=${EMPTY}
&{COIN_ACCEPTOR_RESET}                            type=CoinAcceptor    action=Reset                     information=${EMPTY}
&{COIN_CANISTER_INSERTED}                         type=CoinCanister    action=Inserted                  information=${EMPTY}
&{COIN_CANISTER_NOT_PRESENT}                      type=CoinCanister    action=NotPresent                information=${EMPTY}
&{COIN_CANISTER_PRESENT}                          type=CoinCanister    action=Present                   information=${EMPTY}
&{COIN_CANISTER_REMOVED}                          type=CoinCanister    action=Removed                   information=${EMPTY}
&{COIN_CHANGER_EMPTY}                             type=CoinChanger     action=Empty                     information=${EMPTY}
&{COIN_CHANGER_JAM}                               type=CoinChanger     action=Jam                       information=${EMPTY}
&{COIN_CHANGER_JAM_CLEARED}                       type=CoinChanger     action=JamCleared                information=${EMPTY}
&{COIN_CHANGER_LOW}                               type=CoinChanger     action=Low                       information=5-21; 25-33; 100-11; 200-7;
&{COIN_CHANGER_NORMAL}                            type=CoinChanger     action=Normal                    information=${EMPTY}
&{COIN_CHANGER_NOT_PRESENT}                       type=CoinChanger     action=NotPresent                information=${EMPTY}
&{COIN_CHANGER_PRESENT}                           type=CoinChanger     action=Present                   information=${EMPTY}
&{COIN_ESCROW_JAM}                                type=CoinEscrow      action=Jam                       information=${EMPTY}
&{COIN_ESCROW_JAM_CLEARED}                        type=CoinEscrow      action=JamCleared                information=${EMPTY}
&{COIN_ESCROW_NOT_PRESENT}                        type=CoinEscrow      action=NotPresent                information=${EMPTY}
&{COIN_ESCROW_PRESENT}                            type=CoinEscrow      action=Present                   information=${EMPTY}
&{COIN_HOPPER_1_COINS_DISPENSED}                  type=CoinHopper      action=CoinDispensed             information=1
&{COIN_HOPPER_1_EMPTY}                            type=CoinHopper      action=Empty                     information=1
&{COIN_HOPPER_1_EMPTY_CLEARED}                    type=CoinHopper      action=EmptyCleared              information=1
&{COIN_HOPPER_1_NOT_PRESENT}                      type=CoinHopper      action=NotPresent                information=1
&{COIN_HOPPER_1_PRESENT}                          type=CoinHopper      action=Present                   information=1
&{COIN_HOPPER_2_COINS_DISPENSED}                  type=CoinHopper      action=CoinDispensed             information=2
&{COIN_HOPPER_2_EMPTY}                            type=CoinHopper      action=Empty                     information=2
&{COIN_HOPPER_2_EMPTY_CLEARED}                    type=CoinHopper      action=EmptyCleared              information=2
&{COIN_HOPPER_2_NOT_PRESENT}                      type=CoinHopper      action=NotPresent                information=2
&{COIN_HOPPER_2_PRESENT}                          type=CoinHopper      action=Present                   information=2
&{PAYSTATION_ALARM_OFF}                           type=Paystation      action=AlarmOff                  information=${EMPTY}
&{PAYSTATION_ALARM_ON}                            type=Paystation      action=AlarmOn                   information=${EMPTY}
&{DOOR_OPENED}                                    type=Door            action=DoorOpened                information=${EMPTY}
&{DOOR_CLOSED}                                    type=Door            action=DoorClosed                information=${EMPTY}
&{PAYSTATION_DOOROPENED_CASHVAULT}                type=Paystation      action=DoorOpened                information=CashVault
&{PAYSTATION_DOOROPENED_LOWER}                    type=Paystation      action=DoorOpened                information=Lower
&{PAYSTATION_DOOROPENED_Maintenance}              type=Paystation      action=DoorOpened                information=Maintenance
&{PAYSTATION_DOOROPENED_UPPER}                    type=Paystation      action=DoorOpened                information=Upper
&{PAYSTATION_DOORCLOSED_CASHVAULT}                type=Paystation      action=DoorClosed                information=CashVault
&{PAYSTATION_DOORCLOSED_LOWER}                    type=Paystation      action=DoorClosed                information=Lower
&{PAYSTATION_DOORCLOSED_MAINTENANCE}              type=Paystation      action=DoorClosed                information=Maintenance
&{PAYSTATION_DOORCLOSED_UPPER}                    type=Paystation      action=DoorClosed                information=Upper
&{PAYSTATION_LOWPOWERSHUTDOWNOFF}                 type=Paystation      action=LowPowerShutdownOff       information=${EMPTY}
&{PAYSTATION_LOWERPOWERSHUTDOWN}                  type=Paystation      action=LowPowerShutdown          information=${EMPTY}
&{PAYSTATION_PUBLICKEYUPDATE_TRUE}                type=Paystation      action=PublicKeyUpdate           information=True
&{PAYSTATION_PUBLICKEYUPDATE_OFF}                 type=Paystation      action=PublicKeyUpdateOff        information=${EMPTY}
&{PAYSTATION_SERVICEMODEENABLED}                  type=Paystation      action=ServiceModeEnabled        information=${EMPTY}
&{PAYSTATION_SERVICEMODEDISABLED}                 type=Paystation      action=ServiceModeDisabled       information=${EMPTY}
&{PAYSTATION_SHOCKON}                             type=Paystation      action=ShockOn                   information=${EMPTY}
&{PAYSTATION_SHOCKOFF}                            type=Paystation      action=ShockOff                  information=${EMPTY}
&{PAYSTATION_TICKETNOTTAKEN}                      type=Paystation      action=TicketNotTaken            information=${EMPTY}
&{PAYSTATION_TICKETTAKENCLEARED}                  type=Paystation      action=TicketTakenCleared        information=${EMPTY}
&{PAYSTATION_UPGRADE}                             type=Paystation      action=Upgrade                   information=${EMPTY}
&{PCM_UPGRADE}                                    type=PCM             action=Upgrade                   information=${EMPTY}
&{PCM_WIRELESSSIGNALSTRENGTH}                     type=PCM             action=WirelessSignalStrength    information=${EMPTY}
&{PRINTER_CUTTERERROR}                            type=Printer         action=CutterError               information=${EMPTY}
&{PRINTER_CUTTERERRORCLEARED}                     type=Printer         action=CutterErrorCleared        information=${EMPTY}
&{PRINTER_HEADERROR}                              type=Printer         action=HeadError                 information=${EMPTY}
&{PRINTER_HEADERRORCLEARED}                       type=Printer         action=HeadErrorCleared          information=${EMPTY}
&{PRINTER_LEVERDISENGAGED}                        type=Printer         action=LeverDisengaged           information=${EMPTY}
&{PRINTER_LEVERDISENGAGEDCLEARED}                 type=Printer         action=LeverDisengagedCleared    information=${EMPTY}
&{PRINTER_NOTPRESENT}                             type=Printer         action=NotPresent                information=${EMPTY}
&{PRINTER_PRESENT}                                type=Printer         action=Present                   information$=${EMPTY}
&{PRINTER_PAPERJAM}                               type=Printer         action=PaperJam                  information=${EMPTY}
&{PRINTER_PAERJAM_CLEARED}                        type=Printer         action=PaperJamCleared           information=${EMPTY}
&{PRINTER_PAPERLOW}                               type=Printer         action=PaperLow                  information=${EMPTY}
&{PRINTER_PAPERLOWCLEARED}                        type=Printer         action=PaperLowCleared           information=${EMPTY}
&{PRINTER_PAPEROUT}                               type=Printer         action=PaperOut                  information=${EMPTY}
&{PRINTER_PAPEROUTCLEARED}                        type=Printer         action=PaperOutCleared           information=${EMPTY}
&{PRINTER_PRINTINGFAILURE}                        type=Printer         action=PrintingFailure           information=${EMPTY}
&{PRINTER_PRINTINGFAILURECLEARED}                 type=Printer         action=PrintingFailureCleared    information=${EMPTY}
&{PRINTER_TEMPERATUREERROR}                       type=Printer         action=TemperatureError          information=${EMPTY}
&{PRINTER_TEMPERATUREERRORCLEARED}                type=Printer         action=TemperatureErrorCleared    information=${EMPTY}
&{PRINTER_TICKETDIDNOTPRINT}                      type=Printer         action=TicketDidNotPrint          information=${EMPTY}
&{PRINTER_TICKETDIDNOTPRINTCLEARED}               type=Printer         action=TicketDidNotPrintCleared    information=${EMPTY}
&{PRINTER_VOLTAGEERROR}                           type=Printer         action=VoltageError              information=${EMPTY}
&{PRINTER_VOLTAGEERRORCLEARED}                    type=Printer         action=VoltageErrorCleared       information=${EMPTY}
&{RFIDCARDREADER_NOTPRESENT}                      type=RfidCardReader    action=NotPresent              information=${EMPTY}
&{RFIDCARDREADER_PRESENT}                         type=RfiedCardReader    action=Present                information=${EMPTY}
&{TICKETFOOTERSUCCESS_NONE}                       type=TicketFooterSuccess    action=None               information=${EMPTY}
&{MAINTENANCE_DOOR_OPENED}                        type=Paystation    action=DoorOpened    information=Maintenance
&{PRINTER_NOT_PRESENT}                            type=Printer    action=NotPresent    information=${EMPTY}
&{PRINTER_PRESENT}                                type=Printer    action=Present    information=${EMPTY}


*** Keywords ***

Create Event To Set Paystation Battery: "${battery_number}" To Voltage: "${voltage}"
	${PAYSTATION_VOLTAGE}=    Create Dictionary    type=Paystation    action=Voltage    information=${battery_number}:${voltage}
	[return]    ${PAYSTATION_VOLTAGE}

Create Event To Set Paystation Ambient Temperature To: "${temperature}"
	${AMBIENT_TEMPERATURE}=    Create Dictionary    type=Sensor    action=AmbientTemperature    information=${temperature}
	[return]    ${AMBIENT_TEMPERATURE}

Create Event To Set Paystation Relative Humidity To: "${humidity}"
	${RELATIVE_HUMIDITY}=    Create Dictionary    type=Sensor    action=RelativeHumidity    information=${humidity}
	[return]    ${RELATIVE_HUMIDITY}

Create Event To Set Paystation Output Power To: "${output_power}"
	${OUTPUT_POWER}=    Create Dictionary    type=Sensor    action=OutputPower    information=${output_power}
	[return]    ${OUTPUT_POWER}

Create Event To Set Paystation Input Power To: "${input_power}"
	${INPUT_POWER}=    Create Dictionary    type=Sensor    action=InputPower    information=${input_power}
	[return]    ${INPUT_POWER}

Create Event To Set Paystation Controller Temperature To: "${temperature}"
	${CONTROLLER_TEMPERATURE}=    Create Dictionary    type=Sensor    action=ControllerTemperature    information=${temperature}
	[return]    ${CONTROLLER_TEMPERATURE}