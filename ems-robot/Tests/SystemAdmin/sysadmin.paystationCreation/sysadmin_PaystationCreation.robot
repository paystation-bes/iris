*** Settings ***
Documentation		Test Suite for testing the Creation of Paystations on System Admin for a Customer, including invalid cases and asserting Pay station is/isn't added to the Customer Paystation list 
...                 and verifying the type is correct if created.
...               Author: Johnson N  

Resource          ../../../data/data.robot
Resource          ../../../Keywords/CustomerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Library           Collections
Suite Teardown    Close Browser
Force Tags        acceptance-test
Suite Setup       Setup Browser And New Customer Account for Paystation Testing
Test Setup        Reload Page

*** Variables ***
${NEW CUST PASSWORD}    Password$2


*** Test Cases ***

Test 1: Paystation Creation Test
	[Tags]    smoke-test
	${test_serial}=    Auto-generate Paystation ID For Paystation Type: "LUKE II"
 	Given I search up Customer: "${test_customer_name}" and navigate to Pay Stations tab
 	When Create Paystation With Serial ID: "${test_serial}"
    And Reload Page
    Sleep    2
    Then Wait Until Keyword Succeeds    5s    1s     Paystation With ID: "${test_serial}" Should Be Visible On Paystation List
    And Paystation Serial ID: "${test_serial}" Should Have Paystation Type: "LUKE II" 


Test 2: Creating Duplicate Paystation Test
	${test_serial}=    Auto-generate Paystation ID For Paystation Type: "LUKE II"
	When Create Paystation With Serial ID: "${test_serial}"
	And Reload Page
	Sleep    2
    Then Paystation With ID: "${test_serial}" Should Be Visible On Paystation List
    When Create Paystation with Serial ID: "${test_serial}"
	Then Wait Until Keyword Succeeds    5s    1s    Attention! Popup Should Appear When Adding Already Existing Paystation: "${test_serial}"


Test 3: Paystation with Invalid ID Creation Test
	When Create Paystation With Serial ID: "ABCDEFGHIJKL"
	Then Attention! Popup Should Appear When Adding Invalid Paystation: "ABCDEFGHIJKL"


Test 4: Paystation with Long ID Creation Test
	When Create Paystation With Serial ID: "1111111111111"
	Then Attention! Popup Should Appear When Adding Long Paystation Serial ID: "1111111111111"

Test 5: Auto-generated Paystation Creation Test
	${random_serial}=    Create Auto-generated Paystation For Type: "SHELBY" 
	Sleep    2s
	And Reload Page
	Sleep    2s
	Then Paystation With ID: "${random_serial}" Should Be Visible On Paystation List
    And Wait Until Keyword Succeeds    5s    1s    Paystation Serial ID: "${random_serial}" Should Have Paystation Type: "SHELBY" 

Test 6: Hit Space bar in an empty form

	And Click Add Paystation(s) Button To Open Serial Number Form
	Then Add Paystation(s) Form Should Appear
	When Enter Paystation Serial Number: " "
	Then Attention! Popup Should Appear When Inputting Invalid Character in Add Paystation Form

Test 7: Attempt to enter invalid characters immediately in the form
	Given Click Add Paystation(s) Button To Open Serial Number Form
	Then Add Paystation(s) Form Should Appear
	When Enter Paystation Serial Number: "///"
	Then Attention! Popup Should Appear When Inputting Invalid Character in Add Paystation Form

Test 8: Add a list of 5 new Paystation with differnt Paystation Type
	@{paystation_types}    Create List    V1 LUKE    SHELBY    Radius LUKE    LUKE II    FRODO
 	${id_paystation_type_dict}=    Generate ID's for Paystation Types: "${paystation_types}"
 	${serial_ids}=    Get Dictionary Keys    ${id_paystation_type_dict}
 	Given I search up Customer: "${test_customer_name}" and navigate to Pay Stations tab	
 	When Create Paystations: "${serial_ids}"
 	And Reload Page
 	Then All Recorded Paystation ID's Should Be Created And Mapped To The Correct Paystation Type: "${id_paystation_type_dict}"

Test 9: Add a list of 10 paystations with 2 PS ID is duplicate
	${paystations}=    Mass-generate "9" Paystation IDs For Paystation Type: "LUKE II"
	${duplicate}=    Get From List    ${paystations}    0
	Append To List    ${paystations}    ${duplicate}
	When Create Paystations: "${paystations}"
	Then Message Response Alert Box For Duplicate Paystation: "${duplicate}" Should Appear
	When Reload Page
	Sleep    2s
	Then List Of Paystations "${paystations}" Should Be Created And Have Paystation Type: "LUKE II"
	And Only One Paystation with ID: "${duplicate}" Should Exist On The Paystation List


Test 10: Add a list of 5 new Paystations with all invalid 
	@{invalid_ids}    Create List    123     asdfsfsfsfasfsfsfsfsfasfsfsdf    asdfasf12313    12asfasd    sdf22223323332
	When Create Paystations: "${invalid_ids}"
	Then Alert Message Should Appear


Test 11: Add a list of 5 new paystations with random blank rows between paystations.
	${paystations}=    Mass-generate "5" Paystation IDs For Paystation Type: "LUKE II"

	When Create Paystations (with Blank spaces) : "${paystations}"
	And Reload Page
	Sleep    2s
	Then List Of Paystations "${paystations}" Should Be Created And Have Paystation Type: "LUKE II"

Test 12: Add a list of 50 paystations at once
	${paystations}=    Mass-generate "50" Paystation IDs For Paystation Type: "LUKE II"
	When Create Paystations: "${paystations}"
	And Sleep    2s
	And Reload Page
	Then List Of Paystations "${paystations}" Should Be Created And Have Paystation Type: "LUKE II"

*** Keywords ***

Setup Browser And New Customer Account for Paystation Testing
	# This keyword will set up a browser and brand new customer account with randomly generated name to be used for paystation creation testing. It will also set the customer name as a Suite Variable which can be referred to in a suite of tests
	Login as System Admin    systemadmin    Password$1    password
	${random}=    Evaluate    random.randint(0,9999)    modules=random
     Create Generic Customer    customer${random}    admin@customer${random}    Password$2
     Set Suite Variable    ${test_customer_name}    customer${random}

I search up Customer: "${customer_name}" and navigate to Pay Stations tab
	# This keyword will search up the customer and navigate to their Pay Stations tab
	# [Arguments]: 
	# ${customer_name} - the name of the customer being searched up
	Search Up Customer: "${customer_name}"
	Navigate to Tab: "Pay Stations"


Create Paystations (with Blank spaces) : "${list_of_paystations}"
	# Functions overall similarly to 'Create Paystations: "${list_of_paystations}"`, the only difference being that it includes a random number of blank lines between serial id`s in the Serial Number Forms during submission.  Pay stations should still be created, despite that.
	# [Arguments]: 
	# ${list_of_paystations} - a reference to a list object containing valid pay station serial numbers
	Click Add Paystation(s) Button To Open Serial Number Form
	Add Paystation(s) Form Should Appear
    Enter Paystation Serial Numbers With Random Blank Space: "${list_of_paystations}"
	Click 'Add Paystation' Button to add Paystations To Customer

Enter Paystation Serial Numbers With Random Blank Space: "${list_of_paystations}"
	# Similar to 'Enter Paystation Serial Numbers: "${list_of_paystations}"', this is a helper keyword that will simply input the list of paystations into the serial number form, with random (0-3) blank spaces between each serial number entry
	# [Arguments]:
	# ${list_of_paystations} - a list of paystation serial numbers	
	${paystations}=    Set Variable    ${EMPTY}
	:FOR    ${paystation}    IN     @{list_of_paystations}
	\    ${paystations}=    Catenate    SEPARATOR=    ${paystations}    ${paystation}
	\	 ${paystations}=    Catenate    SEPARATOR=    ${paystations}    \n
	\    ${random}=    Evaluate    random.randint(0,3)    modules=random
	\    ${paystations}=    Catenate "${random}" Additional Lines To String: "${paystations}"
	Input Text    formSerialNumbers    ${paystations}


Catenate "${n}" Additional Lines To String: "${string}"
	# Mainly functions as a helper keyword to 'Enter Paystation Serial Numbers With Random Blank Space: "${list_of_paystations}"'. Helps add the new lines between pay station serial Id's
	# [Arguments]:
	# ${n} - the number of additional lines to add to a string
	# ${string} - the string to add additional new-line characters to
	:FOR    ${INDEX}    IN RANGE    0    ${n}
	\    ${string}=    Catenate    SEPARATOR=    ${string}    \n
	[return]    ${string}
