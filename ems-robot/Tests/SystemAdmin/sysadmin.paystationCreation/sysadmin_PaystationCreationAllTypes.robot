*** Settings ***
Documentation		Test Suite for testing the creation of all known Paystation types and asserting the paystation is created and type matches
...               Author: Johnson N  

Resource          ../../../data/data.robot
Resource          ../../../Keywords/CustomerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Suite Teardown    Close Browser
Force Tags        acceptance-test
Suite Setup       Run Keywords    Login as System Admin    systemadmin    Password$1    password    AND    Search Up Customer: "oranj"
Test Setup    Navigate To Tab: "Pay Stations"
Test Template    Create Paystation Type

*** Test Cases ***    PAYSTATION TYPE
V1 LUKE Test          V1 LUKE
SHELBY Test			  SHELBY
LUKE II Test	      LUKE II
FRODO Test			  FRODO
Test/Demo Test		  Test/Demo
Virtual Test          Virtual





*** Keywords ***

Create Paystation Type
	# This is a template keyword used to test the creation and verification of all pay station types. This will include auto-generating the id for a given pay station type, creating that pay station id, and verifying the paystation is on the paystation list and is of the correct Pay station type:
	# ${paystation_type} - Paystation type, such as SHELBY or LUKE II
	[Arguments]    ${paystation_type}
	${id}=    Create Auto-generated Paystation for Type: "${paystation_type}"
	Sleep    2
    Wait Until Keyword Succeeds    5s    1s    Paystation With ID: "${id}" Should Be Visible On Paystation List
	Paystation Serial ID: "${id}" Should Have Paystation Type: "${paystation_type}" 

