*** Settings ***
# Summary: Automation for EMS-10391
# Author: Billy Hoang

Resource          ../../../data/data.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Library           DatabaseLibrary
Suite Teardown    Close Browser

Test Setup    Run Keywords
...               Login As System Admin    systemadmin    Password$1    password
...         AND   Search Up Customer: "${G_CUSTOMER_NAME}"
...         AND   Click Alerts Tab

Test Teardown    Close Browser



*** Test Case ***

# Steps to reproduce:
# 1. Log into system admin account
# 2. Search for and select to view a customer account
# 3. Go to Alerts tab
# 4. Select to edit an alert (Pay Station Alert)
# 5. Attempt to add an email address to the alert
# 6. Attempt to remove an email address from an alert
#
# Results: System Admin account is not able to add or remove email addresses from an alert
#
# Expected Results: System Admin account should be able to add or remove email addresses from an alert

Test EMS-10391
    # Assumes the default Pay Station Alert is present after the init file has been ran

    ${email}=  Generate Random Email
    Add Email To Alert  Pay Station Alert  ${email}
    Remove Email From Alert  Pay Station Alert  ${email}