*** Settings ***
Documentation       Test Suite for testing the creation of all types of customer, including child, parent, trial etc... and asserting the customer is created and is of the right type
...               Test Suite also covers services for standalone, child and parent customers and asserting the chosen services are activated along with the customer"s creation
...               Author: Johnson N  

Resource          ../../../data/data.robot
Resource          ../../../Keywords/CustomerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Force Tags        acceptance-test
Suite Setup    Login as System Admin    systemadmin    Password$1    password
Suite Teardown    Close Browser
Test Setup    Go To System Admin Page


*** Variables ***
${NEW_CUST PASSWORD}    Password$2

*** Test Cases ***

Parent Customer Creation Scenario 1
    [Tags]    smoke-test
    ${sec}=    Get Time    epoch
    Given I Create Customer With Name: "name${sec}" And Password: "$1asdfPOIU" And Status: "Enabled"
    And Select Parent Flag 
    And Saved Customer Creation
    And Reload Page
    Then Parent Customer: "name${sec}" Should Be Created

Parent Customer Creation Scenario 2
    [Tags]    smoke-test
    ${sec}=    Get Time    epoch
    Given I Create Customer With Name: "name${sec}" And Password: "$1asdfPOIU" And Status: "Disabled"
    And Select Parent Flag
    And Saved Customer Creation
    And Reload Page
    Then Parent Customer: "name${sec}" Should Be Created


Parent Customer Creation Scenario 3
    [Tags]    smoke-test
    ${sec}=    Get Time    epoch
    Given I Create Customer With Name: "name${sec}" And Password: "$1asdfPOIU" And Status: "Trial"
    And Set Trial End Date As: "12/31/2018"
    And Select Parent Flag
    And Saved Customer Creation
    And Reload Page
    Then Parent Customer: "name${sec}" Should Be Created
    And Trial Date Is "12/31/2018"

Parent Customer Creation Scenario 4
    [Tags]    smoke-test
    ${sec}=    Get Time    epoch
    Given I Create Customer With Name: "name${sec}" And Password: "$1asdfPOIU" And Status: "Trial"
    And Set Trial End Date As: "02/29/2020"
    And Select Parent Flag
    And Saved Customer Creation
    And Reload Page
    Then Parent Customer: "name${sec}" Should Be Created
    And Trial Date Is "02/29/2020"

Parent Customer Creation Scenario 5
    [Tags]    smoke-test
    ${sec}=    Get Time    epoch
    Given I Create Customer With Name: "name${sec}" And Password: "$1asdfPOIU" And Status: "Trial"
    And Set Trial End Date As: "01/01/2020"
    And Select Parent Flag
    And Saved Customer Creation
    And Reload Page
    Then Parent Customer: "name${sec}" Should Be Created
    And Trial Date Is "01/01/2020"



Parent Customer Creation Scenario with Different Time Zones
    [Tags]    smoke-test
    ${sec}=    Get Time    epoch
    Given I Create Customer With Name: "name${sec}" And Password: "asdf!@2POIU" And Status: "Disabled"
    And Select Parent Flag
    And Select Timezone As "Choose"
    And Saved Customer Creation
    And Reload Page
    Then Parent Customer: "name${sec}" Should Be Created
    And Timezone Setting Is "Choose"

Parent Customer Creation Scenario with Different Time Zones 2
    [Tags]    smoke-test
    ${sec}=    Get Time    epoch
    Given I Create Customer With Name: "name${sec}" And Password: "12f!@POIU" And Status: "Enabled"
    And Select Parent Flag
    And Select Timezone as "GMT"
    And Saved Customer Creation
    And Reload Page
    Then Parent Customer: "name${sec}" Should Be Created
    And Timezone Setting Is "GMT"


Parent Customer Creation Scenario with Different Time Zones 3
    [Tags]    smoke-test
    ${sec}=    Get Time    epoch
    Given I Create Customer With Name: "name${sec}" And Password: "12f!@PO12123123" And Status: "Enabled"
    And Select Parent Flag
    And Select Timezone As "US/Pacific"
    And Saved Customer Creation
    And Reload Page
    Then Parent Customer: "name${sec}" Should Be Created
    And Timezone Setting Is "US/Pacific"

Parent Customer Creation Scenario with Services 1
    [Tags]    smoke-test
    @{SERVICES}    Create List    Alerts    Coupons    Smart Cards    3rd Party Pay-By-Cell Integration    AutoCount Integration    Batch Credit Card Processing
    ${sec}=    Get Time    epoch
    Given I Create Customer With Name: "name${sec}" And Password: "@12f!@PO12@" And Status: "Enabled"
    And Select Parent Flag
    And Select Timezone As "Canada/Eastern"
    And Select Services: "${SERVICES}"
    And Saved Customer Creation
    And Reload Page
    Then Parent Customer: "name${sec}" Should Be Created
    And Timezone Setting Is "Canada/Eastern"
    And Following Services Should Be Activated: "${SERVICES}"


Parent Customer Creation Scenario with Services 2
    [Tags]    smoke-test
    @{SERVICES}    Create List    Coupons    Extend-By-Phone    Real-Time Credit Card Processing    Standard Reports
    ${sec}=    Get Time    epoch
    Given I Create Customer With Name: "name${sec}" And Password: "123"<td>qaZ" And Status: "Enabled"
    And Select Parent Flag
    And Select Timezone As "US/Indiana-Starke"
    And Select Services: "${SERVICES}"
    And Saved Customer Creation
    And Reload Page
    Then Parent Customer: "name${sec}" Should Be Created
    And Timezone Setting Is "US/Indiana-Starke"
    And Following Services Should Be Activated: "${SERVICES}"

Parent Customer Creation Scenario with Services 3
    [Tags]    smoke-test
    @{SERVICES}    Create List    Digital API: Read    Digital API: Write    Digital API: XChange    Mobile: Digital Collect
    ${sec}=    Get Time    epoch
    Given I Create Customer With Name: "name${sec}" And Password: "Password$2" and Status: "Enabled"
    And Select Parent Flag
    And Select Timezone As "Canada/Atlantic"
    And Select Services: "${SERVICES}"
    And Saved Customer Creation
    And Reload Page
    Then Parent Customer: "name${sec}" Should Be Created
    And Timezone Setting Is "Canada/Atlantic"
    And Following Services Should Be Activated: "${SERVICES}"

Parent Customer Creation Scenario with Services 4
    [Tags]    smoke-test
    @{SERVICES}    Create List    FLEX Integration    Online Pay Station Configuration    Passcards    Smart Cards
    ${sec}=    Get Time    epoch
    Given I Create Customer With Name: "name${sec}" And Password: "Password$2" and Status: "Enabled"
    And Select Parent Flag
    And Select Timezone As "Canada/East-Saskatchewan"
    And Select services: "${SERVICES}"
    And Saved Customer Creation
    And Reload Page
    Then Parent Customer: "name${sec}" Should Be Created
    And Timezone Setting Is "Canada/East-Saskatchewan"
    And Following Services Should Be Activated: "${SERVICES}"

Parent Customer Creation Scenario with Services 5
    [Tags]    smoke-test
    @{SERVICES}    Create List    Real-Time Campus Card Processing    Real-Time Credit Card Processing    Standard Reports
    ${sec}=    Get Time    epoch
    Given I Create Customer With Name: "name${sec}" And Password: "Password$2" And Status: "Disabled"
    And Select Parent Flag
    And Select Timezone As "Canada/Atlantic"
    And Select Services: "${SERVICES}"
    And Saved Customer Creation
    And Reload Page
    Then Parent Customer: "name${sec}" Should Be Created
    And Timezone Setting Is "Canada/Atlantic"
    And Following Services Should Be Activated: "${SERVICES}"




Child Customer Craetion Scenario 1
    [Tags]    smoke-test
    ${sec}=    Get Time    epoch
    Given I Create Customer With Name: "parent${sec}" And Password: "Password$2" and Status: "Disabled"
    And Select Parent Flag
    And Saved Customer Creation
    And Reload Page
    When I Create Customer With Name: "child${sec}" And Password: "Password$2" and Status: "Enabled"
    And Enter Parent Customer Name: "parent${sec}" For Child Customer Creation
    And Saved Customer Creation
    And Reload Page
    Then Child: "child${sec}" Assigned To Parent "parent${sec}" Should Be Created





Child Customer Craetion Scenario 2
    @{SERVICES}    Create List    Real-Time Campus Card Processing    Real-Time Credit Card Processing    Standard Reports    FLEX Integration
    [Tags]    smoke-test
    ${sec}=    Get Time    epoch
    Given I Create Customer With Name: "parent${sec}" And Password: "Password$2" And Status: "Enabled"
    And Select Parent Flag
    And Select Services: "${SERVICES}"
    And Saved Customer Creation
    And Reload Page
    When I Create Customer With Name: "child${sec}" And Password: "Password$2" And Status: "Disabled"
    And Enter Parent Customer Name: "parent${sec}" For Child Customer Creation
    And Select Timezone As "Canada/Atlantic"
    And Select Services: "${SERVICES}"
    And Saved Customer Creation
    And Reload Page
    Then Child: "child${sec}" Assigned To Parent "parent${sec}" Should Be Created
    And Timezone Setting Is "Canada/Atlantic"
    And Following Services Should Be Activated: "${SERVICES}"






@{SERVICES}    Create List    3rd Party Pay-By-Cell Integration    Alerts    AutoCount Integration    Batch Credit Card Processing    Coupons    Digital API: Read     Digital API: Write    Digital API: XChange    Extend-By-Phone    FLEX Integration    Mobile: Digital Collect    Online Pay Station Configuration    Passcards    Real-Time Campus Card Processing    Real-Time Credit Card Processing     Smart Cards    Standard Reports
${sec}=    Get Time    epoch


Creates A Randomly Generated Parent
    Open New Customer Form
    Select Parent Flag
    Fill In Required Customer Fields    ${sec} Parent    ${sec}Parent    Password$2    Password$2    Enabled    N/A    N/A     Canada/Pacific
    Select Services: "${SERVICES}"
    Saved Customer Creation
    Reload Page

Create Generic Customer With Above Parent
    Open New Customer Form
    Fill In Required Customer Fields    ${sec} Customer    admin@${sec}    Password$2    Password$2    Enabled    N/A    N/A    Canada/Pacific
    Select Services: "${SERVICES}"
    Saved Customer Creation
    Reload Page
    # Need To Assign Parent to the new customer


