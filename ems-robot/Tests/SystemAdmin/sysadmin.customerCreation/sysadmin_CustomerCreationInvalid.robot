*** Settings ***
Documentation    Test Suite for testing invalid combinations of required customer 
...				 data during Customer Creation and asserting that all
...              combinations are considered invalid and such customer 
...              credentials should not be created on Iris
...               Author: Johnson N  

Resource          ../../../data/data.robot
Resource          ../../../Keywords/CustomerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Library           Selenium2Library


Suite Setup    Login as System Admin    systemadmin    Password$1    password
Suite Teardown    Close Browser
Test Setup    Reload Page
Force Tags        acceptance-test
Test Template     Create Invalid Customer


*** Test Cases ***           CUSTOMER NAME        ADMIN NAME          DEFAULT PASSWORD       CONFIRM PASSWORD          ACCOUNT STATUS             TRIAL END DATE     		
Empty User Name              ${EMPTY}             admin@customer2     Password$2			 Password$2                Enabled                    ${EMPTY}
Empty Admin name             customer2            ${EMPTY}            Password$2             Password$2                Enabled                    ${EMPTY}
Empty Password               customer2            admin@customer2     ${EMPTY}               Password$2                Enabled                    ${EMPTY}
Empty Confirm Password       customer2            admin@customer2     Password$2             ${EMPTY}                  Enabled                    ${EMPTY}
Empty Account Status         customer2            admin@customer2     Password$2             Password$2                ${EMPTY}                   ${EMPTY}
Invalid Account Status       customer2            admin@customer2     Password$2             Password$2                pickle                     ${EMPTY}
User Name Too Short          c1                   admin@customer2     Password$2             Password$2                Enabled                    ${EMPTY}
Admin Name Too Short         customer2            c2                  Password$2             Password$2                Enabled                    ${EMPTY}
Invalid Password             customer2            admin@customer2     password               password                  Disabled                   ${EMPTY}
Mismatched Passwords         customer2            admin@customer2     Password$2             Password$1                Enabled                    ${EMPTY}
Invalid 1			         c 		              c 			      Password$2             Password$1                Trial                      ${EMPTY}
Invalid 2			         c 		              c 			      c                      d 		                   Enabled                    ${EMPTY}
Invalid Password 2           customer2            admin@customer2     Password               Password                  Enabled                    ${EMPTY}
Invalid Password 3           customer2            admin@customer2     aassWord$              Password                  Enabled                    ${EMPTY}
Invalid Password 4           customer2            admin@customer2     aassW$123              Password                  Enabled                    ${EMPTY}
Invalid Password 5           customer2            admin@customer2     123456789aasdfasdfasdfasdf13124214              ~!@#$%^&*()_+[]\:">?#%$*(&^)(&*)("                  Enabled                        ${EMPTY}
Invalid User Name 2			 !@#$%^&*()__)+(}{|":<?>"})			!@#$%^&*()_{}|{:">?<"}			!@#()_}{":>?"}		~!@#%$_}|{":L?>"		Enabled                        ${EMPTY}
Invalid 3			         customer2            admin@customer2     aassWord$              Password                  Enabled                    ${EMPTY}
Invalid 4		             ${EMPTY}             ${EMPTY}		      ${EMPTY}               ${EMPTY}                  Enabled                    ${EMPTY}
Invalid 5		             asdfasfasfsffasfdasfsfsfafdasfasf             asadfas;lfkjsd;lfj29342894af;lasfkjas;lfjaslf		      ${EMPTY}               ${EMPTY}                  Enabled                        ${EMPTY}
Invalid Trial Date 1         customerX           admin@customerX     Password$2              Password$2                Trial                     99/99/99/
Invalid Trial Date 2         customerX           admin@customerX     Password$2              Password$2                Trial                     /////////
Invalid Trial Date 3         customerX           admin@customerX     Password$2              Password$2                Trial                     12334456
Invalid Trial Date 4         customerX           admin@customerX     Password$2              Password$2                Trial                     123/456/789
Invalid Trial Date 5         customerX           admin@customerX     Password$2              Password$2                Trial                     ${EMPTY}


*** Keywords ***
Create Invalid Customer
    # This is a template keyword that is used to test combinations of customer required information with invalid arguments/fields, such as a customer name being too short, trial end date being a string, or password being invalid. This keyword handles opening the customer form, filling out the required information with one or more argument being invalid and attempting to save the invalid customer. It'll than verify the appearance of an alert message, reload the page and attempt to search up the customer on system admin, to find that the page does not contain any customer with that name
    # [Arguments]:
    # ${customer} - The name of the customer 
    # ${admin} - The username for the customer that will be used for logging into with Iris. 
    # ${default_password} - the default password that is entered in the New Customer Form for first-time logins for new customers. 
    # ${confirm_password} - the password confirmation that is entered in the New Customer Form for first-time logins for new customers. Should be the same as ${default_password} but for testing invalids, does not have to be 
    # ${account_status} - The account status for new customer, either 'Enabled', 'Disabled' or 'Trial 
    # ${trial_end_date} - the trial end date for trial customer, should be set to ${EMPTY} by default 
    [Arguments]    ${customer}    ${admin}    ${default_password}    ${confirm_password}    ${account_status}    ${trial_end_date}
    Open New Customer Form
    Fill In Required Customer Fields    ${customer}    ${admin}    ${default_password}    ${confirm_password}    ${account_status}    ${trial_end_date}    Canada/Pacific
    Saved Customer Creation
    Sleep    2s
    Wait Until Keyword Succeeds    5s    1s    Alert Message Should Appear
    Reload Page
    Sleep    1s
    Run Keyword If    '${customer}' != ''    Customer Should Not Have Been Created    ${customer}


Customer Should Not Have Been Created
    # This is a helper keyword used mainly to assist 'Create Invalid Customer'. This keyword handles the check that the customer is not created when attempting to search it
    # [Arguments]:
    # ${customer} - The name of the customer to verify
    [Arguments]    ${customer}
    Wait Until Element is Visible    css=#search    timeout=3
    Input Text    css=#search    ${customer}
    Page Should Not Contain Element    xpath=.//a[text()='${customer}']
    Sleep    2s


