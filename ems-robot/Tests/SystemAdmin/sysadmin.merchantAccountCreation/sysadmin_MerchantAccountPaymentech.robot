*** Settings ***
# Summary: Test Suite for verifying the creation and deletion of a Paymentech Merchant Account
# Author: Billy Hoang
# Time: Dec 3, 2015

Resource          ../../../data/data.robot
Resource          ../../../Keywords/CustomerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Suite Teardown    Close Browser
Force Tags        smoke-test
Suite Setup       Run Keywords    Login as System Admin    systemadmin    Password$1    password    AND    Search Up Customer: "oranj"
Test Setup    Reload Page


*** Variables ***

${er1}    Processor does not exist
${er2}    must be between 5 to 30 characters long

${ACCOUNT_ID_PATH}    field1
${TERMINAL_ID_PATH}    field2
${USER_PATH}    field3
${PASSWORD_PATH}    field4
${CLIENT_NUMBER_PATH}    field5

*** Test Cases ***


Add Merchant Account: Paymentech - Happy (Random)
    [tags]    smoke-test

    ${random}    Get Time    epoch

    Set Test Variable    ${account_name}    ${random}
    # status: Enabled or Disabled
    Set Test Variable    ${status}         Enabled
    Set Test Variable    ${account_id}     ${random}
    Set Test Variable    ${terminal_id}    ${random}
    Set Test Variable    ${user}           ${random}
    Set Test Variable    ${password}       ${random}
    Set Test Variable    ${client_number}  ${random}

    Given I Go to Add Merchant Account Form
    And I Set Account Name to "${account_name}"
    And I Set Status To: "${status}"
    And I Select Processor: "Paymentech"
    And I Select Close Time: "12:00\ \ am"
    And I Select Time Zone: "Canada/Atlantic"
    And Input Text    ${ACCOUNT_ID_PATH}    ${account_id}
    And Input Text    ${TERMINAL_ID_PATH}    ${terminal_id}
    And Input Text    ${USER_PATH}    ${user}
    And Input Text    ${PASSWORD_PATH}    ${password}
    And Input Text    ${CLIENT_NUMBER_PATH}    ${client_number}
    When I Click Add Account
    Then New Paymentech Account Listing Should Be Visible    ${account_name}    ${status}    12:00 am    Canada/Atlantic    ${account_id}    ${terminal_id}    ${user}    ${password}    ${client_number}
    And Merchant Account Test Fails    ${account_name}    Paymentech
    And Merchant Account Is Deleted    ${account_name}    Paymentech


# Note: Need to work on getting close time and time zone as a test variable
# Summary: Adds an Alliance Merchant Account
# Assertions:
# - Verifies Info is saved by clicking on it's listing
# - Verifies that the test button fails
# Tear Down:
# - Deletes the created merchant account
Add Merchant Account: Paymentech - Happy
    [tags]    smoke-test

    Set Test Variable    ${account_name}    AllianceAutomationxx
    # status: Enabled or Disabled
    Set Test Variable    ${status}         Enabled
    Set Test Variable    ${account_id}     1234567
    Set Test Variable    ${terminal_id}    1234567
    Set Test Variable    ${user}           1234567
    Set Test Variable    ${password}       1234567
    Set Test Variable    ${client_number}  0009

    Given I Go to Add Merchant Account Form
    And I Set Account Name to "${account_name}"
    And I Set Status To: "${status}"
    And I Select Processor: "Paymentech"
    And I Select Close Time: "12:00\ \ am"
    And I Select Time Zone: "Canada/Atlantic"
    And Input Text    ${ACCOUNT_ID_PATH}    ${account_id}
    And Input Text    ${TERMINAL_ID_PATH}    ${terminal_id}
    And Input Text    ${USER_PATH}    ${user}
    And Input Text    ${PASSWORD_PATH}    ${password}
    And Input Text    ${CLIENT_NUMBER_PATH}    ${client_number}
    When I Click Add Account
    Then New Paymentech Account Listing Should Be Visible    ${account_name}    ${status}    12:00 am    Canada/Atlantic    ${account_id}    ${terminal_id}    ${user}    ${password}    ${client_number}
    And Merchant Account Test Fails    ${account_name}    Paymentech
    And Merchant Account Is Deleted    ${account_name}    Paymentech













*** Keywords ***

New Paymentech Account Listing Should Be Visible
    [arguments]    ${account_name}    ${status}    ${close_time}    ${time_zone}    ${account_id}    ${terminal_id}    ${user}    ${password}    ${client_number}
    Click Element    xpath=//ul[@id='merchantAccountList']//p[contains(text(),'${account_name}')]
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    css=#mrchntAccntFormArea
    Sleep    1
    Verify Label And Label Value    Account    ${account_name}
    Verify Label And Label Value    Account    ${status}
    Verify Label And Label Value    Processor    Paymentech

    # Verification is Invalid: Refer to: EMS-10933
    #Verify Label And Label Value    Account    ${close_time}
    #Verify Label And Label Value    Account    ${time_zone}

    Verify Label And Label Value    Account ID    ${account_id}
    Verify Label And Label Value    Terminal ID    ${terminal_id}
    Verify Label And Label Value    User    ${user}
    Verify Label And Label Value    Password    ${password}
    Verify Label And Label Value    Client Number    ${client_number}
    Click Element    xpath=//span[contains(text(),'Ok')]







