*** Settings ***
Documentation       Test Suite for testing the Creation and Deletion of Merchant Accounts on System Admin for a Customer
...            Assumes no merchant account is already associated to that customer

Resource          ../../../data/data.robot
Resource          ../../../Keywords/CustomerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Suite Teardown    Close Browser
Force Tags        acceptance-test
Suite Setup       Run Keywords    Login as System Admin    systemadmin    Password$1    password    AND    Search Up Customer: "oranj"
Test Setup    Reload Page
Test Template    Create Merchant Account With No Processor    

*** Variables ***

*** Test Cases ***      ACCOUNT_NAME        STATUS
Happy                   AccountName         ENABLE
Empty Account Name      ${EMPTY}            ENABLE
No Enable               AccountName         ${EMPTY}
All Empty               ${EMPTY}            ${EMPTY}





*** Keywords ***

Create Merchant Account With No Processor
    [arguments]    ${account_name}    ${status}
    Click Element    css=#btnAddMrchntAccnt
    Sleep    1
    Input Text    css=#account    ${account_name}
    Run Keyword If    '${status}' == 'ENABLE'    Click Element    css=#statusCheck
    Click Element    xpath=//span[contains(text(),'Add Account')]
    Sleep   1
    Alert Message Should Appear
    
