*** Settings ***
# Summary: Test Suite for verifying the creation and deletion of an BlackBoard Merchant Account
# Author: Billy Hoang
# Time: Dec 3, 2015

Resource          ../../../data/data.robot
Resource          ../../../Keywords/CustomerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Suite Teardown    Close Browser
Force Tags        smoke-test
Suite Setup       Run Keywords    Login as System Admin    systemadmin    Password$1    password    AND    Search Up Customer: "oranj"
Test Setup    Reload Page


*** Variables ***

${er1}    Processor does not exist
${er2}    must be between 5 to 30 characters long

${ENCRYPTION_KEY_PATH}    field1
${SERVER_IP_PATH}    field2
${TENDER_NUMBER_PATH}    field3
${VENDOR_NUMBER_PATH}    field4

*** Test Cases ***


# Note: Need to work on getting close time and time zone as a test variable
# Summary: Adds an BlackBoard Merchant Account
# Assertions:
# - Verifies Info is saved by clicking on it's listing
# - Verifies that the test button fails
# Tear Down:
# - Deletes the created merchant account
Add Merchant Account: BlackBoard - Happy
    [tags]    smoke-test

    Set Test Variable    ${account_name}    BlackBoardAutomation
    # status: Enabled or Disabled
    Set Test Variable    ${status}          Enabled

    Set Test Variable    ${encryption_key}     1234567
    Set Test Variable    ${server_ip}     1234567
    Set Test Variable    ${tender_number}     1234567
    Set Test Variable    ${vendor_number}     1234567

    Given I Go to Add Merchant Account Form
    And I Set Account Name to "${account_name}"
    And I Set Status To: "${status}"
    And I Select Processor: "BlackBoard"
    And I Select Close Time: "12:00\ \ am"
    And I Select Time Zone: "Canada/Atlantic"

    And Input Text    ${ENCRYPTION_KEY_PATH}    ${encryption_key}
    And Input Text    ${SERVER_IP_PATH}    ${server_ip}
    And Input Text    ${TENDER_NUMBER_PATH}    ${tender_number}
    And Input Text    ${VENDOR_NUMBER_PATH}    ${vendor_number}

    When I Click Add Account
    Then New BlackBoard Account Listing Should Be Visible    ${account_name}    ${status}    12:00 am    Canada/Atlantic    ${encryption_key}    ${server_ip}    ${tender_number}    ${vendor_number}
    # Note: No testing feature for campus cards
    #And Merchant Account Test Fails    ${account_name}    BlackBoard
    And Merchant Account Is Deleted    ${account_name}    BlackBoard








*** Keywords ***

New BlackBoard Account Listing Should Be Visible
    [arguments]    ${account_name}    ${status}    ${close_time}    ${time_zone}    ${encryption_key}    ${server_ip}    ${tender_number}    ${vendor_number}
    Click Element    xpath=//ul[@id='merchantAccountList']//p[contains(text(),'${account_name}')]
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    css=#mrchntAccntFormArea
    Sleep    1
    Verify Label And Label Value    Account    ${account_name}
    Verify Label And Label Value    Account    ${status}
    Verify Label And Label Value    Processor    BlackBoard

    # Verification is Invalid: Refer to: EMS-10933
    #Verify Label And Label Value    Account    ${close_time}
    #Verify Label And Label Value    Account    ${time_zone}

    Verify Label And Label Value    Encryption Key    ${encryption_key}
    Verify Label And Label Value    Server IP    ${server_ip}
    Verify Label And Label Value    Tender Number    ${tender_number}
    Verify Label And Label Value    Vendor Number    ${vendor_number}

    Click Element    xpath=//span[contains(text(),'Ok')]







