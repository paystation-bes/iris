*** Settings ***
# Summary: Test Suite for verifying the creation and deletion of an Authorize.Net Merchant Account
# Author: Billy Hoang
# Time: Dec 3, 2015

Resource          ../../../data/data.robot
Resource          ../../../Keywords/CustomerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Suite Teardown    Close Browser
Force Tags        smoke-test
Suite Setup       Run Keywords    Login as System Admin    systemadmin    Password$1    password    AND    Search Up Customer: "oranj"
Test Setup    Reload Page


*** Variables ***

${er1}    Processor does not exist
${er2}    must be between 5 to 30 characters long

${LOGIN_ID_PATH}    field1
${PROCESSOR_EXPAND_PATH}    field2Expand
${PROCESSOR_PATH}    field2
${TRANSACTION_KEY_PATH}    field3

*** Test Cases ***


# Note: Need to work on getting close time and time zone as a test variable
# Summary: Adds an Authorize.Net Merchant Account
# Assertions:
# - Verifies Info is saved by clicking on it's listing
# - Verifies that the test button fails
# Tear Down:
# - Deletes the created merchant account
Add Merchant Account: Authorize.Net - Happy
    [tags]    smoke-test

    Set Test Variable    ${account_name}    Authorize.NetAutomation
    # status: Enabled or Disabled
    Set Test Variable    ${status}          Enabled

    Set Test Variable    ${login_id}     1234567
    Set Test Variable    ${processor}     Chase Paymentech Solutions
    Set Test Variable    ${transaction_key}    1234567

    Given I Go to Add Merchant Account Form
    And I Set Account Name to "${account_name}"
    And I Set Status To: "${status}"
    And I Select Processor: "Authorize.Net"
    And I Select Close Time: "12:00\ \ am"
    And I Select Time Zone: "Canada/Atlantic"

    And Input Text    ${LOGIN_ID_PATH}    ${login_id}
    And Input Text    ${PROCESSOR_PATH}    ${processor}
    And Input Text    ${TRANSACTION_KEY_PATH}    ${transaction_key}

    When I Click Add Account
    Then New Authorize.Net Account Listing Should Be Visible    ${account_name}    ${status}    12:00 am    Canada/Atlantic    ${login_id}    ${processor}    ${transaction_key}
    And Merchant Account Test Fails    ${account_name}    Authorize.Net
    And Merchant Account Is Deleted    ${account_name}    Authorize.Net








*** Keywords ***

New Authorize.Net Account Listing Should Be Visible
    [arguments]    ${account_name}    ${status}    ${close_time}    ${time_zone}    ${login_id}    ${processor}    ${transaction_key}
    Click Element    xpath=//ul[@id='merchantAccountList']//p[contains(text(),'${account_name}')]
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    css=#mrchntAccntFormArea
    Sleep    1
    Verify Label And Label Value    Account    ${account_name}
    Verify Label And Label Value    Account    ${status}
    Verify Label And Label Value    Processor    Authorize.Net

    # Verification is Invalid: Refer to: EMS-10933
    #Verify Label And Label Value    Account    ${close_time}
    #Verify Label And Label Value    Account    ${time_zone}

    Verify Label And Label Value    Login ID    ${login_id}
    Verify Label And Label Value    Processor    ${processor}
    Verify Label And Label Value    Transaction Key    ${transaction_key}

    Click Element    xpath=//span[contains(text(),'Ok')]







