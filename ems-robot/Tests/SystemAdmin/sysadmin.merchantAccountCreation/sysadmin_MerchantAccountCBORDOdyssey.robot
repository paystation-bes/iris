*** Settings ***
# Summary: Test Suite for verifying the creation and deletion of an CBORD - Odyssey Merchant Account
# Author: Billy Hoang
# Time: Dec 3, 2015

Resource          ../../../data/data.robot
Resource          ../../../Keywords/CustomerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Suite Teardown    Close Browser
Force Tags        smoke-test
Suite Setup       Run Keywords    Login as System Admin    systemadmin    Password$1    password    AND    Search Up Customer: "oranj"
Test Setup    Reload Page


*** Variables ***

${er1}    Processor does not exist
${er2}    must be between 5 to 30 characters long

${SERVER_IP_PATH}    field1
${PORT_NUMBER_PATH}    field2
${PROVIDER_NAME_PATH}    field3
${LOCATION_PATH}    field4
${CODE_MAP_PATH}    field5

*** Test Cases ***


# Note: Need to work on getting close time and time zone as a test variable
# Summary: Adds an CBORD - Odyssey Merchant Account
# Assertions:
# - Verifies Info is saved by clicking on it's listing
# - Verifies that the test button fails
# Tear Down:
# - Deletes the created merchant account
Add Merchant Account: CBORD - Odyssey - Happy
    [tags]    smoke-test

    Set Test Variable    ${account_name}    CBORD-CSOdyssey
    # status: Enabled or Disabled
    Set Test Variable    ${status}          Enabled

    Set Test Variable    ${server_ip}     1234567
    Set Test Variable    ${port_number}     1234567
    Set Test Variable    ${provider_name}     1234567
    Set Test Variable    ${location}    1234567
    Set Test Variable    ${code_map}    1234567

    Given I Go to Add Merchant Account Form
    And I Set Account Name to "${account_name}"
    And I Set Status To: "${status}"
    And I Select Processor: "CBORD - Odyssey"

    And Input Text    ${SERVER_IP_PATH}    ${server_ip}
    And Input Text    ${PORT_NUMBER_PATH}    ${port_number}
    And Input Text    ${PROVIDER_NAME_PATH}    ${provider_name}
    And Input Text    ${LOCATION_PATH}    ${location}
    And Input Text    ${CODE_MAP_PATH}    ${code_map}

    When I Click Add Account
    Then New CBORD - Odyssey Account Listing Should Be Visible    ${account_name}    ${status}    ${server_ip}    ${port_number}    ${provider_name}    ${location}    ${code_map}
    # Note: No testing feature for campus cards
    #And Merchant Account Test Fails    ${account_name}    CBORD - Odyssey
    And Merchant Account Is Deleted    ${account_name}    CBORD - Odyssey








*** Keywords ***

New CBORD - Odyssey Account Listing Should Be Visible
    [arguments]    ${account_name}    ${status}    ${server_ip}    ${port_number}      ${provider_name}    ${location}    ${code_map}
    Click Element    xpath=//ul[@id='merchantAccountList']//p[contains(text(),'${account_name}')]
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    css=#mrchntAccntFormArea
    Sleep    1
    Verify Label And Label Value    Account    ${account_name}
    Verify Label And Label Value    Account    ${status}
    Verify Label And Label Value    Processor    CBORD - Odyssey

    # Verification is Invalid: Refer to: EMS-10933
    #Verify Label And Label Value    Account    ${close_time}
    #Verify Label And Label Value    Account    ${time_zone}

    Verify Label And Label Value    Server IP    ${server_ip}
    Verify Label And Label Value    Port Number    ${port_number}
    Verify Label And Label Value    Provider Name    ${provider_name}
    Verify Label And Label Value    Location    ${location}
    Verify Label And Label Value    Code Map    ${code_map}

    Click Element    xpath=//span[contains(text(),'Ok')]







