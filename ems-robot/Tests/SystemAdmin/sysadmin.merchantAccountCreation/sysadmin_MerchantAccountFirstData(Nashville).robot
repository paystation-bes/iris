*** Settings ***
# Summary: Test Suite for verifying the creation and deletion of an First Data (Nashville) Merchant Account
# Author: Billy Hoang
# Time: Dec 3, 2015

Resource          ../../../data/data.robot
Resource          ../../../Keywords/CustomerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Suite Teardown    Close Browser
Force Tags        smoke-test
Suite Setup       Run Keywords    Login as System Admin    systemadmin    Password$1    password    AND    Search Up Customer: "oranj"
Test Setup    Reload Page


*** Variables ***

${er1}    Processor does not exist
${er2}    must be between 5 to 30 characters long

${COMPANY_ID_PATH}    field1
${TERMINAL_ID_PATH}    field2
${ZIP_CODE_PATH}    field5
${MERCHANT_CATEGORY_CODE_PATH}    field6

*** Test Cases ***


# Note: Need to work on getting close time and time zone as a test variable
# Summary: Adds an First Data (Nashville) Merchant Account
# Assertions:
# - Verifies Info is saved by clicking on it's listing
# - Verifies that the test button fails
# Tear Down:
# - Deletes the created merchant account
Add Merchant Account: First Data (Nashville) - Happy
    [tags]    smoke-test

    Set Test Variable    ${account_name}    FirstData(Nashville)Automation
    # status: Enabled or Disabled
    Set Test Variable    ${status}          Enabled

    Set Test Variable    ${company_id}     1234567
    Set Test Variable    ${terminal_id}     1234567
    Set Test Variable    ${zip_code}    12345567
    Set Test Variable    ${merchant_category_code}    1234567

    Given I Go to Add Merchant Account Form
    And I Set Account Name to "${account_name}"
    And I Set Status To: "${status}"
    And I Select Processor: "First Data (Nashville)"
    And I Select Close Time: "12:00\ \ am"
    And I Select Time Zone: "Canada/Atlantic"

    And Input Text    ${COMPANY_ID_PATH}    ${company_id}
    And Input Text    ${TERMINAL_ID_PATH}    ${terminal_id}
    And Input Text    ${ZIP_CODE_PATH}     ${zip_code}
    And Input Text    ${MERCHANT_CATEGORY_CODE_PATH}    ${merchant_category_code}

    When I Click Add Account
    Then New First Data (Nashville) Account Listing Should Be Visible    ${account_name}    ${status}    12:00 am    Canada/Atlantic    ${company_id}    ${terminal_id}    ${zip_code}    ${merchant_category_code}
    And Merchant Account Test Fails    ${account_name}    First Data (Nashville)
    And Merchant Account Is Deleted    ${account_name}    First Data (Nashville)








*** Keywords ***

New First Data (Nashville) Account Listing Should Be Visible
    [arguments]    ${account_name}    ${status}    ${close_time}    ${time_zone}    ${company_id}    ${terminal_id}    ${zip_code}    ${merchant_category_code}
    Click Element    xpath=//ul[@id='merchantAccountList']//p[contains(text(),'${account_name}')]
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    css=#mrchntAccntFormArea
    Sleep    1
    Verify Label And Label Value    Account    ${account_name}
    Verify Label And Label Value    Account    ${status}
    Verify Label And Label Value    Processor    First Data (Nashville)

    # Verification is Invalid: Refer to: EMS-10933
    #Verify Label And Label Value    Account    ${close_time}
    #Verify Label And Label Value    Account    ${time_zone}

    Verify Label And Label Value    Company ID    ${company_id}
    Verify Label And Label Value    Terminal ID    ${terminal_id}
    Verify Label And Label Value    ZIP Code    ${zip_code}
    Verify Label And Label Value    Merchant Category Code    ${merchant_category_code}

    Click Element    xpath=//span[contains(text(),'Ok')]







