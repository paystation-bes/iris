*** Settings ***
# Summary: Test Suite for verifying the creation and deletion of an Elavon (Converge) Merchant Account
# Author: Billy Hoang
# Time: Dec 3, 2015

Resource          ../../../data/data.robot
Resource          ../../../Keywords/CustomerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Suite Teardown    Close Browser
Force Tags        smoke-test
Suite Setup       Run Keywords    Login as System Admin    systemadmin    Password$1    password    AND    Search Up Customer: "oranj"
Test Setup    Reload Page


*** Variables ***

${er1}    Processor does not exist
${er2}    must be between 5 to 30 characters long

${MERCHANT_ID_PATH}    field1
${USER_ID_PATH}     field2
${PIN_PATH}    field3
${DYNAMIC_DBA_PREFIX_LENGTH_EXPAND_PATH}    field4Expand
${DYNAMIC_DBA_PREFIX_LENGTH_PATH}    field4


*** Test Cases ***


# Note: Need to work on getting close time and time zone as a test variable
# Summary: Adds an Elavon (Converge) Merchant Account
# Assertions:
# - Verifies Info is saved by clicking on it's listing
# - Verifies that the test button fails
# Tear Down:
# - Deletes the created merchant account
Add Merchant Account: Elavon (Converge) - Happy
    [tags]    smoke-test

    Set Test Variable    ${account_name}    Elavon(Converge)Automationxx
    # status: Enabled or Disabled
    Set Test Variable    ${status}          Enabled
    Set Test Variable    ${merchant_id}     1234567
    Set Test Variable    ${user_id}         1234567
    Set Test Variable    ${pin}             1234567
    Set Test Variable    ${dynamic_dba_prefix_length}    3

    Given I Go to Add Merchant Account Form
    And I Set Account Name to "${account_name}"
    And I Set Status To: "${status}"
    And I Select Processor: "Elavon (Converge)"
    And I Select Close Time: "12:00\ \ am"
    And I Select Time Zone: "Canada/Atlantic"
    And Input Text    ${MERCHANT_ID_PATH}    ${merchant_id}
    And Input Text    ${USER_ID_PATH}    ${user_id}
    And Input Text    ${PIN_PATH}    ${pin}
    And Input Text    ${DYNAMIC_DBA_PREFIX_LENGTH_PATH}    ${dynamic_dba_prefix_length}
    #And Select Dynamic DBA Prefix Length: "${dynamic_dba_prefix_length}"
    When I Click Add Account
    Then New Elavon (Converge) Account Listing Should Be Visible    ${account_name}    ${status}    12:00 am    Canada/Atlantic    ${merchant_id}    ${user_id}    ${pin}    ${dynamic_dba_prefix_length}
    And Merchant Account Test Fails    ${account_name}    Elavon (Converge)
    And Merchant Account Is Deleted    ${account_name}    Elavon (Converge)






*** Keywords ***

New Elavon (Converge) Account Listing Should Be Visible
    [arguments]    ${account_name}    ${status}    ${close_time}    ${time_zone}    ${merchant_id}    ${user_id}    ${pin}    ${dynamic_dba_prefix_length}
    Click Element    xpath=//ul[@id='merchantAccountList']//p[contains(text(),'${account_name}')]
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    css=#mrchntAccntFormArea
    Sleep    1
    Verify Label And Label Value    Account    ${account_name}
    Verify Label And Label Value    Account    ${status}
    Verify Label And Label Value    Processor    Elavon (Converge)

    # Verification is Invalid: Refer to: EMS-10933
    #Verify Label And Label Value    Account    ${close_time}
    #Verify Label And Label Value    Account    ${time_zone}

    Verify Label And Label Value    Merchant ID    ${merchant_id}
    Verify Label And Label Value    User ID    ${user_id}
    Verify Label And Label Value    PIN    ${pin}
    Run Keyword If    '${dynamic_dba_prefix_length}'=='N/A'    Verify Label And Label Value    Dynamic DBA Prefix Length    0    ELSE    Verify Label And Label Value    Dynamic DBA Prefix Length    ${dynamic_dba_prefix_length}
    Click Element    xpath=//span[contains(text(),'Ok')]


# Does not recognize the anchor element
Select Dynamic DBA Prefix Length: "${dynamic_dba_prefix_length}"
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    ${DYNAMIC_DBA_PREFIX_LENGTH_PATH}
    Click Element    ${DYNAMIC_DBA_PREFIX_LENGTH_PATH}
    Sleep    5
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    xpath=//a[contains(text(),'${dynamic_dba_prefix_length}')]
    Click Element    xpath=//a[contains(text(),'${dynaimc_dba_prefix_length}')]







