*** Settings ***
# Summary: Test Suite for verifying the creation and deletion of an Elavon Merchant Account
# Author: Billy Hoang
# Time: Dec 3, 2015

Resource          ../../../data/data.robot
Resource          ../../../Keywords/CustomerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Suite Teardown    Close Browser
Force Tags        smoke-test
Suite Setup       Run Keywords    Login as System Admin    systemadmin    Password$1    password    AND    Search Up Customer: "oranj"
Test Setup    Reload Page


*** Variables ***

${er1}    Processor does not exist
${er2}    must be between 5 to 30 characters long

${TERMINAL_ID_PATH}    field1
${MERCHANT_ID_PATH}    field2
${CLOSE_BATCH_SIZE_PATH}    field3


*** Test Cases ***


# Note: Need to work on getting close time and time zone as a test variable
# Summary: Adds an Elavon Merchant Account
# Assertions:
# - Verifies Info is saved by clicking on it's listing
# - Verifies that the test button fails
# Tear Down:
# - Deletes the created merchant account
Add Merchant Account: Elavon - Happy
    [tags]    smoke-test

    Set Test Variable    ${account_name}    ElavonAutomationxx
    # status: Enabled or Disabled
    Set Test Variable    ${status}          Enabled
    Set Test Variable    ${terminal_id}     1234567
    Set Test Variable    ${merchant_id}     1234567
    Set Test Variable    ${close_batch_size}    8000

    Given I Go to Add Merchant Account Form
    And I Set Account Name to "${account_name}"
    And I Set Status To: "${status}"
    And I Select Processor: "Elavon"
    And I Select Close Time: "12:00\ \ am"
    And I Select Time Zone: "Canada/Atlantic"
    And Input Text    ${TERMINAL_ID_PATH}    ${terminal_id}
    And Input Text    ${MERCHANT_ID_PATH}    ${merchant_id}
    And Input Text    ${CLOSE_BATCH_SIZE_PATH}    ${close_batch_size}
    When I Click Add Account
    Then New Elavon Account Listing Should Be Visible    ${account_name}    ${status}    12:00 am    Canada/Atlantic    ${merchant_id}    ${terminal_id}    ${close_batch_size}
    And Merchant Account Test Fails    ${account_name}    Elavon
    And Merchant Account Is Deleted    ${account_name}    Elavon








*** Keywords ***

New Elavon Account Listing Should Be Visible
    [arguments]    ${account_name}    ${status}    ${close_time}    ${time_zone}    ${merchant_id}    ${terminal_id}    ${close_batch_size}
    Click Element    xpath=//ul[@id='merchantAccountList']//p[contains(text(),'${account_name}')]
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    css=#mrchntAccntFormArea
    Sleep    1
    Verify Label And Label Value    Account    ${account_name}
    Verify Label And Label Value    Account    ${status}
    Verify Label And Label Value    Processor    Elavon

    # Verification is Invalid: Refer to: EMS-10933
    #Verify Label And Label Value    Account    ${close_time}
    #Verify Label And Label Value    Account    ${time_zone}

    Verify Label And Label Value    Merchant ID    ${merchant_id}
    Verify Label And Label Value    Terminal ID    ${terminal_id}
    Verify Label And Label Value    Close Batch Size    ${close_batch_size}
    Click Element    xpath=//span[contains(text(),'Ok')]







