*** Settings ***
# Summary: Test Suite for verifying the creation and deletion of an Link2Gov Merchant Account
# Author: Billy Hoang
# Time: Dec 3, 2015

Resource          ../../../data/data.robot
Resource          ../../../Keywords/CustomerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Suite Teardown    Close Browser
Force Tags        smoke-test
Suite Setup       Run Keywords    Login as System Admin    systemadmin    Password$1    password    AND    Search Up Customer: "oranj"
Test Setup    Reload Page


*** Variables ***

${er1}    Processor does not exist
${er2}    must be between 5 to 30 characters long

${MERCHANT_CODE1_PATH}    field1Part1
${MERCHANT_CODE2_PATH}    field1Part2
${MERCHANT_CODE3_PATH}    field1Part3
${MERCHANT_CODE4_PATH}    field1Part4

${SETTLE_MERCHANT_CODE1_PATH}    field2Part1
${SETTLE_MERCHANT_CODE2_PATH}    field2Part2
${SETTLE_MERCHANT_CODE3_PATH}    field2Part3
${SETTLE_MERCHANT_CODE4_PATH}    field2Part4

${MERCHANT_PASSWORD_PATH}    field3

*** Test Cases ***


# Note: Need to work on getting close time and time zone as a test variable
# Summary: Adds an Link2Gov Merchant Account
# Assertions:
# - Verifies Info is saved by clicking on it's listing
# - Verifies that the test button fails
# Tear Down:
# - Deletes the created merchant account
Add Merchant Account: Link2Gov - Happy
    [tags]    smoke-test

    Set Test Variable    ${account_name}    Link2GovAutomation
    # status: Enabled or Disabled
    Set Test Variable    ${status}          Enabled

    Set Test Variable    ${merchant_code1}     12345
    Set Test Variable    ${merchant_code2}     12345
    Set Test Variable    ${merchant_code3}     12345
    Set Test Variable    ${merchant_code4}     1

    Set Test Variable    ${merchant_code}    ${merchant_code1}-${merchant_code2}-${merchant_code3}-${merchant_code4}

    Set Test Variable    ${settle_merchant_code1}    12345
    Set Test Variable    ${settle_merchant_code2}    12345
    Set Test Variable    ${settle_merchant_code3}    12345
    Set Test Variable    ${settle_merchant_code4}    11

    Set Test Variable    ${settle_merchant_code}    ${settle_merchant_code1}-${settle_merchant_code2}-${settle_merchant_code3}-${settle_merchant_code4}

    Set Test Variable    ${merchant_password}    12345678

    Given I Go to Add Merchant Account Form
    And I Set Account Name to "${account_name}"
    And I Set Status To: "${status}"
    And I Select Processor: "Link2Gov"
    And I Select Close Time: "12:00\ \ am"
    And I Select Time Zone: "Canada/Atlantic"

    And Input Text    ${MERCHANT_CODE1_PATH}    ${merchant_code1}
    And Input Text    ${MERCHANT_CODE2_PATH}    ${merchant_code2}
    And Input Text    ${MERCHANT_CODE3_PATH}    ${merchant_code3}
    And Input Text    ${MERCHANT_CODE4_PATH}    ${merchant_code4}

    And Input Text    ${SETTLE_MERCHANT_CODE1_PATH}    ${settle_merchant_code1}
    And Input Text    ${SETTLE_MERCHANT_CODE2_PATH}    ${settle_merchant_code2}
    And Input Text    ${SETTLE_MERCHANT_CODE3_PATH}    ${settle_merchant_code3}
    And Input Text    ${SETTLE_MERCHANT_CODE4_PATH}    ${settle_merchant_code4}

    And Input text    ${MERCHANT_PASSWORD_PATH}    ${merchant_password}

    When I Click Add Account
    Then New Link2Gov Account Listing Should Be Visible    ${account_name}    ${status}    12:00 am    Canada/Atlantic    ${merchant_code}    ${settle_merchant_code}    ${merchant_password}
    And Merchant Account Test Fails    ${account_name}    Link2Gov
    And Merchant Account Is Deleted    ${account_name}    Link2Gov








*** Keywords ***

New Link2Gov Account Listing Should Be Visible
    [arguments]    ${account_name}    ${status}    ${close_time}    ${time_zone}    ${merchant_code}    ${settle_merchant_code}    ${merchant_password}
    Click Element    xpath=//ul[@id='merchantAccountList']//p[contains(text(),'${account_name}')]
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    css=#mrchntAccntFormArea
    Sleep    1
    Verify Label And Label Value    Account    ${account_name}
    Verify Label And Label Value    Account    ${status}
    Verify Label And Label Value    Processor    Link2Gov

    # Verification is Invalid: Refer to: EMS-10933
    #Verify Label And Label Value    Account    ${close_time}
    #Verify Label And Label Value    Account    ${time_zone}

    Verify Label And Label Value    Merchant Code    ${merchant_code}
    Verify Label And Label Value    Settle Merchant Code    ${settle_merchant_code}
    Verify Label And Label Value    Merchant Password    ${merchant_password}

    Click Element    xpath=//span[contains(text(),'Ok')]







