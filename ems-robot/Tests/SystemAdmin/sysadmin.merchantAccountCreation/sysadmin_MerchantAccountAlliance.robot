*** Settings ***
# Summary: Test Suite for verifying the creation and deletion of an Alliance Merchant Account
# Author: Billy Hoang
# Time: Dec 3, 2015

Resource          ../../../data/data.robot
Resource          ../../../Keywords/CustomerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Suite Teardown    Close Browser
Force Tags        smoke-test
Suite Setup       Run Keywords    Login as System Admin    systemadmin    Password$1    password    
...               AND    Search Up Customer: "oranj"
#...               AND    Connect to Database    ${DB_API_MODULE_NAME}    ${DB_NAME}    ${DB_USERNAME}    ${DB_PASSWORD}    ${DB_HOST}    ${DB_PORT}
Test Setup    Reload Page


*** Variables ***

${er1}    Processor does not exist
${er2}    must be between 5 to 30 characters long

${MERCHANT_ID_PATH}    field1
${TERMINAL_ID_PATH}    field2


*** Test Cases ***


 Add Merchant Account: Alliance - Happy (Random)
     [tags]    smoke-test
     ${random}    Get Time    epoch
     Set Test Variable    ${account_name}    ${random}
     # status: Enabled or Disabled
     Set Test Variable    ${status}          Enabled
     Set Test Variable    ${merchant_id}     ${random}
     Set Test Variable    ${terminal_id}     ${random}

     Given I Go to Add Merchant Account Form
     And I Set Account Name to "${account_name}"
     And I Set Status To: "${status}"
     And I Select Processor: "Alliance"
     And I Select Close Time: "12:00\ \ am"
     And I Select Time Zone: "Canada/Atlantic"
     And Input Text    ${MERCHANT_ID_PATH}    ${merchant_id}
     And Input Text    ${TERMINAL_ID_PATH}    ${terminal_id}
     When I Click Add Account
     Then New Alliance Account Listing Should Be Visible    ${account_name}    ${status}    12:00 am    Canada/Atlantic    ${merchant_id}    ${terminal_id}
     And Merchant Account Test Fails    ${account_name}    Alliance
     And Merchant Account Is Deleted    ${account_name}    Alliance

# Summary: Adds an Alliance Merchant Account
# Assertions:
# - Verifies Info is saved by clicking on it's listing
# - Verifies that the test button fails 
# Tear Down:
# - Deletes the created merchant account
#Add Merchant Account: Alliance - Happy
#    [tags]    smoke-test
#
#    Set Test Variable    ${account_name}    AllianceAutomationxx
#    # status: Enabled or Disabled
#    Set Test Variable    ${status}          Enabled
#    Set Test Variable    ${merchant_id}     1234567
#    Set Test Variable    ${terminal_id}     1234567
#
#    Given I Go to Add Merchant Account Form
#    And I Set Account Name to "${account_name}"
#    And I Set Status To: "${status}"
#    And I Select Processor: "Alliance"
#    And I Select Close Time: "12:00\ \ am"
#    And I Select Time Zone: "Canada/Atlantic"
#    And Input Text    ${MERCHANT_ID_PATH}    ${merchant_id}
#    And Input Text    ${TERMINAL_ID_PATH}    ${terminal_id}
#    When I Click Add Account
#    Then New Alliance Account Listing Should Be Visible    ${account_name}    ${status}    12:00 am    Canada/Atlantic    ${merchant_id}    ${terminal_id}
#    And Merchant Account Test Fails    ${account_name}    Alliance
#    And Merchant Account Is Deleted    ${account_name}    Alliance


# Add Merchant Account: Alliance - Happy (Random, DB)
#     [tags]    smoke-test

#     ${random}    Get Time    epoch

#     Set Test Variable    ${account_name}    ${random}
#     # status: Enabled or Disabled
#     Set Test Variable    ${status}          Enabled
#     Set Test Variable    ${merchant_id}     1234567
#     Set Test Variable    ${terminal_id}     1234567


#     Given Merchant Account Is Not In Database With Name: "${account_name}"
#     Given I Go to Add Merchant Account Form
#     And I Set Account Name to "${account_name}"
#     And I Set Status To: "${status}"
#     And I Select Processor: "Alliance"
#     And I Select Close Time: "12:00\ \ am"
#     And I Select Time Zone: "Canada/Atlantic"
#     And Input Text    ${MERCHANT_ID_PATH}    ${merchant_id}
#     And Input Text    ${TERMINAL_ID_PATH}    ${terminal_id}
#     When I Click Add Account
#     Then New Alliance Account Listing Should Be Visible    ${account_name}    ${status}    12:00 am    Canada/Atlantic    ${merchant_id}    ${terminal_id}
#     And Merchant Account Is In Database With Name: "${account_name}"
#     And Merchant Account Test Fails    ${account_name}    Alliance
#     And Merchant Account Is Deleted    ${account_name}    Alliance
#     And Merchant Account Is Deleted In Database With Name: "${account_name}"






*** Keywords ***

Merchant Account Is Not In Database With Name: "${account_name}"
    Check If Not Exists In Database    select * from MerchantAccount where Name='${account_name}'

Merchant Account Is In Database With Name: "${account_name}"
    Check If Exists In Database    select * from MerchantAccount where Name='${account_name}'

Merchant Account Is Deleted In Database With Name: "${account_name}"
    Check If Exists In Database    select * from MerchantAccount where Name='${account_name}' and MerchantStatusTypeId='2';


New Alliance Account Listing Should Be Visible
    [arguments]    ${account_name}    ${status}    ${close_time}    ${time_zone}    ${merchant_id}    ${terminal_id}
    Click Element    xpath=//ul[@id='merchantAccountList']//p[contains(text(),'${account_name}')]
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    css=#mrchntAccntFormArea
    Sleep    1
    Verify Label And Label Value    Account    ${account_name}
    Verify Label And Label Value    Account    ${status}
    Verify Label And Label Value    Processor    Alliance

    # Verification is Invalid: Refer to: EMS-10933
    #Verify Label And Label Value    Account    ${close_time} 
    #Verify Label And Label Value    Account    ${time_zone}
    
    Verify Label And Label Value    Merchant ID    ${merchant_id}
    Verify Label And Label Value    Terminal ID    ${terminal_id}
    Click Element    xpath=//span[contains(text(),'Ok')]


 




