*** Settings ***
# Summary: Test Suite for verifying the fields and inputs of each Merchant Account processor when adding a Merchant Account.
# Author: Billy Hoang
# Time: Dec 3, 2015

Resource          ../../../data/data.robot
Resource          ../../../Keywords/CustomerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Suite Teardown    Close Browser
Force Tags        smoke-test
Suite Setup       Run Keywords    Login as System Admin    systemadmin    Password$1    password    AND    Search Up Customer: "oranj"
Test Setup    Reload Page
Test Template    Verify Merchant Account Form Expansion


*** Variables ***
# Summary: Every Processor and their fields in the Add Merchant Account Form
# Processor                  Field1              Field2                  Field3              Field4                     Field5
@{Alliance}                  Merchant ID         Terminal ID
@{Elavon}                    Terminal ID         Merchant ID             Close Batch Size
@{Elavon (Converge)}         Merchant ID         User ID                 PIN                 Dynamic DBA Prefix Length
@{First Data (Concord)}      Store ID            Store Key
@{First Data (Nashville)}    Company ID          Terminal ID             ZIP Code            Merchant Category Code
@{First Horizon}             Account ID          Terminal ID
@{Heartland}                 Merchant ID         Terminal ID
@{Link2Gov}                  Merchant Code       Settle Merchant Code    Merchant Password
@{Moneris}                   Store ID            API Token
@{Paymentech}                Account ID          Terminal ID             User                Password                    Client Number
@{Authorize.Net}             Login ID            Processor               Transaction Key
@{PayPros (Paradata)}        Token               Processor
@{BlackBoard}                Encryption Key      Server IP               Tender Number       Vendor Number
@{CBORD - CS Gold}           Server IP           Port Number             Provider Name       Location                    Code Map
@{CBORD - Odyssey}           Server IP           Port Number             Provider Name       Location                    Code Map
@{NuVision}                  Encryption Key      Server IP               Port Number         Packet Type
@{TotalCard}                 Security Key        Server IP               Store Number        Terminal Number


*** Test Cases ***
# Summary: Goes into Add Merchant Account Form and selects the given Processor to expand the form, then asserts that the fields given in Variables are visible
# Arguments: Merchant Processor

Alliance Form Expansion                   Alliance
Elavon Form Expansion                     Elavon
Elavon (Converge) Form Expansion          Elavon (Converge)
First Data (Concord) Form Expansion       First Data (Concord)
First Data (Nashville) Form               First Data (Nashville)
First Horizon Form Expansion              First Horizon
Heartland Form                            Heartland
Link2Gov Form Expansion                   Link2Gov
Moneris Form                              Moneris
Paymentech Form Expansion                 Paymentech
Authorize.Net Form                        Authorize.Net
PayPros (Paradata) Form Expansion         PayPros (Paradata)
BlackBoard Form                           BlackBoard
CBORD - CS Gold Form Expansion            CBORD - CS Gold
CBORD - Odyssey Form                      CBORD - Odyssey
NuVision Form Expansion                   NuVision
TotalCard Form Expansion                  TotalCard

*** Keywords ***


# Usage: Used as Test Template for sysadmin_MerchantAccountFormExpansion
# Summary: Goes into the Add Merchant Account Form, selects the given processor and asserts the respective fields are visible
# Arguments: Merchant Processor (ie. "Paymentech")
Verify Merchant Account Form Expansion
    [arguments]    ${processor}
    Given I Go To Add Merchant Account Form
    When I Select Processor: "${processor}"
    Then "${processor}" Merchant Account Form Should Be Visible

    # NOTE: The following is an attempt to further the scope of this suite by verifying
    # the actual creation of the Merchant Accounts. Please ignore for now.

    # Given Fill Form With Smoke Values    ${processor}
    # When I Click Add Account
    # Then New Merchant Account Listing Should Be Visible    Smoke Value    ${processor}


# Requires: access to variables defined in sysadmin_MerchantAccountFormExpansion
# Summary: Asserts the fields that appear when a processor is selected when the form expands
# Arguments: ${processor} - Merchant Processor (ie. "Paymentech")
# Variables: ${${processor}} refers to the list variables defined up top
"${processor}" Merchant Account Form Should Be Visible
    ${number_of_fields}=    Get Length    ${${processor}}
    Log To Console    Number of fields is ${number_of_fields}   
    Verify Labels    ${processor}
    Verify Inputs    ${processor}

Verify Labels
    [arguments]    ${processor}
    ${number_of_fields}=    Get Length    ${${processor}}
    :FOR    ${FIELD}    IN    @{${processor}}
    \    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    xpath=//label[contains(text(),'${FIELD}')]

# NOTE: FIRST DATA (NASHVILLE) HAS FIELDS1/2/5/6. TOTALLY SKIPS 3/4 - Dec, 2015 - Billy Hoang
# NOTE: First Data (Nashville) has Fields1/2/5/6, and so  
# Summary: Verifies all inputs as field# with the exception of Link2Gov
# Arguments: ${processor} - Merchant Processor (ie. "Paymentech")
Verify Inputs
    [arguments]    ${processor}
    ${number_of_fields}=    Get Length    ${${processor}}
    Run Keyword If    '${processor}'!='Link2Gov'    Verify Inputs Default    ${number_of_fields}    ELSE    Verify Inputs For Link2Gov


Verify Inputs Default
    [arguments]    ${number_of_fields}
    :FOR    ${index}    IN RANGE    1    ${number_of_fields}+1
    \    Element Should Be visible    xpath=//input[@id='field${index}']

# Link2Gov is the only Processor where it's inputs do not go by the normal naming convention, thus we call a different Keyword to validate it's inputs - Dec 2015 - Billy Hoang
Verify Inputs For Link2Gov
    Element Should Be visible    xpath=//input[@name='field1Part1']
    Element Should Be visible    xpath=//input[@name='field1Part2']
    Element Should Be visible    xpath=//input[@name='field1Part3']
    Element Should Be visible    xpath=//input[@name='field1Part4']
    Element Should Be visible    xpath=//input[@name='field2Part1']
    Element Should Be visible    xpath=//input[@name='field2Part2']
    Element Should Be visible    xpath=//input[@name='field2Part3']
    Element Should Be visible    xpath=//input[@name='field2Part4']
    Element Should Be visible    xpath=//input[@id='field3']


















# NOTE: The following is an attempt to further the scope of this suite by verifying
# the actual creation of the Merchant Accounts. Please ignore for now.
Fill Form With Smoke Values
    [arguments]    ${processor}
    I Set Account Name To "Smoke Value"
    Run Keyword If    '${processor}'=='Authorize.Net'    Fill Form With Smoke Values For Authorize.Net    ELSE    Run Keyword If    '${processor}'=='PayPros (Paradata)'    Fill Form With Smoke Values For PayPros (Paradata)    ELSE    Run Keyword If    '${processor}'=='Link2Gov'    Fill Form With Smoke Values For Link2Gov    ELSE    Run Keyword If    '${processor}'=='Elavon (Converge)'    Fill Form With Smoke Values For Elavon (Converge)   ELSE    Fill Form With Smoke Values Default    ${processor}


# NOTE: The following is an attempt to further the scope of this suite by verifying
# the actual creation of the Merchant Accounts. Please ignore for now.
Fill Form With Smoke Values Default
    [arguments]    ${processor}
    ${number_of_fields}=    Get Length    ${${processor}}
    :FOR    ${index}    IN RANGE    1    ${number_of_fields}+1
    \    Input Text    xpath=//input[@id='field${index}']    Smoke Value

# NOTE: The following is an attempt to further the scope of this suite by verifying
# the actual creation of the Merchant Accounts. Please ignore for now.
Fill Form With Smoke Values For Authorize.Net
    Input Text    xpath=//input[@id='field1']    Smoke Value
    Click Element    xpath=//a[@id='field2Expand']
    Wait Until Keyword Succeeds    10s    1s    Click Element    //a[contains(text(),'Chase Paymentech Solutions')]
    Input Text    xpath=//input[@id='field3']    Smoke Value

# NOTE: The following is an attempt to further the scope of this suite by verifying
# the actual creation of the Merchant Accounts. Please ignore for now.
Fill Form With Smoke Values For PayPros (Paradata)
    Input Text    xpath=//input[@id='field1']    Smoke Value
    Click Element    xpath=//a[@id='field2Expand']
    Wait Until Keyword Succeeds    10s    1s    Click Element    //a[contains(text(),'NOVA')]

# NOTE: The following is an attempt to further the scope of this suite by verifying
# the actual creation of the Merchant Accounts. Please ignore for now.
Fill Form With Smoke Values For Link2Gov
    Input Text    xpath=//input[@name='field1Part1']    11111
    Input Text    xpath=//input[@name='field1Part2']    11111
    Input Text    xpath=//input[@name='field1Part3']    11111
    Input Text    xpath=//input[@name='field1Part4']    1
    Input Text    xpath=//input[@name='field2Part1']    11111
    Input Text    xpath=//input[@name='field2Part2']    11111
    Input Text    xpath=//input[@name='field2Part3']    11111
    Input Text    xpath=//input[@name='field2Part4']    11
    Input Text    xpath=//input[@id='field3']    Smoke Value

# NOTE: The following is an attempt to further the scope of this suite by verifying
# the actual creation of the Merchant Accounts. Please ignore for now.
Fill Form With Smoke Values For Elavon (Converge)
    Input Text    xpath=//input[@id='field1']    Smoke Value
    Input Text    xpath=//input[@id='field2']    Smoke Value
    Input Text    xpath=//input[@id='field3']    Smoke Value
    Click Element    xpath=//a[@id='field4Expand']
    Wait Until Keyword Succeeds    10s    1s    Click Element    //a[contains(text(),'N/A')]








