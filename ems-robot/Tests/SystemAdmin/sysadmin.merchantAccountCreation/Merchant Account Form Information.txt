Merchant Account Form Information

Account                 input[@id='account']                    Text
Status                  a[@id='statusCheck']                    a


DIRECT:

Allicance
    Close Time          input[@id='closeTime']                  Drop Down
    Time Zone           input[@id='mrchSelectTimeZone']         Drop Down
    Merchant ID         input[@id='field1']                     Text
    Terminal ID         input[@id='field2']                     Text

Elavon
    Close Time          input[@id='closeTime']                  Drop Down
    Time Zone           input[@id='mrchSelectTimeZone']         Drop Down
    Terminal ID         input[@id='field1']                     Text   
    Merchant ID         input[@id='field2']                     Text
    Close Batch Size    input[@id='field3']                     Text

Elavon (Converge)
    Close Time          input[@id='closeTime']                 Drop Down
    Time Zone           input[@id='mrchSelectTimeZone']         Drop Down
    Merchant ID
    User ID
    PIN
    Dyanamic DBA Prefix Length                                  Drop Down

First Data (Concord)
    Close Time          input[@id='closeTime']                  Drop Down
    Time Zone           input[@id='mrchSelectTimeZone']         Drop Down
    Store ID            input[@id='field1']                     Text
    Store Key           input[@id'field2']                      Text

First Data (Nashville)
    Close Time
    Time Zone
    Company ID
    Terminal ID
    ZIP Code
    Merchant Category Code

First Horizon
    Close Time
    Time Zone
    Account ID
    Terminal ID

Heartland
    Close Time
    Time Zone
    Merchant ID
    Terminal ID

Link2Gov
    Close Time
    Time Zone
    Merchant Code
        name=field1Part1, field1Part2, field1Part3, field1Part4
    Settle Merchant Code
        name=field2Part1, field2Part2, field2Part3, field2Part4
    Merchant Password

Moneris
    Close Time
    Time Zone
    Store ID
    API Token

Paymentech
    Close Time
    Time Zone
    Account ID
    Terminal ID
    User
    Password
    Client Number


GATEWAY:

Authorize.Net
    Close Time
    Time Zone
    Login ID
    Processor   Drop down
    Transaction Key

PayPros (Paradata)
    Close Time
    Time Zone
    Token
    Processor   Drop down

CAMPUS CARDS:

BlackBoard
    Close Time
    Time Zone
    Encryption Key
    Server IP
    Tender Number
    Vendor Number

CBORD - CS Gold
    Server IP
    Port Number
    Provider Name
    Location
    Code Map (Not Req)

CBORD - Odyssey
    Server IP
    Port Number
    Provider Name
    Location
    Code Map

NuVision
    Close Time
    Time Zone
    Encryption Key
    Server IP
    Port Number
    Packet Type

TotalCard
    Close Time
    Time Zone
    Security Key
    Server IP
    Store Number
    Terminal Number