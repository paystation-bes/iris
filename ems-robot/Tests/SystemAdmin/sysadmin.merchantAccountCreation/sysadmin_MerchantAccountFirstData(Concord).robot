*** Settings ***
# Summary: Test Suite for verifying the creation and deletion of an First Data (Concord) Merchant Account
# Author: Billy Hoang
# Time: Dec 3, 2015

Resource          ../../../data/data.robot
Resource          ../../../Keywords/CustomerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Suite Teardown    Close Browser
Force Tags        smoke-test
Suite Setup       Run Keywords    Login as System Admin    systemadmin    Password$1    password    AND    Search Up Customer: "oranj"
Test Setup    Reload Page


*** Variables ***

${er1}    Processor does not exist
${er2}    must be between 5 to 30 characters long

${STORE_ID_PATH}    field1
${STORE_KEY_PATH}    field2


*** Test Cases ***


# Note: Need to work on getting close time and time zone as a test variable
# Summary: Adds an First Data (Concord) Merchant Account
# Assertions:
# - Verifies Info is saved by clicking on it's listing
# - Verifies that the test button fails
# Tear Down:
# - Deletes the created merchant account
Add Merchant Account: First Data (Concord) - Happy
    [tags]    smoke-test

    Set Test Variable    ${account_name}    FirstData(Concord)Automationxx
    # status: Enabled or Disabled
    Set Test Variable    ${status}          Enabled
    Set Test Variable    ${store_id}     1234567
    Set Test Variable    ${store_key}     1234567

    Given I Go to Add Merchant Account Form
    And I Set Account Name to "${account_name}"
    And I Set Status To: "${status}"
    And I Select Processor: "First Data (Concord)"
    And I Select Close Time: "12:00\ \ am"
    And I Select Time Zone: "Canada/Atlantic"
    And Input Text    ${STORE_ID_PATH}    ${store_id}
    And Input Text    ${STORE_KEY_PATH}    ${store_key}
    When I Click Add Account
    Then New First Data (Concord) Account Listing Should Be Visible    ${account_name}    ${status}    12:00 am    Canada/Atlantic    ${store_id}    ${store_key}
    And Merchant Account Test Fails    ${account_name}    First Data (Concord)
    And Merchant Account Is Deleted    ${account_name}    First Data (Concord)








*** Keywords ***

New First Data (Concord) Account Listing Should Be Visible
    [arguments]    ${account_name}    ${status}    ${close_time}    ${time_zone}    ${store_id}    ${store_key_id}
    Click Element    xpath=//ul[@id='merchantAccountList']//p[contains(text(),'${account_name}')]
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    css=#mrchntAccntFormArea
    Sleep    1
    Verify Label And Label Value    Account    ${account_name}
    Verify Label And Label Value    Account    ${status}
    Verify Label And Label Value    Processor    First Data (Concord)

    # Verification is Invalid: Refer to: EMS-10933
    #Verify Label And Label Value    Account    ${close_time}
    #Verify Label And Label Value    Account    ${time_zone}

    Verify Label And Label Value    Store ID    ${store_id}
    Verify Label And Label Value    Store Key    ${store_key_id}
    Click Element    xpath=//span[contains(text(),'Ok')]







