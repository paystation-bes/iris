*** Settings ***
Library           Selenium2Library
Resource		  ../../../Keywords/globalKeywordDirectory.robot
Resource          ../../../Keywords/uiCrawlerKeywords/UICrawlerKeywords.robot

Suite Setup       Login As System Admin    systemadmin    Password$1    password
Suite Teardown    Close Browser


*** Variables ***

${PARENT_COMPANY_CHOICE_BUTTON}    css=#prntCustomerACExpand

*** Test Cases ***

System Admin - Home
	Given Open New Customer Form
	And Page Should Contain    New Customer
	And Click Element          css=#parentFlagCheck
	And Element Should Not Be Visible    ${PARENT_COMPANY_CHOICE_BUTTON} 
	And Close New Customer Form
	# Create a parent company and create a customer admin on the system admin
	# Search up the specific account
	# Make sure for a customer the following appears:
	#	Customer Details
	#	Reports [Can Create Report As Well]
	# 	Licenses 
	#		Digital API
	#		Mobile Licenses
	#		Integrations
	#	Pay Stations
	#		Pay Station Details
	#		Pay Station Placement
	#	Locations
	# 	Routes
	#	Alerts
	# 
	# For A PARENT COMPANY:
	#	Customer Details
	#	Reports

System Admin - Reports
	Given Go To 'Reports' Section (Left)
	And Header Should Be Visible: "Pending and Incomplete Reports"
	And Header Should Be Visible: "Completed Reports"
	And Header Should Be Visible: "Scheduled Reports"
	When Click Create Report
	Then Header Should Be Visible: "Report Type"
	And Header Should Be Visible: "Report Details"
	And Header Should Be Visible: "Report Schedule"

System Admin - User Administration: Admin User Accounts
	Given Go To 'User Administration' Section (Left)
	And Click Admin User Accounts Tab 1
	And Header Should Be Visible: "Admin User Accounts"
	When Click Add Button
	Then Header Should Be Visible: "Add User"

System Admin - User Administration: Admin User Roles
	Given Click Admin User Roles Tab 1
	And Header Should Be Visible: "Roles"
	When Click Add Button
	Then Header Should Be Visible: "Add Role"

System Admin - Notifications
	Given Go To 'Notifications' Section (Left)
	And Header Should Be Visible: "System Notifications"
	And Table Info Should Be Visible: "Title"
	And Table Info Should Be Visible: "Date"
	When Click Add Button
	# Then Header Should Be Visible: "Notification Details"
	And Element Should Be Visible    css=#formNotificationTitle
	And Element Should Be Visible    css=#formNotificationMessage

System Admin - System Activity: System Monitor
	Given Go To 'System Activity' Section (Left)
	And Click System Monitor Tab 1
	And Header Should Be Visible: "System Status"
	And Page Should Contain    Encryption Services
	And Page Should Contain    Digital Iris Information

System Admin - System Activity: Server Admin
	Given Click Server Admin Tab 1
	And Header Should Be Visible: "Cluster Member List"
	And Header Should Be Visible: "EMS Key List"
	And Header Should Be Visible: "Card Processing Queue Status"

System Admin - Customer Migration: Migration Status
	Given Go To 'Customer Migration' Section (Left)
	And Click Migration Status Tab 1
	And Table Info Should Be Visible: "Customer"
	And Table Info Should Be Visible: "Boarded Date"
	And Table Info Should Be Visible: "Migration Date"
	And Table Info Should Be Visible: "Pay Stations"


System Admin - Customer Migration: Migration Schedule
	Given Click Migration Schedule Tab 1
	And Header Should Be Visible: "Customer Schedule"
	And Header Should Be Visible: "This Weeks Schedule"

	# Migration needs work on and the notification details need work on as well


