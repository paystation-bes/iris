*** Settings ***
# Summary: Test Suite for verifying the creation and deletion of a Report
# Author: Billy Hoang
# Time: Dec 3, 2015

Resource          ../../data/data.robot
Resource          ../../Keywords/systemAdminReportKeywords/systemAdminReportKeywords.robot
Resource          ../../Keywords/globalKeywordDirectory.robot
Library           Selenium2Library
Library           HttpLibrary.HTTP
Library           OperatingSystem
#Library           DatabaseLibrary
Suite Teardown    Close Browser
Force Tags        smoke-test
Suite Setup       Run Keywords    Login as System Admin    systemadmin    Password$1    password
...               AND    Search Up Customer: "oranj"
#...               AND    Connect to Database    ${DB_API_MODULE_NAME}    ${DB_NAME}    ${DB_USERNAME}    ${DB_PASSWorD}    ${DB_HOST}    ${DB_PorT}
Test Setup    Run Keywords    Click Reports Tab
...                           AND    Click Create Report



*** Variables ***



*** Test Cases ***

Transaction - All 1
    I Create A Transaction - All Report

Transaction - All (Details) 2
    [Documentation]    Specifies a specific range of months
    ${report_type}=  Set Variable    Transaction - All
    ${detail_type}=  Set Variable    Details
    ${transaction_date/time}=  Set Variable    Month  #
    ${start_month}=  Set Variable    January  #
    ${start_year}=  Set Variable    2015  #
    ${end_month}=  Set Variable    March  #
    ${end_year}=  Set Variable    2015  #
    ${location_type}=  Set Variable    Pay Station Route  #
    ${archived_pay_stations?}=  Set Variable    No  #
    ${space_number}=  Set Variable    All
    ${license_plate}=  Set Variable    ${EMPTY}
    ${ticket_number}=  Set Variable    All
    ${coupon_code}=  Set Variable    N/A
    ${transaction_type}=  Set Variable    All
    ${group_by}=  Set Variable    Month  #
    ${schedule}=  Set Variable    Submit Now

    Given Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Report Detail Type: "${detail_type}"
    And Select Transaction Date/Time: "${transaction_date/time}"
    And Input Start Month: "${start_month}"
    And Input Start Year: "${start_year}"
    And Input End Month: "${end_month}"
    And Input End Year: "${end_year}"
    And Select Location Type: "${location_type}"
    And Select All Location Type Listings
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    And Input License Plate: "${license_plate}"
    And Select Ticket Number: "${ticket_number}"
    And Select Coupon Code: "${coupon_code}"
    And Select Transaction Type: "${transaction_type}"
    And Select Group By: "${group_by}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    And Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed    

Transaction - All (Details) 3
    [Documentation]    Specifies a >= range for space number
    ${report_type}=  Set Variable    Transaction - All
    ${detail_type}=  Set Variable    Details
    ${transaction_date/time}=    Set Variable    Yesterday  #
    ${location_type}=  Set Variable    Pay Station
    ${archived_pay_stations?}=  Set Variable    Yes
    ${space_number}=  Set Variable    >=  #
    ${first_space_number}= Set Variable     1
    ${license_plate}=  Set Variable    ${EMPTY}
    ${ticket_number}=  Set Variable    All
    ${coupon_code}=  Set Variable    N/A
    ${transaction_type}=  Set Variable    All
    ${group_by}=  Set Variable    None
    ${schedule}=  Set Variable    Submit Now
    ${output_format}=  Set Variable    CSV

    Given Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Report Detail Type: "${detail_type}"
    And Select Transaction Date/Time: "${transaction_date/time}"
    And Select Location Type: "${location_type}"
    And Select All Location Type Listings
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    And Input First Space Number: "${first_space_number}"
    And Input License Plate: "${license_plate}"
    And Select Ticket Number: "${ticket_number}"
    And Select Coupon Code: "${coupon_code}"
    And Select Transaction Type: "${transaction_type}"
    And Select Output Format: "${output_format}"
    And Select Group By: "${group_by}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    And Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed


Transaction - All (Summary) 4
    [Documentation]  Specifies a range of space numbers
    ${report_type}=  Set Variable    Transaction - All
    ${detail_type}=  Set Variable    Summary
    ${transaction_date/time}=    Set Variable    This Week
    ${location_type}=  Set Variable    Location
    ${archived_pay_stations?}=  Set Variable    No
    ${space_number}=  Set Variable    Between
    ${first_space_number}=  Set Variable    2
    ${second_space_number}=  Set Variable    15
    ${license_plate}=  Set Variable    ${EMPTY}
    ${ticket_number}=  Set Variable    All
    ${coupon_code}=  Set Variable    N/A
    ${transaction_type}=  Set Variable    All
    ${group_by}=  Set Variable    Payment Type
    ${schedule}=  Set Variable    Submit Now

    Given Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Report Detail Type: "${detail_type}"
    And Select Transaction Date/Time: "${transaction_date/time}"
    And Select Location Type: "${location_type}"
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    And Input First Space Number: "${first_space_number}"
    And Input Second Space Number: "${second_space_number}"
    And Input License Plate: "${license_plate}"
    And Select Ticket Number: "${ticket_number}"
    And Select Coupon Code: "${coupon_code}"
    And Select Transaction Type: "${transaction_type}"
    And Select Group By: "${group_by}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    And Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed

Transaction - All (Details) 5
    [Template]    I Create a Transaction - All Report
         detail_type=Details
    ...  transaction_date/time=This Month
    ...  location_type=Pay Station Route
    ...  group_by=Day
    ...  output_format=CSV

Transaction - All (Details) 6
    [Template]    I Create a Transaction - All Report
         detail_type=Details
    ...  transaction_date/time=Last 12 Months
    ...  location_type=Pay Station
    ...  archived_pay_stations?=No
    ...  group_by=Location

Transaction - All (Details) 6
    [Documentation]  Refined By Pay Station and selects a specific Location
    ${report_type}=  Set Variable    Transaction - All
    ${detail_type}=  Set Variable    Summary
    ${transaction_date/time}=    Set Variable    Last 7 days
    ${location_type}=  Set Variable    Pay Station
    ${pay_station_filter}=  Set Variable    Unassigned
    ${archived_pay_stations?}=  Set Variable    Yes
    ${space_number}=  Set Variable    N/A
    ${license_plate}=  Set Variable    ${EMPTY}
    ${ticket_number}=  Set Variable    All
    ${coupon_code}=  Set Variable    Specific Code
    ${actual_coupon_code}=  Set Variable    ABC123
    ${transaction_type}=  Set Variable    Test
    ${group_by}=  Set Variable    Trans Type
    ${output_format}=  Set Variable    PDF
    ${schedule}=  Set Variable    Submit Now

    Given Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Report Detail Type: "${detail_type}"
    And Select Transaction Date/Time: "${transaction_date/time}"
    And Select Location Type: "${location_type}"
    And Select 
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    And Input License Plate: "${license_plate}"
    And Select Ticket Number: "${ticket_number}"
    And Select Coupon Code: "${coupon_code}"
    And Input Coupon Code: "${actual_coupon_code}"
    And Select Transaction Type: "${transaction_type}"
    And Select Group By: "${group_by}"
    And Select Output Format: "${output_format}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    And Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed


Transaction - All (Summary) 7
    [Documentation]  Specifies a specific coupon code
    ${report_type}=  Set Variable    Transaction - All
    ${detail_type}=  Set Variable    Summary
    ${transaction_date/time}=    Set Variable    Last 7 days
    ${location_type}=  Set Variable    Location
    ${archived_pay_stations?}=  Set Variable    Yes
    ${space_number}=  Set Variable    N/A
    ${license_plate}=  Set Variable    ${EMPTY}
    ${ticket_number}=  Set Variable    All
    ${coupon_code}=  Set Variable    Specific Code
    ${actual_coupon_code}=  Set Variable    ABC123
    ${transaction_type}=  Set Variable    Test
    ${group_by}=  Set Variable    Trans Type
    ${output_format}=  Set Variable    PDF
    ${schedule}=  Set Variable    Submit Now

    Given Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Report Detail Type: "${detail_type}"
    And Select Transaction Date/Time: "${transaction_date/time}"
    And Select Location Type: "${location_type}"
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    And Input License Plate: "${license_plate}"
    And Select Ticket Number: "${ticket_number}"
    And Select Coupon Code: "${coupon_code}"
    And Input Coupon Code: "${actual_coupon_code}"
    And Select Transaction Type: "${transaction_type}"
    And Select Group By: "${group_by}"
    And Select Output Format: "${output_format}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    And Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed


Transaction - All (Details) 8
    [Template]    I Create a Transaction - All Report
         detail_type=Details
    ...  transaction_date/time=Last Year
    ...  location_type=Pay Station Route
    ...  archived_pay_stations?=No
    ...  transaction_type=Add Time


Transaction - All (Summary) 9
    [Documentation]    Specifies a specific Date/Time Range
    ${report_type}=  Set Variable    Transaction - All
    ${detail_type}=  Set Variable    Details
    ${transaction_date/time}=    Set Variable    Date/Time
    # ${beginning_date}
    # ${beginning_time}
    # ${end_date}
    # ${end_time}
    ${location_type}=  Set Variable    Pay Station
    ${archived_pay_stations?}=  Set Variable    Yes
    ${space_number}=  Set Variable    N/A
    ${license_plate}=  Set Variable    ${EMPTY}
    ${ticket_number}=  Set Variable    All
    ${coupon_code}=  Set Variable    N/A
    ${transaction_type}=  Set Variable    SC Recharge
    ${group_by}=  Set Variable    Month
    ${output_format}=  Set Variable    CSV
    ${schedule}=  Set Variable    Submit Now

    Given Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Report Detail Type: "${detail_type}"
    And Select Transaction Date/Time: "${transaction_date/time}"
    And Select Transaction Date/Time
    And Select Location Type: "${location_type}"
    And Select All Location Type Listings
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    And Input License Plate: "${license_plate}"
    And Select Ticket Number: "${ticket_number}"
    And Select Coupon Code: "${coupon_code}"
    And Input Coupon Code: "${actual_coupon_code}"
    And Select Transaction Type: "${transaction_type}"
    And Select Group By: "${group_by}"
    And Select Output Format: "${output_format}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    And Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed


Transaction - All (Summary) 10
    [Template]    I Create a Transaction - All Report
         archived_pay_stations=No
         transaction_type=Replenish
         group_by=Pay Station

# Transaction - Credit Card (Summary) 11
#     [Documentation]  Runs a Transaction - All Summary Report and Verifies it's creation
#     ${report_type}=  Set Variable    Transaction - Credit Card
#     ${detail_type}=  Set Variable    Summary
#     ${transaction_date/time}=    Set Variable    Date/Time
#     # ${beginning_date}
#     # ${beginning_time}
#     # ${end_date}
#     # ${end_time}
#     ${location_type}=  Set Variable    Location
#     ${archived_pay_stations?}=  Set Variable    Yes
#     ${space_number}=  Set Variable    No Stall
#     ${license_plate}=  Set Variable    ${EMPTY}
#     ${ticket_number}=  Set Variable    All
#     ${coupon_code}=  Set Variable    N/A
#     ${transaction_type}=  Set Variable    All
#     ${group_by}=  Set Variable    Day
#     ${output_format}=  Set Variable    PDF
#     ${schedule}=  Set Variable    Submit Now

#     Given Select Report Type: "${report_type}"
#     And Proceed To Step 2
#     And Select Report Detail Type: "${detail_type}"
#     And Select Transaction Date/Time: "${transaction_date/time}"
#     And Select Transaction Date/Time
#     And Select Location Type: "${location_type}"
#     And Select All Location Type Listings
#     And Include Archived Pay Stations: "${archived_pay_stations?}"
#     And Select Space Number: "${space_number}"
#     And Input License Plate: "${license_plate}"
#     And Select Ticket Number: "${ticket_number}"
#     And Select Coupon Code: "${coupon_code}"
#     And Input Coupon Code: "${actual_coupon_code}"
#     And Select Transaction Type: "${transaction_type}"
#     And Select Group By: "${group_by}"
#     And Select Output Format: "${output_format}"
#     And Proceed To Step 3
#     And Select Schedule: "${schedule}"
#     And Click Save Report
#     Then Report "${report_type}" Is Pending
#     And Report "${report_type}" Is Completed

# Transaction - Credit Card (Summary) 12
#     [Documentation]  Runs a Transaction - All Summary Report and Verifies it's creation
#     ${report_type}=  Set Variable    Transaction - Credit Card
#     ${detail_type}=  Set Variable    Details
#     ${transaction_date/time}=    Set Variable    Today
#     # ${beginning_date}
#     # ${beginning_time}
#     # ${end_date}
#     # ${end_time}
#     ${location_type}=  Set Variable    Pay Station Route
#     ${archived_pay_stations?}=  Set Variable    No
#     ${space_number}=  Set Variable    =
#     ${specific_space_number}=  Set Variable    1
#     ${license_plate}=  Set Variable    ${EMPTY}
#     ${ticket_number}=  Set Variable    All
#     ${coupon_code}=  Set Variable    N/A
#     ${transaction_type}=  Set Variable    All
#     ${group_by}=  Set Variable    Location
#     ${output_format}=  Set Variable    PDF
#     ${schedule}=  Set Variable    Submit Now

#     Given Select Report Type: "${report_type}"
#     And Proceed To Step 2
#     And Select Report Detail Type: "${detail_type}"
#     And Select Transaction Date/Time: "${transaction_date/time}"
#     And Select Location Type: "${location_type}"
#     And Select All Location Type Listings
#     And Include Archived Pay Stations: "${archived_pay_stations?}"
#     And Select Space Number: "${space_number}"
#     And Input First Space Number: "${specific_space_number}"
#     And Input License Plate: "${license_plate}"
#     And Select Ticket Number: "${ticket_number}"
#     And Select Coupon Code: "${coupon_code}"
#     And Input Coupon Code: "${actual_coupon_code}"
#     And Select Transaction Type: "${transaction_type}"
#     And Select Group By: "${group_by}"
#     And Select Output Format: "${output_format}"
#     And Proceed To Step 3
#     And Select Schedule: "${schedule}"
#     And Click Save Report
#     Then Report "${report_type}" Is Pending
#     And Report "${report_type}" Is Completed

# Transaction - Credit Card (Summary) 13
#     [Documentation]  Runs a Transaction - All Summary Report and Verifies it's creation
#     ${report_type}=  Set Variable    Transaction - Credit Card
#     ${detail_type}=  Set Variable    Details
#     ${transaction_date/time}=    Set Variable    Last 24 Hours
#     # ${beginning_date}
#     # ${beginning_time}
#     # ${end_date}
#     # ${end_time}
#     ${location_type}=  Set Variable    Pay Station
#     ${archived_pay_stations?}=  Set Variable    Yes
#     ${space_number}=  Set Variable    <=
#     ${specific_space_number}=  Set Variable    1
#     ${license_plate}=  Set Variable    ${EMPTY}
#     ${ticket_number}=  Set Variable    All
#     ${coupon_code}=  Set Variable    N/A
#     ${transaction_type}=  Set Variable    All
#     ${group_by}=  Set Variable    Transaction Type
#     ${output_format}=  Set Variable    CSV
#     ${schedule}=  Set Variable    Submit Now

#     Given Select Report Type: "${report_type}"
#     And Proceed To Step 2
#     And Select Report Detail Type: "${detail_type}"
#     And Select Transaction Date/Time: "${transaction_date/time}"
#     And Select Location Type: "${location_type}"
#     And Select All Location Type Listings
#     And Include Archived Pay Stations: "${archived_pay_stations?}"
#     And Select Space Number: "${space_number}"
#     And Input First Space Number: "${specific_space_number}"
#     And Input License Plate: "${license_plate}"
#     And Select Ticket Number: "${ticket_number}"
#     And Select Coupon Code: "${coupon_code}"
#     And Input Coupon Code: "${actual_coupon_code}"
#     And Select Transaction Type: "${transaction_type}"
#     And Select Group By: "${group_by}"
#     And Select Output Format: "${output_format}"
#     And Proceed To Step 3
#     And Select Schedule: "${schedule}"
#     And Click Save Report
#     Then Report "${report_type}" Is Pending
#     And Report "${report_type}" Is Completed


# Transaction - Credit Card (Summary) 14
#     [Documentation]  Runs a Transaction - All Summary Report and Verifies it's creation
#     ${report_type}=  Set Variable    Transaction - Credit Card
#     ${detail_type}=  Set Variable    Summary
#     ${transaction_date/time}=    Set Variable    Last Week
#     ${location_type}=  Set Variable    Location
#     ${archived_pay_stations?}=  Set Variable    No
#     ${space_number}=  Set Variable    N/A
#     ${license_plate}=  Set Variable    123ABC
#     ${ticket_number}=  Set Variable    All
#     ${coupon_code}=  Set Variable    N/A
#     ${transaction_type}=  Set Variable    All
#     ${group_by}=  Set Variable    None
#     ${output_format}=  Set Variable    PDF
#     ${schedule}=  Set Variable    Submit Now

#     Given Select Report Type: "${report_type}"
#     And Proceed To Step 2
#     And Select Report Detail Type: "${detail_type}"
#     And Select Transaction Date/Time: "${transaction_date/time}"
#     And Select Location Type: "${location_type}"
#     And Include Archived Pay Stations: "${archived_pay_stations?}"
#     And Select Space Number: "${space_number}"
#     And Input License Plate: "${license_plate}"
#     And Select Ticket Number: "${ticket_number}"
#     And Select Coupon Code: "${coupon_code}"
#     And Input Coupon Code: "${actual_coupon_code}"
#     And Select Transaction Type: "${transaction_type}"
#     And Select Group By: "${group_by}"
#     And Select Output Format: "${output_format}"
#     And Proceed To Step 3
#     And Select Schedule: "${schedule}"
#     And Click Save Report
#     Then Report "${report_type}" Is Pending
#     And Report "${report_type}" Is Completed

# Transaction - Credit Card (Summary) 15
#     [Documentation]  Runs a Transaction - All Summary Report and Verifies it's creation
#     ${report_type}=  Set Variable    Transaction - Credit Card
#     ${detail_type}=  Set Variable    Details
#     ${transaction_date/time}=    Set Variable    Last Month
#     ${location_type}=  Set Variable    Pay Station Route
#     ${archived_pay_stations?}=  Set Variable    Yes
#     ${space_number}=  Set Variable    N/A
#     ${license_plate}=  Set Variable    ${EMPTY}
#     ${ticket_number}=  Set Variable    Betweeen
#     ${first_ticket_number}=  Set Variable    2
#     ${second_ticket_number}=  Set Variable    25
#     ${coupon_code}=  Set Variable    N/A
#     ${transaction_type}=  Set Variable    All
#     ${group_by}=  Set Variable    Month
#     ${output_format}=  Set Variable    CSV
#     ${schedule}=  Set Variable    Submit Now

#     Given Select Report Type: "${report_type}"
#     And Proceed To Step 2
#     And Select Report Detail Type: "${detail_type}"
#     And Select Transaction Date/Time: "${transaction_date/time}"
#     And Select Location Type: "${location_type}"
#     And Include Archived Pay Stations: "${archived_pay_stations?}"
#     And Select Space Number: "${space_number}"
#     And Input First Space Number: "${specific_space_number}"
#     And Input License Plate: "${license_plate}"
#     And Select Ticket Number: "${ticket_number}"
#     And Select Coupon Code: "${coupon_code}"
#     And Input Coupon Code: "${actual_coupon_code}"
#     And Select Transaction Type: "${transaction_type}"
#     And Select Group By: "${group_by}"
#     And Select Output Format: "${output_format}"
#     And Proceed To Step 3
#     And Select Schedule: "${schedule}"
#     And Click Save Report
#     Then Report "${report_type}" Is Pending
#     And Report "${report_type}" Is Completed


# Transaction - Credit Card (Details) 16
#     [Documentation]  Runs a Transaction - All Summary Report and Verifies it's creation
#     ${report_type}=  Set Variable    Transaction - Credit Card
#     ${detail_type}=  Set Variable    Details
#     ${transaction_date/time}=    Set Variable    Last 30 Days
#     ${location_type}=  Set Variable    Pay Station
#     ${archived_pay_stations?}=  Set Variable    No
#     ${space_number}=  Set Variable    N/A
#     ${license_plate}=  Set Variable    ${EMPTY}
#     ${ticket_number}=  Set Variable    All
#     ${coupon_code}=  Set Variable    No Coupons
#     ${transaction_type}=  Set Variable    All
#     ${group_by}=  Set Variable    Pay Station
#     ${output_format}=  Set Variable    PDF
#     ${schedule}=  Set Variable    Submit Now

#     Given Select Report Type: "${report_type}"
#     And Proceed To Step 2
#     And Select Report Detail Type: "${detail_type}"
#     And Select Transaction Date/Time: "${transaction_date/time}"
#     And Select Location Type: "${location_type}"
#     And Select All Location Type Listings
#     And Include Archived Pay Stations: "${archived_pay_stations?}"
#     And Select Space Number: "${space_number}"
#     And Input First Space Number: "${specific_space_number}"
#     And Input License Plate: "${license_plate}"
#     And Select Ticket Number: "${ticket_number}"
#     And Select Coupon Code: "${coupon_code}"
#     And Input Coupon Code: "${actual_coupon_code}"
#     And Select Transaction Type: "${transaction_type}"
#     And Select Group By: "${group_by}"
#     And Select Output Format: "${output_format}"
#     And Proceed To Step 3
#     And Select Schedule: "${schedule}"
#     And Click Save Report
#     Then Report "${report_type}" Is Pending
#     And Report "${report_type}" Is Completed



# Transaction - Credit Card (Details) 17
#     [Documentation]  Runs a Transaction - All Summary Report and Verifies it's creation
#     ${report_type}=  Set Variable    Transaction - Credit Card
#     ${detail_type}=  Set Variable    Summary
#     ${transaction_date/time}=    Set Variable    This Year
#     ${location_type}=  Set Variable    Location
#     ${archived_pay_stations?}=  Set Variable    Yes
#     ${space_number}=  Set Variable    N/A
#     ${license_plate}=  Set Variable    ${EMPTY}
#     ${ticket_number}=  Set Variable    All
#     ${coupon_code}=  Set Variable    N/A
#     ${transaction_type}=  Set Variable    Regular
#     ${group_by}=  Set Variable    Pay Station Type
#     ${output_format}=  Set Variable    PDF
#     ${schedule}=  Set Variable    Submit Now

#     Given Select Report Type: "${report_type}"
#     And Proceed To Step 2
#     And Select Report Detail Type: "${detail_type}"
#     And Select Transaction Date/Time: "${transaction_date/time}"
#     And Select Location Type: "${location_type}"
#     And Include Archived Pay Stations: "${archived_pay_stations?}"
#     And Select Space Number: "${space_number}"
#     And Input First Space Number: "${specific_space_number}"
#     And Input License Plate: "${license_plate}"
#     And Select Ticket Number: "${ticket_number}"
#     And Select Coupon Code: "${coupon_code}"
#     And Input Coupon Code: "${actual_coupon_code}"
#     And Select Transaction Type: "${transaction_type}"
#     And Select Group By: "${group_by}"
#     And Select Output Format: "${output_format}"
#     And Proceed To Step 3
#     And Select Schedule: "${schedule}"
#     And Click Save Report
#     Then Report "${report_type}" Is Pending
#     And Report "${report_type}" Is Completed


# Transaction - Credit Card (Details) 18
#     [Documentation]  Runs a Transaction - All Summary Report and Verifies it's creation
#     ${report_type}=  Set Variable    Transaction - Credit Card
#     ${detail_type}=  Set Variable    Details
#     ${transaction_date/time}=    Set Variable    All
#     ${location_type}=  Set Variable    Pay Station Route
#     ${archived_pay_stations?}=  Set Variable    No
#     ${space_number}=  Set Variable    N/A
#     ${license_plate}=  Set Variable    ${EMPTY}
#     ${ticket_number}=  Set Variable    All
#     ${coupon_code}=  Set Variable    N/A
#     ${transaction_type}=  Set Variable    Monthly
#     ${group_by}=  Set Variable    Day
#     ${output_format}=  Set Variable    PDF
#     ${schedule}=  Set Variable    Submit Now

#     Given Select Report Type: "${report_type}"
#     And Proceed To Step 2
#     And Select Report Detail Type: "${detail_type}"
#     And Select Transaction Date/Time: "${transaction_date/time}"
#     And Select Location Type: "${location_type}"
#     And Select All Location Type Listings
#     And Include Archived Pay Stations: "${archived_pay_stations?}"
#     And Select Space Number: "${space_number}"
#     And Input First Space Number: "${specific_space_number}"
#     And Input License Plate: "${license_plate}"
#     And Select Ticket Number: "${ticket_number}"
#     And Select Coupon Code: "${coupon_code}"
#     And Input Coupon Code: "${actual_coupon_code}"
#     And Select Transaction Type: "${transaction_type}"
#     And Select Group By: "${group_by}"
#     And Select Output Format: "${output_format}"
#     And Proceed To Step 3
#     And Select Schedule: "${schedule}"
#     And Click Save Report
#     Then Report "${report_type}" Is Pending
#     And Report "${report_type}" Is Completed


# Transaction - Credit Card (Details) 19
#     [Documentation]  Runs a Transaction - All Summary Report and Verifies it's creation
#     ${report_type}=  Set Variable    Transaction - Credit Card
#     ${detail_type}=  Set Variable    Details
#     ${transaction_date/time}=    Set Variable    Month
#     ${start_month}=  Set Variable    February
#     ${start_year}=  Set Variable    2015
#     ${end_month}=  Set Variable    April
#     ${end_year}=  Set Variable    2015
#     ${location_type}=  Set Variable    Pay Station
#     ${archived_pay_stations?}=  Set Variable    Yes
#     ${space_number}=  Set Variable    N/A
#     ${license_plate}=  Set Variable    ${EMPTY}
#     ${ticket_number}=  Set Variable    All
#     ${coupon_code}=  Set Variable    N/A
#     ${transaction_type}=  Set Variable    
#     ${group_by}=  Set Variable    Day
#     ${output_format}=  Set Variable    CSV
#     ${schedule}=  Set Variable    Submit Now

#     Given Select Report Type: "${report_type}"
#     And Proceed To Step 2
#     And Select Report Detail Type: "${detail_type}"
#     And Select Transaction Date/Time: "${transaction_date/time}"
#     And Input Start Month: "${start_month}"
#     And Input Start Year: "${start_year}"
#     And Input End Month: "${end_month}"
#     And Input End Year: "${end_year}"
#     And Select Location Type: "${location_type}"
#     And Select All Location Type Listings
#     And Include Archived Pay Stations: "${archived_pay_stations?}"
#     And Select Space Number: "${space_number}"
#     And Input First Space Number: "${specific_space_number}"
#     And Input License Plate: "${license_plate}"
#     And Select Ticket Number: "${ticket_number}"
#     And Select Coupon Code: "${coupon_code}"
#     And Input Coupon Code: "${actual_coupon_code}"
#     And Select Transaction Type: "${transaction_type}"
#     And Select Group By: "${group_by}"
#     And Select Output Format: "${output_format}"
#     And Proceed To Step 3
#     And Select Schedule: "${schedule}"
#     And Click Save Report
#     Then Report "${report_type}" Is Pending
#     And Report "${report_type}" Is Completed














# Transaction - Credit Card (Summary)

#     ${report_type}=  Set Variable    Transaction - Credit Card
#     ${detail_type}=  Set Variable    Summary
#     ${transaction_date/time}=    Set Variable    Today
#     ${location_type}=  Set Variable    Location
#     ${archived_pay_stations?}=  Set Variable    Yes
#     ${space_number}=  Set Variable    N/A
#     ${license_plate}=  Set Variable    ${EMPTY}
#     ${ticket_number}=  Set Variable    All
#     ${coupon_code}=  Set Variable    N/A
#     ${card_type}=  Set Variable    All
#     ${approval_status}=  Set Variable    All
#     ${merchant_account}=  Set Variable    All
#     ${card_number}=  Set Variable    All
#     ${transaction_type}=  Set Variable    All
#     ${group_by}=  Set Variable    None
#     ${schedule}=  Set Variable    Submit Now

#     Given Select Report Type: "${report_type}"
#     And Proceed To Step 2
#     And Select Report Detail Type: "${detail_type}"
#     And Select Transaction Date/Time: "${transaction_date/time}"
#     And Select Location Type: "${location_type}"
#     And Include Archived Pay Stations: "${archived_pay_stations?}"
#     And Select Space Number: "${space_number}"
#     And Input License Plate: "${license_plate}"
#     And Select Ticket Number: "${ticket_number}"
#     And Select Coupon Code: "${coupon_code}"
#     And Select Card Type: "${card_type}"
#     And Select Approval Status: "${approval_status}"
#     And Select Merchant Account: "${merchant_account}"
#     And Select Card Number: "${card_number}"Z
#     And Select Transaction Type: "${transaction_type}"
#     And Select Group By: "${group_by}"
#     And Proceed To Step 3
#     And Select Schedule: "${schedule}"
#     And Click Save Report
#     Then Report "${report_type}" Is Pending
#     And Report "${report_type}" Is Completed


# Transaction - Credit Card (Details)

#     ${report_type}=  Set Variable    Transaction - Credit Card
#     ${detail_type}=  Set Variable    Details
#     ${transaction_date/time}=    Set Variable    Today
#     ${location_type}=  Set Variable    Location
#     ${archived_pay_stations?}=  Set Variable    Yes
#     ${space_number}=  Set Variable    N/A
#     ${license_plate}=  Set Variable    ${EMPTY}
#     ${ticket_number}=  Set Variable    All
#     ${coupon_code}=  Set Variable    N/A
#     ${card_type}=  Set Variable    All
#     ${approval_status}=  Set Variable    All
#     ${merchant_account}=  Set Variable    All
#     ${card_number}=  Set Variable    All
#     ${transaction_type}=  Set Variable    All
#     ${group_by}=  Set Variable    None
#     ${schedule}=  Set Variable    Submit Now

#     Given Select Report Type: "${report_type}"
#     And Proceed To Step 2
#     And Select Report Detail Type: "${detail_type}"
#     And Select Transaction Date/Time: "${transaction_date/time}"
#     And Select Location Type: "${location_type}"
#     And Include Archived Pay Stations: "${archived_pay_stations?}"
#     And Select Space Number: "${space_number}"
#     And Input License Plate: "${license_plate}"
#     And Select Ticket Number: "${ticket_number}"
#     And Select Coupon Code: "${coupon_code}"
#     And Select Card Type: "${card_type}"
#     And Select Approval Status: "${approval_status}"
#     And Select Merchant Account: "${merchant_account}"
#     And Select Card Number: "${card_number}"
#     And Select Transaction Type: "${transaction_type}"
#     And Select Group By: "${group_by}"
#     And Proceed To Step 3
#     And Select Schedule: "${schedule}"
#     And Click Save Report
#     Then Report "${report_type}" Is Pending
#     And Report "${report_type}" Is Completed

# Stall Report

#     ${report_type}=  Set Variable    Stall Reports
#     ${space_location}=  Set Variable    Airport
#     ${archived_pay_stations?}=  Set Variable    Yes
#     ${space_number}=  Set Variable    All
#     ${stall_status}=  Set Variable    All
#     ${schedule}=  Set Variable    Submit Now

#     Given Select Report Type: "${report_type}"
#     And Proceed To Step 2
#     And Select Space Location: "${space_location}"
#     And Include Archived Pay Stations: "${archived_pay_stations?}"
#     And Select Space Number: "${space_number}"
#     And Select Stall Status: "${stall_status}"
#     And Proceed To Step 3
#     And Select Schedule: "${schedule}"
#     When Click Save Report
#     Then Report "${report_type}" Is Pending
#     And Report "${report_type}" Is Completed
#     #When View Report With Id: "${report_id}"
#     And Get Report With Id: "${report_id}"

*** Keywords ***

I Create A Transaction - All Report
    [arguments]
    ...  ${detail_type}=Summary

    ...  ${transaction_date/time}=Today
    ...  ${location_type}=Location
    ...  ${archived_pay_stations?}=Yes
    ...  ${space_number}=N/A
    ...  ${license_plate}=${EMPTY}
    ...  ${ticket_number}=All
    ...  ${coupon_code}=N/A
    ...  ${transaction_type}=All
    ...  ${group_by}=None
    ...  ${schedule}=Submit Now
    ...  ${output_format}=PDF

    Select Report Type: "Transaction - All"
    Proceed To Step 2
    Select Report Detail Type: "${detail_type}"
    Select Transaction Date/Time: "${transaction_date/time}"
    Select Location Type All: "${location_type}"
    Include Archived Pay Stations: "${archived_pay_stations?}"
    Select Space Number: "${space_number}"
    Input License Plate: "${license_plate}"
    Select Ticket Number: "${ticket_number}"
    Select Coupon Code: "${coupon_code}"
    Select Transaction Type: "${transaction_type}"
    Select Group By: "${group_by}"
    Select Output Format: "${output_format}"
    Proceed To Step 3
    Select Schedule: "${schedule}"
    Click Save Report
    Report "Transaction - All" Is Pending
    Report "Transaction - All" Is 

I Create A Transaction - Credit Card Report
    [arguments]
    ...  ${detail_type}=Summary

    ...  ${transaction_date/time}=Today
    ...  ${location_type}=Location
    ...  ${archived_pay_stations?}=Yes
    ...  ${space_number}=N/A
    ...  ${license_plate}=${EMPTY}
    ...  ${ticket_number}=All
    ...  ${coupon_code}=N/A
    ...  ${transaction_type}=All
    ...  ${group_by}=None
    ...  ${schedule}=Submit Now
    ...  ${output_format}=PDF

    ...  ${card_type}=All
    ...  ${approval_status}=All
    ...  ${merchant_account}=All
    ...  ${card_number}=All

    Select Report Type: "Transaction - All"
    Proceed To Step 2
    Select Report Detail Type: "${detail_type}"
    Select Transaction Date/Time: "${transaction_date/time}"
    Select Location Type All: "${location_type}"
    Include Archived Pay Stations: "${archived_pay_stations?}"
    Select Space Number: "${space_number}"
    Input License Plate: "${license_plate}"
    Select Ticket Number: "${ticket_number}"
    Select Coupon Code: "${coupon_code}"

    Select Card Type: "${card_type}"
    Select Approval Status: "${approval_status}"
    Select Merchant Account: "${merchant_account}"
    Select Card Number: "${card_number}"

    Select Transaction Type: "${transaction_type}"
    Select Group By: "${group_by}"
    Select Output Format: "${output_format}"
    Proceed To Step 3
    Select Schedule: "${schedule}"
    Click Save Report
    Report "Transaction - All" Is Pending
    Report "Transaction - All" Is Completed







