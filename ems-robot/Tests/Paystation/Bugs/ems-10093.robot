*** Settings ***
# Summary: Automation for EMS-10093
# Author: Paul Breland

Resource          ../../../data/data.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Library           DatabaseLibrary
Suite Teardown    Close Browser

Test Setup    Run Keywords
...               Login As Generic Customer    ${G_CUST_ADMIN_USERNAME}    Password$1
...         AND   Go To Settings Section

Test Teardown    Close Browser

Test Template    Test EMS-10093

*** Test Case ***

Test EMS-10093 D Following Number    402D   


Test EMS-10093 F Following Number    402F



*** Keywords ***

Verify Pay Station Name In Location Type List
    # This Keyword verifies that the Pay Station Name listed in the Location Type List
    # Note that in this case, Location Type List is Step 2 Report Details where the user chooses
    # between Location, Pay Station Route, and Pay Station
    [arguments]    ${new_name}
    Wait Until Element Is Visible    xpath=//li[@id='reportPayStations_full_node_0']//strong[contains(text(), '${new_name}')]

Test EMS-10093
    [arguments]    ${paystation_name}
    ${new_name}=    Set Variable    ${paystation_name}
    ${serial}=    Set Variable    ${G_PAYSTATION_0}
    ${report_type}=    Set Variable    Transaction - All
    ${location_type}=    Set Variable    Pay Station
    Click Pay Stations Tab
    Edit Pay Station Name    ${serial}    ${new_name}
    Verify Pay Station Name In Pay Station Details    ${new_name}
    Click Locations Tab
    Select Unassigned In Location Details    Unassigned
    Verify Pay Station Name In Edit Location All Pay Stations List    ${new_name}
    Go To Reports Section
    Click Create Report
    Select Report Type: "${report_type}"
    Proceed to Step 2
    Select Location Type: "${location_type}"
    Verify Pay Station Name In Location Type List    ${new_name}
    Cancel Report
    Go To Settings Section
    Click Pay Stations Tab
    Edit Pay Station Name    ${serial}    ${serial}
    Verify Pay Station Name In Pay Station Details    ${serial}