*** Settings ***
# Summary: Suite Setup
# Post Conditions:
#    ${G_CUST_ADMIN_USERNAME}
#    ${G_CUSTOMER_NAME}
#    ${G_PAYSTATION_X} where x is 0 to ${num_of_new_paystations}
#
# Author: Billy Hoang
# Time: Dec 3, 2015

Resource          ../../../data/data.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Library           DatabaseLibrary
Library           Collections
Suite Setup       Run Keywords  Set Up Suite Testing Environment

Suite Teardown    Run Keywords
...               Close Browser
...               AND    Log To Console    Suite Tear Down Is Completed

*** Variables ***
${new_customer_name}
${num_of_new_paystations}  5

*** Keywords ***

# - Connects To Database
# - Calls a Stored Procedure to create a new Customer and new Pay Stations
# - Globally declares: Customer Admin Username and Pay Stations
Set Up Suite Testing Environment
    Connect To Database    ${DB_API_MODULE_NAME}     ${DB_NAME}    ${DB_USERNAME}    ${DB_PASSWORD}    ${DB_HOST}    ${DB_PORT}

    # Construct a new customer name
    ${random}=  Get Time  epoch
    ${new_customer_name}=  Catenate  SEPARATOR=  name  ${random}
    Set Global Variable  ${G_CUSTOMER_NAME}  ${new_customer_name}
    Log To Console  New Customer Name is: ${G_CUSTOMER_NAME}

    Run Store Procedure To Inject Customer And Pay Stations  ${new_customer_name}

    Set New Customer Admin Username Globally  ${new_customer_name}
    Set Pay Stations Globally  ${new_customer_name}

    # Logs into the new customer for the first time and changes it's password to Password$1
    Log To Console    Performing initial login for ${G_CUST_ADMIN_USERNAME}
    Login As Generic Customer  ${G_CUST_ADMIN_USERNAME}  password
    Close Browser
    Log To Console    Initial login for ${G_CUST_ADMIN_USERNAME} completed
	

Run Store Procedure To Inject Customer And Pay Stations
    [arguments]  ${new_customer_name}
    Query    CALL sp_Preview_CreateAccount('DBAutoCustTest','DBAutoTitleTest','DBAutoOrgTest','${new_customer_name}',NULL);


Set New Customer Admin Username Globally
    [arguments]  ${customer_name}
    ${cust_admin_username}=  Catenate  SEPARATOR=  admin@  ${customer_name}
    Set Global Variable  ${G_CUST_ADMIN_USERNAME}  ${cust_admin_username}
    Log To Console  Customer Admin Username is: ${cust_admin_username}



Set Pay Stations Globally
    [arguments]  ${customer_name}
     ${list_of_paystations}=  Get New Pay Station Serial Numbers  ${customer_name}
    :FOR  ${index}  IN RANGE  0  ${num_of_new_paystations}
    \    Set Global Variable  ${G_PAYSTATION_${index}}  ${list_of_paystations[${index}][0]}
    \    Log To Console  Paystation_${index} is: ${G_PAYSTATION_${index}}


Get New Pay Station Serial Numbers
    [arguments]  ${customer_name}
    Connect To Database    ${dbapiModuleName}     ${dbName}    ${dbUsername}    ${dbPassword}    ${dbHost}    ${dbPort}
    ${customer_id}=  Query  SELECT Id FROM Customer WHERE Name = '${customer_name}'
    ${customer_id}=  Set Variable  ${customer_id[0][0]}
    ${customer_id}=  Convert To String  ${customer_id}
    Log To Console  Customer ID is: ${customer_id}
    Wait Until Keyword Succeeds  10s  1s  Row Count Is Equal To X  SELECT SerialNumber FROM PointOfSale WHERE CustomerId = '${customer_id}'  ${num_of_new_paystations}  
    ${result}=  Query  SELECT SerialNumber FROM PointOfSale WHERE CustomerId = '${customer_id}'
    [return]  ${result}

