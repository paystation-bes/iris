*** Settings ***
Documentation		Test Suite for testing the sending paystation events such as Heartbeat, Alerts and Transactions to a Paystation and verifying the results on the UI
...               Author: Johnson N

Resource          ../../Keywords/globalKeywordDirectory.robot
Library           HttpLibrary.HTTP
Resource          ../../data/alertData.robot


*** Test Cases ***
Test 1: Test Heartbeat
 	Send Heartbeat To Paystation: "${G_PAYSTATION_0}"

Test 2: Test Upper Door Opened
    Send Event: "${DOOR_OPENED}" To Paystation: "${G_PAYSTATION_1}"

Test 3: Multiple Events
 	@{list_of_events}=    Create List    ${PAYSTATION_ALARM_ON}    ${COIN_ACCEPTOR_JAM}    ${BILL_STACKER_FULL}    ${COIN_CHANGER_EMPTY}
 	Send Events: "${list_of_events}" To Paystation: "${G_PAYSTATION_2}"
