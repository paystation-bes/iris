*** Settings ***
Library           Selenium2Library
Library           AutoItLibrary
Resource          ../../../data/data.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Force Tags        smoke-test

Test Setup     		Run Keywords
...					Login as Generic Customer  ${CHILD_USERNAME}  Password$1
...  AND			Go To Accounts Section

Test Teardown    	Close Browser


*** Test Cases ***
Passcards Section - Create Passcard (Valid Passcard Number)
	Given Create A Passcard    5547621    Blackboard    All    ${EMPTY}    ${EMPTY}    ${EMPTY}    No    0    5
	And Delete Existing "Passcard": "5547621"

Passcard Section - Create Invalid Passcard (Invalid Passcard Number)
	Given Go To "Passcards" Section
	And Click "Add" In "Passcards"
	When Input Passcard Number: "3t4"
	Then Attention, Only Basic Numerical Value
	And Close Pop-up Window
	And Cancel Creating Passcard/Update Changes

Passcard Section - Create Invalid Passcard (Invalid Date Range)
	Given Click "Add" In "Passcards"
	And Input Passcard Number: "333"
	And Input Type: "Blackboard"
	And Input Valid From: "05/06/2016" To "02/05/2016"
	When Do Nothing
	Then Attention, Start Date Must Be Before End Date
	And Close Pop-up Window
	And Cancel Creating Passcard/Update Changes

Passcard Section - Create Invalid Passcard (Invalid Date Format From and To)
	Given Click "Add" In "Passcards"
	And Input Passcard Number: "321"
	And Input Type: "CBORD CS Gold"
	And Input Valid From: "3144" To "43251"
	When Click Save Passcard/Update Changes
	Then Attention, Invalid Date Format
	And Close Pop-up Window
	And Cancel Creating Passcard/Update Changes

Passcard Section - Create Invalid Passcard (Invalid Input for Max Usage)
	Given Click "Add" In "Passcards"
	And Input Passcard Number: "1234441223"
	And Input Type: "Blackboard live"
	And Select "Passcard" Location: "All"
	And Input Passcard Description: "Student usage"
	And Input Valid From: "02/01/2016" To "05/08/2016"
	When Input Maximum Usage: "df3"
	Then Attention, Only Basic Numerical Value
	And Close Pop-up Window
	And Cancel Creating Passcard/Update Changes

Passcard Section - Create A Valid Card, Edit, and Delete
	Given Create A Passcard   55542    Blackboard live    All      ${EMPTY}    05/25/2016     12/02/2016    Yes    20    55
	When Edit Existing "Passcard": "55542"
	Then Input Passcard Description: "Special offer for university/college students"
	And Click Save Passcard/Update Changes
	And Delete Existing "Passcard": "55542"

Passcard Section - Create A Card (Without Number, Invalid)
	Given Click "Add" In "Passcards"
	And Input Type: "Blackboard"
	When Click Save Passcard/Update Changes
	Then Attention, Number Is Required
	And Close Pop-up Window
	And Cancel Creating Passcard/Update Changes

Passcard Section - Create A Card (No Type, Invalid)
	Given Click "Add" In "Passcards"
	And Input Passcard Number: "32122"
	When Click Save Passcard/Update Changes
	Then Attention, System Unable To Save Information
	And Close Pop-up Window
	And Cancel Creating Passcard/Update Changes

Passcard Section - Search Existing Passcard
	Given Go To "Passcards" Section
	And Search For "Card Type/Number": "321321"

Passcard Section - Import File (Valid)
	Given Import A Passcard File    passcard_data.csv    Merge with existing records
	Then Passcards Should Be Visible From File

Passcard Section - Import File (Invalid Format)
	Given Click "Import" In "Passcards"
	And Upload File In Passcard: "passcard_invalid.csv"
	And Select Passcard Import Mode: "Replace existing records"
	When Click Upload New List
	Then Attention, Incorrect Number Of Columns
	And Close Pop-up Window
	And Cancel Upload New List Of "Passcards"

Passcard Section - Upload List Of Passcards (No File)
	Given Click "Import" In "Passcards"
	And Select Passcard Import Mode: "Merge with existing records"
	When Click Upload New List
	Then Attention, File Is Required
	And Close Pop-up Window
	And Cancel Upload New List Of "Passcards"

Passcard Section - Export All Passcards
	Given Export A Passcard File

Passcard Section - Delete All Passcards
	Given Delete All Passcards