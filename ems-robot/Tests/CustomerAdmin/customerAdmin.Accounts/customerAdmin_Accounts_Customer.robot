*** Settings ***
Library           Selenium2Library
Library           AutoItLibrary
Resource          ../../../data/data.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Force Tags        smoke-test

Test Setup     		Run Keywords
...					Login as Generic Customer  ${G_CUST_ADMIN_USERNAME}  Password$1
...  AND			Go To Accounts Section

Test Teardown    	Close Browser

*** Test Cases ***

Accounts Section - Create A New Customer Account
	Given Create A New Customer Account    Bobb    Ae    bobb@ae.com    Go    0    ${EMPTY}

Accounts Section - Create A New Customer Account, Edit, And Delete
	Given Create A New Customer Account    Marry    Jane    marry@jane.com    '${EMPTY}'    0    0
	And Search For "Name/Email": "Bobb"
	And Delete Existing "Customer": "Bobb"
	And Go To Accounts Section
	And Edit Existing "Customer": "marry@jane.com"
	And Input FirstName Info: "Marry Betty"
	Then Click Save Customer/Update Changes
	And Delete Existing "Customer": "marry@jane.com"

Accounts Section - Create A New Customer (Invalid Name)
	Given Add New Customer Button
	When Input FirstName Info: "be&"
	Then Attention, Only Letters, Digits, Space, Dot, Comma, Quote And Special Characters
	And Close Pop-up Window
	And Cancel Creating Customer/Update Changes

Accounts Section - Create A New Customer (Invalid Email)
	Given Add New Customer Button
	And Input FirstName Info: "Ann"
	And Input LastName Info: "Jo"
	And Input Email Info: "eh*&@mne"
	When Click Save Customer/Update Changes
	Then Attention, "eh*&@mne" Is Not Valid
	And Close Pop-up Window
	And Cancel Creating Customer/Update Changes

Accounts Section - Create A New Customer (No first name, invalid)
	Given Go To "Customer Accounts" Section
	And Add New Customer Button
	And Input LastName Info: "Bee"
	When Click Save Customer/Update Changes 
	Then Attention, First Name Is Required
	And Close Pop-up Window
	And Cancel Creating Customer/Update Changes

Accounts Section - Create A New Customer (No last name, invalid)
	Given Go To "Customer Accounts" Section
	And Add New Customer Button
	And Input FirstName Info: "Apple"
	When Click Save Customer/Update Changes 
	Then Attention, Last Name Is Required
	And Close Pop-up Window
	And Cancel Creating Customer/Update Changes
