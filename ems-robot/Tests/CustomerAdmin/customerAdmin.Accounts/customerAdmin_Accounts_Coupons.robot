*** Settings ***
Library           Selenium2Library
Library           AutoItLibrary
Resource          ../../../data/data.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Force Tags        smoke-test

Test Setup     		Run Keywords
...					Login as Generic Customer  ${G_CUST_ADMIN_USERNAME}  Password$1
...  AND			Go To Accounts Section

Test Teardown    	Close Browser

*** Test Cases ***

Coupons Section - Create A Coupon
	Given Create A Coupon    123289   ${EMPTY}    50    Percent    01/01/2016    02/22/2016    All    Unlimited    Yes    All    5-20
	And Delete Existing "Coupon": "123289"

Coupons Section - Create A Coupon (Invalid Space Number - alphanumeric)
	Given Click "Add" In "Coupons"
	And Input Coupon Code Number: "1111"
	And Input Description Info: "Free Parking"
	And Input Discount: "100 Percent"
	And Allowed For Daily Usage: "No"
	When Select Available For: "All" And "a-4"
	Then Attention, Numbers Separated By Dash/Comma Only
	And Close Pop-up Window
	And Cancel Creating Coupon/Update Changes

Coupons Section - Create A Coupon (Invalid Space Number - symbols)
	Given Click "Add" In "Coupons"
	And Input Coupon Code Number: "1111"
	And Input Description Info: "Half Off"
	And Input Discount: "50 Percent"
	And Allowed For Daily Usage: "No"
	And Select Available For: "All" And "$#@@#!!!"
	Then Attention, Numbers Separated By Dash/Comma Only
	And Close Pop-up Window
	And Cancel Creating Coupon/Update Changes

Coupons Section - Create A Coupon (Valid Space Number - BLANK Spaces)
	Given Create A Coupon    11511    Free    100    Percent    ${EMPTY}    ${EMPTY}    All    5     Yes    Pay By Space    ${SPACE*7}
	And Delete Existing "Coupon": "11511"

Coupons Section - Create, Edit, and Delete an Existing Coupon
	Given Create A Coupon    5555    ${EMPTY}    40    Dollars    05/11/2016    06/12/2016    Unassigned    2    No    All    4-23
	And Edit Existing "Coupon": "5555"
	And Input Description Info: "Employee discounts"
	When Input Discount: "50 Percent"
	Then Click Save Coupon/Update Changes
	And Delete Existing "Coupon": "5555"

Coupons Section - Create Coupon (Invalid Date - End Date Is Before Curernt Date)
	Given Click "Add" In "Coupons"
	And Input Coupon Code Number: "5714"
	And Input Description Info: "30$ Off"
	And Input Discount: "30 Dollars"
	And Input Duration: "01/02/2014" To "05/05/2014"
	And Allowed For Daily Usage: "Yes"
	And Select Available For: "All" And "5-78"
	When Click Save Coupon/Update Changes
  	Then Attention, End Date Cannot Be Before Current Date
	And Close Pop-up Window
	And Cancel Creating Coupon/Update Changes

Coupons Section - Create Coupon (Invalid Date Range - Start Date Is Before End Date)
	Given Click "Add" In "Coupons"
	And Input Coupon Code Number: "4t23"
	And Input Description Info: "30% Off"
	And Input Discount: "30 Percent"
	And Input Duration: "08/20/2016" To "01/02/2016"
	And Do Nothing
  	Then Attention, Start Date Must Be Before End Date
	And Close Pop-up Window
	And Cancel Creating Coupon/Update Changes

Coupons Section - Create A Coupon (Invalid Duration of Coupon)
	Given Click "Add" In "Coupons"
	And Input Coupon Code Number: "3423"
	And Input Discount: "32 Percent"
	And Input Duration: "1212" To "23456"
	When Click Save Coupon/Update Changes
	Then Attention, Invalid Date Format
	And Close Pop-up Window
	And Cancel Creating Coupon/Update Changes

Coupon Section - Create Coupon (Invalid Coupon Code)
	Given Click "Add" In "Coupons"
	When Input Coupon Code Number: "#"
	Then Attention, Only Letters And Digits
	And Close Pop-up Window
	And Cancel Creating Coupon/Update Changes

Coupon Section - Create Coupon (No Coupon Code)
	Given Click "Add" In "Coupons"
	And Input Discount: "30 Percent"
	And Select Available For: "All" And "4-55"
	When Click Save Coupon/Update Changes
	Then Attention, Coupon Code Is Required
	And Close Pop-up Window
	And Cancel Creating Coupon/Update Changes

Coupons Section - Create Coupon (No Discount)
	Given Click "Add" In "Coupons"
	And Input Coupon Code Number: "3321455"
	And Select Available For: "Pay and Display/Pay by License Plate" And "${EMPTY}"
	When Click Save Coupon/Update Changes
	Then Attention, Percentage Discount Is Required
	And Close Pop-up Window
	And Cancel Creating Coupon/Update Changes

Coupons Section - Create Coupon (No Selection of Available For)
	Given Click "Add" In "Coupons"
	And Input Coupon Code Number: "32144125"
	And Input Discount: "40 Dollars"
	When Click Save Coupon/Update Changes
	Then Attention, Choose At Least One Pay Option
	And Close Pop-up Window
	And Cancel Creating Coupon/Update Changes

Coupons Section - Import File (Valid)
	Given Import A Coupon File    coupons_data.csv    Merge with existing records
	Then Coupon Codes Should Be Visible From File

Coupons Section - Import File (Invalid Format)
	Given Click "Import" In "Coupons"
	And Upload File In Coupon: "coupons_invalid.csv"
	And Select Coupon Import Mode: "Replace existing records"
	When Click Upload New List
	Then Attention, Incorrect Number Of Columns
	And Close Pop-up Window
	And Cancel Upload New List Of "Coupons"

Coupons Section - Import File (Invalid Start Date In File)
	Given Click "Import" In "Coupons"
	And Upload File In Coupon: "coupons_data_invalid_date.csv"
	And Select Coupon Import Mode: "Merge with existing records"
	When Click Upload New List
	Then Attention, Start Date Is Invalid
	And Close Pop-up Window
	And Cancel Upload New List Of "Coupons"

Coupons Section - Import File (No File Selected, Invalid)
	Given Click "Import" In "Coupons"
	And Select Coupon Import Mode: "Merge with existing records"
	When Click Upload New List
	Then Attention, File Is Required
	And Close Pop-up Window
	And Cancel Upload New List Of "Coupons"

Coupons Section - Export
	Given Export A Coupon File   

Coupons Section - Search Existing Coupon
	Given Go To "Coupons" Section
	And Search For Existing Coupon Code: "7777", "75", And "All"

