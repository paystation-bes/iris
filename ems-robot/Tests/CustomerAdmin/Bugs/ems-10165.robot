*** Settings ***
# Summary: Automation for EMS-10165
# Author: Billy Hoang

Resource          ../../../data/data.robot
Resource          ../../../data/alertData.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Library           DatabaseLibrary
Suite Teardown    Close Browser

Test Setup    Run Keywords
...               Login As Generic Customer    ${G_CUST_ADMIN_USERNAME}    Password$1
...         AND   Go To Settings Section

Test Teardown    Close Browser


*** Test Cases ***

# Steps:
# Send Coin Canister Removed Alert To pay station
# Alert Should appear in recent activity
# Create Collection Alert with Coin Canister exceeds $10
# Send $11 to trigger the collection alert
# Clear the Coin Canister Removed Alert By sending a Coin Canister Inserted Alert to pay station
# Go to recent activity and check to see that the Coin Canister Removed Alert is resolved and the Coin Collection Alert Is still active


Test EMS-10165

    ${random}=  Get Time  Epoch
    ${user_defined_alert_name}=  Catenate  SEPARATOR=  Alert  ${random}
    ${alert_route}=  Set Variable  All Pay Stations
    ${alert_type}=  Set Variable  Coin Canister
    ${paystation_id}=  Set Variable  ${G_PAYSTATION_0}

   Given Send Event: "${COIN_CANISTER_REMOVED}" To Paystation: "${paystation_id}"
   ${coin_canister_alert_id}=  Alert Is Triggered In Recent Activity Just Now  Coin Canister Removed  ${paystation_id}
   And Create User Defined Alert  ${user_defined_alert_name}  ${alert_route}  ${alert_type}  exceeds=10  exceeds_type=Dollar
   And Send Cash Transaction To Paystation  Coin  0  0  0  11  ${paystation_id}
   And Alert Is Triggered In Recent Activity Just Now  ${user_defined_alert_name}  ${paystation_id}
   And Send Event: "${COIN_CANISTER_INSERTED}" To Paystation: "${paystation_id}"
   Then Alert: "${coin_canister_alert_id}" Is Resolved By: "System" In Recent Activity Just Now In Paystation: "${paystation_id}"
   And Alert Is Active In Recent Activity  ${user_defined_alert_name}  ${paystation_id}