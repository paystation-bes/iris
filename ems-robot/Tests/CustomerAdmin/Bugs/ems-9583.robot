*** Settings ***
# Summary: Automation for EMS-10165
# Author: Billy Hoang

Resource          ../../../data/data.robot
Resource          ../../../data/alertData.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Library           DatabaseLibrary
Suite Teardown    Close Browser

Test Setup    Run Keywords
...               Login As Generic Customer    ${G_CUST_ADMIN_USERNAME}    Password$1
...         AND   Go To Settings Section

Test Teardown    Close Browser


*** Test Cases ***


# Steps:
# Send Alerts Out of order
# As long as there is a clear for an alert that is after the alert, then the alert should be cleared


Test EMS-9583

    ${paystation_id}=  Set Variable  ${G_PAYSTATION_1}

    Send Event: "${COIN_CANISTER_REMOVED}" To Paystation: "${paystation_id}"
    ${coin_canister_removed_alert_id}=  Alert Is Triggered In Recent Activity Just Now  Coin Canister Removed  ${paystation_id}
    Send Event: "${COIN_CANISTER_INSERTED}" To Paystation: "${paystation_id}"
    Alert: "${coin_canister_removed_alert_id}" Is Resolved By: "system" In Recent Activity Just Now In Paystation: "${paystation_id}"
    Send Event: "${COIN_CANISTER_INSERTED}" To Paystation: "${paystation_id}"
    Send Event: "${COIN_CANISTER_INSERTED}" To Paystation: "${paystation_id}"
    Send Event: "${COIN_CANISTER_INSERTED}" To Paystation: "${paystation_id}"