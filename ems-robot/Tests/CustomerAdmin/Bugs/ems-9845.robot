*** Settings ***
# Summary: Automation for EMS-9845
# Author: Billy Hoang

Resource          ../../../data/data.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Library           DatabaseLibrary
Suite Teardown    Close Browser

Test Setup    Run Keywords
...               Login As Generic Customer    ${G_CUST_ADMIN_USERNAME}    Password$1
...         AND   Go To Settings Section

Test Teardown    Close Browser



*** Test Case ***

# STEPS
# CREATE USER DEFINED ALERT
# DISABLE USER DEFINED ALERT DONE
# TRIGGER THE CONDITION OF THAT ALERT DONE
# ASSERT THAT THE ALERT IS NOT TRIGGERED
# ENABLE USER DEFINED ALERT
# ASSERT THAT THE ALERT IS TRIGGERED

Test EMS-9845
    ${random}=  Get Time  Epoch
    ${alert_name}=  Catenate  SEPARATOR=  Alert  ${random}
    ${alert_route}=  Set Variable  Downtown Collections
    ${alert_type}=  Set Variable  Running Total
    ${exceeds}=  Set Variable  5
    ${dollar}=  Set Variable  6
    ${paystation_id}=  Set Variable  ${G_PAYSTATION_0}

    Log To Console  Alert Name: ${alert_name}

    Given Create User Defined Alert
    ...  ${alert_name}
    ...  ${alert_route}
    ...  ${alert_type}
    ...  exceeds=${exceeds}
    And Disable User Defined Alert  ${alert_name}
    And Send "${dollar}" Cash Transaction To Paystation: "${paystation_id}"
    And User Defined Alert Is Not Triggered In Recent Activity  ${alert_name}  ${paystation_id}
    When Enable User Defined Alert  ${alert_name}
    Then Alert Is Triggered In Recent Activity Just Now  ${alert_name}  ${paystation_id}
    And User Defined Alert Is Found In Database As Active  ${alert_name}
    # Post-Condition: the created alert is deleted
    Alert Is Deleted  ${alert_name}






