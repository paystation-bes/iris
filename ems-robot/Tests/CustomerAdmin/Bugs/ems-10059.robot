*** Settings ***
# Summary: Automation for EMS-10059
# Author: Billy Hoang

Resource          ../../../data/data.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Library           DatabaseLibrary
Suite Teardown    Close Browser

Test Setup    Run Keywords
...               Login As Generic Customer    ${G_CUST_ADMIN_USERNAME}    Password$1
...         AND   Go To Settings Section

Test Teardown    Close Browser


*** Test Cases ***

# Steps:
#Preconditions:
#1. Pay Station has enough dollar value of transactions to trigger the default Running Total Alert (over $1000)
#
#Steps to Reproduce:
#1. In the database, verify that the Default Running Total Alert has been entered in POSEventHistory
#SELECT * FROM POSEventHistory where PointOfSaleId=592;
#(change PointOfSaleId to the appropriate test paystation value). There should be an entry with CustomerAlertTypeId 102 and EventSeverityTypeId 3
#2. In Iris, create a Running Total Alert which triggers at, for example, $800
#3. After saving the Alert, it should trigger automatically
#4. Check POSEventHistory again.
#
#Result:
#1. User-defined Running Total Alert has an entry with EventSeverityTypeId = 3
#2. No clear event for the Default Running Total Alert
#
#Expected Result:
#1. User-defined Running Total Alert has an entry with EventSeverityTypeId = 3
#2. Default Running Total Alert has an entry with EventSeverityTypeId = 0

Test EMS-10059

    ${random}=  Get Time  Epoch
    ${user_defined_alert_name}=  Catenate  SEPARATOR=  Alert  ${random}
    ${alert_route}=  Set Variable  All Pay Stations
    ${alert_type}=  Set Variable  Running Total
    ${exceeds}=  Set Variable  800
    ${dollar}=  Set Variable  1001.00
    ${paystation_id}=  Set Variable  ${G_PAYSTATION_0}
    Log To Console  Using Pay station:  ${G_PAYSTATION_0}
    ${default_alert_name}=  Set Variable  Running Total Dollar Default

    Given Send "${dollar}" Cash Transaction To Paystation: "${paystation_id}"

    ${default_alert_id}=  Alert Is Triggered In Recent Activity Just Now  ${default_alert_name}  ${paystation_id}
    ${previous_index_default_alert}=  Get Current Index Of Alert In POSEventHistory  ${default_alert_name}  ${G_CUSTOMER_ID}  ${paystation_id}

    And Alert Is Entered In The Database In POSEventHistory As Active  ${default_alert_name}  ${G_CUSTOMER_ID}  ${paystation_id}  3
    When Create User Defined Alert  ${user_defined_alert_name}  ${alert_route}  ${alert_type}  exceeds=${exceeds}
    ${user_defined_alert_id}=  Alert Is Triggered In Recent Activity Just Now  ${user_defined_alert_name}  ${paystation_id}
    And Alert Is Entered In The Database In POSEventHistory As Active  ${user_defined_alert_name}  ${G_CUSTOMER_ID}  ${paystation_id}  3

    And Alert: "${default_alert_id}" Is Resolved By: "System" In Recent Activity Just Now In Paystation: "${paystation_id}"
    And Alert Is Updated In The Database In POSEventHistory  ${default_alert_name}  ${G_CUSTOMER_ID}  ${paystation_id}  0  0  ${previous_index_default_alert}

    ${previous_index_default_alert}=  Get Current Index Of Alert In POSEventHistory  ${default_alert_name}  ${G_CUSTOMER_ID}  ${paystation_id}
    And Alert Is Deleted  ${user_defined_alert_name}


    And Alert Is Updated In The Database In POSEventHistory  ${default_alert_name}  ${G_CUSTOMER_ID}  ${paystation_id}  3  1  ${previous_index_default_alert}

    And Alert: "${user_defined_alert_id}" Is Resolved By: "System" In Recent Activity Just Now In Paystation: "${paystation_id}"
    And Alert Is Triggered In Recent Activity Just Now  ${default_alert_name}  ${paystation_id}



















