*** Settings ***
Library           Selenium2Library
Resource		  ../../../Keywords/globalKeywordDirectory.robot
Resource		  ../../../Keywords/uiCrawlerKeywords/UICrawlerKeywords.robot


Suite Setup       Login as Generic Customer    ${G_CHILD_USERNAME}    Password$1
Suite Teardown    Close Browser


*** Test Cases ***

Customer Admin - Dashboard: Finance
	Given Go To 'Dashboard' Section (Left)
	When Click Finance Tab 1
	Then Should Be Visible: "Total Revenue by Day"
	And Should Be Visible: "Total Revenue Last 30 Days"
	And Should Be Visible: "Total Revenue Last 7 Days"
	And Should Be Visible: "Total Revenue by Location"
	And Should Be Visible: "Purchases by Rate"
	And Should Be Visible: "Top 5 Locations Last 30 Days"
	And Should Be Visible: "Bottom 5 Locations - 30 Day"
	And Should Be Visible: "Top 5 Pay Stations - 30 Day"

Customer Admin - Dashboard: Operations
	Given Click Operations Tab 1
	And Should Be Visible: "Paid Occupancy by Location"
	And Should Be Visible: "Purchases by Hour"
	And Should Be Visible: "Pay Stations with Alerts"
	And Should Be Visible: "Paid Occupancy by Hour"
	And Should Be Visible: "Utilization by Location"
	And Should Be Visible: "Space Turnover by Day"

Customer Admin - Dashboard: Map
	Given Click Map Tab 1
	And Should Be Visible: "Map"
	And Zoom On Map Should Be Visible

Customer Admin - Dashboard: Parker Metrics
	Given Click Parker Metrics Tab 1
	And Should Be Visible: "Avg Revenue by Day"
	And Should Be Visible: "Avg Price by Day"
	And Should Be Visible: "Avg Duration by Day"

Customer Admin - Dashboard: Card Processing
	Given Click Card Processing Tab 1
	And Should Be Visible: "Monthly Revenue by Method"
	And Should Be Visible: "Daily Revenue by Method"
	And Should Be Visible: "Revenue by Method - 24h"
	And Should Be Visible: "Monthly Purchases by Method"
	And Should Be Visible: "Daily Purchases by Method"
	And Should Be Visible: "Purchases by Method - 24h"
	# And Edit In Dashboard
	# When Add Widget
	# Then Add Widget Window Pops Up
	# And Cancel Adding Widget
	# And Cancel Changes On Dashboard
	# Currently editing doesn't work for it, need to discard any changes

Customer Admin - Reports: Reporting Dashboard
	Given Go To 'Reports' Section (Left)
	And Click Reporting Dashboard Tab 1
	And Header Should Be Visible: "Pending and Incomplete Reports"
	And Header Should Be Visible: "Scheduled Reports"
	And Header Should Be Visible: "Completed Reports"
	When Click Create Report
	And I Create A Tax Report Report 
	Then Page Should Contain     Tax Report  

Customer Admin - Reports: Permit Lookup
	Given Click Permit Lookup Tab 1
	And Header Should Be Visible: "Permit Lookup"

Customer Admin - Maintenance: Current Alerts
	Given Go To 'Maintenance' Section (Left)
	When Click Current Alerts Tab 1
	And Zoom On Map Should Be Visible
	And Header Should Be Visible: "Current Alerts"
	And Element Should Be Visible    xpath=//a[@tooltip='Save Current Alerts list as a PDF.']
	And Element Should Be Visible    css=#locationRouteACExpand
	And Element Should Be Visible    css=#severityACExpand
	And Element Should Be Visible    css=#moduleACExpand

Customer Admin - Maintenance: Resolved Alerts
	Given Click Resolved Alerts Tab 1
	And Zoom On Map Should Be Visible
	And Header Should Be Visible: "Resolved Alerts"
	And Element Should Be Visible    css=#locationRouteACExpand
	And Element Should Be Visible    css=#severityACExpand
	And Element Should Be Visible    css=#moduleACExpand

Customer Admin - Maintenance: Pay Station Summary
	Given Click Pay Station Summary Tab 1
	And Header Should Be Visible: "Pay Station Maintenance Summary"
	And Element Should Be Visible    xpath=//label[text()='Severity']
	And Table Info Should Be Visible: "Pay Stations"
	And Table Info Should Be Visible: "Routes"
	And Table Info Should Be Visible: "Locations"
	And Table Info Should Be Visible: "Last Seen"
	And Table Info Should Be Visible: "Battery"
	And Table Info Should Be Visible: "Paper"

Customer Admin - Collections: Collections Required
	Given Go To 'Collections' Section (Left)
	And Click Collections Required Tab 1
	And Zoom On Map Should Be Visible
	And Element Should Be Visible    css=#showUserCheck
	And Header Should Be Visible: "Collections Required"
	And Element Should Be Visible    css=#locationRouteACExpand
	And Element Should Be Visible    css=#coinCheck
	And Element Should Be Visible    css=#billsCheck
	And Element Should Be Visible    css=#ccardCheck
	And Element Should Be Visible    css=#totalCheck
	And Element Should Be Visible    css=#overdueCheck

Customer Admin - Collections: Recent Collections
	Given Click Recent Collections Tab 1
	And Zoom On Map Should Be Visible
	And Element Should Be Visible    css=#showUserCheck
	And Header Should Be Visible: "Recent Collections"
	And Element Should Be Visible    css=#locationRouteACExpand
	And Element Should Be Visible    css=#coinCheck
	And Element Should Be Visible    css=#billsCheck
	And Element Should Be Visible    css=#ccardCheck
	And Element Should Be Visible    css=#totalCheck
	And Element Should Be Visible    css=#overdueCheck

Customer Admin - Collections: Digital Collect Users
	Given Click Digital Collect Users Tab 1
	And Zoom On Map Should Be Visible
	And Element Should Be Visible    css=#showActiveCollectionsCheck
	And Element Should Be Visible    css=#showRecentCollectionsCheck

Customer Admin - Collections: Pay Station Summary
	Given Click Pay Station Summary Tab 1
	And Wait Until Element Is Visible    css=#filterLocationRouteExpand
	And Element Should Be Visible    css=#filterSeverityLabel
	And Header Should Be Visible: "Pay Station Collection Summary"
	And Table Info Should Be Visible: "Pay Stations"
	And Table Info Should Be Visible: "Routes"
	And Table Info Should Be Visible: "Locations"
	And Table Info Should Be Visible: "Coin"
	And Table Info Should Be Visible: "Bill"
	And Table Info Should Be Visible: "Card"
	And Table Info Should Be Visible: "Running Total"
	And Table Info Should Be Visible: "Last Collected"

Customer Admin - Accounts: Customer Accounts
	Given Go To 'Accounts' Section (Left)
	And Click Customer Accounts Tab 1
	And Header Should Be Visible: "Accounts"
	And Table Info Should Be Visible: "First Name"
	And Table Info Should Be Visible: "Last Name"
	And Table Info Should Be Visible: "Email"
	When Create A New Customer Account    Apple    Bee    apple@bee.com    ${EMPTY}    ${EMPTY}    ${EMPTY}
	Then Page Should Contain     apple@bee.com
	And Search For "Name/Email": "Apple"
	And Header Should Be Visible: "Account Details"
	And Table Info Should Be Visible: "Coupon Code"
	And Table Info Should Be Visible: "Discount"
	And Table Info Should Be Visible: "# of Uses Left"
	And Table Info Should Be Visible: "Daily Single Use"
	And Table Info Should Be Visible: "Number"
	And Table Info Should Be Visible: "Type"
	And Table Info Should Be Visible: "Restricted to a single valid permit"
	And Table Info Should Be Visible: "Grace Period"
	And Delete Existing "Customer": "apple@bee.com"

Customer Admin - Accounts: Coupons
	Given Click Coupons Tab 1
	And Header Should Be Visible: "Coupons"
	And Table Info Should Be Visible: "Coupon Code"
	And Table Info Should Be Visible: "Discount"
	And Table Info Should Be Visible: "Start Date"
	And Table Info Should Be Visible: "End Date"
	And Table Info Should Be Visible: "# of Uses Left"
	And Table Info Should Be Visible: "Daily Single Use" 
	When Create A Coupon    1111    ${EMPTY}    10    Percent    ${EMPTY}    ${EMPTY}    All    3    NO    Pay and Display/Pay by License Plate    ${EMPTY}
	Then Table Info Should Be Visible: "1111"
	When Click "Import" In "Coupons"
	Then Header Should Be Visible: "Upload New List of Discount Coupons"
	And Cancel Upload New List Of "Coupons"
	When Search For Existing Coupon Code: "1111", "10", And "All"
	Then Header Should Be Visible: "Coupon Details" 
	And Delete Existing "Coupon": "1111"

Customer Admin - Accounts: Passcards
	Given Click Passcards Tab 1
	And Header Should Be Visible: "Passcards"
	And Table Info Should Be Visible: "Number"
	And Table Info Should Be Visible: "Type"
	And Table Info Should Be Visible: "Valid From"
	And Table Info Should Be Visible: "Valid To"
	And Table Info Should Be Visible: "Restricted"
	And Table Info Should Be Visible: "Grace Period"
	When Click "Add" In "Passcards"
	And Element Should Be Visible    css=#formCardTypeExpand
	And Element Should Be Visible    css=#formLocationExpand
	And Page Should Contain    Valid From:
	And Cancel Creating Passcard/Update Changes
	When Click "Import" In "Passcards"
	Then Header Should Be Visible: "Upload new list of Passcards"
	And Cancel Upload New List Of "Passcards"

Customer Admin - Card Management: Banned Cards
	Given Go To 'Card Management' Section (Left)
	And Click Banned Cards Tab 1
	And Header Should Be Visible: "Banned Card"
	And Table Info Should Be Visible: "Card Number"
	And Table Info Should Be Visible: "Card Type"
	And Table Info Should Be Visible: "Card Expiry Date"
	And Table Info Should Be Visible: "Added Date"
	And Table Info Should Be Visible: "Source"
	When Click Add/Import Banned Card: "Add"
	And Page Should Contain    Add Banned Card
	And Page Should Contain    Card Expiry Date
	And Click Element    xpath=//span[text()='Cancel']
	When Click Add/Import Banned Card: "Import"
	Then Page Should Contain    Import Banned Cards
	And Page Should Contain    Importing a list of banned cards requires the use of a .csv file.
	And Cancel Import Banned Card

Customer Admin - Card Management: Card Refunds
	Given Click Card Refunds Tab 1
	And Header Should Be Visible: "Transactions"
	And Table Info Should Be Visible: "Card Number"
	And Table Info Should Be Visible: "Card Expiry Date"
	And Table Info Should Be Visible: "Transaction Date"
	And Table Info Should Be Visible: "Amount"

Customer Admin - Settings: Global
	Given Go To 'Settings' Section (Left)
	And Click Global Tab 1
	And Click Sub-Tab: "Settings"
	And Header Should Be Visible: "Global Preferences"
	And Header Should Be Visible: "Digital API / Partner Integrations"
	And Edit Flex Server Credentials
	And Page Should Contain     URL
	And Click Element    xpath=//span[text()='Cancel']
	When Click Sub-Tab: "System Notifications"
	Then Header Should Be Visible: "System Notifications"
	And Page Should Contain    Current Notifications
	And Page Should Contain    Past Notifications
	When Click Sub-Tab: "Recent Activity"
	Then Header Should Be Visible: "Activity Log"
	And Table Info Should Be Visible: "User"
	And Table Info Should Be Visible: "Activity"
	And Table Info Should Be Visible: "Date/Time"

Customer Admin - Settings: Rates
	Given Click Rates Tab 1
	And Click Rates SubTab: Rates
	And Header Should Be Visible: "Rates"
	And Click Add Button
	And Header Should Be Visible: "Add Rate"
	When Click Sub-Tab: "Rate Profiles"
	Then Header Should Be Visible: "Rate Profiles"
	And Click Add Button
	And Header Should Be Visible: "Add Rate Profile"
	When Click Sub-Tab: "Global Rate Settings"
	# Nothing happens when Global Rate Settings is pressed under Settings->Rates->Global Rate Settings

Customer Admin - Settings: Locations
	Given Click Locations Tab 1
	And Click Sub-Tab: "Location Details"
	And Header Should Be Visible: "Locations"
	When Click Sub-Tab: "Extend By Phone"
	And Header Should Be Visible: "Locations"
	And Header Should Be Visible: "Global Settings"
	And Page Should Contain    Warning Period:

Customer Admin - Settings: Pay Stations
	Given Click Pay Stations Tab 1
	And Click Sub-Tab: "Pay Station List"
	And Header Should Be Visible: "Pay Stations"
	When Click Sub-Tab: "Pay Station Configuration"
	Then Header Should Be Visible: "Pay Station Configuration"
	And Zoom On Map Should Be Visible
	And Element Should Be Visible    css=#okStatus
	And Element Should Be Visible    css=#pendingStatus
	And Element Should Be Visible    css=#noSettingStatus
	When Click Sub-Tab: "Pay Station Placement"
	Then Header Should Be Visible: "Pay Stations"
	And Page Should Contain    Mapped Pay Stations

Customer Admin - Settings: Mobile Devices
	Given Click Mobile Devices Tab 1
	And Header Should Be Visible: "Mobile Devices"
	And Element Should Be Visible    css=#deviceACExpand

Customer Admin - Settings: Routes
	Given Click Routes Tab 1
	And Header Should Be Visible: "Routes"
	And Table Info Should Be Visible: "Name"
	And Table Info Should Be Visible: "Type"
	And Click Add Button
	And Header Should Be Visible: "Add Route"

Customer Admin - Settings: Alerts
	Given Click Alerts Tab 1
	And Header Should Be Visible: "Alerts"
	And Table Info Should Be Visible: "Name"
	And Table Info Should Be Visible: "Threshold"
	When Click Add Button
	Then Header Should Be Visible: "Add Alert"
	And Cancel Adding Alert In Settings

Customer Admin - Settings: Users
	Given Click Users Tab 1
	And Click Sub-Tab: "User Account"
	And Header Should Be Visible: "Users"
	And Click Element    xpath=//span[contains(text(),'Administrator')]
	And Page Should Contain    Display Name:
	When Click Add Button
	Then Header Should Be Visible: "Add User"
	When Click Sub-Tab: "Roles"
	Then Header Should Be Visible: "Roles"
	And Click Element    xpath=//span[contains(text(),'Administrator')]
	And Header Should Be Visible: "Administrator"
	And Page Should Contain    Reports Management
	When Click Add Button
	Then Header Should Be Visible: "Add Role"


Customer Admin - Settings: Card Settings
	Given Click Card Settings Tab 1
	And Header Should Be Visible: "Card Retry Queue Settings"
	And Header Should Be Visible: "Card Types"
	And Table Info Should Be Visible: "Name"
	And Table Info Should Be Visible: "Track 2 Pattern"
	And Table Info Should Be Visible: "Authorization Method"
	When Click Add Button
	Then Page Should Contain     Add Card Type
	And Click Element    xpath=//span[text()='Cancel']

