*** Settings ***
Library           Selenium2Library
Resource		  ../../../Keywords/globalKeywordDirectory.robot
Resource		  ../../../Keywords/uiCrawlerKeywords/UICrawlerKeywords.robot


Suite Setup       Login as Generic Customer    ${G_PARENT_USERNAME}    Password$1
Suite Teardown    Close Browser


*** Test Cases ***

Customer Admin Parent - Dashboard: Finance
	Given Go To 'Dashboard' Section (Left)
	And Click Finance Tab 1
	And Should Be Visible: "Total Revenue by Day"
	And Should Be Visible: "Total Revenue Last 30 Days"
	And Should Be Visible: "Total Revenue Last 7 Days"
	And Should Be Visible: "Total Revenue by Organization"
	And Should Be Visible: "Total Revenue by Month"
	And Should Be Visible: "Top 5 - 30 Days"
	And Should Be Visible: "Top 5 Locations - 30 Days"
	And Should Be Visible: "Bottom 5 - 30 Days"
	And Should Be Visible: "Bottom 5 Locations - 30 Days"
	And Should Be Visible: "Total Revenue by Hour"

Customer Admin Parent - Dashboard: Operations 
	Given Click Operations Tab 1
	And Should Be Visible: "Total Purchases by Month"
	And Should Be Visible: "Total Purchases by Day"
	And Should Be Visible: "Total Purchases by Hour"
	And Should Be Visible: "Total Purchases by Rev Type"
	And Should Be Visible: "Total Purchases by Org"

Customer Admin Parent - Dashboard: Map
	Given Click Map Tab 1
	And Zoom On Map Should Be Visible 
	And Should Be Visible: "Map"

Customer Admin Parent - Dashboard: Card Processing
	Given Click Card Processing Tab 1
	And Should Be Visible: "Monthly Purchases by Method"
	And Should Be Visible: "Daily Purchases by Method"
	And Should Be Visible: "Purchases by Method - 24h"
	And Should Be Visible: "Monthly Revenue by Method"
	And Should Be Visible: "Daily Revenue by Method"
	And Should Be Visible: "Revenue by Method - 24h"

Customer Admin Parent - Reports
	Given Go To 'Reports' Section (Left)
	And Header Should Be Visible: "Pending and Incomplete Reports"
	And Header Should Be Visible: "Scheduled Reports"
	And Header Should Be Visible: "Completed Reports"
	And Table Info Should Be Visible: "Title"
	And Table Info Should Be Visible: "Status"
	And Table Info Should Be Visible: "Next Scheduled Time"
	And Table Info Should Be Visible: "Finish Time"
	When Click Create Report
	And Header Should Be Visible: "Report Type"
	And Header Should Be Visible: "Report Details"
	And Header Should BE Visible: "Report Schedule"
	And Cancel Report

Customer Admin Parent - Settings: Global
	Given Go To 'Settings' Section (Left)
	And Click Global Tab 1
	And Click Sub-Tab: "Settings"
	And Header Should Be Visible: "Global Preferences"
	When Click Sub-Tab: "System Notifications"
	Then Header Should Be Visible: "System Notifications"
	When Click Sub-Tab: "Recent Activity"
	Then Header Should Be Visible: "Activity Log"
	And Table Info Should Be Visible: "User"
	And Table Info Should Be Visible: "Activity"
	And Table Info Should Be Visible: "Date/Time"

Customer Admin Parent - Settings: Users
	Given Click Users Tab 1
	And Click Sub-Tab: "User Account"
	And Header Should Be Visible: "Users"
	When Click Add Button
	Then Header Should Be Visible: "Add User"
	When Click Sub-Tab: "Roles"
	Then Header Should Be Visible: "Roles"
	When Click Add Button
	Then Header Should Be Visible: "Add Role"

