*** Settings ***
# Summary: Suite Setup
# Post Conditions:
#    ${G_CUST_ADMIN_USERNAME}
#    ${G_CUSTOMER_NAME}
#    ${G_PAYSTATION_X} where x is 0 to ${num_of_new_paystations}
#
# Author: Billy Hoang
# Time: Dec 3, 2015

Resource          ../../../../iris/ems-robot/Keywords/globalKeywordDirectory.robot
Resource          ../../../../iris/ems-robot/data/data.robot
# Resource          ../../../data/data.robot
# Resource          ../../../Keywords/globalKeywordDirectory.robot
Library           DatabaseLibrary
Library           Collections
Suite Setup       Run Keywords  Set Up Suite Testing Environment

Suite Teardown    Run Keywords
...               Close Browser
...               AND    Log To Console    Suite Tear Down Is Completed

*** Variables ***
${new_customer_name}
${num_of_new_paystations}  5


*** Keywords ***

# - Connects To Database
# - Calls a Stored Procedure to create a new Customer and new Pay Stations
# - Globally declares: Customer Admin Username and Pay Stations
Set Up Suite Testing Environment
    Connect To Database    ${DB_API_MODULE_NAME}     ${DB_NAME}    ${DB_USERNAME}    ${DB_PASSWORD}    ${DB_HOST}    ${DB_PORT}

    # Construct a new customer name
    ${random}=  Get Time  epoch
    ${new_parent_name}=  Catenate  SEPARATOR=  parent  ${random}
    Set Global Variable  ${G_PARENT_NAME}  ${new_parent_name}
    Log To Console  New Parent Name is: ${G_PARENT_NAME}

    ${new_child_name}=  Catenate  SEPARATOR=  child  ${random}
    Set Global Variable  ${G_CHILD_NAME}  ${new_child_name}
    Log To Console  New Child Name is: ${G_CHILD_NAME}

    Set New Customer Admin Username Globally  ${new_parent_name}  G_PARENT_USERNAME
    # Set Pay Stations Globally  ${new_parent_name}

    Set New Customer Admin Username Globally  ${new_child_name}  G_CHILD_USERNAME
    # Set Pay Stations Globally  ${new_child_name}

    Create Parent And Child Customer  ${new_parent_name}  ${new_child_name}

    # Logs into the new customer for the first time and changes it's password to Password$1
    Log To Console    Performing initial login for ${G_PARENT_USERNAME}
    Login As Generic Customer  ${G_PARENT_USERNAME}  Password$2
    Close Browser
    Log To Console    Initial login for ${G_PARENT_USERNAME} completed

    Log To Console    Performing initial login for ${G_CHILD_USERNAME}
    Login As Generic Customer  ${G_CHILD_USERNAME}  password
    Close Browser
    Log To Console    Initial login for ${G_CHILD_USERNAME} completed
	

Create Parent And Child Customer
    [arguments]     ${new_parent_name}    ${new_child_name}
     @{ALL_SERVICES}    Create List    3rd Party Pay-By-Cell Integration    Alerts    AutoCount Integration    Coupons    Smart Cards    Batch Credit Card Processing    Digital API: Read    Digital API: Write    Digital API: XChange    Extend-By-Phone    FLEX Integration    Mobile: Digital Collect    Online Pay Station Configuration    Passcards    Real-Time Campus Card Processing    Real-Time Credit Card Processing    Standard Reports
    # Query    CALL sp_Preview_CreateAccount('DBAutoCustTest','DBAutoTitleTest','DBAutoOrgTest','${new_parent_name}',NULL);
    Login As System Admin     systemadmin    Password$1    password
    Open New Customer Form
    Select Parent Flag
    Fill In Required Customer Fields     ${new_parent_name}    ${G_PARENT_USERNAME}    Password$2    Password$2    Enabled    N/A    Canada/Pacific
    Select Services: "${ALL_SERVICES}"
    Saved Customer Creation
    Close Browser
    # Search Up Customer: "${new_parent_name}"
    # Click Edit Customer Button
    # Select Parent Flag
    # Click Update Customer On Update Customer Form
    ${parent_customer_id[0][0][0]} =    Query    Select Id FROM Customer WHERE Name = '${new_parent_name}' and IsParent=1;
    Query    CALL sp_Preview_CreateAccount('DBAutoCustTest','DBAutoTitleTest','DBAutoOrgTest','${new_child_name}','${parent_customer_id[0][0][0]}');

Run Store Procedure To Inject Customer And Pay Stations
    [arguments]  ${new_customer_name}
    Query    CALL sp_Preview_CreateAccount('DBAutoCustTest','DBAutoTitleTest','DBAutoOrgTest','${new_customer_name}',NULL);


Set New Customer Admin Username Globally
    [arguments]  ${customer_name}  ${g_variable_name}
    ${cust_admin_username}=  Catenate  SEPARATOR=  admin@  ${customer_name}
    Set Global Variable  ${${g_variable_name}}  ${cust_admin_username}
    Log To Console  Customer Admin Username is: ${cust_admin_username}

Set Pay Stations Globally
    [arguments]  ${customer_name}
     ${list_of_paystations}=  Get New Pay Station Serial Numbers  ${customer_name}
    :FOR  ${index}  IN RANGE  0  ${num_of_new_paystations}
    \    Set Global Variable  ${G_PAYSTATION_${index}}  ${list_of_paystations[${index}][0]}
    \    Log To Console  Paystation_${index} is: ${G_PAYSTATION_${index}}


Get New Pay Station Serial Numbers
    [arguments]  ${customer_name}
    Connect To Database    ${dbapiModuleName}     ${dbName}    ${dbUsername}    ${dbPassword}    ${dbHost}    ${dbPort}
    ${customer_id}=  Query  SELECT Id FROM Customer WHERE Name = '${customer_name}'
    ${customer_id}=  Set Variable  ${customer_id[0][0]}
    ${customer_id}=  Convert To String  ${customer_id}
    Log To Console  Customer ID is: ${customer_id}
    Wait Until Keyword Succeeds  10s  1s  Row Count Is Equal To X  SELECT SerialNumber FROM PointOfSale WHERE CustomerId = '${customer_id}'  ${num_of_new_paystations}  
    ${result}=  Query  SELECT SerialNumber FROM PointOfSale WHERE CustomerId = '${customer_id}'
    [return]  ${result}

