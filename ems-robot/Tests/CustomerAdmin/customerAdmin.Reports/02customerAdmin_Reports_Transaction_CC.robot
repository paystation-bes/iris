*** Settings ***
# Summary: Test Suite for verifying the creation of a Transaction - Credit Card Report
# Author: Billy Hoang
# Time: Dec 3, 2015

Resource          ../../../data/data.robot
Resource          ../../../Keywords/CustomerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Suite Teardown    Close Browser
Force Tags        smoke-test

Test Setup    Run Keywords
...           Create Valid Paymentech Merchant Account For: "${G_CUSTOMER_NAME}"
...  AND      Login as Generic Customer  ${G_CUST_ADMIN_USERNAME}  Password$1
...  AND      Go To Reports Section
...  AND      Click Create Report

Test Teardown    Close Browser


*** Test Cases ***

# NOTE: The numbering of the test cases follow the numbering in EMS-10946

Transaction - Credit Card (Summary) 11
    I Create A Transaction - Credit Card Report

Transaction - Credit Card (Summary) 12
    [Documentation]  Runs a Transaction - All Summary Report and Verifies it's creation
    ${report_type}=  Set Variable    Transaction - Credit Card
    ${detail_type}=  Set Variable    Details
    ${transaction_date/time}=    Set Variable    Today
    # ${beginning_date}
    # ${beginning_time}
    # ${end_date}
    # ${end_time}
    ${location_type}=  Set Variable    Pay Station Route
    ${archived_pay_stations?}=  Set Variable    No
    ${space_number}=  Set Variable    =
    ${specific_space_number}=  Set Variable    1
    ${license_plate}=  Set Variable    ${EMPTY}
    ${ticket_number}=  Set Variable    All
    ${coupon_code}=  Set Variable    N/A

    ${card_type}=  Set Variable    AMEX
    ${approval_status}=  Set Variable    Pre-Auth
    ${merchant_account}=  Set Variable    ${G_PAYMENTECH_ACC_NAME}
    ${card_number}=  Set Variable    All

    ${transaction_type}=  Set Variable    All
    ${group_by}=  Set Variable    Location
    ${output_format}=  Set Variable    PDF
    ${schedule}=  Set Variable    Submit Now

    Given Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Report Detail Type: "${detail_type}"
    And Select Transaction Date/Time: "${transaction_date/time}"
    And Select Location Type: "${location_type}"
    And Select Location Type All: "${location_type}"
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    And Input First Space Number: "${specific_space_number}"
    And Input License Plate: "${license_plate}"
    And Select Ticket Number: "${ticket_number}"
    And Select Coupon Code: "${coupon_code}"
    And Select Card Type: "${card_type}"
    And Select Approval Status: "${approval_status}"
    And Select Merchant Account: "${merchant_account}"
    And Select Card Number: "${card_number}"
    And Select Transaction Type: "${transaction_type}"
    And Select Group By: "${group_by}"
    And Select Output Format: "${output_format}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    And Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed

Transaction - Credit Card (Summary) 13
    [Documentation]  Runs a Transaction - All Summary Report and Verifies it's creation
    ${report_type}=  Set Variable    Transaction - Credit Card
    ${detail_type}=  Set Variable    Details
    ${transaction_date/time}=    Set Variable    Last 24 Hours
    # ${beginning_date}
    # ${beginning_time}
    # ${end_date}
    # ${end_time}
    ${location_type}=  Set Variable    Pay Station
    ${specific_refinement}=  Set Variable    Airport Collections
    ${archived_pay_stations?}=  Set Variable    Yes
    ${space_number}=  Set Variable    <=
    ${specific_space_number}=  Set Variable    1
    ${license_plate}=  Set Variable    ${EMPTY}
    ${ticket_number}=  Set Variable    All
    ${coupon_code}=  Set Variable    N/A

    ${card_type}=  Set Variable    Diners Club
    ${approval_status}=  Set Variable    Real-Time
    ${merchant_account}=  Set Variable    All
    ${card_number}=  Set Variable    Specific #
    ${specific_card_number}=  Set Variable    5675

    ${transaction_type}=  Set Variable    All
    ${group_by}=  Set Variable    Payment Type
    ${output_format}=  Set Variable    CSV
    ${schedule}=  Set Variable    Submit Now

    Given Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Report Detail Type: "${detail_type}"
    And Select Transaction Date/Time: "${transaction_date/time}"
    And Select Location Type: "${location_type}"
    And Select Specific Pay Station Option: "${specific_refinement}"
    And Select All Refined Pay Stations
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    And Input First Space Number: "${specific_space_number}"
    And Input License Plate: "${license_plate}"
    And Select Ticket Number: "${ticket_number}"
    And Select Coupon Code: "${coupon_code}"

    And Select Card Type: "${card_type}"
    And Select Approval Status: "${approval_status}"
    And Select Merchant Account: "${merchant_account}"
    And Select Card Number: "${card_number}"
    And Input Card Number: "${specific_card_number}"

    And Select Transaction Type: "${transaction_type}"
    And Select Group By: "${group_by}"
    And Select Output Format: "${output_format}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    And Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed


Transaction - Credit Card (Summary) 14
    [Documentation]  Runs a Transaction - All Summary Report and Verifies it's creation
    [Template]  I Create A Transaction - Credit Card Report
         transaction_date/time=Last Week
    ...  license_plate=123ABC
    ...  card_type=Discover
    ...  approval_status=Batched

Transaction - Credit Card (Summary) 15
    [Documentation]  Runs a Transaction - All Summary Report and Verifies it's creation
    ${report_type}=  Set Variable    Transaction - Credit Card
    ${detail_type}=  Set Variable    Details
    ${transaction_date/time}=    Set Variable    Last Month
    ${location_type}=  Set Variable    Pay Station Route
    ${archived_pay_stations?}=  Set Variable    Yes
    ${space_number}=  Set Variable    N/A
    ${license_plate}=  Set Variable    ${EMPTY}
    ${ticket_number}=  Set Variable    Between
    ${first_ticket_number}=  Set Variable    2
    ${second_ticket_number}=  Set Variable    25
    ${coupon_code}=  Set Variable    N/A


    ${card_type}=  Set Variable    MasterCard
    ${approval_status}=  Set Variable    Offline
    ${merchant_account}=  Set Variable    All
    ${card_number}=  Set Variable    All

    ${transaction_type}=  Set Variable    All
    ${group_by}=  Set Variable    Month
    ${output_format}=  Set Variable    CSV
    ${schedule}=  Set Variable    Submit Now

    Given Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Report Detail Type: "${detail_type}"
    And Select Transaction Date/Time: "${transaction_date/time}"
    And Select Location Type: "${location_type}"
    And Select Location Type All: "${location_type}"
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    And Input License Plate: "${license_plate}"
    And Select Ticket Number: "${ticket_number}"
    And Input First Ticket Number: "${first_ticket_number}"
    And Input Second Ticket Number: "${second_ticket_number}" 
    And Select Coupon Code: "${coupon_code}"

    And Select Card Type: "${card_type}"
    And Select Approval Status: "${approval_status}"
    And Select Merchant Account: "${merchant_account}"
    And Select Card Number: "${card_number}"

    And Select Transaction Type: "${transaction_type}"
    And Select Group By: "${group_by}"
    And Select Output Format: "${output_format}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    And Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed


Transaction - Credit Card (Details) 16
    [Documentation]  Runs a Transaction - All Summary Report and Verifies it's creation
    ${report_type}=  Set Variable    Transaction - Credit Card
    ${detail_type}=  Set Variable    Details
    ${transaction_date/time}=    Set Variable    Last 30 Days
    ${location_type}=  Set Variable    Pay Station
    ${archived_pay_stations?}=  Set Variable    No
    ${space_number}=  Set Variable    N/A
    ${license_plate}=  Set Variable    ${EMPTY}
    ${ticket_number}=  Set Variable    All
    ${coupon_code}=  Set Variable    No Coupons

    ${card_type}=  Set Variable    VISA
    ${approval_status}=  Set Variable    Recoverable
    ${merchant_account}=  Set Variable    All
    ${card_number}=  Set Variable    All

    ${transaction_type}=  Set Variable    All
    ${group_by}=  Set Variable    Pay Station
    ${output_format}=  Set Variable    PDF
    ${schedule}=  Set Variable    Submit Now

    Given Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Report Detail Type: "${detail_type}"
    And Select Transaction Date/Time: "${transaction_date/time}"
    And Select Location Type: "${location_type}"
    And Select Pay Station: "${G_PAYSTATION_0}"
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    And Input License Plate: "${license_plate}"
    And Select Ticket Number: "${ticket_number}"
    And Select Coupon Code: "${coupon_code}"

    And Select Card Type: "${card_type}"
    And Select Approval Status: "${approval_status}"
    And Select Merchant Account: "${merchant_account}"
    And Select Card Number: "${card_number}"

    And Select Transaction Type: "${transaction_type}"
    And Select Group By: "${group_by}"
    And Select Output Format: "${output_format}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    And Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed



Transaction - Credit Card (Details) 17
    [Documentation]  Runs a Transaction - All Summary Report and Verifies it's creation
    ${report_type}=  Set Variable    Transaction - Credit Card
    ${detail_type}=  Set Variable    Summary
    ${transaction_date/time}=    Set Variable    This Year
    ${location_type}=  Set Variable    Location
    ${archived_pay_stations?}=  Set Variable    Yes
    ${space_number}=  Set Variable    N/A
    ${license_plate}=  Set Variable    ${EMPTY}
    ${ticket_number}=  Set Variable    All
    ${coupon_code}=  Set Variable    N/A

    ${card_type}=  Set Variable    All
    ${approval_status}=  Set Variable    Unclosable
    ${merchant_account}=  Set Variable    All
    ${card_number}=  Set Variable    All

    ${transaction_type}=  Set Variable    Regular
    ${group_by}=  Set Variable    Payment Type
    ${output_format}=  Set Variable    PDF
    ${schedule}=  Set Variable    Submit Now

    Given Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Report Detail Type: "${detail_type}"
    And Select Transaction Date/Time: "${transaction_date/time}"
    And Select Location Type: "${location_type}"
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    And Input License Plate: "${license_plate}"
    And Select Ticket Number: "${ticket_number}"
    And Select Coupon Code: "${coupon_code}"

    And Select Card Type: "${card_type}"
    And Select Approval Status: "${approval_status}"
    And Select Merchant Account: "${merchant_account}"
    And Select Card Number: "${card_number}"

    And Select Transaction Type: "${transaction_type}"
    And Select Group By: "${group_by}"
    And Select Output Format: "${output_format}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    And Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed


Transaction - Credit Card (Details) 18
    [Documentation]  Runs a Transaction - All Summary Report and Verifies it's creation
    ${report_type}=  Set Variable    Transaction - Credit Card
    ${detail_type}=  Set Variable    Details
    ${transaction_date/time}=    Set Variable    All
    ${location_type}=  Set Variable    Pay Station Route
    ${archived_pay_stations?}=  Set Variable    No
    ${space_number}=  Set Variable    N/A
    ${license_plate}=  Set Variable    ${EMPTY}
    ${ticket_number}=  Set Variable    All
    ${coupon_code}=  Set Variable    N/A

    ${card_type}=  Set Variable    All
    ${approval_status}=  Set Variable    Encryption Error
    ${merchant_account}=  Set Variable    All
    ${card_number}=  Set Variable    All

    ${transaction_type}=  Set Variable    Monthly
    ${group_by}=  Set Variable    Day
    ${output_format}=  Set Variable    PDF
    ${schedule}=  Set Variable    Submit Now

    Given Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Report Detail Type: "${detail_type}"
    And Select Transaction Date/Time: "${transaction_date/time}"
    And Select Location Type: "${location_type}"
    And Select Location Type All: "${location_type}"
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    And Input License Plate: "${license_plate}"
    And Select Ticket Number: "${ticket_number}"
    And Select Coupon Code: "${coupon_code}"

    And Select Card Type: "${card_type}"
    And Select Approval Status: "${approval_status}"
    And Select Merchant Account: "${merchant_account}"
    And Select Card Number: "${card_number}"

    And Select Transaction Type: "${transaction_type}"
    And Select Group By: "${group_by}"
    And Select Output Format: "${output_format}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    And Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed


Transaction - Credit Card (Details) 19
    [Documentation]  Runs a Transaction - All Summary Report and Verifies it's creation
    ${report_type}=  Set Variable    Transaction - Credit Card
    ${detail_type}=  Set Variable    Details
    ${transaction_date/time}=    Set Variable    Month
    ${start_month}=  Set Variable    February
    ${start_year}=  Set Variable    2015
    ${end_month}=  Set Variable    April
    ${end_year}=  Set Variable    2015
    ${location_type}=  Set Variable    Pay Station
    ${archived_pay_stations?}=  Set Variable    Yes
    ${space_number}=  Set Variable    N/A
    ${license_plate}=  Set Variable    ${EMPTY}
    ${ticket_number}=  Set Variable    All
    ${coupon_code}=  Set Variable    N/A

    ${card_type}=  Set Variable    All
    ${approval_status}=  Set Variable    To be Settled
    ${merchant_account}=  Set Variable    All
    ${card_number}=  Set Variable    All

    ${transaction_type}=  Set Variable    
    ${group_by}=  Set Variable    Location
    ${output_format}=  Set Variable    CSV
    ${schedule}=  Set Variable    Submit Now

    Given Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Report Detail Type: "${detail_type}"
    And Select Transaction Date/Time: "${transaction_date/time}"
    And Input Start Month: "${start_month}"
    And Input Start Year: "${start_year}"
    And Input End Month: "${end_month}"
    And Input End Year: "${end_year}"
    And Select Location Type: "${location_type}"
    And Select Location Type All: "${location_type}"
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    #And Select Space Number: "${space_number}"
    And Input License Plate: "${license_plate}"
    And Select Ticket Number: "${ticket_number}"
    And Select Coupon Code: "${coupon_code}"

    And Select Card Type: "${card_type}"
    And Select Approval Status: "${approval_status}"
    And Select Merchant Account: "${merchant_account}"
    And Select Card Number: "${card_number}"

    And Select Transaction Type: "${transaction_type}"
    And Select Group By: "${group_by}"
    And Select Output Format: "${output_format}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    And Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed

Transaction - Credit Card (Details) 20
    [Documentation]  Runs a Transaction - All Summary Report and Verifies it's creation
    ${report_type}=  Set Variable    Transaction - Credit Card
    ${detail_type}=  Set Variable    Details
    ${transaction_date/time}=    Set Variable    Month
    ${start_month}=  Set Variable    February
    ${start_year}=  Set Variable    2015
    ${end_month}=  Set Variable    April
    ${end_year}=  Set Variable    2015
    ${location_type}=  Set Variable    Pay Station
    ${archived_pay_stations?}=  Set Variable    Yes
    ${space_number}=  Set Variable    N/A
    ${license_plate}=  Set Variable    ${EMPTY}
    ${ticket_number}=  Set Variable    All
    ${coupon_code}=  Set Variable    N/A

    ${card_type}=  Set Variable    All
    ${approval_status}=  Set Variable    To be Settled
    ${merchant_account}=  Set Variable    All
    ${card_number}=  Set Variable    All

    ${transaction_type}=  Set Variable    
    ${group_by}=  Set Variable    Location
    ${output_format}=  Set Variable    CSV
    ${schedule}=  Set Variable    Submit Now

    Given Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Report Detail Type: "${detail_type}"
    And Select Transaction Date/Time: "${transaction_date/time}"
    And Input Start Month: "${start_month}"
    And Input Start Year: "${start_year}"
    And Input End Month: "${end_month}"
    And Input End Year: "${end_year}"
    And Select Location Type: "${location_type}"
    And Select Location Type All: "${location_type}"
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    #And Select Space Number: "${space_number}"
    And Input License Plate: "${license_plate}"
    And Select Ticket Number: "${ticket_number}"
    And Select Coupon Code: "${coupon_code}"

    And Select Card Type: "${card_type}"
    And Select Approval Status: "${approval_status}"
    And Select Merchant Account: "${merchant_account}"
    And Select Card Number: "${card_number}"

    And Select Transaction Type: "${transaction_type}"
    And Select Group By: "${group_by}"
    And Select Output Format: "${output_format}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    And Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed









