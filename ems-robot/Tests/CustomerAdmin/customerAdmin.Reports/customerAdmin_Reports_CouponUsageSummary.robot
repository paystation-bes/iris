*** Settings ***
# Summary: Test Suite for verifying the creation and deletion of a Report
# Author: Billy Hoang
# Time: Dec 3, 2015

Resource          ../../../data/data.robot
Resource          ../../../Keywords/CustomerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Suite Teardown    Close Browser
Force Tags        smoke-test

Test Setup    Run Keywords
...           Login as Generic Customer  ${G_CUST_ADMIN_USERNAME}  Password$1
...  AND      Go To Reports Section
...  AND      Click Create Report

Test Teardown    Close Browser


*** Test Cases ***

Coupon Usage Summary - Default 1
	I Create A Coupon Usage Summary Report

Coupon Usage Summary - Pay Station Route All Selected - 2
	${report_type}=  Set Variable    Coupon Usage Summary
	${detail_type}=  Set Variable    Details
	${transaction_date/time}=  Set Variable    Yesterday
	${location_type}=  Set Variable    Pay Station Route
	${locations_payStationRoutes_or_payStaions}=  Set Variable    Routes
	${archived_pay_stations?}=  Set Variable    No
	${coupon_code}=  Set Variable    All Coupons
	${coupon_type}=  Set Variable    All
	${search_account}=  Set Variable
	${output_format}=  Set Variable    PDF
	${schedule}=  Set Variable    Submit Now

	Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
	And Proceed To Step 2
	And Select Report Detail Type: "${detail_type}"
	And Select Transaction Date/Time: "${transaction_date/time}"
	And Select Location Type: "${location_type}"
	And Select Location Type All: "${location_type}"
	And Include Archived Pay Stations: "${archived_pay_stations?}"
	And Select Coupon Code: "${coupon_code}"
	And Select Coupon Type: "${coupon_type}"
	And Select Output Format: "${output_format}"
	And Proceed to Step 3
	And Select Schedule: "${schedule}"
	When Click Save Report
	Then Report "${report_type}" Is Pending
	And Report "${report_type}" Is Completed

Coupon Usage Summary - Pay Station Route Some Selected - 2
	${report_type}=  Set Variable    Coupon Usage Summary
	${detail_type}=  Set Variable    Details
	${transaction_date/time}=  Set Variable    Last 30 Days
	${location_type}=  Set Variable    Pay Station Route
	${payStationRoute}=  Set Variable    Airport Maintenance
	${payStationRoute2}=  Set Variable    Downtown Maintenance
	${payStationRoute3}=  Set Variable    Airport Collections
	${archived_pay_stations?}=  Set Variable    No
	${coupon_code}=  Set Variable    All Coupons
	${coupon_type}=  Set Variable    All
	${search_account}=  Set Variable
	${output_format}=  Set Variable    PDF
	${schedule}=  Set Variable    Submit Now

	Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
	And Proceed To Step 2
	And Select Report Detail Type: "${detail_type}"
	And Select Transaction Date/Time: "${transaction_date/time}"
	And Select Location Type: "${location_type}"
	And Select Pay Station Route: "${payStationRoute}"
	And Select Pay Station Route: "${payStationRoute2}"
	And Select Pay Station Route: "${payStationRoute3}"
	And Include Archived Pay Stations: "${archived_pay_stations?}"
	And Select Coupon Code: "${coupon_code}"
	And Select Coupon Type: "${coupon_type}"
	And Select Output Format: "${output_format}"
	And Proceed to Step 3
	And Select Schedule: "${schedule}"
	When Click Save Report
	Then Report "${report_type}" Is Pending
	And Report "${report_type}" Is Completed

Coupon Usage Summary - Pay Station Route None Selected - 2
	${report_type}=  Set Variable    Coupon Usage Summary
	${detail_type}=  Set Variable    Summary
	${transaction_date/time}=  Set Variable    Last 7 Days
	${location_type}=  Set Variable    Pay Station Route
	${archived_pay_stations?}=  Set Variable    No
	${coupon_code}=  Set Variable    All Coupons
	${coupon_type}=  Set Variable    All
	${search_account}=  Set Variable
	${output_format}=  Set Variable    PDF
	${schedule}=  Set Variable    Submit Now

	Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
	And Proceed To Step 2
	And Select Report Detail Type: "${detail_type}"
	And Select Transaction Date/Time: "${transaction_date/time}"
	And Select Location Type: "${location_type}"
	And Include Archived Pay Stations: "${archived_pay_stations?}"
	And Select Coupon Code: "${coupon_code}"
	And Select Coupon Type: "${coupon_type}"
	And Select Output Format: "${output_format}"
	And Proceed to Step 3
	And Select Schedule: "${schedule}"
	When Click Save Report
	Then Attention, Route Is Required
	And Close Pop-up Window
	And Cancel Report

Coupon Usage Summary - Pay Station All Selected - 3
	${report_type}=  Set Variable    Coupon Usage Summary
	${detail_type}=  Set Variable    Summary
	${transaction_date/time}=  Set Variable    Yesterday
	${location_type}=  Set Variable    Pay Station
	${archived_pay_stations?}=  Set Variable    No
	${coupon_code}=  Set Variable    All Coupons
	${coupon_type}=  Set Variable    Percentage
	${search_account}=  Set Variable
	${output_format}=  Set Variable    PDF
	${schedule}=  Set Variable    Submit Now

	Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
	And Proceed To Step 2
	And Select Report Detail Type: "${detail_type}"
	And Select Transaction Date/Time: "${transaction_date/time}"
	And Select Location Type: "${location_type}"
	And Select Location Type All: "${location_type}"
	And Include Archived Pay Stations: "${archived_pay_stations?}"
	And Select Coupon Code: "${coupon_code}"
	And Select Coupon Type: "${coupon_type}"
	And Select Output Format: "${output_format}"
	And Proceed to Step 3
	And Select Schedule: "${schedule}"
	When Click Save Report
	Then Report "${report_type}" Is Pending
	And Report "${report_type}" Is Completed

Coupon Usage Summary - Pay Station Some Selected - 3
	${report_type}=  Set Variable    Coupon Usage Summary
	${detail_type}=  Set Variable    Summary
	${transaction_date/time}=  Set Variable    Last 24 Hours
	${location_type}=  Set Variable    Pay Station
	${payStation}=  Set Variable    ${G_PAYSTATION_1}
	${payStation2}=  Set Variable    ${G_PAYSTATION_3}
	${archived_pay_stations?}=  Set Variable    No
	${coupon_code}=  Set Variable    All Coupons
	${coupon_type}=  Set Variable    Dollar Based
	${search_account}=  Set Variable
	${output_format}=  Set Variable    PDF
	${schedule}=  Set Variable    Submit Now

	Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
	And Proceed To Step 2
	And Select Report Detail Type: "${detail_type}"
	And Select Transaction Date/Time: "${transaction_date/time}"
	And Select Location Type: "${location_type}"
	And Select Pay Station: "${payStation}"
	And Select Pay Station: "${payStation2}"
	And Include Archived Pay Stations: "${archived_pay_stations?}"
	And Select Coupon Code: "${coupon_code}"
	And Select Coupon Type: "${coupon_type}"
	And Select Output Format: "${output_format}"
	And Proceed to Step 3
	And Select Schedule: "${schedule}"
	When Click Save Report
	Then Report "${report_type}" Is Pending
	And Report "${report_type}" Is Completed

Coupon Usage Summary - Pay Station None Selected - 3
	${report_type}=  Set Variable    Coupon Usage Summary
	${detail_type}=  Set Variable    Summary
	${transaction_date/time}=  Set Variable    Yesterday
	${location_type}=  Set Variable    Pay Station
	${archived_pay_stations?}=  Set Variable    No
	${coupon_code}=  Set Variable    All Coupons
	${coupon_type}=  Set Variable    Percentage
	${search_account}=  Set Variable
	${output_format}=  Set Variable    PDF
	${schedule}=  Set Variable    Submit Now

	Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
	And Proceed To Step 2
	And Select Report Detail Type: "${detail_type}"
	And Select Transaction Date/Time: "${transaction_date/time}"
	And Select Location Type: "${location_type}"
	And Include Archived Pay Stations: "${archived_pay_stations?}"
	And Select Coupon Code: "${coupon_code}"
	And Select Coupon Type: "${coupon_type}"
	And Select Output Format: "${output_format}"
	And Proceed to Step 3
	And Select Schedule: "${schedule}"
	When Click Save Report
	Then Attention, Pay Station Is Required
	And Close Pop-up Window
	And Cancel Report

Coupon Usage Summary - Valid Coupon - 4
	${report_type}=  Set Variable    Coupon Usage Summary
	${detail_type}=  Set Variable    Summary
	${transaction_date/time}=  Set Variable    Last Week
	${location_type}=  Set Variable    Location
	${archived_pay_stations?}=  Set Variable    No
	${coupon_code}=  Set Variable    Specific Code
	${coupon_code_id}=  Set Variable     1254e23*DE
	${coupon_type}=  Set Variable    Dollar Based
	${search_account}=  Set Variable
	${output_format}=  Set Variable    PDF
	${schedule}=  Set Variable    Submit Now

	Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
	And Proceed To Step 2
	And Select Report Detail Type: "${detail_type}"
	And Select Transaction Date/Time: "${transaction_date/time}"
	And Select Location Type: "${location_type}"
	And Include Archived Pay Stations: "${archived_pay_stations?}"
	And Select Coupon Code: "${coupon_code}"
	And Input Coupon Code: "${coupon_code_id}"
	And Select Coupon Type: "${coupon_type}"
	And Select Output Format: "${output_format}"
	And Proceed to Step 3
	And Select Schedule: "${schedule}"
	When Click Save Report
	Then Report "${report_type}" Is Pending
	And Report "${report_type}" Is Completed

Coupon Usage Summary - Invalid Coupon (invalid character)- 4
	${report_type}=  Set Variable    Coupon Usage Summary
	${detail_type}=  Set Variable    Summary
	${transaction_date/time}=  Set Variable    Last Month
	${location_type}=  Set Variable    Location
	${archived_pay_stations?}=  Set Variable    No
	${coupon_code}=  Set Variable    Specific Code
	${coupon_code_id}=  Set Variable     12$4e2
	${coupon_type}=  Set Variable    Dollar Based
	${search_account}=  Set Variable
	${output_format}=  Set Variable    PDF
	${schedule}=  Set Variable    Submit Now

	Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
	And Proceed To Step 2
	And Select Report Detail Type: "${detail_type}"
	And Select Transaction Date/Time: "${transaction_date/time}"
	And Select Location Type: "${location_type}"
	And Include Archived Pay Stations: "${archived_pay_stations?}"
	And Select Coupon Code: "${coupon_code}"
	When Input Coupon Code: "${coupon_code_id}"
	Then Attention, Only Accepts Letters, Digits, and *
	And Close Pop-up Window
	And Cancel Report

Coupon Usage Summary - No Coupon Code ID (blank space)- 4
	${report_type}=  Set Variable    Coupon Usage Summary
	${detail_type}=  Set Variable    Summary
	${transaction_date/time}=  Set Variable    Last Month
	${location_type}=  Set Variable    Location
	${archived_pay_stations?}=  Set Variable    No
	${coupon_code}=  Set Variable    Specific Code
	${coupon_code_id}=  Set Variable
	${coupon_type}=  Set Variable    Dollar Based
	${search_account}=  Set Variable
	${output_format}=  Set Variable    PDF
	${schedule}=  Set Variable    Submit Now

	Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
	And Proceed To Step 2
	And Select Report Detail Type: "${detail_type}"
	And Select Transaction Date/Time: "${transaction_date/time}"
	And Select Location Type: "${location_type}"
	And Include Archived Pay Stations: "${archived_pay_stations?}"
	And Select Coupon Code: "${coupon_code}"
	And Input Coupon Code: "${coupon_code_id}"
	And Select Coupon Type: "${coupon_type}"
	And Select Output Format: "${output_format}"
	And Proceed To Step 3
	And Select Schedule: "${schedule}"
	When Click Save Report
	Then Attention, Coupon Code Is Required
	And Close Pop-up Window
	And Cancel Report

Coupon Usage Summary - Invalid Coupon (blank spaces)- 4
	${report_type}=  Set Variable    Coupon Usage Summary
	${detail_type}=  Set Variable    Summary
	${transaction_date/time}=  Set Variable    Last Month
	${location_type}=  Set Variable    Location
	${archived_pay_stations?}=  Set Variable    No
	${coupon_code}=  Set Variable    Specific Code
	${coupon_code_id}=  Set Variable     "BLANK  4"
	${coupon_type}=  Set Variable    Dollar Based
	${search_account}=  Set Variable
	${output_format}=  Set Variable    PDF
	${schedule}=  Set Variable    Submit Now

	Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
	And Proceed To Step 2
	And Select Report Detail Type: "${detail_type}"
	And Select Transaction Date/Time: "${transaction_date/time}"
	And Select Location Type: "${location_type}"
	And Include Archived Pay Stations: "${archived_pay_stations?}"
	And Select Coupon Code: "${coupon_code}"
	When Input Coupon Code: "${coupon_code_id}"
	Then Attention, Only Accepts Letters, Digits, and *
	And Close Pop-up Window
	And Cancel Report

Coupon Usage Summary - More than 10 characters Coupon - 4
	${report_type}=  Set Variable    Coupon Usage Summary
	${detail_type}=  Set Variable    Summary
	${transaction_date/time}=  Set Variable    Last Month
	${location_type}=  Set Variable    Location
	${archived_pay_stations?}=  Set Variable    No
	${coupon_code}=  Set Variable    Specific Code
	${coupon_code_id}=  Set Variable     123456789912
	${coupon_type}=  Set Variable    Dollar Based
	${search_account}=  Set Variable
	${output_format}=  Set Variable    PDF
	${schedule}=  Set Variable    Submit Now

	Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
	And Proceed To Step 2
	And Select Report Detail Type: "${detail_type}"
	And Select Transaction Date/Time: "${transaction_date/time}"
	And Select Location Type: "${location_type}"
	And Include Archived Pay Stations: "${archived_pay_stations?}"
	And Select Coupon Code: "${coupon_code}"
	And Input Coupon Code: "${coupon_code_id}"
    And Textfield Should Contain    css=#formCouponNumber    1234567899
    And Select Coupon Type: "${coupon_type}"
    And Select Output Format: "${output_format}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    When Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed


*** Keywords ***
