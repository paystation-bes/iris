*** Settings ***
# Summary: Test Suite for verifying the creation and deletion of a Report
# Author: Billy Hoang
# Time: Dec 3, 2015

Resource          ../../../data/data.robot
Resource          ../../../Keywords/CustomerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Suite Teardown    Close Browser
Force Tags        smoke-test

Test Setup    Run Keywords
...           Login as Generic Customer  ${G_CUST_ADMIN_USERNAME}  Password$1
...  AND      Go To Reports Section
...  AND      Click Create Report

Test Teardown    Close Browser


*** Test Cases ***

Pay Station Summary - Default 1
	I Create A Pay Station Summary Report

Pay Station Summary - No Locations Selected (Invalid) 2
	# By default when location is the location type, all locations will be selected
	${report_type}=  Set Variable    Pay Station Summary
	${location_type}=  Set Variable    Location
	${archived_pay_stations?}=  Set Variable    Yes
	${order_by}=  Set Variable    Pay Station Name
	${output_format}=  Set Variable    PDF
	${schedule}=  Set Variable    Submit Now

	Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
	And Proceed To Step 2
	And Select Location Type: "${location_type}"
	And Select/Deselect All Locations
	And Include Archived Pay Stations: "${archived_pay_stations?}"
	And Select Order By: "${order_by}"
	And Select Output Format: "${output_format}"
	And Proceed to Step 3
	And Select Schedule: "${schedule}"
	When Click Save Report
	Then Attention, Location/Pay Station Is Required
	And Close Pop-up Window
	And Cancel Report

Pay Station Summary - Pay Station Route All Selected 3
	${report_type}=  Set Variable    Pay Station Summary
	${location_type}=  Set Variable    Pay Station Route
	${archived_pay_stations?}=  Set Variable    No
	${order_by}=  Set Variable    Unsettled Credit Card Count
	${output_format}=  Set Variable    PDF
	${schedule}=  Set Variable    Submit Now

	Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
	And Proceed To Step 2
	And Select Location Type: "${location_type}"
	And Select Location Type All: "${location_type}"
	And Include Archived Pay Stations: "${archived_pay_stations?}"
	And Select Order By: "${order_by}"
	And Select Output Format: "${output_format}"
	And Proceed to Step 3
	And Select Schedule: "${schedule}"
	When Click Save Report
	Then Report "Pay Station Summary" Is Pending
	And Report "Pay Station Summary" Is Completed

Pay Station Summary - Pay Station Route Selected 4
	${report_type}=  Set Variable    Pay Station Summary
	${location_type}=  Set Variable    Pay Station Route
	${payStationRoute}=  Set Variable    Airport Maintenance
	${payStationRoute2}=  Set Variable    Downtown Collections
	${archived_pay_stations?}=  Set Variable    No
	${order_by}=  Set Variable    Coin Count
	${output_format}=  Set Variable    PDF
	${schedule}=  Set Variable    Submit Now

	Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
	And Proceed To Step 2
	And Select Location Type: "${location_type}"
	And Select Pay Station Route: "${payStationRoute}"
	And Select Pay Station Route: "${payStationRoute2}"
	And Include Archived Pay Stations: "${archived_pay_stations?}"
	And Select Order By: "${order_by}"
	And Select Output Format: "${output_format}"
	And Proceed to Step 3
	And Select Schedule: "${schedule}"
	When Click Save Report
	Then Report "Pay Station Summary" Is Pending
	And Report "Pay Station Summary" Is Completed

*** Keywords ***


