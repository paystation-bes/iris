*** Settings ***
# Summary: Test Suite for verifying the creation and deletion of a Report
# Author: Billy Hoang
# Time: Dec 3, 2015

Resource          ../../../data/data.robot
Resource          ../../../Keywords/CustomerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Suite Teardown    Close Browser
Force Tags        smoke-test

Test Setup    Run Keywords
...           Login as Generic Customer  ${G_CUST_ADMIN_USERNAME}  Password$1
...  AND      Go To Reports Section
...  AND      Click Create Report

Test Teardown    Close Browser


*** Test Cases ***

Tax Report - 1
	I Create A Tax Report Report

Tax Report - Specific Range of Months - 2
    ${report_type}=  Set Variable    Tax Report
    ${detail_type}=  Set Variable    Details
    ${transaction_date/time}=  Set Variable    Month
    ${start_month}=  Set Variable    January
    ${start_year}=  Set Variable    2015
    ${end_month}=  Set Variable    March
    ${end_year}=  Set Variable    2015
    ${location_type}=  Set Variable    Pay Station Route
    ${archived_pay_stations?}=  Set Variable    No
    ${group_by}=  Set Variable    Tax Name
    ${schedule}=  Set Variable    Submit Now

    Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Report Detail Type: "${detail_type}"
    And Select Transaction Date/Time: "${transaction_date/time}"
    And Input Start Month: "${start_month}"
    And Input Start Year: "${start_year}"
    And Input End Month: "${end_month}"
    And Input End Year: "${end_year}"
    And Select Location Type: "${location_type}"
    And Select Location Type All: "${location_type}"
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Group By: "${group_by}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    When Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed

Tax Report - Location Type = Pay Station Route All Selected - 3
	[Template]    I Create A Tax Report Report
		 transaction_date/time=Last Month
	...  location_type=Pay Station Route

Tax Report - Location Type = Pay Station Route Selected - 4
    ${report_type}=  Set Variable    Tax Report
    ${detail_type}=  Set Variable    Details
    ${transaction_date/time}=  Set Variable    This Year
    ${location_type}=  Set Variable    Pay Station Route
    ${payStationRoute}=  Set Variable    Airport Collections
    ${archived_pay_stations?}=  Set Variable    No
    ${group_by}=  Set Variable    Month
    ${schedule}=  Set Variable    Submit Now
    ${output_format}=  Set Variable    CSV

    Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Report Detail Type: "${detail_type}"
    And Select Transaction Date/Time: "${transaction_date/time}"
    And Select Location Type: "${location_type}"
    And Select Pay Station Route: "${payStationRoute}"
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Group By: "${group_by}"
    And Select Output Format: "${output_format}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    When Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed

Tax Report - No Locations/Pay Stations Selected (Invalid Locations) - 5
    # By default, all the locations will be selcted if the location type is Location
    ${report_type}=  Set Variable    Tax Report
    ${detail_type}=  Set Variable    Summary
    ${transaction_date/time}=  Set Variable    Last Month
    ${location_type}=  Set Variable    Location
    ${archived_pay_stations?}=  Set Variable    No
    ${group_by}=  Set Variable    Month
    ${schedule}=  Set Variable    Submit Now

    Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Report Detail Type: "${detail_type}"
    And Select Transaction Date/Time: "${transaction_date/time}"
    And Select Location Type: "${location_type}"
    And Select/Deselect All Locations
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Group By: "${group_by}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    When Click Save Report
    Then Attention, Location/Pay Station Is Required
    And Close Pop-up Window
    And Cancel Report

Tax Report - No Locations/Pay Stations Selected (Invalid Routes) - 5
	# By default, changing from location->route will have no routes selected
    ${report_type}=  Set Variable    Tax Report
    ${detail_type}=  Set Variable    Summary
    ${transaction_date/time}=  Set Variable    Last Month
    ${location_type}=  Set Variable    Pay Station Route
    ${archived_pay_stations?}=  Set Variable    No
    ${group_by}=  Set Variable    Month
    ${schedule}=  Set Variable    Submit Now

    Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Report Detail Type: "${detail_type}"
    And Select Transaction Date/Time: "${transaction_date/time}"
    And Select Location Type: "${location_type}"
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Group By: "${group_by}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    When Click Save Report
    Then Attention, Route Is Required
    And Close Pop-up Window
    And Cancel Report

*** Keywords ***
