*** Settings ***
# Summary: Test Suite for verifying the creation and deletion of a Report
# Author: Billy Hoang
# Time: Dec 3, 2015

Resource          ../../../data/data.robot
Resource          ../../../Keywords/CustomerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Suite Teardown    Close Browser
Force Tags        smoke-test

Test Setup    Run Keywords
...           Login as Generic Customer  ${G_CUST_ADMIN_USERNAME}  Password$1
...  AND      Go To Reports Section
...  AND      Click Create Report

Test Teardown    Close Browser


*** Test Cases ***

Stall Reports - All Locations - 1
	I Create A Stall Reports Report

Stall Reports - Between Range - 2
    ${report_type}=  Set Variable    Stall Reports
    ${space_location}=  Set Variable    Unassigned
    ${archived_pay_stations?}=  Set Variable    No
    ${space_number}=    Set Variable    Between
    ${first_space_number}=  Set Variable    2
    ${second_space_number}=    Set Variable    25
    ${other_report_type}=  Set Variable    Valid
    ${schedule}=  Set Variable    Submit Now

    Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Space Location: "${space_location}"
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    And Input First Space Number: "${first_space_number}"
    And Input Second Space Number: "${second_space_number}"
    And Select Other Report Type: "${other_report_type}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    When Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed


Stall Reports - Between Invalid Space Number (Negative) - 2
    ${report_type}=  Set Variable    Stall Reports
    ${space_location}=  Set Variable    Unassigned
    ${archived_pay_stations?}=  Set Variable    No
    ${space_number}=    Set Variable    Between
    ${first_space_number}=  Set Variable    -2
    ${second_space_number}=    Set Variable    25
    ${other_report_type}=  Set Variable    Valid
    ${schedule}=  Set Variable    Submit Now

    Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Space Location: "${space_location}"
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    When Input First Space Number: "${first_space_number}"
    Then Attention, Only Basic Numerical Value
    And Close Pop-up Window
    And Cancel Report

Stall Reports - Between Invalid Space Number (letters and symbols) - 2
    ${report_type}=  Set Variable    Stall Reports
    ${space_location}=  Set Variable    Unassigned
    ${archived_pay_stations?}=  Set Variable    No
    ${space_number}=    Set Variable    Between
    ${first_space_number}=  Set Variable    s$@f
    ${second_space_number}=    Set Variable    abc
    ${other_report_type}=  Set Variable    Valid
    ${schedule}=  Set Variable    Submit Now

    Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Space Location: "${space_location}"
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    And Input First Space Number: "${first_space_number}"
    And Input Second Space Number: "${second_space_number}"
    When Click Anywhere On Page
    Then Attention, Only Basic Numerical Value
    And Close Pop-up Window
    And Cancel Report

Stall Reports - Between Blank Spaces in Space Number - 2
    ${report_type}=  Set Variable    Stall Reports
    ${space_location}=  Set Variable    Unassigned
    ${archived_pay_stations?}=  Set Variable    No
    ${space_number}=    Set Variable    Between
    ${first_space_number}=  Set Variable
    ${second_space_number}=    Set Variable
    ${other_report_type}=  Set Variable    Valid
    ${schedule}=  Set Variable    Submit Now

    Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Space Location: "${space_location}"
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    And Input First Space Number: "${first_space_number}"
    And Input Second Space Number: "${second_space_number}"
    And Select Other Report Type: "${other_report_type}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    When Click Save Report
    Then Attention, Unable To Process Request
    And Close Pop-up Window
    And Cancel Report

Stall Reports - Between Invalid Range (Larger number first) - 2
    ${report_type}=  Set Variable    Stall Reports
    ${space_location}=  Set Variable    Unassigned
    ${archived_pay_stations?}=  Set Variable    No
    ${space_number}=    Set Variable    Between
    ${first_space_number}=  Set Variable    233
    ${second_space_number}=    Set Variable    25
    ${other_report_type}=  Set Variable    Valid
    ${schedule}=  Set Variable    Submit Now

    Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Space Location: "${space_location}"
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    And Input First Space Number: "${first_space_number}"
    And Input Second Space Number: "${second_space_number}"
    When Click Anywhere On Page
    Then Attention, Ending Space Number Must Be Larger
    And Close Pop-up Window
    And Cancel Report

Stall Reports - Between with more than 5-digit Space Numbers - 2
    ${report_type}=  Set Variable    Stall Reports
    ${space_location}=  Set Variable    Unassigned
    ${archived_pay_stations?}=  Set Variable    No
    ${space_number}=    Set Variable    Between
    ${first_space_number}=  Set Variable    2
    ${second_space_number}=    Set Variable    251245
    ${other_report_type}=  Set Variable    Valid
    ${schedule}=  Set Variable    Submit Now

    Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Space Location: "${space_location}"
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    And Input First Space Number: "${first_space_number}"
    And Input Second Space Number: "${second_space_number}"
    And Select Other Report Type: "${other_report_type}"
    And Textfield Should Contain    css=#formSpaceNumberEnd    25124
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    When Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed

Stall Reports - Between the Same Space Number - 2
    ${report_type}=  Set Variable    Stall Reports
    ${space_location}=  Set Variable    Unassigned
    ${archived_pay_stations?}=  Set Variable    No
    ${space_number}=    Set Variable    Between
    ${first_space_number}=  Set Variable    2
    ${second_space_number}=    Set Variable    2
    ${other_report_type}=  Set Variable    Expired
    ${schedule}=  Set Variable    Submit Now

    Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Space Location: "${space_location}"
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    And Input First Space Number: "${first_space_number}"
    And Input Second Space Number: "${second_space_number}"
    Then Page Should Not Contain Textfield    ${second_space_number}
    And Select Other Report Type: "${other_report_type}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    When Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed

Stall Reports - Expired - 3
	[Template]    I Create A Stall Reports Report
		 stall_status=Expired


*** Keywords ***

