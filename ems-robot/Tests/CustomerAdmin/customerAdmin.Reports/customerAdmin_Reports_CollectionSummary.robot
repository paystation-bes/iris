*** Settings ***
# Summary: Test Suite for verifying the creation and deletion of a Report
# Author: Billy Hoang
# Time: Dec 3, 2015

Resource          ../../../data/data.robot
Resource          ../../../Keywords/CustomerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Suite Teardown    Close Browser
Force Tags        smoke-test

Test Setup    Run Keywords
...           Login as Generic Customer  ${G_CUST_ADMIN_USERNAME}  Password$1
...  AND      Go To Reports Section
...  AND      Click Create Report

Test Teardown    Close Browser


*** Test Cases ***

Collection Summary - All Collection Types 1
    I Create A Collection Summary Report

Collection Summary - One From Each Category Selected (Collection Type) 1
    ${report_type}=  Set Variable    Collection Summary
    ${detail_type}=  Set Variable    Summary
    ${location_type}=  Set Variable    Location
    ${collection_count}=  Set Variable    Coin
    ${collection_dollars}=  Set Variable   Bill
    ${collection_count2}=  Set Variable    Card
    ${group_by}=  Set Variable    Location
    ${schedule}=  Set Variable    Submit Now
    ${output_format}=  Set Variable    PDF

    Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Location Type: "${location_type}"
    And Select Location Type All: "${location_type}"
    And Select Collection Type: ${collection_count} - Count
    And Select Collection Type: ${collection_dollars} - Dollars
    And Select Collection Type: ${collection_count2} - Count
    And Select Collection Type: Running Total - Dollars
    And Select Collection Type: Collection Interval - Overdue
    And Select Group By: "${group_by}"
    And Select Output Format: "${output_format}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    When Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed

Collection Summary - Select All Pay Station Routes - 2
    [Template]    I Create A Collection Summary Report
         location_type=Pay Station Route

Collection Summary - Selected Pay Station Routes - 2
    ${report_type}=  Set Variable    Collection Summary
    ${detail_type}=  Set Variable    Summary
    ${location_type}=  Set Variable    Pay Station Route
    ${pay_station_route}=  Set Variable    Airport Maintenance
    ${pay_station_route2}=  Set Variable    Downtown Collections
    ${collection_count}=  Set Variable    Coin
    ${collection_dollars}=  Set Variable   Coin
    ${collection_count2}=  Set Variable    Card
    ${group_by}=  Set Variable    Pay Station Route
    ${schedule}=  Set Variable    Submit Now
    ${output_format}=  Set Variable    PDF

    Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Location Type: "${location_type}"
    And Select Pay Station Route: "${pay_station_route}"
    And Select Pay Station Route: "${pay_station_route2}"
    And Select Collection Type: ${collection_count} - Count
    And Select Collection Type: ${collection_dollars} - Dollars
    And Select Collection Type: ${collection_count2} - Count
    And Select Collection Type: Running Total - Dollars
    And Select Group By: "${group_by}"
    And Select Output Format: "${output_format}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    When Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed

Collection Summary - No Pay Station Routes Selected (Invalid) - 2
    ${report_type}=  Set Variable    Collection Summary
    ${detail_type}=  Set Variable    Summary
    ${location_type}=  Set Variable    Pay Station Route
    ${collection_count}=  Set Variable    Coin
    ${collection_count2}=  Set Variable    Card
    ${group_by}=  Set Variable    Pay Station Route
    ${schedule}=  Set Variable    Submit Now
    ${output_format}=  Set Variable    PDF

    Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Location Type: "${location_type}"
    And Select Collection Type: ${collection_count} - Count
    And Select Collection Type: ${collection_count2} - Count
    And Select Collection Type: Running Total - Dollars
    And Select Group By: "${group_by}"
    And Select Output Format: "${output_format}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    When Click Save Report
    Then Attention, Route Is Required
    And Close Pop-up Window
    And Cancel Report

Collection Summary - Select All Pay Stations - 3
    [Template]    I Create A Collection Summary Report
         location_type=Pay Station

Collection Summary - Selected Pay Stations - 3
    ${report_type}=  Set Variable    Collection Summary
    ${detail_type}=  Set Variable    Details
    ${location_type}=  Set Variable    Pay Station
    ${pay_station}=  Set Variable    ${G_PAYSTATION_3}
    ${pay_station2}=  Set Variable    ${G_PAYSTATION_4}
    ${collection_count}=  Set Variable    Coin
    ${collection_dollars}=  Set Variable   Coin
    ${group_by}=  Set Variable    Pay Station
    ${schedule}=  Set Variable    Submit Now
    ${output_format}=  Set Variable    CSV

    Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Location Type: "${location_type}"
    And Select Pay Station: "${pay_station}"
    And Select Pay Station: "${pay_station2}"
    And Select Collection Type: ${collection_count} - Count
    And Select Collection Type: ${collection_dollars} - Dollars
    And Select Collection Type: Running Total - Dollars
    And Select Collection Type: Collection Interval - Overdue
    And Select Group By: "${group_by}"
    And Select Output Format: "${output_format}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    When Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed

Collection Summary - No Pay Stations Selected (Invalid) - 3    ${report_type}=  Set Variable    Collection Summary
    ${detail_type}=  Set Variable    Summary
    ${location_type}=  Set Variable    Pay Station
    ${collection_count}=  Set Variable    Coin
    ${group_by}=  Set Variable    Pay Station
    ${schedule}=  Set Variable    Submit Now
    ${output_format}=  Set Variable    PDF

    Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Location Type: "${location_type}"
    And Select Collection Type: ${collection_count} - Count
    And Select Collection Type: Running Total - Dollars
    And Select Group By: "${group_by}"
    And Select Output Format: "${output_format}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    When Click Save Report
    Then Attention, Pay Station Is Required
    And Close Pop-up Window
    And Cancel Report

Collection Summary - Selected Pay Stations at Specific Location - 3
    ${report_type}=  Set Variable    Collection Summary
    ${detail_type}=  Set Variable    Details
    ${location_type}=  Set Variable    Pay Station
    ${route_or_location}=  Set Variable    Downtown
    ${pay_station}=  Set Variable    ${G_PAYSTATION_0}
    ${pay_station2}=  Set Variable    ${G_PAYSTATION_2}
    ${collection_count}=  Set Variable    Coin
    ${collection_dollars}=  Set Variable   Coin
    ${group_by}=  Set Variable    Location
    ${schedule}=  Set Variable    Submit Now
    ${output_format}=  Set Variable    PDF

    Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Location Type: "${location_type}"
    And Select Specific Pay Station Option: "${route_or_location}"
    And Select Pay Station: "${pay_station}"
    And Select Pay Station: "${pay_station2}"
    And Select Collection Type: ${collection_count} - Count
    And Select Collection Type: ${collection_dollars} - Dollars
    And Select Collection Type: Running Total - Dollars
    And Select Collection Type: Collection Interval - Overdue
    And Select Group By: "${group_by}"
    And Select Output Format: "${output_format}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    When Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed

Collection Summary - Selected Pay Stations at Specific Route - 3
    ${report_type}=  Set Variable    Collection Summary
    ${detail_type}=  Set Variable    Details
    ${location_type}=  Set Variable    Pay Station
    ${route_or_location}=  Set Variable    Airport Collections
    ${pay_station}=  Set Variable    ${G_PAYSTATION_3}
    ${pay_station2}=  Set Variable    ${G_PAYSTATION_4}
    ${collection_count}=  Set Variable    Bill
    ${group_by}=  Set Variable    Pay Station
    ${schedule}=  Set Variable    Submit Now
    ${output_format}=  Set Variable    PDF

    Given Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Location Type: "${location_type}"
    And Select Specific Pay Station Option: "${route_or_location}"
    And Select Pay Station: "${pay_station}"
    And Select Pay Station: "${pay_station2}"
    And Select Collection Type: ${collection_count} - Count
    And Select Collection Type: Running Total - Dollars
    And Select Collection Type: Collection Interval - Overdue
    And Select Group By: "${group_by}"
    And Select Output Format: "${output_format}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    When Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed

*** Keywords ***
