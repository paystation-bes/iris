*** Settings ***
# Summary: Test Suite for verifying the creation and deletion of a Report
# Author: Billy Hoang
# Time: Dec 3, 2015

Resource          ../../../data/data.robot
Resource          ../../../Keywords/CustomerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Suite Teardown    Close Browser
Force Tags        smoke-test

Test Setup    Run Keywords
...           Login as Generic Customer  ${G_CUST_ADMIN_USERNAME}  Password$1
...  AND      Go To Reports Section
...  AND      Click Create Report

Test Teardown    Close Browser


*** Test Cases ***

I Create A Transaction - All Report (Scheduled - Daily) 2
    ${random}=  Get Time  Epoch
    ${detail_type}=  Set Variable  Summary
    ${transaction_date/time}=  Set Variable  Today
    ${location_type}=  Set Variable  Location
    ${archived_pay_stations?}=  Set Variable  Yes
    ${space_number}=  Set Variable  N/A
    ${license_plate}=  Set Variable  ${EMPTY}
    ${ticket_number}=  Set Variable  All
    ${coupon_code}=  Set Variable  N/A
    ${transaction_type}=  Set Variable  All
    ${group_by}=  Set Variable  None
    ${schedule}=  Set Variable  Schedule
    ${output_format}=  Set Variable  PDF
    ${schedule_title}=  Catenate  Title${random}
    ${schedule_time}=  Set Variable  12:00 AM
    ${repeats}=  Set Variable  Daily
    ${once_every}=  Set Variable  3
    ${e_mail}=  Set Variable  billy.hoang@t2systems.com

    Select Report Type: "Transaction - All"
    Proceed To Step 2
    Select Report Detail Type: "${detail_type}"
    Select Transaction Date/Time: "${transaction_date/time}"
    Select Location Type All: "${location_type}"
    Include Archived Pay Stations: "${archived_pay_stations?}"
    Select Space Number: "${space_number}"
    Input License Plate: "${license_plate}"
    Select Ticket Number: "${ticket_number}"
    Select Coupon Code: "${coupon_code}"
    Select Transaction Type: "${transaction_type}"
    Select Group By: "${group_by}"
    Select Output Format: "${output_format}"
    Proceed To Step 3
    Select Schedule: "${schedule}"
    Input Schedule Title: "${schedule_title}"
    Select Schedule Time: "${schedule_time}"
    Select Repeat: "${repeats}"
    Input E-mail: "${e_mail}"
    Click Save Report
    Scheduled Report With Name: "${schedule_title}" Is Created

I Create A Transaction - All Report (Scheduled - Weekly) 3
    ${random}=  Get Time  Epoch
    ${detail_type}=  Set Variable  Summary
    ${transaction_date/time}=  Set Variable  Today
    ${location_type}=  Set Variable  Location
    ${archived_pay_stations?}=  Set Variable  Yes
    ${space_number}=  Set Variable  N/A
    ${license_plate}=  Set Variable  ${EMPTY}
    ${ticket_number}=  Set Variable  All
    ${coupon_code}=  Set Variable  N/A
    ${transaction_type}=  Set Variable  All
    ${group_by}=  Set Variable  None
    ${schedule}=  Set Variable  Schedule
    ${output_format}=  Set Variable  PDF
    ${schedule_title}=  Catenate  Title${random}
    ${schedule_time}=  Set Variable  12:00 AM
    ${repeats}=  Set Variable  Weekly
    ${once_every}=  Set Variable  2
    ${e_mail}=  Set Variable  billy.hoang@t2systems.com

    Select Report Type: "Transaction - All"
    Proceed To Step 2
    Select Report Detail Type: "${detail_type}"
    Select Transaction Date/Time: "${transaction_date/time}"
    Select Location Type All: "${location_type}"
    Include Archived Pay Stations: "${archived_pay_stations?}"
    Select Space Number: "${space_number}"
    Input License Plate: "${license_plate}"
    Select Ticket Number: "${ticket_number}"
    Select Coupon Code: "${coupon_code}"
    Select Transaction Type: "${transaction_type}"
    Select Group By: "${group_by}"
    Select Output Format: "${output_format}"
    Proceed To Step 3
    Select Schedule: "${schedule}"
    Input Schedule Title: "${schedule_title}"
    Select Schedule Time: "${schedule_time}"
    Select Repeat: "${repeats}"
    Select Repeat On For Weekly
    Input E-mail: "${e_mail}"
    Click Save Report
    Scheduled Report With Name: "${schedule_title}" Is Created


I Create A Transaction - All Report (Scheduled - Monthly) 4
    ${random}=  Get Time  Epoch
    ${detail_type}=  Set Variable  Summary
    ${transaction_date/time}=  Set Variable  Today
    ${location_type}=  Set Variable  Location
    ${archived_pay_stations?}=  Set Variable  Yes
    ${space_number}=  Set Variable  N/A
    ${license_plate}=  Set Variable  ${EMPTY}
    ${ticket_number}=  Set Variable  All
    ${coupon_code}=  Set Variable  N/A
    ${transaction_type}=  Set Variable  All
    ${group_by}=  Set Variable  None
    ${schedule}=  Set Variable  Schedule
    ${output_format}=  Set Variable  PDF
    ${schedule_title}=  Catenate  Title${random}
    ${schedule_time}=  Set Variable  12:00 AM
    ${repeats}=  Set Variable  Monthly
    ${once_every}=  Set Variable  2
    ${e_mail}=  Set Variable  billy.hoang@t2systems.com

    Select Report Type: "Transaction - All"
    Proceed To Step 2
    Select Report Detail Type: "${detail_type}"
    Select Transaction Date/Time: "${transaction_date/time}"
    Select Location Type All: "${location_type}"
    Include Archived Pay Stations: "${archived_pay_stations?}"
    Select Space Number: "${space_number}"
    Input License Plate: "${license_plate}"
    Select Ticket Number: "${ticket_number}"
    Select Coupon Code: "${coupon_code}"
    Select Transaction Type: "${transaction_type}"
    Select Group By: "${group_by}"
    Select Output Format: "${output_format}"
    Proceed To Step 3
    Select Schedule: "${schedule}"
    Input Schedule Title: "${schedule_title}"
    Select Schedule Time: "${schedule_time}"
    Select Repeat: "${repeats}"
    Select Repeat On For Monthly
    Input E-mail: "${e_mail}"
    Click Save Report
    Scheduled Report With Name: "${schedule_title}" Is Created