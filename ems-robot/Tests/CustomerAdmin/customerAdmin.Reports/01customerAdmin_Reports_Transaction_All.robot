*** Settings ***
# Summary: Test Suite for verifying the creation and deletion of a Report
# Author: Billy Hoang
# Time: Dec 3, 2015

Resource          ../../../data/data.robot
Resource          ../../../Keywords/CustomerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Suite Teardown    Close Browser
Force Tags        smoke-test

Test Setup    Run Keywords
...           Create Valid Paymentech Merchant Account For: "${G_CUSTOMER_NAME}"
...  AND      Login as Generic Customer  ${G_CUST_ADMIN_USERNAME}  Password$1
...  AND      Go To Reports Section
...  AND      Click Create Report

Test Teardown    Close Browser


*** Variables ***


*** Test Cases ***
# NOTE: The numbering of the test cases follow the numbering in EMS-10946

Transaction - All 1
    I Create A Transaction - All Report

Transaction - All (Details) 2
    [Documentation]    Specifies a specific range of months
    ${report_type}=  Set Variable    Transaction - All
    ${detail_type}=  Set Variable    Details
    ${transaction_date/time}=  Set Variable    Month  #
    ${start_month}=  Set Variable    January  #
    ${start_year}=  Set Variable    2015  #
    ${end_month}=  Set Variable    March  #
    ${end_year}=  Set Variable    2015  #
    ${location_type}=  Set Variable    Pay Station Route  #
    ${archived_pay_stations?}=  Set Variable    No  #
    ${space_number}=  Set Variable    All
    ${license_plate}=  Set Variable    ${EMPTY}
    ${ticket_number}=  Set Variable    All
    ${coupon_code}=  Set Variable    N/A
    ${transaction_type}=  Set Variable    All
    ${group_by}=  Set Variable    Month  #
    ${schedule}=  Set Variable    Submit Now

    Given Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Report Detail Type: "${detail_type}"
    And Select Transaction Date/Time: "${transaction_date/time}"
    And Input Start Month: "${start_month}"
    And Input Start Year: "${start_year}"
    And Input End Month: "${end_month}"
    And Input End Year: "${end_year}"
    And Select Location Type: "${location_type}"
    And Select Location Type All: "${location_type}"
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    And Input License Plate: "${license_plate}"
    And Select Ticket Number: "${ticket_number}"
    And Select Coupon Code: "${coupon_code}"
    And Select Transaction Type: "${transaction_type}"
    And Select Group By: "${group_by}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    And Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed

Transaction - All (Details) 3
    [Documentation]    Specifies a >= range for space number
    ${report_type}=  Set Variable    Transaction - All
    ${detail_type}=  Set Variable    Details
    ${transaction_date/time}=    Set Variable    Yesterday  #
    ${location_type}=  Set Variable    Pay Station
    ${archived_pay_stations?}=  Set Variable    Yes
    ${space_number}=  Set Variable    >=  #
    ${first_space_number}=  Set Variable     1
    ${license_plate}=  Set Variable    ${EMPTY}
    ${ticket_number}=  Set Variable    All
    ${coupon_code}=  Set Variable    N/A
    ${transaction_type}=  Set Variable    All
    ${group_by}=  Set Variable    None
    ${schedule}=  Set Variable    Submit Now
    ${output_format}=  Set Variable    CSV

    Given Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Report Detail Type: "${detail_type}"
    And Select Transaction Date/Time: "${transaction_date/time}"
    And Select Location Type: "${location_type}"
    And Select Location Type All: "${location_type}"
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    And Input First Space Number: "${first_space_number}"
    And Input License Plate: "${license_plate}"
    And Select Ticket Number: "${ticket_number}"
    And Select Coupon Code: "${coupon_code}"
    And Select Transaction Type: "${transaction_type}"
    And Select Output Format: "${output_format}"
    And Select Group By: "${group_by}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    And Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed


Transaction - All (Summary) 4
    [Documentation]  Specifies a range of space numbers
    ${report_type}=  Set Variable    Transaction - All
    ${detail_type}=  Set Variable    Summary
    ${transaction_date/time}=    Set Variable    This Week
    ${location_type}=  Set Variable    Location
    ${archived_pay_stations?}=  Set Variable    No
    ${space_number}=  Set Variable    Between
    ${first_space_number}=  Set Variable    2
    ${second_space_number}=  Set Variable    15
    ${license_plate}=  Set Variable    ${EMPTY}
    ${ticket_number}=  Set Variable    All
    ${coupon_code}=  Set Variable    N/A
    ${transaction_type}=  Set Variable    All
    ${group_by}=  Set Variable    Payment Type
    ${schedule}=  Set Variable    Submit Now

    Given Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Report Detail Type: "${detail_type}"
    And Select Transaction Date/Time: "${transaction_date/time}"
    And Select Location Type: "${location_type}"
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    And Input First Space Number: "${first_space_number}"
    And Input Second Space Number: "${second_space_number}"
    And Input License Plate: "${license_plate}"
    And Select Ticket Number: "${ticket_number}"
    And Select Coupon Code: "${coupon_code}"
    And Select Transaction Type: "${transaction_type}"
    And Select Group By: "${group_by}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    And Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed

Transaction - All (Details) 5
    [Template]    I Create a Transaction - All Report
         detail_type=Details
    ...  transaction_date/time=This Month
    ...  location_type=Pay Station Route
    ...  group_by=Day
    ...  output_format=CSV

Transaction - All (Summary) 6
    [Documentation]  Specific Location
    ${report_type}=  Set Variable    Transaction - All
    ${detail_type}=  Set Variable    Details
    ${transaction_date/time}=    Set Variable    Last 12 Months
    ${location_type}=  Set Variable    Pay Station
    ${specific_refinement}=  Set Variable    Downtown
    ${archived_pay_stations?}=  Set Variable    No
    ${space_number}=  Set Variable    Between
    ${first_space_number}=  Set Variable    2
    ${second_space_number}=  Set Variable    15
    ${license_plate}=  Set Variable    ${EMPTY}
    ${ticket_number}=  Set Variable    All
    ${coupon_code}=  Set Variable    N/A
    ${transaction_type}=  Set Variable    All
    ${group_by}=  Set Variable    Payment Type
    ${schedule}=  Set Variable    Submit Now

    Given Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Report Detail Type: "${detail_type}"
    And Select Transaction Date/Time: "${transaction_date/time}"
    And Select Location Type: "${location_type}"
    And Select Specific Pay Station Option: "${specific_refinement}"
    And Select All Refined Pay Stations
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    And Input First Space Number: "${first_space_number}"
    And Input Second Space Number: "${second_space_number}"
    And Input License Plate: "${license_plate}"
    And Select Ticket Number: "${ticket_number}"
    And Select Coupon Code: "${coupon_code}"
    And Select Transaction Type: "${transaction_type}"
    And Select Group By: "${group_by}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    And Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed

Transaction - All (Summary) 7
    [Documentation]  Specifies a specific coupon code
    ${report_type}=  Set Variable    Transaction - All
    ${detail_type}=  Set Variable    Summary
    ${transaction_date/time}=    Set Variable    Last 7 Days
    ${location_type}=  Set Variable    Location
    ${archived_pay_stations?}=  Set Variable    Yes
    ${space_number}=  Set Variable    N/A
    ${license_plate}=  Set Variable    ${EMPTY}
    ${ticket_number}=  Set Variable    All
    ${coupon_code}=  Set Variable    Specific Code
    ${actual_coupon_code}=  Set Variable    ABC123
    ${transaction_type}=  Set Variable    Test
    ${group_by}=  Set Variable    Trans Type
    ${output_format}=  Set Variable    PDF
    ${schedule}=  Set Variable    Submit Now

    Given Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Report Detail Type: "${detail_type}"
    And Select Transaction Date/Time: "${transaction_date/time}"
    And Select Location Type: "${location_type}"
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    And Input License Plate: "${license_plate}"
    And Select Ticket Number: "${ticket_number}"
    And Select Coupon Code: "${coupon_code}"
    And Input Coupon Code: "${actual_coupon_code}"
    And Select Transaction Type: "${transaction_type}"
    And Select Group By: "${group_by}"
    And Select Output Format: "${output_format}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    And Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed


Transaction - All (Details) 8
    [Template]    I Create a Transaction - All Report
         detail_type=Details
    ...  transaction_date/time=Last Year
    ...  location_type=Pay Station Route
    ...  archived_pay_stations?=No
    ...  transaction_type=Add Time


Transaction - All (Summary) 9
    [Documentation]    Specifies a specific Date/Time Range
    ${report_type}=  Set Variable    Transaction - All
    ${detail_type}=  Set Variable    Details
    ${transaction_date/time}=    Set Variable    Date/Time
    # ${beginning_date}
    # ${beginning_time}
    # ${end_date}
    # ${end_time}
    ${location_type}=  Set Variable    Pay Station
    ${specific_refinement}=  Set Variable    Airport Collections
    ${archived_pay_stations?}=  Set Variable    Yes
    ${space_number}=  Set Variable    N/A
    ${license_plate}=  Set Variable    ${EMPTY}
    ${ticket_number}=  Set Variable    All
    ${coupon_code}=  Set Variable    N/A
    ${transaction_type}=  Set Variable    SC Recharge
    ${group_by}=  Set Variable    Month
    ${output_format}=  Set Variable    CSV
    ${schedule}=  Set Variable    Submit Now

    Given Select Report Type: "${report_type}"
    And Proceed To Step 2
    And Select Report Detail Type: "${detail_type}"
    And Select Transaction Date/Time: "${transaction_date/time}"
    And Select Transaction Date/Time
    And Select Location Type: "${location_type}"
    And Select Specific Pay Station Option: "${specific_refinement}"
    And Select All Refined Pay Stations
    And Include Archived Pay Stations: "${archived_pay_stations?}"
    And Select Space Number: "${space_number}"
    And Input License Plate: "${license_plate}"
    And Select Ticket Number: "${ticket_number}"
    And Select Coupon Code: "${coupon_code}"
    And Select Transaction Type: "${transaction_type}"
    And Select Group By: "${group_by}"
    And Select Output Format: "${output_format}"
    And Proceed To Step 3
    And Select Schedule: "${schedule}"
    And Click Save Report
    Then Report "${report_type}" Is Pending
    And Report "${report_type}" Is Completed


Transaction - All (Summary) 10
    [Template]    I Create a Transaction - All Report
         archived_pay_stations?=No
    ...  transaction_type=Replenish
    ...  group_by=Pay Station














