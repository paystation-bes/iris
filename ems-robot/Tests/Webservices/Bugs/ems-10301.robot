*** Settings ***
# Summary: Automation for EMS-10391
# Author: Billy Hoang

Resource          ../../../data/data.robot
Resource          ../../../Keywords/globalKeywordDirectory.robot
Library           DatabaseLibrary
Suite Teardown    Enable All Digital API Services  ${G_CUSTOMER_NAME}
Test Teardown    Close Browser



*** Test Case ***

# Original Bug:
# If Digital API: Write is disabled with systemadmin, Digital API: Read would not show up
# Each Digital API feature should be able to display independently of each other

# STEPS:
# In System Admin, change the user's services in the following combinations:
# Note: Pre-fixed with ~ means to disable
# R = Digital API Read
# W = Digital API Write
# X = Digital API XChange
    # R,W,X
    # ~R,W,X
    # R,~W,X
    # R,W,~X
    # ~R,~W,X
    # ~R,W,~X
    # R,~W,~X
    # ~R,~W,~X


Test EMS-10301: R,W,X
    ${enabled_services}=  Create List  Digital API: Read  Digital API: Write  Digital API: XChange
    ${disabled_services}=  Create List  ${EMPTY}
    Test EMS-10301  ${G_CUSTOMER_NAME}  ${G_CUST_ADMIN_USERNAME}  ${enabled_services}  ${disabled_services}

Test EMS-10301: ~R,W,X
    ${enabled_services}=  Create List  Digital API: Write  Digital API: XChange
    ${disabled_services}=  Create List    Digital API: Read
    Test EMS-10301  ${G_CUSTOMER_NAME}  ${G_CUST_ADMIN_USERNAME}  ${enabled_services}  ${disabled_services}

Test EMS-10301: R,~W,X
    ${enabled_services}=  Create List  Digital API: Read  Digital API: XChange
    ${disabled_services}=  Create List    Digital API: Write
    Test EMS-10301  ${G_CUSTOMER_NAME}  ${G_CUST_ADMIN_USERNAME}  ${enabled_services}  ${disabled_services}

Test EMS-10301: R,W,~X
    ${enabled_services}=  Create List  Digital API: Read  Digital API: Write
    ${disabled_services}=  Create List  Digital API: XChange
    Test EMS-10301  ${G_CUSTOMER_NAME}  ${G_CUST_ADMIN_USERNAME}  ${enabled_services}  ${disabled_services}

Test EMS-10301: ~R,~W,X
    ${enabled_services}=  Create List  Digital API: XChange
    ${disabled_services}=  Create List  Digital API: Read  Digital API: Write
    Test EMS-10301  ${G_CUSTOMER_NAME}  ${G_CUST_ADMIN_USERNAME}  ${enabled_services}  ${disabled_services}

Test EMS-10301: ~R,W,~X
    ${enabled_services}=  Create List  Digital API: Write
    ${disabled_services}=  Create List  Digital API: Read  Digital API: XChange
    Test EMS-10301  ${G_CUSTOMER_NAME}  ${G_CUST_ADMIN_USERNAME}  ${enabled_services}  ${disabled_services}

Test EMS-10301: R,~W,~X
    ${enabled_services}=  Create List  Digital API: Read
    ${disabled_services}=  Create List  Digital API: Write  Digital API: XChange
    Test EMS-10301  ${G_CUSTOMER_NAME}  ${G_CUST_ADMIN_USERNAME}  ${enabled_services}  ${disabled_services}

Test EMS-10301: ~R,~W,~X
    ${enabled_services}=  Create List  ${EMPTY}
    ${disabled_services}=  Create List  Digital API: Read  Digital API: Write  Digital API: XChange
    Test EMS-10301  ${G_CUSTOMER_NAME}  ${G_CUST_ADMIN_USERNAME}  ${enabled_services}  ${disabled_services}



*** Keywords ***


Test EMS-10301
# This is a keyword to test the bug for EMS-10301
#
# The test cases above tries out every combination of Digital API services and ensures that the visibility of each service is
# independent of each other in both system admin and customer admin accounts
    [arguments]  ${customer_name}  ${customer_username}  ${enabled_services}  ${disabled_services}

    Given Following Services: "${enabled_services}" For Customer: "${customer_name}" Are: "Enabled"
    And Following Services: "${disabled_services}" For Customer: "${customer_name}" Are: "Disabled"

    Then Following Digital API Services Should Be Visible In System Admin: "${enabled_services}" For Customer: "${customer_name}"
    And Following Digital API Services Should Not Be Visible In System Admin: "${disabled_services}" For Customer: "${customer_name}"
    And Following Digital API Services Should Be Visible In Customer Admin: "${enabled_services}" For Customer: "${customer_username}"
    And Following Digital API Services Should Not Be Visible In Customer Admin: "${disabled_services}" For Customer: "${customer_username}"


Enable All Digital API Services
    [arguments]  ${customer_name}
# Post-Conditon:
# All Digital API Services will be re-enabled again
    ${digital_api_services}=  Create List  Digital API: Read  Digital API: Write  Digital API: XChange
    Following Services: "${digital_api_services}" For Customer: "${customer_name}" Are: "Enabled"






