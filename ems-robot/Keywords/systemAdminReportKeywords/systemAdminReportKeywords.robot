*** Keywords ***

Click Reports Tab
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    xpath=//a[@name='Reports']
    Click Element    xpath=//a[@name='Reports']

Click Create Report
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    xpath=//a[@id='btnAddScheduleReportWS']
    Click Element    xpath=//a[@id='btnAddScheduleReportWS']

Select Report Type: "${report_type}"
    Click Link    css=#reportTypeIDExpand
    Click Element    xpath=//ul[@id='ui-id-1']//a[contains(text(),'${report_type}')]

Proceed To Step 2
    Click Link    css=#btnStep2Select
    Wait Until Keyword Succeeds    10s    1s    Element should Be Visible    css=#reportDetailsArea

Proceed To Step 3
    Click Link    css=#btnStep3Select
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    css=#reportSchedulingArea

Select Report Detail Type: "${detail_type}"
    Should Be True    '${detail_type}'=='Summary' or '${detail_type}'=='Details'
    Run Keyword If    '${detail_type}'=='Summary'    Click Element    xpath=//a[@id='isSummary:true']
    Run Keyword If    '${detail_type}'=='Details'    Click Element    xpath=//a[@id='isSummary:false']

Select Transaction Date/Time: "${date/time}"
    Click Link    css=#formPrimaryDateTypeExpand
    Click Element    xpath=//ul[@id='ui-id-2']//a[contains(text(),'${date/time}')]

Select Location Type: "${location_type}"
    Click Link    css=#formLocationTypeExpand
    Click Element    xpath=//ul[@id='ui-id-16']//a[text()='${location_type}']

Select Location Type All: "${location_type}"
    Select Location Type: "${location_type}"
    Run Keyword If    '${location_type}'=='Pay Station Route'    Select All Pay Station Routes
    Run Keyword If    '${location_type}'=='Pay Station'    Select All Pay Stations
    

Select Space Location: "${space_location}"
    Click Link    css=#spaceLocationExpand
    Click Element    xpath=//ul[@id='ui-id-19']//a[contains(text(),'${space_location}')]

Include Archived Pay Stations: "${archived_pay_stations?}"
    Should Be True    '${archived_pay_stations?}'=='Yes' or '${archived_pay_stations?}'=='No'
    Run Keyword If    '${archived_pay_stations?}'=='Yes'    Click Element    xpath=//a[@id='showHiddenPayStations:true']
    Run Keyword If    '${archived_pay_stations?}'=='No'    Click Element    xpath=//a[@id='showHiddenPayStations:false']

Select Space Number: "${space_number}"
    Click Link    css=#formSpaceNumberTypeExpand
    Click Element    xpath=//ul[@id='ui-id-18']//a[contains(text(),'${space_number}')]
    Sleep    3

Input License Plate: "${license_plate}"
    Input Text    css=#formLicensePlate    ${license_plate}

Select Stall Status: "${stall_status}"
    Click Link    css=#formOtherParametersExpand
    Click Element    xpath=//ul[@id='ui-id-30']//a[contains(text(),'${stall_status}')]

Select Ticket Number: "${ticket_number}"
    Click Link    css=#formTicketNumberTypeExpand
    Click Element    xpath=//ul[@id='ui-id-20']//a[contains(text(),'${ticket_number}')]

Select Coupon Code: "${coupon_code}"
    Click Link    css=#formCouponNumberTypeExpand
    Click Element    xpath=//ul[@id='ui-id-21']//a[contains(text(),'${coupon_code}')]

Select Card Type: "${card_type}"
    Click Link    css=#formCardTypeExpand
    Click Element    xpath=//ul[@id='ui-id-25']//a/span[contains(text(),'${card_type}')]

Select Approval Status: "${approval_status}"
    Click Link    css=#formApprovalStatusExpand
    Click Element    xpath=//ul[@id='ui-id-26']//a[contains(text(),'${approval_status}')]

Select Merchant Account: "${merchant_account}"
    Click Link    css=#merchantAccountExpand
    Click Element    xpath=//ul[@id='ui-id-27']//a/span[contains(text(),'${merchant_account}')]

Select Card Number: "${card_number}"
    Click Link    css=#formCardNumberTypeExpand
    Click Element    xpath=//ul[@id='ui-id-24']//a[contains(text(),'${card_number}')]

Select Transaction Type: "${transaction_type}"
    Click Link    css=#formOtherParametersExpand
    Click Element    xpath=//ul[@id='ui-id-30']//a[contains(text(),'${transaction_type}')]

Select Group By: "${group_by}"
    Click Link    css=#formGroupByExpand
    Click Element    xpath=//ul[@id='ui-id-31']//a[contains(text(),'${group_by}')]

Select Schedule: "${schedule}"
    Should Be True    '${schedule}'=='Submit Now' or '${schedule}'=='Schedule'
    Run Keyword If    '${schedule}'=='Submit Now'    Click Element    xpath=//a[@id='isScheduled:false']
    Run Keyword If    '${schedule}'=='Schedule'    Click Element    xpath=//a[@id='isScheduled:true']

Click Save Report
    Sleep    3
    Click Element    xpath=//a[contains(text(),'Save')]
    Sleep    3

Report "${report_type}" Is Pending
    Element Should Be Visible    xpath=//ul[@id='queuedReportsList']/li[1]/div[@class='col1']/p[contains(text(),'${report_type}')]

Report "${report_type}" Is Completed
    Wait Until Keyword Succeeds    60s    1s
    ...    Run Keywords    Click Element    xpath=//h2[contains(text(),'Pending and Incomplete Reports')]/following-sibling::a[@id='opBtn_reloadReport']
    ...    AND    Element Should Be Visible    xpath=//ul[@id='reportsList']/li[1]//p[contains(text(),'${report_type}')]
    ...    AND    Element Should Be Visible    xpath=//ul[@id='reportsList']/li[1]//p[contains(text(),'just now')]
    Get New Report Id

Get New Report Id
    ${new_report_id}    Get Element Attribute    xpath=//ul[@id='reportsList']/li[1]@id
    ${new_report_id}    Get Substring    ${new_report_id}  7
    Log To Console    Report ID is: ${new_report_id}
    Set Test Variable    ${report_id}    ${new_report_id}

View Report with Id: "${report_id}"
    Click Element    xpath=//li[@id='report_${report_id}']/a[@title='Option Menu']
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    xpath=//section[@id='menu_${report_id}']//a[@class='view']
    Click Element    xpath=//section[@id='menu_${report_id}']//a[@class='view']

Get Report With Id: "${report_id}"
    Create HTTP Context     ${TEST_SERVER_URL}    https
    GET    /systemAdmin/reporting/viewReport.html?&repositoryID=${report_id}
    Response Status Code Should Equal    302
    ${body}=    Get Response Body
    Log To Console    ${body}

Input Start Month: "${start_month}"
    Click Element    css=#formPrimaryMonthBegin
    Click Element    xpath=//ul[@id='ui-id-5']//a[contains(text(),'${start_month}')]

Input Start Year: "${start_year}"
    Click Element    css=#formPrimaryYearBegin
    Click Element    xpath=//ul[@id='ui-id-6']//a[contains(text(),'${start_year}')]

Input End Month: "${end_month}"
    Click Element    css=#formPrimaryMonthEnd
    Click Element    xpath=//ul[@id='ui-id-7']//a[contains(text(),'${end_month}')]

Input End Year: "${end_year}"
    Click Element    css=#formPrimaryYearEnd
    Click Element    xpath=//ul[@id='ui-id-8']//a[contains(text(),'${end_year}')]

Select All Pay Station Routes
    Click Element    xpath=//section[@id='reportRoute']//label/a

Select All Pay Stations
    Click Element    xpath=//label[@id='reportPayStationsSelAllBtn']/a

Select Output Format: "${output_format}"
    Should Be True    '${output_format}'=='PDF' or '${output_format}'=='CSV'
    Run Keyword If    '${output_format}'=='PDF'    Click Element    xpath=//label[@id='switchIsPDF']/a
    Run Keyword If    '${output_format}'=='CSV'    Click Element    xpath=//label[@id='switchIsNotPDF']/a

Input First Space Number: "${first_space_number}"
    Input Text    css=#formSpaceNumberBegin    ${first_space_number}

Input Second Space Number: "${second_space_number}"
    Input Text    css=#formSpaceNumberEnd    ${second_space_number}

Input Coupon Code: "${coupon_code}"
    Input Text    css=#formCouponNumber    ${coupon_code}

Input First Ticket Number: "${first_ticket_number}"
    Input Text    css=#formTicketNumberBegin    ${first_ticket_number}

Input Second Ticket Number: "${second_ticket_number}"
    Input Text    css=#formTicketNumberEnd    ${second_ticket_number}

#  Always picks the same beginning and end dates
Select Transaction Date/Time
    Click Element    css=#formPrimaryDateBegin
    Click Element    xpath=//a[@title='Prev']
    Click Element    xpath=//a[contains(text(),'13')]
    Click Element    css=#formPrimaryTimeBegin
    Click Element    xpath=//ul[@id='ui-id-3']//a[contains(text(),'12:00 AM')]
    Click Element    css=#formPrimaryTimeBegin
    Click Element    xpath=//a[@title='Prev']
    Click Element    xpath=//a[contains(text(),'14')]
    Click Element    xpath=//ul[@id='ui-id-4']//a[contains(text(),'12:00 AM')]

Select Card Type: "${card_type}"
    Click Element    css=#formCardTypeExpand
    Click Element    xpath=//ul[@id='ui-id-25']//span[coantains(text(),'${card_type}')]/..

Select Approval Status: "${approval_status}"
    Click Element    css=#formApprovalStatusExpand
    Click Element    xpath=//ul[@id='ui-id-26']//span[coantains(text(),'${approval_status}')]/..

Select Merchant Account: "${merchant_account}"
    Click Element    css=#merchantAccountExpand
    Click Element    xpath=//ul[@id='ui-id-27']//span[coantains(text(),'${merchant_account}')]/..

Select Card Number: "${card_number}"
    Click Element    css=#formCardNumberTypeExpand
    Click Element    xpath=//ul[@id='ui-id-24']//span[coantains(text(),'${card_number}')]/..

Select All Pay Stations Under Filter
    ${count}=    Get Matchng Xpath Count    //span[@id='formPayStations']//li
        :FOR    ${index}    IN RANGE    1    ${count}
    \    
    \    Append To List    @{names}    Get Element Attribute    //span[@id='formPayStations']//li[3]@name




    #   A DATA DRIVEN RENDITION OF A TRANSACTION - ALL REPORT KEYWORD

I Create A Transaction - All Report
    [arguments]
    ...  ${detail_type}
    ...  ${transaction_date/time}
    ...  ${location_type}
    ...  ${archived_pay_stations?}
    ...  ${space_number}
    ...  ${license_plate}
    ...  ${ticket_number}
    ...  ${coupon_code}
    ...  ${transaction_type}
    ...  ${group_by}
    ...  ${schedule}

    Select Report Type: "Transaction - All"
    Proceed To Step 2
    Select Report Detail Type: "${detail_type}"
    Select Transaction Date/Time: "${transaction_date/time}"
    Select Location Type: "${location_type}"
    Include Archived Pay Stations: "${archived_pay_stations?}"
    Select Space Number: "${space_number}"
    Input License Plate: "${license_plate}"
    Select Ticket Number: "${ticket_number}"
    Select Coupon Code: "${coupon_code}"
    Select Transaction Type: "${transaction_type}"
    Select Group By: "${group_by}"
    Proceed To Step 3
    Select Schedule: "${schedule}"
    Click Save Report
