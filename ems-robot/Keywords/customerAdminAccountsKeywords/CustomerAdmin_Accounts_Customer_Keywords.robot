*** Settings ***
# Summary: Creates a new customer account
Library           Selenium2Library
Library     	  AutoItLibrary 
Library 		  OperatingSystem
Resource          ../navigationalKeywords/sideBarNav.robot
Resource          ../CustomerAdminAccountsKeywords/CustomerAdmin_Accounts_Coupons_Keywords.robot
Resource          ../CustomerAdminAccountsKeywords/CustomerAdmin_Accounts_Passcards_Keywords.robot

*** Keywords ***

# General Keywords

Add ${coupon_or_passcard} To Customer: "${couponCode_or_cardNumber}"
	# This keyword adds a card type/number to the customer so that it can be associated with it
	# ${card_type/number} - the unique number of the card type/number
	Run Keyword If    '${coupon_or_passcard}'=='Coupon'    	Input Text   	css=#couponQuickAddText    	${couponCode_or_cardNumber}    
	...		ELSE    										Input Text 		css=#cardQuickAddText    	${card_type/number}
	Wait Until Element Is Visible    	xpath=//a[contains(text(),'${couponCode_or_cardNumber}')]    timeout=3
	Click Element                   	xpath=//a[contains(text(),'${couponCode_or_cardNumber}')]
	Click Element                   	xpath=//h3[contains(text(),'Assigned ${coupon_or_passcard}(s):')]/following-sibling::form//a[@title='Add']

Cancel Creating ${customer_coupon_or_passcard}/Update Changes
	# This Keyword will cancel creating a new [customer, coupon, or passcard] or it will cancel the changes that you have inputed into the fields and will return with its original attributes
	Run Keyword If    '${customer_coupon_or_passcard}'=='Customer'    	Click Element    xpath=//section[@id='consumerFormPanel']//a[contains(text(),'Cancel')]                   
	...    ELSE IF    '${customer_coupon_or_passcard}'=='Coupon'     	Click Element    xpath=//form[@id='couponForm']//a[text()='Cancel']	                                      
	...    ELSE IF    '${customer_coupon_or_passcard}'=='Passcard'    	Click Element    xpath=//form[@id='customerCardForm']//a[text()='Cancel']
	Attention, Cancel And Lose Changes
	Click Element    xpath=//span[text()='Yes, cancel now']
	Sleep    3

Click Save ${customer_coupon_or_passcard}/Update Changes
	# This Keyword saves the new customer that was just created
	Sleep    3
	Run Keyword If 		'${customer_coupon_or_passcard}'=='Customer'    Click Element    xpath=//section[@id='consumerFormPanel']//a[contains(text(),'Save')]
	...		ELSE IF 	'${customer_coupon_or_passcard}'=='Coupon'    	Click Element    xpath=//form[@id='couponForm']//a[contains(text(),'Save')]
	...		ELSE IF 	'${customer_coupon_or_passcard}'=='Passcard'    Click Element    xpath=//form[@id='customerCardForm']//a[contains(text(),'Save')] 
	# In xpath, Customer uses section for both, but when you use qa3, it uses form and dev uses section for coupon and passcard
	Sleep    3

Create A New Customer Account
	# This keyword creates a new customer account within the customer admin user
	# [Arguments]:
	# ${first_name} - first name of the new customer
	# ${last_name} - last name of the new customer
	# ${email} - email address of the new customer
	# ${description} - description of the new customer

	[arguments]    ${first_name}    ${last_name}    ${email}    ${description}    ${coupon_code}    ${passcard_number}
	#Go To Accounts Section
	Add New Customer Button
	Fill In Personal Information  ${first_name}  ${last_name}  ${email}  ${description}
	Run Keyword If      '${coupon_code}'=='${EMPTY}' or '${coupon_code}'=='0'    			Do Nothing    
	...			ELSE 	   																	Add Coupon To Customer: "${coupon_code}"
	Run Keyword If      '${passcard_number}'=='${EMPTY}' or '${passcard_number}'=='0'    	Do Nothing    
	...			ELSE    																	Add Passcard To Customer: "${passcard_number}"
	Click Save Customer/Update Changes
	Wait Until Element Is Visible    xpath=//p[contains(text(),'${first_name}')]/..//following-sibling::div[1]//p[contains(text(),'${last_name}')]    timeout=3

Delete "${coupon_or_passcard}" In Customer Section: "${coupon_code_or_passcard_number}"
	# This keyword deletes the coupon or passcard associate with that customer and the specific code or number
	# ${coupon_or_passcard} - A coupon or passcard
	# ${coupon_code_or_passcard_number} - If it is a passcard, you will provide the passcard number, otherwise the coupon code will be provided for a coupon to be removed from the account
	Wait Until Element Is Visible    xpath=//p[text()=${coupon_code_or_passcard_number}]//a
	Click Element					 xpath=//p[text()=${coupon_code_or_passcard_number}]//a
	
Delete Existing "${customer_coupon_or_passcard}": "${email_couponID_or_pcNumber}"
	# This Keyword deletes an existing customer, coupon, or passcard with the email, coupon ID, or pc number provided
	# ${coupon_code} - coupon associated with the coupon code that has to be deleted
	Wait Until Element Is Visible    xpath=//p[contains(text(),'${email_couponID_or_pcNumber}')]/../..//a[@title='Option Menu']   timeout=3
	Click Element                    xpath=//p[contains(text(),'${email_couponID_or_pcNumber}')]/../..//a[@title='Option Menu']
	Wait Until Element Is Visible    xpath=//p[contains(text(),'${email_couponID_or_pcNumber}')]/../../following-sibling::section[1]//a[@class='delete']    timeout=3
	Click Element                    xpath=//p[contains(text(),'${email_couponID_or_pcNumber}')]/../../following-sibling::section[1]//a[@class='delete']
	Wait Until Element Is Visible    xpath=//span[text()='Ok']    timeout=3
	Click Element   				 xpath=//span[text()='Ok']
	Sleep   2
	Page Should Not Contain    		 xpath=//p[contains(text(),'${email_couponID_or_pcNumber}')]

Do Nothing
	Click Element    xpath=//html

Edit Existing "${customer_coupon_or_passcard}": "${email_couponID_or_pcNumber}"
	# This Keyword edits an existing customer, coupon, or passcard with the email, coupon ID, or pc number provided
	# ${coupon_code} - coupon associated with the coupon code that has to be edited
	Wait Until Element Is Visible    xpath=//p[contains(text(),'${email_couponID_or_pcNumber}')]/../..//a[@title='Option Menu']   timeout=3
	Click Element                    xpath=//p[contains(text(),'${email_couponID_or_pcNumber}')]/../..//a[@title='Option Menu']
	Wait Until Element Is Visible    xpath=//p[contains(text(),'${email_couponID_or_pcNumber}')]/../../following-sibling::section[1]//a[@class='edit']    timeout=3
	Click Element                    xpath=//p[contains(text(),'${email_couponID_or_pcNumber}')]/../../following-sibling::section[1]//a[@class='edit']
	Sleep   3

Go To "${customer_coupons_or_passcards}" Section
	# This Keyword goes to the specific sections within the Accounts Tab
	# ${customer_coupons_or_passcards} - Customer Accounts, Coupons, or Passcards can be chosen
	Wait Until Element Is Visible		xpath=//a[text()='${customer_coupons_or_passcards}']
	Click Element    					xpath=//a[text()='${customer_coupons_or_passcards}']
	Sleep    2

Input ${field} Info: "${input_info}"
	# ${field} - can be FirstName, LastName, Email, Description, SpaceNumber, Location
    Clear Element Text          css=#form${field}
	Input Text                  css=#form${field}    ${input_info}

Search For "${name_or_card_number}": "${search_info}"
	# This Keyword searches for a customer or passcard depending on the attributes passed if
	# ${search_info} - can be searched in the field of the customer account or passcode. You need to search
	#					either the Name/Email or Card Type/Number
	# Clear Element Text    xpath=//label[text()='${name_or_card_number}']/../following-sibling::section[1]//input
	# Input Text            xpath=//label[text()='${name_or_card_number}']/../following-sibling::section[1]//input    ${search_info}
	Clear Element Text   	css=#formFilterValue
	Input Text 				css=#formFilterValue 		${search_info}
	Run Keyword If 		'${name_or_card_number}'=='Name/Email'  	    Click Element    xpath=//form[@id='consumerFilterForm']//a[text()='Search']
	...  	ELSE IF 	'${name_or_card_number}'=='Card Type/Number'    Click Element    xpath=//form[@id='customerCardFilterForm']//a[text()='Search']
	Run Keyword If    	'${name_or_card_number}'=='Name/Email'  		Wait Until Element Is Visible    xpath=//h2[text()='Account Details']    timeout=3
	...  	ELSE IF 	'${name_or_card_number}'=='Card Type/Number'	Wait Until Element Is Visible    xpath=//h2[text()='Passcard Details']    timeout=3
	Wait Until Element Is Visible    xpath=//dd[contains(text(),'${search_info}')]    timeout=3
	# need to load up the customer account if it does exist, else do nothing

====================================================================================================================================================================
# Minor, low level keywords specfic to these files
--------------------------------------------------------------------------------------------------------------------------------------------------------------------

Add New Customer Button
	# This Keyword clicks the add button for a new customer to start on their information
	Wait Until Element Is Visible    css=#opBtn_addConsumer   timeout=3
	Click Element    css=#opBtn_addConsumer
	Sleep    3

Allowed For Daily Usage: "${daily_usage?}"
	# This Keyword fills the checkbox as 'Yes' if daily single use is restricted meaning that the coupon can only be use for maximum once a day
	# ${daily_usage?} - 'Yes' means that it can only be used once a day. otherwise there is no limit to the number of times you use it in a day
	Run Keyword If     '${daily_usage?}'=='Yes'    Click Element    //span[@id='formDailySingleUseOptions']//a[@class='checkBox']

Attention, "${email}" Is Not Valid
	Page Should Contain    Email "${email}" is not an email address.

Attention, Cancel And Lose Changes
	# This Keyword asserts that a warning sign should pop-up asking if you want to cancel now without saving any of the changes
	Page Should Contain    Are you sure you would like to Cancel now and lose your changes?

Attention, End Date Cannot Be Before Current Date
	# This Keywords asserts that a warning sign should pop-up stating that the end date has to be after the current date
	Page Should Contain    End Date cannot be before Current Date.

Attention, Choose At Least One Pay Option
	# This Keyword asserts that you a warning sign should pop-up stating that you have to choose at least one of the Pay and 
	# Display/Pay by License Plate or Pay By Space should be selected
	Page Should Contain    The following errors occurred: Choose at least one of Pay and Display/Pay by License Plate or Pay By Space.

Attention, Incorrect Number Of Columns
	Page Should Contain    The following errors occurred: Invalid Header Data - Incorrect # of columns 

Attention, Numbers Separated By Dash/Comma Only
	# This Keyword asserts that a warning sign should pop-up with the field only accpeting numbers sepearated by dashes and commas
	Page Should Contain    This field only accepts Number separated by dash/comma;

Attention, Only Letters And Digits
	# This Keyword asserts that a warning sign should pop-up stating that the field should only contain letters and digis
	Page Should Contain    This field only accepts Letters and digits. Space is not allowed.

Attention, Only Letters, Digits, Space, Dot, Comma, Quote And Special Characters
	# This Keyword asserts that only specific characters are accepted in this field
	Page Should Contain     This field only accepts Letters, digits, space, dot, comma, quote and special characters

Attention, Start Date Must Be Before End Date
	Page Should Contain    Start Date must be before the End Date.

Attention, System Unable To Save Information
	Page Should Contain    Sorry, the system was unable to save your information, please try again.

Fill In Personal Information
	# This Keyword fills in the personal information of a new customer
	# [Arguments]:
	# ${first_name} - customer's first name
	# ${last_name} - customer's last name
	[arguments]    ${first_name}    ${last_name}  ${email}  ${description}
    Input FirstName Info: "${first_name}"
    Input LastName Info: "${last_name}"
    Input Email Info: "${email}"
    Input Description Info: "${description}"
