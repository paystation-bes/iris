*** Settings ***
# Summary: Creates a new customer account
Library           Selenium2Library
Library     	  AutoItLibrary
Library 		  OperatingSystem
Resource          ../navigationalKeywords/sideBarNav.robot
Resource          ../CustomerAdminAccountsKeywords/CustomerAdmin_Accounts_Customer_Keywords.robot
Resource          ../CustomerAdminAccountsKeywords/CustomerAdmin_Accounts_Passcards_Keywords.robot

*** Keywords ***

# General Keywords

Cancel Upload New List Of "${coupons_or_passcards}"
	# This Keyword cancels the upload of a new list of discount coupons to be imported
	Run Keyword If    '${coupons_or_passcards}'=='Coupons'      Click Element    xpath=//form[@id='couponUploadForm']//a[text()='Cancel']
	...    ELSE IF    '${coupons_or_passcards}'=='Passcards'    Click Element    xpath=//form[@id='cardUploadForm']//a[text()='Cancel']
	Attention, Cancel And Lose Changes
	Click Element    xpath=//span[text()='Yes, cancel now']
	Sleep    2

Click "${add_import_or_export}" In "${coupons_or_passcards}"
	# This Keyword chooses between adding, importing, or exporting under the coupons or passcards section
	# ${coupons_or_passcards} - either in the 'Coupon' section or 'Passcard' section
	# ${add_import_or_export} - choose between 'Add', 'Import', and 'Export'
	Go To "${coupons_or_passcards}" Section
	Wait Until Element Is Visible    xpath=//h2[text()='${coupons_or_passcards}']/..//a[@title='Option Menu']    timeout=3
	Click Element                    xpath=//h2[text()='${coupons_or_passcards}']/..//a[@title='Option Menu']
	Wait Until Element Is Visible    xpath=//a[text()='${add_import_or_export}']    timeout=3
	Click Element                    xpath=//a[text()='${add_import_or_export}']
	Sleep    2

Create A Coupon
	# This Keyword creates a coupon under the coupon section which is in the accounts tab
	# [Arguments]:
	# ${coupon_code} - coupon code assigned and used to identify the coupon
	# ${description} - brief description of what the coupon is used if applicable
	# ${discount_amount} - actual discount amount the coupon obtains
	# ${percent_or_dollars} - discount amount is either a percentage of dollar amount
	# ${start_date} - start date of the coupon, if applicable
	# ${end_date} - end date of the coupon, if applicable
	# ${location} - location that the coupon can be used at
	# ${number_of_uses} - specific number of uses of the coupon, if any
	# ${daily_usage?} - limited to daily usage or not
	# ${available_for} - type of display it is available for
	# ${space_range} - if 'Pay By Space' is selected, space range has to between integers

	[arguments]    	${coupon_code}    ${description}    ${discount_amount}    ${percent_or_dollars}    ${start_date}    ${end_date}
	...             ${location}    ${number_of_uses}    ${daily_usage?}    ${available_for}    ${space_range}
	#Go To Accounts Section
	Click "Add" In "Coupons"
	Input Coupon Code Number: "${coupon_code}"
	Run Keyword If    '${description}'=='${EMPTY}'    								Do Nothing
	...			ELSE    															Input Description Info: "${description}"
	Input Discount: "${discount_amount} ${percent_or_dollars}"
	Run Keyword If    '${start_date}'=='${EMPTY}' and '${end_date}'=='${EMPTY}'   	Do Nothing
	...			ELSE    															Input Duration: "${start_date}" To "${end_date}"
	Run Keyword If    '${location}'=='All'   									 	Do Nothing
	...			ELSE    															Select "Coupon" Location: "${location}"
	Run Keyword If    '${number_of_uses}'=='Unlimited'    							Do Nothing
	...			ELSE    															Select Number Of Uses: "${number_of_uses}"
	Allowed For Daily Usage: "${daily_usage?}"
	Select Available For: "${available_for}" and "${space_range}"
	Click Save Coupon/Update Changes
	Wait Until Element Is Not Visible    xpath=//label[text()='Daily Use Restriction:']    timeout=3

Export A Coupon File
	# This Keyword exports all the coupons into a file 
	# Currently, this file only opens up the exported file and does not save it 
	Click "Export" In "Coupons"
	Send     {ENTER}
	Sleep    3

Import A Coupon File
	# This Keyword imports a file into the coupon section
	# [Arguments]:
	# ${file} - file that will be uploaded into the coupon section
	# ${import_mode} - when imported, it can be merged with the existing data or it can replace exisitng coupons with the new ones

	[arguments]    ${file}    ${import_mode}
	Click "Import" In "Coupons"
	Upload File In Coupon: "${file}"
	Select Coupon Import Mode: "${import_mode}"
	Click Upload New List
	Wait Until Element Is Visible     //p[contains(text(),'Start Date')]

Search For Existing Coupon Code: "${coupon_code}", "${discount}", And "${status}"
	# This Keyword searches for an existing coupon with one of the following provided: coupon code, discount, and/or status.
	# ${coupon_code} - coupon code that exists and cane be searched up
	# ${discount} - percentage or dollar amount of the discount provided
	# ${status} - whether the coupon is active, expired, or included all
	Wait Until Element Is Visible    	css=#formFilterValue
	Clear Element Text    				css=#formFilterValue
	Input Text            				css=#formFilterValue    ${coupon_code}
	Clear Element Text    				css=#formFilterDiscount
	Input Text            				css=#formFilterDiscount    ${discount}
	Click Element         				css=#formFilterStatusExpand
	Click Element          			 xpath=//a[@title='Search']
	Wait Until Element Is Visible	 xpath=//dt[text()='Coupon Code:']/following-sibling::dd[text()='${coupon_code}']
	
====================================================================================================================================================================
# Minor, low level keywords specfic to these files
--------------------------------------------------------------------------------------------------------------------------------------------------------------------

Attention, Start Date Is Invalid
	Page Should Contain 	The following errors occurred: Start Date is invalid

Click Upload New List
	# This Keyword uploads/imports the new list of discount coupons or passcards into the customer admin's account
	Sleep    2
	Click Element    xpath=//a[text()='Upload']
	Sleep    2

Coupon Codes Should Be Visible From File
	Wait Until Element Is Visible    xpath=//p[contains(text(),'123654')]		timeout=3
	Wait Until Element Is Visible    xpath=//p[contains(text(),'7777')]			timeout=3
	Wait Until Element Is Visible    xpath=//p[contains(text(),'8888')]			timeout=3
	Wait Until Element Is Visible    xpath=//p[contains(text(),'9999')]			timeout=3

Input Coupon Code Number: "${coupon_code}"
	# This Keyword inputs the coupon code of a new coupon or edits and updates it in an existing coupon
	# ${coupon_code} - unique coupon code id
	Wait Until Element Is Visible    css=#formCouponCode   timeout=3
	Clear Element Text               css=#formCouponCode
	Input Text                       css=#formCouponCode    ${coupon_code}

Input Discount: "${discount_amount} ${percent_or_dollars}"
	# This Keyword inputs the discount amount and whether it is in percentage or dollar amount of a new coupon or edits and updates it in an existing coupon
	# ${discount_amount} - amount of the discount numerically
	# ${percent_or_dollars} - type that the discount is in 'Percent' or 'Dollars'
	Wait Until Element Is Visible    css=#formDiscountValue    timeout=3
	Clear Element Text               css=#formDiscountValue
	Input Text                       css=#formDiscountValue    ${discount_amount}
	Click Element                    css=#formDiscountTypeExpand
	Wait Until Element Is Visible	 xpath=//ul[@id='ui-id-2']//a[text()='${percent_or_dollars}']   timeout=3
	Click Element                    xpath=//ul[@id='ui-id-2']//a[text()='${percent_or_dollars}']

Input Duration: "${start_date}" To "${end_date}"
	# This Keyword inputs the duration of a new coupon or edits and updates it in an existing coupon
	# ${start_date} - must be in the format MM/DD/YYYY, start time when the coupon can be used
	# ${end_date} - must be in the format MM/DD/YYYY, last day coupon can be used before it no longer works
	Clear Element Text     css=#formStartDate
	Input Text             css=#formStartDate    ${start_date}
	Clear Element Text     css=#formEndDate
	Input Text             css=#formEndDate    ${end_date}

Select ${coupon_or_passcard} Import Mode: "${import_mode}"
	# This Keyword selects the import mode whether to
	Click Element                    css=#formUploadModeExpand
	Run Keyword If    '${coupon_or_passcard}'=='Coupon'    Wait Until Element Is Visible    xpath=//ul[@id='ui-id-6']//a[text()='${import_mode}']     timeout=3
	...    ELSE IF 	  '${coupon_or_passcard}'=='Passcard'    Wait Until Element Is Visible    xpath=//ul[@id='ui-id-4']//a[text()='${import_mode}']    timeout=3
	Run Keyword If    '${coupon_or_passcard}'=='Coupon'      Click Element   xpath=//ul[@id='ui-id-6']//a[text()='${import_mode}']
	...    ELSE IF	  '${coupon_or_passcard}'=='Passcard'    Click Element   xpath=//ul[@id='ui-id-4']//a[text()='${import_mode}']

Select "${coupon_or_passcard}" Location: "${location}"
	# This Keyword selects the location that coupon/passcard can be used at and can also be updated
	# ${location} - location of the place that it can used at
	Wait Until Element Is Visible    css=#formLocationExpand
	Click Element    css=#formLocationExpand
	Run Keyword If    '${coupon_or_passcard}'=='Passcard'     	Select Passcard Location/Pay Station: "${location}"
	...					ELSE    								Click Element    xpath=//ul[@id='ui-id-5']//a[text()='${location}']

Select Available For: "${available_for}" And "${space_range}"
	# This Keyword selects the type of payment option display the coupon is available for
	# ${available_for} - if it's 'All', a keyword will be called to select all the options, otherwise only one will be selected
	# ${space_range} - this field will only have a value if 'Pay By Space' is selected
	Run Keyword If    '${available_for}'=='All'    	Select All Types Of Displays: "${space_range}"
	...               ELSE    						Select One Type Of Display: "${available_for}" and "${space_range}"

Select All Types Of Displays: "${space_range}"
	# This Keyword selects all the possible displays for the coupon to be presented, Pay and Display/Pay by License Plate and Pay By Space
	# ${space_range} - the interval in which the range occurs, from the smallest integer to the largest
	Select One Type Of Display: "Pay and Display/Pay by License Plate" And "${EMPTY}"
	Select One Type Of Display: "Pay By Space" And "${space_range}"

Select Number Of Uses: "${number_of_uses}"
	# This Keyword selects 'Specify' since that will all the user to input a specific value the coupon can be used
	# ${number_of_uses} - number of times the coupon can be used
	Click Element                    css=#formMaxNumOfUsesTypeExpand
	Click Element                    xpath=//ul[@id='ui-id-4']//a[text()='Specify']
	Wait Until Element Is Visible    css=#formMaxNumOfUses    timeout=3
	Clear Element Text               css=#formMaxNumOfUses
	Input Text                       css=#formMaxNumOfUses    ${number_of_uses}

Select One Type Of Display: "${available_for}" And "${space_range}"
	# This Keyword is used to select the type of display the coupon requires to be
	# ${available_for} - type of display (Pay and Display/Pay by License Plate or Pay By Space)
	# ${space_range} - will only be a range of integers if Pay by Space is selected
	Click Element      xpath=//label[contains(text(),'${available_for}')]//a
	Run Keyword If     '${available_for}'=='Pay By Space'    Input SpaceRange Info: "${space_range}"

Upload File In Coupon: "${file}"
	# This Keyword uploads the file to be imported in the coupon section
	# ${file} - file that is to be uploaded into the coupon section
	Wait Until Element Is Visible    css=#formCouponFile    timeout=3
	Click Element                    css=#formCouponFile
	Wait For Active Window			File Upload    ${EMPTY}    
	Win Activate					File Upload    ${EMPTY}	
	Send    						C:\\iris\\ems-robot\\Tests\\CustomerAdmin\\customerAdmin.Accounts\\data\\${file}
	Control Click    				File Upload    &Open    [CLASS:Button;INSTANCE:1]    LEFT    1
	Sleep    						1

