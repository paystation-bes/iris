*** Settings ***
# Summary: Creates a new customer account
Library           Selenium2Library
Library     	  AutoItLibrary
Library 		  OperatingSystem
Resource          ../navigationalKeywords/sideBarNav.robot
Resource          ../CustomerAdminAccountsKeywords/CustomerAdmin_Accounts_Customer_Keywords.robot
Resource          ../CustomerAdminAccountsKeywords/CustomerAdmin_Accounts_Coupons_Keywords.robot

*** Keywords ***


Create A Passcard
	# This Keyword creates a new passcard with specific required arguments without any passcards having the same number (no duplicates)
	# [Arguments]:
	# ${number} - the number for the passcard, unique number
	# ${type} - the type of passcard it is (Ex. student card...)
	# ${location/pay_station} - the location that the passcard can be used at
	# ${description} - little details about the passcard and perhaps remarks
	# ${valid_from} - date that it is valid from, if any
	# ${valid_to} - expiration date, if there is any
	# ${restricted_to_single_valid_permit?} - is it restricted to single valid permit
	# ${maximum_usage} - maximum number of times it can be used, if exists

	[arguments]    	${number}    ${type}    ${location/pay_station}    ${description}    ${valid_from}    ${valid_to}
	...             ${restricted_to_single_valid_permit?}    ${grace_period_minutes}    ${maximum_usage}
	#Go To Accounts Section
	Click "Add" In "Passcards"
	Input Passcard Number: "${number}"
	Input Type: "${type}"
	Run Keyword If    '${location/pay_station}'=='${EMPTY}'  					  	Do Nothing
	...			ELSE    															Select "Passcard" Location: "${location/pay_station}"
	Run Keyword If    '${description}'=='${EMPTY}'    								Do Nothing
	...			ELSE    															Input Passcard Description: "${description}"
	Run Keyword If    '${valid_from}'=='${EMPTY}' and '${valid_to}'=='${EMPTY}'    	Do Nothing
	...			ELSE    															Input Valid From: "${valid_from}" To "${valid_to}"
	Run Keyword If    '${restricted_to_single_valid_permit?}'=='YES'    			Enter Grace Period: "${grace_period_minutes}"
	Run Keyword If    '${maximum_usage}'=='${EMPTY}' 								Do Nothing
	...			ELSE    															Input Maximum Usage: "${maximum_usage}"
	Click Save Passcard/Update Changes
	# Wait Until Element Is Visible    xpath=//p[contains(text(),'${number}')]/../following-sibling::div[1]//p[contains(text(),'${type}')]

Delete All Passcards
	Click "Delete all" In "Passcards"
	Page Should Contain              Are you sure you would like to delete all Passcard(s)?
	Click Element                    xpath=//span[text()='Ok']
	Wait Until Element Is Visible    xpath=//li[text()='Not available']    timeout=3
	Sleep    2

Export A Passcard File
	# This Keyword exports all the coupons into a file
	# ${open_or_save} - Choose between open or save the file onto your computer
	Click "Export" In "Passcards"
	Send     {ENTER}
	Sleep    3

Import A Passcard File
	# This Keyword imports a passcard file and will only work if the file imported is in the correct formt as a .csv file
	# [Arguments]:
	# ${file} - the file containing the information of the list of passcards
	# ${import_mode} - the way you want the new file to interact with the old records, to either merge it with the existing
	# records or to replace the old records with the new file

	[arguments]    ${file}    ${import_mode}
	Click "Import" In "Passcards"
	Upload File In Passcard: "${file}"
	Select Passcard Import Mode: "${import_mode}"
	Click Upload New List

====================================================================================================================================================================
# Minor, low level keywords specfic to these files
--------------------------------------------------------------------------------------------------------------------------------------------------------------------

Attention, Invalid Date Format
	Page Should Contain    is not a valid date (date pattern "MM/DD/YYYY").

Enter Grace Period: "${grace_period_minutes}"
	# This Keyword inputs the grace period in terms of minutes
	# ${grace_period_minutes} - number of minutes that the grace period allows
	Click Element    					css=.checkBox
	Wait Until Element Is Visible    	css=#formGracePeriod
	Input Text     						css=#formGracePeriod		${grace_period_minutes}

Input Maximum Usage: "${maximum_usage}"
	# This Keyword inputs the maximum usage for the passcard
	Clear Element Text    css=#formMaxNumOfUses
	Input Text            css=#formMaxNumOfUses    ${maximum_usage}
	
Input Passcard Description: "${description}"
	# This Keyword inputs the description field in the passcode
	# ${description} - the description field of the passcode section
	Clear Element Text   		css=#formComment
	Input Text    				css=#formComment    ${description}

Input Passcard Number: "${number}"
	# This Keyword inputs the passcard number 
	# ${number} - passcard number
	Clear Element Text    css=#formCardNumber
	Input Text    		  css=#formCardNumber	 ${number}

Input Type: "${type}"
	# This Keyword inputs the type of passcard that is created or can be updated as well
	# ${type} - type of passcard 
	Click Element    				 css=#formCardTypeExpand
	Wait Until Element Is Visible    xpath=//ul[@id='ui-id-2']//a[text()='${type}']    timeout=3
	Click Element  					 xpath=//ul[@id='ui-id-2']//a[text()='${type}']

Input Valid From: "${valid_from}" To "${valid_to}"
	#${valid_from} must be in the format of MM/DD/YYYY
	#${valid_to} must be in the format of MM/DD/YYYY
	Clear Element Text    css=#formValidFrom
	Input Text            css=#formValidFrom    ${valid_from}
	Clear Element Text    css=#formValidTo
	Input Text            css=#formValidTo   ${valid_to}

Passcards Should Be Visible From File
	Wait Until Element Is Visible		xpath=//p[contains(text(),'22222')]			timeout=3
	Wait Until Element Is Visible		xpath=//p[contains(text(),'6521')]			timeout=3
	Wait Until Element Is Visible		xpath=//p[contains(text(),'121212')]		timeout=3
	Wait Until Element Is Visible		xpath=//p[contains(text(),'321321')]		timeout=3

Select Passcard Location/Pay Station: "${location}"
	# This Keyword selects the location and or pay station that will be inspected
	# ${location} - the location or pay station in the passcard tab
	Click Element    xpath=//ul[@id='ui-id-3']//a[text()='${location}']

Upload File In Passcard: "${file}"
	# This Keyword uploads the file to be imported in the coupon section
	# ${file} - file that is to be uploaded into the coupon section
	Wait Until Element Is Visible    css=#formFile    timeout=3
	Click Element                    css=#formFile
	Wait For Active Window			File Upload    ${EMPTY}    
	Win Activate					File Upload    ${EMPTY}	
	Send    						C:\\iris\\ems-robot\\Tests\\CustomerAdmin\\customerAdmin.Accounts\\data\\${file}
	Control Click    				File Upload    &Open    [CLASS:Button;INSTANCE:1]    LEFT    1
	Sleep    						1

