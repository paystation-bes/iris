*** Settings ***
Documentation     A resource file with reusable keywords for general purposes
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Resource          ../../data/data.robot





*** Keywords ***


Close Message Response Alert Box
    Wait Until Element is Visible    css=#messageResponseAlertBox    timeout=3
    Click Element    xpath=//*[@id='messageResponseAlertBox']/section/a[@title='Close']


Navigate to Tab: "${tab_title}"  
	Click Element    xpath=.//a[@title='${tab_title}']

Alert Message Should Appear
    Element Should Be Visible    xpath=.//*[@id='messageResponseAlertBox']/section/h4/strong[contains(text(), 'Attention!')]
    
Log Out
	Wait Until Element Is Visible    xpath=//a[@title='Logout']    timeout=3
	Click Element					 xpath=//a[@title='Logout']