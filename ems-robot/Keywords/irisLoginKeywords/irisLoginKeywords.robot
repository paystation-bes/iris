*** Settings ***
Documentation     A resource file with reusable keywords related to Iris Login
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
...               Author: Johnson N
...               Version Number: 1.0
Library           Selenium2Library
Resource          ../../data/data.robot


*** Keywords ***
# Below are generic Keywords
Login as Generic Customer
    [Arguments]    ${username}    ${password}
    # This Keyword opens up the browser to Iris Login page and attempts to login to a customer account
    # [Arguments]
    # ${username} - The username (not to be confused with customer/company name) of the account
    # ${password} - The current password for the account
    Open Browser To Iris Login Page
    Input Username    ${username}
    Input Password    ${password}
    Submit Credentials
    ${title}=    Get Title
    Run Keyword if    '${title}' == 'Digital Iris - Service Agreement'    Run Keywords    Fill in Service Agreement Default
    Sleep    1
    ${title}=    Get Title
    Run Keyword if    '${title}' == 'Digital Iris - Change Password'    Run Keywords    Enter New Password Defaults

Login as System Admin
    [Arguments]    ${username}    ${password}    ${default_password}
    # This Keyword opens up the browser to Iris Login page and attempts to login to a system admin account
    # [Arguments]
    # ${username} - The username (not to be confused with customer/company name) of the account
    # ${password} - The current password for the account
    # ${default_password} - The default password, if logging in using the default database, usually is 'password'
    Open Browser To Iris Login Page
    Input Username    ${username}
    Input Password    ${password}
    Submit Credentials
    ${title}=    Get Title
    Run Keyword if    '${title}' != 'Digital Iris - System Administration'    Run Keywords    Go To Login Page    AND    Input Username    ${username}    AND    Input Password    ${default_password}    AND    Submit Credentials    AND    Enter New Password    ${password}
    System Admin Welcome Page Should Be Open


Fill in Service Agreement Default
    # A default version of calling Fill in Service Agreement with, designed to fill in service agreement with default values
    Fill in Service Agreement with    NameSampleText    TitleSampleText    LegalNameSampleText

Fill in Service Agreement with
    # Fills in Service Agreement Form that opens up after login to a new customer
    # [Arguments]
    # ${name} - The name of the customer
    # ${title} - The title of the customer ex: Ms./Mr.
    # ${legal_name} - The legal name of the customer/organization
    [Arguments]   ${name}    ${title}    ${legal_name}
    Execute Javascript    var textarea = document.getElementById('saDetails'); textarea.scrollTop = textarea.scrollHeight
    Sleep    1
    Input Text    css=#name    ${name}
    Input Text    css=#title    ${title}
    Input Text    css=#organization    ${legal_name}
    Click Button    saAccept

Enter New Password
    # Method to set new user password in the Change Password form 
    # [Arguments]:
    # ${new_password} - The new password for the user
    [Arguments]    ${new_password}
    Input Text    css=#newPassword    ${new_password}
    Input Text    css=#c_newPassword    ${new_password}
    Click Element    css=#changePassword

Enter New Password Defaults
    # default for Enter New Password. Will fill password with Password$1
    Enter New Password    Password$1

# Minor low-level keywords, meant to translate selenium keywords of certain elements into a natural, easy-to-understand human language

Customer Admin Welcome Page Should Be Open
    # This keyword verifies that the browser is opened to the customer admin page by checking the URL and title. Should pass if on Main (Dashboard) page, and fail otherwise
    Location Should Be    ${CUSTOMER ADMIN URL}
    Title Should Be    Digital Iris - Main(Dashboard)

Go To Login Page
    # Not to be confused with 'Open Browser to Iris Login Page', this keyword will navigate browser to the Iris Login Page.
    Go To    ${LOGIN_URL}
    Login Page Should Be Open

Go To System Admin Page
    # This keyword will navigate browser to the Iris System Admin Page
    Go To    ${SYSTEM_ADMIN_URL}
    System Admin Welcome Page Should Be Open

Input Username
    # This keyword will fill in the username text field with ${username}
    # [Arguments]:
    # ${username} - The Username to input
    [Arguments]    ${username}
    Input Text    css=#username   ${username}

Input Password
    # This keyword will fill in the password text field with ${password}
    # [Arguments]:
    # ${password} - The password to input
    [Arguments]    ${password}
    Input Text     css=#password    ${password}

Login Page Should Be Open
    # This keyword verifies that the browser is opened to the iris login page by checking the title. Should pass if on Login page, and fail otherwise
    Title Should Be    Digital Iris - Login.

Open Browser to Iris Login Page
    # Not to be confused with 'Go To Login Page', this keyword will open up a new browser object and open it to Iris login Page
    Open Browser    url=${LOGIN_URL}    browser=${BROWSER}    ff_profile_dir=${FF_PROFILE_DIR}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}
    Login Page Should Be Open

Submit Credentials
    # This keyword will simply click the button that will submit the username and password to be verified
    Click Button    submit


System Admin Welcome Page Should Be Open
    # This keyword verifies that the browser is opened to the system admin page by checking the URL and title. Should pass if on System Administration page, and fail otherwise
    Location Should Be    ${SYSTEM_ADMIN_URL}
    Title Should Be    Digital Iris - System Administration


