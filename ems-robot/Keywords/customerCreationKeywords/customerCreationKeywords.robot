*** Settings ***
Documentation     A resource file with reusable keywords related to Iris Customer Creation
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
...               Author: Johnson N
...               Version Number: 1.0
Library           Selenium2Library
Resource          ../../data/data.robot
Resource          ../../Keywords/globalKeywordDirectory.robot


*** Keywords ***
# Below are generic Keywords
Create Generic Customer
    # This keyword will create a generic customer with status 'Enabled' and timezone set to 'Canada/Pacific' and all services enabled
    # [Arguments]:
    # ${customer} - The name of the customer
    # ${username} - The username for the customer that will be used for logging into with Iris.
    # ${password} - The default password that a user will use for first-time login
    [Arguments]    ${customer}    ${username}    ${password}
    @{SERVICES}    Create List    3rd Party Pay-By-Cell Integration    Alerts    AutoCount Integration    Coupons    Smart Cards    Batch Credit Card Processing    Digital API: Read    Digital API: Write    Digital API: XChange    Extend-By-Phone    FLEX Integration    Mobile: Digital Collect    Online Pay Station Configuration    Passcards    Real-Time Campus Card Processing    Real-Time Credit Card Processing    Standard Reports
    Open New Customer Form
    Fill In Required Customer Fields    ${customer}    ${username}   ${password}    ${password}    Enabled    ${EMPTY}    Canada/Pacific
    Select Services: "${SERVICES}"
    Saved Customer Creation
    Reload Page

# Below are keywords for the CustomerCreation script

Child: "${child}" Assigned To Parent "${parent}" Should Be Created
    # This keyword verifies that child is assigned to the parent customer, by entering the child in the search bar and checking in the search results for an existing entry and the assigned parent. It then searches for the customer and on the workspace, opens the Update Customer Form to verify that the parent customer is assigned to the customer
    # [Arguments]:
    # ${child} - name of child customer to verify
    # ${parent} - name of parent customer that should be assigned to the child
    Enter Customer Name: "${child}" In Search Bar
    Child Customer: "${child}" With Parent Customer: "${parent}" Should Be Found In Search Bar Results
    Click Customer Name: "${child}" In System Admin Customer Search Bar
    Click Go Button to Search for Customer
    Workspace For Customer: "${child}" Should Appear On System Admin
    Click Edit Customer Button
    Update Customer Form Should Appear
    Element Should Be Visible    xpath=.//*[@id='prntCustomerAC' and @value='${parent}']

Customer: "${customer}" With Services: "${services}" Should Be Created
    # This keyword will search up the customer and navigate to the dashboard and on that dashboard, verifies that the services listed are activated for that customer
    # [Arguments]:
    # ${customer} - the name of the customer to verify
    # ${services} - the list of all services that the customer should have enabled
    Search Up Customer: "${customer}"
    Following Services Should Be Activated: "${services}"


I Create Customer With Name: "${customer}" And Password: "${default_password}" And Status: "${account_status}"
    # This keyword will open up the New Customer Form and fill in the values. This does NOT include saving the customer details. This keyword is normally called along 'Saved Customer Creation' keyword
    # [Arguments]:
    # ${customer} - the name of the new customer to create. Do not confuse this with username
    # ${default_password}  - The default password that a user will use for first-time login
    # ${account_status} - The account status for new customer, either 'Enabled', 'Disabled' or 'Trial'
    Open New Customer Form
    Fill In Required Customer Fields    ${customer}    admin@${customer}    ${default_password}    ${default_password}    ${account_status}    ${EMPTY}    Canada/Pacific

Parent Customer: "${customer}" Should Be Created
    # This keyword will search up the customer and check the search results for an existing entry, which should be labelled '(Parent)'. This keyword also searches the customer and checks the dashboard for a list (possibly empty) of child companies. 
    # [Arguments]:
    # ${customer} - the name of the customer to verify
    Enter Customer Name: "${customer}" In Search Bar
    Wait Until Element is Visible    xpath=.//a[contains(text(), '${customer}')]    timeout=2
    Page Should Contain Element    xpath=.//a[contains(text(), '${customer}')]/span[contains(text(), '(Parent)')]
    Click Customer Name: "${customer}" In System Admin Customer Search Bar
    Click Go Button to Search for Customer
    Workspace For Customer: "${customer}" Should Appear On System Admin
    Page Should Contain Element    css=#childCompanies>header>h2

Search Up Customer: "${customer}"
    # This is a helper keyword, normally used to assist in searching up a normal customer, clicking their name in the search results and navigating to their customer workspace. This keyword should not be used for verifying the creation of customer, whether they'd be child, parent, trial, disabled etc... There are other keywords that will handle that.
    # [Arguments]:
    # ${customer} - the name of the customer to search
    Enter Customer Name: "${customer}" In Search Bar
    Wait Until Element is Visible    xpath=.//a[contains(text(), '${customer}')]    timeout=10
    Click Customer Name: "${customer}" In System Admin Customer Search Bar
    Click Go Button to Search for Customer
    Workspace For Customer: "${customer}" Should Appear On System Admin



# Minor low-level keywords, meant to translate selenium keywords of certain elements into a natural, easy-to-understand human language

Child Customer: "${child}" With Parent Customer: "${parent}" Should Be Found In Search Bar results
    # This keyword verifies that child is assigned to the parent customer, by entering the child in the search bar and checking in the search results for an existing entry and the assigned parent. It then searches for the customer and on the workspace, opens the Update Customer Form to verify that the parent customer is assigned to the customer. Will fail if child does not exist or if not a child, or if parent assignment is wrong.
    # [Arguments]:
    # ${child} - name of child customer to verify
    # ${parent} - name of parent customer that should be assigned to the child
    Wait Until Element is Visible    xpath=.//a[contains(text(), '${child}')]    timeout=2
    Page Should Contain Element    xpath=.//a[contains(text(), '${child}')]/span[contains(text(), '${parent}')]

Click Customer Name: "${customer}" In System Admin Customer Search Bar
    # This is a helper keyword that performs the action of clicking the customer name that should appear in the search results under the search bar.
    # [Arguments]:
    # ${customer} - the name of the customer to search
    Click Element    xpath=.//a[contains(text(), '${customer}')]

Click Edit Customer Button
    # This is a helper keyword that simply clicks the Edit Customer button on the System Admin - Customer Dashboard
    Wait Until Element is Visible    css=#btnEditCustomer    timeout=2
    Click Element    css=#btnEditCustomer

Click Go Button To Search For Customer
    # This is a helper keyword that simply clicks the Go Button next to the Customer Search Bar 
    Click Element    css=#btnGo

Click Update Customer On Update Customer Form
    # This is a helper keyword that simply clicks Update Customer button the update customer form apears
    Click Element    xpath=//section[@id='editCustomerFormArea']/following-sibling::div[1]//span[text()='Update Customer']
    Sleep   3

Close New Customer Form
    # This is a helper keyword that handles pressing the 'Cancel' button on the New Customer form. This also handles the subsequent alert message that pops up and clicks the 'Yes, cancel now' button on the response.
    Click Element    xpath=//span[@class = 'ui-button-text' and contains(text(), 'Cancel')]
    Wait Until Element is Visible    css=#messageResponseAlertBox    timeout=3
    Click Element    xpath=//span[@class = 'ui-button-text' and contains(text(), 'Yes, cancel now')]


Customer: "${customer}" Should Not Appear In Search Bar Results
    # This keyword verifies that after a customer name is entered in the search bar, no customer of that name appears in the search results
    # [Arguments]:
    # ${customer} - the name of the customer to search
    Sleep    1
    Page Should Not Contain Element    xpath=.//a[contains(text(), '${customer}')]

Customer: "${customer}" Should Show Status: "${status}" On System Admin Dashboard
    # This keyword verifies that the customer's status on the system admin dashboard is what it is expected from the specified status. This is done by simply checking the status of the account next to the customer name.
    # [Arguments]:
    # ${customer} - the name of the customer to verify
    # ${status} - the expected status of the customer
    Run Keyword If    '${status}'=='ENABLED'    Page Should Contain Element    xpath=.//*[@id='mainContent' and @class='sysAdminWorkSpace']/h1[contains(text(), '${customer}')]
    Run Keyword If    '${status}'=='DISABLED' or '${status}'=='TRIAL'    Element Should Contain    xpath=.//*[@id='mainContent' and @class='sysAdminWorkSpace']/h1[contains(text(), '${customer}')]/span    ${status}

Enter Customer Name: "${customer}" In Search Bar
    # This is a helper keyword that handles the task of entering the customer name in the search bar
    # [Arguments]:
    # ${customer} - the name of the customer to search
    Clear Element Text    css=#search
    Wait Until Element is Visible    css=#search    timeout=3
    Input Text    css=#search    ${customer}

Enter Default Password As: "${default_password}" And Enter Password Confirmation As: "${confirm_password}"
    # This is a helper keyword that handles the entering of default password information in the New Customer Form. This includes entering the default password as well as it's confirmation. The keyword is written so that default and confirmation does not necessarily have to be the same, or valid passwords for the matter, for testing invalid cases.
    # [Arguments]:
    # ${default_password} - the default password that is entered in the New Customer Form for first-time logins for new customers.
    # ${confirm_password} - the password confirmation that is entered in the New Customer Form for first-time logins for new customers. Should be the same as ${default_password} but for testing invalids, does not have to be
    Clear Element Text    css=#newPassword
    Input Text    css=#newPassword    ${default_password}
    Mouse Over    css=#passwordIcn
    Sleep    1
    ${present}=    Run Keyword and Return Status    Element Should Be Enabled    css=#c_newPassword
    Run Keyword If    ${present}    Clear Element Text    css=#c_newPassword
    Run Keyword If    ${present}    Input Text    css=#c_newPassword    ${confirm_password}


Enter Parent Customer Name: "${parent}" For Child Customer Creation
    # This is a helper keyword that handles the entering of parent customer name by inputting the name into the textbox, as opposed to clicking the dropdown and selecting.
    # [Arguments]:
    # ${parent} - the name of the parent to assign to the customer
    Input Text    css=#prntCustomerAC    ${parent}
    Sleep    1
    Press Key    css=#prntCustomerAC    \\13

Fill In Required Customer Fields
    # This is a helper keyword that handles the entering of all the required customer information for creating new customer. This does not handle any service selection.
    # [Arguments]:
    # ${customer} - The name of the customer 
    # ${admin} - The username for the customer that will be used for logging into with Iris. 
    # ${default_password} - the default password that is entered in the New Customer Form for first-time logins for new customers. 
    # ${confirm_password} - the password confirmation that is entered in the New Customer Form for first-time logins for new customers. Should be the same as ${default_password} but for testing invalids, does not have to be 
    # ${account_status} - The account status for new customer, either 'Enabled', 'Disabled' or 'Trial 
    # ${trial_end_date} - the trial end date for trial customer, should be set to ${EMPTY} by default 
    # ${timezone} - the customer's time zone
    [Arguments]    ${customer}    ${admin}    ${default_password}    ${confirm_password}    ${account_status}    ${trial_end_date}    ${timezone}
    Input New Customer Name: "${customer}" On Creation Form
    Sleep    3
    Select Timezone As "${timezone}"
    Input Account Status As: "${account_status}"
    Run Keyword if    '${account_status}' == 'Trial'    Set Trial End Date As: "${trial_end_date}"
    Input New Customer Username For Login As: "${admin}"
    Enter Default Password As: "${default_password}" And Enter Password Confirmation As: "${confirm_password}"

Following Services Should Be Activated: "${services}"
    # This is  a helper keyword that handles the verification that the services listed are activated for that customer by checking the dashboard. This does not handle the navigation to the customer dashboard that lists all the activated services
    # [Arguments]:
    # ${services} - a reference to a list of services
     :FOR    ${service}    IN    @{services}
    \    Page Should Contain Element    xpath=.//li[@title='${service} is Activated']

Following Services Should Be Disabled: "${services}"
    # This is  a helper keyword that handles the verification that the services listed are activated for that customer by checking the dashboard. This does not handle the navigation to the customer dashboard that lists all the activated services
    # [Arguments]:
    # ${services} - a reference to a list of services
     :FOR    ${service}    IN    @{services}
    \    Page Should Contain Element    xpath=.//li[@title='${service} is Available']

Input New Customer Name: "${customer}" On Creation Form 
    # This is a helper keyword that handles the entering of customer name by inputting the name into the Customer Name textbox. Do not confuse this with 'Input New Customer Username For Login As: "${username}"'
    # [Arguments]:
    # ${customer} - The name of the customer
    Clear Element Text    css=#formCustomerName
    Input Text    css=#formCustomerName    ${customer}

Input New Customer Username For Login As: "${username}"
    # This is a helper keyword that handles the entering ofuser name by inputting the name into the Admin User Name textbox. Do not confuse this with 'Input New Customer Name: "${customer}" On Creation Form '
    # [Arguments]:
    # ${username}  - The username for the customer that will be used for logging into with Iris.
    Clear Element Text    css=#formUserName
    Input Text    css=#formUserName    ${username} 

Input Account Status As: "${account_status}"
    # This is a helper keyword that handles the clicking of the account status on the New Customer Form, assuming the account status specified is valid. If account status is invalid or left blank, it'll simply do nothing.
    # [Arguments]:
    # ${account_status} - The account status for new customer, either 'Enabled', 'Disabled' or 'Trial, but does not necessarily have to be, for invalid purposes
    Run Keyword If    '${account_status}' == 'Enabled' or '${account_status}' == 'Disabled' or '${account_status}' == 'Trial'    Wait Until Element is Visible    xpath=.//*[@id='statusOptionArea']/label[contains(text(), '${account_status}')]    timeout=2
    Run Keyword If    '${account_status}' == 'Enabled' or '${account_status}' == 'Disabled' or '${account_status}' == 'Trial'    Click Element    xpath=.//*[@id='statusOptionArea']/label[contains(text(), '${account_status}')]
    Run Keyword If    '${account_status}' == 'Enabled' or '${account_status}' == 'Disabled' or '${account_status}' == 'Trial'    Click Element    xpath=.//*[@id='statusOptionArea']/label[contains(text(), '${account_status}')]

Open New Customer Form
    # This is a helper keyword that handles the clicking of the button that will open up the New Customer Form on the main System Admin page.
    Wait Until Element is Visible    css=#btnAddNewCustomer    timeout=3
    Click Element    css=#btnAddNewCustomer
    Wait Until Element is Visible    newCustomerFormArea    timeout=2

Parent Customer: "${customer}" Should Be Found In Search Bar Results
    # This keyword verifies that the customer entered in the search bar returns a result that is a parent customer with that customer name. Will fail if customer does not exist or is not a parent .This does not handle the entering of the customer name in the search bar.
    # [Arguments]:
    # ${customer} - the name of the customer to search
    Wait Until Element is Visible    xpath=.//a[contains(text(), '${customer}')]    timeout=2
    Page Should Contain Element    xpath=.//a[contains(text(), '${customer}')]/span[contains(text(), '(Parent)')]

Saved Customer Creation
    # This is a helper keyword that handles clicking the 'Add Customer' button that would attempt to save the newly created customer
    Click Element    xpath=//span[@class = 'ui-button-text' and contains(text(), 'Add Customer')]

Select Parent Flag
    # This is a helper keyword that handles the selecting the Parent Flag of the New Customer Form to set the new customer as a Parent.
    Wait Until Element Is Visible    css=#parentFlagCheck   timeout=3
    Click Element    css=#parentFlagCheck 

Select Services: "${services}"
    # This is a helper keyword that handles the selecting of services specified in the list for a new customer. All services in the list should have the exact name of the services on the list provided on the New Customer Form. Keep in mind that it is case-sensitive as well.
    # [Arguments]:
    # ${services} - a reference to a list of services to be selected
    :FOR    ${service}    IN    @{services}
    \    Click Element    xpath=//label/text()[normalize-space() = '${service}']/../a

Select Timezone As "${timezone}"
    # This is a helper keyword that handles the selecting of the time zone for the new customer, by entering the timezone into the text field, as opposed to selecting from the dropdown menu.
    # [Arguments]:
    # ${timezone} - the customer's time zone 
    Wait Until Element is Visible    css=#selectTimeZone    timeout=3
    Clear Element Text    css=#selectTimeZone
    Input Text    selectTimeZone    ${timezone}
    Sleep    1
    Press Key    selectTimeZone    \\13

Set Trial End Date As: "${mm/dd/yyyy}"
    # This is a helper keyword that handles the entering of the trial end date for trial customers, by manually typing the date into the text field, as opposed to selecting from the calendar widget
    # [Arguments]:
    # ${mm/dd/yyyy} - the trial end date,  written in the format mm/dd/yyyy
    Clear Element Text    css=#trialEndDat
    Input Text    css=#trialEndDat    ${mm/dd/yyyy}

Timezone Setting Is "${timezone}"
    # This keyword verifies that the time zone for a customer is what is expected b checking the time zone shown on the system admin dashboard. This keyword does not handle the searching of the customer being verified.
    # [Arguments]:
    # ${timezone} - the customer's expected time zone
    Run Keyword If    '${timezone}'=='Choose'    Element Text Should Be    xpath=.//dt[contains(text(), 'Current Time Zone:')]/following-sibling::dd    GMT
    Run Keyword Unless    '${timezone}'=='Choose'    Element Text Should Be    xpath=.//dt[contains(text(), 'Current Time Zone:')]/following-sibling::dd    ${timezone}

Trial Date Is "${expected}"
    # This keyword verifies that the trial end date for a trial customer is what is expected, by clicking the Edit Customer button and checking the trial end date on the Edit Customer form. This keyword does not handle the searching of the customer being verified.
    # [Arguments]:
    # ${expected} - the expected trial end date, written in the format mm/dd/yyyy
    Click Edit Customer Button
    Update Customer Form Should Appear
    Wait Until Element is Visible    trialEndDat    timeout=2
    Element Should Be Visible    xpath=.//*[@id='trialEndDat' and @value='${expected}']
    Reload Page

Update Customer Form Should Appear
    # This keyword simply verifies that the Update Customer Form is visible after clicking the Edit Customer button for a customer on System Admin. This keyword does not handle the clicking of the Edit Customer Button that brings up the form.
    Wait Until Element is Visible    xpath=.//span[contains(text(), 'Update Customer')]    timeout=2

Workspace For Customer: "${customer}" Should Appear On System Admin
    # This keyword verifies that the main workspace for a customer is visible, most likely after searching for the specified customer name. This keyword does not handle the actual searching of the customer or running the search. 
    # [Arguments]:
    # ${customer} - the name of the customer to verify
    Wait Until Element is Visible    xpath=.//*[@id='mainContent' and @class='sysAdminWorkSpace']/h1[contains(text(), '${customer}')]    timeout=5









    







