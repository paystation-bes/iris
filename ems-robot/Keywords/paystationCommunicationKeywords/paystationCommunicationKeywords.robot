*** Settings ***
Documentation     A resource file with reusable keywords for paystation communication purposes, including sending heartbeats, alerts, transactions etc...
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported http library.
Library           HttpLibrary.HTTP
Library           String
Resource          ../../data/data.robot
Library           Collections
Library           DateTime
*** Keywords ***

# Note: For Sending ALL XMLRPC messages to Iris, this is the general flow
# First, set up Context by using Create HTTP Context and putting in the server url (ex.qa3.digitalpaytech.com) and protocol (http or https) as arguments. 
# Second, construct the XML body that you wish to send
# Third, set the request header using the Set Request Header and Set Request Body keywords
# Fourth, send the post request using POST with /XMLRPC_PS2 
# Finally, verify the response returned using Get Response Status
# Note: With the exception of setting up the request body initially, you need to pass the request body you're constructing through a series of keywords of appending tags to make the request

#TODO: We need to figure out how to divide the scripts and keywords for XMLRPC as it is going to be large. We should also decide how the request body is going to be constructed and how that work will be divided (i.e. having keywords that will append one tag to the request body and have one for each known tag or having keywords with a series of tags that can be set up). This becomes especially true when dealing with cash/card transactions where users can add optional tags like <PaidByRFID> to cover more areas. Also, current scripts may need some re-factoring. Current scripts purpose is to show that Robot can send XMLRPC to Iris. The formatting is subjected to change. Documentation for now will remain light and solely on this file until the details are finalized.


Send "${dollar}" Cash Transaction To Paystation: "${paystation_id}"
	# This will send a simple cash transaction of amount ${dollars} (written in format X.XX) to paystation id specified in ${paystation_id}. This keyword currently generates a GMT Date String Of Current Time as well as its expiry time according to permit time (currently hard coded). It throws in some hard-coded values in the xml request on top of some auto-generated variables. Further discussion needed to decide what values should be hard-coded or turn into variables
	Create HTTP Context	    ${TEST_SERVER_URL}    https
	${gmt_date_string}=    Generate GMT Date String Of Current Time
	${gmt_expiry_date_string}=    Generate GMT Expiry Date String By Adding Current Time By "30 minutes" 
	${message_number}=    Generate Random Message Number
	${original_amount}=    Replace String    ${dollar}    .    ${EMPTY}
	${body}=    Append Header to Request Body
	${body}=    Append Request Body For Cash Transaction    ${body}    ${message_number}    ${paystation_id}    ABC678    10    Regular    ${gmt_date_string}    ${original_amount}    ${dollar}    ${dollar}    false    ${gmt_expiry_date_string}
	${body}=    Append Footer To Request Body: "${body}"
	Set Request Header And Body "${body}" For Paystation Communication
	POST	/XMLRPC_PS2
	Verify The Response


Send Cash Transaction To Paystation
    [arguments]  ${type}  ${count1}  ${count2}  ${count3}  ${count4}  ${serial}
# This Keyword sends either Bill, or Coin transaction to the specified paystation
# Arguments:
# ${type} Bill or Coin
#
# If ${type} is Bill
#   ${count1} = # of $5 bills
#   ${count2} = # of $10 bills
#   ${count3} = # of $20 bills
#   ${count4} = # of $50 bills
#
# If ${type} is Coin
#   ${count1} = # of $0.05 Coins
#   ${count2} = # of $0.10 Coins
#   ${count3} = # of $0.25 Coins
#   ${count4} = # of $1.00 Coins

    #Should Be True  '${type}'=='Bill' or '${type}'=='Coin'

    ${charged_amount}=  Calculate Total Cash Amount  ${type}  ${count1}  ${count2}  ${count3}  ${count4}
    ${charged_amount}=  Convert To String  ${charged_amount}
    ${charged_amount}=  Catenate  SEPARATOR=  ${charged_amount}  0
    ${original_amount}=  Convert To String  ${charged_amount}
    ${original_amount}=  Replace String  ${charged_amount}  .  ${EMPTY}

    Log To Console  Charged Amount Is: ${charged_amount}
    Log To Console  Original Amount Is: ${original_amount}


    Create HTTP Context	    ${TEST_SERVER_URL}    https
 	${gmt_date_string}=    Generate GMT Date String Of Current Time
	${gmt_expiry_date_string}=    Generate GMT Expiry Date String By Adding Current Time By "30 minutes"
	${message_number}=    Generate Random Message Number
    ${body}=    Append Header to Request Body
    Run Keyword If  '${type}'=='Coin'  Append Coin Transaction Body  ${body}  ${message_number}  ${serial}  ABC123  ${gmt_date_string}  ${original_amount}  ${charged_amount}  ${count1}  ${count2}  ${count3}  ${count4}  ${gmt_expiry_date_string}
    Run Keyword If  '${type}'=='Bill'  Append Bill Transaction Body  ${body}  ${message_number}  ${serial}  ABC123  ${gmt_date_string}  ${original_amount}  ${charged_amount}  ${count1}  ${count2}  ${count3}  ${count4}  ${gmt_expiry_date_string}
    ${body}=    Append Footer To Request Body: "${body}"
    Set Request Header And Body "${body}" For Paystation Communication
    Log To Console  ${body}
    POST	/XMLRPC_PS2
	Verify The Response




Calculate Total Cash Amount
# Calculates the total amount given the type of cash the different counts
# Returns the total cash amount
    [arguments]  ${type}  ${count1}  ${count2}  ${count3}  ${count4}

    Should Be True  '${type}'=='Bill' or '${type}'=='Coin'
    Run Keyword If  '${type}'=='Bill'  Calculate Bill Amount  ${count1}  ${count2}  ${count3}  ${count4}
    run keyword If  '${type}'=='Coin'  Calculate Coin Amount  ${count1}  ${count2}  ${count3}  ${count4}
    Log To Console  Returning From Calculate Total Cash Amount: ${amount}
    [return]  ${amount}

Calculate Bill Amount
# Calculates the total amount given the counts of each bill
    [arguments]  ${count1}  ${count2}  ${count3}  ${count4}
    ${result}=  Evaluate  ${count1}*5.00
    ${result}=  Evaluate  ${result}+(${count2}*10.00)
    ${result}=  Evaluate  ${result}+(${count3}*20.00)
    ${result}=  Evaluate  ${result}+(${count4}*50.00)
    Log To Console  Total Bill Amount Is: ${result}
    Set Test Variable  ${amount}  ${result}

Calculate Coin Amount
# Calculates the total amount given the counts of each Coin
    [arguments]  ${count1}  ${count2}  ${count3}  ${count4}
    ${result}=  Evaluate  ${count1}*0.05
    ${result}=  Evaluate  ${result}+(${count2}*0.10)
    ${result}=  Evaluate  ${result}+(${count3}*0.25)
    ${result}=  Evaluate  ${result}+(${count4}*1.00)
    Log To Console  Total Coin Amount Is: ${result}
    Set Test Variable  ${amount}  ${result}

Append Coin Transaction Body
	# this keyword will append to the request body all the tags required for basic cash transaction. Further discussion needed on whether or not to divide the appending of the tags into seperate keywords as well as what can be variable and what can stay hard-coded. This keyword will return the body with the newly-appended tags.
	[Arguments]     ${body}    ${message_num}    ${paystation_id}    ${license_num}    ${gmt_date_string}    ${original_amount}    ${charged_amount}  ${count1}  ${count2}  ${count3}  ${count4}    ${gmt_exp_date_string}
	${body}=    Catenate    SEPARATOR=    ${body}    <Transaction MessageNumber="${message_num}">
	${body}=    Catenate    SEPARATOR=    ${body}    <PaystationCommAddress>${paystation_id}</PaystationCommAddress>
	${body}=    Catenate    SEPARATOR=    ${body}    <Number>${message_num}</Number>
	${body}=    Catenate    SEPARATOR=    ${body}    <LotNumber>EMS 6.3.11.22</LotNumber>
	${body}=    Catenate    SEPARATOR=    ${body}    <LicensePlateNo>${license_num}</LicensePlateNo>
	${body}=    Catenate    SEPARATOR=    ${body}    <Type>Regular</Type>
	${body}=    Catenate    SEPARATOR=    ${body}    <PurchasedDate>${gmt_date_string}</PurchasedDate>
	${body}=    Catenate    SEPARATOR=    ${body}    <ParkingTimePurchased>30</ParkingTimePurchased>
	${body}=    Catenate    SEPARATOR=    ${body}    <OriginalAmount>${original_amount}</OriginalAmount>
	${body}=    Catenate    SEPARATOR=    ${body}    <ChargedAmount>${charged_amount}</ChargedAmount>
	${body}=    Catenate    SEPARATOR=    ${body}    <CouponNumber></CouponNumber>
	${body}=    Catenate    SEPARATOR=    ${body}    <CashPaid>${charged_amount}</CashPaid>
	${body}=    Catenate    SEPARATOR=    ${body}    <CardPaid></CardPaid>
	${body}=    Catenate    SEPARATOR=    ${body}    <CardData></CardData>
	${body}=    Catenate    SEPARATOR=    ${body}    <IsRefundSlip>false</IsRefundSlip>
	${body}=    Catenate    SEPARATOR=    ${body}    <ExpiryDate>${gmt_exp_date_string}</ExpiryDate>
	${body}=    Catenate    SEPARATOR=    ${body}    <CoinCol>5:${count1},10:${count2},25:${count3},100:${count4}</CoinCol>
	${body}=    Catenate    SEPARATOR=    ${body}    <Tax1Rate>7.24</Tax1Rate>
	${body}=    Catenate    SEPARATOR=    ${body}    <Tax1Value>1</Tax1Value>
	${body}=    Catenate    SEPARATOR=    ${body}    <Tax1Name>GST</Tax1Name>
	${body}=    Catenate    SEPARATOR=    ${body}    <Tax2Rate>5.00</Tax2Rate>
	${body}=    Catenate    SEPARATOR=    ${body}    <Tax2Value>1.00</Tax2Value>
	${body}=    Catenate    SEPARATOR=    ${body}    <Tax2Name>PST</Tax2Name>
	${body}=    Catenate    SEPARATOR=    ${body}    <LicensePlateNo>${license_num}</LicensePlateNo>
	${body}=    Catenate    SEPARATOR=    ${body}    <RateName>early bird</RateName>
	${body}=    Catenate    SEPARATOR=    ${body}    <RateID>1</RateID>
	${body}=    Catenate    SEPARATOR=    ${body}    <RateValue>400</RateValue>
	${body}=    Catenate    SEPARATOR=    ${body}    <TaxType>N/A</TaxType>
	${body}=    Catenate    SEPARATOR=    ${body}    </Transaction>
	#[return]    ${body}
    Set Test Variable  ${body}  ${body}
Append Bill Transaction Body
	# this keyword will append to the request body all the tags required for basic cash transaction. Further discussion needed on whether or not to divide the appending of the tags into seperate keywords as well as what can be variable and what can stay hard-coded. This keyword will return the body with the newly-appended tags.
	[Arguments]     ${body}    ${message_num}    ${paystation_id}    ${license_num}    ${gmt_date_string}    ${original_amount}    ${charged_amount}  ${count1}  ${count2}  ${count3}  ${count4}    ${gmt_exp_date_string}
	${body}=    Catenate    SEPARATOR=    ${body}    <Transaction MessageNumber="${message_num}">
	${body}=    Catenate    SEPARATOR=    ${body}    <PaystationCommAddress>${paystation_id}</PaystationCommAddress>
	${body}=    Catenate    SEPARATOR=    ${body}    <Number>${message_num}</Number>
	${body}=    Catenate    SEPARATOR=    ${body}    <LotNumber>EMS 6.3.11.22</LotNumber>
	${body}=    Catenate    SEPARATOR=    ${body}    <LicensePlateNo>${license_num}</LicensePlateNo>
	${body}=    Catenate    SEPARATOR=    ${body}    <Type>Regular</Type>
	${body}=    Catenate    SEPARATOR=    ${body}    <PurchasedDate>${gmt_date_string}</PurchasedDate>
	${body}=    Catenate    SEPARATOR=    ${body}    <ParkingTimePurchased>30</ParkingTimePurchased>
	${body}=    Catenate    SEPARATOR=    ${body}    <OriginalAmount>${original_amount}</OriginalAmount>
	${body}=    Catenate    SEPARATOR=    ${body}    <ChargedAmount>${charged_amount}</ChargedAmount>
	${body}=    Catenate    SEPARATOR=    ${body}    <CouponNumber></CouponNumber>
	${body}=    Catenate    SEPARATOR=    ${body}    <CashPaid>${charged_amount}</CashPaid>
	${body}=    Catenate    SEPARATOR=    ${body}    <CardPaid></CardPaid>
	${body}=    Catenate    SEPARATOR=    ${body}    <CardData></CardData>
	${body}=    Catenate    SEPARATOR=    ${body}    <IsRefundSlip>false</IsRefundSlip>
	${body}=    Catenate    SEPARATOR=    ${body}    <ExpiryDate>${gmt_exp_date_string}</ExpiryDate>
    ${body}=    Catenate    SEPARATOR=    ${body}    <BillCol>5:${count1},10:${count2},20:${count3},50:${count4}</BillCol>
	${body}=    Catenate    SEPARATOR=    ${body}    <Tax1Rate>7.24</Tax1Rate>
	${body}=    Catenate    SEPARATOR=    ${body}    <Tax1Value>1</Tax1Value>
	${body}=    Catenate    SEPARATOR=    ${body}    <Tax1Name>GST</Tax1Name>
	${body}=    Catenate    SEPARATOR=    ${body}    <Tax2Rate>5.00</Tax2Rate>
	${body}=    Catenate    SEPARATOR=    ${body}    <Tax2Value>1.00</Tax2Value>
	${body}=    Catenate    SEPARATOR=    ${body}    <Tax2Name>PST</Tax2Name>
	${body}=    Catenate    SEPARATOR=    ${body}    <LicensePlateNo>${license_num}</LicensePlateNo>
	${body}=    Catenate    SEPARATOR=    ${body}    <RateName>early bird</RateName>
	${body}=    Catenate    SEPARATOR=    ${body}    <RateID>1</RateID>
	${body}=    Catenate    SEPARATOR=    ${body}    <RateValue>400</RateValue>
	${body}=    Catenate    SEPARATOR=    ${body}    <TaxType>N/A</TaxType>
	${body}=    Catenate    SEPARATOR=    ${body}    </Transaction>
    #[return]    ${body}
    Set Test Variable  ${body}  ${body}

Send Heartbeat To Paystation: "${paystation_id}"
	# This wil send a simple heartbeat to a paystation.
	Create HTTP Context	    ${TEST_SERVER_URL}    https
	${body}=    Append Header to Request Body
	${body}=    Catenate    SEPARATOR=    ${body}    <HeartBeat MessageNumber="HB">
	${body}=    Catenate    SEPARATOR=    ${body}    <PaystationCommAddress>${paystation_id}</PaystationCommAddress>
	${body}=    Catenate    SEPARATOR=    ${body}    </HeartBeat>
	${body}=    Append Footer To Request Body: "${body}"
	Set Request Header And Body "${body}" For Paystation Communication
    Log To Console  ${body}
	POST	/XMLRPC_PS2
	Verify The Response

Send Events: "${list_of_events}" To Paystation: "${paystation_id}"
	# This will send a list of events (events are dictionary objects) to a paystation by parsing the information to the body xml and incrementing the message number 
	Create HTTP Context	    ${TEST_SERVER_URL}    https
	${message_number}=    Generate Random Message Number
	${gmt_date_string}=    Generate GMT Date String Of Current Time
	${body}=    Append Header to Request Body
	:FOR    ${event}    IN    @{list_of_events}
	\	${type}=    Get From Dictionary    ${event}    type
	\	${action}=    Get From Dictionary    ${event}    action
	\	${information}=    Get From Dictionary    ${event}    information
	\	${body}=    Catenate    SEPARATOR=    ${body}    <Event MessageNumber="${message_number}">
	\	${body}=    Catenate    SEPARATOR=    ${body}    <PaystationCommAddress>${paystation_id}</PaystationCommAddress>
	\	${body}=    Catenate    SEPARATOR=    ${body}    <TimeStamp>${gmt_date_string}</TimeStamp>
	\	${body}=    Catenate    SEPARATOR=    ${body}    <Type>${type}</Type>
	\	${body}=    Catenate    SEPARATOR=    ${body}    <Action>${action}</Action>
	\	${body}=    Catenate    SEPARATOR=    ${body}    <Information>${information}</Information>
	\	${body}=    Catenate    SEPARATOR=    ${body}    </Event>
	\	${message_number}=    Evaluate    ${message_number} + 1
	${body}=    Append Footer To Request Body: "${body}"
	Set Request Header And Body "${body}" For Paystation Communication
	POST	/XMLRPC_PS2
	Verify The Response


Send Event: "${event}" To Paystation: "${paystation_id}" 
	# This will send an event (events are dictionary objects) to a paystation by parsing the information to the body xml 
	${type}=    Get From Dictionary    ${event}    type
	${action}=    Get From Dictionary    ${event}    action
	${information}=    Get From Dictionary    ${event}    information
	Create HTTP Context	    ${TEST_SERVER_URL}    https
	${message_number}=    Generate Random Message Number    
	${gmt_date_string}=    Generate GMT Date String Of Current Time
	${body}=    Append Header to Request Body
	${body}=    Catenate    SEPARATOR=    ${body}    <Event MessageNumber="${message_number}">
	${body}=    Catenate    SEPARATOR=    ${body}    <PaystationCommAddress>${paystation_id}</PaystationCommAddress>
	${body}=    Catenate    SEPARATOR=    ${body}    <TimeStamp>${gmt_date_string}</TimeStamp>
	${body}=    Catenate    SEPARATOR=    ${body}    <Type>${type}</Type>
	${body}=    Catenate    SEPARATOR=    ${body}    <Action>${action}</Action>
	${body}=    Catenate    SEPARATOR=    ${body}    <Information>${information}</Information>
	${body}=    Catenate    SEPARATOR=    ${body}    </Event>
	${body}=    Append Footer To Request Body: "${body}"
	Set Request Header And Body "${body}" For Paystation Communication
	POST	/XMLRPC_PS2
	Log To Console  Sent Event: ${event}
	Log To Console  ${body}
	Verify The Response

#Helper Keywords

Append Footer To Request Body: "${body}"
	# This is a helper keyword. This will append the footer needed for the xml request and return that request body. This should be called last after all the operations to the request body is done.
	${footer}=    Set Variable    </Message4EMS></Paystation_2></value></param></params></methodCall>
	${body}=    Catenate    SEPARATOR=    ${body}    ${footer}
	[return]    ${body}

Append Header to Request Body
	# This is a helper keyword. This will append the header needed for the xml request and return that request body. Since this is the first thing that is needed to setup a request, it does not require an argument since it is going into a new string anyways.
	${body}=    Set Variable    <?xml version="1.0" encoding="ISO-8859-1"?><methodCall><methodName>XMLRPCRequestHandler.processXML</methodName><params><param><value><?xml version="1.0"?><Paystation_2 Password="zaq123$ESZxsw234%RDX" UserId="emsadmin" Version="6.3 build 0013"><Message4EMS>
	[return]    ${body}

Append Request Body For Cash Transaction
	# this keyword will append to the request body all the tags required for basic cash transaction. Further discussion needed on whether or not to divide the appending of the tags into seperate keywords as well as what can be variable and what can stay hard-coded. This keyword will return the body with the newly-appended tags.
	[Arguments]    ${body}    ${message_num}    ${paystation_id}    ${license_num}    ${stall_num}    ${type}    ${gmt_date_string}    ${original_amount}    ${charged_amount}    ${cash_paid}    ${is_refund_slip}    ${gmt_exp_date_string}
	${body}=    Catenate    SEPARATOR=    ${body}    <Transaction MessageNumber="${message_num}">
	${body}=    Catenate    SEPARATOR=    ${body}    <PaystationCommAddress>${paystation_id}</PaystationCommAddress>    
	${body}=    Catenate    SEPARATOR=    ${body}    <Number>${message_num}</Number>
	${body}=    Catenate    SEPARATOR=    ${body}    <LotNumber>EMS 6.3.11.22</LotNumber>
	${body}=    Catenate    SEPARATOR=    ${body}    <LicensePlateNo>${license_num}</LicensePlateNo>
	${body}=    Catenate    SEPARATOR=    ${body}    <AddTimeNum>270000</AddTimeNum>
	${body}=    Catenate    SEPARATOR=    ${body}    <StallNumber>${stall_num}</StallNumber>
	${body}=    Catenate    SEPARATOR=    ${body}    <Type>${type}</Type>
	${body}=    Catenate    SEPARATOR=    ${body}    <PurchasedDate>${gmt_date_string}</PurchasedDate>
	${body}=    Catenate    SEPARATOR=    ${body}    <ParkingTimePurchased>30</ParkingTimePurchased>
	${body}=    Catenate    SEPARATOR=    ${body}    <OriginalAmount>${original_amount}</OriginalAmount>
	${body}=    Catenate    SEPARATOR=    ${body}    <ChargedAmount>${charged_amount}</ChargedAmount>
	${body}=    Catenate    SEPARATOR=    ${body}    <CouponNumber></CouponNumber>
	${body}=    Catenate    SEPARATOR=    ${body}    <CashPaid>${cash_paid}</CashPaid>
	${body}=    Catenate    SEPARATOR=    ${body}    <CardPaid></CardPaid>
	${body}=    Catenate    SEPARATOR=    ${body}    <CardData></CardData>
	${body}=    Catenate    SEPARATOR=    ${body}    <IsRefundSlip>${is_refund_slip}</IsRefundSlip>
	${body}=    Catenate    SEPARATOR=    ${body}    <ExpiryDate>${gmt_exp_date_string}</ExpiryDate>
	${body}=    Catenate    SEPARATOR=    ${body}    <Tax1Rate>7.24</Tax1Rate>
	${body}=    Catenate    SEPARATOR=    ${body}    <Tax1Value>1</Tax1Value>
	${body}=    Catenate    SEPARATOR=    ${body}    <Tax1Name>GST</Tax1Name>
	${body}=    Catenate    SEPARATOR=    ${body}    <Tax2Rate>5.00</Tax2Rate>
	${body}=    Catenate    SEPARATOR=    ${body}    <Tax2Value>1.00</Tax2Value>
	${body}=    Catenate    SEPARATOR=    ${body}    <Tax2Name>PST</Tax2Name>
	${body}=    Catenate    SEPARATOR=    ${body}    <LicensePlateNo>${license_num}</LicensePlateNo>
	${body}=    Catenate    SEPARATOR=    ${body}    <RateName>early bird</RateName>
	${body}=    Catenate    SEPARATOR=    ${body}    <RateID>1</RateID>
	${body}=    Catenate    SEPARATOR=    ${body}    <RateValue>400</RateValue>
	${body}=    Catenate    SEPARATOR=    ${body}    <TaxType>N/A</TaxType>
	${body}=    Catenate    SEPARATOR=    ${body}    </Transaction>
	[return]    ${body}

Generate GMT Expiry Date String By Adding Current Time By "${parking_time_purchased}"
	# This is a helper keyword that generates an expiry date according to parking time purchased and adding that to current time. We do that by grabbing the current time and through a series of time format conversions and additions, return a string in the format: ${year}:${month}:${day}:${expiry_time}:GMT
	${year}    ${month}    ${day}    ${hour}    ${min}    ${sec}=	Get Time	year month day hour min sec    UTC
	${current_time}=    Convert Time      ${hour}:${min}:${sec}    timer    exclude_millis=yes
	${expiry_time}=    Add Time To Time    ${current_time}    ${parking_time_purchased}    exclude_millis=yes
	${expiry_time}=    Convert Time      ${expiry_time}    timer    exclude_millis=yes
	[return]    ${year}:${month}:${day}:${expiry_time}:GMT    

Generate GMT Date String Of Current Time
	# This is a helper keyword that will generate a string of the current date-time in the format: ${year}:${month}:${day}:${expiry_time}:GMT
	${year}    ${month}    ${day}    ${hour}    ${min}    ${sec}=	Get Time	year month day hour min sec    UTC
	${gmt_date_string}=    Catenate    SEPARATOR=:    ${year}	${month}	${day}	${hour}	${min}	${sec}	GMT
	[return]    ${gmt_date_string}

Generate Random Message Number
	# this keyword simply generates and returns a random number between 1 and 1000
	${message_number}=    Evaluate    random.randint(1,1000)    modules=random
	[return]    ${message_number}

Get Byte Length Of String: "${string}"
	# this keyword will return the byte-length of the string. Needed for setting the Request Header.
	${bytes}=    Convert To Bytes    ${string}    text
	${byte_length}=    Get Length    ${bytes}
	[return]    ${byte_length}



Set Request Header And Body "${body}" For Paystation Communication
	# This keyword will Set the Request Header And Body so it's ready to be sent to a paystation
	Set Request Header    content-type    text/xml
	${byte_length}=    Get Byte Length Of String: "${body}"
	Set Request Header    content-length    ${byte_length}
	Set Request Body    ${body}

Verify The Response
	# This keyword will verify the response code returned. Still needs to handle error handling cases as well as UI Checking.
	${status}=    Get Response Status
	# Log To Console    ${status}
	Response Status Code Should Equal    200 OK
	${response_body}=    Get Response Body
	${response_body}=    Replace String    ${response_body}    <?xml version="1.0" encoding="ISO-8859-1"?><methodResponse><params><param><value><base64>    ${EMPTY}
	${response_body}=    Replace String    ${response_body}    </base64></value></param></params></methodResponse>    ${EMPTY}
	Log To Console    Response:${response_body}

