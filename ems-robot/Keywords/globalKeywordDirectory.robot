*** Settings ***
#  Declare all irisLoginKeywords keyword files here
Resource		  irisLoginKeywords/irisLoginKeywords.robot
#  Declare all customerCreationKeywords keyword files here
Resource          customerCreationKeywords/customerCreationKeywords.robot
#  Declare all paystationCreationKeywords keyword files here
Resource          paystationCreationKeywords/paystationCreationKeywords.robot
#  Declare all merchantAccountCreationKeywords keyword files here
Resource          merchantAccountCreationKeywords/merchantAccountCreationKeywords.robot
#  Declare all generalKeywords keyword files here
Resource          generalKeywords/generalKeywords.robot

# Declare all paystationCommunicationKeywords keyword files here
Resource          paystationCommunicationKeywords/paystationCommunicationKeywords.robot

# Declare all navigationalKeywords files here
Resource          navigationalKeywords/sideBarNav.robot

# Declare all Report Keywords here
Resource          customerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot

# Declare all Alert Keywords here
Resource          customerAdmin_Alerts_Keywords/AlertKeywords.robot

# Declare all Services Keywords here
Resource          servicesKeywords/customerAdmin_Services_Keywords.robot
Resource          servicesKeywords/systemAdmin_Services_Keywords.robot

# Declare all paystationEditKeywords keyword files here
Resource          paystationEditKeywords/paystationEditKeywords.robot

# Declare all paystationEditKeywords keyword files here
Resource          customerAdmin_Location_Keywords/customerAdmin_Location_Keywords.robot

# Declare all customerAdminAccountsKeywords here
Resource		  customerAdminAccountsKeywords/CustomerAdmin_Accounts_Coupons_Keywords.robot
Resource		  customerAdminAccountsKeywords/CustomerAdmin_Accounts_Customer_Keywords.robot
Resource		  customerAdminAccountsKeywords/CustomerAdmin_Accounts_Passcards_Keywords.robot


# Declare all customerAdminReportKeywords here
Resource		  customerAdminReportKeywords/CustomerAdmin_Report_Keywords.robot

