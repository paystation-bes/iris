*** Settings ***
Documentation     A resource file with reusable keywords related to Merchant Account Creation
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           DatabaseLibrary
Resource          ../../data/data.robot

*** Variables ***
# Summary: Every Processor and their fields in the Add Merchant Account Form
# Processor                  Field1              Field2                  Field3              Field4                     Field5
@{default_labels}            Account             Status                  Processor
@{Alliance}                  Merchant ID         Terminal ID
@{Elavon}                    Terminal ID         Merchant ID             Close Batch Size
@{Elavon (Converge)}         Merchant ID         User ID                 PIN                 Dynamic DBA Prefix Length
@{First Data (Concord)}      Store ID            Store Key
@{First Data (Nashville)}    Company ID          Terminal ID             ZIP Code            Merchant Category Code
@{First Horizon}             Account ID          Terminal ID
@{Heartland}                 Merchant ID         Terminal ID
@{Link2Gov}                  Merchant Code       Settle Merchant Code    Merchant Password
@{Moneris}                   Store ID            API Token
@{Paymentech}                Account ID          Terminal ID             User                Password                    Client Number
@{Authorize.Net}             Login ID            Processor               Transaction Key
@{PayPros (Paradata)}        Token               Processor
@{BlackBoard}                Encryption Key      Server IP               Tender Number       Vendor Number
@{CBORD - CS Gold}           Server IP           Port Number             Provider Name       Location                    Code Map
@{CBORD - Odyssey}           Server IP           Port Number             Provider Name       Location                    Code Map
@{NuVision}                  Encryption Key      Server IP               Port Number         Packet Type
@{TotalCard}                 Security Key        Server IP               Store Number        Terminal Number


*** Keywords ***

Create Valid Paymentech Merchant Account For: "${customer_name}"
    # Creates a Valid Paymentech Merchant Account for the given customer_name
    # Updates the Client Code in the database to 0002
    # Sets a Global Variable called ${G_PAYMENTECH_ACC_NAME}
    Login As System Admin    systemadmin    Password$1    password
    Search Up Customer: "${customer_name}"
    I Go to Add Merchant Account Form

    ${random}=  Get Time  Epoch
    ${account_name}=  Catenate    SEPARATOR=    MerchName${random}
    Set Global Variable  ${G_PAYMENTECH_ACC_NAME}  ${account_name}

    I Set Account Name to "${G_PAYMENTECH_ACC_NAME}"
    I Set Status To: "Enabled"
    I Select Processor: "Paymentech"
    I Select Close Time: "12:00\ \ am"
    I Select Time Zone: "Canada/Atlantic"
    Input Text    field1    700000002018
    Input Text    field2    001
    Input Text    field3    digitech1
    Input Text    field4    digpaytec01
    Input Text    field5    ${random}
    I Click Add Account
    Connect To Database    ${DB_API_MODULE_NAME}     ${DB_NAME}    ${DB_USERNAME}    ${DB_PASSWORD}    ${DB_HOST}    ${DB_PORT}
    Update Paymentech Client Code  ${account_name}
    Verify Paymentech Client Code  ${account_name}
    Reload Page
    Merchant Account Test Passes  ${account_name}  Paymentech
    Close Browser



Update Paymentech Client Code
    # This Keyword updates a Paymentech's client code to 0002 given the account name
    [arguments]  ${account_name}
    ${merchant_acc_id}=  Get Merchant Account Id  ${account_name}
    Execute Sql String  UPDATE `${DB_NAME}`.`MerchantAccount` SET `Field5`='0002' WHERE `Id`='${merchant_acc_id}'

Get Merchant Account Id
    # This Keyword gets the Id field from the MerchantAccount table given the account name
    [arguments]  ${account_name}
    ${result}=  Query  SELECT Id FROM MerchantAccount WHERE Name = '${account_name}'
    Log To Console  Merchant Account Id is: ${result[0][0]}
    [return]  ${result[0][0]}

Verify Paymentech Client Code
    # This Keyword verifies that the client code of a Paymentech Merchant Account is 0002 given it's account name
    [arguments]  ${account_name}
    Check If Exists In Database    SELECT field5 FROM MerchantAccount WHERE Name='${account_name}' AND field5=0002
# ------------- Navigational ------------- #

I Go To Add Merchant Account Form
    Click Element    css=#btnAddMrchntAccnt
    Wait Until Keyword Succeeds    10s    1s   Element Should Be Visible    xpath=//span[contains(text(),'Add Merchant Account')]


# ------------- Form ------------- #

I Set Account Name To "${account_name}"
    Input Text    css=#account    ${account_name}


# Only runs if status is "Enabled"
I Set Status To: "${status}"
    Run Keyword If    '${status}'=='Enabled'    Click Element    css=#statusCheck

I Select Processor: "${processor}"
    Wait Until Keyword Succeeds    10s    1s    Run Keywords    Click Element    css=#processorsExpand    AND    Element Should Be Visible    xpath=//a[contains(text(),'${processor}')]
    Click Link    xpath=//a[contains(text(),'${processor}')]
    Sleep    1
    Run Keyword Unless    '${processor}'    ==    'CBORD - CS Gold'    OR    '${processor}'    ==    'CBORD - Odyssey'    Wait Until Keyword Succeeds    10s    1s    Assert Time Labels

Assert Time Labels
    Element Should Be Visible   xpath=//label[contains(text(),'Close Time')]
    Element Should Be Visible   xpath=//label[contains(text(),'Time Zone')]

I Click Add Account
    Sleep    3
    Click Element    xpath=//span[contains(text(),'Add Account')]/..
    Sleep    1
    Reload Page

# Needs 2 spaces between the time and am/pm
# Example:     I Select Close Time: "12:00\ \ am"
I Select Close Time: "${close_time}"
    Wait Until Keyword Succeeds    10s    1s    Run Keywords    Click Element    css=#closeTimeExpand    AND     Element Should Be Visible    xpath=//a[contains(text(),'${close_time}')]
    Wait Until Keyword Succeeds    10s    1s    Click Element    xpath=//a[contains(text(),'${close_time}')]

#Example:    I Select Time Zone: "Canada/Atlantic"
I Select Time Zone: "${time_zone}"
    Wait Until Keyword Succeeds    10s    1s    Run Keywords    Click Element    css=#mrchSelectTimeZoneExpand    AND     Element Should Be Visible    xpath=//a[contains(text(),'${time_zone}')]
    Wait Until Keyword Succeeds    10s    1s    Click Element    xpath=//a[contains(text(),'${time_zone}')]

New Merchant Account Listing Should Be Visible
    [arguments]    ${account_name}    ${processor}
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    xpath=//ul[@id='merchantAccountList']//p[contains(text(),'${account_name}')]/../following-sibling::div/p[contains(text(),'${processor}')]

Verify Label And Label Value
    [arguments]    ${label}    ${label_value}
    Element Should Be Visible    xpath=//dt[contains(text(),'${label}')]/following-sibling::dd[contains(text(),'${label_value}')][1]

# Only works if there are no duplicate Merchant Accounts
Merchant Account Is Deleted
    [arguments]    ${account_name}    ${processor}
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    xpath=//ul[@id='merchantAccountList']//p[contains(text(),'${account_name}')]/../following-sibling::div[1]//p[contains(text(),'${processor}')]
    Click Element    xpath=//ul[@id='merchantAccountList']//p[contains(text(),'${account_name}')]/../following-sibling::div[1]//p[contains(text(),'${processor}')]/../following-sibling::a[@title='Option Menu']
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    xpath=//ul[@id='merchantAccountList']//p[contains(text(),'${account_name}')]/../following-sibling::div[1]//p[contains(text(),'${processor}')]/../../following-sibling::section[1]//a[@class='delete'] 
    Click Element    xpath=//ul[@id='merchantAccountList']//p[contains(text(),'${account_name}')]/../following-sibling::div[1]//p[contains(text(),'${processor}')]/../../following-sibling::section[1]//a[@class='delete']
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    xpath=//span[contains(text(),'Delete')]
    Click Element    xpath=//span[contains(text(),'Delete')]
    Sleep    2
    Reload Page
    Element Should Not Be Visible    xpath=//ul[@id='merchantAccountList']//p[contains(text(),'${account_name}')]/../following-sibling::div[1]//p[contains(text(),'${processor}')]

Merchant Account Test Fails
    [arguments]    ${account_name}    ${proccessor}
    Sleep    3
    Click Element    xpath=//p[contains(text(),'${account_name}')]/../following-sibling::div/p[contains(text(),'${proccessor}')]/../following-sibling::a[@title='Option Menu'][1]
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    xpath=//p[contains(text(),'${account_name}')]/../following-sibling::div/p[contains(text(),'${proccessor}')]/../../following-sibling::section[1]//a[@class='test']
    Click Element    xpath=//p[contains(text(),'${account_name}')]/../following-sibling::div/p[contains(text(),'${proccessor}')]/../../following-sibling::section[1]//a[@class='test']
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    xpath=//section[@id='messageResponseAlertBox']
    Reload Page

Merchant Account Test Passes
    [arguments]    ${account_name}    ${proccessor}
    Sleep    3
    Click Element    xpath=//p[contains(text(),'${account_name}')]/../following-sibling::div/p[contains(text(),'${proccessor}')]/../following-sibling::a[@title='Option Menu'][1]
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    xpath=//p[contains(text(),'${account_name}')]/../following-sibling::div/p[contains(text(),'${proccessor}')]/../../following-sibling::section[1]//a[@class='test']
    Click Element    xpath=//p[contains(text(),'${account_name}')]/../following-sibling::div/p[contains(text(),'${proccessor}')]/../../following-sibling::section[1]//a[@class='test']
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    xpath=//strong[contains(text(),'Merchant Account is active.')]
    Reload Page

# ------------- Alert/Error Messages ------------- #

Merchant Account Attention Pop Up Should Appear with error: "${error_message}"
    Element Should Be Visible    xpath=//article[contains(text(),'The following errors occurred:')]//li[contains(text(),'${error_message}')]

Invalid Account Name Attention Pop Up Should Appear
    Element Should Be Visible    xpath=//article[contains(text(),"This field only accepts Letters, digits, space, dot, comma, quote and special characters ':', ';', '#', '@', '-', '|', '/', '\', '{', '}', '(' and ')'")]

