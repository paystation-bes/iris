*** Keywords ***

Create User Defined Alert
    # This Keyword Creates a User Defined Alert in Settings
    # Pre-Reqs: Must Be In Settings
    [arguments]
    ...  ${alert_name}
    ...  ${alert_route}
    ...  ${alert_type}
    ...  ${exceeds}=${EMPTY}
    ...  ${exceeds_type}=${EMPTY}

    Click Alerts Tab
    Click Add Alert
    Input Alert Name: "${alert_name}"
    Configure Threshold  ${alert_type}  exceeds=${exceeds}  exceeds_type=${exceeds_type}
    Select Alert Route: "${alert_route}"
    Click Save Alert
    Verify Alert Creation  ${alert_name}  ${alert_type}

Click Add Alert
    Wait Until Element Is Visible  btnAddAlert  10s
    Click Element  btnAddAlert

Input Alert Name: "${alert_name}"
    # Inputs the given alert name into the alert name field when creating a new user defined alert
    Input Text  formAlertName  ${alert_name}

Configure Threshold
    # Configures the Threshold fields when creating a new user defined alert.
    # Fills in When, Exceeds, and Exceeds Type
    # When invoking this keyword, provide the alert type and depending on your alert type,
    # also provide exceeds=${your value} and/or ${exceeds_type}=${your_type}
    # Example of use:
    #     Configure Threshold  ${alert_type}  exceeds=${exceeds}
    [arguments]
    ...  ${alert_type}
    ...  ${exceeds}=${EMPTY}
    ...  ${exceeds_type}=${EMPTY}

    Select Alert Type: "${alert_type}"
    Run Keyword If  '${alert_type}'=='Last Seen Interval' or '${alert_type}'=='Running Total' or '${alert_type}'=='Coin Canister' or '${alert_type}'=='Bill Stacker' or '${alert_type}'=='Unsettled Credit Cards' or '${alert_type}'=='Last Collection Interval'  Input Text  formDisplayExceedAmount  ${exceeds}
    Run Keyword If  '${alert_type}'=='Coin Canister' or '${alert_type}'=='Bill Stacker' or '${alert_type}'=='Unsettled Credit Cards'  Select Exceeds Type: "${exceeds_type}"

Select Alert Type: "${alert_type}"
    # Selects the alert type when creating a new user defined alert. ie). Coin Canister, Running Total
    Sleep  1
    Click Element  formAlertTypeExpand
    Wait Until Element Is Visible  xpath=//ul[@id='ui-id-1']//a[contains(text(),'${alert_type}')]
    Click Element  xpath=//ul[@id='ui-id-1']//a[contains(text(),'${alert_type}')]

Select Exceeds Type: "${exceeds_type}"
    # Selects the exceeds type when creating a new user defined alert ie). Dollar, Count
    Click Element  formDisplayExceedTypeExpand
    Wait Until Element Is Visible  xpath=//ul[@id='ui-id-2']//a[contains(text(),'${exceeds_type}')]  10s
    Click Element  xpath=//ul[@id='ui-id-2']//a[contains(text(),'${exceeds_type}')]

Select Alert Route: "${alert_route}"
    # This keyword selects the alert route when creating a new user defined alert
    Click Element  formAlertRouteExpand
    Wait Until Element Is Visible  xpath=//ul[@id='ui-id-3']//a[contains(text(),'${alert_route}')]
    Click Element  xpath=//ul[@id='ui-id-3']//a[contains(text(),'${alert_route}')]

Click Save Alert
    # This Keyword clicks the save button when editing or crating a user defined alert
    Click Element  xpath=//form[@id='alertSettings']//a[contains(text(),'Save')]

Verify Alert Creation
    # This Keyword does a high-level check that the alert exists in the alerts list using the alert name and alert type
    [arguments]  ${alert_name}  ${alert_type}
    Wait Until Element Is Visible  xpath=//div[@class='col1 routeName']/p[text()='${alert_name}']/../..//div[@class='col2 routeType']/p[text()='${alert_type}']  10s

Disable User Defined Alert
    # Diables a user defined alert given the alert's name
    # Pre-Req: Must be in Settings
    [arguments]  ${alert_name}

    Edit Alert: "${alert_name}"
    # Uncheck Enable
    Sleep  1
    Wait Until Element Is Visible  statusCheck  10s
    # Get the status of the Enabled checkbox
    ${status}=  Get Element Attribute  xpath=//a[@id='statusCheck']@class
    # Only Click the checkbox again if the class is not checked
    Run Keyword If  '${status}'=='checkBox checked'  Click Element  statusCheck
    # Click Save
    Click Save Alert
    # Verify that Alert is Disabled after save
    Wait Until Element Is Visible  //article[@id='alertView']//dd[@id='detailAlertStatus' and contains(text(),'Disabled')]  10s

Click Option Menu: "${alert_name}"
# Clicks the option menu for the given alert name
# Pre-Req: Must be in Settings
    Click Alerts Tab
    # Click the option menu
    Wait Until Element Is Visible  xpath=//div[@class='col1 routeName']/p[text()='${alert_name}']/../following-sibling::a[1]  10s
    Click Element  xpath=//div[@class='col1 routeName']/p[text()='${alert_name}']/../following-sibling::a[1]
    Sleep  1

Edit Alert: "${alert_name}"
    # Goes into the edit menu of the alert with the given alert name
    # Pre-Req: Must be in Settings
    Click Option Menu: "${alert_name}"
    # Click Edit Button
    Wait Until Element Is Visible  xpath=//div[@class='col1 routeName']/p[text()='${alert_name}']/../../following-sibling::section[1]/section/a[contains(text(),'Edit')]  10s
    Click Element  xpath=//div[@class='col1 routeName']/p[text()='${alert_name}']/../../following-sibling::section[1]/section/a[contains(text(),'Edit')]

Add Email To Alert
    # Adds the given E-mail to the E-mail contacts of an alert with the given alert name
    # Pre-Req: Must be in Settings
    [arguments]  ${alert_name}  ${email}
    Edit Alert: "${alert_name}"
    Wait Until Element Is Visible  newNotificationEmail
    Input Text  newNotificationEmail  ${email}
    Click Element  alertEmailAddBtn
    Wait Until Element Is Visible  xpath=//ul[@id='formDisplayAlertNotifyList']/li[@class='selected']/span[@class='contactEmail' and text()='${email}']  5s
    Click Save Alert
    Wait Until Element Is Visible  xpath=//ul[@id='detailNotificationList']/text()[normalize-space()='${email}']/..  15s

Remove Email From Alert
    # Removes the given email from an alert with the given Alert name
    # Pre-Req: Must be in Settings
    [arguments]  ${alert_name}  ${email}
    Edit Alert: "${alert_name}"
    # Wait until the email shows up as "selected"
    Wait Until Element Is Visible  xpath=//ul[@id='formDisplayAlertNotifyList']/li[@class='selected']/span[@class='contactEmail' and text()='${email}']  15s
    # click the checkbox if it does show up as selected
    Click Element  xpath=//ul[@id='formDisplayAlertNotifyList']/li[@class='selected']/span[@class='contactEmail' and text()='${email}']/../a
    # Make sure that the email is unselected
    Wait Until Element Is Visible  xpath=//ul[@id='formDisplayAlertNotifyList']/li[@class='unselected']/span[@class='contactEmail' and text()='${email}']  1s
    Click Save Alert
    Sleep  5
    Wait Until Element Is Visible  css=#alertDetails  10s
    # Verify that the alert is no longer listed
    Element Should Not Be Visible  xpath=//ul[@id='detailNotificationList']/text()[normalize-space()='${email}']/..

Generate Random Email
    # Generates a random email in the form of email@123415134.com
    ${random}=  Get Time  Epoch
    ${result}=  Catenate  SEPARATOR=  email@  ${random}  .com
    [return]  ${result}

User Defined Alert Is Not Triggered In Recent Activity
    # Checks the Recent Activity section to see if the the given alert name is not triggered
    [arguments]  ${alert_name}  ${paystation_id}
    Click Pay Stations Tab
    Sleep  5
    Click Paystation In Pay Station List  ${paystation_id}
    Click Recent Activity
    Verify That "${alert_name}" Is Not Triggered

Enable User Defined Alert
    # Enables a user defined alert given the alert's name
    # Pre-Req: Must be in Settings
    [arguments]  ${alert_name}
    Click Alerts Tab
    # Click the option menu
    Wait Until Element Is Visible  xpath=//div[@class='col1 routeName']/p[text()='${alert_name}']/../following-sibling::a[1]  10s
    Click Element  xpath=//div[@class='col1 routeName']/p[text()='${alert_name}']/../following-sibling::a[1]
    Sleep  1
    # Click Edit Button
    Wait Until Element Is Visible  xpath=//div[@class='col1 routeName']/p[text()='${alert_name}']/../../following-sibling::section[1]/section/a[contains(text(),'Edit')]  10s
    Click Element  xpath=//div[@class='col1 routeName']/p[text()='${alert_name}']/../../following-sibling::section[1]/section/a[contains(text(),'Edit')]
    # Uncheck Enable
    Sleep  1
    Wait Until Element Is Visible  statusCheck  10s
    # Get the status of the Enabled checkbox
    ${status}=  Get Element Attribute  xpath=//a[@id='statusCheck']@class
    # Only Click the checkbox again if the class is not checked
    Run Keyword If  '${status}'=='checkBox'  Click Element  statusCheck
    # Click Save
    Click Save Alert
    # Verify that Alert is Disabled after save
    Wait Until Element Is Visible  //article[@id='alertView']//dd[@id='detailAlertStatus' and contains(text(),'Enabled')]  10s

Click Paystation In Pay Station List
    # Selects the pay station with the given id in the Pay Station List
    [arguments]  ${paystation_id}
    Wait Until Element Is Visible  xpath=//li[@name='${paystation_id}']
    Click Element  xpath=//li[@name='${paystation_id}']
    Wait Until Element Is Visible  xpath=//h1[@id='paystationName']  10s

Click Recent Activity
    # Clicks the Recent Activity tab for a Pay Station
    Click Element  xpath=//a[@title='Recent Activity']
    Wait Until Element Is Visible  xpath=//ul[@id='crntAlertList']  10s

Verify That "${alert_name}" Is Not Triggered
    # Looks at the recent activity tab and verifies that an active alert with the given alert name is not present
    Sleep  3
    Element Should Not Be Visible  xpath=//ul[@id='crntAlertList']//li//p[contains(text(),'${alert_name}')]/../..//p[contains(text(),'Active')]

Alert Is Triggered In Recent Activity Just Now
    # Checks the Recent Activity section to see if the the given alert name is triggered
    [arguments]  ${alert_name}  ${paystation_id}
    Click Pay Stations Tab
    Sleep  5
    Click Paystation In Pay Station List  ${paystation_id}
    Click Recent Activity
    ${alert_id}=  Verify That "${alert_name}" Is Triggered Just Now
    [return]  ${alert_id}

Verify That "${alert_name}" Is Triggered Just Now
# Looks at the recent activity tab and verifies that an active alert with the given alert name is present
# Also Returns the Alert's ID
    Sleep  3
    Wait Until Keyword Succeeds    120s    1s
    ...    Run Keywords    Click Element    xpath=//article[@id='alertsArea']/a[text()='Reload']
    ...    AND    Element Should Be Visible  xpath=//ul[@id='crntAlertList']//li//p[contains(text(),'${alert_name}')]/../..//p[contains(text(),'just now')]/../..//p[contains(text(),'Active')]
    Sleep  3
    ${count}=  Get Matching Xpath Count  //div[@class='col1']/p[contains(text(),'${alert_name}')]/../../div[@class='col4']/p[contains(text(),'Active')]
    Should Be True  ${count}==1
    ${alert_id}=  Get Element Attribute  xpath=//ul[@id='crntAlertList']//li//p[contains(text(),'${alert_name}')]/../..//p[contains(text(),'Active')]/../..@id
    Log To Console  Verified that ${alert_name} has just been triggered with id: ${alert_id}
    [return]  ${alert_id}

User Defined Alert Is Found In Database As Active
    [arguments]  ${alert_name}
    Connect To Database    ${DB_API_MODULE_NAME}     ${DB_NAME}    ${DB_USERNAME}    ${DB_PASSWORD}    ${DB_HOST}    ${DB_PORT}
    ${alert_status}  Query  select IsActive from POSEventHistory where CustomerAlertTypeId = (select Id from CustomerAlertType where Name = '${alert_name}');
    Log To Console  Alert Status Is: ${alert_status[0][0]}
    Should Be True  '${alert_status[0][0]}'=='1'


Alert Is Updated In The Database In POSEventHistory
    [arguments]  ${alert_name}  ${customer_id}  ${serial}  ${event_severity_type_id}  ${is_active}  ${previous_index}
# Verifies that the given default alert is updated in the database.
# Verifies this in the POSEventHistoryTable and checks that the EventSeverityTypeId is 3 and the IsActive is 1
# Arguments:
# ${default_alert_name} The name of the default alert to check. ie). Running Total Default Alert
# ${customer_id} The customer id to check
# ${serial} The pay station serial number that the given alert should be active on
# ${event_severity_type_id} The expected event severity type of the alert
# ${is_active} The expected status of the alert: 1 for active, 0 for not active
# ${previous_index} The previous index of this query. We need this to check if the table actually got updated
    Connect To Database    ${DB_API_MODULE_NAME}     ${DB_NAME}    ${DB_USERNAME}    ${DB_PASSWORD}    ${DB_HOST}    ${DB_PORT}

    # Make sure that the table actually got updated
    Log To Console  Previous index is: ${previous_index[0][0]}
    ${current_index}=  Get Current Index Of Alert In POSEventHistory  ${alert_name}  ${customer_id}  ${serial}
    ${current_index}=  Set Variable  ${current_index[0][0]}
    Log To Console  Current index is: ${current_index}
    Should Be True  '${current_index}'>'${previous_index[0][0]}'

    ${event_severity_type}=  Query  select EventSeverityTypeId from POSEventHistory where Id=${current_index} and CustomerAlertTypeId= (select Id from CustomerAlertType where CustomerId=${customer_id} and name='${alert_name}') and PointOfSaleId=(select Id from PointOfSale where SerialNumber=${serial});
    Should Be True  '${event_severity_type[0][0]}'=='${event_severity_type_id}'
    ${status}=  Query  select IsActive from POSEventHistory where Id=${current_index} and CustomerAlertTypeId= (select Id from CustomerAlertType where CustomerId=${customer_id} and name='${alert_name}') and PointOfSaleId=(select Id from PointOfSale where SerialNumber=${serial});
    Should Be True  '${status[0][0]}'=='${is_active}'


Get Current Index Of Alert In POSEventHistory
    [arguments]  ${default_alert_name}  ${customer_id}  ${serial}
# Gets the current index of the specified default alert for the specified paystation
# This should be used when you want to make sure that the table you are verifying is getting updated
# For example, you want to check that in POSEventHistory that the alert you just triggered is the one you're getting from your query
# and that you're not getting information from previous data
    Connect To Database    ${DB_API_MODULE_NAME}     ${DB_NAME}    ${DB_USERNAME}    ${DB_PASSWORD}    ${DB_HOST}    ${DB_PORT}
    ${current_index}=  Query  select max(Id) from POSEventHistory where PointOfSaleId = (select Id from PointOfSale where SerialNumber=${serial}) and CustomerAlertTypeId = (select Id from CustomerAlertType where CustomerId=${customer_id} and name='${default_alert_name}');
    [return]  ${current_index}


Alert Is Entered In The Database In POSEventHistory As Active
# Verifies that the given alert is entered into POSEventHistory with the given event severity for the given paystation
    [arguments]  ${alert_name}  ${customer_id}  ${serial}  ${event_severity_type_id}
    Connect To Database    ${DB_API_MODULE_NAME}     ${DB_NAME}    ${DB_USERNAME}    ${DB_PASSWORD}    ${DB_HOST}    ${DB_PORT}
    ${status}  Query  select IsActive from POSEventHistory where CustomerAlertTypeId= (select Id from CustomerAlertType where CustomerId=${customer_id} and name='${alert_name}') and PointOfSaleId=(select Id from PointOfSale where SerialNumber=${serial});
    Should Be True  '${status[0][0]}'=='1'


Alert Is Deleted
# Deletes the Alert with the given alert name
# Pre-Req: Must be in settings
    [arguments]  ${alert_name}

    Click Option Menu: "${alert_name}"
    # Click the delete button
    Wait Until Element Is Visible  xpath=//div[@class='col1 routeName']/p[text()='${alert_name}']/../../following-sibling::section[1]/section/a[contains(text(),'Delete')]  10s
    Click Element  xpath=//div[@class='col1 routeName']/p[text()='${alert_name}']/../../following-sibling::section[1]/section/a[contains(text(),'Delete')]
    Wait Until Element Is Visible  xpath=//span[contains(text(),'Delete')]/..  10s
    Click Element  xpath=//span[contains(text(),'Delete')]/..
    Wait Until Element Is Not Visible  xpath=//*[@id='messageResponseAlertBox']  10s
    Sleep   4
    Wait Until Element Is Not Visible  xpath=//div[@class='col1 routeName']/p[text()='${alert_name}']

Alert: "${alert_id}" Is Resolved By: "${resolver}" In Recent Activity Just Now In Paystation: "${paystation_id}"
# Verifies that the alert with the given alert id is resolved by the given resolver just now
# Arguments
# ${alert_id} The unique alert id to check is resolved
# ${resolver} The resolver that appears in status when the alert is cleared ie. Administrator, System
# ${paystation_id} The serial number of the paystation of interest
    Click Pay Stations Tab
    Sleep  5
    Click Paystation In Pay Station List  ${paystation_id}
    Click Recent Activity
    Verify That "${alert_id}" Is Resolved By: "${resolver}" Just Now

Verify That "${alert_id}" Is Resolved By: "${resolver}" Just Now
# Performs the verification that the alert with the given alert id is resolved by the given resolver in recent activity
# Arguments
# ${alert_id} The unique alert id to check is resolved
# ${resolver} The resolver that appears in status when the alert is cleared ie. Administrator, System
# ${paystation_id} The serial number of the paystation of interest
    Sleep  3
    Wait Until Keyword Succeeds    60s    1s
    ...    Run Keywords    Click Element    xpath=//article[@id='alertsArea']/a[text()='Reload']
    ...    AND    Element Should Be Visible  xpath=//section[@id='${alert_id}']/div[@class='col3']/p[contains(text(),'just now')]/../../div[@class='col4']/p[contains(text(),'Resolved by') and contains(text(),'${resolver}')]
    Log To Console  Verified that ${alert_id} is Resolved by ${resolver}

Alert Is Active In Recent Activity
# Verifies that the alert with the given name is active in Recent Activity
    [arguments]  ${alert_name}  ${paystation_id}
    Click Pay Stations Tab
    Sleep  5
    Click Paystation In Pay Station List  ${paystation_id}
    Click Recent Activity
    Sleep  3
    Wait Until Keyword Succeeds    60s    1s
    ...    Run Keywords    Click Element    xpath=//article[@id='alertsArea']/a[text()='Reload']
    ...    AND    Element Should Be Visible  xpath=//p[contains(text(),'${alert_name}')]/../../div[@class='col4']/p[contains(text(),'Active')]
    Sleep  3
    ${id}=  Get Element Attribute  xpath=//p[contains(text(),'${alert_name}')]/../../div[@class='col4']/p[contains(text(),'Active')]/../..@id
    Log To Console  Verified that ${alert_name} is active
    Log To Console  ${alert_name} has id: ${id}
    [return]  ${id}