*** Keywords ***

Select Unassigned In Location Details
    [arguments]    ${location_name}
    Click Element    xpath=//section[contains(text(), 'Unassigned')]/following-sibling::a[@title='Edit']
    Wait Until Element Is Visible    xpath=//h2[contains(text(), 'Edit Location')]    10s


Verify Pay Station Name In Edit Location All Pay Stations List
    [arguments]    ${pay_station_name}
    Wait Until Element Is Visible    xpath=//ul[@id='locPSFullList']//li[contains(text(), '${pay_station_name}')]    10s