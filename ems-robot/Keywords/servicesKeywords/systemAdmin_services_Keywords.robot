*** Keywords ***

Following Services: "${services}" For Customer: "${customer_name}" Are: "${status}"
# Logs into system admin and enables the given services for the given customer name
# Arguments:
# - ${services}: The lsit of services that you want to be enabled. Text must match exactly what is seen on the section boxes
# - ${customer_name}: The customer name that sytstem admin will search update customer form should appear
# - ${status}: Enabled or Disabled

    Run Keyword If  '${services[0]}'=='${EMPTY}'  Return From Keyword

    Run Keywords
    ...               Login As System Admin    systemadmin    Password$1    password
    ...         AND   Search Up Customer: "${customer_name}"
    Click Element  btnEditCustomer

    Should Be True  '${status}'=='Enabled' or '${status}'=='Disabled'

    Wait Until Element Is Visible  xpath=//section[@id='ServicesCheckBoxes']  10s

    :FOR  ${service}  IN  @{services}
    \    ${check_box}=  Get Element Attribute  xpath=//section[@id='ServicesCheckBoxes']//label/text()[normalize-space()='${service}']/../a@class
    \    ${check_box}=  Strip String  ${check_box}
    \    Run Keyword If  '${status}'=='Enabled'  Run Keyword If  '${check_box}'=='checkBox'  Click Element  xpath=//label/text()[normalize-space()='${service}']/../a
    \    Run Keyword If  '${status}'=='Disabled'  Run Keyword If  '${check_box}'=='checkBox checked'  Click Element  //label/text()[normalize-space()='${service}']/../a
    Click Save Update Customer
    Sleep  2
    :FOR  ${service}  IN  @{services}
    \    Run Keyword If  '${status}'=='Enabled'  Wait Until Element Is Visible  xpath=//li[@class='activatedService' and contains(text(),'${service}')]
    \    Run Keyword If  '${status}'=='Disabled'  Wait Until Element Is Visible  xpath=//li[@class='availableService' and contains(text(),'${service}')]

    Close Browser

Following Digital API Services Should Be Visible In System Admin: "${services}" For Customer: "${customer_name}"
# Verifies that the given Digital API services are visible in system admin
# Also verifies that the Services only appear once
# Arguments:
# - ${services} A list of the Digital API Services that should be visible
# - ${customer_name} The Customer name that system admin will search and perform the verification on
    Run Keywords
    ...               Login As System Admin    systemadmin    Password$1    password
    ...         AND   Search Up Customer: "${customer_name}"
    Click Licenses Tab

    Run Keyword If  '${services[0]}'=='${EMPTY}'  Element Should Not Be Visible  xpath=//h2[contains(text(),'Digital API')]
    Run Keyword If  '${services[0]}'=='${EMPTY}'  Close Browser
    Run Keyword If  '${services[0]}'=='${EMPTY}'  Return From Keyword

    :FOR  ${service}  IN  @{services}
    \    ${digital_api_service}=  Get Substring  ${service}  13
    \    ${xpath_count}=  Get Matching Xpath Count  //h2[contains(text(),'Digital API') and contains(text(),'${digital_api_service}')]
    \    Should Be True  ${xpath_count}==1
    \    Wait Until Element Is Visible  xpath=//h2[contains(text(),'Digital API') and contains(text(),'${digital_api_service}')]  10s
    Close Browser

Following Digital API Services Should Not Be Visible In System Admin: "${services}" For Customer: "${customer_name}"
# Verifies that the given Digital API services are not visible in system admin
# Arguments:
# - ${services} A list of the Digital API Services that should be visible
# - ${customer_name} The Customer name that system admin will search and perform the verification on

    Run Keyword If  '${services[0]}'=='${EMPTY}'  Return From Keyword

    Run Keywords
    ...               Login As System Admin    systemadmin    Password$1    password
    ...         AND   Search Up Customer: "${customer_name}"
    Click Licenses Tab

    :FOR  ${service}  IN  @{services}
    \    ${digital_api_service}=  Get Substring  ${service}  13
    \    Sleep  3
    \    Element Should Not Be Visible  xpath=//h2[contains(text(),'Digital API') and contains(text(),'${digital_api_service}')]
    Close Browser

Click Save Update Customer
# Clicks the save button in the Update Customer Form in System Admin
    Click Element  xpath=//div[@class='ui-dialog-buttonset']//span[contains(text(),'Update Customer')]/..
