*** Keywords ***

Following Digital API Services Should Be Visible In Customer Admin: "${services}" For Customer: "${customer_username}"
# Verifies that the given Digital API services are visible in customer admin
# Also verifies that the Services only appear once
# Arguments:
# - ${services} A list of the Digital API Services that should be visible
# - ${customer_name} The Customer name that system admin will search and perform the verification on

    Login As Generic Customer  ${customer_username}  Password$1
    Go To Settings Section

    ${list_length}=  Get Length  ${services}

    Run Keyword If  '${services[0]}'=='${EMPTY}'  Element Should Not Be Visible  xpath=//h3[contains(text(),'Digital API')]
    Run Keyword If  '${services[0]}'=='${EMPTY}'  Close Browser
    Run Keyword If  '${services[0]}'=='${EMPTY}'  Return From Keyword

    :FOR  ${service}  IN  @{services}
    \    ${digital_api_service}=  Get Substring  ${service}  13
    \    ${xpath_count}=  Get Matching Xpath Count  //h3[contains(text(),'Digital API') and contains(text(),'${digital_api_service}')]
    \    Should Be True  ${xpath_count}==1
    \    Wait Until Element Is Visible  xpath=//h3[contains(text(),'Digital API') and contains(text(),'${digital_api_service}')]  10s
    Close Browser


Following Digital API Services Should Not Be Visible In Customer Admin: "${services}" For Customer: "${customer_username}"
# Verifies that the given Digital API services are not visible in customer admin

    Run Keyword If  '${services[0]}'=='${EMPTY}'  Return From Keyword

    Login As Generic Customer  ${customer_username}  Password$1
    Go To Settings Section

    ${list_length}=  Get Length  ${services}

    :FOR  ${service}  IN  @{services}
    \    ${digital_api_service}=  Get Substring  ${service}  13
    \    Sleep  3
    \    Element Should Not Be Visible  xpath=//h3[contains(text(),'Digital API') and contains(text(),'${digital_api_service}')]
    Close Browser