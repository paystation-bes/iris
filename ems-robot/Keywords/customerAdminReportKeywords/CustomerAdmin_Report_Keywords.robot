*** Keywords ***
# Below are generic Keywords

Click Create Report
    # This Keyword clicks the create report tab
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    css=#btnAddScheduleReport
    Click Element    css=#btnAddScheduleReport
    Sleep    1s 

Select Report Type: "${report_type}"
    # This Keyword selects the variable ${report_type} 
    # ${report_type} - the report that you want to generate
    Wait Until Element Is Visible    css=#reportTypeIDExpand    10s
    Click Link    css=#reportTypeIDExpand
    Wait Until Element Is Visible    xpath=//ul[@id='ui-id-1']//a[contains(text(),'${report_type}')]    10s
    Click Element    xpath=//ul[@id='ui-id-1']//a[contains(text(),'${report_type}')]

Click Anywhere On Page
    # This Keyword clicks anywhere on the webpage 
    Click Element    css=#settings

Close Pop-up Window
    # This Keyword closes the pop-up window
    Click Element    xpath=//a[@title='Close']

# Click Reports Tab
#     # This Keyword clicks the report tab
#     Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    xpath=//a[@name='Reports']
#     Click Element    xpath=//a[@name='Reports']/img

I Create A Collection Summary Report
    # This Keyword creates a generic andthe default case of a Collection Summary Report
    [arguments]
    ...  ${detail_type}=Summary
    ...  ${location_type}=Location
    ...  ${group_by}=Location
    ...  ${schedule}=Submit Now
    ...  ${output_format}=PDF

    Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "Collection Summary"
    Proceed To Step 2
    Select Location Type All: "${location_type}"
    Select Collection Type: All
    Select Group By: "${group_by}"
    Select Output Format: "${output_format}"
    Proceed To Step 3
    Select Schedule: "${schedule}"
    Click Save Report
    Report "Collection Summary" Is Pending
    Report "Collection Summary" Is Completed

I Create A Coupon Usage Summary Report
    # This Keyword creates a generic/ default case of a Coupon Usage Summary Report
    [arguments]
    ...  ${detail_type}=Details
    ...  ${transaction_date/time}=Today
    ...  ${location_type}=Location
    ...  ${archived_pay_stations?}=Yes
    ...  ${coupon_code}=All Coupons
    ...  ${coupon_type}=All
    ...  ${search_account}=
    ...  ${output_format}=PDF
    ...  ${schedule}=Submit Now

    Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "Coupon Usage Summary"
    Proceed To Step 2
    Select Report Detail Type: "${detail_type}"
    Select Transaction Date/Time: "${transaction_date/time}"
    Select Location Type: "${location_type}"
    Include Archived Pay Stations: "${archived_pay_stations?}"
    Select Coupon Code: "${coupon_code}"
    Select Coupon Type: "${coupon_type}"
    Select Output Format: "${output_format}"
    Proceed To Step 3
    Select Schedule: "${schedule}"
    Click Save Report
    Report "Coupon Usage Summary" Is Pending
    Report "Coupon Usage Summary" Is Completed

I Create A Transaction - Credit Card Report
# This is keyword that creates a Transaction - Credit Card reports with the default variables below unless specified otherwise
    [arguments]
    ...  ${detail_type}=Summary

    ...  ${transaction_date/time}=Today
    ...  ${location_type}=Location
    ...  ${archived_pay_stations?}=Yes
    ...  ${space_number}=N/A
    ...  ${license_plate}=${EMPTY}
    ...  ${ticket_number}=All
    ...  ${coupon_code}=N/A
    ...  ${transaction_type}=All
    ...  ${group_by}=None
    ...  ${schedule}=Submit Now
    ...  ${output_format}=PDF

    ...  ${card_type}=All
    ...  ${approval_status}=All
    ...  ${merchant_account}=All
    ...  ${card_number}=All

    Select Report Type: "Transaction - Credit Card"
    Proceed To Step 2
    Select Report Detail Type: "${detail_type}"
    Select Transaction Date/Time: "${transaction_date/time}"
    Select Location Type All: "${location_type}"
    Include Archived Pay Stations: "${archived_pay_stations?}"
    Select Space Number: "${space_number}"
    Input License Plate: "${license_plate}"
    Select Ticket Number: "${ticket_number}"
    Select Coupon Code: "${coupon_code}"

    Select Card Type: "${card_type}"
    Select Approval Status: "${approval_status}"
    Select Merchant Account: "${merchant_account}"
    Select Card Number: "${card_number}"

    Select Transaction Type: "${transaction_type}"
    Select Group By: "${group_by}"
    Select Output Format: "${output_format}"
    Proceed To Step 3
    Select Schedule: "${schedule}"
    Click Save Report
    Report "Transaction - Credit Card" Is Pending
    Report "Transaction - Credit Card" Is Completed

I Create A Pay Station Summary Report
    # This Keyword creates a generic/ default case of a Pay Station Summary Report
    [arguments]
    ...  ${location_type}=Location
    ...  ${archived_pay_stations?}=Yes
    ...  ${order_by}=Pay Station Name
    ...  ${output_format}=PDF
    ...  ${schedule}=Submit Now

    Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "Pay Station Summary"
    Proceed To Step 2
    Select Location Type: "${location_type}"
    Include Archived Pay Stations: "${archived_pay_stations?}"
    Select Order By: "${order_by}"
    Select Output Format: "${output_format}"
    Proceed To Step 3
    Select Schedule: "${schedule}"
    Click Save Report
    Report "Pay Station Summary" Is Pending
    Report "Pay Station Summary" Is Completed

I Create A Stall Reports Report
    # This Keyword creates a default Stall Reports Report
    [arguments]

    ...  ${space_location}=Unassigned
    ...  ${archived_pay_stations?}=Yes
    ...  ${space_number}=All
    ...  ${stall_status}=All
    ...  ${schedule}=Submit Now
    ...  ${output_format}=PDF

    Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "Stall Reports"
    Proceed To Step 2
    Select Space Location: "${space_location}"
    Include Archived Pay Stations: "${archived_pay_stations?}"
    Select Space Number: "${space_number}"
    Select Other Report Type: "${stall_status}"
    Select Output Format: "${output_format}"
    Proceed To Step 3
    Select Schedule: "${schedule}"
    Click Save Report
    Report "Stall Reports" Is Pending
    Report "Stall Reports" Is Completed

I Create A Tax Report Report
    # This Keyword creates a Tax Report Report
    [arguments]
    ...  ${detail_type}=Details

    ...  ${transaction_date/time}=Today
    ...  ${location_type}=Location
    ...  ${archived_pay_stations?}=Yes
    ...  ${group_by}=None
    ...  ${schedule}=Submit Now
    ...  ${output_format}=PDF

    Wait Until Keyword Succeeds    9 sec    3 sec    Select Report Type: "Tax Report"
    Proceed To Step 2
    Select Report Detail Type: "${detail_type}"
    Select Transaction Date/Time: "${transaction_date/time}"
    Select Location Type All: "${location_type}"
    Include Archived Pay Stations: "${archived_pay_stations?}"
    Select Group By: "${group_by}"
    Select Output Format: "${output_format}"
    Proceed To Step 3
    Select Schedule: "${schedule}"
    Click Save Report
    Report "Tax Report" Is Pending
    Report "Tax Report" Is Completed

I Create A Transaction - All Report
    # This is keyword that creates a Transaction - All reports with the default variables below unless specified otherwise
    [arguments]
    ...  ${detail_type}=Summary

    ...  ${transaction_date/time}=Today
    ...  ${location_type}=Location
    ...  ${archived_pay_stations?}=Yes
    ...  ${space_number}=N/A
    ...  ${license_plate}=${EMPTY}
    ...  ${ticket_number}=All
    ...  ${coupon_code}=N/A
    ...  ${transaction_type}=All
    ...  ${group_by}=None
    ...  ${schedule}=Submit Now
    ...  ${output_format}=PDF

    Select Report Type: "Transaction - All"
    Proceed To Step 2
    Select Report Detail Type: "${detail_type}"
    Select Transaction Date/Time: "${transaction_date/time}"
    Select Location Type All: "${location_type}"
    Include Archived Pay Stations: "${archived_pay_stations?}"
    Select Space Number: "${space_number}"
    Input License Plate: "${license_plate}"
    Select Ticket Number: "${ticket_number}"
    Select Coupon Code: "${coupon_code}"
    Select Transaction Type: "${transaction_type}"
    Select Group By: "${group_by}"
    Select Output Format: "${output_format}"
    Proceed To Step 3
    Select Schedule: "${schedule}"
    Click Save Report
    Report "Transaction - All" Is Pending
    Report "Transaction - All" Is Completed

# Minor low-leve keywords, meant to translate selenuium keywords of specific elements into natural, easy-to-read and understand human language 

Attention, ${required_field} Is Required
    Page Should Contain    ${required_field} is required.

Attention, Ending Space Number Must Be Larger
    # This Keyword promps the Warning sign to pop-up as the textfield only allows the second field 
    # of the space number to be a larger number
    Page Should Contain    Ending Space Number must be greater than or equal to Starting Space Number.

Attention, Only Basic Numerical Value
    # This Keyword prompts the Warning sign to pop-up as the textfield only allows integers to be inputted
    Page Should Contain    This field only accepts a basic numerical value.

Attention, Only Accepts Letters, Digits, and *
    Page Should Contain    This field only accepts Letters, digits and '*'. Space is not allowed.

Attention, Unable To Process Request
    # This Keyword prompts the Warning sign to pop-up as the request was unable to process
    Page Should Contain   Sorry, the site is not responding or was unable to process your request. Please try again.

Cancel Report
    # This Keyword cancels the current report that you are working on, discarding all the changes
    Click Element    xpath=//section[@id='reportButtonsSection']//a[contains(text(),'Cancel')]
    Wait Until Element Is Visible    xpath=//span[contains(text(),'Yes, cancel now')]
    Click Element    xpath=//span[contains(text(),'Yes, cancel now')]

Click Save Report
    # This Keyword saves the report
    Sleep    3
    Click Element    xpath=//section[@id='reportButtonsSection']/a[contains(text(),'Save')]
    Sleep    3

Get New Report Id
    # This Keyword gets the new report id that was just completed
    ${new_report_id}    Get Element Attribute    xpath=//ul[@id='reportsList']/li[1]@id
    ${new_report_id}    Get Substring    ${new_report_id}  7
    Log To Console    Report ID is: ${new_report_id}
    Set Test Variable    ${report_id}    ${new_report_id}

Get Report With Id: "${report_id}"
    # This Keyword gets the report with the given report id if it is valid
    # ${report_id} - report id 
    Create HTTP Context     ${TEST_SERVER_URL}    https
    GET    /systemAdmin/reporting/viewReport.html?&repositoryID=${report_id}
    Response Status Code Should Equal    302
    ${body}=    Get Response Body
    Log To Console    ${body}

Include Archived Pay Stations: "${archived_pay_stations?}"
    # This Keywords asks if you want to include archived pay stations (Yes or No)
    # ${archived_pay_stations?} - asks you if you want to include it or not
    Should Be True    '${archived_pay_stations?}'=='Yes' or '${archived_pay_stations?}'=='No'
    Run Keyword If    '${archived_pay_stations?}'=='Yes'    Click Element    xpath=//a[@id='showHiddenPayStations:true']
    Run Keyword If    '${archived_pay_stations?}'=='No'    Click Element    xpath=//a[@id='showHiddenPayStations:false']
    Sleep  1

Input Coupon Code: "${coupon_code}"
    # This Keyword inputs the coupon code 
    # ${coupon_code} - counpon code number
    Input Text    css=#formCouponNumber    ${coupon_code}

Input Card Number: "${specific_card_number}"
    # This Keyword inputs the given specific card number when "Specific #" is selected for the Card Number field
    Click Element   formCardNumberTypeExpand
    Input Text   formCardNumberSpecific     ${specific_card_number}   

Input E-mail: "${e_mail}"
    Input Text    formNewEmail    ${e_mail}
    Click Element    addEmailBtn
    Wait Until Element Is Visible    xpath=//ul[@id='formCurrentEmailList']//span[contains(text(),'${e_mail}')]    10s

Input End Month: "${end_month}"
    # This Keyword inputs the end month
    # ${end_month} - end month
    Click Element    css=#formPrimaryMonthEnd
    Click Element    xpath=//ul[@id='ui-id-7']//a[contains(text(),'${end_month}')]

Input End Year: "${end_year}"
    # This Keyword inputs the end year
    # ${end_year} - end year
    Click Element    css=#formPrimaryYearEnd
    Click Element    xpath=//ul[@id='ui-id-8']//a[contains(text(),'${end_year}')]

Input First Space Number: "${first_space_number}"
    # This Keyword inputs the first space number (first blank)
    # ${first_space_number} - first blank for space number
    Input Text    css=#formSpaceNumberBegin    ${first_space_number}

Input First Ticket Number: "${first_ticket_number}"
    # This Keyword inputs the first ticket number 
    # ${first_ticket_number} - first ticket number
    Input Text    css=#formTicketNumberBegin    ${first_ticket_number}

Input License Plate: "${license_plate}"
    # This Keyword inputs the license plate number
    # ${license_plate} - license plate number
    Input Text    css=#formLicensePlate    ${license_plate}

Input Schedule Title: "${schedule_title}"
    # This Keyword inputs the given schedule title
    Input Text    reportTitle    ${schedule_title}

Input Second Space Number: "${second_space_number}"
    # This Keyword inputs the second space number (second blank)
    # ${second_space_number} - second blank for space number
    Input Text    css=#formSpaceNumberEnd    ${second_space_number}


Input Second Ticket Number: "${second_ticket_number}"
    # This Keyword inputs the second ticket number
    # ${second_ticket_number} - second ticket number
    Input Text    css=#formTicketNumberEnd    ${second_ticket_number}

Input Start Month: "${start_month}"
    # This Keyword inputs the start month
    # ${start_month} - start month
    Click Element    css=#formPrimaryMonthBegin
    Click Element    xpath=//ul[@id='ui-id-5']//a[contains(text(),'${start_month}')]

Input Start Year: "${start_year}"
    # This Keyword inputs the start year
    # ${start_year} - start month
    Click Element    css=#formPrimaryYearBegin
    Click Element    xpath=//ul[@id='ui-id-6']//a[contains(text(),'${start_year}')]

Proceed To Step 2
    # This Keyword clicks onto Step 2 when creating a report
    Click Link    css=#btnStep2Select
    Wait Until Keyword Succeeds    10s    1s    Element should Be Visible    css=#reportDetailsArea
    Sleep    1s

Proceed To Step 3
    # This Keyword clicks onto Step 3 when creating a report
    Click Link    css=#btnStep3Select
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    css=#reportSchedulingArea

Report "${report_type}" Is Pending
    # This Keyword indicates that the report is compiling the data together so it is pending
    # ${report_type} - type of report specified
    Element Should Be Visible    xpath=//ul[@id='queuedReportsList']/li[1]/div[@class='col1']/p[contains(text(),'${report_type}')]

Report "${report_type}" Is Completed
    # This Keyword indicates that the report has been processed and is now completed to review
    # ${report_type} - type of report specified
    Wait Until Keyword Succeeds    60s    1s
    ...    Run Keywords    Click Element    xpath=//h2[contains(text(),'Pending and Incomplete Reports')]/following-sibling::a[@id='opBtn_reloadReport']
    ...    AND    Element Should Be Visible    xpath=//ul[@id='reportsList']/li[1]//p[contains(text(),'${report_type}')]
    ...    AND    Element Should Be Visible    xpath=//ul[@id='reportsList']/li[1]//p[contains(text(),'just now')]
    Get New Report Id

Scheduled Report With Name: "${schedule_title}" Is Created
    Element Should Be Visible    xpath=//section[@id='scheduledReportsBox']//p[contains(text(),'${schedule_title}')]

Select All Pay Station Routes
    # This Keyword selects all the pay station routes avaliable
    Click Element    xpath=//section[@id='reportRoute']//label/a

Select All Pay Stations
    # This Keyword selects all the pay station avaliable
    Click Element    xpath=//label[@id='reportPayStationsSelAllBtn']/a

Select All Refined Pay Stations
    # This Keyword Selects all the Pay Stations listed through the filter when Pay Station is selected as the Location Type
    ${count}=  Get Matching Xpath Count    //section[@id='reportPayStations']//ul[@class='treepicker-list-full']/li
    :FOR    ${index}    IN RANGE    1    ${count}+1
    \    Click Element    //section[@id='reportPayStations']//ul[@class='treepicker-list-full']/li[${index}]

Select Approval Status: "${approval_status}"
    # This Keyword selects the approval statuss
    # ${approval_status} - approval status 
    Click Link    css=#formApprovalStatusExpand
    Click Element    xpath=//ul[@id='ui-id-26']//a[contains(text(),'${approval_status}')]

Select Boxed Location Type: "${check_location}"
    # This Keyword selects the specfic location type that you want 
    # ${check_location} - location that will be marked off and included
    Click Element    xpath=//strong[contains(text(),'${check_location}')]/../a

Select Card Number: "${card_number}"
    # This Keyword selects the card number
    # ${card_number} - card number
    Click Link    css=#formCardNumberTypeExpand
    Click Element    xpath=//ul[@id='ui-id-24']//a[contains(text(),'${card_number}')]

Select Card Type: "${card_type}"
    # This Keyword selects the card type
    # ${card_type} - card type can include VISA, MasterCard, etc...
    Click Link    css=#formCardTypeExpand
    Click Element    xpath=//ul[@id='ui-id-25']//a/span[contains(text(),'${card_type}')]

Select Collection Type: ${collection_count} - Count
    # This Keyword selects the Count option of either the Coin or Bill category
    # ${collection_count} - can be either the Coin or Bill category
    Click Element    css=#isFor${collection_count}CountCheck

Select Collection Type: ${collection_dollars} - Dollars
    # This Keyword selects the dollars option of either the Coin or Bill category
    # ${collection_dollars} - can be either the Coin or Bill category
    Click Element    css=#isFor${collection_dollars}AmountCheck    

Select Collection Type: All
    # This Keyword selects all the collection types available 
    Click Element    css=#isForCoinCountCheck
    Click Element    css=#isForCoinAmountCheck
    Click Element    css=#isForBillCountCheck
    Click Element    css=#isForBillAmountCheck
    Click Element    css=#isForCardCountCheck
    Click Element    css=#isForCardAmountCheck
    Click Element    css=#isForRunningTotalCheck
    Click Element    css=#isForOverdueCollectionCheck

Select Collection Type: Collection Interval - Overdue
    # This Keyword selects the overdue option of the collection interval
    Click Element    css=#isForOverdueCollectionCheck
    
Select Collection Type: Running Total - Dollars
    # This Keyword selects the dollars option of the running total
    Click Element    css=#isForRunningTotalCheck

Select Coupon Code: "${coupon_code}"
    # This Keyword selects the coupon code
    # ${coupon_code} - coupon code
    Click Link    css=#formCouponNumberTypeExpand
    Click Element    xpath=//ul[@id='ui-id-21']//a[contains(text(),'${coupon_code}')]

Select Coupon Code2
    [arguments]  ${selection}  ${specific_code}
    Select Coupon Code: "${coupon_cocde}"
    Run Keyword If  '${coupon_code}'=='Specific Code'  Input Coupon Code  ${specific_code}

Select Coupon Type: "${coupon_type}"
    # This Keyword selects the coupon type 
    # ${coupon_type} - coupon type
    Click Link    css=#formCouponTypeExpand
    Click Element    xpath=//ul[@id='ui-id-22']//a[contains(text(),'${coupon_type}')]

Select/Deselect All Locations 
    Click Element    xpath=//label[contains(.,'All Locations')]/a

Select Location Type: "${location_type}"
    # This Keyword selects the location type, make sure to not confuse with the options under the location type
    # You can select between location, pay station, and pay station route
    # ${location_type} - the type of location you want to see
    Click Link    css=#formLocationTypeExpand
    Click Element    xpath=//ul[@id='ui-id-16']//a[text()='${location_type}']

Select Group By: "${group_by}"
    # This Keyword selects the group by option
    # ${group_by} - group by
   Wait Until Element Is Visible    css=#formGroupByExpand    10s
    Click Link    css=#formGroupByExpand
    Wait Until Element Is Visible    xpath=//ul[@id='ui-id-31']//a[contains(text(),'${group_by}')]    10s
    Click Element    xpath=//ul[@id='ui-id-31']//a[contains(text(),'${group_by}')]

Select Location Type All: "${location_type}"
    # This Keyword selects all of the location type that you have given 
    # ${location_type} - the type of location that you want to select
    # For instance location, pay station, or pay station route
    Select Location Type: "${location_type}"
    Run Keyword If    '${location_type}'=='Pay Station Route'    Select All Pay Station Routes
    Run Keyword If    '${location_type}'=='Pay Station'    Select All Pay Stations

Select Merchant Account: "${merchant_account}"
    # This Keyword selects the merchant account
    # ${merchant_account} - merchant account number
    Click Link    css=#merchantAccountExpand
    Click Element    xpath=//ul[@id='ui-id-27']//a/span[contains(text(),'${merchant_account}')]

Select Order By: "${order_by}"
    # This Keyword selects the order by field
    # ${order_by} - 
    Click Link    css=#formSortByExpand
    Click Element    xpath=//ul[@id='ui-id-32']//a[contains(text(),'${order_by}')]

Select Other Report Type: "${other_report_type}"
    # This Keyword changes the stall status 
    # ${report_type} - report type that you want to view
    Click Link    css=#formOtherParametersExpand
    Click Element    xpath=//ul[@id='ui-id-30']//a[contains(text(),'${other_report_type}')]

Select Output Format: "${output_format}"
    # This Keyword selects the output form desired (PDF or CSV)
    # ${output_format} - the format you want the report to eb outputted 
    Should Be True    '${output_format}'=='PDF' or '${output_format}'=='CSV'
    Run Keyword If    '${output_format}'=='PDF'    Click Element    xpath=//label[@id='switchIsPDF']/a
    Run Keyword If    '${output_format}'=='CSV'    Click Element    xpath=//label[@id='switchIsNotPDF']/a

Select Pay Station: "${pay_station}"
    # This Keyword selects the pay stations with a specific pay station id
    # ${pay_station} - the number id of the pay station
    Click Element    xpath=//strong[contains(text(),'${pay_station}')]/../a

Select Pay Station Route: "${pay_station_route}"
    Click Element    xpath=//strong[contains(text(),'${pay_station_route}')]/../a

Select Repeat: "${repeat}"
    # This Keyword the Repeats field in scheduled reports ie. Daily, Weekly, Monthly
    Click Element    formRepeatTypeExpand
     Wait Until Element Is Visible    xpath=//ul[@id='ui-id-34']//a[contains(text(),'${repeat}')]    10s
    Click Element    xpath=//ul[@id='ui-id-34']//a[contains(text(),'${repeat}')]

Select Repeat On For Weekly
    # This keyword hard selects Tuesday and Thursday when "Weekly" is selected when scheduling a report
    Click Element  xpath=//section[@id='dayOfWeekSelector']//label[3]/a
    Click Element  xpath=//section[@id='dayOfWeekSelector']//label[5]/a

Select Repeat On For Monthly
    # This keyword hard selects days 2, 12, and 22 when "Monthly" is selected when scheduling a report
    Click Element  xpath=//ul[@class='dayCal']/li[text()='2']
    Click Element  xpath=//ul[@class='dayCal']/li[text()='12']
    Click Element  xpath=//ul[@class='dayCal']/li[text()='22']

Select Report Detail Type: "${detail_type}"
    # This Keyword selects the Report Detail type 
    # ${detail_type} - the details of the report you want, either Summary or Details
    Should Be True    '${detail_type}'=='Summary' or '${detail_type}'=='Details'
    Run Keyword If    '${detail_type}'=='Summary'    Click Element    xpath=//a[@id='isSummary:true']
    Run Keyword If    '${detail_type}'=='Details'    Click Element    xpath=//a[@id='isSummary:false']

Select Route: "${route_collections}"
    # This Keyword selects the route that you want
    # ${route_collections} - the route that will be selected for collections
    Click Element    xpath=//strong[contains(text(),'${route_collections}')]/../a

Select Schedule Date/Time
    # This Keyword selects the Date/Time for the report schedule
    # Note: This always picks the same day of the current month
    # NEED TO IMPLEMENT

Select Schedule Time: "${schedule_time}"
    # This Keyword selects the time that the report will be run
    Click Element    formScheduleStartTime
    Wait Until Element Is Visible    xpath=//ul[@id='ui-id-33']//a[contains(text(),'${schedule_time}')]    10s
    Click Element    xpath=//ul[@id='ui-id-33']//a[contains(text(),'${schedule_time}')]


Select Space Location: "${space_location}"
    # This Keyword selects the space location that needs to be changed
    # ${space_location} - Space Location that you want to see 
    Click Link    css=#spaceLocationExpand
    Click Element    xpath=//ul[@id='ui-id-19']//a[contains(text(),'${space_location}')]


Select Space Number: "${space_number}"
    # This Keyword selects the range of the space number
    # ${space_number} - enter No Stall, All, =, >=, <=, or between to specify the range 
    # of the space number that you want it to lie between
    Click Link    css=#formSpaceNumberTypeExpand
    Wait Until Element Is Visible    xpath=//ul[@id='ui-id-18']//a[contains(text(),'${space_number}')]    10s
    Click Element    xpath=//ul[@id='ui-id-18']//a[contains(text(),'${space_number}')]
    Sleep    1

Select Specific Pay Station Option: "${route_or_location}"
    # This Keyword will select the specific pay station option on the drop down menu
    # ${route_or_location} - the specific route or location that you want to view
    Click Element    css=#formPaystationsFilterExpand
    Click Element    xpath=//ul[@id='ui-id-17']//a[text()='${route_or_location}']

Select Stall Status: "${stall_status}"
    # This Keyword changes the stall status 
    # ${stall_status} - status of the stall that you want to view
    Click Link    css=#formOtherParametersExpand
    Click Element    xpath=//ul[@id='ui-id-30']//a[contains(text(),'${stall_status}')]

Select Ticket Number: "${ticket_number}"    
    # This Keyword selects the ticket number 
    # ${ticket_number} - ticket number 
    Click Link    css=#formTicketNumberTypeExpand
    Click Element    xpath=//ul[@id='ui-id-20']//a[contains(text(),'${ticket_number}')]

Select Ticket Number2
    # This Keyword selects and inputs the ticket number(s)
    [arguments]    ${selection}  ${first}  ${second}
    Select Ticket Number: "${selection}"

    Run Keyword If  '${selection}'=='Specific #'  Run Keywords
    ...  Input First Ticket Number: "${first}"

    Run Keyword If  '${selection}'=='Between'  Run Keywords
    ...  Input First Ticket Number: "${first}"
    ...  AND  Input Second Ticket Number: "${second}"



#  Always picks the same beginning and end dates
Select Transaction Date/Time
    # This Keyword selects the transaction date as specified and will always pick the 
    # same beginning and end dates
    Click Element    css=#formPrimaryDateBegin
    Wait Until Element Is Visible    xpath=//a[@title='Prev']    10s  
    Click Element    xpath=//a[@title='Prev']
    Wait Until Element Is Visible    xpath=//a[contains(text(),'13')]    10s
    Click Element    xpath=//a[contains(text(),'13')]
    Click Element    css=#formPrimaryTimeBegin
    Wait Until Element Is Visible    xpath=//ul[@id='ui-id-3']//a[contains(text(),'12:00 AM')]    10s
    Click Element    xpath=//ul[@id='ui-id-3']//a[contains(text(),'12:00 AM')]
    Click Element    css=#formPrimaryDateEnd
    Wait Until Element Is Visible    xpath=//a[@title='Prev']    10s
    Click Element    xpath=//a[@title='Prev']
    Wait Until Element Is Visible    xpath=//a[contains(text(),'14')]    10s
    Click Element    xpath=//a[contains(text(),'14')]
    Click Element    css=#formPrimaryTimeEnd
    Wait Until Element Is Visible    xpath=//ul[@id='ui-id-4']//a[contains(text(),'12:00 AM')]    10s
    Click Element    xpath=//ul[@id='ui-id-4']//a[contains(text(),'12:00 AM')]

Select Transaction Date/Time: "${date/time}"
    # This Keyword selects the date/time as you wish to input
    # ${date/time} - the time/date of the transaction you wish to view
    Click Link    css=#formPrimaryDateTypeExpand
    Click Element    xpath=//ul[@id='ui-id-2']//a[contains(text(),'${date/time}')]

Select Transaction Type: "${transaction_type}"
    # This Keyword selects the transaction type
    # ${transaction_type} - transaction type
    Click Link    css=#formOtherParametersExpand
    Click Element    xpath=//ul[@id='ui-id-30']//a[contains(text(),'${transaction_type}')]

Select Schedule: "${schedule}"
    # This Keyword selects the type of scheduling you want to happen
    # ${schedule} - can choose to schedule the report as 'Submit Now' or 'Schedule' and input a time
    Should Be True    '${schedule}'=='Submit Now' or '${schedule}'=='Schedule'
    Run Keyword If    '${schedule}'=='Submit Now'    Click Element    xpath=//a[@id='isScheduled:false']
    Run Keyword If    '${schedule}'=='Schedule'    Click Element    xpath=//a[@id='isScheduled:true']


View Report with Id: "${report_id}"
    # This Keyword views the report with the given report id if it is valid
    # ${report_id} - report id 
    Click Element    xpath=//li[@id='report_${report_id}']/a[@title='Option Menu']
    Wait Until Keyword Succeeds    10s    1s    Element Should Be Visible    xpath=//section[@id='menu_${report_id}']//a[@class='view']
    Click Element    xpath=//section[@id='menu_${report_id}']//a[@class='view']
