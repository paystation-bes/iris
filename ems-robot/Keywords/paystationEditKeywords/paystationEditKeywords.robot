*** Keywords ***
Click Pay Station Option Menu: "${serial}"
    Wait Until Element Is Visible    xpath=//a[@name='edit_${serial}']    10s
	Click Element    xpath=//a[@name='edit_${serial}']


Edit Pay Station Name
	[arguments]    ${serial}    ${new_name}
	Click Pay Station Option Menu: "${serial}"
	Wait Until Element Is Visible    formPayStationName    10s
	Clear Element Text    formPayStationName
	Input Text    formPayStationName    ${new_name}
	Click Save Pay Station Details


Click Save Pay Station Details
	Click Element    xpath=//a[@name='save']


Verify Pay Station Name In Pay Station Details
    [arguments]    ${new_name}
    Wait Until Element Is Visible    xpath=//h1[@id='paystationName' and contains(text(),'${new_name}')]    10s