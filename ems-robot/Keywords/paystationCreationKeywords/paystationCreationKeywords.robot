*** Settings ***
Documentation     A resource file with reusable keywords related to Iris Paystation Creation
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
...               Author: Johnson N
...               Version Number: 1.0
Library           Selenium2Library
Library           String
Resource          ../../data/data.robot


***Keywords***

Create Auto-generated Paystation For Type: "${paystation_type}"  
	# This keyword will create a paystation with auto-generated id for the paystation type, specified in ${paystation_type}. The specified pay station type must be valid and is case-sensitive 
	# [Arguments]:
	# ${paystation_type} - a valid pay station type. Example: Radius lUKE, SHELBY
	${serial_ID}=    Auto-generate Paystation ID For Paystation Type: "${paystation_type}"
	Create Paystation With Serial ID: "${serial_ID}"
	[return]    ${serial_ID}

Create Paystation With Serial ID: "${serial_number}"
	# This keyword will open up the form for adding pay stations, input the serial number specified in ${serial_number} and save paystation to customer. If successful, new paystation should appear under customer.
	# [Arguments]:
	# ${serial_number} - a pay station serial number
	Click Add Paystation(s) Button To Open Serial Number Form
	Add Paystation(s) Form Should Appear
	Enter Paystation Serial Number: "${serial_number}"
	Click 'Add Paystation' Button to add Paystations To Customer

Create Paystations: "${list_of_paystations}"
	# This keyword will open up the form for adding a list of pay stations, input the serial numbers specified in ${list_of_paystations} and save paystations to customer. If successful, new paystations should appear under customer.
	# [Arguments]:
	# ${list_of_paystations} - a reference to a list object containing valid pay station serial numbers 
	Click Add Paystation(s) Button To Open Serial Number Form
	Add Paystation(s) Form Should Appear
	Enter Paystation Serial Numbers: "${list_of_paystations}"
	Click 'Add Paystation' Button to add Paystations To Customer




# Minor low-level keywords, meant to translate selenium keywords of certain elements into a natural, easy-to-understand human language



Add Paystation(s) Form Should Appear
	# This keyword will simply verify that the Add Paystation(s) Form appears on the UI during Paystation creation. Will pass if visible, and fail if not
	Wait Until Element Is Visible    css=#formSerialNumbers    timeout=3
	Element Should Be Visible    css=#formSerialNumbers

All Recorded Paystation ID's Should Be Created And Mapped To The Correct Paystation Type: "${paystation_id_to_type_dictionary}"
	# This keyword will take the dictionary object and use the ID keys to verify that all paystations in the dictionary exist on the Pay station list, as well as is assigned the correct pay station type, according to it's mapped key value 
	# [Arguments]:
	# ${paystation_id_to_type_dictionary} - a dictionary object, holding pay station ID's as keys mapped to pay station type values.
	${items}=	Get Dictionary Items    ${paystation_id_to_type_dictionary}
	:FOR    ${id}   ${type}    IN    @{items}
	\    Paystation With ID: "${id}" Should Be Visible On Paystation List
    \    Paystation Serial ID: "${id}" Should Have Paystation Type: "${type}"

Attention! Popup Should Appear When Adding Long Paystation Serial ID: "${id}"
	# This keyword simply verifies that an Attention! popup appears with the approiate text when adding a serial number with a long (12+) serial ID
	# [Arguments]:
	# ${id} - ID of paystation being checked during attention popup
	Wait Until Element is Visible    xpath=//*[@id='messageResponseAlertBox']/section/article[contains(text(), 'The following errors occurred:')]/ul/li[contains(text(),'must be between 1 to 12 characters long.')]/strong[contains(text(), '${id}')]    timeout=3
    Close Message Response Alert Box

Attention! Popup Should Appear When Inputting Invalid Character in Add Paystation Form
	# This keyword simply verifies the appearance of the Attention! popup
	Wait Until Element is Visible    xpath=//*[@id='messageResponseAlertBox']/section/article[contains(text(), 'This field only accepts Letters and digits. Space is not allowed.')]    timeout=3
	Close Message Response Alert Box


Attention! Popup Should Appear When Adding Already Existing Paystation: "${id}"
	# This keyword simply verifies that an Attention! popup appears with the appropriate text when adding a serial number of an already existing paystation
	# [Arguments]:
	# ${id} - ID of paystation being checked during attention popup
    Wait Until Element is Visible    xpath=//*[@id='messageResponseAlertBox']/section/article[contains(text(), 'The following pay station(s) already exist: [${id}].')]   timeout=3
	Close Message Response Alert Box

Attention! Popup Should Appear When Adding Invalid Paystation: "${id}"
	# This keyword simply verifies that an Attention! popup appears with the appropriate text when adding an invalid serial number
	# [Arguments]:
	# ${id} - ID of paystation being checked during attention popup
    Wait Until Element is Visible    xpath=//*[@id='messageResponseAlertBox']/section/article[contains(text(), 'The following pay station(s) have an unknown pay station type: [${id}].')]  timeout=3
    Close Message Response Alert Box

Auto-generate Paystation ID For Paystation Type: "${paystation_type}"
	# Not to be confused with 'Create Auto-generated Paystation For Type: "${paystation_type}"', this keyword simply auto-generates a serial number based on the pay station type. Does not handle actual pay station creation
	# [Arguments]:
	# ${paystation_type} - a valid pay station type. Example: Radius lUKE, SHELBY	
	${sec}=    Get Time    epoch
	Sleep    1
	${type_ID}=    Set Variable If	'${paystation_type}' == 'V1 LUKE'    100000000000	'${paystation_type}' == 'SHELBY'    200000000000		'${paystation_type}' == 'Radius LUKE'    300000000000	'${paystation_type}' == 'LUKE II'    500000000000	'${paystation_type}' == 'FRODO'    600000000000		'${paystation_type}' == 'Test/Demo'    800000000000		'${paystation_type}' == 'Virtual'    900000000000
	${result}=    Evaluate    ${type_ID}+${sec}
	[return]    ${result}

Click 'Add Paystation' Button To Add Paystations To Customer
	# This is a helper keyword that will simply click the 'Add Pay Station' button that appears on the Pay Station Serial Number Form
	Click Element    xpath=//span[contains(text(), 'Add Pay Station') and @class='ui-button-text']

Click Add Paystation(s) Button To Open Serial Number Form
	# This is a helper keyword that will simply click the button on the Pay Station List that will enable a user to create new pay stations.
	Wait Until Element Is Visible    css=#btnAddPayStation    timeout=3
	Click Element    css=#btnAddPayStation

Enter Paystation Serial Number: "${paystation}"  
	# This is a helper keyword that will simply input the text in ${paystation} into the serial number form
	# [Arguments]:
	# ${paystation} - a paystation serial number	
	Wait Until Element Is Visible    css=#formSerialNumbers    timeout=2
	Input Text    css=#formSerialNumbers    ${paystation}

Enter Paystation Serial Numbers: "${list_of_paystations}"
	# This is a helper keyword that will simply input the list of paystations into the serial number form
	# [Arguments]:
	# ${list_of_paystations} - a list of paystation serial numbers	
	${paystations}=    Set Variable    ${EMPTY}
	:FOR    ${paystation}    IN     @{list_of_paystations}
	\    ${paystations}=    Catenate    SEPARATOR=    ${paystations}    ${paystation}
	\	 ${paystations}=    Catenate    SEPARATOR=    ${paystations}    \n
	Input Text    formSerialNumbers    ${paystations}
	
Generate ID's for Paystation Types: "${paystation_types}"
	# Not to be confused with 'Mass-generate "${number}" Paystation IDs For Paystation Type: "${paystation_type}"', this keyword takes in a list of pay station types and from that list, generates a serial ID for each pay station type in the list, and creates a dictionary object that maps those id's as keys to their pay station types as values. A list of paystation types can contain multiples of the same pay station type
	# [Arguments]:
	# ${paystation_types} - a list of paystation types
	${id_paystation_type_dict}=    Create Dictionary
	:FOR    ${type}    IN    @{paystation_types}
	\    ${serial}=    Auto-generate Paystation ID For Paystation Type: "${type}"
	\    ${serial}=    Convert To String    ${serial}
	\    Set To Dictionary    ${id_paystation_type_dict}    ${serial}    ${type}
	[return]    ${id_paystation_type_dict}


List Of Paystations "${list_of_paystations}" Should Be Created And Have Paystation Type: "${paystation_type}"
	# This keyword verifies that all pay stations specified in the list are created as well as have the correct pay station type. This keyword is designed more for mass-checking a huge list of pay stations with the same type
	# [Arguments]:
	# ${list_of_paystations} - a list of paystation serial numbers, should all have the same pay station type 
	# ${paystation_type} - Paystation type, such as SHELBY or LUKE II
	:FOR    ${id}    IN    @{list_of_paystations}
	\    Paystation With ID: "${id}" Should Be Visible On Paystation List
	\    Sleep    1
    \    Paystation Serial ID: "${id}" Should Have Paystation Type: "${paystation_type}"

Mass-generate "${number}" Paystation IDs For Paystation Type: "${paystation_type}"
	# Not to be confused with 'Generate ID's for Paystation Types: "${paystation_types}"', this keyword takes in a number, which represents the number of pay stations that the user wishes to create, and a pay station type, which will be the type for all the n pay stations that will be generated with this keyword. This will return a list of n paystation id's, all which will possess the same pay station type
	# [Arguments]:
	# ${number} - The number of paystation ids wished to be generated. Must be 1 or greater
	# ${paystation_type} - Paystation type, such as SHELBY or LUKE II, that will be used to determine which batch of pay station id's will be generated
	@{list_of_paystations}=    Create List
	:FOR    ${INDEX}    IN RANGE    0    ${number}
	\    ${serial}=    Auto-generate Paystation ID For Paystation Type: "${paystation_type}"
	\    ${serial}=    Convert To String    ${serial}
	\    Append To List    ${list_of_paystations}    ${serial}
	[return]    ${list_of_paystations}

Message Response Alert Box For Duplicate Paystation: "${duplicate}" Should Appear
	# This keyword simply verifies that an Attention! popup appears with the appropriate text when adding a collection of paystations where two paystations in the request have the same ID. Not to be confused with Attention! Popup Should Appear When Adding Already Existing Paystation: "${id}", this keyword should be called when trying to save two pay stations with the same ID in the Serial Number Form
	# [Arguments]: 
	# ${duplicate} - the id of a duplicate pay station
    Wait Until Element Is Visible    xpath=.//*[@id='messageResponseAlertBox']/section/article/strong//br[following-sibling::text()='The following pay station(s) already exist: [${duplicate}].']    timeout=3


Only One Paystation with ID: "${id}" Should Exist On The Paystation List
	# This keyword checks on the paystation list that there is only one pay station exists on a customer's pay station list and a duplicate shouldn't appear. Will fail if it turns out there are no paystations of that id on the list
	# [Arguments]: 
	# ${id} - - ID of paystation being checked on paystation list
	Xpath Should Match X Times    .//span[@class="textLine" and contains(text(), '${id}')]    1

Paystation With ID: "${id}" Should Be Visible On Paystation List
	# This keyword checks on the pay station list that the pay station id is visible on the list 
	# [Arguments]: 
	# ${id} - - ID of paystation being checked on paystation list
	Wait Until Element Is Visible    xpath=.//span[@class="textLine" and contains(text(), '${id}')]    timeout=3
	Element Should Be Visible	xpath=.//span[@class="textLine" and contains(text(), '${id}')]


Paystation With ID: "${id}" Should NOT Be On Paystation List
	# This keyword checks on the pay station list that the pay station id is not present on the list
	# [Arguments]: 
	# ${id} - - ID of paystation being checked on paystation list
	Page Should Not Contain Element	   xpath=.//span[@class="textLine" and contains(text(), '${id}')]

Paystation Serial ID: "${paystation_id}" Should Have Paystation Type: "${paystation_type}" 
	# This keyword verifies that the paystation specified in ${paystation_id} has expected paystation type ${paystation_type}
	
	Click Element    xpath=.//span[@class="textLine" and contains(text(), '${paystation_id}')]
	Sleep    1
	Wait Until Element Is Visible    css=#infoBtn > a.tab    timeout=7
	Click Element    css=#infoBtn > a.tab
	Wait Until Element Is Visible    xpath=//*[@id='infoArea']    timeout=7
	Element Should Contain    xpath=.//*[@id='infoArea']/dl/section[@id='psTypeLine']/dd[@id = 'psType']    ${paystation_type}

