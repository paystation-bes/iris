*** Keywords ***

Go To Reports Section
    Wait Until Element Is Visible  css=[name='Reports']>img  10s
    Click Element    css=[name='Reports']>img
    Wait Until Keyword Succeeds  10s  1s  Element Should Be Visible  xpath=//h1[contains(text(),'Reports')]
    Wait Until Keyword Succeeds  10s  1s  Element Should Be Visible  css=#btnAddScheduleReport

Go To Accounts Section
    Wait Until Element Is Visible  css=[title='Accounts']>img  10s
    # Click Element    css=[name='Accounts']>img (used for dev)
    Click Element    css=[title='Accounts']>img
    Wait Until Keyword Succeeds  10s  1s  Element Should Be Visible  xpath=//h1[contains(text(),'Customer Accounts')]
    Wait Until Keyword Succeeds  10s  1s  Element Should Be Visible  css=#opBtn_addConsumer

Go To Settings Section
    Wait Until Element Is Visible  css=[name='Settings']>img  10s
    Click Element    css=[name='Settings']>img
    Wait Until Element Is Visible  xpath=//h1[contains(text(),'Settings')]  10s
    Wait Until Element Is Visible  xpath=//a[@name='Alerts']  10s
    Wait Until Element Is Visible  xpath=//a[@name='Pay Stations']  10s

# The following Keywords are for clicking tabs

Click ${tab_name} Tab
    Wait Until Element Is Visible      xpath=//a[@class='tab' and @name='${tab_name}']  5s
    Click Element  xpath=//a[@class='tab' and @name='${tab_name}']

Click ${tab_name} Tab 1
    # for dev 8474 section
    Wait Until Element Is Visible      xpath=//a[@class='tab' and @title='${tab_name}']  5s
    Click Element  xpath=//a[@class='tab' and @title='${tab_name}']

Go To '${section_name}' Section (Left)
    Wait Until Element Is Visible    xpath=//a[@name='${section_name}' or @title='${section_name}']
    Click Element                    xpath=//a[@name='${section_name}' or @title='${section_name}']/img
    Run Keyword If                   '${section_name}'=='Dashboard'     Wait Until Element Is Visible       xpath=//a[contains(text(),'Dashboard')]/img
    ...    ELSE                      Wait Until Element Is Visible    xpath=//h1[contains(text(),'${section_name}')]