*** Settings ***


*** Keywords ***
Add Banned Card
	[arguments]    ${card_type}    ${card_number}    ${card_expiry_date}    ${comment}
	Select Banned Card Type: "${card_type}"
	Input Banned Card Number: "${card_number}"
	Run Keyword If     '${card_type}'=='Credit Card'    Input Banned Card Expiry Date: "${card_expiry_date}"
	Click Save Banned Card

Click Add/Import Banned Card: "${add_or_import}"
	Click Banned Cards Tab 1
	Wait Until Element Is Visible    xpath=//h2[text()='Banned Card']/..//a[@title='Option Menu']
	Click Element 					 xpath=//h2[text()='Banned Card']/..//a[@title='Option Menu']
	Wait Until Element Is Visible    xpath=//a[text()='${add_or_import}']
	Click Element				     xpath=//a[text()='${add_or_import}']
	Sleep    2

# ====================================================================================================
Select Banned Card Type: "${card_type}"
	Wait Until Element Is Visible    css=#formBannedCardTypeExpand    timeout=3
	Click Element    css=#formBannedCardTypeExpand
	Wait Until Element Is Visible    xpath=//a[text()='${card_type}']
	Click Element    xpath=//a[text()='${card_type}']

Input Banned Card Number: "${card_number}"
	Clear Text    css=#formBannedCardNumber
	Input Text    css=#formBannedCardNumber    ${card_number}

Input Banned Card Expiry Date: "${date}"
	Input Text    css=#formBannedCardExpiryLabel    ${date}

Input Banned Card Comment: "${comment}"
	Clear Text    css=#formBannedCardComment
	Input Text    css=#formBannedCardComment    ${comment}

Click Save Banned Card
	Click Element     xpath=//span[text()='Save']
# ====================================================================================================
Add Widget
	Wait Until Element Is Visible    xpath=//a[@title='Add Widget']    timeout=3
	Click Element    xpath=//a[@title='Add Widget']

Add Widget Window Pops Up
	Page Should Contain    Pay Stations with Alerts
	Page Should Contain    Alerts by Type
	Page Should Contain    Alerts by Location (P)
	Page Should Contain    Alerts by Location
	Page Should Contain    Alerts by Route

Cancel Adding Widget
	Wait Until Element Is Visible    xpath=//span[text()='Cancel']/..    timeout=3
	Click Element    xpath=//span[text()='Cancel']/..

Cancel Adding Alert In Settings
	Wait Until Element Is Visible     xpath=//h2[text()='Alerts']/../../../..//a[text()='Cancel']
	Click Element     				  xpath=//h2[text()='Alerts']/../../../..//a[text()='Cancel']

Cancel Changes On Dashboard
	Wait Until Element Is Visible    xpath=//a[text()='Cancel']    timeout=3
	Click Element    xpath=a[text()='Cancel']
	Alert Message Should Appear
	Click Element    xpath=span[text()='Yes, Cancel Now']

Cancel Import Banned Card
	Click Element    xpath=//span[text()='Import Banned Cards']/../following-sibling::div[1]//span[text()='Cancel']


Click Add Button
	Wait Until Element Is Visible    xpath=//a[@title='Add']
	Click Element    				 xpath=//a[@title='Add']

Click Rates SubTab: Rates
	Wait Until Element Is Visible    xpath=//a[@href="/secure/settings/rates/rateDetails.html?"]
	Click Element                    xpath=//a[@href="/secure/settings/rates/rateDetails.html?"]

Click Sub-Tab: "${sub_tab}"
	Wait Until Element Is Visible    xpath=//a[text()="${sub_tab}"]
	Click Element 	  			     xpath=//a[text()="${sub_tab}"]

Edit In Dashboard
	Wait Until Element Is Visible    xpath=//a[@title='Edit' and text()='Edit']    timeout=3
	Click Element    //a[@title='Edit' and text()='Edit']
	Sleep   2

Edit Flex Server Credentials
	Wait Until Element Is Visible    css=#btnEditFlexCredentials     timeout=3
	Click Element    				 css=#btnEditFlexCredentials

Header Should Be Visible: "${header_text}"
	Wait Until Element Is Visible    xpath=//h2[text()='${header_text}']

Should Be Visible: "${text}"
	Wait Until Element Is Visible    xpath=//span[text()='${text}']    timeout=3

Table Info Should Be Visible: "${table_info}"
	Wait Until Element Is Visible    xpath=//p[contains(text(),'${table_info}')]

Zoom On Map Should Be Visible
	Wait Until Element Is Visible    xpath=//a[@title="Zoom in"]
	Wait Until Element Is Visible   xpath=//a[@title="Zoom out"]
