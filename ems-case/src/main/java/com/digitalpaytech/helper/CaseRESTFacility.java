package com.digitalpaytech.helper;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CaseRESTFacility {
    
    @JsonProperty("facility_id")
    private Integer facilityId;
    @JsonProperty("facility_name")
    private String facilityName;
    @JsonProperty("facility_active")
    private int facilityActive;
    @JsonProperty("owner_id")
    private Integer ownerId;
    @JsonProperty("owner_active")
    private int ownerActive;
    @JsonProperty("owner_name")
    private String ownerName;
    @JsonProperty("account_id")
    private Integer accountId;
    @JsonProperty("account_name")
    private String accountName;
    @JsonProperty("account_active")
    private int accountActive;
    
    private Boolean isActive;
    
    public final Integer getFacilityId() {
        return this.facilityId;
    }
    
    public final void setFacilityId(final Integer facilityId) {
        this.facilityId = facilityId;
    }
    
    public final String getFacilityName() {
        return this.facilityName;
    }
    
    public final void setFacilityName(final String facilityName) {
        this.facilityName = facilityName;
    }
    
    public final int getFacilityActive() {
        return this.facilityActive;
    }
    
    public final int isFacilityActive() {
        return this.facilityActive;
    }
    
    public final void setFacilityActive(final int facilityActive) {
        this.facilityActive = facilityActive;
    }
    
    public final Integer getOwnerId() {
        return this.ownerId;
    }
    
    public final void setOwnerId(final Integer ownerId) {
        this.ownerId = ownerId;
    }
    
    public final int getOwnerActive() {
        return this.ownerActive;
    }
    
    public final void setOwnerActive(final int ownerActive) {
        this.ownerActive = ownerActive;
    }
    
    public final String getOwnerName() {
        return this.ownerName;
    }
    
    public final void setOwnerName(final String ownerName) {
        this.ownerName = ownerName;
    }
    
    public final Integer getAccountId() {
        return this.accountId;
    }
    
    public final void setAccountId(final Integer accountId) {
        this.accountId = accountId;
    }
    
    public final String getAccountName() {
        return this.accountName;
    }
    
    public final void setAccountName(final String accountName) {
        this.accountName = accountName;
    }
    
    public final int getAccountActive() {
        return this.accountActive;
    }
    
    public final void setAccountActive(final int accountActive) {
        this.accountActive = accountActive;
    }
    
    public final boolean getIsActive() {
        if (this.isActive == null) {
            this.isActive = this.facilityActive == 1;
        }
        return this.isActive;
    }
    
    public final void setIsActive(final Boolean isActive) {
        this.isActive = isActive;
    }
    
}
