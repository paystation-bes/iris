package com.digitalpaytech.helper;

import java.util.Date;
import java.util.TimeZone;

import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CaseRESTAccount {
    
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("version")
    private String version;
    @JsonProperty("active")
    private int active;
    @JsonProperty("contact_id")
    private Integer contactId;
    @JsonProperty("date_created")
    private String dateCreated;
    @JsonProperty("description")
    private String description;
    @JsonProperty("last_updated")
    private String lastUpdated;
    
    private Date lastUpdatedGMT;
    private Date dateCreatedGMT;
    private Boolean isActive;
    
    public final Integer getId() {
        return this.id;
    }
    
    public final void setId(final Integer id) {
        this.id = id;
    }
    
    public final String getVersion() {
        return this.version;
    }
    
    public final void setVersion(final String version) {
        this.version = version;
    }
    
    public final int getActive() {
        return this.active;
    }
    
    public final void setActive(final int active) {
        this.active = active;
    }
    
    public final Integer getContactId() {
        return this.contactId;
    }
    
    public final void setContactId(final Integer contactId) {
        this.contactId = contactId;
    }
    
    public final String getDateCreated() {
        return this.dateCreated;
    }
    
    public final void setDateCreated(final String dateCreated) {
        this.dateCreated = dateCreated;
    }
    
    public final String getDescription() {
        return this.description;
    }
    
    public final void setDescription(final String description) {
        this.description = description;
    }
    
    public final String getLastUpdated() {
        return this.lastUpdated;
    }
    
    public final void setLastUpdated(final String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
     
    
    public final Date getDateCreatedGMT() {
        if (this.dateCreatedGMT == null) {
            this.dateCreatedGMT = DateUtil.parse(this.dateCreated, DateUtil.DATABASE_FORMAT, TimeZone.getTimeZone(WebCoreConstants.GMT));
        }
        return this.dateCreatedGMT;
    }
    
    public final void setDateCreatedGMT(final Date dateCreatedGMT) {
        this.dateCreatedGMT = dateCreatedGMT;
    }
    
    public final Date getLastUpdatedGMT() {
        if (this.lastUpdatedGMT == null) {
            this.lastUpdatedGMT = DateUtil.parse(this.lastUpdated, DateUtil.DATABASE_FORMAT, TimeZone.getTimeZone(WebCoreConstants.GMT));
        }
        return this.lastUpdatedGMT;
    }
    
    public final void setLastUpdatedGMT(final Date lastUpdatedGMT) {
        this.lastUpdatedGMT = lastUpdatedGMT;
    }
    
    public final Boolean getIsActive() {
        if (this.isActive == null) {
            this.isActive = this.active == 1;
        }
        return this.isActive;
    }
    
    public final void setIsActive(final Boolean isActive) {
        this.isActive = isActive;
    }
    
    
}
