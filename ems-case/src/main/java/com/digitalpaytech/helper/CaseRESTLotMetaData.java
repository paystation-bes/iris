package com.digitalpaytech.helper;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class CaseRESTLotMetaData implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -5450581461381254617L;
        
    @JacksonXmlProperty(localName = "lotid")
    private Integer lotId;
    
    @JacksonXmlProperty(localName = "facility")
    private String facility;
    
    @JacksonXmlProperty(localName = "lotname")
    private String lotName;
    
    @JacksonXmlProperty(localName = "totalspaces")
    private Integer totalSpaces;
    
    @JacksonXmlProperty(localName = "latitude")
    private Double latitude;
    
    @JacksonXmlProperty(localName = "longitude")
    private Double longitude;

    public final Integer getLotId() {
        return this.lotId;
    }

    public final void setLotId(final Integer lotId) {
        this.lotId = lotId;
    }

    public final String getFacility() {
        return this.facility;
    }

    public final void setFacility(final String facility) {
        this.facility = facility;
    }

    public final String getLotName() {
        return this.lotName;
    }

    public final void setLotName(final String lotName) {
        this.lotName = lotName;
    }

    public final Integer getTotalSpaces() {
        return this.totalSpaces;
    }

    public final void setTotalSpaces(final Integer totalSpaces) {
        this.totalSpaces = totalSpaces;
    }

    public final Double getLatitude() {
        return this.latitude;
    }

    public final void setLatitude(final Double latitude) {
        this.latitude = latitude;
    }

    public final Double getLongitude() {
        return this.longitude;
    }

    public final void setLongitude(final Double longitude) {
        this.longitude = longitude;
    }
    
}
