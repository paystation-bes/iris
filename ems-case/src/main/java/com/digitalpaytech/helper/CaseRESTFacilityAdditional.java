package com.digitalpaytech.helper;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CaseRESTFacilityAdditional {
    @JsonProperty("facility_id")
    private Integer facilityId;
    @JsonProperty("owner_id")
    private Integer ownerId;
    @JsonProperty("contact_id")
    private Integer contactId;
    @JsonProperty("time_zone")
    private String timeZone;
    @JsonProperty("active")
    private String active;
    @JsonProperty("description")
    private String description;
    
    private Boolean isActive;

    public final Integer getFacilityId() {
        return this.facilityId;
    }

    public final void setFacilityId(final Integer facilityId) {
        this.facilityId = facilityId;
    }

    public final Integer getOwnerId() {
        return this.ownerId;
    }

    public final void setOwnerId(final Integer ownerId) {
        this.ownerId = ownerId;
    }

    public final Integer getContactId() {
        return this.contactId;
    }

    public final void setContactId(final Integer contactId) {
        this.contactId = contactId;
    }

    public final String getTimeZone() {
        return this.timeZone;
    }

    public final void setTimeZone(final String timeZone) {
        this.timeZone = timeZone;
    }

    public final String getActive() {
        return this.active;
    }

    public final void setActive(final String active) {
        this.active = active;
    }

    public final String getDescription() {
        return this.description;
    }

    public final void setDescription(final String description) {
        this.description = description;
    }
      
    public final boolean getIsActive() {
        if (this.isActive == null) {
            this.isActive = "true".equals(this.active);
        }
        return this.isActive;
    }
    
    public final void setIsActive(final Boolean isActive) {
        this.isActive = isActive;
    }
}
