package com.digitalpaytech.helper;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import com.digitalpaytech.service.TimeService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class CaseRESTLotOccupancy implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 5244848476083767236L;
    
    @JacksonXmlProperty(localName = "lotid")
    private Integer lotid;
    
    @JacksonXmlProperty(localName = "occ")
    private Integer occ;
    
    @JacksonXmlProperty(localName = "ts")
    private String ts;
    
    private Date tsGMT;
    
    private Integer tsGMTBucketId;
    
    public final Integer getLotid() {
        return this.lotid;
    }
    
    public final void setLotid(final Integer lotid) {
        this.lotid = lotid;
    }
    
    public final Integer getOcc() {
        if (this.occ < 0) {
            this.occ = 0;
        }
        return this.occ;
    }
    
    public final void setOcc(final Integer occ) {
        this.occ = occ;
    }
    
    public final String getTs() {
        return this.ts;
    }
    
    public final void setTs(final String ts) {
        this.ts = ts;
    }
    
    public final Date getTsGMT() {
        if (this.tsGMT == null) {
            this.tsGMT = DateUtil.parse(this.ts, DateUtil.DATABASE_FORMAT, WebCoreConstants.GMT);
        }
        return this.tsGMT;
    }
    
    public final void setTsGMT(final Date tsGMT) {
        this.tsGMT = tsGMT;
    }
    
    public final int getTsGMTBucketId(final TimeService timeService) {
        if (this.tsGMTBucketId == null) {
            final Calendar tsCalendar = Calendar.getInstance();
            tsCalendar.setTime(getTsGMT());
            this.tsGMTBucketId = timeService.findBucket(tsCalendar).getId();
        }
        return this.tsGMTBucketId;
    }
    
    public final void setTsGMTBucketId(final Integer tsGMTBucketId) {
        this.tsGMTBucketId = tsGMTBucketId;
    }
    
}
