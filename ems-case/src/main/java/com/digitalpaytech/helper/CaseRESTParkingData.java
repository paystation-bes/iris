package com.digitalpaytech.helper;

import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "parkingmeta")
public class CaseRESTParkingData {
    @JacksonXmlProperty(localName = "lotocc")
    private List<CaseRESTLotOccupancy> lotOccupancy;
    
    public final List<CaseRESTLotOccupancy> getLotOccupancy() {
        return this.lotOccupancy;
    }
    
    @SuppressWarnings("unused")
    private void setLotOccupancy(final List<CaseRESTLotOccupancy> lotOccupancy) {
        this.lotOccupancy = lotOccupancy;
    }
}
