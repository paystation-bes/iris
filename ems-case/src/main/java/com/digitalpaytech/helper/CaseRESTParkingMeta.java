package com.digitalpaytech.helper;

import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "parkingmeta")
public class CaseRESTParkingMeta {
    
    @JacksonXmlProperty(localName = "lotmeta")
    private List<CaseRESTLotMetaData> lotmeta;
    
    public final List<CaseRESTLotMetaData> getLotmeta() {
        return this.lotmeta;
    }
    
    @SuppressWarnings("unused")
    private void setLotmeta(final List<CaseRESTLotMetaData> lotmeta) {
        final List<CaseRESTLotMetaData> lotmetaCopy = lotmeta;
        this.lotmeta = lotmetaCopy;
    }
}
