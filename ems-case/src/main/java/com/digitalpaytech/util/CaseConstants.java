package com.digitalpaytech.util;

public final class CaseConstants {
    public static final String ACCOUNTS_URL = "/caseparking/api/public/api/account/summary";
    public static final String FACILITIES_URL = "/caseparking/api/public/api/account/collection";
    public static final String LOTMETA_URL = "/caseparking/api/public/api/counts/lotmeta";
    public static final String LOTOCC_URL = "/caseparking/api/public/api/counts/lotocc";
    public static final String FACILITY_URL = "/caseparking/api/public/api/facility/settings?id_facility=";
    
    public static final String HEADER_AUTHORIZATION = "Authorization";
    
    public static final String TYPE_ACCOUNTS = "accounts";
    public static final String TYPE_FACILITIES = "facilities";
    public static final String TYPE_LOT_META = "lotmeta";
    public static final String TYPE_LOT_OCC = "lotocc";
    public static final String TYPE_LOT_FACILITY = "facility";
    
    public static final Byte WS_TYPE_CITATION_WIDGET = 1;

    private CaseConstants() {
    }
}
