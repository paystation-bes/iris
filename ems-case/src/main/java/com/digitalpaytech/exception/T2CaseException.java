package com.digitalpaytech.exception;

public class T2CaseException extends Exception {
    
    /**
     * 
     */
    private static final long serialVersionUID = 8997745930449083387L;

    public T2CaseException(final String msg) {
        super(msg);
    }
    
    public T2CaseException(final String msg, final Throwable t) {
        super(msg, t);
    }
    
    public T2CaseException(final Throwable t) {
        super(t);
    }
}
