package com.digitalpaytech.exception;

public class CaseRESTException extends Exception {
    
    /**
     * 
     */
    private static final long serialVersionUID = 6540842584478455903L;
    
    public CaseRESTException(final String msg) {
        super(msg);
    }
    
    public CaseRESTException(final String msg, final Throwable t) {
        super(msg, t);
    }
    
    public CaseRESTException(final Throwable t) {
        super(t);
    }
}
