package com.digitalpaytech.exception;

public class CaseException extends Exception {
 
    /**
     * 
     */
    private static final long serialVersionUID = 6540842584478455903L;

    public CaseException(final String msg) {
        super(msg);
    }
    
    public CaseException(final String msg, final Throwable t) {
        super(msg, t);
    }
    
    public CaseException(final Throwable t) {
        super(t);
    }
}
