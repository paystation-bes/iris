package com.digitalpaytech.service;

import java.util.Collection;
import java.util.List;

import com.digitalpaytech.helper.CaseRESTAccount;
import com.digitalpaytech.helper.CaseRESTFacility;
import com.digitalpaytech.helper.CaseRESTLotMetaData;
import com.digitalpaytech.helper.CaseRESTLotOccupancy;

public interface CaseRESTService {
    
    Collection<CaseRESTAccount> findAllAccounts();
    
    Collection<CaseRESTFacility> findAllFacilities();
    
    //    CaseRESTLotMetaData[] findAllLotMetaData();
    //    
    //    CaseRESTLotOccupancy[] findAllLotOccupancy();
    List<CaseRESTLotMetaData> findAllLotMetaData();
    
    List<CaseRESTLotOccupancy> findAllLotOccupancy();
    
    //TODO
    String findCaseCustomerFacilityTimeZoneByCaseFacilityId(Integer facilityId);

    Integer findLotOccupancyByLotId(Integer lotId);
    
}
