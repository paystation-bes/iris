package com.digitalpaytech.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.CaseCustomerAccount;
import com.digitalpaytech.domain.CaseCustomerFacility;
import com.digitalpaytech.domain.CaseLocationLot;
import com.digitalpaytech.helper.CaseRESTAccount;
import com.digitalpaytech.helper.CaseRESTFacility;
import com.digitalpaytech.helper.CaseRESTLotMetaData;
import com.digitalpaytech.service.CaseRESTService;
import com.digitalpaytech.service.CaseService;
import com.digitalpaytech.service.CaseSynchronizationHelper;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebSecurityConstants;

@Component("caseSynchronizationHelper")
//PMD.UseConcurrentHashMap: Not necessary 
@SuppressWarnings({ "PMD.UseConcurrentHashMap" })
public class CaseSynchronizationHelperImpl implements CaseSynchronizationHelper {
    
    @Autowired
    private CaseService caseService;
    
    @Autowired
    private CaseRESTService caseRESTService;
    
    public final CaseService getCaseService() {
        return this.caseService;
    }
    
    public final CaseRESTService getCaseRESTService() {
        return this.caseRESTService;
    }
    
    public final void setCaseService(final CaseService caseService) {
        this.caseService = caseService;
    }
    
    public final void setCaseRESTService(final CaseRESTService caseRESTService) {
        this.caseRESTService = caseRESTService;
    }
    
    @Override
    //PMD.AvoidInstantiatingObjectsInLoops required
    @SuppressWarnings({ "PMD.AvoidInstantiatingObjectsInLoops" })
    public final void syncCaseAccounts() {
        final Collection<CaseRESTAccount> caseAccounts = this.caseRESTService.findAllAccounts();
        for (CaseRESTAccount caseRESTAccount : caseAccounts) {
            CaseCustomerAccount caseCustomerAccount = this.caseService.findCaseCustomerAccountByCaseAccountId(caseRESTAccount.getId());
            if (caseCustomerAccount == null) {
                caseCustomerAccount = new CaseCustomerAccount(null, caseRESTAccount.getId(), caseRESTAccount.getDescription(),
                        caseRESTAccount.getIsActive(), caseRESTAccount.getLastUpdatedGMT(), DateUtil.getCurrentGmtDate(),
                        WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
                this.caseService.saveCaseCustomerAccount(caseCustomerAccount);
            } else if (caseCustomerAccount.getLastUpdatedGmt().before(caseRESTAccount.getLastUpdatedGMT())) {
                caseCustomerAccount.setCaseAccountName(caseRESTAccount.getDescription());
                caseCustomerAccount.setLastUpdatedGmt(caseRESTAccount.getLastUpdatedGMT());
                caseCustomerAccount.setIsActive(caseRESTAccount.getIsActive());
                caseCustomerAccount.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
                caseCustomerAccount.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
                this.caseService.updateCaseCustomerAccount(caseCustomerAccount);
            }
        }
    }
    
    @Override
    public final void syncCaseFacilities() {
        final List<CaseCustomerAccount> caseCustomerAccountList = this.caseService.findAssignedCaseCustomerAccount();
        if (caseCustomerAccountList != null && !caseCustomerAccountList.isEmpty()) {
            final Collection<CaseRESTFacility> caseFacilities = this.caseRESTService.findAllFacilities();
            processCaseRESTFacilities(caseFacilities, caseCustomerAccountList);
        }
    }
    
    @Override
    public final void syncCaseLots() {
        final List<CaseCustomerAccount> caseCustomerAccountList = this.caseService.findAssignedCaseCustomerAccount();
        if (caseCustomerAccountList != null && !caseCustomerAccountList.isEmpty()) {
            final List<CaseRESTLotMetaData> caseLotList = this.caseRESTService.findAllLotMetaData();
            processCaseRESTLots(caseLotList, caseCustomerAccountList);
        }
    }
    
    //PMD.AvoidInstantiatingObjectsInLoops required
    @SuppressWarnings({ "PMD.AvoidInstantiatingObjectsInLoops" })
    private void processCaseRESTFacilities(final Collection<CaseRESTFacility> caseFacilities, final List<CaseCustomerAccount> caseCustomerAccountList) {
        final Map<Integer, List<CaseRESTFacility>> facilityAccountMapping = new HashMap<Integer, List<CaseRESTFacility>>();
        
        for (CaseRESTFacility caseRESTFacility : caseFacilities) {
            if (!facilityAccountMapping.containsKey(caseRESTFacility.getAccountId())) {
                facilityAccountMapping.put(caseRESTFacility.getAccountId(), new ArrayList<CaseRESTFacility>());
            }
            facilityAccountMapping.get(caseRESTFacility.getAccountId()).add(caseRESTFacility);
        }
        
        for (CaseCustomerAccount caseCustomerAccount : caseCustomerAccountList) {
            final List<CaseRESTFacility> caseRESTFacilityList = facilityAccountMapping.get(caseCustomerAccount.getCaseAccountId());
            if (caseRESTFacilityList == null) {
                continue;
            }
            for (CaseRESTFacility caseRESTFacility : caseRESTFacilityList) {
                CaseCustomerFacility caseCustomerFacility = this.caseService.findCaseCustomerFacilityByCaseFacilityId(caseRESTFacility
                        .getFacilityId());
                final String caseFacilityTimeZone = this.caseRESTService.findCaseCustomerFacilityTimeZoneByCaseFacilityId(caseRESTFacility
                        .getFacilityId());
                
                if (caseCustomerFacility == null) {
                    caseCustomerFacility = new CaseCustomerFacility(caseCustomerAccount.getCustomer(), caseCustomerAccount,
                            caseRESTFacility.getFacilityId(), caseRESTFacility.getFacilityName(), caseRESTFacility.getIsActive(),
                            caseFacilityTimeZone, DateUtil.getCurrentGmtDate(), WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
                    this.caseService.saveCaseCustomerFacility(caseCustomerFacility);
                } else {
                    caseCustomerFacility.setCaseCustomerAccount(caseCustomerAccount);
                    caseCustomerFacility.setCustomer(caseCustomerAccount.getCustomer());
                    caseCustomerFacility.setFacilityName(caseRESTFacility.getFacilityName());
                    caseCustomerFacility.setIsActive(caseRESTFacility.getIsActive());
                    caseCustomerFacility.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
                    caseCustomerFacility.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
                    caseCustomerFacility.setTimeZone(caseFacilityTimeZone);
                    this.caseService.updateCaseCustomerFacility(caseCustomerFacility);
                }
                caseFacilities.remove(caseRESTFacility);
            }
        }
        
        //cleanup facilities not assigned to a customer
        for (CaseRESTFacility caseRESTFacility : caseFacilities) {
            final CaseCustomerFacility caseCustomerFacility = this.caseService.findCaseCustomerFacilityByCaseFacilityId(caseRESTFacility
                    .getFacilityId());
            if (caseCustomerFacility != null) {
                caseCustomerFacility.setCustomer(null);
                caseCustomerFacility.setFacilityName(caseRESTFacility.getFacilityName());
                caseCustomerFacility.setIsActive(caseRESTFacility.getIsActive());
                caseCustomerFacility.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
                caseCustomerFacility.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
                this.caseService.updateCaseCustomerFacility(caseCustomerFacility);
            }
        }
    }
    
    //PMD.AvoidInstantiatingObjectsInLoops required
    //Checkstyle cyclomatic/NPath compexity: 3 tier loop is necessary because lots are linked to accounts through facilities
    @SuppressWarnings({ "checkstyle:cyclomaticcomplexity", "checkstyle:npathcomplexity", "PMD.AvoidInstantiatingObjectsInLoops" })
    private void processCaseRESTLots(final Collection<CaseRESTLotMetaData> caseLotList, final List<CaseCustomerAccount> caseCustomerAccountList) {
        final Map<String, List<CaseRESTLotMetaData>> lotFacilityMapping = new HashMap<String, List<CaseRESTLotMetaData>>();
        
        for (CaseRESTLotMetaData caseRESTLotMetaData : caseLotList) {
            if (!lotFacilityMapping.containsKey(caseRESTLotMetaData.getFacility())) {
                lotFacilityMapping.put(caseRESTLotMetaData.getFacility(), new ArrayList<CaseRESTLotMetaData>());
            }
            lotFacilityMapping.get(caseRESTLotMetaData.getFacility()).add(caseRESTLotMetaData);
        }
        
        for (CaseCustomerAccount caseCustomerAccount : caseCustomerAccountList) {
            final List<CaseCustomerFacility> caseCustomerFacilityList = this.caseService.findCaseCustomerFacilityByCustomerId(caseCustomerAccount
                    .getCustomer().getId());
            if (caseCustomerFacilityList == null || caseCustomerFacilityList.isEmpty()) {
                continue;
            }
            
            for (CaseCustomerFacility caseCustomerFacility : caseCustomerFacilityList) {
                final List<CaseRESTLotMetaData> caseRESTLotMetaDataList = lotFacilityMapping.get(caseCustomerFacility.getFacilityName());
                if (caseRESTLotMetaDataList == null) {
                    continue;
                }
                for (CaseRESTLotMetaData caseRESTLotMetaData : caseRESTLotMetaDataList) {
                    CaseLocationLot caseLocationLot = this.caseService.findCaseLocationLotByLotId(caseRESTLotMetaData.getLotId());
                    if (caseLocationLot == null) {
                        caseLocationLot = new CaseLocationLot(caseCustomerFacility, caseCustomerFacility.getCustomer(),
                                caseRESTLotMetaData.getLotId(), caseRESTLotMetaData.getLotName(), caseCustomerFacility.isIsActive(),
                                caseRESTLotMetaData.getTotalSpaces(), new BigDecimal(caseRESTLotMetaData.getLatitude()), new BigDecimal(
                                        caseRESTLotMetaData.getLongitude()), DateUtil.getCurrentGmtDate(),
                                WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
                        this.caseService.saveCaseLocationLot(caseLocationLot);
                    } else {
                        if (caseLocationLot.getCustomer() != null
                            && (caseCustomerFacility.getCustomer() == null || caseLocationLot.getCustomer().getId().intValue() != caseCustomerFacility
                                    .getCustomer().getId().intValue())) {
                            caseLocationLot.setLocation(null);
                        }
                        caseLocationLot.setCustomer(caseCustomerFacility.getCustomer());
                        caseLocationLot.setLatitude(new BigDecimal(caseRESTLotMetaData.getLatitude()));
                        caseLocationLot.setLongitude(new BigDecimal(caseRESTLotMetaData.getLongitude()));
                        caseLocationLot.setLotName(caseRESTLotMetaData.getLotName());
                        caseLocationLot.setTotalSpaces(caseRESTLotMetaData.getTotalSpaces());
                        caseLocationLot.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
                        caseLocationLot.setLastModifiedGmt(DateUtil.getCurrentGmtDate());

                        this.caseService.updateCaseLocationLot(caseLocationLot);
                    }
                    caseLotList.remove(caseRESTLotMetaData);
                }
            }
        }
        
        //cleanup lots not assigned to a customer
        for (CaseRESTLotMetaData caseRESTLotMetaData : caseLotList) {
            final CaseLocationLot caseLocationLot = this.caseService.findCaseLocationLotByLotId(caseRESTLotMetaData.getLotId());
            if (caseLocationLot != null) {
                caseLocationLot.setCustomer(null);
                caseLocationLot.setLocation(null);
                caseLocationLot.setLatitude(new BigDecimal(caseRESTLotMetaData.getLatitude()));
                caseLocationLot.setLongitude(new BigDecimal(caseRESTLotMetaData.getLongitude()));
                caseLocationLot.setLotName(caseRESTLotMetaData.getLotName());
                caseLocationLot.setTotalSpaces(caseRESTLotMetaData.getTotalSpaces());
                caseLocationLot.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
                caseLocationLot.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
                this.caseService.updateCaseLocationLot(caseLocationLot);
            }
        }
    }
}
