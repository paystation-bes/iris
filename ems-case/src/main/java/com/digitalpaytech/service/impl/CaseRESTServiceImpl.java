package com.digitalpaytech.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.digitalpaytech.helper.CaseRESTAccount;
import com.digitalpaytech.helper.CaseRESTFacility;
import com.digitalpaytech.helper.CaseRESTFacilityAdditional;
import com.digitalpaytech.helper.CaseRESTLotMetaData;
import com.digitalpaytech.helper.CaseRESTLotOccupancy;
import com.digitalpaytech.helper.CaseRESTParkingData;
import com.digitalpaytech.helper.CaseRESTParkingMeta;
import com.digitalpaytech.service.CaseRESTService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.CaseConstants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
@Service("caseRESTService")
public class CaseRESTServiceImpl implements CaseRESTService {
    
    public static final Logger LOGGER = Logger.getLogger(CaseRESTServiceImpl.class);
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    public final EmsPropertiesService getEmsPropertiesService() {
        return this.emsPropertiesService;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final Collection<CaseRESTAccount> findAllAccounts() {
        return (Collection<CaseRESTAccount>) getRESTRequestJSON(this.emsPropertiesService.getPropertyValue(EmsPropertiesService.CASE_URL,
                                                                                                           EmsPropertiesService.DEFAULT_CASE_URL)
                                                                        + CaseConstants.ACCOUNTS_URL, CaseConstants.TYPE_ACCOUNTS);
        
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final Collection<CaseRESTFacility> findAllFacilities() {
        return (Collection<CaseRESTFacility>) getRESTRequestJSON(this.emsPropertiesService.getPropertyValue(EmsPropertiesService.CASE_URL,
                                                                                                            EmsPropertiesService.DEFAULT_CASE_URL)
                                                                         + CaseConstants.FACILITIES_URL, CaseConstants.TYPE_FACILITIES);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<CaseRESTLotMetaData> findAllLotMetaData() {
        return (List<CaseRESTLotMetaData>) getRESTRequestXML(this.emsPropertiesService.getPropertyValue(EmsPropertiesService.CASE_URL,
                                                                                                        EmsPropertiesService.DEFAULT_CASE_URL)
                                                                     + CaseConstants.LOTMETA_URL, CaseConstants.TYPE_LOT_META);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<CaseRESTLotOccupancy> findAllLotOccupancy() {
        return (List<CaseRESTLotOccupancy>) getRESTRequestXML(this.emsPropertiesService.getPropertyValue(EmsPropertiesService.CASE_URL,
                                                                                                         EmsPropertiesService.DEFAULT_CASE_URL)
                                                                      + CaseConstants.LOTOCC_URL, CaseConstants.TYPE_LOT_OCC);
    }
    
    @Override
    public final String findCaseCustomerFacilityTimeZoneByCaseFacilityId(final Integer facilityId) {
        final CaseRESTFacilityAdditional[] facilityAdditional = getRESTRequestJSONArray(this.emsPropertiesService.getPropertyValue(EmsPropertiesService.CASE_URL,
                                                                                                                                   EmsPropertiesService.DEFAULT_CASE_URL)
                                                                                                + CaseConstants.FACILITY_URL + facilityId,
                                                                                        CaseConstants.TYPE_LOT_FACILITY);
        if (facilityAdditional.length > 0) {
            return getTimeZone(facilityAdditional[0]);
        }
        return null;
    }
    
    private CaseRESTFacilityAdditional[] getRESTRequestJSONArray(final String url, final String type) {
        final ResponseEntity<String> response = getResponse(url);
        CaseRESTFacilityAdditional[] list = null;
        
        try {
            if ("facility".equals(type)) {
                final CaseRESTFacilityAdditional[] facility = new ObjectMapper().readValue(response.getBody(), CaseRESTFacilityAdditional[].class);
                
                list = facility;
            }
            
        } catch (IOException e) {
            LOGGER.error(e);
        }
        
        return list;
    }
    
    private String getTimeZone(final CaseRESTFacilityAdditional facility) {
        return facility.getTimeZone();
    }
    
    private List<?> getRESTRequestXML(final String url, final String type) {
        
        final ResponseEntity<String> response = getResponse(url);
        
        final JacksonXmlModule module = new JacksonXmlModule();
        module.setDefaultUseWrapper(false);
        final XmlMapper xmlMapper = new XmlMapper(module);
        List<?> list = new ArrayList<Object>();
        
        try {
            if ("lotmeta".equals(type)) {
                final CaseRESTParkingMeta lotmetaData = xmlMapper.readValue(response.getBody(), CaseRESTParkingMeta.class);
                list = lotmetaData.getLotmeta();
                
            } else if ("lotocc".equals(type)) {
                final CaseRESTParkingData lotoccData = xmlMapper.readValue(response.getBody(), CaseRESTParkingData.class);
                list = lotoccData.getLotOccupancy();
            }
            
        } catch (IOException e) {
            LOGGER.error(e);
        }
        
        return list;
    }
    
    @SuppressWarnings("PMD.UseConcurrentHashMap")
    private Collection<?> getRESTRequestJSON(final String url, final String type) {
        
        final ResponseEntity<String> response = getResponse(url);
        Collection<?> list = null;
        
        try {
            if ("accounts".equals(type)) {
                final Map<String, CaseRESTAccount> accounts = new ObjectMapper()
                        .readValue(response.getBody(), new TypeReference<ConcurrentHashMap<String, CaseRESTAccount>>() {
                        });
                list = accounts.values();
                
            } else if ("facilities".equals(type)) {
                final Map<String, CaseRESTFacility> accounts = new ObjectMapper()
                        .readValue(response.getBody(), new TypeReference<ConcurrentHashMap<String, CaseRESTFacility>>() {
                        });
                
                list = accounts.values();
            }
            
        } catch (IOException e) {
            LOGGER.error(e);
        }
        
        return list;
    }
    
    private ResponseEntity<String> getResponse(final String url) {
        
        final RestTemplate restTemplate = new RestTemplate();
        final SimpleClientHttpRequestFactory rf = (SimpleClientHttpRequestFactory) restTemplate.getRequestFactory();
        final int timeOut = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.CASE_REQUEST_TIMEOUT_TIME,
                                                                            EmsPropertiesService.DEFAULT_CASE_REQUEST_TIMEOUT_TIME);
        rf.setReadTimeout(timeOut);
        rf.setConnectTimeout(timeOut);
        final HttpHeaders headers = new HttpHeaders();
        headers.set(CaseConstants.HEADER_AUTHORIZATION, this.emsPropertiesService.getPropertyValue(EmsPropertiesService.CASE_AUTHORIZATION,
                                                                                                   EmsPropertiesService.DEFAULT_CASE_AUTHORIZATION));
        @SuppressWarnings({ "unchecked", "rawtypes" })
        final HttpEntity entity = new HttpEntity(headers);
        return restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        
    }

    @Override
    public Integer findLotOccupancyByLotId(Integer lotId) {
        // TODO Auto-generated method stub
        final List<CaseRESTLotOccupancy> caseLotOcc = (List<CaseRESTLotOccupancy>) getRESTRequestXML(this.emsPropertiesService
                                                                                                     .getPropertyValue(EmsPropertiesService.CASE_URL,
                                                                                                         EmsPropertiesService.DEFAULT_CASE_URL)
                                                                      + CaseConstants.LOTOCC_URL, CaseConstants.TYPE_LOT_OCC);
        if (caseLotOcc.size() > 0) {
            return caseLotOcc.get(0).getOcc();
        }
        return 0;
    }
    
}
