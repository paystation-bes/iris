package com.digitalpaytech.service;

public interface CaseSynchronizationHelper {
    
    void syncCaseAccounts();
    
    void syncCaseFacilities();
    
    void syncCaseLots();
}
