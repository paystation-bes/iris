package com.digitalpaytech.helper.impl;

import org.apache.log4j.Logger;
import org.junit.Test;
import static org.junit.Assert.*;

import com.digitalpaytech.dto.EmbeddedTxObject;
import com.digitalpaytech.dto.cps.CPSResponse;
import com.digitalpaytech.dto.paystation.CardEaseData;
import com.digitalpaytech.dto.paystation.TransactionDto;

public class TransactionHelperImplTest {
    private static final Logger LOG = Logger.getLogger(TransactionHelperImplTest.class);
    private static final String CARD_EASE_DATA 
        = "{  \"Transaction type\":\"SALE\",  \"TID\":\"99962778\",  \"Card type\":\"ICC\",  \"Total\":\"USD 0.50\",  \"CVM\":\"NO CARDHOLDER VERIFICATION\",  \"Err code\":\"0000\",  \"Date\":\"19/09/16\",  \"Time\":\"13\",  \"Ref\":\"b3c61e0b-a97e-e611-8e66-0050569228c2\",  " + 
               "\"Auth Code\":\"F54ADC\",  \"Acquirer\":\"VISA\",  \"Merchant\":\"1234567890\",  \"Pan\":\"xxxxxxxxxxxx2222\",  \"Pan Seq No\":\"09\",  \"Application\":\"VISA DEBIT02\",  \"AID\":\"A0000000031010\",  \"82\":\"5C00\",  \"95\":\"0000008000\",  \"9A\":\"160919\"," + 
               "\"9F21\":\"1338\",  \"9C\":\"00\",  \"9F02\":\"000000000050\",  \"9F10\":\"06010A03600000\",  \"9F27\":\"40\",  \"9F1A\":\"0826\",  \"5F2A\":\"840\",  \"9F26\":\"3ED598761548ABD5\",  \"9F36\":\"0001\",  \"9F37\":\"75681564\",  \"9B\":\"F800\",  \"9F09\":\"008C\"," + 
               "  \"9F33\":\"6008C8\",  \"9F34\":\"1F0002\",  \"9F35\":\"25\",  \"CardReference\":\"63f6011b-4264-e411-b898-001422187e37\",  \"CardHash\":\"o+YsS+1XvFgx9x8AcnqWuWNwkDw=\"  }";
    
    private static final String CPS_RESP
        = "{ \"chargeToken\":\"ea8f1db9-esd8-42dd-b978-a98b8206ccdf\","
                + "\"authorizationNumber\":\"095308\", "
                + "\"cardType\":\"VISA\", "
                + "\"last4Digits\":\"8291\", "
                + "\"referenceNumber\":\"000007\", "
                + "\"processorTransactionId\":\"00000001\", "
                + "\"processedDate\":\"2017-10-04T15:42:28.892Z\" }";
            
    @Test
    public void testCardEaseData() {
        final TransactionDto tdto = new TransactionDto();
        tdto.setCardEaseData(CARD_EASE_DATA);
        
        final TransactionHelperImpl helper = new TransactionHelperImpl();
        final EmbeddedTxObject txObj = helper.createEmbeddedTxObject(tdto);
        assertNotNull(txObj);
        
        final CardEaseData data = (CardEaseData) txObj;
        assertNotNull(data);

        LOG.info("data is: \r\n " + data);
        assertNotNull(data.getApplication());
    }
    
    @Test
    public void testCPSResponse() {
        final TransactionDto tdto = new TransactionDto();
        tdto.setCPSResponse(CPS_RESP);
        
        final TransactionHelperImpl helper = new TransactionHelperImpl();
        final EmbeddedTxObject txObj = helper.createEmbeddedTxObject(tdto);
        assertNotNull(txObj);
        
        final CPSResponse cps = (CPSResponse) txObj;
        assertNotNull(cps);
        
        LOG.debug(cps + ", chargeToken: " + cps.getChargeToken());
        assertNotNull(cps.getChargeToken());
    }
}
