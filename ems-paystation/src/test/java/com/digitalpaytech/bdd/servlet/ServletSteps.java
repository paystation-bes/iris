package com.digitalpaytech.bdd.servlet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import java.util.HashMap;

import org.jbehave.core.annotations.AfterScenario;
import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.mockito.Mockito;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.mock.web.MockMultipartHttpServletRequest;
import org.springframework.ui.ModelMap;

import com.digitalpaytech.bdd.services.TransactionFileUploadServiceMockImpl;
import com.digitalpaytech.bdd.util.AbstractSteps;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandlers;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.TimezoneVId;
import com.digitalpaytech.domain.TransactionFileUpload;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.TransactionFileUploadService;
import com.digitalpaytech.service.ServiceAgreementService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.impl.CustomerServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.impl.ServiceAgreementServiceImpl;
import com.digitalpaytech.servlet.UploadServlet;
import com.digitalpaytech.servlet.DownloadServlet;
import com.digitalpaytech.servlet.LinuxConfigurationServlet;
import com.digitalpaytech.servlet.PosBaseServlet;

public final class ServletSteps extends AbstractSteps {
    /*
     * A file must be created for calculating the checksum
     * It the path has to be set in every place that the test writer could switch the file name
     */
    private String filePath = "./src/test/resources/";
    
    public ServletSteps(final JBehaveTestHandlers testHandlers) {
        super(testHandlers);
    }
    
    @BeforeScenario
    public void setUpForServlet() {
        final ControllerFieldsWrapper controllerFields = new ControllerFieldsWrapper();
        controllerFields.setRequest(new MockMultipartHttpServletRequest());
        controllerFields.setResponse(new MockHttpServletResponse());
        controllerFields.setModel(new ModelMap());
        controllerFields.setSession(new MockHttpSession());
        controllerFields.getRequest().setSession(controllerFields.getSession());
        TestDBMap.saveControllerFieldsWrapperToMem(controllerFields);
        TestLookupTables.getExpressionParser().register(new Class<?>[] { Customer.class, TimezoneVId.class, TransactionFileUpload.class, });
    }
    
    @AfterScenario
    public void teardownForServlet() {
        final File file = new File(this.filePath);
        file.delete();
    }
    
    /**
     * Asserts that a file does not exist in the database
     * 
     * @.example Given the File named *555555555555_1501119151750.xml.hpz* does not exist in the database
     * @param fileName
     *            the name to search for
     */
    @Given("the File named $fileName does not exist in the database")
    public void fileNameDoesNotExist(final String fileName) {
        //set the file path here 
        this.filePath = this.filePath.contains(fileName) ? this.filePath : this.filePath + fileName;
        writeFile(this.filePath);
        //search for story file's file name
        TransactionFileUpload file = (TransactionFileUpload) TestDBMap.findObjectByFieldFromMemory(fileName, TransactionFileUpload.ALIAS_FILE_NAME,
                                                                                                   TransactionFileUpload.class);
        Assert.assertNull(file);
        //search for full path file name to double check
        file = (TransactionFileUpload) TestDBMap.findObjectByFieldFromMemory(this.filePath, TransactionFileUpload.ALIAS_FILE_NAME,
                                                                             TransactionFileUpload.class);
        Assert.assertNull(file);
    }
    
    /**
     * Asserts that a file does exist in the database
     * 
     * @.example Given the File named *555555555555_1501119151750.xml.hpz* exists in the database
     * @param fileName
     *            the name to search for
     */
    @Given("the File named $fileName exists in the database")
    public void fileNameExists(final String fileName) {
        //Set file path 
        this.filePath = this.filePath.contains(fileName) ? this.filePath : this.filePath + fileName;
        writeFile(this.filePath);
        final TransactionFileUpload file = (TransactionFileUpload) TestDBMap
                .findObjectByFieldFromMemory(fileName, TransactionFileUpload.ALIAS_FILE_NAME, TransactionFileUpload.class);
        Assert.assertNotNull(file);
        //update file name to be full path 
        file.setFileName(this.filePath);
    }
    
    /**
     * Uploads the file to Iris for validation
     * 
     * @.example When Iris verifies *555555555555_1501119151750.xml.hpz*
     * @param fileName
     *            the name of the file to be uploaded
     */
    @When("Iris verifies $fileName")
    public void irisVerifiesFileName(final String fileName) {
        //set file path
        this.filePath = this.filePath.contains(fileName) ? this.filePath : this.filePath + fileName;
        final UploadServlet uploadServlet = new UploadServlet();
        final Method updateDuplicatedTransactionFile;
        Boolean result = null;
        try {
            updateDuplicatedTransactionFile =
                    uploadServlet.getClass().getDeclaredMethod("updateDuplicatedTransactionFile", Customer.class, String.class);
            //testing a private method
            updateDuplicatedTransactionFile.setAccessible(true);
            final Customer customer = (Customer) TestDBMap.findObjectByFieldFromMemory("Mackenzie Parking", "Customer Name", Customer.class);
            final TransactionFileUploadService transactionFileUploadService = Mockito.mock(TransactionFileUploadService.class);
            Mockito.when(transactionFileUploadService.findTransactionFileUpload(Mockito.eq(customer), Mockito.eq(this.filePath), Mockito.anyString()))
                    .thenAnswer(TransactionFileUploadServiceMockImpl.findTransactionFileUpload());
            Mockito.doAnswer(TransactionFileUploadServiceMockImpl.updateTransactionFileUpload()).when(transactionFileUploadService)
                    .updateTransactionFileUpload(Mockito.any(TransactionFileUpload.class));
            
            uploadServlet.setTransactionFileUploadService(transactionFileUploadService);
            //Invoke method with full path file name
            result = (Boolean) updateDuplicatedTransactionFile.invoke(uploadServlet, customer, this.filePath);
        } catch (IllegalAccessException | NoSuchMethodException | SecurityException | IllegalArgumentException | InvocationTargetException e) {
            Assert.fail(e.toString());
        }
        final ControllerFieldsWrapper fieldsWrapper = TestDBMap.getControllerFieldsWrapperFromMem();
        fieldsWrapper.setBooleanResult(result);
    }
    
    @When("the $serialNumber sends pay station settings request")
    public void getPaystationSettingsFile(final String serialNumber) {
        final TestContext ctx = TestContext.getInstance();
        
        final DownloadServlet downloadServlet = new DownloadServlet();
        try {
            final EntityDaoTest entityDao = new EntityDaoTest();
            final PointOfSaleService pointOfSaleService = new PointOfSaleServiceImpl();
            final ServiceAgreementService serviceAgreementService = new ServiceAgreementServiceImpl();
            final CustomerService customerService = new CustomerServiceImpl();
            final PosBaseServlet posBaseServlet = Mockito.mock(PosBaseServlet.class);
            
            ctx.autowires(entityDao, pointOfSaleService, serviceAgreementService, customerService, posBaseServlet, downloadServlet);
            
            final PointOfSale pos = pointOfSaleService.findPointOfSaleBySerialNumber(serialNumber);
            final Customer customer = customerService.findCustomer(pos.getCustomer().getId());
            
            final Map<String, String> params = new HashMap<String, String>();
            params.put("type", "lotsetting");
            params.put("commaddress", serialNumber);
            
            final MockHttpServletRequest req = new MockHttpServletRequest();
            req.addParameters(params);
            
            downloadServlet.doGet(req, TestDBMap.getControllerFieldsWrapperFromMem().getResponse());
            
        } catch (IOException e) {
            Assert.fail(e.toString());
        }
    }
    
    @Then("$file is sent")
    public void thenFileIsSent() {
        final ControllerFieldsWrapper fieldsWrapper = TestDBMap.getControllerFieldsWrapperFromMem();
        Assert.assertTrue(fieldsWrapper.getResponse().getContentLength() > 0);
    }
    
    /**
     * Asserts that Iris responds correctly to a file that does not exist in the database
     * 
     * @.example Then the file is saved
     * 
     */
    @Then("the file is saved")
    public void thenFileIsSaved() {
        final ControllerFieldsWrapper fieldsWrapper = TestDBMap.getControllerFieldsWrapperFromMem();
        final Boolean result = fieldsWrapper.getBooleanResult();
        Assert.assertFalse(result);
    }
    
    @Then("the servlet response has status code $code")
    public void responseReceived(final int code) {
        final ControllerFieldsWrapper fieldsWrapper = TestDBMap.getControllerFieldsWrapperFromMem();
        final MockHttpServletResponse result = fieldsWrapper.getResponse();
        Assert.assertEquals(code, result.getStatus());
    }
    
    /**
     * Asserts that Iris responds correctly to a file that already exists in the database
     * 
     * @.example Then the file is acknowledged
     * 
     */
    @Then("the file is acknowledged")
    public void thenFileIsAcknowledged() {
        final ControllerFieldsWrapper fieldsWrapper = TestDBMap.getControllerFieldsWrapperFromMem();
        final Boolean result = fieldsWrapper.getBooleanResult();
        Assert.assertTrue(result);
    }
    
    private void writeFile(final String path) {
        try {
            final FileOutputStream outputStream = new FileOutputStream(path, false);
            outputStream.write(1);
            outputStream.close();
        } catch (IOException e) {
            Assert.fail(e.toString());
        }
    }
}
