package com.digitalpaytech.bdd.rpc.ps2;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.rpc.ps2.LotSettingSuccessHandler;
import com.digitalpaytech.util.kafka.KafkaBaseSteps;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class SendPaystationLotSettingSuccessStories extends AbstractStories {
    public SendPaystationLotSettingSuccessStories() {
        super();
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
        TestContext
                .getInstance()
                .getObjectParser()
                .register(new Class[] { LotSettingSuccessHandler.class });
    }
    
    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(),
                                        new BaseSteps(this.testHandlers),
                                        new PaystationCommunicationSteps(this.testHandlers),
                                        new KafkaBaseSteps(this.testHandlers));
    }
}
