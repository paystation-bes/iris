package com.digitalpaytech.bdd.rpc.ps2;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.AlertSteps;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.dto.paystation.EmvTelemetryDto;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class SendPaystationTelemetryStories extends AbstractStories {
    public SendPaystationTelemetryStories() {
        super();
        
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
        
        TestContext.getInstance().getObjectParser().register(new Class[] { Customer.class, PointOfSale.class, EmvTelemetryDto.class});
    }
    
    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new PaystationCommunicationSteps(this.testHandlers), new AlertSteps(this.testHandlers));
    }

}
