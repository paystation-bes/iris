package com.digitalpaytech.bdd.rpc.ps2.transactionhandler;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.beanstream.Gateway;
import com.beanstream.exceptions.BeanstreamApiException;
import com.beanstream.requests.CardPaymentRequest;
import com.digitalpaytech.bdd.rpc.ps2.util.TestXMLRPCBehavior;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.cardprocessing.cps.dto.ProcessingDestinationServiceImpl;
import com.digitalpaytech.cardprocessing.impl.CardProcessingManagerImpl;
import com.digitalpaytech.cardprocessing.impl.CardProcessorFactoryImpl;
import com.digitalpaytech.client.CardTypeClient;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.dao.impl.OpenSessionDaoTest;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.SubscriptionType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.exception.CardProcessorPausedException;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.SystemException;
import com.digitalpaytech.ribbon.mock.MockClientFactory;
import com.digitalpaytech.rpc.ps2.AuthorizeCardRequestHandler;
import com.digitalpaytech.rpc.ps2.PS2XMLBuilder;
import com.digitalpaytech.rpc.ps2.PS2XMLBuilderBase;
import com.digitalpaytech.service.CcFailLogService;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.CustomerSubscriptionService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.cps.impl.CoreCPSServiceImpl;
import com.digitalpaytech.service.crypto.CryptoAlgorithmFactory;
import com.digitalpaytech.service.impl.CardTypeServiceImpl;
import com.digitalpaytech.service.impl.CcFailLogServiceImpl;
import com.digitalpaytech.service.impl.CommonProcessingServiceImpl;
import com.digitalpaytech.service.impl.CreditCardTypeServiceImpl;
import com.digitalpaytech.service.impl.CustomerAdminServiceImpl;
import com.digitalpaytech.service.impl.CustomerAgreementServiceImpl;
import com.digitalpaytech.service.impl.CustomerCardServiceImpl;
import com.digitalpaytech.service.impl.CustomerCardTypeServiceImpl;
import com.digitalpaytech.service.impl.CustomerServiceImpl;
import com.digitalpaytech.service.impl.CustomerSubscriptionServiceImpl;
import com.digitalpaytech.service.impl.EmsPropertiesServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.LinkCardTypeServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.service.impl.PaymentTypeServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.impl.PreAuthServiceImpl;
import com.digitalpaytech.service.impl.ProcessorServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.service.impl.WebWidgetHelperServiceImpl;
import com.digitalpaytech.service.kpi.KPIService;
import com.digitalpaytech.service.kpi.impl.KPIListenerServiceImpl;
import com.digitalpaytech.service.processor.TDMerchantCommunicationService;
import com.digitalpaytech.service.processor.impl.TestTDMerchantCommunicationServiceMockImpl;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.impl.TransactionFacadeImpl;

import junit.framework.Assert;

@SuppressWarnings({ "PMD.ExcessiveImports", "PMD.TooManyFields", "PMD.UnusedPrivateField" })
public class TestPreAuthXMLRPCTransactions implements TestXMLRPCBehavior {
    
    private static final String MESSAGE_NUMBER = "1";
    
    @Mock
    private CryptoService cryptoService;
    
    @InjectMocks
    private CustomerServiceImpl customerService;
    
    @InjectMocks
    private PointOfSaleServiceImpl pointOfSaleService;
    
    @InjectMocks
    private CustomerAgreementServiceImpl customerAgreementService;
    
    @Mock
    private CryptoAlgorithmFactory cryptoAlgorithmFactory;
    
    @InjectMocks
    private EntityServiceImpl entityService;
    
    @InjectMocks
    private OpenSessionDaoTest openSessionDao;
    
    @InjectMocks
    private CardTypeServiceImpl cardTypeService;
    
    @InjectMocks
    private CustomerCardServiceImpl customerCardService;
    
    @Spy
    @InjectMocks
    private CustomerCardTypeServiceImpl customerCardTypeService;
    
    @InjectMocks
    private CustomerSubscriptionServiceImpl customerSubscriptionService;
    
    @Mock
    private CustomerSubscriptionService customerSubscriptionServiceMock;
    
    @InjectMocks
    private CardProcessingManagerImpl cardProcessingManager;
    
    @InjectMocks
    private CustomerCardTypeServiceImpl cardCardTypeService;
    
    @InjectMocks
    private TransactionFacadeImpl transactionFacade;
    
    @InjectMocks
    private CreditCardTypeServiceImpl creditCardTypeService;
    
    @InjectMocks
    private MerchantAccountServiceImpl merchantAccountService;
    
    @InjectMocks
    private CommonProcessingServiceImpl commonProcessingService;
    
    @Spy
    @InjectMocks
    private LinkCardTypeServiceImpl linkCardTypeServiceImpl;
    
    @Mock
    private MailerService mailerService;
    
    @Mock
    private KPIService kpiService;
    
    @InjectMocks
    private KPIListenerServiceImpl kpiListnerService;
    
    @InjectMocks
    private CustomerAdminServiceImpl customerAdminService;
    
    @InjectMocks
    private WebWidgetHelperServiceImpl webWidgetHelperService;
    
    @InjectMocks
    private EmsPropertiesServiceImpl emsPropertiesService;
    
    @Mock
    private TDMerchantCommunicationService tdMerchantCommunicationService;
    
    @InjectMocks
    private ProcessorServiceImpl processorService;
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @Mock
    private MessageHelper messages;
    
    @InjectMocks
    private CardProcessorFactoryImpl cardProcessorFactory;
    
    @InjectMocks
    private CcFailLogServiceImpl ccFailLogService;
    
    @Mock
    private CcFailLogService ccFailLogServiceMock;
    
    @InjectMocks
    private PreAuthServiceImpl preAuthServiceMock;
    
    @InjectMocks
    private PaymentTypeServiceImpl paymentTypeService;
    
    @InjectMocks
    private CoreCPSServiceImpl coreCPSService;
    
    @InjectMocks
    private ProcessingDestinationServiceImpl processingDestinationService;
    
    private final AuthorizeCardRequestHandler authorizationHandler;
    
    private MockClientFactory client = MockClientFactory.getInstance();
    
    public TestPreAuthXMLRPCTransactions() throws BeanstreamApiException, CardProcessorPausedException {
        MockitoAnnotations.initMocks(this);
        
        Mockito.doReturn(new ArrayList<>()).when(this.linkCardTypeServiceImpl).getCollapsedBinRanges();
        Mockito.doNothing().when(this.linkCardTypeServiceImpl).getBinRangesFromCardTypeService();
        
        
        this.authorizationHandler = new AuthorizeCardRequestHandler(MESSAGE_NUMBER);
        
        TestContext.getInstance().autowiresAttributes(this);
        
        when(this.tdMerchantCommunicationService.sendPreAuthRequest(Mockito.any(Gateway.class), Mockito.any(CardPaymentRequest.class)))
                .thenAnswer(TestTDMerchantCommunicationServiceMockImpl.sendPreAuthRequest());
        
        try {
            when(this.cryptoService.encryptData(Mockito.anyInt(), Mockito.anyString())).thenAnswer(new Answer<String>() {
                
                @Override
                public String answer(final InvocationOnMock invocation) throws Throwable {
                    return (String) invocation.getArguments()[1];
                }
            });
        } catch (CryptoException e) {
            e.printStackTrace();
        }
        try {
            when(this.cryptoService.decryptData(Mockito.anyString(), Mockito.anyInt(), Mockito.any(Date.class))).thenAnswer(new Answer<String>() {
                
                @Override
                public String answer(final InvocationOnMock invocation) throws Throwable {
                    final String cardData = (String) invocation.getArguments()[0];
                    return cardData.replace("X", "");
                }
            });
        } catch (CryptoException e) {
            e.printStackTrace();
        }
        
        when(this.cryptoService.isLinkCryptoKey(Mockito.anyString())).thenAnswer(new Answer<Boolean>() {
            
            @Override
            public Boolean answer(final InvocationOnMock invocation) throws Throwable {
                final String cardData = (String) invocation.getArguments()[0];
                return cardData.startsWith("link");
            }
        });
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(new UserAccountServiceImpl() {
            public UserAccount findUserAccount(final int userAccountId) {
                return (UserAccount) TestDBMap.findObjectByIdFromMemory(userAccountId, UserAccount.class);
            }
            
            public UserAccount findUndeletedUserAccount(final String encodedUserName) {
                return new UserAccount();
            }
        });
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(final Object entity) {
                return entity;
            }
        });
        
        when(this.customerSubscriptionServiceMock.findActiveCustomerSubscriptionByCustomerId(Mockito.anyInt(), Mockito.anyBoolean()))
                .thenAnswer(new Answer<Collection<CustomerSubscription>>() {
                    @Override
                    public Collection<CustomerSubscription> answer(final InvocationOnMock invocation) throws Throwable {
                        final CustomerSubscription custSub = new CustomerSubscription();
                        custSub.setSubscriptionType(new SubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_REALTIME_CC_PROCESSING));
                        custSub.setIsEnabled(true);
                        final Collection<CustomerSubscription> col = new ArrayList<CustomerSubscription>();
                        col.add(custSub);
                        return col;
                    }
                });
        
        this.client
                .prepareForSuccessRequest(CardTypeClient.class,
                                          "{\"response\":\"MasterCard\",\"status\":{\"errors\": null,\"responseStatus\": \"SUCCESS\"}}".getBytes());
        this.linkCardTypeServiceImpl.init();
        this.customerCardTypeService.init();
        
        Mockito.doReturn(true).when(this.customerCardTypeService).isCreditCardTrack2(Mockito.anyString());
        
        util.setCustomerSubscriptionService(this.customerSubscriptionServiceMock);
    }
    
    @Override
    public final String createSetup(final Object dto) {
        final PreAuth preAuth = (PreAuth) dto;
        this.authorizationHandler.setPosSerialNumber(preAuth.getPointOfSale().getSerialNumber());
        this.authorizationHandler.setCardData(preAuth.getCardData());
        this.authorizationHandler.setRefId(preAuth.getReferenceId());
        this.authorizationHandler.setChargeAmount(String.valueOf(((PreAuth) dto).getAmount()));
        
        return WebCoreConstants.BLANK;
        
    }
    
    @Override
    public final void process(final String xmlMessage) {
        final PS2XMLBuilder xmlBuilder = new PS2XMLBuilder(PS2XMLBuilderBase.MESSAGE_FOR_PAYSTATION2_ELEMENT_NAME_STRING);
        xmlBuilder.setOriginalMessage(xmlMessage);
        try {
            xmlBuilder.initialize("user_id", "user_password");
        } catch (InvalidDataException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (SystemException e) {
            e.printStackTrace();
            Assert.fail();
        }
        this.authorizationHandler.process(xmlBuilder);
    }
    
}
