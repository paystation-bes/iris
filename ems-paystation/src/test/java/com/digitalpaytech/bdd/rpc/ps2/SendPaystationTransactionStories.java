package com.digitalpaytech.bdd.rpc.ps2;

import java.util.TimeZone;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import com.digitalpaytech.bdd.util.RibbonClientBaseSteps;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.client.CardTypeClient;
import com.digitalpaytech.client.CoreCPSClient;
import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.CcFailLog;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PaystationSetting;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.dto.paystation.CardEaseData;
import com.digitalpaytech.dto.paystation.TransactionDto;
import com.digitalpaytech.util.kafka.KafkaBaseSteps;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class SendPaystationTransactionStories extends AbstractStories {
    
    public SendPaystationTransactionStories() {
        super();
        // To assert the purchase timestamps the JVM must be running in the same timezone it will run in on prod
        TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
        
        TestContext.getInstance().getObjectParser()
                .register(new Class[] { Customer.class, PointOfSale.class, TransactionDto.class, PaystationSetting.class, Purchase.class,
                    CardRetryTransaction.class, CardType.class, MerchantAccount.class, CcFailLog.class, ProcessorTransaction.class, Permit.class,
                    PosServiceState.class, CardEaseData.class, CoreCPSClient.class, CardTypeClient.class });
    }
    
    // Here we specify the steps classes
    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new PaystationCommunicationSteps(testHandlers),
                new KafkaBaseSteps(this.testHandlers), new RibbonClientBaseSteps(this.testHandlers));
    }
}
