package com.digitalpaytech.bdd.rpc.ps2.util;

import com.beanstream.exceptions.BeanstreamApiException;
import com.digitalpayment.bdd.rpc.ps2.eventhandler.TestXMLRPCEvents;
import com.digitalpaytech.bdd.rpc.ps2.handler.TestXMLRPCEmvMerchantAccount;
import com.digitalpaytech.bdd.rpc.ps2.handler.TestXMLRPCEmvMerchantAccountSuccess;
import com.digitalpaytech.bdd.rpc.ps2.handler.TestXMLRPCLotSettingSuccess;
import com.digitalpaytech.bdd.rpc.ps2.handler.TestXMLRPCTelemetry;
import com.digitalpaytech.bdd.rpc.ps2.transactionhandler.TestPreAuthXMLRPCTransactions;
import com.digitalpaytech.bdd.rpc.ps2.transactionhandler.TestXMLRPCTransactions;

public class TestXMLHandlerFactory {
    
    private static final String PRE_AUTH_TRANSACTION_MESSAGE = "preauth transaction";
    private static final String TRANSACTION_MESSAGE = "transaction";
    private static final String EVENT_MESSAGE = "event";
    private static final String EMV_TELEMETRY_MESSAGE = "emv telemetry";
    private static final String EMV_MERCHANT_ACCOUNT_SUCCESS_MESSAGE = "emv merchant account success";
    private static final String EMV_MERCHANT_ACCOUNT_MESSAGE = "emv merchant account";
    private static final String LOT_SETTING_UPDATE_SUCCESS = "setting update acknowledge";
    
    public static final TestXMLRPCBehavior getHandlerClass(final String messageType) throws BeanstreamApiException {
        switch (messageType.toLowerCase()) {
            case TRANSACTION_MESSAGE:
                return new TestXMLRPCTransactions();
            case EVENT_MESSAGE:
                return new TestXMLRPCEvents();
            case PRE_AUTH_TRANSACTION_MESSAGE:

                try {
                    return new TestPreAuthXMLRPCTransactions();
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            case EMV_TELEMETRY_MESSAGE:
                return new TestXMLRPCTelemetry();
            case EMV_MERCHANT_ACCOUNT_SUCCESS_MESSAGE:
                return new TestXMLRPCEmvMerchantAccountSuccess();
            case EMV_MERCHANT_ACCOUNT_MESSAGE:
                return new TestXMLRPCEmvMerchantAccount();
            case LOT_SETTING_UPDATE_SUCCESS:
                return new TestXMLRPCLotSettingSuccess();
            default:
                return null;
        }
    }
    
}
