package com.digitalpaytech.bdd.rpc.ps2.handler;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.digitalpaytech.bdd.rpc.ps2.util.TestXMLRPCBehavior;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.SystemException;
import com.digitalpaytech.rpc.ps2.LotSettingSuccessHandler;
import com.digitalpaytech.rpc.ps2.PS2XMLBuilder;
import com.digitalpaytech.rpc.ps2.PS2XMLBuilderBase;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.PaystationSettingService;
import com.digitalpaytech.service.impl.CustomerAgreementServiceImpl;
import com.digitalpaytech.service.impl.CustomerServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.paystation.impl.PosServiceStateServiceImpl;
import com.digitalpaytech.util.kafka.MockMessageProducer;
import com.digitalpaytech.util.kafka.KafkaKeyConstants;

import junit.framework.Assert;

public class TestXMLRPCLotSettingSuccess implements TestXMLRPCBehavior {
    private static final String TOPIC_DEVICE_UPGRADE_ACK = "Device Upgrade Ack";
    
    @InjectMocks
    private PosServiceStateServiceImpl posServiceStateService;
    
    @InjectMocks
    private PointOfSaleServiceImpl posService;
    
    @InjectMocks
    private CustomerServiceImpl customerService;
    
    @InjectMocks
    private CustomerAgreementServiceImpl customerAgreementService;
    
    @Mock
    private PaystationSettingService settingService;
    
    @Mock
    private MailerService mailerService;
    
    private EntityDaoTest entityDao = new EntityDaoTest();
    private MockMessageProducer producer = MockMessageProducer.getInstance();
    
    private LotSettingSuccessHandler handler;
    
    public TestXMLRPCLotSettingSuccess() {
        
    }
    
    @Override
    public final String createSetup(final Object dto) {
        this.producer.overrideTopicConf(KafkaKeyConstants.FMS_KAFKA_UPGRADE_ACK_TOPIC_NAME, TOPIC_DEVICE_UPGRADE_ACK, true);
        
        this.handler = (LotSettingSuccessHandler) dto;
        this.handler.setProducer(this.producer);
        
        MockitoAnnotations.initMocks(this);
        
        TestContext.getInstance().autowiresAttributes(this);
        
        return null;
    }
    
    @Override
    public final void process(final String xmlMessage) {
        final TestContext ctx = TestContext.getInstance();
        ctx.getDatabase().session().flush();
        
        final PS2XMLBuilder xmlBuilder = new PS2XMLBuilder(PS2XMLBuilderBase.MESSAGE_FOR_PAYSTATION2_ELEMENT_NAME_STRING);
        xmlBuilder.setOriginalMessage(xmlMessage);
        try {
            xmlBuilder.initialize("user_id", "user_password");
        } catch (InvalidDataException | SystemException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        
        this.handler.process(xmlBuilder);
    }
}
