package com.digitalpaytech.bdd.rpc.ps2;


import static org.junit.matchers.JUnitMatchers.*;
import static org.junit.Assert.*;
import org.springframework.util.Assert;
import org.w3c.dom.*;
import org.xml.sax.*;
import javax.xml.parsers.*;
import java.io.*;
import java.util.Map;



import org.jbehave.core.annotations.When;
import org.jbehave.core.annotations.Then;
import org.json.JSONObject;

import com.beanstream.exceptions.BeanstreamApiException;
import com.digitalpaytech.bdd.rpc.ps2.util.TestXMLHandlerFactory;
import com.digitalpaytech.bdd.rpc.ps2.util.TestXMLRPCBehavior;
import com.digitalpaytech.bdd.util.AbstractSteps;
import com.digitalpaytech.bdd.util.JBehaveTestHandlers;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.expression.StoryObjectBuilder;
import com.digitalpaytech.rpc.ps2.BaseHandler;
import com.digitalpaytech.rpc.ps2.PS2XMLBuilder;

public class PaystationCommunicationSteps extends AbstractSteps {
    private static final String DEFAULT_MESSAGE_NUMBER = "1";
    private static final String EMPTY_STRING = "";
    private static final String MESSAGE4PAYSTATION2 = "Message4Paystation2";
    
    public PaystationCommunicationSteps(final JBehaveTestHandlers testHandlers) {
        super(testHandlers);
    }
    
    /**
     * Method sets up the DTO domain object for the incomming request from the
     * pay station and invokes the appropriate handler.
     * 
     * @.example When iris receives an incoming *event* message where the<br>
     *           *pay station is 500000070001
     *           Timestamp is now
     *           Type is Modem
     *           Action is APN
     *           Information is TestURL.COM*
     * 
     * @param messageType
     *            type of incoming message e.g. event, transaction, audit etc.
     * @param messageData
     *            data that would be sent by the pay station
     * @throws BeanstreamApiException
     */
    @When("iris receives an incoming $messageType message where the $messageData")
    public final void formWithValue(final String messageType, final String messageData) throws BeanstreamApiException {
        formWithValue(messageType, DEFAULT_MESSAGE_NUMBER, messageData);
    }
    
    @When("iris receives an incoming $messageType message with message number $messageNumber where the $messageData")
    public final void formWithValue(final String messageType, final String messageNumber, final String messageData) throws BeanstreamApiException {
        final String dtoName = messageType + " data";
        final StoryObjectBuilder objBuilder = this.context.getObjectBuilder();
        
        final Class<?> dtoType = objBuilder.resolveType(dtoName);
        final boolean handlerFromStory = BaseHandler.class.isAssignableFrom(dtoType);
        final Object dto;
        if (handlerFromStory) {
            dto = objBuilder.instantiate(dtoType, messageNumber);
        } else {
            dto = objBuilder.instantiate(dtoType);
        }
        
        this.context.getObjectParser().parse(messageData, dto);
        
        final TestXMLRPCBehavior handler = TestXMLHandlerFactory.getHandlerClass(messageType);
        final String xmlMessage = handler.createSetup(dto);
        handler.process(xmlMessage); 
    }
    
    /**
     * Asserts that xml rpc response contains $xmlTag
     * 
     * @.example Then pay station response will contain *EMVMerchantAccountNotification*
     * 
     * @param xmlTag
     *            xml tag that is being assert
     */
    @Then("pay station with message number $messageNumber will contain $xmlTag")
    public final void assertSuccessfulResponse(final String messageNumber, final String xmlTag) {
        final TestContext ctx = TestContext.getInstance();
        final Map<Object, PS2XMLBuilder> xmlFromCache = ctx.getCache().map(PS2XMLBuilder.class);
        final String xml = ctx.getCache().get(PS2XMLBuilder.class, messageNumber).toString();
        assertThat(xml, containsString(xmlTag));
    }
    
    /**
     * Asserts that xml rpc response does not contains $xmlTag
     * 
     * @.example Then pay station response will not contain *EMVMerchantAccountNotification*
     * 
     * @param xmlTag
     *            xml tag that is being assert
     */
    @Then("pay station with message number $messageNumber will not contain $xmlTag")
    public final void assertFailureResponse(final String messageNumber, final String xmlTag) {
        final TestContext ctx = TestContext.getInstance();
        final Map<Object, PS2XMLBuilder> xmlFromCache = ctx.getCache().map(PS2XMLBuilder.class);
        final String xml = ctx.getCache().get(PS2XMLBuilder.class, messageNumber).toString();       
        Assert.doesNotContain(xml, xmlTag);
    }
    
    /**
     * Grabs the $xmlTag received by pay station in JSON
     * Asserts the $field in the JSON with expected $value
     * 
     * @.example Then there exists a *MerchantInfo* tag where the *Merchant Processor* is *17681535*
     * 
     * @param xmlTag
     *            xml tag in json
     * @param field
     *            field to be asserted on
     * @param value
     *            value expected in field
     * 
     */   
    @Then("there exists a message number $messageNumber with $xmlTag tag where the $field is $value")
    public final void assertJSONValues(final String messageNumber, final String xmlTag, final String field, final String value) {
        final String json = grabValue(xmlTag, messageNumber);
        final JSONObject jsonObject = new JSONObject(json);
        final String expectedValue = jsonObject.getString(field);
        assertEquals(value, expectedValue);
    }
     
    /**
     * Asserts that $xmlTag received by pay station contains the expected value of $data
     * 
     * @.example Then pay station will respond *MerchantID* tag with a value of *1*
     * 
     * @param xmlTag
     *            xml tag that is being asserted on
     * @param data
     *            data expected in xmlTag
     * 
     */
    @Then("pay station with message number $messageNumber will respond $xmlTag tag with a value of $data")
    public final void assertTagwithValue(final String messageNumber, final String xmlTag, final String data) {
        final String actualValue = grabValue(xmlTag, messageNumber);
        assertEquals(data, actualValue);
    }

    /**
     * Find the value in the XMLTag
     * 
     * @param xmlTag
     *            given by the pay station response
     * @return value in the xmlTag
     */
    private String grabValue(final String xmlTag, final String messageNumber) {
        final TestContext ctx = TestContext.getInstance();
        final Map<Object, PS2XMLBuilder> xmlFromCache = ctx.getCache().map(PS2XMLBuilder.class);
        final String xml = ctx.getCache().get(PS2XMLBuilder.class, messageNumber).toString();
        try {
            final DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            final InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));
            Document doc = null;
            doc = db.parse(is);
            final NodeList nodes = doc.getElementsByTagName(MESSAGE4PAYSTATION2);
            final Element element = (Element) nodes.item(0);
            final NodeList xmlName = element.getElementsByTagName(xmlTag);
            final Element value = (Element) xmlName.item(0);
            final String actualValue = getCharacterDataFromElement(value);
            return actualValue;
            
        } catch (SAXException | ParserConfigurationException | FactoryConfigurationError | IOException e) {
            fail(e.getMessage());
        }
        return EMPTY_STRING;
    }
    
    public static String getCharacterDataFromElement(final Element e) {
        final Node child = e.getFirstChild();
        if (child instanceof CharacterData) {
            final CharacterData cd = (CharacterData) child;
            return cd.getData();
        }
        return EMPTY_STRING;
    }
}
