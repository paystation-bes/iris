package com.digitalpaytech.bdd.rpc.ps2.handler;

import java.util.Properties;

import junit.framework.Assert;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;

import com.digitalpaytech.bdd.rpc.ps2.util.TestXMLRPCBehavior;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.dao.impl.OpenSessionDaoTest;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.SystemException;
import com.digitalpaytech.rpc.ps2.EMVMerchantAccountSuccessHandler;
import com.digitalpaytech.rpc.ps2.PS2XMLBuilder;
import com.digitalpaytech.rpc.ps2.PS2XMLBuilderBase;
import com.digitalpaytech.service.impl.CustomerAdminServiceImpl;
import com.digitalpaytech.service.impl.CustomerAgreementServiceImpl;
import com.digitalpaytech.service.impl.CustomerServiceImpl;
import com.digitalpaytech.service.impl.EmsPropertiesServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.util.MessageHelper;

public class TestXMLRPCEmvMerchantAccountSuccess implements TestXMLRPCBehavior {
  
    @InjectMocks
    private MerchantAccountServiceImpl merchantAccountService;
    
    @InjectMocks
    private CustomerAdminServiceImpl customerAdminService;
    
    @InjectMocks
    private CustomerAgreementServiceImpl customerAgreementService;
    
    @InjectMocks
    private CustomerServiceImpl customerService;
    
    @InjectMocks
    private PointOfSaleServiceImpl pointOfSaleService;
    
    @InjectMocks
    private OpenSessionDaoTest openSessionDao;
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @InjectMocks
    private EmsPropertiesServiceImpl emsPropertiesService;
    
    @Mock
    private MessageSource messageSource;
    
    @Mock
    private MessageHelper messages;
    
    private EMVMerchantAccountSuccessHandler emvMerchantAccountSuccessHandler;
    
    public TestXMLRPCEmvMerchantAccountSuccess() {
        MockitoAnnotations.initMocks(this);
        this.messages.setMessageSource(this.messageSource);
        this.emvMerchantAccountSuccessHandler = null;
        final Properties properties = new Properties();
        properties.clear();
        properties.put("SensorProcessingThreadCount", "1");
        properties.put("SensorDataBatchCount", "1");
        this.emsPropertiesService.setProperties(properties);
        
        TestContext.getInstance().autowiresAttributes(this);
    }
    
    @Override
    public final String createSetup(final Object dto) {
        this.emvMerchantAccountSuccessHandler = (EMVMerchantAccountSuccessHandler) dto;
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    @Override
    public final void process(final String xmlMessage) {
        final PS2XMLBuilder xmlBuilder = new PS2XMLBuilder(PS2XMLBuilderBase.MESSAGE_FOR_PAYSTATION2_ELEMENT_NAME_STRING);
        xmlBuilder.setOriginalMessage(xmlMessage);
        try {
            xmlBuilder.initialize("user_id", "user_password");
        } catch (InvalidDataException | SystemException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        this.emvMerchantAccountSuccessHandler.process(xmlBuilder);
        final TestContext ctx = TestContext.getInstance();
        ctx.getCache().save(PS2XMLBuilder.class, this.emvMerchantAccountSuccessHandler.getMessageNumber(), xmlBuilder);
    }
    
}
