package com.digitalpaytech.bdd.rpc.ps2.handler;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import org.junit.Assert;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;

import com.digitalpaytech.bdd.rpc.ps2.util.TestXMLRPCBehavior;
import com.digitalpaytech.bdd.services.MessageHelperMock;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.client.PaymentClient;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.dao.impl.OpenSessionDaoTest;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.SystemException;
import com.digitalpaytech.helper.impl.TransactionHelperImpl;
import com.digitalpaytech.ribbon.mock.MockClientFactory;
import com.digitalpaytech.rpc.ps2.EMVMerchantAccountHandler;
import com.digitalpaytech.rpc.ps2.PS2XMLBuilder;
import com.digitalpaytech.rpc.ps2.PS2XMLBuilderBase;
import com.digitalpaytech.service.impl.CustomerAdminServiceImpl;
import com.digitalpaytech.service.impl.CustomerAgreementServiceImpl;
import com.digitalpaytech.service.impl.CustomerServiceImpl;
import com.digitalpaytech.service.impl.EmsPropertiesServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.impl.TransactionServiceImpl;
import com.digitalpaytech.util.MessageHelper;

public class TestXMLRPCEmvMerchantAccount implements TestXMLRPCBehavior {
    public static final String MESSAGE_NUMBER = "1";
    
    @Mock
    private MessageSource messageSource;
    
    @InjectMocks
    private TransactionHelperImpl transactionHelper;
    
    @InjectMocks
    private Locale defaultLocale;
    
    @InjectMocks
    private TransactionServiceImpl transactionService;
    
    @Mock
    private MessageHelper messages;
    
    @InjectMocks
    private MerchantAccountServiceImpl merchantAccountService;
    
    @InjectMocks
    private CustomerAdminServiceImpl customerAdminService;
    
    @InjectMocks
    private CustomerAgreementServiceImpl customerAgreementService;
    
    @InjectMocks
    private CustomerServiceImpl customerService;
    
    @InjectMocks
    private PointOfSaleServiceImpl pointOfSaleService;
    
    @InjectMocks
    private OpenSessionDaoTest openSessionDao;
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @InjectMocks
    private EmsPropertiesServiceImpl emsPropertiesService;
    private EMVMerchantAccountHandler emvMerchantAccountHandler;
    
    MockClientFactory client = new MockClientFactory();
    
    public TestXMLRPCEmvMerchantAccount() {
        MockitoAnnotations.initMocks(this);
        this.messages.setMessageSource(this.messageSource);
        this.emvMerchantAccountHandler = null;
        
        final Properties properties = new Properties();
        properties.clear();
        properties.put("SensorProcessingThreadCount", "1");
        properties.put("SensorDataBatchCount", "1");
        this.emsPropertiesService.setProperties(properties);
        
        TestContext.getInstance().autowiresAttributes(this);
        
        final Map<String, String> labelsMapping = new HashMap<String, String>();
        labelsMapping.put("label.terminalAccount.field1.20", "merchantProcessor");
        labelsMapping.put("label.terminalAccount.field2.20", "merchantId");
        labelsMapping.put("label.terminalAccount.field3.20", "terminalId");
        labelsMapping.put("label.terminalAccount.field4.20", "transactionKey");
        labelsMapping.put("label.terminalAccount.field5.20", "currency");
        labelsMapping.put("label.terminalAccount.terminalObjectId", "_id");
        Mockito.doAnswer(MessageHelperMock.getMessage(labelsMapping)).when(this.messages).getMessage(Mockito.any());
        this.client
                .prepareForSuccessRequest(PaymentClient.class,
                                          "{\"merchantId\":\"merchantID\",\"merchantProcessor\":\"Elavon\",\"transactionKey\":\"TRANSACTION\",\"currency\":\"CAD\",\"terminalId\":\"11223344\"}"
                                                  .getBytes());
    }
    
    @Override
    public final String createSetup(final Object dto) {
        this.emvMerchantAccountHandler = (EMVMerchantAccountHandler) dto;
        final String serialNumber = this.emvMerchantAccountHandler.getPaystationCommAddress();
        this.emvMerchantAccountHandler.setPosSerialNumber(serialNumber);
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    @Override
    public final void process(final String xmlMessage) {
        final PS2XMLBuilder xmlBuilder = new PS2XMLBuilder(PS2XMLBuilderBase.MESSAGE_FOR_PAYSTATION2_ELEMENT_NAME_STRING);
        xmlBuilder.setOriginalMessage(xmlMessage);
        try {
            xmlBuilder.initialize("user_id", "user_password");
        } catch (InvalidDataException | SystemException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        this.emvMerchantAccountHandler.process(xmlBuilder);
        TestContext.getInstance().getDatabase().session().flush();
        final TestContext ctx = TestContext.getInstance();
        ctx.getCache().save(PS2XMLBuilder.class, this.emvMerchantAccountHandler.getMessageNumber(), xmlBuilder);
    }
    
}
