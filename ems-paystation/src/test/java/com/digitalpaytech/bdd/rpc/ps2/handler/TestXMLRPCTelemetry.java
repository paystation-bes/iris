package com.digitalpaytech.bdd.rpc.ps2.handler;

import java.util.Date;

import junit.framework.Assert;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.digitalpaytech.bdd.rpc.ps2.util.TestXMLRPCBehavior;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.domain.EventType;
import com.digitalpaytech.dto.paystation.EmvTelemetryDto;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.SystemException;
import com.digitalpaytech.rpc.ps2.EMVTelemetryHandler;
import com.digitalpaytech.rpc.ps2.PS2XMLBuilder;
import com.digitalpaytech.rpc.ps2.PS2XMLBuilderBase;
import com.digitalpaytech.service.impl.CustomerAgreementServiceImpl;
import com.digitalpaytech.service.impl.CustomerServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.impl.PosTelemetryServiceImpl;
import com.digitalpaytech.service.paystation.impl.EventAlertServiceImpl;
import com.digitalpaytech.service.queue.QueueEventService;
import com.digitalpaytech.bdd.services.QueueEventServiceMockImpl;

public class TestXMLRPCTelemetry implements TestXMLRPCBehavior {
    public static final String MESSAGE_NUMBER = "1";
    
    @InjectMocks
    private PointOfSaleServiceImpl pointOfSaleService;
    
    @InjectMocks
    private CustomerServiceImpl customerService;
    
    @InjectMocks
    private PosTelemetryServiceImpl posTelemetryService;
    
    @Mock
    private QueueEventService queueEventService;
    
    @InjectMocks
    private EventAlertServiceImpl eventAlertService;
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @InjectMocks
    private CustomerAgreementServiceImpl customerAgreementService;

    
    private EMVTelemetryHandler emvTelemetryHandler;
    
    public TestXMLRPCTelemetry() {
        MockitoAnnotations.initMocks(this);
        this.emvTelemetryHandler = new EMVTelemetryHandler(MESSAGE_NUMBER);
        
        TestContext.getInstance().autowiresAttributes(this);
        
        Mockito.doAnswer(QueueEventServiceMockImpl.addQueueEventToQueue()).when(queueEventService).createEventQueue(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt(), Mockito.any(EventType.class), Mockito.anyInt(), Mockito.anyInt(), Mockito.any(Date.class), Mockito.anyString(), Mockito.anyBoolean(), Mockito.anyBoolean(), Mockito.anyInt(), Mockito.any(Byte.class));
    }
    
    
    @Override
    public String createSetup(final Object dto) {
        this.emvTelemetryHandler.setType(((EmvTelemetryDto) dto).getType());
        this.emvTelemetryHandler.setReferenceCounter(((EmvTelemetryDto) dto).getReferenceCounter());
        this.emvTelemetryHandler.setTimestamp(((EmvTelemetryDto) dto).getTimeStamp());
        this.emvTelemetryHandler.setPaystationCommAddress(((EmvTelemetryDto) dto).getPaystationCommAddress());
        this.emvTelemetryHandler.setTelemetry(((EmvTelemetryDto) dto).getTelemetry());    
        return null;
    }

    @Override
    public void process(final String xmlMessage) {
        this.entityDao.flush();
        final PS2XMLBuilder xmlBuilder = new PS2XMLBuilder(PS2XMLBuilderBase.MESSAGE_FOR_PAYSTATION2_ELEMENT_NAME_STRING);
        xmlBuilder.setOriginalMessage(xmlMessage);
        try {
            xmlBuilder.initialize("user_id", "user_password");
        } catch (InvalidDataException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (SystemException e) {
            e.printStackTrace();
            Assert.fail();
        }
        
        this.emvTelemetryHandler.process(xmlBuilder);
        
    }
}
