package com.digitalpaytech.bdd.rpc.ps2.transactionhandler;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.MessageSource;

import com.beanstream.Gateway;
import com.beanstream.exceptions.BeanstreamApiException;
import com.beanstream.requests.CardPaymentRequest;
import com.digitalpaytech.bdd.rpc.ps2.util.TestXMLRPCBehavior;
import com.digitalpaytech.bdd.services.util.TransactionFacadeMock;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.cardprocessing.CardProcessingManager;
import com.digitalpaytech.cardprocessing.CardProcessingMaster;
import com.digitalpaytech.cardprocessing.cps.dto.ProcessingDestinationServiceImpl;
import com.digitalpaytech.cardprocessing.impl.CardProcessingManagerImpl;
import com.digitalpaytech.cardprocessing.impl.CardProcessorFactoryImpl;
import com.digitalpaytech.client.CardTypeClient;
import com.digitalpaytech.client.PaymentClient;
import com.digitalpaytech.dao.EntityDaoIRIS;
import com.digitalpaytech.dao.EntityDaoReport;
import com.digitalpaytech.dao.impl.EntityDaoReportTest;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.dao.impl.OpenSessionDaoTest;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerPropertyType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.dto.paystation.TransactionDto;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.SystemException;
import com.digitalpaytech.helper.impl.TransactionHelperImpl;
import com.digitalpaytech.ribbon.mock.MockClientFactory;
import com.digitalpaytech.rpc.ps2.PS2XMLBuilder;
import com.digitalpaytech.rpc.ps2.PS2XMLBuilderBase;
import com.digitalpaytech.rpc.ps2.TransactionHandler;
import com.digitalpaytech.rpc.support.threads.SensorExtractionThread;
import com.digitalpaytech.service.ArchiveService;
import com.digitalpaytech.service.CardRetryTransactionTypeService;
import com.digitalpaytech.service.ClusterMemberService;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.CustomerCardService;
import com.digitalpaytech.service.DenominationTypeService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.MobileNumberService;
import com.digitalpaytech.service.PermitIssueTypeService;
import com.digitalpaytech.service.PermitService;
import com.digitalpaytech.service.SmsAlertService;
import com.digitalpaytech.service.TaxService;
import com.digitalpaytech.service.ThirdPartyServiceAccountService;
import com.digitalpaytech.service.UnifiedRateService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.crypto.CryptoAlgorithmFactory;
import com.digitalpaytech.service.impl.ActivePermitServiceImpl;
import com.digitalpaytech.service.impl.CardRetryTransactionServiceImpl;
import com.digitalpaytech.service.impl.CardTypeServiceImpl;
import com.digitalpaytech.service.impl.CcFailLogServiceImpl;
import com.digitalpaytech.service.impl.CommonProcessingServiceImpl;
import com.digitalpaytech.service.impl.CouponServiceImpl;
import com.digitalpaytech.service.impl.CreditCardTypeServiceImpl;
import com.digitalpaytech.service.impl.CustomerAdminServiceImpl;
import com.digitalpaytech.service.impl.CustomerAgreementServiceImpl;
import com.digitalpaytech.service.impl.CustomerCardTypeServiceImpl;
import com.digitalpaytech.service.impl.CustomerServiceImpl;
import com.digitalpaytech.service.impl.EmsPropertiesServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.LicensePlateServiceImpl;
import com.digitalpaytech.service.impl.LinkCardTypeServiceImpl;
import com.digitalpaytech.service.impl.LocationServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.service.impl.PaymentCardServiceImpl;
import com.digitalpaytech.service.impl.PaymentTypeServiceImpl;
import com.digitalpaytech.service.impl.PaystationSettingServiceImpl;
import com.digitalpaytech.service.impl.PermitTypeServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.impl.PreAuthServiceImpl;
import com.digitalpaytech.service.impl.ProcessorServiceImpl;
import com.digitalpaytech.service.impl.ProcessorTransactionServiceImpl;
import com.digitalpaytech.service.impl.ProcessorTransactionTypeServiceImpl;
import com.digitalpaytech.service.impl.PurchaseServiceImpl;
import com.digitalpaytech.service.impl.RawSensorDataServiceImpl;
import com.digitalpaytech.service.impl.TransactionServiceImpl;
import com.digitalpaytech.service.impl.TransactionTypeServiceImpl;
import com.digitalpaytech.service.impl.WebWidgetHelperServiceImpl;
import com.digitalpaytech.service.kpi.KPIService;
import com.digitalpaytech.service.kpi.impl.KPIListenerServiceImpl;
import com.digitalpaytech.service.paystation.EventSensorService;
import com.digitalpaytech.service.paystation.PosServiceStateService;
import com.digitalpaytech.service.processor.TDMerchantCommunicationService;
import com.digitalpaytech.service.processor.impl.TestTDMerchantCommunicationServiceMockImpl;
import com.digitalpaytech.service.report.impl.ReportDataServiceImpl;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.TransactionFacade;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.impl.TransactionFacadeImpl;
import com.digitalpaytech.util.kafka.KafkaKeyConstants;
import com.digitalpaytech.util.kafka.MockMessageProducer;

import junit.framework.Assert;

@SuppressWarnings({ "PMD.ExcessiveImports", "PMD.TooManyFields", "PMD.UnusedPrivateField" })
public class TestXMLRPCTransactions implements TestXMLRPCBehavior {
    
    private static final Logger LOG = Logger.getLogger(TestXMLRPCTransactions.class);
    
    private final MockClientFactory client = new MockClientFactory();
    
    private final MockMessageProducer producer = MockMessageProducer.getInstance();
    
    @InjectMocks
    private LocationServiceImpl locationService;
    
    @Mock
    private ThirdPartyServiceAccountService thirdPartyServiceAccountService;
    
    @Mock
    private ArchiveService archiveService;
    
    @Mock
    private EventSensorService eventSensorService;
    
    @Mock
    private MailerService mailerService;
    
    @Mock
    private PosServiceStateService posServiceStateService;
    
    @Mock
    private UserAccountService userAccountService;
    
    @InjectMocks
    private CustomerAgreementServiceImpl customerAgreementService;
    
    @InjectMocks
    private CustomerServiceImpl customerService;
    
    @InjectMocks
    private MerchantAccountServiceImpl merchantAccountService;
    
    @Spy
    @InjectMocks
    private CustomerCardTypeServiceImpl customerCardTypeService;
    
    @Mock
    private CustomerCardService customerCardService;
    
    @InjectMocks
    private CardTypeServiceImpl cardTypeService;
    
    @Mock
    private CardRetryTransactionTypeService cardRetryTransactionTypeService;
    
    @Mock
    private ClusterMemberService clusterMemberService;
    
    @Mock
    private UnifiedRateService unifiedRateService;
    
    @Mock
    private MobileNumberService mobileNumberService;
    
    @Mock
    private PermitService permitService;
    
    @Mock
    private PermitIssueTypeService permitIssueTypeService;
    
    @Mock
    private TaxService taxService;
    
    @InjectMocks
    private PreAuthServiceImpl preAuthService;
    
    @Mock
    private DenominationTypeService denominationTypeService;
    
    @Mock
    private CryptoService cryptoService;
    
    @InjectMocks
    private CreditCardTypeServiceImpl creditCardTypeService;
    
    @InjectMocks
    private LicensePlateServiceImpl licensePlateService;
    
    @Mock
    private CryptoAlgorithmFactory cryptoAlgorithmFactory;
    
    @InjectMocks
    private PointOfSaleServiceImpl pointOfSaleService;
    
    @Mock
    private EntityDaoIRIS entityDaoIris;
    
    @Mock
    private SmsAlertService smsAlertService;
    
    @Mock
    private MessageSource messageSource;
    
    @Mock
    private CardProcessingMaster cardProcessingMaster;
    
    @Mock
    private BeanFactory beanFactory;
    
    @InjectMocks
    private CcFailLogServiceImpl ccFailLogService;
    
    @SuppressWarnings("unused")
    private final EntityDaoReport entityDaoReport = new EntityDaoReportTest();
    
    @InjectMocks
    private ReportDataServiceImpl reportDataService;
    
    @Spy
    @InjectMocks
    private TransactionFacadeImpl transactionFacade;
    
    @Mock
    private TransactionFacade transactionFacadeMock;
    
    @Spy
    @InjectMocks
    private CustomerAdminServiceImpl customerAdminService;
    
    @InjectMocks
    private RawSensorDataServiceImpl rawSensorDataService;
    
    @InjectMocks
    private PermitTypeServiceImpl permitTypeService;
    
    @InjectMocks
    private PaymentCardServiceImpl paymentCardService;
    
    @InjectMocks
    private OpenSessionDaoTest openSessionDao;
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @InjectMocks
    private EmsPropertiesServiceImpl emsPropertiesService;
    
    @InjectMocks
    private PaymentTypeServiceImpl paymentTypeService;
    
    @InjectMocks
    private TransactionTypeServiceImpl transactionTypeService;
    
    @InjectMocks
    private PaystationSettingServiceImpl paystationSettingService;
    
    @InjectMocks
    private CouponServiceImpl couponService;
    
    @InjectMocks
    private EntityServiceImpl entityService;
    
    @InjectMocks
    private ActivePermitServiceImpl activePermitService;
    
    @InjectMocks
    private TransactionHelperImpl transactionHelper;
    
    @InjectMocks
    private TransactionServiceImpl transactionService;
    
    @InjectMocks
    private KPIListenerServiceImpl kpiListenerService;
    
    @Mock
    private KPIService kpiService;
    
    @InjectMocks
    private ProcessorTransactionServiceImpl processorTransactionService;
    
    @InjectMocks
    private PurchaseServiceImpl purchaseService;
    
    @InjectMocks
    private ProcessorTransactionTypeServiceImpl processorTransactionTypeService;
    
    @InjectMocks
    private CommonProcessingServiceImpl commonProcessingService;
    
    @Mock
    private TDMerchantCommunicationService tdMerchantCommunicationService;
    
    @InjectMocks
    private ProcessorServiceImpl processorService;
    
    @InjectMocks
    private CardProcessorFactoryImpl cardProcessorFactory;
    
    @InjectMocks
    private CardProcessingManagerImpl cardProcessingManager;
    
    @InjectMocks
    private MessageHelper messages;
    
    @InjectMocks
    private WebWidgetHelperServiceImpl webWidgetHelperService;
    
    @InjectMocks
    private CardRetryTransactionServiceImpl cardRetryTransactionService;
    
    @Spy
    @InjectMocks
    private LinkCardTypeServiceImpl linkCardTypeServiceImpl;
    
    @Mock
    private MessageHelper messageHelper;
    
    @InjectMocks
    private ProcessingDestinationServiceImpl processingDestinationService;
    
    private final TransactionHandler transactionHandler;
    
    @SuppressWarnings({ "deprecation", "PMD.AvoidCatchingThrowable", "checkstyle:illegalcatch", "checkstyle:cyclomaticcomplexity" })
    public TestXMLRPCTransactions() throws BeanstreamApiException {
        
        MockitoAnnotations.initMocks(this);
        
        Mockito.doReturn(new ArrayList<>()).when(this.linkCardTypeServiceImpl).getCollapsedBinRanges();
        Mockito.doNothing().when(this.linkCardTypeServiceImpl).getBinRangesFromCardTypeService();
        
        this.transactionHandler = new TransactionHandler(StandardConstants.STRING_ONE);
        
        this.messages.setMessageSource(this.messageSource);
        this.messageHelper.setMessageSource(this.messageSource);
        this.licensePlateService.setMessageProducer(this.producer);
        final Properties properties = new Properties();
        properties.clear();
        properties.put("SensorProcessingThreadCount", StandardConstants.STRING_ONE);
        properties.put("SensorDataBatchCount", StandardConstants.STRING_ONE);
        this.emsPropertiesService.setProperties(properties);
        new SensorExtractionThread(this.eventSensorService, this.rawSensorDataService, this.pointOfSaleService, this.emsPropertiesService,
                this.archiveService);
        
        TestContext.getInstance().autowiresAttributes(this);
        
        when(this.tdMerchantCommunicationService.sendPostAuthRequest(Mockito.any(Gateway.class), Mockito.anyString(), Mockito.anyDouble()))
                .thenAnswer(TestTDMerchantCommunicationServiceMockImpl.sendPostAuthRequest());
        
        when(this.messageHelper.getMessageWithDefault(Mockito.anyString(), Mockito.anyString())).thenAnswer(new Answer<String>() {
            @Override
            public String answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                return (String) args[1];
            }
        });
        
        Mockito.doAnswer(new Answer<Void>() {
            public Void answer(final InvocationOnMock invocation) {
                final ProcessorTransaction ptd = (ProcessorTransaction) invocation.getArguments()[1];
                ptd.setLoaded(false);
                cardProcessingManager.processTransactionSettlement(ptd);
                return null;
            }
        }).when(this.cardProcessingMaster).addTransactionToSettlementQueue(Mockito.any(PointOfSale.class), Mockito.any(ProcessorTransaction.class));
        
        try {
            when(this.tdMerchantCommunicationService.makePayment(Mockito.any(Gateway.class), Mockito.any(CardPaymentRequest.class)))
                    .thenAnswer(TestTDMerchantCommunicationServiceMockImpl.makePayment());
        } catch (BeanstreamApiException e) {
            LOG.error(e);
        }
        
        try {
            when(this.transactionFacade.decryptCardData(Mockito.anyString())).thenAnswer(TransactionFacadeMock.decryptCardData());
            when(this.cryptoService.encryptData(Mockito.anyInt(), Mockito.anyString())).thenAnswer(TransactionFacadeMock.encryptCardData());
        } catch (CryptoException e) {
            LOG.error(e);
        } catch (Throwable e) {
            LOG.error(e);
        }
        
        when(this.transactionFacadeMock.isCreditCard(Mockito.anyObject())).thenAnswer(TransactionFacadeMock.isCreditCard());
        when(this.transactionFacadeMock.findApprovedPreAuth(Mockito.anyLong())).thenAnswer(TransactionFacadeMock.findApprovedPreAuth());
        when(this.transactionFacadeMock.findPermitType(Mockito.anyString())).thenAnswer(TransactionFacadeMock.findPermitType());
        when(this.transactionFacadeMock.findOrCreatePaystationSetting(Mockito.anyObject(), Mockito.anyString()))
                .thenAnswer(TransactionFacadeMock.findOrCreatePaystationSetting());
        
        when(this.transactionFacadeMock.findPaymentType(Mockito.anyInt())).thenAnswer(TransactionFacadeMock.findPaymentType());
        when(this.transactionFacadeMock.findTransactionType(Mockito.anyString())).thenAnswer(TransactionFacadeMock.findTransactionType());
        when(this.transactionFacadeMock.findCardType(Mockito.anyInt())).thenAnswer(TransactionFacadeMock.findCardType());
        
        when(this.beanFactory.getBean(Mockito.anyString(), Mockito.eq(CardProcessingManager.class))).thenReturn(this.cardProcessingManager);
        
        when(this.messageSource.getMessage(anyString(), any(Object[].class), any(Locale.class))).thenAnswer(new Answer<String>() {
            @Override
            public String answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                return (String) args[0];
            }
        });
        
        final List<CustomerProperty> customerProperties = TestContext.getInstance().getDatabase().find(CustomerProperty.class);
        
        CustomerProperty jurisdictionTypePrefered = null;
        CustomerProperty jurisdictionTypeLimited = null;
        for (CustomerProperty prop : customerProperties) {
            if (prop.getCustomerPropertyType() != null
                && prop.getCustomerPropertyType().getId() == WebCoreConstants.CUSTOMER_PROPERTY_TYPE_JURISDICTION_TYPE_PREFERRED) {
                jurisdictionTypePrefered = prop;
            } else if (prop.getCustomerPropertyType() != null
                       && prop.getCustomerPropertyType().getId() == WebCoreConstants.CUSTOMER_PROPERTY_TYPE_JURISDICTION_TYPE_LIMITED) {
                jurisdictionTypeLimited = prop;
            }
        }
        
        if (jurisdictionTypePrefered == null) {
            jurisdictionTypePrefered = new CustomerProperty(new CustomerPropertyType(), new Customer(),
                    String.valueOf(PaystationConstants.PAYSTATION_JURISDICTION_PREFERRED_BY_LOCATION), new Date(), 1);
        }
        if (jurisdictionTypeLimited == null) {
            jurisdictionTypeLimited = new CustomerProperty(new CustomerPropertyType(), new Customer(),
                    String.valueOf(PaystationConstants.PAYSTATION_JURISDICTION_LIMITED_BY_CUSTOMER), new Date(), 1);
        }
        Mockito.doReturn(jurisdictionTypePrefered).when(this.customerAdminService).getCustomerPropertyByCustomerIdAndCustomerPropertyType(Mockito
                .anyInt(), Mockito.eq(WebCoreConstants.CUSTOMER_PROPERTY_TYPE_JURISDICTION_TYPE_PREFERRED));
        
        Mockito.doReturn(jurisdictionTypeLimited).when(this.customerAdminService).getCustomerPropertyByCustomerIdAndCustomerPropertyType(Mockito
                .anyInt(), Mockito.eq(WebCoreConstants.CUSTOMER_PROPERTY_TYPE_JURISDICTION_TYPE_LIMITED));
        
        Mockito.doReturn(new CustomerProperty(new CustomerPropertyType(), new Customer(), "Canada/Pacific", new Date(), 1))
                .when(this.customerAdminService)
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(Mockito.anyInt(),
                                                                        Mockito.eq(WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE));
        this.customerCardTypeService.init();
        Mockito.doReturn(true).when(this.customerCardTypeService).isCreditCardTrack2(Mockito.anyString());
        
        this.client
                .prepareForSuccessRequest(CardTypeClient.class,
                                          "{\"response\":\"MasterCard\",\"status\":{\"errors\": null,\"responseStatus\": \"SUCCESS\"}}".getBytes());
        
        this.linkCardTypeServiceImpl.init();
        
        this.client.prepareForSuccessRequest(PaymentClient.class, "{}".getBytes());
    }
    
    @Override
    public final String createSetup(final Object dto) {
        final TransactionDto transactionDto = (TransactionDto) dto;
        this.producer.overrideTopicConf(KafkaKeyConstants.LINK_KAFKA_LIMITED_PREFERRED_RATE_INCREMENT_TOPIC_NAME, "Limited Preferred Rate", true);
        
        this.producer.clear();
        this.transactionHandler.setPosSerialNumber(transactionDto.getPaystationCommAddress());
        this.transactionHandler.setTxDto(transactionDto);
        
        return WebCoreConstants.BLANK;
    }
    
    @Override
    public final void process(final String xmlMessage) {
        final PS2XMLBuilder xmlBuilder = new PS2XMLBuilder(PS2XMLBuilderBase.MESSAGE_FOR_PAYSTATION2_ELEMENT_NAME_STRING);
        xmlBuilder.setOriginalMessage(xmlMessage);
        try {
            xmlBuilder.initialize("user_id", "user_password");
        } catch (InvalidDataException e) {
            LOG.error(e);
            Assert.fail();
        } catch (SystemException e) {
            LOG.error(e);
            Assert.fail();
        }
        this.transactionHandler.process(xmlBuilder);
        
        TestContext.getInstance().getCache().save(PS2XMLBuilder.class, this.transactionHandler.getMessageNumber(), xmlBuilder);
    }
    
}
