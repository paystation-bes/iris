package com.digitalpaytech.bdd.rpc.ps2;

import org.jbehave.core.steps.InjectableStepsFactory;

import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;


import com.digitalpaytech.rpc.ps2.EMVMerchantAccountHandler;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class SendPaystationEmvMerchantAccountStories extends AbstractStories {
    
    public SendPaystationEmvMerchantAccountStories() {
        super();
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
        TestContext
                .getInstance()
                .getObjectParser()
                .register(new Class[] { EMVMerchantAccountHandler.class, Customer.class, PointOfSale.class, CardType.class, MerchantAccount.class, });
    }
    
    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new PaystationCommunicationSteps(testHandlers));
    }
    
}
