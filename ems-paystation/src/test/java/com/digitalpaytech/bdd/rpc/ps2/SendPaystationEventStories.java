package com.digitalpaytech.bdd.rpc.ps2;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.PaystationSetting;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.dto.paystation.EventData;
import com.digitalpaytech.dto.paystation.TransactionDto;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class SendPaystationEventStories extends AbstractStories {
    public SendPaystationEventStories() {
        super();
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
        TestLookupTables.getExpressionParser().register(new Class[] { Customer.class, PointOfSale.class, TransactionDto.class,
                                                            PaystationSetting.class, Purchase.class, Permit.class, EventData.class });
    }
    
    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new PaystationCommunicationSteps(testHandlers));
    }
}
