package com.digitalpaytech.bdd.rpc.ps2.util;

public interface TestXMLRPCBehavior {
    
    String createSetup(Object dto);
    
    void process(String xml);
    
}
