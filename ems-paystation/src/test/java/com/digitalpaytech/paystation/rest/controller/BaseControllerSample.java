package com.digitalpaytech.paystation.rest.controller;

/**
 * REST API client sample code to retrieve session token.
 */
public class BaseControllerSample {
    
    protected static final String REST_URL = "qa.digitalpaytech.com";
    protected static final String REST_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    
    protected static final String HTTP_METHOD_GET = "POST";
    protected static final String HTTP_ENCODE_CHARSET = "UTF-8";
    
    protected static final String CONTENT_TYPE = "Content-Type";
    protected static final String APPLICATION_XML = "application/xml; charset=UTF-8";
    protected static final String STRING_AMP = "&";
    
    protected static final String HMAC_SHA256_ALGORITHM = "HmacSHA256";
    
    protected static final String PARAMETER_SERIALNUMBER = "SerialNumber=";
    protected static final String PARAMETER_SIGNATURE = "Signature=";
    protected static final String PARAMETER_SIGNATURE_VERSION = "SignatureVersion=";
    protected static final String PARAMETER_TIMESTAMP = "Timestamp=";
    protected static final String PARAMETER_VERSION = "Version=";
    
    protected static final int HUNDRED = 100;
    protected static final int THOUSAND = 1000;
    
    protected BaseControllerSample() {
        
    }
}
