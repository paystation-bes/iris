package com.digitalpaytech.paystation.rest.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import com.digitalpaytech.util.DateUtil;

/**
 * REST API client sample code to retrieve session token.
 */
public class HeartBeatControllerSample extends BaseControllerSample {
    private static final String REST_TOKEN = "/PayStation/HeartBeat";
    
    public static void main(final String[] args) {
        try {
            
            /* account name and secret key created in EMS admin. */
            final String secretKey = new String(Base64.encodeBase64("zaq123$ESZxsw234%RDX".getBytes()));
            final String serialNumber = "500000020001";
            final String timestamp = DateUtil.format(new Date(23049L), REST_TIME_FORMAT, "UTC");
            final String version = "6.4.3";
            final String signatureVersion = "1";
            
            final StringBuilder xmlString = new StringBuilder();
            xmlString.append("<HeartBeat MessageNumber=\"2\">").append("<PaystationCommAddress>").append(serialNumber)
                    .append("</PaystationCommAddress>").append("</HeartBeat>");
            
            /*
             * create HMAC signature string for GetToken.
             * necessary parameter: Account, SignatureVersion, and Timestamp.
             */
            final StringBuilder signatureStr = new StringBuilder(HTTP_METHOD_GET);
            signatureStr.append(REST_URL);
            signatureStr.append(REST_TOKEN);
            signatureStr.append(PARAMETER_SERIALNUMBER).append(URLEncoder.encode(serialNumber.trim(), HTTP_ENCODE_CHARSET));
            signatureStr.append(STRING_AMP);
            signatureStr.append(PARAMETER_SIGNATURE_VERSION).append(URLEncoder.encode(signatureVersion.trim(), HTTP_ENCODE_CHARSET));
            signatureStr.append(STRING_AMP);
            signatureStr.append(PARAMETER_TIMESTAMP).append(URLEncoder.encode(timestamp.trim(), HTTP_ENCODE_CHARSET));
            signatureStr.append(STRING_AMP);
            signatureStr.append(PARAMETER_VERSION).append(URLEncoder.encode(version.trim(), HTTP_ENCODE_CHARSET));
            signatureStr.append(xmlString.toString().trim());
            
            /*
             * create signature with secret key.
             * As the distributed secret key is encoded with Base64, please decode it when you create the signature.
             */
            final SecretKey seckey = new SecretKeySpec(Base64.decodeBase64(secretKey.getBytes(HTTP_ENCODE_CHARSET)), HMAC_SHA256_ALGORITHM);
            final Mac mac = Mac.getInstance(HMAC_SHA256_ALGORITHM);
            mac.init(seckey);
            final byte[] finalByte = mac.doFinal(signatureStr.toString().getBytes(HTTP_ENCODE_CHARSET));
            final String signature = new String(Base64.encodeBase64(finalByte), HTTP_ENCODE_CHARSET);
            
            /*
             * create URL string for URL connection.
             * necessary query parameter: Account, Signature, SignatureVersion, and Timestamp.
             */
            final StringBuilder urlStr = new StringBuilder("http://");
            urlStr.append(REST_URL);
            urlStr.append(REST_TOKEN).append("?");
            urlStr.append(PARAMETER_SERIALNUMBER).append(URLEncoder.encode(serialNumber.trim(), HTTP_ENCODE_CHARSET));
            urlStr.append(STRING_AMP);
            urlStr.append(PARAMETER_SIGNATURE).append(URLEncoder.encode(signature.trim(), HTTP_ENCODE_CHARSET));
            urlStr.append(STRING_AMP);
            urlStr.append(PARAMETER_SIGNATURE_VERSION).append(URLEncoder.encode(signatureVersion.trim(), HTTP_ENCODE_CHARSET));
            urlStr.append(STRING_AMP);
            urlStr.append(PARAMETER_TIMESTAMP).append(URLEncoder.encode(timestamp.trim(), HTTP_ENCODE_CHARSET));
            urlStr.append(STRING_AMP);
            urlStr.append(PARAMETER_VERSION).append(URLEncoder.encode(version.trim(), HTTP_ENCODE_CHARSET));
            
            /* create HttpClient provided by Apache. */
            final HttpClient client = new DefaultHttpClient();
            final HttpPost httpPost = new HttpPost(urlStr.toString());
            httpPost.addHeader(CONTENT_TYPE, APPLICATION_XML);
            
            /* create your XML string entity. */
            final StringEntity entity = new StringEntity(xmlString.toString());
            
            /* set the XML string in the HttpPost. */
            httpPost.setEntity(entity);
            
            final HttpResponse response = client.execute(httpPost);
            
            if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                System.out.println("Operation failed: " + response.getStatusLine().getStatusCode());
            }
            
            System.out.println("Content-Type: " + response.getEntity().getContentType().getValue());
            System.out.println("Content-Length: " + response.getEntity().getContentLength());
            final BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = reader.readLine();
            
            while (line != null) {
                System.out.println(line);
                line = reader.readLine();
            }
            
            client.getConnectionManager().shutdown();
        } catch (IOException e) {
            e.printStackTrace();
            /* Please define your exception logic here. */
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            /* Please define your exception logic here. */
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            /* Please define your exception logic here. */
        }
    }
}
