package com.digitalpaytech.paystation.rest.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import com.digitalpaytech.util.DateUtil;

/**
 * REST API client sample code to retrieve session token.
 */
public final class TransactionControllerSample extends BaseControllerSample {
    
    private static final String REST_TOKEN = "/PayStation/Transaction";
    
    private TransactionControllerSample() {
        
    }
    
    public static void main(final String[] args) {
        try {
            
            /* account name and secret key created in EMS admin. */
            String secretKey = null;
            String serialNumber = null;
            
            final boolean isOne = true;
            
            if (isOne) {
                secretKey = "f4cGHhoy7E7UacJuH/QGnbrlPlUoFPksAd00E0UOHyI=";
                serialNumber = "500000060001";
            } else {
                secretKey = "9qUDMla6YvTDMfI0Yfxk4R/JyB5dR/rBIx4Zx5fTAYo=";
                serialNumber = "555555555555";
            }
            
            final String version = "6.4.3";
            final String signatureVersion = "1";
            final String timestamp = DateUtil.format(new Date(), REST_TIME_FORMAT, "UTC");
            
            final String xmlString = cashTransaction(serialNumber);
            //            String xmlString = storeAndForwardTransaction(serialNumber);
            //                                    String xmlString = onlineTransaction(serialNumber);
            //                                    String xmlString = onlineExternalCustomTransaction(serialNumber);
            //                                    String xmlString = onlineEncryptedInternalCustomTransaction(serialNumber);
            //                                    String xmlString = onlineInternalCustomTransaction(serialNumber);
            //            String xmlString = smartCardTransaction(serialNumber);
            //            String xmlString = cancelTransaction(serialNumber);
            
            /*
             * create HMAC signature string for GetToken.
             * necessary parameter: Account, SignatureVersion, and Timestamp.
             */
            final StringBuilder signatureStr = new StringBuilder(HTTP_METHOD_GET);
            signatureStr.append(REST_URL);
            signatureStr.append(REST_TOKEN);
            signatureStr.append(PARAMETER_SERIALNUMBER).append(URLEncoder.encode(serialNumber.trim(), HTTP_ENCODE_CHARSET));
            signatureStr.append(STRING_AMP);
            signatureStr.append(PARAMETER_SIGNATURE_VERSION).append(URLEncoder.encode(signatureVersion.trim(), HTTP_ENCODE_CHARSET));
            signatureStr.append(STRING_AMP);
            signatureStr.append(PARAMETER_TIMESTAMP).append(URLEncoder.encode(timestamp.trim(), HTTP_ENCODE_CHARSET));
            signatureStr.append(STRING_AMP);
            signatureStr.append(PARAMETER_VERSION).append(URLEncoder.encode(version.trim(), HTTP_ENCODE_CHARSET));
            signatureStr.append(xmlString.trim());
            
            /*
             * create signature with secret key.
             * As the distributed secret key is encoded with Base64, please decode it when you create the signature.
             */
            final SecretKey seckey = new SecretKeySpec(Base64.decodeBase64(secretKey.getBytes(HTTP_ENCODE_CHARSET)), HMAC_SHA256_ALGORITHM);
            final Mac mac = Mac.getInstance(HMAC_SHA256_ALGORITHM);
            mac.init(seckey);
            final byte[] finalByte = mac.doFinal(signatureStr.toString().getBytes(HTTP_ENCODE_CHARSET));
            final String signature = new String(Base64.encodeBase64(finalByte), HTTP_ENCODE_CHARSET);
            
            /*
             * create URL string for URL connection.
             * necessary query parameter: Account, Signature, SignatureVersion, and Timestamp.
             */
            final StringBuilder urlStr = new StringBuilder("http://");
            urlStr.append(REST_URL);
            urlStr.append(REST_TOKEN).append("?");
            urlStr.append(PARAMETER_SERIALNUMBER).append(URLEncoder.encode(serialNumber.trim(), HTTP_ENCODE_CHARSET));
            urlStr.append(STRING_AMP);
            urlStr.append(PARAMETER_SIGNATURE).append(URLEncoder.encode(signature.trim(), HTTP_ENCODE_CHARSET));
            urlStr.append(STRING_AMP);
            urlStr.append(PARAMETER_SIGNATURE_VERSION).append(URLEncoder.encode(signatureVersion.trim(), HTTP_ENCODE_CHARSET));
            urlStr.append(STRING_AMP);
            urlStr.append(PARAMETER_TIMESTAMP).append(URLEncoder.encode(timestamp.trim(), HTTP_ENCODE_CHARSET));
            urlStr.append(STRING_AMP);
            urlStr.append(PARAMETER_VERSION).append(URLEncoder.encode(version.trim(), HTTP_ENCODE_CHARSET));
            
            /* create HttpClient provided by Apache. */
            final HttpClient client = new DefaultHttpClient();
            final HttpPost httpPost = new HttpPost(urlStr.toString());
            httpPost.addHeader(CONTENT_TYPE, APPLICATION_XML);
            
            /* create your XML string entity. */
            final StringEntity entity = new StringEntity(xmlString);
            
            /* set the XML string in the HttpPost. */
            httpPost.setEntity(entity);
            
            final HttpResponse response = client.execute(httpPost);
            
            if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                System.out.println("Operation failed: " + response.getStatusLine().getStatusCode());
            }
            
            System.out.println("Content-Type: " + response.getEntity().getContentType().getValue());
            final BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = reader.readLine();
            
            while (line != null) {
                System.out.println(line);
                line = reader.readLine();
            }
            
            client.getConnectionManager().shutdown();
        } catch (IOException e) {
            e.printStackTrace();
            /* Please define your exception logic here. */
        } catch (NoSuchAlgorithmException e) {
            /* Please define your exception logic here. */
        } catch (InvalidKeyException e) {
            /* Please define your exception logic here. */
        }
    }
    
    private static String cashTransaction(final String serialNumber) {
        
         final String purchaseString = DateUtil.createColonDelimitedDateString();
//        final String purchaseString = "2014:11:03:19:20:59:GMT";
        final String timestampString = DateUtil.createColonDelimitedDateString();
        final int parkingMinutes = 200;
        final Calendar expiry = Calendar.getInstance();
        expiry.add(Calendar.MINUTE, parkingMinutes);
        final String expiryString = DateUtil.createColonDelimitedDateString(expiry.getTime());
        final Random rand = new Random();
        final int stallNumber = rand.nextInt() % THOUSAND;
        final boolean plateOrStall = false;
        //        final int number = rand.nextInt() % THOUSAND;
        final int number = 481;
        
        final StringBuilder xmlString = new StringBuilder();
        xmlString.append("<Transaction MessageNumber=\"2\">");
        xmlString.append("<PaystationCommAddress>").append(serialNumber).append("</PaystationCommAddress>");
        xmlString.append("<Number>").append(number).append("</Number>");
        xmlString.append("<LotNumber>EMS 6.3.11.22</LotNumber>");
        if (plateOrStall) {
            xmlString.append("<LicensePlateNo>ABC678</LicensePlateNo>");
        } else {
            xmlString.append("<StallNumber>").append(stallNumber).append("</StallNumber>");
        }
        xmlString.append("<Type>Regular</Type>");
        xmlString.append("<PurchasedDate>").append(purchaseString).append("</PurchasedDate>");
        xmlString.append("<Timestamp>").append(timestampString).append("</Timestamp>");
        xmlString.append("<ParkingTimePurchased>").append(parkingMinutes).append("</ParkingTimePurchased>");
        xmlString.append("<OriginalAmount>400</OriginalAmount>");
        xmlString.append("<ChargedAmount>4.00</ChargedAmount>");
        xmlString.append("<CouponNumber></CouponNumber>");
        xmlString.append("<CashPaid>4.00</CashPaid>");
        xmlString.append("<CardPaid></CardPaid>");
        xmlString.append("<CardData></CardData>");
        xmlString.append("<IsRefundSlip>false</IsRefundSlip>");
        xmlString.append("<ExpiryDate>").append(expiryString).append("</ExpiryDate>");
        xmlString.append("<Tax1Rate></Tax1Rate>");
        xmlString.append("<Tax1Value></Tax1Value>");
        xmlString.append("<Tax1Name></Tax1Name>");
        xmlString.append("<Tax2Rate></Tax2Rate>");
        xmlString.append("<Tax2Value></Tax2Value>");
        xmlString.append("<Tax2Name></Tax2Name>");
        xmlString.append("<RateName>early bird</RateName>");
        xmlString.append("<RateID>1</RateID>");
        xmlString.append("<RateValue>400</RateValue>");
        xmlString.append("<TaxType>N/A</TaxType>");
        xmlString.append("<CoinCol>100:2,200:1</CoinCol>");
        xmlString.append("</Transaction>");
        
        return xmlString.toString();
    }
    
    private static String storeAndForwardTransaction(final String serialNumber) {
        final String purchaseString = DateUtil.createColonDelimitedDateString();
        final String timestampString = DateUtil.createColonDelimitedDateString();
        final int parkingMinutes = 30;
        final Calendar expiry = Calendar.getInstance();
        expiry.add(Calendar.MINUTE, parkingMinutes);
        final String expiryString = DateUtil.createColonDelimitedDateString(expiry.getTime());
        
        final StringBuilder xmlString = new StringBuilder();
        xmlString.append("<Transaction MessageNumber=\"3\">");
        xmlString.append("<PaystationCommAddress>").append(serialNumber).append("</PaystationCommAddress>");
        xmlString.append("<Number>2</Number>");
        xmlString.append("<LotNumber>EMS 6.3.11.22</LotNumber>");
        xmlString.append("<LicensePlateNo>ABC678</LicensePlateNo>");
        xmlString.append("<Type>Regular</Type>");
        xmlString.append("<PurchasedDate>").append(purchaseString).append("</PurchasedDate>");
        xmlString.append("<Timestamp>").append(timestampString).append("</Timestamp>");
        xmlString.append("<ParkingTimePurchased>").append(parkingMinutes).append("</ParkingTimePurchased>");
        xmlString.append("<OriginalAmount>200</OriginalAmount>");
        xmlString.append("<ChargedAmount>2.00</ChargedAmount>");
        xmlString.append("<CouponNumber></CouponNumber>");
        xmlString.append("<CashPaid>1.00</CashPaid>");
        xmlString.append("<CardPaid>1.00</CardPaid>");
        xmlString
                .append("<CardData>000002mFIyB+7AMYdL9zMHqrUjnki9Wy1NQZV6xn+A06IzRkjSDoLTcwCs9C1UYzx5q/xrTl7Qs4smmIl3RI/JnOt1B/W2u4Dir2qBiZgzgU2dou673AcwJ+E5WrQQaLEswJ41DJYRl7O+JZCO82pNE6ovISqgu20x6JYc03cwNcxH5FOZLcncX2f7qqnUIw0xqiEMY1gtjpR/8Ki3ho5qxe1jtCWVCseYDy319cfwT+0EHjDvVH0KfOVXDV2g97Vyq7KvQ5U96HYWDopLetT8V9lEY8vSF3wv13xHHRsIyftQYD19ZsGyEsNZxAnPHuI0b4irlwj/WHWCBk1d7Ajseui7kg==</CardData>");
        xmlString.append("<ExpiryDate>").append(expiryString).append("</ExpiryDate>");
        xmlString.append("<Tax1Rate>7.24</Tax1Rate>");
        xmlString.append("<Tax1Value>1</Tax1Value>");
        xmlString.append("<Tax1Name>GST</Tax1Name>");
        xmlString.append("<Tax2Rate>5.00</Tax2Rate>");
        xmlString.append("<Tax2Value>1.00</Tax2Value>");
        xmlString.append("<Tax2Name>PST</Tax2Name>");
        xmlString.append("<RateName>early bird</RateName>");
        xmlString.append("<RateID>1</RateID>");
        xmlString.append("<RateValue>200</RateValue>");
        xmlString.append("<TaxType>N/A</TaxType>");
        xmlString.append("<StoreAndForward>1</StoreAndForward>");
        xmlString.append("</Transaction>");
        
        return xmlString.toString();
        
    }
    
    private static String onlineTransaction(final String serialNumber) {
        final String purchaseString = DateUtil.createColonDelimitedDateString();
        final String timestampString = DateUtil.createColonDelimitedDateString();
        final int parkingMinutes = 30;
        final Calendar expiry = Calendar.getInstance();
        expiry.add(Calendar.MINUTE, parkingMinutes);
        final String expiryString = DateUtil.createColonDelimitedDateString(expiry.getTime());
        final Random rand = new Random();
        final int stallNumber = rand.nextInt() % THOUSAND;
        final boolean plateOrStall = false;
        final int number = rand.nextInt() % HUNDRED;
        
        final StringBuilder xmlString = new StringBuilder();
        xmlString.append("<Transaction MessageNumber=\"3\">");
        xmlString.append("<PaystationCommAddress>").append(serialNumber).append("</PaystationCommAddress>");
        xmlString.append("<Number>").append(number).append("</Number>");
        xmlString.append("<LotNumber>EMS 6.3.11.22</LotNumber>");
        xmlString.append("<LicensePlateNo>ABC678</LicensePlateNo>");
        xmlString.append("<Type>Regular</Type>");
        xmlString.append("<PurchasedDate>").append(purchaseString).append("</PurchasedDate>");
        xmlString.append("<Timestamp>").append(timestampString).append("</Timestamp>");
        xmlString.append("<ParkingTimePurchased>").append(parkingMinutes).append("</ParkingTimePurchased>");
        xmlString.append("<OriginalAmount>800</OriginalAmount>");
        xmlString.append("<ChargedAmount>8.00</ChargedAmount>");
        xmlString.append("<CouponNumber></CouponNumber>");
        xmlString.append("<CashPaid>4.00</CashPaid>");
        xmlString.append("<CardPaid>4.00</CardPaid>");
        xmlString
                .append("<CardData>000002mFIyB+7AMYdL9zMHqrUjnki9Wy1NQZV6xn+A06IzRkjSDoLTcwCs9C1UYzx5q/xrTl7Qs4smmIl3RI/JnOt1B/W2u4Dir2qBiZgzgU2dou673AcwJ+E5WrQQaLEswJ41DJYRl7O+JZCO82pNE6ovISqgu20x6JYc03cwNcxH5FOZLcncX2f7qqnUIw0xqiEMY1gtjpR/8Ki3ho5qxe1jtCWVCseYDy319cfwT+0EHjDvVH0KfOVXDV2g97Vyq7KvQ5U96HYWDopLetT8V9lEY8vSF3wv13xHHRsIyftQYD19ZsGyEsNZxAnPHuI0b4irlwj/WHWCBk1d7Ajseui7kg==</CardData>");
        xmlString.append("<ExpiryDate>").append(expiryString).append("</ExpiryDate>");
        xmlString.append("<Tax1Rate></Tax1Rate>");
        xmlString.append("<Tax1Value></Tax1Value>");
        xmlString.append("<Tax1Name></Tax1Name>");
        xmlString.append("<Tax2Rate></Tax2Rate>");
        xmlString.append("<Tax2Value></Tax2Value>");
        xmlString.append("<Tax2Name></Tax2Name>");
        xmlString.append("<RateName>early bird</RateName>");
        xmlString.append("<RateID>1</RateID>");
        xmlString.append("<RateValue>800</RateValue>");
        xmlString.append("<TaxType>N/A</TaxType>");
        xmlString.append("</Transaction>");
        
        return xmlString.toString();
        
    }
    
    private static String onlineExternalCustomTransaction(final String serialNumber) {
        final String purchaseString = DateUtil.createColonDelimitedDateString();
        final String timestampString = DateUtil.createColonDelimitedDateString();
        final int parkingMinutes = 30;
        final Calendar expiry = Calendar.getInstance();
        expiry.add(Calendar.MINUTE, parkingMinutes);
        final String expiryString = DateUtil.createColonDelimitedDateString(expiry.getTime());
        final Random rand = new Random();
        final int stallNumber = rand.nextInt() % THOUSAND;
        final boolean plateOrStall = false;
        final int number = rand.nextInt() % 50;
        
        final StringBuilder xmlString = new StringBuilder();
        xmlString.append("<Transaction MessageNumber=\"3\">");
        xmlString.append("<PaystationCommAddress>").append(serialNumber).append("</PaystationCommAddress>");
        xmlString.append("<Number>").append(number).append("</Number>");
        xmlString.append("<LotNumber>EMS 6.3.11.22</LotNumber>");
        xmlString.append("<LicensePlateNo>ABC678</LicensePlateNo>");
        xmlString.append("<Type>Regular</Type>");
        xmlString.append("<PurchasedDate>").append(purchaseString).append("</PurchasedDate>");
        xmlString.append("<Timestamp>").append(timestampString).append("</Timestamp>");
        xmlString.append("<ParkingTimePurchased>").append(parkingMinutes).append("</ParkingTimePurchased>");
        xmlString.append("<OriginalAmount>600</OriginalAmount>");
        xmlString.append("<ChargedAmount>6.00</ChargedAmount>");
        xmlString.append("<CouponNumber></CouponNumber>");
        xmlString.append("<CashPaid>2.00</CashPaid>");
        xmlString.append("<CardPaid>4.00</CardPaid>");
        xmlString.append("<CardData>375019001002880</CardData>");
        xmlString.append("<ExpiryDate>").append(expiryString).append("</ExpiryDate>");
        xmlString.append("<Tax1Rate></Tax1Rate>");
        xmlString.append("<Tax1Value></Tax1Value>");
        xmlString.append("<Tax1Name></Tax1Name>");
        xmlString.append("<Tax2Rate></Tax2Rate>");
        xmlString.append("<Tax2Value></Tax2Value>");
        xmlString.append("<Tax2Name></Tax2Name>");
        xmlString.append("<RateName>early bird</RateName>");
        xmlString.append("<RateID>1</RateID>");
        xmlString.append("<RateValue>6</RateValue>");
        xmlString.append("<TaxType>N/A</TaxType>");
        xmlString.append("</Transaction>");
        
        return xmlString.toString();
        
    }
    
    private static String onlineInternalCustomTransaction(final String serialNumber) {
        final String purchaseString = DateUtil.createColonDelimitedDateString();
        final String timestampString = DateUtil.createColonDelimitedDateString();
        final int parkingMinutes = 30;
        final Calendar expiry = Calendar.getInstance();
        expiry.add(Calendar.MINUTE, parkingMinutes);
        final String expiryString = DateUtil.createColonDelimitedDateString(expiry.getTime());
        final Random rand = new Random();
        final int stallNumber = Math.abs(rand.nextInt() % THOUSAND);
        final boolean plateOrStall = false;
        final int number = Math.abs(rand.nextInt() % 60);
        
        final StringBuilder xmlString = new StringBuilder();
        xmlString.append("<Transaction MessageNumber=\"3\">");
        xmlString.append("<PaystationCommAddress>").append(serialNumber).append("</PaystationCommAddress>");
        xmlString.append("<Number>").append(number).append("</Number>");
        xmlString.append("<LotNumber>EMS 6.3.11.22</LotNumber>");
        xmlString.append("<LicensePlateNo>ABC678</LicensePlateNo>");
        xmlString.append("<Type>Regular</Type>");
        xmlString.append("<PurchasedDate>").append(purchaseString).append("</PurchasedDate>");
        xmlString.append("<Timestamp>").append(timestampString).append("</Timestamp>");
        xmlString.append("<ParkingTimePurchased>").append(parkingMinutes).append("</ParkingTimePurchased>");
        xmlString.append("<OriginalAmount>4</OriginalAmount>");
        xmlString.append("<ChargedAmount>4.00</ChargedAmount>");
        xmlString.append("<CouponNumber></CouponNumber>");
        xmlString.append("<CashPaid>0.00</CashPaid>");
        xmlString.append("<CardPaid>4.00</CardPaid>");
        xmlString.append("<CardData>9999001</CardData>");
        xmlString.append("<ExpiryDate>").append(expiryString).append("</ExpiryDate>");
        xmlString.append("<Tax1Rate></Tax1Rate>");
        xmlString.append("<Tax1Value></Tax1Value>");
        xmlString.append("<Tax1Name></Tax1Name>");
        xmlString.append("<Tax2Rate></Tax2Rate>");
        xmlString.append("<Tax2Value></Tax2Value>");
        xmlString.append("<Tax2Name></Tax2Name>");
        xmlString.append("<RateName>early bird</RateName>");
        xmlString.append("<RateID>1</RateID>");
        xmlString.append("<RateValue>4</RateValue>");
        xmlString.append("<TaxType>N/A</TaxType>");
        xmlString.append("</Transaction>");
        
        return xmlString.toString();
        
    }
    
    private static String onlineEncryptedInternalCustomTransaction(final String serialNumber) {
        final String purchaseString = DateUtil.createColonDelimitedDateString();
        final String timestampString = DateUtil.createColonDelimitedDateString();
        final int parkingMinutes = 30;
        final Calendar expiry = Calendar.getInstance();
        expiry.add(Calendar.MINUTE, parkingMinutes);
        final String expiryString = DateUtil.createColonDelimitedDateString(expiry.getTime());
        
        final StringBuilder xmlString = new StringBuilder();
        xmlString.append("<Transaction MessageNumber=\"3\">");
        xmlString.append("<PaystationCommAddress>").append(serialNumber).append("</PaystationCommAddress>");
        xmlString.append("<Number>2</Number>");
        xmlString.append("<LotNumber>EMS 6.3.11.22</LotNumber>");
        xmlString.append("<LicensePlateNo>ABC678</LicensePlateNo>");
        xmlString.append("<Type>Regular</Type>");
        xmlString.append("<PurchasedDate>").append(purchaseString).append("</PurchasedDate>");
        xmlString.append("<Timestamp>").append(timestampString).append("</Timestamp>");
        xmlString.append("<ParkingTimePurchased>").append(parkingMinutes).append("</ParkingTimePurchased>");
        xmlString.append("<OriginalAmount>5</OriginalAmount>");
        xmlString.append("<ChargedAmount>0.05</ChargedAmount>");
        xmlString.append("<CouponNumber></CouponNumber>");
        xmlString.append("<CashPaid>0.00</CashPaid>");
        xmlString.append("<CardPaid>0.05</CardPaid>");
        xmlString
                .append("<CardData>000002hDwYrhO6wBPKw1bb+kqXcGmUQcsn/tYHgnymgyedidri+WiAIwaxePNzlRRi0EqGuSQ/59uxVRXf7/stuzlw9io3ahn0ozl7dO/H/zmTOi0uFr4r74fFP4rERn/DDcXo87T2q9S5qrxrq/NfbqzhwgSmz3+EHGz9dBOhbThGPkazYeB4JzMf6NV7lK+bUIHxRmRyj2wM0FiHzkyBlfoBDPdCk85yvP5eZxXyEUU6VruTO39P8zs5lzYE1HcZRWA+hNsDzICpqXeoFn1Hb/t0nxKS1EZAceoZfqhb0zSK0Hx0/+nFe8co4ri20EsOW/4V8vf1mPHjmPQF/z/sqpPHeg==</CardData>");
        xmlString.append("<ExpiryDate>").append(expiryString).append("</ExpiryDate>");
        xmlString.append("<Tax1Rate>7.24</Tax1Rate>");
        xmlString.append("<Tax1Value>1</Tax1Value>");
        xmlString.append("<Tax1Name>GST</Tax1Name>");
        xmlString.append("<Tax2Rate>5.00</Tax2Rate>");
        xmlString.append("<Tax2Value>1.00</Tax2Value>");
        xmlString.append("<Tax2Name>PST</Tax2Name>");
        xmlString.append("<RateName>early bird</RateName>");
        xmlString.append("<RateID>1</RateID>");
        xmlString.append("<RateValue>5</RateValue>");
        xmlString.append("<TaxType>N/A</TaxType>");
        xmlString.append("</Transaction>");
        
        return xmlString.toString();
        
    }
    
    private static String smartCardTransaction(final String serialNumber) {
        
        final String purchaseString = DateUtil.createColonDelimitedDateString();
        final String timestampString = DateUtil.createColonDelimitedDateString();
        final int parkingMinutes = 30;
        final Calendar expiry = Calendar.getInstance();
        expiry.add(Calendar.MINUTE, parkingMinutes);
        final String expiryString = DateUtil.createColonDelimitedDateString(expiry.getTime());
        final Random rand = new Random();
        final int stallNumber = rand.nextInt() % THOUSAND;
        final boolean plateOrStall = false;
        final int number = Math.abs(rand.nextInt() % THOUSAND);
        
        final StringBuilder xmlString = new StringBuilder();
        xmlString.append("<Transaction MessageNumber=\"77\">");
        xmlString.append("<PaystationCommAddress>500000060001</PaystationCommAddress>");
        xmlString.append("<Number>77</Number>");
        xmlString.append("<LotNumber>UBC Parking</LotNumber>");
        xmlString.append("<AddTimeNum>440888</AddTimeNum>");
        xmlString.append("<Type>Regular</Type>");
        xmlString.append("<PurchasedDate>2014:09:18:00:02:15:UTC</PurchasedDate>");
        xmlString.append("<ParkingTimePurchased>5</ParkingTimePurchased>");
        xmlString.append("<OriginalAmount>400</OriginalAmount>");
        xmlString.append("<ChargedAmount>4</ChargedAmount>");
        xmlString.append("<CashPaid>3.00</CashPaid>");
        xmlString.append("<CardPaid>");
        xmlString.append("</CardPaid>");
        xmlString.append("<CardData>");
        xmlString.append("</CardData>");
        xmlString.append("<SmartCardData>1</SmartCardData>");
        xmlString.append("<SmartCardPaid>1.00</SmartCardPaid>");
        xmlString.append("<IsRefundSlip>false</IsRefundSlip>");
        xmlString.append("<ExpiryDate>2014:09:18:00:07:15:UTC</ExpiryDate>");
        xmlString.append("<Tax1Rate>");
        xmlString.append("</Tax1Rate>");
        xmlString.append("<Tax1Value>");
        xmlString.append("</Tax1Value>");
        xmlString.append("<Tax1Name>");
        xmlString.append("</Tax1Name>");
        xmlString.append("<Tax2Rate>");
        xmlString.append("</Tax2Rate>");
        xmlString.append("<Tax2Value>");
        xmlString.append("</Tax2Value>");
        xmlString.append("<Tax2Name>");
        xmlString.append("</Tax2Name>");
        xmlString.append("<CoinCol>5:0,10:0,25:4,100:2</CoinCol>");
        xmlString.append("<BillCol>1:0,5:0,10:0,20:0,50:0</BillCol>");
        xmlString.append("<RateName>SinglePhaseTx</RateName>");
        xmlString.append("<RateID>69</RateID>");
        xmlString.append("<RateValue>400</RateValue>");
        xmlString.append("<TaxType>N/A</TaxType>");
        xmlString.append("<SmartCardType>ATMEL</SmartCardType>");
        xmlString.append("</Transaction>");
        return xmlString.toString();
    }
    
    private static String cancelTransaction(final String serialNumber) {
        
        final String purchaseString = DateUtil.createColonDelimitedDateString();
        final String timestampString = DateUtil.createColonDelimitedDateString();
        final int parkingMinutes = 30;
        final Calendar expiry = Calendar.getInstance();
        expiry.add(Calendar.MINUTE, parkingMinutes);
        final String expiryString = DateUtil.createColonDelimitedDateString(expiry.getTime());
        final Random rand = new Random();
        final int stallNumber = rand.nextInt() % THOUSAND;
        final boolean plateOrStall = false;
        final int number = rand.nextInt() % THOUSAND;
        
        final StringBuilder xmlString = new StringBuilder();
        xmlString.append("<Transaction MessageNumber=\"2\">");
        xmlString.append("<PaystationCommAddress>").append(serialNumber).append("</PaystationCommAddress>");
        xmlString.append("<Number>").append(number).append("</Number>");
        xmlString.append("<LotNumber>EMS 6.3.11.22</LotNumber>");
        xmlString.append("<StallNumber>").append(stallNumber).append("</StallNumber>");
        xmlString.append("<Type>Cancelled</Type>");
        xmlString.append("<PurchasedDate>").append(purchaseString).append("</PurchasedDate>");
        xmlString.append("<Timestamp>").append(timestampString).append("</Timestamp>");
        xmlString.append("<ChargedAmount>0</ChargedAmount>");
        xmlString.append("<CashPaid>10</CashPaid>");
        xmlString.append("<CardPaid>0</CardPaid>");
        xmlString.append("<ChangeDispensed>1</ChangeDispensed>");
        xmlString.append("<IsRefundSlip>True</IsRefundSlip>");
        xmlString.append("<SmartCardData>1</SmartCardData>");
        xmlString.append("<HopperDispensed>0-0:0-0</HopperDispensed>");
        xmlString.append("<ExpiryDate>2014:06:05:22:45:57:GMT</ExpiryDate>");
        xmlString.append("<RateName>");
        xmlString.append("</RateName>");
        xmlString.append("<RateID>0</RateID>");
        xmlString.append("<RateValue>0</RateValue>");
        xmlString.append("<TaxType>N/A</TaxType>");
        xmlString.append("<BillCol>10:1</BillCol>");
        xmlString.append("</Transaction>");
        
        return xmlString.toString();
    }
    
    private static String testTransaction(final String serialNumber) {
        
        final String purchaseString = DateUtil.createColonDelimitedDateString();
        final String timestampString = DateUtil.createColonDelimitedDateString();
        final int parkingMinutes = 30;
        final Calendar expiry = Calendar.getInstance();
        expiry.add(Calendar.MINUTE, parkingMinutes);
        final Random rand = new Random();
        final int number = rand.nextInt() % THOUSAND;
        final int stallNumber = rand.nextInt() % THOUSAND;

        
        final StringBuilder xmlString = new StringBuilder();
        xmlString.append("<Transaction MessageNumber=\"2\">");
        xmlString.append("<PaystationCommAddress>").append(serialNumber).append("</PaystationCommAddress>");
        xmlString.append("<Number>").append(number).append("</Number>");
        xmlString.append("<LotNumber>EMS 6.3.11.22</LotNumber>");
        xmlString.append("<StallNumber>").append(stallNumber).append("</StallNumber>");
        xmlString.append("<Type>Test</Type>");
        xmlString.append("<PurchasedDate>").append(purchaseString).append("</PurchasedDate>");
        xmlString.append("<Timestamp>").append(timestampString).append("</Timestamp>");
        xmlString.append("<OriginalAmount>0</OriginalAmount>");
        xmlString.append("<ChargedAmount>0</ChargedAmount>");
        xmlString.append("<SmartCardType></SmartCardType>");
        xmlString.append("<HopperDispensed>0-0:0-0</HopperDispensed>");
        xmlString.append("<ExpiryDate>2014:06:05:22:45:57:GMT</ExpiryDate>");
        xmlString.append("<RateName></RateName>");
        xmlString.append("<RateID>0</RateID>");
        xmlString.append("<RateValue>0</RateValue>");
        xmlString.append("</Transaction>");
        
        return xmlString.toString();
    }
    
}
