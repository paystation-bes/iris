package com.digitalpaytech.util;

import org.junit.Test;
import static org.junit.Assert.*;
import org.apache.log4j.Logger;

public class PaymentTypeIdFinderTest {
    private static final Logger LOG = Logger.getLogger(PaymentTypeIdFinderTest.class);
    
    @Test
    public void cashCreditChipPaymentTypeId() {
        PaymentTypeIdFinder idFinder = new PaymentTypeIdFinder(1);
        idFinder.setUsedCash(true);
        idFinder.setUsedCCChip(true);
        
        LOG.debug("-----> " + idFinder);
        assertEquals(WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CH, idFinder.findPaymentTypeId());
        assertTrue(idFinder.isUsedCash());
        assertTrue(idFinder.isUsedCCChip());
        
        idFinder.setUsedCash(false);
        assertEquals(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CHIP, idFinder.findPaymentTypeId());
    }
    
    @Test
    public void cashCreditFSwipePaymentTypeId() {
        PaymentTypeIdFinder idFinder = new PaymentTypeIdFinder(1);
        idFinder.setUsedCash(true);
        idFinder.setUsedCCFSwipe(true);
        
        LOG.debug("-----> " + idFinder);
        assertEquals(WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CARD_FSWIPE, idFinder.findPaymentTypeId());
        assertTrue(idFinder.isUsedCash());
        assertTrue(idFinder.isUsedCCFSwipe());
        
        idFinder.setUsedCCChip(true);
        assertEquals(WebCoreConstants.PAYMENT_TYPE_UNKNOWN, idFinder.findPaymentTypeId());
    }
    
    
    @Test
    public void cashCreditSwipePaymentTypeId() {
        PaymentTypeIdFinder idFinder = new PaymentTypeIdFinder(1);
        idFinder.setUsedCash(true);
        idFinder.setUsedCCSwipe(true);
        
        LOG.debug("-----> " + idFinder);
        assertEquals(WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_SWIPE, idFinder.findPaymentTypeId());
        assertFalse(idFinder.isUsedCCFSwipe());
        
        idFinder.setUsedCCSwipe(false);
        assertEquals(WebCoreConstants.PAYMENT_TYPE_CASH, idFinder.findPaymentTypeId());
    }
    
    
    @Test
    public void cashCreditTapPaymentTypeId() {
        PaymentTypeIdFinder idFinder = new PaymentTypeIdFinder(1);
        idFinder.setUsedCash(true);
        idFinder.setUsedCCCL(true);
        
        LOG.debug("-----> " + idFinder);
        assertEquals(WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CL, idFinder.findPaymentTypeId());
        assertFalse(idFinder.isUsedCCChip());
        assertFalse(idFinder.isUsedCCFSwipe());

        idFinder.setUsedCCSwipe(true);
        assertEquals(WebCoreConstants.PAYMENT_TYPE_UNKNOWN, idFinder.findPaymentTypeId());
    }
    
    
    @Test
    public void cashPaymentTypeId() {
        PaymentTypeIdFinder idFinder = new PaymentTypeIdFinder(1);
        idFinder.setUsedCash(true);
        
        LOG.debug("-----> " + idFinder);
        assertEquals(WebCoreConstants.PAYMENT_TYPE_CASH, idFinder.findPaymentTypeId());
        assertFalse(idFinder.isUsedSC());
        assertFalse(idFinder.isUsedValueCard());
        assertFalse(idFinder.isUsedCC());
        
        idFinder.setUsedValueCard(true);
        assertEquals(WebCoreConstants.PAYMENT_TYPE_CASH_VALUE, idFinder.findPaymentTypeId());
    }
    
    
    @Test
    public void cashSmartCardPaymentTypeId() {
        PaymentTypeIdFinder idFinder = new PaymentTypeIdFinder(1);
        idFinder.setUsedCash(true);
        idFinder.setUsedSC(true);

        LOG.debug("-----> " + idFinder);
        assertEquals(WebCoreConstants.PAYMENT_TYPE_CASH_SMART, idFinder.findPaymentTypeId());
        assertFalse(idFinder.isUsedCC());
        
        idFinder.setUsedCCSwipe(true);
        assertEquals(WebCoreConstants.PAYMENT_TYPE_UNKNOWN, idFinder.findPaymentTypeId());
    }
    
    
    @Test
    public void cashValueCardPaymentTypeId() {
        PaymentTypeIdFinder idFinder = new PaymentTypeIdFinder(1);
        idFinder.setUsedCash(true);
        idFinder.setUsedValueCard(true);
        
        LOG.debug("-----> " + idFinder);
        assertEquals(WebCoreConstants.PAYMENT_TYPE_CASH_VALUE, idFinder.findPaymentTypeId());
        assertFalse(idFinder.isUsedCC());
    }
    
    
    @Test
    public void creditCardChipPaymentTypeId() {
        PaymentTypeIdFinder idFinder = new PaymentTypeIdFinder(1);
        idFinder.setUsedCCChip(true);
        
        LOG.debug("-----> " + idFinder);
        assertEquals(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CHIP, idFinder.findPaymentTypeId());

        idFinder.setUsedCCChip(false);
        idFinder.setUsedCCCL(true);
        assertFalse(idFinder.isUsedCCChip());
        assertEquals(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CONTACTLESS, idFinder.findPaymentTypeId());
    }
    
    
    @Test
    public void creditCardFSwipePaymentTypeId() {
        PaymentTypeIdFinder idFinder = new PaymentTypeIdFinder(1);
        idFinder.setUsedCCFSwipe(true);
        
        LOG.debug("-----> " + idFinder);
        assertEquals(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_FSWIPE, idFinder.findPaymentTypeId());

        idFinder.setUsedCCSwipe(true);
        assertEquals(WebCoreConstants.PAYMENT_TYPE_UNKNOWN, idFinder.findPaymentTypeId());
    }
    
    
    @Test
    public void creditCardSwipePaymentTypeId() {
        PaymentTypeIdFinder idFinder = new PaymentTypeIdFinder(1);
        idFinder.setUsedCCSwipe(true);
        
        LOG.debug("-----> " + idFinder);
        assertEquals(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_SWIPE, idFinder.findPaymentTypeId());

        idFinder.setUsedCCSwipe(false);
        idFinder.setUsedCCFSwipe(true);
        assertEquals(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_FSWIPE, idFinder.findPaymentTypeId());
    }
    
    @Test
    public void creditCardTapPaymentTypeId() {
        PaymentTypeIdFinder idFinder = new PaymentTypeIdFinder(1);
        idFinder.setUsedCCCL(true);
        
        LOG.debug("-----> " + idFinder);
        assertEquals(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CONTACTLESS, idFinder.findPaymentTypeId());

        idFinder.setUsedCash(true);
        assertEquals(WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CL, idFinder.findPaymentTypeId());
    }
    
    
    @Test
    public void smartCardPaymentTypeId() {
        PaymentTypeIdFinder idFinder = new PaymentTypeIdFinder(1);
        idFinder.setUsedSC(true);

        LOG.debug("-----> " + idFinder.findPaymentTypeId());
        assertEquals(WebCoreConstants.PAYMENT_TYPE_SMART_CARD, idFinder.findPaymentTypeId());

        idFinder.setUsedCCCL(true);
        idFinder.setUsedCash(true);
        assertEquals(WebCoreConstants.PAYMENT_TYPE_UNKNOWN, idFinder.findPaymentTypeId());
    }
    
    
    @Test
    public void valueCardPaymentTypeId() {
        PaymentTypeIdFinder idFinder = new PaymentTypeIdFinder(1);
        idFinder.setUsedValueCard(true);

        LOG.debug("-----> " + idFinder);
        assertEquals(WebCoreConstants.PAYMENT_TYPE_VALUE_CARD, idFinder.findPaymentTypeId());

        idFinder.setUsedCCCL(true);
        idFinder.setUsedSC(true);
        assertEquals(WebCoreConstants.PAYMENT_TYPE_UNKNOWN, idFinder.findPaymentTypeId());
    }
    
}
