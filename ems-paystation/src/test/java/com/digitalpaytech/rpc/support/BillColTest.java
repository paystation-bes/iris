package com.digitalpaytech.rpc.support;

import org.junit.Test; 
import static org.junit.Assert.assertEquals;

public class BillColTest {
    
    @Test
    public void parse1() {
        BillCol billCol = new BillCol("1:3,2:1,10:1");
        assertEquals(3, billCol.getCount1());
        assertEquals(1, billCol.getCount2());
        assertEquals(1, billCol.getCount10());
        assertEquals("1:3,2:1,10:1", billCol.getBillCollection());
        
        assertEquals(0, billCol.getCount5());
        assertEquals(0, billCol.getCount20());
        assertEquals(0, billCol.getCount50());
    }
    
    @Test
    public void parse2() {
        BillCol billCol = new BillCol();
        billCol.setBillCollection("1:5,2:1,5:5,10:3,20:4,50:7");
        billCol.parse();
        
        assertEquals(5, billCol.getCount1());
        assertEquals(1, billCol.getCount2());
        assertEquals(5, billCol.getCount5());
        assertEquals(3, billCol.getCount10());
        assertEquals(4, billCol.getCount20());
        assertEquals(7, billCol.getCount50());
    }
    
    @Test
    public void parseEmpty() {
        BillCol billCol = new BillCol();
        billCol.parse();
        
        assertEquals(0, billCol.getCount1());
        assertEquals(0, billCol.getCount2());
        assertEquals(0, billCol.getCount5());
        assertEquals(0, billCol.getCount10());
        assertEquals(0, billCol.getCount20());
        assertEquals(0, billCol.getCount50());
    }
    
    
    @Test
    public void getBillDollars() {
        // 3 + 2 + 10 = 15
        BillCol billCol = new BillCol("1:3,2:1,10:1");
        assertEquals(1500, billCol.getBillDollars());
        
        // 5 + 2 + 25 + 30 + 80 + 350
        billCol = new BillCol("1:5,2:1,5:5,10:3,20:4,50:7");
        assertEquals(49200, billCol.getBillDollars());
    }
    
    @Test
    public void getBillCount() {
        BillCol billCol = new BillCol("1:3,2:1,10:1");
        assertEquals(5, billCol.getBillCount());

        billCol = new BillCol("1:5,2:1,5:5,10:3,20:4,50:7");
        assertEquals(25, billCol.getBillCount());
    }
    
    @Test
    public void getAmount1() {
        BillCol billCol = new BillCol("1:5,2:1,5:5,10:3,20:4,50:7");
        assertEquals(100, billCol.getAmount1());
    }
    
    @Test
    public void getAmount2() {
        BillCol billCol = new BillCol("1:5,2:1,5:5,10:3,20:4,50:7");
        assertEquals(200, billCol.getAmount2());
    }
    
    @Test
    public void getAmount5() {
        BillCol billCol = new BillCol("1:5,2:1,5:5,10:3,20:4,50:7");
        assertEquals(500, billCol.getAmount5());
    }
    
    @Test
    public void getAmount10() {
        BillCol billCol = new BillCol("1:5,2:1,5:5,10:3,20:4,50:7");
        assertEquals(1000, billCol.getAmount10());
    }
    
    @Test
    public void getAmount20() {
        BillCol billCol = new BillCol("1:5,2:1,5:5,10:3,20:4,50:7");
        assertEquals(2000, billCol.getAmount20());
    }
    
    @Test
    public void getAmount50() {
        BillCol billCol = new BillCol("1:5,2:1,5:5,10:3,20:4,50:7");
        assertEquals(5000, billCol.getAmount50());
    }
    
    @Test
    public void parseTrim() {
        BillCol billCol = new BillCol();
        billCol.setBillCollection("1:5,5:1,10:0, 20:1");
        billCol.parse();
        
        assertEquals(0, billCol.getCount10());
        assertEquals(1, billCol.getCount20());
        assertEquals(2000, billCol.getAmount20());
        
        billCol.setBillCollection("1:5,  5:1  , 10:2,  20:2    ,    50:3");
        billCol.parse();
        assertEquals(5, billCol.getCount1());
        assertEquals(1, billCol.getCount5());
        assertEquals(2, billCol.getCount10());
        assertEquals(2, billCol.getCount20());
        assertEquals(3, billCol.getCount50());
    }

}
