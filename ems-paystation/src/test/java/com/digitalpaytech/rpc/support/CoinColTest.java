package com.digitalpaytech.rpc.support;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class CoinColTest {
    
    @Test
    public void parse1() {
        CoinCol coinCol = new CoinCol("25:2,10:4,100:1");
        assertEquals(2, coinCol.getCount025());
        assertEquals(4, coinCol.getCount010());
        assertEquals(1, coinCol.getCount100());
        assertEquals("25:2,10:4,100:1", coinCol.getCoinCollection());
        
        assertEquals(0, coinCol.getCount005());
        assertEquals(0, coinCol.getCount200());
    }
    
    @Test
    public void parse2() {
        CoinCol coinCol = new CoinCol();
        coinCol.setCoinCollection("5:5,10:1,25:5,100:3,200:4");
        coinCol.parse();
        
        assertEquals(5, coinCol.getCount005());
        assertEquals(1, coinCol.getCount010());
        assertEquals(5, coinCol.getCount025());
        assertEquals(3, coinCol.getCount100());
        assertEquals(4, coinCol.getCount200());
    }
    
    @Test
    public void parseEmpty() {
        CoinCol coinCol = new CoinCol();
        coinCol.parse();
        
        assertEquals(0, coinCol.getCount005());
        assertEquals(0, coinCol.getCount010());
        assertEquals(0, coinCol.getCount025());
        assertEquals(0, coinCol.getCount100());
        assertEquals(0, coinCol.getCount200());
    }
    
    @Test
    public void getCoinDollars() {
        // 25 + 10 + 125 + 300 + 800 = 1260 
        CoinCol coinCol = new CoinCol("5:5,10:1,25:5,100:3,200:4");
        assertEquals(1260, coinCol.getCoinDollars());
        
        // 50 + 40 + 100 = 190
        coinCol = new CoinCol("25:2,10:4,100:1");
        assertEquals(190, coinCol.getCoinDollars());
    }
    
    
    @Test
    public void getCoinCount() {
        CoinCol coinCol = new CoinCol("5:5,10:1,25:5,100:3,200:4");
        assertEquals(18, coinCol.getCoinCount());
        
        coinCol = new CoinCol("25:2,10:4,100:1");
        assertEquals(7, coinCol.getCoinCount());
    }
    
    
    @Test
    public void getCoinDollarsDecimal() {
        CoinCol coinCol = new CoinCol("5:5,10:1,25:5,100:3,200:4");
        assertEquals("12.60", coinCol.getCoinDollarsDecimal().toString());
    }
    
    @Test
    public void parseTrim() {
        CoinCol coinCol = new CoinCol("    5:1       ,  10:2  , 25:0      , 100:6,   200:4    ");
        assertEquals(1, coinCol.getCount005());
        assertEquals(2, coinCol.getCount010());
        assertEquals(0, coinCol.getCount025());
        assertEquals(6, coinCol.getCount100());
        assertEquals(4, coinCol.getCount200());
    }


}
