package com.digitalpaytech.servlet;

import org.junit.Test;
import static org.junit.Assert.*;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.HashAlgorithmType;
import com.digitalpaytech.dto.crypto.HashAlgorithmData;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.HashAlgorithmTypeService;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.impl.HashAlgorithmTypeServiceImpl;

public class PosBaseServletTest {
    private static final String LINUX_PS_SERIAL_NUM = "500000070003";
    
    @Test
    public void getAlgorithmTypeId() {
        final PosBaseServlet servlet = new PosBaseServlet();
        servlet.setPointOfSaleService(mockPointOfSaleService());
        servlet.setHashAlgorithmTypeService(mockHashAlgorithmTypeService());
        
        assertEquals(2, servlet.getAlgorithmTypeId("2", LINUX_PS_SERIAL_NUM, true).intValue());
        assertEquals(5, servlet.getAlgorithmTypeId("2", "500000070002", true).intValue());
        assertEquals(1, servlet.getAlgorithmTypeId("1", "500000070001", false).intValue());
        assertEquals(1, servlet.getAlgorithmTypeId(null, "500000070004", false).intValue());
        assertEquals(1, servlet.getAlgorithmTypeId("", "500000070005", false).intValue());
        assertEquals(2, servlet.getAlgorithmTypeId("2", "BOSS", false).intValue());
        assertEquals(1, servlet.getAlgorithmTypeId("1", "boss", false).intValue());
    }
    
    
    private HashAlgorithmTypeService mockHashAlgorithmTypeService() {
        final HashAlgorithmTypeService hashAlgServ = new HashAlgorithmTypeServiceImpl() {
            @Override
            public HashAlgorithmData findForMessageDigest(final Integer hashAlgorithmTypeId) {
                final HashAlgorithmType type = new HashAlgorithmType();
                type.setId(hashAlgorithmTypeId);
                return new HashAlgorithmData(type, null);
            }
        };
        return hashAlgServ;
    }
    
    private PointOfSaleService mockPointOfSaleService() {
        final PointOfSaleService pointOfSaleService = new PointOfSaleServiceImpl() {
            @Override
            public PointOfSale findPointOfSaleBySerialNumber(final String serialNumber) {
                final PointOfSale pos = new PointOfSale();
                if (serialNumber.equals(LINUX_PS_SERIAL_NUM)) {
                    pos.setIsLinux(true);
                } else {
                    pos.setIsLinux(false);
                }
                pos.setSerialNumber(serialNumber);
                return pos;
            }
        };
        return pointOfSaleService;
    }
}
