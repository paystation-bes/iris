package com.digitalpaytech.servlet;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipInputStream;

import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.matchers.JUnitMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;

import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.client.BadCardClient;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.domain.AuthorizationType;
import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerBadCard;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerPropertyType;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MerchantStatusType;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosStatus;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.domain.ServiceAgreement;
import com.digitalpaytech.domain.SubscriptionType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.ribbon.mock.MockClientFactory;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerBadCardService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.ServiceAgreementService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.CardRetryTransactionServiceImpl;
import com.digitalpaytech.service.impl.CardTypeServiceImpl;
import com.digitalpaytech.service.impl.CustomerCardTypeServiceImpl;
import com.digitalpaytech.service.impl.CustomerServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.LinkBadCardServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.util.StableDateUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

@SuppressWarnings({ "PMD.UnusedPrivateField", "PMD.ExcessiveImports", "PMD.TooManyFields" })
public class DownloadBadCardFileIntegrationTest {
    
    private static final String TEST_PARKING = "TestParking";
    
    private static final int TEST_ID = 1;
    
    @InjectMocks
    private DownloadServlet downloadServlet;
    
    @Mock
    private PointOfSaleService pointOfSaleService;
    
    @Mock
    private ServiceAgreementService serviceAgreementService;
    
    @InjectMocks
    private CustomerServiceImpl customerService;
    
    @Mock
    private UserAccountService userAccountService;
    
    @Mock
    private AuthenticationManager authenticationManager;
    
    @InjectMocks
    private MerchantAccountServiceImpl merchantAcccountService;
    
    @InjectMocks
    private CardTypeServiceImpl cardTypeService;
    
    @InjectMocks
    private EntityServiceImpl entityService;
    
    @InjectMocks
    private CustomerCardTypeServiceImpl customerCardTypeService;
    
    @Mock
    private CustomerBadCardService customerBadCardService;
    
    @InjectMocks
    private MockClientFactory client;
    
    @InjectMocks
    private CardRetryTransactionServiceImpl cardRetryTransactionService;
    
    @InjectMocks
    private LinkBadCardServiceImpl linkBadCardService;
    
    @Mock
    private CustomerAdminService customerAdminService;
    
    private final TestContext ctx = TestContext.getInstance();
    
    private final EntityDaoTest entityDao = new EntityDaoTest();
    
    private MerchantAccount merchantAccount;
    
    private Customer customer;
    
    @Before
    public void setUp() throws IllegalAccessException {
        MockitoAnnotations.initMocks(this);
        this.ctx.autowiresFromTestObject(this);
        
        this.customer = new Customer();
        this.customer.setName(TEST_PARKING);
        this.customer.setId(1);
        this.customer.setIsMigrated(true);
        this.customer.setUnifiId(1);
        this.entityDao.save(this.customer);
        
        final UserAccount userAccount = new UserAccount();
        userAccount.setId(1);
        userAccount.setCustomer(this.customer);
        
        final CustomerSubscription creditCardSubscription = new CustomerSubscription();
        final SubscriptionType creditCardSubscriptionType = new SubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_REALTIME_CC_PROCESSING);
        creditCardSubscription.setCustomer(this.customer);
        creditCardSubscription.setIsEnabled(true);
        creditCardSubscription.setSubscriptionType(creditCardSubscriptionType);
        
        this.entityDao.save(creditCardSubscription);
        
        this.merchantAccount = new MerchantAccount();
        this.merchantAccount.setIsLink(false);
        this.merchantAccount.setIsValidated(true);
        this.merchantAccount.setMerchantStatusType(new MerchantStatusType(TEST_ID, "Enabled", new Date(), TEST_ID));
        this.merchantAccount.setCustomer(this.customer);
        final Processor processor = new Processor();
        processor.setId(TEST_ID);
        this.merchantAccount.setProcessor(processor);
        this.entityDao.save(this.merchantAccount);
        
        final Permission permission = new Permission();
        permission.setId(WebSecurityConstants.PERMISSION_MANAGE_BANNED_CARDS);
        
        Mockito.when(this.userAccountService.findPermissions(Mockito.anyInt())).thenReturn(Arrays.asList(permission));
        Mockito.when(this.userAccountService.findUndeletedUserAccount(Mockito.anyString())).thenReturn(userAccount);
        Mockito.when(this.serviceAgreementService.findLatestServiceAgreementByCustomerId(TEST_ID)).thenReturn(new ServiceAgreement());
        Mockito.when(this.customerAdminService.getCustomerPropertyByCustomerIdAndCustomerPropertyType(Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(new CustomerProperty(new CustomerPropertyType(), null, "1", null, 1));
    }
    
    @After
    public void tearDown() {
        this.ctx.getDatabase().clear();
    }
    
    @Test
    public void testDownloadIrisBadCard() throws IOException, IllegalAccessException {
        
        this.merchantAccount.setIsLink(false);
        this.entityDao.saveOrUpdate(this.merchantAccount);
        
        @SuppressWarnings("PMD.UseConcurrentHashMap")
        final Map<String, String> params = buildRequestParams();
        
        final MockHttpServletRequest req = new MockHttpServletRequest();
        req.addParameters(params);
        
        final MockHttpServletResponse res = new MockHttpServletResponse();
        
        final String hash1 = "2AFq9jt3kPdV6tPD6XaPnkRHktM=";
        
        final CardType cardType = new CardType();
        cardType.setId(1);
        
        final AuthorizationType authorizationType = new AuthorizationType(0);
        final Set<CustomerBadCard> badCardSet = new HashSet<>();
        
        final CustomerCardType customerCardType = new CustomerCardType(cardType, this.customer, authorizationType, "CardTypeName", "123",
                "description", false, new Date(), 1, badCardSet);
        
        final CustomerBadCard customerBadCard = new CustomerBadCard(customerCardType, hash1, new Date(), new Date(), 1);
        
        badCardSet.add(customerBadCard);
        final PointOfSale pos = new PointOfSale();
        pos.setId(1);
        
        Mockito.when(this.pointOfSaleService.findPointOfSalesByCustomerId(1)).thenReturn(Arrays.asList(pos));
        
        Mockito.when(this.customerBadCardService.findNoExpiredBadCreditCard(1, 1)).thenReturn(Arrays.asList(customerBadCard));
        this.downloadServlet.doGet(req, res);
        
        final String result = getResultAsString(res);
        
        assertThat(result, JUnitMatchers.containsString(hash1));
    }
    
    @Test
    public void testDownloadLinkBadCard() throws IOException, IllegalAccessException {
        
        this.merchantAccount.setIsLink(true);
        this.entityDao.saveOrUpdate(this.merchantAccount);
        
        @SuppressWarnings("PMD.UseConcurrentHashMap")
        final Map<String, String> params = buildRequestParams();
        
        final MockHttpServletRequest req = new MockHttpServletRequest();
        req.addParameters(params);
        
        final MockHttpServletResponse res = new MockHttpServletResponse();
        
        final String hash1 = "KwHtwo4Xpu2yF4Yv660Y4D5h9C9D+VQ1iMvZyh9MuN8=";
        final String hash2 = "KwHtwo4Xpu2yF4Yv660Y4D5h9C9D+VQ1iMvZyh9MuN9=";
        this.client.prepareForSuccessRequest(BadCardClient.class, ("{\"status\":{\"responseStatus\":\"SUCCESS\",\"errors\":[]},\"response\":[" + "\""
                                                                   + hash1 + "\"," + "\"" + hash2 + "\"]}").getBytes());
        
        this.downloadServlet.doGet(req, res);
        
        final String result = getResultAsString(res);
        
        assertThat(result, JUnitMatchers.containsString(hash1));
        assertThat(result, JUnitMatchers.containsString(hash2));
    }
    
    @Test
    public void testDownloadLinkBadCardAndIrisCardRetry() throws IOException, IllegalAccessException {
        
        Mockito.when(this.pointOfSaleService.findPosIdsByCustomerId(Mockito.anyInt())).thenReturn(Arrays.asList(1));
        
        this.merchantAccount.setIsLink(true);
        this.entityDao.saveOrUpdate(this.merchantAccount);
        
        MerchantAccount irisMerchantAccount = new MerchantAccount();
        irisMerchantAccount.setIsLink(false);
        irisMerchantAccount.setIsValidated(true);
        irisMerchantAccount.setMerchantStatusType(new MerchantStatusType(TEST_ID, "Enabled", new Date(), TEST_ID));
        irisMerchantAccount.setCustomer(this.customer);
        final Processor processor = new Processor();
        processor.setId(TEST_ID);
        irisMerchantAccount.setProcessor(processor);
        this.entityDao.save(irisMerchantAccount);
        
        PosStatus posStatus = new PosStatus();
        
        posStatus.setIsActivated(true);
        posStatus.setIsVisible(true);
        posStatus.setIsDecommissioned(false);
        posStatus.setIsDeleted(false);
        
        PointOfSale pointOfSale = new PointOfSale();
        pointOfSale.setCustomer(this.customer);
        pointOfSale.setId(1);
        pointOfSale.setPosStatus(posStatus);
        this.entityDao.save(pointOfSale);
        
        posStatus.setPointOfSale(pointOfSale);
        this.entityDao.save(posStatus);
        CardRetryTransaction cardRetryTransaction = new CardRetryTransaction();
        cardRetryTransaction.setPointOfSale(pointOfSale);
        cardRetryTransaction.setCardExpiry((short) (StableDateUtil.createCurrentYYMM() + 1));
        cardRetryTransaction.setNumRetries(Integer.MAX_VALUE);
        cardRetryTransaction.setBadCardHash("FIt0uVTTRNCPcen+fkvMoDnFn7M=");
        
        this.entityDao.save(cardRetryTransaction);
        
        @SuppressWarnings("PMD.UseConcurrentHashMap")
        final Map<String, String> params = buildRequestParams();
        
        final MockHttpServletRequest req = new MockHttpServletRequest();
        req.addParameters(params);
        
        final MockHttpServletResponse res = new MockHttpServletResponse();
        
        final String linkHash1 = "KwHtwo4Xpu2yF4Yv660Y4D5h9C9D+VQ1iMvZyh9MuN8=";
        final String linkHash2 = "AwHtwo4Xpu2yF4Yv660Y4D5h9C9D+VQ1iMvZyh9MuN9=";
        final String localHash1 = "FIt0uVTTRNCPcen+fkvMoDnFn7M=";
        final String localHash2 = "ZIt0uVTTRNCPcen+fkvMoDnFn7N=";
        
        this.client.prepareForSuccessRequest(BadCardClient.class, ("{\"status\":{\"responseStatus\":\"SUCCESS\",\"errors\":[]},\"response\":[" + "\""
                                                                   + linkHash1 + "\"," + "\"" + linkHash2 + "\"]}").getBytes());
        
        this.downloadServlet.doGet(req, res);
        
        final String result = getResultAsString(res);
        String[] splitResult = StringUtils.strip(result).split("\n");
        
        List<String> expectedList = new ArrayList<>();
        expectedList.add(linkHash1);
        expectedList.add(linkHash2);
        expectedList.add(localHash1);
        expectedList.add(localHash2);
        Collections.sort(expectedList);
        
        assertThat(Arrays.asList(splitResult), is(Arrays.asList(splitResult)));
    }
    
    private String getResultAsString(final MockHttpServletResponse res) throws IOException {
        final byte[] zippedResult = res.getContentAsByteArray();
        final ByteArrayInputStream bai = new ByteArrayInputStream(zippedResult);
        final ZipInputStream zis = new ZipInputStream(bai);
        zis.getNextEntry();
        
        final byte[] buffer = new byte[1024];
        zis.read(buffer);
        
        return new String(buffer);
    }
    
    private Map<String, String> buildRequestParams() {
        @SuppressWarnings("PMD.UseConcurrentHashMap")
        final Map<String, String> params = new HashMap<String, String>();
        params.put("type", PosBaseServlet.TYPE_BAD_CREDITCARD);
        params.put("commaddress", "500000070001");
        params.put("customerName", TEST_PARKING);
        params.put("userName", "TestParkingUser");
        params.put("passwordEnc", "password");
        return params;
    }
    
}
