package com.digitalpayment.bdd.rpc.ps2.eventhandler;

import java.text.Format;
import java.text.SimpleDateFormat;

import junit.framework.Assert;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.digitalpaytech.bdd.rpc.ps2.util.TestXMLRPCBehavior;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.dto.paystation.EventData;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.SystemException;
import com.digitalpaytech.rpc.ps2.EventHandler;
import com.digitalpaytech.rpc.ps2.PS2XMLBuilder;
import com.digitalpaytech.rpc.ps2.PS2XMLBuilderBase;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.PosEventDelayedService;
import com.digitalpaytech.service.RawSensorDataService;
import com.digitalpaytech.service.impl.AccessPointServiceImpl;
import com.digitalpaytech.service.impl.CustomerAgreementServiceImpl;
import com.digitalpaytech.service.impl.CustomerServiceImpl;
import com.digitalpaytech.service.impl.ModemTypeServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.impl.PosEventDelayedServiceImpl;
import com.digitalpaytech.service.paystation.EventAlertService;
import com.digitalpaytech.service.paystation.impl.EventModemServiceImpl;
import com.digitalpaytech.util.DateUtil;

public class TestXMLRPCEvents implements TestXMLRPCBehavior {
    
    public static final String MESSAGE_NUMBER = "1";
    
    @Mock
    private MailerService mailerService;
    
    @InjectMocks
    private CustomerServiceImpl customerService;
    
    @Mock
    private EventAlertService eventAlertService;
    
    @Mock
    private RawSensorDataService rawSensorDataService;
    
    @InjectMocks
    private PointOfSaleServiceImpl pointOfSaleService;
    
    @InjectMocks
    private CustomerAgreementServiceImpl customerAgreementService;
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @InjectMocks
    private EventModemServiceImpl eventModemService;
    
    @InjectMocks
    private AccessPointServiceImpl accessPointService;
    
    @InjectMocks
    private ModemTypeServiceImpl modemTypeService;
    
    @InjectMocks
    private PosEventDelayedServiceImpl posEventDelayedService;
    
    private EventHandler eventHandler;
    
    public TestXMLRPCEvents() {
        MockitoAnnotations.initMocks(this);
        this.eventHandler = new EventHandler(MESSAGE_NUMBER);
        
        TestContext.getInstance().autowiresAttributes(this);
    }
    
    @Override
    public final String createSetup(final Object dto) {
        
        final Format formatter = new SimpleDateFormat(DateUtil.COLON_DELIMITED_DATE_FORMAT);
        final String dateString = formatter.format(((EventData) dto).getTimeStamp());
        
        this.eventHandler.setPosSerialNumber(TestContext.getInstance().getDatabase()
                .findById(PointOfSale.class, ((EventData) dto).getPointOfSaleId()).getSerialNumber());
        this.eventHandler.setAction(((EventData) dto).getAction());
        this.eventHandler.setInformation(((EventData) dto).getInformation());
        this.eventHandler.setType(((EventData) dto).getType());
        try {
            this.eventHandler.setTimeStamp(dateString);
        } catch (InvalidDataException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    @Override
    public final void process(final String xmlMessage) {
        final PS2XMLBuilder xmlBuilder = new PS2XMLBuilder(PS2XMLBuilderBase.MESSAGE_FOR_PAYSTATION2_ELEMENT_NAME_STRING);
        xmlBuilder.setOriginalMessage(xmlMessage);
        try {
            xmlBuilder.initialize("user_id", "user_password");
        } catch (InvalidDataException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (SystemException e) {
            e.printStackTrace();
            Assert.fail();
        }
        this.eventHandler.process(xmlBuilder);
        
    }
}
