Narrative:
In order to test XMLRPC transaction processing
As a user
I want to send paystation transactions and assert the desired behavior

Scenario: Successful 2$ transaction using TD Merchant Processor (Pre-Auth / Post-Auth)
Given there exists a Customer Property Type where the
id is 11
name is jurisdiction type preferred
Given there exists a Customer Property Type where the
id is 12
name is jurisdiction type limited
And there exists a customer where the 
customer Name is Mackenzie Parking
unifi id is 1
is migrated
query spaces by is 1
Jurisdiction type preferred is 1
Jurisdiction type limited is 2
is subscribed to Real-Time Credit Card Processing
And there exists a location where the 
location name is Downtown
And there exists a merchant account where the
name is TestTDAccount
customer is Mackenzie Parking
merchant account status is enabled
processor is processor.tdmerchant
Field1 is 17681535
Field2 is D9231CC32Ac1431686121828E1a7e171
is not link
And there exists a pay station where the  
serial number is 500000070001
Name is paystation1
is activated
location is Downtown 
customer is Mackenzie Parking
Credit card Merchant Account is TestTDAccount
And the Card Type Service response with message 
{
  "response": "MasterCard",
  "status": {
    "errors": null,
    "responseStatus": "SUCCESS"
  }
}
When iris receives an incoming preauth transaction message where the
serial number is 500000070001
card data is 4030000010001234=1907101000000012300XXXXX
charge amount is 2
reference id is 535
authorization number is 12345678
Then there exists a preauth transaction data where the
preauth id is 1
Charge amount is 200
Last 4 digits of card is 1234
is Approved
serial number is 500000070001
pay station ref id is 535
authorization number is 12345678
When iris receives an incoming transaction message where the
serial number is 500000070001
ticket number is 1
license plate number is ABC123
stall number is 1
type is Regular
purchase date is 2017:03:01:22:10:36:GMT
parking ticket purchased is 60
original amount is 200
charged amount is 2.00
coupon number is left blank
cash paid is 0.00
card paid is 2.00
card authorization id is 12345678
ems preauth id is 1
is not refund slip
expiry time is 2017:03:01:23:10:36:GMT
rate name is early bird
rate id is 1
rate value is 400
limited rate is true
preferred rate is true
tax type is NA
Then there exists a purchase where the 
location is Downtown
card paid amount is 200
payment type is cc (swipe)
transaction type is regular
Customer Name is Mackenzie Parking
serial number is 500000070001
original amount is 200
charged amount is 200
And there exists a permit where the
location name  is Downtown
stall number is 1
license plate number is ABC123
And there is no preauth transaction data where the
preauth id is 1
And there exists a processor transaction where the 
paystation is 500000070001
processor transaction type is Real-Time
amount is 200
last 4 digits of card is 1234
is approved
authorization number is TEST:1
Then the message is sent to kafka's topic 'Limited Preferred Rate' with data 
{
  "licensePlate": "ABC123",
  "customerId": 1,
  "locationName": "Downtown",
  "isPreferred": true,
  "isPreferredByLocation": false,
  "isLimited": true,
  "isLimitedByLocation": true,
  "localDate": "2017-03-01",
  "purchaseUTC": "2017-03-01T22:10:36.0Z"
}

Scenario: Scala encrypted card in PreAuth when merchant account is link
Given there exists a customer where the 
customer Name is Mackenzie Parking
unifi id is 1
is migrated
query spaces by is 1
Jurisdiction type preferred is 1
Jurisdiction type limited is 2
is subscribed to Real-Time Credit Card Processing
And there exists a location where the 
location name is Downtown
And there exists a processor where the 
processor name is processor.paymentech.link
And there exists a merchant account where the
name is TestLinkMerchantAccount
customer is Mackenzie Parking
merchant account status is enabled
is link
terminal token is 9f4bfda9-1b9b-4bbe-87ae-827e9ca38a12
processor is processor.paymentech.link
And there exists a pay station where the  
serial number is 500000070001
Name is paystation1
is activated
location is Downtown 
customer is Mackenzie Parking
Credit card Merchant Account is TestLinkMerchantAccount
When iris receives an incoming preauth transaction message where the
serial number is 500000070001
card data is 4030000010001234=1907101000000012300XXXXXXX
charge amount is 2
Then a preAuth request is not sent to Core CPS
Then there is no preauth transaction data where the 
Charge amount is 200
Last 4 digits of card is 2123
is Approved
serial number is 500000070001
authorization number is 192539
card data is blank
card type is MasterCard
reference number is 000312
card token is 0d1a2dfd-d707-4a7d-b4aa-4795d42af915
charge token is 6121a1c7-5a02-476e-9ae5-239ca4a45e00

Scenario: Unencrypted card in PreAuth when merchant account is link
Given there exists a customer where the 
customer Name is Mackenzie Parking
unifi id is 1
is migrated
query spaces by is 1
Jurisdiction type preferred is 1
Jurisdiction type limited is 2
is subscribed to Real-Time Credit Card Processing
And there exists a location where the 
location name is Downtown
And there exists a processor where the 
processor name is processor.paymentech.link
And there exists a merchant account where the
name is TestMerchantAccount
customer is Mackenzie Parking
merchant account status is enabled
is link
terminal token is 9f4bfda9-1b9b-4bbe-87ae-827e9ca38a12
processor is processor.paymentech.link
And there exists a pay station where the  
serial number is 500000070001
Name is paystation1
is activated
location is Downtown 
customer is Mackenzie Parking
Credit card Merchant Account is TestMerchantAccount
When iris receives an incoming preauth transaction message where the
serial number is 500000070001
card data is 12345
charge amount is 2
Then a preAuth request is not sent to Core CPS


Scenario: Core CPS Card transaction that no longer has merchant account
Given there exists a Customer Property Type where the
id is 11
name is jurisdiction type preferred
Given there exists a Customer Property Type where the
id is 12
name is jurisdiction type limited
And there exists a customer where the 
customer Name is Mackenzie Parking
unifi id is 1
is migrated
query spaces by is 1
jurisdiction type preferred is 2
jurisdiction type limited is 1
is subscribed to Real-Time Credit Card Processing
And there exists a location where the 
location name is Downtown
And there exists a merchant account where the
name is TestCoreCPSOld
customer is Mackenzie Parking
merchant account status is enabled
processor is processor.creditcall
Field1 is 17681535
Field2 is D9231CC32Ac1431686121828E1a7e171
terminal token is 4c43ffc1-e6f1-4239-85ae-ca09fa1cce38
Is Link
And there exists a pay station where the  
serial number is 500000070001
Name is paystation1
is activated
location is Downtown 
customer is Mackenzie Parking
is new merchant account
And there exists a merchant pos where the
pay station is 500000070001
card type is credit card
name is TestCoreCPSOld
is deleted
When iris receives an incoming transaction message where the
serial number is 500000070001
ticket number is 1
lot number is Student Parking
license plate number is ABC123
stall number is 1
type is Regular
purchase date is 2017:03:01:22:10:36:GMT
transaction date is 2017:03:01:22:10:36:GMT
parking ticket purchased is 60
original amount is 200
charged amount is 2.00
coupon number is left blank
cash paid is 0.00
card paid is 2.00
cps response is {"chargeToken":"ce40c262-32e7-46a2-8f62-d46b6c1f5d4a","terminalToken":"4c43ffc1-e6f1-4239-85ae-ca09fa1cce38","authorizationNumber":"095308","cardType":"VISA","last4Digits":"8291","referenceNumber":"000007","processorTransactionId":"00000001","optionalData":null,"processedDate":"2017-10-04T15:42:28.892Z"}
card authorization id is 095308
is not refund slip
limited rate is true
preferred rate is true
expiry time is 2017:03:01:23:10:36:GMT
rate name is early bird
rate id is 1
rate value is 400
tax type is NA
Then there exists a transaction card token where the
processor transaction authorization number is 095308
token is not left blank
And there exists a processor transaction where the 
paystation is 500000070001
amount is 200
last 4 digits of card is 8291
is approved
authorization number is 095308
processor transaction id is 00000001
merchant account is TestCoreCPSOld
Then the message is sent to kafka's topic 'Limited Preferred Rate' with data 
{
  "licensePlate": "ABC123",
  "customerId": 1,
  "locationName": "Downtown",
  "isPreferred": true,
  "isPreferredByLocation": true,
  "isLimited": true,
  "isLimitedByLocation": false,
  "localDate": "2017-03-01",
  "purchaseUTC": "2017-03-01T22:10:36.0Z"
}


Scenario: Core CPS Card transaction with different merchant account
Given there exists a Customer Property Type where the
id is 11
name is jurisdiction type preferred
Given there exists a Customer Property Type where the
id is 12
name is jurisdiction type limited
And there exists a customer where the 
customer Name is Mackenzie Parking
unifi id is 1
is migrated
query spaces by is 1
jurisdiction type preferred is 2
jurisdiction type limited is 1
is subscribed to Real-Time Credit Card Processing
And there exists a location where the 
location name is Downtown
And there exists a merchant account where the
name is TestCoreCPSOld
customer is Mackenzie Parking
merchant account status is enabled
processor is processor.creditcall
Field1 is 17681535
Field2 is D9231CC32Ac1431686121828E1a7e171
terminal token is 4c43ffc1-e6f1-4239-85ae-ca09fa1cce38
Is Link
And there exists a merchant account where the
name is TestCoreCPSNew
customer is Mackenzie Parking
merchant account status is enabled
processor is processor.creditcall
Field1 is Elavon
Field2 is merchantID
Field3 is 11223344
Field4 is TRANSACTION
Field5 is CAD
Is Link
terminal token is 4c43ffc1-e6f1-4239-85ae-ca09fa1cce40
And there exists a pay station where the  
serial number is 500000070001
Name is paystation1
is activated
location is Downtown 
customer is Mackenzie Parking
Credit card Merchant Account is TestCoreCPSNew
is new merchant account
And there exists a merchant pos where the
pay station is 500000070001
card type is credit card
name is TestCoreCPSNew
And there exists a merchant pos where the
pay station is 500000070001
card type is credit card
name is TestCoreCPSOld
is deleted
When iris receives an incoming transaction message where the
serial number is 500000070001
ticket number is 1
lot number is Student Parking
license plate number is ABC123
stall number is 1
type is Regular
purchase date is 2017:03:01:22:10:36:GMT
transaction date is 2017:03:01:22:10:36:GMT
parking ticket purchased is 60
original amount is 200
charged amount is 2.00
coupon number is left blank
cash paid is 0.00
card paid is 2.00
cps response is {"chargeToken":"ce40c262-32e7-46a2-8f62-d46b6c1f5d4a","terminalToken":"4c43ffc1-e6f1-4239-85ae-ca09fa1cce38","authorizationNumber":"095308","cardType":"VISA","last4Digits":"8291","referenceNumber":"000007","processorTransactionId":"00000001","optionalData":null,"processedDate":"2017-10-04T15:42:28.892Z"}
card authorization id is 095308
is not refund slip
limited rate is true
preferred rate is true
expiry time is 2017:03:01:23:10:36:GMT
rate name is early bird
rate id is 1
rate value is 400
tax type is NA
Then there exists a transaction card token where the
processor transaction authorization number is 095308
token is not left blank
And there exists a processor transaction where the 
paystation is 500000070001
amount is 200
last 4 digits of card is 8291
is approved
authorization number is 095308
processor transaction id is 00000001
merchant account is TestCoreCPSOld
Then the message is sent to kafka's topic 'Limited Preferred Rate' with data 
{
  "licensePlate": "ABC123",
  "customerId": 1,
  "locationName": "Downtown",
  "isPreferred": true,
  "isPreferredByLocation": true,
  "isLimited": true,
  "isLimitedByLocation": false,
  "localDate": "2017-03-01",
  "purchaseUTC": "2017-03-01T22:10:36.0Z"
}

Scenario: Core CPS Card transaction 
Given there exists a Customer Property Type where the
id is 11
name is jurisdiction type preferred
Given there exists a Customer Property Type where the
id is 12
name is jurisdiction type limited
And there exists a customer where the 
customer Name is Mackenzie Parking
unifi id is 1
is migrated
query spaces by is 1
jurisdiction type preferred is 2
jurisdiction type limited is 1
is subscribed to Real-Time Credit Card Processing
And there exists a location where the 
location name is Downtown
And there exists a merchant account where the
name is TestCoreCPS
customer is Mackenzie Parking
merchant account status is enabled
processor is processor.creditcall
Field1 is 17681535
Field2 is D9231CC32Ac1431686121828E1a7e171
terminal token is c7979190-dac1-4e37-af7e-35f289b9c924
Is Link
And there exists a pay station where the  
serial number is 500000070001
Name is paystation1
is activated
location is Downtown 
customer is Mackenzie Parking
Credit card Merchant Account is TestCoreCPS
is new merchant account
When iris receives an incoming transaction message where the
serial number is 500000070001
ticket number is 1
lot number is Student Parking
license plate number is ABC123
stall number is 1
type is Regular
purchase date is 2017:03:01:22:10:36:GMT
transaction date is 2017:03:01:22:10:36:GMT
parking ticket purchased is 60
original amount is 200
charged amount is 2.00
coupon number is left blank
cash paid is 0.00
card paid is 2.00
cps response is {"chargeToken":"ea8f1db9-esd8-42dd-b978-a98b8206ccdf","terminalToken":"c7979190-dac1-4e37-af7e-35f289b9c924","authorizationNumber":"095308","cardType":"VISA","last4Digits":"8291","referenceNumber":"000007","processorTransactionId":"00000001","optionalData":null,"processedDate":"2017-10-04T15:42:28.892Z"}
card authorization id is 095308
is not refund slip
limited rate is true
preferred rate is true
expiry time is 2017:03:01:23:10:36:GMT
rate name is early bird
rate id is 1
rate value is 400
tax type is NA
Then there exists a transaction card token where the
processor transaction authorization number is 095308
token is not left blank
And there exists a processor transaction where the 
paystation is 500000070001
amount is 200
last 4 digits of card is 8291
is approved
authorization number is 095308
processor transaction id is 00000001
merchant account is TestCoreCPS
Then the message is sent to kafka's topic 'Limited Preferred Rate' with data 
{
  "licensePlate": "ABC123",
  "customerId": 1,
  "locationName": "Downtown",
  "isPreferred": true,
  "isPreferredByLocation": true,
  "isLimited": true,
  "isLimitedByLocation": false,
  "localDate": "2017-03-01",
  "purchaseUTC": "2017-03-01T22:10:36.0Z"
}


Scenario: Sending a Store & Forward transaction for TD Merchant Services that is Denied
Given there exists a customer where the 
customer Name is Mackenzie Parking
max offline retry is 5
unifi id is 1
is migrated
query spaces by is 1
And there exists a location where the 
location name is Downtown
And there exists a merchant account where the  
name is TestTDAccount
customer is Mackenzie Parking
processor is processor.tdmerchant
merchant account status is enabled
Close Time is 16
Field1 is 17681535
Field2 is D9231CC32Ac1431686121828E1a7e171
Field3 is CAD
And there exists a pay station where the  
serial number is 500000070002
is activated
location is Downtown 
customer is Mackenzie Parking
Credit card Merchant Account is TestTDAccount
When iris receives an incoming transaction message where the
serial number is 500000070002
ticket number is 1
lot number is Student Parking
license plate number is ABC123
stall number is 1
type is regular
purchase date is 2017:02:29:20:40:36:GMT
parking ticket purchased is 60
original amount is 401
charged amount is 4.01
coupon number is left blank
cash paid is 0.00
card paid is 4.01
card data is 4124939999999990=191290154321396145
is not refund slip
expiry time is 2017:01:26:21:12:36:GMT
rate name is early bird
rate id is 1
rate value is 400
tax type is NA
Then there exists a purchase where the 
card paid amount is 401
payment type is cc (swipe)
transaction type is regular
Customer Name is Mackenzie Parking
serial number is 500000070002
And there exists a permit where the
location name is Downtown
stall number is 1
license plate number is ABC123
And there exists a card retry transaction where the 
pay station is 500000070002
amount is 401

Scenario: New Merchant Account tag is triggered when using an emv merchant account
Given there exists a customer where the 
customer Name is Mackenzie Parking
unifi id is 1
is migrated
query spaces by is 1
Jurisdiction type preferred is 1
Jurisdiction type limited is 2
is subscribed to Real-Time Credit Card Processing
And there exists a location where the 
location name is Downtown
And there exists a merchant account where the
name is TestNotEMV
customer is Mackenzie Parking
merchant account status is enabled
processor is processor.tdmerchant
Field1 is 17681535
Field2 is D9231CC32Ac1431686121828E1a7e171
And there exists a pay station where the  
serial number is 500000070001
Name is paystation1
is activated
location is Downtown 
customer is Mackenzie Parking
Credit card Merchant Account is TestNotEMV
When iris receives an incoming transaction message with message number 1 where the
serial number is 500000070001
ticket number is 1
lot number is Student Parking
license plate number is ABC123
stall number is 1
type is Regular
purchase date is 2017:03:01:22:10:36:GMT
parking ticket purchased is 60
original amount is 200
charged amount is 2.00
coupon number is left blank
cash paid is 0.00
card paid is 2.00
card authorization id is 12345678
ems preauth id is 5
is not refund slip
expiry time is 2017:03:01:23:10:36:GMT
rate name is early bird
rate id is 1
rate value is 400
tax type is NA
Then pay station with message number 1 will not contain EMVMerchantAccountNotification


Scenario: New Merchant Account tag is not triggered when not using an emv merchant account
Given there exists a customer where the 
customer Name is Mackenzie Parking
unifi id is 1
is migrated
query spaces by is 1
is subscribed to Real-Time Credit Card Processing
And there exists a location where the 
location name is Downtown
And there exists a merchant account where the
name is TestEMV
customer is Mackenzie Parking
merchant account status is enabled
processor is processor.creditcall
Field1 is 17681535
Field2 is D9231CC32Ac1431686121828E1a7e171
And there exists a pay station where the  
serial number is 500000070001
Name is paystation1
is activated
location is Downtown 
customer is Mackenzie Parking
Credit card Merchant Account is TestEMV
is new merchant account
When iris receives an incoming transaction message with message number 1 where the
serial number is 500000070001
ticket number is 1
lot number is Student Parking
license plate number is ABC123
stall number is 1
type is Regular
purchase date is 2017:03:01:22:10:36:GMT
parking ticket purchased is 60
original amount is 200
charged amount is 2.00
coupon number is left blank
cash paid is 2.00
card paid is 0.00
is not refund slip
expiry time is 2017:03:01:23:10:36:GMT
rate name is early bird
rate id is 1
rate value is 400
tax type is NA
Then pay station with message number 1 will contain EMVMerchantAccountNotification


Scenario: Sending a delayed XML cash transaction would not override the current active permit
Given there exists a Customer Property Type where the
id is 11
name is jurisdiction type preferred
Given there exists a Customer Property Type where the
id is 12
name is jurisdiction type limited
And there exists a customer where the 
customer Name is Mackenzie Parking
unifi id is 1
is migrated
query spaces by is 3
Jurisdiction type preferred is 2
Jurisdiction type limited is 2
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is activated
location is Downtown 
customer is Mackenzie Parking
And there exists a active permit where the
location is Downtown
stall number is 1
permit begin is now
licence plate number is NOW123
When iris receives an incoming transaction message where the
serial number is 500000070001
ticket number is 1
lot number is Student Parking
license plate number is OLD123
stall number is 1
type is regular
purchase date is 2016:05:26:19:10:36:GMT
parking ticket purchased is 60
original amount is 200
charged amount is 0.00
coupon number is left blank
cash paid is 2.00
card paid is 0.00
card data is left blank
is not refund slip
limited rate is true
preferred rate is true
expiry time is 2016:05:26:59:10:36:GMT
coin collected is 5:0,10:0,25:0,100:2
bill collected is 5:0,10:0,20:0,50:0
rate name is early bird
rate id is 1
rate value is 400
tax type is NA
Then there exists a purchase where the 
cash paid amount is 200
Customer Name is Mackenzie Parking
serial number is 500000070001
And there exists a permit where the
location name  is Downtown
stall number is 1
license plate number is OLD123
Then there exists a purchase where the 
cash paid amount is 200
Customer Name is Mackenzie Parking
serial number is 500000070001
And there exists a permit where the
location name  is Downtown
stall number is 1
license plate number is OLD123
And there exists a active permit where the
location is Downtown
stall number is 1
licence plate number is NOW123
Then the message is sent to kafka's topic 'Limited Preferred Rate' with data 
{
  "licensePlate": "OLD123",
  "customerId": 1,
  "locationName": "Downtown",
  "isPreferred": true,
  "isPreferredByLocation": true,
  "isLimited": true,
  "isLimitedByLocation": true,
  "localDate": "2016-05-26",
   "purchaseUTC": "2016-05-26T19:10:36.0Z"
}


Scenario: Sending a XMLRPC cash transaction
Given there exists a customer where the 
customer Name is Mackenzie Parking
unifi id is 1
is migrated
query spaces by is 1
Jurisdiction type preferred is 1
Jurisdiction type limited is 1
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is activated
location is Downtown 
customer is Mackenzie Parking
When iris receives an incoming transaction message where the
serial number is 500000070001
ticket number is 1
lot number is Student Parking
license plate number is ABC123
stall number is 1
type is regular
purchase date is 2017:01:26:18:10:36:GMT
parking ticket purchased is 60
original amount is 200
charged amount is 0.00
coupon number is left blank
cash paid is 2.00
card paid is 0.00
card data is left blank
is not refund slip
expiry time is 2017:01:26:19:10:36:GMT
coin collected is 5:0,10:0,25:0,100:2
bill collected is 5:0,10:0,20:0,50:0
rate name is early bird
rate id is 1
rate value is 400
tax type is NA
Then there exists a purchase where the 
cash paid amount is 200
Customer Name is Mackenzie Parking
serial number is 500000070001
And there exists a permit where the
location name  is Downtown
stall number is 1
license plate number is ABC123
Then the message is not sent to kafka's topic 'Limited Preferred Rate'

Scenario: Sending a XMLRPC cash transaction limited rate only
Given there exists a Customer Property Type where the
id is 11
name is jurisdiction type preferred
Given there exists a Customer Property Type where the
id is 12
name is jurisdiction type limited
And there exists a customer where the 
customer Name is Mackenzie Parking
unifi id is 1
is migrated
query spaces by is 1
Jurisdiction type preferred is 2
Jurisdiction type limited is 1
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is activated
location is Downtown 
customer is Mackenzie Parking
When iris receives an incoming transaction message where the
serial number is 500000070001
ticket number is 1
lot number is Student Parking
license plate number is ABC123
stall number is 1
type is regular
purchase date is 2017:01:26:18:10:36:GMT
parking ticket purchased is 60
original amount is 200
charged amount is 0.00
coupon number is left blank
cash paid is 2.00
card paid is 0.00
card data is left blank
is not refund slip
expiry time is 2017:01:26:19:10:36:GMT
coin collected is 5:0,10:0,25:0,100:2
bill collected is 5:0,10:0,20:0,50:0
rate name is early bird
rate id is 1
rate value is 400
tax type is NA
limited rate is true
preferred rate is false
Then there exists a purchase where the 
cash paid amount is 200
Customer Name is Mackenzie Parking
serial number is 500000070001
And there exists a permit where the
location name  is Downtown
stall number is 1
license plate number is ABC123
Then the message is sent to kafka's topic 'Limited Preferred Rate' with data 
{
  "licensePlate": "ABC123",
  "customerId": 1,
  "locationName": "Downtown",
  "isPreferred": false,
  "isPreferredByLocation": true,
  "isLimited": true,
  "isLimitedByLocation": false,
  "localDate": "2017-01-26",
  "purchaseUTC": "2017-01-26T18:10:36.0Z"
}

Scenario: Sending a XMLRPC cancelled transaction both limited and preferred
Given there exists a Customer Property Type where the
id is 11
name is jurisdiction type preferred
Given there exists a Customer Property Type where the
id is 12
name is jurisdiction type limited
And there exists a customer where the 
customer Name is Mackenzie Parking
unifi id is 1
is migrated
query spaces by is 1
Jurisdiction type preferred is 1
Jurisdiction type limited is 2
And there exists a location where the 
location name is Downtown
And there exists a pay station where the 
serial number is 500000070001
is activated
location is Downtown 
customer is Mackenzie Parking
When iris receives an incoming transaction message where the
serial number is 500000070001
ticket number is 1
lot number is Student Parking
license plate number is ABC123
stall number is 1
type is cancelled
purchase date is 2017:01:26:18:10:36:GMT
parking ticket purchased is 60
original amount is 200
charged amount is 0.00
coupon number is left blank
cash paid is 2.00
card paid is 0.00
card data is left blank
is not refund slip
expiry time is 2017:01:26:19:10:36:GMT
coin collected is 5:0,10:0,25:0,100:2
bill collected is 5:0,10:0,20:0,50:0
rate name is early bird
rate id is 1
rate value is 400
tax type is NA
limited rate is true
preferred rate is true
Then there exists a purchase where the 
cash paid amount is 200
Customer Name is Mackenzie Parking
serial number is 500000070001
And there exists a permit where the
location name  is Downtown
stall number is 1
license plate number is ABC123
Then the message is not sent to kafka's topic 'Limited Preferred Rate'

Scenario: Sending a XMLRPC cash transaction preferred rate only
Given there exists a Customer Property Type where the
id is 11
name is jurisdiction type preferred
Given there exists a Customer Property Type where the
id is 12
name is jurisdiction type limited
And there exists a customer where the 
customer Name is Mackenzie Parking
unifi id is 1
is migrated
query spaces by is 1
Jurisdiction type preferred is 1
Jurisdiction type limited is 1
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is activated
location is Downtown 
customer is Mackenzie Parking
When iris receives an incoming transaction message where the
serial number is 500000070001
ticket number is 1
lot number is Student Parking
license plate number is ABC123
stall number is 1
type is regular
purchase date is 2017:01:26:18:10:36:GMT
parking ticket purchased is 60
original amount is 200
charged amount is 0.00
coupon number is left blank
cash paid is 2.00
card paid is 0.00
card data is left blank
is not refund slip
expiry time is 2017:01:26:19:10:36:GMT
coin collected is 5:0,10:0,25:0,100:2
bill collected is 5:0,10:0,20:0,50:0
rate name is early bird
rate id is 1
rate value is 400
tax type is NA
limited rate is false
preferred rate is true
Then there exists a purchase where the 
cash paid amount is 200
Customer Name is Mackenzie Parking
serial number is 500000070001
And there exists a permit where the
location name  is Downtown
stall number is 1
license plate number is ABC123
Then the message is sent to kafka's topic 'Limited Preferred Rate' with data 
{
  "licensePlate": "ABC123",
  "customerId": 1,
  "locationName": "Downtown",
  "isPreferred": true,
  "isPreferredByLocation": false,
  "isLimited": false,
  "isLimitedByLocation": false,
  "localDate": "2017-01-26",
  "purchaseUTC": "2017-01-26T18:10:36.0Z"
}

Scenario: Sending a XMLRPC cash transaction not preferred or limited rate
Given there exists a customer where the 
customer Name is Mackenzie Parking
unifi id is 1
is migrated
query spaces by is 1
Jurisdiction type preferred is 1
Jurisdiction type limited is 2
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is activated
location is Downtown 
customer is Mackenzie Parking
When iris receives an incoming transaction message where the
serial number is 500000070001
ticket number is 1
lot number is Student Parking
license plate number is ABC123
stall number is 1
type is regular
purchase date is 2017:01:26:18:10:36:GMT
parking ticket purchased is 60
original amount is 200
charged amount is 0.00
coupon number is left blank
cash paid is 2.00
card paid is 0.00
card data is left blank
is not refund slip
expiry time is 2017:01:26:19:10:36:GMT
coin collected is 5:0,10:0,25:0,100:2
bill collected is 5:0,10:0,20:0,50:0
rate name is early bird
rate id is 1
rate value is 400
tax type is NA
limited rate is false
preferred rate is false
Then there exists a purchase where the 
cash paid amount is 200
Customer Name is Mackenzie Parking
serial number is 500000070001
And there exists a permit where the
location name  is Downtown
stall number is 1
license plate number is ABC123
Then the message is not sent to kafka's topic 'Limited Preferred Rate'


Scenario: Sending a Store & Forward transaction for TD Merchant Services that is Denied
Given there exists a customer where the 
customer Name is Mackenzie Parking
max offline retry is 5
unifi id is 1
is migrated
query spaces by is 1
And there exists a location where the 
location name is Downtown
And there exists a merchant account where the  
name is TestTDAccount
customer is Mackenzie Parking
processor is processor.tdmerchant
merchant account status is enabled
Close Time is 16
Field1 is 17681535
Field2 is D9231CC32Ac1431686121828E1a7e171
Field3 is CAD
And there exists a pay station where the  
serial number is 500000070002
is activated
location is Downtown 
customer is Mackenzie Parking
Credit card Merchant Account is TestTDAccount
And the Card Type Service response with message 
{
  "response": "MasterCard",
  "status": {
    "errors": null,
    "responseStatus": "SUCCESS"
  }
}
When iris receives an incoming transaction message where the
serial number is 500000070002
ticket number is 1
lot number is Student Parking
license plate number is ABC123
stall number is 1
type is regular
purchase date is 2017:02:29:20:40:36:GMT
parking ticket purchased is 60
original amount is 401
charged amount is 4.01
coupon number is left blank
cash paid is 0.00
card paid is 4.01
card data is 4124939999999990=191290154321396145
is not refund slip
expiry time is 2017:01:26:21:12:36:GMT
rate name is early bird
rate id is 1
rate value is 400
tax type is NA
Then there exists a purchase where the 
card paid amount is 401
payment type is cc (swipe)
transaction type is regular
Customer Name is Mackenzie Parking
serial number is 500000070002
And there exists a permit where the
location name is Downtown
stall number is 1
license plate number is ABC123
And there exists a card retry transaction where the 
pay station is 500000070002
amount is 401

Scenario: Sending a cash transaction with a Flat Rate Fee
Given there exists a customer where the 
customer Name is Mackenzie Parking
unifi id is 1
is migrated
query spaces by is 1
Jurisdiction type preferred is 1
Jurisdiction type limited is 1
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is activated
location is Downtown 
customer is Mackenzie Parking
When iris receives an incoming transaction message where the
serial number is 500000070001
ticket number is 1
lot number is Student Parking
license plate number is ABC123
stall number is 1
type is regular
purchase date is 2017:01:26:18:10:36:GMT
parking ticket purchased is 60
original amount is 200
charged amount is 0.00
coupon number is left blank
cash paid is 2.00
card paid is 0.00
card data is left blank
tax three rate is 0
tax three value is 1.5
tax three name is Flat Fee Rate
is not refund slip
expiry time is 2017:01:26:19:10:36:GMT
coin collected is 5:0,10:0,25:0,100:2
bill collected is 5:0,10:0,20:0,50:0
rate name is early bird
rate id is 1
rate value is 400
tax type is NA
Then there exists a purchase where the 
cash paid amount is 200
Customer Name is Mackenzie Parking
serial number is 500000070001
And there exists a permit where the
location name  is Downtown
stall number is 1
license plate number is ABC123
And there exists a purchase tax where the 
tax rate is 0
tax amount is 150
And the message is not sent to kafka's topic 'Limited Preferred Rate'


Scenario: Sending a cash transaction with a Flat Rate Fee in tax 1
Given there exists a customer where the 
customer Name is Mackenzie Parking
unifi id is 1
is migrated
query spaces by is 1
Jurisdiction type preferred is 1
Jurisdiction type limited is 1
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is activated
location is Downtown 
customer is Mackenzie Parking
When iris receives an incoming transaction message where the
serial number is 500000070001
ticket number is 1
lot number is Student Parking
license plate number is ABC123
stall number is 1
type is regular
purchase date is 2017:01:26:18:10:36:GMT
parking ticket purchased is 60
original amount is 200
charged amount is 0.00
coupon number is left blank
cash paid is 2.00
card paid is 0.00
card data is left blank
tax one rate is 0
tax one value is 1.5
tax one name is Flat Fee Rate
is not refund slip
expiry time is 2017:01:26:19:10:36:GMT
coin collected is 5:0,10:0,25:0,100:2
bill collected is 5:0,10:0,20:0,50:0
rate name is early bird
rate id is 1
rate value is 400
tax type is NA
Then there exists a purchase where the 
cash paid amount is 200
Customer Name is Mackenzie Parking
serial number is 500000070001
And there exists a permit where the
location name  is Downtown
stall number is 1
license plate number is ABC123
And there exists a purchase tax where the 
tax rate is 0
tax amount is 150
And the message is not sent to kafka's topic 'Limited Preferred Rate'	