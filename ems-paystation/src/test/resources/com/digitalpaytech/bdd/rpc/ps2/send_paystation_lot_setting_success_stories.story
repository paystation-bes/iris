Narrative: This test is to make sure that when Iris receive settings update acknowledgement
, Iris will also publish that to the correct kafka topic

Scenario: Sending a XMLRPC setting update success acknowledgement
Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
query spaces by is 1
And there exists a pay station where the  
serial number is 500000070001
name is 500000070001
is activated
is new paystation setting 
customer is Mackenzie Parking
When iris receives an incoming setting update acknowledge message with message number 1 where the
serial number is 500000070001
group name is airport lot
updated date is 30 minutes ago
Then the message is sent to kafka's topic 'Device Upgrade Ack'
And there exists a pos service state where the
point of sale is 500000070001
is not new paystation setting
setting name is airport lot
