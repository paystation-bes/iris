Narrative:
In order to test XMLRPC EMV Merchant Account Success Handler
As a user
I want to send pay station transactions and assert the desired behavior

Scenario: Sending a XMLRPC emv merchant account success message
Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
query spaces by is 1
is subscribed to Real-Time Credit Card Processing
And there exists a location where the 
location name is Downtown
And there exists a merchant account where the
name is TestEMV
customer is Mackenzie Parking
merchant account status is enabled
processor is processor.t2emv
Field1 is 17681535
Field2 is D9231CC32Ac1431686121828E1a7e171
And there exists a pay station where the  
serial number is 500000070001
name is 500000070001
is activated
location is Downtown 
customer is Mackenzie Parking
Credit card Merchant Account is TestEMV
And there exists a merchant pos where the
pay station is 500000070001
card type is credit card
name is TestEMV
When iris receives an incoming emv merchant account success message with message number 1 where the
serial number is 500000070001
Then pay station with message number 1 will contain Acknowledge
And there exists a pos service state where the
point of sale is 500000070001
is not new merchant account
merchant account uploaded gmt greater than 3 minute ago
