Narrative:
In order to text XMLRPC EMV Telemetry
As a user
I want to send EMV Telemetry data and asser the desired behaviour


Scenario: Send a EMV Card Reader Violated telemetry message to Iris

Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
query spaces by is 1
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is activated
location is Downtown
pay station type is LUKE II 
customer is Mackenzie Parking
When iris receives an incoming EMV Telemetry message where the
type is Reset
Reference Counter is 1
Time Stamp is 2016:03:01:22:10:36:GMT
Serial Number is 500000070001
Telemetry is {"ViolationStatus" : "Violated"}
Then a new queue event is placed in the recent event Queue where the
event type is Violation Status
severity is Critical
is active
pay station is 500000070001

Scenario: Send a EMV Card Reader Tampered telemetry message to Iris

Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
query spaces by is 1
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is activated
location is Downtown
pay station type is LUKE II 
customer is Mackenzie Parking
When iris receives an incoming EMV Telemetry message where the
Type is Reset
reference counter is 1
Time Stamp is 2016:03:01:22:10:36:GMT
Serial Number is 500000070001
Telemetry is {"ViolationStatus" : "Tampered"}
Then a new queue event is placed in the recent event Queue where the
event type is Violation Status
severity is Critical
is active
pay station is 500000070001

Scenario: Send a EMV Card Reader Fault telemetry message to Iris

Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
query spaces by is 1
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is activated
location is Downtown
pay station type is LUKE II 
customer is Mackenzie Parking
When iris receives an incoming EMV Telemetry message where the
Type is Reset
reference counter is 1
Time Stamp is 2016:03:01:22:10:36:GMT
Serial Number is 500000070001
Telemetry is {"ViolationStatus" : "Fault"}
Then a new queue event is placed in the recent event Queue where the
event type is Violation Status
severity is Major
is active
pay station is 500000070001

Scenario: Send a EMV Card Reader Ok telemetry message to Iris

Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
query spaces by is 1
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is activated
location is Downtown
pay station type is LUKE II 
customer is Mackenzie Parking
When iris receives an incoming EMV Telemetry message where the
type is Reset
reference counter is 1
Time Stamp is 2016:03:01:22:10:36:GMT
Serial Number is 500000070001
Telemetry is {"ViolationStatus" : "Ok"}
Then a new queue event is placed in the recent event Queue where the
event type is Violation Status
severity is Clear
is not active
pay station is 500000070001

Scenario: Send misspelled telemetry data
Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
query spaces by is 1
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is activated
location is Downtown
pay station type is LUKE II 
customer is Mackenzie Parking
When iris receives an incoming EMV Telemetry message where the
Type is Reset
Reference Counter is 1
Time Stamp is 2016:03:01:22:10:36:GMT
Serial NUmber is 500000070001
Telemetry is {"violationstatus" : "Ok", "firmwareversion" : "2.0"}
Then a new queue event is placed in the recent event Queue where the
event type is Violation Status
action type is Normal
severity is Clear
is not active
pay station is 500000070001
Then there exists a Paystation Telemetry where the
Name is FirmwareVersion
Value is 2.0
History is false

Scenario: Send emv telemetry request with skipping reference numbers

Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
query spaces by is 1
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is activated
location is Downtown
pay station type is LUKE II 
customer is Mackenzie Parking
When iris receives an incoming EMV Telemetry message where the
type is Reset
reference counter is 1
Time Stamp is 2016:03:01:22:10:36:GMT
Serial Number is 500000070001
Telemetry is {"ViolationStatus" : "Violated"}
Then there exists a Paystation Telemetry where the
Name is ViolationStatus
Value is Violated
History is false
Then a new queue event is placed in the recent event Queue where the
event type is Violation Status
action type is Violated
severity is Critical
is active
pay station is 500000070001
When iris receives an incoming EMV Telemetry message where the
type is Update
reference counter is 2
Time Stamp is 2016:03:01:22:10:36:GMT
Serial Number is 500000070001
Telemetry is {"ViolationStatus" : "Fault"}
Then there exists a Paystation Telemetry where the
Name is ViolationStatus
Value is Fault
History is false
Then a new queue event is placed in the recent event Queue where the
event type is Violation Status
action type is Fault
severity is Major
is active
pay station is 500000070001
When iris receives an incoming EMV Telemetry message where the
type is Update
reference counter is 4
Time Stamp is 2016:03:01:22:10:36:GMT
Serial Number is 500000070001
Telemetry is {"ViolationStatus" : "Tampered"}
Then there exists a Paystation Telemetry where the
Name is ViolationStatus
Value is Tampered
History is true
Then a new queue event is not placed in the recent event Queue where the
event type is Violation Status
action type is Tampered
severity is Critical
is active
pay station is 500000070001
When iris receives an incoming EMV Telemetry message where the
type is Reset
reference counter is 5
Time Stamp is 2016:03:01:22:10:36:GMT
Serial Number is 500000070001
Telemetry is {"ViolationStatus" : "Fault"}
Then there exists a Paystation Telemetry where the
Name is ViolationStatus
Value is Fault
History is false
Then a new queue event is placed in the recent event Queue where the
event type is Violation Status
action type is Fault
severity is Major
is active
pay station is 500000070001
