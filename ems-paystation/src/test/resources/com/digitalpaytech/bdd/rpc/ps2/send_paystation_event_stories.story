Narrative:
In order to test XMLRPC event processing
As a user
I want to send paystation events and assert the desired behavior

Scenario: Send a XMLRPC event for sending an access point.

Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is activated
location is Downtown 
customer is Mackenzie Parking
When iris receives an incoming event message where the
pay station is 500000070001
Timestamp is now
Type is Modem
Action is APN
Information is TestURL.COM
Then there exists a access point where the 
Name is TestURL.COM
Id is not 0

Scenario:  Send Paystation Event to update Modem Type
Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is activated
location is Downtown 
customer is Mackenzie Parking
When iris receives an incoming event message where the
pay station is 500000070001
Timestamp is now
Type is Modem
Action is Type
Information is TestModem
Then there exists a modem type where the 
Name is TestModem
Id is not 0
