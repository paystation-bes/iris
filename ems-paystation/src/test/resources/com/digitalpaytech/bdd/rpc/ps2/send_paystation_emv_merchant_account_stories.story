Narrative:
In order to test XMLRPC EMV Merchant Account processing
As a user
I want to send paystation transactions and assert the desired behavior

Scenario: Sending a XMLRPC emv merchant account (CreditCall)
Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
query spaces by is 1
is subscribed to Real-Time Credit Card Processing
And there exists a location where the
location name is Downtown
And there exists a merchant account where the
name is TestCreditCall
customer is Mackenzie Parking
merchant account status is enabled
processor is processor.creditcall
Field1 is Elavon
Field2 is merchantID
Field3 is 11223344
Field4 is TRANSACTION
Field5 is CAD
terminal token is abcdefg
And there exists a pay station where the
serial number is 500000070001
name is 500000070001
is activated
location is Downtown
customer is Mackenzie Parking
Credit card Merchant Account is TestCreditCall
And there exists a merchant pos where the
pay station is 500000070001
card type is credit card
name is TestCreditCall
When iris receives an incoming emv merchant account message with message number 1 where the
serial number is 500000070001
Then there exists a message number 1 with MerchantInfo tag where the merchantProcessor is Elavon
And there exists a message number 1 with MerchantInfo tag where the transactionKey is TRANSACTION
And there exists a message number 1 with MerchantInfo tag where the terminalId is 11223344
And there exists a message number 1 with MerchantInfo tag where the merchantId is merchantID
And there exists a message number 1 with MerchantInfo tag where the currency is CAD
