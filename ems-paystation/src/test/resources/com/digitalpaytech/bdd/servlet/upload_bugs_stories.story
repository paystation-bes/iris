Narrative:
As an operator I want to upload some transaction batch files to Iris using BOSS - EMS-10747

Scenario: Successfully parsed and processed an uploaded transaction batch file - EMS-10747
Given there exists a Customer where the 
Customer name is Mackenzie Parking
And the File named 555555555555_1501119151750.xml.hpz does not exist in the database
When Iris verifies 555555555555_1501119151750.xml.hpz
Then the file is saved

Scenario: Upload a duplicated batch file will return an acknowledge response - EMS-10747
Given there exists a Customer where the 
Customer name is Mackenzie Parking
And there exists a File where the 
File owner is Mackenzie Parking
File name is 555555555555_1501119151750.xml.hpz
And the File named 555555555555_1501119151750.xml.hpz exists in the database
When Iris verifies 555555555555_1501119151750.xml.hpz
Then the file is acknowledged
