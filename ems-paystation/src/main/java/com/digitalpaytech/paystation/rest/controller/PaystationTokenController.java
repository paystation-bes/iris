package com.digitalpaytech.paystation.rest.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.domain.EventDefinition;
import com.digitalpaytech.domain.PosMacAddress;
import com.digitalpaytech.dto.paystation.EventData;
import com.digitalpaytech.dto.rest.paystation.PaystationQueryParamList;
import com.digitalpaytech.dto.rest.paystation.PaystationToken;
import com.digitalpaytech.dto.rest.paystation.PaystationTokenResponse;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.PaystationCommunicationException;
import com.digitalpaytech.paystation.rest.support.PaystationMessageInfo;
import com.digitalpaytech.rpc.support.XMLHandlerFactory;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.servlet.DPTHttpServletRequestWrapper;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.util.RestCoreConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Controller
@RequestMapping(value = "/GetToken")
public class PaystationTokenController extends PaystationBaseController {
    
    private static final Logger LOGGER = Logger.getLogger(PaystationTokenController.class);
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private MailerService mailerService;
    
    @Autowired
    private MessageHelper messageHelper;
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final void setMessageHelper(final MessageHelper messageHelper) {
        this.messageHelper = messageHelper;
    }
    
    public final void setMailerService(final MailerService mailerService) {
        this.mailerService = mailerService;
    }
    
    @RequestMapping(method = RequestMethod.POST, consumes = "application/xml", produces = "application/xml")
    @ResponseBody
    public final PaystationTokenResponse postToken(final HttpServletResponse response, final HttpServletRequest request,
        @RequestBody final PaystationToken paystationRequest, @RequestParam("Signature") final String signature,
        @RequestParam("SignatureVersion") final String signatureVersion, @RequestParam("Timestamp") final String timestamp,
        @RequestParam("SerialNumber") final String serialNumber, @RequestParam("Version") final String version) throws ApplicationException {
        
        final String methodName = this.getClass().getName() + ".postToken()";
        final PaystationMessageInfo messageInfo = new PaystationMessageInfo();
        
        final StringBuilder logMessage = new StringBuilder();
        logMessage.append("PointOfSale:");
        logMessage.append(paystationRequest.getPaystationCommAddress());
        logMessage.append(" with mac address:");
        logMessage.append(paystationRequest.getMacAddress());
        logMessage.append(" requested a new secretKey.");
        LOGGER.info(logMessage);
        
        try {
            final PaystationQueryParamList param = new PaystationQueryParamList();
            param.setSerialNumber(serialNumber);
            param.setSignature(signature);
            param.setSignatureVersion(signatureVersion);
            param.setTimestamp(timestamp);
            param.setVersion(version);
            
            super.validateRequest(param, paystationRequest, messageInfo);
            
            final String secretKey = new String(Base64.encodeBase64(XMLHandlerFactory.PS2_PASSWORD_VAL.getBytes()));
            
            /* verify the payload by calling the method in encryptionService. */
            if (!super.encryptionService.verifyHMACSignature(request.getServerName(), RestCoreConstants.HTTP_METHOD_POST, param.getSignature(),
                                                             param, ((DPTHttpServletRequestWrapper) request).getBody(), secretKey)) {
                final StringBuilder errorMessage = new StringBuilder();
                errorMessage.append("Signature failed for PointOfSale:");
                errorMessage.append(paystationRequest.getPaystationCommAddress());
                errorMessage.append(" with mac address:");
                errorMessage.append(paystationRequest.getMacAddress());
                LOGGER.info(errorMessage);
                
                throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_SIGNATURE_DOES_NOT_MATCH);
            }
            
            final int duplicateSerialNumberCount = this.emsPropertiesService
                    .getPropertyValueAsInt(EmsPropertiesService.DUPLICATE_SERIALNUMBER_COUNT,
                                           EmsPropertiesService.DEFAULT_DUPLICATE_SERIALNUMBER_COUNT);
            
            final Date now = DateUtil.getCurrentGmtDate();
            boolean isUpdate = false;
            boolean isAlert = false;
            PosMacAddress posMacAddress = super.posMacAddressService.findPosMacAddressByPointOfSaleId(messageInfo.getPointOfSale().getId());
            if (posMacAddress != null) {
                if (!posMacAddress.getMacAddress().equals(paystationRequest.getMacAddress())) {
                    final PosMacAddress secondPosMacAddress = super.posMacAddressService.findPosMacAddressByMacAddress(paystationRequest
                            .getMacAddress());
                    if (secondPosMacAddress != null) {
                        final StringBuilder errorMessage = new StringBuilder();
                        errorMessage.append("POSMacAddress for PointOfSale:");
                        errorMessage.append(secondPosMacAddress.getPointOfSale().getId());
                        errorMessage.append("-");
                        errorMessage.append(secondPosMacAddress.getPointOfSale().getSerialNumber());
                        errorMessage.append(" had mac address: ");
                        errorMessage.append(secondPosMacAddress.getMacAddress());
                        errorMessage.append(". Mac address and Secret key set to empty string.");
                        LOGGER.error(errorMessage);
                        
                        secondPosMacAddress.setMacAddress(WebCoreConstants.EMPTY_STRING);
                        secondPosMacAddress.setSecretKey(WebCoreConstants.EMPTY_STRING);
                        secondPosMacAddress.setLastModifiedGmt(now);
                        super.posMacAddressService.saveOrUpdate(secondPosMacAddress, false);
                        
                    }
                    final StringBuilder errorMessage = new StringBuilder();
                    errorMessage.append("POSMacAddress for PointOfSale:");
                    errorMessage.append(messageInfo.getPointOfSale().getId());
                    errorMessage.append("-");
                    errorMessage.append(messageInfo.getPointOfSale().getSerialNumber());
                    errorMessage.append(" had mac address:");
                    errorMessage.append(posMacAddress.getMacAddress());
                    errorMessage.append(" changed to ");
                    errorMessage.append(paystationRequest.getMacAddress());
                    LOGGER.error(errorMessage);
                    
                    posMacAddress.setMacAddress(paystationRequest.getMacAddress());
                    if (now.getTime() - posMacAddress.getLastModifiedGmt().getTime() < DateUtil.ONE_DAY) {
                        isUpdate = true;
                        posMacAddress.setAlertCount((short) (posMacAddress.getAlertCount() + 1));
                        if (posMacAddress.getAlertCount() == duplicateSerialNumberCount) {
                            isAlert = true;
                        }
                    } else {
                        posMacAddress.setAlertCount((short) 0);
                    }
                    
                }
            } else {
                posMacAddress = super.posMacAddressService.findPosMacAddressByMacAddress(paystationRequest.getMacAddress());
                if (posMacAddress != null) {
                    final StringBuilder errorMessage = new StringBuilder();
                    errorMessage.append("POSMacAddress for mac address:");
                    errorMessage.append(posMacAddress.getMacAddress());
                    errorMessage.append(" had PointOfSale:");
                    errorMessage.append(posMacAddress.getPointOfSale().getId());
                    errorMessage.append("-");
                    errorMessage.append(posMacAddress.getPointOfSale().getSerialNumber());
                    errorMessage.append(" changed to PointOfSale:");
                    errorMessage.append(messageInfo.getPointOfSale().getId());
                    errorMessage.append("-");
                    errorMessage.append(messageInfo.getPointOfSale().getName());
                    LOGGER.error(errorMessage);
                    
                    posMacAddress.setMacAddress(WebCoreConstants.EMPTY_STRING);
                    posMacAddress.setSecretKey(WebCoreConstants.EMPTY_STRING);
                    posMacAddress.setLastModifiedGmt(now);
                    super.posMacAddressService.saveOrUpdate(posMacAddress, false);
                }
                posMacAddress = new PosMacAddress();
                posMacAddress.setPointOfSale(messageInfo.getPointOfSale());
                posMacAddress.setMacAddress(paystationRequest.getMacAddress());
                posMacAddress.setAlertCount((short) 0);
                
                isUpdate = true;
            }
            
            posMacAddress.setSecretKey(super.encryptionService.createHMACSecretKey());
            posMacAddress.setLastModifiedGmt(now);
            super.posMacAddressService.saveOrUpdate(posMacAddress, isUpdate);
            
            if (isUpdate) {
                processUpdate(messageInfo, isAlert, now, serialNumber);
                
            }
            /* Initiate the response. */
            final PaystationTokenResponse paystationResponse = new PaystationTokenResponse();
            paystationResponse.setMessageNumber(paystationRequest.getMessageNumber());
            paystationResponse.setSecretKey(posMacAddress.getSecretKey());
            paystationResponse.setResponseMessage(PaystationConstants.RESPONSE_MESSAGE_SUCCESS);
            
            super.addNotifications(paystationResponse, messageInfo);
            
            return paystationResponse;
        } catch (PaystationCommunicationException pce) {
            LOGGER.debug("#### ERROR IN " + methodName + " ####");
            PaystationTokenResponse paystationResponse = new PaystationTokenResponse();
            paystationResponse = (PaystationTokenResponse) createErrorResponse(request, paystationResponse, paystationRequest.getMessageNumber(), pce);
            LOGGER.debug("#### REPONSE RETURNED " + methodName + " ####");
            return paystationResponse;
            
        } catch (ApplicationException ae) {
            throw ae;
        }
    }
    
    private void processUpdate(final PaystationMessageInfo messageInfo, final boolean isAlert, final Date now, final String serialNumber)
        throws InvalidDataException {
        final EventDefinition uidChangeDefinition = super.alertsService.findEventDefinitionById(WebCoreConstants.EVENT_DEFINITION_UID_CHANGED);
        final EventData eventData = new EventData();
        eventData.setPointOfSaleId(messageInfo.getPointOfSale().getId());
        eventData.setType(WebCoreConstants.EVENT_TYPE_PAYSTATION);
        eventData.setAction(WebCoreConstants.EVENT_ACTION_UID_CHANGED_STRING);
        eventData.setSeverity(uidChangeDefinition.getEventSeverityType().getName());
        eventData.setTimeStamp(now);
        super.eventAlertService.processEvent(messageInfo.getPointOfSale(), eventData);
        
        if (isAlert) {
            final EventDefinition uidAlertDefinition = super.alertsService.findEventDefinitionById(WebCoreConstants.EVENT_DEFINITION_UID_ALERT);
            final EventData alertData = new EventData();
            alertData.setPointOfSaleId(messageInfo.getPointOfSale().getId());
            alertData.setType(WebCoreConstants.EVENT_TYPE_PAYSTATION);
            alertData.setAction(WebCoreConstants.EVENT_ACTION_UID_ALERT_STRING);
            alertData.setSeverity(uidAlertDefinition.getEventSeverityType().getName());
            alertData.setTimeStamp(now);
            super.eventAlertService.processEvent(messageInfo.getPointOfSale(), alertData);
            
            final String emailAddress = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.DUPLICATE_SERIALNUMBER_EMAIL,
                                                                                   EmsPropertiesService.DEFAULT_DUPLICATE_SERIALNUMBER_EMAIL);
            
            final String subject = this.messageHelper.getMessage("alert.email.doubleSerialNumber.admin.subject.template", serialNumber);
            final String content = this.messageHelper.getMessage("alert.email.doubleSerialNumber.admin.content.template", serialNumber);
            
            this.mailerService.sendMessage(emailAddress, subject, content, null);
            
        }
    }
}
