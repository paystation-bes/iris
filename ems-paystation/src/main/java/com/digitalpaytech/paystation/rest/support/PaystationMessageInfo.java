package com.digitalpaytech.paystation.rest.support;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.URLReroute;

public class PaystationMessageInfo {
    
    private PointOfSale pointOfSale;
    private int status;
    
    private URLReroute urlReroute;
    private boolean addLotSettingNotification;
    private boolean addEncryptionNotification;
    private boolean addTimestampNotification;
    private boolean addRootCertificateNotification;
    
    public PaystationMessageInfo() {
    }
    
    public final PointOfSale getPointOfSale() {
        return this.pointOfSale;
    }
    
    public final void setPointOfSale(final PointOfSale pointOfSale) {
        this.pointOfSale = pointOfSale;
    }
    
    public final URLReroute getUrlReroute() {
        return this.urlReroute;
    }
    
    public final void setUrlReroute(final URLReroute urlReroute) {
        this.urlReroute = urlReroute;
    }
    
    public final int getStatus() {
        return this.status;
    }
    
    public final void setStatus(final int status) {
        this.status = status;
    }
    
    public final boolean isAddLotSettingNotification() {
        return this.addLotSettingNotification;
    }
    
    public final void setAddLotSettingNotification(final boolean addLotSettingNotification) {
        this.addLotSettingNotification = addLotSettingNotification;
    }
    
    public final boolean isAddEncryptionNotification() {
        return this.addEncryptionNotification;
    }
    
    public final void setAddEncryptionNotification(final boolean addEncryptionNotification) {
        this.addEncryptionNotification = addEncryptionNotification;
    }
    
    public final boolean isAddTimestampNotification() {
        return this.addTimestampNotification;
    }
    
    public final void setAddTimestampNotification(final boolean addTimestampNotification) {
        this.addTimestampNotification = addTimestampNotification;
    }
    
    public final boolean isAddRootCertificateNotification() {
        return this.addRootCertificateNotification;
    }
    
    public final void setAddRootCertificateNotification(final boolean addRootCertificateNotification) {
        this.addRootCertificateNotification = addRootCertificateNotification;
    }
}
