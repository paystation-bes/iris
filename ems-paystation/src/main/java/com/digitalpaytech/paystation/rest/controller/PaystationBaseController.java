package com.digitalpaytech.paystation.rest.controller;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.digitalpaytech.cardprocessing.CardProcessingManager;
import com.digitalpaytech.cardprocessing.CardProcessingMaster;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerURLReroute;
import com.digitalpaytech.domain.EventDefinition;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.ServiceAgreement;
import com.digitalpaytech.domain.URLReroute;
import com.digitalpaytech.dto.paystation.EventData;
import com.digitalpaytech.dto.rest.paystation.EMSDomainNotification;
import com.digitalpaytech.dto.rest.paystation.EncryptKeyNotification;
import com.digitalpaytech.dto.rest.paystation.LotSettingNotification;
import com.digitalpaytech.dto.rest.paystation.PaystationQueryParamList;
import com.digitalpaytech.dto.rest.paystation.PaystationRequest;
import com.digitalpaytech.dto.rest.paystation.PaystationResponse;
import com.digitalpaytech.dto.rest.paystation.PaystationToken;
import com.digitalpaytech.dto.rest.paystation.RootCertificateNotification;
import com.digitalpaytech.dto.rest.paystation.TimestampNotification;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.PaystationCommunicationException;
import com.digitalpaytech.helper.TransactionHelper;
import com.digitalpaytech.paystation.rest.support.PaystationMessageInfo;
import com.digitalpaytech.service.AlertsService;
import com.digitalpaytech.service.CardRetryTransactionService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.EncryptionService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.PosMacAddressService;
import com.digitalpaytech.service.ProcessorTransactionService;
import com.digitalpaytech.service.PurchaseService;
import com.digitalpaytech.service.ServiceAgreementService;
import com.digitalpaytech.service.TransactionService;
import com.digitalpaytech.service.paystation.EventAlertService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.KPIConstants;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Controller
public class PaystationBaseController {
    
    private static final Logger LOGGER = Logger.getLogger(PaystationBaseController.class);
    private static final int MINUTE_DIFFERENCE = 15;
    
    @Autowired
    protected PosMacAddressService posMacAddressService;
    
    @Autowired
    protected EncryptionService encryptionService;
    
    @Autowired
    protected PurchaseService purchaseService;
    
    @Autowired
    protected ProcessorTransactionService processorTransactionService;
    
    @Autowired
    protected TransactionService transactionService;
    
    @Autowired
    protected TransactionHelper transactionHelper;
    
    @Autowired
    protected CardRetryTransactionService cardRetryTransactionService;
    
    @Autowired
    protected CardProcessingMaster cardProcessingMaster;
    
    @Autowired
    protected CardProcessingManager cardProcessingManager;
    
    @Autowired
    protected AlertsService alertsService;
    
    @Autowired
    protected EventAlertService eventAlertService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private ServiceAgreementService serviceAgreementService;
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final void setPosMacAddressService(final PosMacAddressService posMacAddressService) {
        this.posMacAddressService = posMacAddressService;
    }
    
    public final void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }
    
    public final void setEncryptionService(final EncryptionService encryptionService) {
        this.encryptionService = encryptionService;
    }
    
    public final void setServiceAgreementService(final ServiceAgreementService serviceAgreementService) {
        this.serviceAgreementService = serviceAgreementService;
    }
    
    public final void setPurchaseService(final PurchaseService purchaseService) {
        this.purchaseService = purchaseService;
    }
    
    public final void setProcessorTransactionService(final ProcessorTransactionService processorTransactionService) {
        this.processorTransactionService = processorTransactionService;
    }
    
    public final void setTransactionService(final TransactionService transactionService) {
        this.transactionService = transactionService;
    }
    
    public final void setTransactionHelper(final TransactionHelper transactionHelper) {
        this.transactionHelper = transactionHelper;
    }
    
    public final void setCardRetryTransactionService(final CardRetryTransactionService cardRetryTransactionService) {
        this.cardRetryTransactionService = cardRetryTransactionService;
    }
    
    public final void setCardProcessingMaster(final CardProcessingMaster cardProcessingMaster) {
        this.cardProcessingMaster = cardProcessingMaster;
    }
    
    public final void setCardProcessingManager(final CardProcessingManager cardProcessingManager) {
        this.cardProcessingManager = cardProcessingManager;
    }
    
    public final void setAlertsService(final AlertsService alertsService) {
        this.alertsService = alertsService;
    }
    
    public final void setEventAlertService(final EventAlertService eventAlertService) {
        this.eventAlertService = eventAlertService;
    }
    
    protected final void validateRequest(final PaystationQueryParamList param, final PaystationRequest request,
        final PaystationMessageInfo messageInfo) throws PaystationCommunicationException {
        if (!validateSerialNumber(param.getSerialNumber(), request, messageInfo)) {
            throw new PaystationCommunicationException(messageInfo.getStatus());
        } else if (!validateSignatureVersion(param.getSignatureVersion(), request, messageInfo)) {
            throw new PaystationCommunicationException(messageInfo.getStatus());
        } else if (!validateParameters(request, messageInfo)) {
            throw new PaystationCommunicationException(messageInfo.getStatus());
        } else if (!validatePOS(request, messageInfo)) {
            final URLReroute urlReroute = messageInfo.getUrlReroute();
            if (urlReroute == null) {
                throw new PaystationCommunicationException(messageInfo.getStatus());
            } else {
                throw new PaystationCommunicationException(messageInfo.getStatus(), new EMSDomainNotification(urlReroute.getNewManagementSysURL(),
                        urlReroute.getNewManagementSysIP(), urlReroute.getDigitalConnectIP()));
            }
        } else if (!validateTimestamp(param.getTimestamp(), messageInfo)) {
            try {
                processTimestampUpdate(messageInfo);
            } catch (InvalidDataException ide) {
                LOGGER.warn("validate Request Timestamp Update Failed");
            }
            throw new PaystationCommunicationException(messageInfo.getStatus(), new TimestampNotification(DateUtil.createColonDelimitedDateString()));
        }
    }
    
    private boolean validateTimestamp(final String timestamp, final PaystationMessageInfo messageInfo) {
        try {
            final Date timestampDate = DateUtil.convertFromRESTDateString(timestamp);
            final Calendar before15mins = Calendar.getInstance();
            before15mins.add(Calendar.MINUTE, -MINUTE_DIFFERENCE);
            final Calendar after15mins = Calendar.getInstance();
            after15mins.add(Calendar.MINUTE, MINUTE_DIFFERENCE);
            if (timestampDate.before(before15mins.getTime()) || timestampDate.after(after15mins.getTime())) {
                messageInfo.setStatus(PaystationConstants.PAYSTATION_STATUS_TYPE_TIMESTAMP_FAILURE);
                messageInfo.setAddTimestampNotification(true);
                return false;
            }
        } catch (InvalidDataException ide) {
            messageInfo.setStatus(PaystationConstants.PAYSTATION_STATUS_TYPE_INVALID_PARAMETER);
            return false;
        }
        return true;
    }
    
    private boolean validateParameters(final PaystationRequest request, final PaystationMessageInfo messageInfo) {
        if (request instanceof PaystationToken) {
            final String macAddress = ((PaystationToken) request).getMacAddress();
            if (!macAddress.matches(PaystationConstants.REGEX_MACADDRESS)) {
                messageInfo.setStatus(PaystationConstants.PAYSTATION_STATUS_TYPE_INVALID_PARAMETER);
                return false;
            }
        }
        
        return true;
    }
    
    private boolean validateSerialNumber(final String serialNumber, final PaystationRequest request, final PaystationMessageInfo messageInfo) {
        if (serialNumber == null) {
            messageInfo.setStatus(PaystationConstants.PAYSTATION_STATUS_TYPE_SERIALNUMBER_EMPTY);
            LOGGER.debug("++ serialNumber is empty +++");
            return false;
        } else if (request == null) {
            messageInfo.setStatus(PaystationConstants.PAYSTATION_STATUS_TYPE_REQUEST_EMPTY);
            LOGGER.debug("++ requestBody is empty +++");
            return false;
        } else if (request.getPaystationCommAddress() == null) {
            messageInfo.setStatus(PaystationConstants.PAYSTATION_STATUS_TYPE_COMMADDRESS_EMPTY);
            LOGGER.debug("++ request.PaystationCommAddress empty +++");
            return false;
        } else if (!serialNumber.equals(request.getPaystationCommAddress())) {
            messageInfo.setStatus(PaystationConstants.PAYSTATION_STATUS_TYPE_DOES_NOT_MATCH);
            LOGGER.debug("++ serialNumber:" + serialNumber + "and request.PaystationCommAddress:" + request.getPaystationCommAddress()
                         + " don't match +++");
            return false;
        }
        return true;
    }
    
    private boolean validatePOS(final PaystationRequest request, final PaystationMessageInfo messageInfo) {
        PointOfSale pos = null;
        pos = processPOSRetrieval(request, messageInfo);
        if (pos == null) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("POS: ").append(request.getPaystationCommAddress()).append(" does not exist");
            LOGGER.error(bdr.toString());
            
            return false;
            
        } else if (isDeactivated(pos)) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("POS: ").append(request.getPaystationCommAddress()).append(" is deactivated.");
            LOGGER.warn(bdr.toString());
            
            final CustomerURLReroute customerUrlReroute = this.customerService.findURLRerouteByCustomerId(pos.getCustomer().getId());
            if (customerUrlReroute != null) {
                messageInfo.setUrlReroute(customerUrlReroute.getUrlReroute());
            }
            messageInfo.setStatus(PaystationConstants.PAYSTATION_STATUS_TYPE_IS_DEACTIVATED);
            return false;
        } else if (!isServiceAgreementSigned(pos.getCustomer().getId(), true)) {
            LOGGER.error("+++ Service Agreement is not accepted +++");
            messageInfo.setStatus(PaystationConstants.PAYSTATION_STATUS_TYPE_IS_INVALID);
            return false;
        } else if (pos.getCustomer().getCustomerStatusType().getId() == WebCoreConstants.CUSTOMER_STATUS_TYPE_DISABLED
                   || pos.getCustomer().getCustomerStatusType().getId() == WebCoreConstants.CUSTOMER_STATUS_TYPE_DELETED) {
            LOGGER.error("+++ Customer is disabled +++");
            messageInfo.setStatus(PaystationConstants.PAYSTATION_STATUS_TYPE_IS_INVALID);
            return false;
        }
        
        final PosServiceState posServiceState = this.pointOfSaleService.findPosServiceStateByPointOfSaleId(pos.getId());
        
        messageInfo.setPointOfSale(pos);
        messageInfo.setStatus(PaystationConstants.PAYSTATION_STATUS_TYPE_OK);
        if (posServiceState.isIsNewPaystationSetting()) {
            messageInfo.setAddLotSettingNotification(true);
        }
        if (posServiceState.isIsNewPublicKey()) {
            messageInfo.setAddEncryptionNotification(true);
        }
        
        if (posServiceState.isIsNewRootCertificate()) {
            messageInfo.setAddRootCertificateNotification(true);
        }
        
        return true;
    }
    
    private boolean validateSignatureVersion(final String signatureVersion, final PaystationRequest request, final PaystationMessageInfo messageInfo) {
        
        if (signatureVersion == null) {
            messageInfo.setStatus(PaystationConstants.PAYSTATION_STATUS_TYPE_INVALID_SIGNATURE_VERSION);
            LOGGER.debug("++ signatureVersion is empty +++");
            return false;
        } else if (!signatureVersion.equals(PaystationConstants.ACCEPTABLE_SIGNATURE_VERSION)) {
            LOGGER.debug("++ pass in signatureVersion: " + signatureVersion + ", supported signatureVersions: "
                         + PaystationConstants.PAYSTATION_STATUS_TYPE_INVALID_SIGNATURE_VERSION + " +++");
            messageInfo.setStatus(PaystationConstants.PAYSTATION_STATUS_TYPE_INVALID_SIGNATURE_VERSION);
            return false;
        }
        
        return true;
    }
    
    private PointOfSale processPOSRetrieval(final PaystationRequest request, final PaystationMessageInfo messageInfo) {
        final PointOfSale pos = this.pointOfSaleService.findPointOfSaleBySerialNumber(request.getPaystationCommAddress());
        if (pos == null) {
            messageInfo.setStatus(PaystationConstants.PAYSTATION_STATUS_TYPE_DOES_NOT_EXIST);
            return null;
        }
        
        try {
            this.pointOfSaleService.updatePaystationActivityTime(pos, false);
        } catch (StaleObjectStateException sose) {
            // There are no more chances that heartbeat update will failed because of StaleObjectException.
            // However, it is a good idea to just catch the exception, because even the heartbeat update failed, other things still have to be saved.
            final StringBuilder bdr = new StringBuilder();
            bdr.append("BaseHandler.processPOSRetrieval, cannot update PosHeartbeat.");
            LOGGER.error(bdr.toString());
        }
        return pos;
    }
    
    private boolean isDeactivated(final PointOfSale pos) {
        if (!pos.getPosStatus().isIsActivated()) {
            return true;
        }
        return false;
    }
    
    protected final String getErrorMessage(final int status) {
        String errorMessage;
        switch (status) {
            case PaystationConstants.PAYSTATION_STATUS_TYPE_SERIALNUMBER_EMPTY:
            case PaystationConstants.PAYSTATION_STATUS_TYPE_REQUEST_EMPTY:
            case PaystationConstants.PAYSTATION_STATUS_TYPE_COMMADDRESS_EMPTY:
            case PaystationConstants.PAYSTATION_STATUS_TYPE_DOES_NOT_MATCH:
            case PaystationConstants.PAYSTATION_STATUS_TYPE_DOES_NOT_EXIST:
            case PaystationConstants.PAYSTATION_STATUS_TYPE_INVALID_SIGNATURE_VERSION:
            case PaystationConstants.PAYSTATION_STATUS_TYPE_INVALID_PARAMETER:
                errorMessage = PaystationConstants.RESPONSE_MESSAGE_PARAMETER_ERROR;
                break;
            case PaystationConstants.PAYSTATION_STATUS_TYPE_IS_DEACTIVATED:
            case PaystationConstants.PAYSTATION_STATUS_TYPE_IS_INVALID:
            case PaystationConstants.PAYSTATION_STATUS_TYPE_TIMESTAMP_FAILURE:
                errorMessage = PaystationConstants.RESPONSE_MESSAGE_EMS_UNAVAILABLE;
                break;
            case PaystationConstants.PAYSTATION_STATUS_TYPE_SIGNATURE_DOES_NOT_MATCH:
                errorMessage = PaystationConstants.RESPONSE_MESSAGE_SIGNATURE_ERROR;
                break;
            case PaystationConstants.PAYSTATION_STATUS_TYPE_CREDIT_CARD_BAD_CARD:
                errorMessage = PaystationConstants.RESPONSE_MESSAGE_BADCARD_ERROR;
                break;
            case PaystationConstants.PAYSTATION_STATUS_TYPE_CREDIT_CARD_DENIED:
                errorMessage = PaystationConstants.RESPONSE_MESSAGE_DENIED_ERROR;
                break;
            case PaystationConstants.PAYSTATION_STATUS_TYPE_CREDIT_CARD_INVALID_CARD:
                errorMessage = PaystationConstants.RESPONSE_MESSAGE_INVALIDCARD_ERROR;
                break;
            case PaystationConstants.PAYSTATION_STATUS_TYPE_INVALID_MERCHANTACCOUNT:
                errorMessage = PaystationConstants.RESPONSE_MESSAGE_INVALID_MERCHANTACCOUNT_ERROR;
                break;
            case PaystationConstants.PAYSTATION_STATUS_TYPE_ENCRYPTION_ERROR:
                errorMessage = PaystationConstants.RESPONSE_MESSAGE_ENCRYPTION_ERROR;
                break;
            case PaystationConstants.PAYSTATION_STATUS_TYPE_UNSUBSCRIBED_SERVICE_ERROR:
                errorMessage = PaystationConstants.RESPONSE_MESSAGE_UNSUBSCRIBEDSERVICE_ERROR;
                break;
            case PaystationConstants.PAYSTATION_STATUS_TYPE_CANNOT_CANCEL_ERROR:
                errorMessage = PaystationConstants.RESPONSE_MESSAGE_CANNOT_CANCEL_ERROR;
                break;
            case PaystationConstants.PAYSTATION_STATUS_TYPE_UNKNOWN_ERROR:
                errorMessage = PaystationConstants.RESPONSE_MESSAGE_EMS_UNAVAILABLE;
                break;
            case PaystationConstants.PAYSTATION_STATUS_TYPE_PROCESSOR_PAUSED:
                errorMessage = PaystationConstants.RESPONSE_MESSAGE_EMS_UNAVAILABLE;
                break;
            default:
                errorMessage = PaystationConstants.RESPONSE_MESSAGE_EMS_UNAVAILABLE;
                break;
        }
        
        return errorMessage;
    }
    
    private int getHTTPErrorCode(final int status) {
        int httpStatus;
        switch (status) {
            case PaystationConstants.PAYSTATION_STATUS_TYPE_SERIALNUMBER_EMPTY:
            case PaystationConstants.PAYSTATION_STATUS_TYPE_REQUEST_EMPTY:
            case PaystationConstants.PAYSTATION_STATUS_TYPE_COMMADDRESS_EMPTY:
            case PaystationConstants.PAYSTATION_STATUS_TYPE_DOES_NOT_MATCH:
            case PaystationConstants.PAYSTATION_STATUS_TYPE_DOES_NOT_EXIST:
            case PaystationConstants.PAYSTATION_STATUS_TYPE_INVALID_SIGNATURE_VERSION:
            case PaystationConstants.PAYSTATION_STATUS_TYPE_INVALID_PARAMETER:
                httpStatus = PaystationConstants.FAILURE_INVALID_PARAMETER;
                break;
            case PaystationConstants.PAYSTATION_STATUS_TYPE_IS_DEACTIVATED:
            case PaystationConstants.PAYSTATION_STATUS_TYPE_IS_INVALID:
                httpStatus = PaystationConstants.FAILURE_INTERNAL_ERROR;
                break;
            case PaystationConstants.PAYSTATION_STATUS_TYPE_SIGNATURE_DOES_NOT_MATCH:
                httpStatus = PaystationConstants.FAILURE_SIGNATURE_ERROR;
                break;
            default:
                httpStatus = PaystationConstants.FAILURE_INTERNAL_ERROR;
                break;
        }
        
        return httpStatus;
    }
    
    @ExceptionHandler({ Exception.class, RuntimeException.class })
    public final void handleExceptions(final Exception e, final HttpServletResponse response) {
        int status;
        if (e instanceof PaystationCommunicationException) {
            status = getHTTPErrorCode(((PaystationCommunicationException) e).getStatus());
            response.setStatus(status);
            
            try {
                response.getWriter().print(getErrorMessage(((PaystationCommunicationException) e).getStatus()));
            } catch (IOException ioe) {
                LOGGER.error(e.getMessage(), ioe);
            }
            
        } else {
            status = PaystationConstants.FAILURE_INTERNAL_ERROR;
            response.setStatus(PaystationConstants.FAILURE_INTERNAL_ERROR);
        }
    }
    
    private boolean isServiceAgreementSigned(final int customerId, final boolean checkMigrated) {
        final Customer customer = this.customerService.findCustomer(customerId);
        int realCustomerId = customerId;
        if (customer.getParentCustomer() != null) {
            realCustomerId = customer.getParentCustomer().getId();
        }
        final ServiceAgreement agreement = this.serviceAgreementService.findLatestServiceAgreementByCustomerId(realCustomerId);
        return !(agreement == null);
    }
    
    protected final void addNotifications(final PaystationResponse paystationResponse, final PaystationMessageInfo messageInfo) {
        if (messageInfo.isAddEncryptionNotification()) {
            paystationResponse.setEncryptKeyNotification(new EncryptKeyNotification());
        }
        if (messageInfo.isAddLotSettingNotification()) {
            paystationResponse.setLotSettingNotification(new LotSettingNotification());
        }
        if (messageInfo.isAddTimestampNotification()) {
            paystationResponse.setTimestampNotification(new TimestampNotification(DateUtil.createColonDelimitedDateString()));
        }
        if (messageInfo.isAddRootCertificateNotification()) {
            paystationResponse.setRootCertificateNotification(new RootCertificateNotification());
        }
    }
    
    protected final PaystationResponse createErrorResponse(final HttpServletRequest request, final PaystationResponse response,
        final String messageNumber, final PaystationCommunicationException pce) throws PaystationCommunicationException {
        request.setAttribute(KPIConstants.XMLRPC_STATUS_CODE, HttpServletResponse.SC_BAD_REQUEST);
        final String errorMessage = getErrorMessage(pce.getStatus());
        if (PaystationConstants.RESPONSE_MESSAGE_PARAMETER_ERROR.equals(errorMessage)
            || PaystationConstants.RESPONSE_MESSAGE_SIGNATURE_ERROR.equals(errorMessage)) {
            throw pce;
        }
        response.setMessageNumber(messageNumber);
        response.setResponseMessage(errorMessage);
        response.setEmsDomainNotification(pce.getEmsDomainNotification());
        response.setTimestampNotification(pce.getTimestampNotification());
        return response;
    }
    
    private void processTimestampUpdate(final PaystationMessageInfo messageInfo) throws InvalidDataException {
        final EventDefinition uidChangeDefinition = this.alertsService.findEventDefinitionById(WebCoreConstants.EVENT_DEFINITION_CLOCK_CHANGED);
        final EventData eventData = new EventData();
        eventData.setPointOfSaleId(messageInfo.getPointOfSale().getId());
        eventData.setType(WebCoreConstants.EVENT_TYPE_PAYSTATION);
        eventData.setAction(WebCoreConstants.EVENT_ACTION_CLOCK_CHANGED_STRING);
        eventData.setSeverity(uidChangeDefinition.getEventSeverityType().getName());
        eventData.setTimeStamp(new Date());
        this.eventAlertService.processEvent(messageInfo.getPointOfSale(), eventData);
    }
    
}
