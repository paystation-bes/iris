package com.digitalpaytech.paystation.rest.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.domain.PosMacAddress;
import com.digitalpaytech.dto.rest.paystation.PaystationHeartBeat;
import com.digitalpaytech.dto.rest.paystation.PaystationHeartBeatResponse;
import com.digitalpaytech.dto.rest.paystation.PaystationQueryParamList;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.PaystationCommunicationException;
import com.digitalpaytech.paystation.rest.support.PaystationMessageInfo;
import com.digitalpaytech.servlet.DPTHttpServletRequestWrapper;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.util.RestCoreConstants;

@Controller
@RequestMapping(value = "/HeartBeat")
public class PaystationHeartBeatController extends PaystationBaseController {
    
    private static final Logger LOGGER = Logger.getLogger(PaystationHeartBeatController.class);
    
    @RequestMapping(method = RequestMethod.POST, consumes = "application/xml", produces = "application/xml")
    @ResponseBody
    public final PaystationHeartBeatResponse postHeartBeat(final HttpServletResponse response, final HttpServletRequest request,
        @RequestBody final PaystationHeartBeat paystationRequest, @RequestParam("Signature") final String signature,
        @RequestParam("SignatureVersion") final String signatureVersion, @RequestParam("Timestamp") final String timestamp,
        @RequestParam("SerialNumber") final String serialNumber, @RequestParam("Version") final String version) throws ApplicationException {
        
        final String methodName = this.getClass().getName() + ".postHeartBeat()";
        final PaystationMessageInfo messageInfo = new PaystationMessageInfo();
        
        try {
            final PaystationQueryParamList param = new PaystationQueryParamList();
            param.setSerialNumber(serialNumber);
            param.setSignature(signature);
            param.setSignatureVersion(signatureVersion);
            param.setTimestamp(timestamp);
            param.setVersion(version);
            
            super.validateRequest(param, paystationRequest, messageInfo);
            
            final PosMacAddress posMacAddress = super.posMacAddressService.findPosMacAddressByPointOfSaleId(messageInfo.getPointOfSale().getId());
            if (posMacAddress == null) {
                final StringBuilder errorMessage = new StringBuilder();
                errorMessage.append("No POSMacAddress found. Signature failed for PointOfSale:");
                errorMessage.append(paystationRequest.getPaystationCommAddress());
                LOGGER.error(errorMessage);
                
                throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_SIGNATURE_DOES_NOT_MATCH);
            }
            
            final String secretKey = posMacAddress.getSecretKey();
            
            /* verify the payload by calling the method in encryptionService. */
            if (!super.encryptionService.verifyHMACSignature(request.getServerName(), RestCoreConstants.HTTP_METHOD_POST, param.getSignature(),
                                                             param, ((DPTHttpServletRequestWrapper) request).getBody(), secretKey)) {
                final StringBuilder errorMessage = new StringBuilder();
                errorMessage.append("Signature failed for PointOfSale:");
                errorMessage.append(paystationRequest.getPaystationCommAddress());
                LOGGER.info(errorMessage);
                
                throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_SIGNATURE_DOES_NOT_MATCH);
            }
            
            /* Initiate the response. */
            final PaystationHeartBeatResponse paystationResponse = new PaystationHeartBeatResponse();
            paystationResponse.setMessageNumber(paystationRequest.getMessageNumber());
            paystationResponse.setResponseMessage(PaystationConstants.RESPONSE_MESSAGE_SUCCESS);
            super.addNotifications(paystationResponse, messageInfo);
            
            return paystationResponse;
        } catch (PaystationCommunicationException pce) {
            LOGGER.debug("#### ERROR IN " + methodName + " ####");
            PaystationHeartBeatResponse paystationResponse = new PaystationHeartBeatResponse();
            paystationResponse = (PaystationHeartBeatResponse) createErrorResponse(request, paystationResponse, paystationRequest.getMessageNumber(),
                                                                                   pce);
            LOGGER.debug("#### REPONSE RETURNED " + methodName + " ####");
            return paystationResponse;
        }
    }
}
