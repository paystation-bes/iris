package com.digitalpaytech.paystation.rest.controller;

import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.cxf.common.util.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.data.TransactionData;
import com.digitalpaytech.domain.PosMacAddress;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.dto.paystation.CardEaseData;
import com.digitalpaytech.dto.paystation.TransactionDto;
import com.digitalpaytech.dto.rest.paystation.PaystationQueryParamList;
import com.digitalpaytech.dto.rest.paystation.PaystationTransaction;
import com.digitalpaytech.dto.rest.paystation.PaystationTransactionResponse;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.DuplicateTransactionRequestException;
import com.digitalpaytech.exception.PaystationCommunicationException;
import com.digitalpaytech.paystation.rest.support.PaystationMessageInfo;
import com.digitalpaytech.rpc.support.threads.SensorExtractionThread;
import com.digitalpaytech.servlet.DPTHttpServletRequestWrapper;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.KPIConstants;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.util.RestCoreConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
@RequestMapping(value = "/Transaction")
public class PaystationTransactionController extends PaystationBaseController {
    
    private static final Logger LOGGER = Logger.getLogger(PaystationTransactionController.class);
    
    @RequestMapping(method = RequestMethod.POST, consumes = "application/xml", produces = "application/xml")
    @ResponseBody
    public final PaystationTransactionResponse postTransaction(final HttpServletResponse response, final HttpServletRequest request,
        @RequestBody final PaystationTransaction paystationRequest, @RequestParam("Signature") final String signature,
        @RequestParam("SignatureVersion") final String signatureVersion, @RequestParam("Timestamp") final String timestamp,
        @RequestParam("SerialNumber") final String serialNumber, @RequestParam("Version") final String version) throws ApplicationException {
        
        final String methodName = this.getClass().getName() + ".postTransaction()";
        final PaystationMessageInfo messageInfo = new PaystationMessageInfo();
        TransactionData transactionData = null;
        
        try {
            final PaystationQueryParamList param = new PaystationQueryParamList();
            param.setSerialNumber(serialNumber);
            param.setSignature(signature);
            param.setSignatureVersion(signatureVersion);
            param.setTimestamp(timestamp);
            param.setVersion(version);
            
            super.validateRequest(param, paystationRequest, messageInfo);
            
            final PosMacAddress posMacAddress = super.posMacAddressService.findPosMacAddressByPointOfSaleId(messageInfo.getPointOfSale().getId());
            if (posMacAddress == null) {
                final StringBuilder errorMessage = new StringBuilder();
                errorMessage.append("No POSMacAddress found. Signature failed for PointOfSale:");
                errorMessage.append(paystationRequest.getPaystationCommAddress());
                LOGGER.error(errorMessage);
                
                throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_SIGNATURE_DOES_NOT_MATCH);
            }
            
            final String secretKey = posMacAddress.getSecretKey();
            
            /* verify the payload by calling the method in encryptionService. */
            if (!super.encryptionService.verifyHMACSignature(request.getServerName(), RestCoreConstants.HTTP_METHOD_POST, param.getSignature(),
                param, ((DPTHttpServletRequestWrapper) request).getBody(), secretKey)) {
                
                final StringBuilder errorMessage = new StringBuilder();
                errorMessage.append("Signature failed for PointOfSale:");
                errorMessage.append(paystationRequest.getPaystationCommAddress());
                LOGGER.error(errorMessage);
                
                throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_SIGNATURE_DOES_NOT_MATCH);
            }
            
            final Purchase purchase = super.purchaseService.findUniquePurchaseForSms(messageInfo.getPointOfSale().getCustomer().getId(), messageInfo
                    .getPointOfSale().getId(), DateUtil.convertFromColonDelimitedDateString(paystationRequest.getPurchasedDate()), Integer
                    .parseInt(paystationRequest.getNumber()));
            if (purchase != null) {
                final PaystationTransactionResponse paystationResponse = new PaystationTransactionResponse();
                paystationResponse.setMessageNumber(paystationRequest.getMessageNumber());
                paystationResponse.setResponseMessage(PaystationConstants.RESPONSE_MESSAGE_SUCCESS);
                paystationResponse.setPurchaseId(purchase.getId());
                
                if (purchase.getCardPaidAmount() > 0) {
                    final Collection<ProcessorTransaction> pts = super.processorTransactionService.findProcessorTransactionByPurchaseId(purchase
                            .getId());
                    if (pts != null && !pts.isEmpty()) {
                        final ProcessorTransaction pt = pts.iterator().next();
                        if (!StringUtils.isEmpty(pt.getAuthorizationNumber())) {
                            paystationResponse.setAuthorizationId(pt.getSanitizedAuthorizationNumber());
                        }
                    }
                }
                
                super.addNotifications(paystationResponse, messageInfo);
                return paystationResponse;
            }
            
            final TransactionDto txDto = new TransactionDto(paystationRequest);
            transactionData = super.transactionHelper.getTransactionData(messageInfo.getPointOfSale(), txDto, null, version, true);
            transactionData.setStoreForward(paystationRequest.getStoreAndForward() != null
                                            && Integer.parseInt(paystationRequest.getStoreAndForward()) == 1);
            transactionData.setSingleTransaction(true);
            boolean isNoRefund = false;
            
            request.setAttribute(KPIConstants.PAYMENT_TYPE, (int) transactionData.getPurchase().getPaymentType().getId());
            request.setAttribute(KPIConstants.IS_STOREFORWARD, transactionData.isStoreForward());
            if (transactionData.getPermit() != null) {
                request.setAttribute(KPIConstants.PERMIT_ISSUE_TYPE, (int) transactionData.getPermit().getPermitIssueType().getId());
            } else {
                request.setAttribute(KPIConstants.PERMIT_ISSUE_TYPE, (int) WebCoreConstants.PERMIT_ISSUE_TYPE_NA);
                
            }
            
            
            boolean isManualSave = super.cardProcessingMaster.getCardProcessingManager().isManualSave(transactionData.getProcessorTransaction().getPointOfSale());
            if (super.transactionHelper.hasDataToProcess(transactionData.getPurchase().getPaymentCards()) && transactionData.isStoreForward()) {
                // Store & forward.                
                final List<Object> objects = super.transactionService.processTransaction(transactionData, isManualSave, null, txDto.getEmbeddedTxObject());                
                super.cardProcessingMaster.addTransactionToStoreForwardQueue(super.cardRetryTransactionService.getCardRetryTransaction(objects));
                
            } else {
                if (super.transactionHelper.hasDataToProcess(transactionData.getPurchase().getPaymentCards())) {
                    if (super.transactionHelper.hasCustomerCardTypeAuthInternal(transactionData)) {
                        final Integer[] subscriptionCodes = { WebCoreConstants.SUBSCRIPTION_TYPE_PASSCARDS };
                        if (!WebSecurityUtil.checkForPosSubscriptions(transactionData.getPointOfSale(), subscriptionCodes)) {
                            throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_UNSUBSCRIBED_SERVICE_ERROR);
                        }
                        if (!super.cardProcessingManager.processTransactionAuthorizationSinglePhaseTransaction(transactionData)) {
                            throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_CREDIT_CARD_DENIED);
                        }
                    } else if (super.transactionHelper.hasCustomerCardTypeAuthExternal(transactionData)) {
                        final Integer[] subscriptionCodes = { WebCoreConstants.SUBSCRIPTION_TYPE_REALTIME_CAMPUS_CARD_PROCESSING };
                        if (!WebSecurityUtil.checkForPosSubscriptions(transactionData.getPointOfSale(), subscriptionCodes)) {
                            throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_UNSUBSCRIBED_SERVICE_ERROR);
                        }
                        if (!super.cardProcessingManager.processTransactionAuthorizationSinglePhaseTransaction(transactionData)) {
                            throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_CREDIT_CARD_DENIED);
                        }
                        
                        if (isManualSave) {
                            super.cardProcessingManager.processCardForSingleTransactionManualSave(transactionData, false);
                        } else {
                            super.cardProcessingManager.processCardForSingleTransaction(transactionData, false);
                        }
                        isNoRefund = true;
                    } else if (super.transactionHelper.hasCustomerCardTypeAuthNone(transactionData)
                               || super.transactionHelper.hasSmartCardDataAndPaid(txDto)) {
                        //DO NOT DO ANYTHING
                    } else {
                        final Integer[] subscriptionCodes = { WebCoreConstants.SUBSCRIPTION_TYPE_REALTIME_CC_PROCESSING };
                        if (!WebSecurityUtil.checkForPosSubscriptions(transactionData.getPointOfSale(), subscriptionCodes)) {
                            throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_UNSUBSCRIBED_SERVICE_ERROR);
                        }                        
                        if (isManualSave) {
                            isNoRefund = super.cardProcessingManager.processCardForSingleTransactionManualSave(transactionData, true);
                        } else {
                            isNoRefund = super.cardProcessingManager.processCardForSingleTransaction(transactionData, true);
                        }                        
                    }
                }
                if (super.transactionHelper.hasDataToProcess(transactionData.getSmsAlert())) {
                    super.transactionService.processTransactionWithSmsAlert(transactionData, txDto.getEmbeddedTxObject());
                } else {                    
                    super.transactionService.processTransaction(transactionData, isManualSave, null, txDto.getEmbeddedTxObject());                    
                }
                
                super.cardProcessingManager.specificProcessorPostProcessing(transactionData.getProcessorTransaction());
                
            }
            
            SensorExtractionThread.getInstance().dataAvailable();
            
            /* Initiate the response. */
            final PaystationTransactionResponse paystationResponse = new PaystationTransactionResponse();
            paystationResponse.setMessageNumber(paystationRequest.getMessageNumber());
            if (isNoRefund) {
                paystationResponse.setResponseMessage(PaystationConstants.RESPONSE_MESSAGE_SUCCESSNOREFUND);
            } else {
                paystationResponse.setResponseMessage(PaystationConstants.RESPONSE_MESSAGE_SUCCESS);
            }
            
            paystationResponse.setPurchaseId(transactionData.getPurchase().getId());
            
            if (transactionData.getPurchase().getCardPaidAmount() > 0) {
                paystationResponse.setAuthorizationId(transactionData.getProcessorTransaction().getSanitizedAuthorizationNumber());
            }
            
            super.addNotifications(paystationResponse, messageInfo);
            
            return paystationResponse;
            
        } catch (DuplicateTransactionRequestException dtre) {
            
            final Purchase purchase = super.purchaseService.findUniquePurchaseForSms(messageInfo.getPointOfSale().getCustomer().getId(), messageInfo
                    .getPointOfSale().getId(), DateUtil.convertFromColonDelimitedDateString(paystationRequest.getPurchasedDate()), Integer
                    .parseInt(paystationRequest.getNumber()));
            if (purchase != null) {
                final PaystationTransactionResponse paystationResponse = new PaystationTransactionResponse();
                paystationResponse.setMessageNumber(paystationRequest.getMessageNumber());
                paystationResponse.setResponseMessage(PaystationConstants.RESPONSE_MESSAGE_SUCCESS);
                paystationResponse.setPurchaseId(purchase.getId());
                
                if (purchase.getCardPaidAmount() > 0) {
                    final Collection<ProcessorTransaction> pts = super.processorTransactionService.findProcessorTransactionByPurchaseId(purchase
                            .getId());
                    if (pts != null && !pts.isEmpty()) {
                        final ProcessorTransaction pt = pts.iterator().next();
                        if (!StringUtils.isEmpty(pt.getAuthorizationNumber())) {
                            paystationResponse.setAuthorizationId(pt.getSanitizedAuthorizationNumber());
                        }
                    }
                }
                
                super.addNotifications(paystationResponse, messageInfo);
                return paystationResponse;
            } else {
                LOGGER.debug("#### ERROR IN " + methodName + " ####");
                PaystationTransactionResponse paystationResponse = new PaystationTransactionResponse();
                paystationResponse = (PaystationTransactionResponse) createErrorResponse(request, paystationResponse,
                    paystationRequest.getMessageNumber(), new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_IS_INVALID));
                LOGGER.debug("#### REPONSE RETURNED " + methodName + " ####");
                return paystationResponse;
                
            }
            
        } catch (PaystationCommunicationException pce) {
            LOGGER.debug("#### ERROR IN " + methodName + " ####");
            PaystationTransactionResponse paystationResponse = new PaystationTransactionResponse();
            paystationResponse = (PaystationTransactionResponse) createErrorResponse(request, paystationResponse,
                paystationRequest.getMessageNumber(), pce);
            LOGGER.debug("#### REPONSE RETURNED " + methodName + " ####");
            return paystationResponse;
        } catch (ApplicationException ae) {
            throw ae;
        }
    }
}
