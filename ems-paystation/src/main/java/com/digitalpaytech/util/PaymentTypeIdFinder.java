package com.digitalpaytech.util;

import org.apache.log4j.Logger;

import com.digitalpaytech.util.WebCoreConstants;

public final class PaymentTypeIdFinder {
    private static final Logger LOG = Logger.getLogger(PaymentTypeIdFinder.class);
    private boolean usedCash;
    private boolean usedCC;
    private boolean usedValueCard;
    private boolean usedSC;
    private boolean usedCCChip;
    private boolean usedCCSwipe;
    private boolean usedCCCL;
    private boolean usedCCFSwipe;
    private int pointOfSaleId;
    
    public PaymentTypeIdFinder(final int pointOfSaleId) {
        this.pointOfSaleId = pointOfSaleId;
    }
    
    public int findPaymentTypeId() {
        if (this.usedCash && !this.usedCCSwipe && !this.usedCCCL && !this.usedCCChip && !this.usedValueCard && !this.usedSC && !this.usedCCFSwipe) {
            // Cash Payment
            return WebCoreConstants.PAYMENT_TYPE_CASH;

        } else if (this.usedCash && this.usedCCSwipe && !this.usedCCCL && !this.usedCCChip && !this.usedValueCard && !this.usedSC && !this.usedCCFSwipe) {
            // Cash/Credit Card Swipe Payment
            return WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_SWIPE;
        
        } else if (this.usedCash && !this.usedCCSwipe && this.usedCCCL && !this.usedCCChip && !this.usedValueCard && !this.usedSC && !this.usedCCFSwipe) {
            // Cash/Credit Card Contactless Payment
            return WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CL;
        
        } else if (this.usedCash && !this.usedCCSwipe && !this.usedCCCL && this.usedCCChip && !this.usedValueCard && !this.usedSC && !this.usedCCFSwipe) {
            // Cash and Chip
            return WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CH;
            
        } else if (!this.usedCash && !this.usedCCSwipe && !this.usedCCCL && !this.usedCCChip && !this.usedValueCard && !this.usedSC && this.usedCCFSwipe) {
            // Credit Card FSwipe Payment
            return WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_FSWIPE;
            
        } else if (this.usedCash && !this.usedCCSwipe && !this.usedCCCL && !this.usedCCChip && !this.usedValueCard && !this.usedSC && this.usedCCFSwipe) {
            // Cash and FSwipe
            return WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CARD_FSWIPE;
        
        } else if (this.usedCash && !this.usedCCSwipe && !this.usedCCCL && !this.usedCCChip && this.usedValueCard && !this.usedSC && !this.usedCCFSwipe) {
            // Cash/Value Card Payment
            return WebCoreConstants.PAYMENT_TYPE_CASH_VALUE;
        
        } else if (this.usedCash && !this.usedCCSwipe && !this.usedCCCL && !this.usedCCChip && !this.usedValueCard && this.usedSC && !this.usedCCFSwipe) {
            // Cash/SmartCard Payment
            return WebCoreConstants.PAYMENT_TYPE_CASH_SMART;
        
        } else if (!this.usedCash && this.usedCCSwipe && !this.usedCCCL && !this.usedCCChip && !this.usedValueCard && !this.usedSC && !this.usedCCFSwipe) {
            // Credit Card Swipe Payment
            return WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_SWIPE;
        
        } else if (!this.usedCash && !this.usedCCSwipe && this.usedCCCL && !this.usedCCChip && !this.usedValueCard && !this.usedSC && !this.usedCCFSwipe) {
            // Credit Card Contactless Payment
            return WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CONTACTLESS;
        
        } else if (!this.usedCash && !this.usedCCSwipe && !this.usedCCCL && this.usedCCChip && !this.usedValueCard && !this.usedSC && !this.usedCCFSwipe) {
            // Chip
            return WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CHIP;
        
        } else if (!this.usedCash && !this.usedCCSwipe && !this.usedCCCL && !this.usedCCChip && this.usedValueCard && !this.usedSC && !this.usedCCFSwipe) {
            // Value Card Payment
            return WebCoreConstants.PAYMENT_TYPE_VALUE_CARD;
        
        } else if (!this.usedCash && !this.usedCCSwipe && !this.usedCCCL && !this.usedCCChip && !this.usedValueCard && this.usedSC && !this.usedCCFSwipe) {
            // SmartCard Payment
            return WebCoreConstants.PAYMENT_TYPE_SMART_CARD;
        
        } else {
            // Unknown Payment
            final StringBuilder bdr = new StringBuilder();
            bdr.append("Invalid Payment Type for PointOfSaleId: ").append(this.pointOfSaleId).append(". ").append(this.toString());
            LOG.warn(bdr.toString());
            return WebCoreConstants.PAYMENT_TYPE_UNKNOWN;
        }    
    }
    
    @Override
    public String toString() {
        return "PaymentOptions [usedCash=" + this.usedCash + ", usedCC=" + this.usedCC + ", usedValueCard=" + this.usedValueCard 
                + ", usedSC=" + this.usedSC + ", usedCCChip=" + this.usedCCChip + ", usedCCSwipe=" + this.usedCCSwipe 
                + ", usedCCCL=" + this.usedCCCL + ", usedCCFSwipe=" + this.usedCCFSwipe + "]";
    }
    
    public boolean isUsedCash() {
        return this.usedCash;
    }
    public void setUsedCash(final boolean usedCash) {
        this.usedCash = usedCash;
    }
    public boolean isUsedCC() {
        return this.usedCC;
    }
    public void setUsedCC(final boolean usedCC) {
        this.usedCC = usedCC;
    }
    public boolean isUsedValueCard() {
        return this.usedValueCard;
    }
    public void setUsedValueCard(final boolean usedValueCard) {
        this.usedValueCard = usedValueCard;
    }
    public boolean isUsedSC() {
        return this.usedSC;
    }
    public void setUsedSC(final boolean usedSC) {
        this.usedSC = usedSC;
    }
    public boolean isUsedCCChip() {
        return this.usedCCChip;
    }
    public void setUsedCCChip(final boolean usedCCChip) {
        this.usedCCChip = usedCCChip;
    }
    public boolean isUsedCCSwipe() {
        return this.usedCCSwipe;
    }
    public void setUsedCCSwipe(final boolean usedCCSwipe) {
        this.usedCCSwipe = usedCCSwipe;
    }
    public boolean isUsedCCCL() {
        return this.usedCCCL;
    }
    public void setUsedCCCL(final boolean usedCCCL) {
        this.usedCCCL = usedCCCL;
    }
    public boolean isUsedCCFSwipe() {
        return this.usedCCFSwipe;
    }
    public void setUsedCCFSwipe(final boolean usedFSwipe) {
        this.usedCCFSwipe = usedFSwipe;
    }
    public int getPointOfSaleId() {
        return this.pointOfSaleId;
    }
}
