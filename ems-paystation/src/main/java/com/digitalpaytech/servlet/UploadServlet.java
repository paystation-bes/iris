package com.digitalpaytech.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.zip.ZipFile;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import com.digitalpaytech.client.FacilitiesManagementClient;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.SettingsFile;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.TransactionFileUpload;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.rpc.support.threads.DeleteTempFileThread;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.DigitalAPIUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.crypto.CryptoConstants;
import com.digitalpaytech.util.support.PaystationSettingZipReader;

import io.netty.buffer.ByteBuf;
import rx.Observable;
import rx.functions.Action0;
import rx.observables.StringObservable;

@SuppressWarnings("checkstyle:illegalcatch")
public class UploadServlet extends PosBaseServlet {
    private static final long serialVersionUID = -4599522742332912792L;
    
    private static final Logger LOG = Logger.getLogger(UploadServlet.class);
    
    private static final String LOT_SETTING = "lotsetting";
    
    private static final String FILE_UPLOAD = "FileUpload";
    
    private static final String PAYMENTSTATION_QUERY = "paymentstationquery";
    
    private static final String[] ALLOWED_FILE_EXTENSION = { "zip", "hpz", "hzt" };
    private static final String OFFLINE_TRANSACTION = "OffLineXMLTransaction";
    
    private static final String FILE_PREFIX_CC = "cc";
    private static final String FILE_PREFIX_SETTINGS = "settings";
    
    private static final String ERR_EMPTY_FILE_NAME = "Upload filename cannot be empty";
    
    private static final String ERRFMT_INVALID_FEATURE =
            "Returning ''feature not enabled - User Status is NOT enabled'' {0} for username: {2}(customerid={1}).";
    
    private static final String ERRFMT_MOVED_POS = "PaystationId ''{1}'' is no longer belong to customerId ''{0}''";
    
    private static final String FEATURE_UPLOAD_CARD_TRANSACTION = "uploadCardTransaction";
    private static final String FEATURE_UPLOAD_XML_TRANSACTION = "uploadXMLTransaction";
    private static final String FEATURE_UPLOAD_PS_SETTING = "uploadPaystationSetting";
    
    private static final String RETURNED_ERROR_UPLOAD_PS_SETTING = "Returning 'upload error' PaystationSettingUploadResponse";
    private static final String RETURNED_ERROR_INTERNAL_CARD_TX = "Returning 'internal error' CardTransactionUploadResponse";
    private static final String RETURNED_ERROR_UPLOAD_CARD_TX = "Returning 'upload error' CardTransactionUploadResponse";
    private static final String RETURNED_ERROR_INTERNAL_XML_TX = "Returning 'internal error' XMLTransactionUploadResponse";
    private static final String RETURNED_ERROR_UPLPOAD_XML_TX = "Returning 'upload error' XMLTransactionUploadResponse";
    
    public final void init(final ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        
        LOG.info("Upload Servlet Initialized.");
    }
    
    public final void doPost(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        // Get Current Session
        final HttpSession session = request.getSession(false);
        
        // Checks if the request actually contains upload file
        if (!ServletFileUpload.isMultipartContent(request)) {
            // If not, we stop here
            response.getWriter().println(BO2_CODE_INTERNAL_ERROR);
            // Terminate session if have exception
            if (session != null) {
                session.invalidate();
            }
            return;
        }
        
        // get the upload type
        final String type = request.getParameter(TYPE_PARAMETER);
        if (checkForInvalidCharacters(TYPE_PARAMETER, type)) {
            if (isUploadTypeSupported(type)) {
                
                // process upload
                try {
                    processUpload(type, request, response);
                    
                } catch (Exception e) {
                    LOG.error("Bad Request and upload error", e);
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                }
                
            } else {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                LOG.error("Bad Request. No upload handler found for type: " + type);
            }
        }
        // Terminate session doPost finish
        if (session != null) {
            session.invalidate();
        }
    }
    
    private UploadReturn uploadFile(final HttpServletRequest request, final HttpServletResponse response, final String location,
        final String fileNamePrefix, final String fileExtention) {
        
        final DiskFileItemFactory factory = new DiskFileItemFactory();
        
        final ServletFileUpload upload = new ServletFileUpload(factory);
        
        // Creates the directory if it does not exist
        final File uploadDir = new File(location);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }
        
        final UploadReturn uploadReturn = new UploadReturn();
        try {
            final int maxSize = super.getEmsPropertiesService().getPropertyValueAsInt(EmsPropertiesService.MAX_UPLOAD_FILE_SIZE_IN_BYTES,
                                                                                      EmsPropertiesService.DEFAULT_MAX_UPLOAD_FILE_SIZE_IN_BYTES);
            
            factory.setSizeThreshold(maxSize);
            
            // Directory where upload file is located.
            factory.setRepository(new File(location));
            
            upload.setFileSizeMax(maxSize);
            upload.setSizeMax(maxSize * 2);
            
            // Parses the request's content to extract file data and login information
            final List<FileItem> formItems = upload.parseRequest(request);
            
            // Creates and authenticates user
            final UserAccount userAccount = authenticate(request, response, formItems);
            if (userAccount == null) {
                return uploadReturn;
            }
            
            uploadReturn.setUserAccount(userAccount);
            final FileItem item = getUploadFile(formItems);
            if (item == null) {
                return uploadReturn;
            } else if (item.getSize() > maxSize) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append("Upload file size: ").append(item.getSize()).append(" exceeds the max size: ").append(maxSize);
                LOG.error(bdr.toString());
                throw new IOException(bdr.toString());
            }
            
            // Processes only fields that are not form fields
            if (!item.isFormField()) {
                String fileName = item.getName();
                if (FILE_PREFIX_CC.equals(fileNamePrefix)) {
                    final StringBuilder fileNameBuilder = new StringBuilder(fileNamePrefix);
                    fileNameBuilder.append(userAccount.getCustomer().getId());
                    fileNameBuilder.append(StandardConstants.CHAR_UNDERSCORE);
                    fileNameBuilder.append(System.currentTimeMillis());
                    fileNameBuilder.append(StandardConstants.CHAR_DOT);
                    fileNameBuilder.append(fileExtention);
                    
                    fileName = fileNameBuilder.toString();
                } else if (FILE_PREFIX_SETTINGS.equals(fileNamePrefix)) {
                    final StringBuilder fileNameBuilder = new StringBuilder();
                    fileNameBuilder.append(userAccount.getCustomer().getId());
                    fileNameBuilder.append(StandardConstants.CHAR_UNDERSCORE);
                    fileNameBuilder.append(System.currentTimeMillis());
                    fileNameBuilder.append(StandardConstants.CHAR_UNDERSCORE);
                    fileNameBuilder.append(fileName);
                    
                    fileName = fileNameBuilder.toString();
                }
                final String filePath = location + fileName;
                final File storeFile = new File(filePath);
                if (FILE_PREFIX_SETTINGS.equals(fileNamePrefix) && !storeFile.createNewFile()) {
                    final String errorMsg = "Detected duplicated file: " + filePath;
                    LOG.error(errorMsg);
                    throw new IOException(errorMsg);
                }
                // Saves the file on disk
                item.write(storeFile);
                uploadReturn.setFullFilePath(storeFile.getAbsolutePath());
            }
            
        } catch (Exception e) {
            LOG.error("Cannot upload file, ", e);
            return null;
        }
        return uploadReturn;
    }
    
    private void uploadPaystationSetting(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        final PrintWriter printWriter = response.getWriter();
        
        try {
            
            // e.g. fileName = <customer.getId>_<System.currentTimeMillis()>.zip
            
            // Get file location, use customerid and timestamp for name to
            // keep unique in temp directory
            final String dirName = WebCoreConstants.DEFAULT_TEMPFILE_LOCATION;
            
            // Save file
            
            final UploadReturn uploadReturn = uploadFile(request, response, dirName, FILE_PREFIX_SETTINGS, ALLOWED_FILE_EXTENSION[0]);
            
            if (uploadReturn == null || uploadReturn.getUserAccount() == null) {
                return;
            } else if (uploadReturn.getFullFilePath() == null) {
                throw new FileNotFoundException(ERR_EMPTY_FILE_NAME);
            }
            
            final UserAccount userAccount = uploadReturn.getUserAccount();
            final Customer customer = userAccount.getCustomer();
            
            // Check if UserStatusType is NOT 'Enabled' (1).
            if (userAccount.getUserStatusType().getId() != 1) {
                printWriter.println(BO2_CODE_FEATURE_NOT_ENABLED);
                LOG.error(MessageFormat.format(ERRFMT_INVALID_FEATURE, FEATURE_UPLOAD_XML_TRANSACTION, customer.getId(), userAccount.getUserName()));
                return;
            }
            
            // Check permissions
            final Integer[] perms = { WebSecurityConstants.PERMISSION_UPLOAD_CONFIG_FROM_BOSS };
            if (!enforceFeaturePermissions(userAccount, perms)) {
                printWriter.println(BO2_CODE_FEATURE_NOT_ENABLED);
                LOG.error(MessageFormat.format(ERRFMT_INVALID_FEATURE, FEATURE_UPLOAD_PS_SETTING, customer.getId(), userAccount.getUserName()));
                return;
            }
            
            final String fullFilePath = uploadReturn.getFullFilePath();
            
            // Return 'success' if transaction file is a duplicate.
            if (updateDuplicatedTransactionFile(customer, fullFilePath)) {
                printWriter.println(BO2_CODE_SUCCESS);
                return;
            }
            
            final File tmpFile = new File(fullFilePath);
            
            //forward file to facility management service 
            final Future<ByteBuf> fmsResponse = sendFileToFMS(customer.getId(), tmpFile);
            
            // add/update lotsetting for the customer
            final PaystationSettingZipReader zipReader = new PaystationSettingZipReader(fullFilePath);
            String fileName = null;
            if (fullFilePath.contains(StandardConstants.SEPARATOR_URL)) {
                fileName = fullFilePath.substring(fullFilePath.lastIndexOf(StandardConstants.SEPARATOR_URL) + 1);
            } else {
                fileName = fullFilePath.substring(fullFilePath.lastIndexOf("\\") + 1);
            }
            final SettingsFile newPaystationSetting = createPaystationSetting(customer, zipReader, fileName);
            final List<String> serialNumbers = zipReader.getSerialNumbers();
            
            super.getPaystationSettingService().processSettingsFileUpload(customer, fullFilePath, newPaystationSetting, serialNumbers);
            
            ByteBuf fmsResult = null;
            try {
                // get response from FMS
                fmsResult = fmsResponse.get();
                
            } finally {
                if (fmsResult != null) {
                    fmsResult.release();
                }
                
                // delete the temp file
                if (tmpFile.exists() && tmpFile.isFile()) {
                    super.getTransactionFileUploadService().saveTransactionFileUpload(customer, fullFilePath);
                    new DeleteTempFileThread(tmpFile.getAbsolutePath()).start();
                }
            }
            
            // Return results
            printWriter.println(BO2_CODE_SUCCESS);
            if (LOG.isInfoEnabled()) {
                LOG.info("Returning 'successful' PaystationSettingUploadResponse for: " + userAccount.getUserName());
            }
        } catch (SecurityException e) {
            printWriter.println(BO2_CODE_UPLOAD_ERROR);
            LOG.error(RETURNED_ERROR_UPLOAD_PS_SETTING, e);
        } catch (FileNotFoundException e) {
            printWriter.println(BO2_CODE_UPLOAD_ERROR);
            LOG.error(RETURNED_ERROR_UPLOAD_PS_SETTING, e);
        } catch (CancellationException | ExecutionException | InterruptedException e) {
            printWriter.println(BO2_CODE_INTERNAL_ERROR);
            LOG.error("Error forwarding file to facility management service", e);
        } catch (Exception e) {
            printWriter.println(BO2_CODE_INTERNAL_ERROR);
            LOG.error(RETURNED_ERROR_INTERNAL_CARD_TX, e);
        } finally {
            if (printWriter != null) {
                printWriter.close();
            }
        }
    }
    
    private Future<ByteBuf> sendFileToFMS(final Integer customerId, final File file) throws FileNotFoundException {
        
        final FileInputStream fileInputStream;
        fileInputStream = new FileInputStream(file);
        final FacilitiesManagementClient fmClient = super.getClientFactory().from(FacilitiesManagementClient.class);
        final Observable<byte[]> fileObservable = StringObservable.from(fileInputStream, StandardConstants.ONE_KILOBYTE * 10);
        
        fileObservable.doOnCompleted(new Action0() {
            @Override
            public void call() {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    LOG.warn("Failed to close FileInputStream", e);
                }
            }
        });
        
        return fmClient.uploadPaystationSettings(String.valueOf(customerId), fileObservable).queue();
    }
    
    private SettingsFile createPaystationSetting(final Customer customer, final PaystationSettingZipReader zipReader, final String fileName)
        throws InvalidDataException {
        final SettingsFile newSetting = zipReader.readPaystationSettingZip();
        newSetting.setCustomer(customer);
        newSetting.setUploadGmt(new Date());
        newSetting.setFileName(fileName);
        return newSetting;
    }
    
    private void uploadCardTransaction(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        final PrintWriter printWriter = response.getWriter();
        
        try {
            
            final String dirName = WebCoreConstants.DEFAULT_FILE_UPLOAD_LOCATION;
            
            final UploadReturn uploadReturn = uploadFile(request, response, dirName, FILE_PREFIX_CC, ALLOWED_FILE_EXTENSION[0]);
            
            if (uploadReturn == null || uploadReturn.getUserAccount() == null) {
                return;
            } else if (uploadReturn.getFullFilePath() == null) {
                throw new FileNotFoundException(ERR_EMPTY_FILE_NAME);
            }
            
            final UserAccount userAccount = uploadReturn.getUserAccount();
            final Customer customer = userAccount.getCustomer();
            
            // Check if UserStatusType is NOT 'Enabled' (1).
            if (userAccount.getUserStatusType().getId() != 1) {
                printWriter.println(BO2_CODE_FEATURE_NOT_ENABLED);
                LOG.error(MessageFormat.format(ERRFMT_INVALID_FEATURE, FEATURE_UPLOAD_XML_TRANSACTION, customer.getId(), userAccount.getUserName()));
                return;
            }
            
            final Integer[] perms =
                    { WebSecurityConstants.PERMISSION_UPLOAD_CONFIG_FROM_BOSS, WebSecurityConstants.PERMISSION_PROCESS_CARD_CHARGE_FROM_BOSS, };
            if (!enforceFeatureSubscriptions(userAccount, WebCoreConstants.SUBSCRIPTION_TYPE_BATCH_CC_PROCESSING)
                || !enforceFeaturePermissions(userAccount, perms)) {
                printWriter.println(BO2_CODE_FEATURE_NOT_ENABLED);
                LOG.error(MessageFormat.format(ERRFMT_INVALID_FEATURE, FEATURE_UPLOAD_CARD_TRANSACTION, customer.getId(), userAccount.getUserName()));
                return;
            }
            
            final String fullFilePath = uploadReturn.getFullFilePath();
            
            // Return 'success' if transaction file is a duplicate.
            if (updateDuplicatedTransactionFile(customer, fullFilePath)) {
                printWriter.println(BO2_CODE_SUCCESS);
                return;
            }
            
            // verify total file in zip File and the file name in zip file
            if (!isTotalFileInZipFile(fullFilePath, 1) || !super.getCardTransactionUploadHelper().isCsvFileInZipValid(fullFilePath)) {
                // delete the saved file, send back response
                System.gc();
                WebCoreUtil.secureDelete(new File(fullFilePath));
                printWriter.println(BO2_CODE_UPLOAD_ERROR);
                LOG.error("Only can have one file in Zip file, CardTransactionUploadResponse");
                return;
            }
            
            // verify the hash
            final String hashReceived = request.getParameter("hash");
            final boolean hashMatched =
                    super.getCryptoAlgorithmFactory().verifySha1HashHex(hashReceived, new File(fullFilePath), CryptoConstants.HASH_FILE_UPLOAD);
            
            boolean headersVerified = false;
            String commAddress = null;
            if (hashMatched) {
                // check if commAddress exist for the customer
                PointOfSale pointOfSale = null;
                commAddress = super.getCardTransactionUploadHelper().getCommAddressFromZipFile(fullFilePath);
                
                if (commAddress == null
                    || (commAddress.length() != WebCoreConstants.LENGTH_PS_SERIAL_NUMBER && !commAddress.equals(COMMADDRESS_BADCARDS))) {
                    // delete the saved file, send back response
                    System.gc();
                    WebCoreUtil.secureDelete(new File(fullFilePath));
                    // syu: todo: change the following error code back to BO2_CODE_PAYMENTSTATION_NOT_EXIST
                    // once BOSS supports it
                    printWriter.println(BO2_CODE_UPLOAD_ERROR);
                    LOG.error("Returning 'commAddress does not exist' CardTransactionUploadResponse");
                    return;
                }
                
                if (!commAddress.equals(COMMADDRESS_BADCARDS)) {
                    pointOfSale = super.getPointOfSaleService().findTxablePointOfSaleBySerialNumber(commAddress);
                    if ((pointOfSale == null) || !customer.getId().equals(pointOfSale.getCustomer().getId())) {
                        // delete the saved file, send back response
                        System.gc();
                        WebCoreUtil.secureDelete(new File(fullFilePath));
                        // syu: todo: change the following error code back to
                        // BO2_CODE_PAYMENTSTATION_NOT_EXIST
                        // once BOSS supports it
                        printWriter.println(BO2_CODE_UPLOAD_ERROR);
                        LOG.error(MessageFormat.format(ERRFMT_MOVED_POS, customer.getId(), pointOfSale.getId()));
                        //LOG.error("Returning 'ps does not exist' CardTransactionUploadResponse");
                        return;
                    }
                }
                
                // verify headers
                headersVerified = verifyHeaders(fullFilePath);
            } else {
                // delete the saved file, send back response
                System.gc();
                WebCoreUtil.secureDelete(new File(fullFilePath));
                printWriter.println(BO2_CODE_UPLOAD_ERROR_INVALID_HASH);
                LOG.error("Returning 'invalid hash' CardTransactionUploadResponse to: " + userAccount.getUserName() + " and PS: " + commAddress);
                return;
            }
            
            if (!headersVerified) {
                // delete the saved file, send back response
                System.gc();
                WebCoreUtil.secureDelete(new File(fullFilePath));
                printWriter.println(BO2_CODE_UPLOAD_ERROR_INVALID_HEADERS);
                
                LOG.error("Returning 'invalid headers' CardTransactionUploadResponse to: " + userAccount.getUserName() + " and PS: " + commAddress);
            } else {
                final boolean bDisableProc =
                        super.getEmsPropertiesService().getPropertyValueAsBoolean(EmsPropertiesService.DISABLE_PROCESSING_UPLOADED_TRANSFILE, false);
                if (bDisableProc) {
                    LOG.info("Don't processing uploaded transaction file, cause disable flag be set in EmsProperties table");
                } else {
                    // create queue, send back response
                    super.getCardTransactionZipProcessor().addFileToQueue(fullFilePath);
                }
                
                // Return results
                printWriter.println(BO2_CODE_SUCCESS);
                LOG.info("Returning 'successful' CardTransactionUploadResponse to: " + userAccount.getUserName() + " and PS: " + commAddress);
            }
        } catch (SecurityException e) {
            printWriter.println(BO2_CODE_UPLOAD_ERROR);
            LOG.error(RETURNED_ERROR_UPLOAD_CARD_TX, e);
        } catch (CryptoException e) {
            printWriter.println(BO2_CODE_UPLOAD_ERROR);
            LOG.error(RETURNED_ERROR_UPLOAD_CARD_TX, e);
        } catch (Exception e) {
            printWriter.println(BO2_CODE_INTERNAL_ERROR);
            LOG.error(RETURNED_ERROR_INTERNAL_CARD_TX, e);
        } finally {
            if (printWriter != null) {
                printWriter.close();
            }
        }
    }
    
    private void uploadXMLTransactions(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        final PrintWriter printWriter = response.getWriter();
        try {
            
            final String dirName = WebCoreConstants.DEFAULT_TRANSACTION_UPLOAD_LOCATION;
            final UploadReturn uploadReturn = uploadFile(request, response, dirName, WebCoreConstants.EMPTY_STRING, ALLOWED_FILE_EXTENSION[1]);
            
            if (uploadReturn == null || uploadReturn.getUserAccount() == null) {
                return;
            } else if (uploadReturn.getFullFilePath() == null) {
                throw new FileNotFoundException(ERR_EMPTY_FILE_NAME);
            }
            
            final UserAccount userAccount = uploadReturn.getUserAccount();
            final Customer customer = userAccount.getCustomer();
            
            // Check if UserStatusType is NOT 'Enabled' (1).
            if (userAccount.getUserStatusType().getId() != 1) {
                printWriter.println(BO2_CODE_FEATURE_NOT_ENABLED);
                LOG.error(MessageFormat.format(ERRFMT_INVALID_FEATURE, FEATURE_UPLOAD_XML_TRANSACTION, customer.getId(), userAccount.getUserName()));
                return;
            }
            
            final Integer[] perms =
                    { WebSecurityConstants.PERMISSION_UPLOAD_CONFIG_FROM_BOSS, WebSecurityConstants.PERMISSION_PROCESS_CARD_CHARGE_FROM_BOSS, };
            if (!enforceFeatureSubscriptions(userAccount, WebCoreConstants.SUBSCRIPTION_TYPE_BATCH_CC_PROCESSING)
                || !enforceFeaturePermissions(userAccount, perms)) {
                printWriter.println(BO2_CODE_FEATURE_NOT_ENABLED);
                LOG.error(MessageFormat.format(ERRFMT_INVALID_FEATURE, FEATURE_UPLOAD_CARD_TRANSACTION, customer.getId(), userAccount.getUserName()));
                return;
            }
            
            final String fullFilePath = uploadReturn.getFullFilePath();
            
            // Return 'success' if transaction file is a duplicate.
            if (updateDuplicatedTransactionFile(customer, fullFilePath)) {
                printWriter.println(BO2_CODE_SUCCESS);
                return;
            }
            
            final long savedFileSize = new File(fullFilePath).length();
            
            if (LOG.isInfoEnabled()) {
                LOG.info("++++ saved hpz file size:" + savedFileSize + " +++");
            }
            
            final String serialNumber = fullFilePath.substring(dirName.length(), fullFilePath.indexOf("_"));
            if (LOG.isInfoEnabled()) {
                LOG.info("++++ hzp file uploaded at: " + fullFilePath);
            }
            // Verify serial number and evit PointOfSale object.
            final PointOfSale pointOfSale = super.getPointOfSaleService().findTxablePointOfSaleBySerialNumber(serialNumber);
            if ((pointOfSale == null) || !customer.getId().equals(pointOfSale.getCustomer().getId())) {
                // delete the saved file, send back response
                System.gc();
                WebCoreUtil.secureDelete(new File(fullFilePath));
                printWriter.println(BO2_CODE_UPLOAD_ERROR);
                final Integer customerId = customer == null ? null : customer.getId();
                final Integer posId = pointOfSale == null ? null : pointOfSale.getId();
                
                LOG.error(MessageFormat.format(ERRFMT_MOVED_POS, customerId, posId));
                return;
            }
            // create file name queue, send back response
            super.getXmlTransactionThreadRunner().addFileToQueue(fullFilePath);
            // Return results
            printWriter.println(BO2_CODE_SUCCESS);
        } catch (SecurityException e) {
            printWriter.println(BO2_CODE_UPLOAD_ERROR);
            LOG.error(RETURNED_ERROR_UPLPOAD_XML_TX, e);
        } catch (Exception e) {
            printWriter.println(BO2_CODE_INTERNAL_ERROR);
            LOG.error(RETURNED_ERROR_INTERNAL_XML_TX, e);
        } finally {
            if (printWriter != null) {
                printWriter.close();
            }
        }
        
    }
    
    /**
     * check if the headers of the csv file is valid
     * 
     * @param zipFileFullName
     * @return true if the headers are valid
     */
    private boolean verifyHeaders(final String zipFileFullName) {
        return super.getCardTransactionUploadHelper().verifyHeadersForZipFile(zipFileFullName);
    }
    
    private void handlePaymentstationQuery(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        final PrintWriter printWriter = response.getWriter();
        try {
            final int maxSize = super.getEmsPropertiesService().getPropertyValueAsInt(EmsPropertiesService.MAX_UPLOAD_FILE_SIZE_IN_BYTES,
                                                                                      EmsPropertiesService.DEFAULT_MAX_UPLOAD_FILE_SIZE_IN_BYTES);
            
            final DiskFileItemFactory factory = new DiskFileItemFactory();
            factory.setSizeThreshold(maxSize);
            
            // Directory where upload file is located.
            factory.setRepository(new File(WebCoreConstants.DEFAULT_TEMPFILE_LOCATION));
            
            final ServletFileUpload upload = new ServletFileUpload(factory);
            upload.setFileSizeMax(maxSize);
            
            // Parses the request's content to extract file data and login information
            final List<FileItem> formItems = upload.parseRequest(request);
            
            // Creates and authenticates user
            final UserAccount userAccount = authenticate(request, response, formItems);
            if (userAccount == null) {
                LOG.error("+++ User does not exist +++");
                return;
            }
            
            final Customer customer = userAccount.getCustomer();
            
            // check if the PointOfSale exists for the given commAddress
            PointOfSale pointOfSale = null;
            final String serialNum = request.getParameter(COMMADDRESS_PARAMETER);
            
            if (!checkForInvalidCharacters(COMMADDRESS_PARAMETER, serialNum)) {
                return;
            }
            
            if (serialNum != null && serialNum.length() == WebCoreConstants.LENGTH_PS_SERIAL_NUMBER) {
                pointOfSale = super.getPointOfSaleService().findTxablePointOfSaleBySerialNumber(serialNum);
            }
            
            if ((pointOfSale == null) || pointOfSale.getPosStatus().isIsLocked() || !customer.getId().equals(pointOfSale.getCustomer().getId())) {
                printWriter.println(BO2_CODE_PAYMENTSTATION_NOT_EXIST);
                if (LOG.isInfoEnabled()) {
                    LOG.info("Returning 'ps does not exist' PSQueryResponse");
                }
            } else {
                printWriter.println(BO2_CODE_PAYMENTSTATION_EXIST);
                if (LOG.isInfoEnabled()) {
                    LOG.info("Returning 'ps exists' PSQueryResponse");
                }
            }
        } catch (FileUploadException fue) {
            LOG.error("Cannot upload file, ", fue);
            return;
        } finally {
            if (printWriter != null) {
                printWriter.close();
            }
        }
        
    }
    
    private void processUpload(final String type, final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        if (type.equals(LOT_SETTING)) {
            uploadPaystationSetting(request, response);
        } else if (type.equals(FILE_UPLOAD)) {
            uploadCardTransaction(request, response);
        } else if (type.equals(PAYMENTSTATION_QUERY)) {
            handlePaymentstationQuery(request, response);
        } else if (type.equals(OFFLINE_TRANSACTION)) {
            uploadXMLTransactions(request, response);
        }
    }
    
    /**
     * determine if the system supports the upload type
     * 
     * @param type
     * @return
     */
    private boolean isUploadTypeSupported(final String type) {
        boolean supported = false;
        
        if (type != null) {
            // currently only 4 types are supported
            // if need to support more types, add it here
            supported = type.equals(LOT_SETTING) || type.equals(FILE_UPLOAD) || type.equals(PAYMENTSTATION_QUERY) || type.equals(OFFLINE_TRANSACTION);
        }
        
        return supported;
    }
    
    /**
     * Authenticate the request by checking the user credentials
     * 
     * @param request
     * @param response
     * 
     * @return
     * @throws IOException
     */
    private UserAccount authenticate(final HttpServletRequest request, final HttpServletResponse response, final List<FileItem> fileItems)
        throws IOException {
        final PrintWriter printWriter = response.getWriter();
        
        try {
            // get login info
            String customerName = null;
            String userName = null;
            String password = null;
            
            for (int i = 0; i < fileItems.size(); i++) {
                final FileItem fileItem = fileItems.get(i);
                if (CUSTOMER_NAME_PARAMETER.equals(fileItem.getFieldName())) {
                    customerName = fileItem.getString();
                    fileItems.remove(fileItem);
                    i = -1;
                }
                if (USER_NAME_PARAMETER.equals(fileItem.getFieldName())) {
                    userName = fileItem.getString();
                    fileItems.remove(fileItem);
                    i = -1;
                }
                if (PASSWORD_PARAMETER_ENCODED.equals(fileItem.getFieldName())) {
                    password = fileItem.getString();
                    fileItems.remove(fileItem);
                    i = -1;
                }
            }
            
            if (!checkForInvalidCharacters(CUSTOMER_NAME_PARAMETER, customerName) || !checkForInvalidCharacters(USER_NAME_PARAMETER, userName)) {
                return null;
            }
            
            // Authenticate login info
            UserAccount userAccount = super.authenticateRequest(customerName, userName, password);
            userAccount = switchToAlias(userAccount, customerName);
            if (userAccount == null) {
                LOG.error("+++ Invalid Login +++");
                printWriter.println(BO2_CODE_INVALID_LOGIN);
                
                return null;
            } else if (!checkServiceAggrementSigned(super.getCustomerId(userAccount.getCustomer()))) {
                LOG.error("+++ Service Agreement is not accepted +++");
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                
                return null;
            }
            
            LOG.info("Authenticated customer/user: " + customerName + StandardConstants.SEPARATOR_URL + userName);
            
            return userAccount;
        } catch (Exception e) {
            printWriter.println(BO2_CODE_INTERNAL_ERROR);
            LOG.error("Unable to authenticate user due to internal error", e);
        }
        return null;
    }
    
    private boolean checkForInvalidCharacters(final String paramName, final String paramValue) {
        if (paramValue == null || !DigitalAPIUtil.validateString(paramValue)) {
            LOG.error("Parameter " + paramName + " have invalid character (" + paramValue + ")");
            return false;
        }
        return true;
    }
    
    /**
     * Only can attach one zip file
     * 
     * @return
     */
    private FileItem getUploadFile(final List<FileItem> formItems) {
        FileItem item;
        final Iterator<FileItem> iter = formItems.iterator();
        while (iter.hasNext()) {
            item = iter.next();
            if (item.getContentType().toLowerCase().startsWith("application")) {
                return item;
            } else if (item.getContentType().toLowerCase().contains("text")) {
                return item;
            }
        }
        return null;
    }
    
    private boolean isTotalFileInZipFile(final String zipFileFullName, final int totalFile) {
        ZipFile zipFile = null;
        boolean result = true;
        try {
            final File file = new File(zipFileFullName);
            zipFile = new ZipFile(file);
            if (zipFile.size() != totalFile) {
                result = false;
            }
            
        } catch (Exception e) {
            result = false;
        } finally {
            if (zipFile != null) {
                try {
                    zipFile.close();
                } catch (Exception e) {
                }
            }
        }
        return result;
    }
    
    /*
     * Query the database to verify if the incoming transaction file has been processed successfully.
     * If it's a new file, return false.
     * if it's a duplicated file, update TransactionFileUpload table, NoOfUploadAttempt column by one.
     * @param customer Customer for file upload
     * @param fullPathFileName Transaction file has been uploaded.
     * @return boolean true if the transaction file has been uploaded and processed successful.
     * false if it's a new file.
     */
    private boolean updateDuplicatedTransactionFile(final Customer customer, final String fullPathFileName) {
        String checksum = null;
        try {
            checksum = CardProcessingUtil.createMD5Checksum(fullPathFileName);
        } catch (IOException e) {
            LOG.error(fullPathFileName + " causes IO Exception.", e);
        } catch (NoSuchAlgorithmException nsae) {
            LOG.error("MD5 cannot be recognized as a MessageDigest algorithm.", nsae);
        }
        final TransactionFileUpload fileUp = super.getTransactionFileUploadService().findTransactionFileUpload(customer, fullPathFileName, checksum);
        
        // Not exist in TransactionFileUpload table, it's a new file upload.
        if (fileUp == null) {
            return false;
        } else {
            int num = fileUp.getNoOfUploadAttempt();
            fileUp.setNoOfUploadAttempt(++num);
            super.getTransactionFileUploadService().updateTransactionFileUpload(fileUp);
        }
        return true;
    }
    
    private class UploadReturn {
        private UserAccount userAccount;
        private String fullFilePath;
        
        public UserAccount getUserAccount() {
            return this.userAccount;
        }
        
        public void setUserAccount(final UserAccount userAccount) {
            this.userAccount = userAccount;
        }
        
        public String getFullFilePath() {
            return this.fullFilePath;
        }
        
        public void setFullFilePath(final String fullFilePath) {
            this.fullFilePath = fullFilePath;
        }
    }
}
