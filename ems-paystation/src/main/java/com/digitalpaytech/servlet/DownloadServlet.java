package com.digitalpaytech.servlet;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.digitalpaytech.client.FacilitiesManagementClient;
import com.digitalpaytech.client.dto.KeyPackage;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.HashAlgorithm;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.crypto.CryptoConstants;

import io.netty.buffer.ByteBuf;
import rx.Observable;

@SuppressWarnings({ "checkstyle:illegalcatch", "PMD.ExcessiveImports", "PMD.GodClass" })
public class DownloadServlet extends PosBaseServlet {
    
    public static final byte[] FILE_HEADER_RECORD_TYPE = { (byte) 0x93, (byte) 0xEF, (byte) 0x08, (byte) 0xAE };
    
    public static final byte[] PUBLIC_KEY_RECORD_SHA256_TYPE = { (byte) 0x00, (byte) 0x00, (byte) 0x20, (byte) 0x01 };
    
    protected static final String REPONSE_TYPE_PKF = "application/octet-stream";
    protected static final String CONTENT_DISPOSITION = "Content-Disposition";
    protected static final String HEADER_CONTENT_DISPOSITION = "attachment;filename=";
    
    private static final long serialVersionUID = 5086744516416903601L;
    
    private static final Logger LOG = Logger.getLogger(DownloadServlet.class);
    private static final String PAYSTATION_COMM_ADDRESS = "commaddress";
    private static final String FILENAME_LOT_SETTINGS = "settings.zip";
    private static final String FILENAME_PUBLIC_KEY = "publicKey.txt";
    private static final String FILENAME_PUBLIC_KEY_PKF = "publickey.pkf";
    private static final String FILENAME_BAD_CREDITCARD = "badccdata.zip";
    private static final String FILENAME_BAD_SMARTCARD = "badscdata.zip";
    private static final String FILENAME_INVALID_FILE = "invalid";
    private static final String ZIP_ENTRY_BAD_SMARTCARD = "badSmartCardList.bo2";
    private static final String BOSS = "BOSS";
    
   
    
    private static final int INFO_FIELD_LENGTH = 4;
    
    private static final int ERR_UNABLE_TO_READ_DATA_STREAM_CODE = 8;
    private static final String ERR_UNABLE_TO_READ_DATA_STREAM_FMS = "Unable to read data stream from FMS for PS: {0}";
    
    private static final String ERR_AGREEMENT_NOT_ACCEPTED = "+++ Service Agreement is not accepted +++";
    
    private static final String ERRFMT_PUBLIC_KEY_NOT_FOUND = "Unable to find PublicKey for PS: {0}";
    private static final String ERRFMT_LOT_SETTINGS_NOT_FOUND = "Unable to find lot settings for PS: {0}";
    
    private static final String RETFMT_PUBLIC_KEY = "Returning PublicKeyDownloadResponse for PS: {0}";
    private static final String RETFMT_PS_SETTING = "Returning PaystationSettingDownloadResponse for PS: {0}";
    
    public void init(final ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        LOG.info("DownloadServlet Initialized.");
    }
    
    public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        doGet(request, response);
    }
    
    public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        // get the type of download requested
        final String type = request.getParameter(TYPE_PARAMETER);
        switch (request.getParameter(TYPE_PARAMETER)) {
            case TYPE_LOT_SETTING:
                downloadPaystationSettings(request, response);
                break;
            case TYPE_PUBLIC_KEY_REQUEST:
                downloadPublicKey(request, response);
                break;
            case TYPE_BAD_CREDITCARD:
                downloadBadCreditCard(request, response);
                break;
            case TYPE_BAD_SMARTCARD:
                downloadBadSmartCard(request, response);
                break;
            default:
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                LOG.error("Bad Request. No download handler found for type: " + type);
                break;
        }
    }
    
    private void downloadPaystationSettings(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        final String serialNum = request.getParameter(PAYSTATION_COMM_ADDRESS);
        if (LOG.isInfoEnabled()) {
            LOG.info("Processing PaystationSettingDownloadRequest for PS: " + serialNum);
        }
        ServletOutputStream op = null;
        try {
            op = response.getOutputStream();
            final PointOfSale pos = super.getPointOfSaleService().findPointOfSaleBySerialNumber(serialNum);
            if (pos == null || pos.getPosStatus().isIsLocked()) {
                op.write(PS2_CODE_LOCKED);
                return;
            }
            
            if (pos != null && !checkServiceAggrementSigned(super.getCustomerId(pos.getCustomer()))) {
                LOG.error(ERR_AGREEMENT_NOT_ACCEPTED);
                op.write(PS2_CODE_INVALID_LOGIN);
                return;
            }
            
            final String mimeType = URLConnection.getFileNameMap().getContentTypeFor(FILENAME_LOT_SETTINGS);
            response.setContentType(mimeType);
            response.setHeader(CONTENT_DISPOSITION, HEADER_CONTENT_DISPOSITION + FILENAME_LOT_SETTINGS);
            
            final Boolean result = streamFMSPayStationSettingsFile(op, pos);
            
            if (LOG.isInfoEnabled()) {
                LOG.info(MessageFormat.format(RETFMT_PS_SETTING, serialNum));
                LOG.info("Completed? " + result);
            }
        } catch (FileNotFoundException e) {
            op.write(PS2_CODE_DOWNLOAD_ERROR);
            LOG.error(MessageFormat.format(ERRFMT_LOT_SETTINGS_NOT_FOUND, serialNum), e);
        } catch (IOException | NullPointerException e) {
            op.write(ERR_UNABLE_TO_READ_DATA_STREAM_CODE);
            LOG.error(MessageFormat.format(ERR_UNABLE_TO_READ_DATA_STREAM_FMS, serialNum));
        } catch (Exception e) {
            op.write(PS2_CODE_INTERNAL_ERROR);
            LOG.error(MessageFormat.format(ERRFMT_LOT_SETTINGS_NOT_FOUND, serialNum), e);
        } finally {
            if (op != null) {
                op.close();
            }
        }
    }
    
    public final void downloadPublicKey(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        
        final WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
        final String serialNumber = request.getParameter(PAYSTATION_COMM_ADDRESS);
        
        if (LOG.isInfoEnabled()) {
            LOG.info("Processing PublicKeyDownloadRequest for PointOfSale serialNumber: " + serialNumber);
        }
        
        final String mimeType = URLConnection.getFileNameMap().getContentTypeFor(FILENAME_PUBLIC_KEY);
        response.setContentType(mimeType);
        response.setHeader(CONTENT_DISPOSITION, HEADER_CONTENT_DISPOSITION + FILENAME_PUBLIC_KEY);
        final ServletOutputStream op = response.getOutputStream();
        
        try {
            if (StringUtils.isBlank(serialNumber)) {
                return;
            }
            
            int posCheck = 0;
            // If not a BOSS request, check PS status
            if (!BOSS.equalsIgnoreCase(serialNumber)) {
                posCheck = super.processPOSRetrieval(serialNumber);
            }
            
            if (posCheck != 0) {
                if (posCheck == PS2_CODE_AGGREMENT_NOT_SIGNED) {
                    posCheck = PS2_CODE_INVALID_LOGIN;
                    LOG.error(ERR_AGREEMENT_NOT_ACCEPTED);
                }
                response.setHeader(CONTENT_DISPOSITION, HEADER_CONTENT_DISPOSITION + FILENAME_INVALID_FILE);
                op.write(posCheck);
                return;
            }
            
            // All customers have been migrated to Link.
            /*
             * TODO: Check IRIS-13508 description and we should be able to remove variables
             * (e.g. switchLinkCryptoFlag) and POSServiceState table columns.
             */
            final boolean switchLinkCryptoFlag = true;
            
            /*
             * What the algorithm type is will depend on which crypto is used
             */
            final Integer algTypeId;
            final String algTypeIdStr = request.getParameter(CryptoConstants.ALGORITHM_TYPE_ID_PARAMETER);
            if (algTypeIdStr != null && !WebCoreUtil.checkIfAllDigits(algTypeIdStr)) {
                algTypeId = 0;
            } else {
                final Integer algTypeIdObj = super.getAlgorithmTypeId(algTypeIdStr, serialNumber, switchLinkCryptoFlag);
                if (algTypeIdObj == null) {
                    algTypeId = 0;
                } else {
                    algTypeId = algTypeIdObj;
                }
            }
            if (algTypeId == 0) {
                op.write(String.valueOf(PS2_CODE_INVALID_ALGORITHM_TYPE).getBytes());
                response.setStatus(WebCoreConstants.HTTP_STATUS_CODE_404);
                response.setHeader(CONTENT_DISPOSITION, HEADER_CONTENT_DISPOSITION + FILENAME_INVALID_FILE);
                LOG.error("Unable to find HashAlgorithmType record for PS: " + serialNumber);
                return;
            }
            KeyPackage keyPackage = null;
            try {
                if (!BOSS.equalsIgnoreCase(serialNumber)) {
                    keyPackage =
                            super.getCryptoService().retrieveKey(super.getCryptoService().currentExternalKey(switchLinkCryptoFlag, serialNumber), 
                                                                 HashAlgorithm.resolveById(algTypeId));
                } else {
                    keyPackage =
                            super.getCryptoService().retrieveKey(super.getCryptoService().currentExternalKey(), HashAlgorithm.resolveById(algTypeId));
                }
            } catch (CryptoException cre) {
                LOG.error("Failed to retrieve Public Key", cre);
            }
            
            String publicKey = null;
            if (keyPackage != null) {
                publicKey = keyPackage.getSignatureSource() + "<Signature>" + keyPackage.getSignature() + "</Signature>";
            } else {
                op.write(PS2_CODE_INTERNAL_ERROR);
                response.setStatus(WebCoreConstants.HTTP_STATUS_CODE_500);
                response.setHeader(CONTENT_DISPOSITION, HEADER_CONTENT_DISPOSITION + FILENAME_INVALID_FILE);
                LOG.error(MessageFormat.format(ERRFMT_PUBLIC_KEY_NOT_FOUND, serialNumber));
                
                return;
            }
            
            switch (algTypeId) {
                
                case (int) CryptoConstants.HASH_ALGORITHM_TYPE_SHA1:
                    response.setContentLength(publicKey.length());
                    op.write(publicKey.getBytes());
                    
                    LOG.info(MessageFormat.format(RETFMT_PUBLIC_KEY, serialNumber));
                    break;
                
                case (int) CryptoConstants.HASH_ALGORITHM_TYPE_SHA256:
                case (int) CryptoConstants.HASH_ALGORITHM_TYPE_SHA256_IRIS:
                    returnPublicKey(PUBLIC_KEY_RECORD_SHA256_TYPE, response, keyPackage);
                    LOG.info(MessageFormat.format(RETFMT_PUBLIC_KEY, serialNumber));
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            op.write(PS2_CODE_INTERNAL_ERROR);
            response.setStatus(WebCoreConstants.HTTP_STATUS_CODE_500);
            LOG.error(MessageFormat.format(ERRFMT_PUBLIC_KEY_NOT_FOUND, serialNumber), e);
        } finally {
            if (op != null) {
                op.close();
            }
        }
        
    }
    
    /**
     * 
     * This Method returns the public key For all algorithms except SHA1WithRSA.
     * The file format returned is PKF. Please refer to pay station
     * documentation on RDF format for PKF file for more information.
     * 
     * 
     * @param publicKeyRecordType
     *            This byte array determines the types of the key been sent back
     * @param response
     * @param algTypeId
     * @throws IOException
     * @throws CryptoException
     */
    private void returnPublicKey(final byte[] publicKeyRecordType, final HttpServletResponse response, final KeyPackage keyPackage)
        throws IOException, CryptoException {
        
        response.setContentType(REPONSE_TYPE_PKF);
        response.setHeader(CONTENT_DISPOSITION, HEADER_CONTENT_DISPOSITION + FILENAME_PUBLIC_KEY_PKF);
        final ServletOutputStream outputStream = response.getOutputStream();
        
        outputStream.write(FILE_HEADER_RECORD_TYPE);
        outputStream.write(publicKeyRecordType);
        
        final byte[] publicKey = keyPackage.getSignatureSource().getBytes(Charset.defaultCharset());
        
        final byte[] signature = keyPackage.getSignature().getBytes(Charset.defaultCharset());
        
        final byte[] signatureLength = ByteBuffer.allocate(INFO_FIELD_LENGTH).putInt(Base64.decodeBase64(signature).length).array();
        
        final byte[] recordZeroDataLength =
                ByteBuffer.allocate(INFO_FIELD_LENGTH).putInt(Base64.decodeBase64(signature).length + publicKey.length + INFO_FIELD_LENGTH).array();
        
        outputStream.write(recordZeroDataLength);
        outputStream.write(signatureLength);
        outputStream.write(Base64.decodeBase64(signature));
        outputStream.write(publicKey);
        
    }
    
    private void downloadBadCreditCard(final HttpServletRequest request, final HttpServletResponse response) {
        final String customerName = request.getParameter(CUSTOMER_NAME_PARAMETER);
        final String userName = request.getParameter(USER_NAME_PARAMETER);
        final String password = request.getParameter(PASSWORD_PARAMETER_ENCODED);
        final UserAccount userAccount = super.authenticateRequest(customerName, userName, password);
        
        response.setHeader(CONTENT_DISPOSITION, HEADER_CONTENT_DISPOSITION + FILENAME_BAD_CREDITCARD);
        response.setContentType(URLConnection.getFileNameMap().getContentTypeFor(FILENAME_BAD_CREDITCARD));
        
        ServletOutputStream out = null;
        try {
            out = response.getOutputStream();
            if (verifyRequest(out, userAccount)) {
                downloadLinkBadCards(out, response, userAccount, WebCoreConstants.CARD_TYPE_CREDIT_CARD);
            }
        } catch (IOException e) {
            LOG.error("Unable to write BadCard for credit cards file response", e);
            sendErrorToPS(out);
        } finally {
            try {
                if (out != null) {
                    out.flush();
                    out.close();
                }
            } catch (IOException e) {
                LOG.error("Error closing BadCard for credit cards Zip Stream", e);
            }
        }
    }
    
    private void downloadBadSmartCard(final HttpServletRequest request, final HttpServletResponse response) {
        final String customerName = request.getParameter(CUSTOMER_NAME_PARAMETER);
        final String userName = request.getParameter(USER_NAME_PARAMETER);
        final String password = request.getParameter(PASSWORD_PARAMETER_ENCODED);
        final UserAccount userAccount = super.authenticateRequest(customerName, userName, password);
        
        ServletOutputStream out = null;
        try {
            out = response.getOutputStream();
            if (verifyRequest(out, userAccount)) {
                downloadBadCard(userAccount, out, response,
                                super.getCardTypeService().getCardTypesMap().get(CardProcessingConstants.CARD_TYPE_SMART_CARD).getId());
                response.setHeader(CONTENT_DISPOSITION, HEADER_CONTENT_DISPOSITION + FILENAME_BAD_SMARTCARD);
                response.setContentType(URLConnection.getFileNameMap().getContentTypeFor(FILENAME_BAD_SMARTCARD));
            }
        } catch (IOException e) {
            LOG.error("Unable to write BadCard for smart cards file response", e);
            sendErrorToPS(out);
        } finally {
            try {
                if (out != null) {
                    out.flush();
                    out.close();
                }
            } catch (IOException e) {
                LOG.error("Error closing BadCard for smart cards Zip Stream", e);
            }
        }
    }
    
    private void downloadLinkBadCards(final ServletOutputStream op, final HttpServletResponse response, final UserAccount userAccount,
        final int cardType) {
        
        final Customer customer = userAccount.getCustomer();
        
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            super.getLinkBadCardService().buildBadCardCreditCardFile(baos, customer, cardType);
            response.setContentLength(baos.size());
            op.write(baos.toByteArray());
            
        } catch (NumberFormatException nfe) {
            LOG.error(String.format("Failed sending request to Bad Card Service", customer.getId()), nfe);
            sendErrorToPS(op);
        } catch (IOException e) {
            LOG.error(e);
            sendErrorToPS(op);
        }
    }
    
    private void downloadBadCard(final UserAccount userAccount, final ServletOutputStream op, final HttpServletResponse response,
        final int cardTypeId) {
        
        FileOutputStream fos = null;
        ZipOutputStream zipfos = null;
        try {
            // Generate BadCard file
            final String badccDir = WebCoreConstants.DEFAULT_TEMPFILE_LOCATION;
            
            final File tempDir = new File(badccDir);
            if (!tempDir.exists()) {
                tempDir.mkdirs();
            }
            final String fileName = getBadCardFileName(userAccount, new Date());
            
            final String fullBadCCNameZip = badccDir + "\\" + fileName + ".zip";
            final File badCCFile = new File(fullBadCCNameZip);
            if (badCCFile.exists()) {
                System.gc();
                final boolean isDeleted = WebCoreUtil.secureDelete(badCCFile);
                if (!isDeleted) {
                    op.write(PS2_CODE_INTERNAL_ERROR);
                    LOG.error("Download BadCCard List failure, can't delete old file " + fullBadCCNameZip);
                    return;
                }
            }
            fos = new FileOutputStream(fullBadCCNameZip);
            zipfos = new ZipOutputStream(fos);
            zipfos.putNextEntry(new ZipEntry(ZIP_ENTRY_BAD_SMARTCARD));
            
            // Get cardservice beans
            super.getCustomerCardTypeService().generateBadCardList(userAccount.getCustomer().getId(), cardTypeId, zipfos);
            
            zipfos.close();
            fos.close();
            
            // Attach the file to response
            final File file = new File(fullBadCCNameZip);
            
            response.setContentLength((int) file.length());
            
            final byte[] fileContent = FileUtils.readFileToByteArray(file);
            
            op.write(fileContent);
            System.gc();
            
            // Remove the temporary file
            WebCoreUtil.secureDelete(file);
            
        } catch (Exception ex) {
            LOG.error("Download BadCCard List failure", ex);
            sendErrorToPS(op);
        } finally {
            try {
                if (op != null) {
                    op.flush();
                    op.close();
                }
                if (fos != null) {
                    fos.close();
                }
                if (zipfos != null) {
                    zipfos.close();
                }
            } catch (Exception e) {
            }
        }
    }
    
    private boolean verifyRequest(final ServletOutputStream op, final UserAccount userAccount) throws IOException {
        if (userAccount == null) {
            op.write(PS2_CODE_INVALID_LOGIN);
            return false;
        }
        if (!checkServiceAggrementSigned(super.getCustomerId(userAccount.getCustomer()))) {
            LOG.error(ERR_AGREEMENT_NOT_ACCEPTED);
            op.write(PS2_CODE_INVALID_LOGIN);
            return false;
        }
        
        if (!enforceFeatureSubscriptions(userAccount, WebCoreConstants.SUBSCRIPTION_TYPE_REALTIME_CC_PROCESSING,
                                         WebCoreConstants.SUBSCRIPTION_TYPE_REALTIME_CAMPUS_CARD_PROCESSING,
                                         WebCoreConstants.SUBSCRIPTION_TYPE_BATCH_CC_PROCESSING)
            || !enforceFeaturePermissions(userAccount, WebSecurityConstants.PERMISSION_VIEW_BANNED_CARDS,
                                          WebSecurityConstants.PERMISSION_MANAGE_BANNED_CARDS)) {
            op.write(PS2_CODE_FEATURE_NOT_ENABLED);
            return false;
        }
        return true;
    }
    
    private void sendErrorToPS(final ServletOutputStream op) {
        if (op != null) {
            try {
                op.write(PS2_CODE_INTERNAL_ERROR);
            } catch (IOException e) {
                LOG.error("Error returning error message to PS", e);
            }
        }
    }
    
    private String getBadCardFileName(final UserAccount userAccount, final Date today) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append(userAccount.getCustomer().getId()).append("_").append(new SimpleDateFormat("yyyyMMdd").format(today));
        return bdr.toString();
    }
    
    private Boolean streamFMSPayStationSettingsFile(final ServletOutputStream op, final PointOfSale pos) throws IOException {
        final FacilitiesManagementClient fmClient = super.getClientFactory().from(FacilitiesManagementClient.class);
        final Observable<ByteBuf> observable = fmClient.downloadPaystationSettings(String.valueOf(pos.getCustomer().getId()), pos.getSerialNumber()).observe();
        final ServletUtils servletUtils = new ServletUtils();
        return servletUtils.streamFile(observable, op);
    }
    
    private PosServiceState findPosServiceStateBySerialNumber(final String serialNumber) throws CryptoException {
        final PointOfSale pos = super.getPointOfSaleService().findPointOfSaleBySerialNumber(serialNumber);
        if (pos == null) {
            throw new CryptoException("+++ +++ Cannot find PointOfSale by serialNumber: " + serialNumber);
        }
        return super.getPosServiceStateService().findPosServiceStateById(pos.getId(), true);
    }
}
