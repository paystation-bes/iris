package com.digitalpaytech.servlet;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.Instant;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.digitalpaytech.client.LinkCryptoClient;
import com.digitalpaytech.client.dto.MultipartFileImpl;
import com.digitalpaytech.clientcommunication.dto.MultipartFileWrapper;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.RootCertificateFile;
import com.digitalpaytech.util.crypto.CryptoConstants;
import com.netflix.hystrix.exception.HystrixRuntimeException;


public class CertificateServlet extends DownloadServlet {
    
    private static final long serialVersionUID = 2063740924667706062L;
    private static final Logger LOG = Logger.getLogger(CertificateServlet.class);
    private static final String MULTIPART_FILE = "multipartFile";
    private static final byte[] DATA_TYPE_ID = { (byte) 0x00, (byte) 0x00, (byte) 0x80, (byte) 0x01 };
    
    private String rootCertificateDirectory;
    private String rootCertificateZipFilePathLinux;
    private String rootCertificateZipFilePathWinCE;
    private LinkCryptoClient linkCryptoClient;
    
    @Override
    public void init(final ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        setUp();
        LOG.info("DownloadServlet Initialized.");
    }
    
    public void setUp() {
        this.rootCertificateDirectory = super.getEmsPropertiesService().getPropertyValue("RootCertificateDirectory");
        this.rootCertificateZipFilePathLinux = super.getEmsPropertiesService().getPropertyValue("RootCertificateZipFilePathLinux");
        this.rootCertificateZipFilePathWinCE = super.getEmsPropertiesService().getPropertyValue("RootCertificateZipFilePathWinCE");
        
        if (StringUtils.isBlank(this.rootCertificateDirectory) || StringUtils.isBlank(this.rootCertificateZipFilePathLinux)
            || StringUtils.isBlank(this.rootCertificateZipFilePathWinCE)) {
            LOG.error(String.format("Misconfigured Root Certificate File Paths\n[RootCertificateDirectory %s]"
                                    + "\n[RootCertificateZipFilePathLinux %s]\n[RootCertificateZipFilePathWinCE %s] ", this.rootCertificateDirectory,
                                    this.rootCertificateZipFilePathLinux, this.rootCertificateZipFilePathWinCE));
            
        }
        
        this.linkCryptoClient = super.getClientFactory().from(LinkCryptoClient.class);
    }
    
    public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        
        final String serialNumber = request.getParameter("commAddress");
        
        if (StringUtils.isBlank(serialNumber)) {
            LOG.warn("Received download request for certificates without paystation serial number");
            response.setStatus(HttpStatus.SC_BAD_REQUEST);
        } else {
            
            final PointOfSale pos = this.getPointOfSaleService().findPointOfSaleBySerialNumber(serialNumber);
            if (pos == null) {
                LOG.error("Received download request for non existant POS");
                response.setStatus(HttpStatus.SC_BAD_REQUEST);
            } else {
                try {
                    buildCertsFile(pos.isIsLinux(), response);
                } catch (HystrixRuntimeException hre) {
                    LOG.error("Unable to sign file for Root Certificate Download", hre);
                    response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
                }
            }
        }
    }
    
    public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        doGet(request, response);
    }
    
    private void buildCertsFile(final boolean isLinux, final HttpServletResponse response) throws IOException {
        
        response.setContentType(REPONSE_TYPE_PKF);
        response.setHeader(CONTENT_DISPOSITION, HEADER_CONTENT_DISPOSITION + "certs.rdf");
        
        final Path zippedFilePath = getFilePath(isLinux);
        
        try (ByteArrayOutputStream lastZippedFile = new ByteArrayOutputStream()) {
            
            if (!Files.exists(zippedFilePath)) {
                Files.createFile(zippedFilePath);
            }
            
            Files.copy(zippedFilePath, lastZippedFile);
            final ServletOutputStream responseOutputStream = response.getOutputStream();
            
            final RootCertificateFile lastFile = this.getRootCertificateFileService().findByMaxId();
            
            if (!returnCachedFile(isLinux, lastZippedFile, lastFile)) {
                buildNewFile(isLinux, zippedFilePath, responseOutputStream, lastFile);
            } else {
                lastZippedFile.writeTo(responseOutputStream);
            }
        }
    }
    
    private void buildNewFile(final boolean isLinux, final Path zippedFilePath, final ServletOutputStream responseOutputStream,
        final RootCertificateFile lastFile) throws IOException {
        
        final byte[] rawBytes = zipFile(this.rootCertificateDirectory);
        
        /*
         * 4 bytes : File ID header: 0x93EF08AE
         * ------------------------------------
         * Record 0 head
         * 4 bytes : Record Type CertificateFile (0x00008001)
         * 4 bytes : Record Length: Record 0 Data
         * ------------------------------------
         * Record 0 body
         * 4 bytes : Length of SHA-256 signature
         * sig len : SHA-256 signature
         * data len : file data
         */
        
        final byte[] signature = signFile(isLinux, rawBytes);
        
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            
            // 4 bytes  : File ID header: 0x93EF08AE
            baos.write(FILE_HEADER_RECORD_TYPE);
            
            // Record 0 head
            // 4 bytes  : Record Type CertificateFile (0x00008001)
            baos.write(DATA_TYPE_ID);
            
            // 4 bytes : Record Length: Record 0 Data
            baos.write(intToByteArray(Integer.BYTES + signature.length + rawBytes.length));
            
            // Record 0 body
            // 4 bytes  : Length of SHA-256 signature
            baos.write(intToByteArray(signature.length));
            
            // signature length  : SHA-256 signature
            baos.write(signature);
            
            // data length : file data
            baos.write(rawBytes);
            
            final byte[] bytes = baos.toByteArray();
            final ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
            
            Files.copy(bais, zippedFilePath, StandardCopyOption.REPLACE_EXISTING);
            
            buildNewRootCert(isLinux, bytes, lastFile);
            
            baos.writeTo(responseOutputStream);
        }
    }
    
    private boolean returnCachedFile(final boolean isLinux, final ByteArrayOutputStream lastZippedFile, final RootCertificateFile lastFile)
        throws IOException {
        if (lastFile == null) {
            return false;
        }
        
        final byte[] bytes = lastZippedFile.toByteArray();
        
        if (bytes.length < 1) {
            return false;
        }
        
        if (isLinux) {
            return !StringUtils.isBlank(lastFile.getLinuxHash()) && lastFile.getLinuxHash().equals(DigestUtils.md5Hex(bytes));
        } else {
            return !StringUtils.isBlank(lastFile.getWinCEHash()) && lastFile.getWinCEHash().equals(DigestUtils.md5Hex(bytes));
        }
        
    }
    
    private byte[] signFile(final boolean isLinux, final byte[] zipFile) throws IOException {
        
        final MultipartFileWrapper multipartFileWrapper = new MultipartFileWrapper(new MultipartFileImpl(MULTIPART_FILE, zipFile),
                Long.toHexString(Instant.now().toEpochMilli()), new ConcurrentHashMap<String, String>(), MULTIPART_FILE);
        
        final String response = this.linkCryptoClient.signFile(isLinux ? CryptoConstants.SHA_256 : CryptoConstants.SHA_256_IRIS, multipartFileWrapper)
                .execute().toString(Charset.defaultCharset());
        
        if (StringUtils.isBlank(response)) {
            final String errorMsg = "Failed signing zip file!";
            LOG.error(errorMsg);
            throw new IOException(errorMsg);
        }
        
        final Decoder decoder = Base64.getDecoder();
        return decoder.decode(response);
        
    }
    
    private byte[] zipFile(final String sourceDirectory) throws IOException {
        
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            
            /*
             * ZipOutputStream is placed in its own try-with-resources
             * because must be forced to close first to create non-corrupt .zip file
             */
            try (ZipOutputStream zippedOutPutStream = new ZipOutputStream(baos)) {
                final Path sourceFolderPath = Paths.get(sourceDirectory);
                Files.walk(sourceFolderPath).filter(path -> !Files.isDirectory(path)).forEach(path -> {
                    final ZipEntry zipEntry = new ZipEntry(sourceFolderPath.relativize(path).toString());
                    try {
                        zippedOutPutStream.putNextEntry(zipEntry);
                        Files.copy(path, zippedOutPutStream);
                        zippedOutPutStream.closeEntry();
                    } catch (IOException e) {
                        LOG.error(e);
                    }
                    
                });
            }
            
            return baos.toByteArray();
        }
        
    }
    
    private void buildNewRootCert(final boolean isLinux, final byte[] bytes, final RootCertificateFile lastFile) {
        
        final RootCertificateFile rootCertificateFile;
        if (lastFile == null) {
            rootCertificateFile = new RootCertificateFile();
            rootCertificateFile.setCreatedGMT(new Date());
        } else {
            rootCertificateFile = lastFile;
        }
        
        setHash(isLinux, bytes, rootCertificateFile);
        
        this.getRootCertificateFileService().saveOrUpdate(rootCertificateFile);
    }
    
    private void setHash(final boolean isLinux, final byte[] bytes, final RootCertificateFile rootCertificateFile) {
        if (isLinux) {
            rootCertificateFile.setLinuxHash(DigestUtils.md5Hex(bytes));
        } else {
            rootCertificateFile.setWinCEHash(DigestUtils.md5Hex(bytes));
        }
    }
    
    private Path getFilePath(final boolean isLinux) {
        if (isLinux) {
            return Paths.get(this.rootCertificateZipFilePathLinux);
        } else {
            return Paths.get(this.rootCertificateZipFilePathWinCE);
        }
        
    }
    
    private byte[] intToByteArray(final int num) {
        return ByteBuffer.allocate(Integer.BYTES).order(ByteOrder.BIG_ENDIAN).putInt(num).array();
    }
}
