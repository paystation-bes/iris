package com.digitalpaytech.servlet;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.digitalpaytech.cardprocessing.support.CardTransactionUploadHelper;
import com.digitalpaytech.cardprocessing.support.CardTransactionZipProcessor;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosStatus;
import com.digitalpaytech.domain.ServiceAgreement;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.crypto.HashAlgorithmData;
import com.digitalpaytech.ribbon.ClientFactory;
import com.digitalpaytech.rpc.support.threads.XMLTransactionThreadRunner;
import com.digitalpaytech.service.CardTypeService;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.HashAlgorithmTypeService;
import com.digitalpaytech.service.LinkBadCardService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.PaystationSettingService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.RootCertificateFileService;
import com.digitalpaytech.service.ServiceAgreementService;
import com.digitalpaytech.service.TransactionFileUploadService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.crypto.CryptoAlgorithmFactory;
import com.digitalpaytech.service.paystation.LinuxConfigurationFileService;
import com.digitalpaytech.service.paystation.PosServiceStateService;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebSecurityConstants;

public class PosBaseServlet extends HttpServlet {
    
    public static final String CUSTOMER_NAME_PARAMETER = "customerName";
    public static final String USER_NAME_PARAMETER = "userName";
    public static final String COMMADDRESS_PARAMETER = "commAddress";
    
    public static final String COMMADDRESS_BADCARDS = "BadCards";
    
    /**
     * Starting in BOSS 6.2.1, password parameter is base 64 encoded.
     */
    public static final String PASSWORD_PARAMETER_ENCODED = "passwordEnc";
    public static final String PASSWORD_ENCODING = "UTF-8";
    
    protected static final String TYPE_PARAMETER = "type";
    protected static final String TYPE_LOT_SETTING = "lotsetting";
    protected static final String TYPE_PUBLIC_KEY_REQUEST = "publicKey";
    protected static final String TYPE_BAD_CREDITCARD = "badcreditcard";
    protected static final String TYPE_BAD_SMARTCARD = "badsmartcard";
    protected static final String TYPE_LINUX_FILE_DOWNLOAD = "linuxFileDownload";
    
    // codes returned to BO2 depending on result.
    protected static final String BO2_CODE_SUCCESS = "88007";
    protected static final String BO2_CODE_INVALID_LOGIN = "99040";
    protected static final String BO2_CODE_INTERNAL_ERROR = "99041";
    protected static final String BO2_CODE_UPLOAD_ERROR = "99042";
    protected static final String BO2_CODE_UPLOAD_ERROR_INVALID_HASH = "99043";
    protected static final String BO2_CODE_UPLOAD_ERROR_INVALID_HEADERS = "99044";
    protected static final String BO2_CODE_FEATURE_NOT_ENABLED = "99060";
    protected static final String BO2_CODE_ACCOUNT_LOCKED = "99061";
    protected static final String BO2_CODE_PAYMENTSTATION_EXIST = "1";
    protected static final String BO2_CODE_PAYMENTSTATION_NOT_EXIST = "0";
    
    // codes returned to PS2 depending on result.
    protected static final int PS2_CODE_INVALID_LOGIN = 1;
    protected static final int PS2_CODE_INTERNAL_ERROR = 2;
    protected static final int PS2_CODE_DOWNLOAD_ERROR = 3;
    protected static final int PS2_CODE_FEATURE_NOT_ENABLED = 4;
    protected static final int PS2_CODE_LOCKED = 5;
    protected static final int PS2_CODE_AGGREMENT_NOT_SIGNED = 6;
    protected static final int PS2_CODE_INVALID_ALGORITHM_TYPE = 7;
    
    protected static final String ERR_AGREEMENT_NOT_ACCEPTED = "+++ Service Agreement is not accepted +++";
    protected static final String CONTENT_DISPOSITION = "Content-Disposition";
    protected static final String HEADER_CONTENT_DISPOSITION = "attachment;filename=";
    
    private static final String BOSS = "BOSS";
    private static final long serialVersionUID = -5828700557570666102L;
    
    private static final Logger LOG = Logger.getLogger(PosBaseServlet.class);
    
    private ServletConfig servletConfig;
    private EmsPropertiesService emsPropertiesService;
    private UserAccountService userAccountService;
    private PointOfSaleService pointOfSaleService;
    private ServiceAgreementService serviceAgreementService;
    private PaystationSettingService paystationSettingService;
    private AuthenticationManager authMgr;
    private CryptoService cryptoService;
    private XMLTransactionThreadRunner xmlTransactionThreadRunner;
    private CardTransactionZipProcessor cardTransactionZipProcessor;
    private CardTransactionUploadHelper cardTransactionUploadHelper;
    private CustomerCardTypeService customerCardTypeService;
    private CardTypeService cardTypeService;
    private CustomerService customerService;
    private CryptoAlgorithmFactory cryptoAlgorithmFactory;
    private HashAlgorithmTypeService hashAlgorithmTypeService;
    private TransactionFileUploadService transactionFileUploadService;
    private ClientFactory clientFactory;
    private PosServiceStateService posServiceStateService;
    private LinuxConfigurationFileService linuxConfigurationFileService;
    private MerchantAccountService merchantAccountService;
    private LinkBadCardService linkBadCardService;
    private RootCertificateFileService rootCertificateFileService;
    
    @SuppressWarnings("checkstyle:designforextension")
    public void init(final ServletConfig newServletConfig) throws ServletException {
        super.init(newServletConfig);
        this.servletConfig = newServletConfig;
        LOG.info("Initialized.");
        
        // Get spring beans
        final WebApplicationContext ctx = getWebApplicationContext();
        
        this.emsPropertiesService = (EmsPropertiesService) ctx.getBean("emsPropertiesService");
        this.userAccountService = (UserAccountService) ctx.getBean("userAccountService");
        this.authMgr = (AuthenticationManager) ctx.getBean("authenticationManager");
        this.pointOfSaleService = (PointOfSaleService) ctx.getBean("pointOfSaleService");
        this.serviceAgreementService = (ServiceAgreementService) ctx.getBean("serviceAgreementService");
        this.paystationSettingService = (PaystationSettingService) ctx.getBean("paystationSettingService");
        this.cryptoService = (CryptoService) ctx.getBean("cryptoService");
        this.xmlTransactionThreadRunner = (XMLTransactionThreadRunner) ctx.getBean("xmlTransactionThreadRunner");
        this.cardTransactionZipProcessor = (CardTransactionZipProcessor) ctx.getBean("cardTransactionZipProcessor");
        this.cardTransactionUploadHelper = (CardTransactionUploadHelper) ctx.getBean("cardTransactionUploadHelper");
        this.customerCardTypeService = (CustomerCardTypeService) ctx.getBean("customerCardTypeService");
        this.cardTypeService = (CardTypeService) ctx.getBean("cardTypeService");
        this.customerService = (CustomerService) ctx.getBean("customerService");
        this.cryptoAlgorithmFactory = (CryptoAlgorithmFactory) ctx.getBean("cryptoAlgorithmFactory");
        this.hashAlgorithmTypeService = (HashAlgorithmTypeService) ctx.getBean("hashAlgorithmTypeService");
        this.transactionFileUploadService = (TransactionFileUploadService) ctx.getBean("transactionFileUploadService");
        this.linuxConfigurationFileService = (LinuxConfigurationFileService) ctx.getBean("linuxConfigurationFileService");
        this.clientFactory = ctx.getBean(ClientFactory.class);
        this.setMerchantAccountService(ctx.getBean(MerchantAccountService.class));
        this.setLinkBadCardService(ctx.getBean(LinkBadCardService.class));
        this.setRootCertificateFileService(ctx.getBean(RootCertificateFileService.class));
        this.setPosServiceStateService(ctx.getBean(PosServiceStateService.class));
    }
    
    protected final WebApplicationContext getWebApplicationContext() {
        return WebApplicationContextUtils.getWebApplicationContext(getServletContext());
    }
    
    // This method required to return null UserAccount if there are any Exceptions occurred.
    @SuppressWarnings("checkstyle:illegalcatch")
    protected final UserAccount authenticateRequest(final String customerName, final String userName, final String password) {
        if (userName == null || password == null) {
            return null;
        }
        UserAccount userAccount = null;
        try {
            final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                    URLEncoder.encode(userName, WebSecurityConstants.URL_ENCODING_LATIN1), URLDecoder.decode(password, PASSWORD_ENCODING));
            this.authMgr.authenticate(token);
            userAccount = this.userAccountService.findUndeletedUserAccount(URLEncoder.encode(userName, WebSecurityConstants.URL_ENCODING_LATIN1));
            userAccount = switchToAlias(userAccount, customerName);
        } catch (Exception e) {
            LOG.error("Unable to authenticate userName: " + userName, e);
            return null;
        }
        
        return userAccount;
    }
    
    protected final UserAccount switchToAlias(final UserAccount mainAccount, final String childCustomerName) {
        UserAccount targetAccount = mainAccount;
        if ((mainAccount != null) && (childCustomerName != null) && (!childCustomerName.isEmpty()) && mainAccount.getCustomer().isIsParent()) {
            final String targetCustomerName = childCustomerName.toLowerCase();
            if (!targetCustomerName.equals(mainAccount.getCustomer().getName().toLowerCase())) {
                final Customer targetCustomer = this.userAccountService.findChildCustomer(mainAccount.getCustomer().getId(), targetCustomerName);
                if (targetCustomer == null) {
                    targetAccount = null;
                } else {
                    targetAccount =
                            this.userAccountService.findAliasUserAccountByUserAccountIdAndCustomerId(mainAccount.getId(), targetCustomer.getId());
                }
            }
        }
        
        return targetAccount;
    }
    
    protected final UserAccount findByName(final String userName) {
        return this.userAccountService.findUndeletedUserAccount(userName);
    }
    
    protected final boolean checkServiceAggrementSigned(final int customerId) {
        final ServiceAgreement agree = this.serviceAgreementService.findLatestServiceAgreementByCustomerId(customerId);
        if (agree != null) {
            final Customer customer = this.customerService.findCustomer(customerId);
            if (!customer.isIsMigrated()) {
                return false;
            }
            return true;
        }
        return false;
    }
    
    protected final int processPOSRetrieval(final String serialNumber) {
        final PointOfSale pos = this.pointOfSaleService.findPointOfSaleBySerialNumber(serialNumber);
        if (pos == null) {
            return PS2_CODE_INVALID_LOGIN;
        }
        final PosStatus posStatus = this.pointOfSaleService.findPointOfSaleStatusByPOSId(pos.getId(), true);
        if (posStatus == null) {
            return PS2_CODE_INVALID_LOGIN;
        }
        pos.setPosStatus(posStatus);
        if (!checkServiceAggrementSigned(getCustomerId(pos.getCustomer()))) {
            return PS2_CODE_AGGREMENT_NOT_SIGNED;
        }
        
        // Verify paystation
        if (pos == null || pos.getPosStatus().isIsLocked()) {
            return PS2_CODE_LOCKED;
        }
        return 0;
    }
    
    protected final Integer getCustomerId(final Customer customer) {
        final Integer id;
        if (customer.getParentCustomer() != null) {
            id = customer.getParentCustomer().getId();
        } else {
            id = customer.getId();
        }
        return id;
    }
    
    /**
     * Checks the permissions for a specific feature that a user has
     */
    protected final boolean enforceFeaturePermissions(final UserAccount user, final Integer... permissions) {
        final Set<Integer> list = new HashSet<Integer>(permissions.length * 2);
        Collections.addAll(list, permissions);
        
        final List<Permission> permissionList = getUserAccountService().findPermissions(user.getId());
        for (Permission permission : permissionList) {
            if (list.contains(permission.getId())) {
                return true;
            }
        }
        return false;
    }
    
    protected final boolean enforceFeatureSubscriptions(final UserAccount user, final Integer... subscriptions) {
        final Set<Integer> list = new HashSet<Integer>(subscriptions.length * 2);
        Collections.addAll(list, subscriptions);
        
        final List<CustomerSubscription> subscriptionsList = getCustomerService().findCustomerSubscriptions(user.getCustomer().getId());
        for (CustomerSubscription subscription : subscriptionsList) {
            if (list.contains(subscription.getSubscriptionType().getId())) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Iris 7.10 update: if it's a Linux PS, algorithmTypeId is 2. Otherwise algorithmTypeIdStr is 5 for Link Crypto, "SHA-245-Iris",
     * algorithmTypeIdStr is 2 for Scala Crypto.
     * 
     * @param request
     * @return Integer HashAlgorithmType table id. null will be returned if id is invalid.
     */
    protected final Integer getAlgorithmTypeId(final String algorithmTypeIdStr, final String serialNumber, final boolean isSwitchLinkCrypt) {
        final boolean linuxFlag;
        
        if (BOSS.equalsIgnoreCase(serialNumber)) {
            linuxFlag = false;
        } else {
            linuxFlag = this.pointOfSaleService.findPointOfSaleBySerialNumber(serialNumber).isIsLinux();
        }
        
        Integer algorithmTypeId = null;
        if (StringUtils.isBlank(algorithmTypeIdStr)) {
            algorithmTypeId = Integer.valueOf(EmsPropertiesService.DEFAULT_HASH_ALGORITHM_TYPE_SHA_1_ID);
        
        } else if (linuxFlag) {
            // For Linux it is always 2 because it always goes to Link, Linux pay stations cannot get keys from Scala
            algorithmTypeId = StandardConstants.CONSTANT_2;
            
        } else if (algorithmTypeIdStr.equals(StandardConstants.STRING_TWO)) {
            if (isSwitchLinkCrypt) {
                // Any call that goes to Link for SHA-256 should use 5 = SHA-256-Iris
                algorithmTypeId = StandardConstants.CONSTANT_5;
            
            } else {
                // Any call that goes to Scala for SHA-256 should use 2
                algorithmTypeId = StandardConstants.CONSTANT_2;
            }
        } else {
            // LinuxFlag is false and algorithmTypeIdStr is 1.
            algorithmTypeId = Integer.parseInt(algorithmTypeIdStr);
        }
        final HashAlgorithmData data = this.getHashAlgorithmTypeService().findForMessageDigest(algorithmTypeId);
        if (data == null || data.isForSigning() || data.isDeleted()) {
            return null;
        }
        return Integer.valueOf(data.getHashAlgorithmType().getId());
    }
    
    public final PointOfSaleService getPointOfSaleService() {
        return this.pointOfSaleService;
    }
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final PaystationSettingService getPaystationSettingService() {
        return this.paystationSettingService;
    }
    
    public final CryptoService getCryptoService() {
        return this.cryptoService;
    }
    
    public final EmsPropertiesService getEmsPropertiesService() {
        return this.emsPropertiesService;
    }
    
    public final ServletConfig getServletConfig() {
        return this.servletConfig;
    }
    
    public final XMLTransactionThreadRunner getXmlTransactionThreadRunner() {
        return this.xmlTransactionThreadRunner;
    }
    
    public final CardTransactionZipProcessor getCardTransactionZipProcessor() {
        return this.cardTransactionZipProcessor;
    }
    
    public final CardTransactionUploadHelper getCardTransactionUploadHelper() {
        return this.cardTransactionUploadHelper;
    }
    
    public final CustomerCardTypeService getCustomerCardTypeService() {
        return this.customerCardTypeService;
    }
    
    public final CardTypeService getCardTypeService() {
        return this.cardTypeService;
    }
    
    public final UserAccountService getUserAccountService() {
        return this.userAccountService;
    }
    
    public final CustomerService getCustomerService() {
        return this.customerService;
    }
    
    public final void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }
    
    public final CryptoAlgorithmFactory getCryptoAlgorithmFactory() {
        return this.cryptoAlgorithmFactory;
    }
    
    public final HashAlgorithmTypeService getHashAlgorithmTypeService() {
        return this.hashAlgorithmTypeService;
    }
    
    public final void setHashAlgorithmTypeService(final HashAlgorithmTypeService hashAlgorithmTypeService) {
        this.hashAlgorithmTypeService = hashAlgorithmTypeService;
    }
    
    public final TransactionFileUploadService getTransactionFileUploadService() {
        return this.transactionFileUploadService;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final AuthenticationManager getAuthMgr() {
        return this.authMgr;
    }
    
    public final void setAuthMgr(final AuthenticationManager authMgr) {
        this.authMgr = authMgr;
    }
    
    public final void setUserAccountService(final UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }
    
    public final void setTransactionFileUploadService(final TransactionFileUploadService transactionFileUploadService) {
        this.transactionFileUploadService = transactionFileUploadService;
    }
    
    public final ClientFactory getClientFactory() {
        return this.clientFactory;
    }
    
    public final void setServiceAgreementService(final ServiceAgreementService serviceAgreementService) {
        this.serviceAgreementService = serviceAgreementService;
    }
    
    public final PosServiceStateService getPosServiceStateService() {
        return this.posServiceStateService;
    }
    
    public final void setPosServiceStateService(final PosServiceStateService posServiceStateService) {
        this.posServiceStateService = posServiceStateService;
    }
    
    public final LinuxConfigurationFileService getLinuxConfigurationFileService() {
        return this.linuxConfigurationFileService;
    }
    
    public final void setLinuxConfigurationFileService(final LinuxConfigurationFileService linuxConfigurationFileService) {
        this.linuxConfigurationFileService = linuxConfigurationFileService;
    }
    
    public final MerchantAccountService getMerchantAccountService() {
        return this.merchantAccountService;
    }
    
    public final void setMerchantAccountService(final MerchantAccountService merchantAccountService) {
        this.merchantAccountService = merchantAccountService;
    }
    
    public final LinkBadCardService getLinkBadCardService() {
        return this.linkBadCardService;
    }
    
    public final void setLinkBadCardService(final LinkBadCardService linkBadCardService) {
        this.linkBadCardService = linkBadCardService;
    }
    
    public RootCertificateFileService getRootCertificateFileService() {
        return this.rootCertificateFileService;
    }
    
    public void setRootCertificateFileService(final RootCertificateFileService rootCertificateFileService) {
        this.rootCertificateFileService = rootCertificateFileService;
    }
}
