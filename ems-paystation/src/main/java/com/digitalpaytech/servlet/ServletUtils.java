package com.digitalpaytech.servlet;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import io.netty.buffer.ByteBuf;
import rx.Observable;

public class ServletUtils {
    private static final Logger LOG = Logger.getLogger(ServletUtils.class);
    
    //checkstyle doesn't understand lambdas
    @SuppressWarnings({ "checkstyle:indentation" })
    public final Boolean streamFile(final Observable<ByteBuf> observable, final ServletOutputStream op) {
        final CompletableFuture<Boolean> future = new CompletableFuture<Boolean>();
        observable.subscribe(
                             // onNext
                             byteBuf -> {
                                 final byte[] b;
                                 if (byteBuf.hasArray()) {
                                     b = byteBuf.array();
                                 } else {
                                     final int length = byteBuf.readableBytes();
                                     b = new byte[length];
                                     byteBuf.getBytes(byteBuf.readerIndex(), b);
                                 }
                                 try {
                                     op.write(b, 0, b.length);
                                 } catch (IOException ioe) {
                                     LOG.error(ioe);
                                     future.completeExceptionally(ioe);
                                 }
                             },
                             // onError
                             throwable -> {
                                 LOG.error(throwable);
                                 future.completeExceptionally(throwable);
                             },
                             // onCompleted
                             () -> {
                                 try {
                                     op.flush();
                                     future.complete(Boolean.TRUE);
                                 } catch (IOException ioe) {
                                     LOG.error("Cannot close ServletOutputStream in 'onCompleted', ", ioe);
                                     future.completeExceptionally(ioe);
                                 } finally {
                                     if (op != null) {
                                         try {
                                             op.close();
                                         } catch (IOException ioe) {
                                             LOG.error("Cannot close output stream, ", ioe);
                                         }
                                     }
                                 }
                             });
        boolean result = false;
        try {
            result = future.get();
        } catch (InterruptedException | ExecutionException e) {
            LOG.error("Unable to read data stream data", e);
            result = false;
        } 
        return result;
    }
    
    public final void sendErrorResponse(final int code, final String message, final HttpServletResponse response) {
        try {
            response.getWriter().println(message);
        } catch (IOException e) {
            LOG.error("Error writing message to response", e);
        } finally {
            response.setStatus(code);
        }
    }
}
