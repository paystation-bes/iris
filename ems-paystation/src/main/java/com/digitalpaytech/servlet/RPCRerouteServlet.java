package com.digitalpaytech.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.cxf.helpers.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.PointOfSaleService;

/**
 * This servlet class will reroute XML-RPC requests to either old version of EMS
 * or new version of EMS depending on customer's migration status.<br/>
 * During the migration time, the servlet will return the "EMSUnavailable"
 * status back to pay station by design.
 * 
 * @author Brian Kim
 * 
 */
public class RPCRerouteServlet extends HttpServlet {
	
	private static final long serialVersionUID = -5038767865245090416L;
	private static final Logger logger = Logger.getLogger(RPCRerouteServlet.class);
	private static final String URL_PREFIX = "XMLRPC_PS2";
	
	private PointOfSaleService pointOfSaleService;
	private EmsPropertiesService emsPropertiesService;
	
	/**
	 * initialize the some services.
	 */
	public void init() throws UnavailableException {
		try {
			logger.info("++++ Initializing RPCRerouteServlet... ++++");
			WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
			pointOfSaleService = (PointOfSaleService) ctx.getBean("pointOfSaleService");
			emsPropertiesService = (EmsPropertiesService) ctx.getBean("emsPropertiesService");
		} catch (Exception e) {
			throw new UnavailableException("Exception while initializing RPCRerouteServlet.");
		}
	}
	
	/**
	 * receive the XML-RPC from pay station and determine if the request should be rerouted.
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws UnavailableException {
		if (request.isSecure()) {
			try {
				/* get the pay station serial number from request in order to retrieve customer. */
				request = new DPTHttpServletRequestWrapper(request);
				ServletInputStream inputStream = request.getInputStream();
				String requestText = new String(IOUtils.readBytesFromStream(inputStream));
				String serialNumber = findPaystationCommAddressValue(requestText);
				if (StringUtils.isBlank(serialNumber)) {
					logger.warn("Couldn't find any serial number in the XML-RPC request, so exiting the process.");
					return;
				}
				
				PointOfSale pos = pointOfSaleService.findPointOfSaleBySerialNumber(serialNumber);
				if (pos == null) {
					logger.warn("Couldn't find pay station information with the request serial number, so exiting the process.");
					return;
				}
				
                if (!pos.getCustomer().isIsMigrated()) {
                    rerouteRequest(requestText, response);
                } else {
                    getServletContext().getNamedDispatcher("RPCConnectorServlet").forward(request, response);
                }

                //TODO Remove after confirmation 
//				if (migration == null) {
//					/* reroute request to old version of EMS because customer didn't request the final migration. */
//					rerouteRequest(requestText, response);
//				} else if (migration.isIsMigrationComplete()) {
//					/* always use new version of EMS because final migration successfully completed. */ 
//					getServletContext().getNamedDispatcher("RPCConnectorServlet").forward(request, response);
//				} else if (migration.isIsMigrationInProgress() || migration.isIsCustomerLocked()) {
//					/* request is rejected because the customer migration is in progress. */
//					response.setStatus(HttpStatus.SERVICE_UNAVAILABLE.value());
//					return;
//				} else {
//					/* reroute request to old version of EMS. */
//					rerouteRequest(requestText, response);
//				}
				
			} catch (IOException e) {
				logger.error("Unable to read XML message.", e);
			} catch (ServletException e) {
				logger.error("Unable to reroute request XML message.", e);
			}
		} else {
			logger.warn("XML-RPC request is not secure.");
		}
	}
	
	/**
	 * GET method is not supported so it will log if any GET request is coming.
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws UnavailableException {
		logger.warn("++++ GET method is not supported in this XML-RPC request. ++++");
	}
	
	/**
	 * This method will reroute the XML-RPC request to old EMS and return the
	 * response back to pay station.
	 * 
	 * @param requestText
	 *            String value of XML-RPC request
	 * @param response
	 *            HttpServletResponse object
	 * @throws DocumentException
	 * @throws IOException
	 */
	private void rerouteRequest(String requestText, HttpServletResponse response) throws IOException {
		
		HttpClient client = new DefaultHttpClient();
		String postUrl = emsPropertiesService.getPropertyValue(EmsPropertiesService.REROUTE_TO_EMS6_URL);
		
		if (postUrl == null) {
			logger.warn("++++ URL to reroute the XML-RPC request has not configured in EmsProperties table. ++++");
			return;
		}
		
		if (postUrl.endsWith("/")) {
			postUrl = new StringBuilder(postUrl).append(URL_PREFIX).toString();
		} else {
			postUrl = new StringBuilder(postUrl).append("/").append(URL_PREFIX).toString();
		}
		
		HttpPost httpPost = new HttpPost(postUrl);
		httpPost.addHeader("Content-Type", "application/xml");
		
		/* create your XML string entity. */
		StringEntity entity = new StringEntity(requestText);
		logger.debug(new StringBuilder("##### XML-RPC REQUEST: ").append(requestText).toString());
		
		/* set the XML string in the HttpPost. */
		httpPost.setEntity(entity);
		
		/* reroute the request and get the response. */
		HttpResponse res = client.execute(httpPost);
		InputStream in = res.getEntity().getContent();
		String responseText = new String(IOUtils.readBytesFromStream(in));
		logger.debug(new StringBuilder("##### XML-RPC RESPONSE: ").append(responseText).toString());
		
		/* write the response. */
		processResponse(responseText, response);	
	}
	
	/**
	 * This method writes XML-RPC response in HttpServletResponse object.
	 * 
	 * @param responseText
	 *            String value of XML-RPC response
	 * @param response
	 *            HttpServletResponse object
	 * @throws IOException
	 *             exception when it writes response text in output stream.
	 */
	private void processResponse(String responseText, HttpServletResponse response) throws IOException {
		
		OutputStream output_stream = null;
		
		response.setContentType("text/xml");
		response.setContentLength(responseText.getBytes().length);
		try {
			output_stream = response.getOutputStream();
			output_stream.write(responseText.getBytes());
		} catch (IOException e) {
			logger.error("Exception occurred during processing XML-RPC response.");
			throw e;
		} finally {
			if (output_stream != null) {
				output_stream.flush();
				output_stream.close();
			}
		}
	}
	
	/**
	 * This method finds the value of &lt;PaystationCommAddress&gt; element in
	 * the XML-RPC request.
	 * 
	 * @param requestText
	 *            String value of XML-RPC request
	 * @return String value of &lt;PaystationCommAddress&gt; element
	 */
	private String findPaystationCommAddressValue(String requestText) {
		String elementValue = "";
		String tagName = "PaystationCommAddress";
		int startIndex = -1;
		int endIndex = -1;

		startIndex = requestText.indexOf("&lt;" + tagName, startIndex);

		if (startIndex > -1) {
			startIndex = requestText.indexOf("&gt;", startIndex) + 4;

			/* This is a "<tagname/>" tag which indicates no data */
			if (requestText.charAt(startIndex - 2) != '/') {
				endIndex = requestText.indexOf("&lt;/" + tagName, startIndex);
				if (endIndex > startIndex) {
					elementValue = requestText.substring(startIndex, endIndex);
				}
			}
		}
		return elementValue.trim();
	}
}
