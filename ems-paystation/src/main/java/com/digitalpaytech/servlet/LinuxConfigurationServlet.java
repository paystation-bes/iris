package com.digitalpaytech.servlet;

import java.io.IOException;
import java.net.URLConnection;
import java.text.MessageFormat;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.log4j.Logger;
import org.apache.commons.lang.StringUtils;

import com.digitalpaytech.client.DcmsClient;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosServiceState;

import io.netty.buffer.ByteBuf;
import rx.Observable;

public class LinuxConfigurationServlet extends PosBaseServlet {
    
    private static final long serialVersionUID = 5827822500534390771L;
    private static final Logger LOG = Logger.getLogger(LinuxConfigurationServlet.class);
    
    private final ServletUtils servletUtils = new ServletUtils();
    
    public final void init(final ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        LOG.info("ServletConfig Initialized.");
    }
    
    public final void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        ServletOutputStream op = null;
        final String deviceId = request.getParameter("serialNumber");
        final String fileId = request.getParameter("fileId");
        final String fileName = request.getParameter("fileName");
        final String snapshotId = request.getParameter("configSnapshotId");
        
        if (LOG.isInfoEnabled()) {
            LOG.info(MessageFormat.format("Processing download file request for PS [{0}] FileId [{1}]", deviceId, fileId));
        }
        
        final PointOfSale pos = super.getPointOfSaleService().findPointOfSaleBySerialNumber(deviceId);
        try {
            
            if (pos == null) {
                this.servletUtils.sendErrorResponse(HttpStatus.SC_NOT_FOUND, "The Pay Station could not be found", response);
                return;
            }
            
            final PosServiceState pss = super.getPosServiceStateService().findPosServiceStateById(pos.getId(), false);
            
            if (pos.getPosStatus().isIsLocked()) {
                this.servletUtils.sendErrorResponse(HttpStatus.SC_FORBIDDEN, "The Pay Station is locked", response);
            } else if (!checkServiceAggrementSigned(super.getCustomerId(pos.getCustomer()))) {
                LOG.error(ERR_AGREEMENT_NOT_ACCEPTED);
                this.servletUtils.sendErrorResponse(HttpStatus.SC_FORBIDDEN, ERR_AGREEMENT_NOT_ACCEPTED, response);
            } else if (dcmsMessageInvalid(fileId, fileName, snapshotId, pss)) {
                final String err = MessageFormat.format("Unable to process message for deviceID {0}", deviceId);
                LOG.error(err);
                this.servletUtils.sendErrorResponse(HttpStatus.SC_BAD_REQUEST, err, response);
            } else if (snapshotId.equals(pss.getNewConfigurationId())
                       && this.getLinuxConfigurationFileService().fileExistsByFileIdAndSnapShotIdAndPointOfSaleId(fileId, pos.getId(), snapshotId)) {
                final String mimeType = URLConnection.getFileNameMap().getContentTypeFor(fileName);
                response.setContentType(mimeType);
                response.setHeader(CONTENT_DISPOSITION, HEADER_CONTENT_DISPOSITION + fileName);
                op = response.getOutputStream();
                final Boolean fu = streamLinuxPayStationSettingsFile(op, pos, snapshotId, fileId);
                
                if (!fu) {
                    final String unableToStreamData =
                            MessageFormat.format("Unable to read data stream data for PS [{0}] FileId [{1}]", deviceId, fileId);
                    LOG.error(unableToStreamData);
                    response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
                }
                LOG.info("Finished streaming file");
                
            } else {
                response.setStatus(HttpStatus.SC_NO_CONTENT);
            }
        } finally {
            if (op != null) {
                op.close();
            }
        }
    }
    
    private boolean dcmsMessageInvalid(final String fileId, final String fileName, final String configSnapshotId, final PosServiceState pss) {
        return StringUtils.isBlank(fileId) || StringUtils.isBlank(fileName) || StringUtils.isBlank(configSnapshotId) || pss == null;
    }
    
    private Boolean streamLinuxPayStationSettingsFile(final ServletOutputStream op, final PointOfSale pos, final String configSnapshotId,
        final String fileId) throws IOException {
        final DcmsClient dcmsClient = super.getClientFactory().from(DcmsClient.class);
        final Observable<ByteBuf> observable =
                dcmsClient.downloadConfiguration(pos.getSerialNumber(), pos.getCustomer().getUnifiId().intValue(), configSnapshotId, fileId).observe();
        return this.servletUtils.streamFile(observable, op);
    }
}
