package com.digitalpaytech.rpc.support.threads;

import java.io.File;
import com.digitalpaytech.util.WebCoreUtil;
import org.apache.log4j.Logger;

public class DeleteTempFileThread extends Thread {
    private static Logger logger = Logger.getLogger(DeleteTempFileThread.class);
    private String filepath;
    private int maxRetries = 50;
    
    public DeleteTempFileThread(String filepath) {
        this.filepath = filepath;
    }
    
    public void setMaxRetries(int maxRetries) {
        this.maxRetries = maxRetries;
    }
    
    public void run() {
        boolean isErased = false;
        File f = new File(filepath);
        int retries = 0;
        
        while (f.exists() && !isErased && retries++ < maxRetries) {
            // Even if all references to the file have been closed and removed if garbage collector did not run before the file will not be deleted.
            System.gc();
            isErased = WebCoreUtil.secureDelete(f);
            try {
                Thread.sleep(200);
            } catch (Exception e) {
            }
        }
        
        if (!isErased) {
            logger.error("Error deleting temp file: " + filepath);
        }
    }
    
}
