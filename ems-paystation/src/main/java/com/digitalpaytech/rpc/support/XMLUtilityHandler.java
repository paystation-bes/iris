/*
 * Copyright @ 2002 Digital Pioneer. All Rights Reserved.
 */
package com.digitalpaytech.rpc.support;

import org.xml.sax.Attributes;

import com.digitalpaytech.util.xml.XmlSaxHandlerBase;

public class XMLUtilityHandler extends XmlSaxHandlerBase
{
//	private final static Logger logger__ = Logger.getLogger(XMLUtilityHandler.class);
	
	private final static String PASSWORD_ATTRIBUTE = "Password";
	private final static String USERID_ATTRIBUTE = "UserId";
	private final static String VERSION_ATTRIBUTE = "Version";

	private String elementName = null;
	private String password = null;
	private String userName = null;
	private String version = null;

	public void startElement(String uri, String localName, String qName, Attributes attributes)
	{
		if (elementName == null)
		{
			elementName = qName.trim();
			userName = attributes.getValue(USERID_ATTRIBUTE);
			password = attributes.getValue(PASSWORD_ATTRIBUTE);
			version = attributes.getValue(VERSION_ATTRIBUTE);
		}
	}

	protected String getMainElement()
	{
		return (elementName);
	}
	
	protected String getUserName()
	{
		return (userName);
	}
	
	protected String getPassword()
	{
		return (password);
	}
	
	protected String getVersion() 
	{
	    return version;
	}
}