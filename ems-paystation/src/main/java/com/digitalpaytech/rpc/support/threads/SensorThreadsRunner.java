package com.digitalpaytech.rpc.support.threads;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.service.ArchiveService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.RawSensorDataService;
import com.digitalpaytech.service.paystation.EventSensorService;

/**
 * In EMS 6.3.11 SensorExtractionThread is called by Spring and all services are injected from the configuration file.
 * In EMS 7 since SensorThreadsRunner is annotated with @Component, Spring will set all @Autowired services and call @PostConstruct method, 
 * 'startSensorExtractionThread'. 
 */

@Component("sensorThreadsRunner")
public class SensorThreadsRunner {
    
    @Autowired
    private EventSensorService eventSensorService;

    @Autowired
    private RawSensorDataService rawSensorDataService;

    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private ArchiveService archiveService;
    

    @PostConstruct
    private void startSensorExtractionThread() {
        SensorExtractionThread t = new SensorExtractionThread(eventSensorService, rawSensorDataService, pointOfSaleService, emsPropertiesService, archiveService);
        t.start();
    }
    
    
    public void setEventSensorService(EventSensorService eventSensorService) {
        this.eventSensorService = eventSensorService;
    }

    public void setRawSensorDataService(RawSensorDataService rawSensorDataService) {
        this.rawSensorDataService = rawSensorDataService;
    }

    public void setPointOfSaleService(PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }

    public void setEmsPropertiesService(EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }

    public void setArchiveService(ArchiveService archiveService) {
        this.archiveService = archiveService;
    }
}
