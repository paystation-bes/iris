package com.digitalpaytech.rpc.support.threads;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.annotation.PostConstruct;

import org.apache.commons.digester.Digester;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import com.digitalpaytech.cardprocessing.CardProcessingMaster;
import com.digitalpaytech.cardprocessing.cps.ProcessingDestinationService;
import com.digitalpaytech.cardprocessing.cps.impl.ProcessingDestinationDTO;
import com.digitalpaytech.data.TransactionData;
import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.MerchantPOS;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.dto.cps.CPSResponse;
import com.digitalpaytech.dto.paystation.PsTransaction;
import com.digitalpaytech.dto.paystation.TransactionDto;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.DuplicateTransactionRequestException;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.helper.TransactionHelper;
import com.digitalpaytech.service.CardRetryTransactionService;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.CustomerSubscriptionService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.LicensePlateService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.PurchaseService;
import com.digitalpaytech.service.TransactionFileUploadService;
import com.digitalpaytech.service.TransactionService;
import com.digitalpaytech.service.crypto.CryptoAlgorithmFactory;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CryptoUtil;
import com.digitalpaytech.util.StableDateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.TransactionFacade;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.crypto.CryptoConstants;
import com.digitalpaytech.util.json.JSON;
import com.digitalpaytech.util.xml.XMLRulesDriver;
import com.digitalpaytech.util.xml.XMLUtil;

/**
 * Copied from EMS 6.3.11 com.digitalpioneer.appservice.transaction.offline.Impl.XMLTransactionAppServiceImpl which its 'initializeThreads' method is
 * called by Spring and all services are injected from ems-appservice.xml.
 * In EMS 7 since XMLTransactionThreadRunner is annotated with @Component, Spring will set all @Autowired services and call @PostConstruct method,
 * 'initializeThreads'.
 */

@Component("xmlTransactionThreadRunner")
@SuppressWarnings({ "PMD.ExcessiveImports", "PMD.GodClass", "PMD.TooManyFields" })
public class XMLTransactionThreadRunnerImpl implements XMLTransactionThreadRunner {
    
    // Record Types   
    public static final int SHA256_HASH = 0x00001002;
    
    private static final String NULL_STRING = "null";
    private static final String SHA1_HASH_MATCH = "++++ Sha1Hash match? ";
    private static final String ZIP_FILE_FULL_NAME = "zipFileFullName: ";
    private static final String PARSE_ERROR_MSG = "Unable to parse HZT or HPZ file ";
    
    private static final String FILE_SEPARATOR = "file.separator";
    private static final Logger LOG = Logger.getLogger(XMLTransactionThreadRunnerImpl.class);
    private static final int DEFAULT_THREAD_COUNT = 1;
    private static int threadCount = DEFAULT_THREAD_COUNT;
    
    // 28 bytes
    private static final int SHA_HASH_ENCODE_LENGTH = 28;
    private static final String OFFLINE_TRANS_HPZ_FILE_EXTENSION = ".xml.hpz";
    private static final String OFFLINE_TRANS_HZT_FILE_EXTENSION = ".xml.hzt";
    private static final String OFFLINE_TRANSACTION_FILE_ZIP_EXTENSION = ".zip";
    // 28 bytes
    private static final int INFO_FIELD_LENGTH = 4;
    
    private static XMLTransactionThread[] xmlTransactionThreads;
    private static int currentThread;
    
    private final ThreadLocal<StringBuilder> fileProcessError = new ThreadLocal<StringBuilder>();
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private CryptoAlgorithmFactory cryptoAlgorithmFactory;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    @Autowired
    private CustomerSubscriptionService customerSubscriptionService;
    
    @Autowired
    private XMLRulesDriver xmlRulesDriver;
    
    @Autowired
    private MailerService mailerService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private TransactionService transactionService;
    
    @Autowired
    private TransactionFacade transactionFacade;
    
    @Autowired
    private TransactionHelper transactionHelper;
    
    @Autowired
    private CardProcessingMaster cardProcessingMaster;
    
    @Autowired
    private PurchaseService purchaseService;
    
    @Autowired
    private TransactionFileUploadService transactionFileUploadService;
    
    @Autowired
    private CardRetryTransactionService cardRetryTransactionService;
    
    @Autowired
    private LicensePlateService licensePlateService;
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    @Autowired
    private ProcessingDestinationService processingDestinationService;
    
    private JSON json = new JSON();
    
    /**
     * setup the process queue
     * 
     * @param hpzFileFullName
     */
    @Override
    public final synchronized void addFileToQueue(final String hpzFileFullName) {
        xmlTransactionThreads[currentThread].addTransactionToQueue(hpzFileFullName);
        
        // Rotate to next thread in round-robin format
        currentThread++;
        if (currentThread == threadCount) {
            currentThread = 0;
        }
    }
    
    /**
     * This method is called by XMLTransactionThread in 'run' method. It'll check if anything is
     * in the queue which is inserted by 'addFileToQueue' method above.
     */
    @Override
    public final void processXMLTransactions(final String fileFullName) {
        try {
            // filename without extension
            String fileExtension = OFFLINE_TRANS_HPZ_FILE_EXTENSION;
            boolean isHzt = false;
            if (fileFullName.indexOf(OFFLINE_TRANS_HPZ_FILE_EXTENSION) == -1) {
                fileExtension = OFFLINE_TRANS_HZT_FILE_EXTENSION;
                isHzt = true;
            }
            
            final String filename = fileFullName.substring(0, fileFullName.indexOf(fileExtension));
            boolean hashMatched = false;
            if (isHzt) {
                hashMatched = this.verifyShaHash(filename);
            } else {
                hashMatched = this.verifySha1Hash(filename);
            }
            
            final Map<String, PointOfSale> posMap = new HashMap<String, PointOfSale>();
            if (hashMatched) {
                parseAndProcess(filename + OFFLINE_TRANSACTION_FILE_ZIP_EXTENSION, posMap);
                if (LOG.isInfoEnabled()) {
                    LOG.info(filename + OFFLINE_TRANSACTION_FILE_ZIP_EXTENSION + " is processed.");
                }
                deleteFile(fileFullName);
                deleteZipFile(filename + OFFLINE_TRANSACTION_FILE_ZIP_EXTENSION);
            } else {
                final String serialNumber = filename.substring(filename.lastIndexOf(System.getProperty(FILE_SEPARATOR)) + 1, filename.indexOf('_'));
                sendEmailAlert(filename, serialNumber);
                LOG.error("+++ xml transaction file (" + fileFullName + fileExtension + ") hash not match +++");
            }
            posMap.clear();
        } catch (final FileNotFoundException e) {
            LOG.error(e.getMessage(), e);
        } catch (final SAXException e) {
            final String subject = PARSE_ERROR_MSG + fileFullName;
            this.mailerService.sendAdminWarnAlert(subject, subject, e);
        } catch (final Exception e) {
            final String subject = PARSE_ERROR_MSG + fileFullName;
            this.mailerService.sendAdminWarnAlert(subject, subject, e);
        }
    }
    
    private void parseAndProcess(final String zipFileFullName, final Map<String, PointOfSale> posMap) throws IOException, SAXException {
        if (LOG.isInfoEnabled()) {
            LOG.info("parsing xml zip file: " + zipFileFullName);
        }
        this.fileProcessError.set(new StringBuilder());
        final File file = new File(zipFileFullName);
        final ZipFile zipFile = new ZipFile(file);
        // one entry, one file
        final Enumeration<? extends ZipEntry> zipEntries = zipFile.entries();
        if (zipEntries.hasMoreElements()) {
            final ZipEntry zipEntry = (ZipEntry) zipEntries.nextElement();
            
            if (LOG.isDebugEnabled()) {
                LOG.debug("zip entry name: " + zipEntry.getName());
            }
            
            final String fullFileErrorLoc = WebCoreConstants.DEFAULT_TRANSACTION_FILES_ERROR_LOCATION + file.getName();
            boolean okFlag = true;
            BufferedReader reader = null;
            InputStream is = null;
            try {
                is = zipFile.getInputStream(zipEntry);
                final byte[] xmlDocument = XMLUtil.encodeXmlDocAmpersand(is);
                reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(xmlDocument)));
                
                final Digester parser = this.xmlRulesDriver.getTransactionParser();
                final PsTransaction pstx = (PsTransaction) parser.parse(reader);
                
                TransactionDto transactionDto;
                final Iterator<TransactionDto> iter = pstx.getTransactionDtos().iterator();
                while (iter.hasNext()) {
                    transactionDto = iter.next();
                    transactionDto.setVersion(pstx.getVersion());
                    processTransaction(transactionDto, posMap);
                }
                
                if (!posMap.isEmpty()) {
                    // Copy the file to $CATALINA_HOME/zipHpzErrors directory.
                    okFlag = copyFileToErrorDirectory(file, fullFileErrorLoc);
                    final StringBuilder bdr = new StringBuilder();
                    bdr.append("Cannot parse file ! ").append(zipFileFullName);
                    if (okFlag) {
                        bdr.append(", copied it to: ");
                    } else {
                        bdr.append(", cannot copy it to: ");
                    }
                    bdr.append(fullFileErrorLoc);
                    LOG.error(bdr.toString());
                    this.fileProcessError.get().append(bdr.toString());
                }
            } catch (final Exception e) {
                okFlag = copyFileToErrorDirectory(file, fullFileErrorLoc);
                final StringBuilder bdr = new StringBuilder();
                bdr.append("Exception while parsing file ! ").append(zipFileFullName).append(WebCoreConstants.EMPTY_SPACE);
                if (okFlag) {
                    bdr.append("Copy it to: ");
                } else {
                    bdr.append("Cannot copy it to: ");
                }
                bdr.append(fullFileErrorLoc);
                LOG.error(bdr.toString(), e);
                this.fileProcessError.get().append(bdr.toString());
                
            } finally {
                if (reader != null) {
                    reader.close();
                }
                if (is != null) {
                    is.close();
                }
                if (zipFile != null) {
                    zipFile.close();
                }
                if (this.fileProcessError.get() != null && this.fileProcessError.get().length() > 0) {
                    // file processing error, send admin alert.
                    final StringBuilder err = this.fileProcessError.get();
                    if (err != null && err.length() > 0) {
                        final String subject = "Unable to parse HPZ file " + zipFileFullName + "\n";
                        err.insert(0, subject);
                        this.mailerService.sendAdminWarnAlert(subject, err.toString(), null);
                    }
                }
            }
        } else {
            LOG.warn("!!! empty entry for xml transaction (" + zipFileFullName + ") !!!");
        }
    }
    
    private void deleteFile(final String fileFullName) {
        System.gc();
        final File file = new File(fileFullName);
        final StringBuilder bdr = new StringBuilder();
        if (file.exists() && file.isFile()) {
            saveTransactionFileUpload(fileFullName);
            WebCoreUtil.secureDelete(file);
            bdr.append("file ").append(fileFullName).append(" is deleted");
            LOG.info(bdr.toString());
        } else {
            bdr.append("!!! file ").append(fileFullName).append(" is directory or does not exist !!!");
            LOG.warn(bdr.toString());
        }
    }

    private void deleteZipFile(final String fileFullName) {
        final File file = new File(fileFullName);
        final StringBuilder bdr = new StringBuilder();
        if (file.exists() && file.isFile()) {
            WebCoreUtil.secureDelete(file);
            bdr.append("file ").append(fileFullName).append(" is deleted");
            LOG.info(bdr.toString());
        } else {
            bdr.append("!!! file ").append(fileFullName).append(" is directory or does not exist !!!");
            LOG.warn(bdr.toString());
        }
    }

    private void saveTransactionFileUpload(final String fileFullName) {
        final String serialNum = fileFullName.substring(fileFullName.lastIndexOf(System.getProperty(FILE_SEPARATOR)) + 1,
                                                        fileFullName.indexOf(StandardConstants.STRING_UNDERSCORE));
        final Customer cust = findCustomer(serialNum);
        if (cust != null) {
            this.transactionFileUploadService.saveTransactionFileUpload(cust, fileFullName);
        }
    }
    
    private Customer findCustomer(final String serialNumber) {
        final PointOfSale pos = this.pointOfSaleService.findPointOfSaleBySerialNumber(serialNumber);
        if (pos != null) {
            return pos.getCustomer();
        }
        LOG.error("Cannot find Customer by serialNumber: " + serialNumber);
        return null;
    }
    
    public final boolean isDisableProcessing() {
        return this.emsPropertiesService.getPropertyValueAsBoolean(EmsPropertiesService.DISABLE_PROCESSING_UPLOADED_TRANSFILE, false);
    }
    
    private boolean verifyShaHash(final String filename) {
        boolean hashMatched = false;
        FileOutputStream fileOutputStream = null;
        FileInputStream fileInputStream = null;
        BufferedInputStream bufferedInputStream = null;
        
        try {
            final File file = new File(filename + OFFLINE_TRANS_HZT_FILE_EXTENSION);
            final String zipFileFullName = filename + OFFLINE_TRANSACTION_FILE_ZIP_EXTENSION;
            if (LOG.isDebugEnabled()) {
                LOG.debug(ZIP_FILE_FULL_NAME + zipFileFullName);
            }
            fileInputStream = new FileInputStream(file);
            bufferedInputStream = new BufferedInputStream(fileInputStream);
            final byte[] fileTypeByteArray = new byte[INFO_FIELD_LENGTH];
            byte[] recordTypeByteArray = new byte[INFO_FIELD_LENGTH];
            byte[] recordLengthByteArray = new byte[INFO_FIELD_LENGTH];
            int signatureOrHashType = -1;
            
            int recordLength = -1;
            
            final long fileLength = file.length();
            if (fileLength > Integer.MAX_VALUE) {
                hashMatched = false;
            }
            
            bufferedInputStream.read(fileTypeByteArray);
            bufferedInputStream.read(recordTypeByteArray);
            bufferedInputStream.read(recordLengthByteArray);
            
            ByteBuffer byteBuffer = ByteBuffer.wrap(fileTypeByteArray);
            byteBuffer.order(ByteOrder.BIG_ENDIAN);
            
            byteBuffer = ByteBuffer.wrap(recordTypeByteArray);
            byteBuffer.order(ByteOrder.BIG_ENDIAN);
            
            /*
             * PROBLEM BEGINS
             * TODO We should not assume the new SHA type will always be SHA256. We should read the type from the file compare it to
             * what we have available in the DB and if it is an acceptable type continue using it. We should not set it to SHA256.
             * And why is the type in the file 4098?
             */
            signatureOrHashType = byteBuffer.getInt();
            
            // TODO: THIS NEEDS TO WORK THE OTHER WAY AROUND, SHA-1 SHOULD TEST FOR FAILURE THEN TRY THIS
            if (signatureOrHashType != SHA256_HASH) {
                LOG.error("Incorrect record type value: " + signatureOrHashType);
                return false;
                
            }
            
            signatureOrHashType = EmsPropertiesService.DEFAULT_HASH_ALGORITHM_TYPE_SHA_256_ID;
            // PROBLEM ENDS
            
            byteBuffer = ByteBuffer.wrap(recordLengthByteArray);
            byteBuffer.order(ByteOrder.BIG_ENDIAN);
            recordLength = byteBuffer.getInt();
            
            final byte[] signatureByteBuffer = new byte[recordLength];
            bufferedInputStream.read(signatureByteBuffer);
            
            if (LOG.isInfoEnabled()) {
                LOG.info("+++ Signature Hash: '" + new String(signatureByteBuffer, CardProcessingConstants.CHARSET_ISO_8859_1));
            }
            
            recordTypeByteArray = new byte[INFO_FIELD_LENGTH];
            bufferedInputStream.read(recordTypeByteArray);
            
            recordLengthByteArray = new byte[INFO_FIELD_LENGTH];
            bufferedInputStream.read(recordLengthByteArray);
            
            byteBuffer = ByteBuffer.wrap(recordTypeByteArray);
            byteBuffer.order(ByteOrder.BIG_ENDIAN);
            
            byteBuffer = ByteBuffer.wrap(recordLengthByteArray);
            byteBuffer.order(ByteOrder.BIG_ENDIAN);
            recordLength = byteBuffer.getInt();
            
            final byte[] payload = new byte[recordLength];
            bufferedInputStream.read(payload);
            
            final byte[] signatureData = new byte[recordLength + (2 * INFO_FIELD_LENGTH)];
            
            for (int i = 0; i < INFO_FIELD_LENGTH; i++) {
                signatureData[i] = recordTypeByteArray[i];
            }
            for (int i = 0; i < INFO_FIELD_LENGTH; i++) {
                signatureData[i + INFO_FIELD_LENGTH] = recordLengthByteArray[i];
            }
            for (int i = 0; i < recordLength; i++) {
                final int offset = 2 * INFO_FIELD_LENGTH;
                signatureData[i + offset] = payload[i];
            }
            
            hashMatched = this.cryptoAlgorithmFactory.verifyShaHash(new String(signatureByteBuffer, CardProcessingConstants.CHARSET_ISO_8859_1),
                                                                    signatureData, CryptoConstants.HASH_TRANSACTION_UPLOAD,
                                                                    CardProcessingConstants.CHARSET_ISO_8859_1, signatureOrHashType);
            if (hashMatched) {
                fileOutputStream = new FileOutputStream(zipFileFullName);
                fileOutputStream.write(payload);
            }
            if (LOG.isInfoEnabled()) {
                LOG.info(SHA1_HASH_MATCH + hashMatched);
            }
        } catch (final Exception e) {
            LOG.error("verifyShaHash error: ", e);
            return false;
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (final IOException ioe) {
                }
            }
            if (bufferedInputStream != null) {
                try {
                    bufferedInputStream.close();
                } catch (final IOException ioe) {
                }
            }
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (final IOException ioe) {
                }
            }
        }
        return hashMatched;
    }
    
    private boolean verifySha1Hash(final String filename) {
        boolean hashMatched = false;
        FileOutputStream fos = null;
        FileInputStream fis = null;
        ByteArrayInputStream bis = null;
        try {
            final File file = new File(filename + OFFLINE_TRANS_HPZ_FILE_EXTENSION);
            fis = new FileInputStream(file);
            final long length = file.length();
            if (length > Integer.MAX_VALUE) {
                hashMatched = false;
            }
            final byte[] bytes = new byte[(int) length];
            fis.read(bytes);
            
            final String zipFileFullName = filename + OFFLINE_TRANSACTION_FILE_ZIP_EXTENSION;
            if (LOG.isDebugEnabled()) {
                LOG.debug(ZIP_FILE_FULL_NAME + zipFileFullName);
            }
            
            final byte[] receivedEncodedHash = new byte[SHA_HASH_ENCODE_LENGTH];
            final byte[] receivedData = new byte[(int) length - SHA_HASH_ENCODE_LENGTH];
            bis = new ByteArrayInputStream(bytes, 0, bytes.length);
            bis.read(receivedEncodedHash, 0, SHA_HASH_ENCODE_LENGTH);
            
            if (LOG.isInfoEnabled()) {
                LOG.info("+++ receivedEncodedHash: '" + new String(receivedEncodedHash, CardProcessingConstants.CHARSET_ISO_8859_1));
            }
            bis.read(receivedData, 0, receivedData.length);
            
            hashMatched = this.cryptoAlgorithmFactory.verifySha1Hash(new String(receivedEncodedHash, CardProcessingConstants.CHARSET_ISO_8859_1),
                                                                     receivedData, CryptoConstants.HASH_TRANSACTION_UPLOAD,
                                                                     CardProcessingConstants.CHARSET_ISO_8859_1);
            
            if (hashMatched) {
                fos = new FileOutputStream(zipFileFullName);
                fos.write(receivedData);
            } else {
                return this.verifyShaHash(filename);
            }
            
            if (LOG.isInfoEnabled()) {
                LOG.info(SHA1_HASH_MATCH + hashMatched);
            }
        } catch (final Exception e) {
            LOG.error("verifySha1Hash error: ", e);
            return false;
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (final IOException ioe) {
                }
            }
            if (bis != null) {
                try {
                    bis.close();
                } catch (final IOException ioe) {
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (final IOException ioe) {
                }
            }
        }
        return hashMatched;
    }
    
    /**
     * Called by 'parseAndProcess' method and it will perform processing transaction.
     */
    private synchronized void processTransaction(final TransactionDto txDto, final Map<String, PointOfSale> posMap) {
        // Remove any space in between, e.g. SC Recharge -> SCRecharge. 
        txDto.setType(txDto.getType().trim().replaceAll("\\s+", ""));
        
        if (LOG.isDebugEnabled()) {
            LOG.debug(txDto.toString());
        }
        
        PointOfSale pointOfSale = null;
        try {
            
            if (StringUtils.isBlank(txDto.getCPSResponse())) {
                pointOfSale = this.pointOfSaleService.findPointOfSaleBySerialNumber(txDto.getPaystationCommAddress());
            } else {
                pointOfSale = handleCPSResponse(txDto);
            }
            
            // 1. Look up Customer object and set into PointOfSale.
            final int customerId = pointOfSale.getCustomer().getId();
            pointOfSale.setCustomer(this.customerService.findCustomer(customerId));
            pointOfSale.getCustomer().setCustomerSubscriptions(this.customerSubscriptionService.findByCustomerId(customerId, true));
            pointOfSale.getCustomer().setCustomerProperties(this.customerAdminService.findCustomerPropertyByCustomerId(customerId));
            
            // 2. Make sure it's not a duplicate transaction by looking into ProcessorTransaction table.
            this.transactionFacade.verifyIfDuplicateTransaction(pointOfSale,
                                                                StableDateUtil.convertFromColonDelimitedDateString(txDto.getPurchasedDate()),
                                                                Integer.parseInt(txDto.getNumber()), false);
            
            final ProcessingDestinationDTO destinationDTO = checkProcessDestination(pointOfSale, txDto);
            
            // 3. Call transactionHelper.getTransactionData to create and populate domain objects.
            final TransactionData txData = this.transactionHelper.getTransactionData(pointOfSale, txDto, destinationDTO, txDto.getVersion(), false);
            txData.getPurchase().setIsOffline(true);
            txData.getProcessorTransaction().setIsOffline(true);
            txData.setUploadFromBoss(true);
            txData.setOffline(true);
            
            // 4. Call 'executeTransaction' to start transaction process.
            if (this.transactionHelper.hasDataToProcess(txData.getSmsAlert())) {
                // Processes transaction with SMS.
                this.transactionService.processTransactionWithSmsAlert(txData, txDto.getEmbeddedTxObject());
                this.cardProcessingMaster.addTransactionToSettlementQueue(pointOfSale, txData.getProcessorTransaction());
                
            } else if (this.transactionHelper.hasSmartCardDataAndPaid(txDto)) {
                // For regular smart card.
                this.transactionService.processTransaction(txData, false, null, txDto.getEmbeddedTxObject());
                
            } else if (this.purchaseService.findUniquePurchaseForSms(pointOfSale.getCustomer().getId(), pointOfSale.getId(),
                                                                     txData.getPurchase().getPurchaseGmt(),
                                                                     txData.getPurchase().getPurchaseNumber()) != null) {
                
                // Batched for credit card
                if (StringUtils.isNotBlank(txDto.getCardData())) {
                    final CardRetryTransaction crt = this.transactionHelper.createCardRetryTransaction(txData, txDto);
                    crt.setCardRetryTransactionType(this.transactionFacade
                            .findCardRetryTransactionType(WebCoreConstants.CARD_RETRY_TRANSACTION_TYPE_BATCHED));
                    this.cardRetryTransactionService.save(crt);
                    this.cardProcessingMaster.addTransactionToStoreForwardQueue(crt);
                    
                } else {
                    LOG.warn(getCashMessage(txDto));
                }
            } else {
                // Processes transaction & processor transaction
                final List<Object> objs = this.transactionService.processTransaction(txData, false, null, txDto.getEmbeddedTxObject());
                final CardRetryTransaction crt = this.cardRetryTransactionService.getCardRetryTransaction(objs);
                
                if (destinationDTO.isProcessInLink() && StringUtils.isNotBlank(destinationDTO.getCardTypeName())) {
                    crt.setCardType(destinationDTO.getCardTypeName());
                }
                
                if (!txData.isCoreCPS()) {
                    if (crt == null && txData.getPurchase().getCardPaidAmount() > 0) {
                        this.cardProcessingMaster.addTransactionToSettlementQueue(pointOfSale, txData.getProcessorTransaction());
                    } else if (crt != null && txData.getPurchase().getCardPaidAmount() > 0) {
                        this.cardProcessingMaster.addTransactionToStoreForwardQueue(crt);
                    }
                }
            }
            
            if (!txData.isCancelledTransaction()) {
                this.licensePlateService.updateLicensePlateService(txDto, pointOfSale, txData.getPurchase());
            }
            
        } catch (final DuplicateTransactionRequestException dtre) {
            LOG.warn(dtre.getMessage());
        } catch (final Exception e) {
            final StringBuilder err = new StringBuilder("Unable to process transaction: ").append(createPermitInfo(txDto)).append("Error: ")
                    .append(e.getClass().getName()).append(" - ").append(e.getMessage()).append("\n");
            this.fileProcessError.get().append(err.toString());
            LOG.error("exception when processing offline xml transaction: " + err.toString(), e);
            
            // Add serial number to the HashMap to notify the error.
            posMap.put(txDto.getPaystationCommAddress(), pointOfSale);
        }
    }
    
    private ProcessingDestinationDTO checkProcessDestination(final PointOfSale pointOfSale, final TransactionDto txDto) throws CryptoException {
        ProcessingDestinationDTO processDto = null;
        if (!CryptoUtil.isDecryptedCardData(txDto.getCardData())) {
            processDto =
                    this.processingDestinationService.pickProcessingDest(txDto.getCardData(), pointOfSale.getCustomer().getId(), pointOfSale.getId());
        } else {
            processDto = new ProcessingDestinationDTO();
            processDto.setProcessInLink(false);
        }
        return processDto;
    }
    
    private PointOfSale handleCPSResponse(final TransactionDto txDto) throws JsonException {
        
        final CPSResponse cpsResponse = this.json.deserialize(txDto.getCPSResponse(), CPSResponse.class);
        
        final String terminalToken = cpsResponse.getTerminalToken();
        
        if (StringUtils.isBlank(terminalToken)) {
            final IllegalArgumentException iae =
                    new IllegalArgumentException("Terminal Token is blank in CoreCPS transaction for POS " + txDto.getPaystationCommAddress());
            LOG.error(iae);
            throw iae;
        }
        
        if (this.merchantAccountService.findByTerminalTokenAndIsLink(terminalToken) == null) {
            final String title = "TerminalToken: " + terminalToken + " doesn't exist ";
            final String err = title + "in MerchantAccount for serial number: " + txDto.getPaystationCommAddress();
            LOG.error(err);
            this.mailerService.sendAdminErrorAlert(title, err, null);
            throw new IllegalArgumentException(err);
        }
        
        final MerchantPOS relation = this.merchantAccountService
                .findMerchantPOSByMerchantAccountTerminalTokenAndSerialNumber(terminalToken, txDto.getPaystationCommAddress());
        
        if (relation == null || relation.getPointOfSale() == null) {
            return this.pointOfSaleService.findPointOfSaleBySerialNumber(txDto.getPaystationCommAddress());
        } else {
            return this.pointOfSaleService.findPointOfSaleById(relation.getPointOfSale().getId());
        }
        
    }
    
    private String createPermitInfo(final TransactionDto txDto) {
        final StringBuilder bdr = new StringBuilder().append("Pos serial number: ")
                .append(txDto == null ? NULL_STRING : txDto.getPaystationCommAddress()).append(WebCoreConstants.EMPTY_SPACE).append("PurchasedDate: ")
                .append(txDto == null ? NULL_STRING : txDto.getPurchasedDate()).append(WebCoreConstants.EMPTY_SPACE).append("TicketNumber: ")
                .append(txDto == null ? NULL_STRING : txDto.getNumber()).append(WebCoreConstants.EMPTY_SPACE);
        return bdr.toString();
    }
    
    private void sendEmailAlert(final String fileName, final String companyName) {
        final String subject = "Invalid hash in uploaded file";
        final StringBuilder content = new StringBuilder();
        content.append("Invalid hash in uploaded file (").append(fileName).append("), Company Name: ").append(companyName);
        this.mailerService.sendAdminErrorAlert(subject, content.toString(), null);
        if (LOG.isInfoEnabled()) {
            LOG.info("!!! Admin error alert email is sent because of xml transaction file hash mismatch !!!");
        }
    }
    
    @PostConstruct
    @SuppressWarnings("PMD.UnusedPrivateMethod")
    private synchronized void initializeThreads() {
        final File errDir = new File(WebCoreConstants.DEFAULT_TRANSACTION_FILES_ERROR_LOCATION);
        if (!errDir.exists()) {
            errDir.mkdirs();
        }
        
        final int transactionThreadCount = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.FILE_PARSER_THREAD_COUNT, 1);
        if (transactionThreadCount > 0) {
            threadCount = transactionThreadCount;
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("xmlTransactionThread threadCount: " + threadCount);
        }
        
        // Initialize array and create settlement threads
        xmlTransactionThreads = new XMLTransactionThread[threadCount];
        for (int i = 0; i < threadCount; i++) {
            xmlTransactionThreads[i] = new XMLTransactionThread(i, this);
            xmlTransactionThreads[i].start();
        }
    }
    
    private String getCashMessage(final TransactionDto txDto) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("Purchase, Permit were created and this is a CASH transaction, ignore now.\n").append(createPermitInfo(txDto));
        return bdr.toString();
    }
    
    private boolean copyFileToErrorDirectory(final File file, final String fullFileErrorLoc) throws IOException {
        final File errDirFileName = new File(fullFileErrorLoc);
        FileUtils.copyFile(file, errDirFileName);
        return errDirFileName.exists();
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final void setCryptoAlgorithmFactory(final CryptoAlgorithmFactory cryptoAlgorithmFactory) {
        this.cryptoAlgorithmFactory = cryptoAlgorithmFactory;
    }
    
    public final void setXmlRulesDriver(final XMLRulesDriver xmlRulesDriver) {
        this.xmlRulesDriver = xmlRulesDriver;
    }
    
    public final void setMailerService(final MailerService mailerService) {
        this.mailerService = mailerService;
    }
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final void setTransactionService(final TransactionService transactionService) {
        this.transactionService = transactionService;
    }
    
    public final void setTransactionFacade(final TransactionFacade transactionFacade) {
        this.transactionFacade = transactionFacade;
    }
    
    public final void setCardProcessingMaster(final CardProcessingMaster cardProcessingMaster) {
        this.cardProcessingMaster = cardProcessingMaster;
    }
    
    public final void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }
    
    public final void setCustomerSubscriptionService(final CustomerSubscriptionService customerSubscriptionService) {
        this.customerSubscriptionService = customerSubscriptionService;
    }
    
    public final void setCustomerAdminService(final CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    public final void setPurchaseService(final PurchaseService purchaseService) {
        this.purchaseService = purchaseService;
    }
    
    public final void setTransactionFileUploadService(final TransactionFileUploadService transactionFileUploadService) {
        this.transactionFileUploadService = transactionFileUploadService;
    }
    
    public final void setCardRetryTransactionService(final CardRetryTransactionService cardRetryTransactionService) {
        this.cardRetryTransactionService = cardRetryTransactionService;
    }
}
