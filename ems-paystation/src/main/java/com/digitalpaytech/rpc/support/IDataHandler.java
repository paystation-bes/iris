package com.digitalpaytech.rpc.support;

import org.springframework.web.context.WebApplicationContext;

import com.digitalpaytech.util.xml.XMLBuilderBase;

public interface IDataHandler {
	public String getRootElementName();

	public void process(XMLBuilderBase xmlBuilder);

	public void setWebApplicationContext(WebApplicationContext ctx);
	
	public void setVersion(String version);
}