package com.digitalpaytech.rpc.support.threads;

public interface XMLTransactionThreadRunner {
    void addFileToQueue(String hpzFileFullName);
    
    boolean isDisableProcessing();
    
    void processXMLTransactions(String hpzFileFullName);
}
