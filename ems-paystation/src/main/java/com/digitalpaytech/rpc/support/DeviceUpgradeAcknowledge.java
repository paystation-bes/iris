package com.digitalpaytech.rpc.support;

import java.io.Serializable;
import java.util.Date;

public final class DeviceUpgradeAcknowledge implements Serializable {
    private static final long serialVersionUID = -6880513682326140308L;
    private String customerId;
    private String deviceSerialNumber;
    private String groupName;
    private Date uploadDate;
    
    public DeviceUpgradeAcknowledge() {
        
    }
    
    public String getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(final String customerId) {
        this.customerId = customerId;
    }

    public String getDeviceSerialNumber() {
        return this.deviceSerialNumber;
    }

    public void setDeviceSerialNumber(final String deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }

    public String getGroupName() {
        return this.groupName;
    }

    public void setGroupName(final String groupName) {
        this.groupName = groupName;
    }

    public Date getUploadDate() {
        return this.uploadDate;
    }

    public void setUploadDate(final Date uploadDate) {
        this.uploadDate = uploadDate;
    }
}
