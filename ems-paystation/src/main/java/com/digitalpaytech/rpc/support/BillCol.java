package com.digitalpaytech.rpc.support;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

// Migrated from EMS 6.3.11

public class BillCol {
    private static final String DOLLAR_1 = "1";
    private static final String DOLLARS_2 = "2";
    private static final String DOLLARS_5 = "5";
    private static final String DOLLARS_10 = "10";
    private static final String DOLLARS_20 = "20";
    private static final String DOLLARS_50 = "50";
    
    private Map<String, Integer> billMap;
    private String billCollection;
    
    
    public BillCol() {
        billMap = new HashMap<String, Integer>();
    }
    
    
    public BillCol(String collection) {
        billMap = new HashMap<String, Integer>();
        this.billCollection = collection;
        parse();
    }


    public void parse() {
        if (StringUtils.isBlank(billCollection)) {
            return;
        }
        
        //e.g. 1:3,2:1,10:1 -> [1:3], [2:1], [10:1]
        String[] dominationCounts = billCollection.split(",");
        for (String str : dominationCounts) {
            String[] dominationCount = str.split(":");
            billMap.put(dominationCount[0].trim(), Integer.parseInt(dominationCount[1].trim()));
        }
    }
    
    
    public String getBillCollection() {
        return billCollection;
    }
    
    public void setBillCollection(String billCollection) {
        this.billCollection = billCollection;
    }
    
    public int getCount1() {
        // 1 = "$1"
        return returnZeroIfNull(billMap.get(DOLLAR_1));
    }
    
    public int getAmount1() {
        return 100;
    }
    
    public int getTotalAmount1() {
        return getCount1() * Integer.parseInt(DOLLAR_1) * 100;
    }
    
    public int getCount2() {
        // 2 = "$2"
        return returnZeroIfNull(billMap.get(DOLLARS_2));
    }
    
    public int getAmount2() {
        return 200;
    }
    
    public int getTotalAmount2() {
        return getCount2() * Integer.parseInt(DOLLARS_2) * 100;
    }
    
    public int getCount5() {
        // 5 = "$5"
        return returnZeroIfNull(billMap.get(DOLLARS_5));
    }
    
    public int getAmount5() {
        return 500;
    }
    
    public int getTotalAmount5() {
        return getCount5() * Integer.parseInt(DOLLARS_5) * 100;
    }
    
    public int getCount10() {
        // 10 = "$10"
        return returnZeroIfNull(billMap.get(DOLLARS_10));
    }
    
    public int getAmount10() {
        return 1000;
    }
    
    public int getTotalAmount10() {
        return getCount10() * Integer.parseInt(DOLLARS_10) * 100;
    }
    
    public int getCount20() {
        // 20 = "$20"
        return returnZeroIfNull(billMap.get(DOLLARS_20));
    }

    public int getAmount20() {
        return 2000;
    }
    
    public int getTotalAmount20() {
        return getCount20() * Integer.parseInt(DOLLARS_20) * 100;
    }
    
    public int getCount50() {
        // 50 = "$50"
        return returnZeroIfNull(billMap.get(DOLLARS_50));
    }
    
    public int getAmount50() {
        return 5000;
    }
    
    public int getTotalAmount50() {
        return getCount50() * Integer.parseInt(DOLLARS_50) * 100;
    }
    
    public int getBillDollars() {
        int total1 = getTotalAmount1();
        int total2 = getTotalAmount2();
        int total5 = getTotalAmount5();
        int total10 = getTotalAmount10();
        int total20 = getTotalAmount20();
        int total50 = getTotalAmount50();
        return total1 + total2 + total5 + total10 + total20 + total50;    
    }   
    
    public int getBillCount() {
        return getCount1() + getCount2() + getCount5() + getCount10() + getCount20() + getCount50();
    }
    
    private int returnZeroIfNull(Integer intObj) {
        if (intObj == null) {
            return 0;
        }
        return intObj;
    }
}
