package com.digitalpaytech.rpc.support;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("FileInfo")
public class LinuxConfigFileNotificationInfo {
    
    @XStreamAlias("FileId")
    private String fileId;
    
    @XStreamAlias("FileName")
    private String fileName;
    
    @XStreamAlias("Hash")
    private String hash;
    
    @XStreamAlias("Size")
    private long size;
    
    public LinuxConfigFileNotificationInfo() {
        super();
    }
    
    public LinuxConfigFileNotificationInfo(String fileId, String fileName, String hash, long size) {
        this.fileId = fileId;
        this.fileName = fileName;
        this.hash = hash;
        this.size = size;
    }
    
    public final String getFileId() {
        return fileId;
    }
    public final void setFileId(final String fileId) {
        this.fileId = fileId;
    }
    public final String getFileName() {
        return fileName;
    }
    public final void setFileName(final String fileName) {
        this.fileName = fileName;
    }
    public final String getHash() {
        return hash;
    }
    public final void setHash(final String hash) {
        this.hash = hash;
    }
    public final long getSize() {
        return size;
    }
    public final void setSize(final long size) {
        this.size = size;
    }
    
    
    
}
