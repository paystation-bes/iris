package com.digitalpaytech.rpc.support;

public final class MessageTags {
    public static final String AUDIT_TAG_NAME = "Audit";
    public static final String AUTHORIZE_CARD_REQUEST_TAG_NAME = "AuthorizeCardRequest";
    public static final String AUTHORIZE_CARD_RESPONSE_TAG_NAME = "AuthorizeCardResponse";
    public static final String AUTHORIZE_REQUEST_TAG_NAME = "AuthorizeRequest";
    public static final String AUTHORIZE_RESPONSE_TAG_NAME = "AuthorizeResponse";
    public static final String AUTHENTICATE_CARD_REQUEST_TAG_NAME = "AuthenticateCardRequest";
    public static final String AUTHENTICATE_CARD_RESPONSE_TAG_NAME = "AuthenticateCardResponse";
    public static final String CREDIT_CARD_CHARGE_REQUEST_TAG_NAME = "CreditCardChargeRequest";
    public static final String CREDIT_CARD_REFUND_REQUEST_TAG_NAME = "CreditCardRefundRequest";
    public static final String EMV_MERCHANT_ACCOUNT_SUCCESS_TAG_NAME = "EMVMerchantAccountSuccess";
    public static final String EMV_MERCHANT_ACCOUNT_TAG_NAME = "EMVMerchantAccount";
    public static final String EVENT_TAG_NAME = "Event";
    public static final String EVENT_PCM_TAG_NAME = "EventPcm";
    public static final String HEART_BEAT_TAG_NAME = "HeartBeat";
    public static final String INITIALIZATION_TAG_NAME = "Initialization";
    public static final String STALL_INFO_REQUEST_TAG_NAME = "StallInfoRequest";
    public static final String STALL_INFO_RESPONSE_TAG_NAME = "StallInfoResponse";
    public static final String STALL_REPORT_REQUEST_TAG_NAME = "StallReportRequest";
    public static final String STALL_REPORT_RESPONSE_TAG_NAME = "StallReportResponse";
    public static final String TRANSACTION_TAG_NAME = "Transaction";
    public static final String RECEIPT_TAG_NAME = "Receipt";
    public static final String REPLENISH_TAG_NAME = "Replenish";
    public static final String UNDEFINED_TAG_NAME = "Undefined";
    public static final String VALIDATE_COUPON_REQUEST_TAG_NAME = "ValidateCouponRequest";
    public static final String VALIDATE_COUPON_RESPONSE_TAG_NAME = "ValidateCouponResponse";
    public static final String LOT_SETTING_SUCCESS_TAG_NAME = "LotSettingSuccess";
    public static final String TICKET_FOOTER_SUCCESS_TAG_NAME = "TicketFooterSuccess";
    public static final String EMV_TAG_NAME = "EMVBankingMessage";
    public static final String LINUX_CONFIGURATION_SUCCESS = "LinuxConfigurationSuccess";
    public static final String LINUX_CONFIG = "LinuxConfig";
    public static final String PLATE_CHECK = "PlateCheck";
    public static final String PLATE_CHECK_RESPONSE = "PlateCheckResponse";
    public static final String LINUX_BIN_RANGE_SUCCESS = "LinuxBINRangeSuccess";
    public static final String LINUX_BAD_CARD_LIST_SUCCESS = "LinuxBadCardListSuccess";
    
    public static final String CHARGE_REQUEST_TAG_NAME = "ChargeRequest";
    public static final String REFUND_REQUEST_TAG_NAME = "RefundRequest";
    public static final String PLATE_REPORT_REQUEST_TAG_NAME = "PlateRptReq";
    
    public static final String COLLECTION_REQUEST_TAG_NAME = "Collection";
    public static final String COUPON_AUTHORIZE_REQUEST_TAG_NAME = "CouponAuthRequest";
    
    public static final String EMV_TELEMETRY_TAG_NAME = "EMVTelemetry";
    public static final String EMV_REFUND_REQUEST_TAG_NAME = "EMVRefund";
    public static final String ROOT_CERTIFICATE_SUCCESS_TAG_NAME = "RootCertificateSuccess";
    
    private MessageTags() {
    }
}
