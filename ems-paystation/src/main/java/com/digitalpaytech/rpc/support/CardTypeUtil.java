package com.digitalpaytech.rpc.support;

public final class CardTypeUtil {
    public static final String AMEX_STRING = "Amex";
    public static final String DISCOVER_STRING = "Disc";
    public static final String MASTERCARD_STRING = "MC";
    public static final String OTHER_STRING = "Other";
    public static final String SMART_CARD_STRING = "SC";
    public static final String VISA_STRING = "Visa";
    public static final String DINERS_STRING = "Diners";
    public static final String JCB_STRING = "JCB";
    
    public static final class CardType extends NameValue {
        public static final CardType UNDEFINED = new CardType(UNDEFINED_VALUE, UNDEFINED_STRING);
        public static final CardType AMEX = new CardType(1, AMEX_STRING);
        public static final CardType DISCOVER = new CardType(2, DISCOVER_STRING);
        public static final CardType MASTERCARD = new CardType(3, MASTERCARD_STRING);
        public static final CardType OTHER = new CardType(4, OTHER_STRING);
        public static final CardType SMART_CARD = new CardType(5, SMART_CARD_STRING);
        public static final CardType VISA = new CardType(6, VISA_STRING);
        public static final CardType DINERS = new CardType(7, DINERS_STRING);
        public static final CardType JCB = new CardType(8, JCB_STRING);
        private CardType(final int value, final String name) {
            super(value, name);
        }        
    }
    
    abstract static class NameValue {
        static final int UNDEFINED_VALUE = -1;
        static final String UNDEFINED_STRING = "Undefined";
        
        private int value;
        private String name;
        
        NameValue(final int value, final String name) {
            this.value = value;
            this.name = name;
        }
        
        public int getValue() {
            return this.value;
        }
        
        public void setValue(final int value) {
            this.value = value;
        }
        
        public String getName() {
            return this.name;
        }
        
        public void setName(final String name) {
            this.name = name;
        }
        
        public String toString() {
            final StringBuilder bdr = new StringBuilder();
            return bdr.append(this.value).append(" : ").append(this.name).toString();
        }
    }
    
    private CardTypeUtil(final int value, final String name) {
    }
}
