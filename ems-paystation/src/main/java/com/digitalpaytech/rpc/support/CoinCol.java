package com.digitalpaytech.rpc.support;

import java.util.HashMap;
import java.util.Map;
import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;

// Migrated from EMS 6.3.11

public class CoinCol {
    private static final String CENTS_5 = "5";
    private static final String CENTS_10 = "10";
    private static final String CENTS_25 = "25";
    private static final String DOLLAR_1 = "100";
    private static final String DOLLARS_2 = "200";
    
    
    private Map<String, Integer> coinMap;
    private String coinCollection;
    
    
    public CoinCol() {
        coinMap = new HashMap<String, Integer>();
    }
    
    
    public CoinCol(String collection) {
        coinMap = new HashMap<String, Integer>();
        this.coinCollection = collection;
        parse();
    }


    public void parse() {
        if (StringUtils.isBlank(coinCollection)) {
            return;
        }
        
        //e.g. 25:2,10:4,100:1 -> [25:2], [10:4], [100:1]
        String[] dominationCounts = coinCollection.split(",");
        for (String str : dominationCounts) {
            String[] dominationCount = str.split(":");
            coinMap.put(dominationCount[0].trim(), Integer.parseInt(dominationCount[1].trim()));
        }
    }
    
    
    public String getCoinCollection() {
        return coinCollection;
    }
    
    public void setCoinCollection(String coinCollection) {
        this.coinCollection = coinCollection;
    }
    
    public int getCount005() {
        // 5 = "$0.05"
        return returnZeroIfNull(coinMap.get(CENTS_5));
    }
    
    public int getCount010() {
        // 10 = "$0.10"
        return returnZeroIfNull(coinMap.get(CENTS_10));
    }
    
    public int getCount025() {
        // 25 = "$0.25"
        return returnZeroIfNull(coinMap.get(CENTS_25));
    }
    
    public int getCount100() {
        // 100 = "$1"
        return returnZeroIfNull(coinMap.get(DOLLAR_1));
    }
    
    public int getCount200() {
        // coin200 = "$2"
        return returnZeroIfNull(coinMap.get(DOLLARS_2));
    }
    
    public int getAmount005() {
        return 5;
    }

    public int getAmount010() {
        return 10;
    }
    
    public int getAmount025() {
        return 25;
    }
    
    public int getAmount100() {
        return 100;
    }
    
    public int getAmount200() {
        return 200;
    }
    
    public int getCoinDollars() {
        int total5 = getCount005() * Integer.parseInt(CENTS_5);
        int total10 = getCount010() * Integer.parseInt(CENTS_10);
        int total25 = getCount025() * Integer.parseInt(CENTS_25);
        int total100 = getCount100() * Integer.parseInt(DOLLAR_1);
        int total200 = getCount200() * Integer.parseInt(DOLLARS_2);
        
        return total5 + total10 + total25 + total100 + total200;
    }

    /**
     * @return BigDecimal e.g. 15.75, instead of 1575
     */
    public BigDecimal getCoinDollarsDecimal() {
        BigDecimal total = new BigDecimal(getCoinDollars());
        total = total.divide(new BigDecimal(100));
        return total.setScale(2);
    }
    
    public int getCoinCount() {
        return getCount005() + getCount010() + getCount025() + getCount100() + getCount200();
    }
    
    private int returnZeroIfNull(Integer intObj) {
        if (intObj == null) {
            return 0;
        }
        return intObj;
    }
}
