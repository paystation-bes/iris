package com.digitalpaytech.rpc.support;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

public final class LinuxConfigNotificationInfo {
    
    @XStreamAlias("PointOfSaleId")
    private int pointOfSaleId;
    
    @XStreamAlias("SnapShotId")
    private String snapShotId;
    
    @XStreamImplicit
    private List<LinuxConfigFileNotificationInfo> fileInfo;
    
    public LinuxConfigNotificationInfo(int pointOfSaleId, String snapShotId, List<LinuxConfigFileNotificationInfo> fileInfo) {
        this.pointOfSaleId = pointOfSaleId;
        this.snapShotId = snapShotId;
        this.fileInfo = fileInfo;
    }

    public int getPointOfSaleId() {
        return this.pointOfSaleId;
    }

    public void setPointOfSaleId(int pointOfSaleId) {
        this.pointOfSaleId = pointOfSaleId;
    }

    public String getSnapShotId() {
        return snapShotId;
    }

    public void setSnapShotId(String snapShotId) {
        this.snapShotId = snapShotId;
    }

    public List<LinuxConfigFileNotificationInfo> getFileInfo() {
        return fileInfo;
    }

    public void setFileInfo(List<LinuxConfigFileNotificationInfo> fileInfo) {
        this.fileInfo = fileInfo;
    }
    
    
    
}
