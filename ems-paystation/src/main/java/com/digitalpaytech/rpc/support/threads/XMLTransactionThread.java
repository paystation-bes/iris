package com.digitalpaytech.rpc.support.threads;

import java.util.Vector;

import org.apache.log4j.Logger;

import com.digitalpaytech.util.EMSThread;


/*
 * Copied from EMS 6.3.11 com.digitalpioneer.appservice.transaction.offline.XMLTransactionThread
 */

public class XMLTransactionThread extends EMSThread {
    private static final Logger LOGGER = Logger.getLogger(XMLTransactionThread.class);
    
    private Vector<String> transactions = new Vector<String>();
    
    private int threadId;
    private XMLTransactionThreadRunner xmlTransactionThreadRunner;
    
    public XMLTransactionThread(final int threadId, final XMLTransactionThreadRunner xmlTransactionThreadRunner) {
        super(XMLTransactionThread.class.getName());
//        setDaemon(true);
        this.threadId = threadId;
        this.xmlTransactionThreadRunner = xmlTransactionThreadRunner;
    }
    
    public synchronized void addTransactionToQueue(final String hpzFileFullName) {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("+++ add hpz file(" + hpzFileFullName + ") to hpz thread " + threadId + " +++");
        }
        this.transactions.add(hpzFileFullName);
        // Wake up processor
        synchronized (this) {
            this.notify();
        }
    }
    
    private synchronized String getNextTransactionFromQueue() {
        if (isTransactionQueueEmpty()) {
            return null;
        }
        
        if (xmlTransactionThreadRunner.isDisableProcessing()) {
            if (LOGGER.isInfoEnabled()) {
                LOGGER.info("Don't processing next transaction file, cause flag be set in EmsProperties table");
            }
            this.transactions.clear();
            return null;
        }
        final String zipFileFullName = this.transactions.remove(0);
        return zipFileFullName;
    }
    
    private synchronized boolean isTransactionQueueEmpty() {
        return this.transactions.isEmpty();
    }
    
    @Override
    public final void run() {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Starting XMLTransactionThread: " + this.threadId);
        }
        String hpzFileFullName = null;
        
        while (!isShutdown()) {
            hpzFileFullName = getNextTransactionFromQueue();
            if (hpzFileFullName != null) {
                this.xmlTransactionThreadRunner.processXMLTransactions(hpzFileFullName);
            }
            // if no more transactions, wait.
            if (isTransactionQueueEmpty()) {
                try {
                    synchronized (this) {
                        wait();
                    }
                    if (LOGGER.isInfoEnabled()) {
                        LOGGER.info("+++ hpz thread " + threadId + " was waked up +++");
                    }
                    
                    final int sleepTime = 2000;                    
                    Thread.sleep(sleepTime);
                
                } catch (InterruptedException e) {
                    // Empty block
                }
            }
        }
    }
    
}
