/*
 * Copyright @ 2002 Digital Pioneer. All Rights Reserved.
 */
package com.digitalpaytech.rpc.support;

import org.springframework.web.context.WebApplicationContext;

import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.SystemException;
import com.digitalpaytech.rpc.ps2.PS2XMLBuilderBase;
import com.digitalpaytech.rpc.ps2.PS2XMLHandler;
import com.digitalpaytech.util.xml.XmlHandler;

public final class XMLHandlerFactory {
    public static final String PS2_PASSWORD_VAL = "zaq123$ESZxsw234%RDX";
    // Factory types
    public static final int PS2 = 2;

    private static final String PS2_USERID_VAL = "emsadmin";
    
    
    private XMLHandlerFactory() {
        // Empty private constructor
    }
    
    public static XmlHandler getXMLHandler(final byte[] message, final WebApplicationContext ctx) throws ApplicationException, SystemException {
        PS2XMLHandler xmlHandler = null;
        
        // get the first element for the message and return it
        final XMLUtilityHandler utilityHandler = new XMLUtilityHandler();
        utilityHandler.process(message);
        final String mainElement = utilityHandler.getMainElement();
        final String userName = utilityHandler.getUserName();
        final String password = utilityHandler.getPassword();
        final String version = utilityHandler.getVersion();
        
        // authenticate message and initialize xml handler
        if (mainElement == null) {
            return null;
        } else if (mainElement.equals(PS2XMLBuilderBase.XML_PAYSTATION2_DTD_NAME) && PS2_USERID_VAL.equals(userName) && PS2_PASSWORD_VAL.equals(password)) {
            xmlHandler = new PS2XMLHandler();
        }
        if (xmlHandler != null) {
            xmlHandler.setWebApplicationContext(ctx);
            xmlHandler.setVersion(version);
        }
        
        return xmlHandler;
    }
}
