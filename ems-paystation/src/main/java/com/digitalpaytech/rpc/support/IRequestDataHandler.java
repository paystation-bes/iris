package com.digitalpaytech.rpc.support;

import org.springframework.web.context.WebApplicationContext;


public interface IRequestDataHandler extends IDataHandler {
	public String getPosSerialNumber();

	public String getMessageNumber();

	public void setWebApplicationContext(WebApplicationContext ctx);

}