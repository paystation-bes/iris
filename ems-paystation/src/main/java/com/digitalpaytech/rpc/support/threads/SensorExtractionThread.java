package com.digitalpaytech.rpc.support.threads;

import java.util.Collection;
import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;

import com.digitalpaytech.domain.RawSensorData;
import com.digitalpaytech.service.ArchiveService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.RawSensorDataService;
import com.digitalpaytech.service.paystation.EventSensorService;
import com.digitalpaytech.util.EMSThread;

/**
 * Extract Raw Sensor Data from database and put them into queue. SensorProcessingThread(s) will process
 * sensor data in queue.
 */

public class SensorExtractionThread extends EMSThread {
    private final static Logger logger = Logger.getLogger(SensorExtractionThread.class);
    //TODO increase wait times after back log is cleared
    //    private final static int MIN_LOAD_INTERVAL_MS = 15 * 60 * 1000; // minimum db load interval in ms
    private final static int MIN_LOAD_INTERVAL_MS = 10 * 1000; // minimum db load interval in ms
    private final static int WAIT_TIME_MS = 5 * 1000; // minimum db load interval in ms
    
    private static SensorExtractionThread theSensorExtractionThread = null;
    
    private EventSensorService eventSensorService;
    private RawSensorDataService rawSensorDataService;
    private SensorProcessingThread[] sensorProcessingThreads;
    
    private int numProcessingThreads = Integer.parseInt(EmsPropertiesService.DEFAULT_SENSOR_PROCESS_THREAD_COUNT);
    private int batchSize = Integer.parseInt(EmsPropertiesService.DEFAULT_SENSOR_BATCH_SIZE);
    
    private boolean loadData = true;
    private ConcurrentLinkedQueue<RawSensorData> rawSensorQueue = new ConcurrentLinkedQueue<RawSensorData>();
    
    /**
     * The constructor will not start the threading unlike EMS 6.3.11. SensorThreadsRunner's @PostConstruct private method
     * will create an instance of SensorExtractionThread and call 'start'.
     */
    public SensorExtractionThread(EventSensorService eventSensorService, RawSensorDataService rawSensorDataService, PointOfSaleService pointOfSaleService, EmsPropertiesService emsPropertiesService,
            ArchiveService archiveService) {
        super(SensorExtractionThread.class.getName());
        
        setDaemon(true);
        setPriority(Thread.NORM_PRIORITY);
        this.eventSensorService = eventSensorService;
        this.rawSensorDataService = rawSensorDataService;
        this.numProcessingThreads = Integer.parseInt(emsPropertiesService.getPropertyValue(EmsPropertiesService.SENSOR_PROCESSING_THREAD_COUNT,
                                                                                           EmsPropertiesService.DEFAULT_SENSOR_PROCESS_THREAD_COUNT));
        this.batchSize = Integer.parseInt(emsPropertiesService.getPropertyValue(EmsPropertiesService.SENSOR_DATA_BATCH_COUNT,
                                                                                EmsPropertiesService.DEFAULT_SENSOR_BATCH_SIZE));
        // Create SensorProcessingThreads and its constructors will start threading.
        sensorProcessingThreads = new SensorProcessingThread[numProcessingThreads];
        for (int i = 0; i < numProcessingThreads; i++) {
            sensorProcessingThreads[i] = new SensorProcessingThread(i, eventSensorService, pointOfSaleService, rawSensorDataService, archiveService, this);
        }
        
        //start();
        theSensorExtractionThread = this;
    }
    
    public static SensorExtractionThread getInstance() {
        return theSensorExtractionThread;
    }
    
    public void run() {
        int state = 0; // state flag so that debug message only print when state changes
        Date lastLoadFromDb = new Date();
        Date lastExceptionLog = null;
        int exceptionCount = 0;
        boolean notifyThreads;
        if (logger.isInfoEnabled()) {
            logger.info("Raw sensor data extraction thread started.");
        }
        while (!this.isShutdown()) {
            notifyThreads = false;
            try {
                boolean processing = false;
                
                for (int i = 0; i < sensorProcessingThreads.length; i++) {
                    if (sensorProcessingThreads[i].isProcessing()) {
                        processing = true;
                        break;
                    }
                }
                synchronized (this) {
                    if (rawSensorQueue.isEmpty()) {
                        if ((!processing && loadData) || lastLoadFromDb.getTime() < (System.currentTimeMillis() - MIN_LOAD_INTERVAL_MS)) {
                            Collection<RawSensorData> sensorEvents = this.rawSensorDataService.findRawSensorDataForServer(batchSize);
                            if (sensorEvents == null || sensorEvents.isEmpty()) {
                                if (state != 1) {
                                    if (logger.isDebugEnabled()) {
                                        logger.debug("No raw sensor data in database. Waiting for sensor data.");
                                    }
                                }
                                state = 1;
                                loadData = false;
                            } else {
                                if (logger.isDebugEnabled()) {
                                    logger.debug("Adding " + sensorEvents.size() + " raw sensor data to queue.");
                                }
                                rawSensorQueue.addAll(sensorEvents);
                                notifyThreads = true;
                            }
                            lastLoadFromDb = new Date();
                        } else if (!loadData && !processing) {
                            if (state != 2) {
                                if (logger.isDebugEnabled()) {
                                    logger.debug("No raw sensor data in database. Waiting for sensor data.");
                                }
                            }
                            state = 2;
                        } else {
                            if (state != 3) {
                                if (logger.isDebugEnabled()) {
                                    logger.debug("Wait until the last raw sensor data batch is processed.");
                                }
                            }
                            state = 3;
                        }
                    } else {
                        if (state != 3) {
                            if (logger.isDebugEnabled()) {
                                logger.debug("Wait until the last raw sensor data batch is processed.");
                            }
                        }
                        state = 3;
                        //TODO needed for cleanup;
                        sleep(1100);
                    }
                }
                if (notifyThreads) {
                    for (int i = 0; i < sensorProcessingThreads.length; i++) {
                        sensorProcessingThreads[i].wakeup();
                    }
                }
            } catch (Exception e) {
                // any other exceptions
                // should not reach here
                // put in as a fail safe
                exceptionCount++;
                if (lastExceptionLog == null || new Date(System.currentTimeMillis() - 10 * 60 * 1000).after(lastExceptionLog)) {
                    logger.error(exceptionCount + " sensor extraction exception since last error log.", e);
                    lastExceptionLog = new Date();
                    exceptionCount = 0;
                }
            }
            waitForEvents();
        }
    }
    
    private synchronized void waitForEvents() {
        try {
            wait(WAIT_TIME_MS);
        } catch (InterruptedException e) {
            if (logger.isDebugEnabled()) {
                logger.debug("InterruptedException is called in 'waitForEvents'");
            }
        }
    }
    
    /**
     * Notify that a processing thread has done processing the queue. Called by SensorProcessingThread when
     * queue is empty.
     */
    public synchronized void doneProcessing() {
        for (int i = 0; i < sensorProcessingThreads.length; i++) {
            if (sensorProcessingThreads[i] != null && sensorProcessingThreads[i].isProcessing()) {
                return;
            }
        }
        // all done processing, notify
        notify();
    }
    
    /**
     * Notify that there are new data available in database
     */
    public void dataAvailable() {
        synchronized (this) {
            loadData = true;
        }
    }
    
    public ConcurrentLinkedQueue<RawSensorData> getRawSensorDataQueue() {
        return rawSensorQueue;
    }
    
}
