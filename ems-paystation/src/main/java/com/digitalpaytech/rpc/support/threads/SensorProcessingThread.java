package com.digitalpaytech.rpc.support.threads;

import java.util.Date;
import java.util.NoSuchElementException;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.RawSensorData;
import com.digitalpaytech.dto.paystation.EventData;
import com.digitalpaytech.service.ArchiveService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.RawSensorDataService;
import com.digitalpaytech.service.paystation.EventSensorService;
import com.digitalpaytech.util.EMSThread;
import com.digitalpaytech.util.WebCoreUtil;

public class SensorProcessingThread extends EMSThread {
    private final static Logger logger = Logger.getLogger(SensorProcessingThread.class);

    //TODO increase wait times after back log is cleared
    private final static int MIN_LOAD_INTERVAL_MS = 10 * 1000; // minimum db load interval in ms
    private final static int WAIT_TIME_MS = 1 * 1000; // minimum db load interval in ms

    private int threadId;
    private boolean processing = false;
    private ConcurrentLinkedQueue<RawSensorData> rawSensorQueue;
    
    private ArchiveService archiveService;
    private EventSensorService eventSensorService;
    private PointOfSaleService pointOfSaleService;
    private RawSensorDataService rawSensorDataService;
    private SensorExtractionThread sensorExtractionThread;
    
    public SensorProcessingThread(int threadId, 
                                  EventSensorService eventSensorService, 
                                  PointOfSaleService pointOfSaleService, 
                                  RawSensorDataService rawSensorDataService,
                                  ArchiveService archiveService,
                                  SensorExtractionThread sensorExtractionThread) {
        
        super(SensorProcessingThread.class.getName());
        setDaemon(true);
        setPriority(Thread.NORM_PRIORITY);
        this.threadId = threadId;
        this.pointOfSaleService = pointOfSaleService;
        this.eventSensorService = eventSensorService;
        this.rawSensorDataService = rawSensorDataService;
        this.sensorExtractionThread = sensorExtractionThread;
        this.archiveService = archiveService;
        this.rawSensorQueue = sensorExtractionThread.getRawSensorDataQueue();
        // The constructor will start threading.
        this.start();
    }
    
    
    public void run() {
        int state = 0; // state flag so that debug message only print when state changes
        Date lastExceptionLog = null;
        int exceptionCount = 0;
        if (logger.isInfoEnabled()) {
            logger.info("Raw sensor processing thread " + threadId + " started.");
        }
        while (!this.isShutdown()) {
            try {
                if (!rawSensorQueue.isEmpty()) {
                    processing = true;
                    RawSensorData sensor = rawSensorQueue.remove();
                    if (sensor != null) {
                        EventData event = new EventData();
                        event.setAction(sensor.getAction());
                        event.setInformation(sensor.getInformation());
                        event.setTimeStamp(sensor.getDateField());
                        event.setType(sensor.getType());
                        
                        PointOfSale ps = pointOfSaleService.findPointOfSaleBySerialNumber(sensor.getSerialNumber());
                        if (ps != null) {
                            state = 1;
                            try {
                                eventSensorService.processRawSensorData(event, sensor, threadId);
                            } catch (Exception e) {
                                archiveService.handleRawSensorDataTransformationError(e, event, sensor, exceptionCount);
                            }
                        } else {
                            archiveService.archiveRawSensorData(sensor);
                        }
                    }
                } else {
                    // the queue is empty
                    if (state != 2) {
                        if (logger.isDebugEnabled()) {
                            logger.debug("Thread " + threadId + " waiting for sensor data to process.");
                        }
                    }
                    state = 2;
                    processing = false;
                    sensorExtractionThread.doneProcessing();
                    waitForMoreData();
                }
            } catch (NoSuchElementException e) {
                // the queue is empty
                if (state != 2) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Thread " + threadId + " waiting for sensor data to process.");
                    }
                }
                state = 2;
                processing = false;
                sensorExtractionThread.doneProcessing();
                waitForMoreData();
            } catch (Exception e) {
                // any other exceptions
                // should not reach here
                // put in as a fail safe
                exceptionCount++;
                if (lastExceptionLog == null || new Date(System.currentTimeMillis() - 10 * 60 * 1000).after(lastExceptionLog)) {
                    logger.error(exceptionCount + " sensor processing exception since last error log.", e);
                    lastExceptionLog = new Date();
                    exceptionCount = 0;
                }
            }
        }
    }
    
    private synchronized void waitForMoreData() {
        if (!rawSensorQueue.isEmpty()) {
            processing = true;
            return;
        }
        
        try {
            processing = false;
            wait(WAIT_TIME_MS);
        } catch (InterruptedException e) {
            // Empty block
        }
    }
    
    public synchronized void wakeup() {
        notify();
    }
    
    protected boolean isProcessing() {
        return processing;
    }
    
}
