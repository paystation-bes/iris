package com.digitalpaytech.rpc.ps2;

import org.apache.log4j.Logger;

import com.digitalpaytech.rpc.support.MessageTags;
import com.digitalpaytech.util.xml.XMLBuilderBase;


public class HeartBeatHandler extends BaseHandler {

    private static Logger log = Logger.getLogger(HeartBeatHandler.class);
    
    public HeartBeatHandler(String messageNumber) {
        super(messageNumber);
    }

    public String getRootElementName() {
        return MessageTags.HEART_BEAT_TAG_NAME;
    }
    
    public void process(XMLBuilderBase xmlBuilder) {
        long start_time = System.currentTimeMillis();
        process((PS2XMLBuilder) xmlBuilder);

        if (log.isInfoEnabled()) {
            float processing_time = (System.currentTimeMillis() - start_time) / MILLISECOND_TO_SECOND;
            StringBuilder bdr = new StringBuilder();
            bdr.append("Returning HeartbeatResponse to PointOfSale serialNumber: ").append(posSerialNumber).append(" after ").append(processing_time).append(" seconds.");
            log.info(bdr.toString());
        }
    }

    private void process(PS2XMLBuilder xmlBuilder) {
        // Validate PointOfSale
        if (!validatePOS(xmlBuilder)) {
            return;
        }

        xmlBuilder.insertAcknowledge(messageNumber);

        // check for new lot setting
        checkForNotifications(xmlBuilder);
    }
}
