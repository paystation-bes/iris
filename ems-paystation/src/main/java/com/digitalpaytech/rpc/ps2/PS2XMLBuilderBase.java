/**
 * Copyright @ 2002 Digital Pioneer. All Rights Reserved.
 */
package com.digitalpaytech.rpc.ps2;


import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.digitalpaytech.util.xml.XMLBuilderBase;

public class PS2XMLBuilderBase extends XMLBuilderBase {
    public static final String XML_PAYSTATION2_DTD_NAME = "Paystation_2";
    public static final String XML_PAYSTATION2_DTD_VERSION = "0.0.1";
    public static final String MESSAGE_FOR_EMS_ELEMENT_NAME_STRING = "Message4EMS";
    public static final String MESSAGE_FOR_PAYSTATION2_ELEMENT_NAME_STRING = "Message4Paystation2";
    public static final String VERSION_ATTRIBUTE_NAME_STRING = "Version";
    public static final String USER_ID_ATTRIBUTE_NAME_STRING = "UserId";
    public static final String PASSWORD_ATTRIBUTE_NAME_STRING = "Password";

    //  private final static Logger logger__ = Logger.getLogger(PS2XMLBuilderBase.class);
  
    public PS2XMLBuilderBase(final String dtdLocation, final String messageDestination) {
        super(dtdLocation, messageDestination);
    }

    protected final Document createDocument(final String userId, final String password) {
        final Document xmlDocument = documentBuilder.newDocument();

        createDocumentBase(xmlDocument, userId, password);

        return xmlDocument;
    }

    protected final Element createDocumentBase(final Document xmlDocument, final String userId, final String userPassword) {
        final Element rootElement = createNamedElement(xmlDocument, xmlDocument, XML_PAYSTATION2_DTD_NAME);

        // zxzx Can we have this created automatically when validating against the DTD.
        rootElement.setAttribute(VERSION_ATTRIBUTE_NAME_STRING, XML_PAYSTATION2_DTD_VERSION);

        rootElement.setAttribute(USER_ID_ATTRIBUTE_NAME_STRING, userId);

        rootElement.setAttribute(PASSWORD_ATTRIBUTE_NAME_STRING, userPassword);

        return createNamedElement(xmlDocument, rootElement, xmlMessageDestination);
    }
}
