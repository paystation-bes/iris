package com.digitalpaytech.rpc.ps2;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;

import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.xml.XMLBuilderBase;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.rpc.support.MessageTags;

/**
 * LinuxBadCardListSuccessHandler should be called after pay station downloaded bad card list directly from CPS 
 * and then it sends acknowledgement message to Iris to turn off notification.
 */

public class LinuxBadCardListSuccessHandler extends BaseHandler {
    private static final Logger LOG = Logger.getLogger(LinuxBadCardListSuccessHandler.class);
    
    public LinuxBadCardListSuccessHandler(final String messageNumber) {
        super(messageNumber);
    }
    
    @Override
    public final String getRootElementName() {
        return MessageTags.LINUX_BAD_CARD_LIST_SUCCESS;
    }
    
    @Override
    public final void process(final XMLBuilderBase xmlBuilder) {
        final long startTime = System.currentTimeMillis();
        process((PS2XMLBuilder) xmlBuilder);
        
        if (LOG.isInfoEnabled()) {
            final float processingTime = (System.currentTimeMillis() - startTime) / StandardConstants.FLOAT_THOUSAND;
            final StringBuilder bdr = new StringBuilder();
            bdr.append("Returning LinuxBadCardListSuccessHandler to PointOfSale serialNumber: ").append(super.posSerialNumber).append(" after ")
                    .append(processingTime).append(" seconds.");
            LOG.info(bdr.toString());
        }
    }
 
    private void process(final PS2XMLBuilder xmlBuilder) {
        if (!validatePOS(xmlBuilder)) {
            return;
        }
        
        final PosServiceState posServiceState = this.pointOfSale.getPosServiceState();
        if (posServiceState.isIsNewBadCardList()) {
            posServiceState.setIsNewBadCardList(false);
            this.posServiceStateService.updatePosServiceState(posServiceState);
        }
        xmlBuilder.insertAcknowledge(messageNumber);
    }

    
    public final void setWebApplicationContext(final WebApplicationContext ctx) {
        super.setWebApplicationContext(ctx);
    }
}
