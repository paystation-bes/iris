package com.digitalpaytech.rpc.ps2;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;

import com.digitalpaytech.domain.Coupon;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.rpc.support.MessageTags;
import com.digitalpaytech.service.CouponService;
import com.digitalpaytech.util.xml.XMLBuilderBase;

public class CouponAuthRequestHandler extends BaseHandler {
    private final static Logger log = Logger.getLogger(CouponAuthRequestHandler.class);
    /** The coupon number provided by the parker. */
    private String couponCode;
    /** The stall number the user is parked at. Omit for PND or PBL permits. */
    private String stallNumber;
    
    private CouponService couponService = null;
    
    public CouponAuthRequestHandler(String messageNumber) {
        super(messageNumber);
    }
    
    public String getRootElementName() {
        return MessageTags.COUPON_AUTHORIZE_REQUEST_TAG_NAME;
    }
    
    public void process(XMLBuilderBase xmlBuilder) {
        long start_time = System.currentTimeMillis();
        String couponAuthString = process((PS2XMLBuilder) xmlBuilder);
        
        if (log.isInfoEnabled()) {
            float processing_time = (System.currentTimeMillis() - start_time) / MILLISECOND_TO_SECOND;
            StringBuilder bdr = new StringBuilder();
            if (pointOfSale == null) {
                bdr.append("PointOfSale serial number is incorrect, unable to retrieve record.");
            } else {
                bdr.append("Processing CouponAuthRequestHandler(").append(couponCode).append(", ").append(stallNumber);
                bdr.append(") for POS: ").append(super.getPosSerialNumber());
                bdr.append("\r\nReturning '").append(couponAuthString).append("' after ").append(processing_time).append(" seconds.");
            }
            log.info(bdr.toString());
        }
    }
    
    private String process(PS2XMLBuilder xmlBuilder) {
        // Validate POS & couponCode.
        if (!validatePOS(xmlBuilder) || !validateCouponCodeStallNumber()) {
            return null;
        }
        
        try {
            int stallNum = 0;
            if (StringUtils.isNotBlank(stallNumber)) {
                stallNum = Integer.parseInt(stallNumber);
            }
            
            Coupon coupon = couponService.processCouponAuthorization(pointOfSale, couponCode.trim().toUpperCase(), stallNum);
            if (coupon.getId() != null) {
                String type;
                String dollar = null;
                String percent = null;
                if (coupon.getDollarDiscountAmount() > 0) {
                    type = "Dollar";
                    dollar = String.valueOf(new BigDecimal(coupon.getDollarDiscountAmount()).divide(new BigDecimal(100)));
                } else {
                    type = "Percent";
                    percent = String.valueOf(coupon.getPercentDiscount());
                }
                xmlBuilder.createCouponAuthResponse(this, couponCode, "Success", type, dollar, percent);
            } else {
                xmlBuilder.createCouponAuthResponse(this, couponCode, coupon.getDescription(), null, null, null);
            }
            return coupon.getDescription();
        } catch (Exception e) {
        	if (e instanceof InvalidDataException) {
        		xmlBuilder.createCouponAuthResponse(this, couponCode, AuthorizeRequestHandler.INVALID_STRING, null, null, null);
        		return AuthorizeRequestHandler.INVALID_STRING;
        	}
        	
        	StringBuilder bdr = new StringBuilder();
            bdr.append("Unable to process CouponAuthRequestHandler(").append(couponCode).append(", ");
            bdr.append(stallNumber).append(") for POS: ").append(super.getPosSerialNumber());
            processException(bdr.toString(), e);
            xmlBuilder.createCouponAuthResponse(this, couponCode, EMS_UNAVAILABLE_STRING, null, null, null);
            return (EMS_UNAVAILABLE_STRING);
        }
    }
    
    /**
     * When authorizing a coupon, EMS should decline a dollar based coupon if the pay station uses the old request format.
     * 
     * @return boolean false if it's invalid.
     */
    private boolean validateCouponCodeStallNumber() {
        if (StringUtils.isBlank(couponCode) || couponCode.indexOf(":") != -1) {
            return false;
        }
        if (StringUtils.isNotBlank(stallNumber) && !StringUtils.isNumeric(stallNumber)) {
            return false;
        }
        return true;
    }
    
    public String getCouponCode() {
        return couponCode;
    }
    
    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }
    
    public String getStallNumber() {
        return stallNumber;
    }
    
    public void setStallNumber(String stallNumber) {
        this.stallNumber = stallNumber;
    }
    
    public void setWebApplicationContext(WebApplicationContext ctx) {
        super.setWebApplicationContext(ctx);
        this.couponService = (CouponService) ctx.getBean("couponService");
    }
}
