/*
 * Copyright @ 2002 Digital Pioneer. All Rights Reserved.
 */
package com.digitalpaytech.rpc.ps2;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.xml.sax.Attributes;

import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.SystemException;
import com.digitalpaytech.rpc.support.IDataHandler;
import com.digitalpaytech.util.xml.XmlSaxHandlerBase;

public final class PS2XMLHandler extends XmlSaxHandlerBase {
    private static Logger logger = Logger.getLogger(PS2XMLHandler.class);
    private static final String SET_STRING = "set";
    private static final String HANDLER_STRING = "Handler";
    private static final String DATA_HANDLER_PACKAGE = "com.digitalpaytech.rpc.ps2";
    private static final String INVALID_REQUEST_STRING = "InvalidRequest";
    private static final String BRACKET_OPEN = "(";
    private static final String BRACKET_CLOSE = ")";
    private static final int INIT_BUFFER_SIZE = 50;
    private static final int THIRD_LEVEL = 3;
    
    protected WebApplicationContext ctx;
    
    private String messageNumber;
    private IDataHandler currentDataHandler;
    private String currentElementName;
    private StringBuffer currentElementValue = new StringBuffer(INIT_BUFFER_SIZE);
    
    private PS2XMLBuilder xmlBuilder;
    private int elementLevel;
    private String version;
    
    private String requestType;
    private boolean isStoreForward;
    private int paymentType;
    private int permitIssueType;
    
    public PS2XMLHandler() throws ApplicationException, SystemException {
        this.xmlBuilder = new PS2XMLBuilder(PS2XMLBuilder.MESSAGE_FOR_PAYSTATION2_ELEMENT_NAME_STRING);
        this.xmlBuilder.initialize("user_id", "user_password");
    }
    
    /*
     * @Autowired private RpcPaystation2AppService paystationAppService;
     * 
     * public void getPaystationAppService(RpcPaystation2AppService
     * paystationAppService) { this.paystationAppService = paystationAppService;
     * }
     */
    public boolean isStoreForward() {
        return this.isStoreForward;
    }
    
    public int getPaymentType() {
        return this.paymentType;
    }
    
    public int getPermitIssueType() {
        return this.permitIssueType;
    }
    
    public String getCurrentRequestType() {
        return this.requestType;
    }

    public void characters(final char[] document, final int offset, final int length) {
        if (this.currentDataHandler != null) {
            this.currentElementValue.append(document, offset, length);
        }
    }
    
    public void startDocument() {
        this.xmlBuilder.setOriginalMessage(requestMessage);
    }
    
    public void startElement(final String uri, final String localName, final String qName, final Attributes elementAttributes) {
        this.elementLevel++;
        this.currentElementName = qName;
        
        // Only elements at the 3rd level correspond to data handlers
        if ((this.elementLevel == THIRD_LEVEL) && (this.currentDataHandler == null)) {
            this.currentDataHandler = getDataHandler(qName, elementAttributes.getValue(0));
            this.requestType = qName;
        }
    }
    
    public void endElement(final String uri, final String localName, final String qName) {
        if (this.currentDataHandler != null) {
            // This is the end of the current data handler
            if ((this.elementLevel == THIRD_LEVEL) && (qName.equals(this.currentDataHandler.getRootElementName()))) {
                if (!this.xmlBuilder.isErrorState(this.messageNumber)) {
                    this.currentDataHandler.process(this.xmlBuilder);
                    if (this.requestType.equals("Transaction")) {
                        final TransactionHandler transactionHandler = (TransactionHandler) this.currentDataHandler;
                        this.isStoreForward = transactionHandler.isStoreForward();
                        try {
                            this.paymentType = transactionHandler.getPurchase().getPaymentType().getId();
                        } catch (NullPointerException e) {
                            this.paymentType = -1;
                        }
                        try {
                            this.permitIssueType = transactionHandler.getPermit().getPermitIssueType().getId();
                        } catch (NullPointerException e) {
                            this.permitIssueType = -1;
                        }
                    }
                }
                
                this.currentDataHandler = null;
                this.messageNumber = null;
            } else {
                // This is the end of an element
                final String currentElementValueString = this.currentElementValue.toString().trim();
                if (currentElementValueString.length() > 0 && !this.xmlBuilder.isErrorState(this.messageNumber)) {
                    setByVariableName(this.currentDataHandler, this.currentElementName, currentElementValueString);
                } else {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Value is blank: " + this.currentElementValue);
                    }
                }
            }
            this.currentElementValue = new StringBuffer(INIT_BUFFER_SIZE);
        } else {
            // if (currentDataHandler == null)
            this.messageNumber = null;
        }
        
        this.currentElementName = null;
        this.elementLevel--;
    }
    
    public void endDocument() {
        if (this.xmlBuilder.isValidPaystation()) {
            responseMessage = this.xmlBuilder.toString();
        } else {
            // if the paystation is not valid return an empty string
            responseMessage = PS2XMLBuilder.ERROR_INVALID_PAYSTATION;
        }
    }
    
    public IDataHandler getDataHandler(final String dataObjectName, final String messageNumberInput) {
        IDataHandler dataHandler = null;
        final String dataHandlerName = DATA_HANDLER_PACKAGE + "." + dataObjectName + HANDLER_STRING;
        
        if (logger.isDebugEnabled()) {
            logger.debug("Getting new " + dataObjectName + HANDLER_STRING + ", name: " + dataHandlerName);
        }
        
        this.messageNumber = messageNumberInput;
        final String failedMsg = "Failed to create: ";
        
        try {
            final Class classObject = Class.forName(dataHandlerName);
            
            final Constructor constructor = classObject.getConstructor(new Class[] { String.class });
            
            dataHandler = (IDataHandler) constructor.newInstance(new Object[] { messageNumberInput });
            dataHandler.setWebApplicationContext(this.ctx);
            dataHandler.setVersion(getVersion());
            
        } catch (ClassNotFoundException e) {
            processException(failedMsg + dataHandlerName, e);
            this.xmlBuilder.insertErrorMessage(messageNumberInput, INVALID_REQUEST_STRING);
        } catch (NoSuchMethodException e) {
            processException(failedMsg + dataHandlerName, e);
            this.xmlBuilder.insertErrorMessage(messageNumberInput, INVALID_REQUEST_STRING);
        } catch (InvocationTargetException e) {
            processException(failedMsg + dataHandlerName, e);
            this.xmlBuilder.insertErrorMessage(messageNumberInput, INVALID_REQUEST_STRING);
        } catch (InstantiationException e) {
            processException(failedMsg + dataHandlerName, e);
            this.xmlBuilder.insertErrorMessage(messageNumberInput, INVALID_REQUEST_STRING);
        } catch (IllegalAccessException e) {
            processException(failedMsg + dataHandlerName, e);
            this.xmlBuilder.insertErrorMessage(messageNumberInput, INVALID_REQUEST_STRING);
        }
        
        return dataHandler;
    }
    
    private void setByVariableName(final Object targetObject, final String xmlVariableName, final String xmlVariableValue) {
        final String methodName = SET_STRING + xmlVariableName;
        
        try {
            final Method methodToInvoke = targetObject.getClass().getMethod(methodName, new Class[] { String.class });
            
            methodToInvoke.invoke(targetObject, new Object[] { xmlVariableValue });
        } catch (NoSuchMethodException e) {
            logger.warn("Ignoring method call: " + methodName + BRACKET_OPEN + xmlVariableValue + BRACKET_CLOSE);
        } catch (InvocationTargetException e) {
            processException("Failed to call method: " + methodName + BRACKET_OPEN + xmlVariableValue + BRACKET_CLOSE, e);
            this.xmlBuilder.insertErrorMessage(this.messageNumber, INVALID_REQUEST_STRING);
        } catch (IllegalAccessException iae) {
            processException("Illegal access to call method: " + methodName + BRACKET_OPEN + xmlVariableValue + BRACKET_CLOSE, iae);
            this.xmlBuilder.insertErrorMessage(this.messageNumber, INVALID_REQUEST_STRING);            
        }
    }
    
    protected void processException(final String msg, final Exception e) {
        logger.error(msg, e);
        // TODO AdminErrorAlert
        // paystationAppService.sendAdminErrorAlert(MailerAppService.EXCEPTION,
        // msg, e);
        return;
    }
    
    public String getVersion() {
        return this.version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

    public void setWebApplicationContext(final WebApplicationContext aCtx) {
        this.ctx = aCtx;
    }
}
