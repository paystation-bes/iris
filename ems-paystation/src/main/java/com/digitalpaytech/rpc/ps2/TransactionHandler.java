package com.digitalpaytech.rpc.ps2;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;

import com.digitalpaytech.cardprocessing.CardProcessingMaster;
import com.digitalpaytech.cardprocessing.CardProcessorFactory;
import com.digitalpaytech.cardprocessing.cps.impl.ProcessingDestinationDTO;
import com.digitalpaytech.cardprocessing.impl.LinkCPSProcessor;
import com.digitalpaytech.client.dto.corecps.Authorization;
import com.digitalpaytech.data.TransactionData;
import com.digitalpaytech.domain.CPSStoreAndForwardAttempt;
import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.dto.paystation.TransactionDto;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.DuplicateTransactionRequestException;
import com.digitalpaytech.helper.TransactionHelper;
import com.digitalpaytech.rpc.support.MessageTags;
import com.digitalpaytech.rpc.support.threads.SensorExtractionThread;
import com.digitalpaytech.service.CardRetryTransactionService;
import com.digitalpaytech.service.LicensePlateService;
import com.digitalpaytech.service.StoreAndForwardService;
import com.digitalpaytech.service.TransactionService;
import com.digitalpaytech.util.CryptoUtil;
import com.digitalpaytech.util.StableDateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.TransactionFacade;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.dto.Pair;
import com.digitalpaytech.util.xml.XMLBuilderBase;
import com.digitalpaytech.util.xml.XMLUtil;
import com.netflix.client.ClientException;

/**
 * EMS 6 Transaction is now consist of the following EMS 7 classes:
 * 
 * Purchase
 * Permit
 * PaymentCash
 * PaymentCard
 * DenominationType
 * PaymentCard
 * MobileNumber
 * LicencePlate
 * PurchaseCommit
 * PurchaseQuarantine
 * QuarantineType
 * UnifiedRate
 * Tax
 * PurchaseTax
 * LotNumber
 * ActivePermit
 * CardRetryTransaction
 * PaymentSmartCard
 * POSBalance (EMS 6 - PaystationBalance)
 * PurchaseCollection (EMS 6 - TransactionCollection)
 * UnifiedRate (EMS 6 - Rates)
 * 
 * @author Allen Liang
 */

@SuppressWarnings({ "PMD.ExcessivePublicCount", "PMD.GodClass", "PMD.CommentSize", "PMD.ExcessiveImports" })
public class TransactionHandler extends BaseHandler {
    
    private static final Logger LOG = Logger.getLogger(TransactionHandler.class);
    private static final int COIN_HOPPER_1 = 3;
    private TransactionFacade transactionFacade;
    private TransactionService transactionService;
    private CardProcessingMaster cardProcessingMaster;
    private CardProcessorFactory cardProcessorFactory;
    private TransactionDto txDto;
    private TransactionHelper transactionHelper;
    private LicensePlateService licensePlateService;
    private CardRetryTransactionService cardRetryTransactionService;
    private StoreAndForwardService storeAndForwardService;
    
    private Permit permit;
    private Purchase purchase;
    private boolean storeForward;
    
    // Domain objects
    public TransactionHandler(final String messageNumber) {
        super(messageNumber);
        this.txDto = new TransactionDto();
    }
    
    public final void setTxDto(final TransactionDto txDto) {
        this.txDto = txDto;
    }
    
    public final String getRootElementName() {
        return MessageTags.TRANSACTION_TAG_NAME;
    }
    
    public final void process(final XMLBuilderBase xmlBuilder) {
        final long startTime = System.currentTimeMillis();
        process((PS2XMLBuilder) xmlBuilder);
        
        if (LOG.isInfoEnabled()) {
            final float processingTime = (System.currentTimeMillis() - startTime) / StandardConstants.FLOAT_THOUSAND;
            final StringBuilder bdr = new StringBuilder();
            if (pointOfSale == null) {
                bdr.append("PointOfSale serial number is incorrect, unable to retrieve record. Processing time: ");
            } else {
                bdr.append("Returning to TransactionHandler for PointToSale serialNumber: ").append(pointOfSale.getSerialNumber()).append(" after ");
            }
            bdr.append(processingTime).append(" seconds.");
            LOG.info(bdr.toString());
        }
    }
    
    @SuppressWarnings({ "checkstyle:illegalcatch", "PMD.NullAssignment" })
    private void process(final PS2XMLBuilder xmlBuilder) {
        // switches to time of transaction point of sale and validates
        if (!validatePOS(xmlBuilder, this.txDto.getCPSResponse())) {
            return;
        }
        
        try {
            // Make sure it's not a duplicate transaction by looking into ProcessorTransaction and Purchase table.
            this.transactionFacade.verifyIfDuplicateTransaction(this.pointOfSale,
                                                                StableDateUtil.convertFromColonDelimitedDateString(this.txDto.getPurchasedDate()),
                                                                Integer.parseInt(this.txDto.getNumber()), true);
            
            // get information to decide if the transaction is link or not, must be done before getTransactionData to avoid calling link with Scala data
            final ProcessingDestinationDTO processDto = getProcessDestination();
            final TransactionData txData = this.transactionHelper.getTransactionData(this.pointOfSale, this.txDto, processDto, super.getVersion(), false);
            executeTransaction(txData, this.txDto, processDto);
            SensorExtractionThread.getInstance().dataAvailable();
            xmlBuilder.insertAcknowledge(this.messageNumber);
            checkForNotifications(xmlBuilder);
            checkPaystationSettingAndSettingFile(this.txDto);
            
            if (!txData.isCancelledTransaction()) {
                this.licensePlateService.updateLicensePlateService(this.txDto, this.pointOfSale, this.purchase);
            }
            
        } catch (DuplicateTransactionRequestException dtre) {
            LOG.warn(dtre.getMessage());
            xmlBuilder.insertAcknowledge(messageNumber);
        } catch (CryptoException cex) {
            LOG.error("Unable to decrypt card data", cex);
            if (cex.getCause() instanceof ClientException) {
                xmlBuilder.insertErrorMessage(messageNumber, BaseHandler.CRYPTO_EXCEPTION_STRING);
            } else {
                xmlBuilder.insertErrorMessage(messageNumber, BaseHandler.EMS_UNAVAILABLE_STRING);
            }
        } catch (ApplicationException rex) {
            LOG.error(rex);
            xmlBuilder.insertErrorMessage(messageNumber, rex.getMessage());
        } catch (Exception e) {
            final StringBuilder bdr = new StringBuilder().append("TransactionHandler, Unable to process TransactionRequest for POS serialNumber: ")
                    .append(pointOfSale.getSerialNumber()).append(", name: ").append(pointOfSale.getName()).append(", cusomter id: ")
                    .append(pointOfSale.getCustomer().getId()).append(", reason: ").append(e.getMessage());
            processException(bdr.toString(), e);
            xmlBuilder.insertErrorMessage(messageNumber, BaseHandler.EMS_UNAVAILABLE_STRING);
        } finally {
            this.txDto.setCardData(null);
            this.txDto = null;
        }
    }
    
    private ProcessingDestinationDTO getProcessDestination() throws CryptoException {
        ProcessingDestinationDTO processDto = null;
        
        if (!CryptoUtil.isDecryptedCardData(this.txDto.getCardData())) {
            processDto = this.processingDestinationService.pickProcessingDest(this.txDto.getCardData(), this.pointOfSale.getCustomer().getId(),
                                                                              this.pointOfSale.getId());
            
        }
        return processDto;
    }
    
    private void executeTransaction(final TransactionData transactionData, final TransactionDto transactionDto,
        final ProcessingDestinationDTO processDestinationDto) throws DuplicateTransactionRequestException, ApplicationException {
        
        this.permit = transactionData.getPermit();
        this.purchase = transactionData.getPurchase();
        this.storeForward = transactionData.isStoreForward();
        
        if (this.transactionHelper.hasCustomerCardTypeAuthInternal(transactionData) || this.transactionHelper.hasSmartCardDataAndPaid(this.txDto)) {
            // Processes value card / smart card but not going to processor.
            this.transactionService.processTransaction(transactionData, false, null, this.txDto.getEmbeddedTxObject());
            
        } else if (this.transactionHelper.hasDataToProcess(transactionData.getSmsAlert())) {
            // Processes transaction with SMS. It's only for online and preAuth has been done.
            this.transactionService.processTransactionWithSmsAlert(transactionData, this.txDto.getEmbeddedTxObject());
            if (!transactionData.isCancelledTransaction()) {
                this.cardProcessingMaster.addTransactionToSettlementQueue(this.pointOfSale, transactionData.getProcessorTransaction());
            }
            
        } else if (this.transactionHelper.hasDataToProcess(transactionData.getPurchase().getPaymentCards()) && transactionData.isStoreForward()) {
            
            handleStoreAndForward(transactionData, transactionDto, processDestinationDto);
            
        } else {
            // Processes transaction & processor transaction
            this.transactionService.processTransaction(transactionData, false, this.txDto, this.txDto.getEmbeddedTxObject());
            if (requireSettlement(transactionData)) {
                this.cardProcessingMaster.addTransactionToSettlementQueue(this.pointOfSale, transactionData.getProcessorTransaction());
            }
        }
    }
    
    private void handleStoreAndForward(final TransactionData transactionData, final TransactionDto transactionDto,
        final ProcessingDestinationDTO processDTO) throws CryptoException {
        
        final CardRetryTransaction cardRetryTransaction = this.cardRetryTransactionService
                .getCardRetryTransaction(transactionData.getPointOfSale(), transactionData.getPurchase(), transactionData.getProcessorTransaction(),
                                         transactionData.getPurchase().getCardData());
        
        if (processDTO.isProcessInLink()) {
            /*
             * Creates in-memory CardRetryTransaction domain object.
             * It will try do send StoreAndForward to CoreCPS. If CoreCPS is unreachable, it throw exception making TransactionHandler return EMSUnavailable to
             * PS.
             * Case Successful StoreAndForward to CoreCPS. It will use this.transactionService.processTransaction to create and save all related S&F domain
             * objects and discard CardRetryTransaction in the end.
             */
            
            final LinkCPSProcessor linkCPSProcessor =
                    this.cardProcessorFactory.getLinkCPSProcessor(transactionData.getProcessorTransaction().getMerchantAccount(), this.pointOfSale);
            
            final Pair<String, Authorization> chargeTokenAuthorization =
                    linkCPSProcessor.processLinkStoreAndForwardOrFailTransaction(transactionData.getProcessorTransaction(), cardRetryTransaction,
                                                                                 processDTO.getCardTypeName());
            
            this.transactionService.processTransaction(transactionData, false, null, this.txDto.getEmbeddedTxObject());
            
            linkCPSProcessor.updateObjectsAfterSuccessfulCoreCPSStoreAndForward(transactionData.getProcessorTransaction(),
                                                                                chargeTokenAuthorization.getRight(), cardRetryTransaction,
                                                                                chargeTokenAuthorization.getLeft());
            
            final CPSStoreAndForwardAttempt attempt = this.storeAndForwardService.findByChargeToken(chargeTokenAuthorization.getLeft());
            
            if (attempt != null && attempt.isIsSuccessful()) {
                this.storeAndForwardService.deleteCPSStoreAndForwardAttempt(attempt);
            }
        } else {
            if (!transactionData.isCancelledTransaction()) {
                final List<Object> obj = this.transactionService.processTransaction(transactionData, false, null, this.txDto.getEmbeddedTxObject());
                
                this.cardProcessingMaster
                        .addTransactionToStoreForwardQueueIfNotExhausted(this.cardRetryTransactionService.getCardRetryTransaction(obj));
            }
        }
    }
    
    private boolean requireSettlement(final TransactionData txData) {
        return !txData.isEMV() && txData.getPurchase().getPaymentCards().size() > 0 && !txData.isStoreForward() && !txData.isCancelledTransaction()
               && !txData.isCoreCPS();
    }
    

    // ----------------------------------------------------------------------------------------------------------------------
    // Sets XML data. The following methods are according to EMS 6.3.11 TransactionHandler.java and these documents:
    // Confluence - PS2 Transaction: http://confluence:8080/display/EMS/PS2+Transaction
    //            - PS2 Transaction Request: http://confluence:8080/display/EMS/Transaction+Request
    // ----------------------------------------------------------------------------------------------------------------------
    public final void setParkingTimePurchased(final String parkingTimePurchased) {
        this.txDto.setParkingTimePurchased(parkingTimePurchased);
    }
    
    public final void setNumberBillsAccepted(final String numberBillsAccepted) {
        this.txDto.setNumberBillsAccepted(numberBillsAccepted);
    }
    
    public final void setNumberCoinsAccepted(final String numberCoinsAccepted) {
        this.txDto.setNumberCoinsAccepted(numberCoinsAccepted);
    }
    
    public final void setHopperDispensed(final String data) {
        final String[] values = data.split("[-:]");
        setHopper2CoinsDispensed(values[1]);
        setHopper1CoinsDispensed(values[COIN_HOPPER_1]);
    }
    
    // xxx Remove when all PS2 at 6.0.0.0 and above.
    public final void setHopper1CoinsDispensed(final String hopper1CoinsDispensed) {
        this.txDto.setHopper1CoinsDispensed(hopper1CoinsDispensed);
    }
    
    public final void setHopper2CoinsDispensed(final String hopper2CoinsDispensed) {
        this.txDto.setHopper2CoinsDispensed(hopper2CoinsDispensed);
    }
    
    public final void setAddTimeNum(final String addTimeNumber) {
        this.txDto.setAddTimeNum(addTimeNumber);
    }
    
    public final void setCardAuthorizationId(final String cardAuthId) {
        this.txDto.setCardAuthorizationId(cardAuthId);
    }
    
    public final void setCardData(final String cardData) {
        this.txDto.setCardData(cardData);
    }
    
    public final void setTax1Name(final String tax1Name) {
        this.txDto.setTax1Name(tax1Name);
    }
    
    public final void setTax1Rate(final String tax1Rate) {
        this.txDto.setTax1Rate(tax1Rate);
    }
    
    public final void setTax1Value(final String tax1Value) {
        this.txDto.setTax1Value(tax1Value);
    }
    
    public final void setTax2Name(final String tax2Name) {
        this.txDto.setTax2Name(tax2Name);
    }
    
    public final void setTax2Rate(final String tax2Rate) {
        this.txDto.setTax2Rate(tax2Rate);
    }
    
    public final void setTax2Value(final String tax2Value) {
        this.txDto.setTax2Value(tax2Value);
    }
    
    public void setTax3Rate(final String tax3Rate) {
        this.txDto.setTax3Rate(tax3Rate);
    }
    
    public void setTax3Value(final String tax3Value) {
        this.txDto.setTax3Value(tax3Value);
    }
    
    public void setTax3Name(final String tax3Name) {
        this.txDto.setTax3Name(tax3Name);
    }
    
    public final void setCardPaid(final String cardPaid) {
        this.txDto.setCardPaid(cardPaid);
    }
    
    public final void setCashPaid(final String cashPaid) {
        this.txDto.setCashPaid(cashPaid);
    }
    
    public final void setChangeDispensed(final String changeDispensed) {
        this.txDto.setChangeDispensed(changeDispensed);
    }
    
    public final void setChargedAmount(final String chargedAmount) {
        this.txDto.setChargedAmount(chargedAmount);
    }
    
    // New function since PS send original amount to EMS
    // The portion $$$ shall be an integer representing number of cents. (PS
    // request)
    public final void setOriginalAmount(final String originalAmount) {
        this.txDto.setOriginalAmount(originalAmount);
    }
    
    public final void setIsRefundSlip(final String isRefundSlip) {
        this.txDto.setIsRefundSlip(isRefundSlip);
    }
    
    public final void setLotNumber(final String paystationSetting) {
        this.txDto.setLotNumber(paystationSetting);
    }
    
    // Ticket Number
    public final void setNumber(final String number) {
        this.txDto.setNumber(number);
    }
    
    public final void setEmsPreAuthId(final String preAuthId) {
        this.txDto.setEmsPreAuthId(preAuthId);
    }
    
    public final void setStallNumber(final String stall) {
        this.txDto.setStallNumber(stall);
    }
    
    public final void setType(final String type) {
        this.txDto.setType(StringUtils.deleteWhitespace(type));
    }
    
    public final void setExpiryDate(final String expiryDate) {
        this.txDto.setExpiryDate(expiryDate);
    }
    
    public final void setPurchasedDate(final String purchasedDate) {
        this.txDto.setPurchasedDate(purchasedDate);
    }
    
    public final void setTransDateTime(final String transDateTime) {
        this.txDto.setTransDateTime(transDateTime);
    }
    
    public final void setExcessPayment(final String excessPayment) {
        this.txDto.setExcessPayment(excessPayment);
    }
    
    public final void setSmartCardPaid(final String amount) {
        this.txDto.setSmartCardPaid(amount);
    }
    
    public final void setSmartCardData(final String data) {
        this.txDto.setSmartCardData(data);
    }
    
    public final void setSmartCardType(final String smartCardType) {
        this.txDto.setSmartCardType(smartCardType);
    }
    
    public final void setSmartCardMerchantInfo(final String smartCardMerchantInfo) {
        this.txDto.setSmartCardMerchantInfo(smartCardMerchantInfo);
    }
    
    public final void setSmartCardSettlementInfo(final String smartCardSettlementInfo) {
        this.txDto.setSmartCardSettlementInfo(smartCardSettlementInfo);
    }
    
    public final void setCouponNumber(final String data) {
        this.txDto.setCouponNumber(data);
    }
    
    public final void setCouponPercent(final String percent) {
        this.txDto.setCouponPercent(percent);
    }
    
    public final void setCouponAmount(final String amount) {
        this.txDto.setCouponAmount(amount);
    }
    
    public final void setRateID(final String rateId) {
        this.txDto.setRateID(rateId);
    }
    
    public final void setRateName(final String rateName) {
        this.txDto.setRateName(rateName);
    }
    
    public final void setRateValue(final String rateValue) {
        this.txDto.setRateValue(rateValue);
    }
    
    // Included, Added, Non
    public final void setTaxType(final String taxType) {
        this.txDto.setTaxType(taxType);
    }
    
    public final void setLicensePlateNo(final String licensePlateNo) {
        this.txDto.setLicensePlateNo(licensePlateNo);
    }
    
    public final void setMobileNumber(final String mobileNumber) {
        if (StringUtils.isNotBlank(mobileNumber) && !mobileNumber.startsWith(WebCoreConstants.AREA_CODE_NORTH_AMERICA)) {
            final StringBuffer sb = new StringBuffer();
            sb.append(WebCoreConstants.AREA_CODE_NORTH_AMERICA).append(mobileNumber);
            this.txDto.setMobileNumber(sb.toString());
        } else {
            this.txDto.setMobileNumber(mobileNumber);
        }
    }
    
    public final void setCreditCardData(final String creditCardData) {
        this.txDto.setCreditCardData(creditCardData);
    }
    
    public final void setPaidByRFID(final String paidByRFID) {
        this.txDto.setPaidByRFID(paidByRFID);
    }
    
    // e.g. <CardAmountOther>72.88</CardAmountOther>
    public final void setCardAmountOther(final String cardAmountOther) {
        this.txDto.setCardAmountOther(cardAmountOther);
    }
    
    // e.g. <CardAmountSC>63.83</CardAmountSC>
    public final void setCardAmountSC(final String cardAmountSC) {
        this.txDto.setCardAmountSC(cardAmountSC);
    }
    
    // e.g. <CardAmountVisa>15.72</CardAmountVisa>
    public final void setCardAmountVisa(final String cardAmountVisa) {
        this.txDto.setCardAmountVisa(cardAmountVisa);
    }
    
    // e.g. <CardAmountMC>84.27</CardAmountMC>
    public final void setCardAmountMC(final String cardAmountMC) {
        this.txDto.setCardAmountMC(cardAmountMC);
    }
    
    // e.g. <CardAmountAmex>78.23</CardAmountAmex>
    public final void setCardAmountAmex(final String cardAmountAmex) {
        this.txDto.setCardAmountAmex(cardAmountAmex);
    }
    
    // e.g. <CardAmountDiners>69.93</CardAmountDiners>
    public final void setCardAmountDiners(final String cardAmountDiners) {
        this.txDto.setCardAmountDiners(cardAmountDiners);
    }
    
    // e.g. <CardAmountDiscover>27.7</CardAmountDiscover>
    public final void setCardAmountDiscover(final String cardAmountDiscover) {
        this.txDto.setCardAmountDiscover(cardAmountDiscover);
    }
    
    // e.g. <CoinCol>25:7,100:4</CoinCol>
    public final void setCoinCol(final String coinCollection) {
        this.txDto.setCoinCol(coinCollection);
    }
    
    // e.g. <BillCol>5:4</BillCol>
    public final void setBillCol(final String billCollection) {
        this.txDto.setBillCol(billCollection);
    }
    
    public final void setEMVTransactionData(final String emvTransactionData) {
        this.txDto.setEmvTransactionData(emvTransactionData);
    }
    
    // e.g. <AuthorizationNumber>70447</AuthorizationNumber>
    public final void setAuthorizationNumber(final String authorizationNumber) {
        this.txDto.setAuthorizationNumber(authorizationNumber);
    }
    
    public final void setWebApplicationContext(final WebApplicationContext ctx) {
        super.setWebApplicationContext(ctx);
        this.transactionFacade = (TransactionFacade) ctx.getBean("transactionFacade");
        this.transactionService = (TransactionService) ctx.getBean("transactionService");
        this.transactionHelper = (TransactionHelper) ctx.getBean("transactionHelper");
        this.cardProcessingMaster = (CardProcessingMaster) ctx.getBean("cardProcessingMaster");
        this.licensePlateService = ctx.getBean(LicensePlateService.class);
        this.cardProcessorFactory = (CardProcessorFactory) ctx.getBean("cardProcessorFactory");
        this.cardRetryTransactionService = ctx.getBean(CardRetryTransactionService.class);
        this.storeAndForwardService = ctx.getBean(StoreAndForwardService.class);
    }
    
    public final Permit getPermit() {
        return this.permit;
    }
    
    public final void setPermit(final Permit permit) {
        this.permit = permit;
    }
    
    public final Purchase getPurchase() {
        return this.purchase;
    }
    
    public final void setPurchase(final Purchase purchase) {
        this.purchase = purchase;
    }
    
    public final boolean isStoreForward() {
        return this.storeForward;
    }
    
    public final void setStoreForward(final boolean storeNForward) {
        this.storeForward = storeNForward;
    }
    
    public final void setPaidByChipCard(final String paidByChipCard) {
        this.txDto.setPaidByChipCard(paidByChipCard);
    }
    
    public final void setCardEaseData(final String cardEaseData) {
        this.txDto.setCardEaseData(cardEaseData);
    }
    
    public final void setCPSResponse(final String cpsResponse) {
        this.txDto.setCPSResponse(cpsResponse);
    }
    
    public final void setPreferredRate(final String preferredRate) {
        this.txDto.setPreferredRate(XMLUtil.numStringToBooleanObject(preferredRate));
    }
    
    public final void setLimitedRate(final String limitedRate) {
        this.txDto.setLimitedRate(XMLUtil.numStringToBooleanObject(limitedRate));
    }
}
