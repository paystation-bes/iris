package com.digitalpaytech.rpc.ps2;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;

import com.digitalpaytech.annotation.AttributeTransformer;
import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.annotation.StoryLookup;
import com.digitalpaytech.domain.EventDefinition;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.OutOfOrderException;
import com.digitalpaytech.rpc.support.MessageTags;
import com.digitalpaytech.service.paystation.EventAlertService;
import com.digitalpaytech.service.paystation.PosTelemetryService;
import com.digitalpaytech.service.queue.QueueEventService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.xml.XMLBuilderBase;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@StoryAlias(EMVTelemetryHandler.ALIAS)

public class EMVTelemetryHandler extends BaseHandler {
    public static final String ALIAS = "telemetry request";
    public static final String ALIAS_TYPE = "type";
    public static final String ALIAS_REFERENCE_COUNTER = "reference counter";
    public static final String ALIAS_TIMESTAMP = "time";
    public static final String ALIAS_TELEMETRY = "data";
    
    public static final String ALIAS_TELEMETRY_MAP = "telemetry data";
    public static final String ALIAS_TELEMETRY_MAP_ID = "telemetry id";
    
    private static final Logger LOGGER = Logger.getLogger(AuditHandler.class);
    private static final float MILLISECOND_FLOAT = 1000f;
    private static final String VIOLATION_STATUS = "violationStatus";
    private static final Integer CARD_READER_DEVICE_TYPE_ID = 4;
    
    private String type;
    private String referenceCounter;
    private String timestamp;
    private String telemetry;
    private PosTelemetryService posTelemetryService;
    private QueueEventService queueEventService;
    private EventAlertService eventAlertService;
    
    public EMVTelemetryHandler(final String messageNumber) {
        super(messageNumber);
    }
    
    public final PosTelemetryService getPosTelemetryService() {
        return this.posTelemetryService;
    }
    
    public final void setType(final PosTelemetryService newPosTelemetryService) {
        this.posTelemetryService = newPosTelemetryService;
    }
    
    @StoryAlias(ALIAS_TYPE)
    public final String getType() {
        return this.type;
    }
    
    public final void setType(final String type) {
        this.type = type;
    }
    
    @StoryAlias(ALIAS_REFERENCE_COUNTER)
    public final String getReferenceCounter() {
        return this.referenceCounter;
    }
    
    public final void setReferenceCounter(final String referenceCounter) {
        this.referenceCounter = referenceCounter;
    }
    
    @StoryAlias(ALIAS_TIMESTAMP)
    public final String getTimestamp() {
        return this.timestamp;
    }
    
    public final void setTimestamp(final String timeStamp) {
        this.timestamp = timeStamp;
    }
    
    @StoryAlias(ALIAS_TELEMETRY)
    @StoryLookup(type = ALIAS_TELEMETRY_MAP, searchAttribute = ALIAS_TELEMETRY_MAP_ID)
    @AttributeTransformer("jacksonJSON")
    public final String getTelemetry() {
        return this.telemetry;
    }
    
    public final void setTelemetry(final String telemetry) {
        this.telemetry = telemetry;
    }
    
    @Override
    public final String getRootElementName() {
        return MessageTags.EMV_TELEMETRY_TAG_NAME;
    }
    
    @Override
    public final void process(final XMLBuilderBase xmlBuilder) {
        final long startTime = System.currentTimeMillis();
        process((PS2XMLBuilder) xmlBuilder);
        
        if (LOGGER.isInfoEnabled()) {
            final float processingTime = (System.currentTimeMillis() - startTime) / MILLISECOND_FLOAT;
            final StringBuilder bdr = new StringBuilder();
            if (super.pointOfSale == null) {
                bdr.append("PointOfSale serial number is incorrect, unable to retrieve record. Processing time: ");
            } else {
                bdr.append("Returning response to PointToSale serialNumber: ").append(super.pointOfSale.getSerialNumber()).append(" after ");
            }
            bdr.append(processingTime).append(" seconds.");
            LOGGER.info(bdr.toString());
        }
        
    }
    
    private void process(final PS2XMLBuilder xmlBuilder) {
        Paystation paystation = null;
        final int paystationTypeId;
        if (!validatePOS(xmlBuilder)) {
            return;
        }
        
        try {
            paystation = this.pointOfSaleService.findPayStationByPointOfSaleId(super.pointOfSale.getId());
            paystationTypeId = paystation.getPaystationType().getId();
            
            final Map<String, String> telemetryMap = parseTelemetryJSON();
            
            final Map<String, String> lowerCaseTelemetryMap = new HashMap<String, String>();
            
            final Iterator<Entry<String, String>> it = telemetryMap.entrySet().iterator();
            
            while (it.hasNext()) {
                final Entry<String, String> entry = it.next();
                lowerCaseTelemetryMap.put(entry.getKey().toLowerCase(), entry.getValue());
            }
            
            this.posTelemetryService.processPosTelemetry(super.pointOfSale, this.timestamp, lowerCaseTelemetryMap, this.type,
                                                         Integer.parseInt(this.referenceCounter));
            
            EventDefinition eventDefinition = null;
            if (lowerCaseTelemetryMap.containsKey(EMVTelemetryHandler.VIOLATION_STATUS.toLowerCase())) {
                final String descriptionNoSpace = EMVTelemetryHandler.VIOLATION_STATUS
                                                  + lowerCaseTelemetryMap.get(EMVTelemetryHandler.VIOLATION_STATUS.toLowerCase()).toString();
                eventDefinition = this.eventAlertService
                        .getEventDefinitionByDeviceTypeIdAndDescriptionNoSpace(EMVTelemetryHandler.CARD_READER_DEVICE_TYPE_ID, descriptionNoSpace);
                if (eventDefinition != null) {
                    final Date date = DateUtil.convertFromColonDelimitedDateString(this.timestamp);
                    final Boolean isActive = eventDefinition.getEventSeverityType().getId() != 0;
                    this.queueEventService
                            .createEventQueue(pointOfSale.getId(), paystationTypeId, null, eventDefinition.getEventType(),
                                              eventDefinition.getEventSeverityType().getId(), eventDefinition.getEventActionType().getId(), date, "",
                                              isActive, true, WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID,
                                              (byte) (eventDefinition.getEventType().isIsAlert()
                                                      ? WebCoreConstants.QUEUE_TYPE_RECENT_EVENT | WebCoreConstants.QUEUE_TYPE_HISTORICAL_EVENT : 0));
                }
            }
            xmlBuilder.insertAcknowledge(super.messageNumber);
        } catch (OutOfOrderException oooe) {
            LOGGER.error("Reference counter out of order!");
            xmlBuilder.insertErrorMessage(super.messageNumber, BaseHandler.OUT_OF_ORDER);
        } catch (InvalidDataException e) {
            LOGGER.error(e.getMessage());
            xmlBuilder.insertErrorMessage(super.messageNumber, BaseHandler.INVALID_TYPE_STRING);
        } catch (IOException ioe) {
            LOGGER.error("UnExpected message", ioe);
            xmlBuilder.insertErrorMessage(super.messageNumber, BaseHandler.INVALID_MESSAGE);
        } catch (Exception e) {
            LOGGER.error("Couldn't process EMV Telemetry", e);
            xmlBuilder.insertErrorMessage(super.messageNumber, BaseHandler.EMS_UNAVAILABLE_STRING);
        }
    }
    
    private Map<String, String> parseTelemetryJSON() throws IOException {
        
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final Map<String, String> telemetryMap = mapper.readValue(this.telemetry, new TypeReference<Map<String, String>>() {
            });
            
            return (Map<String, String>) telemetryMap;
            
        } catch (JsonParseException | JsonMappingException e) {
            throw new IOException("Unable to parse JSON!", e);
        }
    }
    
    public final void setWebApplicationContext(final WebApplicationContext ctx) {
        super.setWebApplicationContext(ctx);
        this.posTelemetryService = ctx.getBean(PosTelemetryService.class);
        this.queueEventService = ctx.getBean(QueueEventService.class);
        this.eventAlertService = ctx.getBean(EventAlertService.class);
    }
    
}
