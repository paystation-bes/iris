package com.digitalpaytech.rpc.ps2;

import org.apache.log4j.Logger;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.context.WebApplicationContext;

import com.digitalpaytech.rpc.support.MessageTags;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.xml.XMLBuilderBase;
import com.digitalpaytech.cardprocessing.cps.EMVRefundService;
import com.digitalpaytech.helper.emvrefund.EMVRefundHelper;
import com.digitalpaytech.domain.emvrefund.EMVRefund;
import com.digitalpaytech.domain.emvrefund.EMVRefundResponse;

/**
 * - new xml rpc request listener and basic validation
 * - insert charge request information to the new database table
 * - send charge request to CPS
 *
 * Return EMSUnavailable if any part fails
 */

public class EMVRefundHandler extends BaseHandler {
    private static final Logger LOG = Logger.getLogger(EMVRefundHandler.class);
    private EMVRefundService emvRefundService;
    private EMVRefundHelper helper;
    private String serialNumber;
    private String processedDate;
    private String cardEaseData;
    private String amount;
   
    
    public EMVRefundHandler(final String messageNumber) {
        super(messageNumber);
        
    }
    
    public final String getRootElementName() {
        return MessageTags.EMV_REFUND_REQUEST_TAG_NAME;
    }
    
    public final void process(final XMLBuilderBase xmlBuilder) {
        final long startTime = System.currentTimeMillis();
        process((PS2XMLBuilder) xmlBuilder);
        
        if (LOG.isInfoEnabled()) {
            final float processingTime = (System.currentTimeMillis() - startTime) / MILLISECOND_TO_SECOND;
            final StringBuilder bdr = new StringBuilder();
            if (pointOfSale == null) {
                bdr.append("PointOfSale serial number is incorrect, unable to retrieve record. Processing time: ");
            } else {
                bdr.append("Returning response to PointOfSale serialNumber: ").append(pointOfSale.getSerialNumber()).append(" after ");
            }
            bdr.append(processingTime).append(" seconds.");
            LOG.info(bdr.toString());
        }
    }


    private void process(final PS2XMLBuilder xmlBuilder) {
        if (!validatePOS(xmlBuilder)) {
            return;
        }
        if (StringUtils.isBlank(this.cardEaseData)) {
            xmlBuilder.insertErrorMessage(super.messageNumber, BaseHandler.EMS_UNAVAILABLE_STRING);
            return;
        }
        
        if (LOG.isDebugEnabled()) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("serialNumber: ").append(this.serialNumber).append(", processedDate: ").append(this.processedDate);
            bdr.append(StandardConstants.STRING_NEXT_LINE).append("cardEaseData: ").append(this.cardEaseData).append(", amount: ").append(this.amount);
            LOG.debug(bdr.toString());
        }
        
        try {
            // Create EMVRefund domain object, save data to database and call CPS.
            final EMVRefund refund = this.helper.createEMVRefund(super.pointOfSale, this.processedDate, this.cardEaseData, this.amount);
            this.emvRefundService.saveCharge(refund);
            final EMVRefund updatedEMVRefund = this.emvRefundService.callPushEMVData(refund);
            
            // Call Refund.
            final EMVRefundResponse result = this.emvRefundService.callRefundsRealTime(updatedEMVRefund);
            LOG.info("CPS refund result is: " + result);
            
            // Delete EMVRefund record in database if refund process is completed.
            if (result.getCode().equalsIgnoreCase(StandardConstants.STRING_STATUS_CODE_ACCEPTED)) {
                final boolean done = this.emvRefundService.deleteEMVRefund(updatedEMVRefund.getId());
                if (done) {
                    xmlBuilder.insertAcknowledge(super.messageNumber);
                } else {
                    xmlBuilder.insertErrorMessage(super.messageNumber, BaseHandler.EMS_UNAVAILABLE_STRING);        
                }
            } else {
                xmlBuilder.insertErrorMessage(super.messageNumber, BaseHandler.EMS_UNAVAILABLE_STRING);
            }
        } catch (Exception e) {
            processException("Unable to process EMVRefund for PointOfSale SerialNumber: " + this.serialNumber, e);
            xmlBuilder.insertErrorMessage(messageNumber, BaseHandler.EMS_UNAVAILABLE_STRING);            
        }
    }
    
    
    public final void setPaystationCommAddress(final String paystationCommAddress) {
        super.setPaystationCommAddress(paystationCommAddress);
        this.serialNumber = paystationCommAddress;
    }
    public final void setProcessedDate(final String processedDate) {
        this.processedDate = processedDate;
    }
    public final void setCardEaseData(final String cardEaseData) {
        this.cardEaseData = cardEaseData;
    }
    public final void setAmount(final String amount) {
        this.amount = amount;
    }

    public final void setWebApplicationContext(final WebApplicationContext ctx) {
        super.setWebApplicationContext(ctx);        
        this.emvRefundService = (EMVRefundService) ctx.getBean("emvRefundService");
        this.helper = (EMVRefundHelper) ctx.getBean("emvRefundHelper");
    }
}
