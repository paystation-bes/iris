package com.digitalpaytech.rpc.ps2;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;

import com.digitalpaytech.util.xml.XMLBuilderBase;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.rpc.support.MessageTags;

/**
 * LinuxBINRangeSuccessHandler should be called after pay station downloaded update (directly from CPS not through Iris)
 * and pay station sends acknowledgement message to Iris to turn off notification.
 */

public class LinuxBINRangeSuccessHandler extends BaseHandler {
    private static final Logger LOG = Logger.getLogger(LinuxConfigurationSuccessHandler.class);
    
    
    public LinuxBINRangeSuccessHandler(final String messageNumber) {
        super(messageNumber);
    }
    
    @Override
    public final String getRootElementName() {
        return MessageTags.LINUX_BIN_RANGE_SUCCESS;
    }
    
    @Override
    public final void process(final XMLBuilderBase xmlBuilder) {
        final long startTime = System.currentTimeMillis();
        process((PS2XMLBuilder) xmlBuilder);
        
        if (LOG.isInfoEnabled()) {
            final float processingTime = (System.currentTimeMillis() - startTime) / 1000f;
            final StringBuilder bdr = new StringBuilder();
            bdr.append("Returning LinuxBINRangeSuccessHandler to PointOfSale serialNumber: ").append(posSerialNumber).append(" after ")
                    .append(processingTime).append(" seconds.");
            LOG.info(bdr.toString());
        }
    }

    private void process(final PS2XMLBuilder xmlBuilder) {
        if (!validatePOS(xmlBuilder)) {
            return;
        }
        
        final PosServiceState pss = this.pointOfSale.getPosServiceState();
        if (pss.isIsNewBINRange()) {
            pss.setIsNewBINRange(false);
            this.posServiceStateService.updatePosServiceState(pss);
            
        }
        
        xmlBuilder.insertAcknowledge(messageNumber);
    }

    
    public final void setWebApplicationContext(final WebApplicationContext ctx) {
        super.setWebApplicationContext(ctx);
    }
}
