package com.digitalpaytech.rpc.ps2;

import org.apache.log4j.Logger;

import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.rpc.support.MessageTags;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.xml.XMLBuilderBase;

public class RootCertificateSuccessHandler extends BaseHandler {
    private static final Logger LOG = Logger.getLogger(RootCertificateSuccessHandler.class);
    
    public RootCertificateSuccessHandler(final String messageNumber) {
        super(messageNumber);
    }
    
    @Override
    public String getRootElementName() {
        return MessageTags.ROOT_CERTIFICATE_SUCCESS_TAG_NAME;
    }
    
    @Override
    public final void process(final XMLBuilderBase xmlBuilder) {
        final long startTime = System.currentTimeMillis();
        process((PS2XMLBuilder) xmlBuilder);
        
        if (LOG.isInfoEnabled()) {
            final float processingTime = (System.currentTimeMillis() - startTime) / StandardConstants.FLOAT_THOUSAND;
            final StringBuilder bdr = new StringBuilder();
            if (pointOfSale == null) {
                bdr.append("PointOfSale serial number is incorrect, unable to retrieve record. Processing time: ");
            } else {
                bdr.append("Returning to response to PointToSale serialNumber: ").append(pointOfSale.getSerialNumber()).append(" after ");
            }
            bdr.append(processingTime).append(" seconds.");
            LOG.info(bdr.toString());
        }
    }
    
    private void process(final PS2XMLBuilder xmlBuilder) {
        if (!validatePOS(xmlBuilder)) {
            return;
        }
        
        final PosServiceState posServiceState = this.pointOfSale.getPosServiceState();
        posServiceState.setIsNewRootCertificate(false);
        
        try {
            this.posServiceStateService.updatePosServiceState(posServiceState);
            xmlBuilder.insertAcknowledge(messageNumber);
        } catch (RuntimeException e) {
            final String msg = "Unable to process RootCertificateSuccessRequest for pointOfSale serialNumber: " + this.pointOfSale.getSerialNumber();
            processException(msg, e);
            xmlBuilder.insertErrorMessage(this.messageNumber, BaseHandler.EMS_UNAVAILABLE_STRING);
        }
    }
    
    public final String getPaystationCommAddress() {
        return super.getPaystationCommAddress();
    }
    
    public final void setPaystationCommAddress(final String paystationCommAddress) {
        super.setPaystationCommAddress(paystationCommAddress);
    }
}
