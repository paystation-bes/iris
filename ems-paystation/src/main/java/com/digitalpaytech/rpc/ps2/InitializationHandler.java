package com.digitalpaytech.rpc.ps2;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;

import com.digitalpaytech.dto.paystation.PosServiceStateInitInfo;
import com.digitalpaytech.rpc.support.MessageTags;
import com.digitalpaytech.rpc.ps2.PS2XMLBuilder;
import com.digitalpaytech.service.paystation.PosServiceStateService;
import com.digitalpaytech.util.xml.XMLBuilderBase;


public class InitializationHandler extends BaseHandler {

    private static Logger log = Logger.getLogger(InitializationHandler.class);
    private PosServiceStateInitInfo posServiceStateInitInfo = new PosServiceStateInitInfo();
    private PosServiceStateService posServiceStateService;
    
    public InitializationHandler(String messageNumber) {
        super(messageNumber);
    }

    public String getRootElementName() {
        return MessageTags.INITIALIZATION_TAG_NAME;
    }

    public void process(XMLBuilderBase xmlBuilder) {
        long start_time = System.currentTimeMillis();
        process((PS2XMLBuilder) xmlBuilder);

        if (log.isInfoEnabled()) {
            float processing_time = (System.currentTimeMillis() - start_time) / MILLISECOND_TO_SECOND;
            StringBuilder bdr = new StringBuilder();
            if (pointOfSale == null) {
                bdr.append("PointOfSale serial number is incorrect, unable to retrieve record. Processing time: ");
            } else {
                bdr.append("Returning InitializationResponse to PointToSale serialNumber: ").append(pointOfSale.getSerialNumber()).append(" after ");
            }
            bdr.append(processing_time).append(" seconds.");
            log.info(bdr.toString());
        }
    }

    private void process(PS2XMLBuilder xmlBuilder) {
        if (!validatePOS(xmlBuilder)) {
            return;
        }

        try {
            posServiceStateService.processInitialization(pointOfSale.getId(), posServiceStateInitInfo);
            xmlBuilder.insertAcknowledge(messageNumber);

        } catch (Exception e) {
            String msg = "Unable to process InitializationRequest for PointOfSale serialNumber: " + pointOfSale.getSerialNumber();
            processException(msg, e);

            xmlBuilder.insertErrorMessage(messageNumber, BaseHandler.EMS_UNAVAILABLE_STRING);
        }
    }
    
    /*
     * Here is the method mappings between Iniialization object, ServiceState and PosServiceState.
     * See EMS 6.3.11 RpcPaystationServiceImpl, public void processInitialization(Paystation ps, Initialization init)
     * 
     * Iniialization          |  ServiceState               |   PosServiceState
     *      paystationSetting               lotNumber                      paystationSetting
     *      machineNumber            machineNumber                  <no longer needed>
     *      primaryVersion           firmwareVersio                 primaryVersion
     *      bbserialNumber           blackboxSerialNumber           bbserialNumber
     */
    public void setLotNumber(String value) {
        posServiceStateInitInfo.setPaystationSetting(value);
    }

    public void setFirmwareVersion(String value) {
        posServiceStateInitInfo.setPrimaryVersion(value);
    }

    public void setBBSerialNumber(String value) {
        posServiceStateInitInfo.setBbSerialNumber(value);
    }

    public void setWebApplicationContext(WebApplicationContext ctx) {
        super.setWebApplicationContext(ctx);
        this.posServiceStateService = (PosServiceStateService) ctx.getBean("posServiceStateService");
    }
}
