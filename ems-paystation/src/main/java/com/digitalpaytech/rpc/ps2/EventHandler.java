package com.digitalpaytech.rpc.ps2;

import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;

import com.digitalpaytech.dto.paystation.EventData;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.rpc.support.MessageTags;
import com.digitalpaytech.rpc.support.threads.SensorExtractionThread;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.RawSensorDataService;
import com.digitalpaytech.service.paystation.EventAlertService;
import com.digitalpaytech.service.paystation.EventModemService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.xml.XMLBuilderBase;

public class EventHandler extends BaseHandler {
    
    private static Logger log = Logger.getLogger(EventHandler.class);
    private String timeStamp;
    private String type;
    private String action;
    private String information;
    private MailerService mailerService;
    private EventAlertService eventAlertService;
    private RawSensorDataService rawSensorDataService;
    private EventModemService eventModemService;
    
    public EventHandler(String messageNumber) {
        super(messageNumber);
    }
    
    public String getRootElementName() {
        return MessageTags.EVENT_TAG_NAME;
    }
    
    public void process(XMLBuilderBase xmlBuilder) {
        long start_time = System.currentTimeMillis();
        process((PS2XMLBuilder) xmlBuilder);
        
        if (log.isInfoEnabled()) {
            float processing_time = (System.currentTimeMillis() - start_time) / MILLISECOND_TO_SECOND;
            StringBuilder bdr = new StringBuilder();
            if (pointOfSale == null) {
                bdr.append("PointOfSale serial number is incorrect, unable to retrieve record. Processing time: ");
            } else {
                bdr.append("Returning EventResponse to PointToSale serialNumber: ").append(pointOfSale.getSerialNumber()).append(" after ");
            }
            bdr.append(processing_time).append(" seconds.");
            log.info(bdr.toString());
        }
    }
    
    private void process(PS2XMLBuilder xmlBuilder) {
        // Validate paystation
//	    try {	
	        if (!validatePOS(xmlBuilder)) {
	            return;
	        }	        
//		} catch (Exception sose) {
//			StringBuilder bdr = new StringBuilder();
//			bdr.append("EventHandler.validatePOS, cannot update PosHeartbeat.");			
//			log.error(bdr.toString(), sose);
//		}        
        
        EventData eventData = null;
        try {
            eventData = buildEventData();
            eventData.setInformation(eventData.getInformation());
            if (eventData.getType().equals(WebCoreConstants.EVENT_TYPE_SENSOR)){
            	
            	if(!xmlBuilder.isValidPaystation() || !xmlBuilder.isHeartBeatUpdated()) {
            		return;
            	}
            }
            
            if (isServiceStateData(eventData)) {
                this.rawSensorDataService.saveRawSensorData(pointOfSale.getSerialNumber(), eventData);
                // notify sensor extraction thread
                SensorExtractionThread.getInstance().dataAvailable();
                xmlBuilder.insertAcknowledge(messageNumber);
                
            } else if (eventData.getType().equals(WebCoreConstants.EVENT_TYPE_MODEM)) {
                this.eventModemService.saveModemSetting(pointOfSale, eventData);
                xmlBuilder.insertAcknowledge(messageNumber);
            } else if (!eventData.getType().equals(WebCoreConstants.EVENT_TYPE_SENSOR)) {
                // Process all other events.
                this.eventAlertService.processEvent(pointOfSale, eventData);
                xmlBuilder.insertAcknowledge(messageNumber);
            }
        } catch (Exception e) {
            String msg = "Unable to process EventRequest for PointOfSale SerialNumber: ";
            if(pointOfSale != null) {
            	msg += pointOfSale.getSerialNumber();
            }
            processException(msg, e);
            xmlBuilder.insertErrorMessage(messageNumber, BaseHandler.EMS_UNAVAILABLE_STRING);
        }
    }
    
    private EventData buildEventData() throws InvalidDataException {
        EventData eventData = new EventData();
        eventData.setPointOfSaleId(pointOfSale.getId());
        eventData.setType(getCorrectedType());
        eventData.setAction(action);
        eventData.setInformation(information);
        try {
            eventData.setTimeStamp(DateUtil.convertFromColonDelimitedDateString(timeStamp));
        } catch (InvalidDataException ide) {
            StringBuilder bdr = new StringBuilder();
            bdr.append("EventHandler, setTimeStamp, problem occurred in 'DateUtil.convertFromColonDelimitedDateString', input value is: ").append(timeStamp);
            log.error(bdr.toString(), ide);
            mailerService.sendAdminErrorAlert("EventHandler parse timestamp error", bdr.toString(), ide);
            throw ide;
        }
        return eventData;
    }
    
    private String getCorrectedType() throws InvalidDataException {
        if (WebCoreConstants.EVENT_TYPE_PAYSTATION.equals(type)) {
            if (WebCoreConstants.EVENT_ACTION_DOOR_CLOSED_STRING.equals(action) || WebCoreConstants.EVENT_ACTION_DOOR_OPENED_STRING.equals(action)) {
                if (information == null || WebCoreConstants.EMPTY_STRING.equals(information)) {
                    return WebCoreConstants.EVENT_DEVICE_TYPE_DOOR_STRING;
                } else if (WebCoreConstants.INFO_DOOR_LOWER.equals(information)) {
                    return WebCoreConstants.EVENT_DEVICE_TYPE_DOOR_LOWER_STRING;
                } else if (WebCoreConstants.INFO_DOOR_UPPER.equals(information)) {
                    return WebCoreConstants.EVENT_DEVICE_TYPE_DOOR_UPPER_STRING;
                } else if (WebCoreConstants.INFO_MAINTENANCE.equals(information)) {
                    return WebCoreConstants.EVENT_DEVICE_TYPE_DOOR_MAINTENANCE_STRING;
                } else if (WebCoreConstants.INFO_CASH_VAULT.equals(information)) {
                    return WebCoreConstants.EVENT_DEVICE_TYPE_DOOR_CASH_VAULT_STRING;
                } else {
                    throw new InvalidDataException();
                }
            } else if (WebCoreConstants.EVENT_ACTION_SHOCK_ON_STRING.equals(action) || WebCoreConstants.EVENT_ACTION_SHOCK_OFF_STRING.equals(action) || 
                    WebCoreConstants.EVENT_ACTION_ALARM_ON_STRING.equals(action) || WebCoreConstants.EVENT_ACTION_ALARM_OFF_STRING.equals(action)) {
                return WebCoreConstants.EVENT_DEVICE_TYPE_SHOCK_ALARM_STRING;
            } else {
                return type;
            }
        } else if (WebCoreConstants.EVENT_DEVICE_TYPE_BILL_CANISTER_STRING.equals(type)) {
            return WebCoreConstants.EVENT_TYPE_BILL_STACKER;
        } else if (WebCoreConstants.EVENT_TYPE_COIN_HOPPER.equals(type)) {
            if (WebCoreConstants.EVENT_ACTION_COINS_DISPENSED_STRING.equals(action)) {
                StringTokenizer infoTokens = new StringTokenizer(information,
                                                                  WebCoreConstants.COLON);
                int hopperNumber = Integer.parseInt(infoTokens.nextToken());
                if ((hopperNumber < 1) || (hopperNumber > 2)) {
                    throw new InvalidDataException("Unknown HopperNumber");
                }
                information = infoTokens.nextToken();
                return type + hopperNumber;
            } else {
                return type + information;
            }
        } else {
            return type;
        }
    }
    
    /**
     * The following content are for state information:
     * - <Type>Sensor</Type>
     * - <Type>Paystation</Type><Action>Voltage</Action>
     * - <Type>BillAcceptor</Type>
     * - <Type>CoinAcceptor</Type>
     * - <Type>CardReader</Type>
     */
    private boolean isServiceStateData(EventData eventData) {
        if (eventData.getType().equalsIgnoreCase(WebCoreConstants.EVENT_TYPE_SENSOR)) {
            return true;
        } else if (eventData.getType().equalsIgnoreCase(WebCoreConstants.EVENT_TYPE_PAYSTATION)
                   && eventData.getAction().equals(WebCoreConstants.ACTION_VOLTAGE)) {
            return true;
        }
        return false;
    }
    
    public void setType(String value) {
        this.type = value;
    }
    
    public String getTimeStamp() {
        return timeStamp;
    }
    
    public String getType() {
        return type;
    }
    
    public String getAction() {
        return action;
    }
    
    public String getInformation() {
        return information;
    }
    
    public void setAction(String value) {
        this.action = value;
    }
    
    public void setInformation(String value) {
        this.information = value;
    }
    
    public void setTimeStamp(String value) throws InvalidDataException {
        this.timeStamp = value;
    }
    
    public void setWebApplicationContext(WebApplicationContext ctx) {
        super.setWebApplicationContext(ctx);
        this.mailerService = (MailerService) ctx.getBean("mailerService");
        this.eventAlertService = (EventAlertService) ctx.getBean("eventAlertService");
        this.rawSensorDataService = (RawSensorDataService) ctx.getBean("rawSensorDataService");
        this.eventModemService = (EventModemService) ctx.getBean("eventModemService");
    }
}
