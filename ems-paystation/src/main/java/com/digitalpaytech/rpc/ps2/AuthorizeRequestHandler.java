package com.digitalpaytech.rpc.ps2;

import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;

import com.digitalpaytech.domain.Coupon;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.rpc.support.MessageTags;
import com.digitalpaytech.service.CouponService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.WebWidgetHelperService;
import com.digitalpaytech.util.CouponUtil;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.xml.XMLBuilderBase;
import com.digitalpaytech.exception.InvalidDataException;

public class AuthorizeRequestHandler extends BaseHandler {

	public final static int INVALID = -1;
	public final static int INACTIVE = -2;
	public final static int EXPIRED = -3;

	public final static String SUCCESS_STRING = "Success";
	public final static String EXPIRED_STRING = "Expired";
	public final static String INVALID_STRING = "Invalid";
	public final static String INACTIVE_STRING = "Inactive";

	private final static Logger logger = Logger.getLogger(AuthorizeRequestHandler.class);

	private CouponService couponService = null;
	private LocationService locationService = null;
	private WebWidgetHelperService webWidgetHelperService = null;

	
	private String type = null;
	private String data = null;

	public AuthorizeRequestHandler(String messageNumber) {
		super(messageNumber);
	}

	public String getRootElementName() {
		return MessageTags.AUTHORIZE_REQUEST_TAG_NAME;
	}

	public void process(XMLBuilderBase xmlBuilder) {
		long start_time = System.currentTimeMillis();
		String auth_string = process((PS2XMLBuilder) xmlBuilder);

		float processing_time = (System.currentTimeMillis() - start_time) / MILLISECOND_TO_SECOND;
		
		StringBuilder bdr = new StringBuilder();
		bdr.append("Processing AuthorizeRequest(").append(type).append(") for PS: ").append(posSerialNumber);
		bdr.append(" Returning '").append(auth_string).append("' AuthorizeResponse(").append(type).append(") to PS: ");
		bdr.append(posSerialNumber).append(" after ").append(processing_time).append(" seconds.");
		logger.info(bdr.toString());
	}

	private String process(PS2XMLBuilder xmlBuilder) {
		// Validate paystation
		if (!validatePOS(xmlBuilder)) {
			return null;
		}

		try {
			String response_data = authorizeCoupon(pointOfSale, type, data);
			xmlBuilder.createAuthorizeResponse(this, type, response_data);
			return (response_data.substring(0, response_data.indexOf(':')));
		} catch (Exception e) {
			String msg = "Unable to process AuthorizeRequest(" + type + ") for PS: " + posSerialNumber;
			processException(msg, e);
			xmlBuilder.createAuthorizeResponse(this, type, EMS_UNAVAILABLE_STRING);
			return (EMS_UNAVAILABLE_STRING);
		}
	}

	
	public String authorizeCoupon(PointOfSale pointOfSale, String type, String data) throws InvalidDataException {
		String[] dataArray = data.split(":");
				
        // Added validation to ensure there are coupon id (data_array[0]) and stall number (data_array[1])
        if ("Coupon".equals(type) && dataArray.length == 2) {
		
            String couponIdString = dataArray[0].trim();
            if (couponIdString.length() > 10)
            {
                couponIdString = couponIdString.substring(couponIdString.length() - 10).toUpperCase();
            }
			int percentDiscount;
			// For XML that is sent by a pre 6.4.1 pay station, needs to use different sql.
			if (StringUtils.isNotBlank(super.getVersion()) && WebCoreUtil.isPsPre641(super.getVersion())) {
			    percentDiscount = authorizeCouponForPre641(pointOfSale, couponIdString, Integer.parseInt(dataArray[1]));
			} else {
			    percentDiscount = authorizeCoupon(pointOfSale, couponIdString, Integer.parseInt(dataArray[1]));
			}
			return (getCouponStatusName(percentDiscount) + ":" + percentDiscount);
		}
				
		return null;
	}

	
	public int authorizeCouponForPre641(PointOfSale pos, String couponId, int stallNumber) throws InvalidDataException {
	    Coupon coupon = couponService.findCouponWithShortestCode(pos.getCustomer().getId(), couponId);
	    return validateCoupon(coupon, pos, couponId, stallNumber);
	}
	
	public int authorizeCoupon(PointOfSale pos, String couponId, int stallNumber) throws InvalidDataException {
		Coupon coupon = couponService.findCouponByCustomerIdAndCouponCode(pos.getCustomer().getId(), couponId);
		return validateCoupon(coupon, pos, couponId, stallNumber);
	}

    /**
     * Validate the coupon record and return percent discount if it's valid.
     * @return int percent discount.
     * @throws InvalidDataException If validation returns INVALID = -1, INACTIVE = -2, or EXPIRED = -3, it will throw 
     */	
	private int validateCoupon(Coupon coupon, PointOfSale pos, String couponId, int stallNumber) throws InvalidDataException {
        int result = 0;
        String timeZone = webWidgetHelperService.getCustomerTimeZoneByCustomerId(pos.getCustomer().getId());
        
	    if (coupon == null) {
            logger.warn("Unable to find coupon: " + couponId);
            return INVALID;
        }

        // Reset 'SingleUseDate' if it's before current day.
        if (coupon.getSingleUseDate() != null && CouponUtil.isFromDayBefore(coupon.getSingleUseDate(), timeZone)) {
            coupon.setSingleUseDate(null);
        }
        
        // Get current time and convert to local PS time
        long currentDateMS = System.currentTimeMillis();
        currentDateMS += TimeZone.getTimeZone(timeZone).getOffset(currentDateMS);
        Date currentDate = new Date(currentDateMS);

        if (currentDate.before(coupon.getStartDateLocal())) {
            logger.info("Inactive coupon: " + couponId);
            result = (INACTIVE);
        } else if (currentDate.after(coupon.getEndDateLocal())) {
            logger.info("Expired coupon: " + couponId);
            result =  (EXPIRED);
        } else if (coupon.getNumberOfUsesRemaining() != null && coupon.getNumberOfUsesRemaining() == 0) {
            logger.info("No more uses left for coupon: " + couponId);
            result = (EXPIRED);
        } else if (coupon.getLocation() != null && !isValidCouponLocation(coupon.getLocation().getId(), pos)) {
            logger.info("Invalid Region " + pos.getLocation().getId() + " for coupon: " + couponId);
            result = (INVALID);
        } else if (stallNumber == 0 && !coupon.isIsPndEnabled()) {
            logger.info("PND not valid for coupon: " + couponId);
            result = (INVALID);
        } else if (stallNumber > 0 && !coupon.isIsPbsEnabled()) {
            logger.info("PBS not valid for coupon: " + couponId);
            result = (INVALID);
        } else if (stallNumber > 0 && coupon.isIsPbsEnabled() && !isValidStall(coupon.getSpaceRange(), stallNumber)) {
            logger.info("Stall " + stallNumber + " is not valid for coupon: " + couponId);
            result = (INVALID);
        } else if (coupon.getValidForNumOfDay() > 0 && coupon.getSingleUseDate() != null) {
            logger.info("Single Day Use coupon has applied today, coupon number: " + couponId);
            result = (EXPIRED);
        }
        if (result != INVALID && result != INACTIVE && result != EXPIRED  && coupon.getDollarDiscountAmount() == 0) {
            return coupon.getPercentDiscount();
        }
        if (coupon.getDollarDiscountAmount() > 0)  {
            result = INVALID;
        }        
        if (result == INVALID || result == INACTIVE || result == EXPIRED) {
        	return result;
        }
        
        throw new InvalidDataException(String.valueOf(result));
	}
	
	private boolean isValidCouponLocation(int couponLocationId, PointOfSale pos)
	{
		if (couponLocationId == 0 || couponLocationId == pos.getLocation().getId()) {
			return (true);
		}

		// Valid for an entire location
		Location location = locationService.findLocationById(pos.getLocation().getId());
		location = locationService.findLocationById(location.getLocation().getId());

		// Loop through regions to root or until we find a match
		while (location != null) {
			if (logger.isDebugEnabled()) {
				logger.debug("Comparing coupon region: '" + couponLocationId + "' with current region: '"
						+ location.getId() + "'.");
			}
			if (couponLocationId == location.getId()) {
				// We pass
				return true;
			}
			location = locationService.findLocationById(location.getLocation().getId());
		}
		// We hit the root region -> no match
		return (false);
	}

	private boolean isValidStall(String stallRange, int stallNumber)
	{
		if (stallRange == null || stallRange.trim().equals("")) {
			return true;
		}

		String[] stall_groups = stallRange.split(",");
		for (int i = 0; i < stall_groups.length; i++) {
			String[] stall_values = stall_groups[i].split("-");
			switch (stall_values.length) {
				case 1:
					int number = Integer.parseInt(stall_values[0].trim());
					if (stallNumber == number) {
						return (true);
					}
					break;

				case 2:
					int firstNumber = Integer.parseInt(stall_values[0].trim());
					int secondNumber = Integer.parseInt(stall_values[1].trim());

					if (stallNumber >= firstNumber && stallNumber <= secondNumber) {
						return (true);
					}
					break;
			}
		}
		return (false);
	}

	
	private String getCouponStatusName(int percentDiscount)
	{
		if (percentDiscount >= 0) {
			return (SUCCESS_STRING);
		}

		switch (percentDiscount)
		{
			case EXPIRED:
				return (EXPIRED_STRING);

			case INACTIVE:
				return (INACTIVE_STRING);

			default:
				return (INVALID_STRING);
		}
	}


	
	public void setData(String data) {
		this.data = data;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setWebApplicationContext(WebApplicationContext ctx) {
		super.setWebApplicationContext(ctx);
		this.couponService = (CouponService) ctx.getBean("couponService");
		this.locationService = (LocationService) ctx.getBean("locationService");
		this.webWidgetHelperService = (WebWidgetHelperService) ctx.getBean("webWidgetHelperService");
	}
}