package com.digitalpaytech.rpc.ps2;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;

import com.digitalpaytech.domain.ActivePermit;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.dto.paystation.SpaceInfo;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.SystemException;
import com.digitalpaytech.rpc.support.MessageTags;
import com.digitalpaytech.service.ActivePermitService;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.PaystationSettingService;
import com.digitalpaytech.service.paystation.SpaceInfoService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.xml.XMLBuilderBase;

public class StallInfoRequestHandler extends BaseHandler {
    private final static String NO_STALL_INFO_STRING = "NoStallInfo";
    
    private final static Logger logger = Logger.getLogger(StallInfoRequestHandler.class);
    protected String lotNumber = null;
    private int stallNumber = 0;
    private int addTimeNumber = 0;
    protected Date timeStamp = null;
    private String plateNum;
    
    protected ActivePermitService activePermitService = null;
    protected SpaceInfoService spaceInfoService = null;
    
    public StallInfoRequestHandler(String messageNumber) {
        super(messageNumber);
    }
    
    public String getRootElementName() {
        return MessageTags.STALL_INFO_REQUEST_TAG_NAME;
    }
    
    public void process(XMLBuilderBase xmlBuilder) {
        long start_time = System.currentTimeMillis();
        
        logger.info("Processing StallInfoRequest for PS: " + posSerialNumber + ".");
        
        process((PS2XMLBuilder) xmlBuilder);
        
        float processing_time = (System.currentTimeMillis() - start_time) / MILLISECOND_TO_SECOND;
        logger.info("Returning StallInfoResponse to PS: " + posSerialNumber + " after " + processing_time + " seconds.");
    }
    
    private void process(PS2XMLBuilder xmlBuilder) {
        // Validate paystation
        if (!validatePOS(xmlBuilder)) {
            return;
        }
        
        try {
            if ((lotNumber == null) || (StringUtils.isBlank(plateNum) && stallNumber == 0 && addTimeNumber == 0)) {
                logger.warn("Encountered NULL LotNumber or StallNumber or AddTimeNumber while processing " + getRootElementName());
                xmlBuilder.createStallInfoResponse(this, null, NO_STALL_INFO_STRING);
            } else {
                SpaceInfo stall_info = getStallInfo(pointOfSale, lotNumber, stallNumber, addTimeNumber, timeStamp, plateNum);
                
                if (stall_info == null) {
                    if (addTimeNumber == 0) {
                        xmlBuilder.createStallInfoResponse(this, null, NO_STALL_INFO_STRING);
                    } else {
                        xmlBuilder.createStallInfoForAddTimeNumResponse(this, null, NO_STALL_INFO_STRING);
                    }
                } else {
                    if (addTimeNumber == 0) {
                        xmlBuilder.createStallInfoResponse(this, stall_info, SUCCESS_STRING);
                    } else {
                        xmlBuilder.createStallInfoForAddTimeNumResponse(this, stall_info, SUCCESS_STRING);
                    }
                }
            }
        } catch (Exception e) {
            String msg = "Unable to process StallInfoRequest for PS: " + posSerialNumber;
            processException(msg, e);
            
            // PBS with no addtime number
            if (addTimeNumber == 0) {
                xmlBuilder.createStallInfoForAddTimeNumResponse(this, null, BaseHandler.EMS_UNAVAILABLE_STRING);
            }
            
            // PBS with addtime number
            // PND with addtime number
            else {
                xmlBuilder.createStallInfoResponse(this, null, BaseHandler.EMS_UNAVAILABLE_STRING);
            }
        }
    }
    
    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }
    
    public void setAddTimeNum(String addTimeNum) {
        addTimeNumber = Integer.parseInt(addTimeNum);
    }
    
    public void setStallNumber(String stall) {
        stallNumber = Integer.parseInt(stall);
    }
    
    public void setTimeStamp(String value) throws InvalidDataException {
        timeStamp = DateUtil.convertFromColonDelimitedDateString(value);
    }
    
    public String getPlateNum() {
        return plateNum;
    }
    
    public void setPlateNum(String plateNum) {
        this.plateNum = plateNum;
    }
    
    // Copied from RPC PaystationAppServiceImpl
    
    public SpaceInfo getStallInfo(PointOfSale pos, String paystationSetting, int stallNumber, int addTimeNumber, Date currentPsDate, String plateNum)
            throws ApplicationException, SystemException {
        // Use original getStallInfo method if no plateNum.
        if (StringUtils.isBlank(plateNum)) {
            return getStallInfo(pos, paystationSetting, stallNumber, addTimeNumber, currentPsDate);
        }
        
        return getStallInfoByPlateNumber(pos, paystationSetting, currentPsDate, plateNum, stallNumber, addTimeNumber);
    }
    
    private SpaceInfo getStallInfoByPlateNumber(PointOfSale pos, String paystationSetting, Date currentPsDate, String plateNum, int stallNumber, int addTimeNumber)
            throws ApplicationException {
        
        CustomerProperty custProp = customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(pos.getCustomer().getId(), WebCoreConstants.CUSTOMER_PROPERTY_TYPE_QUERY_SPACES_BY);
        // Use default if 'querySpaceBy' is not defined.
        int querySpaceBy = PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME;
        if (custProp != null) {
            querySpaceBy = Integer.parseInt(custProp.getPropertyValue());
        }

        SpaceInfo stall = null;
        if (querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME || querySpaceBy ==  PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT) {        
        stall = this.spaceInfoService.getSpaceInfoByCustomerAndPlateNumber(pos.getCustomer().getId(), currentPsDate, plateNum, querySpaceBy);
        } else {
            stall = this.spaceInfoService.getSpaceInfoByLocationAndPlateNumber(pos.getCustomer().getId(), pos.getLocation().getId(), currentPsDate, plateNum);
            
        }
        
        // Check if stall info exists
        if (stall == null) {
            return (null);
        }
        
        // Set total time purchased
        stall.setTotalTimePurchased(determineTotalTimePurchased(stall.getStartDate(), stall.getEndDate()));
        return stall;
    }
    
    private SpaceInfo getStallInfoByAddTimeNumber(PointOfSale pos, String paystationSettingName, int addTimeNumber, int spaceNumber, Date currentPosDate)
            throws ApplicationException {
        SpaceInfo stall = null;
        
        int querySpaceBy = super.getQuerySpaceBy(-1);
        switch (querySpaceBy) {
            case PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_EXPIRY_TIME_OF_MOST_RECENT_PERMIT: {
                stall = spaceInfoService.getValidByLocationPurchaseExpiryAddTime(pos.getLocation().getId(), addTimeNumber, currentPosDate, pos.getCustomer()
                        .getId());
                break;
            }
            case PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_LATEST_EXPIRY_TIME: {
                stall = spaceInfoService.getValidByLocationPurchaseExpiryAddTimeOrderByLatestExpiry(pos.getLocation().getId(), addTimeNumber, currentPosDate, pos.getCustomer()
                        .getId());
                break;
            }
            case PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT: {
                stall = spaceInfoService.getValidByCustomerPurchaseExpiryAddTime(addTimeNumber, currentPosDate, pos.getCustomer().getId());
                break;
            }
            case PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME: {
                stall = spaceInfoService.getValidByCustomerPurchaseExpiryAddTimeOrderByLatestExpiry(addTimeNumber, currentPosDate, pos.getCustomer().getId());
                break;
            }
            default: {
                StringBuilder bdr = new StringBuilder();
                bdr.append("Invalid stall query type(-1) for PS: ").append(pos.getSerialNumber());
                throw new ApplicationException(bdr.toString());
            }
        }
        
        // Check if stall info exists
        if (stall == null) {
            return (null);
        }
        
        // Set total time purchased
        stall.setTotalTimePurchased(determineTotalTimePurchased(stall.getStartDate(), stall.getEndDate()));
        return stall;
    }
    
    public final SpaceInfo getStallInfo(PointOfSale pos, String paystationSetting, int stallNumber, int addTimeNumber, Date currentPsDate) throws ApplicationException {
        // PBS with no addtime number (all versions PS1 and PS2)
        if (addTimeNumber == 0) {
            return (getStallInfoByStall(pos, paystationSetting, stallNumber, currentPsDate));
        }
        
        // PBS with addtime number (6.1 and above)
        return (getStallInfoByAddTimeNumber(pos, paystationSetting, addTimeNumber, stallNumber, currentPsDate));
    }
    
    private final SpaceInfo getStallInfoByStall(PointOfSale pos, String paystationSettingName, int spaceNumber, Date currentPosDate) throws ApplicationException {
        SpaceInfo stall = null;

        int querySpaceBy = super.getQuerySpaceBy(-1);
        switch (querySpaceBy) {
            case PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_EXPIRY_TIME_OF_MOST_RECENT_PERMIT: {
                stall = this.spaceInfoService.getValidByLocationPurchaseExpirySpaceNumber(pos.getLocation().getId(), spaceNumber, currentPosDate, pos.getCustomer().getId());
                break;
            }
            case PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_LATEST_EXPIRY_TIME: {
                stall = this.spaceInfoService.getValidByLocationPurchaseExpirySpaceNumberOrderByLatestExpiry(pos.getLocation().getId(), spaceNumber, currentPosDate, pos.getCustomer().getId());
                break;
            }            
            case PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT: {
                stall = this.spaceInfoService.getValidByCustomerPurchaseExpirySpaceNumber(spaceNumber, currentPosDate, pos.getCustomer().getId());
                break;
            }
            case PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME: {
                stall = this.spaceInfoService.getValidByCustomerPurchaseExpirySpaceNumberOrderByLatestExpiry(spaceNumber, currentPosDate, pos.getCustomer().getId());
                break;
            }            
            default: {
                StringBuilder bdr = new StringBuilder();
                bdr.append("getStallInfoByStall, invalid stall query type(-1) for PS: ").append(pos.getSerialNumber());
                throw new ApplicationException(bdr.toString());
            }
        }
        // Check if stall info exists
        if (stall == null) {
            return (null);
        }
        
        // Set total time purchased
        stall.setTotalTimePurchased(determineTotalTimePurchased(stall.getStartDate(), stall.getEndDate()));
        
        return stall;
        
    }
    
    private int determineTotalTimePurchased(List<ActivePermit> maxPurchasedDateList, List<ActivePermit> maxExpiryDateList) {
        Date max_purchased_date = null, max_expiry_date = null;
        
        // Max purchased date
        if (maxPurchasedDateList.size() == 1) {
            max_purchased_date = maxPurchasedDateList.get(0).getPermitBeginGmt();
        }
        if (max_purchased_date == null) {
            return (0);
        }
        
        // Max expiry date
        if (maxExpiryDateList.size() == 1) {
            max_expiry_date = maxExpiryDateList.get(0).getPermitExpireGmt();
        }
        if (max_expiry_date == null) {
            throw new RuntimeException("Unable to get total time purchased as max expiry date cannot be found");
        }
        
        // Total time purchased
        long timeInMs = max_expiry_date.getTime() - max_purchased_date.getTime();
        return ((int) (timeInMs / 1000));
    }
    
    private int determineTotalTimePurchased(Date purchasedGmt, Date expireGmt) {
        
        // Total time purchased
        long timeInMs = expireGmt.getTime() - purchasedGmt.getTime();
        return ((int) (timeInMs / 1000));
    }
    
    public void setWebApplicationContext(WebApplicationContext ctx) {
        super.setWebApplicationContext(ctx);
        this.customerAdminService = (CustomerAdminService) ctx.getBean("customerAdminService");
        this.activePermitService = (ActivePermitService) ctx.getBean("activePermitService");
        this.spaceInfoService = (SpaceInfoService) ctx.getBean("spaceInfoService");
        this.paystationSettingService = (PaystationSettingService) ctx.getBean("paystationSettingService");
    }
    
}