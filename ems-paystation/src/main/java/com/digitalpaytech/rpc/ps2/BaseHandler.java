package com.digitalpaytech.rpc.ps2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.annotation.StoryLookup;
import com.digitalpaytech.cardprocessing.cps.ProcessingDestinationService;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerAgreement;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerURLReroute;
import com.digitalpaytech.domain.LinuxConfigurationFile;

import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MerchantPOS;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.SettingsFile;
import com.digitalpaytech.domain.URLReroute;
import com.digitalpaytech.dto.cps.CPSResponse;
import com.digitalpaytech.dto.paystation.TransactionDto;
import com.digitalpaytech.exception.CcProcessingRestrictionException;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.ribbon.ClientFactory;
import com.digitalpaytech.rpc.support.IRequestDataHandler;

import com.digitalpaytech.rpc.support.LinuxConfigFileNotificationInfo;
import com.digitalpaytech.rpc.support.LinuxConfigNotificationInfo;

import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerAgreementService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.PaystationSettingService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.paystation.LinuxConfigurationFileService;
import com.digitalpaytech.service.paystation.PosServiceStateService;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.json.JSON;

public abstract class BaseHandler implements IRequestDataHandler {
    
    protected static final String EMS_UNAVAILABLE_STRING = "EMSUnavailable";
    protected static final String EMS_LOGIN_FAILED_STRING = "EMSLoginFailed";
    protected static final String CRYPTO_EXCEPTION_STRING = "CryptoException";
    protected static final String INVALID_TYPE_STRING = "InvalidType";
    protected static final String MESSAGE_NUMBER_STRING = "MessageNumber";
    protected static final String PAYSTATION_COMM_ADDRESS_STRING = "PaystationCommAddress";
    protected static final String SUCCESS_STRING = "Success";
    protected static final String UNSUBSCRIBED_SERVICE_STRING = "UnsubscribedService";
    protected static final String EMPTY_STRING = "";
    protected static final String OUT_OF_ORDER = "OutOfOrder";
    protected static final String INVALID_MESSAGE = "InvalidMessage";
    protected static final String MERCHANT_ACCOUNT_ID_STRING = "MerchantID";
    protected static final float MILLISECOND_TO_SECOND = 1000f;
    private static final String POS_LOG = "POS: ";
    private static final String MERCHANT_ACCOUNT_NO_TERMINAL_TOKEN = "Merchant Accounts does not contain Terminal Token";
    private static final String SERVICE_AGREEMENT_IS_NOT_ACCEPTED = "+++ Service Agreement is not accepted +++";
    private static final Logger LOG = Logger.getLogger(BaseHandler.class);
    
    protected PointOfSale pointOfSale;
    protected String messageNumber;
    
    // Name left as Point Of Sale Serial Number because of direct relation to
    // the message XML
    protected String posSerialNumber;
    
    protected ClientFactory clientFactory;
    protected MessageHelper messageHelper;
    protected MailerService mailerService;
    protected MerchantAccountService merchantAccountService;
    protected PointOfSaleService pointOfSaleService;
    protected PosServiceStateService posServiceStateService;
    protected UserAccountService userAccountService;
    protected CustomerAgreementService customerAgreementService;
    protected CustomerService customerService;
    protected PaystationSettingService paystationSettingService;
    protected CustomerAdminService customerAdminService;
    protected LinuxConfigurationFileService linuxConfigurationFileService;
    protected ProcessingDestinationService processingDestinationService;
    
    protected String version;
    private JSON json = new JSON();
    
    // TODO PaystationAppService
    /*
     * @Autowired protected RpcPaystation2AppService paystationAppService;
     */
    
    // TODO CustomerAppService
    /*
     * @Autowired protected CustomerAppService customerAppService;
     */
    
    // TODO SensorAppService
    /*
     * @Autowired protected SensorAppService sensorAppService;
     */
    
    /*
     * public void setPaystationAppService(RpcPaystation2AppService
     * paystationAppService) { this.paystationAppService = paystationAppService;
     * }
     */
    
    /*
     * public void setCustomerAppService(CustomerAppService customerAppService)
     * { this.customerAppService = customerAppService; }
     * public void setSensorAppService(SensorAppService sensorAppService) {
     * this.sensorAppService = sensorAppService; }
     */
    
    protected BaseHandler(final String messageNumber) {
        this.messageNumber = messageNumber;
    }
    
    public final String getMessageNumber() {
        return this.messageNumber;
    }
    
    @StoryAlias("serial number")
    @StoryLookup(type = PointOfSale.ALIAS, searchAttribute = PointOfSale.ALIAS_SERIAL_NUMBER)
    public final String getPosSerialNumber() {
        return this.posSerialNumber;
    }
    
    public final void setPosSerialNumber(final String posSerialNumber) {
        this.posSerialNumber = posSerialNumber;
    }
    
    public String getPaystationCommAddress() {
        return this.posSerialNumber;
    }
    
    public void setPaystationCommAddress(final String paystationCommAddress) {
        this.posSerialNumber = paystationCommAddress;
    }
    
    public final String getSuccessString() {
        return SUCCESS_STRING;
    }
    
    protected void processWarning(final String subject, final String msg, final Exception e) {
        LOG.warn(msg, e);
        this.mailerService.sendAdminWarnAlert(null, msg, e);
    }
    
    public String getVersion() {
        return this.version;
    }
    
    public final void setVersion(final String version) {
        this.version = version;
    }
    
    protected void processProcessorOfflineException(final String msg, final Exception e) {
        LOG.warn(msg, e);
        this.mailerService.sendAdminWarnAlert(MailerService.PROCESSOR_OFFLINE, msg, e);
    }
    
    protected void processException(final String msg, final Exception e) {
        LOG.error(msg, e);
        this.mailerService.sendAdminErrorAlert(MailerService.EXCEPTION, msg, e);
    }
    
    protected final boolean validatePOS(final PS2XMLBuilder xmlBuilder) {
        final PointOfSale pos = processPOSRetrieval(this.posSerialNumber, xmlBuilder);
        return validatePOSAndSetReference(xmlBuilder, pos);
    }
    
    protected final boolean validatePOS(final PS2XMLBuilder xmlBuilder, final String cpsResponseString) {
        
        if (StringUtils.isBlank(cpsResponseString)) {
            return validatePOS(xmlBuilder);
        } else {
            try {
                final CPSResponse cpsResponse = this.json.deserialize(cpsResponseString, CPSResponse.class);
                final String terminalToken = cpsResponse.getTerminalToken();
                
                if (StringUtils.isBlank(terminalToken)) {
                    LOG.error("Terminal Token is blank in message " + this.messageNumber);
                    xmlBuilder.insertErrorMessage(this.messageNumber, BaseHandler.EMS_UNAVAILABLE_STRING);
                    return false;
                }
                
                final PointOfSale pos = handleChangedMerchantAccount(terminalToken, this.posSerialNumber, xmlBuilder);
                return validatePOSAndSetReference(xmlBuilder, pos);

            } catch (JsonException e) {
                LOG.error("Unable to process XMLRPC from POS " + this.posSerialNumber, e);
                xmlBuilder.insertErrorMessage(this.messageNumber, BaseHandler.EMS_UNAVAILABLE_STRING);
                return false;
            }
        }
    }

    private boolean validatePOSAndSetReference(final PS2XMLBuilder xmlBuilder, final PointOfSale pos) {
        if (pos == null) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append(POS_LOG).append(this.posSerialNumber).append(" does not exist");
            LOG.error(bdr.toString());
            
            xmlBuilder.setIsValidPaystation(false);
            return false;
        } else if (isDeactivated(pos)) {
            LOG.warn(POS_LOG + this.posSerialNumber + " is deactivated.");
            checkCustomerURLReroute(xmlBuilder, pos);
            xmlBuilder.insertErrorMessage(this.messageNumber, BaseHandler.EMS_UNAVAILABLE_STRING);
            return false;
        } else if (!isCustomerServiceAgreementSigned(pos.getCustomer().getId())) {
            LOG.error(SERVICE_AGREEMENT_IS_NOT_ACCEPTED);
            xmlBuilder.setIsValidPaystation(false);
            return false;
        }
        this.pointOfSale = pos;
        return true;
    }
    
    private PointOfSale processPOSRetrieval(final String posSerialNum, final PS2XMLBuilder xmlBuilder) {
        final PointOfSale pos = this.pointOfSaleService.findPointOfSaleBySerialNumber(posSerialNum);
        if (pos == null) {
            return null;
        }
        
        // TODO PosStatus Table empty
        /*
         * PosStatus posStatus = pointOfSaleService.findPointOfSaleStatusByPOSId(pos.getId());
         * if (posStatus == null) return null;
         * pos.getPointOfSaleStatuses().add(posStatus);
         * if (!posStatus.isIsLocked()) {
         */
        try {
            this.pointOfSaleService.updatePaystationActivityTime(pos, xmlBuilder.isHeartBeatUpdated());
            xmlBuilder.setIsHeartBeatUpdated(true);
        } catch (Exception sose) {
            // There are no more chances that heartbeat update will failed because of StaleObjectException.
            // However, it is a good idea to just catch the exception, because even the heartbeat update failed, other things still have to be saved.
            final StringBuilder bdr = new StringBuilder();
            bdr.append("BaseHandler.processPOSRetrieval, cannot update PosHeartbeat.");
            xmlBuilder.setIsHeartBeatUpdated(true);
            LOG.error(bdr.toString());
        }
        
        return pos;
    }
    
    private PointOfSale handleChangedMerchantAccount(final String terminalToken, final String posSerialNum, final PS2XMLBuilder xmlBuilder) {
        
        final MerchantAccount merchantAccount = this.merchantAccountService.findByTerminalTokenAndIsLink(terminalToken);
        
        if (merchantAccount == null) {
            final StringBuilder bdr = new StringBuilder().append(MERCHANT_ACCOUNT_NO_TERMINAL_TOKEN).append(": ").append(terminalToken).append(". ");
            
            LOG.error(bdr.toString());
            this.mailerService.sendAdminErrorAlert(MERCHANT_ACCOUNT_NO_TERMINAL_TOKEN, bdr.toString(), null);
            
            xmlBuilder.insertErrorMessage(this.messageNumber, BaseHandler.EMS_UNAVAILABLE_STRING);
            return null;
        }
        
        final MerchantPOS relation =
                this.merchantAccountService.findMerchantPOSByMerchantAccountTerminalTokenAndSerialNumber(terminalToken, posSerialNum);
        
        final PointOfSale timeOfTransactionPOS;
        if (relation == null) {
            timeOfTransactionPOS = this.pointOfSaleService.findPointOfSaleBySerialNumber(posSerialNum);
        } else {
            timeOfTransactionPOS = relation.getPointOfSale();
        }
        
        if (timeOfTransactionPOS == null) {
            return null;
        }
        
        this.pointOfSaleService.updatePaystationActivityTime(timeOfTransactionPOS, xmlBuilder.isHeartBeatUpdated());
        xmlBuilder.setIsHeartBeatUpdated(true);
        
        return timeOfTransactionPOS;
    }
    
    private boolean isCustomerServiceAgreementSigned(final int customerId) {
        
        final Customer customer = this.customerService.findCustomer(customerId);
        int usedCustomerId = customer.getId();
        if (!customer.isIsParent() && customer.getParentCustomer() != null) {
            usedCustomerId = customer.getParentCustomer().getId();
        }
        final CustomerAgreement agreement = this.customerAgreementService.findCustomerAgreementByCustomerId(usedCustomerId);
        return !(agreement == null);
    }
    
    protected final void checkPaystationSettingAndSettingFile(final TransactionDto txDto) {
        if (this.pointOfSale.getSettingsFile() == null || !txDto.getLotNumber().equals(this.pointOfSale.getSettingsFile().getName())) {
            final SettingsFile settingsFile =
                    this.paystationSettingService.findSettingsFileByCustomerIdAndName(this.pointOfSale.getCustomer().getId(), txDto.getLotNumber());
            this.pointOfSale.setSettingsFile(settingsFile);
            
            final PosServiceState posServiceState = this.pointOfSale.getPosServiceState();
            posServiceState.setPaystationSettingName(txDto.getLotNumber());
            this.posServiceStateService.updatePosServiceState(posServiceState);
            this.pointOfSaleService.update(this.pointOfSale);
        }
    }
    
    protected final void checkForNotifications(final PS2XMLBuilder xmlBuilder) {
        try {
            if (this.pointOfSale.getPosServiceState().isIsNewPaystationSetting()) {
                xmlBuilder.createPaystationSettingNotification();
            }
        } catch (Exception e) {
            final String msg = "Unable to process PaystationSettingCheckRequest for PointOfSale: " + this.pointOfSale;
            processException(msg, e);
        }
        
        try {
            // when key recycling implemented
            if (this.pointOfSale.getPosServiceState().isIsNewPublicKey()) {
                xmlBuilder.createNewPublicKeyNotification();
            }
        } catch (Exception e) {
            processException("Unable to process NewPublicKeyCheckRequest for PS: " + this.posSerialNumber, e);
        }
        try {
            if (this.pointOfSale.getPosServiceState().isIsNewMerchantAccount()) {
                xmlBuilder.createNewMerchantAccountNotification();
            }
        } catch (Exception e) {
            processException("Unable to process NewMerchantAccount for PS: " + this.posSerialNumber, e);
        }
        try {
            if (this.pointOfSale.getPosServiceState().isIsNewOTAFirmwareUpdate()) {
                xmlBuilder.createNewOTAFirmwareUpdateNotification();
            }
        } catch (Exception e) {
            processException("Unable to process OTAFirmwareUpdate for PS: " + this.posSerialNumber, e);
        }
        try {
            if (this.pointOfSale.getPosServiceState().isIsNewOTAFirmwareUpdateCancelPending()) {
                xmlBuilder.createNewOTAFirmwareUpdateCancelPendingNotification();
            }
        } catch (Exception e) {
            processException("Unable to process OTAFirmwareUpdateCancelPending for PS: " + this.posSerialNumber, e);
        }
        try {
            if (this.pointOfSale.getPosServiceState().getNewConfigurationId() != null) {
                final int pointOfSaleId = this.pointOfSale.getId();
                final String snapShotId = this.pointOfSale.getPosServiceState().getNewConfigurationId();
                final List<LinuxConfigFileNotificationInfo> configFileInfoList = new ArrayList<LinuxConfigFileNotificationInfo>();
                final List<LinuxConfigurationFile> fileList =
                        this.linuxConfigurationFileService.findByPointOfSaleIdAndSnapshotId(pointOfSaleId, snapShotId);
                if (fileList != null) {
                    for (LinuxConfigurationFile file : fileList) {
                        configFileInfoList
                                .add(new LinuxConfigFileNotificationInfo(file.getFileId(), file.getFileName(), file.getHash(), file.getSize()));
                    }
                }
                final LinuxConfigNotificationInfo notification = new LinuxConfigNotificationInfo(pointOfSaleId, snapShotId, configFileInfoList);
                xmlBuilder.createNewConfigurationNotification(pointOfSaleId, snapShotId, notification);
            }
        } catch (Exception e) {
            processException("Unable to process NewConfiguration for PS: " + this.posSerialNumber, e);
        }
        try {
            if (this.pointOfSale.getPosServiceState().isIsNewBINRange()) {
                xmlBuilder.createNewBINRangeNotification();
            }
        } catch (Exception e) {
            processException("Unable to process NewBINRange for PS: " + this.posSerialNumber, e);
        }
        
        try {
            
            if (this.pointOfSale.getPosServiceState().isIsNewRootCertificate()) {
                xmlBuilder.createNewRootCertificateNotification();
            }
        } catch (Exception e) {
            processException("Unable to process NewRootCertificateCheckRequest for PS: " + this.posSerialNumber, e);
        }

        try {
            if (this.pointOfSale.getPosServiceState().isIsNewBadCardList()) {
                xmlBuilder.createNewBadCardListNotification();
            }
        } catch (Exception e) {
            processException("Unable to process NewBadCardListNotification for PS: " + this.posSerialNumber, e);
        }        
        
        checkCustomerURLReroute(xmlBuilder, this.pointOfSale);
        
    }
    
    private void checkCustomerURLReroute(final PS2XMLBuilder xmlBuilder, final PointOfSale pos) {
        try {
            final CustomerURLReroute customerURLReroute = this.customerService.findURLRerouteByCustomerId(pos.getCustomer().getId());
            if (customerURLReroute != null) {
                addMigrationNotification(xmlBuilder, customerURLReroute);
            }
        } catch (Exception e) {
            processException("Unable to process URLReroute for PS: " + pos.getSerialNumber() + ", exception message: " + e.getMessage(), e);
        }
    }
    
    protected final boolean checkInternalPassCardProcessingRestriction() {
        final Integer[] subscriptionCodes = { WebCoreConstants.SUBSCRIPTION_TYPE_PASSCARDS };
        if (!WebSecurityUtil.checkForPosSubscriptions(this.pointOfSale, subscriptionCodes)) {
            throw new CcProcessingRestrictionException("Not allow to process internally authorized passcards", true);
        }
        return true;
    }
    
    protected final boolean checkExternalPassCardProcessingRestriction() {
        final Integer[] subscriptionCodes = { WebCoreConstants.SUBSCRIPTION_TYPE_REALTIME_CAMPUS_CARD_PROCESSING };
        if (!WebSecurityUtil.checkForPosSubscriptions(this.pointOfSale, subscriptionCodes)) {
            throw new CcProcessingRestrictionException("Not allow to process externally authorized passcards", true);
        }
        return true;
    }
    
    protected final boolean checkCcProcessingRestriction() {
        final Integer[] subscriptionCodes = { WebCoreConstants.SUBSCRIPTION_TYPE_REALTIME_CC_PROCESSING };
        if (!WebSecurityUtil.checkForPosSubscriptions(this.pointOfSale, subscriptionCodes)) {
            throw new CcProcessingRestrictionException("Not allow to process Credit Card", true);
        }
        return true;
    }
    
    /**
     * @param defaultQuerySpaceById
     *            If 'query space by' CustomerProperty doesn't exist, return the specific defaultQuerySpaceById.
     * @return int query space by value in CustomerProperty table or defaultQuerySpaceById if doesn't exist.
     */
    protected int getQuerySpaceBy(final int defaultQuerySpaceById) {
        final CustomerProperty custProp = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(this.pointOfSale.getCustomer().getId(),
                                                                        WebCoreConstants.CUSTOMER_PROPERTY_TYPE_QUERY_SPACES_BY);
        
        if (custProp != null) {
            return Integer.parseInt(custProp.getPropertyValue());
        }
        return defaultQuerySpaceById;
    }
    
    /**
     * A pay station is deactivated when PosStatus table, IsActivated column is 0 (false).
     * Note, IsLocked column is not being used.
     * 
     * @return boolean true if IsActivated = 0 (false).
     */
    protected boolean isDeactivated(final PointOfSale pos) {
        if (!pos.getPosStatus().isIsActivated()) {
            return true;
        }
        return false;
    }
    
    public void setWebApplicationContext(final WebApplicationContext ctx) {
        this.pointOfSaleService = (PointOfSaleService) ctx.getBean("pointOfSaleService");
        this.posServiceStateService = (PosServiceStateService) ctx.getBean("posServiceStateService");
        this.mailerService = (MailerService) ctx.getBean("mailerService");
        this.merchantAccountService = (MerchantAccountService) ctx.getBean("merchantAccountService");
        this.userAccountService = (UserAccountService) ctx.getBean("userAccountService");
        this.customerAgreementService = (CustomerAgreementService) ctx.getBean("customerAgreementService");
        this.customerService = (CustomerService) ctx.getBean("customerService");
        this.paystationSettingService = (PaystationSettingService) ctx.getBean("paystationSettingService");
        this.customerAdminService = (CustomerAdminService) ctx.getBean("customerAdminService");
        this.messageHelper = (MessageHelper) ctx.getBean("MessageHelper");
        this.clientFactory = (ClientFactory) ctx.getBean("clientFactory");
        this.linuxConfigurationFileService = (LinuxConfigurationFileService) ctx.getBean("linuxConfigurationFileService");
        this.processingDestinationService = ctx.getBean(ProcessingDestinationService.class);
    }
    
    private PS2XMLBuilder addMigrationNotification(final PS2XMLBuilder xmlBuilder, final CustomerURLReroute customerURLReroute) throws Exception {
        
        final URLReroute urlReroute = customerURLReroute.getUrlReroute();
        
        final Map<String, String> namesValues = new HashMap<String, String>(3);
        namesValues.put(WebCoreConstants.DOMAIN_NOTIFICATION, urlReroute.getNewManagementSysURL());
        namesValues.put(WebCoreConstants.IP_NOTIFICATION, urlReroute.getNewManagementSysIP());
        namesValues.put(WebCoreConstants.DIGITAL_CONNECT_IP_NOTIFICATION, urlReroute.getDigitalConnectIP());
        xmlBuilder.createEMSPaystationURLRerouteNotification(namesValues);
        
        return xmlBuilder;
    }
    
    public final ClientFactory getClientFactory() {
        return this.clientFactory;
    }
    
    public final void setClientFactory(final ClientFactory clientFactory) {
        this.clientFactory = clientFactory;
    }
    
}
