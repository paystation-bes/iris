package com.digitalpaytech.rpc.ps2;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;

import com.digitalpaytech.domain.PosCollection;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.rpc.support.BillCol;
import com.digitalpaytech.rpc.support.CoinCol;
import com.digitalpaytech.rpc.support.MessageTags;
import com.digitalpaytech.service.CollectionTypeService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.paystation.PosCollectionService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.xml.XMLBuilderBase;

public class CollectionHandler extends BaseHandler {
    private final static Logger log = Logger.getLogger(CollectionHandler.class);
    private static final String BILL = "Bill";
    private static final String COIN = "Coin";
    private static final String CARD = "Card";
    
    private PosCollection audit;
    private PosCollectionService posCollectionService;
    private CollectionTypeService collectionTypeService;
    private String type;
    private StringBuilder coinCollection;
    private StringBuilder billCollection;
    private String lotNumber;
    
    public CollectionHandler(String messageNumber) {
        super(messageNumber);
        audit = new PosCollection();
        coinCollection = new StringBuilder();
        billCollection = new StringBuilder();
    }
    
    public String getRootElementName() {
        return MessageTags.COLLECTION_REQUEST_TAG_NAME;
    }
    
    public void process(XMLBuilderBase xmlBuilder) {
        long start_time = System.currentTimeMillis();
        process((PS2XMLBuilder) xmlBuilder);
        
        if (log.isInfoEnabled()) {
            float processing_time = (System.currentTimeMillis() - start_time) / MILLISECOND_TO_SECOND;
            StringBuilder bdr = new StringBuilder();
            if (pointOfSale == null) {
                bdr.append("PointOfSale serial number is incorrect, unable to retrieve record. Processing time: ");
            } else {
                bdr.append("Returning response to PointToSale serialNumber: ").append(pointOfSale.getSerialNumber()).append(" after ");
            }
            bdr.append(processing_time).append(" seconds.");
            log.info(bdr.toString());
        }
    }
    
    private void process(PS2XMLBuilder xmlBuilder) {
        if (!validatePOS(xmlBuilder)) {
            return;
        }
        try {
            // Set PaystationSetting.
            audit.setPaystationSetting(paystationSettingService.findByCustomerIdAndLotName(pointOfSale.getCustomer(), this.lotNumber));
            
            // Populate PosCollection class with xml data.
            audit.setPointOfSale(pointOfSale);
            audit.setCustomer(pointOfSale.getCustomer());
            
            // Coin
            audit.setCoinTotalAmount(getCoinAmountBase100());
            
            // Bill
            BillCol billCol = new BillCol(billCollection.toString());
            audit.setBillAmount1(billCol.getTotalAmount1());
            audit.setBillAmount2(billCol.getTotalAmount2());
            audit.setBillAmount5(billCol.getTotalAmount5());
            audit.setBillAmount10(billCol.getTotalAmount10());
            audit.setBillAmount20(billCol.getTotalAmount20());
            audit.setBillAmount50(billCol.getTotalAmount50());
            audit.setBillTotalAmount(billCol.getBillDollars());
            
            // Card
            audit.setCardTotalAmount(getCardAmountBase100());
            
            // Verify if pay station does or does not provide details.
            if (!isValidXmlRpc(billCollection.toString())) {
                StringBuilder bdr = new StringBuilder();
                bdr.append("CollectionHandler, Failed to process, no ").append(type).append(" detail.");
                log.error(bdr.toString());
                xmlBuilder.insertErrorMessage(messageNumber, bdr.toString());
                return;
            }
            
            if (posCollectionService.isDuplicateRecord(audit)) {
                log.info("Found duplicate PosCollection: " + audit);
                xmlBuilder.insertAcknowledge(messageNumber);
                return;
            }
            
            posCollectionService.processPosCollection(pointOfSale, audit);
            xmlBuilder.insertAcknowledge(messageNumber);
            
        } catch (Exception e) {
            String msg = "Unable to process Collection for PointOfSale SerialNumber: " + pointOfSale.getSerialNumber();
            processException(msg, e);
            xmlBuilder.insertErrorMessage(messageNumber, BaseHandler.EMS_UNAVAILABLE_STRING);
        }
        
    }
    
    private boolean isValidXmlRpc(String billCollection) {
        if (StringUtils.isEmpty(type)) {
            return false;
        }
        if (type.equals(COIN) && audit.getCoinTotalAmount() >= 0) {
            return true;
        }
        if (type.equals(BILL) && StringUtils.isNotEmpty(billCollection)) {
            return true;
        }
        if (type.equals(CARD) && hasDollars()) {
            return true;
        }
        return false;
    }
    
    private boolean hasDollars() {
        if (audit.getAmexAmount() >= 0 
                && audit.getDiscoverAmount() >= 0 
                && audit.getMasterCardAmount() >= 0 
                && audit.getValueCardAmount() >= 0
                && audit.getVisaAmount() >= 0 
                && audit.getSmartCardAmount() >= 0 
                && audit.getDinersAmount() >= 0
                && audit.getJCBAmount() >= 0) {
            return true;
        }
        return false;
    }
    
    // --------------------------------------------------------------------------------
    // Common fields
    // --------------------------------------------------------------------------------
    
    // e.g. <Type>Coin</Type>
    public void setType(String type) {
        this.type = type;
        audit.setCollectionType(collectionTypeService.findCollectionType(type));
    }
    
    // e.g. <PaystationCommAddress>000000001234</PaystationCommAddress>, set in BaseHandler.
    
    // e.g. <ReportNumber>1</ReportNumber>
    public void setReportNumber(String reportNumber) {
        audit.setReportNumber(Integer.parseInt(reportNumber));
    }
    
    // e.g. <LotNumber>5th and Main</LotNumber>
    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }
    
    // e.g. <MachineNumber>3</MachineNumber>
    public void setMachineNumber(String machineNumber) {
        log.warn("CollectionHandler, <MachineNumber> value is: " + machineNumber);
    }
    
    // e.g. <StartDate>2012:01:01:19:21:45:GMT</StartDate>
    public void setStartDate(String startDate) throws InvalidDataException {
        audit.setStartGmt(DateUtil.convertFromColonDelimitedDateString(startDate));
    }
    
    // e.g. <EndDate>2012:01:06:15:35:25:GMT</EndDate>
    public void setEndDate(String endDate) throws InvalidDataException {
        audit.setEndGmt(DateUtil.convertFromColonDelimitedDateString(endDate));
    }
    
    // e.g. <TicketCount>417</TicketCount>
    public void setTicketCount(String ticketCount) {
        audit.setTicketsSold(Integer.parseInt(ticketCount));
    }
    
    // e.g. <StartTransNumber>193</StartTransNumber>
    public void setStartTransNumber(String startTransNumber) {
        audit.setStartTransactionNumber(Integer.parseInt(startTransNumber));
    }
    
    // e.g. <EndTransNumber>646</EndTransNumber>
    public void setEndTransNumber(String endTransNumber) {
        audit.setEndTransactionNumber(Integer.parseInt(endTransNumber));
    }
    
    private StringBuilder appendCommaIfNecessary(StringBuilder collection) {
        if (collection.length() != 0) {
            collection.append(StandardConstants.STRING_COMMA);
        }
        return collection;
    }
    
    // --------------------------------------------------------------------------------
    // Coins
    // --------------------------------------------------------------------------------
    
    // e.g. <CoinCount005>473</CoinCount005>
    public void setCoinCount005(String coinCount005) {
        audit.setCoinCount05(Integer.parseInt(coinCount005));
        coinCollection = appendCommaIfNecessary(coinCollection);
        coinCollection.append("5:").append(coinCount005);
    }
    
    // e.g. <CoinCount010>310</CoinCount010>
    public void setCoinCount010(String coinCount010) {
        audit.setCoinCount10(Integer.parseInt(coinCount010));
        coinCollection = appendCommaIfNecessary(coinCollection);
        coinCollection.append("10:").append(coinCount010);
    }
    
    // e.g. <CoinCount025>177</CoinCount025>
    public void setCoinCount025(String coinCount025) {
        audit.setCoinCount25(Integer.parseInt(coinCount025));
        coinCollection = appendCommaIfNecessary(coinCollection);
        coinCollection.append("25:").append(coinCount025);
        
    }
    
    // e.g. <CoinCount100>263</CoinCount100>
    public void setCoinCount100(String coinCount100) {
        audit.setCoinCount100(Integer.parseInt(coinCount100));
        coinCollection = appendCommaIfNecessary(coinCollection);
        coinCollection.append("100:").append(coinCount100);
    }
    
    // e.g. <CoinCount200>480</CoinCount200>
    public void setCoinCount200(String coinCount200) {
        audit.setCoinCount200(Integer.parseInt(coinCount200));
        coinCollection = appendCommaIfNecessary(coinCollection);
        coinCollection.append("200:").append(coinCount200);
    }
    
    public BigDecimal getCoinAmount() {
        CoinCol coinCol = new CoinCol(coinCollection.toString());
        return coinCol.getCoinDollarsDecimal();
    }
    
    public int getCoinAmountBase100() {
        CoinCol coinCol = new CoinCol(coinCollection.toString());
        return coinCol.getCoinDollars();
    }
    
    // --------------------------------------------------------------------------------
    // Bills
    // --------------------------------------------------------------------------------
    
    // e.g. <BillCount01>319</BillCount01>
    public void setBillCount01(String billCount01) {
        audit.setBillCount1(Integer.parseInt(billCount01));
        billCollection = appendCommaIfNecessary(billCollection);
        billCollection.append("1:").append(billCount01);
    }
    
    // e.g. <BillCount02>84</BillCount02>
    public void setBillCount02(String billCount02) {
        audit.setBillCount2(Integer.parseInt(billCount02));
        billCollection = appendCommaIfNecessary(billCollection);
        billCollection.append("2:").append(billCount02);
    }
    
    // e.g. <BillCount05>49</BillCount05>
    public void setBillCount05(String billCount05) {
        audit.setBillCount5(Integer.parseInt(billCount05));
        billCollection = appendCommaIfNecessary(billCollection);
        billCollection.append("5:").append(billCount05);
    }
    
    // e.g. <BillCount10>103</BillCount10>
    public void setBillCount10(String billCount10) {
        audit.setBillCount10(Integer.parseInt(billCount10));
        billCollection = appendCommaIfNecessary(billCollection);
        billCollection.append("10:").append(billCount10);
    }
    
    // e.g. <BillCount20>253</BillCount20>
    public void setBillCount20(String billCount20) {
        audit.setBillCount20(Integer.parseInt(billCount20));
        billCollection = appendCommaIfNecessary(billCollection);
        billCollection.append("20:").append(billCount20);
    }
    
    // e.g. <BillCount50>52</BillCount50>
    public void setBillCount50(String billCount50) {
        audit.setBillCount50(Integer.parseInt(billCount50));
        billCollection = appendCommaIfNecessary(billCollection);
        billCollection.append("50:").append(billCount50);
    }
    
    // --------------------------------------------------------------------------------
    // Credit cards
    // --------------------------------------------------------------------------------
    
    // e.g. <CardAmountOther>72.88</CardAmountOther>
    public void setCardAmountOther(String cardAmountOther) {
        audit.setValueCardAmount(WebCoreUtil.convertToBase100IntValue(cardAmountOther));
    }
    
    // e.g. <CardAmountSC>63.83</CardAmountSC>
    public void setCardAmountSC(String cardAmountSC) {
        audit.setSmartCardAmount(WebCoreUtil.convertToBase100IntValue(cardAmountSC));
    }
    
    // e.g. <CardAmountVisa>15.72</CardAmountVisa>
    public void setCardAmountVisa(String cardAmountVisa) {
        audit.setVisaAmount(WebCoreUtil.convertToBase100IntValue(cardAmountVisa));
    }
    
    // e.g. <CardAmountMC>84.27</CardAmountMC>
    public void setCardAmountMC(String cardAmountMC) {
        audit.setMasterCardAmount(WebCoreUtil.convertToBase100IntValue(cardAmountMC));
    }
    
    // e.g. <CardAmountAmex>78.23</CardAmountAmex>
    public void setCardAmountAmex(String cardAmountAmex) {
        audit.setAmexAmount(WebCoreUtil.convertToBase100IntValue(cardAmountAmex));
    }
    
    // e.g. <CardAmountDiners>69.93</CardAmountDiners>
    public void setCardAmountDiners(String cardAmountDiners) {
        audit.setDinersAmount(WebCoreUtil.convertToBase100IntValue(cardAmountDiners));
    }
    
    // e.g. <CardAmountDiscover>27.7</CardAmountDiscover>
    public void setCardAmountDiscover(String cardAmountDiscover) {
        audit.setDiscoverAmount(WebCoreUtil.convertToBase100IntValue(cardAmountDiscover));
    }
    
    // e.g. <CardAmountJCB>92.62</CardAmountJCB>
    public void setCardAmountJCB(String cardAmountJCB) {
        audit.setJCBAmount(WebCoreUtil.convertToBase100IntValue(cardAmountJCB));
    }
    
    public int getCardAmountBase100() {
        return audit.getDiscoverAmount() 
                + audit.getDinersAmount() 
                + audit.getAmexAmount() 
                + audit.getMasterCardAmount() 
                + audit.getVisaAmount()
                + audit.getJCBAmount()
                + audit.getSmartCardAmount() 
                + audit.getValueCardAmount();
    }
    
    public void setWebApplicationContext(WebApplicationContext ctx) {
        super.setWebApplicationContext(ctx);
        this.mailerService = (MailerService) ctx.getBean("mailerService");
        this.posCollectionService = (PosCollectionService) ctx.getBean("posCollectionService");
        this.collectionTypeService = (CollectionTypeService) ctx.getBean("collectionTypeService");
    }
}
