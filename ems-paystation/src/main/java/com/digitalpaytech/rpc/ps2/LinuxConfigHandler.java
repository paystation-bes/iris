package com.digitalpaytech.rpc.ps2;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;

import com.digitalpaytech.domain.LinuxConfigurationFile;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.rpc.support.LinuxConfigFileNotificationInfo;
import com.digitalpaytech.rpc.support.LinuxConfigNotificationInfo;
import com.digitalpaytech.rpc.support.MessageTags;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.xml.XMLBuilderBase;

public class LinuxConfigHandler extends BaseHandler {
    private static final Logger LOG = Logger.getLogger(LinuxConfigHandler.class);
    
    public LinuxConfigHandler(final String messageNumber) {
        super(messageNumber);
    }
    
    @Override
    public final String getRootElementName() {
        return MessageTags.LINUX_CONFIG;
    }
    
    @Override
    public final void process(final XMLBuilderBase xmlBuilder) {
        final long startTime = System.currentTimeMillis();
        process((PS2XMLBuilder) xmlBuilder);
        
        if (LOG.isInfoEnabled()) {
            final float processingTime = (System.currentTimeMillis() - startTime) / 1000f;
            final StringBuilder bdr = new StringBuilder();
            bdr.append("Returning LinuxConfigHandler to PointOfSale serialNumber: ").append(posSerialNumber).append(" after ")
                    .append(processingTime).append(" seconds.");
            LOG.info(bdr.toString());
        }
    }
    
    private void process(final PS2XMLBuilder xmlBuilder) {
        if (!validatePOS(xmlBuilder)) {
            return;
        }

        final int pointOfSaleId = this.pointOfSale.getId();
        if (StringUtils.isBlank(this.pointOfSale.getPosServiceState().getNewConfigurationId())) {
            
            final Optional<LinuxConfigurationFile> optConfigFile = this.linuxConfigurationFileService.findLatestByPointOfSaleId(pointOfSaleId);
            if (!optConfigFile.isPresent()) {
                xmlBuilder.createNewConfigurationNotification(pointOfSaleId, 
                                                              StandardConstants.STRING_EMPTY_STRING,
                                                              new LinuxConfigNotificationInfo(pointOfSaleId, 
                                                                                              StandardConstants.STRING_EMPTY_STRING, 
                                                                                              new ArrayList<LinuxConfigFileNotificationInfo>()));
            } else {
                final LinuxConfigurationFile file = optConfigFile.get();
                final List<LinuxConfigurationFile> fileList = new ArrayList<>();
                fileList.add(file);
                
                final PosServiceState pss = this.pointOfSale.getPosServiceState();
                pss.setNewConfigurationId(file.getSnapShotId());
                this.posServiceStateService.updatePosServiceState(pss);
                
                populateNotificationInfoAndCreateNewConfiguration(xmlBuilder, pointOfSaleId, file.getSnapShotId(), fileList);
                
            }
        } else {
            final String snapShotId = this.pointOfSale.getPosServiceState().getNewConfigurationId();
            final List<LinuxConfigurationFile> fileList = 
                    this.linuxConfigurationFileService.findByPointOfSaleIdAndSnapshotId(pointOfSaleId, snapShotId);
            
            if (fileList == null || fileList.isEmpty()) {
                final String subject = "LinuxConfigurationFile record doesn't exist";
                final StringBuilder bdr = new StringBuilder();
                bdr.append(subject).append(" for NewConfigurationId: ").append(snapShotId);
                xmlBuilder.insertErrorMessage(messageNumber, PaystationConstants.RESPONSE_MESSAGE_NO_CONFIGURATION_ERROR);
                super.mailerService.sendAdminWarnAlert(subject, bdr.toString(), null);
            } else {
                populateNotificationInfoAndCreateNewConfiguration(xmlBuilder, pointOfSaleId, snapShotId, fileList);
            }
        }
    }

    private void populateNotificationInfoAndCreateNewConfiguration(final PS2XMLBuilder xmlBuilder, 
                                                                   final int pointOfSaleId, 
                                                                   final String snapShotId, 
                                                                   final List<LinuxConfigurationFile> fileList) {
        
        final List<LinuxConfigFileNotificationInfo> configFileInfoList = new ArrayList<LinuxConfigFileNotificationInfo>();
        for (LinuxConfigurationFile file : fileList) {
            configFileInfoList.add(createLinuxConfigFileNotificationInfo(file));
        }
        final LinuxConfigNotificationInfo notification = new LinuxConfigNotificationInfo(pointOfSaleId, snapShotId, configFileInfoList);
        xmlBuilder.createNewConfigurationNotification(pointOfSaleId, snapShotId, notification);
    }
    
    private LinuxConfigFileNotificationInfo createLinuxConfigFileNotificationInfo(final LinuxConfigurationFile file) {
        return new LinuxConfigFileNotificationInfo(file.getFileId(), 
                                                   file.getFileName(), 
                                                   file.getHash(), 
                                                   file.getSize());
    }
    
    public final void setWebApplicationContext(final WebApplicationContext ctx) {
        super.setWebApplicationContext(ctx);
    }
}
