package com.digitalpaytech.rpc.ps2;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.context.WebApplicationContext;

import com.digitalpaytech.rpc.support.MessageTags;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.paystation.PosCollectionService;
import com.digitalpaytech.util.xml.XMLBuilderBase;
import com.digitalpaytech.domain.CollectionType;
import com.digitalpaytech.domain.PosCollection;
import com.digitalpaytech.rpc.support.CardTypeUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.exception.InvalidDataException;

public class AuditHandler extends BaseHandler {
    
    private static Logger log = Logger.getLogger(AuditHandler.class);
    private PosCollection posCollection;
    private String cardType;
    private String lotNumber;
    private PosCollectionService posCollectionService;
    private EntityService entityService;

    public AuditHandler(String messageNumber) {
        super(messageNumber);
        posCollection = new PosCollection();
    }

    
    public String getRootElementName() {
        return MessageTags.AUDIT_TAG_NAME;
    }

    
    public void process(XMLBuilderBase xmlBuilder) {
        long start_time = System.currentTimeMillis();
        process((PS2XMLBuilder) xmlBuilder);

        if (log.isInfoEnabled()) {
            float processing_time = (System.currentTimeMillis() - start_time) / MILLISECOND_TO_SECOND;
            StringBuilder bdr = new StringBuilder();
            if (pointOfSale == null) {
                bdr.append("PointOfSale serial number is incorrect, unable to retrieve record. Processing time: ");
            } else {
                bdr.append("Returning response to PointToSale serialNumber: ").append(pointOfSale.getSerialNumber()).append(" after ");
            }
            bdr.append(processing_time).append(" seconds.");
            log.info(bdr.toString());
        }
    }
    
    private void process(PS2XMLBuilder xmlBuilder) {
        if (!validatePOS(xmlBuilder)) {
            return;
        }
        
        String msg = "Unable to process Audit for PointOfSale SerialNumber: " + pointOfSale.getSerialNumber();
        try {
            // Default to audit (all) report.
            posCollection.setCollectionType(entityService.loadAll(CollectionType.class).get(0));
            posCollection.setPointOfSale(pointOfSale);     
            posCollection.setCustomer(pointOfSale.getCustomer());

            // Bill
            posCollection.setBillTotalAmount(posCollection.getBillAmount1() + 
                                             posCollection.getBillAmount2() + 
                                             posCollection.getBillAmount5() + 
                                             posCollection.getBillAmount10() + 
                                             posCollection.getBillAmount20() + 
                                             posCollection.getBillAmount50());
            
            // Card
            posCollection.setCardTotalAmount(posCollection.getAmexAmount() + 
                                             posCollection.getDiscoverAmount() + 
                                             posCollection.getMasterCardAmount() + 
                                             posCollection.getValueCardAmount() + 
                                             posCollection.getVisaAmount() +                                               
                                             posCollection.getDinersAmount() +
                                             posCollection.getJCBAmount());
            
            if (StringUtils.isNotBlank(lotNumber)) {
                posCollection.setPaystationSetting(paystationSettingService.findByCustomerIdAndLotName(pointOfSale.getCustomer(), lotNumber));
            }

            if (posCollectionService.isDuplicateRecord(posCollection)) {
                log.info("Found duplicate PosCollection: " + posCollection);
                xmlBuilder.insertAcknowledge(messageNumber);
                return;
            }            
            
            posCollectionService.processPosCollection(pointOfSale, posCollection);
            xmlBuilder.insertAcknowledge(messageNumber);

        } catch (Exception e) {
            processException(msg, e);
            xmlBuilder.insertErrorMessage(messageNumber, BaseHandler.EMS_UNAVAILABLE_STRING);
        }

    }    
    
    
    public void setWebApplicationContext(WebApplicationContext ctx) {
        super.setWebApplicationContext(ctx);
        this.mailerService = (MailerService) ctx.getBean("mailerService");
        this.posCollectionService = (PosCollectionService) ctx.getBean("posCollectionService");
        this.entityService = (EntityService) ctx.getBean("entityService");
    }    

    
    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public void setCardAmount(String cardAmount) {
        BigDecimal card_amount = new BigDecimal(WebCoreUtil.convertToBase100IntValue(cardAmount));

        if (cardType.equals(CardTypeUtil.AMEX_STRING)) {
            posCollection.setAmexAmount(card_amount.intValue());
        } else if (cardType.equals(CardTypeUtil.DISCOVER_STRING)) {
            posCollection.setDiscoverAmount(card_amount.intValue());
        } else if (cardType.equals(CardTypeUtil.MASTERCARD_STRING)) {
            posCollection.setMasterCardAmount(card_amount.intValue());
        } else if (cardType.equals(CardTypeUtil.OTHER_STRING)) {
            posCollection.setValueCardAmount(card_amount.intValue());
        } else if (cardType.equals(CardTypeUtil.VISA_STRING)) {
            posCollection.setVisaAmount(card_amount.intValue());
        } else if (cardType.equals(CardTypeUtil.SMART_CARD_STRING)) {
            posCollection.setSmartCardAmount(card_amount.intValue());
        } else if (cardType.equals(CardTypeUtil.DINERS_STRING)) {
            posCollection.setDinersAmount(card_amount.intValue());
        } else if (cardType.equals(CardTypeUtil.JCB_STRING)) {
            posCollection.setJCBAmount(card_amount.intValue());
            
        } else {
            log.warn("AuditHandler, Unable to set card amount for unknown card type: " + cardType);
        }
    }

    public void setStartDate(String value) throws InvalidDataException {
        posCollection.setStartGmt(DateUtil.convertFromColonDelimitedDateStringToCalendar(value).getTime());
    }

    public void setEndDate(String value) throws InvalidDataException {
        posCollection.setEndGmt(DateUtil.convertFromColonDelimitedDateStringToCalendar(value).getTime());
    }

    public void setReportNumber(String value) {
        posCollection.setReportNumber(Integer.parseInt(value));
    }

    public void setLotNumber(String value) {
        this.lotNumber = value;
    }

    public void setMachineNumber(String value) {
        log.warn("AuditHandler, <MachineNumber> value is: " + value);
    }

    public void setPatrollerTicketsSold(String value) {
        posCollection.setPatrollerTicketsSold(Integer.parseInt(value));
    }

    public void setTicketsSold(String value) {
        posCollection.setTicketsSold(Integer.parseInt(value));
    }

    public void setCoinAmount(String value) {
        posCollection.setCoinTotalAmount(WebCoreUtil.convertToBase100IntValue(value));
    }

    public void setOnesAmount(String value) {
        posCollection.setBillAmount1(WebCoreUtil.convertToBase100IntValue(value));
    }

    public void setTwoesAmount(String value) {
        posCollection.setBillAmount2(WebCoreUtil.convertToBase100IntValue(value));
    }

    public void setFivesAmount(String value) {
        posCollection.setBillAmount5(WebCoreUtil.convertToBase100IntValue(value));
    }

    public void setTensAmount(String value) {
        posCollection.setBillAmount10(WebCoreUtil.convertToBase100IntValue(value));
    }

    public void setTwentiesAmount(String value) {
        posCollection.setBillAmount20(WebCoreUtil.convertToBase100IntValue(value));
    }

    public void setFiftiesAmount(String value) {
        posCollection.setBillAmount50(WebCoreUtil.convertToBase100IntValue(value));
    }

    public void setSmartCardRechargeAmount(String value) {
        posCollection.setSmartCardRechargeAmount(WebCoreUtil.convertToBase100IntValue(value));
    }

    public void setCitationsPaid(String value) {
        log.warn("AuditHandler, <CitationsPaid> value is: " + value);
    }

    public void setCitationAmount(String value) {
        log.warn("AuditHandler, <CitationAmount> value is: " + value);
    }

    public void setChangeIssued(String value) {
        posCollection.setChangeIssuedAmount(WebCoreUtil.convertToBase100IntValue(value));
    }

    public void setRefundIssued(String value) {
        posCollection.setRefundIssuedAmount(WebCoreUtil.convertToBase100IntValue(value));
    }

    public void setExcessPayment(String value) {
        posCollection.setExcessPaymentAmount(WebCoreUtil.convertToBase100IntValue(value));
    }

    public void setNextTicketNumber(String value) {
        posCollection.setNextTicketNumber(Integer.parseInt(value));
    }

    public void setAttendantTicketsSold(String value) {
        posCollection.setAttendantTicketsSold(Integer.parseInt(value));
    }

    public void setAttendantTicketsAmount(String value) {
        posCollection.setAttendantTicketsAmount(WebCoreUtil.convertToBase100IntValue(value));
    }

    public void setAttendantDepositAmount(String value) {
        posCollection.setAttendantDepositAmount(WebCoreUtil.convertToBase100IntValue(value));
    }

    public void setAcceptedFloatAmount(String value) {
        posCollection.setAcceptedFloatAmount(Integer.parseInt(value));
    }

    public void setOverfillAmount(String value) {
        posCollection.setOverfillAmount(Integer.parseInt(value));
    }

    public void setReplenishedAmount(String value) {
        posCollection.setReplenishedAmount(Integer.parseInt(value));
    }

    public void setCurrentTubeStatus(String currentTubeStatus) {
        // xxx remove '-' when all PS2 5.3+
        String[] values = currentTubeStatus.split("[_:]");

        if (values.length >= 2) {
            posCollection.setTube1type(Short.parseShort(values[0]));
            posCollection.setTube1amount(Integer.parseInt(values[1]));
        }

        if (values.length >= 4) {
            posCollection.setTube2type(Short.parseShort(values[2]));
            posCollection.setTube2amount(Integer.parseInt(values[3]));
        }

        if (values.length >= 6) {
            posCollection.setTube3type(Short.parseShort(values[4]));
            posCollection.setTube3amount(Integer.parseInt(values[5]));
        }

        if (values.length >= 8) {
            posCollection.setTube4type(Short.parseShort(values[6]));
            posCollection.setTube4amount(Integer.parseInt(values[7]));
        }
    }

    public void setHopperInfo(String hopperDispensed) {
        String[] values = hopperDispensed.split("[_:]");

        if (values.length >= 2) {
            posCollection.setHopper1type(Short.parseShort(values[0]));
            posCollection.setHopper2type(Short.parseShort(values[1]));
        }

        if (values.length >= 4) {
            posCollection.setHopper1dispensed(Integer.parseInt(values[2]));
            posCollection.setHopper2dispensed(Integer.parseInt(values[3]));
        }

        if (values.length >= 6) {
            posCollection.setHopper1current(Integer.parseInt(values[4]));
            posCollection.setHopper2current(Integer.parseInt(values[5]));
        }

        if (values.length >= 8) {
            posCollection.setHopper1replenished(Integer.parseInt(values[6]));
            posCollection.setHopper2replenished(Integer.parseInt(values[7]));
        }
    }

    public void setTestDispensed(String data) {
        String[] values = data.split("[:]");

        posCollection.setTestDispensedHopper1(Integer.parseInt(values[0]));
        posCollection.setTestDispensedHopper2(Integer.parseInt(values[1]));
        posCollection.setTestDispensedChanger(Integer.parseInt(values[2]));
    }
}
