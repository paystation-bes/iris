package com.digitalpaytech.rpc.ps2;

import java.util.Date;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.rpc.support.MessageTags;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.xml.XMLBuilderBase;


@StoryAlias(value = EMVMerchantAccountSuccessHandler.ALIAS)
public class EMVMerchantAccountSuccessHandler extends BaseHandler {
    public static final String ALIAS = "emv merchant account success data";
    public static final String ALIAS_SERIAL_NUMBER = "serial number";
    private static final Logger LOG = Logger.getLogger(EMVMerchantAccountSuccessHandler.class);

    
    public EMVMerchantAccountSuccessHandler(final String messageNumber) {
        super(messageNumber);
    }
    
    @Override
    public final String getRootElementName() {
        return MessageTags.EMV_MERCHANT_ACCOUNT_SUCCESS_TAG_NAME;
    }
    
    @Override
    public final void process(final XMLBuilderBase xmlBuilder) {
        final long startTime = System.currentTimeMillis();
        process((PS2XMLBuilder) xmlBuilder);
        
        if (LOG.isInfoEnabled()) {
            final float processingTime = (System.currentTimeMillis() - startTime) / StandardConstants.FLOAT_THOUSAND;
            final StringBuilder bdr = new StringBuilder();
            if (pointOfSale == null) {
                bdr.append("PointOfSale serial number is incorrect, unable to retrieve record. Processing time: ");
            } else {
                bdr.append("Returning to response to PointToSale serialNumber: ").append(pointOfSale.getSerialNumber()).append(" after ");
            }
            bdr.append(processingTime).append(" seconds.");
            LOG.info(bdr.toString());
        }
    }
    
    private void process(final PS2XMLBuilder xmlBuilder) {
        if (!validatePOS(xmlBuilder)) {
            return;
        }
        final Date now = DateUtil.getCurrentGmtDate();
        final PosServiceState posServiceState = this.pointOfSale.getPosServiceState();
        posServiceState.setMerchantAccountUploadedGMT(now);
        posServiceState.setIsNewMerchantAccount(false);
        try {
            customerAdminService.saveOrUpdatePointOfSale(pointOfSale);
            xmlBuilder.insertAcknowledge(messageNumber);
        } catch (RuntimeException e) {
            final String msg = "Unable to process EMV MerchantAccount for PointOfSale SerialNumber: " + pointOfSale.getSerialNumber();
            processException(msg, e);
            xmlBuilder.insertErrorMessage(messageNumber, BaseHandler.EMS_UNAVAILABLE_STRING);
        }
    }
    
    @StoryAlias (ALIAS_SERIAL_NUMBER)
    public final String getPaystationCommAddress() {
        return super.getPaystationCommAddress();
    }
    
    public final void setPaystationCommAddress(final String paystationCommAddress) {
        super.setPaystationCommAddress(paystationCommAddress);
    }
    
}
