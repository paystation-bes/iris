package com.digitalpaytech.rpc.ps2;

import org.springframework.web.context.WebApplicationContext;

import com.digitalpaytech.rpc.ps2.PS2XMLBuilder;
import com.digitalpaytech.rpc.support.MessageTags;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.xml.XMLBuilderBase;
import com.digitalpaytech.domain.Replenish;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.service.ReplenishService;
import com.digitalpaytech.service.ReplenishTypeService;

import org.apache.commons.httpclient.util.DateParseException;
import org.apache.log4j.Logger;

public class ReplenishHandler extends BaseHandler {
    
    private static Logger log = Logger.getLogger(ReplenishHandler.class);
    private ReplenishService replenishService;
    private ReplenishTypeService replenishTypeService;
    private Replenish replenish;

    public ReplenishHandler(String messageNumber) {
        super(messageNumber);
        replenish = new Replenish();
    }
    
    public String getRootElementName() {
        return MessageTags.REPLENISH_TAG_NAME;
    }
    
    public void process(XMLBuilderBase xmlBuilder) {
        long start_time = System.currentTimeMillis();
        process((PS2XMLBuilder) xmlBuilder);
        
        if (log.isInfoEnabled()) {
            float processing_time = (System.currentTimeMillis() - start_time) / MILLISECOND_TO_SECOND;
            StringBuilder bdr = new StringBuilder();
            if (pointOfSale == null) {
                bdr.append("PointOfSale serial number is incorrect, unable to retrieve record. Processing time: ");
            } else {
                bdr.append("Returning to TransactionHandler for PointToSale serialNumber: ").append(pointOfSale.getSerialNumber()).append(" after ");
            }
            bdr.append(processing_time).append(" seconds.");
            log.info(bdr.toString());
        }
    }
    
    private void process(PS2XMLBuilder xmlBuilder) {
        // Validates PointOfSale
        if (!validatePOS(xmlBuilder)) {
            return;
        }
        
        try {
            replenish.setPointOfSale(pointOfSale);
            replenishService.processReplenish(replenish);
            xmlBuilder.insertAcknowledge(messageNumber);        
        } catch (org.springframework.dao.DataIntegrityViolationException ex) {
        	if (ex.getCause() instanceof org.hibernate.exception.ConstraintViolationException) {
        		xmlBuilder.insertAcknowledge(messageNumber);
        	} else {
                String msg = "Unable to process ReplenishRequest for PS: " + pointOfSale.getSerialNumber();
                processException(msg, ex);
                xmlBuilder.insertErrorMessage(messageNumber, "Failed to persist " + getRootElementName());        		
        	}
        } catch (Exception e) {
            String msg = "Unable to process ReplenishRequest for PS: " + pointOfSale.getSerialNumber();
            processException(msg, e);
            xmlBuilder.insertErrorMessage(messageNumber, "Failed to persist " + getRootElementName());
        }
    }
    
    public void setDate(String value) throws InvalidDataException {
        replenish.setReplenishGmt(DateUtil.convertFromColonDelimitedDateString(value));
    }
    
    public void setNumber(String value) {
        replenish.setNumber(Integer.parseInt(value));
    }
    
    public void setType(String value) {
        replenish.setReplenishType(replenishTypeService.getReplenishType(value));
    }
    
    public void setTube1(String value) {
        String[] values = value.split(":");
        
        if (values.length >= 3) {
            replenish.setTube1type(Integer.parseInt(values[0]));
            replenish.setTube1changedCount(Integer.parseInt(values[1]));
            replenish.setTube1currentCount(Integer.parseInt(values[2]));
        }
    }
    
    public void setTube2(String value) {
        String[] values = value.split(":");
        
        if (values.length >= 3) {
            replenish.setTube2type(Integer.parseInt(values[0]));
            replenish.setTube2changedCount(Integer.parseInt(values[1]));
            replenish.setTube2currentCount(Integer.parseInt(values[2]));
        }
    }
    
    public void setTube3(String value) {
        String[] values = value.split(":");
        
        if (values.length >= 3) {
            replenish.setTube3type(Integer.parseInt(values[0]));
            replenish.setTube3changedCount(Integer.parseInt(values[1]));
            replenish.setTube3currentCount(Integer.parseInt(values[2]));
        }
    }
    
    public void setTube4(String value) {
        String[] values = value.split(":");
        
        if (values.length >= 3) {
            replenish.setTube4type(Integer.parseInt(values[0]));
            replenish.setTube4changedCount(Integer.parseInt(values[1]));
            replenish.setTube4currentCount(Integer.parseInt(values[2]));
        }
    }
    
    public void setHopper1(String value) {
        setTube1(value);
    }
    
    public void setHopper2(String value) {
        setTube2(value);
    }
    
    public void setCoinbag(String value) {
        String[] values = value.split(":");
        
        if (values.length >= 5) {
            replenish.setCoinBag005addedCount(Integer.parseInt(values[0]));
            replenish.setCoinBag010addedCount(Integer.parseInt(values[1]));
            replenish.setCoinBag025addedCount(Integer.parseInt(values[2]));
            replenish.setCoinBag100addedCount(Integer.parseInt(values[3]));
            replenish.setCoinBag200addedCount(Integer.parseInt(values[4]));
        }
    }
    
    public void setCommAddress(String value) {
        setPaystationCommAddress(value);
    }
    
    
    public void setWebApplicationContext(WebApplicationContext ctx) {
        super.setWebApplicationContext(ctx);
        replenishService = (ReplenishService) ctx.getBean("replenishService");
        replenishTypeService = (ReplenishTypeService) ctx.getBean("replenishTypeService");
    }    
}
