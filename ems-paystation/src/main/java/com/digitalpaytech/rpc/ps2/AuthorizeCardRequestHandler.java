package com.digitalpaytech.rpc.ps2;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;

import com.digitalpaytech.cardprocessing.CardProcessingManager;
import com.digitalpaytech.cardprocessing.cps.impl.ProcessingDestinationDTO;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.exception.CcProcessingRestrictionException;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.rpc.support.MessageTags;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.CryptoUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.xml.XMLBuilderBase;
import com.netflix.client.ClientException;

public class AuthorizeCardRequestHandler extends BaseHandler {
    
    private static final Logger LOG = Logger.getLogger(AuthorizeCardRequestHandler.class);
    private final PreAuth preAuth = new PreAuth();
    private CryptoService cryptoService;
    private CardProcessingManager cardProcessingManager;
    private CustomerCardTypeService customerCardTypeService;
    
    public AuthorizeCardRequestHandler(final String messageNumber) {
        super(messageNumber);
    }
    
    public String getRootElementName() {
        return MessageTags.AUTHORIZE_CARD_REQUEST_TAG_NAME;
    }
    
    public void process(final XMLBuilderBase xmlBuilder) {
        final long startTime = System.currentTimeMillis();
        final String authString = process((PS2XMLBuilder) xmlBuilder);
        
        if (LOG.isInfoEnabled()) {
            final float processingTime = (System.currentTimeMillis() - startTime) / MILLISECOND_TO_SECOND;
            final StringBuilder bdr = new StringBuilder();
            if (pointOfSale == null) {
                bdr.append("PointOfSale serial number is incorrect, unable to retrieve record. Processing time: ");
            } else {
                bdr.append("Returning '").append(authString).append("' AuthorizeCardResponse to PointToSale serialNumber: ")
                        .append(pointOfSale.getSerialNumber()).append(" after ");
            }
            bdr.append(processingTime).append(" seconds.");
            LOG.info(bdr.toString());
        }
    }
    
    private String process(final PS2XMLBuilder xmlBuilder) {
        // Validate PointOfSale
        if (!validatePOS(xmlBuilder)) {
            return null;
        }
        
        try {
            
            final int authStatusId = this.processCardAuthorization(this.preAuth);
            
            final String authStatusName = getStatusName(authStatusId);
            
            // Send authorization results('authorized' or 'denied') to PS
            if (this.preAuth.getAuthorizationNumber() != null) {
                xmlBuilder.createAuthorizeCardResponse(this, this.preAuth.getAuthorizationNumber(), authStatusName,
                                                       Long.toString(this.preAuth.getId()));
            } else {
                // Auth # is null -> send empty string in place of authorization number
                Long id = 0L;
                if (this.preAuth.getId() != null) {
                    id = this.preAuth.getId();
                }
                xmlBuilder.createAuthorizeCardResponse(this, EMPTY_STRING, authStatusName, id.toString());
            }
            
            return authStatusName;
            
        } catch (Exception e) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("Unable to process AuthorizeCardRequest for PointOfSale id: ").append(pointOfSale.getId()).append(", Customer id: ")
                    .append(pointOfSale.getCustomer().getId());
            bdr.append(", serialNumber: ").append(pointOfSale.getSerialNumber());
            
            processException(bdr.toString(), e);
            final String resultStr;
            if (e instanceof CryptoException) {
                final CryptoException ce = (CryptoException) e;
                /*
                 * ServerError means server returned 500 or not reachable
                 * ClientException means bad data
                 * if cause is null is unknown and should return EMS_UNAVAILABLE_STRING
                 */
                if (ce.getCause() instanceof ClientException) {
                    xmlBuilder.createAuthorizeCardResponse(this, EMPTY_STRING, BaseHandler.CRYPTO_EXCEPTION_STRING, EMPTY_STRING);
                    resultStr = CRYPTO_EXCEPTION_STRING;
                } else {
                    xmlBuilder.createAuthorizeCardResponse(this, EMPTY_STRING, BaseHandler.EMS_UNAVAILABLE_STRING, EMPTY_STRING);
                    resultStr = EMS_UNAVAILABLE_STRING;
                }
                
            } else if (e instanceof CcProcessingRestrictionException) {
                final CcProcessingRestrictionException ccre = (CcProcessingRestrictionException) e;
                if (ccre.isUnsubscribedService()) {
                    xmlBuilder.createAuthorizeCardResponse(this, EMPTY_STRING, BaseHandler.UNSUBSCRIBED_SERVICE_STRING, EMPTY_STRING);
                    resultStr = UNSUBSCRIBED_SERVICE_STRING;
                } else {
                    xmlBuilder.createAuthorizeCardResponse(this, EMPTY_STRING, BaseHandler.EMS_UNAVAILABLE_STRING, EMPTY_STRING);
                    resultStr = EMS_UNAVAILABLE_STRING;
                }
            } else {
                xmlBuilder.createAuthorizeCardResponse(this, EMPTY_STRING, BaseHandler.EMS_UNAVAILABLE_STRING, EMPTY_STRING);
                resultStr = EMS_UNAVAILABLE_STRING;
            }
            return resultStr;
        }
    }
    
    private int processCardAuthorization(final PreAuth preauth) throws CryptoException, JsonException {
        if (CryptoUtil.isDecryptedCardData(preauth.getCardData())) {
            return processIrisAuth(preauth);
        } else {
            final ProcessingDestinationDTO processDto = this.processingDestinationService
                    .pickProcessingDest(preauth.getCardData(), this.pointOfSale.getCustomer().getId(), this.pointOfSale.getId());
            
            if (processDto.isProcessInLink()) {
                return this.cardProcessingManager.processLinkCPSTransactionAuthorization(processDto.getMerchantAccount(), pointOfSale, this.preAuth,
                                                                                         processDto.getCardTypeName());
            } else {
                return processIrisAuth(preauth);
            }
            
        }
    }
    
    private int processIrisAuth(final PreAuth preauth) throws CryptoException {
        boolean isCreditCard = false;
        // Decrypt card data if length > 40(max track2 length is 37)
        if (preauth.getCardData().length() > CardProcessingConstants.NOT_ENCRYPTED_CARD_DATA_MAX_LENGTH) {
            
            String decryptedData = null;
            try {
                decryptedData = this.cryptoService.decryptData(preauth.getCardData(), this.pointOfSale.getId(), preauth.getProcessingDate());
                preauth.setCardData(decryptedData);
                preauth.setPreAuthDate(DateUtil.getCurrentGmtDate());
                /*
                 * In EMS 6 Track2Card.java, determineCardNameAndType method, it checks by track 2 first. If logic returns 'unknown', it
                 * then checks by account number (credit card number).
                 */
                isCreditCard = this.customerCardTypeService.isCreditCardTrack2(decryptedData);
                
                final int idx = decryptedData.indexOf(WebCoreConstants.EQUAL_SIGN);
                if (!isCreditCard && idx != -1) {
                    final String acctNum = decryptedData.substring(0, idx);
                    isCreditCard = this.customerCardTypeService.isCreditCardAccountNumber(acctNum);
                }
            } catch (CryptoException cex) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append("Unable to decryptData, pointOfSale id: ").append(pointOfSale.getId()).append(", processing date: ")
                        .append(preauth.getProcessingDate());
                LOG.error(bdr.toString());
                throw cex;
            }
        } else {
            LOG.warn("+++ PreAuth contains unencrypted carddata +++ ");
        }
        // Check subscription
        if (isCreditCard) {
            this.checkCcProcessingRestriction();
        } else {
            final CustomerCardType customerCardType =
                    this.customerCardTypeService.getCardTypeByTrack2Data(pointOfSale.getCustomer().getId(), preauth.getCardData());
            if (customerCardType != null) {
                if (customerCardType.getCardType().getId() == WebCoreConstants.CARD_TYPE_NA) {
                    return CardProcessingConstants.STATUS_INVALID_CARD;
                }
                if (customerCardType.getAuthorizationType().getId() == WebCoreConstants.AUTH_TYPE_EXTERNAL_SERVER) {
                    this.checkExternalPassCardProcessingRestriction();
                } else if (customerCardType.getAuthorizationType().getId() == WebCoreConstants.AUTH_TYPE_INTERNAL_LIST) {
                    this.checkInternalPassCardProcessingRestriction();
                }
            }
        }
        return this.cardProcessingManager.processTransactionAuthorization(preauth, pointOfSale, isCreditCard);
    }
    
    public void setCardData(final String cardData) {
        this.preAuth.setCardData(cardData);
    }
    
    public void setChargeAmount(final String chargeAmount) {
        this.preAuth.setAmount(CardProcessingUtil.getDollarsAmountInCents(chargeAmount).intValue());
    }
    
    public void setRefId(final String refId) {
        this.preAuth.setPsRefId(refId);
    }
    
    public void setPaidByRFID(final String paidByRFID) {
        if (paidByRFID != null && paidByRFID.trim().equals(CardProcessingConstants.RFID_DATA)) {
            this.preAuth.setIsRfid(true);
        }
    }
    
    private String getStatusName(final int statusId) {
        switch (statusId) {
            case CardProcessingConstants.CARD_STATUS_AUTHORIZED:
                return CardProcessingConstants.AUTHORIZED_STRING;
            case CardProcessingConstants.CARD_STATUS_DENIED:
                return CardProcessingConstants.DENIED_STRING;
            case CardProcessingConstants.CARD_STATUS_SUCCESS:
                return CardProcessingConstants.SUCCESS_STRING;
            case CardProcessingConstants.CARD_STATUS_BAD_CARD:
                return CardProcessingConstants.BAD_CARD_STRING;
            case CardProcessingConstants.CARD_STATUS_INVALID_CARD:
                return CardProcessingConstants.INVALID_CARD_STRING;
            case CardProcessingConstants.CARD_STATUS_UNSUBSCRIBED_SERVICE:
                return CardProcessingConstants.UNSUBSCRIBED_SERVICE_STRING;
            default:
                return EMS_UNAVAILABLE_STRING;
        }
    }
    
    public void setWebApplicationContext(final WebApplicationContext ctx) {
        super.setWebApplicationContext(ctx);
        this.cryptoService = (CryptoService) ctx.getBean("cryptoService");
        this.cardProcessingManager = (CardProcessingManager) ctx.getBean("cardProcessingManager");
        this.customerCardTypeService = (CustomerCardTypeService) ctx.getBean("customerCardTypeService");
        this.merchantAccountService = (MerchantAccountService) ctx.getBean("merchantAccountService");
    }
}
