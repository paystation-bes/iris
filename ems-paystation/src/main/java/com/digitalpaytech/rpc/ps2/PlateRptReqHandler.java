package com.digitalpaytech.rpc.ps2;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;

import com.digitalpaytech.domain.ActivePermit;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.rpc.support.MessageTags;
import com.digitalpaytech.service.ActivePermitService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.xml.XMLBuilderBase;

public class PlateRptReqHandler extends BaseHandler {

    private static Logger log = Logger.getLogger(PlateRptReqHandler.class);
    private static final String MISSING_REPORT_DATE = "Missing ReportDate";
    private static final String START_POSITION_TOO_BIG = "StartPosition should be between 0 and 99999999";
    private static final String RECORD_RETURN_TOO_BIG = "Max returned records should be between 0 and  5000";
    private ActivePermitService activePermitService;
    private EmsPropertiesService emsPropertiesService;
    
    private Date reportDate;
    // Set the first row to retrieve. 
    private int startPosition;
    private int recordsReturned;
    
    
    public PlateRptReqHandler(final String messageNumber) {
        super(messageNumber);
    }

    public final String getRootElementName() {
        return MessageTags.PLATE_REPORT_REQUEST_TAG_NAME;
    }

    public final void process(final XMLBuilderBase xmlBuilder) {
        final long startTime = System.currentTimeMillis();
        process((PS2XMLBuilder) xmlBuilder);

        if (log.isInfoEnabled()) {
            final float processingtime = (System.currentTimeMillis() - startTime) / MILLISECOND_TO_SECOND;
            final StringBuilder bdr = new StringBuilder();
            if (pointOfSale == null) {
                bdr.append("PointOfSale serial number is incorrect, unable to retrieve record. Processing time: ");
            } else {
                bdr.append("Returning PlateReportResponse to PointToSale serialNumber: ").append(pointOfSale.getSerialNumber()).append(" after ");
            }
            bdr.append(processingtime).append(" seconds.");
            log.info(bdr.toString());
        }
    }

    private void process(final PS2XMLBuilder xmlBuilder) {
        // Validate PointOfSale
        if (!validatePOS(xmlBuilder)) {
            return;
        }

        try {
            String response = BaseHandler.SUCCESS_STRING;

            if (this.reportDate == null) {
                response = MISSING_REPORT_DATE;
                log.info("Invalid ReportDate: 'null'");
            }

            final int min = 0;
            final int maxStartPos = 99999999;
            final int maxReturned = 5000;
            if (this.startPosition > maxStartPos || this.startPosition < min) {
                response = START_POSITION_TOO_BIG;
                log.info("Invalid StartPosition is not between 0 and 99999999, startPosition: " + this.startPosition);
            }

            if (this.recordsReturned > maxReturned || this.recordsReturned < 0) {
                response = RECORD_RETURN_TOO_BIG;
                log.info("RecReturned is not between 0 and 5000, recordsReturned: " + this.recordsReturned);
            }
            
            
            if (!response.equals(BaseHandler.SUCCESS_STRING)) {
                xmlBuilder.createPlateReportErrorResponse(this, response);
            } else {
                // If return size is 0, use default value defined in EmsProperties table ('PlateReportReturnSize' = 1000).
                if (this.recordsReturned == 0) {
                    this.recordsReturned = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.PLATE_REPORT_RETURN_SIZE, 
                                                                                 EmsPropertiesService.DEFAULT_PLATE_REPORT_RETURN_SIZE);
                }
                
                List<ActivePermit> activePermitList = new ArrayList<ActivePermit>();
                int totalPlates = -1;
                /*
                 * 1st, depending on 'Permits Enforced By' (4 options), it gets list of ActivePermit objects by locationId or customerId
                 * 2nd, create a sql query in ActivePermitService to return 'LicencePlateNumber' & 'PermitExpireGMT' in ActivePermit objects. 
                 */
                final int querySpaceBy = getQuerySpaceBy(PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_LATEST_EXPIRY_TIME);
                if (querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_LATEST_EXPIRY_TIME
                        || querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_EXPIRY_TIME_OF_MOST_RECENT_PERMIT) {

                    activePermitList = this.activePermitService.getValidByLocationId(this.pointOfSale.getLocation().getId(), 
                                                                                     this.reportDate, 
                                                                                     this.startPosition, 
                                                                                     this.recordsReturned);
                    totalPlates = this.activePermitService.getValidByLocationIdTotalCount(pointOfSale.getLocation().getId(), 
                                                                                          this.reportDate);
                    
                } else if (querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME 
                        || querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT) {
                    
                    activePermitList = this.activePermitService.getValidByCustomerId(this.pointOfSale.getCustomer().getId(), 
                                                                                     this.reportDate, 
                                                                                     this.startPosition, 
                                                                                     this.recordsReturned, 
                                                                                     querySpaceBy);
                    totalPlates = this.activePermitService.getValidByCustomerIdTotalCount(this.pointOfSale.getCustomer().getId(), 
                                                                                          this.reportDate, 
                                                                                          querySpaceBy);
                }

                final String location = this.pointOfSale.getLocation().getName();
                xmlBuilder.createPlateReportResponse(this, activePermitList, this.startPosition, totalPlates, location);
            }
        } catch (Exception e) {
            final String msg = "Unable to process PlateReportRequest for PointOfSale serialNumber: " + pointOfSale.getSerialNumber();
            processException(msg, e);
            xmlBuilder.insertErrorMessage(messageNumber, BaseHandler.EMS_UNAVAILABLE_STRING);
        }
    }

    public final void setWebApplicationContext(final WebApplicationContext ctx) {
        super.setWebApplicationContext(ctx);
        this.activePermitService = (ActivePermitService) ctx.getBean("activePermitService");
        this.emsPropertiesService = (EmsPropertiesService) ctx.getBean("emsPropertiesService");
    }  
    
    public final void setRptDate(final String aReportDate) throws InvalidDataException {
        this.reportDate = DateUtil.convertTimeZone(WebCoreConstants.SERVER_TIMEZONE_GMT, 
                                                   TimeZone.getDefault().getID(), 
                                                   DateUtil.convertFromColonDelimitedDateString(aReportDate)); 
    }

    public final void setStartPos(final String aStartPosition) {
        this.startPosition = Integer.parseInt(aStartPosition);
    }

    public final void setRecReturned(final String recReturned) {
        this.recordsReturned = Integer.parseInt(recReturned);
    }

    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
}
