package com.digitalpaytech.rpc.ps2;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;

import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.dto.paystation.SpaceInfo;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.rpc.support.MessageTags;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.xml.XMLBuilderBase;

public class StallReportRequestHandler extends StallInfoRequestHandler {
    private final static Logger logger__ = Logger.getLogger(StallReportRequestHandler.class);
    
    final static String STALL_REPORT_TYPE_EXPIRED_STRING = "Expired";
    final static String STALL_REPORT_TYPE_VALID_STRING = "Valid";
    private final static String INVALID_STALL_RANGE_STRING = "InvalidStallRange";
    
    private String reportType = null;
    private int firstStallNumber = -1;
    private int lastStallNumber = -1;
    private Date timeStamp = null;
    
    public StallReportRequestHandler(String messageNumber) {
        super(messageNumber);
    }
    
    public String getRootElementName() {
        return (MessageTags.STALL_REPORT_REQUEST_TAG_NAME);
    }
    
    public void process(XMLBuilderBase xmlBuilder) {
        long start_time = System.currentTimeMillis();
        
        logger__.info("Processing StallReportRequest for PS: " + posSerialNumber);
        
        process((PS2XMLBuilder) xmlBuilder);
        
        float processing_time = (System.currentTimeMillis() - start_time) / MILLISECOND_TO_SECOND;
        logger__.info("Returning StallReportResponse to PS: " + posSerialNumber + " after " + processing_time + " seconds.");
    }
    
    private void process(PS2XMLBuilder xmlBuilder) {
        // Validate paystation
        if (!validatePOS(xmlBuilder)) {
            return;
        }
        
        List<SpaceInfo> stall_reports = null;
        try {
            String response = BaseHandler.SUCCESS_STRING;
            
            if (lotNumber == null) {
                lotNumber = "null";
                response = INVALID_STALL_RANGE_STRING;
                
                logger__.info("Invalid lot number specified: 'null'");
            } else if (reportType == null) {
                reportType = "null";
                response = INVALID_TYPE_STRING;
                
                logger__.info("Invalid report type specified: 'null'");
            } else if ((firstStallNumber == -1) || (lastStallNumber == -1)) {
                response = INVALID_STALL_RANGE_STRING;
                logger__.info("Invalid stall range specified: 'null'");
            } else if ((firstStallNumber < 0) || (lastStallNumber < firstStallNumber)) {
                response = INVALID_STALL_RANGE_STRING;
                logger__.info("Invalid stall range specified: '" + firstStallNumber + "' to '" + lastStallNumber + "'");
            } else if (reportType.equals(STALL_REPORT_TYPE_VALID_STRING)) {
                stall_reports = getStallReport(pointOfSale, lotNumber, firstStallNumber, lastStallNumber, timeStamp, true);
                xmlBuilder.createStallReportResponse(this, reportType, lotNumber, stall_reports, response);
                return;
            } else if (reportType.equals(STALL_REPORT_TYPE_EXPIRED_STRING)) {
                stall_reports = getStallReport(pointOfSale, lotNumber, firstStallNumber, lastStallNumber, timeStamp, false);
                xmlBuilder.createStallReportResponse(this, reportType, lotNumber, stall_reports, response);
                return;
            } else {
                response = INVALID_TYPE_STRING;
                logger__.info("Invalid report type specified: '" + reportType + "'");
            }
            
            xmlBuilder.createStallReportErrorResponse(this, reportType, lotNumber, response);
        } catch (Exception e) {
            String msg = "Unable to process StallReportRequest for PS: " + posSerialNumber;
            processException(msg, e);
            
            xmlBuilder.createStallReportErrorResponse(this, reportType, lotNumber, BaseHandler.EMS_UNAVAILABLE_STRING);
        }
    }
    
    // Copied from RpcPaystationAppServiceImpl
    
    public List<SpaceInfo> getStallReport(PointOfSale pos, String paystationSetting, int firstStall, int lastStall, Date currentPsDate, boolean isValid)
            throws ApplicationException {
        if (isValid) {
            return (getStallReportValid(pos, paystationSetting, firstStall, lastStall, currentPsDate));
        }
        
        return (getStallReportExpired(pos, paystationSetting, firstStall, lastStall, currentPsDate));
    }
    
    private List<SpaceInfo> getStallReportValid(PointOfSale pos, String paystationSetting, int firstStall, int lastStall, Date currentPsDate)
            throws ApplicationException {
        
        int querySpaceBy = super.getQuerySpaceBy(-1);
        if (querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_LATEST_EXPIRY_TIME
                || querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_EXPIRY_TIME_OF_MOST_RECENT_PERMIT) {
            
            return this.spaceInfoService.getSpaceInfoListByLocationAndSpaceRangeForPosDate(pos.getCustomer().getId(), pos.getLocation().getId(),
                                                                                           firstStall, lastStall, PaystationConstants.STALL_TYPE_VALID,
                                                                                           currentPsDate, true);
        
        } else if (querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME 
            || querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT) {

            return this.spaceInfoService.getSpaceInfoListByCustomerAndSpaceRangeForPosDate(pos.getCustomer().getId(), firstStall, lastStall,
                                                                                           PaystationConstants.STALL_TYPE_VALID, currentPsDate, true);

        } else {
            StringBuilder bdr = new StringBuilder();
            bdr.append("getStallReportValid, invalid stall query type(-1) for PS: ").append(pos.getSerialNumber());
            throw new ApplicationException(bdr.toString());
            
        }
    }
    
    // Copied from RpcPaystation2AppServiceImpl
    
    protected final List<SpaceInfo> getStallReportExpired(PointOfSale pos, String lotNumber, int startStall, int endStall, Date currentPsDate)
            throws ApplicationException {
        
        int querySpaceBy = super.getQuerySpaceBy(-1);
        if (querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_LATEST_EXPIRY_TIME
                || querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_EXPIRY_TIME_OF_MOST_RECENT_PERMIT) {
            return this.spaceInfoService.getSpaceInfoListByLocationAndSpaceRangeForPosDate(pos.getCustomer().getId(), pos.getLocation().getId(),
                                                                                           startStall, endStall, PaystationConstants.STALL_TYPE_EXPIRED,
                                                                                           currentPsDate, true);
            
        } else if (querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME 
                || querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT) {
            return this.spaceInfoService.getSpaceInfoListByCustomerAndSpaceRangeForPosDate(pos.getCustomer().getId(), startStall, endStall,
                                                                                           PaystationConstants.STALL_TYPE_EXPIRED, currentPsDate, true);
            
        } else {
            StringBuilder bdr = new StringBuilder();
            bdr.append("getStallReportExpired, invalid stall query type(-1) for PS: ").append(pos.getSerialNumber());
            throw new ApplicationException(bdr.toString());
        }
    }
    
    public void setFirstStallNumber(String stallNumber) {
        this.firstStallNumber = Integer.parseInt(stallNumber);
    }
    
    public void setLastStallNumber(String stallNumber) {
        this.lastStallNumber = Integer.parseInt(stallNumber);
    }
    
    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }
    
    public void setType(String reportType) {
        this.reportType = reportType;
    }
    
    public void setTimeStamp(String value) throws InvalidDataException {
        this.timeStamp = DateUtil.convertFromColonDelimitedDateString(value);
    }
    
    public void setWebApplicationContext(WebApplicationContext ctx) {
        super.setWebApplicationContext(ctx);
    }
    
}