package com.digitalpaytech.rpc.ps2;

import java.util.Date;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.annotation.StoryTransformer;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.SettingsFile;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.rpc.support.DeviceUpgradeAcknowledge;
import com.digitalpaytech.rpc.support.MessageTags;
import com.digitalpaytech.service.paystation.PosServiceStateService;
import com.digitalpaytech.util.StableDateUtil;
import com.digitalpaytech.util.kafka.AbstractMessageProducer;
import com.digitalpaytech.util.kafka.KafkaKeyConstants;
import com.digitalpaytech.util.kafka.impl.IrisMessageProducer;
import com.digitalpaytech.util.xml.XMLBuilderBase;

@StoryAlias(LotSettingSuccessHandler.ALIAS)
public class LotSettingSuccessHandler extends BaseHandler {
    public static final String ALIAS = "Setting Update Acknowledge data";
    public static final String ALIAS_SERIAL_NUMBER = "serial number";
    public static final String ALIAS_GROUP_NAME = "group name";
    public static final String ALIAS_GROUP_UPDATED_DATE = "updated date";
    private static final Logger LOG = Logger.getLogger(LotSettingSuccessHandler.class);
    
    private PosServiceStateService posServiceStateService;
    
    private AbstractMessageProducer producer;
    
    private final DeviceUpgradeAcknowledge ack;
    
    public LotSettingSuccessHandler(final String messageNumber) {
        super(messageNumber);
        this.ack = new DeviceUpgradeAcknowledge();
    }
    
    public String getRootElementName() {
        return MessageTags.LOT_SETTING_SUCCESS_TAG_NAME;
    }
    
    public void process(final XMLBuilderBase xmlBuilder) {
        final long startTime = System.currentTimeMillis();
        process((PS2XMLBuilder) xmlBuilder);
        
        if (LOG.isInfoEnabled()) {
            final float processingTime = (System.currentTimeMillis() - startTime) / MILLISECOND_TO_SECOND;
            final StringBuilder bdr = new StringBuilder();
            bdr.append("Returning LotSettingSuccessResponse to PointOfSale serialNumber: ").append(posSerialNumber).append(" after ")
                    .append(processingTime).append(" seconds.");
            LOG.info(bdr.toString());
        }
    }
    
    private void process(final PS2XMLBuilder xmlBuilder) {
        if (!validatePOS(xmlBuilder)) {
            return;
        }
        
        try {
            this.ack.setDeviceSerialNumber(this.pointOfSale.getSerialNumber());
            
            final Customer customer = this.customerService.findCustomer(this.pointOfSale.getCustomer().getId());
            
            /**
             * ack.customerId refers to the field customerId in FMS, which is mapped by the Link id in Iris
             * we don't use the link/unifi term in FMS so for serialization this must be called ack.customerId
             */
            
            this.ack.setCustomerId(String.valueOf(customer.getId()));
            
            final Future<Boolean> publishedAck =
                    this.producer.send(KafkaKeyConstants.FMS_KAFKA_UPGRADE_ACK_TOPIC_NAME, this.ack.getDeviceSerialNumber(), this.ack);
            
            final PosServiceState ss = this.pointOfSale.getPosServiceState();
            ss.setIsNewPaystationSetting(false);
            ss.setLastPaystationSettingUploadGmt(this.getUploadedDate());
            ss.setPaystationSettingName(this.getSettingName());
            this.posServiceStateService.updatePosServiceState(ss);
            
            if (ss.getSettingsFile() == null || !ss.getSettingsFile().getName().equals(this.getSettingName())) {
                final SettingsFile settingsFile = this.paystationSettingService
                        .findSettingsFileByCustomerIdAndName(this.pointOfSale.getCustomer().getId(), this.getSettingName());
                this.pointOfSale.setSettingsFile(settingsFile);
            } else {
                this.pointOfSale.setSettingsFile(ss.getSettingsFile());
            }
            super.pointOfSaleService.update(this.pointOfSale);
            
            // Verify that the publish of acknowledge is completed before response to the request.
            publishedAck.get();
            if (LOG.isDebugEnabled()) {
                LOG.debug("Published the pointOfSale upgraded acknowledge for serialNumber: " + this.pointOfSale.getSerialNumber());
            }
            
            xmlBuilder.insertAcknowledge(messageNumber);
            
        } catch (Exception e) {
            final String msg = "Unable to process LotSettingSuccessRequest for pointOfSale serialNumber: " + this.pointOfSale.getSerialNumber();
            processException(msg, e);
            xmlBuilder.insertErrorMessage(this.messageNumber, BaseHandler.EMS_UNAVAILABLE_STRING);
        }
    }
    
    @StoryAlias(ALIAS_GROUP_NAME)
    public String getSettingName() {
        return this.ack.getGroupName();
    }
    
    public void setSettingName(final String settingName) {
        this.ack.setGroupName(settingName);
    }
    
    @StoryAlias(ALIAS_GROUP_UPDATED_DATE)
    @StoryTransformer("dateString")
    public Date getUploadedDate() {
        return this.ack.getUploadDate();
    }
    
    public void setUploadedDate(final String timestamp) throws InvalidDataException {
        this.ack.setUploadDate(StableDateUtil.convertFromColonDelimitedDateString(timestamp));
    }
    
    public void setUploadedDate(final Date timestamp) {
        this.ack.setUploadDate(timestamp);
    }
    
    public void setProducer(final AbstractMessageProducer producer) {
        this.producer = producer;
    }
    
    @Override
    @StoryAlias(ALIAS_SERIAL_NUMBER)
    public String getPaystationCommAddress() {
        return super.getPaystationCommAddress();
    }
    
    @Override
    public void setPaystationCommAddress(final String paystationCommAddress) {
        super.setPaystationCommAddress(paystationCommAddress);
    }
    
    public void setWebApplicationContext(final WebApplicationContext ctx) {
        super.setWebApplicationContext(ctx);
        this.posServiceStateService = (PosServiceStateService) ctx.getBean("posServiceStateService");
        this.producer = ctx.getBean(IrisMessageProducer.class);
    }
}
