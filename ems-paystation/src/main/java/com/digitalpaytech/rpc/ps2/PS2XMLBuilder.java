/**
 * Copyright @ 2002 Digital Pioneer. All Rights Reserved.
 */

package com.digitalpaytech.rpc.ps2;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.domain.ActivePermit;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.dto.PlateCheckResponse;
import com.digitalpaytech.dto.paystation.SpaceInfo;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.SystemException;
import com.digitalpaytech.exception.XMLErrorException;
import com.digitalpaytech.rpc.support.IRequestDataHandler;
import com.digitalpaytech.rpc.support.LinuxConfigNotificationInfo;
import com.digitalpaytech.rpc.support.MessageTags;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.thoughtworks.xstream.XStream;

@StoryAlias(PS2XMLBuilder.ALIAS)
@SuppressWarnings({ "PMD.TooManyMethods" })
public class PS2XMLBuilder extends PS2XMLBuilderBase {
    public static final String ALIAS = "PS2XMLBuilder";
    
    public static final String ERROR_INVALID_PAYSTATION = "Pay station is not provisioned";
    
    private static final Logger LOG = Logger.getLogger(PS2XMLBuilder.class);
    
    private static final String AUTHORIZATION_CODE = "AuthorizationCode";
    
    private static final String NULL = "null";
    
    private static final String ACKNOWLEDGE_STRING = "Acknowledge";
    private static final String ERROR_MESSAGE_STRING = "ErrorMessage";
    private static final String MESSAGE_NUMBER_STRING = "MessageNumber";
    private static final String RESPONSE_MESSAGE_STRING = "ResponseMessage";
    private static final String LOT_NUMBER_STRING = "LotNumber";
    private static final String STALL_NUMBER_STRING = "StallNumber";
    private static final String ADD_TIME_NUM_STRING = "AddTimeNum";
    private static final String PURCHASED_DATE_STRING = "PurchaseDate";
    private static final String MERCHANT_INFO_STRING = "MerchantInfo";
    private static final String EXPIRY_DATE_STRING = "ExpiryDate";
    private static final String TOTAL_TIME_PURCHASED_STRING = "TotalTimePurchased";
    private static final String EMV_MERCHANT_ACCOUNT_RESPONSE_STRING = "EMVMerchantAccountResponse";
    private static final String TYPE = "Type";
    // The server time zone
    private static final String SERVER_TIME_ZONE = "UTC";
    private static final String GMT_TIME_ZONE = "GMT";
    
    // AuthorizeCardResponse
    private static final String AUTHORIZE_CARD_RESPONSE_STRING = "AuthorizeCardResponse";
    private static final String AUTHORIZATION_ID_STRING = "AuthorizationId";
    private static final String COUPON_AUTH_RESPONSE_STRING = "CouponAuthResponse";
    private static final String EMS_PRE_AUTH_ID_STRING = "EmsPreAuthId";
    private static final String EMV_MERCHANT_ACCOUNT_NOTIFICATION = "EMVMerchantAccountNotification";
    private static final String OTA_FIRMWARE_NOTIFICATION = "OTAFirmwareUpdate";
    private static final String OTA_FIRMWARE_CANCEL_PENDING_NOTIFICATION = "CancelOTAFirmwareUpdate";
    private static final String CONFIGURATION_NOTIFICATION = "ConfigurationUpdate";
    private static final String BIN_RANGE_NOTIFICATION = "NewBINRange";
    private static final String BAD_CARD_LIST_NOTIFICATION = "NewBadCardList";
    
    private static final String ERROR_MESSAGE_TYPE_STRING = TYPE;
    
    // StallInfoResponse
    private static final String STALL_INFO_RESPONSE_STRING = "StallInfoResponse";
    
    // StallReportResponse
    private static final String STALL_REPORT_RESPONSE_STRING = "StallReportResponse";
    private static final String PAYMENT_GATEWAY_RESPONSE_STRING = "PaymentGatewayResponse";
    private static final String STALL_REPORT_TYPE_STRING = TYPE;
    
    // PaystationSettingResponse
    private static final String LOT_SETTING_NOTIFICATION_STRING = "LotSettingNotification";
    private static final String PUBLIC_KEY_NOTIFICATION_STRING = "EncryptKeyNotification";
    private static final String EMS_DOMAIN_NOTIFICATION_STRING = "EMSDomainNotification";
    private static final String ROOT_CERTIFICATE_NOTIFICATION_STRING = "RootCertificateNotification";
    
    // PlateReportResponse
    private static final String PLATE_REPORT_RESPONSE_STRING = "PlateRpt";
    private static final String PLATE_REPORT_RETURN_MESSAGE_NUMBER_STRING = "MsgNum";
    private static final String PLATE_REPORT_RESPONSE_START_POSITION_STRING = "StartPos";
    private static final String PLATE_REPORT_RESPONSE_RECORDS_RETURNED_STRING = "RecReturned";
    private static final String PLATE_REPORT_RESPONSE_REPORT_SIZE_STRING = "RptSize";
    private static final String PLATE_REPORT_RESPONSE_PLATES_STRING = "Plates";
    private static final String PLATE_REPORT_RESPONSE_PLATE_REGION_STRING = "Region";
    private static final String PLATE_REPORT_RESPONSE_PLATE_STRING = "Plate";
    private static final String PLATE_REPORT_RESPONSE_PLATE_RETURN_SEQUENCE_NUMBER_STRING = "Pos";
    private static final String PLATE_REPORT_RESPONSE_PLATE_EXPIRY_DATE_STRING = "Expiry";
    
    private static final String PLATE_CHECK_RESPONSE_STRING = "PlateCheckResponse";
    private static final String PLATE_CHECK_RESPONSE_IS_PREFERRED = "IsPreferred";
    private static final String PLATE_CHECK_RESPONSE_DAILY_LIMITED_COUNT = "DailyLimitedCount";
    private static final String PLATE_CHECK_RESPONSE_DAILY_PREFERRED_COUNT = "DailyPreferredCount";
    
    private static final String XML_DTD_FILE_LOCATION_STRING = "xml/Paystation2.dtd";
    
    private String originalMessage;
    private Document xmlPaystationDocument;
    private Element destinationElement;
    private boolean validPaystation = true;
    private List<String> errorStateList;
    
    // if set to false, add to paystationsetting notifcation if required.
    
    private boolean isPaystationSettingNotificationAdded;
    private boolean isNewPublicKeyNotificationAdded;
    private boolean isNewOTAFirmwareUpdateNotification;
    private boolean isNewOTAFirmwareUpdateCancelPendingNotification;
    private boolean isNewConfigurationNotification;
    private boolean isNewBINRangeNotification;
    private boolean isNewMerchantAccountNotificationAdded;
    private boolean isNewRootCertificateNotificationAdded;
    private boolean isNewBadCardListNotification;
    
    // update the heartbeat only once per message bundle
    private boolean heartBeatUpdated;
    
    private static XStream xstream;
    static {
        xstream = new XStream();
        xstream.autodetectAnnotations(true);
        xstream.alias("LinuxConfig", LinuxConfigNotificationInfo.class);
    }
    
    public PS2XMLBuilder(final String messageDestination) {
        super(XML_DTD_FILE_LOCATION_STRING, messageDestination);
    }
    
    public final void createAuthorizeCardResponse(final IRequestDataHandler requestDataHandler, final String authorizationId,
        final String responseMessage, final String emsPreAuthId) {
        final Element authorizeCardResponseElement = createPaystationResponse(requestDataHandler, AUTHORIZE_CARD_RESPONSE_STRING);
        
        createNamedElement(xmlPaystationDocument, authorizeCardResponseElement, AUTHORIZATION_ID_STRING, authorizationId);
        
        createNamedElement(xmlPaystationDocument, authorizeCardResponseElement, RESPONSE_MESSAGE_STRING, responseMessage);
        
        createNamedElement(xmlPaystationDocument, authorizeCardResponseElement, EMS_PRE_AUTH_ID_STRING, emsPreAuthId);
    }
    
    public final void createAuthorizeResponse(final IRequestDataHandler requestDataHandler, final String type, final String data) {
        final Element authorizeResponseElement = createPaystationResponse(requestDataHandler, MessageTags.AUTHORIZE_RESPONSE_TAG_NAME);
        
        createNamedElement(xmlPaystationDocument, authorizeResponseElement, TYPE, type);
        
        createNamedElement(xmlPaystationDocument, authorizeResponseElement, "Data", data);
    }
    
    public final void createCouponAuthResponse(final IRequestDataHandler requestDataHandler, final String couponCode, final String message,
        final String discountType, final String discountAmount, final String discountPercent) {
        final Element couponAuthRespElem = createPaystationResponse(requestDataHandler, COUPON_AUTH_RESPONSE_STRING);
        createNamedElement(xmlPaystationDocument, couponAuthRespElem, "CouponCode", couponCode);
        createNamedElement(xmlPaystationDocument, couponAuthRespElem, "Result", message);
        
        if (StringUtils.isNotBlank(discountType)) {
            createNamedElement(xmlPaystationDocument, couponAuthRespElem, "DiscountType", discountType);
        }
        if (StringUtils.isNotBlank(discountAmount)) {
            createNamedElement(xmlPaystationDocument, couponAuthRespElem, "DiscountAmount", discountAmount);
        } else if (StringUtils.isNotBlank(discountPercent)) {
            createNamedElement(xmlPaystationDocument, couponAuthRespElem, "DiscountPercent", discountPercent);
        }
    }
    
    public final void createStallInfoResponse(final IRequestDataHandler requestDataHandler, final SpaceInfo stallInfo, final String responseMessage) {
        final Element stallInfoResponseElement = createPaystationResponse(requestDataHandler, STALL_INFO_RESPONSE_STRING);
        
        if (stallInfo == null) {
            final Date date = new Date();
            createNamedElement(xmlPaystationDocument, stallInfoResponseElement, LOT_NUMBER_STRING, NULL);
            
            createNamedElement(xmlPaystationDocument, stallInfoResponseElement, STALL_NUMBER_STRING, NULL);
            
            createNamedElement(xmlPaystationDocument, stallInfoResponseElement, PURCHASED_DATE_STRING,
                               DateUtil.createColonDelimitedDateString(SERVER_TIME_ZONE, date));
            
            createNamedElement(xmlPaystationDocument, stallInfoResponseElement, EXPIRY_DATE_STRING,
                               DateUtil.createColonDelimitedDateString(SERVER_TIME_ZONE, date));
        } else {
            createNamedElement(xmlPaystationDocument, stallInfoResponseElement, LOT_NUMBER_STRING, stallInfo.getPaystationSettingName());
            
            createNamedElement(xmlPaystationDocument, stallInfoResponseElement, STALL_NUMBER_STRING, stallInfo.getStallNumber());
            
            createNamedElement(xmlPaystationDocument, stallInfoResponseElement, PURCHASED_DATE_STRING,
                               DateUtil.createColonDelimitedDateString(SERVER_TIME_ZONE, stallInfo.getStartDate()));
            
            createNamedElement(xmlPaystationDocument, stallInfoResponseElement, EXPIRY_DATE_STRING,
                               DateUtil.createColonDelimitedDateString(SERVER_TIME_ZONE, stallInfo.getEndDate()));
        }
        
        createNamedElement(xmlPaystationDocument, stallInfoResponseElement, RESPONSE_MESSAGE_STRING, responseMessage);
        
        if (stallInfo == null) {
            createNamedElement(xmlPaystationDocument, stallInfoResponseElement, TOTAL_TIME_PURCHASED_STRING, StandardConstants.STRING_ZERO);
        } else {
            createNamedElement(xmlPaystationDocument, stallInfoResponseElement, TOTAL_TIME_PURCHASED_STRING,
                               Integer.toString(stallInfo.getTotalTimePurchased()));
        }
    }
    
    public final void createStallInfoForAddTimeNumResponse(final IRequestDataHandler requestDataHandler, final SpaceInfo stallInfo,
        final String responseMessage) {
        final Element stallInfoResponseElement = createPaystationResponse(requestDataHandler, STALL_INFO_RESPONSE_STRING);
        
        if (stallInfo == null) {
            final Date date = new Date();
            createNamedElement(xmlPaystationDocument, stallInfoResponseElement, LOT_NUMBER_STRING, NULL);
            
            // always return the stall number, even for a addtimenum request
            createNamedElement(xmlPaystationDocument, stallInfoResponseElement, ADD_TIME_NUM_STRING, NULL);
            
            createNamedElement(xmlPaystationDocument, stallInfoResponseElement, STALL_NUMBER_STRING, NULL);
            
            createNamedElement(xmlPaystationDocument, stallInfoResponseElement, PURCHASED_DATE_STRING,
                               DateUtil.createColonDelimitedDateString(SERVER_TIME_ZONE, date));
            
            createNamedElement(xmlPaystationDocument, stallInfoResponseElement, EXPIRY_DATE_STRING,
                               DateUtil.createColonDelimitedDateString(SERVER_TIME_ZONE, date));
        } else {
            createNamedElement(xmlPaystationDocument, stallInfoResponseElement, LOT_NUMBER_STRING, stallInfo.getPaystationSettingName());
            
            // always return the stall number, even for a addtimenum request
            createNamedElement(xmlPaystationDocument, stallInfoResponseElement, ADD_TIME_NUM_STRING, stallInfo.getAddTimeNumber());
            
            createNamedElement(xmlPaystationDocument, stallInfoResponseElement, STALL_NUMBER_STRING, stallInfo.getStallNumber());
            
            createNamedElement(xmlPaystationDocument, stallInfoResponseElement, PURCHASED_DATE_STRING,
                               DateUtil.createColonDelimitedDateString(SERVER_TIME_ZONE, stallInfo.getStartDate()));
            
            createNamedElement(xmlPaystationDocument, stallInfoResponseElement, EXPIRY_DATE_STRING,
                               DateUtil.createColonDelimitedDateString(SERVER_TIME_ZONE, stallInfo.getEndDate()));
        }
        
        createNamedElement(xmlPaystationDocument, stallInfoResponseElement, RESPONSE_MESSAGE_STRING, responseMessage);
        
        if (stallInfo == null) {
            createNamedElement(xmlPaystationDocument, stallInfoResponseElement, TOTAL_TIME_PURCHASED_STRING, StandardConstants.STRING_ZERO);
        } else {
            createNamedElement(xmlPaystationDocument, stallInfoResponseElement, TOTAL_TIME_PURCHASED_STRING,
                               Integer.toString(stallInfo.getTotalTimePurchased()));
        }
        
    }
    
    public final void createStallReportErrorResponse(final IRequestDataHandler requestDataHandler, final String reportType, final String lotNumber,
        final String responseMessage) {
        final Element responseElement = createPaystationResponse(requestDataHandler, STALL_REPORT_RESPONSE_STRING);
        
        createNamedElement(xmlPaystationDocument, responseElement, STALL_REPORT_TYPE_STRING, reportType);
        createNamedElement(xmlPaystationDocument, responseElement, LOT_NUMBER_STRING, lotNumber);
        createNamedElement(xmlPaystationDocument, responseElement, RESPONSE_MESSAGE_STRING, responseMessage);
        
    }
    
    public final void createStallReportResponse(final IRequestDataHandler requestDataHandler, final String reportType, final String lotNumber,
        final List<SpaceInfo> stallInfo, final String responseMessage) {
        final Element responseElement = createPaystationResponse(requestDataHandler, STALL_REPORT_RESPONSE_STRING);
        
        createNamedElement(xmlPaystationDocument, responseElement, STALL_REPORT_TYPE_STRING, reportType);
        
        createNamedElement(xmlPaystationDocument, responseElement, LOT_NUMBER_STRING, lotNumber);
        
        final Iterator<SpaceInfo> it = stallInfo.iterator();
        if (StallReportRequestHandler.STALL_REPORT_TYPE_VALID_STRING.equals(reportType)) {
            while (it.hasNext()) {
                final SpaceInfo currStall = it.next();
                
                createNamedElement(xmlPaystationDocument, responseElement, STALL_NUMBER_STRING, currStall.getStallNumber());
                
                createNamedElement(xmlPaystationDocument, responseElement, PURCHASED_DATE_STRING,
                                   DateUtil.createColonDelimitedDateString(SERVER_TIME_ZONE, currStall.getStartDate()));
                
                createNamedElement(xmlPaystationDocument, responseElement, EXPIRY_DATE_STRING,
                                   DateUtil.createColonDelimitedDateString(SERVER_TIME_ZONE, currStall.getEndDate()));
            }
        } else {
            while (it.hasNext()) {
                final SpaceInfo currStall = (SpaceInfo) it.next();
                
                createNamedElement(xmlPaystationDocument, responseElement, STALL_NUMBER_STRING, currStall.getStallNumber());
                
                createNamedElement(xmlPaystationDocument, responseElement, EXPIRY_DATE_STRING,
                                   DateUtil.createColonDelimitedDateString(SERVER_TIME_ZONE, currStall.getEndDate()));
            }
        }
        
        createNamedElement(xmlPaystationDocument, responseElement, RESPONSE_MESSAGE_STRING, responseMessage);
    }
    
    public final void createEMVMerchantAccountResponse(final IRequestDataHandler requestDataHandler, final PointOfSale pointOfSale,
        final Integer merchantAccountId, final String jsonString) {
        createEMVMerchantAccountResponse(requestDataHandler, pointOfSale, merchantAccountId.toString(), jsonString);
    }
    
    public final void createEMVMerchantAccountResponse(final IRequestDataHandler requestDataHandler, final PointOfSale pointOfSale,
        final String merchantAccountId, final String jsonString) {
        final Element emvMaMessage = createEMVMerchantAccountXMLTag(requestDataHandler, EMV_MERCHANT_ACCOUNT_RESPONSE_STRING, merchantAccountId);
        createNamedElement(xmlPaystationDocument, emvMaMessage, MERCHANT_INFO_STRING, jsonString);
        
    }
    
    public final void createPlateReportResponse(final IRequestDataHandler requestDataHandler, final Collection<ActivePermit> activePermits,
        final int startPosition, final int totalPlates, final String region) {
        final Element plateReportElement = createPaystationResponseForPlateReport(requestDataHandler, PLATE_REPORT_RESPONSE_STRING);
        plateReportElement.setAttribute(PLATE_REPORT_RETURN_MESSAGE_NUMBER_STRING, requestDataHandler.getMessageNumber());
        plateReportElement.setAttribute(PLATE_REPORT_RESPONSE_START_POSITION_STRING, Integer.toString(startPosition));
        plateReportElement.setAttribute(PLATE_REPORT_RESPONSE_RECORDS_RETURNED_STRING, Integer.toString(activePermits.size()));
        plateReportElement.setAttribute(PLATE_REPORT_RESPONSE_REPORT_SIZE_STRING, Integer.toString(totalPlates));
        
        final Element plates = createNamedElement(xmlPaystationDocument, plateReportElement, PLATE_REPORT_RESPONSE_PLATES_STRING);
        plates.setAttribute(PLATE_REPORT_RESPONSE_PLATE_REGION_STRING, region);
        
        int i = 0;
        for (ActivePermit currPlate : activePermits) {
            
            final Element plateElement =
                    createNamedElement(xmlPaystationDocument, plates, PLATE_REPORT_RESPONSE_PLATE_STRING, currPlate.getLicencePlateNumber());
            plateElement.setAttribute(PLATE_REPORT_RESPONSE_PLATE_RETURN_SEQUENCE_NUMBER_STRING, Integer.toString(startPosition + i++));
            plateElement.setAttribute(PLATE_REPORT_RESPONSE_PLATE_EXPIRY_DATE_STRING,
                                      DateUtil.createColonDelimitedDateString(GMT_TIME_ZONE, currPlate.getPermitExpireGmt()));
        }
    }
    
    public final void createPlateCheckResponse(final IRequestDataHandler requestDataHandler, final PlateCheckResponse plateCheckResponse) {
        
        final Element plateCheckResp = createPaystationResponse(requestDataHandler, PLATE_CHECK_RESPONSE_STRING);
        createNamedElement(this.xmlPaystationDocument, plateCheckResp, PLATE_CHECK_RESPONSE_IS_PREFERRED,
                           String.valueOf(plateCheckResponse.getIsPreferred()));
        createNamedElement(this.xmlPaystationDocument, plateCheckResp, PLATE_CHECK_RESPONSE_DAILY_LIMITED_COUNT,
                           String.valueOf(plateCheckResponse.getDailyLimitedCount()));
        createNamedElement(this.xmlPaystationDocument, plateCheckResp, PLATE_CHECK_RESPONSE_DAILY_PREFERRED_COUNT,
                           String.valueOf(plateCheckResponse.getDailyPreferredCount()));
    }
    
    public final void createPlateReportErrorResponse(final IRequestDataHandler requestDataHandler, final String responseMessage) {
        final Element responseElement = createPaystationResponseForPlateReport(requestDataHandler, PLATE_REPORT_RESPONSE_STRING);
        createNamedElement(xmlPaystationDocument, responseElement, RESPONSE_MESSAGE_STRING, responseMessage);
    }
    
    public final void createPaymentGatewayResponse(final IRequestDataHandler requestDataHandler, final String xmlResponse) {
        createPaystationResponse(requestDataHandler, PAYMENT_GATEWAY_RESPONSE_STRING);
        Node fragmentNode = fromXMLString(xmlResponse).getDocumentElement();
        fragmentNode = xmlPaystationDocument.importNode(fragmentNode, true);
        xmlPaystationDocument.getChildNodes().item(0).getFirstChild().getFirstChild().appendChild(fragmentNode);
    }
    
    public final void initialize(final String userId, final String password) throws InvalidDataException, SystemException {
        super.initialize();
        
        xmlPaystationDocument = createDocument(userId, password);
        destinationElement = getDestinationNode(xmlPaystationDocument);
    }
    
    public final void insertAcknowledge(final String messageNumber) {
        final Element acknowledgeMessage = createNamedElement(xmlPaystationDocument, destinationElement, ACKNOWLEDGE_STRING);
        
        acknowledgeMessage.setAttribute(MESSAGE_NUMBER_STRING, messageNumber);
        
    }
    
    public final void insertErrorMessage(final String messageNumber, final String errorMessage) {
        if (errorStateList == null) {
            errorStateList = new ArrayList<String>(20);
        }
        errorStateList.add(messageNumber);
        
        final Element errorMessageElement = createNamedElement(xmlPaystationDocument, destinationElement, ERROR_MESSAGE_STRING);
        
        createNamedElement(xmlPaystationDocument, errorMessageElement, ERROR_MESSAGE_TYPE_STRING, errorMessage);
        
        errorMessageElement.setAttribute(MESSAGE_NUMBER_STRING, messageNumber);
    }
    
    public final void insertAuthorizationCode(final String authCode) {
        final Element authCodeElement = createNamedElement(xmlPaystationDocument, destinationElement, AUTHORIZATION_CODE);
        
        authCodeElement.setAttribute(AUTHORIZATION_CODE, authCode);
        
    }
    
    public final boolean isErrorState(final String messageNumber) {
        return errorStateList != null && errorStateList.contains(messageNumber);
    }
    
    public final String toString() {
        try {
            return documentToString(xmlPaystationDocument);
        } catch (XMLErrorException e) {
            LOG.error("Failed to serialize document to a string!", e);
            
            return null;
        }
    }
    
    public final Element createPaystationResponse(final IRequestDataHandler requestDataHandler, final String messageName) {
        final Element responseElement = createNamedElement(xmlPaystationDocument, destinationElement, messageName);
        
        responseElement.setAttribute(BaseHandler.MESSAGE_NUMBER_STRING, requestDataHandler.getMessageNumber());
        
        createNamedElement(xmlPaystationDocument, responseElement, BaseHandler.PAYSTATION_COMM_ADDRESS_STRING,
                           requestDataHandler.getPosSerialNumber());
        
        return responseElement;
    }
    
    public final Element createEMVMerchantAccountXMLTag(final IRequestDataHandler requestDataHandler, final String messageName,
        final String merchantAccountId) {
        final Element responseElement = createNamedElement(this.xmlPaystationDocument, this.destinationElement, messageName);
        
        responseElement.setAttribute(BaseHandler.MESSAGE_NUMBER_STRING, requestDataHandler.getMessageNumber());
        
        createNamedElement(this.xmlPaystationDocument, responseElement, BaseHandler.MERCHANT_ACCOUNT_ID_STRING, merchantAccountId);
        return responseElement;
    }
    
    public final Element createPaystationResponseForPlateReport(final IRequestDataHandler requestDataHandler, final String messageName) {
        return createNamedElement(xmlPaystationDocument, destinationElement, messageName);
    }
    
    public final void createPaystationSettingNotification() {
        if (!isPaystationSettingNotificationAdded) {
            createNamedElement(xmlPaystationDocument, destinationElement, LOT_SETTING_NOTIFICATION_STRING);
            isPaystationSettingNotificationAdded = true;
        }
    }
    
    public final void createNewMerchantAccountNotification() {
        if (!this.isNewMerchantAccountNotificationAdded) {
            createNamedElement(this.xmlPaystationDocument, this.destinationElement, EMV_MERCHANT_ACCOUNT_NOTIFICATION);
            this.isNewMerchantAccountNotificationAdded = true;
        }
    }
    
    public final void createNewPublicKeyNotification() {
        if (!isNewPublicKeyNotificationAdded) {
            createNamedElement(xmlPaystationDocument, destinationElement, PUBLIC_KEY_NOTIFICATION_STRING);
            isNewPublicKeyNotificationAdded = true;
        }
    }
    

    public final void createNewOTAFirmwareUpdateNotification() {
        if (!isNewOTAFirmwareUpdateNotification) {
            createNamedElement(xmlPaystationDocument, destinationElement, OTA_FIRMWARE_NOTIFICATION);
            isNewOTAFirmwareUpdateNotification = true;
        }
    }

    public final void createNewOTAFirmwareUpdateCancelPendingNotification() {
        if (!isNewOTAFirmwareUpdateCancelPendingNotification) {
            createNamedElement(xmlPaystationDocument, destinationElement, OTA_FIRMWARE_CANCEL_PENDING_NOTIFICATION);
            isNewOTAFirmwareUpdateCancelPendingNotification = true;
        }
    }

    public final void createNewBINRangeNotification() {
        if (!isNewBINRangeNotification) {
            createNamedElement(xmlPaystationDocument, destinationElement, BIN_RANGE_NOTIFICATION);
            isNewBINRangeNotification = true;
        }
    }
    
    public final void createNewConfigurationNotification(final int pointOfSaleId, final String snapShotId,
        final LinuxConfigNotificationInfo notificationInfo) {
        if (!this.isNewConfigurationNotification) {
            InputStream xml = null;
            try {
                xml = new ByteArrayInputStream(xstream.toXML(notificationInfo).getBytes("UTF-8"));
                final Document doc = documentBuilder.parse(xml);
                final NodeList nodes = doc.getChildNodes();
                createNamedElementWithXMLBody(xmlPaystationDocument, destinationElement, CONFIGURATION_NOTIFICATION, nodes);
            } catch (IOException | SAXException e) {
                LOG.error("Unable to create configuration notification XML", e);
            } finally {
                if (xml != null) {
                    try {
                        xml.close();
                    } catch (IOException ioe) {
                        final StringBuilder bdr = new StringBuilder();
                        bdr.append("Cannot close InputStream, pointOfSaleId: ").append(pointOfSaleId).append(", snapShotId: ").append(snapShotId);
                        LOG.error(bdr.toString(), ioe);
                    }
                }
            }
            this.isNewConfigurationNotification = true;
        }
    }
    

    public final void createNewRootCertificateNotification() {
        if (!this.isNewRootCertificateNotificationAdded) {
            createNamedElement(xmlPaystationDocument, destinationElement, ROOT_CERTIFICATE_NOTIFICATION_STRING);
            this.isNewRootCertificateNotificationAdded = true;
        }
    }
    
    public final void createNewBadCardListNotification() {
        if (!this.isNewBadCardListNotification) {
            createNamedElement(this.xmlPaystationDocument, this.destinationElement, BAD_CARD_LIST_NOTIFICATION);
            this.isNewBadCardListNotification = true;
        }
    }
    
    public final void createEMSPaystationURLRerouteNotification(final Map<String, String> attributeNamesValues) {
        createNamedElementWithAttributes(xmlPaystationDocument, destinationElement, EMS_DOMAIN_NOTIFICATION_STRING, attributeNamesValues);
    }
    
    public final String getOriginalMessage() {
        return originalMessage;
    }
    
    public final void setOriginalMessage(final String originalMessage) {
        this.originalMessage = originalMessage;
    }
    
    public final boolean isValidPaystation() {
        return validPaystation;
    }
    
    public final void setIsValidPaystation(final boolean isValidPaystation) {
        this.validPaystation = isValidPaystation;
    }
    
    public final boolean isHeartBeatUpdated() {
        return heartBeatUpdated;
    }
    
    public final void setIsHeartBeatUpdated(final boolean isHeartBeatUpdated) {
        this.heartBeatUpdated = isHeartBeatUpdated;
    }
}
