package com.digitalpaytech.rpc.ps2;

import java.nio.charset.Charset;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.client.PaymentClient;
import com.digitalpaytech.client.dto.merchant.CardReaderConfig;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.rpc.support.MessageTags;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.json.JSON;
import com.digitalpaytech.util.json.JSONConfigurationBean;
import com.digitalpaytech.util.xml.XMLBuilderBase;
import com.fasterxml.jackson.core.type.TypeReference;

@StoryAlias(value = EMVMerchantAccountHandler.ALIAS)
public class EMVMerchantAccountHandler extends BaseHandler {
    public static final String ALIAS = "emv merchant account data";
    public static final String ALIAS_SERIAL_NUMBER = "serial number";
    private static final Logger LOG = Logger.getLogger(EMVMerchantAccountHandler.class);
    private static final String KEY_MERCHANT_STATUS = "merchantStatus";
    private static final String KEY_PROCESSOR_URL = "processorURL";
    
    private static JSON json;
    
    static {
        final JSONConfigurationBean jsonConfig = new JSONConfigurationBean(true, false, false);
        json = new JSON(jsonConfig);
    }
    
    public EMVMerchantAccountHandler(final String messageNumber) {
        super(messageNumber);
    }
    
    @Override
    public final String getRootElementName() {
        return MessageTags.EMV_MERCHANT_ACCOUNT_TAG_NAME;
    }
    
    @Override
    public final void process(final XMLBuilderBase xmlBuilder) {
        final long startTime = System.currentTimeMillis();
        process((PS2XMLBuilder) xmlBuilder);
        
        if (LOG.isInfoEnabled()) {
            final float processingTime = (System.currentTimeMillis() - startTime) / StandardConstants.FLOAT_THOUSAND;
            final StringBuilder bdr = new StringBuilder();
            if (pointOfSale == null) {
                bdr.append("PointOfSale serial number is incorrect, unable to retrieve record. Processing time: ");
            } else {
                bdr.append("Returning to response to PointToSale serialNumber: ").append(pointOfSale.getSerialNumber()).append(" after ");
            }
            bdr.append(processingTime).append(" seconds.");
            LOG.info(bdr.toString());
        }
    }
    
    private void process(final PS2XMLBuilder xmlBuilder) {
        if (!validatePOS(xmlBuilder)) {
            return;
        }
        final Date now = DateUtil.getCurrentGmtDate();
        final PosServiceState posServiceState = this.pointOfSale.getPosServiceState();
        posServiceState.setMerchantAccountRequestedGMT(now);
        customerAdminService.saveOrUpdatePointOfSale(pointOfSale);
        final MerchantAccount merchantAccount =
                super.merchantAccountService.findByPointOfSaleIdAndCardTypeId(this.pointOfSale.getId(), WebCoreConstants.CARD_TYPE_CREDIT_CARD);
        
        if (merchantAccount == null) {
            LOG.error("No Merchant Account found");
            xmlBuilder.insertErrorMessage(messageNumber, WebCoreConstants.TERMINAL_DISABLED);
        } else if (StringUtils.isBlank(merchantAccount.getTerminalToken())) {
            LOG.error("MerchantAccount ID: " + merchantAccount.getId() + " doesn't have terminalToken. Return 'Disabled' back to EMV pay station.");
            xmlBuilder.insertErrorMessage(messageNumber, WebCoreConstants.TERMINAL_DISABLED);
        } else {
            try {
                final Map<String, String> configMap;
                final String jsonString;
                
                if (merchantAccount.getIsLink()) {
                    final CardReaderConfig crc = super.merchantAccountService.getCardReaderConfig(merchantAccount.getTerminalToken());
                    jsonString = EMVMerchantAccountHandler.json.serialize(crc);
                    xmlBuilder.createEMVMerchantAccountResponse(this, pointOfSale, crc.getMerchantId(), jsonString);
                } else {
                    
                    final PaymentClient payService = this.clientFactory.from(PaymentClient.class);
                    final String terminalConfiguration =
                            payService.grabTerminalInfo(merchantAccount.getTerminalToken()).execute().toString(Charset.defaultCharset());
                    configMap = EMVMerchantAccountHandler.json.deserialize(terminalConfiguration, new TypeReference<Map<String, String>>() {
                    });
                    
                    final String processorURL = merchantAccount.getProcessor().isIsTest() ? merchantAccount.getProcessor().getTestUrl()
                            : merchantAccount.getProcessor().getProductionUrl();
                    configMap.put(KEY_PROCESSOR_URL, processorURL);
                    configMap.put(KEY_MERCHANT_STATUS, merchantAccount.getMerchantStatusType().getName());
                    jsonString = EMVMerchantAccountHandler.json.serialize(configMap);
                    xmlBuilder.createEMVMerchantAccountResponse(this, pointOfSale, merchantAccount.getId(), jsonString);
                }
                
            } catch (JsonException | InvalidDataException e) {
                LOG.error("Unable to process JSON Mapping or MerchantAccount is invalid, id: " + merchantAccount.getId(), e);
                xmlBuilder.insertErrorMessage(messageNumber, BaseHandler.EMS_UNAVAILABLE_STRING);
            }
        }
        
    }
    
    @StoryAlias(ALIAS_SERIAL_NUMBER)
    public final String getPaystationCommAddress() {
        return super.getPaystationCommAddress();
    }
    
    public final void setPaystationCommAddress(final String paystationCommAddress) {
        super.setPaystationCommAddress(paystationCommAddress);
    }
    
}
