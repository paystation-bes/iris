package com.digitalpaytech.rpc.ps2;

import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import com.digitalpaytech.domain.EmailAddress;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.PurchaseEmailAddress;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.rpc.support.MessageTags;
import com.digitalpaytech.service.ConsumerService;
import com.digitalpaytech.service.PurchaseEmailAddressService;
import com.digitalpaytech.service.PurchaseService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.xml.XMLBuilderBase;

public class ReceiptHandler extends BaseHandler {
    
    private static Logger log = Logger.getLogger(ReceiptHandler.class);
    
    private ConsumerService consumerService;
    private PurchaseEmailAddressService purchaseEmailAddressService;
    private PurchaseService purchaseService;
    
    private Date purchasedDate;
    private EmailAddress emailAddress;
    private Integer number;
    
    public ReceiptHandler(String messageNumber) {
        super(messageNumber);
    }
    
    @Override
    public String getRootElementName() {
        return MessageTags.RECEIPT_TAG_NAME;
    }
    
    @Override
    public void process(XMLBuilderBase xmlBuilder) {
        long start_time = System.currentTimeMillis();
        process((PS2XMLBuilder) xmlBuilder);
        
        if (log.isInfoEnabled()) {
            float processing_time = (System.currentTimeMillis() - start_time) / MILLISECOND_TO_SECOND;
            StringBuilder bdr = new StringBuilder();
            if (pointOfSale == null) {
                bdr.append("PointOfSale serial number is incorrect, unable to retrieve record. Processing time: ");
            } else {
                bdr.append("Returning to ReceiptHandler for PointToSale serialNumber: ").append(pointOfSale.getSerialNumber()).append(" after ");
            }
            bdr.append(processing_time).append(" seconds.");
            log.info(bdr.toString());
        }
    }
    
    private void process(PS2XMLBuilder xmlBuilder) {
        
        if (!validatePOS(xmlBuilder)) {
            return;
        }
        
        try {
            if (purchasedDate == null) {
                throw new InvalidDataException("PurchasedDate is null for PaystationCommAddress: " + pointOfSale.getSerialNumber());
            }
            
            if (number == null) {
                throw new InvalidDataException("Number is null for PaystationCommAddress: " + pointOfSale.getSerialNumber() + ", PurchasedDate: "
                                               + purchasedDate);
            }
            
            Purchase purchase = purchaseService.findPurchaseByPointOfSaleIdAndPurchaseNumberAndpPurchaseGmt(pointOfSale.getId(), number, purchasedDate);
            
            if (purchase == null) {
                throw new InvalidDataException("Purchase not found for PaystationCommAddress: " + pointOfSale.getSerialNumber() + ", Number: " + number
                                               + ", PurchasedDate: " + purchasedDate);
            }
            
            if (emailAddress != null && !emailAddress.getEmail().equals("") && emailAddress.getEmail().length() <= WebCoreConstants.VALIDATION_MAX_LENGTH_255) {
                
                purchaseEmailAddressService.saveEmailAddress(this.emailAddress);
                PurchaseEmailAddress purchaseEmailAddress = new PurchaseEmailAddress(purchase, emailAddress, new Date(), false);
                purchaseEmailAddressService.savePurchaseEmail(purchaseEmailAddress);
            } else {
                
                log.info("No receipt sent for Email Address:" + emailAddress != null ? emailAddress.getEmail() : "" + ", PaystationCommAddress: "
                                                                                                                 + pointOfSale.getSerialNumber() + ", Number: "
                                                                                                                 + number + ", PurchasedDate: " + purchasedDate);
            }
            
            xmlBuilder.insertAcknowledge(messageNumber);
            checkForNotifications(xmlBuilder);
            
        } catch (Exception e) {
            
            log.info("ReceiptHandler", e);
            StringBuilder bdr = new StringBuilder();
            bdr.append("ReceiptHandler, ");
            bdr.append(e.getMessage());
            bdr.append("Unable to process Receipt for POS serialNumber: " + pointOfSale.getSerialNumber());
            bdr.append(", Name: ").append(pointOfSale.getName());
            bdr.append(", Customer Id: ").append(pointOfSale.getCustomer().getId());
            processException(bdr.toString(), e);
            
            xmlBuilder.insertErrorMessage(messageNumber, BaseHandler.EMS_UNAVAILABLE_STRING);
        }
    }
    
    public void setPurchasedDate(String purchasedDate) throws InvalidDataException {
        this.purchasedDate = DateUtil.convertFromColonDelimitedDateString(purchasedDate);
    }
    
    public Date getPurchasedDate() {
        return purchasedDate;
    }
    
    public void setNumber(String number) {
        this.number = Integer.parseInt(number);
    }
    
    public Integer getNumber() {
        return number;
    }
    
    public void setEmailAddress(String emailAddress) {
        List<EmailAddress> emailAddresses = consumerService.findEmailAddressByEmail(emailAddress);
        if (emailAddresses == null) {
            this.emailAddress = new EmailAddress(emailAddress, new Date());
        } else {
            this.emailAddress = emailAddresses.get(0);
        }
    }
    
    public EmailAddress getEmailAddress() {
        return emailAddress;
    }
    
    public void setWebApplicationContext(WebApplicationContext ctx) {
        super.setWebApplicationContext(ctx);
        consumerService = (ConsumerService) ctx.getBean("consumerService");
        purchaseEmailAddressService = (PurchaseEmailAddressService) ctx.getBean("purchaseEmailAddressService");
        purchaseService = (PurchaseService) ctx.getBean("purchaseService");
    }
}