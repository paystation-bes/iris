package com.digitalpaytech.rpc.ps2;

import java.nio.charset.Charset;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;

import com.digitalpaytech.client.DcmsClient;
import com.digitalpaytech.client.dto.LinuxConfigurationAck;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.rpc.support.MessageTags;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.xml.XMLBuilderBase;
import com.netflix.hystrix.exception.HystrixBadRequestException;
import com.netflix.hystrix.exception.HystrixRuntimeException;

public class LinuxConfigurationSuccessHandler extends BaseHandler {
    
    private static final Logger LOG = Logger.getLogger(LinuxConfigurationSuccessHandler.class);
    
    private LinuxConfigurationAck ack;
    
    public LinuxConfigurationSuccessHandler(final String messageNumber) {
        super(messageNumber);
        this.ack = new LinuxConfigurationAck();
    }
    
    @Override
    public final String getRootElementName() {
        return MessageTags.LINUX_CONFIGURATION_SUCCESS;
    }
    
    @Override
    public final void process(final XMLBuilderBase xmlBuilder) {
        final long startTime = System.currentTimeMillis();
        process((PS2XMLBuilder) xmlBuilder);
        
        if (LOG.isInfoEnabled()) {
            final float processingTime = (System.currentTimeMillis() - startTime) / 1000f;
            final StringBuilder bdr = new StringBuilder();
            bdr.append("Returning LinuxConfigurationSuccess to PointOfSale serialNumber: ").append(posSerialNumber).append(" after ")
                    .append(processingTime).append(" seconds.");
            LOG.info(bdr.toString());
        }
        
    }
    
    private void process(final PS2XMLBuilder xmlBuilder) {
        if (!validatePOS(xmlBuilder)) {
            return;
        }
        
        final String deviceId = this.pointOfSale.getSerialNumber();
        this.ack.setDeviceSerialNumber(deviceId);
        
        final Customer customer = this.customerService.findCustomer(this.pointOfSale.getCustomer().getId());
        
        /**
         * ack.customerId refers to the field customerId in LDCMS, which is mapped by the Link id in Iris
         * we don't use the link/unifi term in any MS so for serialization this must be called ack.customerId
         */
        
        this.ack.setCustomerId(customer.getUnifiId());
        
        final PosServiceState pss = this.pointOfSale.getPosServiceState();
        
        final String ackConfigSnapshotId = this.ack.getConfigSnapshotId();
        try {
            final DcmsClient dcmsClient = super.getClientFactory().from(DcmsClient.class);
            if (pss.getNewConfigurationId() != null && pss.getNewConfigurationId().equals(ackConfigSnapshotId)) {
                
                dcmsClient.configurationAck(customer.getUnifiId().intValue(), deviceId, this.ack).execute().toString(Charset.defaultCharset());
                
                pss.setNewConfigurationId(null);
                this.posServiceStateService.updatePosServiceState(pss);
                xmlBuilder.insertAcknowledge(messageNumber);
                
            } else {
                dcmsClient.configurationAck(customer.getUnifiId().intValue(), deviceId, this.ack).execute().toString(Charset.defaultCharset());
                LOG.error("Received mismatch configurationSnapshotIds message [" + ackConfigSnapshotId + "] and db [" + pss.getNewConfigurationId()
                         + "] for POS " + this.pointOfSale.getSerialNumber());
                xmlBuilder.insertAcknowledge(messageNumber);
            }
        } catch (HystrixRuntimeException | HystrixBadRequestException hre) {
            LOG.error("Communication exception with DCMS", hre);
            super.processException("Failed to update device state", hre);
            xmlBuilder.insertErrorMessage(messageNumber, BaseHandler.EMS_UNAVAILABLE_STRING);
            
        }
    }
    
    public final Integer getCustomerId() {
        return this.ack.getCustomerId();
    }
    
    public final void setCustomerId(final Integer customerId) {
        this.ack.setCustomerId(customerId);
    }
    
    public final String getPayStationCommAddress() {
        return this.ack.getDeviceSerialNumber();
    }
    
    public final void setPayStationCommAddress(final String deviceId) {
        this.ack.setDeviceSerialNumber(deviceId);
    }
    
    public final String getConfigSnapshotId() {
        return this.ack.getConfigSnapshotId();
    }
    
    public final void setConfigSnapshotId(final String configSnapshotId) {
        this.ack.setConfigSnapshotId(configSnapshotId);
    }
    
    public final String getActiveDateTime() {
        return this.ack.getActiveDateTime();
    }
    
    public final void setActiveDateTime(final String activeDateTime) throws InvalidDataException {
        this.ack.setActiveDateTime(DateUtil.colonDelimitedStringToZuluString(activeDateTime));
    }
    
    public final void setWebApplicationContext(final WebApplicationContext ctx) {
        super.setWebApplicationContext(ctx);
    }
}
