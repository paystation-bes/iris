package com.digitalpaytech.rpc.ps2;

import java.io.UnsupportedEncodingException;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.UriUtils;

import com.digitalpaytech.client.LicensePlateClient;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.dto.PlateCheck;
import com.digitalpaytech.dto.PlateCheckResponse;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.rpc.support.MessageTags;
import com.digitalpaytech.service.LicensePlateService;
import com.digitalpaytech.util.LicensePlateConstants;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.util.StableDateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.xml.XMLBuilderBase;
import com.netflix.hystrix.exception.HystrixRuntimeException;

public class PlateCheckHandler extends BaseHandler {
    private static final Logger LOG = Logger.getLogger(PlateCheckHandler.class);
    
    private int maxDailyLimitedCount;
    
    private LicensePlateService licensePlateService;
    
    private String licensePlateNo;
    
    public PlateCheckHandler(final String messageNumber) {
        super(messageNumber);
    }
    
    @Override
    public final String getRootElementName() {
        return MessageTags.PLATE_CHECK;
    }
    
    @Override
    public final void process(final XMLBuilderBase xmlBuilder) {
        final long startTime = System.currentTimeMillis();
        process((PS2XMLBuilder) xmlBuilder);
        
        if (LOG.isInfoEnabled()) {
            final float processingTime = (System.currentTimeMillis() - startTime) / MILLISECOND_TO_SECOND;
            final StringBuilder bdr = new StringBuilder();
            bdr.append("Returning PlateCheck to PointOfSale serialNumber: ").append(this.posSerialNumber).append(" after ").append(processingTime)
                    .append(" seconds.");
            LOG.info(bdr.toString());
        }
    }
    
    @SuppressWarnings({ "checkstyle:illegalcatch" })
    private void process(final PS2XMLBuilder xmlBuilder) {
        if (!validatePOS(xmlBuilder)) {
            return;
        }
        
        if (StringUtils.isBlank(this.licensePlateNo)) {
            xmlBuilder.insertErrorMessage(this.messageNumber, BaseHandler.EMS_UNAVAILABLE_STRING);
        } else {
            final Customer customer = this.pointOfSale.getCustomer();
            final CustomerProperty timeZoneProperty = this.customerAdminService
                    .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customer.getId(), WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
            
            final CustomerProperty preferredByLocationProp = this.customerAdminService
                    .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customer.getId(),
                                                                            WebCoreConstants.CUSTOMER_PROPERTY_TYPE_JURISDICTION_TYPE_PREFERRED);
            
            final CustomerProperty limitedByLocationProp = this.customerAdminService
                    .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customer.getId(),
                                                                            WebCoreConstants.CUSTOMER_PROPERTY_TYPE_JURISDICTION_TYPE_LIMITED);
            
            final boolean isPreferredDisabled = this.licensePlateService
                    .compareJurisdictionType(PaystationConstants.PAYSTATION_JURISDICTION_PREFERRED_DISABLED, preferredByLocationProp);
            
            final boolean isLimitedDisabled =
                    this.licensePlateService.compareJurisdictionType(PaystationConstants.PAYSTATION_JURISDICTION_LIMITED_DISABLED, limitedByLocationProp);
            
            if (isPreferredDisabled && isLimitedDisabled) {
                xmlBuilder.createPlateCheckResponse(this, new PlateCheckResponse(false, this.maxDailyLimitedCount, 0));
            } else {
                
                final boolean isPreferredByLocation = this.licensePlateService
                        .compareJurisdictionType(PaystationConstants.PAYSTATION_JURISDICTION_PREFERRED_BY_LOCATION, preferredByLocationProp);
                
                final boolean isLimitedByLocation = this.licensePlateService
                        .compareJurisdictionType(PaystationConstants.PAYSTATION_JURISDICTION_LIMITED_BY_LOCATION, limitedByLocationProp);
                
                final String localDate = StableDateUtil.getDateFormattedWithChronoUnitDays(StableDateUtil.DATE_ONLY_FORMAT,
                                                                                           timeZoneProperty.getPropertyValue(), new Date());
                
                try {
                    final String locName = UriUtils.encodePath(this.pointOfSale.getLocation().getName(), StandardConstants.HTTP_UTF_8_ENCODE_CHARSET);
                    final PlateCheck plateCheck = new PlateCheck(this.licensePlateNo, locName, localDate, customer.getUnifiId(),
                            isPreferredByLocation, isLimitedByLocation);
                    
                    final PlateCheckResponse plateCheckResponse = this.licensePlateService.checkPlateLookup(plateCheck);
                    
                    if (isPreferredDisabled) {
                        plateCheckResponse.setDailyPreferredCount(0);
                        plateCheckResponse.setIsPreferred(false);
                    } else if (isLimitedDisabled) {
                        plateCheckResponse.setDailyLimitedCount(this.maxDailyLimitedCount);
                    }
                    
                    xmlBuilder.createPlateCheckResponse(this, plateCheckResponse);
                    
                } catch (JsonException | HystrixRuntimeException | UnsupportedEncodingException e) {
                    LOG.error("Unable to contact " + LicensePlateClient.ALIAS, e);
                    xmlBuilder.insertErrorMessage(messageNumber, PaystationConstants.RESPONSE_MESSAGE_EMS_UNAVAILABLE);
                }
            }
        }
    }
    
    public final void setWebApplicationContext(final WebApplicationContext ctx) {
        super.setWebApplicationContext(ctx);
        this.licensePlateService = (LicensePlateService) ctx.getBean("licensePlateService");
        this.maxDailyLimitedCount =
                ctx.getEnvironment().getProperty("max.daily.limited.count", Integer.TYPE, LicensePlateConstants.DEFAULT_MAX_LIMITED_USE);
    }
    
    public final void setLicensePlateNo(final String licensePlateNo) {
        this.licensePlateNo = licensePlateNo;
    }
    
    public final void setLicensePlateService(final LicensePlateService licensePlateService) {
        this.licensePlateService = licensePlateService;
    }
}
