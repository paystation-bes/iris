/*
 * Copyright @ 2002 Digital Pioneer. All Rights Reserved.
 */
package com.digitalpaytech.rpc;

import java.io.OutputStream;

import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.cxf.helpers.IOUtils;
import org.apache.log4j.Logger;
import org.bouncycastle.util.encoders.Base64;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.digitalpaytech.rpc.ps2.PS2XMLHandler;
import com.digitalpaytech.rpc.support.XMLHandlerFactory;
import com.digitalpaytech.util.KPIConstants;
import com.digitalpaytech.util.xml.XMLUtil;
import com.digitalpaytech.util.xml.XmlHandler;
import com.digitalpaytech.service.EmsPropertiesService;

public class RPCConnectorServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    
    private final static String CONTENT_TYPE = "text/xml";
    
    // private final static XmlRpcServer xmlRpcServer__ = new XmlRpcServer();
    
    private final static Logger logger__ = Logger.getLogger(RPCConnectorServlet.class);
    private WebApplicationContext ctx;
    private boolean isDevMode;
    
    public void init() throws UnavailableException {
        this.ctx = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
        if (this.ctx == null) {
            throw new UnavailableException("Cannot create Spring WebApplicationContextUtils.");
        }
        
        this.isDevMode =
                ((EmsPropertiesService) ctx.getBean("emsPropertiesService")).getPropertyValueAsBoolean(EmsPropertiesService.DEVELOPMENT_MODE, false);
        
        final StringBuilder bdr = new StringBuilder();
        bdr.append("RPCConnectorServlet - Initialized, WebApplicationContext: ").append(ctx).append(", is Development mode? ").append(isDevMode);
        System.out.println(bdr.toString());
    }
    
    public Object execute(byte[] requestString, HttpServletRequest request, HttpServletResponse response) {
        String response_message = null;
        String received_document = null;
        
        try {
            received_document = new String(requestString);
            
            if (logger__.isDebugEnabled()) {
                // xxx should mask passwords etc for opening tag
                String logRequest = XMLUtil.hideAllElementValues(received_document, "CardData");
                logRequest = XMLUtil.hideAllElementValues(logRequest, "CardNumber");
                logRequest = XMLUtil.hideAllElementValues(logRequest, "ExpiryDate");
                // PBPRequest XML-RPC transaction.
                logRequest = XMLUtil.hideAllElementValues(logRequest, "CreditCardData");
                // remove login info coming from BOSS
                logRequest = XMLUtil.hideAllElementValues(logRequest, "LoginData");
                logRequest = XMLUtil.hideAllElementValues(logRequest, "Password");
                logger__.debug("Request: \n<START>\n" + logRequest + "<END>\n");
            }
            
            //WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
            XmlHandler xml_handler = XMLHandlerFactory.getXMLHandler(requestString, ctx);
            if (xml_handler != null) {
                logger__.info("xml_handler name: " + xml_handler.getClass().getName());
                response_message = xml_handler.process(requestString);
                if (response_message.contains("EMSUnavailable") || response_message.contains("EMSLoginFailed") || response_message.contains("CryptoException")
                    || response_message.contains("InvalidType") || response_message.contains("UnsubscribedService")
                    || response_message.contains(KPIConstants.INVALID_STALL_RANGE_STRING) || response_message.contains(KPIConstants.ERROR_INVALID_PAYSTATION)
                    || response_message.contains(KPIConstants.INVALID_REQUEST_STRING) || response_message.contains(KPIConstants.MISSING_REPORT_DATE)
                    || response_message.contains(KPIConstants.START_POSITION_TOO_BIG) || response_message.contains(KPIConstants.RECORD_RETURN_TOO_BIG)) {
                    request.setAttribute(KPIConstants.XMLRPC_STATUS_CODE, HttpServletResponse.SC_BAD_REQUEST);
                }
                String handlerType = ((PS2XMLHandler) xml_handler).getCurrentRequestType();
                request.setAttribute(KPIConstants.XMLRPC_REQUEST, handlerType);
                if (handlerType.equals("Transaction")) {
                    PS2XMLHandler ps2XMLHandler = (PS2XMLHandler) xml_handler;
                    request.setAttribute(KPIConstants.PAYMENT_TYPE, ps2XMLHandler.getPaymentType());
                    request.setAttribute(KPIConstants.IS_STOREFORWARD, ps2XMLHandler.isStoreForward());
                    request.setAttribute(KPIConstants.PERMIT_ISSUE_TYPE, ps2XMLHandler.getPermitIssueType());
                }
            } else {
                logger__.error("+++ xml_handler name: null +++");
            }
        } catch (Exception e) {
            logger__.error("Unable to process XML request", e);
        }
        
        if (response_message != null) {
            if (logger__.isDebugEnabled()) {
                logger__.debug("Response: '" + response_message + "'.");
            }
            return (response_message.getBytes());
        }
        logger__.warn("Response: <null>");
        return (null);
    }
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        //TODO Remove Session
        //		HttpSession session = null;
        
        try {
            //			session = request.getSession(true);
            
            byte[] xml_rpc_result = null;
            
            if (request.isSecure() || isDevMode) {
                String loggedString = null;
                try {
                    byte inputRequest[] = IOUtils.readBytesFromStream(request.getInputStream());
                    String inputString = new String(inputRequest);
                    loggedString = inputString;
                    
                    if (inputString.indexOf("<![CDATA[") > -1) {
                        inputString = inputString.substring(inputString.indexOf("<![CDATA[") + 9, inputString.indexOf("]]>"));
                    } else {
                        if (inputString.indexOf("<string>") > -1)
                            inputString = inputString.substring(inputString.indexOf("<string>") + 8, inputString.indexOf("</string>"));
                        else
                            inputString = inputString.substring(inputString.indexOf("<value>") + 7, inputString.indexOf("</value>"));
                        inputString = StringEscapeUtils.unescapeXml(inputString);
                    }
                    
                    // This execute method is moved from the api dependent
                    // XMLRPCRequestHandler class to the servlet.
                    byte[] responseByteArray = (byte[]) execute(inputString.getBytes(), request, response);
                    String responseString = new String(Base64.encode(responseByteArray));
                    String responseXML = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><methodResponse><params><param><value><base64>" + responseString
                                         + "</base64></value></param></params></methodResponse>";
                    xml_rpc_result = responseXML.getBytes();                                        
                    
                } catch (Exception e) {
                	String transferEncoding = request.getHeader("Transfer-Encoding");
                    String contentLength = String.valueOf(request.getContentLength());
                    String remoteAddress = request.getRemoteAddr();
                    String contentType = request.getContentType();
                    String connectionType = request.getHeader("Connection");
                    String logMsg = "RPCConnectorServlet.doPost() Exception: content-length: " + contentLength +
                    				", remote address: " + remoteAddress + ", content-type: " + contentType + ", connection: " + connectionType;
                    if (transferEncoding != null) {
                    	logMsg += ", transfer-encoding: " + transferEncoding;
                    }
                    
                    logger__.debug(logMsg);
                    logger__.debug("InputString with Exception: " + loggedString);
                    throw e;
                }
                // xml_rpc_result =
                // xmlRpcServer__.execute(request.getInputStream());
            }
            
            if (xml_rpc_result != null) {
                response.setContentType(CONTENT_TYPE);
                response.setContentLength(xml_rpc_result.length);
                
                OutputStream output_stream = response.getOutputStream();
                output_stream.write(xml_rpc_result);
                output_stream.flush();
            }
        } catch (Exception e) {
            logger__.error("Unable to process XML message.", e);
            // Sends an error response to the PS using code 500.
            try {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            } catch (Exception e2) {
                logger__.error("Unable to send error back to PS, ", e2);
            }
        } finally {
            // cleanup each session unless we change XMLRPC Clients to maintain
            // sessions inbetween calls
            //			if (session != null) {
            //				try {
            //					session.invalidate();
            //				} catch (IllegalStateException ise) {
            //					logger__.debug("Unable to invalidate session, as session is already invalidated.");
            //				}
            //			}
        }
    }
}