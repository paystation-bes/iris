package com.digitalpaytech.helper.impl;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.cardprocessing.CardProcessingManager;
import com.digitalpaytech.cardprocessing.cps.impl.ProcessingDestinationDTO;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.data.TransactionData;
import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.domain.Coupon;
import com.digitalpaytech.domain.CreditCardType;
import com.digitalpaytech.domain.CustomerCard;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.ExtensiblePermit;
import com.digitalpaytech.domain.LicencePlate;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MerchantPOS;
import com.digitalpaytech.domain.MobileNumber;
import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.PaymentCash;
import com.digitalpaytech.domain.PaymentSmartCard;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.PermitType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.PreAuthHolding;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.PurchaseCollection;
import com.digitalpaytech.domain.PurchaseCollectionCancelled;
import com.digitalpaytech.domain.PurchaseTax;
import com.digitalpaytech.domain.SingleTransactionCardData;
import com.digitalpaytech.domain.SmsAlert;
import com.digitalpaytech.domain.Tax;
import com.digitalpaytech.domain.UnifiedRate;
import com.digitalpaytech.dto.EmbeddedTxObject;
import com.digitalpaytech.dto.cps.CPSResponse;
import com.digitalpaytech.dto.paystation.CardEaseData;
import com.digitalpaytech.dto.paystation.TransactionDto;
import com.digitalpaytech.dto.rest.paystation.PaystationCancelTransaction;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.exception.PaystationCommunicationException;
import com.digitalpaytech.helper.TransactionHelper;
import com.digitalpaytech.rpc.support.BillCol;
import com.digitalpaytech.rpc.support.CoinCol;
import com.digitalpaytech.service.CreditCardTypeService;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.LinkCardTypeService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.PermitService;
import com.digitalpaytech.service.ProcessorService;
import com.digitalpaytech.service.ProcessorTransactionService;
import com.digitalpaytech.service.SingleTransactionCardDataService;
import com.digitalpaytech.service.TransactionService;
import com.digitalpaytech.service.crypto.CryptoAlgorithmFactory;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.CouponUtil;
import com.digitalpaytech.util.CryptoUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.PaymentTypeIdFinder;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.TransactionFacade;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.crypto.CryptoConstants;
import com.digitalpaytech.util.json.JSON;
import com.digitalpaytech.util.json.JSONConfigurationBean;
import com.fasterxml.jackson.core.type.TypeReference;

@Component("transactionHelper")
public class TransactionHelperImpl implements TransactionHelper {
    
    private static final int LAST_4_DIGITS = 4;
    private static final String TRANSACTION_TYPE_SCRECHARGE_ID = "transaction.type.screcharge.id";
    private static final String PROCESSOR_PREFIX = "processor.";
    private static final Logger LOG = Logger.getLogger(TransactionHelperImpl.class);
    
    private static JSON json;
    
    @Autowired
    private PermitService permitService;
    
    @Autowired
    private ProcessorTransactionService processorTransactionService;
    
    @Autowired
    private SingleTransactionCardDataService singleTransactionCardDataService;
    
    @Autowired
    private TransactionFacade transactionFacade;
    
    @Autowired
    private TransactionService transactionService;
    
    @Autowired
    private CryptoService cryptoService;
    
    @Autowired
    private CardProcessingManager cardProcessingManager;
    
    @Autowired
    private CryptoAlgorithmFactory cryptoAlgorithmFactory;
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    @Autowired
    private CreditCardTypeService creditCardTypeService;
    
    @Autowired
    private ProcessorService processorService;
    
    @Autowired
    private LinkCardTypeService linkCardTypeService;
    
    @Autowired
    private MessageHelper messageHelper;
    
    static {
        final JSONConfigurationBean jsonConfig = new JSONConfigurationBean(true, false, false);
        json = new JSON(jsonConfig);
    }
    
    public final void setPermitService(final PermitService permitService) {
        this.permitService = permitService;
    }
    
    public final void setLinkCardTypeService(final LinkCardTypeService linkCardTypeService) {
        this.linkCardTypeService = linkCardTypeService;
    }
    
    public final void setProcessorTransactionService(final ProcessorTransactionService processorTransactionService) {
        this.processorTransactionService = processorTransactionService;
    }
    
    public final void setSingleTransactionCardDataService(final SingleTransactionCardDataService singleTransactionCardDataService) {
        this.singleTransactionCardDataService = singleTransactionCardDataService;
    }
    
    public final void setTransactionFacade(final TransactionFacade transactionFacade) {
        this.transactionFacade = transactionFacade;
    }
    
    public final void setTransactionService(final TransactionService transactionService) {
        this.transactionService = transactionService;
    }
    
    public final void setCardProcessingManager(final CardProcessingManager cardProcessingManager) {
        this.cardProcessingManager = cardProcessingManager;
    }
    
    public final void setMerchantAccountService(final MerchantAccountService merchantAccountService) {
        this.merchantAccountService = merchantAccountService;
    }
    
    public final void setCreditCardTypeService(final CreditCardTypeService creditCardTypeService) {
        this.creditCardTypeService = creditCardTypeService;
    }
    
    public final void setProcessorService(final ProcessorService processorService) {
        this.processorService = processorService;
    }
    
    public final void setMessageHelper(final MessageHelper messageHelper) {
        this.messageHelper = messageHelper;
    }
    
    @Override
    public final CardRetryTransaction createCardRetryTransaction(final TransactionData transactionData, final TransactionDto txDto)
        throws CryptoException {
        
        final String cardNum;
        final String expDate;
        if (CryptoUtil.isDecryptedCardData(txDto.getCardData())) {
            final int idx = txDto.getCardData().indexOf(Track2Card.TRACK_2_DELIMITER);
            cardNum = txDto.getCardData().substring(0, idx);
            final int expBegin = idx + StandardConstants.CONSTANT_1;
            expDate = txDto.getCardData().substring(expBegin, expBegin + PaystationConstants.FOUR_DIGITS);
        } else {
            cardNum = WebCoreConstants.N_A_STRING;
            expDate = WebCoreConstants.ZERO;
        }
        
        final CardRetryTransaction ctx = new CardRetryTransaction();
        ctx.setPointOfSale(transactionData.getPointOfSale());
        ctx.setPurchasedDate(transactionData.getPurchase().getPurchaseGmt());
        ctx.setTicketNumber(Integer.parseInt(txDto.getNumber()));
        ctx.setLastRetryDate(DateUtil.getCurrentGmtDate());
        ctx.setNumRetries(0);
        
        if (StringUtils.isNotBlank(transactionData.getProcessorTransaction().getCardHash())) {
            ctx.setCardHash(transactionData.getProcessorTransaction().getCardHash());
        } else if (StringUtils.isBlank(transactionData.getProcessorTransaction().getCardHash())
                   && CryptoUtil.isDecryptedCardData(txDto.getCardData())) {
            
            // Re-create 'account number'='expiry date'. 
            final StringBuilder bdr = new StringBuilder();
            bdr.append(cardNum).append(Track2Card.TRACK_2_DELIMITER).append(expDate);
            ctx.setCardHash(this.transactionFacade.getSha1Hash(bdr.toString(), CryptoConstants.HASH_CREDIT_CARD_PROCESSING));
        } else {
            ctx.setCardHash(WebCoreConstants.N_A_STRING);
        }
        
        if (transactionData.getPurchase().isIsOffline()) {
            ctx.setCardRetryTransactionType(this.transactionFacade
                    .findCardRetryTransactionType(WebCoreConstants.CARD_RETRY_TRANSACTION_TYPE_BATCHED));
        } else {
            ctx.setCardRetryTransactionType(this.transactionFacade
                    .findCardRetryTransactionType(WebCoreConstants.CARD_RETRY_TRANSACTION_TYPE_IMPORTED));
        }
        ctx.setCardExpiry(Short.parseShort(expDate));
        ctx.setAmount(transactionData.getProcessorTransaction().getAmount());
        
        String cardType = null;
        final Iterator<PaymentCard> iter = transactionData.getPurchase().getPaymentCards().iterator();
        while (iter.hasNext()) {
            final PaymentCard pc = iter.next();
            if (pc.getCardType() != null && pc.getCardType().getId() == WebCoreConstants.CREDIT_CARD) {
                cardType = pc.getCreditCardType().getName();
            }
        }
        if (StringUtils.isBlank(cardType)) {
            cardType = transactionData.getProcessorTransaction().getCardType();
        }
        
        ctx.setCardType(cardType);
        ctx.setLast4digitsOfCardNumber(cardNum == WebCoreConstants.N_A_STRING ? StandardConstants.CONSTANT_0
                : Short.parseShort(cardNum.substring(cardNum.length() - LAST_4_DIGITS, cardNum.length())));
        ctx.setBadCardHash(cardNum == WebCoreConstants.N_A_STRING ? cardNum
                : this.cryptoAlgorithmFactory.getSha1Hash(cardNum, CryptoConstants.HASH_BAD_CREDIT_CARD));
        ctx.setCreationDate(DateUtil.getCurrentGmtDate());
        ctx.setIgnoreBadCard(false);
        ctx.setLastResponseCode(0);
        ctx.setIsRfid(false);
        
        // Track 2 data will remain in memory and 'decryptedCardData' is a Transient field in CardRetryTransaction.
        ctx.setDecryptedCardData(txDto.getCardData());
        ctx.setCardData(CryptoUtil.isDecryptedCardData(txDto.getCardData())
                ? this.transactionFacade.encryptCardData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL, txDto.getCardData()) : txDto.getCardData());
        return ctx;
    }
    
    @Override
    public final boolean hasDataToProcess(final SmsAlert smsAlert) {
        if (smsAlert.getMobileNumber() != null && smsAlert.getTicketNumber() > 0 && smsAlert.getPermitBeginGmt() != null) {
            return true;
        }
        return false;
    }
    
    @Override
    public final boolean hasDataToProcess(final Set<PaymentCard> paymentCards) {
        if (paymentCards != null && paymentCards.size() > 0) {
            return true;
        }
        return false;
    }
    
    @Override
    public final boolean hasSmartCardDataAndPaid(final TransactionDto transactionDto) {
        if (StringUtils.isNotBlank(transactionDto.getSmartCardData()) && StringUtils.isNotBlank(transactionDto.getSmartCardPaid())
            && WebCoreUtil.convertToBase100IntValue(transactionDto.getSmartCardPaid()) > 0) {
            return true;
        }
        return false;
    }
    
    @Override
    public final boolean hasCustomerCardTypeAuthInternal(final TransactionData transactionData) {
        PaymentCard pCard = null;
        final Iterator<PaymentCard> iter = transactionData.getPurchase().getPaymentCards().iterator();
        while (iter.hasNext()) {
            pCard = iter.next();
            if (pCard.getCustomerCard() != null
                && pCard.getCustomerCard().getCustomerCardType().getAuthorizationType().getId() == WebCoreConstants.AUTH_TYPE_INTERNAL_LIST) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public final boolean hasCustomerCardTypeAuthExternal(final TransactionData transactionData) {
        PaymentCard pCard = null;
        final Iterator<PaymentCard> iter = transactionData.getPurchase().getPaymentCards().iterator();
        while (iter.hasNext()) {
            pCard = iter.next();
            if (pCard.getCustomerCard() != null
                && pCard.getCustomerCard().getCustomerCardType().getAuthorizationType().getId() == WebCoreConstants.AUTH_TYPE_EXTERNAL_SERVER) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public final boolean hasCustomerCardTypeAuthNone(final TransactionData transactionData) {
        PaymentCard pCard = null;
        final Iterator<PaymentCard> iter = transactionData.getPurchase().getPaymentCards().iterator();
        while (iter.hasNext()) {
            pCard = iter.next();
            if (pCard.getCustomerCard() != null
                && pCard.getCustomerCard().getCustomerCardType().getAuthorizationType().getId() == WebCoreConstants.AUTH_TYPE_NONE) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public final TransactionData getTransactionData(final PointOfSale pointOfSale, 
                                                    final TransactionDto txDto, 
                                                    final ProcessingDestinationDTO processDto,
                                                    final String version,
                                                    final boolean isSPT) throws ApplicationException {
        
        final Purchase purchase = new Purchase();
        final Permit permit = new Permit();
        final ProcessorTransaction processorTransaction = new ProcessorTransaction();
        final SmsAlert smsAlert = new SmsAlert();
        final ExtensiblePermit extensiblePermit = new ExtensiblePermit();
        final PosServiceState posServiceState = new PosServiceState();
        
        purchase.setPointOfSale(pointOfSale);
        purchase.setLocation(pointOfSale.getLocation());
        purchase.setCreatedGmt(DateUtil.getCurrentGmtDate());
        
        permit.setLocation(pointOfSale.getLocation());
        
        processorTransaction.setPointOfSale(pointOfSale);
        // SMS Pay by phone
        smsAlert.setPointOfSale(pointOfSale);
        smsAlert.setCustomer(pointOfSale.getCustomer());
        smsAlert.setLocation(pointOfSale.getLocation());
        
        final TransactionData transactionData =
                new TransactionData(pointOfSale, purchase, permit, processorTransaction, smsAlert, extensiblePermit, posServiceState, version);
        
        populateDomainObjects(transactionData, txDto, processDto, isSPT);
        
        return transactionData;
    }
    
    private void populateDomainObjects(final TransactionData transactionData, 
                                       final TransactionDto txDto, 
                                       final ProcessingDestinationDTO processDto, 
                                       final boolean isSPT)
        throws ApplicationException {
        populatePurchaseAndPurchaseCollection(transactionData, txDto, processDto);
        populatePaymentInfo(transactionData, txDto, isSPT);
        populatePermitAndSetPurchase(transactionData, txDto);
        populateProcessorTransaction(transactionData, txDto);
        populateSmsAlertAndTypes(transactionData, txDto);
        populateExtensiblePermit(transactionData, txDto);
        populatePaymentSmartCard(transactionData, txDto);
        if (transactionData.isCancelledTransaction()) {
            removeCardData(transactionData);
        }
    }
    
    private void setEmbeddedTransactionTypeAndObject(final TransactionData transactionData, final TransactionDto txDto) {
        final MerchantAccount ma = this.merchantAccountService.findByPointOfSaleIdAndCardTypeId(transactionData.getPointOfSale().getId(),
                                                                                                WebCoreConstants.CARD_TYPE_CREDIT_CARD);
        txDto.setPaystationCommAddress(transactionData.getPointOfSale().getSerialNumber());
        transactionData.getProcessorTransaction().setMerchantAccount(ma);
        if (StringUtils.isNotBlank(txDto.getCardEaseData()) && ma != null && ma.getProcessor() != null) {
            transactionData.setEMV(this.processorService.findProcessor(ma.getProcessor().getId()).getIsEMV());
        } else if (StringUtils.isNotBlank(txDto.getCPSResponse())) {
            transactionData.setCoreCPS(true);
        }
        
        if (hasCardEaseDataOrCPSResponseString(transactionData.isEMV(), txDto.getCardEaseData())
            || hasCardEaseDataOrCPSResponseString(transactionData.isCoreCPS(), txDto.getCPSResponse())) {
            txDto.setEmbeddedTxObject(createEmbeddedTxObject(txDto));
        }
    }
    
    private void removeCardData(final TransactionData transactionData) {
        
        if (transactionData.getPurchase().getPaymentType().getId() != WebCoreConstants.PAYMENT_TYPE_CASH_SMART
            && transactionData.getPurchase().getPaymentType().getId() != WebCoreConstants.PAYMENT_TYPE_SMART_CARD) {
            transactionData.getPurchase().setCardData(null);
            transactionData.getExtensiblePermit().setCardData(null);
            transactionData.getProcessorTransaction().setProcessorTransactionType(this.processorTransactionService
                    .getProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CANCELLED));
            transactionData.getProcessorTransaction().setPreAuth(null);
        }
    }
    
    /*
     * EMS 6.3.8 Aura Technical Design - Technical Design (http://confluence:8080/display/EMS/Technical+Design)
     * "Transaction coming from pay station for Settlement now contains: track2data in <CreditCardData>, and Mobile phone in <MobileNumber> tag."
     */
    private void populateSmsAlertAndTypes(final TransactionData transactionData, final TransactionDto txDto) {
        if (StringUtils.isNotBlank(txDto.getMobileNumber()) && transactionData.getPurchase().getCardPaidAmount() > 0) {
            updateTransactionTypeAndPermitTypesForSms(transactionData);
            
            if (StringUtils.isNotBlank(txDto.getAddTimeNum())) {
                transactionData.getSmsAlert().setAddTimeNumber(Integer.parseInt(txDto.getAddTimeNum()));
            }
            transactionData.getSmsAlert().setLicencePlate(txDto.getLicensePlateNo());
            transactionData.getSmsAlert().setPaystationSettingName(txDto.getLotNumber());
            transactionData.getSmsAlert().setMobileNumber(transactionData.getPermit().getMobileNumber());
            transactionData.getSmsAlert().setPermitBeginGmt(transactionData.getPermit().getPermitBeginGmt());
            transactionData.getSmsAlert().setPermitExpireGmt(transactionData.getPermit().getPermitExpireGmt());
            transactionData.getSmsAlert().setTicketNumber(Integer.parseInt(txDto.getNumber()));
            if (StringUtils.isNotBlank(txDto.getStallNumber())) {
                transactionData.getSmsAlert()
                        .setSpaceNumber(Integer.parseInt(txDto.getStallNumber()) < 0 ? 0 : Integer.parseInt(txDto.getStallNumber()));
            }
        }
    }
    
    /*
     * EMS 6.3.8 Aura Technical Design - Technical Design (http://confluence:8080/display/EMS/Technical+Design)
     * "Transaction coming from pay station for Settlement now contains: track2data in <CreditCardData>, and Mobile phone in <MobileNumber> tag."
     */
    private void populateExtensiblePermit(final TransactionData transactionData, final TransactionDto txDto) {
        if (StringUtils.isNotBlank(txDto.getMobileNumber())) {
            transactionData.getExtensiblePermit().setOriginalPermit(transactionData.getPermit());
            
            transactionData.getExtensiblePermit().setMobileNumber(transactionData.getPermit().getMobileNumber());
            transactionData.getExtensiblePermit().setCardData(txDto.getCreditCardData());
            transactionData.getExtensiblePermit().setPermitBeginGmt(transactionData.getPermit().getPermitBeginGmt());
            transactionData.getExtensiblePermit().setPermitExpireGmt(transactionData.getPermit().getPermitExpireGmt());
            if (StringUtils.isNotBlank(txDto.getPaidByRFID()) && txDto.getPaidByRFID().trim().equals(WebCoreConstants.RFID_DATA)) {
                transactionData.getExtensiblePermit().setIsRfid(true);
            }
            
            // Sets references between ExtensiblePermit and Permit.
            transactionData.getPermit().getExtensiblePermits().add(transactionData.getExtensiblePermit());
        }
    }
    
    /**
     * PaymentSmartCard table is new and only meant for storing SmartCard data such as:
     * <SmartCardData>, <SmartCardType>.
     * It has no parent-child relation with any table and only reference PurchaseId, thus, store an instance in Purchase object as 'transient'
     * property and TransactionService (determineTransactionCardFields) will call save if necessary.
     */
    private void populatePaymentSmartCard(final TransactionData transactionData, final TransactionDto txDto) {
        if (StringUtils.isNotBlank(txDto.getSmartCardData())) {
            final PaymentSmartCard paymentSmartCard = new PaymentSmartCard();
            paymentSmartCard.setPurchase(transactionData.getPurchase());
            paymentSmartCard.setSmartCardData(txDto.getSmartCardData());
            paymentSmartCard.setSmartCardType(txDto.getSmartCardType());
            transactionData.getPurchase().setPaymentSmartCard(paymentSmartCard);
        }
    }
    
    private CreditCardType findCreditCardType(final MerchantAccount merchantAccount, final String cardType, final String cardData)
        throws ApplicationException {
        if (CardProcessingUtil.isNotNullAndIsLink(merchantAccount)) {
            return this.creditCardTypeService.getCreditCardTypeByName(cardType);
        } else {
            return this.transactionFacade.findCreditCardType(cardData);
        }
    }
    
    private void populatePaymentInfo(final TransactionData transactionData, final TransactionDto txDto, final boolean isSPT)
        throws ApplicationException {
        int paymentTypeId = transactionData.getPurchase().getPaymentType().getId();
        // Credit Card, Smart Card or Value Card
        final int cardTypeId = getCardTypeId(txDto, paymentTypeId);
        
        // IRIS-3528 check the card type with Card Type service
        final MerchantAccount merchantAccount =
                this.merchantAccountService.findByPOSAndCardTypeAndNotDeletedAndMerchantStatusEnabled(transactionData.getPointOfSale().getId(),
                                                                                                      WebCoreConstants.CARD_TYPE_CREDIT_CARD);
        
        String decryptCardData = null;
        if (hasSmartCardDataAndPaid(txDto)) {
            // Smart Card card data is in <SmartCardData> field and <CardData> is blank.
            decryptCardData = txDto.getSmartCardData();
            
        } else if (cardTypeId == WebCoreConstants.VALUE_CARD || CardProcessingUtil.isNotNullAndIsLink(merchantAccount)) {
            // Value Card card data is in plain text in <CardData> field. <- No, not always !
            // Link Credit Card card data does not need decrypt
            decryptCardData = txDto.getCardData();
            
        } else if (StringUtils.isNotBlank(txDto.getCardData())
                   && txDto.getCardData().length() > CardProcessingConstants.NOT_ENCRYPTED_CARD_DATA_MAX_LENGTH) {
            decryptCardData = this.transactionFacade.decryptCardData(txDto.getCardData());
        }
        
        if (cardTypeId != WebCoreConstants.N_A) {
            // Looks up CustomerCardType and CustomerCard if it's Value Card.
            CustomerCardType cardType = null;
            CustomerCard custCard = null;
            
            final PaymentCard payCard = new PaymentCard();
            payCard.setPurchase(transactionData.getPurchase());
            
            if (cardTypeId == WebCoreConstants.VALUE_CARD) {
                // If it's for Post Auth, retrieve data from PreAuth table.
                if (transactionData.getPreAuth() == null) {
                    transactionData.setPreAuth(findApprovedPreAuthIfExists(txDto.getEmsPreAuthId()));
                }
                
                if ((decryptCardData == null) && (transactionData.getPreAuth() != null)) {
                    decryptCardData = transactionData.getPreAuth().getCardData();
                }
                
                if (!(StringUtils.isBlank(decryptCardData) || WebCoreUtil.checkIfAllDigits(decryptCardData)
                      || WebCoreUtil.escapeCharsAndCheckIfAllDigits(getEscapeChars(), decryptCardData))) {
                    decryptCardData = this.transactionFacade.decryptCardData(decryptCardData);
                }
                
                cardType =
                        this.transactionFacade.findCustomerCardType(transactionData.getPurchase().getCustomer().getId(), cardTypeId, decryptCardData);
                if (cardType == null) {
                    final StringBuilder bdr = new StringBuilder();
                    bdr.append("Unable to find CustomerCardType for Payment Type Card, PointOfSale id: ")
                            .append(transactionData.getPointOfSale().getId());
                    bdr.append(", serialNumber: ").append(transactionData.getPointOfSale().getSerialNumber());
                    bdr.append(", customerId: ").append(transactionData.getPointOfSale().getCustomer().getId());
                    bdr.append(", ticketNumber: ").append(transactionData.getPurchase().getPurchaseNumber());
                    bdr.append(", purchasedDate: ").append(transactionData.getPurchase().getPurchaseGmt());
                    bdr.append(", masked credit card: ");
                    if (StringUtils.isNotBlank(decryptCardData)) {
                        bdr.append(CardProcessingUtil.getFirst6Last4DigitsOfAccountNumber(decryptCardData, WebCoreConstants.EQUAL_SIGN.charAt(0)));
                    }
                    LOG.error(bdr.toString());
                    if (isSPT) {
                        throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_CREDIT_CARD_INVALID_CARD);
                    } else {
                        /*
                         * Place more information in the debug, set PaymentType to Unknown and return to the caller. Purchase and Permit records
                         * will be created and since it's not a recognized card data, Purchase - PaymentTypeId will be 10, UNKNOWN.
                         */
                        transactionData.getPurchase().setPaymentType(this.transactionFacade.findPaymentType(WebCoreConstants.PAYMENT_TYPE_UNKNOWN));
                        return;
                    }
                }
                if (decryptCardData.indexOf('=') > -1) {
                    decryptCardData = decryptCardData.substring(0, decryptCardData.indexOf('='));
                }
                
                custCard = this.transactionFacade.findCustomerCard(transactionData.getPurchase().getCustomer().getId(), decryptCardData);
                if (custCard == null) {
                    // Authorization type is 1 (Internal List) but couldn't find associated CustomerCard record, throws an error.
                    if (cardType.getAuthorizationType().getId() == WebCoreConstants.AUTH_TYPE_INTERNAL_LIST) {
                        LOG.error("CustomerCardType.authorizationType is 1 (Internal List), invalid!");
                        if (isSPT) {
                            throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_CREDIT_CARD_BAD_CARD);
                        } else {
                            throw new ApplicationException(CardProcessingConstants.BAD_CARD_STRING);
                        }
                    }
                    
                    custCard = new CustomerCard();
                    custCard.setAddedGmt(DateUtil.getCurrentGmtDate());
                    custCard.setCardNumber(decryptCardData);
                    custCard.setLocation(transactionData.getPointOfSale().getLocation());
                    custCard.setIsDeleted(false);
                    custCard.setIsForNonOverlappingUse(false);
                    custCard.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
                    custCard.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
                    custCard.setPointOfSale(transactionData.getPointOfSale());
                    custCard.setNumberOfUses((short) 0);
                } else if (custCard.getMaxNumberOfUses() != null && custCard.getMaxNumberOfUses() > 0) {
                    custCard.setNumberOfUses((short) ((custCard.getNumberOfUses() == null ? 0 : custCard.getNumberOfUses()) + 1));
                }
                
                custCard.setCustomerCardType(cardType);
                
                // 0 - N/A
                payCard.setCreditCardType(this.transactionFacade.findNonCreditCardType());
                
            } else if (cardTypeId == WebCoreConstants.CREDIT_CARD) {
                if (StringUtils.isNotBlank(decryptCardData)) {
                    // It's for Store & Forward or Batch.
                    payCard.setCreditCardType(findCreditCardType(merchantAccount, transactionData.getCardType(), decryptCardData));
                    
                    txDto.setCardData(decryptCardData);
                    transactionData.setStoreForward(true);
                } else if (StringUtils.isBlank(decryptCardData) && StringUtils.isNotBlank(txDto.getEmsPreAuthId())) {
                    // If it's for Post Auth, retrieve data from PreAuth table.
                    if (transactionData.getPreAuth() == null) {
                        PreAuth preAuth = findApprovedPreAuthIfExists(txDto.getEmsPreAuthId());
                        if (preAuth == null) {
                            preAuth = findApprovedPreAuthHoldingIfExists(txDto.getEmsPreAuthId(), txDto.getCardAuthorizationId());
                            if (preAuth != null) {
                                transactionData.setForRecoverable(true);
                            }
                        }
                        transactionData.setPreAuth(preAuth);
                    }
                    
                    if (transactionData.getPreAuth() == null) {
                        return;
                    } else {
                        if (transactionData.getPreAuth().isIsRfid()) {
                            if (transactionData.getPurchase().getPaymentType().getId() == WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_SWIPE) {
                                transactionData.getPurchase()
                                        .setPaymentType(this.transactionFacade.findPaymentType(WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CL));
                                paymentTypeId = WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CL;
                            } else if (transactionData.getPurchase().getPaymentType().getId() == WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_SWIPE) {
                                transactionData.getPurchase().setPaymentType(this.transactionFacade
                                        .findPaymentType(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CONTACTLESS));
                                paymentTypeId = WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CONTACTLESS;
                            }
                        }
                    }
                    payCard.setCreditCardType(this.transactionFacade.findCreditCardTypeByName(transactionData.getPreAuth().getCardType()));
                } else if (!transactionData.isEMVOrCoreCPS()) {
                    /*
                     * The transaction xml doesn't contain enough information to create PaymentCard. Set PaymentCard & PaymentCash to null.
                     */
                    transactionData.getPurchase().getPaymentCards().clear();
                    transactionData.getPurchase().setPaymentCashs(null);
                    return;
                }
            } else if (cardTypeId == WebCoreConstants.SMART_CARD) {
                payCard.setCreditCardType(this.transactionFacade.findNonCreditCardType());
            }
            
            if (transactionData.isEMV()) {
                final CardEaseData easeData = (CardEaseData) txDto.getEmbeddedTxObject();
                payCard.setCardLast4digits(StringUtils.isBlank(easeData.getPan()) ? StandardConstants.CONSTANT_0
                        : Short.parseShort(easeData.getPan()));
                final CreditCardType cct = this.creditCardTypeService.getCreditCardTypeByName(easeData.getAcquirer(), true);
                payCard.setCreditCardType(cct);
            } else if (transactionData.isCoreCPS()) {
                final CPSResponse cpsResp = (CPSResponse) txDto.getEmbeddedTxObject();
                payCard.setCardLast4digits(txDto.getEmbeddedTxObject().isCPSStoreForward() ? 0 : Short.parseShort(cpsResp.getLast4Digits()));
                payCard.setCreditCardType(txDto.getEmbeddedTxObject().isCPSStoreForward() ? this.creditCardTypeService.getCreditCardTypeById(0)
                        : this.creditCardTypeService.getCreditCardTypeByName(cpsResp.getCardType(), true));
            } else if (!CardProcessingUtil.isNotNullAndIsLink(transactionData.getProcessorTransaction().getMerchantAccount())) {
                payCard.setCardLast4digits(findCardLast4Digits(transactionData, cardTypeId, decryptCardData));
            }
            payCard.setCustomerCard(custCard);
            payCard.setAmount(transactionData.getPurchase().getCardPaidAmount());
            payCard.setIsApproved(true);
            payCard.setIsUploadedFromBoss(transactionData.isUploadFromBoss());
            payCard.setProcessorTransaction(transactionData.getProcessorTransaction());
            if (cardTypeId == WebCoreConstants.SMART_CARD) {
                payCard.setCardProcessedGmt(transactionData.getPurchase().getPurchaseGmt());
                payCard.setCardType(this.transactionFacade.findCardType(WebCoreConstants.SMART_CARD));
            } else {
                payCard.setCardProcessedGmt(DateUtil.getCurrentGmtDate());
                payCard.setCardType(this.transactionFacade.findCardType(cardTypeId));
            }
            // PaymentCard.setMerchantAccount is call in TransactionServiceImpl, determineTransactionCardFields method.
            
            final Set<PaymentCard> set = new HashSet<PaymentCard>(1);
            set.add(payCard);
            transactionData.getPurchase().setPaymentCards(set);
            
            // Set CardType to ProcessorTransaction.
            transactionData.getProcessorTransaction().setCardType(payCard.getCardType().getName());
        }
        
        // Cash - for separate collections.
        if (paymentTypeId == WebCoreConstants.PAYMENT_TYPE_CASH || paymentTypeId == WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_SWIPE
            || paymentTypeId == WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CL || paymentTypeId == WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CH
            || paymentTypeId == WebCoreConstants.PAYMENT_TYPE_CASH_SMART || paymentTypeId == WebCoreConstants.PAYMENT_TYPE_VALUE_CARD) {
            
            final Set<PaymentCash> cashSet = new HashSet<PaymentCash>();
            if (StringUtils.isNotBlank(txDto.getBillCol())) {
                final BillCol billCol = new BillCol(txDto.getBillCol());
                if (billCol.getCount1() > 0) {
                    cashSet.add(createPaymentCash(transactionData, billCol.getAmount1(), billCol.getCount1(), false));
                }
                if (billCol.getCount2() > 0) {
                    cashSet.add(createPaymentCash(transactionData, billCol.getAmount2(), billCol.getCount2(), false));
                }
                if (billCol.getCount5() > 0) {
                    cashSet.add(createPaymentCash(transactionData, billCol.getAmount5(), billCol.getCount5(), false));
                }
                if (billCol.getCount10() > 0) {
                    cashSet.add(createPaymentCash(transactionData, billCol.getAmount10(), billCol.getCount10(), false));
                }
                if (billCol.getCount20() > 0) {
                    cashSet.add(createPaymentCash(transactionData, billCol.getAmount20(), billCol.getCount20(), false));
                }
                if (billCol.getCount50() > 0) {
                    cashSet.add(createPaymentCash(transactionData, billCol.getAmount50(), billCol.getCount50(), false));
                }
            }
            if (StringUtils.isNotBlank(txDto.getCoinCol())) {
                final CoinCol coinCol = new CoinCol(txDto.getCoinCol());
                if (coinCol.getCount005() > 0) {
                    cashSet.add(createPaymentCash(transactionData, coinCol.getAmount005(), coinCol.getCount005(), true));
                }
                if (coinCol.getCount010() > 0) {
                    cashSet.add(createPaymentCash(transactionData, coinCol.getAmount010(), coinCol.getCount010(), true));
                }
                if (coinCol.getCount025() > 0) {
                    cashSet.add(createPaymentCash(transactionData, coinCol.getAmount025(), coinCol.getCount025(), true));
                }
                if (coinCol.getCount100() > 0) {
                    cashSet.add(createPaymentCash(transactionData, coinCol.getAmount100(), coinCol.getCount100(), true));
                }
                if (coinCol.getCount200() > 0) {
                    cashSet.add(createPaymentCash(transactionData, coinCol.getAmount200(), coinCol.getCount200(), true));
                }
            }
            transactionData.getPurchase().setPaymentCashs(cashSet);
            
        }
    }
    
    private boolean hasCardEaseDataOrCPSResponseString(final boolean isEMVOrCoreCPS, final String cardEaseDataOrCPSResponse) {
        return isEMVOrCoreCPS && StringUtils.isNotBlank(cardEaseDataOrCPSResponse);
    }
    
    private Short findCardLast4Digits(final TransactionData transactionData, final int cardTypeId, final String decryptedCardData) {
        if (cardTypeId == WebCoreConstants.SMART_CARD) {
            return 0;
        }
        if (transactionData.getPreAuth() != null) {
            return transactionData.getPreAuth().getLast4digitsOfCardNumber();
        }
        if (StringUtils.isBlank(decryptedCardData)) {
            return 0;
        }
        if (decryptedCardData.length() < LAST_4_DIGITS) {
            return Short.parseShort(decryptedCardData);
        } else {
            int accountNumberLength = decryptedCardData.indexOf(Track2Card.TRACK_2_DELIMITER);
            if (accountNumberLength < 0) {
                accountNumberLength = decryptedCardData.length();
            }
            if (accountNumberLength > LAST_4_DIGITS) {
                return Short.parseShort(decryptedCardData.substring(accountNumberLength - LAST_4_DIGITS, accountNumberLength));
            }
            return Short.parseShort(decryptedCardData.substring(0, accountNumberLength));
        }
    }
    
    private PaymentCash createPaymentCash(final TransactionData transactionData, final int denominationAmount, final int quantity,
        final boolean isCoin) {
        final PaymentCash payCash = new PaymentCash();
        payCash.setDenominationType(this.transactionFacade.findDenominationType(isCoin, denominationAmount));
        payCash.setQuantity(quantity);
        // Coins are 'accepted'.
        payCash.setIsAcceptOrDispense(0);
        payCash.setPurchase(transactionData.getPurchase());
        return payCash;
    }
    
    private void populateProcessorTransaction(final TransactionData transactionData, final TransactionDto txDto) throws ApplicationException {
        try {
            transactionData.getProcessorTransaction().setPurchasedDate(DateUtil.convertFromColonDelimitedDateString(txDto.getPurchasedDate()));
        } catch (InvalidDataException ide) {
            LOG.error(ide);
            throw new ApplicationException(PaystationConstants.RESPONSE_MESSAGE_EMS_UNAVAILABLE);
        }
        
        if (transactionData.getPurchase().getPaymentCards() != null && hasCustomerCardTypeAuthInternal(transactionData)) {
            transactionData.getProcessorTransaction().setAuthorizationNumber(null);
            
        } else {
            if (transactionData.isForRecoverable()) {
                transactionData.getProcessorTransaction()
                        .setAuthorizationNumber(ProcessorTransaction.formatAuthAndPreAuth(txDto.getCardAuthorizationId(), txDto.getEmsPreAuthId()));
            } else {
                transactionData.getProcessorTransaction().setAuthorizationNumber(txDto.getCardAuthorizationId());
            }
        }
        transactionData.getProcessorTransaction().setAmount(WebCoreUtil.convertToBase100IntValue(txDto.getCardPaid()));
        transactionData.getProcessorTransaction().setTicketNumber(Integer.parseInt(txDto.getNumber()));
        // Don't set PreAuth if it's a recoverable transaction.
        if (!transactionData.isForRecoverable()) {
            transactionData.getProcessorTransaction().setPreAuth(transactionData.getPreAuth());
        }
        transactionData.getProcessorTransaction().setCardHash(WebCoreConstants.EMPTY_STRING);
        transactionData.getProcessorTransaction().setProcessorTransactionId(WebCoreConstants.EMPTY_STRING);
        transactionData.getProcessorTransaction().setReferenceNumber(WebCoreConstants.EMPTY_STRING);
        transactionData.getProcessorTransaction().setCreatedGmt(DateUtil.getCurrentGmtDate());
        if (StringUtils.isNotBlank(txDto.getPaidByRFID()) && txDto.getPaidByRFID().trim().equals(WebCoreConstants.RFID_DATA)) {
            transactionData.getProcessorTransaction().setIsRfid(true);
        }
        
        if (transactionData.isEMV()) {
            transactionData.setAuthorizationNumber(txDto.getCardAuthorizationId());
        } else {
            transactionData.setAuthorizationNumber(txDto.getAuthorizationNumber());
        }
        
        // ProcessorTransaction cardType is set in method 'populatePaymentInfo'. 
    }
    
    private Permit locateOriginalPermit(final TransactionData transactionData, final TransactionDto txDto) {
        Permit originalPermit = null;
        
        final Integer transactionTypeId = transactionData.getPurchase().getTransactionType().getId();
        if (transactionTypeId == ReportingConstants.TRANSACTION_TYPE_ADDTIME
            || transactionTypeId == ReportingConstants.TRANSACTION_TYPE_EXTEND_BY_PHONE_ADDTIME_VALUE) {
            
            originalPermit = this.transactionFacade
                    .findLatestOriginalPermit(transactionData.getPointOfSale().getLocation().getId(), transactionData.getPermit().getSpaceNumber(),
                                              transactionData.getPermit().getAddTimeNumber(),
                                              txDto.getLicensePlateNo() == null ? StandardConstants.STRING_EMPTY_STRING : txDto.getLicensePlateNo());
            
            transactionData.getPermit().setPermit(originalPermit);
        }
        
        return originalPermit;
    }
    
    private void populatePermitAndSetPurchase(final TransactionData transactionData, final TransactionDto txDto) throws ApplicationException {
        final PermitType permitType = this.transactionFacade.findPermitType(txDto.getType());
        // If transaction type is 'SCRecharge', 'Cancelled' or 'Test', it's not a permit. Sets Permit object to null and return.
        if (permitType == null) {
            transactionData.setPermit(null);
            return;
        }
        
        // Sets references between LicencePlate and Permit.
        final LicencePlate licence = getLicencePlateIfExists(txDto.getLicensePlateNo());
        if (licence != null) {
            transactionData.getPermit().setLicencePlate(licence);
        }
        transactionData.getPermit().setPermitType(permitType);
        transactionData.getPermit().setMobileNumber(findMobileNumberIfExists(txDto.getMobileNumber()));
        transactionData.getPermit().setPermitIssueType(this.transactionFacade.findPermitIssueType(txDto.getLicensePlateNo(), txDto.getStallNumber()));
        if (StringUtils.isNotBlank(txDto.getAddTimeNum())) {
            transactionData.getPermit().setAddTimeNumber(Integer.parseInt(txDto.getAddTimeNum()));
        }
        if (StringUtils.isNotBlank(txDto.getStallNumber())) {
            transactionData.getPermit().setSpaceNumber(Integer.parseInt(txDto.getStallNumber()) < 0 ? 0 : Integer.parseInt(txDto.getStallNumber()));
        } else {
            transactionData.getPermit().setSpaceNumber(0);
        }
        
        // If transaction type is 'AddTime', find original permit.
        locateOriginalPermit(transactionData, txDto);
        
        try {
            final Date purGmt = DateUtil.changeTimeZone(DateUtil.convertFromColonDelimitedDateString(txDto.getPurchasedDate()), DateUtil.GMT);
            final Date expGmt = DateUtil.changeTimeZone(DateUtil.convertFromColonDelimitedDateString(txDto.getExpiryDate()), DateUtil.GMT);
            transactionData.getPermit().setPermitBeginGmt(purGmt);
            transactionData.getPermit().setPermitExpireGmt(expGmt);
            transactionData.getPermit().setPermitOriginalExpireGmt(expGmt);
            if (transactionData.getPermit().getPermit() != null) {
                transactionData.getPermit().getPermit().setPermitExpireGmt(expGmt);
            }
        } catch (InvalidDataException ide) {
            LOG.error(ide);
            throw new ApplicationException(PaystationConstants.RESPONSE_MESSAGE_EMS_UNAVAILABLE);
        }
        
        // Purchase number = permit number = ticket number (<number>)
        transactionData.getPermit().setPermitNumber(Integer.parseInt(txDto.getNumber()));
        
        // Sets references between Purchase and Permit.
        transactionData.getPermit().setPurchase(transactionData.getPurchase());
        
        final Set<Permit> permits = new HashSet<Permit>(1);
        permits.add(transactionData.getPermit());
        transactionData.getPurchase().setPermits(permits);
    }
    
    private void populatePurchaseAndPurchaseCollection(final TransactionData transactionData, 
                                                       final TransactionDto txDto,
                                                       final ProcessingDestinationDTO processDto)
        throws ApplicationException {
        setEmbeddedTransactionTypeAndObject(transactionData, txDto);
        
        final Purchase purchase = transactionData.getPurchase();
        Date purchasedGmt = null;
        try {
            purchasedGmt = DateUtil.changeTimeZone(DateUtil.convertFromColonDelimitedDateString(txDto.getPurchasedDate()), DateUtil.GMT);
            purchase.setPurchaseGmt(purchasedGmt);
        } catch (InvalidDataException ie) {
            LOG.error(ie);
            throw new ApplicationException(PaystationConstants.RESPONSE_MESSAGE_EMS_UNAVAILABLE);
        }
        
        try {
            Date transactionGmt = null;
            if (txDto.getTransDateTime() != null && !StandardConstants.STRING_EMPTY_STRING.equals(txDto.getTransDateTime())) {
                // Wrong format already released in some psApp versions
                if (txDto.getTransDateTime().indexOf(StandardConstants.STRING_EMPTY_SPACE) > -1) {
                    transactionGmt = DateUtil.changeTimeZone(DateUtil.convertFromReportOutputFormatWithNoTimeZoneDateString(txDto.getTransDateTime()),
                                                             DateUtil.GMT);
                } else {
                    transactionGmt = DateUtil.changeTimeZone(DateUtil.convertFromColonDelimitedDateString(txDto.getTransDateTime()), DateUtil.GMT);
                }
                purchase.setTransactionGmt(transactionGmt);
            }
        } catch (InvalidDataException ie) {
            //Ignore if there is an error
            LOG.error(ie);
        }
        
        // Cash
        purchase.setCashPaidAmount(WebCoreUtil.convertToBase100IntValue(txDto.getCashPaid()));
        BillCol billCol = null;
        CoinCol coinCol = null;
        if (StringUtils.isNotBlank(txDto.getBillCol())) {
            billCol = new BillCol(txDto.getBillCol());
            purchase.setBillPaidAmount(billCol.getBillDollars());
            // If billcol is not black numberBillsAccepted will be blank
            txDto.setNumberBillsAccepted(String.valueOf(billCol.getBillCount()));
        }
        if (StringUtils.isNotBlank(txDto.getCoinCol())) {
            coinCol = new CoinCol(txDto.getCoinCol());
            purchase.setCoinPaidAmount(coinCol.getCoinDollars());
            // If coincol is not black numberCoinsAccepted will be blank
            txDto.setNumberCoinsAccepted(String.valueOf(coinCol.getCoinCount()));
        }
        
        // CardPaidAmount is for all different cards - credit / smart / value cards.
        if (WebCoreUtil.convertToBase100IntValue(txDto.getSmartCardPaid()) > 0) {
            purchase.setCardPaidAmount(WebCoreUtil.convertToBase100IntValue(txDto.getSmartCardPaid()));
        } else {
            purchase.setCardPaidAmount(WebCoreUtil.convertToBase100IntValue(txDto.getCardPaid()));
        }
        
        purchase.setCustomer(transactionData.getPointOfSale().getCustomer());
        purchase.setChangeDispensedAmount(WebCoreUtil.convertToBase100IntValue(txDto.getChangeDispensed()));
        purchase.setChargedAmount(WebCoreUtil.convertToBase100IntValue(txDto.getChargedAmount()));
        purchase.setBillCount(Integer.parseInt(txDto.getNumberBillsAccepted()));
        purchase.setCoinCount(Integer.parseInt(txDto.getNumberCoinsAccepted()));
        purchase.setCoupon(findOrCreateCoupon(transactionData, txDto, txDto.getCouponNumber(), purchasedGmt));
        purchase.setCreatedGmt(DateUtil.getCurrentGmtDate());
        purchase.setExcessPaymentAmount(WebCoreUtil.convertToBase100IntValue(txDto.getExcessPayment()));
        purchase.setIsRefundSlip(WebCoreUtil.convertToBoolean(txDto.getIsRefundSlip()));
        purchase.setIsOffline(transactionData.isOffline());
        // Create a new PaystationSetting record if couldn't find it and send warning email.
        purchase.setPaystationSetting(this.transactionFacade.findOrCreatePaystationSetting(transactionData.getPointOfSale().getCustomer(),
                                                                                           txDto.getLotNumber()));
        // Base 100 already
        purchase.setOriginalAmount(Integer.parseInt(txDto.getOriginalAmount()));
        purchase.setTransactionType(this.transactionFacade.findTransactionType(txDto.getType()));
        purchase.setPaymentType(this.transactionFacade.findPaymentType(getPaymentTypeId(transactionData, txDto, processDto)));
        // Purchase number = permit number = ticket number (<number>)
        purchase.setPurchaseNumber(Integer.parseInt(txDto.getNumber()));
        
        purchase.setPurchaseTaxes(createPurchaseTaxes(transactionData, txDto));
        
        final UnifiedRate newUnifiedRate = getUnifiedRate(transactionData, txDto.getType(), txDto.getRateName());
        purchase.setUnifiedRate(newUnifiedRate);
        
        // Base 100 already
        purchase.setRateAmount(Integer.parseInt(txDto.getRateValue()));
        purchase.setRateRevenueAmount(calculateRevenue(transactionData, txDto));
        // cardData is a transient field.
        
        transactionData.setCancelledTransaction(this.transactionFacade.findTransactionType(txDto.getType())
                .getId() == ReportingConstants.TRANSACTION_TYPE_CANCELLED);
        
        if (hasSmartCardDataAndPaid(txDto)) {
            purchase.setCardData(txDto.getSmartCardData());
        } else if (StringUtils.isEmpty(purchase.getCardData())) {
            purchase.setCardData(txDto.getCardData());
        }
        
        if (hasDataToProcess(purchase, txDto.getNumberBillsAccepted(), txDto.getNumberCoinsAccepted())) {
            final PurchaseCollection purchaseCollection = new PurchaseCollection();
            purchaseCollection.setPurchaseGmt(purchase.getPurchaseGmt());
            
            if (billCol != null) {
                purchaseCollection.setBillAmount(billCol.getBillDollars());
                purchaseCollection.setBillCount((byte) billCol.getBillCount());
            } else {
                purchaseCollection.setBillAmount(purchase.getBillPaidAmount());
                purchaseCollection.setBillCount((byte) purchase.getBillCount());
            }
            if (coinCol != null) {
                purchaseCollection.setCoinAmount(coinCol.getCoinDollars());
                purchaseCollection.setCoinCount((byte) coinCol.getCoinCount());
            } else {
                purchaseCollection.setCoinAmount(purchase.getCoinPaidAmount());
                purchaseCollection.setCoinCount((byte) purchase.getCoinCount());
            }
            if (purchase.getCashPaidAmount() > 0) {
                purchaseCollection.setCashAmount(purchase.getCashPaidAmount());
            }
            
            // Sets Purchase, PointOfSale reference to purchaseCollection.
            purchaseCollection.setPurchase(purchase);
            purchaseCollection.setPointOfSale(transactionData.getPointOfSale());
            
            final Set<PurchaseCollection> set = new HashSet<PurchaseCollection>(1);
            set.add(purchaseCollection);
            purchase.setPurchaseCollections(set);
        }
    }
    
    private void updateTransactionTypeAndPermitTypesForSms(final TransactionData transactionData) {
        // TransactionType
        if (transactionData.getPurchase().getTransactionType().getId() == this.transactionService
                .getMappingPropertiesValue("transaction.type.regular.id")) {
            transactionData.getPurchase().setTransactionType(this.transactionFacade
                    .findTransactionTypeById(this.transactionService.getMappingPropertiesValue("transaction.type.regular.ebp.id")));
        } else if (transactionData.getPurchase().getTransactionType().getId() == this.transactionService
                .getMappingPropertiesValue("transaction.type.addtime.id")) {
            transactionData.getPurchase().setTransactionType(this.transactionFacade
                    .findTransactionTypeById(this.transactionService.getMappingPropertiesValue("transaction.type.addtime.ebp.id")));
        }
        
        // PermitType
        if (transactionData.getPermit().getPermitType().getId() == this.transactionService.getMappingPropertiesValue("permit.type.regular.id")) {
            transactionData.getPermit().setPermitType(this.transactionFacade
                    .findPermitTypeById(this.transactionService.getMappingPropertiesValue("permit.type.regular.ebp.id")));
        } else if (transactionData.getPermit().getPermitType().getId() == this.transactionService
                .getMappingPropertiesValue("permit.type.addtime.id")) {
            transactionData.getPermit().setPermitType(this.transactionFacade
                    .findPermitTypeById(this.transactionService.getMappingPropertiesValue("permit.type.addtime.ebp.id")));
        }
    }
    
    private UnifiedRate getUnifiedRate(final TransactionData transactionData, final String transactionType, final String rateName) {
        String finalRateName = null;
        if (WebCoreConstants.TEST_NAME.equals(transactionType)) {
            finalRateName = WebCoreConstants.RATE_NAME_TEST_TRANSACTION;
        } else if (StringUtils.isBlank(rateName)) {
            finalRateName = WebCoreConstants.UNKNOWN;
        } else {
            finalRateName = rateName;
        }
        
        UnifiedRate rate = this.transactionFacade.findUnifiedRate(transactionData.getPointOfSale().getCustomer().getId(), finalRateName);
        if (rate == null) {
            rate = new UnifiedRate();
            rate.setCustomer(transactionData.getPointOfSale().getCustomer());
            rate.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
            rate.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
            rate.setName(finalRateName);
            
            final Set<Purchase> set = new HashSet<Purchase>(1);
            set.add(transactionData.getPurchase());
            rate.setPurchases(set);
        }
        return rate;
    }
    
    private Set<PurchaseTax> createPurchaseTaxes(final TransactionData transactionData, final TransactionDto txDto) {
        final Set<PurchaseTax> taxes = new HashSet<>();
        final Optional<PurchaseTax> tax1PurchaseTax =
                createPurchaseTax(transactionData, txDto.getTax1Name(), txDto.getTax1Rate(), txDto.getTax1Value());
        final Optional<PurchaseTax> tax2PurchaseTax =
                createPurchaseTax(transactionData, txDto.getTax2Name(), txDto.getTax2Rate(), txDto.getTax2Value());
        final Optional<PurchaseTax> tax3PurchaseTax =
                createPurchaseTax(transactionData, txDto.getTax3Name(), txDto.getTax3Rate(), txDto.getTax3Value());
        
        if (tax1PurchaseTax.isPresent()) {
            taxes.add(tax1PurchaseTax.get());
        }
        if (tax2PurchaseTax.isPresent()) {
            taxes.add(tax2PurchaseTax.get());
        }
        if (tax3PurchaseTax.isPresent()) {
            taxes.add(tax3PurchaseTax.get());
        }
        return taxes;
    }
    
    private Optional<PurchaseTax> createPurchaseTax(final TransactionData transactionData, final String taxName, final String taxRate,
        final String taxValue) {
        
        if (StringUtils.isNotBlank(taxName) && StringUtils.isNotBlank(taxValue)) {
            final String cleanTaxRate = StringUtils.isBlank(taxRate) ? StandardConstants.STRING_ZERO : taxRate;
            return Optional.ofNullable(getPurchaseTax(transactionData, taxName, cleanTaxRate, taxValue));
        }
        return Optional.empty();
    }
    
    private PurchaseTax getPurchaseTax(final TransactionData transactionData, final String taxName, final String taxRate, final String taxValue) {
        boolean isNewTax = false;
        Tax tax = this.transactionFacade.findTax(transactionData.getPointOfSale().getCustomer().getId(), taxName);
        if (tax == null) {
            isNewTax = true;
            tax = new Tax();
            tax.setCustomer(transactionData.getPointOfSale().getCustomer());
            tax.setName(taxName);
            tax.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        }
        
        final PurchaseTax purchaseTax = new PurchaseTax(tax, transactionData.getPurchase(), WebCoreUtil.convertToBase100ShortValue(taxRate),
                WebCoreUtil.convertToBase100ShortValue(taxValue));
        
        if (isNewTax) {
            final Set<PurchaseTax> set = new HashSet<PurchaseTax>(1);
            set.add(purchaseTax);
            purchaseTax.getTax().setPurchaseTaxes(set);
        }
        return purchaseTax;
    }
    
    private void deleteCoupon(final Coupon coupon) {
        coupon.setIsDeleted(true);
        coupon.setArchiveDate(new Date());
        
        this.transactionFacade.updateCoupon(coupon);
    }
    
    private Coupon findOrCreateCoupon(final TransactionData transactionData, final TransactionDto txDto, final String couponNum,
        final Date purchasedGmt) {
        Coupon coupon = null;
        if (StringUtils.isNotBlank(couponNum)) {
            String couponUpperCase = couponNum.toUpperCase();
            /*
             * Pay station will send coupon number starting with 'E' (EMS) or 'B' (Boss).
             * If first character is 'B' and appends '+' to the end of coupon code.
             */
            final boolean offlineCouponFlag = CouponUtil.isOfflineCouponFromPS(couponUpperCase);
            if (offlineCouponFlag) {
                couponUpperCase = couponUpperCase + WebCoreConstants.OFFLINE_COUPON_PLUS;
            } else {
                couponUpperCase = couponUpperCase.substring(1);
            }
            // Return if couponNum is blank after strip out the first character. Offline transaction might have '<CouponNumber>0</CouponNumber>'.
            if (StringUtils.isBlank(couponUpperCase)) {
                return null;
            }
            
            txDto.setCouponNumber(couponUpperCase);
            
            // For XML that is sent by a pre 6.4.1 pay station, needs to use different sql.
            boolean pre641 = false;
            if (StringUtils.isNotBlank(transactionData.getVersion()) && WebCoreUtil.isPsPre641(transactionData.getVersion())) {
                pre641 = true;
            }
            
            final Integer customerId = transactionData.getPointOfSale().getCustomer().getId();
            coupon = this.transactionFacade.findCoupon(customerId, couponUpperCase, pre641);
            if (!offlineCouponFlag) {
                if (coupon == null) {
                    coupon = this.transactionFacade.findRecentlyDeletedCoupon(customerId, couponUpperCase, pre641, purchasedGmt);
                    if ((coupon == null) || (CouponUtil.isDiscountValueChanged(coupon, txDto))) {
                        coupon = CouponUtil.createCoupon(transactionData.getPointOfSale(), transactionData.getPurchase(), txDto, false,
                                                         "Non-Existing Coupon");
                        deleteCoupon(coupon);
                    }
                }
            } else {
                // Create the offline coupon if it doesn't exist.
                if ((coupon == null) || (CouponUtil.isDiscountValueChanged(coupon, txDto))) {
                    if (coupon != null) {
                        deleteCoupon(coupon);
                    }
                    
                    coupon = CouponUtil.createCoupon(transactionData.getPointOfSale(), transactionData.getPurchase(), txDto, true, "Offline Coupon");
                }
            }
            
            // Reduces number of uses.
            if (coupon.getNumberOfUsesRemaining() != null
                && coupon.getNumberOfUsesRemaining().shortValue() != WebCoreConstants.COUPON_USES_UNLIMITED) {
                final short currNumOfUses = coupon.getNumberOfUsesRemaining();
                if (currNumOfUses > 0) {
                    coupon.setNumberOfUsesRemaining((short) (currNumOfUses - 1));
                }
            }
            
            // Set 'singleUseDate' using local time to identify this Single Use coupon has applied.
            if (coupon.getValidForNumOfDay() > 0) {
                coupon.setSingleUseDate(transactionData.getPurchase().getPurchaseGmt());
            }
        }
        
        return coupon;
    }
    
    private MobileNumber findMobileNumberIfExists(final String mobileNumber) {
        if (StringUtils.isBlank(mobileNumber)) {
            return null;
        }
        
        MobileNumber num = this.transactionFacade.findMobileNumber(mobileNumber);
        if (num != null) {
            return num;
        }
        // Create new MobileNumber object.
        num = new MobileNumber();
        num.setNumber(mobileNumber);
        num.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        return num;
    }
    
    private LicencePlate getLicencePlateIfExists(final String licensePlateNo) {
        if (StringUtils.isNotBlank(licensePlateNo)) {
            LicencePlate plate = this.transactionFacade.findLicencePlate(licensePlateNo);
            if (plate == null) {
                plate = new LicencePlate(licensePlateNo, DateUtil.getCurrentGmtDate());
            }
            return plate;
        }
        return null;
    }
    
    private PreAuth findApprovedPreAuthIfExists(final String preAuthId) {
        if (StringUtils.isBlank(preAuthId)) {
            return null;
        }
        return this.transactionFacade.findApprovedPreAuth(Long.parseLong(preAuthId));
    }
    
    private PreAuth findApprovedPreAuthHoldingIfExists(final String preAuthId, final String authorizationNumber) {
        if (StringUtils.isBlank(preAuthId) || StringUtils.isBlank(authorizationNumber)) {
            return null;
        }
        PreAuth preAuth = null;
        final PreAuthHolding holding = this.transactionFacade.findApprovedPreAuthHolding(Long.parseLong(preAuthId), authorizationNumber);
        if (holding != null) {
            preAuth = new PreAuth();
            preAuth.setId(holding.getPreAuth().getId());
            preAuth.setAuthorizationNumber(holding.getAuthorizationNumber());
            preAuth.setReferenceNumber(holding.getReferenceNumber());
            preAuth.setReferenceId(holding.getReferenceId());
            preAuth.setResponseCode(holding.getResponseCode());
            preAuth.setCardData(holding.getCardData());
            preAuth.setCardExpiry(holding.getCardExpiry());
            preAuth.setCardType(holding.getCardType());
            preAuth.setProcessorTransactionId(holding.getProcessorTransactionId());
            preAuth.setAmount(holding.getAmount());
            preAuth.setMerchantAccount(holding.getMerchantAccount());
            preAuth.setLast4digitsOfCardNumber(holding.getLast4digitsOfCardNumber());
            preAuth.setPreAuthDate(holding.getPreAuthDate());
            preAuth.setPointOfSale(holding.getPointOfSale());
            preAuth.setCardHash(holding.getCardHash());
            preAuth.setExtraData(holding.getExtraData());
            preAuth.setIsApproved(holding.isApproved());
            preAuth.setIsRfid(holding.isIsRfid());
        }
        return preAuth;
    }
    
    private boolean hasDataToProcess(final Purchase purchase, final String numberBillsAccepted, final String numberCoinsAccepted) {
        if (purchase.getCashPaidAmount() > 0 || purchase.getCoinPaidAmount() > 0 || purchase.getBillPaidAmount() > 0
            || StringUtils.isNotBlank(numberBillsAccepted) && Short.parseShort(numberBillsAccepted) > 0
            || StringUtils.isNotBlank(numberCoinsAccepted) && Short.parseShort(numberCoinsAccepted) > 0) {
            return true;
        }
        return false;
    }
    
    private int getCardTypeId(final TransactionDto txDto, final int paymentTypeId) {
        if (paymentTypeId == WebCoreConstants.PAYMENT_TYPE_CASH_VALUE || paymentTypeId == WebCoreConstants.PAYMENT_TYPE_VALUE_CARD) {
            int retVal = WebCoreConstants.VALUE_CARD;
            if ((txDto.getCardData() == null) && (StringUtils.isBlank(txDto.getEmsPreAuthId()))) {
                retVal = WebCoreConstants.N_A;
            }
            return retVal;
        } else if (paymentTypeId == WebCoreConstants.PAYMENT_TYPE_CASH_SMART || paymentTypeId == WebCoreConstants.PAYMENT_TYPE_SMART_CARD) {
            return WebCoreConstants.SMART_CARD;
        } else if (paymentTypeId == WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_SWIPE
                   || paymentTypeId == WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_SWIPE
                   || paymentTypeId == WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CL
                   || paymentTypeId == WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CONTACTLESS
                   || paymentTypeId == WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CH
                   || paymentTypeId == WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CHIP
                   || paymentTypeId == WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_FSWIPE
                   || paymentTypeId == WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CARD_FSWIPE) {
            return WebCoreConstants.CREDIT_CARD;
        }
        return WebCoreConstants.N_A;
    }
    
    private boolean isZeroAmountLinuxPS(final TransactionData transactionData, final TransactionDto txDto) {
        return transactionData.getPointOfSale().isIsLinux() != null && transactionData.getPointOfSale().isIsLinux()
               && transactionData.getPurchase().getChargedAmount() == 0 && StringUtils.isNotBlank(txDto.getCPSResponse());
    }
    
    private String returnCardTypeOrNull(final int unifiId, final String cardData, final String cardType) throws JsonException, CryptoException {
        if (CryptoUtil.isDecryptedCardData(cardData) || StringUtils.isBlank(cardData)) {
            return null;
        }
        if (StringUtils.isNotBlank(cardType)) {
            return cardType;
        }
        return this.linkCardTypeService.getCardType(unifiId, cardData);
    }
    
    // Copied from EMS 6.3.11 RpcPaystationAppServiceImpl.determineTransactionPaymentTypeId
    private int getPaymentTypeId(final TransactionData transactionData, 
                                 final TransactionDto txDto,
                                 final ProcessingDestinationDTO processDto) throws CryptoException, ApplicationException {
        final PaymentTypeIdFinder idFinder = new PaymentTypeIdFinder(transactionData.getPointOfSale().getId());
        
        //the Linux PS will send a Cash Transaction when the payment is 0$
        if (isZeroAmountLinuxPS(transactionData, txDto)) {
            idFinder.setUsedCC(true);
        }
        
        // $0.00 transactions are recorded as cash payment.
        if (WebCoreConstants.TEST_NAME.equals(txDto.getType())
            || (transactionData.getPurchase().getChargedAmount() <= 0 && txDto.getSmartCardPaid() != null
                && (StandardConstants.STRING_EMPTY_STRING.equals(txDto.getSmartCardPaid().trim())
                    || WebCoreUtil.convertToBase100IntValue(txDto.getSmartCardPaid()) <= 0))) {
            return WebCoreConstants.PAYMENT_TYPE_CASH;
        }
        
        if (WebCoreUtil.convertToBase100IntValue(txDto.getSmartCardPaid()) > 0) {
            idFinder.setUsedSC(true);
            idFinder.setUsedCC(false);
            idFinder.setUsedValueCard(false);
        } else if (WebCoreUtil.convertToBase100IntValue(txDto.getCardPaid()) > 0) {
            final String cardData = txDto.getCardData();
            // This is for CPS Charge (S&F).
            if (CardProcessingUtil.isNotNullAndIsLink(transactionData.getProcessorTransaction().getMerchantAccount())
                && CardProcessingUtil.isBlankOrZero(txDto.getEmsPreAuthId())) {
                try {
                    if (transactionData.getPointOfSale().getCustomer().getUnifiId() == null) {
                        final String err = "Customer ID: " + transactionData.getPointOfSale().getCustomer().getId() + " doesn't have an unifiId.";
                        LOG.error(err);
                        throw new ApplicationException(err);
                    }
                    
                    final Integer unifiId = transactionData.getPointOfSale().getCustomer().getUnifiId();
                    
                    if (StringUtils.isBlank(cardData) && txDto.getEmbeddedTxObject() != null) {
                        idFinder.setUsedCC(true);
                    } else {
                        final String cardType = returnCardTypeOrNull(unifiId, cardData, (processDto != null) ? processDto.getCardTypeName() : null);
                        transactionData.setCardType(cardType);
                        final CreditCardType ccType = this.creditCardTypeService.getCreditCardTypeByName(cardType);
                        if (ccType != null && ccType.isIsValid()) {
                            idFinder.setUsedCC(true);
                        } else {
                            transactionData.getPurchase().setCardData(cardData);
                            idFinder.setUsedValueCard(true);
                        }
                    }
                } catch (JsonException je) {
                    final String err =
                            "Cannot call linkCardTypeService.getCardType, customerId: " + transactionData.getPointOfSale().getCustomer().getId()
                                       + ", unifiId: " + transactionData.getPointOfSale().getCustomer().getUnifiId();
                    LOG.error(err, je);
                    throw new ApplicationException(err, je);
                }
                
            } else if (StringUtils.isNotBlank(cardData)) {
                // This for S&F
                String decryptCardData = cardData;
                if (!(StringUtils.isBlank(decryptCardData) || WebCoreUtil.checkIfAllDigits(decryptCardData)
                      || WebCoreUtil.escapeCharsAndCheckIfAllDigits(getEscapeChars(), decryptCardData))) {
                    decryptCardData = this.transactionFacade.decryptCardData(decryptCardData);
                }
                
                if (this.transactionFacade.isCreditCard(transactionData.getPointOfSale().getCustomer().getId(), decryptCardData)) {
                    idFinder.setUsedCC(true);
                } else {
                    transactionData.getPurchase().setCardData(decryptCardData);
                    idFinder.setUsedValueCard(true);
                }
            } else {
                // This is for Capture (Post-Auth).
                if (transactionData.getPreAuth() == null) {
                    if (StringUtils.isNotBlank(txDto.getEmsPreAuthId())) {
                        PreAuth preAuth = findApprovedPreAuthIfExists(txDto.getEmsPreAuthId());
                        if (preAuth == null) {
                            preAuth = findApprovedPreAuthHoldingIfExists(txDto.getEmsPreAuthId(), txDto.getCardAuthorizationId());
                            if (preAuth != null) {
                                transactionData.setForRecoverable(true);
                            }
                        }
                        transactionData.setPreAuth(preAuth);
                    }
                }
                
                if (transactionData.getPreAuth() == null) {
                    idFinder.setUsedCC(true);
                } else {
                    if (this.transactionFacade.isCreditCard(transactionData.getPreAuth())) {
                        idFinder.setUsedCC(true);
                    } else {
                        idFinder.setUsedValueCard(true);
                    }
                }
            }
        }
        if (WebCoreUtil.convertToBase100IntValue(txDto.getCashPaid()) > 0) {
            idFinder.setUsedCash(true);
        }
        
        if (idFinder.isUsedCC()) {
            if (StringUtils.isNotBlank(txDto.getPaidByRFID()) && txDto.getPaidByRFID().trim().equals(WebCoreConstants.RFID_DATA)) {
                idFinder.setUsedCCCL(true);
            } else if (StringUtils.isNotBlank(txDto.getPaidByChipCard()) && txDto.getPaidByChipCard().trim().equals(WebCoreConstants.CHIP_DATA)) {
                idFinder.setUsedCCChip(true);
            } else if (CardProcessingUtil.hasFSwipeCardType(this.messageHelper, txDto.getCardEaseData())) {
                idFinder.setUsedCCFSwipe(true);
            } else {
                idFinder.setUsedCCSwipe(true);
            }
        }
        
        final int paymentTypeId = idFinder.findPaymentTypeId();
        if (paymentTypeId == WebCoreConstants.PAYMENT_TYPE_UNKNOWN) {
            // Unknown Payment
            final StringBuilder bdr = new StringBuilder();
            bdr.append("Invalid Payment Type? ").append(idFinder).append(" for PointOfSale serialNumber: ")
            .append(transactionData.getPointOfSale().getSerialNumber());
            LOG.warn(bdr.toString());            
        }
        return paymentTypeId;
    }
    
    // Copied from EMS 6.3.11 RpcPaystationAppServiceImpl.calculateRevenue
    private int calculateRevenue(final TransactionData transactionData, final TransactionDto txDto) {
        int revenue = 0;
        int pcRevenue = 0;
        int scRevenue = 0;
        int customCardRevenue = 0;
        int ccRevenue = 0;
        int cashRevenue = 0;
        int ccCollections = 0;
        int ccScRechargeAmount = 0;
        int cashCollections = 0;
        int cashScRechargeAmount = 0;
        final int cashAttendantDeposit = 0;
        int cashTotalRefunds = 0;
        final int replenishAmount = 0;
        
        if (transactionData.getPurchase().getPaymentType().getId() == WebCoreConstants.PAYMENT_TYPE_PATROLLER_VALUE && transactionData.getPurchase()
                .getTransactionType().getId() != this.transactionService.getMappingPropertiesValue("transaction.type.test.id")) {
            pcRevenue = transactionData.getPurchase().getCardPaidAmount();
        }
        
        if (WebCoreUtil.hasNumericNotZeroValue(txDto.getSmartCardPaid())) {
            scRevenue = WebCoreUtil.convertToBase100IntValue(txDto.getSmartCardPaid());
        }
        
        if (transactionData.getPurchase().getPaymentType().getId() == WebCoreConstants.PAYMENT_TYPE_VALUE_CARD
            || transactionData.getPurchase().getPaymentType().getId() == WebCoreConstants.PAYMENT_TYPE_CASH_VALUE) {
            customCardRevenue = transactionData.getPurchase().getCardPaidAmount();
        }
        
        if (transactionData.getPurchase().getPaymentType().getId() == WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_SWIPE
            || transactionData.getPurchase().getPaymentType().getId() == WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_SWIPE
            || transactionData.getPurchase().getPaymentType().getId() == WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CONTACTLESS
            || transactionData.getPurchase().getPaymentType().getId() == WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CL
            || transactionData.getPurchase().getPaymentType().getId() == WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CHIP
            || transactionData.getPurchase().getPaymentType().getId() == WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CH) {
            ccCollections = transactionData.getPurchase().getCardPaidAmount();
        }
        
        if (transactionData.getPurchase().getTransactionType().getId() == this.transactionService
                .getMappingPropertiesValue(TRANSACTION_TYPE_SCRECHARGE_ID)
            && (transactionData.getPurchase().getPaymentType().getId() == WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_SWIPE
                || transactionData.getPurchase().getPaymentType().getId() == WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CONTACTLESS
                || transactionData.getPurchase().getPaymentType().getId() == WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CHIP)) {
            ccScRechargeAmount = transactionData.getPurchase().getChargedAmount();
        }
        
        ccRevenue = ccCollections - ccScRechargeAmount;
        
        cashCollections = transactionData.getPurchase().getCashPaidAmount();
        if (transactionData.getPurchase().getTransactionType().getId() == this.transactionService
                .getMappingPropertiesValue(TRANSACTION_TYPE_SCRECHARGE_ID)
            && transactionData.getPurchase().getPaymentType().getId() == WebCoreConstants.PAYMENT_TYPE_CASH) {
            cashScRechargeAmount = transactionData.getPurchase().getChargedAmount();
        }
        
        /*
         * 8 = EMS 6 TransactionType.DEPOSIT_VALUE
         * if (purchase.getTransactionType().getId() ==8)
         * {
         * cashAttendantDeposit = purchase.getCashPaid();
         * }
         */
        
        if (transactionData.getPurchase().getCashPaidAmount() > 0 && transactionData.getPurchase().isIsRefundSlip()) {
            cashTotalRefunds = transactionData.getPurchase().getCashPaidAmount() + transactionData.getPurchase().getCardPaidAmount()
                               - transactionData.getPurchase().getChargedAmount();
        } else {
            cashTotalRefunds = transactionData.getPurchase().getChangeDispensedAmount();
        }
        
        /*
         * 11 = EMS 6 TransactionType.REPLENISH_VALUE
         * 13 = EMS 6 TransactionType.HOPPER1_ADD_VALUE
         * 14 = EMS 6 TransactionType.HOPPER2_ADD_VALUE
         * if (purchase.getTransactionType().getId() == 11 || purchase.getTypeId() == 13 || purchase.getTypeId() == 14)
         * {
         * replenishAmount = purchase.getCashPaid();
         * }
         */
        cashRevenue = cashCollections - cashScRechargeAmount - cashAttendantDeposit - cashTotalRefunds - replenishAmount;
        revenue = cashRevenue + ccRevenue + customCardRevenue + scRevenue + pcRevenue;
        if (LOG.isDebugEnabled()) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("cashRevenue: ").append(cashRevenue).append(", ccRevenue: ").append(ccRevenue).append(", customCardRevenue: ")
                    .append(customCardRevenue);
            bdr.append(", scRevenue: ").append(scRevenue).append(", pcRevenue: ").append(pcRevenue).append(", ** revenue: ").append(revenue);
            LOG.debug(bdr.toString());
        }
        return revenue;
    }
    
    /**
     * Retrieve escape characters from mapping.properties, name 'charsToEscape' and convert String value to char array.
     * e.g. str = ":,="
     * return [:, =]
     */
    private char[] getEscapeChars() {
        final String str = this.transactionService.getMappingProperties().getProperty("charsToEscape");
        final String[] strArr = str.split(StandardConstants.STRING_COMMA);
        final char[] charsArr = new char[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            charsArr[i] = strArr[i].charAt(0);
        }
        return charsArr;
    }
    
    @Override
    public final void cancelTransaction(final Purchase purchase, final String messageNumber,
        final PaystationCancelTransaction paystationCancelTransaction) throws PaystationCommunicationException {
        final Permit permit = this.permitService.findPermitByPurchaseId(purchase.getId());
        final Collection<ProcessorTransaction> processorTransactions =
                this.processorTransactionService.findProcessorTransactionByPurchaseId(purchase.getId());
        PaymentCard paymentCard = null;
        ProcessorTransaction processorTransaction = null;
        SingleTransactionCardData singleTransactionCardData = null;
        int passcardAuthorizationType = WebCoreConstants.AUTH_TYPE_NONE;
        boolean isCardRefundFailed = false;
        if (purchase.getCardPaidAmount() > 0) {
            if (purchase.getPaymentType().getId() == WebCoreConstants.PAYMENT_TYPE_SMART_CARD
                || purchase.getPaymentType().getId() == WebCoreConstants.PAYMENT_TYPE_CASH_SMART) {
                isCardRefundFailed = true;
            } else {
                final Set<PaymentCard> paymentCards = purchase.getPaymentCards();
                if (paymentCards != null && !paymentCards.isEmpty()) {
                    final Iterator<PaymentCard> iter = paymentCards.iterator();
                    paymentCard = iter.next();
                }
                
                if (purchase.getPaymentType().getId() == WebCoreConstants.PAYMENT_TYPE_VALUE_CARD
                    || purchase.getPaymentType().getId() == WebCoreConstants.PAYMENT_TYPE_CASH_VALUE) {
                    passcardAuthorizationType = paymentCard.getCustomerCard().getCustomerCardType().getAuthorizationType().getId();
                }
                
                if (processorTransactions != null && !processorTransactions.isEmpty()) {
                    final Iterator<ProcessorTransaction> iter = processorTransactions.iterator();
                    processorTransaction = iter.next();
                    
                    singleTransactionCardData = this.singleTransactionCardDataService.findSingleTransactionCardDataByPurchaseId(purchase.getId());
                    
                    if (singleTransactionCardData == null) {
                        isCardRefundFailed = true;
                        //                    throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_CANNOT_CANCEL_ERROR);
                    } else {
                        String decryptedCardData = null;
                        try {
                            decryptedCardData = this.cryptoService.decryptData(singleTransactionCardData.getCardData(),
                                                                               purchase.getPointOfSale().getId(), purchase.getPurchaseGmt());
                        } catch (CryptoException ce) {
                            throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_ENCRYPTION_ERROR);
                        }
                        final Track2Card track2Card = new Track2Card(this.transactionFacade.findCreditCardType(decryptedCardData),
                                purchase.getCustomer().getId(), decryptedCardData);
                        final ProcessorTransaction resultPT = this.cardProcessingManager
                                .processTransactionRefund(processorTransaction, track2Card.getDecryptedCardData(),
                                                          track2Card.getExpirationMonth() + track2Card.getExpirationYear(), false);
                        
                        isCardRefundFailed = resultPT == null || resultPT.getId() == null;
                    }
                }
            }
        }
        fixTransactionData(purchase, paystationCancelTransaction, isCardRefundFailed);
        final PurchaseCollectionCancelled purchaseCollectionCancelled = createPurchaseCollectionCancelled(purchase, paystationCancelTransaction);
        if (purchase.getCoupon() != null) {
            fixCouponData(purchase.getCoupon());
        }
        if (passcardAuthorizationType == WebCoreConstants.AUTH_TYPE_INTERNAL_LIST) {
            fixInternalPassCard(paymentCard.getCustomerCard());
        }
        
        this.transactionService.cancelTransaction(permit, purchase, paymentCard, processorTransaction,
                                                  paymentCard != null ? paymentCard.getCustomerCard() : null, purchase.getCoupon(),
                                                  purchaseCollectionCancelled);
    }
    
    private PurchaseCollectionCancelled createPurchaseCollectionCancelled(final Purchase purchase,
        final PaystationCancelTransaction paystationCancelTransaction) {
        PurchaseCollectionCancelled purchaseCollectionCancelled = null;
        if (WebCoreUtil.convertToBoolean(paystationCancelTransaction.getEscrowAllReturned())) {
            purchaseCollectionCancelled = new PurchaseCollectionCancelled(purchase.getPointOfSale(), purchase, purchase.getPurchaseGmt(), 0,
                    purchase.getCoinPaidAmount(), (byte) purchase.getCoinCount());
        }
        return purchaseCollectionCancelled;
    }
    
    private void fixTransactionData(final Purchase purchase, final PaystationCancelTransaction paystationCancelTransaction,
        final boolean isCardRefundFailed) {
        purchase.setTransactionType(this.transactionFacade.findTransactionTypeById(ReportingConstants.TRANSACTION_TYPE_CANCELLED));
        purchase.setOriginalAmount(0);
        purchase.setChargedAmount(0);
        purchase.setChangeDispensedAmount(WebCoreUtil.convertToBase100IntValue(paystationCancelTransaction.getChangeDispensed()));
        purchase.setIsRefundSlip(WebCoreUtil.convertToBoolean(paystationCancelTransaction.getIsRefundSlip()));
        
        calculateExcessPayment(purchase, isCardRefundFailed);
    }
    
    private void fixInternalPassCard(final CustomerCard customerCard) {
        if (customerCard.getMaxNumberOfUses() != null && customerCard.getMaxNumberOfUses() > 0) {
            customerCard.setNumberOfUses((short) ((customerCard.getNumberOfUses() == null ? 0 : customerCard.getNumberOfUses()) - 1));
        }
    }
    
    private void fixCouponData(final Coupon coupon) {
        if (coupon.getNumberOfUsesRemaining() != null && coupon.getNumberOfUsesRemaining().shortValue() != WebCoreConstants.COUPON_USES_UNLIMITED) {
            final short currNumOfUses = coupon.getNumberOfUsesRemaining();
            if (currNumOfUses > 0) {
                coupon.setNumberOfUsesRemaining((short) (currNumOfUses + 1));
            }
        }
    }
    
    private void calculateExcessPayment(final Purchase purchase, final boolean isCardRefundFailed) {
        
        if (!purchase.isIsRefundSlip()) {
            purchase.setExcessPaymentAmount(purchase.getCashPaidAmount() - purchase.getChangeDispensedAmount());
        }
        if (isCardRefundFailed) {
            purchase.setExcessPaymentAmount(purchase.getExcessPaymentAmount() + purchase.getCardPaidAmount());
        }
    }
    
    public final Processor getCCProcessor(final PointOfSale pos) {
        final Set<MerchantPOS> merchantPoses = pos.getMerchantPOSes();
        
        for (MerchantPOS mpos : merchantPoses) {
            final Processor processor = mpos.getMerchantAccount().getProcessor();
            if (!processor.isIsForValueCard()) {
                return processor;
            }
        }
        
        return null;
    }
    
    @Override
    public final String getProcessorName(final MerchantAccount ma) {
        String procLabel = ma.getProcessor().getName();
        procLabel = procLabel.substring(procLabel.indexOf(PROCESSOR_PREFIX) + PROCESSOR_PREFIX.length());
        final String[] labelPieces = procLabel.split(Pattern.quote(StandardConstants.STRING_EMPTY_STRING));
        if (labelPieces.length <= 1) {
            return StringUtils.capitalize(procLabel);
        }
        final StringBuffer processorNameBuf = new StringBuffer();
        for (int i = -1; ++i < labelPieces.length;) {
            processorNameBuf.append(StringUtils.capitalize(labelPieces[i]));
        }
        return processorNameBuf.toString();
    }
    
    @Override
    public final EmbeddedTxObject createEmbeddedTxObject(final TransactionDto transDto) {
        if (StringUtils.isNotBlank(transDto.getCardEaseData())) {
            CardEaseData cardEaseData = null;
            try {
                cardEaseData = TransactionHelperImpl.json.deserialize(transDto.getCardEaseData(), CardEaseData.class);
            } catch (JsonException e) {
                printError(e);
                return null;
            }
            String pan = cardEaseData.getPan();
            if (StringUtils.isNotBlank(pan)) {
                pan = pan.substring(pan.length() - StandardConstants.CONSTANT_4);
                cardEaseData.setPan(pan);
            }
            try {
                final Map<String, Object> paymentDetails =
                        TransactionHelperImpl.json.deserialize(transDto.getCardEaseData(), new TypeReference<Map<String, Object>>() {
                        });
                cardEaseData.setPaymentDetails(paymentDetails);
            } catch (JsonException je) {
                LOG.error("Cannot parse CardEaseData json: " + transDto, je);
            }
            return cardEaseData;
        } else if (StringUtils.isNotBlank(transDto.getCPSResponse())) {
            final CPSResponse resp;
            try {
                resp = TransactionHelperImpl.json.deserialize(transDto.getCPSResponse(), CPSResponse.class);
            } catch (JsonException e) {
                printError(e);
                return null;
            }
            return resp;
        }
        return null;
    }
    
    private void printError(final JsonException jsone) {
        LOG.error("Unable to read JSON: " + json);
    }
}
