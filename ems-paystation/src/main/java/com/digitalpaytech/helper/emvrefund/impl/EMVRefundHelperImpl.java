package com.digitalpaytech.helper.emvrefund.impl;

import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.emvrefund.EMVRefund;
import com.digitalpaytech.helper.emvrefund.EMVRefundHelper;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.json.JSON;
import com.digitalpaytech.util.json.JSONConfigurationBean;
import com.digitalpaytech.dto.paystation.CardTransaction;
import com.digitalpaytech.dto.paystation.CardEaseData;

import java.util.Date;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("emvRefundHelper")
public class EMVRefundHelperImpl implements EMVRefundHelper {
    private static final Logger LOG = Logger.getLogger(EMVRefundHelperImpl.class);
    private static final int AUTH_CODE_RANDOM_STRING_LENGTH = 6;
    private static JSON json;
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    static {
        final JSONConfigurationBean jsonConfig = new JSONConfigurationBean(true, false, false);
        json = new JSON(jsonConfig);
    }
    
    
    @Override
    public final EMVRefund createEMVRefund(final PointOfSale pointOfSale, final String processedDate, final String cardEaseDataJson, final String amount)
            throws ApplicationException {
        
        final CardEaseData cardEaseData = createCardEaseData(cardEaseDataJson);
        final EMVRefund emvRefund = new EMVRefund();
        emvRefund.setCardEaseReference(cardEaseData.getCardReference());
        emvRefund.setAmount(Integer.parseInt(amount));
        emvRefund.setCardAuthorizationId(cardEaseData.getAuthCode() == null 
                ? generateRandomString(AUTH_CODE_RANDOM_STRING_LENGTH) : cardEaseData.getAuthCode());
        
        emvRefund.setCardType(cardEaseData.getAcquirer());
        emvRefund.setLast4DigitsOfCardNumber(Short.parseShort(cardEaseData.getPan()));
        emvRefund.setNumRetries(0);
        emvRefund.setProcessorTransactionId(cardEaseData.getRef());
        emvRefund.setReferenceNumber(cardEaseData.getTid());
        emvRefund.setSerialNumber(pointOfSale.getSerialNumber());
        try {
            emvRefund.setProcessedDate(DateUtil.convertFromColonDelimitedDateString(processedDate));
        } catch (InvalidDataException ide) {
            final String err = "Invalid date string: " + processedDate;
            LOG.error(err, ide);
            throw new ApplicationException(err, ide);
        }
        
        final Date nowDate = DateUtil.getCurrentGmtDate();
        emvRefund.setCreationDate(nowDate);
        emvRefund.setLastRetryDate(nowDate);
        
        final MerchantAccount ma = this.merchantAccountService.findByPointOfSaleIdAndCardTypeId(pointOfSale.getId(), WebCoreConstants.CARD_TYPE_CREDIT_CARD);
        if (ma == null) {
            throw new ApplicationException("Cannot find MerchantAccount using serialNumber: " + pointOfSale.getSerialNumber());
        }
        emvRefund.setTerminalToken(ma.getTerminalToken());
        
        final CardTransaction cardTransaction = createCardTransaction(emvRefund, cardEaseData, processedDate);
        emvRefund.setCardTransactionJson(createCardTransactionJson(cardTransaction));
        
        return emvRefund;
    }
    
    private CardEaseData createCardEaseData(final String cardEaseDataJson) throws ApplicationException {
        CardEaseData cardEaseData = null;
        try {
            cardEaseData = EMVRefundHelperImpl.json.deserialize(cardEaseDataJson, CardEaseData.class);
        } catch (JsonException jsone) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("Unable to read JSON: ").append(cardEaseDataJson).append(". JSON processor: ").append(EMVRefundHelperImpl.json);
            LOG.error(bdr.toString(), jsone);
            throw new ApplicationException(jsone);
        }
        final String pan = cardEaseData.getPan().substring(cardEaseData.getPan().length() - 4);
        cardEaseData.setPan(pan);
        cardEaseData.switchApplicationToApl();
        return cardEaseData;
    }
    
    private String createCardTransactionJson(final CardTransaction cardTransaction) throws ApplicationException {
        try {
            final String ctJson = EMVRefundHelperImpl.json.serialize(cardTransaction);
            if (LOG.isDebugEnabled()) {
                LOG.debug("CardTransaction JSON: " + ctJson);
            }
            return ctJson;
        } catch (JsonException jsone) {
            LOG.error("Unable to serialize CardTransaction object. ", jsone);
            throw new ApplicationException(jsone);
        }
    }
    
    private CardTransaction createCardTransaction(final EMVRefund emvRefund, 
                                                  final CardEaseData cardEaseData, 
                                                  final String processedDate) {
        
        final CardTransaction cardTransaction = new CardTransaction();
        cardTransaction.setAmount(emvRefund.getAmount());
        cardTransaction.setAuthorizationNumber(emvRefund.getCardAuthorizationId());
        cardTransaction.setCardType(emvRefund.getCardType());
        cardTransaction.setLast4Digits(String.valueOf(emvRefund.getLast4DigitsOfCardNumber()));
        cardTransaction.setProcessedDate(DateUtil.format(emvRefund.getProcessedDate(), DateUtil.UTC_TRANSACTION_DATE_TIME_FORMAT));
        cardTransaction.setProcessorSpecificData(cardEaseData);
        cardTransaction.setProcessorTransactionId(emvRefund.getProcessorTransactionId());
        cardTransaction.setProcessorType("Creditcall");
        cardTransaction.setReferenceNumber(emvRefund.getReferenceNumber());
        cardTransaction.setTerminalToken(emvRefund.getTerminalToken());
        cardTransaction.setTransactionType(CardProcessingConstants.SETTLE_WITH_NO_REFUNDS_TYPE_STRING);
        return cardTransaction;
    }
    
    private String generateRandomString(final int length) {
        return RandomStringUtils.randomAlphanumeric(length);
    }

    public final void setMerchantAccountService(final MerchantAccountService merchantAccountService) {
        this.merchantAccountService = merchantAccountService;
    }
}
