package com.digitalpaytech.helper.emvrefund;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.emvrefund.EMVRefund;
import com.digitalpaytech.exception.ApplicationException;

public interface EMVRefundHelper {
    EMVRefund createEMVRefund(PointOfSale pointOfSale, String processedDate, String cardEaseData, String amount) throws ApplicationException;
}
