package com.digitalpaytech.helper;

import java.util.Set;

import com.digitalpaytech.cardprocessing.cps.impl.ProcessingDestinationDTO;
import com.digitalpaytech.data.TransactionData;
import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.SmsAlert;
import com.digitalpaytech.dto.EmbeddedTxObject;
import com.digitalpaytech.dto.paystation.TransactionDto;
import com.digitalpaytech.dto.rest.paystation.PaystationCancelTransaction;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.PaystationCommunicationException;

public interface TransactionHelper {
    
    TransactionData getTransactionData(PointOfSale pointOfSale, 
                                       TransactionDto txDto, 
                                       ProcessingDestinationDTO processDto, 
                                       String version, 
                                       boolean isSPT) throws ApplicationException;
    
    boolean hasDataToProcess(SmsAlert smsAlert);
    
    boolean hasDataToProcess(Set<PaymentCard> paymentCards);
    
    boolean hasSmartCardDataAndPaid(TransactionDto transactionDto);
    
    boolean hasCustomerCardTypeAuthInternal(TransactionData transactionData);
    
    boolean hasCustomerCardTypeAuthExternal(TransactionData transactionData);
    
    boolean hasCustomerCardTypeAuthNone(TransactionData transactionData);
    
    CardRetryTransaction createCardRetryTransaction(TransactionData transactionData, TransactionDto txDto) throws CryptoException;

    void cancelTransaction(Purchase purchase, String messageNumber, PaystationCancelTransaction paystationCancelTransaction)
        throws PaystationCommunicationException;
    
    Processor getCCProcessor(PointOfSale pos);
    
    String getProcessorName(final MerchantAccount ma);   
    
    EmbeddedTxObject createEmbeddedTxObject(TransactionDto transDto);
}
