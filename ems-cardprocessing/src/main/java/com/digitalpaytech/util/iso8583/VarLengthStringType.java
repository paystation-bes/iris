package com.digitalpaytech.util.iso8583;

import java.util.Map;

/**
 * Variable length alphanumeric field. Encoded value will have a prepended length indicator (BCD). The size of
 * the prepend indicator depends on the maximum field length passed into the constructor.<br>
 * Encode example: For a max 5 bytes field, "V6C" -> 0x03 0x56 0x36 0x43<br>
 * Decode example: For a max 5 bytes field, 0x04 0x41 0x42 0x33 0x34 -> "AB34"
 * 
 * @author pault
 * 
 */
public class VarLengthStringType extends Iso8583DataType
{
	private int maxFieldLength;
	private int lenIndicatorlength; // length of indicator length in bytes

	/**
	 * Construct a variable length alphanumeric data type. The length of the prepended length indicator will
	 * be calculated based on the maxFieldLength.
	 * 
	 * @param fieldId
	 *            The bit number of the field.
	 * @param maxFieldLength
	 *            Maximum data length in bytes excluding the length indicator.
	 */
	public VarLengthStringType(int fieldId, int maxFieldLength)
	{
		super(fieldId);
		this.maxFieldLength = maxFieldLength;
		this.lenIndicatorlength = (Integer.toString(maxFieldLength).length() + 1) / 2;
	}

	/**
	 * Construct a variable length alphanumeric data type with a specific length indicator length.
	 * 
	 * @param fieldId
	 *            The bit number of the field.
	 * @param maxFieldLength
	 *            Maximum data length in bytes excluding the length indicator.
	 * @param lenIndicatorLength
	 *            The length of the prepended length indicator in bytes.
	 */
	public VarLengthStringType(int fieldId, int maxFieldLength, int lenIndicatorLength)
	{
		super(fieldId);
		this.maxFieldLength = maxFieldLength;
		this.lenIndicatorlength = lenIndicatorLength;
	}

	@Override
	public int decodeValue(char[] message, int index, Map<Integer, String> fieldMap)
	{
		// extract the field length
		StringBuffer lenStr = new StringBuffer();
		for (int i = 0; i < lenIndicatorlength; i++)
		{
			// append high nibble
			lenStr.append(Integer.toString(message[index + i] >> 4));

			// append low nibble
			lenStr.append(Integer.toString(message[index + i] & 0x0F));
		}
		int dataLength = Integer.parseInt(lenStr.toString());

		// extract the string
		fieldMap.put(getFieldId(), new String(message, index + lenIndicatorlength, dataLength));

		return index + lenIndicatorlength + dataLength;
	}

	@Override
	public char[] encodeValue(String strValue)
	{
		if (strValue != null)
		{
			if (strValue.length() > maxFieldLength)
			{
				throw new RuntimeException("The value " + strValue + " exceeded the field length limit of "
						+ maxFieldLength + " bytes.");
			}

			StringBuffer sb = new StringBuffer();
			int dataLength = strValue.length();

			// encode the length indicator
			String lenIndicatorStr = Integer.toString(dataLength);
			int numPadLenDigits = lenIndicatorlength * 2 - lenIndicatorStr.length();

			for (int i = 0; i < numPadLenDigits; i++)
			{
				lenIndicatorStr = "0" + lenIndicatorStr;
			}
			for (int i = 0; i < lenIndicatorStr.length(); i += 2)
			{
				sb.append((char)Integer.parseInt(lenIndicatorStr.substring(i, i + 2), 16));
			}

			// append the data
			sb.append(strValue);

			return sb.toString().toCharArray();
		}
		return null;
	}

}
