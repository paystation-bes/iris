/* 
 * Copyright (c) 1997 - 2009  DPT  All rights reserved.
 *
 * Created on May 25, 2009 by rexz
 * 
 * $Header:  $
 * 
 * $Log:  $ 
 *
 */

package com.digitalpaytech.util.iso8583;

import java.util.Hashtable;

import org.apache.log4j.Logger;

import com.digitalpaytech.util.iso8583.FixLengthBcdType;
import com.digitalpaytech.util.iso8583.FixLengthStringType;
import com.digitalpaytech.util.iso8583.Iso8583DataType;
import com.digitalpaytech.util.iso8583.VarLengthBcdType;
import com.digitalpaytech.util.iso8583.VarLengthStringType;
import com.digitalpaytech.cardprocessing.impl.FirstDataRequestBuilder;
import com.digitalpaytech.cardprocessing.impl.FirstDataResponseParser;

/**
 * Iso8583ProtocolFactory
 * 
 * @author rexz
 * @since
 * 
 */
public class Iso8583ProtocolFactory
{
	private final static Logger logger__ = Logger.getLogger(Iso8583ProtocolFactory.class);

	public static final int BITS_64  = 64;  // bitmap bits - 64 for 8 bytes
	public static final String PAN_MARK_STRING = "XXXXXXXXXXXXXXXXXXXX"; // 20 X's
	public static final String TRACK2DATA_MARK_STRING = "XXXXXXXXXXXXXXXXXXXXDXXXXXXXXXXXXXXXX";  // 37
	private static Iso8583ProtocolFactory instance = null;
	private FirstDataRequestBuilder fdBuilder = null;
	private FirstDataResponseParser fdParser = null;
  	
	private static Hashtable typeMap = new Hashtable();

	private static String FIRST_DATA_NASHVILLE = "First Data NashVille";

	static
	{
		Iso8583DataType[] fdTypeMap = {new FixLengthBcdType(0, 2), // 0, Mesaage Type
				null, // 1, Bitmap
				new VarLengthBcdType(2, 10, false), // 2, Primary Account Number
				new FixLengthBcdType(3, 3), // 3, Processing Code
				new FixLengthBcdType(4, 6), // 4, Amount of Transaction
				null,  // 5
				null,  // 6
				new FixLengthBcdType(7, 5), // 7, Transaction Date/Time
				null,  // 8
				null,  // 9
				null,  // 10
				new FixLengthBcdType(11, 3), // 11, System Trace
				new FixLengthBcdType(12, 3), // 12, Time, Local Trransmission
				new FixLengthBcdType(13, 2), // 13, Date, Local Trans
				new FixLengthBcdType(14, 2), // 14, Card Expiration Date
				null,  // 15
				null,  // 16
				null,  // 17
				new FixLengthBcdType(18, 2), // 18, Merchant Category Code 
				null,  // 19
				null,  // 20
				null,  // 21
				new FixLengthBcdType(22, 2), // 22, POS ENtry Mode + Pin Capability
				new FixLengthBcdType(23, 3), // 23, Card Sequence Number
				new FixLengthBcdType(24, 2), // 24, Network International ID
				new FixLengthBcdType(25, 1), // 25, POS Condition Code
				null,  // 26
				null,  // 27
				null,  // 28
				null,  // 29
				null,  // 30
				new VarLengthStringType(31, 99), // 31, Acquirer Reference Data
				new VarLengthBcdType(32, 6, true), // 32, Acquiring ID
				null,  // 33
				null,  // 34
				new VarLengthBcdType(35, 37, false), // 35, Track 2 Data
				null,  // 36
				new FixLengthStringType(37, 12, ' ', false), // 37, Retrieval Reference Number
				new FixLengthStringType(38, 6, ' ', false), // 38, Authorization Identification Rresponse
				new FixLengthStringType(39, 2, ' ', false), // 39, Response Code
				null,  // 40
				new FixLengthStringType(41, 8, ' ', true), // 41, Terminal ID
				new FixLengthStringType(42, 15, ' ', true), // 42, Merchant ID
				new FixLengthStringType(43, 107, ' ', false), // 43, Alternative mechant Name
				new VarLengthStringType(44, 1, 2), // 44, Additional Response Data  
				new VarLengthStringType(45, 76), // 45,Track 1 Data
				null,  // 46
				null,  // 47
				null,  // 48
				new FixLengthBcdType(49, 2), // 49,
				null,  // 50
				null,  // 51
				null,  // 52, Encrypted PIN Data
				null,  // 53
				new VarLengthBcdType(54, 12, true, 2), // 54, Additional Amounts   
				new VarLengthBcdType(55, 999, true, 2), // 55, EMV Data 
				null,  // 56
				null,  // 57
				null,  // 58
				new VarLengthStringType(59, 9), // 59 Merchant Zip/Postal COde
				new FixLengthBcdType(60, 1), // 60, Additional POS information
				null,  // 61
				null,  // 62 First Data private data
				new VarLengthStringType(63, 999),  // 63 First Data private data
				null,  // 64
				null,  // 65
				null,  // 66
				null,  // 67
				null,  // 68
				null,  // 69
				new FixLengthBcdType(70, 2), // 70, Network management Information Code
				null,  // 71
				null,  // 72
				null,  // 73
				null,  // 74
				null,  // 75
				null,  // 76
				null,  // 77
				null,  // 78
				null,  // 79
				null,  // 80
				null,  // 81
				null,  // 82
				null,  // 83
				null,  // 84
				null,  // 85
				null,  // 86
				null,  // 87
				null,  // 88
				null,  // 89
				null,  // 90
				null,  // 91
				null,  // 92
				new FixLengthStringType(93, 5, ' ', false), // 93, Response Indicator
				null,  // 94
				null,  // 95
				new VarLengthBcdType(96, 18, true), // 96, Key Management Data
				null,  // 97
				null,  // 98
				null,  // 99
				new VarLengthBcdType(100, 11, true), // 100, Receiving Institution ID Code				
		};
		typeMap.put(FIRST_DATA_NASHVILLE, fdTypeMap);
	}

	private Iso8583ProtocolFactory()
	{
		fdBuilder = new FirstDataRequestBuilder(getFirstDataNashvilleTypeMap());
		fdParser = new FirstDataResponseParser(getFirstDataNashvilleTypeMap());
	}

	public static Iso8583ProtocolFactory getInstance()
	{
		if (instance == null)
		{
			instance = new Iso8583ProtocolFactory();			
		}
		return instance;
	}

	public Iso8583DataType[] getFirstDataNashvilleTypeMap()
	{
		return (Iso8583DataType[])typeMap.get(FIRST_DATA_NASHVILLE);
	}

	public FirstDataRequestBuilder getFirstDataRequestBuilder()
	{
		return fdBuilder;
	}

	public FirstDataResponseParser getFirstDataResponseParser()
	{
		return fdParser;
	}
}
