package com.digitalpaytech.util.iso8583;

import java.util.Map;

/**
 * Variable length BCD Numeric field. Encoded value will have a prepended length indicator (BCD as well). The
 * size of the prepend indicator depends on the maximum field length passed into the constructor. User can
 * specify where the field is left padded or right padded with 0's. The decoded string will be string in
 * decimal notation.<br>
 * Encode example: For a left padded, max 3 bytes field, "42364" -> 0x05 0x04 0x23 0x64<br>
 * Decode example: For a right padded, max 3 bytes field, 0x05 0x02 0x34 0x50 -> "02345"
 * 
 * @author pault
 * 
 */
public class VarLengthBcdType extends Iso8583DataType
{
	private int maxFieldLength;
	private boolean isLeftPadded;
	private int lenIndicatorlength; // length of indicator length in bytes

	/**
	 * Construct a variable length BCD data type. The length of the prepended length indicator will be
	 * calculated based on the maxFieldLength.
	 * 
	 * @param fieldId
	 *            The bit number of the field.
	 * @param maxFieldLength
	 *            Maximum data length in bytes excluding the length indicator.
	 * @param isLeftPadded
	 *            true - left pad 0's <br>
	 *            false - right pad 0's
	 */
	public VarLengthBcdType(int fieldId, int maxFieldLength, boolean isLeftPadded)
	{
		super(fieldId);
		this.maxFieldLength = maxFieldLength;
		this.isLeftPadded = isLeftPadded;
		this.lenIndicatorlength = (Integer.toString(maxFieldLength).length() + 1) / 2;
	}

	/**
	 * Construct a variable length BCD data type with a specific length indicator length.
	 * 
	 * @param fieldId
	 *            The bit number of the field.
	 * @param maxFieldLength
	 *            Maximum data length in bytes excluding the length indicator.
	 * @param isLeftPadded
	 *            true - left pad 0's <br>
	 *            false - right pad 0's
	 * @param lenIndicatorLength
	 *            The length of the prepended length indicator in bytes.
	 */
	public VarLengthBcdType(int fieldId, int maxFieldLength, boolean isLeftPadded, int lenIndicatorLength)
	{
		super(fieldId);
		this.maxFieldLength = maxFieldLength;
		this.isLeftPadded = isLeftPadded;
		this.lenIndicatorlength = lenIndicatorLength;
	}

	@Override
	public int decodeValue(char[] message, int index, Map<Integer, String> fieldMap)
	{
		// extract the field length
		StringBuffer lenStr = new StringBuffer();
		for (int i = 0; i < lenIndicatorlength; i++)
		{
			// append high nibble
			lenStr.append(Integer.toString(message[index + i] >> 4));

			// append low nibble
			lenStr.append(Integer.toString(message[index + i] & 0x0F));
		}
		int numDigits = Integer.parseInt(lenStr.toString());
		int numBytes = (numDigits + 1) / 2;

		int dataStartIndex = index + lenIndicatorlength;
		StringBuffer sb = new StringBuffer();
		int extractedDigits = 0;
		boolean isOddLength = numDigits % 2 != 0;
		boolean skipFirstDigit = isOddLength && isLeftPadded;

		for (int i = 0; i < numBytes; i++)
		{
			if (i != 0 || !skipFirstDigit)
			{
				// append high nibble
				sb.append(Integer.toString(message[dataStartIndex + i] >> 4));
				extractedDigits++;
			}

			if (extractedDigits < numDigits)
			{
				// append low nibble
				sb.append(Integer.toString(message[dataStartIndex + i] & 0x0F));
				extractedDigits++;
			}
		}
		fieldMap.put(getFieldId(), sb.toString());
		return index + lenIndicatorlength + numBytes;
	}

	@Override
	public char[] encodeValue(String strValue)
	{
		if (strValue != null)
		{
			int numDigits = strValue.length();
			if (numDigits > maxFieldLength * 2)
			{
				throw new RuntimeException("The value " + strValue + " exceeded the field length limit of "
						+ maxFieldLength + " bytes.");
			}

			StringBuffer sb = new StringBuffer();

			// encode the length indicator
			String lenIndicatorStr = Integer.toString(numDigits);
			int numPadLenDigits = lenIndicatorlength * 2 - lenIndicatorStr.length();

			for (int i = 0; i < numPadLenDigits; i++)
			{
				lenIndicatorStr = "0" + lenIndicatorStr;
			}
			for (int i = 0; i < lenIndicatorStr.length(); i += 2)
			{
				sb.append((char)Integer.parseInt(lenIndicatorStr.substring(i, i + 2), 16));
			}

			// encode the actual data
			int fieldLength = (numDigits + 1) / 2;
			if (strValue.length() < fieldLength * 2)
			{
				// need to pad nibbles with 0's
				int numZeros = fieldLength * 2 - strValue.length();
				for (int i = 0; i < numZeros; i++)
				{
					if (isLeftPadded)
					{
						strValue = "0" + strValue;
					}
					else
					{
						strValue = strValue + "0";
					}
				}
			}

			// strValue should have fieldLength * 2 chars now
			for (int i = 0; i < strValue.length(); i += 2)
			{
				sb.append((char)Integer.parseInt(strValue.substring(i, i + 2), 16));
			}
			return sb.toString().toCharArray();
		}
		return null;
	}

}
