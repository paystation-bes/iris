package com.digitalpaytech.util.iso8583;

import java.util.Map;

/**
 * Fixed length BCD Numeric. Encoded value will be left padded with 0's if passed in data is shorter then field
 * length. The decoded string will be string in decimal notation.<br>
 * Encode example: For a 3 bytes field, "379" -> 0x00 0x03 0x79<br>
 * Decode example: For a 3 bytes field, 0x00 0x03 0x79 -> "000379"
 * 
 * @author pault
 * 
 */
public class FixLengthBcdType extends Iso8583DataType
{
	private int fieldLength;

	/**
	 * Construct a fixed length BCD data type.
	 * 
	 * @param fieldId
	 *            The bit number of the field.
	 * @param fieldLength
	 *            The field length in bytes.
	 */
	public FixLengthBcdType(int fieldId, int fieldLength)
	{
		super(fieldId);
		this.fieldLength = fieldLength;
	}

	@Override
	public int decodeValue(char[] message, int index, Map<Integer, String> fieldMap)
	{
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < fieldLength; i++)
		{
			// append high nibble
			sb.append(Integer.toString(message[index + i] >> 4));

			// append low nibble
			sb.append(Integer.toString(message[index + i] & 0x0F));
		}
		fieldMap.put(getFieldId(), sb.toString());
		return index + fieldLength;
	}

	@Override
	public char[] encodeValue(String strValue)
	{
		if (strValue != null)
		{
			if (strValue.length() > fieldLength * 2)
			{
				throw new RuntimeException("The value " + strValue + " exceeded the field length limit of "
						+ fieldLength + " bytes.");
			}
			char[] message = new char[fieldLength];
			if (strValue.length() < fieldLength * 2)
			{
				// need to left pad 0's
				int numZeros = fieldLength * 2 - strValue.length();
				for (int i = 0; i < numZeros; i++)
				{
					strValue = "0" + strValue;
				}
			}

			// strValue should have fieldLength * 2 chars now
			for (int i = 0; i < strValue.length(); i += 2)
			{
				message[i / 2] = (char)Integer.parseInt(strValue.substring(i, i + 2), 16);
			}
			return message;
		}
		return null;
	}

}
