package com.digitalpaytech.util.iso8583;

import java.util.Map;

/**
 * Base ISO8583 data type.
 * 
 * @author pault
 * 
 */
public abstract class Iso8583DataType
{
	private int fieldId;

	public Iso8583DataType(int fieldId)
	{
		super();
		this.fieldId = fieldId;
	}

	public int getFieldId()
	{
		return fieldId;
	}

	/**
	 * Encode a value.
	 * 
	 * @param strValue
	 *            String representation of the value. Interpreted different for different sub-types.
	 * @return Encoded value in char array.
	 */
	public abstract char[] encodeValue(String strValue);

	/**
	 * Extract and decode a value from the message.
	 * 
	 * @param message
	 * @param index
	 *            The start index of the field.
	 * @param fieldMap
	 *            The map to put the decoded result in
	 * @return The array index of the next data field.
	 */
	public abstract int decodeValue(char[] message, int index, Map<Integer, String> fieldMap);
}
