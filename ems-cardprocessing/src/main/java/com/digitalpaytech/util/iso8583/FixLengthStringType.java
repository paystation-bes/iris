package com.digitalpaytech.util.iso8583;

import java.util.Map;

/**
 * Fixed length alphanumeric. Encoded value will be left or right padded with user specified character if
 * passed in data is shorter then field length.<br>
 * Encode example: For a 5 bytes field, left padded with '_', "379" -> 0x5F 0x5F 0x33 0x37 0x39<br>
 * Decode example: For a 3 bytes field, 0x31 0x32 0x33 -> "123"
 * 
 * @author pault
 * 
 */
public class FixLengthStringType extends Iso8583DataType
{
	private int fieldLength;
	private char padCharacter;
	private boolean isLeftPadded;

	/**
	 * Construct a fixed length alphanumeric data type.
	 * 
	 * @param fieldId
	 *            The bit number of the field.
	 * @param fieldLength
	 *            The field length in bytes.
	 * @param padCharacter
	 *            The character used to pad remaining space.
	 * @param isLeftPadded
	 *            true - left pad 0's <br>
	 *            false - right pad 0's
	 */
	public FixLengthStringType(int fieldId, int fieldLength, char padCharacter, boolean isLeftPadded)
	{
		super(fieldId);
		this.fieldLength = fieldLength;
		this.padCharacter = padCharacter;
		this.isLeftPadded = isLeftPadded;
	}

	@Override
	public int decodeValue(char[] message, int index, Map<Integer, String> fieldMap)
	{
		fieldMap.put(getFieldId(), new String(message, index, fieldLength));
		return index + fieldLength;
	}

	@Override
	public char[] encodeValue(String strValue)
	{
		if (strValue != null)
		{
			if (strValue.length() > fieldLength)
			{
				throw new RuntimeException("The value " + strValue + " exceeded the field length limit of "
						+ fieldLength + " bytes.");
			}

			// pad remaining space if data is shorter than field length
			int numPad = fieldLength - strValue.length();

			for (int i = 0; i < numPad; i++)
			{
				if (isLeftPadded)
				{
					strValue = padCharacter + strValue;
				}
				else
				{
					strValue = strValue + padCharacter;
				}
			}
			return strValue.toCharArray();
		}
		return null;
	}
}
