package com.digitalpaytech.util.xml;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("Request")
public class ElavonViaConexRequest {
    @XStreamAlias("Version")
    private String version;
    @XStreamImplicit
    private List<ElavonRequestBlock> blocks;
    // Request type, e.g. Credit Card.Sale, Credit Card.Auth Only
    @XStreamAsAttribute
    private String id;

    public final String getVersion() {
        return this.version;
    }
    public final void setVersion(final String version) {
        this.version = version;
    }
    public final List<ElavonRequestBlock> getBlocks() {
        return this.blocks;
    }
    public final void setBlocks(final List<ElavonRequestBlock> blocks) {
        this.blocks = blocks;
    }
    public final String getId() {
        return this.id;
    }
    public final void setId(final String id) {
        this.id = id;
    }
}
