package com.digitalpaytech.util.xml;

import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;


@XStreamAlias("Block")
public class ElavonResponseBlock {
    @XStreamAlias("Response_Code")
    private String responseCode;
    @XStreamAlias("Issuer_Response_Code")
    private String issuerResponseCode;
    @XStreamAlias("Authorization_Source")
    private String authorizationSource;
    @XStreamAlias("Capture_Code")
    private String captureCode;
    @XStreamAlias("Approval_Code")
    private String approvalCode;
    @XStreamAlias("Authorization_Date")
    private String authorizationDate;
    @XStreamAlias("Authorization_Time")
    private String authorizationTime;
    @XStreamAlias("Batch_Number")
    private String batchNumber;
    @XStreamAlias("Record_Number")
    private String recordNumber;
    @XStreamAlias("Authorization_Response")
    private String authorizationResponse;
    @XStreamAlias("Trace_Number")
    private String traceNumber;
    @XStreamAlias("Transaction_Reference_Nbr")
    private String transactionReferenceNbr;
    @XStreamAlias("Amex_Capture_Code")
    private String amexCaptureCode;
    @XStreamAlias("AVS_Response")
    private String avsResponse;
    @XStreamAlias("CVV2_Response")
    private String cvv2Response;
    @XStreamAlias("PS2000_Data")
    private String ps2000Data;
    @XStreamAlias("MSDI")
    private String msdi;
    @XStreamAlias("Authorized_Amount")
    private String authorizedAmount;
    @XStreamAlias("Account_Balance_1")
    private String accountBalance1;
    @XStreamAlias("Account_Balance_2")
    private String accountBalance2;
    @XStreamAlias("OAR_Data")
    private String oarData;
    @XStreamAlias("Spend_Qualifier")
    private String spendQualifier;
    @XStreamAlias("Association_Name")
    private String associationName;
    @XStreamAlias("Response_Message")
    private String responseMessage;
    @XStreamAlias("Net_Amount_Sign")
    private String netAmountSign;
    @XStreamAlias("Net_Amount")
    private String netAmount;
    @XStreamAlias("Net_Count")
    private String netCount;
    @XStreamAlias("Status")
    private String status;
    @XStreamAlias("Results")
    private String results;
    @XStreamAlias("Token_Account_Status")
    private String tokenAccountStatus;
    @XStreamAlias("Token_Assurance_Level")
    private String tokenAssuranceLevel;
    @XStreamAlias("Token_Requestor_ID")
    private String tokenRequestorID;
    @XStreamAlias("PAN_Last_Four_Digits")
    private String panLastFourDigits;
    @XStreamAlias("Transaction_Code")
    private String transactionCode;
    @XStreamAlias("Credit_Sale_Amount")
    private String creditSaleAmount;
    @XStreamAlias("Credit_Sale_Count")
    private String creditSaleCount;
    @XStreamAlias("Credit_Return_Amount")
    private String creditReturnAmount;
    @XStreamAlias("Credit_Return_Count")
    private String creditReturnCount;
    
    @XStreamAsAttribute
    private String id;

    private Date authorizationDateTime;
   
    
    public final String getResponseCode() {
        return this.responseCode;
    }
    public final void setResponseCode(final String responseCode) {
        this.responseCode = responseCode;
    }
    public final String getIssuerResponseCode() {
        return this.issuerResponseCode;
    }
    public final void setIssuerResponseCode(final String issuerResponseCode) {
        this.issuerResponseCode = issuerResponseCode;
    }
    public final String getAuthorizationSource() {
        return this.authorizationSource;
    }
    public final void setAuthorizationSource(final String authorizationSource) {
        this.authorizationSource = authorizationSource;
    }
    public final String getCaptureCode() {
        return this.captureCode;
    }
    public final void setCaptureCode(final String captureCode) {
        this.captureCode = captureCode;
    }
    public final String getApprovalCode() {
        return this.approvalCode;
    }
    public final void setApprovalCode(final String approvalCode) {
        this.approvalCode = approvalCode;
    }
    public final String getAuthorizationDate() {
        return this.authorizationDate;
    }
    public final void setAuthorizationDate(final String authorizationDate) {
        this.authorizationDate = authorizationDate;
    }
    public final String getAuthorizationTime() {
        return this.authorizationTime;
    }
    public final void setAuthorizationTime(final String authorizationTime) {
        this.authorizationTime = authorizationTime;
    }
    public final String getBatchNumber() {
        return this.batchNumber;
    }
    public final void setBatchNumber(final String batchNumber) {
        this.batchNumber = batchNumber;
    }
    public final String getRecordNumber() {
        return this.recordNumber;
    }
    public final void setRecordNumber(final String recordNumber) {
        this.recordNumber = recordNumber;
    }
    public final String getAuthorizationResponse() {
        return this.authorizationResponse;
    }
    public final void setAuthorizationResponse(final String authorizationResponse) {
        this.authorizationResponse = authorizationResponse;
    }
    public final String getTraceNumber() {
        return this.traceNumber;
    }
    public final void setTraceNumber(final String traceNumber) {
        this.traceNumber = traceNumber;
    }
    public final String getAmexCaptureCode() {
        return this.amexCaptureCode;
    }
    public final void setAmexCaptureCode(final String amexCaptureCode) {
        this.amexCaptureCode = amexCaptureCode;
    }
    public final String getAvsResponse() {
        return this.avsResponse;
    }
    public final void setAvsResponse(final String avsResponse) {
        this.avsResponse = avsResponse;
    }
    public final String getCvv2Response() {
        return this.cvv2Response;
    }
    public final void setCvv2Response(final String cvv2Response) {
        this.cvv2Response = cvv2Response;
    }
    public final String getPs2000Data() {
        return this.ps2000Data;
    }
    public final void setPs2000Data(final String ps2000Data) {
        this.ps2000Data = ps2000Data;
    }
    public final String getMsdi() {
        return this.msdi;
    }
    public final void setMsdi(final String msdi) {
        this.msdi = msdi;
    }
    public final String getAuthorizedAmount() {
        return this.authorizedAmount;
    }
    public final void setAuthorizedAmount(final String authorizedAmount) {
        this.authorizedAmount = authorizedAmount;
    }
    public final String getAccountBalance1() {
        return this.accountBalance1;
    }
    public final void setAccountBalance1(final String accountBalance1) {
        this.accountBalance1 = accountBalance1;
    }
    public final String getAccountBalance2() {
        return this.accountBalance2;
    }
    public final void setAccountBalance2(final String accountBalance2) {
        this.accountBalance2 = accountBalance2;
    }
    public final String getId() {
        return this.id;
    }
    public final void setId(final String id) {
        this.id = id;
    }
    public final Date getAuthorizationDateTime() {
        return this.authorizationDateTime;
    }
    public final void setAuthorizationDateTime(final Date authorizationDateTime) {
        this.authorizationDateTime = authorizationDateTime;
    }
    public final String getTransactionReferenceNbr() {
        return this.transactionReferenceNbr;
    }
    public final void setTransactionReferenceNbr(final String transactionReferenceNbr) {
        this.transactionReferenceNbr = transactionReferenceNbr;
    }
    public final String getOarData() {
        return this.oarData;
    }
    public final void setOarData(final String oarData) {
        this.oarData = oarData;
    }
    public final String getSpendQualifier() {
        return this.spendQualifier;
    }
    public final void setSpendQualifier(final String spendQualifier) {
        this.spendQualifier = spendQualifier;
    }
    public final String getAssociationName() {
        return this.associationName;
    }
    public final void setAssociationName(final String associationName) {
        this.associationName = associationName;
    }
    public final String getResponseMessage() {
        return this.responseMessage;
    }
    public final void setResponseMessage(final String responseMessage) {
        this.responseMessage = responseMessage;
    }
    public final String getNetAmountSign() {
        return this.netAmountSign;
    }
    public final void setNetAmountSign(final String netAmountSign) {
        this.netAmountSign = netAmountSign;
    }
    public final String getNetAmount() {
        return this.netAmount;
    }
    public final void setNetAmount(final String netAmount) {
        this.netAmount = netAmount;
    }
    public final String getNetCount() {
        return this.netCount;
    }
    public final void setNetCount(final String netCount) {
        this.netCount = netCount;
    }
    public final String getStatus() {
        return this.status;
    }
    public final void setStatus(final String status) {
        this.status = status;
    }
    public final String getResults() {
        return this.results;
    }
    public final void setResults(final String results) {
        this.results = results;
    }
    public final String getTokenAccountStatus() {
        return this.tokenAccountStatus;
    }
    public final void setTokenAccountStatus(final String tokenAccountStatus) {
        this.tokenAccountStatus = tokenAccountStatus;
    }
    public final String getTokenAssuranceLevel() {
        return this.tokenAssuranceLevel;
    }
    public final void setTokenAssuranceLevel(final String tokenAssuranceLevel) {
        this.tokenAssuranceLevel = tokenAssuranceLevel;
    }
    public final String getTokenRequestorID() {
        return this.tokenRequestorID;
    }
    public final void setTokenRequestorID(final String tokenRequestorID) {
        this.tokenRequestorID = tokenRequestorID;
    }
    public final String getPanLastFourDigits() {
        return this.panLastFourDigits;
    }
    public final void setPanLastFourDigits(final String panLastFourDigits) {
        this.panLastFourDigits = panLastFourDigits;
    }
    public final String getTransactionCode() {
        return this.transactionCode;
    }
    public final void setTransactionCode(final String transactionCode) {
        this.transactionCode = transactionCode;
    }
    public final String getCreditSaleAmount() {
        return this.creditSaleAmount;
    }
    public final void setCreditSaleAmount(final String creditSaleAmount) {
        this.creditSaleAmount = creditSaleAmount;
    }
    public final String getCreditSaleCount() {
        return this.creditSaleCount;
    }
    public final void setCreditSaleCount(final String creditSaleCount) {
        this.creditSaleCount = creditSaleCount;
    }
    public final String getCreditReturnAmount() {
        return this.creditReturnAmount;
    }
    public final void setCreditReturnAmount(final String creditReturnAmount) {
        this.creditReturnAmount = creditReturnAmount;
    }
    public final String getCreditReturnCount() {
        return this.creditReturnCount;
    }
    public final void setCreditReturnCount(final String creditReturnCount) {
        this.creditReturnCount = creditReturnCount;
    }
    
    public final ElavonResponseBlock.CurrentBatch getCurrentBatch() {
        return new ElavonResponseBlock.CurrentBatch();
    }

    public final ElavonResponseBlock.BatchCloseResult getBatchCloseResult() {
        return new ElavonResponseBlock.BatchCloseResult();
    }
    
    public class CurrentBatch {
        public final String getBatchNumber() {
            return ElavonResponseBlock.this.batchNumber;
        }
        public final String getResponseMessage() {
            return ElavonResponseBlock.this.responseMessage;
        }
        public final String getNetAmountSign() {
            return ElavonResponseBlock.this.netAmountSign;
        }
    }
    
     
    public class BatchCloseResult {
        public final String getResponseMessage() {
            return ElavonResponseBlock.this.responseMessage;
        }
        public final String getBatchNumber() {
            return ElavonResponseBlock.this.batchNumber;
        }
        public final String getNetAmountSign() {
            return ElavonResponseBlock.this.netAmountSign;
        }
        public final String getNetAmount() {
            return ElavonResponseBlock.this.netAmount;
        }
        public final String getNetCount() {
            return ElavonResponseBlock.this.netCount;
        }
        public final String getCreditSaleAmount() {
            return ElavonResponseBlock.this.creditSaleAmount;
        }
        public final String getCreditSaleCount() {
            return ElavonResponseBlock.this.creditSaleCount;
        }
        public final String getCreditReturnAmount() {
            return ElavonResponseBlock.this.creditReturnAmount;
        }
        public final String getCreditReturnCount() {
            return ElavonResponseBlock.this.creditReturnCount;
        }
        public final ElavonResponseBlock getElavonResponseBlock() {
            return ElavonResponseBlock.this;
        }
    }
}
