package com.digitalpaytech.util.xml;

import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.exception.XMLErrorException;
import com.digitalpaytech.util.CardProcessingUtil;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.math.BigDecimal;

//Copied from EMS 6.3.11 com.digitalpioneer.cardprocessing.creditcardprocessors.concord.XMLConcordBuilder

public class XMLConcordBuilder extends XMLBuilderBase {

	public final static String XML_REQUEST_DTD_NAME = "Request";
	public final static String STORE_ID_ATTRIBUTE_NAME_STRING = "StoreID";
	public final static String STORE_KEY_ATTRIBUTE_NAME_STRING = "StoreKey";
	public final static String APPLICATION_ID_ATTRIBUTE_NAME_STRING = "ApplicationID";

	// preauth stuff
	public final static String CREDIT_CARD_AUTHORIZE_STRING = "CreditCardAuthorize";
	public final static String REFERENCE_NUMBER_STRING = "ReferenceNumber";
	public final static String TRANSACTION_AMOUNT_STRING = "TransactionAmount";
	public final static String ORIGINAL_TRANSACTION_AMOUNT_STRING = "OriginalTransactionAmount";
	public final static String ORIGINAL_TRANSACTION_ID_STRING = "OriginalTransactionID";
	public final static String ACCOUNT_NUMBER_STRING = "AccountNumber";
	public final static String EXPIRATION_MONTH_STRING = "ExpirationMonth";
	public final static String EXPIRATION_YEAR_STRING = "ExpirationYear";
	public final static String START_DATE_STRING = "TransactionDateBegin";
	public final static String END_DATE_STRING = "TransactionDateEnd";
	public final static String TRANSACTION_TYPE_STRING = "TransactionType";
	public final static String APPROVAL_NUMBER_STRING = "ApprovalNumber";
	public final static String TRACK2_STRING = "Track2";
	public final static String CREDIT_CARD_SETTLE_CHARGE_STRING = "CreditCardSettle,CreditCardCharge";

	private final static String XML_DTD_FILE_LOCATION_STRING = "https://efsnet.concordebiz.com/efsnet2.dtd";
	private final static Logger log = Logger.getLogger(XMLConcordBuilder.class);

	private Document xmlConcordDocument = null;
	private Element destinationElement = null;

	public XMLConcordBuilder(String messageDestination) {
		super(XML_DTD_FILE_LOCATION_STRING, messageDestination);
	}

	public void createPreAuthRequest(String cardData, Float transactionAmount,
	        String referenceNumber) {
		// create reference number tag
		createNamedElement(xmlConcordDocument, destinationElement,
		        REFERENCE_NUMBER_STRING, referenceNumber);

		// create transaction amount tag
		createNamedElement(xmlConcordDocument, destinationElement,
		        TRANSACTION_AMOUNT_STRING,
		        transactionAmount.toString());

		// create track 2 data tag...track 2 data is needed for
		// card-swiped/card-present transactions
		createNamedElement(xmlConcordDocument, destinationElement,
		        TRACK2_STRING, cardData);
	}

	public void createChargeRequest(Track2Card track2Card,
			Float transactionAmount, String referenceNumber) {
		// create reference number tag
		createNamedElement(xmlConcordDocument, destinationElement,
		        REFERENCE_NUMBER_STRING, referenceNumber);

		// create transaction amount tag
		createNamedElement(xmlConcordDocument, destinationElement,
		        TRANSACTION_AMOUNT_STRING,
		        transactionAmount.toString());

		// create track 2 data tag...track 2 data is needed for
		// card-swiped/card-present transactions
		if (track2Card.isCardDataCreditCardTrack2Data()) {
			createNamedElement(xmlConcordDocument, destinationElement,
			        TRACK2_STRING, track2Card.getDecryptedCardData());
		} else {
			createNamedElement(xmlConcordDocument, destinationElement,
			        ACCOUNT_NUMBER_STRING, track2Card.getPrimaryAccountNumber());

			createNamedElement(xmlConcordDocument, destinationElement,
			        EXPIRATION_MONTH_STRING, track2Card.getExpirationMonth());

			createNamedElement(xmlConcordDocument, destinationElement,
			        EXPIRATION_YEAR_STRING, track2Card.getExpirationYear());
		}
	}

	public void createPostAuthRequest(PreAuth pd, ProcessorTransaction ptd,
	        String referenceNumber) {
		// New ReferenceNumber
		createNamedElement(xmlConcordDocument, destinationElement,
		        REFERENCE_NUMBER_STRING, referenceNumber);

		// Amount from PreAuth
		BigDecimal amt = CardProcessingUtil.getCentsAmountInDollars(pd.getAmount());
		createNamedElement(xmlConcordDocument, destinationElement,
		        TRANSACTION_AMOUNT_STRING, amt.toString());

		// ProcessorTransactionId from PreAuth
		createNamedElement(xmlConcordDocument, destinationElement,
		        ORIGINAL_TRANSACTION_ID_STRING, pd.getProcessorTransactionId());

		// Amount from PreAuth
		amt = CardProcessingUtil.getCentsAmountInDollars(pd.getAmount());
		createNamedElement(xmlConcordDocument, destinationElement,
		        ORIGINAL_TRANSACTION_AMOUNT_STRING, amt.toString());
	}

	public void createRefundTransactionRequest(ProcessorTransaction ptd,
	        Float amountToRefund, String referenceNumber) {
		createNamedElement(xmlConcordDocument, destinationElement,
		        TRANSACTION_AMOUNT_STRING, amountToRefund.toString());

		createNamedElement(xmlConcordDocument, destinationElement,
		        ORIGINAL_TRANSACTION_ID_STRING, ptd.getProcessorTransactionId());

		BigDecimal amt = CardProcessingUtil.getCentsAmountInDollars(ptd.getAmount());
		createNamedElement(xmlConcordDocument, destinationElement,
		        ORIGINAL_TRANSACTION_AMOUNT_STRING, amt.toString());

		createNamedElement(xmlConcordDocument, destinationElement,
		        REFERENCE_NUMBER_STRING, referenceNumber);
	}

	public void initialize(String storeId, String storeKey, String applicationId)
	        throws XMLErrorException {
		super.initialize();

		xmlConcordDocument = createDocument(storeId, storeKey, applicationId);

		destinationElement = getDestinationNode(xmlConcordDocument);
	}

	protected Document createDocument(String storeId, String storeKey,
	        String applicationId) {
		Document xml_document = documentBuilder.newDocument();

		createDocumentBase(xml_document, storeId, storeKey, applicationId);

		return xml_document;
	}

	public String toString() {
		try {
			return documentToString(xmlConcordDocument);
		} catch (XMLErrorException e) {
			log.error("Failed to serialize document to a string!", e);

			return null;
		}
	}

	protected Element createDocumentBase(Document xmlDocument, String storeId,
	        String storeKey, String applicationId) {
		Element root_element = createNamedElement(xmlDocument, xmlDocument,
		        XML_REQUEST_DTD_NAME);

		root_element.setAttribute(STORE_ID_ATTRIBUTE_NAME_STRING, storeId);

		root_element.setAttribute(STORE_KEY_ATTRIBUTE_NAME_STRING, storeKey);

		root_element.setAttribute(APPLICATION_ID_ATTRIBUTE_NAME_STRING,
		        applicationId);

		return createNamedElement(xmlDocument, root_element, xmlMessageDestination);
	}
}
