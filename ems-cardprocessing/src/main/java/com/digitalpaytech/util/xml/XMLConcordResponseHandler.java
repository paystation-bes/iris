package com.digitalpaytech.util.xml;

import java.lang.reflect.Method;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;

import com.digitalpaytech.cardprocessing.impl.ConcordTxReplyHandler;

// Copied from EMS 6.3.11 com.digitalpioneer.cardprocessing.XMLCCResponseHandler

/**
 * Notes - I removed IXMLCCDataHandler, getCurrentDataHandler, getDataHandler methods and combined existing logic into new ConcordTxReplyHandler.
 * Also TransactionReplyHandler is no longer 'implements IXMLCCDataHandler'. The old logic overall is very confusing.
 */

public class XMLConcordResponseHandler extends XmlSaxHandlerBase {
	private static Logger log = Logger.getLogger(XMLConcordResponseHandler.class);
	private static final String SETTOR_STRING = "set";

	private String currentElementName = null;
	private StringBuffer currentElementValue = null;
	private ConcordTxReplyHandler currentDataHandler = null;
	private int elementLevel = 0;

	public XMLConcordResponseHandler() {
		super();
		currentDataHandler = new ConcordTxReplyHandler();
	}

	public void characters(char[] document, int offset, int length) {
		if (currentDataHandler != null) {
			// append current characters to currentElementValue
			currentElementValue.append(document, offset, length);
		}
	}

	public void startDocument() {
		if (log.isDebugEnabled()) {
			log.debug("Start document.");
		}
	}

	public void startElement(String uri, String localName, String qName,
	        Attributes elementAttributes) {
		elementLevel++;
		currentElementName = qName.trim();

		if (log.isDebugEnabled()) {
			log.debug("Start element: '" + qName + "'");
		}

		// Create new string buffer for this element's value
		currentElementValue = new StringBuffer();
	}

	public void endElement(String uri, String localName, String qName) {
		if (log.isDebugEnabled()) {
			log.debug("End element: '" + qName + "'!");
		}

		if (currentDataHandler != null) {
			// this is not the end of the data handler, this is the end of an
			// element
			if (!qName.equals(currentDataHandler.getRootElementName())) {
				if (currentElementValue.length() > 0) {
					setByVariableName(currentDataHandler, currentElementName,
					        currentElementValue.toString());
				} else {
					if (log.isDebugEnabled()) {
						log.debug("Value is ''.");
					}
				}
			}
		}
		elementLevel--;
	}

	public void endDocument() {
		if (log.isDebugEnabled()) {
			log.debug("End document.");
		}
	}

	private void setByVariableName(Object targetObject, String xmlVariableName,
	        String xmlVariableValue) {
		String method_name = SETTOR_STRING + xmlVariableName;

		try {
			Method method_to_invoke = targetObject.getClass().getMethod(
			        method_name, new Class[] { String.class });

			method_to_invoke.invoke(targetObject,
			        new Object[] { xmlVariableValue });
		} catch (IllegalArgumentException e) {
			log.error("Failed to call method: " + method_name + "("
			        + xmlVariableValue + ")", e);
		} catch (NoSuchMethodException e) {
			if (log.isDebugEnabled()) {
				log.debug("Ignoring method call: " + method_name + "("
				        + xmlVariableValue + ")");
			}
		} catch (java.lang.IllegalAccessException e) {
			log.error("Failed to call method: " + method_name + "("
			        + xmlVariableValue + ")", e);
		} catch (java.lang.reflect.InvocationTargetException e) {
			log.error("Failed to call method: " + method_name + "("
			        + xmlVariableValue + ")", e);
		}
	}

	public ConcordTxReplyHandler getCurrentDataHandler() {
    	return currentDataHandler;
    }
}
