package com.digitalpaytech.util.xml;

import java.math.BigDecimal;

import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.util.CardProcessingUtil;



import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;

import org.apache.log4j.Logger;

public class XMLElavonBuilder{
	private final static Logger log = Logger.getLogger(XMLConcordBuilder.class);
	private final static String TRANSACTION_PREAUTH = "ccauthonly";
	private final static String TRANSACTION_POSTAUTH = "cccomplete";
	private final static String TRANSACTION_CHARGE = "ccsale";
	private final static String TRANSACTION_DELETE = "ccdelete";
	private final static String TRANSACTION_REFUND = "ccreturn";
	private final static String TRANSACTION_ELEMENT_NAME_STRING = "txn";
	
	//used by XMLUtil.hideAllElementValues to prevent card numbers from appearing in log
	public final static String ACCOUNT_NUMBER_STRING = "ssl_card_number"; 
	public final static String TRACK2_STRING = "ssl_track_data";
	public final static String CARD_EXP_STRING = "ssl_exp_date";
	
	// These fields are all used by com.thoughtworks.xstream.XStream, bypassing setters and getters.
	// Ignore the "unused field" warning as the jdk is not aware of xstream's dependency on these fields.
	private String ssl_transaction_type;
	private String ssl_merchant_id;
	private String ssl_user_id;
	private String ssl_pin;
	private String ssl_track_data;
	private String ssl_card_number;
	private String ssl_exp_date;
	private String ssl_amount;
	private String ssl_partial_auth_indicator;
	private String ssl_approval_code;
	private String ssl_txn_id;
	private String ssl_dynamic_dba;
	private String ssl_cvv2cvc2_indicator;
	
	public void addCredentials(String merchantId, String userId, String pin){
		ssl_merchant_id = merchantId;
		ssl_user_id = userId;
		ssl_pin = pin;
	}
	
	public void createChargeRequest(Track2Card track2Card, Float transactionAmount, String referenceNumber, String dynamicDba){
		ssl_transaction_type = TRANSACTION_CHARGE;
		ssl_card_number = getCardNumberFromTrackData(track2Card);
		ssl_exp_date = track2Card.getExpirationMonth() + track2Card.getExpirationYear();
		ssl_amount = transactionAmount.toString();
		ssl_cvv2cvc2_indicator = "0";
		if(dynamicDba != null){
			ssl_dynamic_dba = dynamicDba;
		}
	}
	
	public void createChargeRequest(String cardNumber, String expDate, Float transactionAmount, String referenceNumber, String dynamicDba){
	    ssl_transaction_type = TRANSACTION_CHARGE;
        ssl_card_number = cardNumber;
        ssl_exp_date = expDate;
        ssl_amount = transactionAmount.toString();
        ssl_cvv2cvc2_indicator = "0";
        if(dynamicDba != null){
            ssl_dynamic_dba = dynamicDba;
        }
	}
	
	public void createPreAuthRequest(String trackData, Float transactionAmount, String dynamicDba){
		ssl_transaction_type = TRANSACTION_PREAUTH;
		ssl_amount = transactionAmount.toString();
		ssl_track_data = ";" + trackData + "?"; // semicolon and question mark sentinels required by virtual merchant
		ssl_partial_auth_indicator = "1"; // Elavon requires us to always send requests with partial authorization approval - 
												//delete after if not authorized in full using ssl_transaction_type ccdelete
		ssl_cvv2cvc2_indicator = "0"; // 
		if(dynamicDba != null){
			ssl_dynamic_dba = dynamicDba;
		}
	}
	
	public void createPostAuthRequest(PreAuth pd, ProcessorTransaction ptd,
	        String approvalCode, String pan, String exp, String dynamicDba){
		ssl_transaction_type = TRANSACTION_POSTAUTH;
		ssl_txn_id = pd.getProcessorTransactionId();
		BigDecimal amt = CardProcessingUtil.getCentsAmountInDollars(pd.getAmount());
		ssl_amount = amt.toString();
		if(dynamicDba != null){
			ssl_dynamic_dba = dynamicDba;
		}
	}
	
	public void createRefundTransactionRequest(String txnId, int amount, String dynamicDba){
		ssl_transaction_type = TRANSACTION_REFUND;
		ssl_txn_id = txnId;
		BigDecimal amt = CardProcessingUtil.getCentsAmountInDollars(amount);
		ssl_amount = amt.toString();
		if(dynamicDba != null){
			ssl_dynamic_dba = dynamicDba;
		}
	}
	
	public void createTransactionReversalRequest(String txnId){
		ssl_transaction_type = TRANSACTION_DELETE;
		ssl_txn_id = txnId;
	}
	
	public String toString() {
		try {
			XStream xstream = new XStream(new DomDriver("UTF-8", new XmlFriendlyNameCoder("_-", "_")));
			xstream.alias(TRANSACTION_ELEMENT_NAME_STRING, XMLElavonBuilder.class);
			
			String xmlstring = xstream.toXML(this);
			
			return xmlstring;
		} catch (Exception e) {
			log.error("Failed to serialize document to a string!", e);
			return null;
		}
	}
	
	private String getCardNumberFromTrackData(Track2Card track2Card){
		String response = null;
		int len = track2Card.getAccountNumberLength();
		String cardData = track2Card.getDecryptedCardData();
		response = cardData.substring(0, len);
		return response;
	}
}
