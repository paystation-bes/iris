package com.digitalpaytech.util.xml;

import java.util.List;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("Response")
public class ElavonViaConexResponse {
    private static final String DATE_TIME_FORMAT = "MMddyy hhmmss";
    
    @XStreamImplicit
    private List<ElavonResponseBlock> blocks;
    private ElavonResponseBlock dataBlock;
    
    private String errorString;
    
    public final void organizeData() {
        this.dataBlock = new ElavonResponseBlock();
        for (ElavonResponseBlock b : this.blocks) {
            if ("02".equalsIgnoreCase(b.getId())) {
                this.dataBlock.setResponseCode(b.getResponseCode());
                this.dataBlock.setIssuerResponseCode(b.getIssuerResponseCode());
                this.dataBlock.setAuthorizationSource(b.getAuthorizationSource());
                this.dataBlock.setCaptureCode(b.getCaptureCode());
                this.dataBlock.setApprovalCode(b.getApprovalCode());
                this.dataBlock.setAuthorizationDate(b.getAuthorizationDate());
                this.dataBlock.setAuthorizationTime(b.getAuthorizationTime());
                this.dataBlock.setAuthorizationDateTime(combineDateAndTime(b.getAuthorizationDate(), b.getAuthorizationTime()));
                this.dataBlock.setBatchNumber(b.getBatchNumber());
                this.dataBlock.setRecordNumber(b.getRecordNumber());
                this.dataBlock.setAuthorizationResponse(b.getAuthorizationResponse());
                this.dataBlock.setTraceNumber(b.getTraceNumber());
                this.dataBlock.setTransactionReferenceNbr(b.getTransactionReferenceNbr());
            } else if ("80".equalsIgnoreCase(b.getId())) {
                this.dataBlock.setAmexCaptureCode(b.getAmexCaptureCode());
                this.dataBlock.setAvsResponse(b.getAvsResponse());
                this.dataBlock.setCvv2Response(b.getCvv2Response());
                this.dataBlock.setPs2000Data(b.getPs2000Data());
                this.dataBlock.setMsdi(b.getMsdi());
            } else if ("87".equalsIgnoreCase(b.getId())) {
                this.dataBlock.setAuthorizedAmount(b.getAuthorizedAmount());
                this.dataBlock.setAccountBalance1(b.getAccountBalance1());
                this.dataBlock.setAccountBalance2(b.getAccountBalance2());
            } else if ("8F".equalsIgnoreCase(b.getId())) {
                this.dataBlock.setTokenAccountStatus(b.getTokenAccountStatus());
                this.dataBlock.setTokenAssuranceLevel(b.getTokenAssuranceLevel());
                this.dataBlock.setTokenRequestorID(b.getTokenRequestorID());
                this.dataBlock.setPanLastFourDigits(b.getPanLastFourDigits());
            } else if ("89".equalsIgnoreCase(b.getId())) {
                this.dataBlock.setBatchNumber(b.getBatchNumber());
                this.dataBlock.setResponseMessage(b.getResponseMessage());
                this.dataBlock.setNetAmount(b.getNetAmount());
                this.dataBlock.setNetAmountSign(b.getNetAmountSign());
                this.dataBlock.setCreditSaleAmount(b.getCreditSaleAmount());
                this.dataBlock.setCreditSaleCount(b.getCreditSaleCount());
                this.dataBlock.setCreditReturnAmount(b.getCreditReturnAmount());
                this.dataBlock.setCreditReturnCount(b.getCreditReturnCount());
            }
        }
    }
    
    private Date combineDateAndTime(final String date, final String time) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append(date).append(" ").append(time);
        final SimpleDateFormat formatter = new SimpleDateFormat(DATE_TIME_FORMAT);
        try {
            final Date dateObj = formatter.parse(bdr.toString());
            return dateObj;
        } catch (ParseException pe) {
            return null;
        }
    }
    
    public final List<ElavonResponseBlock> getBlocks() {
        return this.blocks;
    }
    
    public final void setBlocks(final List<ElavonResponseBlock> blocks) {
        this.blocks = blocks;
    }
    
    public final ElavonResponseBlock getDataBlock() {
        return this.dataBlock;
    }
    
    public final void setDataBlock(final ElavonResponseBlock elavonResponseBlock) {
        this.dataBlock = elavonResponseBlock;
    }
    
    public final String getErrorString() {
        return this.errorString;
    }
    
    public void setErrorString(final String errorString) {
        this.errorString = errorString;
    }
}
