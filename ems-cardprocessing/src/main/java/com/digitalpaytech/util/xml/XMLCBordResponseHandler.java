package com.digitalpaytech.util.xml;

import org.apache.log4j.Logger;

import com.digitalpaytech.cardprocessing.impl.CBordBalanceReqReply;
import com.digitalpaytech.cardprocessing.impl.CBordTransactionReply;
import com.digitalpaytech.cardprocessing.impl.CBordTxReplyHandler;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.mapper.MapperWrapper;

public class XMLCBordResponseHandler {
	private static Logger log = Logger
			.getLogger(XMLConcordResponseHandler.class);
	private CBordTxReplyHandler currentDataHandler = null;

	public CBordTxReplyHandler getCurrentDataHandler() {
		return currentDataHandler;
	}

	public void processTransactionRequest(String inputString) {

		// Use the xstream to convert Elavon response to ElavonTxReplyHandler
		// the custom StaxDriver defined below is to prevent an exception being
		// thrown when XStream encounters a field name
		// which does not exist in the ElavonTxReplyHandler.
		// copied from
		// http://stackoverflow.com/questions/4409717/java-xstream-ignore-tag-that-doesnt-exist-in-xml
		XStream xstream = new XStream(new StaxDriver()) {
			@Override
			protected MapperWrapper wrapMapper(MapperWrapper next) {
				return new MapperWrapper(next) {
					@Override
					public boolean shouldSerializeMember(Class definedIn,
							String fieldName) {
						if (definedIn == Object.class) {
							return false;
						}
						return super
								.shouldSerializeMember(definedIn, fieldName);
					}
				};
			}
		};
		xstream.alias("Message", CBordTxReplyHandler.class);
		xstream.aliasField("CSGoldMessages", CBordTxReplyHandler.class,
				"replyTx");
		xstream.aliasField("Class", CBordTransactionReply.class, "className");

		try {
			currentDataHandler = (CBordTxReplyHandler) xstream
					.fromXML(inputString);
		} catch (Exception e) {
			log.error(e);
		}
		if (currentDataHandler == null) {
			log.info("Elavon Reply = null");
		} else {
			log.info("Elavon Reply = " + currentDataHandler.toString());
		}
	}

	public void processBalanceRequest(String inputString) {

		XStream xstream = new XStream(new StaxDriver()) {
			@Override
			protected MapperWrapper wrapMapper(MapperWrapper next) {
				return new MapperWrapper(next) {
					@Override
					public boolean shouldSerializeMember(Class definedIn,
							String fieldName) {
						if (definedIn == Object.class) {
							return false;
						}
						return super
								.shouldSerializeMember(definedIn, fieldName);
					}
				};
			}
		};
		xstream.alias("Message", CBordTxReplyHandler.class);

		xstream.aliasField("CSGoldMessages", CBordTxReplyHandler.class,
				"balanceReq");

		xstream.aliasField("Class", CBordBalanceReqReply.class, "className");

		try {
			currentDataHandler = (CBordTxReplyHandler) xstream
					.fromXML(inputString);
		} catch (Exception e) {
			log.error(e);
		}
		if (currentDataHandler == null) {
			log.info("Elavon Reply = null");
		} else {
			log.info("Elavon Reply = " + currentDataHandler.toString());
		}
	}

}
