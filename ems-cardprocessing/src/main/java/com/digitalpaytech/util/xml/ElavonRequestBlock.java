package com.digitalpaytech.util.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("Block")
public final class ElavonRequestBlock {
    @XStreamAlias("Network_Status_Byte")
    private String txProcessingNetworkStatusByte;
    @XStreamAlias("Application_ID")
    private String applicationId;
    @XStreamAlias("Terminal_ID")
    private String terminalId;
    @XStreamAlias("POS_Entry_Capability")
    private String posEntryCapability;
    @XStreamAlias("Account_Entry_Mode")
    private String accountEntryMode;
    @XStreamAlias("Partial_Auth_Indicator")
    private String partialAuthIndicator;
    @XStreamAlias("Account_Data")
    private String accountData;
    @XStreamAlias("Transaction_Amount")
    private String transactionAmount;
    @XStreamAlias("Last_Record_Number")
    private String lastRecordNumber;
    @XStreamAlias("Association_Token_Indicator")
    private String associationToken;
    @XStreamAlias("Merchant_Reference_Nbr")
    private String merchantReferenceNbr;
    @XStreamAlias("Approval_Code")
    private String approvalCode;
    @XStreamAlias("PIN_Entry_Capability")
    private String pinEntryCapability;
    @XStreamAlias("Original_Auth_Amount")
    private String originalAuthAmount;
    @XStreamAlias("Terminal_Type")
    private String terminalType;
    @XStreamAlias("CAT_Indicator")
    private String catIndicator;
    @XStreamAlias("Batch_Number")
    private String batchNumber;
    @XStreamAlias("Record_Number")
    private String recordNumber;
    @XStreamAlias("Record_Count")
    private String recordCount;   
    @XStreamAlias("Net_Amount")
    private String netAmount;
    @XStreamAlias("Net_Tip_Amount")
    private String netTipAmount;
    @XStreamAlias("Transaction_Code")
    private String transactionCode;
    @XStreamAlias("Authorization_Date")
    private String authorizationDate;
    @XStreamAlias("Authorization_Time")
    private String authorizationTime;
    @XStreamAlias("Authorization_Source")
    private String authorizationSource;
    @XStreamAlias("Account_Source")
    private String accountSource;
    @XStreamAlias("Response_Code")
    private String responseCode;
    @XStreamAlias("Capture_Tran_Code")
    private String captureTranCode;
    @XStreamAlias("Token_Indicator")
    private String tokenIndicator;
    @XStreamAlias("Card_ID")
    private String cardID;
    @XStreamAlias("Service_Code")
    private String serviceCode;
    @XStreamAlias("AVS_Response")
    private String avsResponse;
    @XStreamAlias("CVV2_Response")
    private String cvv2Response;
    @XStreamAlias("PS2000_Data")
    private String ps2000Data;
    @XStreamAlias("MSDI")
    private String msdi;
    @XStreamAlias("Trace_Number")
    private String traceNumber;
    @XStreamAlias("Transaction_Reference_Nbr")
    private String transactionReferenceNbr;
    @XStreamAlias("Transmission_Date")
    private String transmissionDate;
    @XStreamAlias("Net_Deposit")
    private String netDeposit;
    @XStreamAlias("Hash_Total")
    private String hashTotal;
    
    @XStreamAsAttribute
    private String id;
    
    public String getTxProcessingNetworkStatusByte() {
        return this.txProcessingNetworkStatusByte;
    }
    public void setTxProcessingNetworkStatusByte(final String txProcessingNetworkStatusByte) {
        this.txProcessingNetworkStatusByte = txProcessingNetworkStatusByte;
    }
    public String getApplicationId() {
        return this.applicationId;
    }
    public void setApplicationId(final String applicationId) {
        this.applicationId = applicationId;
    }
    public String getTerminalId() {
        return this.terminalId;
    }
    public void setTerminalId(final String terminalId) {
        this.terminalId = terminalId;
    }
    public String getPosEntryCapability() {
        return this.posEntryCapability;
    }
    public void setPosEntryCapability(final String posEntryCapability) {
        this.posEntryCapability = posEntryCapability;
    }
    public String getAccountData() {
        return this.accountData;
    }
    public void setAccountData(final String accountData) {
        this.accountData = accountData;
    }
    public String getMerchantReferenceNbr() {
        return this.merchantReferenceNbr;
    }
    public void setMerchantReferenceNbr(final String merchantReferenceNbr) {
        this.merchantReferenceNbr = merchantReferenceNbr;
    }
    public String getId() {
        return this.id;
    }
    public void setId(final String id) {
        this.id = id;
    }
    public String getAssociationToken() {
        return this.associationToken;
    }
    public void setAssociationToken(final String associationToken) {
        this.associationToken = associationToken;
    }
    public String getPartialAuthIndicator() {
        return this.partialAuthIndicator;
    }
    public void setPartialAuthIndicator(final String partialAuthIndicator) {
        this.partialAuthIndicator = partialAuthIndicator;
    }
    public String getAccountEntryMode() {
        return this.accountEntryMode;
    }
    public void setAccountEntryMode(final String accountEntryMode) {
        this.accountEntryMode = accountEntryMode;
    }
    public String getTransactionAmount() {
        return this.transactionAmount;
    }
    public void setTransactionAmount(final String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }
    public String getLastRecordNumber() {
        return this.lastRecordNumber;
    }
    public void setLastRecordNumber(final String lastRecordNumber) {
        this.lastRecordNumber = lastRecordNumber;
    }
    public String getApprovalCode() {
        return this.approvalCode;
    }
    public void setApprovalCode(final String approvalCode) {
        this.approvalCode = approvalCode;
    }
    public String getPinEntryCapability() {
        return this.pinEntryCapability;
    }
    public void setPinEntryCapability(final String pinEntryCapability) {
        this.pinEntryCapability = pinEntryCapability;
    }
    public String getOriginalAuthAmount() {
        return this.originalAuthAmount;
    }
    public void setOriginalAuthAmount(final String originalAuthAmount) {
        this.originalAuthAmount = originalAuthAmount;
    }
    public String getTerminalType() {
        return this.terminalType;
    }
    public void setTerminalType(final String terminalType) {
        this.terminalType = terminalType;
    }
    public String getCatIndicator() {
        return this.catIndicator;
    }
    public void setCatIndicator(final String catIndicator) {
        this.catIndicator = catIndicator;
    }
    public String getBatchNumber() {
        return this.batchNumber;
    }
    public void setBatchNumber(final String batchNumber) {
        this.batchNumber = batchNumber;
    }
    public String getRecordNumber() {
        return this.recordNumber;
    }
    public void setRecordNumber(final String recordNumber) {
        this.recordNumber = recordNumber;
    }
    public String getRecordCount() {
        return this.recordCount;
    }
    public void setRecordCount(final String recordCount) {
        this.recordCount = recordCount;
    }
    public String getNetAmount() {
        return this.netAmount;
    }
    public void setNetAmount(final String netAmount) {
        this.netAmount = netAmount;
    }
    public String getNetTipAmount() {
        return this.netTipAmount;
    }
    public void setNetTipAmount(final String netTipAmount) {
        this.netTipAmount = netTipAmount;
    }
    public String getTransactionCode() {
        return this.transactionCode;
    }
    public void setTransactionCode(final String transactionCode) {
        this.transactionCode = transactionCode;
    }
    public String getAuthorizationDate() {
        return this.authorizationDate;
    }
    public void setAuthorizationDate(final String authorizationDate) {
        this.authorizationDate = authorizationDate;
    }
    public String getAuthorizationTime() {
        return this.authorizationTime;
    }
    public void setAuthorizationTime(final String authorizationTime) {
        this.authorizationTime = authorizationTime;
    }
    public String getAuthorizationSource() {
        return this.authorizationSource;
    }
    public void setAuthorizationSource(final String authorizationSource) {
        this.authorizationSource = authorizationSource;
    }
    public String getAccountSource() {
        return this.accountSource;
    }
    public void setAccountSource(final String accountSource) {
        this.accountSource = accountSource;
    }
    public String getCaptureTranCode() {
        return this.captureTranCode;
    }
    public void setCaptureTranCode(final String captureTranCode) {
        this.captureTranCode = captureTranCode;
    }
    public String getTokenIndicator() {
        return this.tokenIndicator;
    }
    public void setTokenIndicator(final String tokenIndicator) {
        this.tokenIndicator = tokenIndicator;
    }
    public String getCardID() {
        return this.cardID;
    }
    public void setCardID(final String cardID) {
        this.cardID = cardID;
    }
    public String getServiceCode() {
        return this.serviceCode;
    }
    public void setServiceCode(final String serviceCode) {
        this.serviceCode = serviceCode;
    }
    public String getPs2000Data() {
        return this.ps2000Data;
    }
    public void setPs2000Data(final String ps2000Data) {
        this.ps2000Data = ps2000Data;
    }
    public String getMsdi() {
        return this.msdi;
    }
    public void setMsdi(final String msdi) {
        this.msdi = msdi;
    }
    public String getTransmissionDate() {
        return this.transmissionDate;
    }
    public void setTransmissionDate(final String transmissionDate) {
        this.transmissionDate = transmissionDate;
    }
    public String getNetDeposit() {
        return this.netDeposit;
    }
    public void setNetDeposit(final String netDeposit) {
        this.netDeposit = netDeposit;
    }
    public String getHashTotal() {
        return this.hashTotal;
    }
    public void setHashTotal(final String hashTotal) {
        this.hashTotal = hashTotal;
    }
    public String getResponseCode() {
        return this.responseCode;
    }
    public void setResponseCode(final String responseCode) {
        this.responseCode = responseCode;
    }
    public String getTraceNumber() {
        return this.traceNumber;
    }
    public void setTraceNumber(final String traceNumber) {
        this.traceNumber = traceNumber;
    }
    public String getTransactionReferenceNbr() {
        return this.transactionReferenceNbr;
    }
    public void setTransactionReferenceNbr(final String transactionReferenceNbr) {
        this.transactionReferenceNbr = transactionReferenceNbr;
    }
    public String getAvsResponse() {
        return this.avsResponse;
    }
    public void setAvsResponse(final String avsResponse) {
        this.avsResponse = avsResponse;
    }
    public String getCvv2Response() {
        return this.cvv2Response;
    }
    public void setCvv2Response(final String cvv2Response) {
        this.cvv2Response = cvv2Response;
    }
}
