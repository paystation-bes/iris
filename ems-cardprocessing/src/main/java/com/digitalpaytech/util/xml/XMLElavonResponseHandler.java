package com.digitalpaytech.util.xml;

import org.apache.log4j.Logger;

import com.digitalpaytech.cardprocessing.impl.ElavonTxReplyHandler;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.mapper.MapperWrapper;
/**
 * Given an XML-formatted string, populates a ElavonTxReplyHandler with the important response data from Elavon.
 * This is styled after the XMLConcordResponseHandler, but uses XStream API rather than the SAX XML parser
 * @author danielm
 *
 */
public class XMLElavonResponseHandler{
	private static Logger log = Logger.getLogger(XMLConcordResponseHandler.class);
	private ElavonTxReplyHandler currentDataHandler = null;
	
	public ElavonTxReplyHandler getCurrentDataHandler() {
    	return currentDataHandler;
    }
	public void process(String inputString){
		
		//Use the xstream to convert Elavon response to ElavonTxReplyHandler
		// the custom StaxDriver defined below is to prevent an exception being thrown when XStream encounters a field name
		// which does not exist in the ElavonTxReplyHandler.
		// copied from http://stackoverflow.com/questions/4409717/java-xstream-ignore-tag-that-doesnt-exist-in-xml
		XStream xstream = new XStream(new StaxDriver()){
			@Override
			protected MapperWrapper wrapMapper(MapperWrapper next){
				return new MapperWrapper(next){
					@Override
					public boolean shouldSerializeMember(Class definedIn, String fieldName){
						if(definedIn == Object.class){
							return false;
						}
						return super.shouldSerializeMember(definedIn, fieldName);
					}
				};
			}
		};
		xstream.alias("txn", ElavonTxReplyHandler.class);
		
		try{
			currentDataHandler = (ElavonTxReplyHandler) xstream.fromXML(inputString);
		}catch(Exception e){
			log.error(e);
		}
		if(currentDataHandler == null){
			log.info("Elavon Reply = null");
		}else{
			log.info("Elavon Reply = " + currentDataHandler.toString());
		}
	}
}
