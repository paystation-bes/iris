package com.digitalpaytech.util.xml;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;

/**
 * 
 * This builds the XML's required for communication with CBORD. It has 3 inner
 * classes(POJO's) representing Balance Request XML , Transactional request &
 * acknowledgement XML.
 * 
 * Ignore checkstyle violations on variable names conventions they r formatted
 * to be extactly as needed in the XML elements.
 * 
 * @author harshd
 * 
 */
public class XMLCBordBuilder {

	private final static Logger log = Logger.getLogger(XMLCBordBuilder.class);

	public static final String PREAUTHORIZATION_CODE_REQUEST = "009999";
	public static final String ACKNOWLEDGE_CODE_REQUEST = "900000";
	public static final String BALANCE_CHECK_CODE_REQEUST = "319090";
	public static final String BALANCE_CHECK_CODE_REQUEST_ODY = "319999";
	public static final String CURRENT_PROCESSOR_GOLD = "Gold";
	public static final String MEDIA_ENTRY_MAGNETIC_SWIPE = "020";
	public static final String OPERATOR_DEFAULT = "1";

	public static final String TRANSMISSION_DATE_FORMAT = "MMddHHmmss";

	CSGoldMessagesPreAuth preAuthCBord;
	CSGoldMessagesAcknowledgment postAuthXMLCBord;
	CSGoldMessagesBalanceRequest balanceRequestCBord;

	private Document xmlCBordDocument = null;
	private Element destinationElement = null;

	private static final String CLASS_NAME = "0200";

	public void setPreAuthCBord(CSGoldMessagesPreAuth preAuthCBord) {
		this.preAuthCBord = preAuthCBord;
	}

	public void setPostAuthXMLCBord(
			CSGoldMessagesAcknowledgment postAuthXMLCBord) {
		this.postAuthXMLCBord = postAuthXMLCBord;
	}

	public void setBalanceRequestCBord(
			CSGoldMessagesBalanceRequest balanceRequestCBord) {
		this.balanceRequestCBord = balanceRequestCBord;
	}

	public final String createTransRequestXML(final PreAuth preAuth,
			final String dateTime, final String provider,
			final String location, final String codeMap,
			final String currentProcessor, final String referenceNumber) {

		final StringTokenizer stk = new StringTokenizer(dateTime, ":");
		final String localTransactionYear = stk.nextToken();
		final String localTransactionDate = stk.nextToken();
		final String localTransactionTime = stk.nextToken();

		final SimpleDateFormat dateFormat = new SimpleDateFormat(
				TRANSMISSION_DATE_FORMAT);
		final String transmissionDate = dateFormat.format(new Date());

		final String amount = createTransactionAmount(
				Integer.toString(preAuth.getAmount()), currentProcessor);

		this.preAuthCBord = new CSGoldMessagesPreAuth();
		this.preAuthCBord.setClassName(CLASS_NAME);
		this.preAuthCBord.setCode(PREAUTHORIZATION_CODE_REQUEST);
		this.preAuthCBord.setProvider(provider);
		this.preAuthCBord.setLocation(location);
		this.preAuthCBord.setCodeMap(codeMap);
		this.preAuthCBord.setLocalTime(localTransactionTime);
		this.preAuthCBord.setLocalDate(localTransactionDate);
		this.preAuthCBord.setMediaValue(preAuth.getCardData());
		this.preAuthCBord.setOperator(OPERATOR_DEFAULT);
		this.preAuthCBord.setMediaEntry(MEDIA_ENTRY_MAGNETIC_SWIPE);
		this.preAuthCBord.setYear(localTransactionYear);
		this.preAuthCBord.setTranAmount(amount);
		this.preAuthCBord.setTransmissionDateTime(transmissionDate);
		this.preAuthCBord.setDupCheckID(referenceNumber);
		this.preAuthCBord.setSysTraceAuditNumber(referenceNumber.substring(
				referenceNumber.length() - 6, referenceNumber.length()));

		final XStream xstream = new XStream(new DomDriver("UTF-8",
				new XmlFriendlyNameCoder("_-", "_")));
		xstream.alias("Message", XMLCBordBuilder.class);
		xstream.aliasField("CSGoldMessages", XMLCBordBuilder.class,
				"preAuthCBord");
		xstream.aliasField("Class", CSGoldMessagesPreAuth.class, "className");

		return xstream.toXML(this);
	}

	private String createTransactionAmount(final String amount,
			final String currentProcessor) {
		// This can change in the future check CBORD specs.

		int transactionAmountLength = 0;
		if (currentProcessor.equalsIgnoreCase(CURRENT_PROCESSOR_GOLD)) {
			transactionAmountLength = 12;
		} else {
			transactionAmountLength = 12;
		}

		StringBuilder amountString = new StringBuilder();
		amountString.append(amount);
		int length = amountString.length();
		while (length != transactionAmountLength) {
			amountString.insert(0, "0");
			length++;
		}

		return amountString.insert(0, "USDC").toString();
	}

	public final String createAckXML(final PreAuth preAuth,
			final ProcessorTransaction processorTransaction,
			final String provider) {

		this.postAuthXMLCBord = new CSGoldMessagesAcknowledgment();
		this.postAuthXMLCBord.setClassName(CLASS_NAME);
		this.postAuthXMLCBord.setCode(ACKNOWLEDGE_CODE_REQUEST);
		this.postAuthXMLCBord.setProvider(provider);
		this.postAuthXMLCBord.setTransID(preAuth.getAuthorizationNumber());

		XStream xstream = new XStream(new DomDriver("UTF-8",
				new XmlFriendlyNameCoder("_-", "_")));
		xstream.alias("Message", XMLCBordBuilder.class);

		xstream.aliasField("CSGoldMessages", XMLCBordBuilder.class,
				"postAuthXMLCBord");
		xstream.aliasField("Class", CSGoldMessagesAcknowledgment.class,
				"className");
		return xstream.toXML(this);

	}

	public final String createBalanceRequestXML(final String provider,
			final PreAuth preAuth, final String codeMap, final String location,
			final String referenceNumber) {

		this.balanceRequestCBord = new CSGoldMessagesBalanceRequest();
		this.balanceRequestCBord.setClassName(CLASS_NAME);
		this.balanceRequestCBord.setCode(BALANCE_CHECK_CODE_REQUEST_ODY);
		this.balanceRequestCBord.setMediaValue(preAuth.getCardData());
		this.balanceRequestCBord.setProvider(provider);
		this.balanceRequestCBord.setCodeMap(codeMap);
		this.balanceRequestCBord.setLocation(location);
		this.balanceRequestCBord.setMediaEntry(MEDIA_ENTRY_MAGNETIC_SWIPE);
		this.balanceRequestCBord.setSysTraceAuditNumber(referenceNumber
				.substring(referenceNumber.length() - 6,
						referenceNumber.length()));

		XStream xstream = new XStream(new DomDriver("UTF-8",
				new XmlFriendlyNameCoder("_-", "_")));
		xstream.alias("Message", XMLCBordBuilder.class);

		xstream.aliasField("CSGoldMessages", XMLCBordBuilder.class,
				"balanceRequestCBord");
		xstream.aliasField("Class", CSGoldMessagesBalanceRequest.class,
				"className");
		return xstream.toXML(this);
	}
}

class CSGoldMessagesBalanceRequest {

	private String className;
	private String Code;
	private String CodeMap;
	private String Location;
	private String Provider;
	private String MediaValue;
	private String MediaEntry;
	private String SysTraceAuditNumber;

	public String getLocation() {
		return Location;
	}

	public void setLocation(String location) {
		Location = location;
	}

	public String getMediaEntry() {
		return MediaEntry;
	}

	public void setMediaEntry(String mediaEntry) {
		MediaEntry = mediaEntry;
	}

	public String getSysTraceAuditNumber() {
		return SysTraceAuditNumber;
	}

	public void setSysTraceAuditNumber(String sysTraceAuditNumber) {
		SysTraceAuditNumber = sysTraceAuditNumber;
	}

	public String getCodeMap() {
		return CodeMap;
	}

	public void setCodeMap(String codeMap) {
		CodeMap = codeMap;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public void setCode(String code) {
		Code = code;
	}

	public void setProvider(String provider) {
		Provider = provider;
	}

	public void setMediaValue(String mediaValue) {
		MediaValue = mediaValue;
	}

	public String getClassName() {
		return className;
	}

	public String getCode() {
		return Code;
	}

	public String getProvider() {
		return Provider;
	}

	public String getMediaValue() {
		return MediaValue;
	}

}

class CSGoldMessagesAcknowledgment {
	private String className;
	private String Code;
	private String TransID;
	private String Provider;

	public void setClassName(String className) {
		this.className = className;
	}

	public void setCode(String code) {
		Code = code;
	}

	public void setTransID(String transID) {
		TransID = transID;
	}

	public void setProvider(String provider) {
		Provider = provider;
	}

	public String getClassName() {
		return className;
	}

	public String getCode() {
		return Code;
	}

	public String getTransID() {
		return TransID;
	}

	public String getProvider() {
		return Provider;
	}

}

class CSGoldMessagesPreAuth {
	private String className;
	private String Code;
	private String CodeMap;
	private String Provider;
	private String MediaValue;
	private String MediaEntry;
	private String Location;
	private String Operator;
	private String LocalTime;
	private String LocalDate;
	private String Year;
	private String TransmissionDateTime;
	private String TranAmount;
	private String SysTraceAuditNumber;
	private String DupCheckID;

	public String getCodeMap() {
		return CodeMap;
	}

	public void setCodeMap(String codeMap) {
		CodeMap = codeMap;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getCode() {
		return Code;
	}

	public void setCode(String Code) {
		this.Code = Code;
	}

	public String getProvider() {
		return Provider;
	}

	public void setProvider(String provider) {
		this.Provider = provider;
	}

	public String getMediaValue() {
		return MediaValue;
	}

	public void setMediaValue(String mediaValue) {
		this.MediaValue = mediaValue;
	}

	public String getMediaEntry() {
		return MediaEntry;
	}

	public void setMediaEntry(String mediaEntry) {
		this.MediaEntry = mediaEntry;
	}

	public String getLocation() {
		return Location;
	}

	public void setLocation(String location) {
		this.Location = location;
	}

	public String getOperator() {
		return Operator;
	}

	public void setOperator(String operator) {
		this.Operator = operator;
	}

	public String getLocalTime() {
		return LocalTime;
	}

	public void setLocalTime(String localTime) {
		this.LocalTime = localTime;
	}

	public String getLocalDate() {
		return LocalDate;
	}

	public void setLocalDate(String localDate) {
		this.LocalDate = localDate;
	}

	public String getYear() {
		return Year;
	}

	public void setYear(String year) {
		this.Year = year;
	}

	public String getTransmissionDateTime() {
		return TransmissionDateTime;
	}

	public void setTransmissionDateTime(String transmissionDateTime) {
		this.TransmissionDateTime = transmissionDateTime;
	}

	public String getTranAmount() {
		return TranAmount;
	}

	public void setTranAmount(String tranAmount) {
		this.TranAmount = tranAmount;
	}

	public String getSysTraceAuditNumber() {
		return SysTraceAuditNumber;
	}

	public void setSysTraceAuditNumber(String sysTraceAuditNumber) {
		this.SysTraceAuditNumber = sysTraceAuditNumber;
	}

	public String getDupCheckID() {
		return DupCheckID;
	}

	public void setDupCheckID(String dupCheckID) {
		this.DupCheckID = dupCheckID;
	}

}