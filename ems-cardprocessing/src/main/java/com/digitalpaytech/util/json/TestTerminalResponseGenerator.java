package com.digitalpaytech.util.json;

import org.apache.log4j.Logger;

import com.digitalpaytech.exception.JsonException;

public class TestTerminalResponseGenerator {
    private static final Logger LOG = Logger.getLogger(TestTerminalResponseGenerator.class);
    private Status status;
    
    public static final TestTerminalResponseGenerator generate(final String jsonString) throws JsonException {
        final JSON json = new JSON();
        try {
            return json.deserialize(jsonString, TestTerminalResponseGenerator.class);
        } catch (JsonException jsone) {
            final String msg = "Cannot deserialize JSON document: " + jsonString;
            LOG.error(msg, jsone);
            throw new JsonException(jsone);
        }
    }
    
    public final Status getStatus() {
        return this.status;
    }
    
    public final void setStatus(final Status status) {
        this.status = status;
    }
    
    public final String getStatusCode() {
        if (this.status != null) {
            return this.status.getCode();
        }
        return null;
    }
}
