package com.digitalpaytech.util.json;

import com.digitalpaytech.exception.JsonException;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.log4j.Logger;

/**
 * e.g.
 * {
 *   "status":
 *   {
 *     "code": "accepted/declined",
 *     "message": ""
 *   },
 *   "chargeToken" : "unique token for the refund",
 *   "processorTransactionId" : "some number?",
 *   "authorizationNumber" : "some number?",
 *   "processedDate" : "some date in UTC",
 *   "referenceNumber": "some alphanumeric" 
 * }
 *
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class CardTransactionResponseGenerator {
    private static final Logger LOG = Logger.getLogger(CardTransactionResponseGenerator.class);
   
    @JsonProperty("status")
    private Status status;
    @JsonProperty("chargeToken")
    private String chargeToken;
    @JsonProperty("processorTransactionId")
    private String processorTransactionId;
    @JsonProperty("authorizationNumber")
    private String authorizationNumber;
    @JsonProperty("processedDate")
    private String processedDate;
    @JsonProperty("referenceNumber")
    private String referenceNumber;
    
    public CardTransactionResponseGenerator() {
    }
    
    public CardTransactionResponseGenerator(final Status status) {
        this.status = status;
    }
    
    public static final CardTransactionResponseGenerator generate(final String jsonString) throws JsonException {
        final JSON json = new JSON();
        try {
            return json.deserialize(jsonString, CardTransactionResponseGenerator.class);
        } catch (JsonException jsone) {
            final String msg = "Cannot deserialize JSON document: " + jsonString;
            LOG.error(msg, jsone);
            throw new JsonException(jsone);
        }
    }

    @Override
    public final String toString() {
        final StringBuilder bdr = new StringBuilder();
        if (this.status != null) {
            bdr.append("status code: ").append(this.status.getCode()).append(", status message: ").append(this.status.getMessage());
        } else {
            bdr.append("status is null.");
        }
        bdr.append(", chargeToken: ").append(this.chargeToken).append(", processorTransactionId: ").append(this.processorTransactionId);
        bdr.append(", authorizationNumber: ").append(this.authorizationNumber).append(", processedDate: ").append(this.processedDate);
        bdr.append(", referenceNumber: ").append(this.referenceNumber);
        return bdr.toString();
    }
    
    public final Status getStatus() {
        return this.status;
    }
    public final void setStatus(final Status status) {
        this.status = status;
    }
    public final String getChargeToken() {
        return this.chargeToken;
    }
    public final void setChargeToken(final String chargeToken) {
        this.chargeToken = chargeToken;
    }
    public final String getProcessorTransactionId() {
        return this.processorTransactionId;
    }
    public final void setProcessorTransactionId(final String processorTransactionId) {
        this.processorTransactionId = processorTransactionId;
    }
    public final String getAuthorizationNumber() {
        return this.authorizationNumber;
    }
    public final void setAuthorizationNumber(final String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }
    public final String getProcessedDate() {
        return this.processedDate;
    }
    public final void setProcessedDate(final String processedDate) {
        this.processedDate = processedDate;
    }
    public final String getReferenceNumber() {
        return this.referenceNumber;
    }
    public final void setReferenceNumber(final String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }
}
