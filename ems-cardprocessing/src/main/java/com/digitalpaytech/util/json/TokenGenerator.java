package com.digitalpaytech.util.json;

import com.digitalpaytech.exception.JsonException;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.apache.log4j.Logger;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TokenGenerator {
    private static final Logger LOG = Logger.getLogger(TokenGenerator.class);
    
    @JsonProperty("chargeToken")
    private String chargeToken;
    @JsonProperty("terminalToken")
    private String terminalToken;
    @JsonProperty("amount")
    private Integer amount;
    
    
    public TokenGenerator(final String chargeToken, final String terminalToken) {
        this.chargeToken = chargeToken;
        this.terminalToken = terminalToken;
    }

    public TokenGenerator(final String chargeToken, final String terminalToken, final Integer amount) {
        this.chargeToken = chargeToken;
        this.terminalToken = terminalToken;
        this.amount = amount;
    }
    
    
    public static final String generate(final TokenGenerator tokenObject) throws JsonException {
        try {
            final JSON json = new JSON();
            return json.serialize(tokenObject);

        } catch (JsonException jsonpe) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("Unable to create JSON, chargeToken: ").append(tokenObject.getChargeToken());
            bdr.append(", terminalToken: ").append(tokenObject.getTerminalToken()).append(", amount: ").append(tokenObject.getAmount());
            LOG.error(bdr.toString(), jsonpe);
            throw new JsonException(bdr.toString(), jsonpe);
        }
    }
    
    public final String getChargeToken() {
        return this.chargeToken;
    }
    public final void setChargeToken(final String chargeToken) {
        this.chargeToken = chargeToken;
    }
    public final String getTerminalToken() {
        return this.terminalToken;
    }
    public final void setTerminalToken(final String terminalToken) {
        this.terminalToken = terminalToken;
    }
    public final Integer getAmount() {
        return this.amount;
    }
    public final void setAmount(final Integer amount) {
        this.amount = amount;
    }
}
