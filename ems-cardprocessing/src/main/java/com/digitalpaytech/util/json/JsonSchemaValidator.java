package com.digitalpaytech.util.json;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.log4j.Logger;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

public final class JsonSchemaValidator {
    private static final Logger LOG = Logger.getLogger(JsonSchemaValidator.class);
    
    private JsonSchemaValidator() {
        
    }
    
    /**
     * A JSON Schema validation implementation in pure Java
     * https://github.com/everit-org/json-schema
     * 
     * @param jsonData
     * @param jsonSchema
     * @return
     * @throws IOException
     * @throws ProcessingException
     */
    public static boolean validate(final String jsonData, final String jsonSchemaPath) throws IOException {
        boolean valid = false;
        try (InputStream inputStream = JsonSchemaValidator.class.getClassLoader().getResourceAsStream(jsonSchemaPath)) {
            if (inputStream == null) {
                throw new FileNotFoundException("Could not locate resource " + jsonSchemaPath);
            }
            
            final JSONObject rawSchema = new JSONObject(new JSONTokener(inputStream));
            final Schema schema = SchemaLoader.load(rawSchema);
            
            try {
                schema.validate(new JSONObject(jsonData));
                valid = true;
            } catch (ValidationException ve) {
                LOG.warn("Either someone tryinh to inject json or something wrong with the schema: " + ve.getMessage());
                for (Exception e : ve.getCausingExceptions()) {
                    if (e instanceof ValidationException) {
                        LOG.warn("..." + e.getMessage());
                    }
                }
            }
        }
        
        return valid;
    }
}
