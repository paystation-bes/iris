package com.digitalpaytech.cardprocessing.impl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

import org.apache.log4j.Logger;

import com.digitalpaytech.cardprocessing.CardProcessor;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DPTResponseCodesMapping;

public class AuthorizeNetProcessor extends CardProcessor 
{
	private static Logger logger__ = Logger.getLogger(AuthorizeNetProcessor.class);
	private static final String LOGIN_ID_STRING = "x_login";
	private static final String TRAN_KEY_STRING = "x_tran_key";
	private static final String VERSION_STRING = "x_cpversion";
	private static final String VERSION_VALUE_STRING = "1.0";
	private static final String TEST_REQUEST_STRING = "x_test_request";
	private static final String METHOD_STRING = "x_method";
	private static final String METHOD_VALUE_STRING = "CC";
	private static final String TYPE_STRING = "x_type";
	private static final String AMOUNT_STRING = "x_amount";
	private static final String DELIM_DATA_STRING = "x_delim_data";
	private static final String DELIM_CHAR_STRING = "x_delim_char";
	private static final String DELIM_CHAR_VALUE = "|";
	private static final String RELAY_RESPONSE_STRING = "x_relay_response";
	private static final String MARKET_TYPE_STRING = "x_market_type";
	private static final String DEVICE_TYPE_STRING = "x_device_type";
	private static final String TRACK2_STRING = "x_track2";
	private static final String DESCRIPTION_STRING = "x_description";
	private static final String RESPONSE_FORMAT_STRING = "x_response_format";
	private static final String RESPONSE_FORMAT_DELIMITED_VALUE = "1";
	private static final String MARKET_TYPE_STRING_VALUE = "2";
	private static final String USER_REF_STRING = "x_user_ref";
	private static final String PROCESSOR_TRANSACTION_ID_STRING = "x_ref_trans_id";
	private static final String CARD_NUM_STRING = "x_card_num";
	private static final String CARD_EXP_DATE_STRING = "x_exp_date";
	private static final String SOLUTION_ID_STRING = "x_solution_id";
	
	private static final int APPROVED_RESPONSE_CODE = 1;
	private static final int DECLINED_RESPONSE_CODE = 2;
	private static final int ERROR_RESPONSE_CODE = 3;

	private static final String TRANSACTION_TYPE_AUTH_ONLY = "AUTH_ONLY";
	private static final String TRANSACTION_TYPE_PRIOR_AUTH_CAPTURE = "PRIOR_AUTH_CAPTURE";
	private static final String TRANSACTION_TYPE_AUTH_CAPTURE = "AUTH_CAPTURE";
	private static final String TRANSACTION_TYPE_CREDIT = "CREDIT";
	private static final String TRANSACTION_TYPE_VOID = "VOID";

	private String destinationURLString_;
	private String loginId_;
	private String tranKey_;

	private String responseAuthCode_ = null;
	private int responseCode_ = 0;
	private int responseReasonCode_ = 0;
	private String responseReasonText_;
	private String responseTransactionId_;

	
	public AuthorizeNetProcessor(MerchantAccount md, CommonProcessingService commonProcessingService, EmsPropertiesService emsPropertiesService) 
	{
		super(md, commonProcessingService, emsPropertiesService);
		loginId_ = md.getField1();
		tranKey_ = md.getField3();
		destinationURLString_ = commonProcessingService.getDestinationUrlString(md.getProcessor());
	}
	
	
	public boolean processPreAuth(PreAuth pd) throws CardTransactionException
	{
		String reference_number = getCommonProcessingService().getNewReferenceNumber(getMerchantAccount());
		StringBuilder sb = new StringBuilder();
		createBaseRequest(sb);
		appendToRequest(sb, TYPE_STRING, TRANSACTION_TYPE_AUTH_ONLY);
		appendToRequest(sb, AMOUNT_STRING, CardProcessingUtil.getCentsAmountInDollars(pd.getAmount()).toString());
		appendToRequest(sb, TRACK2_STRING, pd.getCardData());
		// appendToRequest(sb, TRACK2_STRING, "4005550000000480=09121012122232425262");
		appendToRequest(sb, USER_REF_STRING, reference_number);

		String responseString = sendRequest(sb.toString(), pd.getCardData());
		processResponse(responseString);

		pd.setResponseCode(String.valueOf(responseCode_));
		pd.setProcessorTransactionId(responseTransactionId_);
		pd.setAuthorizationNumber(responseAuthCode_);
		pd.setReferenceNumber(reference_number);
		pd.setProcessingDate(new Date());

		// delete card data from preAuth as it is not needed for post-auth
		pd.setCardData(null);

		if (responseCode_ == this.APPROVED_RESPONSE_CODE)
		{
			pd.setIsApproved(true);
		}
		else
		{
			pd.setIsApproved(false);
		}

		return (pd.isIsApproved());
	}

	public Object[] processCharge(Track2Card track2Card, ProcessorTransaction chargePtd) throws CardTransactionException
	{
		String reference_number = getCommonProcessingService().getNewReferenceNumber(getMerchantAccount());

		StringBuilder sb = new StringBuilder();
		createBaseRequest(sb);
		appendToRequest(sb, TYPE_STRING, this.TRANSACTION_TYPE_AUTH_CAPTURE);
		appendToRequest(sb, AMOUNT_STRING, CardProcessingUtil.getCentsAmountInDollars(chargePtd.getAmount()).toString());
		appendToRequest(sb, USER_REF_STRING, reference_number);

		// Check if we have track 2 data
		String card_data_to_mask;
		if (track2Card.isCardDataCreditCardTrack2Data())
		{
			appendToRequest(sb, TRACK2_STRING, track2Card.getDecryptedCardData());
			card_data_to_mask = track2Card.getDecryptedCardData();
		}
		else
		{
			appendToRequest(sb, CARD_NUM_STRING, track2Card.getPrimaryAccountNumber());
			appendToRequest(sb, CARD_EXP_DATE_STRING, track2Card.getExpirationMonth()
					+ track2Card.getExpirationYear());
			card_data_to_mask = track2Card.getPrimaryAccountNumber();
		}

		String response = sendRequest(sb.toString(), card_data_to_mask);
		return handleChargeResponse(response, chargePtd);
	}

	public Object[] processPostAuth(PreAuth pd, ProcessorTransaction ptd) throws CardTransactionException
	{
		String reference_number = getCommonProcessingService().getNewReferenceNumber(getMerchantAccount());
		StringBuilder sb = new StringBuilder();
		createBaseRequest(sb);
		appendToRequest(sb, TYPE_STRING, this.TRANSACTION_TYPE_PRIOR_AUTH_CAPTURE);
		// appendToRequest(sb, AMOUNT_STRING, (new Float(requestDataHandler.getChargeAmount())).toString());
		// appendToRequest(sb, AMOUNT_STRING, (new Float(10.0f)).toString());
		appendToRequest(sb, PROCESSOR_TRANSACTION_ID_STRING, pd.getProcessorTransactionId());
		appendToRequest(sb, USER_REF_STRING, reference_number);

		String responseString = sendRequest(sb.toString(), "");
		processResponse(responseString);

		// Set ProcessorTransactionData values from processor
		ptd.setProcessingDate(new Date());
		ptd.setReferenceNumber(reference_number);
		ptd.setProcessorTransactionId(responseTransactionId_);

		// Approved
		if (responseCode_ == this.APPROVED_RESPONSE_CODE)
		{
			ptd.setAuthorizationNumber(pd.getAuthorizationNumber());
			ptd.setIsApproved(true);
			return new Object[] {true, responseCode_};
		}

		// Declined
		ptd.setAuthorizationNumber(null);
		ptd.setIsApproved(false);
		return new Object[] {false, responseCode_};
	}

	public Object[] processRefund(boolean isNonEmsRequest, ProcessorTransaction origPtd,
			ProcessorTransaction refundPtd, String ccNumber, String expiryMMYY) throws CardTransactionException
	{
		String reference_number = getCommonProcessingService().getNewReferenceNumber(getMerchantAccount());
		// first try to void the transaction, if that doesnt work then try to refund it
		// further more, try to void only if original amount and refund amount match
		if (origPtd.getAmount() == refundPtd.getAmount())
		{
			int result = processVoid(isNonEmsRequest, origPtd, refundPtd, reference_number, ccNumber, expiryMMYY);
			logger__.debug("RESPONSE CODE IS " + result);
			if (result == DPTResponseCodesMapping.CC_APPROVED_VAL__7000)
			{	
				return new Object[] {DPTResponseCodesMapping.CC_APPROVED_VAL__7000, result};
			}
		}

		StringBuilder sb = new StringBuilder();
		createBaseRequest(sb);
		appendToRequest(sb, TYPE_STRING, this.TRANSACTION_TYPE_CREDIT);
		// appendToRequest(sb, AMOUNT_STRING, (new Float(requestDataHandler.getChargeAmount())).toString());
		// appendToRequest(sb, AMOUNT_STRING, (new Float(10.0f)).toString());
		appendToRequest(sb, PROCESSOR_TRANSACTION_ID_STRING, origPtd.getProcessorTransactionId());
		appendToRequest(sb, USER_REF_STRING, reference_number);
		appendToRequest(sb, CARD_NUM_STRING, ccNumber);
		appendToRequest(sb, CARD_EXP_DATE_STRING, expiryMMYY);
		appendToRequest(sb, AMOUNT_STRING, CardProcessingUtil.getCentsAmountInDollars(refundPtd.getAmount()).toString());

		String responseString = sendRequest(sb.toString(), ccNumber);
		processResponse(responseString);

		// Set processor transaction data values
		refundPtd.setProcessingDate(new Date());
		refundPtd.setReferenceNumber(reference_number);
		refundPtd.setProcessorTransactionId(responseTransactionId_);

		// Approved
		if (responseCode_ == APPROVED_RESPONSE_CODE)
		{
			refundPtd.setAuthorizationNumber(responseAuthCode_);
			refundPtd.setIsApproved(true);
			return new Object[] {DPTResponseCodesMapping.CC_APPROVED_VAL__7000, responseCode_};
		}

		// Declined
		refundPtd.setAuthorizationNumber(null);
		refundPtd.setIsApproved(false);

		// map error response if request was not made by EMS
		if (isNonEmsRequest)
		{
			return new Object[] {mapErrorResponse(responseReasonCode_), responseCode_};
		}
		return new Object[] {responseReasonCode_, responseCode_};
	}

	private void createBaseRequest(StringBuilder stb)
	{
		appendToRequest(stb, LOGIN_ID_STRING, loginId_);
		appendToRequest(stb, TRAN_KEY_STRING, tranKey_);
		appendToRequest(stb, VERSION_STRING, VERSION_VALUE_STRING);
		// appendToRequest(stb, TEST_REQUEST_STRING, "TRUE");
		appendToRequest(stb, METHOD_STRING, METHOD_VALUE_STRING);
		// appendToRequest(stb, DELIM_DATA_STRING, "TRUE");
		appendToRequest(stb, DELIM_CHAR_STRING, DELIM_CHAR_VALUE);
		// appendToRequest(stb, RELAY_RESPONSE_STRING, "FALSE");
		appendToRequest(stb, DESCRIPTION_STRING, "DPT CC Request");
		appendToRequest(stb, RESPONSE_FORMAT_STRING, RESPONSE_FORMAT_DELIMITED_VALUE);
		appendToRequest(stb, MARKET_TYPE_STRING, MARKET_TYPE_STRING_VALUE);
		appendToRequest(stb, DEVICE_TYPE_STRING, "5");
		appendToRequest(stb, SOLUTION_ID_STRING, 
		                super.getEmsPropertiesService().getPropertyValue(EmsPropertiesService.AUTHORIZE_NET_SOLUTION_ID, 
		                                                                 EmsPropertiesService.DEFAULT_AUTHORIZE_NET_SOLUTION_ID));

	}

	private void appendToRequest(StringBuilder stb, String param, String value)
	{
		stb.append(param + "=" + value + "&");
	}

	private String sendRequest(String request, String cardData) throws CardTransactionException
	{
		String log_message = request.replaceAll(cardData, cardData.replaceAll("[^=]", "X"));
    	logger__.info("Request: " + log_message);

		try
		{
			URL url = null;
			url = new URL(destinationURLString_);

			URLConnection connection = url.openConnection();
			connection.setDoOutput(true);
			connection.setUseCaches(false);

			// not necessarily required but fixes a bug with some servers
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

			// POST the data in the string buffer
			DataOutputStream out = new DataOutputStream(connection.getOutputStream());

			out.write(request.getBytes());
			out.flush();
			out.close();

			// process and read the gateway response
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String response;
			response = in.readLine();
			in.close(); // no more data

			return response;
		}
		catch (IOException e)
		{
			throw new CardTransactionException("Authorize.NET Processor is Offline", e);
		}
	}

	private void processResponse(String response)
	{
		logger__.info("Response: " + response);
		
		// we need values from positions 2(response code), 3(reason code), 4(reason text), 5(authorization
		// code),
		// 8(transaction ID) only
		String[] tokens = response.split("\\|", -1);
		responseCode_ = Integer.parseInt(tokens[1]);
		responseReasonCode_ = Integer.parseInt(tokens[2]); // token 3
		responseReasonText_ = tokens[3]; // token 4
		responseAuthCode_ = tokens[4]; // token 5
		responseTransactionId_ = tokens[7];

		if (logger__.isDebugEnabled())
		{
			logger__.debug("Response: " + response);
		}
		else
		{
			StringBuilder sb = new StringBuilder(200);
			sb.append("Response:  ResponseCode: ");
			sb.append(responseCode_);
			sb.append(", ReasonCode: ");
			sb.append(responseReasonCode_);
			sb.append(", ReasonText: ");
			sb.append(responseReasonText_);
			sb.append(", Authorization Number: ");
			sb.append(responseAuthCode_);
			sb.append(", ProcessorTransactionId: ");
			sb.append(responseTransactionId_);
			logger__.info(sb.toString());
		}
	}

	private Object[] handleChargeResponse(String response, ProcessorTransaction ptd) throws CardTransactionException
	{
		// we need values from positions 2(response code), 3(reason code), 4(reason text), 5(authorization
		// code),
		// 8(transaction ID) only, 10(user ref field)
		String[] tokens = response.split("\\|", -1);
		int response_code = Integer.parseInt(tokens[1]);
		int reason_code = Integer.parseInt(tokens[2]); // token 3
		String reason_text = tokens[3]; // token 4
		String auth_code = tokens[4]; // token 5
		String proc_trans_id = tokens[7];
		String ref_number = tokens[9];

		StringBuilder sb = new StringBuilder(200);
		sb.append("Charge Response:  ResponseCode: ");
		sb.append(response_code);
		sb.append(", ReasonCode: ");
		sb.append(reason_code);
		sb.append(", ReasonText: ");
		sb.append(reason_text);
		sb.append(", Authorization Number: ");
		sb.append(auth_code);
		sb.append(", ProcessorTransactionId: ");
		sb.append(proc_trans_id);
		logger__.info(sb.toString());
		
		ptd.setProcessingDate(new Date());
		ptd.setReferenceNumber(ref_number);
		ptd.setProcessorTransactionId(proc_trans_id);

		if (response_code == this.APPROVED_RESPONSE_CODE)
		{
			ptd.setAuthorizationNumber(auth_code);
			ptd.setIsApproved(true);
			return new Object[] {DPTResponseCodesMapping.CC_APPROVED_VAL__7000, response_code};
		}

		// Declined
		ptd.setAuthorizationNumber(null);
		ptd.setIsApproved(false);
		return new Object[] {mapErrorResponse(reason_code), response_code};
	}

	protected int processVoid(boolean isNonEmsRequest, ProcessorTransaction origPtd,
			ProcessorTransaction refundPtd, String referenceNumber, String ccNumber, String expiryMMYY)
			throws CardTransactionException
	{
		StringBuilder sb = new StringBuilder();
		createBaseRequest(sb);
		appendToRequest(sb, TYPE_STRING, this.TRANSACTION_TYPE_VOID);
		// appendToRequest(sb, AMOUNT_STRING, (new Float(requestDataHandler.getChargeAmount())).toString());
		// appendToRequest(sb, AMOUNT_STRING, (new Float(10.0f)).toString());
		appendToRequest(sb, PROCESSOR_TRANSACTION_ID_STRING, origPtd.getProcessorTransactionId());
		appendToRequest(sb, USER_REF_STRING, referenceNumber);
		appendToRequest(sb, CARD_NUM_STRING, ccNumber);
		appendToRequest(sb, CARD_EXP_DATE_STRING, expiryMMYY);
		appendToRequest(sb, AMOUNT_STRING, CardProcessingUtil.getCentsAmountInDollars(refundPtd.getAmount()).toString());

		String responseString = sendRequest(sb.toString(), ccNumber);
		processResponse(responseString);

		// Set processor transaction data values
		refundPtd.setProcessingDate(new Date());
		refundPtd.setReferenceNumber(referenceNumber);
		refundPtd.setProcessorTransactionId(responseTransactionId_);

		// Approved
		if (responseCode_ == APPROVED_RESPONSE_CODE)
		{
			refundPtd.setAuthorizationNumber(responseAuthCode_);
			refundPtd.setIsApproved(true);
			return (DPTResponseCodesMapping.CC_APPROVED_VAL__7000);
		}

		// Declined
		refundPtd.setAuthorizationNumber(null);
		refundPtd.setIsApproved(false);

		// map error response if request was not made by EMS
		if (isNonEmsRequest)
		{
			return (mapErrorResponse(responseReasonCode_));
		}

		return (responseReasonCode_);
	}

	private int mapErrorResponse(int responseCode) throws CardTransactionException
	{
		switch (responseCode)
		{
			// This transaction has been declined
			case 2:
				// This tranasction has been declined
				// Note: The code returned from the processor indicating that the card used needs to be picked
				// up.
			case 4:
				// This transaction has been declined (recieved by prod server)
			case 44:
                // This transaction has been declined
                // Note: This code indicates a referral response.
			case 200:
			    // This is a processor specific error, in this case First Data Omaha.
				return (DPTResponseCodesMapping.CC_DECLINED_VAL__7003);

			case 3:
				return (DPTResponseCodesMapping.CC_PLACE_CALL_VAL__7022);

				// A valid amount is required
				// The value submitted in the amount field did not pass validation for a number.
			case 5:
				// The amount requested for settlement may not be greater than the orig amount authorized
				// Note: This occurs if the merchant tries to capture funds greater than the amount of the
				// original
				// authorization-only transaction.
			case 47:
				// This processor does not accept partial reversals
				// Note: The merchant attempted to settle for less than the originally authorized amount.
			case 48:
				// A transaction amount greater than $99,999 will not be accepted
			case 49:
				// The sum of all credits against this transaction is greater than orig. trans amount
			case 51:
				// The sum of credits against the referenced transaction would exceed the original debit
				// amount.
				// Note: The transaction is rejected if the sum of this credit and prior credits exceeds the
				// original
				// debit amount.
			case 55:
				return (DPTResponseCodesMapping.CC_INVALID_AMOUNT_VAL__7033);

				// The credit card number is invalid.
			case 6:
				// The credit card number is invalid.
			case 37:
				// The Track2 data is invalid.
			case 89:
				return (DPTResponseCodesMapping.CC_BAD_CREDIT_CARD_DATA_VAL__7045);

				// The credit card expiration date is invalid.
				// Note: The format of the date submitted was incorrect.
			case 7:
				// The credit card has expired.
			case 8:
				return (DPTResponseCodesMapping.CC_EXPIRED_VAL__7001);

				// A duplicate transaction has been submitted.
				// Note: A transaction with identical amount and credit card information was submitted two
				// minutes prior.
				// EMS-1480 - Changed to return retry value.
			case 11:
				return (DPTResponseCodesMapping.CC_CANNOT_PROCESS_PLEASE_RETRY_VAL__7004);

				// The transaction was not found.
				// Note: The transaction ID sent in was properly formatted but the gateway had no record of
				// the
				// transaction.
			case 16:
				// This transaction is awaiting settlement and cannot be refunded.
				// Note: Credits or refunds may only be performed against settled transactions. The
				// transaction
				// against which the credit/refund was submitted has not been settled, so a credit cannot be
				// issued.
			case 50:
				return (DPTResponseCodesMapping.CC_UNABLE_TO_PROCESS_VAL__7084);

				// The merchant API login ID is invalid or the account is inactive.
			case 13:
				throw new CardTransactionException(
						"Response indicates invalid API login ID or is inactive account: " + responseCode);

				// An error occurred during processing. Call Merchant Service Provider.
				// Note: The merchant was incorrectly set up at the processor.
			case 35: // fixme confirm error is not related to specific card
				throw new CardTransactionException("Response indicates invalid configuration at processor: "
						+ responseCode);

				// The VITAL identification numbers are incorrect. Call Merchant Service Provider.
				// Note: The merchant was incorrectly set up at the processor.
			case 34:
				throw new CardTransactionException("Response indicates invalid VITAL identification numbers: "
						+ responseCode);

				// This transaction cannot be accepted.
				// Note: A valid fingerprint, transaction key, or password is required for this transaction.
			case 103:
				throw new CardTransactionException("Response indicates invalid transaction key: "
						+ responseCode);

				// This account has not been given the permission(s) required for this request.
				// Note: The transaction request must include the API login ID associated with the payment
				// gateway
				// account.
			case 123:// fixme confirm error is not related to specific card
				throw new CardTransactionException(
						"Response indicates incorrect permissions for this account: " + responseCode);

				// This transaction cannot be processed.
//				 Note: The customer�s financial institution does not currently allow transactions for this
				// account.
			case 128:
				throw new CardTransactionException(
						"Response indicates bank does not allow transactions for this account: " + responseCode);

				// This payment gateway account has been closed.
				// Note: IFT: The payment gateway account status is Blacklisted.
			case 130:
				throw new CardTransactionException("Response indicates this account is blacklisted: "
						+ responseCode);

				// This transaction cannot be accepted at this time.
				// Note: IFT: The payment gateway account status is Suspended-STA.
			case 131:
				throw new CardTransactionException("Response indicates this account is suspended-sta: "
						+ responseCode);

				// This transaction cannot be accepted at this time.
				// Note: IFT: The payment gateway account status is Suspended-Blacklist.
			case 132:
				throw new CardTransactionException(
						"Response indicates this account is suspended-blacklisted: " + responseCode);

				// An error occurred during processing. Please contact the merchant.
				// Note: Concord EFS � Provisioning at the processor has not been completed.
			case 170:
				throw new CardTransactionException("Response indicates Concord account not setup: "
						+ responseCode);

				// An error occurred during processing. Please contact the merchant.
				// Note: Concord EFS � The store ID is invalid.
			case 172:
				throw new CardTransactionException("Response indicates invalid Concord StoreId: "
						+ responseCode);

				// An error occurred during processing. Please contact the merchant.
				// Note: Concord EFS � The store key is invalid.
			case 173:
				throw new CardTransactionException("Response indicates invalid Concord StoreKey: "
						+ responseCode);

				// This transaction has been declined.
				// Note: This error code applies only to merchants on FDC Omaha. The value submitted in the
				// merchant
				// number field is invalid.
			case 205:
				throw new CardTransactionException("Response indicates invalid FDC Omaha merchant number: "
						+ responseCode);

				// The merchant does not accept this type of credit card
			case 17:
				// The merchant does not accept this type of credit card
				// Note: The Merchant ID at the processor was not configured to accept this card type.
			case 28:
				// The configuration with the processor is invalid. Call Merchant Service Provider.
				// This error looks like a processor config error, however in real world, seems to indicate
				// Card type not setup.
			case 30:
				return (DPTResponseCodesMapping.CC_CARD_TYPE_NOT_SETUP_VAL__7076);

				// An error occurred during processing. Please try again in 5 minutes.
			case 19:
				// An error occurred during processing. Please try again in 5 minutes.
			case 20:
				// An error occurred during processing. Please try again in 5 minutes.
			case 21:
				// An error occurred during processing. Please try again in 5 minutes.
			case 22:
				// An error occurred during processing. Please try again in 5 minutes.
			case 23:
				// An error occurred during processing. Please try again in 5 minutes.
			case 25:
				// An error occurred during processing. Please try again in 5 minutes.
			case 26:
				// An error occurred in processing. Please try again in 5 minutes.
			case 57:
				// An error occurred in processing. Please try again in 5 minutes.
			case 58:
				// An error occurred in processing. Please try again in 5 minutes.
			case 59:
				// An error occurred in processing. Please try again in 5 minutes.
			case 60:
				// An error occurred in processing. Please try again in 5 minutes.
			case 61:
				// An error occurred in processing. Please try again in 5 minutes.
			case 62:
				// An error occurred in processing. Please try again in 5 minutes.
			case 63:
				// An error occurred during processing. Please try again.
				// Note: The system-generated void for the original timed-out transaction failed. (The
				// original
				// transaction timed out while waiting for a response from the authorizer.)
			case 120:
				// An error occurred during processing. Please try again.
				// Note: The system-generated void for the original errored transaction failed. (The original
				// transaction experienced a database error.)
			case 121:
				// An error occurred during processing. Please try again.
				// Note: The system-generated void for the original errored transaction failed. (The original
				// transaction experienced a processing error.)
			case 122:
				// An error occurred during processing. Please try again.
				// Note: The processor response format is invalid.
			case 180:
				return (DPTResponseCodesMapping.CC_CANNOT_PROCESS_PLEASE_RETRY_VAL__7004);

				// The authorization was approved, but settlement failed.
			case 36:
				return (DPTResponseCodesMapping.CC_SYSTEM_ERROR_VAL__7047);

				// An authorization code is required but not present.
				// Note: A transaction that required x_auth_code to be present was submitted without a value.
			case 12:
				// The transaction ID is invalid.
				// Note: The transaction ID value is non-numeric or was not present for a transaction that
				// requires it
				// (i.e., VOID, PRIOR_AUTH_CAPTURE, and CREDIT).
			case 15:
				// FIELD cannot be left blank.
				// Note: The word FIELD will be replaced by an actual field name. This error indicates that a
				// field
				// the merchant specified as required was not filled in.
			case 33:
				// The referenced transaction does not meet the criteria for issuing a credit.
			case 54:
				// The version parameter is invalid.
				// Note: The value submitted in x_cpversion was invalid.
			case 68:
				// The transaction type is invalid.
				// Note: The value submitted in x_type was invalid.
			case 69:
				// The transaction method is invalid.
				// Note: The value submitted in x_method was invalid.
			case 70:
				// The authorization code is invalid.
				// Note: The value submitted in x_auth_code was more than six characters in length.
			case 72:
				// The device type is invalid.
				// Note: The value submitted in x_device_type did not match the configured value.
			case 84:
				// The market type is invalid.
				// Note: The value submitted in x_market_type did not match the configured value.
			case 85:
				// The response format is invalid.
				// Note: The value submitted in x_response_format was not equal to �0� or �1.�
			case 86:
				// This market type is not supported.
			case 87:
				// An error occurred during processing. Please contact the merchant.
				// Note: Concord EFS � This request is invalid.
			case 171:
				// The transaction type is invalid. Please contact the merchant.
				// Note: Concord EFS � This transaction type is not accepted by the processor.
			case 174:
				return DPTResponseCodesMapping.CC_INVALID_REQUEST_VAL__7058;
				
            case 315:   
                return DPTResponseCodesMapping.CC_INVALID_CARD_NUMBER_VAL_7102;				

			default:
				throw new CardTransactionException("Unknown Response Code: " + responseCode);
		}
	}

	@Override
	public void processReversal(Reversal reversal, Track2Card track2Card)
	{
		throw new UnsupportedOperationException("AuthorizeNetProcessor, Reversal not supported for this processor");
	}
	
	@Override
	public boolean isReversalExpired(Reversal reversal) {
		throw new UnsupportedOperationException("AuthorizeNetProcessor, isReversalExpired(Reversal) is not support!");
	}
}
