package com.digitalpaytech.cardprocessing.impl;

import java.io.IOException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.SocketTimeoutException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.ws.security.util.StringUtil;
import org.hibernate.Criteria;
import org.hibernate.ScrollableResults;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.cardprocessing.CardProcessingManager;
import com.digitalpaytech.cardprocessing.CardProcessor;
import com.digitalpaytech.cardprocessing.CardProcessorFactory;
import com.digitalpaytech.cardprocessing.cps.CPSProcessor;
import com.digitalpaytech.client.dto.corecps.Authorization;
import com.digitalpaytech.client.util.CommunicationException;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.data.TransactionData;
import com.digitalpaytech.domain.CPSData;
import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.CcFailLog;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerBadCard;
import com.digitalpaytech.domain.CustomerCard;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.ExtensiblePermit;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.PreAuthHolding;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.ProcessorTransactionType;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.domain.SingleTransactionCardData;
import com.digitalpaytech.dto.ProcessorInfo;
import com.digitalpaytech.dto.TransactionSearchCriteria;
import com.digitalpaytech.dto.cps.CPSTransactionDto;
import com.digitalpaytech.exception.CardProcessorException;
import com.digitalpaytech.exception.CardProcessorPausedException;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.exception.CardTransactionIgnorableException;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.MissingCryptoKeyException;
import com.digitalpaytech.exception.PaystationCommunicationException;
import com.digitalpaytech.exception.ProcessorAuthenticationException;
import com.digitalpaytech.exception.SystemException;
import com.digitalpaytech.exception.XMLErrorException;
import com.digitalpaytech.service.CardRetryTransactionService;
import com.digitalpaytech.service.CardTypeService;
import com.digitalpaytech.service.CcFailLogService;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.CreditCardTestNumberService;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerBadCardService;
import com.digitalpaytech.service.CustomerCardService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.ElavonTransactionDetailService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.ExtensiblePermitService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.PaymentCardService;
import com.digitalpaytech.service.PermitService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.PreAuthService;
import com.digitalpaytech.service.ProcessorTransactionService;
import com.digitalpaytech.service.ProcessorTransactionTypeService;
import com.digitalpaytech.service.PurchaseService;
import com.digitalpaytech.service.ReversalArchiveService;
import com.digitalpaytech.service.ReversalService;
import com.digitalpaytech.service.SingleTransactionCardDataService;
import com.digitalpaytech.service.WebWidgetHelperService;
import com.digitalpaytech.service.cps.CPSDataService;
import com.digitalpaytech.service.cps.CPSProcessDataService;
import com.digitalpaytech.service.crypto.CryptoAlgorithmFactory;
import com.digitalpaytech.service.kpi.KPIListenerService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DPTResponseCodesMapping;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.TransactionFacade;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.crypto.CryptoConstants;
import com.digitalpaytech.util.json.CardTransactionResponseGenerator;

@Service("cardProcessingManager")
@Transactional(propagation = Propagation.REQUIRED)
@SuppressWarnings("checkstyle:abbreviationaswordinname")
public class CardProcessingManagerImpl implements CardProcessingManager, BeanFactoryAware, MessageSourceAware {
    private static final Logger LOG = Logger.getLogger(CardProcessingManagerImpl.class);
    private static final String LEFT_ROUND_COLON = "): ";
    private static final String F_SLASH_THREE_BANG = "/3 !";
    private static final String CARD_DATA = "cardData";
    private static final String REFUND_AMT = "refundAmount: ";
    private static final String PROCESS_CHARGE = "processCharge";
    private static final String PROCESS_ADD_ON = "processAddOn";
    private static final String MAX_HOLDING_DAY = "maxHoldingDay";
    private static final String PROC_POSTAUTH = "processPostAuth";
    private static final String UNABLE_CHARGE_RETY_TRANS = "Unable to charge CardRetryTransaction: ";
    private static final String CRYPTO_SERVICE_WARN_CRYPTO_EXCEPTION =
            "processTransactionCharge, 'cryptoService.isNewKeyForData(...)' throws CryptoException, continue with processTransactionCharge, ";
    private static final String MERCH_ACC_NULL = ", is MerchantAccount null: ";
    private static final String INTERNAL_KEY_MISSING = "Internal Key missing Transaction cannot be refunded: ";
    private static final String UNKNOWN_ERROR = "Unknown error: ";
    private static final String TIMEOUT_STRING = "TIMEOUT";
    private static final String PROCTRANS_PROBLEMS = "CardProcessingManager, processTransactionRefund, problems. ";
    private static final String UNABLE_INSERT = "Unable to insert refund attempt into ProcessorTransaction";
    private static final String UN_INSRT = "Unable to insert refund attempt into ProcessorTransaction: \n\n ";
    private static final String UNABLE_INSERT_RE = "Unable to insert failed refund attempt into ProcessorTransaction: \n\n";
    private static final String INSERT_F_CRG_LE = "Unable to insert failed charge attempt into ProcessorTransaction: \n\n";
    private static final String POSTAUTH_EXCEPTION = "Unable to post-auth a transaction due to unexpected exception: ";
    private static final String AS_PS_DNE = " as the PS does not exist.";
    private static final String UNABLE_AUTHORIZE = "Unable to authorize card: '";
    private static final String FOR_POS = "' for PointOfSale SerialNum: '";
    private static final String PAT_N_ACCEPTED = "' as the card pattern is not accepted.";
    private static final String FIND_MERCH_ACT = "UNABLE TO FIND MERCHANT ACCOUNT";
    private static final String FIND_PROC = "UNABLE TO FIND CARD PROCESSOR";
    private static final String W_CUR_TIME = " with current time: ";
    private static final String FAIL_PR_RT = "Failed to capture processor response to database, will start retry #";
    private static final String FAIL_LOCATE_KEY = "Could not locate key from keystore !";
    private static final String CANT_OBTAIN_PROC_OBJ = "Cannot obtain a Processor object from cardProcessorFactory, PointOfSale serial number: ";
    private static final String PROC_NULL = "Processor is null";
    private static final String FAILED_TO_CAPTURE_PROC_RESP = "Failed to capture processor response for refund request!";
    private static final String FAILED_TO_PREVENT_DUP = "Failed to prevent duplicated refund (Processor has already approved this refund)!";
    private static final String PROC_TRANS_REFUND_PROBLEM =
            "processTransactionRefund problem: Processor has approved the refund but Iris could not capture the result.";
    private static final String PROC_TRANS_STILL_GETTING_REFUND = " The following ProcessorTransaction still able to get refund again origPtd:";
    private static final String CHARGE_TRANS_W_CARD_RETRT_TRANS = "Charge Transaction with CardRetryTransaction: ";
    
    private static final int FIRST_DATA_NASHVILLE_TEST_AMT = 1100;
    private static final int LINK2GOV_TEST_AMT = 1000;
    private static final int HEARTLAND_TEST_AMT = 100;
    private static final int MONERIS_TEST_AMT = 10;
    private static final int PARADATA_TEST_AMOUNT = 1;
    private static final int MIN_DECRYPTED_CARD_DATA_LEN = 40;
    private static final int EMS_UNAVAILABLE_VAL = 7070;
    private static final int MAX_RETRIES = 3;
    private static final int THREAD_SLEEP_MS = 100;
    private static final int CARD_FOUR_DIGITS_OFFSET = 4;
    
    private MessageSourceAccessor messageAccessor;
    
    @Autowired
    private CryptoService cryptoService;
    
    @Autowired
    private CustomerCardTypeService customerCardTypeService;
    
    @Autowired
    private CustomerCardService customerCardService;
    
    @Autowired
    private CustomerBadCardService customerBadCardService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    @Autowired
    private ProcessorTransactionService processorTransactionService;
    
    @Autowired
    private WebWidgetHelperService webWidgetHelperService;
    
    @Autowired
    private CardProcessorFactory cardProcessorFactory;
    
    @Autowired
    private MailerService mailerService;
    
    @Autowired
    private LocationService locationService;
    
    @Autowired
    private CcFailLogService ccFailLogService;
    
    @Autowired
    private CardRetryTransactionService cardRetryTransactionService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private PreAuthService preAuthService;
    
    @Autowired
    private ReversalService reversalService;
    
    @Autowired
    private ReversalArchiveService reversalArchiveService;
    
    @Autowired
    private CardTypeService cardTypeService;
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    @Autowired
    private TransactionFacade transactionFacade;
    
    @Autowired
    private PaymentCardService paymentCardService;
    
    @Autowired
    private PurchaseService purchaseService;
    
    @Autowired
    private PermitService permitService;
    
    @Autowired
    private SingleTransactionCardDataService singleTransactionCardDataService;
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private KPIListenerService kpiListenerService;
    
    @Autowired
    private CommonProcessingService commonProcessingService;
    
    @Autowired
    private ElavonTransactionDetailService elavonTransactionDetailService;
    
    @Autowired
    private CryptoAlgorithmFactory cryptoAlgorithmFactory;
    
    @Autowired
    private CPSProcessor cpsProcessor;
    
    @Autowired
    private CPSDataService cpsDataService;
    
    @Autowired
    private CPSProcessDataService cpsProcessDataService;
    
    @Autowired
    private ProcessorTransactionTypeService processorTransactionTypeService;
    
    @Autowired
    private ExtensiblePermitService extensiblePermitService;
    
    @Autowired
    private CreditCardTestNumberService testCreditCardNumberService;
    
    private BeanFactory beanFactory;
    
    private Properties mappingProperties;
    
    // Counter for sending info to processor
    private int processorCounter;
    
    // Stop Processing Flag, will be set by CardProcessingMaster's checkProcessingQueueAndStopServer
    private boolean needStopProcessing;
    
    //----------------------------------------------------------------------------------------------------------------------------------------------
    // Copied from EMS 6.3.11 com.digitalpioneer.appservice.transaction.Impl.CardProcessingAppServiceImpl & 
    // com.digitalpioneer.service.Impl.CardProcessingServiceImpl
    //----------------------------------------------------------------------------------------------------------------------------------------------
    
    @PostConstruct
    private void loadMappingProperties() throws SystemException {
        try {
            this.mappingProperties = PropertiesLoaderUtils.loadAllProperties(WebCoreConstants.MAPPING_PROPERTIES_FILE_NAME);
            
            if (this.mappingProperties == null || this.mappingProperties.isEmpty()) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append("TransactionServiceImpl, loadMappingProperties, ").append(WebCoreConstants.MAPPING_PROPERTIES_FILE_NAME)
                        .append(" is missing!");
                throw new SystemException(bdr.toString());
            }
        } catch (IOException ioe) {
            final StringBuilder bdr = new StringBuilder(61);
            bdr.append("TransactionServiceImpl, loadMappingProperties, error loading ").append(WebCoreConstants.MAPPING_PROPERTIES_FILE_NAME);
            LOG.error(bdr.toString(), ioe);
            throw new SystemException(bdr.toString(), ioe);
        }
    }
    
    @Override
    public final List<ProcessorTransaction> getRefundableTransactions(final String authNumber, final String cardNumber, final String cardExpiryMMYY,
        final Date startDate, final Date endDate, final Integer merchantAccountId, final int customerId) {
        final TransactionSearchCriteria criteria = new TransactionSearchCriteria();
        criteria.setCustomerId(customerId);
        criteria.setStartDate(startDate);
        criteria.setEndDate(endDate);
        
        if (cardNumber.length() >= CARD_FOUR_DIGITS_OFFSET) {
            criteria.setCardType(this.customerCardTypeService.getCreditCardTypeByCardNumber(customerId, cardNumber).getName());
            criteria.setCardLast4Digits(new Short(cardNumber.substring(cardNumber.length() - CARD_FOUR_DIGITS_OFFSET)));
        } else {
            criteria.setCardType(CardProcessingConstants.NAME_CREDIT_CARD);
        }
        
        if (cardNumber.length() >= CARD_FOUR_DIGITS_OFFSET) {
            final String accountNumber = CardProcessingUtil.createAccountNumber(cardNumber, cardExpiryMMYY);
            criteria.setCardCheckSum(CardProcessingUtil.computeIntCRC(accountNumber));
            try {
                criteria.setCardHash(this.cryptoAlgorithmFactory.getSha1Hash(accountNumber, CryptoConstants.HASH_CREDIT_CARD_PROCESSING));
            } catch (CryptoException cre) {
                throw new CardTransactionException(cre);
            }
        } else {
            criteria.setCardCheckSum(0);
            criteria.setCardHash(WebCoreConstants.EMPTY_STRING);
        }
        
        if (merchantAccountId != null) {
            criteria.setMerchantAccountIds(new ArrayList<Integer>(1));
            criteria.getMerchantAccountIds().add(merchantAccountId);
        } else {
            final List<MerchantAccount> mercAccnts = this.merchantAccountService.findRefundableMerchantAccounts(customerId);
            criteria.setMerchantAccountIds(new ArrayList<Integer>(mercAccnts.size()));
            for (MerchantAccount merc : mercAccnts) {
                criteria.getMerchantAccountIds().add(merc.getId());
            }
        }
        
        // This will skip the join and because the chance of a single parker parking more than 10 ps is low, this will speed things up.
        criteria.setPointOfSaleIds(this.pointOfSaleService.findPosIdsByCustomerId(customerId));
        
        if (authNumber != null && !authNumber.isEmpty()) {
            criteria.setAuthNumber(authNumber);
        }
        
        return this.processorTransactionService.findRefundableTransaction(criteria);
    }
    
    @Override
    public final ProcessorTransaction processTransactionRefund(final ProcessorTransaction origPtd, final String cardNumber,
        final String cardExpiryMMYY, final boolean isNonEmsRequest) {
        // Make copy of orig_ptd and clear processing fields
        ProcessorTransaction refundPtd = CardProcessingUtil.buildRefundProcTrans(origPtd);
        
        // Check if refund amount equal to transaction amount
        if (origPtd.getAmount() == 0) {
            refundPtd.setRefundResponseCode(DPTResponseCodesMapping.CC_AMOUNT_OVER_MAXIMUM_VAL__7028);
            return refundPtd;
        }
        
        final String customerName = origPtd.getMerchantAccount().getCustomer().getName();
        
        // Use merchant account from existing transaction
        increaseProcessorCounter();
        try {
            final CardProcessor processor = this.cardProcessorFactory
                    .getCardProcessor(origPtd.getMerchantAccount(), this.pointOfSaleService.findPointOfSaleById(origPtd.getPointOfSale().getId()));
            
            if (processor == null) {
                throw new IllegalStateException(CardProcessingManager.CAN_NOT_LOCAT_PRC_FOR_MERCH_ACCT + origPtd.getMerchantAccount().getName());
            }
            
            Exception refundException = null;
            Object[] responses = null;
            
            try {
                responses = self().requestRefund(processor, customerName, cardNumber, cardExpiryMMYY, origPtd, refundPtd, isNonEmsRequest);
            } catch (Exception e) {
                refundException = e;
            }
            
            final boolean isTransInEms = origPtd.getMerchantAccount().getId() != 0;
            Exception handlingException = null;
            boolean preventDuplicatedRefund = false;
            try {
                self().handleRefundResponse(responses, refundException, customerName, origPtd, refundPtd, isTransInEms);
                // Only call 'updateRefundDetails' when there is no exception.
                if (refundException == null && processor.getIsManualSave()) {
                    processor.updateRefundDetails(refundPtd);
                }
            } catch (Exception e) {
                LOG.error(FAILED_TO_CAPTURE_PROC_RESP, e);
                handlingException = e;
            }
            
            if (handlingException != null) {
                refundPtd = null;
                
                final boolean approved = isTransInEms && responses[0].equals(DPTResponseCodesMapping.CC_APPROVED_VAL__7000);
                if (approved) {
                    try {
                        self().updateRefundStatus(origPtd);
                        preventDuplicatedRefund = true;
                    } catch (Exception e) {
                        LOG.error(FAILED_TO_PREVENT_DUP, e);
                    }
                }
                
                final StringBuilder bdr = new StringBuilder();
                if (approved && !preventDuplicatedRefund) {
                    bdr.append(PROC_TRANS_REFUND_PROBLEM);
                    bdr.append(PROC_TRANS_STILL_GETTING_REFUND);
                    bdr.append(origPtd.toString());
                    bdr.append(WebCoreConstants.NEXT_LINE_SYMBOL);
                    bdr.append(REFUND_AMT).append(origPtd.getAmount()).append(WebCoreConstants.NEXT_LINE_SYMBOL);
                } else {
                    bdr.append(PROCTRANS_REFUND_PROBLEM).append(origPtd.toString()).append(WebCoreConstants.NEXT_LINE_SYMBOL);
                    bdr.append(REFUND_AMT).append(origPtd.getAmount()).append(WebCoreConstants.NEXT_LINE_SYMBOL);
                }
                
                sendAdminErrorAlert(handlingException, bdr.toString());
            }
        } catch (CardProcessorPausedException cppe) {
            throw new IllegalStateException(
                    CardProcessingUtil.createCardProcessorPausedExceptionMessage(origPtd.getMerchantAccount().getProcessor().getId(),
                                                                                 origPtd.getMerchantAccount().getName()));
        } finally {
            decreaseProcessorCounter();
        }
        return refundPtd;
    }
    
    @Override
    public final ProcessorTransaction processCPSTransactionRefund(final ProcessorTransaction origPtd, final String chargeToken) {
        // Make copy of orig_ptd and clear processing fields
        ProcessorTransaction refundPtd = CardProcessingUtil.buildRefundProcTrans(origPtd);
        
        // Check if refund amount equal to transaction amount
        if (origPtd.getAmount() == 0) {
            refundPtd.setRefundResponseCode(DPTResponseCodesMapping.CC_AMOUNT_OVER_MAXIMUM_VAL__7028);
            return refundPtd;
        }
        
        Exception refundException = null;
        Object[] responses = null;
        try {
            responses = self().requestCPSRefund(origPtd, refundPtd, chargeToken);
        } catch (Exception e) {
            refundException = e;
        }
        
        final boolean isTransInEms = origPtd.getMerchantAccount().getId() != 0;
        final String customerName = origPtd.getMerchantAccount().getCustomer().getName();
        
        if (refundException == null && responses != null && responses[0].equals(DPTResponseCodesMapping.CC_APPROVED_VAL__7000)) {
            refundPtd.setIsApproved(true);
        } else {
            refundPtd.setIsApproved(false);
        }
        
        Exception handlingException = null;
        boolean preventDuplicatedRefund = false;
        try {
            self().handleRefundResponse(responses, refundException, customerName, origPtd, refundPtd, isTransInEms);
        } catch (Exception e) {
            LOG.error(FAILED_TO_CAPTURE_PROC_RESP, e);
            handlingException = e;
        }
        
        if (handlingException != null) {
            final boolean approved = isTransInEms && responses[0].equals(DPTResponseCodesMapping.CC_APPROVED_VAL__7000);
            refundPtd = null;
            if (approved) {
                try {
                    self().updateRefundStatus(origPtd);
                    preventDuplicatedRefund = true;
                } catch (Exception e) {
                    LOG.error(FAILED_TO_PREVENT_DUP, e);
                }
            }
            
            final StringBuilder bdr = new StringBuilder();
            if (approved && !preventDuplicatedRefund) {
                bdr.append(PROC_TRANS_REFUND_PROBLEM);
                bdr.append(PROC_TRANS_STILL_GETTING_REFUND);
                bdr.append(origPtd.toString());
                bdr.append(WebCoreConstants.NEXT_LINE_SYMBOL);
                bdr.append(REFUND_AMT).append(origPtd.getAmount()).append(WebCoreConstants.NEXT_LINE_SYMBOL);
            } else {
                bdr.append(PROCTRANS_REFUND_PROBLEM).append(origPtd.toString()).append(WebCoreConstants.NEXT_LINE_SYMBOL);
                bdr.append(REFUND_AMT).append(origPtd.getAmount()).append(WebCoreConstants.NEXT_LINE_SYMBOL);
            }
            
            sendAdminErrorAlert(handlingException, bdr.toString());
        }
        
        return refundPtd;
    }
    
    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public final Object[] requestRefund(final CardProcessor processor, final String customerName, final String cardNumber,
        final String cardExpiryMMYY, final ProcessorTransaction origPtd, final ProcessorTransaction refundPtd, final boolean isNonEmsRequest)
        throws Exception {
        Object[] responses = null;
        // Refund transaction
        try {
            final Method method = CardProcessor.class.getDeclaredMethod(PROCESS_REFUND, new Class[] { boolean.class, ProcessorTransaction.class,
                ProcessorTransaction.class, String.class, String.class, });
            method.setAccessible(true);
            responses = (Object[]) this.kpiListenerService.invoke(processor, method,
                                                                  new Object[] { isNonEmsRequest, origPtd, refundPtd, cardNumber, cardExpiryMMYY });
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            responses = processor.processRefund(isNonEmsRequest, origPtd, refundPtd, cardNumber, cardExpiryMMYY);
        } catch (SecurityException e) {
            e.printStackTrace();
            responses = processor.processRefund(isNonEmsRequest, origPtd, refundPtd, cardNumber, cardExpiryMMYY);
        }
        
        return responses;
    }
    
    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public final Object[] requestCPSRefund(final ProcessorTransaction origPtd, final ProcessorTransaction refundPtd, final String chargeToken)
        throws Exception {
        
        CardTransactionResponseGenerator cardTxResp = null;
        final CPSTransactionDto cpsTransactionDto = new CPSTransactionDto(origPtd, refundPtd, origPtd.getMerchantAccount());
        cpsTransactionDto.setChargeToken(chargeToken);
        
        // Refund transaction
        try {
            final Method method = CPSProcessor.class.getDeclaredMethod(PROCESS_REFUND, new Class[] { CPSTransactionDto.class, });
            method.setAccessible(true);
            cardTxResp =
                    (CardTransactionResponseGenerator) this.kpiListenerService.invoke(this.cpsProcessor, method, new Object[] { cpsTransactionDto });
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            cardTxResp = this.cpsProcessor.processRefund(cpsTransactionDto);
        } catch (SecurityException e) {
            e.printStackTrace();
            cardTxResp = this.cpsProcessor.processRefund(cpsTransactionDto);
        }
        
        if (cardTxResp == null) {
            return new Object[] { DPTResponseCodesMapping.CC_UNABLE_TO_PROCESS_VAL__7084, DPTResponseCodesMapping.CC_UNABLE_TO_PROCESS_STRING__7084 };
        }
        
        final int statusCode;
        if (CardProcessingUtil.getAcceptedIfAcceptedOrAlreadyDone(cardTxResp.getStatus().getCode(), 
                cardTxResp.getStatus().getMessage(), 
                CardProcessingConstants.TX_ALREADY_REFUNDED_VOIDED_WITH_WITHOUT_SPACES).equalsIgnoreCase(StandardConstants.STRING_STATUS_CODE_ACCEPTED)) {

            statusCode = DPTResponseCodesMapping.CC_APPROVED_VAL__7000;
        } else {
            statusCode = DPTResponseCodesMapping.CC_DECLINED_VAL__7003;
        }
        return new Object[] { statusCode, cardTxResp };
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public final void handleRefundResponse(final Object[] responses, final Exception refundException, final String customerName,
        final ProcessorTransaction origPtd, final ProcessorTransaction refundPtd, final boolean isTransInEms) {
        if (refundException != null) {
            LOG.error("Exception occured while trying to peform transaction refund", refundException);
            
            insertFailedRefundIntoProcessorTransaction(refundPtd, EXCEPTION_STRING);
            saveFailedCCTransaction(refundPtd.getPointOfSale(), refundPtd.getMerchantAccount(), refundPtd.getProcessingDate(),
                                    refundPtd.getPurchasedDate(), refundPtd.getTicketNumber(), CardProcessingConstants.CC_TYPE_REFUND,
                                    refundException.getMessage());
            
            final StringBuilder bdr = new StringBuilder();
            bdr.append(PROCTRANS_REFUND_PROBLEM).append(origPtd.toString()).append(WebCoreConstants.NEXT_LINE_SYMBOL).append(PROCTRANS_REFUND)
                    .append(refundPtd).append(WebCoreConstants.NEXT_LINE_SYMBOL).append(CUSTOMER_NAME).append(customerName)
                    .append(WebCoreConstants.NEXT_LINE_SYMBOL);
            sendAdminErrorAlert(refundException, bdr.toString());
        } else {
            // PS does not exist, hence transaction data couldn't be found
            if (refundPtd.getPointOfSale().getId() == null || refundPtd.getPointOfSale().getId() == 0) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append(UNABLE_TO_INSERT).append(refundPtd).append(PS_NOT_EXIST).append(customerName).append(ADD_PS_APP);
                LOG.warn(bdr.toString());
            } else if (refundPtd.getTicketNumber() == 0) {
                // BOSS version < 5.0.4 (ticket number is not available) AND transaction wasn't found in EMS
                final StringBuilder bdr = new StringBuilder();
                bdr.append(UNABLE_TO_INSERT).append(refundPtd).append(AS_STR).append(customerName).append(BOSS_VER_PRIOR).append(PLS_UPGRADE);
                LOG.warn(bdr.toString());
            } else {
                // PS exists
                // And BOSS version >= 5.0.4 (ticket number and purchased date are available)
                refundPtd.setProcessorTransactionType(getRefundType(origPtd));
                
                // Set ProcessingDate and AuthorizationNumber.
                setRefundResponseData(refundPtd, responses);
                
                // Insert into database
                try {
                    this.entityDao.save(refundPtd);
                    
                    // Insert a CPSData record if necessary which contains the Refund ChargeToken and ProcessorTransaction Id.
                    insertCPSDataForSuccessfulRefund(refundPtd, responses);
                    
                    final SingleTransactionCardData singleTransactionCardData =
                            this.singleTransactionCardDataService.findSingleTransactionCardDataByPurchaseId(refundPtd.getPurchase().getId());
                    if (singleTransactionCardData != null) {
                        this.entityDao.delete(singleTransactionCardData);
                    }
                    
                } catch (Exception e) {
                    this.mailerService.sendAdminErrorAlert(PROCTRANS_PROBLEMS, UNABLE_INSERT + refundPtd, e);
                    LOG.error(UN_INSRT + refundPtd, e);
                    
                    throw new CardTransactionIgnorableException();
                }
            }
            
            // Update charge/settle to charge/settle with refunds if it is recorded in EMS
            // and the transaction was successfully refunded
            final int responseCode = (Integer) responses[0];
            if (isTransInEms) {
                if (responseCode == DPTResponseCodesMapping.CC_APPROVED_VAL__7000) {
                    updateRefundStatus(origPtd);
                } else {
                    saveFailedCCTransaction(refundPtd.getPointOfSale(), refundPtd.getMerchantAccount(), refundPtd.getProcessingDate(),
                                            refundPtd.getPurchasedDate(), refundPtd.getTicketNumber(), CardProcessingConstants.CC_TYPE_REFUND,
                                            responses[1].toString());
                }
            }
            
            refundPtd.setRefundResponseCode(responseCode);
        }
    }
    
    private ProcessorTransactionType getRefundType(final ProcessorTransaction origPtd) {
        final int refundType;
        switch (origPtd.getProcessorTransactionType().getId()) {
            case CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_CHARGE_WITH_NO_REFUNDS:
                refundType = CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_REFUND;
                break;
            case CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_CHARGE_WITH_NO_REFUNDS:
                refundType = CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_REFUND;
                break;
            default:
                refundType = CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_REFUND;
                break;
        }
        return this.processorTransactionTypeService.findProcessorTransactionType(refundType);
    }
    
    /**
     * If response is from CPS, the data will be retrieved from CardTransactionResponseGenerator, otherwise
     * refundPtd.processingDate will be populated with current GMT.
     */
    private ProcessorTransaction setRefundResponseData(final ProcessorTransaction refundPtd, final Object[] responses) {
        if (isApprovedAndHasCardTransactionResponseGenerator(responses)) {
            final CardTransactionResponseGenerator resp = (CardTransactionResponseGenerator) responses[1];
            final String processedDate = resp.getProcessedDate();
            refundPtd.setProcessingDate(StringUtils.isNotBlank(processedDate) 
                ? DateUtil.parse(processedDate, DateUtil.UTC_CPS_DATE_TIME_FORMAT, WebCoreConstants.GMT, true) : DateUtil.getCurrentGmtDate());
            refundPtd.setAuthorizationNumber(resp.getAuthorizationNumber());
            refundPtd.setProcessorTransactionId(resp.getProcessorTransactionId());
        } else {
            refundPtd.setProcessingDate(DateUtil.getCurrentGmtDate());
            if (hasCardTransactionResponseGenerator(responses)) {
                final CardTransactionResponseGenerator resp = (CardTransactionResponseGenerator) responses[1];
                if (resp.getStatus() != null) {
                    refundPtd.setCPSResponseMessage(resp.getStatus().getMessage());
                }
                refundPtd.setProcessorTransactionId(resp.getProcessorTransactionId());
            }
        }
        return refundPtd;
    }
    
    private void insertCPSDataForSuccessfulRefund(final ProcessorTransaction refundPtd, final Object[] responses) {
        if (isApprovedAndHasCardTransactionResponseGenerator(responses)) {
            final String chargeToken = ((CardTransactionResponseGenerator) responses[1]).getChargeToken();
            final CPSData cpsData = new CPSData(refundPtd, chargeToken);
            this.entityDao.save(cpsData);
            LOG.info("CPSData for Refund is inserted, processorTransactionId: " + refundPtd.getId() + ", chargeToken is: " + chargeToken);
        }
    }
    
    /**
     * Verify in the responses object array the first element is CC_APPROVED_VAL__7000 and the second element
     * is CardTransactionResponseGenerator.
     */
    private boolean isApprovedAndHasCardTransactionResponseGenerator(final Object[] responses) {
        return responses != null && responses.length > 0 && ((Integer) responses[0]) == DPTResponseCodesMapping.CC_APPROVED_VAL__7000
               && hasCardTransactionResponseGenerator(responses);
    }
    
    private boolean hasCardTransactionResponseGenerator(final Object[] responses) {
        return responses != null && responses.length > 1 && responses[1] != null && (responses[1] instanceof CardTransactionResponseGenerator);
    }
    
    @Deprecated
    // Copied from EMS 6.3.11 com.digitalpioneer.appservice.transaction.Impl.CardProcessingAppServiceImpl, processTransactionRefund
    public final ProcessorTransaction xprocessTransactionRefund(final ProcessorTransaction origPtd, final BigDecimal refundAmount,
        final String cardNumber, final String cardExpiryMMYY, final boolean isNonEmsRequest) {
        
        // Make copy of orig_ptd and clear processing fields
        ProcessorTransaction refundPtd = null;
        try {
            refundPtd = (ProcessorTransaction) origPtd.clone();
        } catch (CloneNotSupportedException cnse) {
            LOG.error("CardProcessingManager, processTransactionRefund, not able to clone origPtd, ", cnse);
            throw new CardTransactionException(cnse);
        }
        refundPtd.setAmount(refundAmount.intValueExact());
        refundPtd.setIsApproved(false);
        refundPtd.setAuthorizationNumber(null);
        refundPtd.setProcessingDate(null);
        refundPtd.setProcessorTransactionId(null);
        refundPtd.setReferenceNumber(null);
        refundPtd.setPaymentCards(null);
        
        // Check if refund amount greater than transaction amount or equal to zero
        if (refundAmount.intValue() > origPtd.getAmount() || refundAmount.intValueExact() == 0) {
            refundPtd.setRefundResponseCode(DPTResponseCodesMapping.CC_AMOUNT_OVER_MAXIMUM_VAL__7028);
            return refundPtd;
        }
        
        // Use merchant account from existing transaction
        try {
            
            increaseProcessorCounter();
            // Refund transaction
            try {
                refundPtd = xprocessTransactionRefund(origPtd.getMerchantAccount().getCustomer().getName(), cardNumber, cardExpiryMMYY, origPtd,
                                                      refundPtd, isNonEmsRequest);
            } catch (CardTransactionIgnorableException ctie) {
                // DO NOTHING!
                refundPtd.setAmount(0);
            }
        } catch (Exception e) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append(PROCTRANS_REFUND_PROBLEM).append(origPtd.toString()).append(WebCoreConstants.NEXT_LINE_SYMBOL).append(REFUND_AMT)
                    .append(refundAmount).append(WebCoreConstants.NEXT_LINE_SYMBOL);
            sendAdminErrorAlert(e, bdr.toString());
        } finally {
            decreaseProcessorCounter();
        }
        return refundPtd;
    }
    
    @Deprecated
    // Copied from EMS 6.3.11 com.digitalpioneer.service.Impl.CardProcessingServiceImpl, processTransactionRefund
    // And modified by adding a line throwing exception !
    public final ProcessorTransaction xprocessTransactionRefund(final String customerName, final String cardNumber, final String cardExpiryMMYY,
        final ProcessorTransaction origPtd, final ProcessorTransaction refundPtd, final boolean isNonEmsRequest) {
        
        boolean needRollback = false;
        final CardProcessingManager thisInstance = this.beanFactory.getBean(CardProcessingManager.class);
        try {
            final CardProcessor processor = this.cardProcessorFactory
                    .getCardProcessor(origPtd.getMerchantAccount(), this.pointOfSaleService.findPointOfSaleById(origPtd.getPointOfSale().getId()));
            
            final boolean isTransInEms = origPtd.getMerchantAccount().getId() != 0;
            // Refund transaction
            Object[] responses;
            try {
                final Method method = CardProcessor.class.getDeclaredMethod(PROCESS_REFUND, new Class[] { boolean.class, ProcessorTransaction.class,
                    ProcessorTransaction.class, String.class, String.class, });
                method.setAccessible(true);
                responses = (Object[]) this.kpiListenerService
                        .invoke(processor, method, new Object[] { isNonEmsRequest, origPtd, refundPtd, cardNumber, cardExpiryMMYY, });
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                responses = processor.processRefund(isNonEmsRequest, origPtd, refundPtd, cardNumber, cardExpiryMMYY);
            } catch (SecurityException e) {
                e.printStackTrace();
                responses = processor.processRefund(isNonEmsRequest, origPtd, refundPtd, cardNumber, cardExpiryMMYY);
            }
            
            final int responseCode = (Integer) responses[0];
            // PS does not exist, hence transaction data couldn't be found
            if (refundPtd.getPointOfSale().getId() == null || refundPtd.getPointOfSale().getId() == 0) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append(UNABLE_TO_INSERT).append(refundPtd).append(PS_NOT_EXIST).append(customerName).append(ADD_PS_APP);
                LOG.warn(bdr.toString());
            } else if (refundPtd.getTicketNumber() == 0) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append(UNABLE_TO_INSERT).append(refundPtd).append(AS_STR).append(customerName).append(BOSS_VER_PRIOR).append(PLS_UPGRADE);
                LOG.warn(bdr.toString());
            } else {
                // PS exists
                // And BOSS version >= 5.0.4 (ticket number and purchased date are available)
                
                ProcessorTransactionType ptt = new ProcessorTransactionType();
                if (origPtd.getProcessorTransactionType()
                        .getId() == CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_CHARGE_WITH_NO_REFUNDS) {
                    ptt = this.processorTransactionService
                            .getProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_REFUND);
                } else {
                    ptt = this.processorTransactionService.getProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_REFUND);
                }
                
                refundPtd.setProcessorTransactionType(ptt);
                // Insert into database
                try {
                    // transactionDao_.createProcessorTransaction(refund_ptd);
                    this.entityDao.save(refundPtd);
                } catch (Exception e) {
                    needRollback = true;
                    this.mailerService.sendAdminErrorAlert(PROCTRANS_PROBLEMS, UNABLE_INSERT + refundPtd, e);
                    LOG.error(UN_INSRT + refundPtd, e);
                }
            }
            
            // Update charge/settle to charge/settle with refunds if it is recorded in EMS
            // and the transaction was successfully refunded
            if (isTransInEms && responseCode == DPTResponseCodesMapping.CC_APPROVED_VAL__7000) {
                thisInstance.updateRefundStatus(origPtd);
            } else if (isTransInEms) {
                thisInstance.saveFailedCCTransaction(refundPtd.getPointOfSale(), refundPtd.getMerchantAccount(), refundPtd.getProcessingDate(),
                                                     refundPtd.getPurchasedDate(), refundPtd.getTicketNumber(),
                                                     CardProcessingConstants.CC_TYPE_REFUND, responses[1].toString());
            }
            
            refundPtd.setRefundResponseCode(responseCode);
        } catch (Exception e) {
            thisInstance.insertFailedRefundIntoProcessorTransaction(refundPtd, EXCEPTION_STRING);
            thisInstance.saveFailedCCTransaction(refundPtd.getPointOfSale(), refundPtd.getMerchantAccount(), refundPtd.getProcessingDate(),
                                                 refundPtd.getPurchasedDate(), refundPtd.getTicketNumber(), CardProcessingConstants.CC_TYPE_REFUND,
                                                 e.getMessage());
            
            final StringBuilder bdr = new StringBuilder();
            bdr.append(PROCTRANS_REFUND_PROBLEM).append(origPtd.toString()).append(WebCoreConstants.NEXT_LINE_SYMBOL).append(PROCTRANS_REFUND)
                    .append(refundPtd).append(WebCoreConstants.NEXT_LINE_SYMBOL).append(CUSTOMER_NAME).append(customerName)
                    .append(WebCoreConstants.NEXT_LINE_SYMBOL);
            sendAdminErrorAlert(e, bdr.toString());
        }
        
        if (needRollback) {
            throw new CardTransactionIgnorableException();
        }
        
        return refundPtd;
    }
    
    // This method will be called when Credit Card Transaction failed. So, the transaction propagation should be REQUIRES_NEW because 
    // there might be Exception thrown from current Hibernate's Session which will prevent subsequent query to that Session.
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public final void saveFailedCCTransaction(final PointOfSale pointOfSale, final MerchantAccount merchantAccount, final Date procDate,
        final Date purchasedDate, final int ticketNumber, final int ccType, final String failedReason) {
        this.ccFailLogService.logFailedCcTransaction(pointOfSale, merchantAccount, procDate, purchasedDate, ticketNumber, ccType, failedReason);
    }
    
    /*
     * This method will be called when Credit Card Transaction failed.
     * So, the transaction propagation should be REQUIRES_NEW because there might be Exception
     * thrown from current Hibernate's Session which will prevent subsequent query to that Session.
     */
    
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public final void insertFailedRefundIntoProcessorTransaction(final ProcessorTransaction ptd, final String reason) {
        // PS does not exist
        if (ptd.getPointOfSale().getId() == 0) {
            LOG.warn(UNABLE_INSERT_RE + ptd + AS_PS_DNE);
        } else if (ptd.getTicketNumber() == 0 || ptd.getPurchasedDate() == null) {
            // BOSS version < 5.0.4 (ticket number and purchased date are not available)
            LOG.warn(UNABLE_INSERT_RE + ptd + BOSS_VER_PRIOR);
        } else {
            // PS exists
            // And BOSS version >= 5.0.4 (ticket number and purchased date are available)
            ptd.setProcessorTransactionType(this.entityDao.get(ProcessorTransactionType.class,
                                                               CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_REFUND));
            ptd.setProcessingDate(new Date());
            ptd.setAuthorizationNumber(reason);
            ptd.setIsApproved(false);
            try {
                this.entityDao.save(ptd);
            } catch (Exception e) {
                this.mailerService.sendAdminErrorAlert("CardProcessingManagerImpl, insertFailedRefundIntoProcessorTransaction, ",
                                                       "Unable to insert failed refund attempt into ProcessorTransaction" + ptd, e);
                LOG.error("Unable to insert failed refund attempt into ProcessorTransaction: \n\n " + ptd, e);
            }
        }
        
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public final void updateRefundStatus(final ProcessorTransaction origPtd) {
        if (origPtd.getProcessorTransactionType().getId() == CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_NO_REFUNDS) {
            
            this.processorTransactionService.updateProcessorTransactionType(origPtd.getId(), origPtd.getProcessorTransactionType().getId(),
                                                                            CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_REFUNDS);
        } else if (origPtd.getProcessorTransactionType().getId() == CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS) {
            
            this.processorTransactionService.updateProcessorTransactionType(origPtd.getId(), origPtd.getProcessorTransactionType().getId(),
                                                                            CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_REFUNDS);
        } else if (origPtd.getProcessorTransactionType().getId() == CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_NO_REFUNDS) {
            
            this.processorTransactionService
                    .updateProcessorTransactionType(origPtd.getId(), origPtd.getProcessorTransactionType().getId(),
                                                    CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_REFUNDS);
        } else if (origPtd.getProcessorTransactionType()
                .getId() == CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_EMS_RETRY_WITH_NO_REFUNDS) {
            
            this.processorTransactionService
                    .updateProcessorTransactionType(origPtd.getId(), origPtd.getProcessorTransactionType().getId(),
                                                    CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_EMS_RETRY_WITH_REFUNDS);
        } else if (origPtd.getProcessorTransactionType()
                .getId() == CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_CHARGE_WITH_NO_REFUNDS) {
            this.processorTransactionService
                    .updateProcessorTransactionType(origPtd.getId(), origPtd.getProcessorTransactionType().getId(),
                                                    CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_CHARGE_WITH_REFUNDS);
        } else if (origPtd.getProcessorTransactionType()
                .getId() == CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_CHARGE_WITH_NO_REFUNDS) {
            this.processorTransactionService
                    .updateProcessorTransactionType(origPtd.getId(), origPtd.getProcessorTransactionType().getId(),
                                                    CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_CHARGE_WITH_REFUNDS);
        } else {
            throw new CardTransactionException("Invalid TypeId: " + origPtd.getProcessorTransactionType().getId());
        }
    }
    
    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public final Reversal createReversal(final String originalMti, final String originalProcessingCode, final BigDecimal originalTransAmountInDollars,
        final String originalReferenceNumber, final String originalDate, final String cardData, final String lastResponseCode,
        final int merchantAccountId, final Date purchasedDate, final int ticketNumber, final int pointOfSaleId) throws CardTransactionException {
        
        Reversal reversal = null;
        final String[] params = { "merchantAccountId", "originalReferenceNumber", "originalTime" };
        final Object[] values = { merchantAccountId, originalReferenceNumber, originalDate };
        final List<Reversal> list = this.entityDao.findByNamedQueryAndNamedParam("Reversal.findByMerchantAccountIdAndRefNumAndTime", params, values);
        if (list != null && !list.isEmpty()) {
            reversal = list.get(0);
        }
        if (reversal == null) {
            //Important step : create the reversal if it does not exist
            reversal = new Reversal();
            reversal.setMerchantAccount(this.merchantAccountService.findById(merchantAccountId));
            String encryptedCardData = null;
            try {
                encryptedCardData = this.cryptoService.encryptData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL, cardData);
            } catch (CryptoException e) {
                LOG.error(e);
            }
            reversal.setMerchantAccount(this.merchantAccountService.findById(merchantAccountId));
            reversal.setCardData(encryptedCardData);
            reversal.setOriginalMessageType(originalMti);
            reversal.setOriginalProcessingCode(originalProcessingCode);
            //reversals store cent values, (ie 1 dollar is stored in db as 100)
            // however the data type is still a decimal to aid in the migration of old dollar-amount (ie 1.00) transactions
            reversal.setOriginalTransactionAmount(CardProcessingUtil.getAmountInCents(originalTransAmountInDollars.floatValue()));
            reversal.setOriginalReferenceNumber(originalReferenceNumber);
            reversal.setOriginalTime(originalDate);
            reversal.setLastResponseCode(lastResponseCode);
            reversal.setPurchasedDate(purchasedDate);
            reversal.setTicketNumber(ticketNumber);
            reversal.setPointOfSaleId(pointOfSaleId);
            reversal.setLastRetryTime(new Date());
            this.reversalService.saveReversal(reversal);
        }
        return reversal;
    }
    
    @Override
    public final void processReversal(final Reversal reversal) {
        if (reversal == null) {
            LOG.error("Cannot perform 'processReversal', Reversal object is null.");
            return;
        }
        final String encryptedCardData = reversal.getCardData();
        String cardData = null;
        if (encryptedCardData != null) {
            try {
                cardData = this.cryptoService.decryptData(encryptedCardData, CryptoService.PURPOSE_CREDIT_CARD_LOCAL, reversal.getPurchasedDate());
            } catch (CryptoException e1) {
                LOG.error(e1);
            }
        }
        final MerchantAccount ma = this.merchantAccountService.findById(reversal.getMerchantAccount().getId());
        Track2Card track2Card = null;
        if (cardData != null && !WebCoreConstants.EMPTY_STRING.equals(cardData)) {
            // only attempt to make track2data if there is encrypted card data;
            // some processors (eg Elavon) do not require any form of card data to be re-sent with the reversal
            track2Card = new Track2Card(this.transactionFacade.findCreditCardType(cardData), ma.getCustomer().getId(), cardData);
        }
        
        CardProcessor processor = null;
        try {
            processor = this.cardProcessorFactory.getCardProcessor(ma, this.pointOfSaleService.findPointOfSaleById(reversal.getPointOfSaleId()));
        } catch (CardProcessorPausedException cppe) {
            LOG.error(CardProcessingUtil.createCardProcessorPausedExceptionMessage(ma.getProcessor().getId(), ma.getName()));
        }
        if (processor == null) {
            LOG.error("Unable to get processor for reverse request: " + reversal);
            
            reversal.incrementRetryAttempts();
            reversal.setLastRetryTime(new Date());
            reversal.setLastResponseCode(EXCEPTION_STRING);
            this.entityDao.update(reversal);
            return;
        }
        if (checkIfReversalExpired(reversal, processor)) {
            reversal.setIsExpired(true);
            reversal.setLastRetryTime(new Date());
            this.reversalService.updateReversal(reversal);
            LOG.info("Updated reversal request with merchant account id: " + ma.getId() + " and original reference number: "
                     + reversal.getOriginalReferenceNumber() + " to expired.");
        } else {
            try {
                increaseProcessorCounter();
                processor.processReversal(reversal, track2Card);
                
            } catch (Exception e) {
                final String msg = "Unable to reverse request: " + reversal;
                LOG.error(msg, e);
                
                reversal.incrementRetryAttempts();
                reversal.setLastRetryTime(new Date());
                reversal.setLastResponseCode(EXCEPTION_STRING);
                
                final StringBuilder bdr = new StringBuilder();
                bdr.append("processReversal problem, Reversal reversal: ").append(reversal.toString());
                sendAdminErrorAlert(e, bdr.toString());
                
            } finally {
                this.entityDao.update(reversal);
                decreaseProcessorCounter();
            }
        }
    }
    
    /**
     * Helper method to determine if a reversal has been attempted
     * a certain number of times or should otherwise be expired.
     */
    private boolean checkIfReversalExpired(final Reversal reversal, final CardProcessor processor) {
        boolean isExpired = false;
        try {
            isExpired = processor.isReversalExpired(reversal);
        } catch (UnsupportedOperationException e) {
            isExpired = true;
            // expire the attempt if not supported by processor
            // and log the error - should not occur
            LOG.error("Attempting to check expiry on reversal on processor " + processor.getMerchantAccount().getProcessor().getName()
                      + " but this processor does not support reversals");
        }
        return isExpired;
    }
    
    /*
     * @Override
     * public void cleanupOverdueCardData() {
     * // Cleanup preauths that have existed longer than 7 days 15 minutes. This is only for auto-close processors.
     * try {
     * Date expiryDate = DateUtil.createDateByMilliseconds(System.currentTimeMillis() - CardProcessingConstants.MS_TO_ATTEMPT_SETTLEMENT
     * - CardProcessingConstants.FIFTEEN_MINUTES_MS);
     * // retrieve the PreAuth data for backup.
     * Collection<PreAuth> preAuths = preAuthService.getExpiredCardAuthorizationBackUp(expiryDate);
     * if (log.isDebugEnabled()) {
     * log.debug("+++ The number of PreAuth data to be backed up: " + preAuths.size() + " +++");
     * }
     * // back up the expired PreAuth data.
     * processExpiredCardAuthorizationBackUp(preAuths);
     * cleanupOverdueCardData(expiryDate);
     * } catch (Exception e) {
     * String msg = "Unable to cleanup overdue card data";
     * log.error(msg, e);
     * mailerService.sendAdminErrorAlert(msg, msg, e);
     * }
     * }
     */
    /**
     * TODO Replace with original logic AFTER migration is completed .
     */
    @Override
    public final void cleanupOverdueCardData() {
        // Cleanup preauths that have existed longer than 7 days 15 minutes. This is only for auto-close processors.
        
        final Date expiryDate = DateUtil.createDateByMilliseconds(System.currentTimeMillis() - CardProcessingConstants.MS_TO_ATTEMPT_SETTLEMENT
                                                                  - CardProcessingConstants.FIFTEEN_MINUTES_MS);
        
        // retrieve the PreAuth data for backup.
        final ScrollableResults preAuthIds = this.preAuthService.getExpiredCardAuthorizationBackUp(expiryDate);
        
        if (LOG.isDebugEnabled()) {
            LOG.debug("+++ The PreAuth data to be backed up before: " + expiryDate);
        }
        int successCount = 0;
        int failedCount = 0;
        
        // back up the expired PreAuth data.
        while (preAuthIds.next()) {
            try {
                final Long authId = (Long) preAuthIds.get(0);
                self().processPreAuth(authId);
                successCount++;
            } catch (Exception e) {
                final String msg = "Unable to cleanup overdue card data";
                LOG.error(msg, e);
                this.mailerService.sendAdminErrorAlert(msg, msg, e);
                failedCount++;
                continue;
            }
        }
        
        if (LOG.isDebugEnabled()) {
            LOG.debug("+++ Successful count: " + successCount + ", failed count: " + failedCount);
        }
        
        this.cardRetryTransactionService.clearRetriesExceededCardDataForMigratedCustomers();
        this.reversalArchiveService.cleanupCardDataForMigratedCustomers();
    }
    
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public final void processPreAuth(final Long authId) throws CryptoException {
        final PreAuth auth = this.preAuthService.findById(authId);
        copyPreAuthToPreAuthHolding(auth);
        cleanupOverdueCardDataForMigratedCustomers(auth);
    }
    
    @Override
    public final void copyPreAuthToPreAuthHolding(final PreAuth auth) throws CryptoException {
        final PreAuthHolding hold = new PreAuthHolding();
        hold.setPreAuth(auth);
        hold.setResponseCode(auth.getResponseCode());
        hold.setProcessorTransactionId(auth.getProcessorTransactionId());
        hold.setAuthorizationNumber(auth.getAuthorizationNumber());
        hold.setReferenceNumber(auth.getReferenceNumber());
        hold.setMerchantAccount(auth.getMerchantAccount());
        hold.setAmount(auth.getAmount());
        hold.setLast4digitsOfCardNumber(auth.getLast4digitsOfCardNumber());
        hold.setCardType(auth.getCardType());
        hold.setApproved(auth.isIsApproved());
        hold.setPreAuthDate(auth.getPreAuthDate());
        hold.setCardData(getEncryptedCardData(auth));
        hold.setCardExpiry(auth.getCardExpiry());
        hold.setPointOfSale(auth.getPointOfSale());
        hold.setReferenceId(auth.getReferenceId());
        hold.setExpired(WebCoreUtil.convertBooleanToInteger(true).byteValue());
        hold.setCardHash(auth.getCardHash());
        hold.setExtraData(auth.getExtraData());
        hold.setPsRefId(auth.getPsRefId());
        hold.setIsRfid(auth.isIsRfid());
        hold.setAddedToHoldingGMT(new Date());
        saveOrUpdateExpiredCardAuthorizationBackUp(hold);
    }
    
    private void processExpiredCardAuthorizationBackUp(final Collection<PreAuth> preAuths) throws CryptoException {
        for (PreAuth auth : preAuths) {
            copyPreAuthToPreAuthHolding(auth);
        }
    }
    
    private void saveOrUpdateExpiredCardAuthorizationBackUp(final PreAuthHolding preAuthHoldingList) {
        this.entityDao.saveOrUpdate(preAuthHoldingList);
    }
    
    private String getEncryptedCardData(final PreAuth auth) throws CryptoException {
        final int customerId = auth.getMerchantAccount().getCustomer().getId();
        final String cardData = auth.getCardData();
        if (StringUtils.isBlank(cardData)) {
            return null;
        }
        final String decryptedCardData = this.cryptoService.decryptData(cardData, auth.getPointOfSale().getId(), auth.getPreAuthDate());
        final Track2Card track2Card = new Track2Card(this.transactionFacade.findCreditCardType(decryptedCardData), customerId, decryptedCardData);
        
        // encrypt PAN/Expiry only and set the card data.
        return this.cryptoService.encryptData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL, track2Card.getCreditCardPanAndExpiry());
    }
    
    public final void cleanupOverdueCardData(final Date expiredDate) {
        // Cleanup preauths that early than expiredDate
        this.preAuthService.cleanupExpiredPreauths(expiredDate);
        final List<PreAuth> preAuths = this.preAuthService.findExpiredPreAuths(expiredDate);
        if (preAuths != null && !preAuths.isEmpty()) {
            final List<Long> ids = new ArrayList<Long>(preAuths.size());
            for (PreAuth pa : preAuths) {
                ids.add(pa.getId());
            }
            this.processorTransactionService.removePreAuthsInProcessorTransactions(ids);
            // Delete preauths that are expired, and one week older than expiredDate
            this.preAuthService.deleteExpiredPreauths(expiredDate);
        }
        
        // clean up card data for retries exceeded card retry transactions
        this.cardRetryTransactionService.clearRetriesExceededCardData();
        // clean up card data for archived reversals
        this.reversalArchiveService.cleanupCardData();
    }
    
    public final int processTransactionAuthorization(final PreAuth pd, final PointOfSale pointOfSale, final boolean isCreditCard) {
        final Track2Card track2Card;
        if (!isCreditCard) {
            final CustomerCardType customerCardType =
                    this.customerCardTypeService.getCardTypeByTrack2Data(pointOfSale.getCustomer().getId(), pd.getCardData());
            track2Card = new Track2Card(customerCardType, pointOfSale.getCustomer().getId(), pd.getCardData());
        } else {
            track2Card =
                    new Track2Card(this.transactionFacade.findCreditCardType(pd.getCardData()), pointOfSale.getCustomer().getId(), pd.getCardData());
        }
        
        pd.setCardType(track2Card.getDisplayName());
        pd.setLast4digitsOfCardNumber(track2Card.getLast4DigitsOfAccountNumber());
        pd.setPointOfSale(pointOfSale);
        
        // Card is an accepted commercial credit card
        if (track2Card.isCreditCard()) {
            pd.setCardExpiry(Short.parseShort(track2Card.getExpirationYear() + track2Card.getExpirationMonth()));
            if (pd.getPsRefId() != null && !pd.getPsRefId().trim().equals(CardProcessingConstants.SPACE_CHARACTER)) {
                String cardHash = null;
                try {
                    cardHash = this.cryptoAlgorithmFactory.getSha1Hash(track2Card.getCreditCardPanAndExpiry(),
                                                                       CryptoConstants.HASH_CREDIT_CARD_PROCESSING);
                } catch (CryptoException ce) {
                    throw new CardTransactionException(ce);
                }
                
                PreAuth preAuth = this.preAuthService.getApprovedPreAuthByPsRefId(pointOfSale.getId(), pd.getPsRefId(), cardHash);
                // If PreAuth record in the database has a different amount but same PsRefId, find it again with the amount from PS.
                if (preAuth != null && preAuth.getAmount() != pd.getAmount()) {
                    preAuth = this.preAuthService.getApprovedPreAuthByPsRefIdAndPSAmount(pointOfSale.getId(), pd.getPsRefId(), cardHash,
                                                                                         pd.getAmount());
                }
                if (preAuth != null) {
                    pd.setAuthorizationNumber(preAuth.getAuthorizationNumber());
                    pd.setReferenceNumber(preAuth.getReferenceNumber());
                    pd.setId(preAuth.getId());
                    pd.setCardHash(cardHash);
                    if (LOG.isInfoEnabled()) {
                        final StringBuilder bdr = new StringBuilder(55);
                        bdr.append("+++ pay station(").append(pointOfSale.getSerialNumber()).append(", RefId = ").append(pd.getPsRefId())
                                .append("): an existing approved Authorization request found +++");
                        LOG.info(bdr.toString());
                    }
                    return CardProcessingConstants.STATUS_AUTHORIZED;
                }
            }
            return processTransactionAuthorizationCreditCard(pd, pointOfSale, track2Card);
        }
        
        // Card is not an accepted commercial credit card
        return processTransactionAuthorizationNonCreditCard(pd, pointOfSale, track2Card);
    }
    
    private MerchantAccount findMerchantAccount(final Integer pointOfSaleId, final int cardTypeId) {
        return this.merchantAccountService.findByPointOfSaleIdAndCardTypeId(pointOfSaleId, cardTypeId);
    }
    
    /**
     * Return "not deleted" and "enabled" Merchant Account.
     * 
     * @return null if - record doesn't exist
     *         - merchant account IsDeleted = 1
     *         - merchant account status IsDeleted = 1
     */
    private MerchantAccount findValidMerchantAccount(final Integer pointOfSaleId, final int cardTypeId) {
        return this.merchantAccountService.findByPOSAndCardTypeAndNotDeletedAndMerchantStatusEnabled(pointOfSaleId, cardTypeId);
    }
    
    private int processTransactionAuthorizationCreditCard(final PreAuth pd, final PointOfSale pointOfSale, final Track2Card track2Card) {
        try {
            pd.setCardHash(this.cryptoAlgorithmFactory.getSha1Hash(track2Card.getCreditCardPanAndExpiry(),
                                                                   CryptoConstants.HASH_CREDIT_CARD_PROCESSING));
        } catch (CryptoException ce) {
            throw new CardTransactionException(ce);
        }
        
        final MerchantAccount mad = this.merchantAccountService
                .findByPOSAndCardTypeAndNotDeletedAndMerchantStatusEnabled(pointOfSale.getId(), WebCoreConstants.CREDIT_CARD);
        
        CardProcessor processor = null;
        if (mad != null) {
            pd.setMerchantAccount(mad);
            try {
                processor = this.cardProcessorFactory.getCardProcessor(mad, pointOfSale);
            } catch (CardProcessorPausedException cppe) {
                // Send 'EMS unavailable' service' to the PS
                return CardProcessingConstants.STATUS_EMS_UNAVAILABLE;
            }
        }
        
        // Check if a processor was assigned to the payment station
        if (processor == null) {
            final StringBuilder bdr = new StringBuilder(49);
            bdr.append("CardProcessingManagerImpl, processTransactionAuthorizationCreditCard, Unable to authorize card: '")
                    .append(track2Card.getDisplayCardNumber()).append("' for PS: '").append(pointOfSale.getSerialNumber())
                    .append("' as no Credit Card merchant account is assigned.");
            // Send 'unsubscribed' service' to the PS
            LOG.error(bdr.toString());
            return CardProcessingConstants.STATUS_UNSUBSCRIBED_SERVICE;
        }
        
        // Send to processor
        return sendAuthorizationRequestToProcessor(processor, pd, pointOfSale.getCustomer().getId());
    }
    
    private int processTransactionAuthorizationNonCreditCard(final PreAuth pd, final PointOfSale pointOfSale, final Track2Card track2Card) {
        // Determine if this is an accepted value card
        // look for Customer Defined Cards
        final CustomerCardType cardTypeData =
                this.customerCardTypeService.findCustomerCardTypeForTransaction(pointOfSale.getCustomer().getId(), WebCoreConstants.VALUE_CARD,
                                                                                track2Card.getDecryptedCardData());
        if (cardTypeData == null || CardProcessingConstants.NAME_UNKNOWN.equals(cardTypeData.getName())) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append(UNABLE_AUTHORIZE).append(track2Card.getDisplayCardNumber()).append(FOR_POS).append(pointOfSale.getSerialNumber())
                    .append(PAT_N_ACCEPTED);
            LOG.error(bdr.toString());
            return CardProcessingConstants.STATUS_INVALID_CARD;
        }
        
        // always check bad card list
        final Collection<CustomerBadCard> list = this.customerBadCardService
                .getCustomerBadCards(pointOfSale.getCustomer().getId(), track2Card.getDecryptedCardData(), cardTypeData.getId());
        if (list != null && !list.isEmpty()) {
            return CardProcessingConstants.STATUS_BAD_CARD;
        }
        
        // Authorize card externally
        if (WebCoreConstants.AUTH_TYPE_EXTERNAL_SERVER == cardTypeData.getAuthorizationType().getId()) {
            final MerchantAccount mad = findMerchantAccount(pointOfSale.getId(), WebCoreConstants.VALUE_CARD);
            if (mad == null) {
                LOG.info(createMerchantAccountErrorMessage(track2Card.getDisplayCardNumber(), pointOfSale.getSerialNumber(), FIND_MERCH_ACT));
                return CardProcessingConstants.STATUS_UNSUBSCRIBED_SERVICE;
            }
            
            pd.setPreAuthDate(DateUtil.getCurrentGmtDate());
            pd.setMerchantAccount(mad);
            pd.setCardData(track2Card.getDecryptedCardData());
            try {
                pd.setCardHash(this.cryptoAlgorithmFactory.getSha1Hash(pd.getCardData(), CryptoConstants.HASH_CREDIT_CARD_PROCESSING));
            } catch (CryptoException ce) {
                throw new CardTransactionException(ce);
            }
            
            CardProcessor processor = null;
            try {
                processor = this.cardProcessorFactory.getCardProcessor(mad, pointOfSale);
            } catch (CardProcessorPausedException cppe) {
                return CardProcessingConstants.STATUS_EMS_UNAVAILABLE;
            }
            // Check if a processor was assigned to the payment station
            if (processor == null) {
                // Send 'unsubscribed service' to the PS
                LOG.info(createMerchantAccountErrorMessage(track2Card.getDisplayCardNumber(), pointOfSale.getSerialNumber(), FIND_PROC));
                return CardProcessingConstants.STATUS_UNSUBSCRIBED_SERVICE;
            }
            return sendAuthorizationRequestToProcessor(processor, pd, pointOfSale.getCustomer().getId());
        }
        
        // Authorize card locally
        if (isLocalCustomCardAuthorized(cardTypeData, pointOfSale, track2Card)) {
            return CardProcessingConstants.STATUS_SUCCESS;
        }
        
        return CardProcessingConstants.STATUS_DENIED;
    }
    
    private boolean isLocalCustomCardAuthorized(final CustomerCardType cardType, final PointOfSale ps, final Track2Card track2Card) {
        
        if (WebCoreConstants.AUTH_TYPE_NONE == cardType.getAuthorizationType().getId()) {
            return true;
        } else if (WebCoreConstants.AUTH_TYPE_INTERNAL_LIST == cardType.getAuthorizationType().getId()) {
            return isLocalCustomCardValidAgainstList(cardType, ps, track2Card);
        }
        
        if (CardProcessingConstants.NAME_PATROLLER_CARD.equalsIgnoreCase(cardType.getName())) {
            // patroller is offline only card
            return false;
        } else {
            // Oops, specified validation method is unknown
            throw new CardTransactionException("Unknown Card Validation Method: " + cardType.getAuthorizationType().getName());
        }
    }
    
    private boolean isLocalCustomCardValidAgainstList(final CustomerCardType cardType, final PointOfSale ps, final Track2Card track2Card) {
        // Check if there are any value cards
        final int count = this.customerCardService.findTotalCustomerCardsByCustomerId(cardType.getCustomer().getId());
        
        if (LOG.isInfoEnabled()) {
            // No cards, so by default card is valid
            LOG.info("# cards in Digital Iris list: " + count);
        }
        if (count == 0) {
            return true;
        }
        
        // Get card from DB
        final CustomerCard card =
                this.customerCardService.findCustomerCardByCustomerIdAndCardNumber(ps.getCustomer().getId(), track2Card.getPrimaryAccountNumber());
        // Does card exist?
        if (LOG.isInfoEnabled()) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("Is card(").append(track2Card.getDisplayCardNumber()).append(") in Digital Iris list: ").append(card != null);
            LOG.info(bdr.toString());
        }
        if (card == null) {
            return false;
        }
        
        // Check number of uses
        if (card.getMaxNumberOfUses() != null && card.getMaxNumberOfUses() != 0 && card.getNumberOfUses() != null
            && card.getMaxNumberOfUses().shortValue() <= card.getNumberOfUses().shortValue()) {
            if (LOG.isInfoEnabled()) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append("Card(").append(track2Card.getDisplayCardNumber()).append(") used all uses: ").append(card != null);
                LOG.info(bdr.toString());
            }
            return false;
        }
        
        // Get current time
        final long currentDateMS = System.currentTimeMillis();
        final String timeZone = this.webWidgetHelperService.getCustomerTimeZoneByCustomerId(cardType.getCustomer().getId());
        
        // Is it before card's start date?
        final Date startDate = card.getCardBeginGmt();
        if (startDate != null) {
            // Convert card start date to UTC assuming card start date is in PS
            // local time.
            long startDateMS = startDate.getTime();
            startDateMS -= TimeZone.getTimeZone(timeZone).getOffset(startDateMS);
            
            if (LOG.isInfoEnabled()) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append("Comparing card start date(").append(startDate).append(LEFT_ROUND_COLON).append(startDateMS).append(W_CUR_TIME)
                        .append(currentDateMS);
                LOG.info(bdr.toString());
            }
            
            if (startDateMS >= currentDateMS) {
                return false;
            }
        }
        
        // Is end date specified? If so, is the current date after the card's
        // end time?
        // End date is specifed in DB as the beginning of the day after the
        // selected day.
        final Date endDate = card.getCardExpireGmt();
        if (endDate != null) {
            // Convert card end date to UTC assuming card end date is in PS
            // local time.
            long endDateMS = endDate.getTime();
            endDateMS -= TimeZone.getTimeZone(timeZone).getOffset(endDateMS);
            
            if (LOG.isInfoEnabled()) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append("Comparing card end date(").append(endDate).append(LEFT_ROUND_COLON).append(endDateMS).append(W_CUR_TIME)
                        .append(currentDateMS);
                LOG.info(bdr.toString());
            }
            
            if (endDateMS <= currentDateMS) {
                return false;
            }
        }
        
        /*
         * Valid for particular PofS
         * CustomerCard might not have a PointOfSale.
         */
        int pointOfSaleId = 0;
        if (card.getPointOfSale() != null) {
            pointOfSaleId = card.getPointOfSale().getId();
        }
        if (pointOfSaleId != 0) {
            if (LOG.isInfoEnabled()) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append("Comparing card PointOfSale: ").append(pointOfSaleId).append(" with current PointOfSale: ").append(ps.getId());
                LOG.info(bdr.toString());
            }
            
            if (pointOfSaleId != ps.getId()) {
                return false;
            }
            // We pass
        }
        
        /*
         * Not valid for particular PS
         * CustomerCard might not have a Location.
         */
        else {
            int locationId = 0;
            if (card.getLocation() != null) {
                locationId = card.getLocation().getId();
            }
            if (locationId != 0) {
                // Valid for an entire region
                Location location = ps.getLocation();
                
                // Loop through regions to root or until we find a match
                do {
                    if (LOG.isInfoEnabled()) {
                        final StringBuilder bdr = new StringBuilder();
                        bdr.append("Comparing card location: ").append(locationId).append(" with current location: ").append(location.getId());
                        LOG.info(bdr.toString());
                    }
                    
                    if (locationId == location.getId()) {
                        // We pass
                        break;
                    }
                    
                    if (location.getLocation() != null) {
                        /*
                         * Retrieve parent location by id and currently the database only uses 'child-parent' relationship but no
                         * multi-level.
                         */
                        location = this.locationService.findLocationById(location.getLocation().getId());
                    } else {
                        location = null;
                    }
                    
                } while (location != null);
                
                // We hit the root region -> no match
                if (location == null) {
                    return false;
                }
            }
            // We pass
        }
        
        // Is card restricted to one valid ticket? If so, does it exceed the max # of valid tickets?
        if (card.isIsForNonOverlappingUse()) {
            final long graceExpiry =
                    currentDateMS + (card.getGracePeriodMinutes() != null ? card.getGracePeriodMinutes() * StandardConstants.MINUTES_IN_MILLIS_1 : 0);
            final Date graceExpiryDate = new Date(graceExpiry);
            
            final PaymentCard paymentCard = this.paymentCardService.findLatestByCustomerCardId(card.getId());
            if (paymentCard != null) {
                final Permit permit = this.permitService.findLatestExpiredPermitByPurchaseId(paymentCard.getPurchase().getId());
                if (graceExpiryDate.before(permit.getPermitExpireGmt())) {
                    return false;
                }
            }
        }
        
        // We passed all requirements
        return true;
    }
    
    private int sendAuthorizationRequestToProcessor(final CardProcessor processor, final PreAuth pd, final int customerId) {
        try {
            increaseProcessorCounter();
            final int ret = sendAuthorizationRequestToProcessor(processor, pd);
            // only log credit card 
            if (!pd.isIsApproved() && customerId > 0) {
                saveCcFailLog(pd, pd.getResponseCode());
            }
            return ret;
        } catch (Exception e) {
            if (LOG.isDebugEnabled() && (e instanceof CardTransactionException)) {
                LOG.debug("PreAuth failed for MerchantAccount: " + processor.getMerchantAccount().getId(), e);
            }
            
            if (customerId > 0) {
                saveCcFailLog(pd, e.getMessage());
            }
            return CardProcessingConstants.STATUS_EMS_UNAVAILABLE;
        } finally {
            decreaseProcessorCounter();
        }
    }
    
    private void saveCcFailLog(final PreAuth pd, final String inReason) {
        final CcFailLog ccFailLog = new CcFailLog();
        String reason = inReason;
        ccFailLog.setPointOfSale(pd.getPointOfSale());
        ccFailLog.setMerchantAccount(pd.getMerchantAccount());
        
        ccFailLog.setProcessingDate(new Date());
        ccFailLog.setPurchasedDate(ccFailLog.getProcessingDate());
        ccFailLog.setCctype(CardProcessingConstants.CC_TYPE_AUTHORIZATION);
        if (StringUtils.isBlank(reason)) {
            reason = "Unable to send data to the end-point";
        }
        ccFailLog.setReason(reason);
        this.ccFailLogService.logFailedCcTransaction(ccFailLog);
    }
    
    private int sendAuthorizationRequestToProcessor(final CardProcessor processor, final PreAuth pd) {
        try {
            try {
                final Method method = CardProcessor.class.getDeclaredMethod("processPreAuth", new Class[] { PreAuth.class });
                method.setAccessible(true);
                this.kpiListenerService.invokeProcessPreAuth(processor, method, new Object[] { pd });
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                processor.processPreAuth(pd);
            } catch (SecurityException e) {
                e.printStackTrace();
                processor.processPreAuth(pd);
            }
            // Encrypt CC data
            final String encryptCCData = this.cryptoService.encryptData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL, pd.getCardData());
            pd.setCardData(encryptCCData);
            
        } catch (CryptoException ce) {
            pd.setIsApproved(false);
            pd.setAuthorizationNumber(EXCEPTION_STRING);
            throw new CardTransactionException(ce);
        } catch (CardTransactionException cte) {
            pd.setIsApproved(false);
            if (cte.getCause() != null && cte.getCause() instanceof SocketTimeoutException) {
                pd.setAuthorizationNumber(TIMEOUT_STRING);
                printTimeoutInformation(cte.getCause());
            } else {
                pd.setAuthorizationNumber(EXCEPTION_STRING);
            }
            
            // Nested CardTransactionException to make it easier to trace the stack trace.
            throw new CardTransactionException(cte);
        } catch (XMLErrorException xmlee) {
            pd.setIsApproved(false);
            if (xmlee.getCause() instanceof SocketTimeoutException) {
                pd.setAuthorizationNumber(TIMEOUT_STRING);
                printTimeoutInformation(xmlee);
            } else {
                pd.setAuthorizationNumber(EXCEPTION_STRING);
            }
            throw xmlee;
        } finally {
            try {
                // Clear card data if not approved
                if (!pd.isIsApproved()) {
                    pd.setCardData(null);
                }
                // Save preauth record
                // preAuthDao_.createPreAuth(pd);
                if (!processor.getIsManualSave()) {
                    this.entityDao.save(pd);
                } else {
                    processor.savePreAuth(pd);
                }
            } catch (Exception e) {
                final String msg = "Unable to insert preauth attempt. " + pd;
                this.mailerService.sendAdminErrorAlert("Insert preauth error", msg, e);
                LOG.error(msg, e);
                throw e;
            }
        }
        
        // Process card authorization and card has been approved
        if (pd.isIsApproved()) {
            return CardProcessingConstants.STATUS_AUTHORIZED;
        }
        
        // Card has been denied
        return CardProcessingConstants.STATUS_DENIED;
    }
    
    private void printTimeoutInformation(final Throwable t) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("Socket Timeout Exception - need to review time out values in EmsProperties table. ");
        bdr.append("Return CARD_STATUS_EMS_UNAVAILABLE");
        LOG.error(bdr.toString(), t);
    }
    
    public final Date getMaxRetryTimeForTransaction(final CardRetryTransaction cardRetryTransaction) {
        return this.cardRetryTransactionService.getMaxRetryTimeForTransaction(cardRetryTransaction);
    }
    
    /**
     * @param testSerialNumberForValueCard
     *            It can be null for non-value card transaction.
     * @param testCardTypeId
     *            e.g. CardProcessingConstants.INTERNAL_TYPE_CREDIT_CARD, CardProcessingConstants.INTERNAL_TYPE_CUSTOM_CARD
     * @return String test result.
     */
    @Override
    public final String processTransactionTestCharge(final MerchantAccount merchantAccount, final int amount,
        final String testSerialNumberForValueCard, final Integer testCardTypeId) {
        merchantAccount.setIsValidated(false);
        
        final PointOfSale testPofS = new PointOfSale();
        testPofS.setSerialNumber(testSerialNumberForValueCard);
        testPofS.setId(0);
        
        try {
            increaseProcessorCounter();
            CardProcessor processor = null;
            try {
                processor = this.cardProcessorFactory.getCardProcessor(merchantAccount, testPofS, true);
            } catch (ProcessorAuthenticationException pae) {
                String message = pae.getCause().getMessage();
                message = message.replaceAll("Error: StatusCode: ", "");
                message = message.replaceAll(":", "-");
                final StringBuilder error = new StringBuilder();
                return error.append(WebCoreConstants.RESPONSE_FALSE).append(StandardConstants.STRING_COLON).append(message).toString();
            }
            
            // Cannot find the requested merchant account - shouldn't be possible under normal use
            if (processor == null) {
                return new StringBuilder(WebCoreConstants.RESPONSE_FALSE).append(StandardConstants.STRING_COLON)
                        .append(this.messageAccessor.getMessage("error.merchantaccount.invalid.processor")).toString();
            }
            
            try {
                return processor.testCharge(merchantAccount);
            } catch (UnsupportedOperationException e) {
                // if not supported, continue with existing logic
                LOG.warn(e);
            }
            // Make up ProcessorTransaction
            final ProcessorTransaction chargePtd = new ProcessorTransaction();
            chargePtd.setIsApproved(false);
            chargePtd.setAmount(amount);
            chargePtd.setPurchasedDate(new Date());
            chargePtd.setPointOfSale(testPofS);
            
            // Make up track2 card object
            final String expiryYYMM = new DecimalFormat("0000").format(DateUtil.createCurrentYYMM() + StandardConstants.CONSTANT_100);
            
            final StringBuilder bdr = new StringBuilder();
            bdr.append(this.testCreditCardNumberService.getDefaultTestCreditCardNumber()).append(WebCoreConstants.EQUAL_SIGN).append(expiryYYMM);
            String accountData = bdr.toString();
            
            StringBuilder bdr2 = null;
            
            // First Data Nashville, Link2Gov, Paradata, Heartland, Heartland-SP+, Moneris QA have special account
            switch (merchantAccount.getProcessor().getId()) {
                case CardProcessingConstants.PROCESSOR_ID_FIRST_DATA_NASHVILLE:
                    chargePtd.setAmount(FIRST_DATA_NASHVILLE_TEST_AMT);
                    accountData = this.testCreditCardNumberService.getDefaultTestFirstDataNashvilleCreditcardNumber() + StandardConstants.STRING_EQAL
                                  + expiryYYMM;
                    break;
                
                case CardProcessingConstants.PROCESSOR_ID_LINK2GOV:
                    accountData =
                            this.testCreditCardNumberService.getDefaultTestLink2GovCreditCardNumber() + StandardConstants.STRING_EQAL + expiryYYMM;
                    chargePtd.setAmount(LINK2GOV_TEST_AMT);
                    break;
                
                case CardProcessingConstants.PROCESSOR_ID_PARADATA:
                    bdr2 = new StringBuilder();
                    bdr2.append(this.testCreditCardNumberService.getDefaultTestParadataCreditCardNumber()).append(StandardConstants.STRING_EQAL)
                            .append(expiryYYMM);
                    accountData = bdr2.toString();
                    // $0.01 will generate a response code 1 for 'Successful transaction: The transaction completed successfully.' message.
                    chargePtd.setAmount(PARADATA_TEST_AMOUNT);
                    break;
                
                case CardProcessingConstants.PROCESSOR_ID_HEARTLAND:
                case CardProcessingConstants.PROCESSOR_ID_HEARTLAND_SPPLUS:
                    bdr2 = new StringBuilder();
                    bdr2.append(this.testCreditCardNumberService.getDefaultTestHeartlandCardNumberApproved())
                        .append(StandardConstants.STRING_EQAL).append(this.testCreditCardNumberService.getDefaultTestHeartlandExpiryYYMM());
                    accountData = bdr2.toString();
                    chargePtd.setAmount(HEARTLAND_TEST_AMT);
                    break;
                
                case CardProcessingConstants.PROCESSOR_ID_MONERIS:
                    if (merchantAccount.getProcessor().isIsTest()) {
                        bdr2 = new StringBuilder();
                        bdr2.append(this.testCreditCardNumberService.getDefaultTestMonerisCreditCardNumber()).append(StandardConstants.STRING_EQAL)
                                .append(expiryYYMM);
                        accountData = bdr2.toString();
                        // $0.10
                        chargePtd.setAmount(MONERIS_TEST_AMT);
                    }
                    break;
                
                //TODO double check this -> added by checkstyle
                default:
                    break;
            }
            
            // e.g. customerCardType.getCardType().getId() == CardProcessingConstants.INTERNAL_TYPE_CREDIT_CARD
            final CardType testCardType = getCardType(testCardTypeId);
            final int digitalDptCustomerId = 1;
            final Track2Card track2Card;
            // Value Card
            if (testSerialNumberForValueCard != null) {
                final CustomerCardType testCustomerCardType = new CustomerCardType();
                testCustomerCardType.setCardType(testCardType);
                testCustomerCardType.setName("Test Customer Card Type");
                track2Card = new Track2Card(testCustomerCardType, digitalDptCustomerId, accountData);
            } else {
                // Credit Card
                track2Card = new Track2Card(this.transactionFacade.findCreditCardType(accountData), digitalDptCustomerId, accountData);
            }
            
            // Process transaction
            String response = processTransactionTestCharge(processor, track2Card, chargePtd);
            final boolean responseStatus = isTestSuccessful(response);
            final String responseMessage;
            if (responseStatus) {
                responseMessage = WebCoreConstants.RESPONSE_TRUE;
            } else {
                response = response.replaceAll(WebCoreConstants.PROCESS_TRANSACTION_TEST_CHARGE_FAILED + StandardConstants.STRING_COMMA,
                                               WebCoreConstants.EMPTY_STRING);
                responseMessage = new StringBuffer(WebCoreConstants.RESPONSE_FALSE).append(StandardConstants.STRING_COLON)
                        .append(this.messageAccessor.getMessage("error.merchantaccount.invalid.merchantaccount", new Object[] { response }))
                        .toString();
            }
            return responseMessage;
        } catch (Exception e) {
            return WebCoreConstants.RESPONSE_FALSE;
        } finally {
            decreaseProcessorCounter();
        }
    }
    
    private boolean isTestSuccessful(final String response) {
        
        if (response.startsWith(WebCoreConstants.PROCESS_TRANSACTION_TEST_CHARGE_FAILED)) {
            return false;
        }
        final String propKey = "cardprocessor.merchantaccount.test.failure.codes";
        final String failureCodes = this.mappingProperties.getProperty(propKey);
        
        if (StringUtils.isBlank(failureCodes)) {
            final StringBuilder bdr = new StringBuilder(38);
            bdr.append("CardProcessingManagerImpl, isTestSuccessful method, unable to find '").append(propKey)
                    .append("' in mapping.properties, return 'true'");
            LOG.error(bdr.toString());
            this.mailerService.sendAdminErrorAlert(bdr.toString(), bdr.toString(), null);
            return true;
        }
        final String[] codes = StringUtil.split(failureCodes, ',');
        for (String code : codes) {
            if (response.indexOf(StringUtils.deleteWhitespace(code)) != -1) {
                return false;
            }
        }
        return true;
    }
    
    private CardType getCardType(final Integer cardTypeId) {
        this.cardTypeService.loadAll();
        return this.cardTypeService.getCardTypesMap().get(cardTypeId);
    }
    
    private String processTransactionTestCharge(final CardProcessor processor, final Track2Card track2Card, final ProcessorTransaction chargePtd) {
        try {
            // Process transaction
            Object[] responses = null;
            try {
                final Method method =
                        CardProcessor.class.getDeclaredMethod(PROCESS_CHARGE, new Class[] { Track2Card.class, ProcessorTransaction.class });
                method.setAccessible(true);
                responses = (Object[]) this.kpiListenerService.invoke(processor, method, new Object[] { track2Card, chargePtd });
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                responses = processor.processCharge(track2Card, chargePtd);
            } catch (SecurityException e) {
                e.printStackTrace();
                responses = processor.processCharge(track2Card, chargePtd);
            }
            
            final int responseCode = (Integer) responses[0];
            
            final StringBuilder bdr = new StringBuilder(46);
            bdr.append("MerchantAccount is valid.  Recieved Response: ").append(DPTResponseCodesMapping.mapValueToString(responseCode));
            return bdr.toString();
        } catch (Exception e) {
            LOG.error("CardProcessingManagerImpl, processTransactionTestCharge, unable to test transaction. ", e);
            final StringBuilder bdr = new StringBuilder();
            bdr.append(WebCoreConstants.PROCESS_TRANSACTION_TEST_CHARGE_FAILED).append(StandardConstants.STRING_COMMA).append(e.getMessage());
            return bdr.toString();
        }
    }
    
    @Override
    public final void processTransactionSettlement(final ProcessorTransaction processorTransaction) {
        final ProcessorTransaction ptd;
        String msg;
        
        if (processorTransaction.isLoaded()) {
            ptd = new ProcessorTransaction(processorTransaction);
        } else {
            ptd = processorTransaction;
        }
        
        if (LOG.isInfoEnabled()) {
            LOG.info("Settling Transaction: " + ptd);
        }
        try {
            final PreAuth pd = getApprovedPreAuthAndEvict(ptd);
            
            // No preauths could be located
            if (pd == null) {
                /*
                 * cleanupIfExpired returns # of records update
                 * If equal to 0 means, not expired thus we have an issue settling.
                 * If equal to -1 means it's for single-phase processor and ProcessorTransaction was updated and PreAuthHolding was deleted.
                 */
                if (cleanupIfExpired(ptd) == 0) {
                    msg = "Unable to settle transaction as there are no matching or there are multiple preauths, ProcessorTransaction id: "
                          + ptd.getId();
                    LOG.warn(msg);
                    this.mailerService.sendAdminErrorAlert("Unable to settle TX ", msg, null);
                } else {
                    msg = "Unable to settle transaction as it is expired, ProcessorTransaction id: " + ptd.getId()
                          + ". Or for single-phase processor ProcessorTransaction was updated and PreAuthHolding was deleted.";
                    // No preauth, but transaction is expired therefore no error alert
                    LOG.warn(msg);
                }
                return;
            }
            
            // Settle transaction
            if (settleTransaction(pd, ptd)) {
                LOG.info("Settlement of transaction has been approved, ProcessorTransaction id: " + ptd.getId());
            } else {
                LOG.info("Settlement of transaction has been declined, ProcessorTransaction id: " + ptd.getId());
            }
        } catch (Exception e) {
            msg = "Unable to settle transaction as there was a problem settling, ProcessorTransaction: " + ptd;
            LOG.error(msg, e);
            this.mailerService.sendAdminErrorAlert("processTransactionSettlement issue", msg, e);
        }
    }
    
    /**
     * Find approved PreAuth record using the preAuthId stored in ProcessorTransaction. Check if customerId in PreAuth's MerchantAccount is the same
     * as in ProcessorTransaction's PointOfSale.
     * 
     * @param ptd
     * @return
     */
    private PreAuth getApprovedPreAuthAndEvict(final ProcessorTransaction ptd) {
        if (ptd.getPreAuth() != null) {
            final PreAuth preAuth = this.preAuthService.getApprovedPreAuthById(ptd.getPreAuth().getId());
            if (preAuth != null
                && preAuth.getMerchantAccount().getCustomer().getId().intValue() == ptd.getPointOfSale().getCustomer().getId().intValue()) {
                this.entityDao.evict(preAuth);
                return preAuth;
            }
        }
        return null;
    }
    
    @Override
    public final PreAuth getPreAuth(final ProcessorTransaction ptd) {
        if (ptd.getPreAuth() == null) {
            return null;
        }
        
        final long preauthId = ptd.getPreAuth().getId();
        PreAuth preauth = null;
        
        // PS2 Versions >= 5.2 AND PS1 Versions >= 3.5.7
        if (preauthId > 0) {
            // preauths = preAuthDao_.getApprovedPreAuthByIdAndEvict(preauth_id);
            preauth = this.preAuthService.getApprovedPreAuthById(preauthId);
        } else {
            // PS2 Versions < 5.2 and PS1 Version < 3.5.7
            final long purchasedDateMS = ptd.getPurchasedDate().getTime();
            try {
                preauth = this.preAuthService
                        .getApprovedPreAuthByPSAmountAndAuthNumAndDate(ptd.getPointOfSale().getId(), ptd.getAmount(),
                                                                       ptd.getSanitizedAuthorizationNumber(),
                                                                       purchasedDateMS - CardProcessingConstants.PREAUTH_TIME_RANGE,
                                                                       purchasedDateMS + CardProcessingConstants.PREAUTH_TIME_RANGE);
            } catch (DataAccessException e) {
                // try other query to see if it works
            }
            
            // If cannot find a preauth in specified time range, try entire table
            if (preauth == null) {
                preauth = this.preAuthService.getApprovedPreAuthByPSAmountAndAuthNum(ptd.getPointOfSale().getId(), ptd.getAmount(),
                                                                                     ptd.getSanitizedAuthorizationNumber());
            }
        }
        
        if (preauth != null) {
            return preauth;
        }
        return null;
    }
    
    private boolean settleTransaction(final PreAuth pd, final ProcessorTransaction ptd) {
        final PointOfSale pointOfSale = pd.getPointOfSale();
        
        /*
         * PreAuth object is retrieved from the database in 'processTransactionSettlement' method.
         * Get processor object for specified merchant account.
         */
        final MerchantAccount ma = pd.getMerchantAccount();
        CardProcessor processor = null;
        try {
            processor = this.cardProcessorFactory.getCardProcessor(ma, pointOfSale);
        } catch (CardProcessorPausedException cppe) {
            final String msg = CardProcessingUtil.createCardProcessorPausedExceptionMessage(ma.getProcessor().getId(), ma.getName());
            handleFailedPostAuth(pd, ptd, EXCEPTION_STRING, msg, EXCEPTION_STRING, cppe);
            return false;
        }
        
        try {
            increaseProcessorCounter();
            // Set ProcessorTransactionData values from PreAuth
            ptd.setCardType(pd.getCardType());
            ptd.setLast4digitsOfCardNumber(pd.getLast4digitsOfCardNumber());
            ptd.setCardHash(pd.getCardHash());
            ptd.setMerchantAccount(ma);
            ptd.setIsRfid(pd.isIsRfid());
            // Decrypt card data
            if (!CardProcessingUtil.isNotNullAndIsLink(ma) && StringUtils.isNotBlank(pd.getCardData())) {
                pd.setCardData(this.cryptoService.decryptData(pd.getCardData()));
            }
            
            // Send to processor
            return settleTransaction(processor, pd, ptd);
        } catch (Exception e) {
            final String msg = POSTAUTH_EXCEPTION + ptd;
            LOG.error(msg, e);
            handleFailedPostAuth(pd, ptd, EXCEPTION_STRING, msg, EXCEPTION_STRING, e);
        } finally {
            decreaseProcessorCounter();
        }
        return false;
    }
    
    private boolean settleTransaction(final CardProcessor processor, final PreAuth pd, final ProcessorTransaction ptd) {
        try {
            // Send to processor
            Object[] responses;
            try {
                final Method method = CardProcessor.class.getDeclaredMethod(PROC_POSTAUTH, new Class[] { PreAuth.class, ProcessorTransaction.class });
                method.setAccessible(true);
                responses = (Object[]) this.kpiListenerService.invoke(processor, method, new Object[] { pd, ptd });
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                responses = processor.processPostAuth(pd, ptd);
            } catch (SecurityException e) {
                e.printStackTrace();
                responses = processor.processPostAuth(pd, ptd);
            }
            if (((Boolean) responses[0]).booleanValue()) {
                /*
                 * Approved, update ProcessorTransaction & PaymentCard domain objects, delete PreAuth
                 * and let Hibernate to issue SQL statements.
                 * ProcesssorTransaction, set the following properties:
                 * - processorTransactionType to 2 or 12. If it has a Link Merchant Account, processTransactionType stays Auth PlaceHolder (1).
                 * - preAuthId to null
                 * - isApproved = 1 for non-link
                 */
                if (ptd.isIsOffline()) {
                    ptd.setProcessorTransactionType(this.processorTransactionService
                            .getProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_EMS_RETRY_WITH_REFUNDS));
                } else if (!CardProcessingUtil.isNotNullAndIsLink(pd.getMerchantAccount())) {
                    ptd.setProcessorTransactionType(this.processorTransactionService
                            .getProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS));
                }
                
                ptd.setAuthorizationNumber(ptd.getAuthorizationNumberAndPreAuthId());
                ptd.setPreAuth(null);

                /*
                 * In case it's a LINK MA, even though CPSCaptureChargeResultConsumerTask and CPSProcessDataServiceImpl will update it later
                 * Leaving setIsApproved = true will make it show up in the reports as Auth placeholder
                 */
                ptd.setIsApproved(true);
                
                /*
                 * PaymentCard, set processorTransactionType to 2 or 12.
                 */
                final PaymentCard pc =
                        this.paymentCardService.findPaymentCardByPurchaseIdAndProcessorTransactionId(ptd.getPurchase().getId(), ptd.getId());
                if (pc != null) {
                    pc.setProcessorTransactionType(ptd.getProcessorTransactionType());
                    this.entityDao.update(pc);
                }
                
                if (processor.getIsManualSave()) {
                    processor.updatePostAuthDetails(pd, ptd);
                } else {
                    this.entityDao.update(ptd);
                }
                
                /*
                 * Remove PreAuth.
                 */
                this.entityDao.delete(pd);
                
                return true;
            }
            
            // Not approved
            String resp = "";
            if (responses[1] != null) {
                resp = responses[1].toString();
            }
            saveFailedCCTransaction(ptd.getPointOfSale(), ptd.getMerchantAccount(), ptd.getProcessingDate(), ptd.getPurchasedDate(),
                                    ptd.getTicketNumber(), CardProcessingConstants.CC_TYPE_SETTLEMENT, resp);
            insertFailedPostAuthIntoProcessorTransaction(pd, ptd, "DECLINED");
        } catch (Exception e) {
            final String msg = POSTAUTH_EXCEPTION + ptd;
            LOG.error(msg, e);
            handleFailedPostAuth(pd, ptd, EXCEPTION_STRING, msg, EXCEPTION_STRING, e);
        }
        return false;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final void routeTransactionChargeProcessing(final CardRetryTransaction crt, final Date maxRetryTime) throws CryptoException {
        
        final PointOfSale pos = this.entityDao.get(PointOfSale.class, crt.getPointOfSale().getId());
        
        if (this.isManualSave(pos)) {
            self().processTransactionChargeManualSave(crt, null);
        } else {
            self().processTransactionCharge(crt, null);
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public final void processTransactionChargeManualSave(final CardRetryTransaction inCrt, final Date maxRetryTime) {
        if (LOG.isDebugEnabled()) {
            LOG.debug(CHARGE_TRANS_W_CARD_RETRT_TRANS + inCrt.getPurchasedDate());
        }
        
        // Associate CardRetryTransaction with current session
        final CardRetryTransaction crt = sync(inCrt);
        
        // Check if already approved
        final ProcessorTransaction ptd =
                this.processorTransactionService.getApprovedTransaction(crt.getPointOfSale().getId(), crt.getPurchasedDate(), crt.getTicketNumber());
        if (ptd != null) {
            this.entityDao.evict(ptd);
        }
        
        // If approved transaction exists, remove CardRetryTransaction record and exit
        if (ptd != null && ptd.getProcessorTransactionType().getId() != CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_RECOVERABLE) {
            self().removeCardRetryTransaction(crt.getId());
            return;
        }
        
        // No approved transaction exists
        
        // Check max retry time
        final Date date = getMaxRetryTimeForTransaction(crt);
        
        // If declined transaction was already processed tonite, do not process again.
        // If maxRetryTime is null, then this is S&F
        if (date != null && maxRetryTime != null && date.after(maxRetryTime)) {
            return;
        }
        
        boolean isCrtEvicted = false;
        
        // Safe to process transaction
        final ProcessorTransaction chargePt = createProcessorTransaction(crt);
        try {
            increaseProcessorCounter();
            final PointOfSale ps = crt.getPointOfSale();
            final MerchantAccount mad = chargePt.getMerchantAccount();
            final CardProcessor processor = this.cardProcessorFactory.getCardProcessor(mad, ps);
            
            // Cannot find the requested merchant account
            if (processor == null) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append(CANT_OBTAIN_PROC_OBJ).append(ps.getSerialNumber()).append(MERCH_ACC_NULL).append(mad);
                LOG.error(bdr.toString());
                this.mailerService.sendAdminErrorAlert(PROC_NULL, bdr.toString(), null);
                return;
            }
            
            // For store and forward, for card data kept in memory, it is not encrypted
            // This only occurs during during initial processing attempt of S&F
            // Credit card data encrypted is min 50 bytes and raw is max 37 bytes
            String decryptedCardData = crt.getCardData();
            if (decryptedCardData.length() > MIN_DECRYPTED_CARD_DATA_LEN) {
                decryptedCardData = this.cryptoService.decryptData(crt.getCardData(), ps.getId(), crt.getPurchasedDate());
            }
            
            final Integer customerId = crt.getPointOfSale().getCustomer().getId();
            final Track2Card track2Card = new Track2Card(this.transactionFacade.findCreditCardType(decryptedCardData), customerId, decryptedCardData);
            
            // set ProcessorTransaction fields
            chargePt.setLast4digitsOfCardNumber(track2Card.getLast4DigitsOfAccountNumber());
            chargePt.setMerchantAccount(mad);
            
            // Check for new encryption key, if so re-encrypt data
            try {
                if (this.cryptoService.isNewKeyForData(crt.getCardData())) {
                    final String newCardData =
                            this.cryptoService.encryptData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL, track2Card.getCreditCardPanAndExpiry());
                    crt.setCardData(newCardData);
                }
            } catch (CryptoException ce) {
                LOG.error(CRYPTO_SERVICE_WARN_CRYPTO_EXCEPTION, ce);
            }
            
            // Process transaction
            this.entityDao.evict(crt);
            isCrtEvicted = true;
            
            processTransactionChargeManualStoreAndForward(processor, crt, chargePt, track2Card, ptd);
            
            return;
        } catch (MissingCryptoKeyException mcke) {
            LOG.warn(FAIL_LOCATE_KEY, mcke);
        } catch (Exception e) {
            
            LOG.error(UNABLE_CHARGE_RETY_TRANS + crt, e);
            
            if (!isCrtEvicted) {
                this.entityDao.evict(crt);
            }
            
            self().handleFailedCharge(chargePt, crt, EXCEPTION_STRING, EMS_UNAVAILABLE_VAL, e);
            
            return;
        } finally {
            decreaseProcessorCounter();
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public final void processLinkCPSStoreAndForward(final ProcessorTransaction processorTransaction,
        final CardRetryTransaction cardRetryTransaction) throws CryptoException {
        this.cardProcessorFactory.getLinkCPSProcessor(processorTransaction.getMerchantAccount(), processorTransaction.getPointOfSale())
                .processLinkStoreAndForward(processorTransaction, cardRetryTransaction);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public final void processTransactionCharge(final CardRetryTransaction inCrt, final Date maxRetryTime) throws CryptoException {
        if (LOG.isDebugEnabled()) {
            LOG.debug(CHARGE_TRANS_W_CARD_RETRT_TRANS + inCrt.getPurchasedDate());
        }
        
        // Associate CardRetryTransaction with current session
        final CardRetryTransaction crt = sync(inCrt);
        
        // Check if already approved
        final ProcessorTransaction ptd =
                this.processorTransactionService.getApprovedTransaction(crt.getPointOfSale().getId(), crt.getPurchasedDate(), crt.getTicketNumber());
        if (ptd != null) {
            this.entityDao.evict(ptd);
        }
        
        // If approved transaction exists, remove CardRetryTransaction record and exit
        if (ptd != null && ptd.getProcessorTransactionType().getId() != CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_RECOVERABLE) {
            self().removeCardRetryTransaction(crt.getId());
            return;
        }
        
        // No approved transaction exists
        
        // Check max retry time
        final Date date = getMaxRetryTimeForTransaction(crt);
        
        // If declined transaction was already processed tonite, do not process again.
        // If maxRetryTime is null, then this is S&F
        if (date != null && maxRetryTime != null && date.after(maxRetryTime)) {
            return;
        }
        
        boolean isCrtEvicted = false;
        
        // Safe to process transaction
        final ProcessorTransaction chargePt = createProcessorTransaction(crt);
        
        if (CardProcessingUtil.isNotNullAndIsLink(chargePt.getMerchantAccount())) {
            self().processLinkCPSStoreAndForward(chargePt, crt);
            return;
        }
        
        try {
            increaseProcessorCounter();
            final PointOfSale ps = crt.getPointOfSale();
            final MerchantAccount mad = chargePt.getMerchantAccount();
            final CardProcessor processor = this.cardProcessorFactory.getCardProcessor(mad, ps);
            
            // Cannot find the requested merchant account
            if (processor == null) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append(CANT_OBTAIN_PROC_OBJ).append(ps.getSerialNumber()).append(MERCH_ACC_NULL).append(mad);
                LOG.error(bdr.toString());
                this.mailerService.sendAdminErrorAlert(PROC_NULL, bdr.toString(), null);
                return;
            }
            
            // For store and forward, for card data kept in memory, it is not encrypted
            // This only occurs during during initial processing attempt of S&F
            String decryptedCardData = crt.getCardData();
            
            //On the first Transaction attempt, It'll still have the Track2 In-Memory, thus, using it.
            //From the second onwards.. It'll use the PAN + EXP 
            if (StringUtils.isNotBlank(crt.getTrack2())) {
                decryptedCardData = crt.getTrack2();
            } else {
                // Credit card data encrypted is min 50 bytes and raw is max 37 bytes
                //            String decryptedCardData = crt.getCardData();
                if (decryptedCardData.length() > MIN_DECRYPTED_CARD_DATA_LEN) {
                    decryptedCardData = this.cryptoService.decryptData(crt.getCardData(), ps.getId(), crt.getPurchasedDate());
                }
            }
            
            final Integer customerId = crt.getPointOfSale().getCustomer().getId();
            final Track2Card track2Card = new Track2Card(this.transactionFacade.findCreditCardType(decryptedCardData), customerId, decryptedCardData);
            
            // set ProcessorTransaction fields
            chargePt.setLast4digitsOfCardNumber(track2Card.getLast4DigitsOfAccountNumber());
            chargePt.setMerchantAccount(mad);
            
            // Check for new encryption key, if so re-encrypt data
            try {
                if (this.cryptoService.isNewKeyForData(crt.getCardData())) {
                    final String newCardData =
                            this.cryptoService.encryptData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL, track2Card.getCreditCardPanAndExpiry());
                    crt.setCardData(newCardData);
                }
            } catch (CryptoException ce) {
                LOG.error(CRYPTO_SERVICE_WARN_CRYPTO_EXCEPTION, ce);
            }
            
            // Process transaction
            this.entityDao.evict(crt);
            isCrtEvicted = true;
            
            processTransactionCharge(processor, crt, chargePt, track2Card, ptd);
            return;
        } catch (MissingCryptoKeyException mcke) {
            LOG.warn(FAIL_LOCATE_KEY, mcke);
        } catch (Exception e) {
            
            LOG.error(UNABLE_CHARGE_RETY_TRANS + crt, e);
            
            if (!isCrtEvicted) {
                this.entityDao.evict(crt);
            }
            
            self().handleFailedCharge(chargePt, crt, EXCEPTION_STRING, EMS_UNAVAILABLE_VAL, e);
            
            return;
        } finally {
            decreaseProcessorCounter();
        }
    }
    
    private void processTransactionCharge(final CardProcessor processor, final CardRetryTransaction crt, final ProcessorTransaction chargePt,
        final Track2Card track2Card, final ProcessorTransaction oldPt) throws Exception {
        // Process transaction
        Object[] responses;
        try {
            final Method method = CardProcessor.class.getDeclaredMethod(PROCESS_CHARGE, new Class[] { Track2Card.class, ProcessorTransaction.class });
            method.setAccessible(true);
            responses = (Object[]) this.kpiListenerService.invoke(processor, method, new Object[] { track2Card, chargePt });
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            responses = processor.processCharge(track2Card, chargePt);
        } catch (SecurityException e) {
            e.printStackTrace();
            responses = processor.processCharge(track2Card, chargePt);
        }
        
        Exception latestException = null;
        
        // This is the best effort to capture response from processor. The better way would be to have optional storage, such as writing it to a file.
        int numRetries = 0;
        boolean handleChargeResponseSuccess = false;
        while (!handleChargeResponseSuccess && numRetries <= MAX_RETRIES) {
            try {
                self().handleChargeResponse(processor, crt, chargePt, track2Card, oldPt, responses);
                handleChargeResponseSuccess = true;
                latestException = null;
            } catch (Exception e) {
                latestException = e;
                ++numRetries;
                LOG.error(FAIL_PR_RT + numRetries + F_SLASH_THREE_BANG, e);
                try {
                    Thread.sleep(THREAD_SLEEP_MS);
                } catch (InterruptedException ie) {
                    // DO NOTHING.
                }
            }
        }
        
        if (latestException != null) {
            throw new CardProcessorException("Failed to charge", latestException);
        }
    }
    
    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
    public final void processTransactionChargeManualStoreAndForward(final CardProcessor processor, final CardRetryTransaction crt,
        final ProcessorTransaction chargePt, final Track2Card track2Card, final ProcessorTransaction oldPt) throws Exception {
        // Process transaction        
        final Object[] responses = doProcessCharge(processor, crt, chargePt, track2Card, oldPt);
        
        // This is the best effort to capture response from processor. The better way would be to have optional storage, such as writing it to a file.
        
        //dohandleChargeResponse(processor, crt, charge_pt, track_2_card, old_pt, responses);
        
        Exception latestException = null;
        
        int numRetries = 0;
        boolean handleChargeResponseSuccess = false;
        while (!handleChargeResponseSuccess && numRetries <= MAX_RETRIES) {
            try {
                handleChargeResponse(processor, crt, chargePt, track2Card, oldPt, responses);
                handleChargeResponseSuccess = true;
                latestException = null;
            } catch (Exception e) {
                latestException = e;
                ++numRetries;
                LOG.error(FAIL_PR_RT + numRetries + F_SLASH_THREE_BANG, e);
                try {
                    Thread.sleep(THREAD_SLEEP_MS);
                } catch (InterruptedException ie) {
                    // DO NOTHING.
                }
            }
        }
        
        if (latestException != null) {
            throw new CardProcessorException("Failed to retry charge", latestException);
        }
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public final void dohandleChargeResponse(final CardProcessor processor, final CardRetryTransaction crt, final ProcessorTransaction chargePt,
        final Track2Card track2Card, final ProcessorTransaction oldPt, final Object[] responses) throws Exception {
        
        Exception latestException = null;
        
        // This is the best effort to capture response from processor. The better way would be to have optional storage, such as writing it to a file.
        int numRetries = 0;
        boolean handleChargeResponseSuccess = false;
        while (!handleChargeResponseSuccess && numRetries <= MAX_RETRIES) {
            try {
                self().handleChargeResponse(processor, crt, chargePt, track2Card, oldPt, responses);
                handleChargeResponseSuccess = true;
                latestException = null;
            } catch (Exception e) {
                latestException = e;
                ++numRetries;
                LOG.error(FAIL_PR_RT + numRetries + F_SLASH_THREE_BANG, e);
                try {
                    Thread.sleep(THREAD_SLEEP_MS);
                } catch (InterruptedException ie) {
                    // DO NOTHING.
                }
            }
        }
        
        if (latestException != null) {
            throw new CardProcessorException("Failed to retry charge", latestException);
        }
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Object[] doProcessCharge(final CardProcessor processor, final CardRetryTransaction crt, final ProcessorTransaction chargePt,
        final Track2Card track2Card, final ProcessorTransaction oldPt) throws Exception {
        
        Object[] responses;
        try {
            final Method method = CardProcessor.class.getDeclaredMethod(PROCESS_CHARGE, new Class[] { Track2Card.class, ProcessorTransaction.class });
            method.setAccessible(true);
            responses = (Object[]) this.kpiListenerService.invoke(processor, method, new Object[] { track2Card, chargePt });
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            responses = processor.processCharge(track2Card, chargePt);
        } catch (SecurityException e) {
            e.printStackTrace();
            responses = processor.processCharge(track2Card, chargePt);
        }
        
        return responses;
    }
    
    /**
     * @param old_pt
     *            ProcessorTransaction record selected from processTransactionCharge or processTransactionChargeManualSave has been
     *            "evicted" ("Remove this instance from the session cache.")
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public final void handleChargeResponse(final CardProcessor processor, final CardRetryTransaction inCrt, final ProcessorTransaction chargePt,
        final Track2Card track2Card, final ProcessorTransaction oldPt, final Object[] responses) throws Exception {
        final int responseCode = (Integer) responses[0];
        
        PaymentCard pc = this.paymentCardService.findByPurchaseIdAndCardType(chargePt, WebCoreConstants.CREDIT_CARD, true);
        if (pc != null) {
            pc.setProcessorTransaction(null);
        }
        
        // Insert ProcessorTransaction record
        this.entityDao.save(chargePt);
        
        if (oldPt != null) {
            /*
             * To avoid two open sessions problem, load old ProcessorTransaction and PaymentCard to session,
             * remove processorTransactionId from paymentCard and delete ProcessorTransaction.
             */
            final ProcessorTransaction oldProcessorTransaction = this.entityDao.get(ProcessorTransaction.class, oldPt.getId());
            if (oldProcessorTransaction != null) {
                final PaymentCard oldPC = this.paymentCardService.findByPurchaseIdAndCardType(oldPt, WebCoreConstants.CREDIT_CARD, true);
                if (oldPC != null) {
                    oldPC.setProcessorTransaction(null);
                    this.entityDao.update(oldPC);
                }
                this.entityDao.delete(oldProcessorTransaction);
            }
        }
        
        CardRetryTransaction crt = inCrt;
        // If approved, remove CardRetryTransaction record
        if (DPTResponseCodesMapping.CC_APPROVED_VAL__7000 == responseCode) {
            this.entityDao.delete(crt);
            if (pc != null) {
                this.paymentCardService.updateProcessorTransactionToBatchedSettled(chargePt, WebCoreConstants.CREDIT_CARD);
            } else if (chargePt.getPurchase() != null) {
                pc = createPaymentCard(chargePt, track2Card.getCreditCardPanAndExpiry(), WebCoreConstants.CREDIT_CARD, null);
                /*
                 * Since this method is called from a @Transactional(propagation = Propagation.REQUIRED method, there will have
                 * two Hibernate sessions created. To avoid 'illegally attempted to associate a proxy with two open Sessions'
                 * exception, use 'merge' to combine all objects into one session and save.
                 */
                this.entityDao.merge(pc);
            }
            
            /*
             * This is for single-charge that S&F is completed successfully and need to delete PreAuth record.
             */
            if (responses[1] != null && responses[1] instanceof PreAuth) {
                this.entityDao.delete(responses[1]);
            }
            
        } else {
            // If declined, handle appropriately
            crt.setLastResponseCode(responseCode);
            crt.setLastRetryDate(new Date());
            
            // If lost/stolen, max out retries so never processed again
            if (DPTResponseCodesMapping.CC_LOST_STOLEN_VAL__7006 == responseCode) {
                crt.setNumRetries(CardProcessingConstants.CC_RETRY_LOST_STOLEN);
                crt.setCardData(CardProcessingConstants.EMPTY_STRING);
            } else {
                // otherwise just increment retry counter, moved the code to 'updateCardRetryTransaction' method and 'handleFailedCharge' is using it as well. 
                crt = this.updateCardRetryTransaction(chargePt, crt);
            }
            String msg = "";
            if (responses[1] != null && responses[1] instanceof Integer) {
                msg = ((Integer) responses[1]).toString();
            } else if (responses[1] != null && responses[1] instanceof String) {
                msg = (String) responses[1];
            } else if (responses[1] != null) {
                LOG.warn("Response is not Integer or String: " + responses[1]);
                msg = responses[1].toString();
            }
            saveFailedCCTransaction(chargePt.getPointOfSale(), chargePt.getMerchantAccount(), chargePt.getProcessingDate(),
                                    chargePt.getPurchasedDate(), chargePt.getTicketNumber(), CardProcessingConstants.CC_TYPE_SALE, msg);
            
            this.entityDao.saveOrUpdate(crt);
        }
        
        if (processor.getIsManualSave()) {
            processor.updateProcessorSpecificDetails(chargePt);
        }
        
        return;
    }
    
    private CardRetryTransaction updateCardRetryTransaction(final ProcessorTransaction chargePt, final CardRetryTransaction crt) {
        final CustomerProperty prop =
                (CustomerProperty) this.entityDao.getNamedQuery("CustomerProperty.findCustomerPropertyByPointOfSaleIdAndPropertyType")
                        .setParameter("pointOfSaleId", chargePt.getPointOfSale().getId())
                        .setParameter("customerPropertyTypeId", WebCoreConstants.CUSTOMER_PROPERTY_TYPE_MAX_OFFLINE_RETRY).uniqueResult();
        
        final int maxAllowRetry = Integer.parseInt(prop.getPropertyValue());
        if (LOG.isDebugEnabled()) {
            LOG.debug("!!! maxAllowRetry: " + maxAllowRetry);
        }
        if ((crt.getNumRetries() + 1) >= maxAllowRetry) {
            crt.setNumRetries(CardProcessingConstants.CC_RETRY_MAX_EXCEEDED);
            crt.setCardData(CardProcessingConstants.EMPTY_STRING);
            
            final StringBuilder bdr = new StringBuilder(33);
            bdr.append("!!! pointOfSaleId:").append(crt.getPointOfSale().getId()).append(", PurchasedDate:").append(crt.getPurchasedDate())
                    .append(", TicketNumber:").append(crt.getTicketNumber()).append(" has exceeded maximum retries !!!");
            LOG.error(bdr.toString());
        } else {
            crt.setNumRetries(crt.getNumRetries() + 1);
        }
        return crt;
    }
    
    @Override
    public final int processTransactionCharge(final String creditcardData, final PointOfSale pointOfSale, final Permit permit, final boolean isRFID,
        final ProcessorTransaction chargePt, final PaymentCard paymentCard, final long origProcTransId) {
        if (LOG.isDebugEnabled()) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("Charge Transaction with creditcardData and Permit, is creditcardData null or empty: ")
                    .append(StringUtils.isBlank(creditcardData)).append(", permit id: ").append(permit.getId());
            LOG.debug(bdr.toString());
        }
        
        if (this.needStopProcessing) {
            LOG.info("+++ Digital Iris is shutting down. Unable to charge credit card +++");
            // AURA_CHARGE_SEVER_SHUTDOWN
            return CardProcessingConstants.AURA_CHARGE_SEVER_SHUTDOWN;
        }
        // AURA_CHARGE_UNKNOWN;        
        int ret = CardProcessingConstants.AURA_CHARGE_UNKNOWN;
        try {
            final MerchantAccount merchantAccount = findMerchantAccount(pointOfSale.getId(), WebCoreConstants.CREDIT_CARD);
            
            final CPSData cpsData = this.cpsDataService.findCPSDataByProcessorTransactionIdAndRefundTokenIsNull(origProcTransId);
            boolean isCPSProcessor = false;
            if (cpsData != null) {
                isCPSProcessor = true;
            }
            
            CardProcessor processor = null;
            if (!isCPSProcessor) {
                increaseProcessorCounter();
                processor = this.cardProcessorFactory.getCardProcessor(merchantAccount, pointOfSale);
            }
            
            // Cannot find the requested merchant account
            if (processor == null && !isCPSProcessor) {
                LOG.warn("++++ can not get processor for merchant accout, pointOfSale id: " + pointOfSale.getId());
                return ret;
            }
            
            // this creditcardData only has PAN + ExpiryDate information. 
            final Purchase purchase = permit.getPurchase();
            final String decryptedCardData = this.cryptoService.decryptData(creditcardData);
            final Track2Card track2Card =
                    new Track2Card(this.transactionFacade.findCreditCardType(decryptedCardData), purchase.getCustomer().getId(), decryptedCardData);
            
            // Moved setProcessorTransactionType(...PAY_BY_PHONE_CHARGE....) to SmsHttpPostCallbackServlet, processChargeAndUpdatePermit.
            chargePt.setPointOfSale(pointOfSale);
            chargePt.setPurchasedDate(purchase.getPurchaseGmt());
            chargePt.setTicketNumber(purchase.getPurchaseNumber());
            chargePt.setIsApproved(false);
            chargePt.setAmount(purchase.getCardPaidAmount());
            chargePt.setMerchantAccount(merchantAccount);
            chargePt.setIsRfid(isRFID);
            
            if (!isCPSProcessor) {
                chargePt.setCardType(track2Card.getDisplayName());
                chargePt.setCardHash(this.cryptoAlgorithmFactory.getSha1Hash(track2Card.getCreditCardPanAndExpiry(),
                                                                             CryptoConstants.HASH_CREDIT_CARD_PROCESSING));
                chargePt.setLast4digitsOfCardNumber(track2Card.getLast4DigitsOfAccountNumber());
                chargePt.setReferenceNumber(this.commonProcessingService.getNewReferenceNumber(chargePt.getMerchantAccount()));
                chargePt.setProcessingDate(new Date());
                ret = processTransactionCharge(processor, chargePt, track2Card);
            } else {
                ret = processCPSTransactionCharge(chargePt, origProcTransId, cpsData.getChargeToken());
            }
            if (ret == 0) {
                if (!isCPSProcessor) {
                    createPaymentCard(chargePt, track2Card.getCreditCardPanAndExpiry(), WebCoreConstants.CREDIT_CARD, paymentCard);
                } else {
                    createPaymentCard(chargePt, origProcTransId, WebCoreConstants.CREDIT_CARD, paymentCard);
                }
            }
            
            return ret;
        } catch (Exception e) {
            final StringBuilder bdr = new StringBuilder(33);
            bdr.append("Is creditcardData null or empty: ").append(StringUtils.isBlank(creditcardData));
            handleFailedCharge(chargePt, EXCEPTION_STRING, bdr.toString());
            LOG.error("Unable to charge SMS, pointOfSale id: " + pointOfSale.getId(), e);
            
            return ret;
        } finally {
            decreaseProcessorCounter();
        }
    }
    
    private int processCPSTransactionCharge(final ProcessorTransaction chargePt, final long origProcTransId, final String chargeToken) {
        
        int ret = CardProcessingConstants.AURA_CHARGE_UNKNOWN;
        
        final ProcessorTransaction oriPt = this.processorTransactionService.findProcessorTransactionById(origProcTransId, true);
        if (oriPt != null) {
            chargePt.setCardType(oriPt.getCardType());
            chargePt.setCardHash(oriPt.getCardHash());
            chargePt.setLast4digitsOfCardNumber(oriPt.getLast4digitsOfCardNumber());
        } else {
            chargePt.setCardType(CardProcessingConstants.NAME_CREDIT_CARD);
            chargePt.setCardHash(StandardConstants.STRING_EMPTY_STRING);
            chargePt.setLast4digitsOfCardNumber((short) 0);
        }
        chargePt.setReferenceNumber(StandardConstants.STRING_EMPTY_STRING);
        
        final CPSTransactionDto cpsTransactionDto = new CPSTransactionDto(null, chargePt, chargePt.getMerchantAccount());
        cpsTransactionDto.setChargeToken(chargeToken);
        
        CardTransactionResponseGenerator cardTxResp = null;
        
        try {
            try {
                final Method method = CPSProcessor.class.getDeclaredMethod(PROCESS_ADD_ON, new Class[] { CPSTransactionDto.class, });
                method.setAccessible(true);
                cardTxResp = (CardTransactionResponseGenerator) this.kpiListenerService.invoke(this.cpsProcessor, method,
                                                                                               new Object[] { cpsTransactionDto });
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                cardTxResp = this.cpsProcessor.processAddOn(cpsTransactionDto);
            } catch (SecurityException e) {
                e.printStackTrace();
                cardTxResp = this.cpsProcessor.processAddOn(cpsTransactionDto);
            }
            
            if (cardTxResp == null) {
                saveFailedCCTransaction(chargePt.getPointOfSale(), chargePt.getMerchantAccount(), chargePt.getProcessingDate(),
                                        chargePt.getPurchasedDate(), chargePt.getTicketNumber(), CardProcessingConstants.CC_TYPE_SALE,
                                        CardProcessingConstants.CPS_RETURNED_NULL);
                return 1;
            }
            
            if (cardTxResp.getStatus().getCode().equalsIgnoreCase(StandardConstants.STRING_STATUS_CODE_ACCEPTED)) {
                ret = 0;
                chargePt.setAuthorizationNumber(cardTxResp.getAuthorizationNumber());
                chargePt.setIsApproved(true);
                chargePt.setReferenceNumber(cardTxResp.getReferenceNumber());
                chargePt.setProcessingDate(DateUtil.parse(cardTxResp.getProcessedDate(), DateUtil.UTC_CPS_DATE_TIME_FORMAT, WebCoreConstants.GMT,
                                                          true));
            } else {
                ret = 1;
                if (cardTxResp.getStatus() != null && StringUtils.isNotBlank(cardTxResp.getStatus().getMessage())) {
                    chargePt.setCPSResponseMessage(cardTxResp.getStatus().getMessage());
                }
                chargePt.setProcessingDate(DateUtil.getCurrentGmtDate());
                saveFailedCCTransaction(chargePt.getPointOfSale(), chargePt.getMerchantAccount(), chargePt.getProcessingDate(),
                                        chargePt.getPurchasedDate(), chargePt.getTicketNumber(), CardProcessingConstants.CC_TYPE_SALE,
                                        cardTxResp.getStatus().getMessage());
            }
            
        } catch (Exception e) {
            
            handleFailedCharge(chargePt, EXCEPTION_STRING, e.getMessage());
            LOG.error("Unable to charge SMS: " + chargePt, e);
        } finally {
            chargePt.setCreatedGmt(DateUtil.getCurrentGmtDate());
            
            // Insert ProcessorTransaction record
            this.entityDao.save(chargePt);
            if (chargePt.isIsApproved()) {
                this.entityDao.save(new CPSData(chargePt, cardTxResp.getChargeToken()));
            }
        }
        return ret;
    }
    
    private int processTransactionCharge(final CardProcessor processor, final ProcessorTransaction chargePt, final Track2Card track2Card) {
        
        int ret = CardProcessingConstants.AURA_CHARGE_UNKNOWN;
        try {
            Object[] responses;
            try {
                final Method method =
                        CardProcessor.class.getDeclaredMethod(PROCESS_CHARGE, new Class[] { Track2Card.class, ProcessorTransaction.class });
                method.setAccessible(true);
                responses = (Object[]) this.kpiListenerService.invoke(processor, method, new Object[] { track2Card, chargePt });
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                responses = processor.processCharge(track2Card, chargePt);
            } catch (SecurityException e) {
                e.printStackTrace();
                responses = processor.processCharge(track2Card, chargePt);
            }
            
            final int responseCode = (Integer) responses[0];
            
            // If approved, remove CardRetryTransaction record
            if (DPTResponseCodesMapping.CC_APPROVED_VAL__7000 == responseCode) {
                // AURA_CHARGE_SUCCEED
                chargePt.setIsApproved(true);
                ret = 0;
            } else {
                // AURA_CHARGE_DECLINE
                ret = 1;
                saveFailedCCTransaction(chargePt.getPointOfSale(), chargePt.getMerchantAccount(), chargePt.getProcessingDate(),
                                        chargePt.getPurchasedDate(), chargePt.getTicketNumber(), CardProcessingConstants.CC_TYPE_SALE,
                                        responses[1].toString());
            }
            
        } catch (Exception e) {
            
            handleFailedCharge(chargePt, EXCEPTION_STRING, e.getMessage());
            LOG.error("Unable to charge SMS: " + chargePt, e);
        } finally {
            chargePt.setCreatedGmt(DateUtil.getCurrentGmtDate());
            
            // Insert ProcessorTransaction record
            this.entityDao.save(chargePt);
            
            // update ElavonTransactionDetails and ActiveBatchSummary
            if (processor.getIsManualSave()) {
                processor.updateProcessorSpecificDetails(chargePt);
            }
        }
        return ret;
    }
    
    private void insertFailedPostAuthIntoProcessorTransaction(final PreAuth pd, final ProcessorTransaction ptd, final String reason) {
        final Date procDate = new Date();
        final ProcessorTransaction procTrans = new ProcessorTransaction();
        procTrans.setPointOfSale(ptd.getPointOfSale());
        procTrans.setPurchasedDate(ptd.getPurchasedDate());
        procTrans.setTicketNumber(ptd.getTicketNumber());
        procTrans.setIsApproved(false);
        procTrans.setProcessorTransactionId(ptd.getProcessorTransactionId());
        procTrans.setProcessorTransactionType(this.processorTransactionService
                .getProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS));
        procTrans.setProcessingDate(procDate);
        procTrans.setMerchantAccount(pd.getMerchantAccount());
        procTrans.setAmount(pd.getAmount());
        procTrans.setCardType(pd.getCardType());
        procTrans.setLast4digitsOfCardNumber(pd.getLast4digitsOfCardNumber());
        procTrans.setCardHash(ptd.getCardHash());
        procTrans.setReferenceNumber(ptd.getReferenceNumber());
        procTrans.setAuthorizationNumber(reason);
        procTrans.setCreatedGmt(procDate);
        try {
            this.entityDao.save(procTrans);
        } catch (Exception e) {
            final String msg = "Unable to insert failed post-auth attempt into ProcessorTransaction, " + ptd;
            this.mailerService.sendAdminErrorAlert("Unable to insert ProcessorTransaction", msg, e);
            LOG.error(msg, e);
        }
    }
    
    private ProcessorTransaction createProcessorTransaction(final CardRetryTransaction crt) {
        final ProcessorTransaction procTx = new ProcessorTransaction();
        procTx.setProcessorTransactionId(WebCoreConstants.BLANK);
        procTx.setReferenceNumber(WebCoreConstants.BLANK);
        procTx.setPointOfSale(crt.getPointOfSale());
        procTx.setPurchasedDate(crt.getPurchasedDate());
        procTx.setTicketNumber(crt.getTicketNumber());
        procTx.setIsApproved(false);
        procTx.setProcessingDate(new Date());
        if (StringUtils.isNotEmpty(crt.getCardType())) {
            procTx.setCardType(crt.getCardType());
        } else {
            procTx.setCardType(CardProcessingConstants.NAME_CREDIT_CARD);
        }
        procTx.setAmount(crt.getAmount());
        procTx.setCardHash(crt.getCardHash());
        procTx.setIsRfid(crt.isIsRfid());
        procTx.setMerchantAccount(findValidMerchantAccount(crt.getPointOfSale().getId(), WebCoreConstants.CREDIT_CARD));
        // Find Purchase record from CardRetryTransaction.
        crt.setPointOfSale(crt.getPointOfSale());
        procTx.setPurchase(this.purchaseService.findUniquePurchaseForSms(crt.getPointOfSale().getCustomer().getId(), crt.getPointOfSale().getId(),
                                                                         crt.getPurchasedDate(), crt.getTicketNumber()));
        procTx.setCreatedGmt(DateUtil.getCurrentGmtDate());
        
        /*
         * In retry process CardRetryTransaction record is inserted into database with numRetries of 1 (attempted once already).
         * ProcessorTransactionType shouldn't be changed and should set to Batched only when CardRetryTransaction is originally
         * created from offline batch process
         */
        if (crt.getNumRetries() > 0 && crt.getCardRetryTransactionType().getId() == WebCoreConstants.CARD_RETRY_TRANSACTION_TYPE_BATCHED) {
            procTx.setProcessorTransactionType(this.processorTransactionService
                    .getProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_EMS_RETRY_WITH_NO_REFUNDS));
        } else if (procTx.getPurchase() == null || procTx.getPurchase().isIsOffline()
                   || crt.getCardRetryTransactionType().getId() == WebCoreConstants.CARD_RETRY_TRANSACTION_TYPE_BATCHED) {
            procTx.setProcessorTransactionType(this.processorTransactionService
                    .getProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_NO_REFUNDS));
        } else {
            procTx.setProcessorTransactionType(this.processorTransactionService
                    .getProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_NO_REFUNDS));
        }
        return procTx;
        
        /*
         * The old code compares 'CardRetryTransaction getTypeId' with constants TYPE_OFFLINE or TYPE_CHARGE_SF_WITH_NO_REFUND
         * in ProcessorTransactionType. Now get ProcessorTransactionType from 'ProcessorTransactionServiceImpl, procTxTypesMap'.
         * Old code:
         * if (CardRetryTransactionType.TYPE_OFFLINE == crt.getTypeId())
         * {
         * setTypeId(ProcessorTransactionType.TYPE_CHARGE_EMS_RETRY_WITH_NO_REFUNDS);
         * }
         * else
         * {
         * setTypeId(ProcessorTransactionType.TYPE_CHARGE_SF_WITH_NO_REFUNDS);
         * }
         */
    }
    
    private void handleFailedCharge(final ProcessorTransaction chargePt, final String subject, final String reason) {
        chargePt.setProcessorTransactionType(this.processorTransactionService
                .getProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_CHARGE_WITH_NO_REFUNDS));
        
        if (chargePt.getProcessingDate() == null) {
            chargePt.setProcessingDate(new Date());
        }
        chargePt.setAuthorizationNumber(subject);
        chargePt.setIsApproved(false);
        chargePt.setCardHash(WebCoreConstants.BLANK);
        chargePt.setCardType(CardProcessingConstants.NAME_CREDIT_CARD);
        chargePt.setLast4digitsOfCardNumber((short) 0);
        chargePt.setReferenceNumber(WebCoreConstants.BLANK);
        chargePt.setCreatedGmt(DateUtil.getCurrentGmtDate());
        
        saveFailedCCTransaction(chargePt.getPointOfSale(), chargePt.getMerchantAccount(), chargePt.getProcessingDate(), chargePt.getPurchasedDate(),
                                chargePt.getTicketNumber(), CardProcessingConstants.CC_TYPE_SALE, reason);
        try {
            this.entityDao.save(chargePt);
        } catch (Exception e2) {
            LOG.error("Unable to insert failed charge into ProcessorTransaction: \n" + chargePt, e2);
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public final void handleFailedCharge(final ProcessorTransaction chargePt, final CardRetryTransaction inCrt, final String reason,
        final int responseCode, final Exception e) {
        insertFailedChargeIntoProcessorTransaction(chargePt, reason);
        final CardRetryTransaction crt = updateCardRetryTransaction(chargePt, inCrt);
        
        /*
         * If CardRetryTransaction record doesn't contain cardData anymore, credit card processing cannot continue and set numRetries to the max
         * value, CardProcessingConstants.CC_RETRY_LOST_STOLEN (32767).
         */
        if (StringUtils.isBlank(crt.getCardData())) {
            crt.setNumRetries(CardProcessingConstants.CC_RETRY_LOST_STOLEN);
        }
        crt.setLastRetryDate(new Date());
        crt.setLastResponseCode(responseCode);
        /*
         * Since this method is called from a @Transactional(propagation = Propagation.REQUIRED method, there will have
         * two Hibernate sessions created. To avoid 'illegally attempted to associate a proxy with two open Sessions'
         * exception, use 'merge' to combine all objects into one session and save.
         */
        this.entityDao.merge(crt);
        final String failureReason;
        if (e == null) {
            failureReason = reason;
        } else {
            failureReason = e.getMessage() != null ? e.getMessage() : reason;
        }
        saveFailedCCTransaction(chargePt.getPointOfSale(), chargePt.getMerchantAccount(), chargePt.getProcessingDate(), chargePt.getPurchasedDate(),
                                chargePt.getTicketNumber(), CardProcessingConstants.CC_TYPE_SALE, failureReason);
    }
    
    private void insertFailedChargeIntoProcessorTransaction(final ProcessorTransaction ptd, final String reason) {
        // PS does not exist
        if (ptd.getPointOfSale().getId() == 0) {
            LOG.error(INSERT_F_CRG_LE + ptd + AS_PS_DNE);
        } else if (ptd.getTicketNumber() == 0 || ptd.getPurchasedDate() == null) {
            LOG.error(INSERT_F_CRG_LE + ptd + BOSS_VER_PRIOR);
        } else {
            // PS exists
            // And BOSS version >= 5.0.4 (ticket number and purchased date are available)
            ptd.setProcessorTransactionType(this.processorTransactionService
                    .getProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_NO_REFUNDS));
            if (ptd.getProcessingDate() == null) {
                ptd.setProcessingDate(new Date());
            }
            ptd.setAuthorizationNumber(reason);
            ptd.setIsApproved(false);
            try {
                // transactionDao_.createProcessorTransaction(ptd);
                this.entityDao.save(ptd);
            } catch (Exception e) {
                final String msg = "Unable to insert failed charge attempt into ProcessorTransaction: \n\n " + ptd;
                this.mailerService.sendAdminErrorAlert(MailerService.EXCEPTION + " - Unable to update transaction", msg, e);
                LOG.error(msg, e);
            }
        }
    }
    
    private int cleanupIfExpired(final ProcessorTransaction ptd) {
        int numUpdatedRecords = 0;
        final long currentTime = System.currentTimeMillis();
        final long time = currentTime - ptd.getPurchasedDate().getTime();
        
        int maxHoldingDays = EmsPropertiesService.DEFAULT_CARD_AUTHORIZATION_BACK_UP_MAX_HOLDING_DAYS;
        try {
            maxHoldingDays =
                    Integer.parseInt(this.emsPropertiesService.getPropertyValue(EmsPropertiesService.CARD_AUTHORIZATION_BACK_UP_MAX_HOLDING_DAYS));
        } catch (Exception e) {
            LOG.error("CardProcessingManagerImpl, cleanupIfExpired, "
                      + "cannot get EmsPropertiesService.CARD_AUTHORIZATION_BACK_UP_MAX_HOLDING_DAYS, use default instead.", e);
        }
        final Date maxHoldingDay = DateUtil.addDaysToCurrentDatetimeInGMT(-maxHoldingDays);
        final long uncloseableTime = currentTime - maxHoldingDay.getTime();
        
        /*
         * 1. Verify that it's for single-phase processor.
         * 2. Parse Authorization number data to obtain Auth Number and PreAuth Id
         * 3. Find correct PreAuthHolding record using Auth Number and PreAuth Id
         * 4. Update ProcessorTransaction record using PreAuthHolding data
         * 5. Delete PreAuthHolding record.
         */
        try {
            final MerchantAccount ma = this.merchantAccountService.findWithProcessorById(ptd.getMerchantAccount().getId());
            if (CardProcessingUtil.isSinglePhaseProcessor(ma.getProcessor().getId())
                && CardProcessingUtil.hasAuthorizationNumberAndPreAuthId(ptd.getAuthorizationNumber())) {
                
                final String[] arr = ptd.getAuthorizationNumber().split(StandardConstants.STRING_COLON);
                final String authNum = arr[0];
                final Long preAuthId = Long.parseLong(arr[1]);
                final PreAuthHolding preAuthHolding = this.preAuthService.findByApprovedPreAuthIdAuthorizationNumber(preAuthId, authNum);
                
                updateProcessorTransactionToSettled(ptd, preAuthHolding);
                deletePreAuthHolding(preAuthHolding);
                return -1;
            }
        } catch (CommunicationException ce) {
            LOG.error(ce);
        }
        if (ptd.getId() != null && time > uncloseableTime) {
            // num_updated_records = transactionDao_.updateProcessorTransactionType(
            // ProcessorTransactionType.TYPE_UNCLOSEABLE, ptd);
            numUpdatedRecords =
                    this.processorTransactionService.updateProcessorTransactionType(ptd.getId(), ptd.getProcessorTransactionType().getId(),
                                                                                    CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_UNCLOSEABLE);
            
        } else if (ptd.getId() != null && time > CardProcessingConstants.MS_TO_ATTEMPT_SETTLEMENT) {
            numUpdatedRecords = this.processorTransactionService.markRecoverable(ptd);
        }
        return numUpdatedRecords;
    }
    
    /**
     * Update ProcessorTransaction record has Auth PlaceHolder type with the following data from PreAuthHolding object:
     * - last4digitsOfCardNumber
     * - cardType
     * - processingDate (using PreAuthHolding preAuthDate)
     * - processorTransactionId
     * - processorTransactionType (to CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS - 2)
     * - referenceNumber
     * - cardHash
     * 
     * @param pt
     *            ProcessorTransaction object with type of Auth PlaceHolder
     * @param preAuthHolding
     *            PreAuthHolding object contains data copied from original PreAuth.
     */
    private void updateProcessorTransactionToSettled(final ProcessorTransaction pt, final PreAuthHolding preAuthHolding) {
        pt.setLast4digitsOfCardNumber(preAuthHolding.getLast4digitsOfCardNumber());
        pt.setCardType(preAuthHolding.getCardType());
        pt.setProcessingDate(preAuthHolding.getPreAuthDate());
        pt.setProcessorTransactionId(preAuthHolding.getProcessorTransactionId());
        pt.setProcessorTransactionType(this.processorTransactionService
                .getProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS));
        pt.setReferenceNumber(preAuthHolding.getReferenceNumber());
        pt.setCardHash(preAuthHolding.getCardHash());
        this.entityDao.update(pt);
    }
    
    private void deletePreAuthHolding(final PreAuthHolding preAuthHolding) {
        this.entityDao.delete(preAuthHolding);
    }
    
    private void handleFailedPostAuth(final PreAuth pd, final ProcessorTransaction ptd, final String reason, final String message,
        final String mailReason, final Exception e) {
        this.mailerService.sendAdminErrorAlert(mailReason, message, e);
        insertFailedPostAuthIntoProcessorTransaction(pd, ptd, reason);
        saveFailedCCTransaction(ptd.getPointOfSale(), ptd.getMerchantAccount(), ptd.getProcessingDate(), ptd.getPurchasedDate(),
                                ptd.getTicketNumber(), CardProcessingConstants.CC_TYPE_SETTLEMENT, e.getMessage());
    }
    
    private void increaseProcessorCounter() {
        synchronized (this) {
            this.processorCounter++;
        }
    }
    
    private synchronized void decreaseProcessorCounter() {
        synchronized (this) {
            this.processorCounter--;
        }
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final void setCryptoService(final CryptoService cryptoService) {
        this.cryptoService = cryptoService;
    }
    
    public final void setCustomerCardTypeService(final CustomerCardTypeService customerCardTypeService) {
        this.customerCardTypeService = customerCardTypeService;
    }
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final void setMerchantAccountService(final MerchantAccountService merchantAccountService) {
        this.merchantAccountService = merchantAccountService;
    }
    
    public final void setProcessorTransactionService(final ProcessorTransactionService processorTransactionService) {
        this.processorTransactionService = processorTransactionService;
    }
    
    public final void setWebWidgetHelperService(final WebWidgetHelperService webWidgetHelperService) {
        this.webWidgetHelperService = webWidgetHelperService;
    }
    
    public final void setCardProcessorFactory(final CardProcessorFactory cardProcessorFactory) {
        this.cardProcessorFactory = cardProcessorFactory;
    }
    
    public final void setMailerService(final MailerService mailerService) {
        this.mailerService = mailerService;
    }
    
    public final int getProcessorCounter() {
        return this.processorCounter;
    }
    
    public final void setProcessorCounter(final int processorCounter) {
        this.processorCounter = processorCounter;
    }
    
    public final void setCustomerBadCardService(final CustomerBadCardService customerBadCardService) {
        this.customerBadCardService = customerBadCardService;
    }
    
    public final void setCustomerCardService(final CustomerCardService customerCardService) {
        this.customerCardService = customerCardService;
    }
    
    public final void setLocationService(final LocationService locationService) {
        this.locationService = locationService;
    }
    
    public final void setCcFailLogService(final CcFailLogService ccFailLogService) {
        this.ccFailLogService = ccFailLogService;
    }
    
    public final void setCardRetryTransactionService(final CardRetryTransactionService cardRetryTransactionService) {
        this.cardRetryTransactionService = cardRetryTransactionService;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final void setPreAuthService(final PreAuthService preAuthService) {
        this.preAuthService = preAuthService;
    }
    
    public final void setReversalArchiveService(final ReversalArchiveService reversalArchiveService) {
        this.reversalArchiveService = reversalArchiveService;
    }
    
    public final void setPaymentCardService(final PaymentCardService paymentCardService) {
        this.paymentCardService = paymentCardService;
    }
    
    public final void setCardTypeService(final CardTypeService cardTypeService) {
        this.cardTypeService = cardTypeService;
    }
    
    public final void setPurchaseService(final PurchaseService purchaseService) {
        this.purchaseService = purchaseService;
    }
    
    public final void setPermitService(final PermitService permitService) {
        this.permitService = permitService;
    }
    
    public final void setCustomerAdminService(final CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    public final void setTransactionFacade(final TransactionFacade transactionFacade) {
        this.transactionFacade = transactionFacade;
    }
    
    public final void setNeedStopProcessing(final boolean needStopProcessing) {
        this.needStopProcessing = needStopProcessing;
    }
    
    public final void setCryptoAlgorithmFactory(final CryptoAlgorithmFactory cryptoAlgorithmFactory) {
        this.cryptoAlgorithmFactory = cryptoAlgorithmFactory;
    }
    
    @Override
    public final int findTotalPreAuthsByCardData(final String cardData) {
        return this.preAuthService.findTotalPreAuthsByCardData(cardData);
    }
    
    @Override
    public final int findTotalReversalsByCardData(final String cardNumber) {
        return this.reversalService.findTotalReversalsByCardData(cardNumber);
    }
    
    @Override
    public final int findTotalPreAuthHoldingsByCardData(final String cardData) {
        return this.entityDao.findTotalResultByNamedQuery("PreAuthHolding.findTotalPreAuthHoldingsByCardData", new String[] { CARD_DATA },
                                                          new Object[] { cardData });
    }
    
    @Override
    public final Collection<PreAuth> findPreAuthsByCardData(final String cardData, final boolean isEvict) {
        return this.preAuthService.findPreAuthsByCardData(cardData, isEvict);
    }
    
    @Override
    public final Collection<Reversal> findReversalsByCardData(final String cardNumber, final boolean isEvict) {
        return this.reversalService.findReversalsByCardData(cardNumber, isEvict);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final Collection<PreAuthHolding> findPreAuthHoldingsByCardData(final String cardData, final boolean isEvict) {
        final List<PreAuthHolding> lists =
                this.entityDao.findByNamedQueryAndNamedParam("PreAuthHolding.findPreAuthHoldingByCardData", CARD_DATA, cardData);
        
        if (isEvict) {
            for (PreAuthHolding holding : lists) {
                this.entityDao.evict(holding);
            }
        }
        
        return lists;
    }
    
    public final void updateReversal(final Reversal reversal) {
        this.reversalService.updateReversal(reversal);
    }
    
    public final void updatePreAuth(final PreAuth preAuth) {
        this.preAuthService.updatePreAuth(preAuth);
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    public final void updatePreAuthHolding(final PreAuthHolding preAuthHolding) {
        this.entityDao.update(preAuthHolding);
    }
    
    /*
     * SEE BELOW, same method signatures exist for migration.
     * @Override
     * @Transactional(propagation = Propagation.SUPPORTS)
     * public Collection<Reversal> getIncompleteReversalsForReversal(Date processedBefore) {
     * return this.reversalService.getIncompleteReversalsForReversal(processedBefore);
     * }
     * @Override
     * @SuppressWarnings("unchecked")
     * @Transactional(propagation = Propagation.SUPPORTS)
     * public Collection<ProcessorTransaction> getAuthorizedProcessorTransactionsForSettlement(Date maxReceiveTime) {
     * return this.entityDao.findByNamedQueryAndNamedParam("ProcessorTransaction.getAuthorizedProcessorTransactionsForSettlement", "maxReceiveTime",
     * maxReceiveTime);
     * }
     * @Override
     * @SuppressWarnings("unchecked")
     * public void updateProcessorTransactionToUncloseable(Date maxHoldingDay) {
     * List<ProcessorTransaction> lists = this.entityDao.findByNamedQueryAndNamedParam("ProcessorTransaction.findUncloseableProcessorTransaction",
     * "maxHoldingDay", maxHoldingDay);
     * ProcessorTransactionType uncloseable = (ProcessorTransactionType) this.entityDao.get(ProcessorTransactionType.class, 8);
     * /* update ProcessorTransactionType Id = 8 (Uncloseable) *
     * for (ProcessorTransaction pt : lists) {
     * this.entityDao.evict(pt);
     * pt.setProcessorTransactionType(uncloseable);
     * this.entityDao.update(pt);
     * }
     * }
     * @Override
     * @SuppressWarnings("unchecked")
     * public void cleanupCardAuthorizationBackup(Date maxHoldingDay) {
     * List<PreAuthHolding> lists = this.entityDao.findByNamedQueryAndNamedParam("PreAuthHolding.findCardAuthorizationBackupForCleanUp", "maxHoldingDay",
     * maxHoldingDay);
     * for (PreAuthHolding preAuthHolding : lists) {
     * this.entityDao.delete(preAuthHolding);
     * }
     * }
     */
    private String createMerchantAccountErrorMessage(final String cardNumber, final String serialNumber, final String error) {
        final StringBuilder bdr = new StringBuilder(95);
        bdr.append("Unable to authorize card: ************'").append(cardNumber).append(FOR_POS).append(serialNumber)
                .append("' as no Value Card merchant account is assigned. Error: ").append(error);
        return bdr.toString();
    }
    
    @Override
    public final void setBeanFactory(final BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }
    
    private void sendAdminErrorAlert(final Exception e, final String msg) {
        LOG.warn(msg, e);
        this.mailerService.sendAdminErrorAlert("CardProcessingManager error occurred, ", msg, e);
    }
    
    private PaymentCard createPaymentCard(final ProcessorTransaction processorTransaction, final Long origProcTransId, final Integer cardTypeId,
        final PaymentCard payCard) {
        
        updatePaymentCard(payCard, processorTransaction, cardTypeId);
        final ProcessorTransaction oriPt = this.processorTransactionService.findProcessorTransactionById(origProcTransId, true);
        final PaymentCard oriPaymentCard = this.paymentCardService.findByPurchaseId(oriPt.getPurchase().getId());
        if (oriPaymentCard != null) {
            payCard.setCreditCardType(oriPaymentCard.getCreditCardType());
            payCard.setCardLast4digits(oriPaymentCard.getCardLast4digits());
        }
        return payCard;
    }
    
    private PaymentCard createPaymentCard(final ProcessorTransaction processorTransaction, final String creditCardPanAndExpiry,
        final Integer cardTypeId, final PaymentCard inPayCard) {
        PaymentCard payCard = inPayCard;
        if (payCard == null) {
            payCard = new PaymentCard();
        }
        payCard = updatePaymentCard(payCard, processorTransaction, cardTypeId);
        payCard.setCreditCardType(this.transactionFacade.findCreditCardType(creditCardPanAndExpiry));
        return payCard;
    }
    
    private PaymentCard updatePaymentCard(final PaymentCard payCard, final ProcessorTransaction processorTransaction, final Integer cardTypeId) {
        payCard.setPurchase(processorTransaction.getPurchase());
        payCard.setProcessorTransaction(processorTransaction);
        payCard.setProcessorTransactionType(processorTransaction.getProcessorTransactionType());
        payCard.setMerchantAccount(processorTransaction.getMerchantAccount());
        payCard.setAmount(processorTransaction.getAmount());
        payCard.setCardLast4digits(processorTransaction.getLast4digitsOfCardNumber());
        payCard.setCardProcessedGmt(DateUtil.getCurrentGmtDate());
        payCard.setIsApproved(true);
        payCard.setIsUploadedFromBoss(processorTransaction.isIsUploadedFromBoss());
        payCard.setCustomerCard(null);
        payCard.setCardType(this.cardTypeService.getCardTypesMap().get(cardTypeId));
        return payCard;
    }
    
    public final boolean checkMigrationStatusOfCustomer(final int pointOfSaleId) {
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
        if (pointOfSale != null) {
            final Customer customer = this.customerAdminService.findCustomerByCustomerId(pointOfSale.getId());
            return customer.isIsMigrated();
        } else {
            return false;
        }
    }
    
    /**
     * TODO Replace with original logic AFTER migration is completed.
     */
    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.REQUIRED)
    public final Collection<ProcessorTransaction> getAuthorizedProcessorTransactionsForSettlement(final Date maxReceiveTime) {
        final Criteria crit = this.cpsProcessDataService.buildAuthorizedTransactionsCriteria(maxReceiveTime);
        crit.add(Restrictions.eq(ReversalService.CUST_IS_MIG, Boolean.TRUE));
        return crit.list();
    }
    
    /**
     * TODO Replace with original logic AFTER migration is completed.
     * This is for TrackDataLoadingTask.java
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<CardRetryTransaction> findCardRetryTransactionsForMigratedCustomers(final List<CardRetryTransaction> cardRetryTransactions) {
        final List<CardRetryTransaction> list = new ArrayList<CardRetryTransaction>();
        for (CardRetryTransaction crt : cardRetryTransactions) {
            if (crt.getPointOfSale().getCustomer().isIsMigrated()) {
                list.add(crt);
            }
        }
        return list;
    }
    
    /**
     * TODO Replace with original logic AFTER migration is completed .
     */
    @Override
    @SuppressWarnings("unchecked")
    public final void updateProcessorTransactionToUncloseable(final Date maxHoldingDay) {
        final List<ProcessorTransaction> lists = this.entityDao
                .findByNamedQueryAndNamedParam("ProcessorTransaction.findUncloseableProcessorTransaction", MAX_HOLDING_DAY, maxHoldingDay);
        final ProcessorTransactionType uncloseable = (ProcessorTransactionType) this.entityDao.get(ProcessorTransactionType.class, 8);
        /* update ProcessorTransactionType Id = 8 (Uncloseable) */
        for (ProcessorTransaction pt : lists) {
            if (pt.getPointOfSale().getCustomer().isIsMigrated()) {
                this.entityDao.evict(pt);
                pt.setProcessorTransactionType(uncloseable);
                this.entityDao.update(pt);
            }
        }
    }
    
    /**
     * TODO Replace with original logic AFTER migration is completed .
     */
    @Override
    @SuppressWarnings("unchecked")
    public final void cleanupCardAuthorizationBackup(final Date maxHoldingDay) {
        final List<PreAuthHolding> lists =
                this.entityDao.findByNamedQueryAndNamedParam("PreAuthHolding.findCardAuthorizationBackupForCleanUp", MAX_HOLDING_DAY, maxHoldingDay);
        for (PreAuthHolding preAuthHolding : lists) {
            if (preAuthHolding.getPointOfSale().getCustomer().isIsMigrated()) {
                this.entityDao.delete(preAuthHolding);
            }
        }
    }
    
    @Override
    public final void cleanupOverdueCardDataForMigratedCustomers(final PreAuth auth) {
        if (auth != null) {
            this.processorTransactionService.removePreAuthInProcessorTransactions(auth.getId());
            this.preAuthService.deleteExpiredPreauthForMigratedCustomer(auth);
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public final void removeCardRetryTransaction(final Long id) {
        CardRetryTransaction crt = null;
        if (id != null) {
            crt = this.entityDao.get(CardRetryTransaction.class, id);
        }
        
        if (crt != null) {
            this.entityDao.delete(crt);
        }
    }
    
    private CardProcessingManager self() {
        return this.beanFactory.getBean("cardProcessingManager", CardProcessingManager.class);
    }
    
    private CardRetryTransaction sync(final CardRetryTransaction inCrt) {
        CardRetryTransaction dbcrt = null;
        CardRetryTransaction crt = inCrt;
        if (crt.getId() != null) {
            dbcrt = this.entityDao.get(CardRetryTransaction.class, crt.getId());
        }
        
        if (dbcrt != null) {
            dbcrt.setAmount(crt.getAmount());
            dbcrt.setBadCardHash(crt.getBadCardHash());
            dbcrt.setCardData(crt.getCardData());
            dbcrt.setCardExpiry(crt.getCardExpiry());
            dbcrt.setCardHash(crt.getCardHash());
            dbcrt.setCardRetryTransactionType(crt.getCardRetryTransactionType());
            dbcrt.setCardType(crt.getCardType());
            dbcrt.setCreationDate(crt.getCreationDate());
            dbcrt.setDecryptedCardData(crt.getDecryptedCardData());
            dbcrt.setIgnoreBadCard(crt.isIgnoreBadCard());
            dbcrt.setIsRfid(crt.isIsRfid());
            dbcrt.setLast4digitsOfCardNumber(crt.getLast4digitsOfCardNumber());
            dbcrt.setLastResponseCode(crt.getLastResponseCode());
            dbcrt.setLastRetryDate(crt.getLastRetryDate());
            dbcrt.setNumRetries(crt.getNumRetries());
            if (crt.getPointOfSale() != null
                && (dbcrt.getPointOfSale() == null || !dbcrt.getPointOfSale().getId().equals(crt.getPointOfSale().getId()))) {
                dbcrt.setPointOfSale(this.entityDao.get(PointOfSale.class, crt.getPointOfSale().getId()));
            }
            
            dbcrt.setPurchasedDate(crt.getPurchasedDate());
            dbcrt.setTicketNumber(crt.getTicketNumber());
            
            crt = dbcrt;
        } else if (crt.getPointOfSale() != null && crt.getPointOfSale().getId() != null) {
            // Retrieve PointOfSale entity which will be managed by Hibernate.
            crt.setPointOfSale(this.entityDao.get(PointOfSale.class, crt.getPointOfSale().getId()));
        }
        return crt;
    }
    
    /* SINGLE PHASE TRANSACTION */
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public final boolean processCardForSingleTransaction(final TransactionData transactionData, final boolean isCreditCard)
        throws PaystationCommunicationException {
        
        // Safe to process transaction
        boolean isNoRefund = false;
        try {
            increaseProcessorCounter();
            final PointOfSale pos = transactionData.getPointOfSale();
            MerchantAccount merchantAccount = null;
            if (isCreditCard) {
                merchantAccount = this.merchantAccountService.findByPointOfSaleIdAndCardTypeId(pos.getId(), WebCoreConstants.CARD_TYPE_CREDIT_CARD);
            } else {
                merchantAccount = this.merchantAccountService.findByPointOfSaleIdAndCardTypeId(pos.getId(), WebCoreConstants.CARD_TYPE_VALUE_CARD);
            }
            if (merchantAccount == null) {
                throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_INVALID_MERCHANTACCOUNT);
            }
            transactionData.getProcessorTransaction().setMerchantAccount(merchantAccount);
            final CardProcessor processor = this.cardProcessorFactory.getCardProcessor(merchantAccount, pos);
            
            // Cannot find the requested merchant account
            if (processor == null) {
                throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_UNSUBSCRIBED_SERVICE_ERROR);
            }
            
            // For store and forward, for card data kept in memory, it is not encrypted
            // This only occurs during during initial processing attempt of S&F
            // Credit card data encrypted is min 50 bytes and raw is max 37 bytes
            String decryptedCardData = transactionData.getPurchase().getCardData();
            if (decryptedCardData.length() > MIN_DECRYPTED_CARD_DATA_LEN) {
                decryptedCardData = this.cryptoService.decryptData(decryptedCardData, pos.getId(), transactionData.getPurchase().getPurchaseGmt());
            }
            
            final Integer customerId = pos.getCustomer().getId();
            final Track2Card track2Card = new Track2Card(this.transactionFacade.findCreditCardType(decryptedCardData), customerId, decryptedCardData);
            
            // set ProcessorTransaction fields
            transactionData.getProcessorTransaction().setLast4digitsOfCardNumber(track2Card.getLast4DigitsOfAccountNumber());
            
            try {
                processChargeSinglePhaseTransaction(processor, transactionData, track2Card, isCreditCard);
            } catch (CryptoException ce) {
                LOG.error(INTERNAL_KEY_MISSING);
                isNoRefund = true;
            }
            
            // TODO
        } catch (MissingCryptoKeyException mcke) {
            LOG.warn(FAIL_LOCATE_KEY, mcke);
            throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_ENCRYPTION_ERROR);
        } catch (PaystationCommunicationException pce) {
            throw pce;
        } catch (Exception e) {
            
            LOG.error(UNKNOWN_ERROR, e);
            throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_UNKNOWN_ERROR);
        } finally {
            decreaseProcessorCounter();
        }
        return isNoRefund;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public final boolean processCardForSingleTransactionManualSave(final TransactionData transactionData, final boolean isCreditCard)
        throws PaystationCommunicationException {
        
        // Safe to process transaction
        boolean isNoRefund = false;
        try {
            increaseProcessorCounter();
            final PointOfSale pos = transactionData.getPointOfSale();
            MerchantAccount merchantAccount = null;
            if (isCreditCard) {
                merchantAccount = this.merchantAccountService.findByPointOfSaleIdAndCardTypeId(pos.getId(), WebCoreConstants.CARD_TYPE_CREDIT_CARD);
            } else {
                merchantAccount = this.merchantAccountService.findByPointOfSaleIdAndCardTypeId(pos.getId(), WebCoreConstants.CARD_TYPE_VALUE_CARD);
            }
            if (merchantAccount == null) {
                throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_INVALID_MERCHANTACCOUNT);
            }
            transactionData.getProcessorTransaction().setMerchantAccount(merchantAccount);
            final CardProcessor processor = this.cardProcessorFactory.getCardProcessor(merchantAccount, pos);
            
            // Cannot find the requested merchant account
            if (processor == null) {
                throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_UNSUBSCRIBED_SERVICE_ERROR);
            }
            
            // For store and forward, for card data kept in memory, it is not encrypted
            // This only occurs during during initial processing attempt of S&F
            // Credit card data encrypted is min 50 bytes and raw is max 37 bytes
            String decryptedCardData = transactionData.getPurchase().getCardData();
            if (decryptedCardData.length() > MIN_DECRYPTED_CARD_DATA_LEN) {
                decryptedCardData = this.cryptoService.decryptData(decryptedCardData, pos.getId(), transactionData.getPurchase().getPurchaseGmt());
            }
            
            final Integer customerId = pos.getCustomer().getId();
            final Track2Card track2Card = new Track2Card(this.transactionFacade.findCreditCardType(decryptedCardData), customerId, decryptedCardData);
            
            // set ProcessorTransaction fields
            transactionData.getProcessorTransaction().setLast4digitsOfCardNumber(track2Card.getLast4DigitsOfAccountNumber());
            
            try {
                processChargeSinglePhaseTransaction(processor, transactionData, track2Card, isCreditCard);
            } catch (CryptoException ce) {
                LOG.error(INTERNAL_KEY_MISSING);
                isNoRefund = true;
            }
            
            // TODO
        } catch (MissingCryptoKeyException mcke) {
            LOG.warn(FAIL_LOCATE_KEY, mcke);
            throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_ENCRYPTION_ERROR);
        } catch (PaystationCommunicationException pce) {
            throw pce;
        } catch (Exception e) {
            
            LOG.error(UNKNOWN_ERROR, e);
            throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_UNKNOWN_ERROR);
        } finally {
            decreaseProcessorCounter();
        }
        return isNoRefund;
    }
    
    @Override
    public final boolean isManualSave(final PointOfSale pointOfSale) {
        CardProcessor processor = null;
        
        final MerchantAccount ma =
                this.merchantAccountService.findByMerchantPOSPointOfSaleIdAndCardTypeId(pointOfSale.getId(), WebCoreConstants.CARD_TYPE_CREDIT_CARD);
        if (ma == null) {
            throw new CardProcessorException("Cannot find enabled CreditCard MerchantAccount, PointOfSaleId: " + pointOfSale.getId());
        }
        
        try {
            processor = this.cardProcessorFactory.getCardProcessor(ma, pointOfSale);
        } catch (CardProcessorPausedException ex) {
            LOG.error(ex.getStackTrace());
            return false;
        }
        
        return processor.getIsManualSave();
    }
    
    @Override
    public void specificProcessorPostProcessing(final ProcessorTransaction pt) {
        CardProcessor processor = null;
        try {
            processor = this.cardProcessorFactory.getCardProcessor(pt.getMerchantAccount(), pt.getPointOfSale());
        } catch (CardProcessorPausedException ex) {
            LOG.error(ex.getStackTrace());
        }
        
        if (processor != null && processor.getIsManualSave()) {
            processor.updateProcessorSpecificDetails(pt);
        }
    }
    
    private void processChargeSinglePhaseTransaction(final CardProcessor processor, final TransactionData transactionData,
        final Track2Card track2Card, final boolean isCreditCard) throws Exception {
        // Process transaction
        Object[] responses;
        if (isCreditCard) {
            try {
                final Method method =
                        CardProcessor.class.getDeclaredMethod(PROCESS_CHARGE, new Class[] { Track2Card.class, ProcessorTransaction.class });
                method.setAccessible(true);
                responses = (Object[]) this.kpiListenerService.invoke(processor, method,
                                                                      new Object[] { track2Card, transactionData.getProcessorTransaction() });
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                responses = processor.processCharge(track2Card, transactionData.getProcessorTransaction());
            } catch (SecurityException e) {
                e.printStackTrace();
                responses = processor.processCharge(track2Card, transactionData.getProcessorTransaction());
            }
            handleChargeResponseSinglePhaseTransaction(transactionData, track2Card, responses);
            
        } else {
            try {
                final Method method = CardProcessor.class.getDeclaredMethod(PROC_POSTAUTH, new Class[] { PreAuth.class, ProcessorTransaction.class });
                method.setAccessible(true);
                responses = (Object[]) this.kpiListenerService
                        .invoke(processor, method, new Object[] { transactionData.getPreAuth(), transactionData.getProcessorTransaction() });
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                responses = processor.processPostAuth(transactionData.getPreAuth(), transactionData.getProcessorTransaction());
            } catch (SecurityException e) {
                e.printStackTrace();
                responses = processor.processPostAuth(transactionData.getPreAuth(), transactionData.getProcessorTransaction());
            }
            
            handlePostAuthResponseSinglePhaseTransaction(transactionData, responses);
        }
    }
    
    private void handleChargeResponseSinglePhaseTransaction(final TransactionData transactionData, final Track2Card track2Card,
        final Object[] responses) throws PaystationCommunicationException, CryptoException {
        
        final int responseCode = (Integer) responses[0];
        
        // If approved, insert ProcessorTransaction record
        if (DPTResponseCodesMapping.CC_APPROVED_VAL__7000 == responseCode) {
            final Set<PaymentCard> paymentCards = transactionData.getPurchase().getPaymentCards();
            if (paymentCards != null && !paymentCards.isEmpty()) {
                final Iterator<PaymentCard> pcIterator = paymentCards.iterator();
                final PaymentCard paymentCard = pcIterator.next();
                paymentCard.setAmount(transactionData.getProcessorTransaction().getAmount());
                paymentCard.setCardProcessedGmt(DateUtil.getCurrentGmtDate());
                paymentCard.setIsApproved(true);
                paymentCard.setMerchantAccount(transactionData.getProcessorTransaction().getMerchantAccount());
                
                //TODO DETERMINE REAL PROCESSORTRANSACTIONTYPE
                paymentCard.setProcessorTransactionType(this.processorTransactionService
                        .getProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_CHARGE_WITH_NO_REFUNDS));
                transactionData.getProcessorTransaction().setProcessorTransactionType(this.processorTransactionService
                        .getProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_CHARGE_WITH_NO_REFUNDS));
                
                transactionData.setSingleTransactionCardData(createSingleTransactionCardData(transactionData, paymentCard, track2Card));
                
                return;
            }
        } else if (!isManualSave(transactionData.getPointOfSale())) {
            
            if (DPTResponseCodesMapping.CC_LOST_STOLEN_VAL__7006 == responseCode
                || DPTResponseCodesMapping.CC_BAD_CREDIT_CARD_DATA_VAL__7045 == responseCode) {
                throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_CREDIT_CARD_BAD_CARD);
            } else if (DPTResponseCodesMapping.CC_EXPIRED_VAL__7001 == responseCode) {
                throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_CREDIT_CARD_INVALID_CARD);
            } else {
                throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_CREDIT_CARD_DENIED);
            }
        }
    }
    
    private void handlePostAuthResponseSinglePhaseTransaction(final TransactionData transactionData, final Object[] responses)
        throws PaystationCommunicationException, CryptoException {
        
        if (((Boolean) responses[0]).booleanValue()) {
            
            final Set<PaymentCard> paymentCards = transactionData.getPurchase().getPaymentCards();
            if (paymentCards != null && !paymentCards.isEmpty()) {
                final Iterator<PaymentCard> pcIterator = paymentCards.iterator();
                final PaymentCard paymentCard = pcIterator.next();
                paymentCard.setAmount(transactionData.getProcessorTransaction().getAmount());
                paymentCard.setCardProcessedGmt(DateUtil.getCurrentGmtDate());
                paymentCard.setIsApproved(true);
                paymentCard.setMerchantAccount(transactionData.getProcessorTransaction().getMerchantAccount());
                
                //TODO DETERMINE REAL PROCESSORTRANSACTIOnTYPE
                paymentCard.setProcessorTransactionType(this.processorTransactionService
                        .getProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_CHARGE_WITH_NO_REFUNDS));
                transactionData.getProcessorTransaction().setProcessorTransactionType(this.processorTransactionService
                        .getProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_CHARGE_WITH_NO_REFUNDS));
            }
            
        } else {
            throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_CREDIT_CARD_DENIED);
        }
    }
    
    private SingleTransactionCardData createSingleTransactionCardData(final TransactionData transactionData, final PaymentCard paymentCard,
        final Track2Card track2Card) throws CryptoException {
        
        final SingleTransactionCardData stCardData = new SingleTransactionCardData();
        stCardData.setPurchase(transactionData.getPurchase());
        stCardData.setProcessorTransaction(transactionData.getProcessorTransaction());
        stCardData.setPurchaseGmt(transactionData.getPurchase().getPurchaseGmt());
        stCardData.setAmount(transactionData.getProcessorTransaction().getAmount());
        stCardData
                .setCardData(this.transactionFacade.encryptCardData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL, track2Card.getCreditCardPanAndExpiry()));
        stCardData.setCardType(paymentCard.getCreditCardType().getName());
        stCardData.setCardExpiry(Short.parseShort(track2Card.getExpirationMonth() + track2Card.getExpirationYear()));
        
        return stCardData;
        
    }
    
    @Override
    public final boolean processTransactionAuthorizationSinglePhaseTransaction(final TransactionData transactionData)
        throws PaystationCommunicationException {
        final CustomerCardType customerCardType = this.customerCardTypeService
                .getCardTypeByTrack2Data(transactionData.getPointOfSale().getCustomer().getId(), transactionData.getPurchase().getCardData());
        final Track2Card track2Card =
                new Track2Card(customerCardType, transactionData.getPointOfSale().getCustomer().getId(), transactionData.getPurchase().getCardData());
        
        // Determine if this is an accepted value card
        // look for Customer Defined Cards
        if (customerCardType == null || CardProcessingConstants.NAME_UNKNOWN.equals(customerCardType.getName())) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append(UNABLE_AUTHORIZE).append(track2Card.getDisplayCardNumber()).append(FOR_POS)
                    .append(transactionData.getPointOfSale().getSerialNumber()).append(PAT_N_ACCEPTED);
            LOG.error(bdr.toString());
            throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_CREDIT_CARD_INVALID_CARD);
        }
        
        // always check bad card list
        final Collection<CustomerBadCard> list =
                this.customerBadCardService.getCustomerBadCards(transactionData.getPointOfSale().getCustomer().getId(),
                                                                track2Card.getDecryptedCardData(), customerCardType.getId());
        if (list != null && !list.isEmpty()) {
            throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_CREDIT_CARD_BAD_CARD);
        }
        
        // Authorize card externally
        if (WebCoreConstants.AUTH_TYPE_EXTERNAL_SERVER == customerCardType.getAuthorizationType().getId()) {
            final MerchantAccount mad = findMerchantAccount(transactionData.getPointOfSale().getId(), WebCoreConstants.VALUE_CARD);
            if (mad == null) {
                LOG.info(createMerchantAccountErrorMessage(track2Card.getDisplayCardNumber(), transactionData.getPointOfSale().getSerialNumber(),
                                                           FIND_MERCH_ACT));
                throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_INVALID_MERCHANTACCOUNT);
            }
            
            CardProcessor processor = null;
            try {
                processor = this.cardProcessorFactory.getCardProcessor(mad, transactionData.getPointOfSale());
            } catch (CardProcessorPausedException cppe) {
                throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_PROCESSOR_PAUSED);
            }
            // Check if a processor was assigned to the payment station
            if (processor == null) {
                // Send 'unsubscribed service' to the PS
                LOG.info(createMerchantAccountErrorMessage(track2Card.getDisplayCardNumber(), transactionData.getPointOfSale().getSerialNumber(),
                                                           FIND_PROC));
                throw new PaystationCommunicationException(PaystationConstants.PAYSTATION_STATUS_TYPE_UNSUBSCRIBED_SERVICE_ERROR);
            }
            
            if (transactionData.getPreAuth() == null) {
                transactionData.setPreAuth(new PreAuth());
            }
            transactionData.getPreAuth().setCardType(track2Card.getDisplayName());
            transactionData.getPreAuth().setLast4digitsOfCardNumber(track2Card.getLast4DigitsOfAccountNumber());
            transactionData.getPreAuth().setPointOfSale(transactionData.getPointOfSale());
            
            transactionData.getPreAuth().setAmount(transactionData.getPurchase().getCardPaidAmount());
            transactionData.getPreAuth().setPreAuthDate(DateUtil.getCurrentGmtDate());
            transactionData.getPreAuth().setMerchantAccount(mad);
            transactionData.getPreAuth().setCardData(track2Card.getDecryptedCardData());
            try {
                transactionData.getPreAuth().setCardHash(this.cryptoAlgorithmFactory.getSha1Hash(transactionData.getPreAuth().getCardData(),
                                                                                                 CryptoConstants.HASH_CREDIT_CARD_PROCESSING));
            } catch (CryptoException ce) {
                throw new CardTransactionException(ce);
            }
            final int response = sendAuthorizationRequestToProcessor(processor, transactionData.getPreAuth(),
                                                                     transactionData.getPointOfSale().getCustomer().getId());
            if (response == CardProcessingConstants.STATUS_AUTHORIZED) {
                transactionData.getPreAuth().setCardData(track2Card.getDecryptedCardData());
                return true;
            } else {
                return false;
            }
            
        }
        
        // Authorize card locally
        if (isLocalCustomCardAuthorized(customerCardType, transactionData.getPointOfSale(), track2Card)) {
            return true;
        }
        
        return false;
    }
    
    @Override
    public final void processElavonReversalCleanup(final Collection<PreAuth> preAuths) {
        try {
            // insert records into PreAuthHolding 
            processExpiredCardAuthorizationBackUp(preAuths);
            // set preAuthId = null in ElavonTransactionDetail table to remove foreign key constraint for delete in PreAuth
            final List<Long> preAuthIds = new ArrayList<Long>(preAuths.size());
            for (PreAuth auth : preAuths) {
                preAuthIds.add(auth.getId());
            }
            // Remove PreAuthIds in ProcessorTransaction records.
            this.processorTransactionService.removePreAuthsInProcessorTransactions(preAuthIds);
            
            this.elavonTransactionDetailService.updateForPreauthTransferToHolding(preAuthIds);
            // delete preAuths
            this.preAuthService.deletePreauthsByIds(preAuthIds);
        } catch (CryptoException e) {
            LOG.error(e);
        }
    }
    
    public final KPIListenerService getKpiListenerService() {
        return this.kpiListenerService;
    }
    
    public final void setKpiListenerService(final KPIListenerService kpiListenerService) {
        this.kpiListenerService = kpiListenerService;
    }
    
    public final void setCPSProcessor(final CPSProcessor cpsProcessor) {
        this.cpsProcessor = cpsProcessor;
    }
    
    @Override
    public final void setMessageSource(final MessageSource messageSource) {
        this.messageAccessor = new MessageSourceAccessor(messageSource);
        
    }
    
    @Override
    public final ProcessorInfo getEMVProcessorInfo(final int processorId) {
        return this.cardProcessorFactory.getEMVProcessorInfo().get(processorId);
    }
    
    @Override
    public final int processLinkCPSTransactionAuthorization(final MerchantAccount merchantAccount, final PointOfSale pointOfSale,
        final PreAuth preAuth, final String cardTypeName) throws CryptoException {
        return this.cardProcessorFactory.getLinkCPSProcessor(merchantAccount, pointOfSale)
                .processLinkCPSTransactionAuthorization(merchantAccount, pointOfSale, preAuth, cardTypeName);
    }
    
    @Override
    public final ProcessorTransaction processLinkCPSTransactionRefund(final ProcessorTransaction origPtd, final MerchantAccount merchantAccount) throws CryptoException {
        return this.cardProcessorFactory.getLinkCPSProcessor(merchantAccount, origPtd.getPointOfSale())
                .processLinkCPSTransactionRefund(origPtd, merchantAccount);
    }
    
    private Object[] processLinkCPSTransactionCharge(final String cardToken, final long origProcTransId,
        final ProcessorTransaction chargeProcessorTransaction, final MerchantAccount merchantAccount, final PointOfSale pointOfSale,
        final Permit permit, final PaymentCard paymentCard) throws Exception {
        return this.cardProcessorFactory.getLinkCPSProcessor(merchantAccount, pointOfSale)
                .processLinkCPSTransactionCharge(cardToken, origProcTransId, chargeProcessorTransaction, merchantAccount, pointOfSale, permit,
                                                 paymentCard);
    }
    
    @Override
    public final int processLinkCPSTransactionCharge(final ExtensiblePermit extensiblePermit, final long origProcTransId,
        final ProcessorTransaction chargeProcessorTransaction, final MerchantAccount merchantAccount, final PointOfSale pointOfSale,
        final Permit permit, final PaymentCard paymentCard) {
        Authorization authorization = null;
        try {
            final Object[] responses =
                    this.processLinkCPSTransactionCharge(extensiblePermit.getCardData(), origProcTransId, chargeProcessorTransaction, merchantAccount,
                                                         pointOfSale, permit, paymentCard);
            if (responses != null && responses.length > 0) {
                authorization = (Authorization) responses[0];
            } else {
                return CardProcessingConstants.AURA_CHARGE_UNKNOWN;
            }
        } catch (Exception e) {
            LOG.error("Unable to charge SMS with Link CPS, pointOfSale id: " + pointOfSale.getId(), e);
            handleFailedCharge(chargeProcessorTransaction, EXCEPTION_STRING, "Communication failed with Link CPS");
            return CardProcessingConstants.AURA_CHARGE_SEVER_SHUTDOWN;
        }
        
        int responseCode = CardProcessingConstants.AURA_CHARGE_UNKNOWN;
        
        if (authorization != null) {
            extensiblePermit.setCardData(authorization.getCardToken().toString());
            this.extensiblePermitService.updateExtensiblePermit(extensiblePermit);
            responseCode = Authorization.AuthotizationStatus.ACCEPTED.toString().equalsIgnoreCase(authorization.getStatus().getResponseStatus())
                    ? CardProcessingConstants.AURA_CHARGE_SUCCEED : CardProcessingConstants.AURA_CHARGE_DECLINE;
            if (responseCode == 0) {
                createPaymentCard(chargeProcessorTransaction, origProcTransId, WebCoreConstants.CREDIT_CARD, paymentCard);
                chargeProcessorTransaction.setIsApproved(true);
            }
        }
        return responseCode;
    }
}