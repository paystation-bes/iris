package com.digitalpaytech.cardprocessing.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.InetSocketAddress;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.Arrays;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import org.apache.http.conn.ssl.StrictHostnameVerifier;
import org.apache.log4j.Logger;

import com.digitalpaytech.cardprocessing.CardProcessor;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DPTResponseCodesMapping;
/*************************************************************************************************************
 * @author joannt
 * 
 *         ADS Processor messages are based upon the ISO 8583 standard.
 * 
 * 
 *         ISO-8583 message structure: Field # Description 0 - MTI Message Type Indicator 1 - Bitmap 64 (or
 *         128) bits indicating presence/absence of other fields 2 - 128 Other fields as specified in bitmap
 * 
 *         For further details refer to the following documents: -Alliance POS 8583 Code Supplement -Alliance
 *         POS 8583 Implementation Guide -Alliance POS 8583 Credit Card Supplement
 * 
 *         Note we are implementing Host Draft Capture mode.
 * 
 *         ADS Messages are sent over the Datawire network. For details refer to Datawire VXN Java API.
 ************************************************************************************************************/

public class HeartlandProcessor extends CardProcessor 
{
	private static Logger logger__ = Logger.getLogger(HeartlandProcessor.class);

	private final static String MESSAGE_ENCODE_CHARSET = "ISO-8859-1";
	private final static String MTI_AUTH_REQUEST = "1100";
	private final static String MTI_AUTH_RESPONSE = "1110";
	private final static String MTI_FINANCIAL_TRANSACTION_REQUEST = "1200"; // purchase
	private final static String MTI_FINANCIAL_TRANSACTION_RESPONSE = "1210";
	private final static String MTI_FINANCIAL_TRANSACTION_ADVICE_REQUEST = "1220"; // post-auth/return
	private final static String MTI_FINANCIAL_TRANSACTION_ADVICE_RESPONSE = "1230";
	private final static String MTI_REVERSAL_TRANSACTION_REQUEST = "1420";
	private final static String MTI_REVERSAL_TRANSACTION_RESPONSE = "1430";
	private final static String DE3_PREAUTH_PROCESSING_CODE = "003000";
	private final static String DE3_POSTAUTH_PROCESSING_CODE = "003000";
	private final static String DE3_SALE_PROCESSING_CODE = "003000";
	private final static String DE3_REFUND_PROCESSING_CODE = "200030";
	private final static String DE22_POS_SWIPED_DATA_CODE = "201201200140";
	private final static String DE22_POS_MANUAL_DATA_CODE = "601201600140";
	private final static String DE39_APPROVED_ACTION_CODE = "000";
	private final static String DE39_TIMEOUT_ACTION_CODE = "911";
	private final static String DE39_DUPLICATE_ACTION_CODE = "481";
	private final static int DE41_FIELD_LENGTH = 8;
	private final static int DE42_FIELD_LENGTH = 15;
	// message control data bitmap
	private final static char[] de48_bitmap_ = { (char) 128, (char) 0,
	        (char) 0, (char) 0, (char) 0, (char) 0, (char) 0, (char) 0 };
	private final static String DE48_BITMAP = new String(de48_bitmap_);
	private final static String DE48_1_COMMUNICATIONS_DIAGNOSTICS = "0000";

	// * Alliance e-header values:
	// * EH1 - total trans length (hearder + message) (to be filled in later)
	// (b2)
	// * EH2 - ID (a2) = EH
	// * EH3 - reserved (b2) = 00
	// * EH4 - type message (b1) = 00100011 (35 = Alliance POS 8583)
	// * EH5 - character set (b1) = 00000001 (ASCII)
	// * EH6 - response code (b1) = 00000000 (default)
	// * EH7 - response code origin (b1) = 00000000 (default)
	// * EH8 - processing flag (b1) = 00000000 (non-persistent)
	// * EH9 - protocol type (b1) = 00000001 (TCP/IP)
	// * EH10 - downstream connection type (b1) = 00110001 (49 = Datawire
	// internet)
	// * EH11 - node identification (ap4) = <spc><spc><spc><spc>
	// * EH12 - origin correlation 1 (b2) = 00000000 00000000 (default when not
	// used)
	// * EH13 - company id (ap4) = Datawire MID
	// * EH14 - origin correlation 2 (b8) =
	// * 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000
	// * EH15 - version (b1) = 1 (version 1)
	private char[] eheader_ = { (char) 0, (char) 0, 'E', 'H', (char) 0,
	        (char) 0, '#', (char) 1, (char) 0, (char) 0, (char) 0, (char) 1,
	        'C', ' ', ' ', ' ', ' ', (char) 0, (char) 0, ' ', ' ', ' ', ' ',
	        (char) 0, (char) 0, (char) 0, (char) 0, (char) 0, (char) 0,
	        (char) 0, (char) 0, (char) 1 };

	private final char[] SALE_BITMAP_TRACK_2 = { (char) 48, (char) 48,
	        (char) 69, (char) 0, (char) 32, (char) 193, (char) 0, (char) 0 };
	private final char[] SALE_BITMAP_PAN_EXPIRATION = { (char) 112, (char) 52,
	        (char) 69, (char) 0, (char) 0, (char) 193, (char) 0, (char) 0 };
	private final String ZERO_STRING = "00";
	private final String X_STRING = "XXXXXXXXXXXXXXXXXXXX"; // 20 X's

	// parsed responses from the processor's server
	private String actionCode_;
	private String authOrErrorCode_;
	private String referenceNumber_;
	private String processorTransactionId_;
	private int merchantAccountId_;
	private String transDate_;
	private String destinationUrlString;

	// merchant account credentials
	private String merchantId;
	private String terminalId;

	public HeartlandProcessor(MerchantAccount md,
	        CommonProcessingService commonProcessingService,
	        EmsPropertiesService emsPropertiesService, CustomerCardTypeService customerCardTypeService) 
	{
		super(md, commonProcessingService, emsPropertiesService);
		setCustomerCardTypeService(customerCardTypeService);
		merchantAccountId_ = md.getId();
		setMerchantId(md.getField1());
		setTerminalId(md.getField2());

		String sd_url_list = md.getField4();
		destinationUrlString = commonProcessingService.getDestinationUrlString(md.getProcessor());

		// Update e-header with merchant id
		for (int i = 0; i < getMerchantId().trim().length(); i++) 
		{
			eheader_[i + 19] = getMerchantId().charAt(i);
		}

		if (logger__.isDebugEnabled()) {
			logger__.debug("Heartland destination url: " + destinationUrlString);
		}
	}

	public boolean processPreAuth(PreAuth pd) throws CardTransactionException 
	{
		CustomerCardType customerCardType = getCustomerCardTypeService().getCardTypeByTrack2Data(getMerchantAccount().getCustomer().getId(), pd.getCardData());
		Track2Card track_2_card = new Track2Card(customerCardType, getMerchantAccount().getCustomer().getId(), pd.getCardData());
		String reference_number = getCommonProcessingService().getNewReferenceNumber(getMerchantAccount());

		// Create and send request
		char[] request = createPreAuthRequest(track_2_card, reference_number, pd.getAmount());
		String response = new String(sendRequest(request, reference_number));

		// Process response
		String pan = track_2_card.getPrimaryAccountNumber();
		
		String log_message = response.replaceAll(pan, X_STRING.substring(0, pan.length()));
		log_message = CardProcessingUtil.replaceBinary(log_message);
		logger__.info("PreAuth response: " + log_message);
	
		pd.setReferenceNumber(reference_number);
		extractPreAuthResponses(response, pd);

		return (pd.isIsApproved());
	}

	public Object[] processPostAuth(PreAuth pd, ProcessorTransaction ptd) throws CardTransactionException
	{
		String reference_number = getCommonProcessingService().getNewReferenceNumber(getMerchantAccount());
		CustomerCardType customerCardType = getCustomerCardTypeService().getCardTypeByTrack2DataOrAccountNumber(getMerchantAccount().getCustomer().getId(), pd.getCardData());
		Track2Card track_2_card = new Track2Card(customerCardType, getMerchantAccount().getCustomer().getId(), pd.getCardData());

		// Create and send request
		char[] request = createPostAuthRequest(track_2_card, reference_number, pd.getAuthorizationNumber(), ptd.getAmount());
		String response = new String(sendRequest(request, reference_number));

		// Process response
		String pan = track_2_card.getPrimaryAccountNumber();
		
		String log_message = response.replaceAll(pan,
		        X_STRING.substring(0, pan.length()));
		log_message = CardProcessingUtil.replaceBinary(log_message);
		logger__.info("Settle response: " + log_message);
	
		extractPostAuthResponses(response);

		// Set ProcessorTransactionData values from processor
		ptd.setProcessingDate(new Date());
		ptd.setReferenceNumber(reference_number);
		ptd.setProcessorTransactionId(processorTransactionId_);
		ptd.setAmount(pd.getAmount());

		// Check for timeout
		if (actionCode_.equals(DE39_TIMEOUT_ACTION_CODE)) 
		{
			String originalDate = String.valueOf(request, 68, 12);

        	createAndSendReversal(MTI_FINANCIAL_TRANSACTION_ADVICE_REQUEST,
        	        DE3_POSTAUTH_PROCESSING_CODE, CardProcessingUtil.getCentsAmountInDollars(ptd.getAmount()),
        	        reference_number, originalDate, track_2_card,
        	        authOrErrorCode_, merchantAccountId_,
        	        ptd.getPurchasedDate(), ptd.getTicketNumber(),
        	        ptd.getPointOfSale().getId());
		}

		// Approved
		if (actionCode_.equals(DE39_APPROVED_ACTION_CODE)
		        || actionCode_.equals(DE39_DUPLICATE_ACTION_CODE)) 
		{
			ptd.setAuthorizationNumber(authOrErrorCode_);
			ptd.setIsApproved(true);
			return new Object[] { true, actionCode_ };
		}

		// Declined
		ptd.setAuthorizationNumber(null);
		ptd.setIsApproved(false);
		return new Object[] { false, actionCode_ };
	}

	public Object[] processRefund(boolean isNonEmsRequest, ProcessorTransaction origPtd, ProcessorTransaction refundPtd,
	        String ccNumber, String expiryMMYY) throws CardTransactionException 
	{
		StringBuilder bdr = new StringBuilder();
		// pan + TRACK_2_DELIMITER + expiryYY + expiryMM
		bdr.append(ccNumber).append(CardProcessingConstants.TRACK_2_DELIMITER).append(expiryMMYY.substring(2)).append(expiryMMYY.substring(0, 2));
		String cardData = bdr.toString();

		CustomerCardType customerCardType = getCustomerCardTypeService().getCardTypeByTrack2DataOrAccountNumber(getMerchantAccount().getCustomer().getId(), cardData);
		Track2Card track_2_card = new Track2Card(customerCardType, getMerchantAccount().getCustomer().getId(), cardData);
		referenceNumber_ = Long.toString(getMerchantAccount().getReferenceCounter());
		String reference_number = getCommonProcessingService().getNewReferenceNumber(getMerchantAccount());

		// Create request
		char[] request = createRefundRequest(track_2_card, reference_number, origPtd.getAmount(), refundPtd.getAmount());

		// Send request
		String response;
		try 
		{
			response = new String(sendRequest(request, reference_number));
		} 
		catch (CardTransactionException e) 
		{
        	createAndSendReversal(MTI_FINANCIAL_TRANSACTION_ADVICE_REQUEST,
        	        DE3_REFUND_PROCESSING_CODE,
        	        CardProcessingUtil.getCentsAmountInDollars(refundPtd.getAmount()), reference_number, transDate_,
        	        track_2_card, null, merchantAccountId_,
        	        refundPtd.getPurchasedDate(), refundPtd.getTicketNumber(),
        	        refundPtd.getPointOfSale().getId());

			throw e;
		}

		// Process response
		String pan = track_2_card.getPrimaryAccountNumber();
	
		String log_message = response.replaceAll(pan,
		        X_STRING.substring(0, pan.length()));
		log_message = CardProcessingUtil.replaceBinary(log_message);
		logger__.info("Refund response: " + log_message);
	
		// Parse response
		extractRefundResponses(response);

		// Set processor transaction data values
		refundPtd.setProcessingDate(new Date());
		refundPtd.setReferenceNumber(reference_number);
		refundPtd.setProcessorTransactionId(processorTransactionId_);

		// Approved
		if (actionCode_.equals(DE39_APPROVED_ACTION_CODE)) 
		{
			refundPtd.setAuthorizationNumber(authOrErrorCode_);
			refundPtd.setIsApproved(true);
			return new Object[] {
			        DPTResponseCodesMapping.CC_APPROVED_VAL__7000, actionCode_ };
		}

		// Declined
		refundPtd.setAuthorizationNumber(null);
		refundPtd.setIsApproved(false);

		// ADS requires reversal attempt on timeout response
		if (actionCode_.equals(DE39_TIMEOUT_ACTION_CODE)) 
		{
            createAndSendReversal(MTI_FINANCIAL_TRANSACTION_ADVICE_REQUEST,
                    DE3_REFUND_PROCESSING_CODE,
                    CardProcessingUtil.getCentsAmountInDollars(refundPtd.getAmount()), reference_number, transDate_,
                    track_2_card, authOrErrorCode_, merchantAccountId_,
                    refundPtd.getPurchasedDate(), refundPtd.getTicketNumber(),
                    refundPtd.getPointOfSale().getId());
		}

		// map error response if request was not made by EMS
		if (isNonEmsRequest) 
		{
			return new Object[] { mapErrorResponse(authOrErrorCode_), actionCode_ };
		}

		// EMS request
		try 
		{
			return new Object[] { Integer.parseInt(authOrErrorCode_),
			        actionCode_ };
		} 
		catch (Exception e) 
		{
			throw new CardTransactionException("Expecting numeric response, but recieved: " + authOrErrorCode_, e);
		}
	}

	public Object[] processCharge(Track2Card track2Card, ProcessorTransaction chargePtd) throws CardTransactionException 
	{
		String reference_number = getCommonProcessingService()
		        .getNewReferenceNumber(getMerchantAccount());

		// Create request
		char[] request = null;
		request = createChargeRequest(track2Card, reference_number, chargePtd.getAmount());

		// Send request
		String response;
		try 
		{
			response = new String(sendRequest(request, reference_number));
		} 
		catch (CardTransactionException e) 
		{
			// xxx Add By Tony
			if (!getCommonProcessingService().isTestMode(super.getMerchantAccount().getId())) 
			{
            	createAndSendReversal(MTI_FINANCIAL_TRANSACTION_REQUEST,
            	        DE3_SALE_PROCESSING_CODE,
            	        CardProcessingUtil.getCentsAmountInDollars(chargePtd.getAmount()), reference_number, transDate_,
            	        track2Card, null, merchantAccountId_,
            	        chargePtd.getPurchasedDate(),
            	        chargePtd.getTicketNumber(),
            	        chargePtd.getPointOfSale().getId());
			}
			throw (e);
		}

		// Extract data from the response
		String pan = track2Card.getPrimaryAccountNumber();
		String log_message = response.replaceAll(pan,
		        X_STRING.substring(0, pan.length()));
		log_message = CardProcessingUtil.replaceBinary(log_message);
		logger__.info("Charge response: " + log_message);
	
		extractSaleResponses(response);

		// Set processor transaction data values
		chargePtd.setProcessingDate(new Date());
		chargePtd.setReferenceNumber(reference_number);
		chargePtd.setProcessorTransactionId(processorTransactionId_);

		// Approved
		if (actionCode_.equals(DE39_APPROVED_ACTION_CODE)) 
		{
			chargePtd.setAuthorizationNumber(authOrErrorCode_);
			chargePtd.setIsApproved(true);
			return new Object[] {
			        DPTResponseCodesMapping.CC_APPROVED_VAL__7000, actionCode_ };
		}

		// Check for timeout
		if (actionCode_.equals(DE39_TIMEOUT_ACTION_CODE)) 
		{
            	createAndSendReversal(MTI_FINANCIAL_TRANSACTION_REQUEST,
            	        DE3_SALE_PROCESSING_CODE,
            	        CardProcessingUtil.getCentsAmountInDollars(chargePtd.getAmount()), reference_number, transDate_,
            	        track2Card, null, merchantAccountId_,
            	        chargePtd.getPurchasedDate(), chargePtd.getTicketNumber(),
            	        chargePtd.getPointOfSale().getId());
		}

		// Declined
		chargePtd.setAuthorizationNumber(null);
		chargePtd.setIsApproved(false);
		return new Object[] { mapErrorResponse(authOrErrorCode_), actionCode_ };
	}

	private void createAndSendReversal(String originalMti,
	        String processingCode, BigDecimal amountInDollars,
	        String referenceNumber, String originalDate, Track2Card track2Card,
	        String lastResponseCode, int merchantAccountId, Date date,
	        int ticketNumber, int pointOfSaleId) 
	{
		Reversal reversal = null;
		try 
		{
            reversal = getCardProcessingManager().createReversal(originalMti,
                    processingCode, amountInDollars, referenceNumber,
                    originalDate, track2Card.getCreditCardPanAndExpiry(),
                    lastResponseCode, merchantAccountId, date, ticketNumber,
                    pointOfSaleId);
		} 
		catch (Exception e) 
		{
			String msg = "Unable to create Reversal record";
			logger__.error(msg, e);
			getCommonProcessingService().sendCardProcessingAdminErrorAlert("Unable to call 'createAndSendReversal' in HeartlandProcessor. ", msg, e);
			// xxx add by tony
			// if create Reversal have problem, stop doing process Reversal
			return;
		}

		try 
		{
			getCardProcessingManager().processReversal(reversal);
		} 
		catch (Exception e) {
			String msg = "Unable to update reversal after processing: " + reversal;
			logger__.error(msg, e);
			getCommonProcessingService().sendCardProcessingAdminErrorAlert("Unable to call 'processReversal' in HeartlandProcessor. ", msg, e);
		}
	}

	// * Pre-Auth request message syntax:
	// * MTI - Message type indicator = 1100
	// * Primary bitmap (b8) =
	// * 00110000 00110000 01000101 00000000 00100000 11000001 00000000 00000000
	// * DE3 - Processing code (n6) = 003000
	// * DE4 - Transaction amount (n12)
	// * DE11 - System trace audit number (STAN) (n6)
	// * DE12 - Transaction date and time (n12 - YYMMDDhhmmss)
	// * DE18 - Merchant type (n4) = 7523
	// * DE22 - Point of service data code (an12) = 200201200140
	// * DE24 - Function code (n3) = 100
	// * DE35 - Track 2 data (LLVAR ns..37)
	// * DE41 - Card Acceptor terminal id code (ans8) =
	// DGPT<spc><spc><spc><spc>,
	// for eg
	// * DE42 - Card Acceptor id code (ans15) = TESTDGPT0101<spc><spc><spc>, for
	// eg
	// * DE48_0 - Message control bitmap (b8) =
	// * 10000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000
	// * DE48_1 - Communications diagnostics (n4) = 0000
	private char[] createPreAuthRequest(Track2Card track2Card, String referenceNumber, int amountInCents) 
	{
		StringBuffer message = new StringBuffer(172);

		message.append(new String(eheader_));
		message.append(MTI_AUTH_REQUEST); // MTI
		char[] auth_bitmap = { (char) 48, (char) 48, (char) 69, (char) 0,
		        (char) 32, (char) 193, (char) 0, (char) 0 };
		message.append(new String(auth_bitmap)); // Primary bitmap
		message.append(DE3_PREAUTH_PROCESSING_CODE); // DE 3
		message.append(formatDollarAmount(amountInCents, 12)); // DE 4
		message.append(referenceNumber.substring(referenceNumber.length() - 6)); // DE
																				 // 11
		message.append(getTransactionDate()); // DE 12
		message.append("7523"); // DE 18
		message.append(DE22_POS_SWIPED_DATA_CODE);
		message.append("100"); // DE 24

		int card_data_length = track2Card.getDecryptedCardData().length();
		if (card_data_length < 10 || card_data_length > 40) 
		{
			throw new IllegalArgumentException("Invalid Track 2 data length: "
			        + card_data_length);
		}
		String track2_length = Integer.toString(card_data_length);

		message.append(track2_length); // DE 35
		message.append(track2Card.getDecryptedCardData()); // DE 35
		message.append(padWithTrailingSpaces(getMerchantId(), DE41_FIELD_LENGTH));
		message.append(padWithTrailingSpaces(getTerminalId(), DE42_FIELD_LENGTH));
		message.append("012"); // DE 48
		message.append(DE48_BITMAP);
		message.append(DE48_1_COMMUNICATIONS_DIAGNOSTICS);

		// Add the message length to the e-header
		int message_length = message.length();
		char[] message_array = new char[message_length];
		message.getChars(0, message_length, message_array, 0);
		message_array[0] = (char) 0;
		message_array[1] = (char) (message_length);

		String request = new String(message_array);
		String log_message = request.replaceAll(track2Card.getDecryptedCardData(),
		        track2Card.getDecryptedCardData().replaceAll("[^=]", "X"));
		log_message = CardProcessingUtil.replaceBinary(log_message);
		logger__.info("PreAuth request: " + log_message);
	
		return message_array;
	}

	// * Pre-Auth expected message syntax for success response:
	// * MTI - Message type indicator = 1110
	// * Primary bitmap (b8) =
	// * 01110000 00110000 00000000 00000100 00000110 11000000 10000000 00000000
	// * DE2 - Primary account number (PAN) (LLVAR n..19)
	// * DE3 - Processing code (n6) = 003000
	// * DE4 - Transaction amount (n12)
	// * DE11 - System trace audit number (STAN) (n6)
	// * DE12 - Transaction date and time (n12 - YYMMDDhhmmss)
	// * DE30 - Original amount (n24)
	// * DE38 - Approval code (anp6)
	// * DE39 - Action code (n3)
	// * DE41 - Card Acceptor terminal id code (ans8) - echoed from original
	// message
	// * DE42 - Card Acceptor id code (ans15) - echoed from original message
	// * DE49 - Transaction currency code (n3) = 840 (US dollars)
	private void extractPreAuthResponses(String heartland_response, PreAuth pd)
	        throws CardTransactionException 
	{
		pd.setProcessingDate(new Date());

		if (heartland_response.length() < 44)
		{
			throw new CardTransactionException(
			        "Heartland response is too short, should be >= 44 characters but is only "
			                + heartland_response.length());
		}

		// message type identified
		String transactionType = heartland_response.substring(32, 36);
		String primary_bitmap = heartland_response.substring(36, 44);

		char[] pre_auth_expected_bitmap_start2 = { (char) 112, (char) 48,
		        (char) 0, (char) 0, (char) 6, (char) 192 };

		if (transactionType.equals(MTI_AUTH_RESPONSE)
		        && primary_bitmap.startsWith(new String(
		                pre_auth_expected_bitmap_start2))) 
		{
			int pan_length = Integer.parseInt(heartland_response.substring(44,
			        46)); // DE2
			int array_position = 46 + pan_length + 18;

			// DE11
			pd.setProcessorTransactionId(heartland_response.substring(
			        array_position, array_position + 6));
			array_position += 18;

			String approval_code = heartland_response.substring(array_position,
			        array_position + 6); // DE38
			pd.setAuthorizationNumber(approval_code);
			array_position += 6;

			// DE39 - Value of 000 means success
			pd.setResponseCode(heartland_response.substring(array_position,
			        array_position + 3));
		}

		else if ((heartland_response.substring(2, 4)).equals("EH")) // error
																	// response
		{
			int start = heartland_response.indexOf(getMerchantId(), 32) - 3;
			String auth_or_error_code = heartland_response.substring(start,
			        start + 3);

			if (!auth_or_error_code.equals("000")) {
				// set auth code so processor offline exception isn't thrown
				pd.setResponseCode(auth_or_error_code);
			}
		} 
		else 
		{
			throw new CardTransactionException(
			        "HeartlandProcessor, extractPreAuthResponses, nothing to do!! heartland_response: "
			                + heartland_response);
		}

		StringBuilder sb = new StringBuilder(200);
		sb.append("PreAuth Response:  ActionCode: ");
		sb.append(actionCode_);
		sb.append(", Auth/Error Code: ");
		sb.append(authOrErrorCode_);
		sb.append(", ProcessorTransactionId: ");
		sb.append(processorTransactionId_);
		logger__.info(sb.toString());
	

		// Check for invalid response
		if ((pd.getResponseCode() == null) || (pd.getResponseCode().equals(""))
		        || (pd.getResponseCode().length() == 0))
		{
			throw new CardTransactionException("No response code");
		}

		// Approved
		if (pd.getResponseCode().equals(DE39_APPROVED_ACTION_CODE)) 
		{
			pd.setIsApproved(true);
		}

		// Declined
		else 
		{
			pd.setIsApproved(false);
		}
	}

	// * Post-Auth request message syntax:
	// Message Type Indicator (MTI) = 1220
	// primary bitmap (b8) =
	// 00110000 00110000 01000101 10000000 00100100 11000001 00000000 00000000
	// DE3 - Processing code (n6) = 003000
	// DE4 - Transaction amount (n12)
	// DE11 - System trace audit number (STAN) (n6)
	// DE12 - Transaction date and time (n12 - YYMMDDhhmmss)
	// DE18 - Merchant type (n4) = 7523
	// DE22 - Point of service data code (an12) = 200201200140
	// DE24 - Function code (n3) = 201
	// DE25 - Message reason code (n4) = 1376 (auth-capture)
	// DE35 - Track 2 data
	// DE38 - Approval code (anp6)
	// DE41 - Card Acceptor terminal id code (ans8) =
	// DGPT<spc><spc><spc><spc>,for eg
	// DE42 - Card Acceptor id code (ans15) = TESTDGPT0101<spc><spc><spc>, for
	// eg
	// DE48_0 - Message control bitmap (b8) =
	// 10000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000
	// DE48_1 - Communications diagnostics (n4) = 0000
	private char[] createPostAuthRequest(Track2Card track2Card, String referenceNumber, String authCode, int amountInCents) 
	{
		StringBuffer message = new StringBuffer(178);
		message.append(new String(eheader_));
		message.append(MTI_FINANCIAL_TRANSACTION_ADVICE_REQUEST); // MTI
		char[] post_auth_bitmap = { (char) 48, (char) 48, (char) 69,
		        (char) 128, (char) 36, (char) 193, (char) 0, (char) 0 };
		message.append(new String(post_auth_bitmap)); // Primary bitmap
		message.append(DE3_POSTAUTH_PROCESSING_CODE); // DE 3
		message.append(formatDollarAmount(amountInCents, 12)); // DE 4
		message.append(referenceNumber.substring(referenceNumber.length() - 6)); // DE
																				 // 11
		message.append(getTransactionDate()); // DE 12
		message.append("7523"); // DE 18
		message.append(DE22_POS_SWIPED_DATA_CODE);
		message.append("201"); // DE 24
		message.append("1376"); // DE 25

		int card_data_length = track2Card.getDecryptedCardData().length();
		if (card_data_length < 10 || card_data_length > 40) 
		{
			throw new IllegalArgumentException("Invalid Track 2 data length: "
			        + card_data_length);
		}
		String track2_length = Integer.toString(card_data_length);

		message.append(track2_length); // DE 35
		message.append(track2Card.getDecryptedCardData()); // DE 35
		message.append(padWithTrailingSpaces(authCode, 6)); // DE 38
		message.append(padWithTrailingSpaces(getMerchantId(), DE41_FIELD_LENGTH));
		message.append(padWithTrailingSpaces(getTerminalId(), DE42_FIELD_LENGTH));
		message.append("012"); // DE 48
		message.append(DE48_BITMAP);
		message.append(DE48_1_COMMUNICATIONS_DIAGNOSTICS);

		// Add the message length to the e-header
		int message_length = message.length();
		char[] message_array = new char[message_length];
		message.getChars(0, message_length, message_array, 0);
		message_array[0] = (char) 0;
		message_array[1] = (char) (message_length);

		String request = new String(message_array);
		String log_message = request.replaceAll(track2Card.getDecryptedCardData(), track2Card.getDecryptedCardData().replaceAll("[^=]", "X"));
		log_message = CardProcessingUtil.replaceBinary(log_message);
		logger__.info("Settle request: " + log_message);
	
		return message_array;
	}

	// * Post-Auth expected message syntax for success response:
	//
	// MTI - Message type indicator = 1230
	// Primary bitmap (b8) =
	// 01110000 00110000 00000000 00000000 00000110 11000000 10000000 00000000
	// DE2 - Primary account number (PAN) (LLVAR n..19)
	// DE3 - Processing code (n6) = 003000
	// DE4 - Transaction amount (n12)
	// DE11 - System trace audit number (STAN) (n6)
	// DE12 - Transaction date and time (n12 - YYMMDDhhmmss)
	// DE18 - Merchant type (n4) = 7523
	// DE38 - Approval code (anp6)
	// DE39 - Action code (n3)
	// DE41 - Card Acceptor terminal id code (ans8) - echoed from original
	// message
	// DE42 - Card Acceptor id code (ans15) - echoed from original message
	// DE49 - Transaction currency code (n3) = 840 (US dollars)
	private void extractPostAuthResponses(String heartland_response) throws CardTransactionException 
	{
		if (heartland_response.length() < 44) {
			throw new CardTransactionException("Heartland response is too short, should be >= 44 characters but is only "
			                + heartland_response.length());
		}

		// message type identified
		String transactionType = heartland_response.substring(32, 36);
		String primary_bitmap = heartland_response.substring(36, 44);

		char[] post_auth_expected_bitmap = { (char) 112, (char) 48, (char) 0,
		        (char) 0, (char) 6, (char) 192, (char) 0, (char) 0 };
		char[] post_auth_expected_bitmap_starts_with = { (char) 112, (char) 48,
		        (char) 0, (char) 0, (char) 6, (char) 192 };

		if (transactionType.equals(MTI_FINANCIAL_TRANSACTION_ADVICE_RESPONSE)
		        && primary_bitmap.startsWith(new String(post_auth_expected_bitmap_starts_with))) 
		{
			int pan_length = Integer.parseInt(heartland_response.substring(44,
			        46)); // DE2
			int array_position = 46 + pan_length + 18;

			String stan = heartland_response.substring(array_position,
			        array_position + 6); // DE11
			processorTransactionId_ = stan;
			array_position += 18;

			String approval_code = heartland_response.substring(array_position,
			        array_position + 6); // DE38
			authOrErrorCode_ = approval_code;
			array_position += 6;

			String action_code = heartland_response.substring(array_position,
			        array_position + 3); // DE39
			actionCode_ = action_code; // Value of 000 means success
		}

		else if ((heartland_response.substring(2, 4)).equals("EH")) // error
																	// response
		{
			int start = heartland_response.indexOf(getMerchantId(), 32) - 3;
			authOrErrorCode_ = heartland_response.substring(start, start + 3);

			if (!authOrErrorCode_.equals("000")) 
			{
				// set auth code so processor offline exception isn't thrown
				actionCode_ = authOrErrorCode_;
			} else 
			{
				actionCode_ = "";
			}
		} 
		else 
		{
			throw new CardTransactionException("HeartlandProcessor, extractPostAuthResponses, nothing to do !! heartland_response: "
			                + heartland_response);
		}
	
		StringBuilder sb = new StringBuilder(200);
		sb.append("Settle Response:  ActionCode: ");
		sb.append(actionCode_);
		sb.append(", Auth/Error Code: ");
		sb.append(authOrErrorCode_);
		sb.append(", ProcessorTransactionId: ");
		sb.append(processorTransactionId_);
		logger__.info(sb.toString());
	}

	// * Sale request message syntax:
	// * Message Type Indicator (MTI) = 1200
	// * primary bitmap (b8) when using track 2
	// * 00110000 00110000 01000101 00000000 00100000 11000001 00000000 00000000
	// * primary bitmap (b8) when using PAN/expiration
	// * 01110000 00110100 01000101 00000000 00000000 11000001 00000000 00000000
	// * DE2 - PAN (n..19)
	// * DE3 - Processing code (n6) = 003000
	// * DE4 - Transaction amount (n12)
	// * DE11 - System trace audit number (STAN) (n6)
	// * DE12 - Transaction date and time (n12 - YYMMDDhhmmss)
	// * DE14 - Expiration (n4 - YYMM)
	// * DE18 - Merchant type (n4) = 7523
	// * DE22 - Point of service data code (an12) = 200201200140
	// * DE24 - Function code (n3) = 200
	// * DE35 - Track 2 data (n..37)
	// * DE41 - Card Acceptor terminal id code (ans8) =
	// DGPT<spc><spc><spc><spc>,
	// for eg
	// * DE42 - Card Acceptor id code (ans15) = TESTDGPT0101<spc><spc><spc>, for
	// eg
	// * DE48_0 - Message control bitmap (b8) =
	// * 10000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000
	// * DE48_1 - Communications diagnostics (n4) = 0000
	private char[] createChargeRequest(Track2Card track2Card, String referenceNumber, int amountInCents) 
	{
		StringBuilder message = new StringBuilder(172);
		message.append(new String(eheader_));
		message.append(MTI_FINANCIAL_TRANSACTION_REQUEST);

		// primary bitmap for track 2 requests
		if (track2Card.isCardDataCreditCardTrack2Data()) 
		{
			message.append(SALE_BITMAP_TRACK_2);
		}

		// primary bitmap and DE2 for PAN requests
		else 
		{
			message.append(SALE_BITMAP_PAN_EXPIRATION);
			String pan_length = ZERO_STRING
			        + track2Card.getPrimaryAccountNumber().length();
			message.append(pan_length.substring(pan_length.length() - 2));
			message.append(track2Card.getPrimaryAccountNumber());
		}

		message.append(DE3_SALE_PROCESSING_CODE); // DE 3
		message.append(formatDollarAmount(amountInCents, 12)); // DE 4
		message.append(referenceNumber.substring(referenceNumber.length() - 6)); // DE11
		transDate_ = getTransactionDate();
		message.append(transDate_); // DE 12

		// DE 14 for PAN requests
		if (!track2Card.isCardDataCreditCardTrack2Data()) 
		{
			message.append(track2Card.getExpirationYear());
			message.append(track2Card.getExpirationMonth());
		}

		message.append("7523"); // DE 18
		if (track2Card.isCardDataCreditCardTrack2Data()) 
		{
			message.append(DE22_POS_SWIPED_DATA_CODE);
		} 
		else {
			message.append(DE22_POS_MANUAL_DATA_CODE);
		}
		message.append("200"); // DE 24

		// DE 35 for Track2 requests
		if (track2Card.isCardDataCreditCardTrack2Data()) 
		{
			String track_2_length = ZERO_STRING + track2Card.getDecryptedCardData().length();
			message.append(track_2_length.substring(track_2_length.length() - 2));
			message.append(track2Card.getDecryptedCardData());
		}

		message.append(padWithTrailingSpaces(getMerchantId(), DE41_FIELD_LENGTH));
		message.append(padWithTrailingSpaces(getTerminalId(), DE42_FIELD_LENGTH));
		message.append("012"); // DE 48
		message.append(DE48_BITMAP);
		message.append(DE48_1_COMMUNICATIONS_DIAGNOSTICS);

		// Add the message length to the e-header
		int message_length = message.length();
		char[] message_array = new char[message_length];
		message.getChars(0, message_length, message_array, 0);
		message_array[0] = (char) 0;
		message_array[1] = (char) (message_length);

		// log outgoing message
		String request = new String(message_array);
		if (track2Card.isCardDataCreditCardTrack2Data()) 
		{
			String log_message = request.replaceAll(track2Card.getDecryptedCardData(), track2Card.getDecryptedCardData().replaceAll("[^=]", "X"));
			log_message = CardProcessingUtil.replaceBinary(log_message);
			logger__.info("Charge request: " + log_message);
		} 
		else 
		{
			String pan = track2Card.getPrimaryAccountNumber();
			String log_message = request.replaceAll(pan, X_STRING.substring(0, pan.length()));
			log_message = CardProcessingUtil.replaceBinary(log_message);
			logger__.info("Charge request: " + log_message);
		}
	
		return message_array;
	}

	// * Sale expected message syntax for success response:
	// * MTI - Message type indicator = 1110
	// * Primary bitmap (b8) =
	// * 01110000 00110000 00000000 00000000 00000110 11000000 10000000 00000000
	// * DE2 - Primary account number (PAN) (LLVAR n..19)
	// * DE3 - Processing code (n6) = 003000
	// * DE4 - Transaction amount (n12)
	// * DE11 - System trace audit number (STAN) (n6)
	// * DE12 - Transaction date and time (n12 - YYMMDDhhmmss)
	// * DE38 - Approval code (anp6)
	// * DE39 - Action code (n3)
	// * DE41 - Card Acceptor terminal id code (ans8) - from original message
	// * DE42 - Card Acceptor id code (ans15) - echoed from original message
	// * DE49 - Transaction currency code (n3) = 840 (US dollars)

	private final static String PURCHASE_EXPECTED_BTTMAP_STARTS_WITH;
	static 
	{
		char[] purchase_expected_bitmap = { (char) 112, (char) 48, (char) 0,
		        (char) 0, (char) 6, (char) 192, (char) 128, (char) 0 };
		PURCHASE_EXPECTED_BTTMAP_STARTS_WITH = new String(
		        purchase_expected_bitmap, 0, 6);
	}

	private void extractSaleResponses(String response) throws CardTransactionException 
	{
		if (response.length() < 44) 
		{
			throw new CardTransactionException("Heartland response is too short, should be >= 44 characters but is only "
	                + response.length());
		}

		String transactionType = response.substring(32, 36); // message type
															 // identifier
		String primary_bitmap = response.substring(36, 44);

		// purchase response
		if (transactionType.equals(MTI_FINANCIAL_TRANSACTION_RESPONSE)
		        && primary_bitmap.startsWith(PURCHASE_EXPECTED_BTTMAP_STARTS_WITH)) 
		{
			int pan_length = Integer.parseInt(response.substring(44, 46));
			int array_position = 46 + pan_length + 18;

			String stan = response
			        .substring(array_position, array_position + 6); // DE11
			processorTransactionId_ = stan;
			array_position += 18;

			String approval_code = response.substring(array_position,
			        array_position + 6); // DE38
			authOrErrorCode_ = approval_code;
			array_position += 6;

			String action_code = response.substring(array_position,
			        array_position + 3); // DE39
			actionCode_ = action_code; // Value of 000 means success
		}

		// error response
		else if ((response.substring(2, 4)).equals("EH")) 
		{
			int start = response.indexOf(getMerchantId(), 32) - 3;
			authOrErrorCode_ = response.substring(start, start + 3);

			if (!authOrErrorCode_.equals("000")) 
			{
				// set action code so processor offline exception isn't thrown
				actionCode_ = authOrErrorCode_;
			} 
			else 
			{
				actionCode_ = "";
			}
		}

		else 
		{
			throw new CardTransactionException("HeartlandProcessor, extractSaleResponses, nothing to do!! response: " + response);
		}

		StringBuilder sb = new StringBuilder(200);
		sb.append("Charge Response:  ActionCode: ");
		sb.append(actionCode_);
		sb.append(", Auth/Error Code: ");
		sb.append(authOrErrorCode_);
		sb.append(", ProcessorTransactionId: ");
		sb.append(processorTransactionId_);
		logger__.info(sb.toString());
	}

	// * Refund request message syntax:
	// * Message Type Indicator (MTI) = 1220
	// * primary bitmap (b8) =
	// * 01110000 00110000 01000101 00000100 00000000 11000001 00000000 00000000
	// * DE2 - PAN
	// * DE3 - Processing code (n6) = 200030
	// * DE4 - Transaction amount (n12)
	// * DE11 - System trace audit number (STAN) (n6)
	// * DE12 - Transaction date and time (n12 - YYMMDDhhmmss)
	// * DE14 - Expiration date (n4 - YYMM)
	// * DE18 - Merchant type (n4) = 7523
	// * DE22 - Point of service data code (an12)
	// * DE24 - Function code (n3) = 200
	// * DE30 - Original amount (n24)
	// * DE41 - Card Acceptor terminal id code (ans8) =
	// DGPT<spc><spc><spc><spc>,
	// for eg
	// * DE42 - Card Acceptor id code (ans15) = TESTDGPT0101<spc><spc><spc>, for
	// eg
	// * DE48_0 - Message control bitmap (b8) =
	// * 10000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000
	// * DE48_1 - Communications diagnostics (n4) = 0000
	private char[] createRefundRequest(Track2Card track2Card, String referenceNumber, int origAmountInCents,
	        int refundAmountInCents) 
	{
		StringBuffer message = new StringBuffer(178);
		message.append(new String(eheader_));
		message.append(MTI_FINANCIAL_TRANSACTION_ADVICE_REQUEST); // MTI
		char[] refund_bitmap = { (char) 112, (char) 52, (char) 69, (char) 4,
		        (char) 0, (char) 193, (char) 0, (char) 0 };
		message.append(new String(refund_bitmap)); // Primary bitmap

		int card_data_length = track2Card.getPrimaryAccountNumber().length();
		if (card_data_length < 10 || card_data_length > 40) 
		{
			throw new IllegalArgumentException("Invalid card number length: "
			        + card_data_length);
		}
		String card_number_length = Integer.toString(card_data_length);

		message.append(card_number_length); // DE 2
		message.append(track2Card.getPrimaryAccountNumber());

		message.append(DE3_REFUND_PROCESSING_CODE); // DE 3
		message.append(formatDollarAmount(refundAmountInCents, 12)); // DE 4
		message.append(referenceNumber.substring(referenceNumber.length() - 6)); // DE
																				 // 11
		transDate_ = getTransactionDate();
		message.append(transDate_); // DE 12
		message.append(track2Card.getExpirationYear() + track2Card.getExpirationMonth()); // DE 14
		message.append("7523"); // DE 18
		message.append(DE22_POS_MANUAL_DATA_CODE);
		message.append("200"); // DE 24
		message.append(formatDollarAmount(origAmountInCents, 24)); // DE 30
		message.append(padWithTrailingSpaces(getMerchantId(), DE41_FIELD_LENGTH));
		message.append(padWithTrailingSpaces(getTerminalId(), DE42_FIELD_LENGTH));
		message.append("012"); // DE 48
		message.append(DE48_BITMAP);
		message.append(DE48_1_COMMUNICATIONS_DIAGNOSTICS);

		int message_length = message.length();
		char[] message_array = new char[message_length];
		message.getChars(0, message_length, message_array, 0);
		message_array[0] = (char) 0;
		message_array[1] = (char) (message_length);

		String request = new String(message_array);
		String pan = track2Card.getPrimaryAccountNumber();
		String log_message = request.replaceAll(pan,
		        X_STRING.substring(0, pan.length()));
		log_message = CardProcessingUtil.replaceBinary(log_message);
		logger__.info("Refund request: " + log_message);
		
		return message_array;
	}

	// * Refund expected message syntax for success response:
	// * MTI - Message type indicator = 1230
	// * Primary bitmap (b8) =
	// * 01110000 00110000 0000000 00000000 00000010 11000000 00000000 00000000
	// * DE2 - Primary account number (PAN) (LLVAR n..19)
	// * DE3 - Processing code (n6) = 003000
	// * DE4 - Transaction amount (n12)
	// * DE11 - System trace audit number (STAN) (n6)
	// * DE12 - Transaction date and time (n12 - YYMMDDhhmmss)
	// * DE39 - Action code (n3)
	// * DE41 - Card Acceptor terminal id code (ans8) - echoed from original
	// message
	// * DE42 - Card Acceptor id code (ans15) - echoed from original message
	private void extractRefundResponses(String heartland_response) throws CardTransactionException 
	{
		if (heartland_response.length() < 44) 
		{
			throw new CardTransactionException("Heartland response is too short, should be >= 44 characters but is only "
			                + heartland_response.length());
		}

		// message type identifier
		String transactionType = heartland_response.substring(32, 36);
		String primary_bitmap = heartland_response.substring(36, 44);

		char[] return_expected_bitmap = { (char) 112, (char) 48, (char) 0,
		        (char) 0, (char) 2, (char) 192, (char) 0, (char) 0 };
		char[] return_expected_bitmap_starts_with = { (char) 112, (char) 48,
		        (char) 0, (char) 0, (char) 2, (char) 192 };

		final char[] expectedReturnAuthBitmap = { (char) 112, (char) 48, (char) 0, (char) 0, (char) 6, (char) 192, (char) 128, (char) 4 };
		final char[] returnedPrimaryBitmap = primary_bitmap.toCharArray();

		if (transactionType.equals(MTI_FINANCIAL_TRANSACTION_ADVICE_RESPONSE)
		        && primary_bitmap.startsWith(new String(return_expected_bitmap_starts_with))) 
		{
			int pan_length = Integer.parseInt(heartland_response.substring(44,
			        46)); // DE2
			int array_position = 46 + pan_length + 18;

			String stan = heartland_response.substring(array_position, array_position + 6); // DE11
			processorTransactionId_ = stan;
			array_position += 18;

			String action_code = heartland_response.substring(array_position, array_position + 3); // DE39
			actionCode_ = action_code; // Value of 000 means success
			authOrErrorCode_ = "N/A";

			// If action code is not "000" then it contains the error code value
			if (!action_code.equals("000")) 
			{
				authOrErrorCode_ = actionCode_;
			}
		}
		else if (transactionType.equals(MTI_FINANCIAL_TRANSACTION_ADVICE_RESPONSE)
		        && Arrays.equals(expectedReturnAuthBitmap, returnedPrimaryBitmap)) 
		{
		    // Copy from above "if block" to minimize code changes.
            int pan_length = Integer.parseInt(heartland_response.substring(44,
                    46)); // DE2
            int array_position = 46 + pan_length + 18;

            String stan = heartland_response.substring(array_position, array_position + 6); // DE11
            processorTransactionId_ = stan;
            array_position += 24;

            String action_code = heartland_response.substring(array_position, array_position + 3); // DE39
            actionCode_ = action_code; // Value of 000 means success
            authOrErrorCode_ = "N/A";

            // If action code is not "000" then it contains the error code value
            if (!action_code.equals("000")) 
            {
                authOrErrorCode_ = actionCode_;
            }
		} 
		else if ((heartland_response.substring(2, 4)).equals("EH")) // error
																	// response
		{
			int start = heartland_response.indexOf(getMerchantId(), 32) - 3;
			authOrErrorCode_ = heartland_response.substring(start, start + 3);

			if (!authOrErrorCode_.equals("000")) 
			{
				// set auth code so processor offline exception isn't thrown
				actionCode_ = authOrErrorCode_;
			} 
			else 
			{
				actionCode_ = "";
			}
		} 
		else 
		{
			throw new CardTransactionException("HeartlandProcessor, extractRefundResponses, nothing to do!! heartland_response: "
			                + heartland_response);
		}

		StringBuilder sb = new StringBuilder(200);
		sb.append("Refund Response:  ActionCode: ");
		sb.append(actionCode_);
		sb.append(", Auth/Error Code: ");
		sb.append(authOrErrorCode_);
		sb.append(", ProcessorTransactionId: ");
		sb.append(processorTransactionId_);
		logger__.info(sb.toString());
	}

	// * Reversal request message syntax:
	// * Message Type Indicator (MTI) = 1420
	// * primary bitmap (b8) =
	// * 01110000 00110100 01000101 10000000 00000000 11000001 00000001 00000000
	// * DE2 - Primary account number (LLVAR n..19)
	// * DE3 - Processing code from original message (n6)
	// * DE4 - Original transaction amount (n12)
	// * DE11 - System trace audit number (STAN) (n6)
	// * DE12 - Transaction date and time (n12 - YYMMDDhhmmss)
	// * DE14 - Expiry date (n4 - YYMM)
	// * DE18 - Merchant type (n4) = 7523
	// * DE22 - Point of service data code (an12)
	// * DE24 - Function code (n3) = 400
	// * DE25 = Message reason code (n4) = 4021 (timeout waiting for response)
	// * DE41 - Card Acceptor terminal id code (ans8) = DGPT<spc><spc><spc><spc>
	// * DE42 - Card Acceptor id code (ans15) = TESTDGPT0101<spc><spc><spc>
	// * DE48_0 - Message control bitmap (b8) =
	// * 10000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000
	// * DE48_1 - Communications diagnostics (n4) = 0000
	// * DE56_1 - Original MTI (n4)
	// * DE56_2 - Original STAN (n6)
	// * DE56_3 - Original date and time,local transaction (n12)
	// * DE56_4 - Original acquiring institution ID code (LLVAR n..11)
	private char[] createReversalRequest(String newReferenceNumber, String originalMti, String originalProcessingCode,
	        int amountInCents, String originalReferenceNumber, String originalDate, Track2Card track2Card) 
	{
		StringBuffer message = new StringBuffer(170);

		message.append(new String(eheader_));
		message.append(MTI_REVERSAL_TRANSACTION_REQUEST); // MTI
		char[] reversal_bitmap = { (char) 112, (char) 52, (char) 69,
		        (char) 128, (char) 0, (char) 193, (char) 1, (char) 0 };
		message.append(new String(reversal_bitmap)); // Primary bitmap

		int card_data_length = track2Card.getPrimaryAccountNumber().length();
		if (card_data_length < 10 || card_data_length > 40) 
		{
			throw new IllegalArgumentException("Invalid card number length: "
			        + card_data_length);
		}
		String card_number_length = Integer.toString(card_data_length);

		message.append(card_number_length); // DE 2
		message.append(track2Card.getPrimaryAccountNumber());

		message.append(originalProcessingCode); // DE 3
		message.append(formatDollarAmount(amountInCents, 12)); // DE 4
		message.append(newReferenceNumber.substring(newReferenceNumber.length() - 6)); // DE
																					   // 11
		message.append(getTransactionDate()); // DE 12
		message.append(track2Card.getExpirationYear()
		        + track2Card.getExpirationMonth()); // DE 14
		message.append("7523"); // DE 18
		
        // POS 8583 Spec - "DE 22 should be the same between originals and non-originals" (P257).
        if (track2Card.isCardDataCreditCardTrack2Data() || track2Card.isCreditCard())
        {
            message.append(DE22_POS_SWIPED_DATA_CODE);
        }
        else
        {
            message.append(DE22_POS_MANUAL_DATA_CODE);
        }		
		
		message.append("400"); // DE 24
		message.append("4021"); // DE 25
		message.append(padWithTrailingSpaces(getMerchantId(), DE41_FIELD_LENGTH));
		message.append(padWithTrailingSpaces(getTerminalId(), DE42_FIELD_LENGTH));
		message.append("012"); // DE 48 - size of field
		message.append(DE48_BITMAP);
		message.append(DE48_1_COMMUNICATIONS_DIAGNOSTICS);
		message.append("24"); // DE 56 LLVAR
		message.append(originalMti); // DE 56.1
		message.append(originalReferenceNumber.substring(originalReferenceNumber.length() - 6)); // DE
		// 56.2
		message.append(originalDate); // DE 56.3
		message.append("00"); // DE 56.4 LLVAR

		int message_length = message.length();
		char[] message_array = new char[message_length];
		message.getChars(0, message_length, message_array, 0);
		message_array[0] = (char) 0;
		message_array[1] = (char) (message_length);

		String request = new String(message_array);
		String pan = track2Card.getPrimaryAccountNumber();
		String log_message = request.replaceAll(pan, X_STRING.substring(0, pan.length()));
		log_message = CardProcessingUtil.replaceBinary(log_message);
		logger__.info("Rversal request: " + log_message);
		
		return message_array;
	}

	// * Reversal expected message syntax for success response:
	// * MTI - Message type indicator = 1430
	// * Primary bitmap (b8) =
	// * 01110000 00110000 00000000 00000000 00000010 11000000 00000000 00000000
	// * DE2 - Primary account number (LLVAR n..19)
	// * DE3 - Processing code (n6) = 003000
	// * DE4 - Original transaction amount (n12)
	// * DE11 - System trace audit number (STAN) (n6)
	// * DE12 - Transaction date and time (n12 - YYMMDDhhmmss)
	// * DE39 - Action code (n3)
	// * DE41 - Card Acceptor terminal id code (ans8) = DGPT<spc><spc><spc><spc>
	// * DE42 - Card Acceptor id code (ans15) = TESTDGPT0101<spc><spc><spc>
	private void extractReversalResponses(String heartland_response) throws CardTransactionException 
	{
		if (heartland_response.length() < 44) 
		{
			throw new CardTransactionException(
			        "Heartland response is too short, should be >= 44 characters but is only " + heartland_response.length());
		}

		// message type identifier
		String transactionType = heartland_response.substring(32, 36);
		String primary_bitmap = heartland_response.substring(36, 44);

		char[] reversal_expected_bitmap = { (char) 112, (char) 48, (char) 0,
		        (char) 0, (char) 2, (char) 192, (char) 0, (char) 0 };
		char[] reversal_expected_bitmap_starts_with = { (char) 112, (char) 48,
		        (char) 0, (char) 0, (char) 2, (char) 192 };

		// reversal response
		if (transactionType.equals(MTI_REVERSAL_TRANSACTION_RESPONSE)
		        && primary_bitmap.startsWith(new String(reversal_expected_bitmap_starts_with))) 
		{
			int start = heartland_response.indexOf(getMerchantId(), 32) - 3;
			authOrErrorCode_ = heartland_response.substring(start, start + 3);
			// set auth code so processor offline exception isn't thrown
			actionCode_ = authOrErrorCode_;
		}

		else if ((heartland_response.substring(2, 4)).equals("EH")) // error
																	// response
		{
			int start = heartland_response.indexOf(getMerchantId(), 32) - 3;
			authOrErrorCode_ = heartland_response.substring(start, start + 3);

			if (!authOrErrorCode_.equals("000")) 
			{
				// set auth code so processor offline exception isn't thrown
				actionCode_ = authOrErrorCode_;
			} 
			else 
			{
				actionCode_ = "";
			}
		} 
		else {
			throw new CardTransactionException("HeartlandProcessor, extractReversalResponses, nothing to do!! heartland_response: "
			                + heartland_response);
		}

		StringBuilder sb = new StringBuilder(200);
		sb.append("Reversal Response:  ActionCode: ");
		sb.append(actionCode_);
		sb.append(", Auth/Error Code: ");
		sb.append(authOrErrorCode_);
		sb.append(", ProcessorTransactionId: ");
		sb.append(processorTransactionId_);
		logger__.info(sb.toString());
	}

	// function to reverse a request previously sent to the ADS server via
	// Datawire.
	public void processReversal(Reversal reversal, Track2Card track2Card) throws CardTransactionException 
	{
		// Get next reference number for this merchant account
		String reference_number = getCommonProcessingService().getNewReferenceNumber(getMerchantAccount());

		// Create the reversal request
		char[] request = createReversalRequest(reference_number,
		        reversal.getOriginalMessageType(),
		        reversal.getOriginalProcessingCode(), reversal
		                .getOriginalTransactionAmountInCents(),
		        reversal.getOriginalReferenceNumber(),
		        reversal.getOriginalTime(), track2Card);

		// Send the reversal request
		String response = new String(sendRequest(request, reference_number));

		// Extract data from the response
		String pan = track2Card.getPrimaryAccountNumber();
	
		String log_message = response.replaceAll(pan,
		        X_STRING.substring(0, pan.length()));
		log_message = CardProcessingUtil.replaceBinary(log_message);
		logger__.info("Reversal response: " + log_message);
	

		extractReversalResponses(response);

		reversal.setLastRetryTime(new Date());
		reversal.setLastResponseCode(authOrErrorCode_);

		// Check for return code for valid reversal response (4XX)
		if (null != authOrErrorCode_ && authOrErrorCode_.substring(0, 1).equals("4")) 
		{
			reversal.setIsSucceeded(true);
			reversal.incrementRetryAttempts();
		} 
		else 
		{
			reversal.incrementRetryAttempts();
		}
	}

	private int mapErrorResponse(String responseCode) throws CardTransactionException 
	{
		// place call
		if ("CALLME".equals(responseCode.trim())) 
		{
			return (DPTResponseCodesMapping.CC_PLACE_CALL_VAL__7022);
		}

		int response_code = -1;
		try 
		{
			response_code = Integer.parseInt(responseCode);
		} 
		catch (Exception e) 
		{
			throw new CardTransactionException("Expecting numeric response, but recieved: " + responseCode, e);
		}

		switch (response_code) {
    		// do not honor - no card pickup
    		case 100:
    			// suspected fraud - no card pickup
    		case 102:
    			// restricted card - no card pickup
    		case 104:
    			// transaction not permitted to cardholder - no card pickup
    		case 119:
    			// security violation - no card pickup
    		case 122:
    			// exceeds withdrawal frequency limit - no card pickup
    		case 123:
    			// do not honor - card pickup
    		case 200:
    			// suspected fraud - card pickup
    		case 202:
    			// restricted card - card pickup
    		case 204:
    			// suspected counterfeit card - card pickup
    		case 210:
    			return (DPTResponseCodesMapping.CC_DECLINED_VAL__7003);
    
    			// expired card - no card pickup
    		case 101:
    			// expired card - card pickup
    		case 201:
    			return (DPTResponseCodesMapping.CC_EXPIRED_VAL__7001);
    
    			// contact aquirer - no card pickup
    		case 103:
    			// refer to card issuer - no card pickup
    		case 107:
    			// refer to card issue special conditions - no card pickup
    		case 108:
    			// card acceptor contact acquirer - card pickup - no card pickup
    		case 203:
    			// card acceptor call acquirer's security department - card pickup
    		case 205:
    			return (DPTResponseCodesMapping.CC_PLACE_CALL_VAL__7022);
    
    			// invalid merchant - no card pickup
    		case 109:
    			// not on file - no card pickup
    		case 180:
    			throw new CardTransactionException(
    			        "Response indicates invalid merchant: " + responseCode);
    
    			// invalid card number - no card pickup
    		case 111:
    			return (DPTResponseCodesMapping.CC_BAD_CREDIT_CARD_DATA_VAL__7045);
    
    			// invalid card type - no card pickup
    		case 118:
    			// card not effective - no card pickup
    		case 125:
    			return (DPTResponseCodesMapping.CC_CARD_TYPE_NOT_SETUP_VAL__7076);
    
    			// insufficient funds - no card pickup
    		case 116:
    			// exceeds withdrawal amount limit - no card pickup
    		case 121:
    			return (DPTResponseCodesMapping.CC_INSUFFICIENT_FUNDS_VAL__7017);
    
    			// allowable PIN retries exceeded - no card pickup
    		case 106:
    			// unacceptable fee - no card pickup
    		case 112:
    			// incorrect PIN - no card pickup
    		case 117:
    			// invalid PIN block - no card pickup
    		case 126:
    			// PIN length error - no card pickup
    		case 127:
    			// PIN key synch error - no card pickup
    		case 128:
    			// allowable PIN retries exceeded - card pickup
    		case 206:
    			return (DPTResponseCodesMapping.CC_UNABLE_TO_AUTHENTICATE_VAL__7055);
    
    			// no account of type requested - no card pickup
    		case 114:
    			return (DPTResponseCodesMapping.CC_INVALID_ACCOUNT_VAL__7088);
    
    			// requested function not supported - no card pickup
    		case 115:
    			// transaction not permitted to terminal - no card pickup
    		case 120:
    			return (DPTResponseCodesMapping.CC_INVALID_REQUEST_VAL__7058);
    
    			// lost card - card pickup
    		case 208:
    			// stolen card - card pickup
    		case 209:
    			return (DPTResponseCodesMapping.CC_LOST_STOLEN_VAL__7006);
    
    			// invalid amount - no card pickup
    		case 110:
    			return (DPTResponseCodesMapping.CC_INVALID_AMOUNT_VAL__7033);
    
    			// transaction already adjusted
    		case 181:
    			// target not found - no card pickup
    		case 182:
    			return (DPTResponseCodesMapping.CC_UNABLE_TO_PROCESS_VAL__7084);
    			
            case 904:
                return (DPTResponseCodesMapping.CC_FORMAT_ERROR_VAL_7099);    			
		}

		throw new CardTransactionException("Unknown Response Code: " + responseCode);
	}

	// Gets the transaction (current) date in yyMMddHHmmss format as required
	// for 8583 messages.
	private String getTransactionDate() 
	{
		SimpleDateFormat date_format = new SimpleDateFormat("yyMMddHHmmss");
		date_format.setTimeZone(TimeZone.getTimeZone(getTimeZone()));
		String transaction_date = date_format.format(getCustomerTimeZoneCurrentDate());
		
		return transaction_date;
	}

	// This function creates a string <fieldLength> characters long, padded with
	// trailing spaces if necessary.
	private String padWithTrailingSpaces(String stringToPad, int fieldLength) 
	{
		for (int i = stringToPad.length(); i < fieldLength; i++) 
		{
			stringToPad += " ";
		}
		return (stringToPad);
	}

	protected char[] sendRequest(char[] message, String referenceNumber) throws CardTransactionException {
		StringBuilder sb = new StringBuilder();
		String gateway = null;
		SSLSocket socket = null;
		OutputStream os = null;
		InputStream is = null;
		int port = 0;

		try 
		{
			URL url = new URL(destinationUrlString);
			gateway = url.getHost();
			port = url.getPort();
			socket = (SSLSocket) SSLSocketFactory.getDefault().createSocket();
			socket.connect(new InetSocketAddress(gateway, port));
			socket.startHandshake();
			boolean hostNameValid = new StrictHostnameVerifier().verify(
			        gateway, socket.getSession());
			if (!hostNameValid) 
			{
				// alert emsadmin for potential attack
				String errStr = "Host name verification failed.\nSSL Cert: "
				        + socket.getSession().getPeerPrincipal().getName() + "\nHost name: " + gateway;
				getCommonProcessingService().sendCardProcessingAdminErrorAlert("Host name verification error", errStr, null);
				throw new CardTransactionException(errStr);
			}
			os = socket.getOutputStream();
			is = socket.getInputStream();

			byte[] bytes = new String(message).getBytes(MESSAGE_ENCODE_CHARSET);
			os.write(bytes);
			os.flush();

			byte[] respBytes = new byte[100];
			int len = is.read(respBytes);
			while (len > 0) 
			{
				sb.append(new String(respBytes, 0, len, MESSAGE_ENCODE_CHARSET));
				if (is.available() <= 0) 
				{
					break;
				} 
				else 
				{
					len = is.read(respBytes);
				}
			}
			return sb.toString().toCharArray();
		} 
		catch (UnsupportedEncodingException e) 
		{
			throw new CardTransactionException("Character set " + MESSAGE_ENCODE_CHARSET + " not supported.", e);
		} 
		catch (IOException e) 
		{
			throw new CardTransactionException("Cannont connect to " + gateway + " port " + port, e);
		} 
		finally 
		{
			try 
			{
				if (os != null) 
				{
					os.close();
				}
				if (is != null) 
				{
					is.close();
				}
				if (socket != null) 
				{
					socket.close();
				}
			} 
			catch (IOException e) 
			{
				logger__.error("Unable to close connection.", e);
			}
		}
	}

	// copied from DatawireProcessor
	protected String formatDollarAmount(int amountInCents, int fieldLength) 
	{
		String amount = Integer.toString(amountInCents);

		String leading_zeros = "";
		for (int i = 0; i < fieldLength; i++) 
		{
			leading_zeros += "0";
		}

		// Add some leading zeros so that the field length is fieldLength
		String formatted_dollar_amount = leading_zeros.substring(amount.trim().length()) + amount;

		return formatted_dollar_amount;
	}

    private Date getCustomerTimeZoneCurrentDate()
    {
        GregorianCalendar cal = new GregorianCalendar(TimeZone.getTimeZone(getTimeZone()));
        return cal.getTime();
    }	
	
	public String getMerchantId() 
	{
		return merchantId;
	}

	public void setMerchantId(String merchantId) 
	{
		this.merchantId = merchantId;
	}

	public String getTerminalId() 
	{
		return terminalId;
	}

	public void setTerminalId(String terminalId) 
	{
		this.terminalId = terminalId;
	}

	@Override
	public boolean isReversalExpired(Reversal reversal) {
		return (reversal.getRetryAttempts() >= EMS_MAX_REVERSAL_RETRIES);
	}
}
