/* 
 * Copyright (c) 1997 - 2009  DPT  All rights reserved.
 *
 * Created on May 25, 2009 by rexz
 * 
 * $Header:  $
 * 
 * $Log:  $ 
 *
 */

package com.digitalpaytech.cardprocessing.impl;

import java.util.HashMap;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import com.digitalpaytech.util.iso8583.Iso8583DataType;

/**
 * FirstDataResponseParser, which parses char[] response to HashMap
 * 
 * @author rexz
 * @since
 * 
 */
public class FirstDataResponseParser
{
	private final static Logger logger = Logger.getLogger(FirstDataResponseParser.class);
	private final static int BIT_EXTENDED_BITMAP = 1; // the bit telling whether extended bitmap is included
	private final static int LENGTH_MTI = 2; // length of MTI in bytes
	private final static int LENGTH_BITMAP = 8; // length of bitmap in bytes
	private Iso8583DataType[] fdTypeMap;

	public FirstDataResponseParser(Iso8583DataType[] typeMap)
	{
		this.fdTypeMap = typeMap;
	}

	/**
	 * Extract all fields from the response.
	 * 
	 * @param response
	 * @return
	 */
	public HashMap<Integer, String> parse(char[] response)
	{
		HashMap<Integer, String> fieldMap = new HashMap<Integer, String>();

		// extract bitmap
		SortedSet<Integer> bitmap = extractBitmap(response);

		// start extracting after MTI and bitmap
		int nextIndex = LENGTH_MTI + LENGTH_BITMAP;

		if(bitmap.contains(BIT_EXTENDED_BITMAP))
		{
			// secondary bitmap present
			nextIndex = nextIndex + LENGTH_BITMAP;

			// if we need to support bits > 64, add the bits to the bitmap set.
			// bitmap = extractSecondaryBitmap(bitmap, message);
		}

		if(logger.isDebugEnabled())
		{
			logger.debug("Extracting bits: " + bitmap);
		}

		// extract all bits
		for (int i : bitmap)
		{
			if (fdTypeMap[i] != null)
			{
				nextIndex = fdTypeMap[i].decodeValue(response, nextIndex, fieldMap);
			}
			else
			{
				// log error and stop traversal
				logger.error("Unable to decode message. Handler for type " + i + " is undefined.");
				break;
			}
		}
		return fieldMap;
	}

	// extract the bitmap from the message
	private SortedSet<Integer> extractBitmap(char[] message)
	{
		TreeSet<Integer> keySet = new TreeSet<Integer>();

		// we only support the first 64 bytes for now
		char[] bitmap = {message[2], message[3], message[4], message[5], message[6], message[7], message[8],
				message[9]};

		// for each byte
		for (int i = 0; i < bitmap.length; i++)
		{
			// check each bit
			for (int k = 0x80, j = 1; k > 0; k >>= 1, j++)
			{
				if ((bitmap[i] & k) != 0)
				{
					keySet.add(8 * i + j);
				}
			}
		}

		return keySet;
	}
}
