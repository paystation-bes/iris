package com.digitalpaytech.cardprocessing.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.cardprocessing.CardProcessingManager;
import com.digitalpaytech.cardprocessing.CardProcessor;
import com.digitalpaytech.cardprocessing.CardProcessorFactory;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.dto.ProcessorInfo;
import com.digitalpaytech.exception.CardProcessorPausedException;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.exception.ProcessorAuthenticationException;
import com.digitalpaytech.service.CardTypeService;
import com.digitalpaytech.service.CcFailLogService;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.PreAuthService;
import com.digitalpaytech.service.ProcessorService;
import com.digitalpaytech.service.ProcessorTransactionService;
import com.digitalpaytech.service.ProcessorTransactionTypeService;
import com.digitalpaytech.service.CreditCardTestNumberService;
import com.digitalpaytech.service.WebWidgetHelperService;
import com.digitalpaytech.service.cps.CoreCPSService;
import com.digitalpaytech.service.kpi.KPIListenerService;
import com.digitalpaytech.service.processor.ElavonViaConexService;
import com.digitalpaytech.service.processor.TDMerchantCommunicationService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.SystemAdminUtil;

@Service("cardProcessorFactory")
@Transactional(propagation = Propagation.REQUIRED)
public class CardProcessorFactoryImpl implements CardProcessorFactory, ApplicationContextAware {
    
    private static Logger log = Logger.getLogger(CardProcessorFactoryImpl.class);
    
    ApplicationContext applicationContext;
    
    @Autowired
    private CommonProcessingService commonProcessingService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private CustomerCardTypeService customerCardTypeService;
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    @Autowired
    private CardProcessingManager cardProcessingManager;
    
    // Injects WebWidgetHelperService for method 'getCustomerTimeZoneByCustomerId'.
    @Autowired
    private WebWidgetHelperService webWidgetHelperService;
    
    @Autowired
    private ProcessorService processorService;
    
    @Autowired
    private ProcessorTransactionTypeService processorTransactionTypeService;
    
    @Autowired
    private ElavonViaConexService elavonViaConexService;
    
    @Autowired
    private TDMerchantCommunicationService tdMerchantCommunicationService;
    
    @Autowired
    private PreAuthService preAuthService;
    
    @Autowired
    private CoreCPSService coreCPSService;
    
    @Autowired
    private MessageHelper messages;
    
    @Autowired
    private KPIListenerService kpiListenerService;
    
    @Autowired
    private ProcessorTransactionService processorTransactionService;
    
    @Autowired
    private CardTypeService cardTypeService;
    
    @Autowired
    private MailerService mailerService;
    
    @Autowired
    private CcFailLogService ccFailLogService;
    
    @Autowired
    private CreditCardTestNumberService testCreditCardNumberService;
    
    private Map<Integer, ProcessorInfo> processorsMap;
    private Set<Integer> processorIds;
    private Set<ProcessorInfo> creditCardProcessor;
    private ProcessorInfo emvProcessorInfo;
    
    /**
     * @return CardProcessor return 'null' if:
     *         - merchantAccount is null.
     *         - processorId is invalid.
     *         - processor is paused processing request.
     */
    @Transactional(propagation = Propagation.SUPPORTS, noRollbackFor = { ProcessorAuthenticationException.class })
    public final CardProcessor getCardProcessor(final MerchantAccount merchantAccount, final PointOfSale pointOfSale)
        throws CardProcessorPausedException {
        return getCardProcessor(merchantAccount, pointOfSale, false);
    }
    
    /**
     * @param isForTestCharge
     *            boolean Is for testing processor connectivity.
     * @return CardProcessor return null if no corresponding Processor Id.
     * @throws CardProcessorPausedException
     *             Processor is paused
     * @throws ProcessorAuthenticationException
     *             If 'isForTestCharge' is true and couldn't instantiate the processor.
     *             For Alliance and First Data Nashville, this exception is thrown when self-registration failed.
     */
    @Transactional(propagation = Propagation.SUPPORTS, noRollbackFor = { ProcessorAuthenticationException.class })
    public final CardProcessor getCardProcessor(final MerchantAccount merchantAccount, final PointOfSale pointOfSale, final boolean isForTestCharge)
        throws CardProcessorPausedException {
        
        if (merchantAccount == null) {
            return null;
        }
        
        // for Link Post-Auth 
        if (CardProcessingUtil.isNotNullAndIsLink(merchantAccount)) {
            return getLinkCPSProcessor(merchantAccount, pointOfSale);
        }
        
        final ProcessorInfo procInfo = this.processorsMap.get(merchantAccount.getProcessor().getId());
        if (procInfo == null) {
            return null;
        }
        if (isProcessorPaused(procInfo.getId())) {
            throw new CardProcessorPausedException();
        }
        
        CardProcessor processor = null;
        switch (procInfo.getId()) {
            case CardProcessingConstants.PROCESSOR_ID_FIRST_DATA_CONCORD:
                processor = new ConcordProcessor(merchantAccount, this.commonProcessingService, this.emsPropertiesService);
                break;
            
            case CardProcessingConstants.PROCESSOR_ID_MONERIS:
                processor =
                        new MonerisProcessor(merchantAccount, this.commonProcessingService, this.emsPropertiesService, this.customerCardTypeService);
                break;
            
            case CardProcessingConstants.PROCESSOR_ID_PAYMENTECH:
                processor = new PaymentechProcessor(merchantAccount, this.commonProcessingService, this.emsPropertiesService,
                        this.customerCardTypeService);
                break;
            
            case CardProcessingConstants.PROCESSOR_ID_FIRST_HORIZON:
                processor = new FirstHorizonProcessor(merchantAccount, this.commonProcessingService, this.emsPropertiesService);
                break;
            
            case CardProcessingConstants.PROCESSOR_ID_ALLIANCE:
                try {
                    // The constructor calls 'selfRegisterMerchant' method which might throw CardTransactionException.
                    processor = new AllianceProcessor(merchantAccount, this.commonProcessingService, this.emsPropertiesService,
                            this.customerCardTypeService, this.merchantAccountService);
                } catch (CardTransactionException e) {
                    // Don't throw ProcessorAuthenticationException when this method is called for card processing.
                    if (isForTestCharge) {
                        throw new ProcessorAuthenticationException(e.getMessage(), e.getCause());
                    }
                }
                break;
            
            case CardProcessingConstants.PROCESSOR_ID_HEARTLAND:
                processor = new HeartlandProcessor(merchantAccount, this.commonProcessingService, this.emsPropertiesService,
                        this.customerCardTypeService);
                break;
            
            case CardProcessingConstants.PROCESSOR_ID_PARADATA:
                processor =
                        new ParadataProcessor(merchantAccount, this.commonProcessingService, this.emsPropertiesService, this.customerCardTypeService);
                break;
            
            case CardProcessingConstants.PROCESSOR_ID_AUTHORIZE_NET:
                processor = new AuthorizeNetProcessor(merchantAccount, this.commonProcessingService, this.emsPropertiesService);
                break;
            
            case CardProcessingConstants.PROCESSOR_ID_BLACK_BOARD:
                processor = new BlackBoardProcessor(merchantAccount, this.commonProcessingService, this.emsPropertiesService, pointOfSale);
                break;
            
            case CardProcessingConstants.PROCESSOR_ID_TOTAL_CARD:
                final String tz = getCustomerTimeZoneByCustomerId(merchantAccount.getCustomer().getId());
                processor = new TotalCardProcessor(merchantAccount, this.commonProcessingService, this.emsPropertiesService, tz);
                break;
            
            case CardProcessingConstants.PROCESSOR_ID_CBORD:
                final String timeZone = getCustomerTimeZoneByCustomerId(merchantAccount.getCustomer().getId());
                processor = new CBordGoldProcessor(merchantAccount, this.commonProcessingService, this.emsPropertiesService, pointOfSale, timeZone);
                break;
            
            case CardProcessingConstants.PROCESSOR_ID_CBORD_ODYSSEY:
                final String timeZoneOdyssey = getCustomerTimeZoneByCustomerId(merchantAccount.getCustomer().getId());
                processor = new CBordOdysseyProcessor(merchantAccount, this.commonProcessingService, this.emsPropertiesService, pointOfSale,
                        timeZoneOdyssey);
                break;
            
            case CardProcessingConstants.PROCESSOR_ID_NU_VISION:
                processor = new NuVisionProcessor(merchantAccount, this.commonProcessingService, this.emsPropertiesService, pointOfSale);
                break;
            
            case CardProcessingConstants.PROCESSOR_ID_FIRST_DATA_NASHVILLE:
                try {
                    // The constructor calls 'selfRegisterMerchant' method which might throw CardTransactionException.
                    processor = new FirstDataNashvilleProcessor(merchantAccount, this.commonProcessingService, this.emsPropertiesService,
                            this.merchantAccountService, this.customerCardTypeService);
                } catch (CardTransactionException e) {
                    // Don't throw ProcessorAuthenticationException when this method is called for card processing.
                    if (isForTestCharge) {
                        throw new ProcessorAuthenticationException(e.getMessage(), e.getCause());
                    }
                }
                break;
            
            case CardProcessingConstants.PROCESSOR_ID_LINK2GOV:
                processor = new Link2GovProcessor(merchantAccount, this.commonProcessingService, this.emsPropertiesService);
                break;
            
            case CardProcessingConstants.PROCESSOR_ID_ELAVON:
                processor = new ElavonProcessor(merchantAccount, this.commonProcessingService, this.emsPropertiesService,
                        this.customerCardTypeService, this.merchantAccountService, this.testCreditCardNumberService);
                break;
            
            case CardProcessingConstants.PROCESSOR_ID_ELAVON_VIACONEX:
                processor = new ElavonViaConexProcessor(merchantAccount, this.commonProcessingService, this.emsPropertiesService,
                        this.customerCardTypeService, this.merchantAccountService, this.elavonViaConexService, this.messages,
                        this.testCreditCardNumberService);
                break;
            
            case CardProcessingConstants.PROCESSOR_ID_HEARTLAND_SPPLUS:
                processor = new HeartlandSpPlusProcessor(merchantAccount, this.commonProcessingService, this.emsPropertiesService,
                        this.customerCardTypeService, pointOfSale);
                
                ((HeartlandSpPlusProcessor) processor).setPreAuthService(this.preAuthService);
                ((HeartlandSpPlusProcessor) processor).setProcessorTransactionTypeService(this.processorTransactionTypeService);
                break;
            
            case CardProcessingConstants.PROCESSOR_ID_TD_MERCHANT:
                processor = new TDMerchantProcessor(merchantAccount, this.commonProcessingService, this.emsPropertiesService,
                        this.tdMerchantCommunicationService, this.customerCardTypeService, this.testCreditCardNumberService);
                break;
            default:
                break;
        }
        
        // Problem, there is no CardProcessor.
        if (processor == null) {
            return null;
        }
        
        processor.setTimeZone(getCustomerTimeZoneByCustomerId(merchantAccount.getCustomer().getId()));
        processor.setCardProcessingManager(this.cardProcessingManager);
        
        if (log.isInfoEnabled()) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("CardProcessorFactory, getCardProcessor, processor is: ").append(processor.getClass().getName()).append(", TimeZone: ")
                    .append(processor.getTimeZone());
            log.info(bdr.toString());
        }
        
        return processor;
    }
    
    /**
     * @param customerId
     * @return String Default timezone is GMT.
     */
    private String getCustomerTimeZoneByCustomerId(final int customerId) {
        return this.webWidgetHelperService.getCustomerTimeZoneByCustomerId(customerId);
    }
    
    /**
     * Loads processors.properties into java.util.Map.
     */
    @PostConstruct
    private void loadProcessors() {
        final List<Processor> processors = this.processorService.findAll();
        this.processorsMap = new HashMap<Integer, ProcessorInfo>(processors.size());
        this.creditCardProcessor = new HashSet<ProcessorInfo>();
        for (Processor proc : processors) {
            final ProcessorInfo info = new ProcessorInfo(proc, this.messages.getMessage(proc.getName()),
                    this.messages.getMessage(proc.getName() + ".formname"), this.messages.getMessage(proc.getName() + ".alias"), proc.getIsLink());
            this.processorsMap.put(proc.getId(), info);
            if (SystemAdminUtil.isCreditCardProcessor(proc)) {
                this.creditCardProcessor.add(info);
            } else if (proc.getIsEMV()) {
                this.emvProcessorInfo = info;
            }
        }
        
        this.creditCardProcessor = Collections.unmodifiableSet(this.creditCardProcessor);
        this.processorIds = Collections.unmodifiableSet(this.processorsMap.keySet());
        
        final StringBuilder bdr = new StringBuilder();
        bdr.append("CardProcessorFactory, processorsMap is loaded, size: ").append(this.processorsMap.size());
        bdr.append(", processorIds size is: ").append(this.processorIds.size());
        log.info(bdr.toString());
        bdr.replace(0, bdr.length(), StandardConstants.STRING_EMPTY_STRING);
        bdr.append("CreditCardProcessor set size is: ").append(this.creditCardProcessor.size());
        bdr.append(", emvProcessorInfo is: ").append(this.emvProcessorInfo);
        log.info(bdr.toString());
    }
    
    /**
     * Resume/activate 'getCardProcessor' method.
     * 
     * @param processorId
     *            database table Processor, column Id.
     * @return boolean 'false' if it's an invalid processor id.
     */
    @Transactional(propagation = Propagation.SUPPORTS)
    public final boolean resumeProcessor(final int processorId) {
        return setPauseOrResumeProcessor(processorId, false);
    }
    
    /**
     * Pause/deactivate 'getCardProcessor' method which will return 'null'.
     * 
     * @param processorId
     *            database table Processor, column Id.
     * @return boolean 'false' if it's an invalid processor id.
     */
    @Transactional(propagation = Propagation.SUPPORTS)
    public final boolean pauseProcessor(final int processorId) {
        return setPauseOrResumeProcessor(processorId, true);
    }
    
    private boolean setPauseOrResumeProcessor(final int processorId, final boolean pauseFlag) {
        if (this.processorsMap.get(processorId) == null) {
            return false;
        }
        final Processor pro = this.processorService.findProcessor(processorId);
        if (pro != null && !pro.getIsLink()) {
            pro.setIsPaused(pauseFlag);
            this.processorService.updateProcessor(pro);
            return true;
        }
        return false;
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Set<ProcessorInfo> getCreditCardProcessorInfo() {
        return this.creditCardProcessor;
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Map<Integer, ProcessorInfo> getEMVProcessorInfo() {
        if (this.emvProcessorInfo == null) {
            return new HashMap<Integer, ProcessorInfo>();
        }
        final Map<Integer, ProcessorInfo> emvMap = new HashMap<Integer, ProcessorInfo>(1);
        emvMap.put(this.emvProcessorInfo.getId(), this.emvProcessorInfo);
        return emvMap;
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public final ProcessorInfo getProcessorInfo(final Integer processorId) {
        return this.processorsMap.get(processorId);
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Set<Integer> getProcessorIds() {
        return this.processorIds;
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public final boolean isProcessorPaused(final Integer processorId) {
        final Processor pro = this.processorService.findProcessor(processorId);
        if (pro != null && pro.isIsPaused()) {
            return true;
        }
        return false;
    }
    
    public final void setCommonProcessingService(final CommonProcessingService commonProcessingService) {
        this.commonProcessingService = commonProcessingService;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final void setCustomerCardTypeService(final CustomerCardTypeService customerCardTypeService) {
        this.customerCardTypeService = customerCardTypeService;
    }
    
    public final MerchantAccountService getMerchantAccountService() {
        return this.merchantAccountService;
    }
    
    public final void setMerchantAccountService(final MerchantAccountService merchantAccountService) {
        this.merchantAccountService = merchantAccountService;
    }
    
    public final void setCardProcessingManager(final CardProcessingManager cardProcessingManager) {
        this.cardProcessingManager = cardProcessingManager;
    }
    
    public final void setWebWidgetHelperService(final WebWidgetHelperService webWidgetHelperService) {
        this.webWidgetHelperService = webWidgetHelperService;
    }
    
    public final void setProcessorService(final ProcessorService processorService) {
        this.processorService = processorService;
    }
    
    public final void setElavonViaConexService(final ElavonViaConexService elavonViaConexService) {
        this.elavonViaConexService = elavonViaConexService;
    }
    
    public final void setPreAuthService(final PreAuthService preAuthService) {
        this.preAuthService = preAuthService;
    }
    
    public final void setProcessorTransactionTypeService(final ProcessorTransactionTypeService processorTransactionTypeService) {
        this.processorTransactionTypeService = processorTransactionTypeService;
    }
    
    // -----------------------------------------------------------------
    // ---------------------- For JUnit test ONLY ----------------------
    public final void setProcessorsMap(final Map<Integer, ProcessorInfo> processorsMap) {
        this.processorsMap = processorsMap;
    }
    
    public final void setCreditCardProcessor(final Set<ProcessorInfo> creditCardProcessor) {
        this.creditCardProcessor = creditCardProcessor;
    }
    
    public final void setKpiListenerService(final KPIListenerService kpiListenerService) {
        this.kpiListenerService = kpiListenerService;
    }
    
    public final void setProcessorTransactionService(final ProcessorTransactionService processorTransactionService) {
        this.processorTransactionService = processorTransactionService;
    }
    
    public final void setCardTypeService(final CardTypeService cardTypeService) {
        this.cardTypeService = cardTypeService;
    }
    
    public final void setMailerService(final MailerService mailerService) {
        this.mailerService = mailerService;
    }
    
    public final void setCcFailLogService(final CcFailLogService ccFailLogService) {
        this.ccFailLogService = ccFailLogService;
    }
    
    @Override
    public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
    
    @Override
    public LinkCPSProcessor getLinkCPSProcessor(final MerchantAccount merchantAccount, final PointOfSale pointOfSale) {
        final LinkCPSProcessor proc =
                new LinkCPSProcessor(merchantAccount, this.commonProcessingService, this.emsPropertiesService, pointOfSale, this.coreCPSService);
        proc.setServices(this.kpiListenerService, this.processorTransactionService, this.cardTypeService, this.mailerService,
                         this.processorTransactionTypeService, this.ccFailLogService);
        return proc;
    }
}
