/* 
 * Copyright (c) 1997 - 2009  DPT  All rights reserved.
 *
 * Created on May 25, 2009 by rexz
 * 
 * $Header:  $
 * 
 * $Log:  $ 
 *
 */

package com.digitalpaytech.cardprocessing.impl;

import java.util.Iterator;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.digitalpaytech.util.iso8583.Iso8583ProtocolFactory;
import com.digitalpaytech.util.iso8583.FixLengthBcdType;
import com.digitalpaytech.util.iso8583.Iso8583DataType;
import com.digitalpaytech.util.CardProcessingUtil;

/**
 * FirstDataRequestBuilder, which builds request char[] based on passed in TreeMap bits data
 * 
 * @author rexz
 * @since
 * 
 */
public class FirstDataRequestBuilder
{
	private final static Logger logger = Logger.getLogger(FirstDataRequestBuilder.class);
	private Iso8583DataType[] fdTypeMap;

	public FirstDataRequestBuilder(Iso8583DataType[] typeMap)
	{
		this.fdTypeMap = typeMap;
	}

	public char[] build(TreeMap<Integer, String> bitsData)
	{
		StringBuffer sb = new StringBuffer();
		Iterator<Integer> it = bitsData.keySet().iterator();
		int key;
		// message type id
		char[] chars = fdTypeMap[0].encodeValue((String)bitsData.get(0));
		sb.append(chars);
		// build primary bitmap
		long bitmap = 0l;
		while (it.hasNext())
		{
			key = it.next();
			if (key != 0 && key != 1) // ignore Message Type
			{
				bitmap = bitmap | (1l << (Iso8583ProtocolFactory.BITS_64 - key));
			}
		}
		logger.debug("++++ Request bitmap: " + Long.toHexString(bitmap));
		FixLengthBcdType t = new FixLengthBcdType(0, 8);
		sb.append(t.encodeValue(Long.toHexString(bitmap)));

		it = bitsData.keySet().iterator();
		while (it.hasNext())
		{
			key = it.next();
			if (key != 0 && key != 1)
			{
				chars = fdTypeMap[key].encodeValue((String)bitsData.get(key));
				if (key == 63)
				{
					StringBuffer tmp = new StringBuffer();
					tmp.append(chars);
					logger.debug("+++++ bit 63 (hex): " + CardProcessingUtil.toHexString(tmp.toString()));
				}
				sb.append(chars);
			}
		}
		String log_message = CardProcessingUtil.toHexString(sb.toString());
		// remove track2 in log message
		String track2Data = bitsData.get(35);
		if (track2Data != null)
		{
			String track2DataL = track2Data.toLowerCase();
			log_message = log_message.replaceAll(track2DataL,
					Iso8583ProtocolFactory.TRACK2DATA_MARK_STRING.substring(0, track2DataL.length()));
		}
		// remove primary card number in log message
		String pan = bitsData.get(2);
		if (pan != null)
		{
			log_message = log_message.replaceAll(pan, Iso8583ProtocolFactory.PAN_MARK_STRING.substring(0, pan
					.length()));
		}
		logger.info("+++ First Data CC request(hex): " + log_message);
		return sb.toString().toCharArray();

	}
}
