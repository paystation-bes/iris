package com.digitalpaytech.cardprocessing.impl;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.Map;

import org.apache.commons.fileupload.ParameterParser;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.log4j.Logger;

import com.digitalpaytech.cardprocessing.CardProcessor;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DPTResponseCodesMapping;

public class Link2GovProcessor extends CardProcessor 
{
	private static Logger logger = Logger.getLogger(Link2GovProcessor.class);
	private final static String PROPERTY_AUTHORIZATION = "Auth";
	private final static String PROPERTY_SETTLEMENT = "Settle";
	private final static String PROPERTY_SALE = "Sale";
	private final static String PROPERTY_REFUND = "Refund";
	private final static String PROPERTY_CONNECTION_TIMEOUT = "ConnectionTimeout";
	private final static String PROPERTY_CHARSET = "Charset";
	private final static String PROPERTY_CONVENIENCE_FEE = "ConvenienceFee";

	private final static String RC_HOST_OK = "0";
	private final static int RC_HOST_DECLINED = -2147220481;

	private static int DEFAULT_HTTP_CONNECTION_TIMEOUT = 2000;
	private static String DEFAULT_CHARSET = "ISO-8859-1";
	private static int DEFAULT_CONVENIENCE_FEE = 0;
	private static ParameterParser responseParser = null;
	private static char RESPONSE_SEPARATOR = '&';
	private final static String RESPONSE_RC = "RC";
	private final static String RESPONSE_RCSTRING = "RCString";
	private final static String RESPONSE_TRANSACTION_ID = "TransactionID";
	private final static String RESPONSE_PROCESSOR_AUTH_CODE = "ProcessorAuthCode";
	private final static String RESPONSE_Status = "Status";
	private final static String RESPONSE_USERPART1 = "UserPart1";
	private final static String RESPONSE_PROCESSOR_AUTH_CODE_SIMULATED = "SIMULATED";
	private final static String RESPONSE_USERPART1_SIMULATED = "SIMULATED";

	public static final String PAN_MARK_STRING = "XXXXXXXXXXXXXXXXXXXXX"; // 21
																		  // X's
	public static final String TRACK2DATA_MARK_STRING = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"; // 36

	private String L2GMerchantCode = null;
	private String SettleMerchantCode = null;
	private String Password = null;

	private String destinationUrlString;
	private HttpClient client;
	private int connectionTimeout;
	private String charset;
	private int convenienceFee;

	public Link2GovProcessor(MerchantAccount merchantAccount,
	        CommonProcessingService commonProcessingService,
	        EmsPropertiesService emsPropertiesService) 
	{
		super(merchantAccount, commonProcessingService, emsPropertiesService);
		L2GMerchantCode = merchantAccount.getField1();
		SettleMerchantCode = merchantAccount.getField2();
		Password = merchantAccount.getField3();
		connectionTimeout = Integer.parseInt(emsPropertiesService.getPropertyValue(EmsPropertiesService.LINK2GOV_CONNECTION_TIMEOUT));
		charset = emsPropertiesService.getPropertyValue(EmsPropertiesService.LINK2GOV_CHARSET);
		convenienceFee = Integer.parseInt(emsPropertiesService.getPropertyValue(EmsPropertiesService.LINK2GOV_CONVENIENCE_FEE));
		destinationUrlString = commonProcessingService.getDestinationUrlString(merchantAccount.getProcessor());
		if (logger.isDebugEnabled()) 
		{
			logger.debug("Link2Gov destination url: " + destinationUrlString);
		}
	}
	
	
	
	@Override
	public boolean processPreAuth(PreAuth pd) throws CardTransactionException, CardTransactionException
	{
		return processAuthorization(pd);
	}
	
	/**
	 * Link2Gov Authorization Request
	 * <p>
	 * L2GMerchantCode --	Required	Your Merchant Code required for every transaction, assigned by Link2Gov <br/>
   * Password	Required --	Your Password, assigned by Link2Gov <br/>
   * SettleMerchantCode --	Required	Settle Merchant Code assigned by Link2Gov for the Reconciliation of funds. <br/>
   * TransactionAmount --	Required	Total Amount of the Transaction; this must always equal the sum of the Merchant Amount and Convenience Fee Fields <br/>
   * MerchantAmount	-- Required	Total Amount of Goods and Services for External Client <br/>
   * ConvenienceFee	-- Required	Total Convenience Fee Charged for the Transaction <br/>
   * TrackTwo -- Credit Card Swiped Transactions <br/>
   * UserPart1	Optional	Recommend External Client’s Unique Identifier for Transaction Varchar (255) <br/>
	 * 
	 * </p>
	 */
	private boolean processAuthorization(PreAuth preAuth) throws CardTransactionException, CardTransactionException
	{	
		getClient();
		PostMethod post = new PostMethod(destinationUrlString + getEmsPropertiesService().getPropertyValue(EmsPropertiesService.LINK2GOV_AUTH));
		String responseRC = "-2";
		try
		{
			post.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(1, false));
			setPostHeaders(post);
			
			NameValuePair l2GMerchantCode = getL2GMerchantCode();
			NameValuePair password = getMerchantPassword();
			NameValuePair settleMerchantCode = getSettleMerchantCode();
			NameValuePair transactionAmount = getTransactionAmount(preAuth);
			NameValuePair merchantAmount = getMerchantAmount(preAuth);
			NameValuePair convenienceFee = getConvenienceFee();			
			NameValuePair trackTwo = new NameValuePair("TrackTwo", preAuth.getCardData());
			NameValuePair userPart1 = getEMSReferenceNumber();
			post.setRequestBody(new NameValuePair[]{l2GMerchantCode, password, settleMerchantCode, transactionAmount, merchantAmount, convenienceFee, trackTwo, userPart1});
			if (logger.isDebugEnabled())
			{
				printPostParameters(post.getParameters());
			}
			logger.info("++++ send Authorization request to Link2Gov +++++");
			int statusCode = client.executeMethod(post);
			logger.debug("return statusCode: " + statusCode);
			if (statusCode == HttpStatus.SC_OK)
			{
				String response = URLDecoder.decode(post.getResponseBodyAsString(), charset);
				logger.info("++++ response from Link2Gov: " + response);
				Map responsePairs = this.parseResponse(response);
				
				preAuth.setProcessingDate(new Date());	
				preAuth.setReferenceNumber(responsePairs.get(RESPONSE_USERPART1).toString());	
				preAuth.setProcessorTransactionId(responsePairs.get(RESPONSE_TRANSACTION_ID).toString());
				
				responseRC = responsePairs.get(RESPONSE_RC).toString();
				// approved
				if (responseRC.equals(RC_HOST_OK))
				{
					preAuth.setAuthorizationNumber(responsePairs.get(RESPONSE_PROCESSOR_AUTH_CODE).toString());
					preAuth.setIsApproved(true);
					preAuth.setCardData(null);
				}
				else
				{
					preAuth.setAuthorizationNumber(null);
					preAuth.setIsApproved(false);
				}
			}
		}
		catch (IllegalArgumentException e)
		{
			logger.error("IllegalArgumentException: ", e);
			throw new CardTransactionException("Link2Gov Processor is Offline(IllegalArgumentException)", e);
		}
		catch (HttpException e)
		{
			logger.error("HttpException: ", e);
			throw new CardTransactionException("Link2Gov Processor is Offline(HttpException)", e);
		}
		catch (IOException e)
		{
			logger.error("IOException: ", e);
			throw new CardTransactionException("Link2Gov Processor is Offline(IOException)", e);
		}
		finally
		{
			post.releaseConnection();
		}
		return (preAuth.isIsApproved());
	}

	/**
	 * @param NameValuePair
	 */
	private void printPostParameters(NameValuePair[] ps)
	{
		logger.debug("++++ request parameters:"  );
		for (int i=0; i<ps.length; i++)
		{
			if (ps[i].toString().indexOf("TrackTwo") != -1)
			{
				logger.debug("name=TrackTwo, value=" + TRACK2DATA_MARK_STRING);
			}
			else if (ps[i].toString().indexOf("AccountNumber") != -1)
			{
			  logger.debug("name=AccountNumber, value=" + PAN_MARK_STRING);
		  }
			else if (ps[i].toString().indexOf("Password") != -1)
			{
				logger.debug("name=Password, value=" + "XXXXX");
			}
			else
			{
				logger.debug(ps[i].toString());
			}
		}
	}
		
	@Override
	public Object[] processPostAuth(PreAuth pd, ProcessorTransaction td) throws CardTransactionException, CardTransactionException
	{
		return processSettle(pd, td);
	}
	
	/**
	 * Link2Gov Settle Request
	 * <p>
	 * L2GMerchantCode --	Required	Your Merchant Code required for every transaction, assigned by Link2Gov <br/>
   * Password	Required --	Your Password, assigned by Link2Gov <br/>
   * SettleMerchantCode --	Required	Settle Merchant Code assigned by Link2Gov for the Reconciliation of funds. <br/>
   * OriginalTransactionId -- Required	L2GNet Transaction ID returned from the Authorization you are attempting to Settle <br/>
	 * 
	 * SettleAmount -- Optional	The amount to be settled. If the caller it settling the transaction for less than the amount of the original authorization, this value can be populated. Otherwise it is assumed to be a full settlement.
   *
	 * </p>
	 */
	private Object[] processSettle(PreAuth preAuth, ProcessorTransaction ptd) throws CardTransactionException, CardTransactionException
	{
		getClient();
		PostMethod post = new PostMethod(destinationUrlString + getEmsPropertiesService().getPropertyValue(EmsPropertiesService.LINK2GOV_SETTLE));
		String responseRC = "-2";
		try
		{
			post.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(1, false));
			setPostHeaders(post);
			
			NameValuePair l2GMerchantCode = getL2GMerchantCode();
			NameValuePair password = getMerchantPassword();
			NameValuePair settleMerchantCode = getSettleMerchantCode();
			NameValuePair originalTransactionId = getOriginalTransactionId(preAuth);
			NameValuePair settleAmount = new NameValuePair("SettleAmount", CardProcessingUtil.getCentsAmountInDollars(preAuth.getAmount()).toString());
			
			post.setRequestBody(new NameValuePair[]{l2GMerchantCode, password, settleMerchantCode, originalTransactionId, settleAmount});
			if (logger.isDebugEnabled())
			{
				printPostParameters(post.getParameters());
			}
			logger.info("++++ send Settlement to Link2Gov +++++");
			int statusCode = client.executeMethod(post);
			logger.debug("return statusCode: " + statusCode);
			if (statusCode == HttpStatus.SC_OK)
			{
				String response = URLDecoder.decode(post.getResponseBodyAsString(), charset);
				logger.info("++++ response from Link2Gov: " + response);
				Map responsePairs = this.parseResponse(response);
				
				ptd.setProcessingDate(new Date());
				ptd.setReferenceNumber(responsePairs.get(RESPONSE_USERPART1).toString());
				ptd.setAmount(preAuth.getAmount());
				ptd.setProcessorTransactionId(responsePairs.get(RESPONSE_TRANSACTION_ID).toString());
				
				responseRC = responsePairs.get(RESPONSE_RC).toString();
				// approved
				if (responseRC.equals(RC_HOST_OK))
				{
					ptd.setAuthorizationNumber(responsePairs.get(RESPONSE_PROCESSOR_AUTH_CODE).toString());
					ptd.setIsApproved(true);		
				}
				else
				{
					ptd.setAuthorizationNumber(null);
					ptd.setIsApproved(false);		
				}
			}
		}
		catch (IllegalArgumentException e)
		{
			logger.error("IllegalArgumentException: ", e);
			throw new CardTransactionException("Link2Gov Processor is Offline(IllegalArgumentException)", e);
		}
		catch (HttpException e)
		{
			logger.error("HttpException: ", e);
			throw new CardTransactionException("Link2Gov Processor is Offline(HttpException)", e);
		}
		catch (IOException e)
		{
			logger.error("IOException: ", e);
			throw new CardTransactionException("Link2Gov Processor is Offline(IOException)", e);
		}
		finally
		{
			post.releaseConnection();
			logger.debug("++++ ptd.isApproved(): " + ptd.isIsApproved());
		}
		return new Object[] {ptd.isIsApproved(), responseRC};
	}

	@Override
	public Object[] processCharge(Track2Card track2Card, ProcessorTransaction chargePtd)
			throws CardTransactionException, CardTransactionException
	{
		return processSale(track2Card, chargePtd);
	}
	
	/**
	 * Link2Gov Sale Request
	 * <p>
	 * L2GMerchantCode --	Required	Your Merchant Code required for every transaction, assigned by Link2Gov <br/>
   * Password	Required --	Your Password, assigned by Link2Gov <br/>
   * SettleMerchantCode --	Required	Settle Merchant Code assigned by Link2Gov for the Reconciliation of funds. <br/>
   * TransactionAmount --	Required	Total Amount of the Transaction; this must always equal the sum of the Merchant Amount and Convenience Fee Fields <br/>
   * MerchantAmount	-- Required	Total Amount of Goods and Services for External Client <br/>
   * ConvenienceFee	-- Required	Total Convenience Fee Charged for the Transaction <br/>
   * AccountNumber	-- Conditional	Credit Card Account Number. Required if TrackOne & TrackTwo are not populated. <br/>
   * ExpirationMonth --	Conditional	Two Digit Expiration Month of Credit Card. Required if TrackOne & TrackTwo are not populated. <br/>
   * ExpirationYear --	Conditional	Two Digit Expiration Year of Credit Card. Required if TrackOne & TrackTwo are not populated. <br/>
   * UserPart1	Optional	Recommend External Client’s Unique Identifier for Transaction Varchar (255) <br/>
	 * 
	 * </p>
	 */
	private Object[] processSale(Track2Card track2Card, ProcessorTransaction chargePtd) throws CardTransactionException,
		CardTransactionException
	{
		boolean succeed = false;
		getClient();
		PostMethod post = new PostMethod(destinationUrlString + getEmsPropertiesService().getPropertyValue(EmsPropertiesService.LINK2GOV_SALE));
		String responseRC = "-2";
		try
		{
			post.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(1, false));
			setPostHeaders(post);
			
			NameValuePair l2GMerchantCode = getL2GMerchantCode();
			NameValuePair password = getMerchantPassword();
			NameValuePair settleMerchantCode = getSettleMerchantCode();
			NameValuePair transactionAmount = getTransactionAmount(chargePtd);
			NameValuePair merchantAmount = getMerchantAmount(chargePtd);
			NameValuePair convenienceFee = getConvenienceFee();
			NameValuePair userPart1 = getEMSReferenceNumber();
			if (track2Card.isCardDataCreditCardTrack2Data())
			{
				NameValuePair trackTwo = new NameValuePair("TrackTwo", track2Card.getDecryptedCardData());
				post.setRequestBody(new NameValuePair[]{l2GMerchantCode, password, settleMerchantCode, transactionAmount, merchantAmount, convenienceFee, trackTwo, userPart1});
				
			}
			else
			{
				NameValuePair accountNumber = new NameValuePair("AccountNumber", track2Card.getPrimaryAccountNumber());
				NameValuePair expirationMonth = new NameValuePair("ExpirationMonth", track2Card.getExpirationMonth());
				NameValuePair expirationYear = new NameValuePair("ExpirationYear", track2Card.getExpirationYear());
				post.setRequestBody(new NameValuePair[]{l2GMerchantCode, password, settleMerchantCode, transactionAmount, merchantAmount, convenienceFee, accountNumber, expirationMonth, expirationYear, userPart1});
			}			
			if (logger.isDebugEnabled())
			{
				printPostParameters(post.getParameters());
			}
			logger.info("++++ send Charge request to Link2Gov +++++");
			int statusCode = client.executeMethod(post);
			logger.debug("return statusCode: " + statusCode);
			if (statusCode != HttpStatus.SC_OK)
			{
				succeed = false;
			}
			else
			{
				succeed = true;
				String response = URLDecoder.decode(post.getResponseBodyAsString(), charset);
				logger.info("++++ response from Link2Gov: " + response);
				Map responsePairs = this.parseResponse(response);
				
				chargePtd.setProcessingDate(new Date());
				chargePtd.setReferenceNumber(responsePairs.get(RESPONSE_USERPART1).toString());
				chargePtd.setProcessorTransactionId(responsePairs.get(RESPONSE_TRANSACTION_ID).toString());
				
				responseRC = responsePairs.get(RESPONSE_RC).toString();
				// approved
				if (responseRC.equals(RC_HOST_OK))
				{
					chargePtd.setAuthorizationNumber(responsePairs.get(RESPONSE_PROCESSOR_AUTH_CODE).toString());
					chargePtd.setIsApproved(true);
					return new Object[] {DPTResponseCodesMapping.CC_APPROVED_VAL__7000, responseRC};
				}
				else
				{
				   // do nothing 
				}
			}
		}	
		catch (IllegalArgumentException e)
		{
			logger.error("IllegalArgumentException: ", e);
			throw new CardTransactionException("Link2Gov Processor is Offline(IllegalArgumentException)", e);
		}
		catch (HttpException e)
		{
			logger.error("HttpException: ", e);
			throw new CardTransactionException("Link2Gov Processor is Offline(HttpException)", e);
		}
		catch (IOException e)
		{
			logger.error("IOException: ", e);
			throw new CardTransactionException("Link2Gov Processor is Offline(IOException)", e);
		}
		finally
		{
			post.releaseConnection();
		}
		// post data error
		chargePtd.setAuthorizationNumber(null);
		chargePtd.setIsApproved(false);
		if (responseRC.equals("-2"))
		{
			logger.error("++++ Post Link2Gov CC data error ++++");
		}
		return new Object[] {mapErrorResponse(responseRC), responseRC};
	}

	/**
	 * Link2Gov Refund Request
	 * <p>
	 * L2GMerchantCode --	Required	Your Merchant Code required for every transaction, assigned by Link2Gov <br/>
   * Password	Required --	Your Password, assigned by Link2Gov <br/>
   * SettleMerchantCode --	Required	Settle Merchant Code assigned by Link2Gov for the Reconciliation of funds. <br/>
   * OriginalTransactionId -- Required	L2GNet Transaction ID returned from the Authorization you are attempting to Settle <br/>
	 * AccountNumber - Conditional  Backend Processor specific.  Required to Perform a Credit Card Credit if required by the backend processor
	 * RefundAmount -- Optional	If the caller it refunding the transaction for less than the amount of the original authorization, this value can be populated. Otherwise it is assumed to be a full refund.
	 * </p>
	 */
	public Object[] processRefund(boolean isNonEmsRequest, ProcessorTransaction origPtd,
			ProcessorTransaction refundPtd, String ccNumber, String expiryMMYY) throws CardTransactionException,
			CardTransactionException
	{
		getClient();
		PostMethod post = new PostMethod(destinationUrlString + getEmsPropertiesService().getPropertyValue(EmsPropertiesService.LINK2GOV_REFUND));
		String responseRC = "-2";
		try
		{
			post.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(1, false));
			setPostHeaders(post);
			
			NameValuePair l2GMerchantCode = getL2GMerchantCode();
			NameValuePair password = getMerchantPassword();
			NameValuePair settleMerchantCode = getSettleMerchantCode();
			NameValuePair originalTransactionId = getOriginalTransactionId(origPtd);
			NameValuePair accountNumber = new NameValuePair("AccountNumber", ccNumber);
			NameValuePair refundAmount = new NameValuePair("RefundAmount", CardProcessingUtil.getCentsAmountInDollars(refundPtd.getAmount()).toString());
			
			NameValuePair userPart1 = getEMSReferenceNumber();
			
			post.setRequestBody(new NameValuePair[]{l2GMerchantCode, password, settleMerchantCode, originalTransactionId, accountNumber, refundAmount, userPart1});
			if (logger.isDebugEnabled())
			{
				printPostParameters(post.getParameters());
			}
			logger.info("++++ send Refund to Link2Gov +++++");
			int statusCode = client.executeMethod(post);
			logger.debug("return statusCode: " + statusCode);
			if (statusCode == HttpStatus.SC_OK)
			{
				String response = URLDecoder.decode(post.getResponseBodyAsString(), charset);
				logger.info("++++ response from Link2Gov: " + response);
				Map responsePairs = this.parseResponse(response);
				
				refundPtd.setProcessingDate(new Date());				
				refundPtd.setProcessorTransactionId(responsePairs.get(RESPONSE_TRANSACTION_ID).toString());
				
				responseRC = responsePairs.get(RESPONSE_RC).toString();
				// approved
				if (responseRC.equals(RC_HOST_OK))
				{
					// no processor auth code returns from test url
					if (getCommonProcessingService().isProcessorTestMode(super.getMerchantAccount().getProcessor().getId()))
					{
						refundPtd.setAuthorizationNumber(RESPONSE_PROCESSOR_AUTH_CODE_SIMULATED);
						refundPtd.setReferenceNumber(RESPONSE_USERPART1_SIMULATED);
					}
					else
					{					
						if (responsePairs.get(RESPONSE_USERPART1) != null)
						{
							refundPtd.setReferenceNumber(responsePairs.get(RESPONSE_USERPART1).toString());
						}
						
						if (responsePairs.get(RESPONSE_PROCESSOR_AUTH_CODE) != null)
						{
							refundPtd.setAuthorizationNumber(responsePairs.get(RESPONSE_PROCESSOR_AUTH_CODE).toString());
						}
					}
					refundPtd.setIsApproved(true);
					return new Object[] {DPTResponseCodesMapping.CC_APPROVED_VAL__7000, responseRC};
				}
				else
				{
					 // Declined
					refundPtd.setAuthorizationNumber(null);
					refundPtd.setIsApproved(false);			
				}
			}
		}
		catch (IllegalArgumentException e)
		{
			logger.error("IllegalArgumentException: ", e);
			throw new CardTransactionException("Link2Gov Processor is Offline(IllegalArgumentException)", e);
		}
		catch (HttpException e)
		{
			logger.error("HttpException: ", e);
			throw new CardTransactionException("Link2Gov Processor is Offline(HttpException)", e);
		}
		catch (IOException e)
		{
			logger.error("IOException: ", e);
			throw new CardTransactionException("Link2Gov Processor is Offline(IOException)", e);
		}
		finally
		{
			post.releaseConnection();
		}		
		return new Object[] {mapErrorResponse(responseRC), responseRC};		
	}

	@Override
	public void processReversal(Reversal reversal, Track2Card track2Card) throws CardTransactionException
	{
		throw new UnsupportedOperationException("Reversal not supported for this processor");
	}
  
	private HttpClient getClient()
	{
		if (client == null)
		{
			MultiThreadedHttpConnectionManager connectionManager = new MultiThreadedHttpConnectionManager();
			connectionManager.getParams().setConnectionTimeout(connectionTimeout);
			logger.debug("++++ HTTP_CONNECTION_TIMEOUT: " + connectionTimeout + " ms +++");
			connectionManager.getParams().setStaleCheckingEnabled(true);
			client = new HttpClient(connectionManager);
		}
		return client;
	}
	
  private Map parseResponse(String response)
  {
  	if (responseParser == null)
  	{
  		responseParser = new ParameterParser();
  	}
  	Map map = responseParser.parse(response, RESPONSE_SEPARATOR);
  	logger.debug("++++ response map:  " + map);
  	return map;
  }	

	private int mapErrorResponse(String responseCode) throws CardTransactionException
	{
		int response_code = -1;
		try
		{
			response_code = Integer.parseInt(responseCode);
		}
		catch (Exception e)
		{
			throw new CardTransactionException("Expecting numeric response, but recieved: "
					+ responseCode, e);
		}

		switch (response_code)
		{
			// RC_DECLINED: Transaction declined
			case RC_HOST_DECLINED:
				return (DPTResponseCodesMapping.CC_DECLINED_VAL__7003);

				// RC_INSUFFICIENTFUNDS: Insufficient funds
			case 21:
				return (DPTResponseCodesMapping.CC_INSUFFICIENT_FUNDS_VAL__7017);

				// RC_INVALIDCARD: Invalid card number, MICR number, or routing number
			case 22:
				return (DPTResponseCodesMapping.CC_BAD_CREDIT_CARD_DATA_VAL__7045);

				// RC_EXPIREDCARD: Card expired
			case 23:
				return (DPTResponseCodesMapping.CC_EXPIRED_VAL__7001);

				// RC_DUPLICATE: A Duplicate Successful Transaction meeting the Merchant duplicate settings exists 
			case 4:
				// RC_PENDING : A Duplicate Pending Transaction meeting the Merchant duplicate settings exists
			case 6:
			  // RC_DUP_UNSUCCESSFUL  : A Duplicate Unsuccessful Transaction meeting the Merchant duplicate settings exists
			case 7:
			  // RC_DUPLICATEAPPROVED  : Transaction previously approved
			case 20:
			  // RC_DUPLICATEREFERENCE  : Duplicate Reference number
			case 29:
				return (DPTResponseCodesMapping.CC_DUPLICATE_TRANS_VAL__7019);

				// RC_REFERRAL: Contact Financial Institution
			case 24:
				return (DPTResponseCodesMapping.CC_PLACE_CALL_VAL__7022);

				// RC_AUTHENTICATE: Authentication required
//			case :
//				// RC_BADAUTHENTICATION: Authentication failed
//			case 261:
//				// RC_UNABLETOAUTHENTICATE: Unable to authenticate
//			case 262:
//				return (DPTResponseCodesMapping.CC_UNABLE_TO_AUTHENTICATE_VAL__7055);

				// RC_STOPPAYMENT: Stop payment
//			case 263:
//				return (DPTResponseCodesMapping.CC_STOP_PAYMENT_VAL__7056);

				// RC_COMMERROR: Communications error, try again
			case 27:
				// RC_COMMFAILURE: Communications failure
			case 28:
				throw new CardTransactionException("Response indicates processor offline: " + responseCode);

				// RC_PROCESSORERROR: Generic processor error
			case 25:
				// RC_ERROR: General error
			case 26:
			
				return (DPTResponseCodesMapping.CC_SYSTEM_ERROR_VAL__7047);

				// RC_INVALIDREQUEST: Invalid request for this transaction occurred
			case 31:
				// RC_INVALIDTRANSACTION: Invalid transaction was submitted
			case 35:
				// RC_NOTPERMITTED: Transaction not permitted
			case 36:
				return (DPTResponseCodesMapping.CC_INVALID_REQUEST_VAL__7058);

				// RC_INVALIDMERCHANT: Invalid merchant information was received
			case 30:
				return (DPTResponseCodesMapping.CC_CARD_TYPE_NOT_SETUP_VAL__7076);

				// RC_BADUSERNAME: Invalid processor information was sent for the account
			case 32:
				throw new CardTransactionException("Response indicates invalid StoreID: "
						+ responseCode);

				// RC_BADPASSWORD: Invalid processor information was sent for the account, similar to (32)
			case 33:
				throw new CardTransactionException("Response indicates invalid StoreKey: "
						+ responseCode);

				// RC_BADPROCESSOR: Processor could not be determined for this transaction
			case 34:
				return (DPTResponseCodesMapping.CC_BAD_PROCESSOR_VAL__7061);

				// RC_BACKEND_PROCESSOR_ERROR: Unknown error
			case 37:
				return (DPTResponseCodesMapping.CC_UNKNOWN_ERROR_VAL__7063);

			default:
				throw new CardTransactionException("Unknown Response Code: " + responseCode);
		}
	}
	
	/**
	 * @return
	 */
	private NameValuePair getConvenienceFee()
	{
		return new NameValuePair("ConvenienceFee", String.valueOf(this.convenienceFee));
	}

	/**
	 * @return
	 */
	private NameValuePair getEMSReferenceNumber()
	{
		return new NameValuePair("UserPart1", getCommonProcessingService().getNewReferenceNumber(getMerchantAccount()));
	}

	/**
	 * @param chargePtd
	 * @return
	 */
	private NameValuePair getMerchantAmount(ProcessorTransaction chargePtd)
	{
		return new NameValuePair("MerchantAmount", CardProcessingUtil.getCentsAmountInDollars(chargePtd.getAmount()).toString());
	}
	
	/**
	 * @param chargePtd
	 * @return
	 */
	private NameValuePair getMerchantAmount(PreAuth preAuth)
	{
		return new NameValuePair("MerchantAmount", CardProcessingUtil.getCentsAmountInDollars(preAuth.getAmount()).toString());
	}

	/**
	 * @param chargePtd
	 * @return
	 */
	private NameValuePair getTransactionAmount(ProcessorTransaction chargePtd)
	{
		Float original = CardProcessingUtil.getCentsAmountInDollars(chargePtd.getAmount()).floatValue();
		return new NameValuePair("TransactionAmount", String.valueOf(original + this.convenienceFee));
	}
	
	/**
	 * @param chargePtd
	 * @return
	 */
	private NameValuePair getTransactionAmount(PreAuth preAuth)
	{
		Float original = CardProcessingUtil.getCentsAmountInDollars(preAuth.getAmount()).floatValue();
		return new NameValuePair("TransactionAmount", String.valueOf(original + this.convenienceFee));
	}

	/**
	 * @return
	 */
	private NameValuePair getSettleMerchantCode()
	{
		return new NameValuePair("SettleMerchantCode", SettleMerchantCode);
	}

	/**
	 * @return
	 */
	private NameValuePair getMerchantPassword()
	{
		return new NameValuePair("Password", Password);
	}

	/**
	 * @return
	 */
	private NameValuePair getL2GMerchantCode()
	{
		return new NameValuePair("L2GMerchantCode", L2GMerchantCode);
	}
	
	/**
	 * @param preAuth
	 * @return
	 */
	private NameValuePair getOriginalTransactionId(PreAuth preAuth)
	{
		logger.debug("preAuth.getProcessorTransactionId(): " + preAuth.getProcessorTransactionId());
		return new NameValuePair("OriginalTransactionId", preAuth.getProcessorTransactionId());
	}
	
	private NameValuePair getOriginalTransactionId(ProcessorTransaction pt)
	{
		logger.debug("pt.getProcessorTransactionId(): " + pt.getProcessorTransactionId());
		return new NameValuePair("OriginalTransactionId", pt.getProcessorTransactionId());
	}
	
	/**
	 * @param post
	 */
	private void setPostHeaders(PostMethod post)
	{
		post.setRequestHeader("content-type", "application/x-www-form-urlencoded");
		post.setRequestHeader("Accept-Language", "en-us,en");
		post.setRequestHeader("Accept-Charset", charset);
	}
	
	@Override
	public boolean isReversalExpired(Reversal reversal) {
		throw new UnsupportedOperationException("Link2GovProcessor, isReversalExpired(Reversal) is not support!");
	}		
}
