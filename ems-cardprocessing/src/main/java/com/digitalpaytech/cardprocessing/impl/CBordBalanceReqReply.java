package com.digitalpaytech.cardprocessing.impl;

public class CBordBalanceReqReply {

	private String Message;
	private String className;
	private String Code;
	private String Location;
	private String Provider;
	private String ResponseCode;
	private String PaymentMedia;
	private String Plan;
	private String ActualSVCbalance;
	private String EndSVCbalance;
	private String Patron;
	private String PatronID;
	private String TransID;
	private String HostMessage;
	private String HostReceipt;

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getCode() {
		return Code;
	}

	public void setCode(String code) {
		Code = code;
	}

	public String getLocation() {
		return Location;
	}

	public void setLocation(String location) {
		Location = location;
	}

	public String getProvider() {
		return Provider;
	}

	public void setProvider(String provider) {
		Provider = provider;
	}

	public String getResponseCode() {
		return ResponseCode;
	}

	public void setResponseCode(String responseCode) {
		ResponseCode = responseCode;
	}

	public String getPaymentMedia() {
		return PaymentMedia;
	}

	public void setPaymentMedia(String paymentMedia) {
		PaymentMedia = paymentMedia;
	}

	public String getPlan() {
		return Plan;
	}

	public void setPlan(String plan) {
		Plan = plan;
	}

	public String getActualSVCbalance() {
		return ActualSVCbalance;
	}

	public void setActualSVCbalance(String actialSVCbalance) {
		ActualSVCbalance = actialSVCbalance;
	}

	public String getEndSVCbalance() {
		return EndSVCbalance;
	}

	public void setEndSVCbalance(String endSVCbalance) {
		EndSVCbalance = endSVCbalance;
	}

	public String getPatron() {
		return Patron;
	}

	public void setPatron(String patron) {
		Patron = patron;
	}

	public String getPatronID() {
		return PatronID;
	}

	public void setPatronID(String patronID) {
		PatronID = patronID;
	}

	public String getTransID() {
		return TransID;
	}

	public void setTransID(String transID) {
		TransID = transID;
	}

	public String getHostMessage() {
		return HostMessage;
	}

	public void setHostMessage(String hostMessage) {
		HostMessage = hostMessage;
	}

	public String getHostReceipt() {
		return HostReceipt;
	}

	public void setHostReceipt(String hostReceipt) {
		HostReceipt = hostReceipt;
	}

}
