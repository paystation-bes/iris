package com.digitalpaytech.cardprocessing.impl;

import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.EmsPropertiesService;

public class CBordGoldProcessor extends CBordProcessor {

	public CBordGoldProcessor(final MerchantAccount merchantAccount,
			final CommonProcessingService commonProcessingService,
			final EmsPropertiesService emsPropertiesService,
			final PointOfSale pointOfSale, final String timeZone) {
		super(merchantAccount, commonProcessingService, emsPropertiesService,
				pointOfSale, timeZone);
		currentProcessor = "Gold";
		// TODO Auto-generated constructor stub
	}

}
