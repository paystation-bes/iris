package com.digitalpaytech.cardprocessing.impl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.commons.lang.StringUtils;

import com.digitalpaytech.cardprocessing.CardProcessor;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DPTResponseCodesMapping;

public class PaymentechProcessor extends CardProcessor 
{
	private static Logger logger__ = Logger.getLogger(PaymentechProcessor.class);
	/* Test & prod server urls are in database Processor table.
	 * private final static String TEST_SERVER_1 = "https://netconnectvar1.paymentech.net/NetConnect/controller";
	 * private final static String TEST_SERVER_2 = "https://netconnectvar2.paymentech.net/NetConnect/controller";
	 * private final static String PROD_SERVER_1 = "https://netconnect1.paymentech.net/NetConnect/controller";
	 * private final static String PROD_SERVER_2 = "https://netconnect2.paymentech.net/NetConnect/controller";
	 */
	private final static String TRANS_CODE_SALE = "01";
	private final static String TRANS_CODE_RETURN = "06";
	private final static String TRANS_CODE_AUTH = "02";
	private final static String TRANS_CODE_PRIOR_SALE_FORCE = "03";
	private final static String TRANS_CODE_REVERSAL = "46";
	
	private final static String ENTRY_DATA_SOURCE_MANUALLY_ENTERED = "02";
	private final static String ENTRY_DATA_SOURCE_TRACK2 = "03";
	private final static String ENTRY_DATA_SOURCE_CONTACTLESS_DEVICE_TRACK2 = "32";
	private final static String ENTRY_DATA_SOURCE_CONTACTLESS_DEVICE_MANUALLY_KEYED = "35";

	private final static char FIELD_SEPARATOR = 0x1c;
	private final static String CARD_DATA__TRACK2 = "03";
	private final static String CARD_DATA__PAN_EXPIRY = "02";
	private final static String ACTION_CODE_APPROVED = "A";
	private final static String ACTION_CODE_ERROR = "E";
	private final static String ERROR_CODE_OVER_CREDIT_LIMIT = "217";

	private final static String ROUTING_INDICATOR = "A02000";
	private final static String TRANSACTION_CLASS = "F";
	private final static String NO_PIN_ENTRY = "2";
	private final static String SINGLE_TRANSACTION = "1";
	private final static String STX = "\02";
	private final static String ETX = "\03";
	private final static String INDUSTRY_RETAIL = "004";
	private final static String INVOICE_NUMBER = "000000";
	private final static String ITEM_CODE = "00000000000000000000";

	private final static String TOKEN_REF_NUMBER = "RN";
	private final static String TOKEN_TRANS_CODE = "TC";
	private final static String TOKEN_UNATTENED_TERMINAL_CODE = "UT";
	private final static String TOKEN_UNATTENED_TERMINAL_VALUE_CAT_LEVEL_3 = "03";
	private final static String TOKEN_P1 = "P1";
	private final static String TOKEN_P2 = "P2";
	private final static String TOKEN_CP = "CP";
	private final static String TOKEN_DUPLICATE_CHECK_AND_VALUE = "DU02";

	private final static String P1_VENDOR_ID = "007F";
	private final static String P1_SOFTWARE_ID = "009A";
	private final static String P1_HARDWARE_SERIAL_NUM = "                    ";

	private final static String P2_BITMAP = "6AA0000000000000";
	private final static String P2_VALUE = "08";	// Auth/Settle
	private final static String P3_VALUE = "40";	// UTF message format
	private final static String P5_VALUE = "20";	// Peripheral - Internal contactless reader enabled
	private final static String P7_VALUE = "10";	// Communication - NetConnect
	private final static String P9_VALUE = "02";	// Industry - Retail
	private final static String P11_VALUE = "44";	// Class B gatway

	private final static String CP_VALUE = "Y";		// Request for partial authorization

	private final static String TRANS_INFO_FILLER_REVERSAL = "" + FIELD_SEPARATOR + FIELD_SEPARATOR
			+ FIELD_SEPARATOR + FIELD_SEPARATOR + FIELD_SEPARATOR + FIELD_SEPARATOR + FIELD_SEPARATOR
			+ FIELD_SEPARATOR + FIELD_SEPARATOR + FIELD_SEPARATOR;
	private final static String TRANS_INFO_FILLER = FIELD_SEPARATOR + "00000000" + FIELD_SEPARATOR
			+ FIELD_SEPARATOR + FIELD_SEPARATOR;

	private String accountId;
	private String terminalId;
	private String user;
	private String password;
	private String clientNumber;
	private String destinationUrlString1_;
	private String destinationUrlString2_;

	// parsed responses from the server
	private String actionCode;
	private String authOrErrorCode;
	private String seqNumber;
	private String responseMessage;
	private String processorTransactionID;
	
	public PaymentechProcessor(MerchantAccount md, 
			CommonProcessingService commonProcessingService, 
			EmsPropertiesService emsPropertiesService, 
			CustomerCardTypeService customerCardTypeService) {
		
		super(md, commonProcessingService, emsPropertiesService);
		setCustomerCardTypeService(customerCardTypeService);
		accountId = md.getField1();
		terminalId = md.getField2();
		user = md.getField3();
		password = md.getField4();
		clientNumber = md.getField5();
		
		// Paymentech processor has 2 urls separated by comma, e.g. 'https://netconnect1.paymentech.net/NetConnect/controller,https://netconnect2.paymentech.net/NetConnect/controller'
		String[] urls = commonProcessingService.getDestinationUrlString(md.getProcessor()).split(",");
		destinationUrlString1_ = urls[0];
		if (urls.length > 1) {
			destinationUrlString2_ = urls[1];
		}
		if (logger__.isDebugEnabled()) {
			StringBuilder bdr = new StringBuilder();
			bdr.append("PaymentechProcessor, destinationUrlString1: ").append(destinationUrlString1_).append(", destinationUrlString2: ").append(destinationUrlString2_);
			logger__.debug(bdr.toString());
		}
	}
	
	
	public boolean processPreAuth(PreAuth pd) throws CardTransactionException
	{
		CustomerCardType customerCardType = getCustomerCardTypeService().getCardTypeByTrack2Data(getMerchantAccount().getCustomer().getId(), pd.getCardData());
		Track2Card track_2_card = new Track2Card(customerCardType, getMerchantAccount().getCustomer().getId(), pd.getCardData());		

		String reference_number = getCommonProcessingService().getNewReferenceNumber(getMerchantAccount());

		String request = createAuthorizationRequest(track_2_card, reference_number, pd);

		String response;
		try
		{
			response = sendAndRetryRequest(request);
		}
		catch (CardTransactionException e)
		{
			createAndSendReversal(TRANS_CODE_AUTH, CardProcessingUtil.getCentsAmountInDollars(pd.getAmount()), reference_number, track_2_card,
					getMerchantAccount().getId(), new Date(), 0, pd.getPointOfSale().getId());
			throw (e);
		}

		extractResponses(response);

		if(actionCode.equals(ACTION_CODE_APPROVED))
		{
			int approvedAmountCents = extractPartialAuthorizationAmount(TRANS_CODE_AUTH, response, pd.getAmount());
			if (approvedAmountCents < pd.getAmount())
			{
				// partial approval, send reversal
				actionCode = ACTION_CODE_ERROR;
				authOrErrorCode = ERROR_CODE_OVER_CREDIT_LIMIT;
				createAndSendReversal(TRANS_CODE_AUTH, CardProcessingUtil.getCentsAmountInDollars(pd.getAmount()), reference_number,
						track_2_card, getMerchantAccount().getId(), new Date(), 0, pd.getPointOfSale().getId());
			}
		}

		StringBuilder sb = new StringBuilder(200);
		sb.append("Auth Response:  ActionCode: ");
		sb.append(actionCode);
		sb.append(", Response Message: ");
		sb.append(responseMessage.trim());
		sb.append(", Authorization Number: ");
		sb.append(authOrErrorCode);
		sb.append(", ReferenceNumber: ");
		sb.append(seqNumber);
		sb.append(", ProcessorTransactionId: ");
		sb.append(processorTransactionID);
		logger__.info(sb.toString());
		
		pd.setProcessingDate(new Date());
		pd.setReferenceNumber(seqNumber);
		pd.setProcessorTransactionId(processorTransactionID);

		if (actionCode.equals(ACTION_CODE_APPROVED))
		{
			pd.setAuthorizationNumber(authOrErrorCode);
			pd.setIsApproved(true);

			pd.setCardData(track_2_card.getCreditCardPanAndExpiry());
		}
		else if (actionCode.equals("E"))
		{
			pd.setIsApproved(false);
			pd.setResponseCode(authOrErrorCode);
		}

		return (pd.isIsApproved());
	}

	public Object[] processCharge(Track2Card track2Card, ProcessorTransaction chargePtd) throws CardTransactionException
	{
		String reference_number = getCommonProcessingService().getNewReferenceNumber(getMerchantAccount());

		String request = createSaleRequest(track2Card, reference_number, CardProcessingUtil.getCentsAmountInDollars(chargePtd.getAmount()).floatValue(), chargePtd.isIsRfid());

		String response;
		try
		{
			response = sendAndRetryRequest(request);
		}
		catch (CardTransactionException e)
		{
			createAndSendReversal(TRANS_CODE_SALE, CardProcessingUtil.getCentsAmountInDollars(chargePtd.getAmount()), reference_number, track2Card,
					getMerchantAccount().getId(), chargePtd.getPurchasedDate(), chargePtd.getTicketNumber(),
					chargePtd.getPointOfSale().getId());

			throw (e);
		}

		return handleChargeResponse(response, chargePtd, reference_number, track2Card);
	}

	public Object[] processPostAuth(PreAuth pd, ProcessorTransaction ptd) throws CardTransactionException
	{
		CustomerCardType customerCardType = getCustomerCardTypeService().getCardTypeByTrack2DataOrAccountNumber(getMerchantAccount().getCustomer().getId(), pd.getCardData());
		Track2Card track_2_card = new Track2Card(customerCardType, getMerchantAccount().getCustomer().getId(), pd.getCardData());

		String reference_number = getCommonProcessingService().getNewReferenceNumber(getMerchantAccount());

		String request = createPriorSaleForceRequest(track_2_card, reference_number, pd);

		String response;
		try
		{
			response = sendAndRetryRequest(request);
		}
		catch (CardTransactionException e)
		{
			createAndSendReversal(TRANS_CODE_PRIOR_SALE_FORCE, CardProcessingUtil.getCentsAmountInDollars(ptd.getAmount()), reference_number,
					track_2_card, getMerchantAccount().getId(), ptd.getPurchasedDate(), ptd.getTicketNumber(), ptd.getPointOfSale().getId());

			throw (e);
		}

		extractResponses(response);

		StringBuilder sb = new StringBuilder(200);
		sb.append("Settle Response:  ActionCode: ");
		sb.append(actionCode);
		sb.append(", Response Message: ");
		sb.append(responseMessage.trim());
		sb.append(", Authorization Number: ");
		sb.append(authOrErrorCode);
		sb.append(", ReferenceNumber: ");
		sb.append(seqNumber);
		sb.append(", ProcessorTransactionId: ");
		sb.append(processorTransactionID);
		logger__.info(sb.toString());
				
		// Set ProcessorTransactionData values from processor
		ptd.setProcessingDate(new Date());
		ptd.setReferenceNumber(seqNumber);
		ptd.setProcessorTransactionId(processorTransactionID);

		// Approved
		if (actionCode.equals(ACTION_CODE_APPROVED))
		{
			ptd.setAuthorizationNumber(authOrErrorCode);
			ptd.setIsApproved(true);
			return new Object[] {true, actionCode};
		}

		// Declined
		ptd.setAuthorizationNumber(null);
		ptd.setIsApproved(false);
		return new Object[] {false, actionCode};
	}

	public Object[] processRefund(boolean isNonEmsRequest, ProcessorTransaction origPtd,
			ProcessorTransaction refundPtd, String ccNumber, String expiryMMYY) throws CardTransactionException
	{
		StringBuilder bdr = new StringBuilder();
		// pan + TRACK_2_DELIMITER + expiryYY + expiryMM
		bdr.append(ccNumber).append(CardProcessingConstants.TRACK_2_DELIMITER).append(expiryMMYY.substring(2)).append(expiryMMYY.substring(0, 2));
		String cardData = bdr.toString();

		CustomerCardType customerCardType = getCustomerCardTypeService().getCardTypeByTrack2DataOrAccountNumber(getMerchantAccount().getCustomer().getId(), cardData);
		Track2Card track_2_card = new Track2Card(customerCardType, getMerchantAccount().getCustomer().getId(), cardData);

		String reference_number = getCommonProcessingService().getNewReferenceNumber(getMerchantAccount());

		String request = createReturnRequest(track_2_card, reference_number, CardProcessingUtil.getCentsAmountInDollars(refundPtd.getAmount()).floatValue(), refundPtd.isIsRfid());

		String response;
		try
		{
			response = sendAndRetryRequest(request);
		}
		catch (CardTransactionException e)
		{
			createAndSendReversal(TRANS_CODE_RETURN, CardProcessingUtil.getCentsAmountInDollars(refundPtd.getAmount()), reference_number,
					track_2_card, getMerchantAccount().getId(), refundPtd.getPurchasedDate(), refundPtd.getTicketNumber(), refundPtd.getPointOfSale().getId());

			throw (e);
		}

		extractResponses(response);
	
		StringBuilder sb = new StringBuilder(200);
		sb.append("Refund Response:  ActionCode: ");
		sb.append(actionCode);
		sb.append(", Response Message: ");
		sb.append(responseMessage.trim());
		sb.append(", Authorization Number: ");
		sb.append(authOrErrorCode);
		sb.append(", ReferenceNumber: ");
		sb.append(seqNumber);
		sb.append(", ProcessorTransactionId: ");
		sb.append(processorTransactionID);
		logger__.info(sb.toString());
	
		// Set processor transaction data values
		refundPtd.setProcessingDate(new Date());
		refundPtd.setReferenceNumber(seqNumber);
		refundPtd.setProcessorTransactionId(processorTransactionID);

		// Approved
		if (actionCode.equals(ACTION_CODE_APPROVED))
		{
			refundPtd.setAuthorizationNumber(authOrErrorCode);
			refundPtd.setIsApproved(true);
			return new Object[] {DPTResponseCodesMapping.CC_APPROVED_VAL__7000, actionCode};
		}

		// Declined
		refundPtd.setAuthorizationNumber(null);
		refundPtd.setIsApproved(false);
		return new Object[] {mapErrorResponse(authOrErrorCode), actionCode};
	}

	private void createAndSendReversal(String transCode, BigDecimal amount, String referenceNumber,
			Track2Card track2Card, int merchantAccountId, Date date, int ticketNumber, int pointOfSaleId)
	{
		Reversal reversal = null;
		try
		{
			reversal = getCardProcessingManager().createReversal(transCode, "", amount, referenceNumber, Long.toString(System.currentTimeMillis()), 
							track2Card.getCreditCardPanAndExpiry(), "", merchantAccountId, date, ticketNumber, pointOfSaleId);
		}
		catch (Exception e)
		{
			String msg = "Unable to create Reversal record";
			logger__.error(msg, e);
			getCommonProcessingService().sendCardProcessingAdminErrorAlert("Unable to call 'createAndSendReversal' in PaymentechProcessor. ", msg, e);
		}
        
		if (reversal == null) 
        {
            StringBuilder msg = new StringBuilder();
            msg.append("Unable to retrieve REVERSAL record using: \r\noriginalMti: ").append(transCode);
            msg.append("\r\noriginalProcessingCode: ").append("\r\namount: ").append(amount).append("\r\nreferenceNumber: ").append(referenceNumber);
            msg.append("\r\nmerchantAccountId: ").append(merchantAccountId).append("\r\nticketNumber: ").append(ticketNumber).append("\r\nPofSId: ").append(pointOfSaleId);
            logger__.error(msg.toString());
            return;
        }
		
		try
		{
			getCardProcessingManager().processReversal(reversal);
		}
		catch (Exception e)
		{
			String msg = "Unable to update reversal after processing: " + reversal;
			logger__.error(msg, e);
			getCommonProcessingService().sendCardProcessingAdminErrorAlert("Unable to call 'processReversal' in PaymentechProcessor. ", msg, e);
		}
	}

	private String sendAndRetryRequest(String request) throws CardTransactionException
	{
		String response = null;
		try
		{
			response = sendRequest(destinationUrlString1_, request);
		}
		catch (CardTransactionException e)
		{
			logger__.info(e.getMessage() + ". Trying url: " + destinationUrlString2_);

			// If 2nd attempt throws exception, let it cascade up
			if (StringUtils.isNotBlank(destinationUrlString2_))
			{
				response = sendRequest(destinationUrlString2_, request);
			}
		}
		return (response);
	}

	public final static String ERROR_CODE_HEADER_NAME = "Error-Code";

	private String sendRequest(String url, String request) throws CardTransactionException
	{
		HttpURLConnection connection = null;
		try
		{
			URL theURL = new URL(url);
			connection = (HttpURLConnection)theURL.openConnection();
			// connection.setRequestMethod("POST");
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);
			connection.setRequestProperty("Auth-MID", accountId);
			connection.setRequestProperty("Auth-TID", terminalId);
			connection.setRequestProperty("Auth-User", user);
			connection.setRequestProperty("Auth-Password", password);
			connection.setRequestProperty("Stateless-Transaction", "true");
			connection.setRequestProperty("Content-Type", "UTF197/HCS");
			connection.setConnectTimeout(35000);
			connection.setReadTimeout(35000);

			// comment out when done certification
			// FYI: leave code for future certifications
			// logger__.debug("Request Headers:");
			// Map map = connection.getRequestProperties();
			// Iterator it = map.entrySet().iterator();
			// while (it.hasNext())
			// {
			// Map.Entry entry = (Map.Entry)it.next();
			// logger__.debug(entry.getKey() + " - " + entry.getValue());
			// }

			DataOutputStream out = new DataOutputStream(connection.getOutputStream());
			connection.connect();
			out.writeBytes(request);
			out.flush();
			out.close();

			// comment out when done certification
			// FYI: leave code for future certifications
			// logger__.debug("Response Headers:");
			// map = connection.getHeaderFields();
			// it = map.entrySet().iterator();
			// while (it.hasNext())
			// {
			// Map.Entry entry = (Map.Entry)it.next();
			// logger__.debug(entry.getKey() + " - " + entry.getValue());
			// }

			checkForResponseError(connection, url);

			InputStream is = (connection.getInputStream());
			BufferedReader b = new BufferedReader(new InputStreamReader(is));
			String s = null;
			String response = null;
			while ((s = b.readLine()) != null)
			{
				response += s;
			}
			b.close();

			// comment out when done certification
			// FYI: leave code for future certifications
			// boolean boo = false;
			// if (boo)
			// throw new CardTransactionException();

			return (response);
		}
		catch (IOException e)
		{
			throw new CardTransactionException("Paymentech: " + url + " is offline", e);
		}
	}

	private void checkForResponseError(HttpURLConnection connection, String url)
			throws CardTransactionException, CardTransactionException
	{
		try
		{
			if (connection.getResponseCode() == 412)
			{
				String error_code = connection.getHeaderField(ERROR_CODE_HEADER_NAME);

				String error_string = null;
				if (error_code.equals("507"))
				{
					error_string = "Unknown Merchant Id";
				}
				else if (error_code.equals("514"))
				{
					error_string = "Invalid Password";
				}
				else if (error_code.equals("519"))
				{
					error_string = "MID/TID in payload does not match MIME headers";
				}
				else
				{
					error_string = "Unknown Error Code: " + error_code;
				}

				throw new CardTransactionException("Merchant account misconfigured: "
						+ getMerchantAccount().getId() + " with error: " + error_string);
			}
		}
		catch (IOException e)
		{
			throw new CardTransactionException("Paymentech: " + url + " is offline", e);
		}
	}

	private int extractPartialAuthorizationAmount(String messageType, String response, int orgAmountCents)
	{
		if (TRANS_CODE_AUTH.equals(messageType) || TRANS_CODE_SALE.equals(messageType))
		{
			String[] tokens = response.split(new String(new char[] {FIELD_SEPARATOR}));

			// search for CP token
			// tokens start after the 10th field separator
			for(int i=10; i<tokens.length; i++)
			{
				String token = tokens[i];
				// if token length is < 16, approved amount is not available from the response
				// that means a full approval, which means return orgAmountCents
				if (token.startsWith(TOKEN_CP))
				{
					if (token.length() >= 16)
					{
						String approvedAmountString = token.substring(4, 16);
						int approvedCents = Integer.parseInt(approvedAmountString);
						if (logger__.isDebugEnabled())
						{
							logger__.debug("Partial approved amount: " + approvedCents);
						}
						return approvedCents;
					}
					else
					{
						return orgAmountCents;
					}
				}
			}
		}
		return orgAmountCents;
	}

	private Object[] handleChargeResponse(String response, ProcessorTransaction ptd, String reference_number, Track2Card track2Card) throws CardTransactionException
	{
		logger__.info("Charge Response: " + response);
		
		int stx_location = response.indexOf(STX);
		String message = response.substring(stx_location + 1);

		String action_code = message.substring(0, 1);

		String auth_code;
		if (action_code.equals(ACTION_CODE_APPROVED))
		{
			float approvedAmountInCents = extractPartialAuthorizationAmount(TRANS_CODE_SALE, response, ptd.getAmount());
			if (approvedAmountInCents < ptd.getAmount())
			{
				// Partial approved. Decline and send reversal
				action_code = ACTION_CODE_ERROR;
				auth_code = ERROR_CODE_OVER_CREDIT_LIMIT;
				createAndSendReversal(TRANS_CODE_SALE, CardProcessingUtil.getCentsAmountInDollars(ptd.getAmount()), reference_number, track2Card,
						getMerchantAccount().getId(), ptd.getPurchasedDate(), ptd.getTicketNumber(),
						ptd.getPointOfSale().getId());
			}
			else
			{
				auth_code = message.substring(2, 8);
			}
		}
		else
		{
			auth_code = message.substring(2, 5);
		}

		String proc_trans_id = message.substring(14, 22);
		String seq_number = message.substring(22, 28);

		String response_message = message.substring(28, 60);
		StringBuilder sb = new StringBuilder(200);
		sb.append("Charge Response:  ActionCode: ");
		sb.append(action_code);
		sb.append(", Response Message: ");
		sb.append(response_message.trim());
		sb.append(", Auth/Error Code: ");
		sb.append(auth_code);
		sb.append(", ReferenceNumber: ");
		sb.append(seq_number);
		sb.append(", ProcessorTransactionId: ");
		sb.append(proc_trans_id);
		logger__.info(sb.toString());
				
		// Set processor transaction data values
		ptd.setProcessingDate(new Date());
		ptd.setReferenceNumber(seq_number);
		ptd.setProcessorTransactionId(proc_trans_id);

		// Approved
		if (action_code.equals(ACTION_CODE_APPROVED))
		{
			ptd.setAuthorizationNumber(auth_code);
			ptd.setIsApproved(true);
			return new Object[] {DPTResponseCodesMapping.CC_APPROVED_VAL__7000, action_code};
		}

		// Declined
		ptd.setAuthorizationNumber(null);
		ptd.setIsApproved(false);
		return new Object[] {mapErrorResponse(auth_code), action_code};
	}

	public void processReversal(Reversal reversal, Track2Card track2Card) throws CardTransactionException
	{
		String reference_number = getCommonProcessingService().getNewReferenceNumber(getMerchantAccount());

		String request = createReversalRequest(track2Card, reference_number, reversal.getOriginalReferenceNumber(), reversal.getOriginalMessageType(), reversal.getOriginalTransactionAmountInDollars().floatValue(), reversal.isIsRfid());

		String response = null;
		try {
		    response = sendAndRetryRequest(request);
		} catch (CardTransactionException cte) {
		    // Call incrementRetryAttempts and setLastRetryTime.
		    updateReversal(reversal);
		    throw new CardTransactionException(cte);
		}

		logger__.info("Reversal response message is: " + response);


		// Extract data from the response
		extractResponses(response);

		StringBuilder sb = new StringBuilder(200);
		sb.append("Reversal Response:  ActionCode: ");
		sb.append(actionCode);
		sb.append(", Response Message: ");
		sb.append(responseMessage.trim());
		sb.append(", Authorization Number: ");
		sb.append(authOrErrorCode);
		sb.append(", ReferenceNumber: ");
		sb.append(seqNumber);
		sb.append(", ProcessorTransactionId: ");
		sb.append(processorTransactionID);
		logger__.info(sb.toString());

		// Call incrementRetryAttempts and setLastRetryTime.		
		updateReversal(reversal);

		// Check for return code for valid reversal response
		if (actionCode.equals(ACTION_CODE_APPROVED))
		{
			reversal.setIsSucceeded(true);
			reversal.setLastResponseCode(authOrErrorCode);
			return;
		}

		// Not approved

		// Allowable Errors - ie. don't need to retry
		// REF NOT FOUND, VOID NOT ALLOWED, REV NOT REQUIRED
		if (authOrErrorCode.equals("405") || authOrErrorCode.equals("404") || authOrErrorCode.equals("473"))
		{
			reversal.setIsSucceeded(true);
		}

		reversal.setLastResponseCode(authOrErrorCode);
	}

	private StringBuilder createBaseRequest(String messageType, Track2Card track2Card, String refNumber, boolean isRFIDData)
	{
		String sequence_number = refNumber.substring(refNumber.length() - 6, refNumber.length());
		StringBuilder sb = new StringBuilder();

		// A - Header Information
		sb.append(STX);
		sb.append("L.");
		sb.append(ROUTING_INDICATOR);
		sb.append(clientNumber);
		sb.append(accountId);
		sb.append(terminalId);
		sb.append(SINGLE_TRANSACTION);
		sb.append(sequence_number);
		sb.append(TRANSACTION_CLASS);
		sb.append(messageType);
		sb.append(NO_PIN_ENTRY);

		// B - Cardholder Information
		String card_data = null;
		if (isRFIDData)
		{
			if (track2Card.isCardDataCreditCardTrack2Data() && !TRANS_CODE_REVERSAL.equals(messageType))
			{
				sb.append(ENTRY_DATA_SOURCE_CONTACTLESS_DEVICE_TRACK2);
				card_data = track2Card.getDecryptedCardData();
			}
			else
			{
				sb.append(ENTRY_DATA_SOURCE_CONTACTLESS_DEVICE_MANUALLY_KEYED);
				card_data = track2Card.getPrimaryAccountNumber();
				card_data += FIELD_SEPARATOR;
				card_data += track2Card.getExpirationMonth();
				card_data += track2Card.getExpirationYear();
			}
		}
		else if (track2Card.isCardDataCreditCardTrack2Data() && !TRANS_CODE_REVERSAL.equals(messageType))
		{
			sb.append(ENTRY_DATA_SOURCE_TRACK2);
			card_data = track2Card.getDecryptedCardData();
		}
		else
		{
			sb.append(ENTRY_DATA_SOURCE_MANUALLY_ENTERED);

			card_data = track2Card.getPrimaryAccountNumber();
			card_data += FIELD_SEPARATOR;
			card_data += track2Card.getExpirationMonth();
			card_data += track2Card.getExpirationYear();
		}
		sb.append(card_data);
		sb.append(FIELD_SEPARATOR);

		return (sb);

	}

	private void appendTransactionFieldsAndEnd(StringBuilder sb, String messageType, Track2Card track2Card,
			String authCode, Float amount, String refNumber)
	{
		// C - Transaction Information
		// make sure that amount is of the format x.xx
		NumberFormat df = new DecimalFormat("0.00");
		sb.append(df.format(amount));
		sb.append(TRANS_INFO_FILLER);

		// F - Industry Specific(Optional)
		sb.append(INDUSTRY_RETAIL);
		sb.append(INVOICE_NUMBER);
		sb.append(ITEM_CODE);

		// G - Miscellaneous information
		sb.append(FIELD_SEPARATOR);
		if (messageType.equals(TRANS_CODE_PRIOR_SALE_FORCE))
		{
			sb.append(authCode);
		}
		sb.append(FIELD_SEPARATOR);
		sb.append(FIELD_SEPARATOR);

		// H - Address verification service
		sb.append(FIELD_SEPARATOR);
		sb.append(FIELD_SEPARATOR);

		// I - Purchase card(Optional)
		sb.append(FIELD_SEPARATOR);

		// J - Token data
		// append FIELD_SEPARATOR if using track2 card data
		if (track2Card.isCardDataCreditCardTrack2Data())
		{
			sb.append(FIELD_SEPARATOR);
		}

		sb.append(TOKEN_REF_NUMBER);
		sb.append(refNumber);
		sb.append(FIELD_SEPARATOR);

		sb.append(TOKEN_DUPLICATE_CHECK_AND_VALUE);
		sb.append(FIELD_SEPARATOR);

		appendTokenP1P2(sb);

		if (TRANS_CODE_AUTH.equals(messageType) || TRANS_CODE_SALE.equals(messageType) || TRANS_CODE_RETURN.equals(messageType))
		{
			appendTokenCP(sb);
		}

		if (track2Card.isCardDataCreditCardTrack2Data() && !TRANS_CODE_REVERSAL.equals(messageType) && !TRANS_CODE_RETURN.equals(messageType))
		{
			/* pay stations are considered as a CAT Level 3. */
			sb.append(TOKEN_UNATTENED_TERMINAL_CODE);
			sb.append(TOKEN_UNATTENED_TERMINAL_VALUE_CAT_LEVEL_3);
			sb.append(FIELD_SEPARATOR);
		}
		
		// End message
		sb.append(ETX);
	}

	private void appendTokenP1P2(StringBuilder sb)
	{
		sb.append(TOKEN_P1);
		sb.append(P1_VENDOR_ID);
		sb.append(P1_SOFTWARE_ID);
		sb.append(P1_HARDWARE_SERIAL_NUM);
		sb.append(FIELD_SEPARATOR);

		sb.append(TOKEN_P2);
		sb.append(P2_BITMAP);
		sb.append(P2_VALUE);
		sb.append(P3_VALUE);
		sb.append(P5_VALUE);
		sb.append(P7_VALUE);
		sb.append(P9_VALUE);
		sb.append(P11_VALUE);
		sb.append(FIELD_SEPARATOR);
	}

	private void appendTokenCP(StringBuilder sb)
	{
		sb.append(TOKEN_CP);
		sb.append(CP_VALUE);
		sb.append(FIELD_SEPARATOR);
	}

	private String createReversalRequest(Track2Card track2Card, String refNumber, String origRefNumber,
			String origTransCode, Float amount, boolean isRFID)
	{
		StringBuilder sb = createBaseRequest(TRANS_CODE_REVERSAL, track2Card, refNumber, isRFID);

		// C - Transaction Information
		// make sure that amount is of the format xxxxx.xx
		NumberFormat df = new DecimalFormat("00000.00");
		sb.append(df.format(amount));
		sb.append(TRANS_INFO_FILLER_REVERSAL);

		// J - Token data
		sb.append(TOKEN_TRANS_CODE);
		sb.append(origTransCode);
		sb.append(FIELD_SEPARATOR);

		sb.append(TOKEN_REF_NUMBER);
		sb.append(origRefNumber);
		sb.append(FIELD_SEPARATOR);

		// End message
		sb.append(ETX);

		String request = sb.toString();

		// log outgoing message
		String log_message = request.replaceAll(track2Card.getPrimaryAccountNumber(), track2Card.getPrimaryAccountNumber().replaceAll("[^=]", "X"));
		logger__.info("Reversal Request: " + log_message);
		
		return (request);
	}

	private String createAuthorizationRequest(Track2Card track2Card, String refNumber, PreAuth preAuth)
	{
		Float amount = CardProcessingUtil.getCentsAmountInDollars(preAuth.getAmount()).floatValue();
		StringBuilder sb = createBaseRequest(TRANS_CODE_AUTH, track2Card, refNumber, preAuth.isIsRfid());
		appendTransactionFieldsAndEnd(sb, TRANS_CODE_AUTH, track2Card, null, amount, refNumber);

		String request = sb.toString();

		String log_message = request.replaceAll(track2Card.getDecryptedCardData(), track2Card.getDecryptedCardData().replaceAll(
				"[^=]", "X"));
		
		logger__.info("Auth request: " + log_message);
	
		return (request);
	}

	private String createReturnRequest(Track2Card track2Card, String refNumber, Float amount, boolean isRFID)
	{
		StringBuilder sb = createBaseRequest(TRANS_CODE_RETURN, track2Card, refNumber, isRFID);
		appendTransactionFieldsAndEnd(sb, TRANS_CODE_RETURN, track2Card, null, amount, refNumber);

		String request = sb.toString();

		String log_message = request.replaceAll(track2Card.getCreditCardPanAndExpiry(), track2Card.getCreditCardPanAndExpiry().replaceAll("[^=]", "X"));
		logger__.info("Refund request: " + log_message);		

		return (request);
	}

	private String createPriorSaleForceRequest(Track2Card track2Card, String refNumber, PreAuth preAuth)
	{
		Float amount = CardProcessingUtil.getCentsAmountInDollars(preAuth.getAmount()).floatValue();
		String authCode = preAuth.getAuthorizationNumber();
		StringBuilder sb = createBaseRequest(TRANS_CODE_PRIOR_SALE_FORCE, track2Card, refNumber, preAuth.isIsRfid());
		appendTransactionFieldsAndEnd(sb, TRANS_CODE_PRIOR_SALE_FORCE, track2Card, authCode, amount, refNumber);

		String request = sb.toString();

		String log_message = request.replaceAll(track2Card.getPrimaryAccountNumber(), track2Card
				.getPrimaryAccountNumber().replaceAll("[^=]", "X"));
	
		logger__.info("Settle request: " + log_message);
	
		return (request);
	}

	private String createSaleRequest(Track2Card track2Card, String refNumber, Float amount, boolean isRFIDData)
	{
		StringBuilder sb = createBaseRequest(TRANS_CODE_SALE, track2Card, refNumber, isRFIDData);
		appendTransactionFieldsAndEnd(sb, TRANS_CODE_SALE, track2Card, null, amount, refNumber);

		String request = sb.toString();
			
		// log outgoing message
		if (track2Card.isCardDataCreditCardTrack2Data())
		{
			String log_message = request.replaceAll(track2Card.getDecryptedCardData(), track2Card.getDecryptedCardData().replaceAll(
					"[^=]", "X"));
			logger__.info("Charge Request: " + log_message);
		}
		else
		{
			String log_message = request.replaceAll(track2Card.getPrimaryAccountNumber(), track2Card.getPrimaryAccountNumber().replaceAll("[^=]", "X"));
			logger__.info("Charge Request: " + log_message);
		}
	
		return (request);
	}

	private void extractResponses(String responseString)
	{
		if (logger__.isInfoEnabled()) 
		{
			logger__.info("Response : " + responseString);
		}
		
		int stx_location = responseString.indexOf(STX);
		String message = responseString.substring(stx_location + 1);

		actionCode = message.substring(0, 1);

		if (actionCode.equals(ACTION_CODE_APPROVED))
		{
			authOrErrorCode = message.substring(2, 8);
		}
		else
		{
			authOrErrorCode = message.substring(2, 5);
		}
		processorTransactionID = message.substring(14, 22);
		seqNumber = message.substring(22, 28);
		responseMessage = message.substring(28, 60);
	}

	private int mapErrorResponse(String responseCode) throws CardTransactionException
	{
		int response_code = -1;
		try
		{
			response_code = Integer.parseInt(responseCode);
		}
		catch (Exception e)
		{
			throw new CardTransactionException(
					"Expecting numeric response, but recieved: " + responseCode, e);
		}

		switch (response_code)
		{
			// Auth Declined: Cardholder's bank did not approve transaction
			case 200:
				// Invalid ICA No: Invalid International Control Account number
			case 206:
				// Invalid ABA No: Invalid American Banking Association number
			case 207:
				// Invalid Bank MID: The Bank merchant ID is incorrect
			case 209:
				// Over Credit Flr: Amount requested exceeds credit limit
			case 217:
				// No checking account: ?? Not listed in error codes
			case 266:
				return (DPTResponseCodesMapping.CC_DECLINED_VAL__7003);

				// Call Voice Oper: Authorizer needs more information for approval
			case 201:
				// Hold - Call: Card issuer does not want that card used. Call for further instructions
			case 202:
				// Call Voice Oper: Authorizer didn't respond within alotted time
			case 203:
				// Call Voice Oper: Authorization Center cannot be reached
			case 214:
				return (DPTResponseCodesMapping.CC_PLACE_CALL_VAL__7022);

				// Invalid Card No: Account #/Mag stripe is invalid
			case 204:
				// Invalid Card: Mag stripe contains invalid data or account # is > 19 digits
			case 302:
				return (DPTResponseCodesMapping.CC_BAD_CREDIT_CARD_DATA_VAL__7045);

				// Invalid PIN: Personal ID code is incorrect
			case 216:
				// CVC2/CID Error: CVC2 or CID entered was not valid for the card number
			case 248:
				return (DPTResponseCodesMapping.CC_UNABLE_TO_AUTHENTICATE_VAL__7055);

				// Card Not Allowed: Merchant does not accept this card
			case 225:
				// Card Not Allowed: Merchant cannot accept this card
			case 228:
				// Dscv Not Allowed: Merchant not set up for Discover
			case 430:
				// AX Not Allowed: AMEX not allowed
			case 454:
				return (DPTResponseCodesMapping.CC_CARD_TYPE_NOT_SETUP_VAL__7076);

				// Invalid Exp. date: Expiration date is either incorrect format or prior to today
			case 205:
				// Invalid Exp. Date: Card has expired, month was not 01-12 or year was not 00-99
			case 303:
				return (DPTResponseCodesMapping.CC_EXPIRED_VAL__7001);

				// Invalid Term ID: The length of the merchant ID is incorrect or contains invalid data
			case 300:
				// Invalid Term ID: Merchant ID not found in merchant file
			case 400:
				// Invalid Term ID: Merchant ID not found in terminal file
			case 401:
				throw new CardTransactionException("Response indicates invalid terminal Id: " + responseCode);

				// Invalid Amount: Amount is either 0, has no decimal, decimal in wrong place or multiple
				// decimals
			case 211:
				// Amt Entry Error: Amount is less than 0.01 or greater than 999999.99 or contained non
				// numeric data
			case 305:
				return (DPTResponseCodesMapping.CC_INVALID_AMOUNT_VAL__7033);

				// Lost/Stolen Card: Card has been reported lost or stolen
			case 215:
				return (DPTResponseCodesMapping.CC_LOST_STOLEN_VAL__7006);

				// Request Denied: Transaction is not valid for this authorizer
			case 218:
				return DPTResponseCodesMapping.CC_INVALID_REQUEST_VAL__7058;

				// Not Online To XX: Fatal communications error
			case 220:
				throw new CardTransactionException("Response indicates processor offline: " + responseCode);

				// Invalid Merchant: Merchant number not on file
			case 999:
				throw new CardTransactionException("Response indicates invalid merchant: " + responseCode);
				
            case 233:
                return DPTResponseCodesMapping.CC_TERMINAL_TYPE_NOT_SUPPORTED_VAL_7101;

            case 412:
                return DPTResponseCodesMapping.CC_UNABLE_TO_READ_REF_NUM_FILE_VAL_7100;				
		}

		throw new CardTransactionException("Unknown Response Code: " + responseCode);
	}


	@Override
	public boolean isReversalExpired(Reversal reversal) {
		return (reversal.getRetryAttempts() >= EMS_MAX_REVERSAL_RETRIES);
	}
	
	private Reversal updateReversal(final Reversal reversal) {
	    reversal.incrementRetryAttempts();
	    reversal.setLastRetryTime(new Date());
	    return reversal;
	}
}
