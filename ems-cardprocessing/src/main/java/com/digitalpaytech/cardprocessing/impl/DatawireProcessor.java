package com.digitalpaytech.cardprocessing.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.digitalpaytech.cardprocessing.CardProcessor;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MerchantAccountService;

public abstract class DatawireProcessor extends CardProcessor 
{
	private final static Logger logger__ = Logger.getLogger(DatawireProcessor.class);
	// Provided by Datawire and hardcoded. (unique to our app??)
	protected String DATAWIRE_APPLICATION_ID = "DPMTCLIENTDGPJAV";
	protected String DATAWIRE_SERVICE_ID = "303";

	// common to payment processor/Datawire - configurable via Merchant Account
	// setup page
	private String merchantId;
	private String terminalId;
	// provided by Datawire through self-registration
	private List urlList;
	private String datawireId;

	// provided by Datawire
	private String selfRegistrationUrl;

	public DatawireProcessor(MerchantAccount md, CommonProcessingService commonProcessingService,
	        EmsPropertiesService emsPropertiesService) 
	{
		super(md, commonProcessingService, emsPropertiesService);
		selfRegistrationUrl = commonProcessingService.getDestinationUrlString(md.getProcessor());
		if (logger__.isDebugEnabled()) 
		{
			logger__.debug("DatawireProcessor, selfRegistrationUrl: "+ selfRegistrationUrl);
		}
	}
	
	public DatawireProcessor(MerchantAccount md,
	        CommonProcessingService commonProcessingService,
	        EmsPropertiesService emsPropertiesService,
	        MerchantAccountService merchantAccountService) 
	{
		super(md, commonProcessingService, emsPropertiesService);
		setMerchantAccountService(merchantAccountService);

		selfRegistrationUrl = commonProcessingService.getDestinationUrlString(md.getProcessor());
		if (logger__.isDebugEnabled()) 
		{
			logger__.debug("DatawireProcessor, selfRegistrationUrl: "+ selfRegistrationUrl);
		}
	}

	protected String getDatawireId() {
		return (datawireId);
	}

	protected void setDatawireId(String datawireId) 
	{
		this.datawireId = datawireId;
	}

	protected String getMerchantId() 
	{
		return (merchantId);
	}

	protected void setMerchantId(String merchantId) 
	{
		this.merchantId = merchantId;
	}

	protected String getTerminalId() 
	{
		return (terminalId);
	}

	protected void setTerminalId(String terminalId) 
	{
		this.terminalId = terminalId;
	}

	protected List getUrlList() 
	{
		return urlList;
	}

	protected void setUrlList(List urlList) 
	{
		this.urlList = urlList;
	}

	public String getSelfRegistrationUrl() 
	{
    	return selfRegistrationUrl;
    }

	// Sends/receives requests to/from Datawire
	protected char[] sendRequest(char[] message, String referenceNumber) throws CardTransactionException {
		try 
		{
			if (logger__.isDebugEnabled()) 
			{
				logger__.debug("Sending request (MID: " + merchantId + ", TID: "
				        + terminalId + ", DID: " + datawireId
				        + ", DATAWIRE_SERVICE_ID: " + DATAWIRE_SERVICE_ID
				        + ", DATAWIRE_APPLICATION_ID: "
				        + DATAWIRE_APPLICATION_ID + ")");
				logger__.debug("URL List: " + urlList);
			}

			System.setProperty("java.protocol.handler.pkgs", "javax.net.ssl");

			net.datawire.vxn3.VXN vxn = net.datawire.vxn3.VXN.getInstance(
			        urlList, datawireId, merchantId, terminalId,
			        DATAWIRE_SERVICE_ID, DATAWIRE_APPLICATION_ID);

			net.datawire.vxn3.SimpleTransaction tr = vxn
			        .newSimpleTransaction(referenceNumber);

			// Set the payload
			tr.setPayload(message);

			// Execute
			tr.executeXmlRequest();

			/*
			 * boolean done; int i; String s, message_string = ""; char []
			 * payload = tr.getPayload(); for(i = 0, done=false; !done; i++) {
			 * try { message_string += Integer.toHexString((int)payload[i]) +
			 * " "; } catch (ArrayIndexOutOfBoundsException e1) { done = true; }
			 * } logger__.info(message_string);
			 */

			// Get and return response
			return (tr.getPayload());
		} 
		catch (net.datawire.vxn3.VXNException e) 
		{
			if (e.getMessage().equals("No usable URL returned from ServiceDiscovery!")) 
			{
				logger__.info("send request failure:", e);
				throw new CardTransactionException(
				        "Invalid merchant account configuration", e);
			}
			throw new CardTransactionException("Datawire is offline.", e);
		} catch (Exception e) 
		{
			throw new CardTransactionException("Datawire is offline.", e);
		}
	}

	/*
	 * Performs registration of the merchant with the Datawire server and
	 * activates the merchant. The DID is obtained from Datawire once only for a
	 * given merchant. Self-activation must be done soon (within 10 minutes?) of
	 * obtaining the DID in order for the customer account to be active
	 */
	protected void selfRegisterMerchant(boolean useTestServer, MerchantAccount md) 
		throws CardTransactionException 
	{
		// Check to see if already registered
		if (datawireId != null && datawireId.length() > 0) 
		{
			return;
		}

		// Self register with Datawire and activate the merchant
		try 
		{
			StringBuilder bdr = new StringBuilder();
			bdr.append("Self-registering (MID: ").append(merchantId).append(", TID: ").append(terminalId).append(" SID: ").append(DATAWIRE_SERVICE_ID).append(" ApplicationID: ");
			bdr.append(DATAWIRE_APPLICATION_ID).append(") @ ").append(selfRegistrationUrl);
			logger__.info(bdr.toString());

			System.setProperty("java.protocol.handler.pkgs", "javax.net.ssl");

			net.datawire.vxn3.SelfRegistration srs = new net.datawire.vxn3.SelfRegistration(
			        selfRegistrationUrl, merchantId, terminalId,
			        DATAWIRE_SERVICE_ID, DATAWIRE_APPLICATION_ID);

			srs.setMaxRegisterAttempts(10);
			srs.setRegisterAttemptsWaitMilliseconds(15000);

			datawireId = srs.registerMerchant();
			srs.activateMerchant();
			urlList = srs.getUrlList();

			logger__.debug("URL List received from self-registration: "+ urlList);
			logger__.debug("Datawire ID recieved from self-registration: "+ datawireId);

		} catch (net.datawire.vxn3.VXNException e) 
		{
			logger__.error("self-register merchant failure:", e);
			throw new CardTransactionException("Unable to self-register merchant due to: " + e.getMessage(), e);
		}

		// Add DID entry to the database
		// Add service discovery URLs to the database
		md.setField3(getDatawireId());
		if (!getUrlList().isEmpty()) 
		{
			String sd_url_list_string = "";
			for (int i = 0; i < getUrlList().size(); i++) 
			{
				sd_url_list_string += getUrlList().get(i).toString() + "|";
			}
			md.setField4(sd_url_list_string.substring(0,
			        sd_url_list_string.length() - 1));
		}
		getMerchantAccountService().updateMerchantAccount(md);
	}

	// This function creates a string <fieldLength> characters long, padded with
	// zeroes on the
	// left if necessary.
	protected String formatDollarAmount(int amountInCents, int fieldLength) 
	{
		String amount = Integer.toString(amountInCents);

		String leading_zeros = "";
		for (int i = 0; i < fieldLength; i++) 
		{
			leading_zeros += "0";
		}

		// Add some leading zeros so that the field length is fieldLength
		String formatted_dollar_amount = leading_zeros.substring(amount.trim().length()) + amount;
		return formatted_dollar_amount;
	}

}
