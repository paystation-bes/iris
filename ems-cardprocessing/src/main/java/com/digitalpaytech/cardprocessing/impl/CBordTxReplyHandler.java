package com.digitalpaytech.cardprocessing.impl;

/**
 * This class is the Reply handler class for XSTREAM implementation for
 * CBORD XML processing.
 * 
 * @author harshd
 *
 */
public class CBordTxReplyHandler {

	private CBordTransactionReply replyTx;
	private CBordBalanceReqReply balanceReq;

	public CBordTransactionReply getReplyTx() {
		return replyTx;
	}

	public CBordBalanceReqReply getBalanceReq() {
		return balanceReq;
	}
}
