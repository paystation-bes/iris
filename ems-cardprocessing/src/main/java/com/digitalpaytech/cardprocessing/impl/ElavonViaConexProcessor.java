package com.digitalpaytech.cardprocessing.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.digitalpaytech.cardprocessing.CardProcessor;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.ActiveBatchSummary;
import com.digitalpaytech.domain.BatchSettlement;
import com.digitalpaytech.domain.BatchSettlementDetail;
import com.digitalpaytech.domain.BatchSettlementType;
import com.digitalpaytech.domain.BatchStatusType;
import com.digitalpaytech.domain.Cluster;
import com.digitalpaytech.domain.ElavonRequestType;
import com.digitalpaytech.domain.ElavonTransactionDetail;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.domain.TransactionSettlementStatusType;
import com.digitalpaytech.dto.BatchSettlementDTO;
import com.digitalpaytech.dto.ElavonViaConexDTO;
import com.digitalpaytech.exception.BatchCloseException;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.CreditCardTestNumberService;
import com.digitalpaytech.service.processor.ElavonViaConexService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.CryptoUtil;
import com.digitalpaytech.util.DPTResponseCodesMapping;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.xml.ElavonRequestBlock;
import com.digitalpaytech.util.xml.ElavonResponseBlock;
import com.digitalpaytech.util.xml.ElavonViaConexRequest;
import com.digitalpaytech.util.xml.ElavonViaConexResponse;
import com.digitalpaytech.util.xml.XMLUtil;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.basic.BooleanConverter;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;

public class ElavonViaConexProcessor extends CardProcessor {
    
    private static final String REQUEST_TYPE_CREDIT_CARD_SALE = "Credit Card.Sale";
    private static final String REQUEST_TYPE_CREDIT_CARD_REVERSAL = "Credit Card.Reversal";
    private static final String REQUEST_TYPE_BATCH_CURRENT_BATCH = "Batch.Current Balance";
    private static final String REQUEST_TYPE_BATCH_DETAIL_HEADER = "Batch.Detail Header";
    private static final String REQUEST_TYPE_BATCH_DETAIL = "Batch.Detail";
    private static final String REQUEST_TYPE_BATCH_DETAIL_TRAILER = "Batch.Detail Trailer";
    
    private static final String ERROR_STR = "ERROR";
    
    private static final String LOG_ERROR_PAN_EXP = "Unable to encrypt PAN and Expiry Date, ";
    
    private static final String COMMA_W_SPACE = ", ";
    
    private static final String FALSE_NUMERIC_OR_ZERO_STRING_VALUE = "0";
    private static final String TRUE_NUMERIC_OR_ONE_STRING_VALUE = "1";
    private static final String DECIMAL_FORMAT_ZERO_THREE = "03";
    private static final String DECIMAL_FORMAT_TWO_DIGITS = "00";
    private static final String DECIMAL_FORMAT_THREE_DIGITS = "000";
    private static final String APPROVED_ALL = "AA";
    
    private static final short POS_ENTRY_CAPABILITY_MANUAL_ENTRY_ONLY = 1;
    private static final short POS_ENTRY_CAPABILITY_MAGNETICALLY_SWIPE = 2;
    private static final short ACCOUNT_ENTRY_MODE_KEY_ENTERED_CARD_NOT_PRESENT = 1;
    private static final short ACCOUNT_ENTRY_MODE_SWIPED = 3;
    private static final short HEADER_TRAILER_COUNT = 2;
    
    private static final String ACCOUNT_SOURCE_SWIPED = "2";
    
    private static final short PAN_EXPIRY_OFFSET = 4;
    
    private static final short MIMIC_REQUEST_TIME = 1000;
    
    private static final short PREAUTH_EXPIRED = 1;
    
    private static final int ONE_DOLLAR_TEN_CENTS = 110;
    private static final int ELAVON_REQUEST_TYPE_CREDIT_CARD_SALE = 1;
    private static final int ELAVON_REQUEST_TYPE_CREDIT_CARD_REVERSAL = 3;
    private static final int ELAVON_REQUEST_TYPE_CREDIT_CARD_RETURN = 4;
    
    private static final int DEFAULT_REAL_TIME_ELAVON_RETRY_ATTEMPTS = 3;
    private static final int DEFAULT_ELAVON_RETRY_WAITING_TIME = 1000;
    
    private static final Pattern PTTRN_STRING_RESPONSE_EXTRACTOR = Pattern.compile("<Response>(.*)</Response>");
    
    private static final Logger LOG = Logger.getLogger(ElavonViaConexProcessor.class);
    
    private static XStream xstream;
    
    private static Pattern panExpDigsPattern = Pattern.compile(WebCoreConstants.REGEX_PAN_EXP_DIGIT_COUNT);
    
    private static Integer writeConnTimeoutMsRealTime;
    private static Integer readConnTimeoutMsRealTime;
    private static Integer writeConnTimeoutMsSinglePhase;
    private static Integer readConnTimeoutMsSinglePhase;
    private static Integer writeConnTimeoutMsCloseBatch;
    private static Integer readConnTimeoutMsCloseBatch;
    private static Integer writeConnTimeoutMsDefault;
    private static Integer readConnTimeoutMsDefault;
    private static Integer expireMsDefault;
    
    private static int noOfRetries;
    private static int waitInterval;
    private static int reversalMaxRetries;
    private static int reversalTimeoutMaxRetries;
    
    private static String applicationId;
    private static String registrationKey;
    private static String specVersion;
    
    private ElavonViaConexService elavonViaConexService;
    
    private ElavonTransactionDetail elavonTransactionDetail;
    
    private ActiveBatchSummary activeBatchSummary;
    
    private boolean saveActiveBatchSummary;
    
    private boolean isReversal;
    
    private boolean fromProcessCharge;
    
    private ProcessorTransaction processorTransaction;
    private MessageHelper messageHelper;
    private CreditCardTestNumberService testCreditCardNumberService;
    
    static {
        /*
         * When using XStream to serialize Java object into XML, alias name with underscore (_) will get duplicate (__).
         * Replace $ with " " and underscores with underscore
         */
        xstream = new XStream(new DomDriver("UTF-8", new XmlFriendlyNameCoder(" ", "_")));
        xstream.processAnnotations(ElavonViaConexRequest.class);
        xstream.processAnnotations(ElavonViaConexResponse.class);
        xstream.registerConverter(new BooleanConverter(TRUE_NUMERIC_OR_ONE_STRING_VALUE, FALSE_NUMERIC_OR_ZERO_STRING_VALUE, true));
    }
    
    public ElavonViaConexProcessor(final MerchantAccount merchantAccount, final CommonProcessingService commonProcessingService,
            final EmsPropertiesService emsPropertiesService, final CustomerCardTypeService customerCardTypeService,
            final MerchantAccountService merchantAccountService, final ElavonViaConexService elavonViaConexService, final MessageHelper messageHelper,
            final CreditCardTestNumberService testCreditCardNumberService) {
        
        super(merchantAccount, commonProcessingService, emsPropertiesService);
        this.testCreditCardNumberService = testCreditCardNumberService;
        setCustomerCardTypeService(customerCardTypeService);
        setMerchantAccountService(merchantAccountService);
        setIsManualSave(true);
        setElavonViaConexService(elavonViaConexService);
        setEmsPropertiesValues();
        setMessageHelper(messageHelper);
    }
    
    @Override
    public boolean processPreAuth(final PreAuth preAuth) {
        LOG.info("ElavonViaConexProcessor preAuth - Credit Card.Sale");
        
        final String merchantRefNum = removeLeadingZeros(this.getCommonProcessingService().getNewReferenceNumber(preAuth.getMerchantAccount()));
        preAuth.setReferenceId(merchantRefNum);
        
        final List<ElavonRequestBlock> blocks = new ArrayList<ElavonRequestBlock>();
        blocks.add(createBlockHD(preAuth.getMerchantAccount().getField1()));
        // createBlock01(final String posEntryCapability, final String accountEntryMode...)
        blocks.add(createBlock01(POS_ENTRY_CAPABILITY_MAGNETICALLY_SWIPE, ACCOUNT_ENTRY_MODE_SWIPED, preAuth.getCardData(), preAuth.getAmount()));
        blocks.add(createBlock03(preAuth.getReferenceId()));
        
        final ElavonViaConexRequest req = createElavonViaConexRequest(blocks, REQUEST_TYPE_CREDIT_CARD_SALE);
        final String xml = toElavonViaConexRequestXml(req);
        
        final MerchantAccount ma = preAuth.getMerchantAccount();
        final Track2Card track2Card = new Track2Card(
                super.getCustomerCardTypeService().getCardTypeByTrack2Data(super.getMerchantAccount().getCustomer().getId(), preAuth.getCardData()),
                super.getMerchantAccount().getCustomer().getId(), preAuth.getCardData());
        String responseXml = null;
        try {
            responseXml = sendOnlineToServer(xml, this.getCommonProcessingService().getDestinationUrlString(ma.getProcessor()), ma,
                                             writeConnTimeoutMsRealTime, readConnTimeoutMsRealTime);
            
        } catch (IOException ioe) {
            processReversal(preAuth, track2Card);
            final StringBuilder bdr = new StringBuilder(130);
            bdr.append("processPreAuth encountered IOException: ").append(ioe.getMessage()).append(". Performed a processReversal");
            LOG.error(bdr.toString(), ioe);
            throw new RuntimeException(ioe);
        }
        
        final ElavonViaConexResponse res = toElavonViaConexResponse(responseXml, true);
        
        // Change to PAN=Expiry Date??
        preAuth.setCardData(track2Card.getCreditCardPanAndExpiry());
        preAuth.setAuthorizationNumber(res.getDataBlock().getApprovalCode());
        preAuth.setResponseCode(res.getDataBlock().getResponseCode());
        preAuth.setProcessingDate(DateUtil.getCurrentGmtDate());
        preAuth.setReferenceNumber(res.getDataBlock().getTraceNumber());
        preAuth.setProcessorTransactionId(res.getDataBlock().getTransactionCode());
        
        if (this.isApproved(res.getDataBlock().getResponseCode())) {
            preAuth.setIsApproved(true);
            
            String encryptedPanExp = null;
            try {
                encryptedPanExp = this.elavonViaConexService.getCryptoService().encryptData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL,
                                                                                            track2Card.getCreditCardPanAndExpiry());
            } catch (CryptoException ce) {
                LOG.error(ce);
                // Perform Credit Card.Reversal to Elavon.
                this.processReversal(preAuth, track2Card);
                
                return false;
            }
            final ElavonViaConexDTO dto = new ElavonViaConexDTO(preAuth, null,
                    (ElavonRequestType) this.elavonViaConexService.getEntityDao().get(ElavonRequestType.class, ELAVON_REQUEST_TYPE_CREDIT_CARD_SALE),
                    encryptedPanExp, this.elavonViaConexService.getEntityDao()
                            .get(TransactionSettlementStatusType.class, CardProcessingConstants.TRANSACTION_SETTLEMENT_STATUS_TYPE_PREAUTH_ID));
            
            createElavonTransactionDetail(res, dto, track2Card);
            createOrUpdateActiveBatchSummary(res, dto);
            
            return true;
        }
        // Denied
        preAuth.setIsApproved(false);
        
        return false;
    }
    
    private void createOrUpdateActiveBatchSummary(final ElavonViaConexResponse res, final ElavonViaConexDTO elavonViaConexDto) {
        
        setSaveActiveBatchSummary(false);
        final int noOfTransactions;
        
        this.activeBatchSummary = this.elavonViaConexService.getActiveBatchSummaryService()
                .findByBatchNoAndMerchantId(Short.parseShort(res.getDataBlock().getBatchNumber()),
                                            elavonViaConexDto.getMerchantAccount().getId().intValue());
        
        if (this.activeBatchSummary == null) {
            setSaveActiveBatchSummary(true);
            this.activeBatchSummary = new ActiveBatchSummary();
            noOfTransactions = 1;
        } else {
            noOfTransactions = this.activeBatchSummary.getNoOfTransactions() + 1;
            this.elavonViaConexService.getEntityDao().evict(this.activeBatchSummary);
        }
        
        this.activeBatchSummary.setNoOfTransactions(noOfTransactions);
        this.activeBatchSummary.setBatchNumber(Short.parseShort(res.getDataBlock().getBatchNumber()));
        if (this.saveActiveBatchSummary) {
            this.activeBatchSummary.setCreatedGMT(DateUtil.getCurrentGmtDate());
        }
        this.activeBatchSummary.setLastModifiedGMT(DateUtil.getCurrentGmtDate());
        this.activeBatchSummary.setMerchantAccount(elavonViaConexDto.getMerchantAccount());
        
        final BatchStatusType batchStatusType =
                this.elavonViaConexService.getEntityDao().get(BatchStatusType.class, CardProcessingConstants.BATCH_STATUS_TYPE_OPEN);
        this.activeBatchSummary.setBatchstatustype(batchStatusType);
    }
    
    private void createElavonTransactionDetail(final ElavonViaConexResponse res, final ElavonViaConexDTO elavonViaConexDto,
        final Track2Card track2Card) {
        
        this.elavonTransactionDetail = new ElavonTransactionDetail();
        this.elavonTransactionDetail.setApprovalCode(res.getDataBlock().getApprovalCode());
        
        if (null != res.getDataBlock().getAuthorizationDateTime()) {
            this.elavonTransactionDetail.setAuthorizationDateTime(res.getDataBlock().getAuthorizationDateTime());
        } else {
            this.elavonTransactionDetail.setAuthorizationDateTime(DateUtil.getCurrentGmtDate());
        }
        
        char authSource = 0;
        if (StringUtils.isNotBlank(res.getDataBlock().getAuthorizationSource())
            && !WebCoreConstants.EMPTY_STRING.equals(res.getDataBlock().getAuthorizationSource().trim())) {
            authSource = res.getDataBlock().getAuthorizationSource().charAt(0);
        }
        this.elavonTransactionDetail.setAuthorizationSource(authSource);
        if (StringUtils.isNotBlank(res.getDataBlock().getAuthorizedAmount())) {
            this.elavonTransactionDetail.setAuthorizedAmount(Integer.parseInt(res.getDataBlock().getAuthorizedAmount()));
        }
        this.elavonTransactionDetail.setBatchNumber(Short.parseShort(res.getDataBlock().getBatchNumber()));
        this.elavonTransactionDetail.setCardData(elavonViaConexDto.getCardData());
        this.elavonTransactionDetail.setCreatedGmt(DateUtil.getCurrentGmtDate());
        
        if (null != res.getDataBlock().getAvsResponse()) {
            this.elavonTransactionDetail.setAvsResponse(res.getDataBlock().getAvsResponse());
        }
        
        if (null != res.getDataBlock().getCvv2Response()) {
            this.elavonTransactionDetail.setCvv2Response(res.getDataBlock().getCvv2Response());
        }
        
        if (null != track2Card && StringUtils.isNotBlank(track2Card.getServiceCode())) {
            this.elavonTransactionDetail.setServiceCode(Short.parseShort(track2Card.getServiceCode()));
        }
        
        this.elavonTransactionDetail.setElavonRequestType(elavonViaConexDto.getElavonRequestType());
        this.elavonTransactionDetail.setMerchantAccount(elavonViaConexDto.getMerchantAccount());
        if (StringUtils.isNotBlank(res.getDataBlock().getMsdi())) {
            this.elavonTransactionDetail.setMsdi(Byte.parseByte(res.getDataBlock().getMsdi()));
        }
        
        int origAmount = 0;
        if (null != res.getDataBlock().getAuthorizedAmount()) {
            // Authorized_Amount the decimal is implied. ($1 is sent as 100)
            origAmount = Integer.parseInt(res.getDataBlock().getAuthorizedAmount());
        }
        this.elavonTransactionDetail.setOriginalAuthAmount(origAmount);
        this.elavonTransactionDetail.setPointOfSale(elavonViaConexDto.getPointOfSale());
        
        this.elavonTransactionDetail.setPreAuth(elavonViaConexDto.getPreAuth());
        if (null != elavonViaConexDto.getProcessorTransaction() && null != elavonViaConexDto.getProcessorTransaction().getId()) {
            this.elavonTransactionDetail.setProcessorTransaction(elavonViaConexDto.getProcessorTransaction());
        }
        
        this.elavonTransactionDetail.setPs2000data(res.getDataBlock().getPs2000Data());
        this.elavonTransactionDetail.setPurchasedDate(elavonViaConexDto.getPurchasedDate());
        
        if (StringUtils.isNotBlank(res.getDataBlock().getResponseCode())) {
            this.elavonTransactionDetail.setResponseCode(res.getDataBlock().getResponseCode());
        } else if (isGoodBatch(res.getDataBlock().getResponseMessage())) {
            this.elavonTransactionDetail.setResponseCode(CardProcessingConstants.RESULT_OK);
        } else {
            this.elavonTransactionDetail.setResponseCode(CardProcessingConstants.RESULT_DECLINED_BATCH_RESPONSE);
        }
        
        this.elavonTransactionDetail.setTicketNumber(elavonViaConexDto.getTicketNumber());
        if (StringUtils.isNotBlank(res.getDataBlock().getTraceNumber())) {
            this.elavonTransactionDetail.setTraceNumber(Integer.parseInt(res.getDataBlock().getTraceNumber()));
        }
        
        this.elavonTransactionDetail.setTransactionSettlementStatusType(elavonViaConexDto.getTransactionSettlementStatusType());
        if (StringUtils.isNotBlank(res.getDataBlock().getCreditSaleAmount()) || StringUtils.isNotBlank(res.getDataBlock().getCreditReturnAmount())) {
            this.elavonTransactionDetail.setPosentryCapability(new Integer(POS_ENTRY_CAPABILITY_MANUAL_ENTRY_ONLY).byteValue());
            this.elavonTransactionDetail.setAccountEntryMode(new Integer(ACCOUNT_ENTRY_MODE_KEY_ENTERED_CARD_NOT_PRESENT).byteValue());
        } else {
            this.elavonTransactionDetail.setPosentryCapability(new Integer(POS_ENTRY_CAPABILITY_MAGNETICALLY_SWIPE).byteValue());
            this.elavonTransactionDetail.setAccountEntryMode(new Integer(ACCOUNT_ENTRY_MODE_SWIPED).byteValue());
        }
        
        // Specific logic for refund
        if (StringUtils.isNotBlank(res.getDataBlock().getCreditReturnAmount()) && StringUtils.isNotBlank(res.getDataBlock().getCreditReturnCount())) {
            this.elavonTransactionDetail.setTransactionAmount(this.elavonTransactionDetail.getAuthorizedAmount());
            this.elavonTransactionDetail.setSettlementGmt(DateUtil.getCurrentGmtDate());
        } else {
            this.elavonTransactionDetail.setTransactionAmount(0);
        }
        
        if (null != elavonViaConexDto && null != elavonViaConexDto.getElavonTransactionDetail()) {
            this.elavonTransactionDetail.setElavonTransactionDetail(elavonViaConexDto.getElavonTransactionDetail());
        }
        
        if (res.getDataBlock().getTransactionReferenceNbr() != null
            && !WebCoreConstants.EMPTY_STRING.equals(res.getDataBlock().getTransactionReferenceNbr())) {
            this.elavonTransactionDetail.setTransactionReferenceNbr(Integer.parseInt(res.getDataBlock().getTransactionReferenceNbr()));
        }
    }
    
    @Override
    public final void savePreAuth(final PreAuth preAuth) {
        
        if (getIsReversal()) {
            preAuth.setExpired(PREAUTH_EXPIRED);
        }
        
        this.elavonViaConexService.getEntityDao().save(preAuth);
        if (this.elavonTransactionDetail != null) {
            this.elavonViaConexService.getEntityDao().save(this.elavonTransactionDetail);
        }
        if (this.activeBatchSummary != null) {
            if (this.saveActiveBatchSummary) {
                this.elavonViaConexService.getEntityDao().save(this.activeBatchSummary);
            } else {
                this.elavonViaConexService.getEntityDao().update(this.activeBatchSummary);
                this.setSaveActiveBatchSummary(false);
            }
        }
    }
    
    public Object[] processPostAuth(final PreAuth preAuth, final ProcessorTransaction pt) {
        LOG.info("ElavonViaConexProcessor post auth - Credit Card.Sale");
        
        pt.setProcessingDate(new Date());
        pt.setAuthorizationNumber(preAuth.getAuthorizationNumber());
        //processorTransaction.setReferenceNumber(preAuth.getReferenceNumber());
        pt.setProcessorTransactionId(null);
        
        // slow things down a bit to mimic call to processor
        // otherwise sessions join together.
        try {
            Thread.sleep(MIMIC_REQUEST_TIME);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        
        return new Object[] { true, preAuth.getAuthorizationNumber() };
    }
    
    @Override
    public final void updatePostAuthDetails(final PreAuth preAuth, final ProcessorTransaction pt) {
        final ElavonTransactionDetail etd =
                this.elavonViaConexService.getElavonTransactionDetailService().findByPreAuthId(preAuth.getId().longValue());
        etd.setPreAuth(null);
        etd.setProcessorTransaction(pt);
        etd.setTransactionAmount(pt.getAmount());
        etd.setTicketNumber(pt.getTicketNumber());
        etd.setPurchasedDate(pt.getPurchasedDate());
        final TransactionSettlementStatusType transactionSettlementStatusType = this.elavonViaConexService.getEntityDao()
                .get(TransactionSettlementStatusType.class, CardProcessingConstants.TRANSACTION_SETTLEMENT_STATUS_TYPE_POSTAUTH_ID);
        etd.setTransactionSettlementStatusType(transactionSettlementStatusType);
        
        this.elavonViaConexService.getEntityDao().getCurrentSession().evict(pt);
        this.elavonViaConexService.getEntityDao().update(pt);
        this.elavonViaConexService.getEntityDao().update(etd);
        /*
         * Remove PreAuth.
         */
        this.elavonViaConexService.getEntityDao().delete(preAuth);
        
        /*
         * Update ActiveBatchSummary
         * If batch status is closed => reopen
         */
        this.activeBatchSummary = this.elavonViaConexService.getActiveBatchSummaryService()
                .findByBatchNoAndMerchantId(etd.getBatchNumber(), etd.getMerchantAccount().getId());
        if (this.activeBatchSummary.getBatchstatustype().getId() == CardProcessingConstants.BATCH_STATUS_TYPE_CLOSE) {
            final BatchStatusType batchStatusType =
                    this.elavonViaConexService.getEntityDao().get(BatchStatusType.class, CardProcessingConstants.BATCH_STATUS_TYPE_PARTIALLY_CLOSED);
            this.activeBatchSummary.setBatchstatustype(batchStatusType);
            this.activeBatchSummary.setLastModifiedGMT(DateUtil.getCurrentGmtDate());
        }
        
        // Place ElavonTransactionDetail 'TransactionReferenceNbr' data into ProcessorTransaction referenceNumber
        pt.setReferenceNumber(String.valueOf(etd.getTransactionReferenceNbr()));
    }
    
    @Override
    public Object[] processCharge(final Track2Card track2Card, final ProcessorTransaction chargeProcessorTransaction) {
        LOG.info("ElavonViaConexProcessor transaction charge - Credit Card.Sale");
        
        final MerchantAccount ma = chargeProcessorTransaction.getMerchantAccount();
        final String merchantRefNum = removeLeadingZeros(this.getCommonProcessingService().getNewReferenceNumber(ma));
        
        final List<ElavonRequestBlock> blocks = new ArrayList<ElavonRequestBlock>();
        blocks.add(createBlockHD(ma.getField1()));
        
        // For S&F, Card Retry or Offline, send manual, key entered and PAN+EXP (MMYY).
        // It is better to not assume that processCharge will never have the full track2
        short posCapability = POS_ENTRY_CAPABILITY_MAGNETICALLY_SWIPE;
        short accountEntryMode = ACCOUNT_ENTRY_MODE_SWIPED;
        String cardData = track2Card.getDecryptedCardData();
        if (!track2Card.isCardDataCreditCardTrack2Data()) {
            posCapability = POS_ENTRY_CAPABILITY_MANUAL_ENTRY_ONLY;
            accountEntryMode = ACCOUNT_ENTRY_MODE_KEY_ENTERED_CARD_NOT_PRESENT;
            cardData = ElavonViaConexProcessor.rearrangeToExpiryMonthYear(track2Card.getCreditCardPanAndExpiry());
        }
        
        blocks.add(createBlock01(posCapability, accountEntryMode, cardData, chargeProcessorTransaction.getAmount()));
        
        blocks.add(createBlock03(merchantRefNum));
        
        final ElavonViaConexRequest req = createElavonViaConexRequest(blocks, REQUEST_TYPE_CREDIT_CARD_SALE);
        final String xml = toElavonViaConexRequestXml(req);
        
        String responseXml = null;
        try {
            responseXml = sendOnlineToServer(xml, this.getCommonProcessingService().getDestinationUrlString(ma.getProcessor()), ma,
                                             writeConnTimeoutMsSinglePhase, readConnTimeoutMsSinglePhase);
            
        } catch (IOException ioe) {
            LOG.error("processCharge encountered IOException, processReversal now: ", ioe);
            this.fromProcessCharge = true;
            this.processorTransaction = chargeProcessorTransaction;
            prepareAndProcessReversal(ma, track2Card, chargeProcessorTransaction);
            return new Object[] { DPTResponseCodesMapping.CC_CANNOT_PROCESS_PLEASE_RETRY_VAL__7004, ERROR_STR };
        }
        
        final ElavonViaConexResponse res = toElavonViaConexResponse(responseXml, true);
        chargeProcessorTransaction.setProcessingDate(new Date());
        
        if (isApproved(res.getDataBlock().getResponseCode())) {
            
            String encryptedPanExp = null;
            try {
                encryptedPanExp = this.elavonViaConexService.getCryptoService().encryptData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL,
                                                                                            track2Card.getCreditCardPanAndExpiry());
            } catch (CryptoException ce) {
                LOG.error(ce);
                // Perform Credit Card.Reversal to Elavon.
                prepareAndProcessReversal(ma, track2Card, chargeProcessorTransaction);
                return new Object[] { DPTResponseCodesMapping.CC_CANNOT_PROCESS_PLEASE_RETRY_VAL__7004,
                    res.getDataBlock().getAuthorizationResponse() };
            }
            
            final ElavonViaConexDTO dto = new ElavonViaConexDTO(null, chargeProcessorTransaction,
                    (ElavonRequestType) this.elavonViaConexService.getEntityDao().get(ElavonRequestType.class, ELAVON_REQUEST_TYPE_CREDIT_CARD_SALE),
                    encryptedPanExp, this.elavonViaConexService.getEntityDao()
                            .get(TransactionSettlementStatusType.class, CardProcessingConstants.TRANSACTION_SETTLEMENT_STATUS_TYPE_POSTAUTH_ID));
            
            createElavonTransactionDetail(res, dto, track2Card);
            createOrUpdateActiveBatchSummary(res, dto);
            
            // Place ElavonTransactionDetail 'TransactionReferenceNbr' data into ProcessorTransaction referenceNumber
            chargeProcessorTransaction.setReferenceNumber(String.valueOf(this.elavonTransactionDetail.getTransactionReferenceNbr()));
            chargeProcessorTransaction.setAuthorizationNumber(res.getDataBlock().getApprovalCode());
            chargeProcessorTransaction.setIsApproved(true);
            
            this.elavonTransactionDetail.setTicketNumber(chargeProcessorTransaction.getTicketNumber());
            
            return new Object[] { DPTResponseCodesMapping.CC_APPROVED_VAL__7000, res.getDataBlock().getApprovalCode() };
        }
        
        // Declined
        chargeProcessorTransaction.setAuthorizationNumber(null);
        chargeProcessorTransaction.setIsApproved(false);
        return new Object[] { DPTResponseCodesMapping.CC_DECLINED_VAL__7003, res.getDataBlock().getAuthorizationResponse() };
    }
    
    @Override
    public final void updateProcessorSpecificDetails(final ProcessorTransaction pt) {
        if (this.elavonTransactionDetail == null) {
            this.elavonTransactionDetail = this.elavonViaConexService.getElavonTransactionDetailService()
                    .findByPurchasedDatePointOfSaleIdAndTicketNo(pt.getPurchasedDate(), pt.getPointOfSale().getId(), pt.getTicketNumber());
        } else {
            this.elavonViaConexService.getEntityDao().save(this.elavonTransactionDetail);
        }
        
        // will be null if declined
        if (this.elavonTransactionDetail != null) {
            this.elavonTransactionDetail.setProcessorTransaction(pt);
            this.elavonTransactionDetail.setTransactionAmount(pt.getAmount());
            this.elavonViaConexService.getEntityDao().update(this.elavonTransactionDetail);
            
            if (this.activeBatchSummary != null) {
                if (this.saveActiveBatchSummary) {
                    this.elavonViaConexService.getEntityDao().save(this.activeBatchSummary);
                } else {
                    this.elavonViaConexService.getEntityDao().update(this.activeBatchSummary);
                    this.setSaveActiveBatchSummary(false);
                }
            }
        }
        
    }
    
    private void prepareAndProcessReversal(final MerchantAccount ma, final Track2Card track2Card,
        final ProcessorTransaction chargeProcessorTransaction) {
        if (track2Card.isCardDataCreditCardTrack2Data()) {
            final PreAuth preAuth = new PreAuth();
            preAuth.setCardType(track2Card.getDisplayName());
            preAuth.setLast4digitsOfCardNumber(track2Card.getLast4DigitsOfAccountNumber());
            preAuth.setPointOfSale(chargeProcessorTransaction.getPointOfSale());
            preAuth.setAmount(chargeProcessorTransaction.getAmount());
            preAuth.setPreAuthDate(null);
            preAuth.setMerchantAccount(ma);
            preAuth.setCardData(track2Card.getDecryptedCardData());
            processReversal(preAuth, track2Card);
        } else {
            final Reversal reversal = new Reversal();
            reversal.setMerchantAccount(ma);
            reversal.setCardData(track2Card.getDecryptedCardData());
            reversal.setOriginalTransactionAmount(chargeProcessorTransaction.getAmount());
            reversal.setOriginalReferenceNumber(String.valueOf(ma.getReferenceCounter()));
            reversal.setPurchasedDate(chargeProcessorTransaction.getPurchasedDate());
            reversal.setTicketNumber(chargeProcessorTransaction.getTicketNumber());
            reversal.setPointOfSaleId(chargeProcessorTransaction.getPointOfSale().getId());
            reversal.setLastRetryTime(new Date());
            reversal.setOriginalTime(null);
            reversal.setOriginalMessageType(null);
            reversal.setOriginalProcessingCode(null);
            reversal.setLastResponseCode(null);
            processReversal(reversal, track2Card);
        }
    }
    
    /**
     * For refund Elavon asks to do a batch close of this particular transaction.
     * 
     * @param isReversal
     * @param track2Card
     */
    @Override
    public Object[] processRefund(final boolean isNonEmsRequest, final ProcessorTransaction origProcessorTransaction,
        final ProcessorTransaction refundProcessorTransaction, final String creditCardNumber, final String expiryMMYY) {
        
        LOG.info("ElavonViaConexProcessor refund");
        
        StringBuilder panExpBdr = new StringBuilder();
        panExpBdr.append(creditCardNumber).append(WebCoreConstants.EQUAL_SIGN).append(expiryMMYY);
        final String panExp = panExpBdr.toString();
        // Remove sensitive data.
        panExpBdr = null;
        
        final ElavonTransactionDetail saleDetail =
                this.elavonViaConexService.findSaleDetail(origProcessorTransaction.getId(), origProcessorTransaction.getPointOfSale().getId(),
                                                          origProcessorTransaction.getMerchantAccount().getId());
        if (saleDetail == null) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("ElavonTransactionDetail with ProcessorTransactionId: ").append(origProcessorTransaction.getId()).append(", PointOfSaleId: ")
                    .append(origProcessorTransaction.getPointOfSale().getId()).append(", MerchantAccountId: ")
                    .append(origProcessorTransaction.getMerchantAccount().getId()).append(" doesn't exist. ");
            return new Object[] { DPTResponseCodesMapping.CC_UNABLE_TO_PROCESS_VAL__7084, bdr.toString() };
        }
        
        // Set plain PAN=EXP to ElavonDetailTransaction CardData and ElavonRequestType to Credit Card.Return.
        saleDetail.setCardData(panExp);
        final ElavonRequestType elavonRequestType =
                (ElavonRequestType) this.elavonViaConexService.getEntityDao().get(ElavonRequestType.class, ELAVON_REQUEST_TYPE_CREDIT_CARD_RETURN);
        saleDetail.setElavonRequestType(elavonRequestType);
        // Remove the original auth from the batch details so that the sale will not be settled.
        saleDetail.setOriginalAuthAmount(0);
        saleDetail.setTransactionAmount(refundProcessorTransaction.getAmount());
        
        ActiveBatchSummary summary = null;
        ElavonViaConexResponse responseBlock = null;
        try {
            summary = this.elavonViaConexService.findActiveBatchSummaryForCloseBatch(origProcessorTransaction.getMerchantAccount(),
                                                                                     saleDetail.getBatchNumber());
            
            final ElavonResponseBlock.BatchCloseResult resultObj = this.processBatchClose(saleDetail, summary);
            responseBlock = new ElavonViaConexResponse();
            responseBlock.setDataBlock(resultObj.getElavonResponseBlock());
            
        } catch (BatchCloseException | IOException ex) {
            LOG.error("Cannot refund (close batch with one record), ", ex);
            return new Object[] { DPTResponseCodesMapping.CC_CANNOT_PROCESS_PLEASE_RETRY_VAL__7004, ERROR_STR };
        }
        
        refundProcessorTransaction.setProcessingDate(DateUtil.getCurrentGmtDate());
        int responseCode = 0;
        String responseMsg = "";
        if (responseBlock != null && isGoodBatch(responseBlock.getDataBlock().getResponseMessage())) {
            refundProcessorTransaction.setIsApproved(true);
            responseCode = DPTResponseCodesMapping.CC_APPROVED_VAL__7000;
            final ElavonViaConexDTO dto = new ElavonViaConexDTO(saleDetail, null, origProcessorTransaction, elavonRequestType, null,
                    this.elavonViaConexService.getEntityDao().get(TransactionSettlementStatusType.class,
                                                                  CardProcessingConstants.TRANSACTION_SETTLEMENT_STATUS_TYPE_SETTLED_ID));
            createElavonTransactionDetail(responseBlock, dto, null);
            this.elavonViaConexService.getEntityDao().save(this.elavonTransactionDetail);
            responseMsg = responseBlock.getDataBlock().getResponseMessage();
        } else {
            refundProcessorTransaction.setIsApproved(false);
            responseCode = DPTResponseCodesMapping.CC_DECLINED_VAL__7003;
        }
        
        final StringBuilder bdr = new StringBuilder(150);
        bdr.append("processRefund for Terminal ID: ").append(origProcessorTransaction.getMerchantAccount().getField1());
        bdr.append(", response: ").append(responseBlock.getDataBlock().getResponseMessage());
        LOG.info(bdr.toString());
        return new Object[] { responseCode, responseBlock.getDataBlock().getResponseMessage() };
    }
    
    @Override
    public void updateRefundDetails(final ProcessorTransaction refundPt) {
        this.elavonTransactionDetail.setProcessorTransaction(refundPt);
        this.elavonViaConexService.getEntityDao().update(this.elavonTransactionDetail);
    }
    
    @Override
    public String testCharge(final MerchantAccount ma) {
        LOG.info("ElavonViaConexProcessor testCharge");
        
        final String merchantRefNum = removeLeadingZeros(this.getCommonProcessingService().getNewReferenceNumber(ma));
        
        final List<ElavonRequestBlock> blocks = new ArrayList<ElavonRequestBlock>();
        blocks.add(createBlockHD(ma.getField1()));
        blocks.add(createBlock01(POS_ENTRY_CAPABILITY_MANUAL_ENTRY_ONLY, ACCOUNT_ENTRY_MODE_KEY_ENTERED_CARD_NOT_PRESENT, ElavonViaConexProcessor
                .createPANExpiryInAYear(this.testCreditCardNumberService.getDefaultTestElavonViaconexCreditCardNumberDeclined()),
                                 ElavonViaConexProcessor.ONE_DOLLAR_TEN_CENTS));
        blocks.add(createBlock03(merchantRefNum));
        
        final ElavonViaConexRequest req = createElavonViaConexRequest(blocks, REQUEST_TYPE_CREDIT_CARD_SALE);
        final String xml = toElavonViaConexRequestXml(req);
        
        String responseXml = null;
        try {
            responseXml = sendToServer(xml, this.getCommonProcessingService().getDestinationUrlString(ma.getProcessor()));
        } catch (IOException ioe) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final ElavonViaConexResponse res = toElavonViaConexResponse(responseXml, false);
        if (isValidTestResponse(res.getDataBlock())) {
            return WebCoreConstants.RESPONSE_TRUE;
        }
        String error = null;
        if (res.getErrorString() == null) {
            error = res.getDataBlock().getAuthorizationResponse().toLowerCase();
            error = getUserFriendlyMessageKey(error);
            error = this.messageHelper.getMessage(error);
        } else {
            error = res.getErrorString();
        }
        final String errorMessage;
        errorMessage = new StringBuffer(WebCoreConstants.RESPONSE_FALSE).append(StandardConstants.STRING_COLON)
                .append(this.messageHelper.getMessage("error.merchantaccount.invalid.merchantaccount", new Object[] { error })).toString();
        return errorMessage;
        
    }
    
    /**
     * Use Credit Card.Reversal and its Account_Entry_Mode uses "01 = Key Entered – Card Not Present (01)".
     * 
     * @param reversal
     * @param track2Card
     */
    @Override
    public void processReversal(final Reversal reversal, final Track2Card track2Card) {
        LOG.info("ElavonViaConexProcessor Reversal request");
        
        setIsReversal(true);
        
        final String merchantRefNum = removeLeadingZeros(this.getCommonProcessingService().getNewReferenceNumber(reversal.getMerchantAccount()));
        
        final List<ElavonRequestBlock> blocks = new ArrayList<ElavonRequestBlock>();
        blocks.add(createBlockHD(reversal.getMerchantAccount().getField1()));
        blocks.add(createBlock01(POS_ENTRY_CAPABILITY_MAGNETICALLY_SWIPE, ACCOUNT_ENTRY_MODE_KEY_ENTERED_CARD_NOT_PRESENT,
                                 ElavonViaConexProcessor.rearrangeToExpiryMonthYear(track2Card.getCreditCardPanAndExpiry()),
                                 reversal.getOriginalTransactionAmountInCents()));
        blocks.add(createBlock03(merchantRefNum));
        blocks.add(createBlock04(REQUEST_TYPE_CREDIT_CARD_SALE, null));
        
        final ElavonViaConexRequest req = createElavonViaConexRequest(blocks, REQUEST_TYPE_CREDIT_CARD_REVERSAL);
        final String xml = toElavonViaConexRequestXml(req);
        
        final MerchantAccount ma = reversal.getMerchantAccount();
        String responseXml = null;
        
        for (int i = 1; i <= ElavonViaConexProcessor.reversalTimeoutMaxRetries; i++) {
            try {
                responseXml = sendOnlineToServer(xml, this.getCommonProcessingService().getDestinationUrlString(ma.getProcessor()), ma,
                                                 writeConnTimeoutMsDefault, readConnTimeoutMsDefault);
                break;
            } catch (IOException ioe) {
                LOG.error("Unable to call 'sendOnlineToServer' in processReversal(final Reversal reversal...), retry times: " + i, ioe);
                if (i == ElavonViaConexProcessor.reversalTimeoutMaxRetries) {
                    return;
                }
            }
        }
        final StringBuilder bdr = new StringBuilder(100);
        bdr.append("processReversal(final Reversal reversal...) for Terminal ID: ").append(ma.getField1()).append(" and ");
        
        final ElavonViaConexResponse res = toElavonViaConexResponse(responseXml, true);
        
        if (isApproved(res.getDataBlock().getResponseCode())) {
            bdr.append("it's approved. Approval_Code: ").append(res.getDataBlock().getApprovalCode());
        } else {
            bdr.append("it's declined. Authorization_Response: ").append(res.getDataBlock().getAuthorizationResponse());
        }
        LOG.info(bdr.toString());
        String encryptedPanExp = null;
        try {
            encryptedPanExp = this.elavonViaConexService.getCryptoService().encryptData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL,
                                                                                        track2Card.getCreditCardPanAndExpiry());
        } catch (CryptoException ce) {
            LOG.error(LOG_ERROR_PAN_EXP, ce);
            return;
        }
        
        final ElavonViaConexDTO dto = new ElavonViaConexDTO(null, null,
                (ElavonRequestType) this.elavonViaConexService.getEntityDao().get(ElavonRequestType.class, ELAVON_REQUEST_TYPE_CREDIT_CARD_REVERSAL),
                encryptedPanExp, this.elavonViaConexService.getEntityDao()
                        .get(TransactionSettlementStatusType.class, CardProcessingConstants.TRANSACTION_SETTLEMENT_STATUS_TYPE_EXCLUDED_ID));
        if (this.processorTransaction != null) {
            this.processorTransaction.setProcessingDate(new Date());
            this.processorTransaction.setTicketNumber(reversal.getTicketNumber());
            dto.setProcessorTransaction(this.processorTransaction);
        }
        dto.setMerchantAccount(reversal.getMerchantAccount());
        dto.setPointOfSale(this.elavonViaConexService.getEntityDao().get(PointOfSale.class, reversal.getPointOfSaleId()));
        createElavonTransactionDetail(res, dto, track2Card);
        
        if (this.fromProcessCharge) {
            this.elavonViaConexService.getEntityDao().save(this.elavonTransactionDetail);
        }
        
        this.activeBatchSummary = null;
        
    }
    
    /**
     * Send a Credit Card.Reversal to Elavon if it does not receive a valid response within a predefined time.
     * 
     * @param preAuth
     * @param track2Card
     */
    @Override
    public void processReversal(final PreAuth preAuth, final Track2Card track2Card) {
        LOG.info("ElavonViaConexProcessor reversal request");
        
        setIsReversal(true);
        
        /*
         * Since this method is called from 'processPreAuth', and 'prepareAndProcessReversal' which already determines Track2Card
         * object contains track 2 (track2Card.isCardDataCreditCardTrack2Data()), use POS_ENTRY_CAPABILITY_MAGNETICALLY_SWIPE (2)
         * and ACCOUNT_ENTRY_MODE_KEY_ENTERED_CARD_NOT_PRESENT (1).
         */
        final List<ElavonRequestBlock> blocks = new ArrayList<ElavonRequestBlock>();
        blocks.add(createBlockHD(preAuth.getMerchantAccount().getField1()));
        blocks.add(createBlock01(POS_ENTRY_CAPABILITY_MAGNETICALLY_SWIPE, ACCOUNT_ENTRY_MODE_KEY_ENTERED_CARD_NOT_PRESENT,
                                 ElavonViaConexProcessor.rearrangeToExpiryMonthYear(track2Card.getCreditCardPanAndExpiry()), preAuth.getAmount()));
        blocks.add(createBlock03(preAuth.getReferenceId()));
        blocks.add(createBlock04(REQUEST_TYPE_CREDIT_CARD_SALE, null));
        
        final ElavonViaConexRequest req = createElavonViaConexRequest(blocks, REQUEST_TYPE_CREDIT_CARD_REVERSAL);
        final String xml = toElavonViaConexRequestXml(req);
        
        final MerchantAccount ma = preAuth.getMerchantAccount();
        
        String responseXml = null;
        
        for (int i = 1; i <= ElavonViaConexProcessor.reversalTimeoutMaxRetries; i++) {
            try {
                responseXml = sendOnlineToServer(xml, this.getCommonProcessingService().getDestinationUrlString(ma.getProcessor()), ma,
                                                 writeConnTimeoutMsDefault, readConnTimeoutMsDefault);
                break;
            } catch (IOException ioe) {
                LOG.error("Unable to call 'sendOnlineToServer' in processReversal(final PreAuth preAuth...), retry times: " + i, ioe);
                if (i == ElavonViaConexProcessor.reversalTimeoutMaxRetries) {
                    return;
                }
            }
        }
        final StringBuilder bdr = new StringBuilder(150);
        bdr.append("processReversal for Terminal ID: ").append(ma.getField1()).append(COMMA_W_SPACE);
        
        final ElavonViaConexResponse res = toElavonViaConexResponse(responseXml, true);
        bdr.append("processReversal result: ").append(res.getDataBlock().getAuthorizationResponse());
        LOG.info(bdr.toString());
        
        String encryptedPanExp = null;
        try {
            encryptedPanExp = this.elavonViaConexService.getCryptoService().encryptData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL,
                                                                                        track2Card.getCreditCardPanAndExpiry());
        } catch (CryptoException ce) {
            LOG.error(LOG_ERROR_PAN_EXP, ce);
            return;
        }
        
        final ElavonViaConexDTO dto = new ElavonViaConexDTO(preAuth, null,
                (ElavonRequestType) this.elavonViaConexService.getEntityDao().get(ElavonRequestType.class, ELAVON_REQUEST_TYPE_CREDIT_CARD_REVERSAL),
                encryptedPanExp, this.elavonViaConexService.getEntityDao()
                        .get(TransactionSettlementStatusType.class, CardProcessingConstants.TRANSACTION_SETTLEMENT_STATUS_TYPE_EXCLUDED_ID));
        
        if (this.processorTransaction != null) {
            this.processorTransaction.setProcessingDate(new Date());
            dto.setProcessorTransaction(this.processorTransaction);
            dto.setPointOfSale(this.processorTransaction.getPointOfSale());
        }
        createElavonTransactionDetail(res, dto, track2Card);
        
        if (this.elavonTransactionDetail.getPreAuth() != null && this.elavonTransactionDetail.getPreAuth().getId() == null) {
            this.elavonTransactionDetail.setPreAuth(null);
        }
        this.elavonViaConexService.getEntityDao().save(this.elavonTransactionDetail);
        this.activeBatchSummary = null;
    }
    
    /**
     * Send a Credit Card.Reversal to Elavon. This method is called from a schedule job:
     * com.digitalpaytech.scheduling.processor.service.impl.ElavonViaConexPreAuthReversalServiceImpl, reversalProcessingManager method
     * 
     * @param preAuth
     */
    public final boolean processElavonReversal(final PreAuth preAuth) {
        LOG.info("ElavonViaConexProcessor reversal request for moving from PreAuth to PreAuthHolding");
        
        final ElavonTransactionDetail saleDetail = this.elavonViaConexService
                .findSaleDetailWithPreAuth(preAuth.getId(), preAuth.getPointOfSale().getId(), preAuth.getMerchantAccount().getId());
        
        final ElavonTransactionDetail revDetail = this.elavonViaConexService.findReversalDetail(saleDetail.getId());
        
        /*
         * If reversal record has the max number of retries, return 'true' now and force moving from
         * PreAuth to PreAuthHolding.
         */
        if (revDetail != null && revDetail.getNumRetries() >= ElavonViaConexProcessor.reversalMaxRetries) {
            return true;
        }
        
        String decryptedPanExp = null;
        try {
            decryptedPanExp = this.elavonViaConexService.getCryptoService().decryptData(preAuth.getCardData());
        } catch (CryptoException ce) {
            LOG.error("Unable to decrypt PAN and Expiry Date, ", ce);
            return false;
        }
        
        final List<ElavonRequestBlock> blocks = new ArrayList<ElavonRequestBlock>();
        blocks.add(createBlockHD(preAuth.getMerchantAccount().getField1()));
        blocks.add(createBlock01(POS_ENTRY_CAPABILITY_MAGNETICALLY_SWIPE, ACCOUNT_ENTRY_MODE_KEY_ENTERED_CARD_NOT_PRESENT,
                                 ElavonViaConexProcessor.rearrangeToExpiryMonthYear(decryptedPanExp), preAuth.getAmount()));
        blocks.add(createBlock03(preAuth.getReferenceId()));
        blocks.add(createBlock04(REQUEST_TYPE_CREDIT_CARD_SALE, saleDetail.getResponseCode()));
        blocks.add(createBlock19(saleDetail));
        
        final ElavonViaConexRequest req = createElavonViaConexRequest(blocks, REQUEST_TYPE_CREDIT_CARD_REVERSAL);
        final String xml = toElavonViaConexRequestXml(req);
        
        final MerchantAccount ma = preAuth.getMerchantAccount();
        
        String responseXml = null;
        
        for (int i = 1; i <= ElavonViaConexProcessor.reversalTimeoutMaxRetries; i++) {
            try {
                responseXml = sendOnlineToServer(xml, this.getCommonProcessingService().getDestinationUrlString(ma.getProcessor()), ma,
                                                 writeConnTimeoutMsDefault, readConnTimeoutMsDefault);
                break;
            } catch (IOException ioe) {
                LOG.error("Unable to call processReversal, retry times: " + i, ioe);
                if (i == ElavonViaConexProcessor.reversalTimeoutMaxRetries) {
                    return false;
                }
            }
        }
        final StringBuilder bdr = new StringBuilder(150);
        
        bdr.append("processElavonReversal for Terminal ID: ").append(ma.getField1()).append(COMMA_W_SPACE);
        
        // responseXml is null if the processor instance did not acquire the lock (semaphore busy) 
        if (responseXml == null) {
            return false;
        }
        final ElavonViaConexResponse res = toElavonViaConexResponse(responseXml, true);
        bdr.append("processElavonReversal result: ").append(res.getDataBlock().getAuthorizationResponse());
        LOG.info(bdr.toString());
        
        final ElavonViaConexDTO dto = new ElavonViaConexDTO(preAuth, null,
                (ElavonRequestType) this.elavonViaConexService.getEntityDao().get(ElavonRequestType.class,
                                                                                  Integer.valueOf(ELAVON_REQUEST_TYPE_CREDIT_CARD_REVERSAL)),
                null, this.elavonViaConexService.getEntityDao().get(TransactionSettlementStatusType.class,
                                                                    CardProcessingConstants.TRANSACTION_SETTLEMENT_STATUS_TYPE_CANNOT_SETTLE_ID));
        
        /*
         * If reversal ElavonTransactionDetail exists, add 1 to number of retries and update the record.
         * If it doesn't exist, create a new record, assign sale ElavonTransactionDetail id, set 1 to numRetries
         * and insert to the database.
         */
        if (revDetail != null && revDetail.getNumRetries() < ElavonViaConexProcessor.reversalMaxRetries) {
            final short numRetries = (short) (revDetail.getNumRetries() + 1);
            revDetail.setNumRetries(numRetries);
            this.elavonViaConexService.getEntityDao().saveOrUpdate(revDetail);
        } else {
            createElavonTransactionDetail(res, dto, null);
            this.elavonTransactionDetail.setElavonTransactionDetail(saleDetail);
            this.elavonTransactionDetail.setNumRetries((short) 1);
            this.elavonViaConexService.getEntityDao().save(this.elavonTransactionDetail);
        }
        
        if (isApproved(res.getDataBlock().getResponseCode())) {
            return true;
        }
        return false;
    }
    
    private Cluster getCluster() {
        final int clusterId = this.elavonViaConexService.getClusterMemberService().getClusterId();
        final Cluster cluster = new Cluster();
        cluster.setId(clusterId);
        return cluster;
    }
    
    /**
     * Follow these steps to find a List of batchSettlementDetail objects to close batch.
     * 1. Find a List of ElavonTransactionDetail object using MerchantAccount, batch number and TransactionSettlementStatusType object
     * 2. Insert a new BatchSettlement record.
     * 3. Insert a BatchSettlementDetail record.
     * 
     * @param merchantAccount
     * @return Returns null if no "Not Settled" record was found.
     */
    private BatchSettlementDTO createBatchSettlementDetails(final MerchantAccount merchantAccount, final ActiveBatchSummary summary,
        final BatchSettlementType batchSettlementType) {
        if (summary == null) {
            return null;
        }
        // Step 1.
        final TransactionSettlementStatusType tsst = this.elavonViaConexService
                .findTransactionSettlementStatusType(CardProcessingConstants.TRANSACTION_SETTLEMENT_STATUS_TYPE_POSTAUTH_ID);
        final List<ElavonTransactionDetail> elavonTxDetails =
                this.elavonViaConexService.findElavonTransactionDetails(merchantAccount, summary.getBatchNumber(), tsst);
        
        if (elavonTxDetails == null || elavonTxDetails.isEmpty()) {
            return null;
        }
        
        // Step 2.
        final BatchSettlement batchSettlement =
                this.elavonViaConexService.saveBatchSettlement(summary, getCluster(), (short) elavonTxDetails.size(), batchSettlementType);
        
        // Step 3.
        final List<BatchSettlementDetail> batchSettlementDetails =
                this.elavonViaConexService.saveBatchSettlementDetails(batchSettlement, elavonTxDetails);
        
        return new BatchSettlementDTO(batchSettlement, batchSettlementDetails, summary);
    }
    
    /**
     * Follow these steps to find a List of batchSettlementDetail objects to close batch.
     * 1. Insert a new BatchSettlement record.
     * 2. Insert a BatchSettlementDetail record.
     * 
     * @return Returns null if no "Not Settled" record was found.
     */
    private BatchSettlementDTO createOneBatchSettlementDetail(final ElavonTransactionDetail etd, final ActiveBatchSummary summary) {
        if (summary == null) {
            return null;
        }
        
        final List<ElavonTransactionDetail> elavonTxDetails = new ArrayList<ElavonTransactionDetail>(1);
        elavonTxDetails.add(etd);
        
        final int batchCloseType = CardProcessingConstants.BATCH_SETTLEMENT_TYPE_REFUND;
        final BatchSettlementType batchSettlementType = elavonViaConexService.getBatchSettlementType((byte) batchCloseType);
        
        // Step 1.
        final BatchSettlement batchSettlement =
                this.elavonViaConexService.saveBatchSettlement(summary, getCluster(), (short) elavonTxDetails.size(), batchSettlementType);
        // Step 2.
        final List<BatchSettlementDetail> batchSettlementDetails =
                this.elavonViaConexService.saveBatchSettlementDetails(batchSettlement, elavonTxDetails);
        
        return new BatchSettlementDTO(batchSettlement, batchSettlementDetails, summary);
    }
    
    /**
     * Process close batch operation of one ElavonTransactionDetail.
     * 1. Create 1 Batch Header (Settlement Balance Record)
     * 2 Create 1 Batch Detail (Settlement Transaction Detail)
     * 3. Create 1 Batch Trailer (Settlement Trailer)
     * 4. Analyze the result (Settlement Response)
     * 
     * @param etd
     * @param merchantAccount
     *            Pay station Merchant Account
     * @param batchSettlementDetails
     *            BatchSettlementDetail in a List
     * @return String Response message
     * @throws IOException
     */
    private ElavonResponseBlock.BatchCloseResult processBatchClose(final ElavonTransactionDetail etd, final ActiveBatchSummary summary)
        throws IOException {
        final BatchSettlementDTO batchSettlementDTO = createOneBatchSettlementDetail(etd, summary);
        if (batchSettlementDTO == null) {
            return null;
        }
        final ElavonResponseBlock.BatchCloseResult result = processBatchCloseWithElavon(summary.getBatchNumber(), batchSettlementDTO);
        return result;
    }
    
    /**
     * Process close batch operation which contains the following three steps:
     * 1. Create 1 Batch Header (Settlement Balance Record)
     * 2. Create 1...n Batch Detail (Settlement Transaction Detail)
     * 3. Create 1 Batch Trailer (Settlement Trailer)
     * 4. Analyze the result (Settlement Response)
     * 
     * @param merchantAccount
     *            Pay station Merchant Account
     * @param batchSettlementDetails
     *            BatchSettlementDetail in a List
     * @return String Response message
     * @throws IOException
     */
    @Override
    public final String processBatchClose(final MerchantAccount merchantAccount, final ActiveBatchSummary summary, final int batchCloseType)
        throws IOException {
        
        String resp = null;
        if (!this.elavonViaConexService.hasBatchClosed(summary.getId())) {
            final BatchSettlementType batchSettlementType = new BatchSettlementType();
            batchSettlementType.setId((byte) batchCloseType);
            final BatchSettlementDTO batchSettlementDTO = createBatchSettlementDetails(merchantAccount, summary, batchSettlementType);
            if (batchSettlementDTO == null) {
                final StringBuilder bdr = new StringBuilder(120);
                bdr.append("ActiveBatchSummary id: ").append(summary.getId()).append(", Batch Number: ").append(summary.getBatchNumber())
                        .append(", has no 'Not Settled' BatchSettlementDetail record.");
                return WebCoreConstants.EVENT_ACTION_EMPTY_STRING;
            }
            
            final ElavonResponseBlock.BatchCloseResult result = processBatchCloseWithElavon(summary.getBatchNumber(), batchSettlementDTO);
            resp = result.getResponseMessage();
        } else {
            resp = WebCoreConstants.DUPLICATE_CLOSE_BATCH_TRANSACTION;
        }
        return resp;
    }
    
    private ElavonResponseBlock.BatchCloseResult processBatchCloseWithElavon(final int batchNumber, final BatchSettlementDTO batchSettlementDTO)
        throws IOException {
        
        List<BatchSettlementDetail> batchSettlementDetails = batchSettlementDTO.getBatchSettlementDetails();
        
        /*
         * Net_Amount - This value is used to identify the net dollar amount of all transactions in the batch. Calculates as:
         * ((Credit Sales + Forces) – Credit Returns) + (Debit and EBT Purchases – Debit and EBT Returns) + (ECS Purchases) = Net Amount.
         */
        long netAmt = 0;
        /*
         * Net_Deposit - This value is used to identify the net deposit amount of all transactions in the batch. Calculates as:
         * ((Credit Sales + Forces) – Credit Returns) + (Debit and EBT Purchases – Debit and EBT Returns) + (ECS Purchases) = Net Deposit
         */
        long netDeposit = 0;
        /*
         * Hash_Total - The Batch Amount Hashing total is created by adding together the transaction amount for all detail transactions.
         * The Credit and Refund transaction amounts are treated as positive and are added to the total NOT subtracted from it.
         */
        long hashTotal = 0;
        for (BatchSettlementDetail detail : batchSettlementDetails) {
            netAmt = netAmt + detail.getElavonTransactionDetail().getTransactionAmount();
            netDeposit = netDeposit + detail.getElavonTransactionDetail().getTransactionAmount();
            hashTotal = hashTotal + detail.getElavonTransactionDetail().getTransactionAmount();
        }
        
        final int recordCount = batchSettlementDetails.size() + HEADER_TRAILER_COUNT;
        int rbCount = 0;
        boolean resultFlag = false;
        
        // ---------------------------------------------------------------------------------------------------------------------
        // Batch.Detail Header
        final String batchNum = ElavonViaConexProcessor.formatNumberToString(DECIMAL_FORMAT_THREE_DIGITS, batchNumber);
        ElavonResponseBlock.BatchCloseResult result = null;
        try {
            final boolean okFlag = processBatchCloseHeader(batchNum, recordCount, netAmt);
            if (!okFlag) {
                return null;
            }
            
            // Batch.Detail
            for (BatchSettlementDetail detail : batchSettlementDetails) {
                resultFlag = processBatchCloseDetail(detail);
                if (!resultFlag) {
                    rbCount++;
                }
            }
            
            // Batch.Detail Trailer
            result = processBatchCloseTrailer(recordCount, netDeposit, hashTotal);
            
            // All good, remove card data.
            if (0 == rbCount && isGoodBatch(result.getResponseMessage())) {
                batchSettlementDetails = updateForSuccessfulSettlement(batchSettlementDetails);
                this.elavonViaConexService.updateForSuccessfulSettlement(batchSettlementDetails);
                batchSettlementDTO.getBatchSettlement().setBatchSettlementEndGmt(DateUtil.getCurrentGmtDate());
                this.elavonViaConexService.updateBatchSettlement(batchSettlementDTO.getBatchSettlement());
                
            } else {
                this.elavonViaConexService.encryptCardData(batchSettlementDetails);
            }
        } catch (IOException ioe) {
            // Issue an empty Batch Trailer to end the communication.
            result = processBatchCloseTrailer(0, 0, 0);
        }
        
        return updateBatchSummary(result, rbCount, batchSettlementDTO);
    }
    
    private ElavonResponseBlock.BatchCloseResult updateBatchSummary(final ElavonResponseBlock.BatchCloseResult result, final int rbCount,
        final BatchSettlementDTO batchSettlementDTO) {
        BatchStatusType batchStatusType = null;
        if (result != null && isGoodBatch(result.getResponseMessage())) {
            batchStatusType = this.elavonViaConexService.findBatchStatusType(CardProcessingConstants.BATCH_STATUS_TYPE_CLOSE);
        } else if (result != null && (rbCount > 0 || !isGoodBatch(result.getResponseMessage()))) {
            batchStatusType = this.elavonViaConexService.findBatchStatusType(CardProcessingConstants.BATCH_STATUS_TYPE_FAILED);
        } else {
            batchStatusType = this.elavonViaConexService.findBatchStatusType(CardProcessingConstants.BATCH_STATUS_TYPE_OPEN);
        }
        batchSettlementDTO.getActiveBatchSummary().setBatchstatustype(batchStatusType);
        this.elavonViaConexService.updateActiveBatchSummary(batchSettlementDTO.getActiveBatchSummary());
        
        // ---------------------------------------------------------------------------------------------------------------------
        final StringBuilder bdr = new StringBuilder(110);
        if (result != null) {
            bdr.append("Current batch number: ").append(result.getBatchNumber()).append(", result message: ").append(result.getResponseMessage());
            bdr.append(". RB count: ").append(rbCount);
        } else {
            bdr.append("BatchCloseResult is null and an exception was probably thrown.");
        }
        LOG.info(bdr.toString());
        
        return result;
    }
    
    @Override
    public boolean isReversalExpired(final Reversal reversal) {
        throw new UnsupportedOperationException("isReversalExpired method is not implemented for this processor");
    }
    
    private boolean processBatchCloseHeader(final String batchNumber, final int recordCount, final long netAmount) throws IOException {
        
        LOG.info("ElavonViaConexProcessor - processBatchCloseHeader");
        
        final List<ElavonRequestBlock> blocks = new ArrayList<ElavonRequestBlock>();
        blocks.add(createBlockHD(super.getMerchantAccount().getField1()));
        blocks.add(createBlock90(batchNumber, String.valueOf(recordCount), String.valueOf(netAmount), String.valueOf(0)));
        
        final ElavonViaConexRequest req = createElavonViaConexRequest(blocks, REQUEST_TYPE_BATCH_DETAIL_HEADER);
        
        final String responseXml = sendToServer(toElavonViaConexRequestXml(req),
                                                this.getCommonProcessingService().getDestinationUrlString(super.getMerchantAccount().getProcessor()),
                                                writeConnTimeoutMsCloseBatch, readConnTimeoutMsCloseBatch);
        
        if (!hasCloseBatchOkResponseCode(responseXml)) {
            LOG.error(responseXml);
            return false;
        }
        return true;
    }
    
    private boolean processBatchCloseDetail(final BatchSettlementDetail batchSettlementDetail) throws IOException {
        LOG.info("ElavonViaConexProcessor - processBatchCloseDetail");
        
        String decryptedPanExp = null;
        
        if (panExpDigsPattern.matcher(batchSettlementDetail.getElavonTransactionDetail().getCardData()).matches()) {
            decryptedPanExp = batchSettlementDetail.getElavonTransactionDetail().getCardData();
        } else {
            try {
                final String cardData = batchSettlementDetail.getElavonTransactionDetail().getCardData();
                decryptedPanExp = this.elavonViaConexService.getCryptoService().decryptData(cardData);
                
            } catch (CryptoException ce) {
                LOG.error(ce);
                return false;
            }
            
            if (!CryptoUtil.isDecryptedCardData(decryptedPanExp)) {
                LOG.error("Not able to decrypt card data in ElavonTransactionDetail id: "
                          + batchSettlementDetail.getElavonTransactionDetail().getId());
                return false;
            }
        }
        
        final List<ElavonRequestBlock> blocks = new ArrayList<ElavonRequestBlock>();
        blocks.add(createBlockHD(super.getMerchantAccount().getField1()));
        blocks.add(createBlock04(batchSettlementDetail.getElavonTransactionDetail().getElavonRequestType().getCode(), null));
        blocks.add(createBlock92(batchSettlementDetail.getElavonTransactionDetail(), decryptedPanExp));
        blocks.add(createBlock93(batchSettlementDetail.getElavonTransactionDetail()));
        final ElavonViaConexRequest req = createElavonViaConexRequest(blocks, REQUEST_TYPE_BATCH_DETAIL);
        final String responseXml = sendToServer(toElavonViaConexRequestXml(req),
                                                this.getCommonProcessingService().getDestinationUrlString(super.getMerchantAccount().getProcessor()),
                                                writeConnTimeoutMsCloseBatch, readConnTimeoutMsCloseBatch);
        
        LOG.info("ElavonViaConexProcessor - processBatchCloseDetail, result: " + responseXml);
        // Failure, update TransactionSettlementStatusType with status TRANSACTION_SETTLEMENT_STATUS_TYPE_FAILED_TO_SETTLE_ID.
        if (!hasCloseBatchOkResponseCode(responseXml)) {
            final TransactionSettlementStatusType tsst = this.elavonViaConexService
                    .findTransactionSettlementStatusType(CardProcessingConstants.TRANSACTION_SETTLEMENT_STATUS_TYPE_FAILED_TO_SETTLE_ID);
            batchSettlementDetail.getElavonTransactionDetail().setTransactionSettlementStatusType(tsst);
            return false;
        }
        return true;
    }
    
    private ElavonResponseBlock.BatchCloseResult processBatchCloseTrailer(final int recordCount, final long netDeposit, final long hashTotal)
        throws IOException {
        
        LOG.info("ElavonViaConexProcessor - processBatchCloseTrailer");
        
        final List<ElavonRequestBlock> blocks = new ArrayList<ElavonRequestBlock>();
        blocks.add(createBlockHD(super.getMerchantAccount().getField1()));
        blocks.add(createBlock98(String.valueOf(recordCount), String.valueOf(netDeposit), String.valueOf(hashTotal)));
        
        final ElavonViaConexRequest req = createElavonViaConexRequest(blocks, REQUEST_TYPE_BATCH_DETAIL_TRAILER);
        
        final String responseXml = sendToServer(toElavonViaConexRequestXml(req),
                                                this.getCommonProcessingService().getDestinationUrlString(super.getMerchantAccount().getProcessor()),
                                                writeConnTimeoutMsCloseBatch, readConnTimeoutMsCloseBatch);
        
        final ElavonViaConexResponse res = toElavonViaConexResponse(responseXml, false);
        final ElavonResponseBlock.BatchCloseResult result = res.getDataBlock().getBatchCloseResult();
        
        LOG.info(responseXml);
        return result;
    }
    
    public final ElavonResponseBlock.CurrentBatch getCurrentBatch(final MerchantAccount merchantAccount) {
        LOG.info("ElavonViaConexProcessor getCurrentBatch");
        
        final List<ElavonRequestBlock> blocks = new ArrayList<ElavonRequestBlock>();
        blocks.add(createBlockHD(merchantAccount.getField1()));
        
        final ElavonViaConexRequest req = createElavonViaConexRequest(blocks, REQUEST_TYPE_BATCH_CURRENT_BATCH);
        try {
            final String responseXml = sendToServer(toElavonViaConexRequestXml(req),
                                                    this.getCommonProcessingService().getDestinationUrlString(merchantAccount.getProcessor()));
            
            final ElavonViaConexResponse res = toElavonViaConexResponse(responseXml, true);
            return res.getDataBlock().getCurrentBatch();
            
        } catch (IOException ioe) {
            return null;
        }
    }
    
    private ElavonViaConexRequest createElavonViaConexRequest(final List<ElavonRequestBlock> blocks, final String requestType) {
        final ElavonViaConexRequest req = new ElavonViaConexRequest();
        req.setBlocks(blocks);
        req.setId(requestType);
        req.setVersion(ElavonViaConexProcessor.specVersion);
        return req;
    }
    
    private ElavonRequestBlock createBlockHD(final String terminalId) {
        final ElavonRequestBlock block = new ElavonRequestBlock();
        block.setId("HD");
        block.setTxProcessingNetworkStatusByte("*");
        block.setApplicationId(applicationId);
        block.setTerminalId(terminalId);
        return block;
    }
    
    /**
     * @param posEntryCapability
     *            This value is used to identify the account entry capabilities on the POS device originating the transaction.
     *            POS Entry Capability Available Account Entry Modes
     *            01 = Manual Entry Only 01, 02
     *            02 = Magnetically Swipe Capability 01, 02, 03
     * @param String
     *            accountEntryMode This value is used to identify the method used to enter the Account Data for the transaction
     *            01 = Key Entered – Card Not Present
     *            02 = Key Entered – Card Present
     *            03 = Swiped
     * @param accountData
     *            Track 2 or PAN+EXP.
     *            Track 2 - POS Entry Capability = 02 + Account Entry Mode = 03
     *            PAN+EXP - POS Entry Capability = 01 + Account Entry Mode = 01
     * @param amount
     *            transaction amount and the decimal is implied.
     * 
     * @return ElavonBlock Block with ID 01
     */
    private ElavonRequestBlock createBlock01(final short posEntryCapability, final short accountEntryMode, final String accountData,
        final int amount) {
        return createBlock01(posEntryCapability, accountEntryMode, accountData, amount, true);
    }
    
    private ElavonRequestBlock createBlock01(final short posEntryCapability, final short accountEntryMode, final String accountData, final int amount,
        final boolean includeAssociationTokenFlag) {
        final ElavonRequestBlock block = new ElavonRequestBlock();
        block.setId("01");
        
        block.setPosEntryCapability(ElavonViaConexProcessor.formatNumberToString(ElavonViaConexProcessor.DECIMAL_FORMAT_TWO_DIGITS,
                                                                                 posEntryCapability));
        block.setAccountEntryMode(ElavonViaConexProcessor.formatNumberToString(ElavonViaConexProcessor.DECIMAL_FORMAT_TWO_DIGITS, accountEntryMode));
        block.setAccountData(accountData);
        if (includeAssociationTokenFlag) {
            block.setAssociationToken(FALSE_NUMERIC_OR_ZERO_STRING_VALUE);
        }
        block.setTransactionAmount(String.valueOf(amount));
        // 0 = Indicates that the POS is not capable of Partial Authorizations
        block.setPartialAuthIndicator(FALSE_NUMERIC_OR_ZERO_STRING_VALUE);
        // In the Terminal-Based environment, the last record number should be 0000.
        block.setLastRecordNumber(FALSE_NUMERIC_OR_ZERO_STRING_VALUE);
        // 03 = Self-Service Terminal (AFD)
        block.setCatIndicator(DECIMAL_FORMAT_ZERO_THREE);
        // 03 = Unattended Terminal (CAT)
        block.setTerminalType(DECIMAL_FORMAT_ZERO_THREE);
        
        block.setTokenIndicator(FALSE_NUMERIC_OR_ZERO_STRING_VALUE);
        // 2 = Cannot accept and pass an online PIN
        block.setPinEntryCapability("2");
        
        return block;
    }
    
    /**
     * e.g.
     * Request=Batch.Detail Header
     * Version=4017
     * HD.Application_ID=TZKQ01GC
     * HD.Terminal_ID=0017340009998881119888
     * 90.Batch_Number=007
     * 90.Record_Count=3
     * 90.Net_Amount=100
     * 90.Net_Tip_Amount=0
     * 
     * @param batchNumber
     * @param recordCount
     * @param netAmount
     * @param netTipAmount
     * @return
     */
    private ElavonRequestBlock createBlock90(final String batchNumber, final String recordCount, final String netAmount, final String netTipAmount) {
        final ElavonRequestBlock block = new ElavonRequestBlock();
        block.setId("90");
        block.setBatchNumber(batchNumber);
        block.setRecordCount(recordCount);
        block.setNetAmount(netAmount);
        block.setNetTipAmount(netTipAmount);
        return block;
    }
    
    /**
     * e.g.
     * Request=Batch.Detail Trailer
     * Version=4017
     * HD.Application_ID=TZKQ01GC
     * HD.Terminal_ID=0017340009998881119888
     * 98.Transmission_Date=0501
     * 98.Record_Count=3
     * 98.Net_Deposit=100
     * 98.Hash_Total=100
     * 
     * @param recordCount
     * @param netDeposit
     * @param hashTotal
     * @return
     */
    private ElavonRequestBlock createBlock98(final String recordCount, final String netDeposit, final String hashTotal) {
        final ElavonRequestBlock block = new ElavonRequestBlock();
        block.setId("98");
        block.setTransmissionDate(DateUtil.format(new GregorianCalendar().getTime(), DateUtil.MM_DD));
        block.setRecordCount(recordCount);
        block.setNetDeposit(netDeposit);
        block.setHashTotal(hashTotal);
        return block;
    }
    
    /**
     * e.g.
     * Request=Batch.Detail
     * Version=4017
     * HD.Application_ID=TZKQ01GC
     * HD.Terminal_ID=0017340009998881119888
     * 04.Transaction_Code=Credit Card.Sale
     * 92.Record_Number=0
     * ...
     * or
     * Request=Credit Card.Reversal
     * Version=4017
     * HD.Application_ID=TZKQ01GC
     * HD.Terminal_ID=0017340009998881119888
     * ...
     * ...
     * 03.Merchant_Reference_Nbr=98765432101
     * 04.Transaction_Code=Credit Card.Sale
     * 04.Response_Code=AA
     * 
     * @param transactionCode
     * @return
     */
    private ElavonRequestBlock createBlock04(final String transactionCode, final String responseCode) {
        final ElavonRequestBlock block = new ElavonRequestBlock();
        block.setId("04");
        block.setTransactionCode(transactionCode);
        block.setResponseCode(responseCode);
        return block;
    }
    
    /**
     * e.g.
     * Request=Credit Card.Reversal
     * Version=4017
     * HD.Application_ID=TZKQ01GC
     * HD.Terminal_ID=0017340009998881119888
     * ...
     * ...
     * 19.Approval_Code=CMC668
     * 19.PS2000_Data=MMCC048F7FYYA 0805
     * 19.Trace_Number=39571
     * 19.Transaction_Reference_Nbr=805233328
     * 
     * @param elavonTransactionDetail
     * @return
     */
    private ElavonRequestBlock createBlock19(final ElavonTransactionDetail elavonTransactionDetail) {
        final ElavonRequestBlock block = new ElavonRequestBlock();
        block.setId("19");
        block.setApprovalCode(elavonTransactionDetail.getApprovalCode());
        block.setPs2000Data(elavonTransactionDetail.getPs2000data());
        block.setTraceNumber(String.valueOf(elavonTransactionDetail.getTraceNumber()));
        block.setTransactionReferenceNbr(String.valueOf(elavonTransactionDetail.getTransactionReferenceNbr()));
        return block;
    }
    
    /**
     * e.g.
     * Request=Batch.Detail
     * Version=4017
     * HD.Application_ID=TZKQ01GC
     * HD.Terminal_ID=0017340009998881119888
     * 04.Transaction_Code=Credit Card.Sale
     * 92.Record_Number=0
     * 92.Account_Data=4124939999999990=1219
     * 92.Account_Entry_Mode=01
     * 92.Transaction_Amount=100
     * 92.Original_Auth_Amount=100
     * 92.Approval_Code=CVI687
     * 92.Authorization_Date=060315
     * 92.Authorization_Time=201652
     * 92.POS_Entry_Capability=01
     * 92.Authorization_Source=0
     * 92.Account_Source=0
     * 92.Capture_Tran_Code=5
     * 92.CAT_Indicator=00
     * 92.Token_Indicator=0
     * 92.Card_ID=@
     * 92.Service_Code=123
     * 
     * @param transactionCode
     * @param accountData
     * @return
     */
    private ElavonRequestBlock createBlock92(final ElavonTransactionDetail etd, final String decryptedCardData) {
        
        final ElavonRequestBlock block = new ElavonRequestBlock();
        block.setId("92");
        block.setRecordNumber(FALSE_NUMERIC_OR_ZERO_STRING_VALUE);
        block.setTransactionAmount(String.valueOf(etd.getTransactionAmount()));
        block.setOriginalAuthAmount(String.valueOf(etd.getOriginalAuthAmount()));
        block.setApprovalCode(etd.getApprovalCode());
        block.setAuthorizationDate(DateUtil.format(etd.getAuthorizationDateTime(), DateUtil.DATE_FORMAT_TWO_DIGITS_YEAR));
        block.setAuthorizationTime(DateUtil.format(etd.getAuthorizationDateTime(), DateUtil.TIME_FORMAT_HOUR_MINUTE_SECOND));
        
        /*
         * Since we only store PAN + Expiry mmyy in the database, POS Entry Capability = 01 and Account Entry Mode = 01.
         * POS Entry Capability 01 = Manual Entry Only
         * Account Entry Mode 01 = Key Entered – Card Not Present
         * Account Data
         */
        block.setPosEntryCapability(ElavonViaConexProcessor.formatNumberToString(ElavonViaConexProcessor.DECIMAL_FORMAT_TWO_DIGITS,
                                                                                 etd.getPosentryCapability()));
        
        block.setAccountData(ElavonViaConexProcessor.rearrangeToExpiryMonthYear(decryptedCardData));
        block.setAuthorizationSource(FALSE_NUMERIC_OR_ZERO_STRING_VALUE);
        if (etd.getServiceCode() <= 0) {
            block.setAccountSource(FALSE_NUMERIC_OR_ZERO_STRING_VALUE);
            block.setAccountEntryMode(ElavonViaConexProcessor.formatNumberToString(ElavonViaConexProcessor.DECIMAL_FORMAT_TWO_DIGITS,
                                                                                   ACCOUNT_ENTRY_MODE_KEY_ENTERED_CARD_NOT_PRESENT));
        } else {
            block.setAccountSource(ACCOUNT_SOURCE_SWIPED);
            block.setAccountEntryMode(ElavonViaConexProcessor.formatNumberToString(ElavonViaConexProcessor.DECIMAL_FORMAT_TWO_DIGITS,
                                                                                   ACCOUNT_ENTRY_MODE_SWIPED));
        }
        
        block.setCaptureTranCode(String.valueOf(etd.getElavonRequestType().getCaptureTranCode()));
        block.setTokenIndicator(FALSE_NUMERIC_OR_ZERO_STRING_VALUE);
        block.setCardID("@");
        block.setServiceCode(formatCodeIfValid(etd.getServiceCode()));
        
        // 03 = Self-Service Terminal (AFD)
        block.setCatIndicator(DECIMAL_FORMAT_ZERO_THREE);
        // 03 = Unattended Terminal (CAT)
        block.setTerminalType(DECIMAL_FORMAT_ZERO_THREE);
        
        return block;
    }
    
    /**
     * e.g.
     * Request=Batch.Detail
     * Version=4017
     * HD.Application_ID=TZKQ01GC
     * HD.Terminal_ID=0017340009998881119888
     * 93.AVS_Response=
     * 93.CVV2_Response=
     * 93.PS2000_Data=E110291966592311EC20A
     * 93.MSDI=1
     * 
     * @param etd
     * @return
     */
    private ElavonRequestBlock createBlock93(final ElavonTransactionDetail etd) {
        final ElavonRequestBlock block = new ElavonRequestBlock();
        block.setId("93");
        block.setAvsResponse(etd.getAvsResponse());
        block.setCvv2Response(etd.getCvv2Response());
        block.setPs2000Data(etd.getPs2000data());
        block.setMsdi(String.valueOf(etd.getMsdi()));
        return block;
    }
    
    private ElavonRequestBlock createBlock03(final String merchantReferenceNumber) {
        final ElavonRequestBlock block = new ElavonRequestBlock();
        block.setId(DECIMAL_FORMAT_ZERO_THREE);
        block.setMerchantReferenceNbr(merchantReferenceNumber);
        return block;
    }
    
    /**
     * In Test environment 10 cents would return DECLINED.
     * 
     * e.g.
     * <Block id="02">
     * <Response_Code>ND</Response_Code>
     * ...
     * <Authorization_Date>051415</Authorization_Date>
     * <Authorization_Time>122851</Authorization_Time>
     * <Batch_Number>043</Batch_Number>
     * <Authorization_Response>DECLINED</Authorization_Response>
     * ...
     * </Block>
     * 
     * @param block
     * @return true if the following aren't null or blank:
     *         - Response_Code
     *         - Authorization_Date
     *         - Authorization_Time
     *         - Batch_Number
     *         - Authorization_Response
     */
    private boolean isValidTestResponse(final ElavonResponseBlock block) {
        if (block == null || StringUtils.isBlank(block.getResponseCode())) {
            return false;
        }
        if (hasCreditCardApprovedResponseCode(block.getResponseCode())) {
            return true;
        }
        if (StringUtils.isNotBlank(block.getAuthorizationDate()) && StringUtils.isNotBlank(block.getAuthorizationTime())
            && StringUtils.isNotBlank(block.getBatchNumber()) && StringUtils.isNotBlank(block.getAuthorizationResponse())
            && acceptableResponse(block.getAuthorizationResponse())) {
            return true;
        }
        return false;
    }
    
    private boolean acceptableResponse(final String response) {
        final String[] responses = new String[] { "INVALID CARD", "DECLINED", "EXPIRED" };
        for (int i = 0; i < responses.length; i++) {
            if (response.indexOf(responses[i]) != WebCoreConstants.INT_RECORD_NOT_FOUND) {
                return true;
            }
        }
        return false;
    }
    
    private List<BatchSettlementDetail> updateForSuccessfulSettlement(final List<BatchSettlementDetail> batchSettlementDetails) {
        final TransactionSettlementStatusType tsst =
                this.elavonViaConexService.findTransactionSettlementStatusType(CardProcessingConstants.TRANSACTION_SETTLEMENT_STATUS_TYPE_SETTLED_ID);
        
        for (BatchSettlementDetail batchSettlementDetail : batchSettlementDetails) {
            batchSettlementDetail.getElavonTransactionDetail().setTransactionSettlementStatusType(tsst);
            batchSettlementDetail.getElavonTransactionDetail().setSettlementGmt(DateUtil.getCurrentGmtDate());
            batchSettlementDetail.getElavonTransactionDetail().setCardData(null);
        }
        return batchSettlementDetails;
    }
    
    private final String toElavonViaConexRequestXml(final ElavonViaConexRequest request) {
        return ElavonViaConexProcessor.xstream.toXML(request);
    }
    
    private ElavonViaConexResponse toElavonViaConexResponse(final String xml, final boolean throwException) {
        final ElavonViaConexResponse resp = (ElavonViaConexResponse) ElavonViaConexProcessor.xstream.fromXML(xml);
        if (resp.getBlocks() == null && resp.getDataBlock() == null) {
            final String errorMessage = "Unexpected response message from Elavon " + xml;
            if (throwException) {
                throw new CardTransactionException(errorMessage);
            } else {
                LOG.error(errorMessage);
                final Matcher matcher = PTTRN_STRING_RESPONSE_EXTRACTOR.matcher(xml);
                if (matcher.find()) {
                    resp.setErrorString(matcher.group(1));
                }
            }
        } else {
            resp.organizeData();
        }
        return resp;
    }
    
    private static boolean isGoodBatch(final String response) {
        if (StringUtils.isNotBlank(response) && (response.toUpperCase().indexOf(CardProcessingConstants.RESULT_GOOD_BATCH_RESPONSE_1) != -1
                                                 || response.toUpperCase().indexOf(CardProcessingConstants.RESULT_GOOD_BATCH_RESPONSE_2) != -1)) {
            return true;
        }
        return false;
    }
    
    private boolean isApproved(final String responseCode) {
        if (StringUtils.isNotBlank(responseCode) && hasCreditCardApprovedResponseCode(responseCode)) {
            return true;
        }
        return false;
    }
    
    private boolean hasCreditCardApprovedResponseCode(final String responseCode) {
        if (StringUtils.isBlank(responseCode)) {
            return false;
        }
        if (APPROVED_ALL.equalsIgnoreCase(responseCode) || "AP".equalsIgnoreCase(responseCode)) {
            return true;
        }
        return false;
    }
    
    private boolean hasCloseBatchOkResponseCode(final String response) {
        if (StringUtils.isBlank(response)) {
            return false;
        }
        if (response.toUpperCase().indexOf(CardProcessingConstants.RESULT_OK) != WebCoreConstants.INT_RECORD_NOT_FOUND
            && response.toUpperCase().indexOf(CardProcessingConstants.RESULT_STATUS_A) != WebCoreConstants.INT_RECORD_NOT_FOUND) {
            return true;
        }
        return false;
    }
    
    private String removeLeadingZeros(final String numberInString) {
        int i = 0;
        try {
            i = Integer.parseInt(numberInString);
            
        } catch (NumberFormatException nfe) {
            LOG.error("Cannot parse String to int: " + numberInString, nfe);
            return "";
        }
        return String.valueOf(i);
    }
    
    private URLConnection prepareURLConnection(final String requestInXml, final String destinationUrlString, final int connectionTimeout,
        final int readTimeout) throws IOException {
        
        final URL url = new URL(destinationUrlString);
        final URLConnection con = url.openConnection();
        con.setConnectTimeout(connectionTimeout);
        con.setReadTimeout(readTimeout);
        con.setDoInput(true);
        con.setDoOutput(true);
        con.setUseCaches(false);
        con.setAllowUserInteraction(false);
        
        con.setRequestProperty("Content-Length", Integer.toString(requestInXml.length()));
        con.setRequestProperty("Registration-Key", registrationKey);
        return con;
    }
    
    private String sendToServer(final String requestInXml, final String destinationUrlString) throws IOException {
        return sendToServer(requestInXml, destinationUrlString, writeConnTimeoutMsDefault, readConnTimeoutMsDefault);
    }
    
    /**
     * Send data to Elavon ViaConex
     * 
     * @param requestInXml
     * @param destinationUrlString
     * @param connectionTimeout
     *            The connect timeout value in milliseconds
     * @param readTimeout
     *            The read connect timeout value in milliseconds
     * @return
     * @throws IOException
     */
    private String sendToServer(final String requestInXml, final String destinationUrlString, final int connectionTimeout, final int readTimeout)
        throws IOException {
        
        InputStream in = null;
        BufferedReader br = null;
        OutputStream out = null;
        
        if (LOG.isDebugEnabled()) {
            LOG.debug("Request: \r\n" + removeCardDataFromXml(requestInXml));
        }
        
        final byte[] request = requestInXml.getBytes();
        try {
            final URLConnection con = prepareURLConnection(requestInXml, destinationUrlString, connectionTimeout, readTimeout);
            out = con.getOutputStream();
            out.write(request);
            out.flush();
            
            in = con.getInputStream();
            if (in == null) {
                return null;
            }
            
            final StringBuilder bdr = new StringBuilder();
            br = new BufferedReader(new InputStreamReader(in));
            String s = null;
            while ((s = br.readLine()) != null) {
                bdr.append(s).append('\r');
            }
            
            final String response = bdr.toString();
            
            if (LOG.isDebugEnabled()) {
                LOG.debug("\n" + formatXml(response));
            }
            
            return response;
            
        } catch (IOException ioe) {
            LOG.error("ElavonViaConexProcessor encountered IO problem in sendToServer, ", ioe);
            throw ioe;
        } finally {
            closeIO(in, br, out);
        }
    }
    
    private String sendOnlineToServer(final String requestInXml, final String destinationUrlString, final MerchantAccount ma,
        final int connectionTimeout, final int readTimeout) throws IOException {
        
        final String tidmid = CardProcessingUtil.createRedisKey(ma);
        for (int i = 0; i < ElavonViaConexProcessor.noOfRetries; i++) {
            if (this.elavonViaConexService.getSemaphoreService().bookConnectionWithExpire(tidmid, ElavonViaConexProcessor.expireMsDefault)) {
                try {
                    return sendToServer(requestInXml, destinationUrlString, connectionTimeout, readTimeout);
                } catch (IOException ioe) {
                    LOG.error("ElavonViaConexProcessor encountered IO problem, " + ioe);
                    throw ioe;
                } finally {
                    this.elavonViaConexService.getSemaphoreService().releaseConnection(tidmid);
                }
            } else {
                try {
                    Thread.sleep(ElavonViaConexProcessor.waitInterval);
                } catch (InterruptedException e) {
                    // Restore the interrupted status
                    Thread.currentThread().interrupt();
                }
            }
        }
        return null;
    }
    
    private void closeIO(final InputStream in, final BufferedReader br, final OutputStream out) {
        if (in != null) {
            try {
                in.close();
            } catch (IOException e) {
                LOG.error("ElavonViaConexProcessor, cannot close InputStream, ", e);
            }
        }
        if (br != null) {
            try {
                br.close();
            } catch (IOException e) {
                LOG.error("ElavonViaConexProcessor, cannot close BufferedReader, ", e);
            }
        }
        if (out != null) {
            try {
                out.close();
            } catch (IOException e) {
                LOG.error("ElavonViaConexProcessor, cannot close OutputStream, ", e);
            }
        }
    }
    
    private String removeCardDataFromXml(final String input) {
        return XMLUtil.hideAllElementValues(input, "Account_Data");
    }
    
    private String formatXml(final String input) {
        try {
            final Source xmlInput = new StreamSource(new StringReader(input));
            final StreamResult xmlOutput = new StreamResult(new StringWriter());
            final Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            transformer.transform(xmlInput, xmlOutput);
            return xmlOutput.getWriter().toString();
        } catch (TransformerException tfe) {
            LOG.error(tfe);
            return input;
        }
    }
    
    private static String createPANExpiryInAYear(final String pan) {
        final Calendar cal = new GregorianCalendar();
        cal.add(Calendar.YEAR, 1);
        final String mmyy = DateUtil.format(cal.getTime(), DateUtil.PANEXP_FORMAT, cal.getTimeZone());
        
        final StringBuilder bdr = new StringBuilder();
        bdr.append(pan).append(WebCoreConstants.EQUAL_SIGN).append(mmyy);
        return bdr.toString();
    }
    
    private static String formatNumberToString(final String pattern, final int number) {
        final DecimalFormat formatter = new DecimalFormat(pattern);
        return formatter.format(number);
    }
    
    /**
     * If code is equal or less than -1, returns null. Otherwise this method will return three digit format String.
     * e.g. -1 returns nulll
     * 1 returns 001
     * 11 returns 011
     * 111 returns 111
     * 10 returns 100
     * 
     * @param code
     * @return String three digits format String
     */
    private static String formatCodeIfValid(final int code) {
        if (code <= WebCoreConstants.INT_RECORD_NOT_FOUND) {
            return null;
        }
        return ElavonViaConexProcessor.formatNumberToString(DECIMAL_FORMAT_THREE_DIGITS, code);
    }
    
    /**
     * e.g. 4111111111111111=1801 -> 4111111111111111=0118
     * 
     * @param panAndExpiryYearMonth
     * @return
     */
    private static String rearrangeToExpiryMonthYear(final String panAndExpiryYearMonth) {
        final String[] panExp = panAndExpiryYearMonth.split(WebCoreConstants.EQUAL_SIGN);
        final StringBuilder bdr = new StringBuilder();
        bdr.append(panExp[0]).append(WebCoreConstants.EQUAL_SIGN)
                // Month
                .append(panExp[1].substring(2, PAN_EXPIRY_OFFSET))
                // Year
                .append(panExp[1].substring(0, 2));
        return bdr.toString();
    }
    
    public final void setEmsPropertiesValues() {
        if (ElavonViaConexProcessor.writeConnTimeoutMsRealTime == null) {
            ElavonViaConexProcessor.writeConnTimeoutMsRealTime =
                    super.getEmsPropertiesService().getPropertyValueAsInt(EmsPropertiesService.ELAVON_VIACONEX_CONNECTION_TIMEOUT_REAL_TIME,
                                                                          EmsPropertiesService.DEFAULT_ELAVON_VIACONEX_CONNECTION_TIMEOUT_REAL_TIME);
            ElavonViaConexProcessor.readConnTimeoutMsRealTime =
                    super.getEmsPropertiesService().getPropertyValueAsInt(EmsPropertiesService.ELAVON_VIACONEX_READ_TIMEOUT_REAL_TIME,
                                                                          EmsPropertiesService.DEFAULT_ELAVON_VIACONEX_READ_TIMEOUT_REAL_TIME);
            ElavonViaConexProcessor.writeConnTimeoutMsSinglePhase = super.getEmsPropertiesService()
                    .getPropertyValueAsInt(EmsPropertiesService.ELAVON_VIACONEX_CONNECTION_TIMEOUT_STORE_FORWARD_SINGLE_PHASE,
                                           EmsPropertiesService.DEFAULT_ELAVON_VIACONEX_CONNECTION_TIMEOUT_STORE_FORWARD_SINGLE_PHASE);
            ElavonViaConexProcessor.readConnTimeoutMsSinglePhase = super.getEmsPropertiesService()
                    .getPropertyValueAsInt(EmsPropertiesService.ELAVON_VIACONEX_READ_TIMEOUT_STORE_FORWARD_SINGLE_PHASE,
                                           EmsPropertiesService.DEFAULT_ELAVON_VIACONEX_READ_TIMEOUT_STORE_FORWARD_SINGLE_PHASE);
            ElavonViaConexProcessor.writeConnTimeoutMsDefault =
                    super.getEmsPropertiesService().getPropertyValueAsInt(EmsPropertiesService.ELAVON_VIACONEX_CONNECTION_TIMEOUT_DEFAULT,
                                                                          EmsPropertiesService.DEFAULT_ELAVON_VIACONEX_CONNECTION_TIMEOUT_DEFAULT);
            ElavonViaConexProcessor.readConnTimeoutMsDefault =
                    super.getEmsPropertiesService().getPropertyValueAsInt(EmsPropertiesService.ELAVON_VIACONEX_READ_TIMEOUT_DEFAULT,
                                                                          EmsPropertiesService.DEFAULT_ELAVON_VIACONEX_READ_TIMEOUT_DEFAULT);
            ElavonViaConexProcessor.writeConnTimeoutMsCloseBatch = super.getEmsPropertiesService()
                    .getPropertyValueAsInt(EmsPropertiesService.ELAVON_VIACONEX_CONNECTION_TIMEOUT_CLOSE_BATCH,
                                           EmsPropertiesService.DEFAULT_ELAVON_VIACONEX_CONNECTION_TIMEOUT_CLOSE_BATCH);
            ElavonViaConexProcessor.readConnTimeoutMsCloseBatch =
                    super.getEmsPropertiesService().getPropertyValueAsInt(EmsPropertiesService.ELAVON_VIACONEX_READ_TIMEOUT_CLOSE_BATCH,
                                                                          EmsPropertiesService.DEFAULT_ELAVON_VIACONEX_READ_TIMEOUT_CLOSE_BATCH);
            
            ElavonViaConexProcessor.applicationId =
                    super.getEmsPropertiesService().getPropertyValue(EmsPropertiesService.ELAVON_VIACONEX_APPLICATION_ID);
            ElavonViaConexProcessor.registrationKey =
                    super.getEmsPropertiesService().getPropertyValue(EmsPropertiesService.ELAVON_VIACONEX_REGISTRATION_KEY);
            
            ElavonViaConexProcessor.noOfRetries = super.getEmsPropertiesService()
                    .getPropertyValueAsInt(EmsPropertiesService.REAL_TIME_ELAVON_RETRY_ATTEMPTS, DEFAULT_REAL_TIME_ELAVON_RETRY_ATTEMPTS);
            ElavonViaConexProcessor.waitInterval = super.getEmsPropertiesService()
                    .getPropertyValueAsInt(EmsPropertiesService.ELAVON_RETRY_WAITING_TIME, DEFAULT_ELAVON_RETRY_WAITING_TIME);
            
            ElavonViaConexProcessor.specVersion = this.getEmsPropertiesService()
                    .getPropertyValue(EmsPropertiesService.ELAVON_VIACONEX_SPEC_VERSION, EmsPropertiesService.DEFAULT_ELAVON_VIACONEX_SPEC_VERSION);
            
            ElavonViaConexProcessor.reversalTimeoutMaxRetries =
                    this.getEmsPropertiesService().getPropertyValueAsInt(EmsPropertiesService.ELAVON_VIACONEX_REVERSAL_TIMEOUT_RETRY_TIMES,
                                                                         EmsPropertiesService.DEFAULT_ELAVON_VIACONEX_REVERSAL_TIMEOUT_RETRY_TIMES);
            
            ElavonViaConexProcessor.reversalMaxRetries =
                    this.getEmsPropertiesService().getPropertyValueAsInt(EmsPropertiesService.ELAVON_VIACONEX_REVERSAL_RETRY_TIMES,
                                                                         EmsPropertiesService.DEFAULT_ELAVON_VIACONEX_REVERSAL_RETRY_TIMES);
            
            ElavonViaConexProcessor.expireMsDefault =
                    this.getEmsPropertiesService().getPropertyValueAsInt(EmsPropertiesService.ELAVON_VIACONEX_REDIS_TX_EXPIRE,
                                                                         EmsPropertiesService.DEFAULT_ELAVON_VIACONEX_REDIS_TX_EXPIRE_MS);
        }
    }
    
    public final void setActiveBatchSummary(final ActiveBatchSummary activeBatchSummary) {
        this.activeBatchSummary = activeBatchSummary;
    }
    
    public final boolean isSaveActiveBatchSummary() {
        return this.saveActiveBatchSummary;
    }
    
    public final void setSaveActiveBatchSummary(final boolean saveActiveBatchSummary) {
        this.saveActiveBatchSummary = saveActiveBatchSummary;
    }
    
    public final void setElavonViaConexService(final ElavonViaConexService elavonViaConexService) {
        this.elavonViaConexService = elavonViaConexService;
    }
    
    public final void setMessageHelper(final MessageHelper messageHelper) {
        this.messageHelper = messageHelper;
    }
    
    public final boolean getIsReversal() {
        return this.isReversal;
    }
    
    public final void setIsReversal(final boolean isReversal) {
        this.isReversal = isReversal;
    }
    
    public ElavonTransactionDetail getElavonTransactionDetail() {
        return elavonTransactionDetail;
    }
    
    public void setElavonTransactionDetail(ElavonTransactionDetail elavonTransactionDetail) {
        this.elavonTransactionDetail = elavonTransactionDetail;
    }
    
    public static String getUserFriendlyMessageKey(final String rawmessage) {
        String key = rawmessage.replace(" ", ".");
        key = "error.elavon." + key;
        return key;
    }
    
}
