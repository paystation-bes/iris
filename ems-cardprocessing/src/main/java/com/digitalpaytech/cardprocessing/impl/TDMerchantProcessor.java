package com.digitalpaytech.cardprocessing.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.apache.commons.lang.StringUtils;

import com.beanstream.Gateway;
import com.beanstream.domain.TransactionRecord;
import com.beanstream.exceptions.BeanstreamApiException;
import com.beanstream.exceptions.BusinessRuleException;
import com.beanstream.requests.CardPaymentRequest;
import com.beanstream.responses.PaymentResponse;
import com.digitalpaytech.cardprocessing.CardProcessor;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.CreditCardTestNumberService;
import com.digitalpaytech.service.processor.TDMerchantCommunicationService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DPTResponseCodesMapping;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;

/*
 * (non-Javadoc)
 * @see com.digitalpaytech.cardprocessing.CardProcessor#testCharge(com.digitalpaytech.domain.MerchantAccount)
 * /// The 'category' is one of 4 numeric values:
 * 1 - The transaction was successfully processed
 * These can be successes or failures. They indicate that the message went to the bank and it returned a response.
 * The response could be a "payment approved" or even a "declined" response, along with many others.
 * These errors should be sent to the card holder and merchant to indicate a problem with their card.
 * 2 - Business rule violation
 * This is usually a problem with how your account is configured. Sometimes it could be duplicate order number errors or something
 * similar. These errors need to be worked out by the developer before the software moves to production.
 * 3 - Input data problem
 * The requests are improperly formatted or the data is wrong. Invalid card number errors (code 52) can also appear here.
 * Most of these errors need to be worked out by the developer before the software moves to production.
 * 4 - Transaction failed due to technical problem
 * There was a problem on the Beanstream or bank servers that is out of your control. These will respond with an http status code
 * in the 500+ range. Card holders should wait a minute and try the transaction again.
 */
public class TDMerchantProcessor extends CardProcessor {
    public static final double PREAUTH_CANCELLATION_AMOUNT = 0.00;
    private static Logger logger = Logger.getLogger(TDMerchantProcessor.class);
    
    private static final String TD_MERCHANT_REQ_TYPE_STRING = "TD Merchant Request Type: ";
    private static final String ORDER_NUMBER_DEBUG = ", orderNumber: ";
    
    private static final int TD_MERCHANT_CATEGORY_TRANSACTION_PROCESSED = 1;
    private static final String TEST_CHARGE_AMOUNT = "1.00";
    private static final String API_KEY_COMMENT = ", apiKey: ";
    private static final String INVALID_MSG = "This Merchant Account is Invalid, reason is ";
    private static Integer socketTimeOut;
    private static Integer connectTimeOut;
    
    private TDMerchantCommunicationService tdMerchantCommunicationService;
    
    private String merchantID;
    private String apiKey;
    private Gateway beanStream;
    private CreditCardTestNumberService testCreditCardNumberService;
    
    public TDMerchantProcessor() {
        super();
    }
    
    public TDMerchantProcessor(final MerchantAccount merchantAccount, final CommonProcessingService commonProcessingService,
            final EmsPropertiesService emsPropertiesService, final TDMerchantCommunicationService tdMerchantCommunicationService,
            final CustomerCardTypeService customerCardTypeService, final CreditCardTestNumberService testCreditCardNumberService) {
        super(merchantAccount, commonProcessingService, emsPropertiesService);
        this.setCustomerCardTypeService(customerCardTypeService);
        this.merchantID = merchantAccount.getField1();
        this.apiKey = merchantAccount.getField2();
        this.tdMerchantCommunicationService = tdMerchantCommunicationService;
        this.testCreditCardNumberService = testCreditCardNumberService;
        setupEmsProperties();
        this.beanStream = createGatewayWithCustomSettings(Integer.parseInt(this.merchantID), this.apiKey);
    }
    
    private void setupEmsProperties() {
        
        if (TDMerchantProcessor.socketTimeOut == null) {
            socketTimeOut = super.getEmsPropertiesService().getPropertyValueAsInt(EmsPropertiesService.TD_MERCHANT_SOCKET_TIMEOUT,
                                                                                  EmsPropertiesService.DEFAULT_TD_MERCHANT_SOCKET_TIMEOUT);
        }
        
        if (TDMerchantProcessor.connectTimeOut == null) {
            connectTimeOut = super.getEmsPropertiesService().getPropertyValueAsInt(EmsPropertiesService.TD_MERCHANT_CONNECTION_TIMEOUT,
                                                                                   EmsPropertiesService.DEFAULT_TD_MERCHANT_CONNECTION_TIMEOUT);
        }
        
    }
    
    private Gateway createGatewayWithCustomSettings(final int merID, final String apiKeyIn) {
        final RequestConfig reqC = RequestConfig.custom().setSocketTimeout(socketTimeOut).setConnectTimeout(connectTimeOut).build();
        final HttpClient client = HttpClients.custom().setDefaultRequestConfig(reqC).build();
        // Gateway(String version, int merchantId, String apiKeyPayments, String apiKeyProfiles, String apiKeyReporting) 
        final Gateway beanstream = new Gateway(super.getEmsPropertiesService().getPropertyValue(EmsPropertiesService.TD_MERCHANT_GATEWAY_VERSION),
                merID, apiKeyIn, apiKeyIn, apiKeyIn);
        beanstream.setCustomHttpsClient(client);
        return beanstream;
    }
    
    @Override
    public final boolean processPreAuth(final PreAuth preAuth) {
        logger.info("TD Merchant Processor preAuth");
        final String methodName = "processPreAuth";
        final CustomerCardType customerCardType =
                getCustomerCardTypeService().getCardTypeByTrack2Data(getMerchantAccount().getCustomer().getId(), preAuth.getCardData());
        final Track2Card track2Card = new Track2Card(customerCardType, getMerchantAccount().getCustomer().getId(), preAuth.getCardData());
        
        final String newReferenceNumber = this.getCommonProcessingService().getNewReferenceNumber(preAuth.getMerchantAccount());
        final String amountToPreAuth = CardProcessingUtil.getCentsAmountInDollars(preAuth.getAmount()).toString();
        preAuth.setReferenceId(newReferenceNumber);
        
        final CardPaymentRequest req = new CardPaymentRequest();
        
        req.setAmount(amountToPreAuth).setOrderNumber(newReferenceNumber);
        
        req.getCard().setNumber(track2Card.getPrimaryAccountNumber()).setExpiryMonth(track2Card.getExpirationMonth())
                .setExpiryYear(track2Card.getExpirationYear());
        
        preAuth.setProcessingDate(new Date());
        
        printPaymentRequestIfInfoEnabled(methodName, req);
        try {
            final PaymentResponse response = this.tdMerchantCommunicationService.sendPreAuthRequest(this.beanStream, req);
            printApprovedPaymentResponseIfInfoEnabled(response);
            
            if (response.isApproved()) {
                preAuth.setIsApproved(true);
                preAuth.setReferenceNumber(response.orderNumber);
                preAuth.setAuthorizationNumber(response.id);
                preAuth.setReferenceId(response.authCode);
                preAuth.setResponseCode(response.messageId);
            }
        } catch (BeanstreamApiException ex) {
            final String detailedMsg = createDetailedExceptionLog(ex, methodName, StandardConstants.STRING_NA_PARANTHESIS);
            logger.info(detailedMsg);
            preAuth.setIsApproved(false);
            preAuth.setReferenceNumber(newReferenceNumber);
            preAuth.setResponseCode(String.valueOf(ex.getCode()) + StandardConstants.STRING_DASH + String.valueOf(ex.getCategory()));
            preAuth.setAuthorizationNumber(null);
            preAuth.setReferenceId(null);
            
            /*
             * If the exception is caused by their backend system error, we should return EMSUnavailable to PS.
             */
            if (!(ex instanceof BusinessRuleException)) {
                throw new CardTransactionException(detailedMsg, ex);
            }
        }
        return preAuth.isIsApproved();
    }
    
    @Override
    public final Object[] processPostAuth(final PreAuth preAuth, final ProcessorTransaction processorTransaction) {
        final String methodName = "processPostAuth";
        logger.info("TD Merchant Processor postAuth");
        
        String responseCode = null;
        final String authorizationNumber = preAuth.getAuthorizationNumber();
        final Double amountToPostAuth = CardProcessingUtil.getCentsAmountInDollars(preAuth.getAmount()).doubleValue();
        processorTransaction.setProcessingDate(new Date());
        
        printCompletionOrReturnRequestIfInfoEnabled(methodName, authorizationNumber, amountToPostAuth);
        try {
            final PaymentResponse postAuthResponse =
                    this.tdMerchantCommunicationService.sendPostAuthRequest(this.beanStream, authorizationNumber, amountToPostAuth);
            printApprovedPaymentResponseIfInfoEnabled(postAuthResponse);
            
            responseCode = postAuthResponse.messageId;
            if (postAuthResponse.isApproved()) {
                
                processorTransaction.setIsApproved(true);
                processorTransaction.setAuthorizationNumber(postAuthResponse.authCode);
                processorTransaction.setProcessorTransactionId(postAuthResponse.id);
                processorTransaction.setReferenceNumber(postAuthResponse.orderNumber);
                return new Object[] { true, responseCode };
            }
        } catch (BeanstreamApiException ex) {
            final String detailedMsg = createDetailedExceptionLog(ex, methodName, authorizationNumber);
            logger.info(detailedMsg);
            responseCode = String.valueOf(ex.getCode()) + StandardConstants.STRING_DASH + String.valueOf(ex.getCategory());
            
            if (ex.getCode() == CardProcessingConstants.BEANSTREAM_COMPLETION_GREATER_THAN_REMAINING_CODE
                && ex.getCategory() == CardProcessingConstants.BEANSTREAM_COMPLETION_GREATER_THAN_REMAINING_CATEGORY) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append(responseCode).append(" --> Since settlement has been completed at Beanstream, ");
                bdr.append("need to complete the transaction by setting:\r\n");
                bdr.append(StandardConstants.STRING_TAB).append("'IsApproved' to 1, ");
                bdr.append(StandardConstants.STRING_TAB).append("'AuthorizationNumber' to preAuth.getAuthorizationNumber + '-' + amount, ");
                bdr.append(StandardConstants.STRING_TAB).append("'ProcessorTransactionId' to N/A, ");
                bdr.append(StandardConstants.STRING_TAB).append("'ReferenceNumber' to N/A ");
                bdr.append(".");
                logger.warn(bdr.toString());
                
                processorTransaction.setIsApproved(true);
                processorTransaction.setAuthorizationNumber(authorizationNumber + StandardConstants.STRING_DASH + amountToPostAuth);
                processorTransaction.setProcessorTransactionId(WebCoreConstants.N_A_STRING);
                processorTransaction.setReferenceNumber(WebCoreConstants.N_A_STRING);
                return new Object[] { true, responseCode };
                
            } else {
                processorTransaction.setIsApproved(false);
                processorTransaction.setAuthorizationNumber(null);
            }
        }
        
        return new Object[] { false, responseCode };
    }
    
    @Override
    public final Object[] processRefund(final boolean isNonEmsRequest, final ProcessorTransaction origProcessorTransaction,
        final ProcessorTransaction refundProcessorTransaction, final String creditCardNumber, final String expiryMMYY) {
        final String methodName = "processRefund";
        logger.info("TD Merchant Processor refund");
        
        String responseCode = null;
        refundProcessorTransaction.setProcessingDate(new Date());
        final String postAuthTransactionId = origProcessorTransaction.getProcessorTransactionId();
        final Double amountToRefund = CardProcessingUtil.getCentsAmountInDollars(origProcessorTransaction.getAmount()).doubleValue();
        
        printCompletionOrReturnRequestIfInfoEnabled(methodName, postAuthTransactionId, amountToRefund);
        try {
            final PaymentResponse refundResponse =
                    this.tdMerchantCommunicationService.sendRefundRequest(this.beanStream, postAuthTransactionId, amountToRefund);
            printApprovedPaymentResponseIfInfoEnabled(refundResponse);
            
            responseCode = refundResponse.messageId;
            if (refundResponse.isApproved()) {
                refundProcessorTransaction.setIsApproved(true);
                refundProcessorTransaction.setProcessorTransactionId(refundResponse.id);
                refundProcessorTransaction.setReferenceNumber(refundResponse.orderNumber);
                refundProcessorTransaction.setAuthorizationNumber(refundResponse.authCode);
                return new Object[] { DPTResponseCodesMapping.CC_APPROVED_VAL__7000, responseCode };
            }
        } catch (BeanstreamApiException ex) {
            final String detailedMsg = createDetailedExceptionLog(ex, methodName, postAuthTransactionId);
            logger.info(detailedMsg);
            responseCode = String.valueOf(ex.getCode()) + StandardConstants.STRING_DASH + String.valueOf(ex.getCategory());
            refundProcessorTransaction.setIsApproved(false);
            refundProcessorTransaction.setAuthorizationNumber(null);
        }
        return new Object[] { DPTResponseCodesMapping.CC_DECLINED_VAL__7003, responseCode };
    }
    
    @Override
    public final Object[] processCharge(final Track2Card track2Card, final ProcessorTransaction chargeProcessorTransaction) {
        logger.info("TD Merchant Processor processCharge");
        final String methodName = "processCharge";
        String responseCode = null;
        final Double amountToCharge = CardProcessingUtil.getCentsAmountInDollars(chargeProcessorTransaction.getAmount()).doubleValue();
        
        /*
         * Query Beanstream if transaction has been completed successfully. If not, continue the charging.
         */
        try {
            final List<TransactionRecord> recs = this.tdMerchantCommunicationService
                    .searchTransaction(this.beanStream, chargeProcessorTransaction.getPointOfSale().getId(),
                                       chargeProcessorTransaction.getPurchasedDate(), chargeProcessorTransaction.getTicketNumber());
            // Response value 1 = Approved.
            if (recs != null && !recs.isEmpty() && recs.get(0).getResponse() == 1) {
                final TransactionRecord trec = recs.get(0);
                
                final StringBuilder bdr = new StringBuilder();
                bdr.append("Beanstream has an existing transaction record for merchantID: ").append(this.merchantID);
                bdr.append(API_KEY_COMMENT).append(this.apiKey).append(", OrderNumber (ReferenceNumber): ");
                bdr.append(trec.getOrderNumber()).append(", Transaction Id: ").append(trec.getTransactionId());
                logger.info(bdr.toString());
                
                Date processingDate;
                try {
                    final String dateTime = DateUtil.convertTimeZoneAndCreateDateString(WebCoreConstants.GMT, trec.getDateTime());
                    final SimpleDateFormat formatter = new SimpleDateFormat(DateUtil.DEFAULT_DATE_FORMAT);
                    processingDate = formatter.parse(dateTime);
                } catch (ParseException pe) {
                    logger.error("Error parsing Beanstream date format to GMT, save processingDate using theirs.", pe);
                    try {
                        processingDate = trec.getDateTime();
                    } catch (ParseException beanstreamPe) {
                        logger.error("Beanstream TransactionRecord getDateTime throws ParseException because NO dateTime in their record, ",
                                     beanstreamPe);
                        processingDate = DateUtil.getCurrentGmtDate();
                    }
                }
                chargeProcessorTransaction.setIsApproved(true);
                chargeProcessorTransaction.setProcessingDate(processingDate);
                chargeProcessorTransaction.setReferenceNumber(trec.getOrderNumber());
                chargeProcessorTransaction.setProcessorTransactionId(String.valueOf(trec.getTransactionId()));
                
                return new Object[] { DPTResponseCodesMapping.CC_APPROVED_VAL__7000, trec.getMessageId() };
            }
        } catch (BeanstreamApiException bae) {
            throw new CardTransactionException(bae);
        }
        
        final String newReferenceNumber = getCommonProcessingService().getNewReferenceNumber(getMerchantAccount());
        final CardPaymentRequest req = new CardPaymentRequest();
        req.setAmount(amountToCharge).setOrderNumber(newReferenceNumber);
        req.getCard().setNumber(track2Card.getPrimaryAccountNumber()).setExpiryMonth(track2Card.getExpirationMonth())
                .setExpiryYear(track2Card.getExpirationYear());
        
        chargeProcessorTransaction.setProcessingDate(new Date());
        chargeProcessorTransaction.setReferenceNumber(newReferenceNumber);
        
        printPaymentRequestIfInfoEnabled(methodName, req);
        try {
            final PaymentResponse chargeResponse = this.tdMerchantCommunicationService.makePayment(this.beanStream, req);
            printApprovedPaymentResponseIfInfoEnabled(chargeResponse);
            
            responseCode = chargeResponse.messageId;
            
            if (chargeResponse.isApproved()) {
                chargeProcessorTransaction.setIsApproved(true);
                chargeProcessorTransaction.setAuthorizationNumber(chargeResponse.authCode);
                chargeProcessorTransaction.setProcessorTransactionId(chargeResponse.id);
                return new Object[] { DPTResponseCodesMapping.CC_APPROVED_VAL__7000, responseCode };
            }
        } catch (BeanstreamApiException ex) {
            final String detailedMsg = createDetailedExceptionLog(ex, methodName, StandardConstants.STRING_NA_PARANTHESIS);
            logger.info(detailedMsg);
            responseCode = String.valueOf(ex.getCode()) + StandardConstants.STRING_DASH + String.valueOf(ex.getCategory());
            chargeProcessorTransaction.setIsApproved(false);
            chargeProcessorTransaction.setAuthorizationNumber(null);
            
            /*
             * If the exception is caused by their backend system error, we should try this again.
             */
            if (!(ex instanceof BusinessRuleException)) {
                throw new CardTransactionException(detailedMsg, ex);
            }
        }
        
        return new Object[] { DPTResponseCodesMapping.CC_DECLINED_VAL__7003, responseCode };
    }
    
    @Override
    public final void processReversal(final Reversal reversal, final Track2Card track2Card) {
        throw new UnsupportedOperationException("TD Merchant, Reversal not supported for this processor");
    }
    
    @Override
    public final boolean isReversalExpired(final Reversal reversal) {
        throw new UnsupportedOperationException("TD Merchant, Reversal not supported for this processor");
    }
    
    @Override
    public final String testCharge(final MerchantAccount ma) {
        logger.info("TD Merchant testCharge");
        final String methodName = "testCharge";
        final String newReferenceNumber = getCommonProcessingService().getNewReferenceNumber(getMerchantAccount());
        final CardPaymentRequest req = new CardPaymentRequest();
        req.setAmount(TEST_CHARGE_AMOUNT).setOrderNumber(newReferenceNumber);
        req.getCard().setNumber(this.testCreditCardNumberService.getDefaultTestTDMerchantCardNumberDeclined())
                .setExpiryMonth(CardProcessingConstants.DEFAULT_TEST_TD_MERCHANT_CARD_NUMBER_EXPIRY_MONTH).setExpiryYear(createYYInTwoYear());
        //Since this test card it will always get rejected and throw a bean stream exception we want to make sure that the category of error is 1.
        printPaymentRequestIfInfoEnabled(methodName, req);
        try {
            final PaymentResponse resp = this.tdMerchantCommunicationService.makePayment(this.beanStream, req);
            printApprovedPaymentResponseIfInfoEnabled(resp);
            return WebCoreConstants.RESPONSE_TRUE;
        } catch (BeanstreamApiException ex) {
            if (ex.getCategory() == TD_MERCHANT_CATEGORY_TRANSACTION_PROCESSED) {
                return WebCoreConstants.RESPONSE_TRUE;
            } else {
                final StringBuilder logString = new StringBuilder();
                final String errorMessage = new StringBuffer(WebCoreConstants.RESPONSE_FALSE).append(StandardConstants.STRING_COLON)
                        .append(INVALID_MSG).append(ex.getMessage()).toString();
                logString.append("TD Merchant testCharge failed (Category is not 1) ");
                logString.append("Code: ").append(ex.getCode()).append(" Category: ").append(ex.getCategory()).append(" Message: ")
                        .append(ex.getMessage());
                logger.info(logString);
                return errorMessage;
            }
        }
    }
    
    private String createDetailedExceptionLog(final BeanstreamApiException exception, final String target, final String authorizationNumber) {
        final StringBuilder logString = new StringBuilder();
        logString.append("TD Merchant failed for ").append(target).append(StandardConstants.STRING_EMPTY_SPACE);
        logString.append(authorizationNumber).append(", Code: ").append(exception.getCode()).append(", Category: ");
        logString.append(exception.getCategory()).append(", Message: ").append(exception.getMessage());
        return logString.toString();
    }
    
    private String createYYInTwoYear() {
        final Calendar cal = new GregorianCalendar();
        cal.add(Calendar.YEAR, 2);
        final SimpleDateFormat formatter = new SimpleDateFormat("yy");
        return formatter.format(cal.getTime());
    }
    
    private void printPaymentRequestIfInfoEnabled(final String requestType, final CardPaymentRequest cardReq) {
        if (logger.isInfoEnabled()) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append(TD_MERCHANT_REQ_TYPE_STRING).append(requestType);
            bdr.append(StandardConstants.STRING_NEXT_LINE).append("merchantId: ").append(this.merchantID);
            bdr.append(API_KEY_COMMENT).append(this.apiKey).append(", amount: ").append(cardReq.getAmount());
            bdr.append(ORDER_NUMBER_DEBUG).append(cardReq.getOrderNumber()).append(StandardConstants.STRING_NEXT_LINE);
            if (cardReq.getCard() != null && StringUtils.isNotBlank(cardReq.getCard().getName())) {
                final String last4d = cardReq.getCard().getNumber().substring(cardReq.getCard().getNumber().length() - StandardConstants.CONSTANT_4);
                bdr.append("CC last 4 digits: ").append(last4d).append(", Exp yyMM: ");
                bdr.append(cardReq.getCard().getExpiryYear()).append(cardReq.getCard().getExpiryMonth());
            }
            logger.info(bdr.toString());
        }
    }
    
    private void printCompletionOrReturnRequestIfInfoEnabled(final String requestType, final String authorizationNumber,
        final Double amountToPostAuth) {
        if (logger.isInfoEnabled()) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append(TD_MERCHANT_REQ_TYPE_STRING).append(requestType).append(StandardConstants.STRING_NEXT_LINE);
            bdr.append(StandardConstants.STRING_NEXT_LINE).append("authorizationNumber: ").append(authorizationNumber);
            bdr.append(", amountToPostAuth: ").append(amountToPostAuth);
            logger.info(bdr.toString());
        }
    }
    
    private void printApprovedPaymentResponseIfInfoEnabled(final PaymentResponse response) {
        if (logger.isInfoEnabled()) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("Approved, authCode: ").append(response.authCode).append(", messageId: ").append(response.messageId);
            bdr.append(ORDER_NUMBER_DEBUG).append(response.orderNumber).append(", message: ").append(response.message);
            logger.info(bdr.toString());
        }
    }
}
