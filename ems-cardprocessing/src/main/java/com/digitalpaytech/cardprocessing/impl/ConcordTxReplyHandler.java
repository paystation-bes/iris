package com.digitalpaytech.cardprocessing.impl;

import com.digitalpaytech.util.CardProcessingConstants;


// Copied from com.digitalpioneer.cardprocessing.creditcardprocessors.concord.TransactionReplyHandler

public class ConcordTxReplyHandler {
	private String responseCode = null;
	private String resultCode = null;
	private String resultMessage = null;
	private StringBuffer errorMessage_ = null;
	//  the transaction Id generated and returned by concord...do not confuse with EMS transaction Id
	private String transactionID = null;
	private String authorizationNumber = null;
	private float transactionAmount = 0;
	private String referenceNumber = null;
	private String approvalNumber = null;
	private String transactionDate = null;
	private String transactionTime = null;
	//for QueryTransactionsMethod
	private String originalTransactionID = null;
	private float originalTransactionAmount = 0;

	public void initializationError(String initializationError) {
		errorMessage_.append(initializationError);
	}

	public void invalidData(String fieldName, String value) {
		errorMessage_.append("Invalid data: <").append(fieldName).append(">").append(value).append("<").append(fieldName).append("> for ").append(getRootElementName()).append(". ");	
	}

	public String getRootElementName() {
		return CardProcessingConstants.XML_RESPONSE_ROOT_ELEMENT_NAME;
	}

	public String getResponseCode() {
		return responseCode;
	}
  
	public void setResponseCode(String string) {
		responseCode = string;
	}

	public String getResultCode() {
		return resultCode;
	}
	
	public void setResultCode(String string) {
		resultCode = string;
	}
	
	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String string) {
		resultMessage = string;
	}

	public String getAuthorizationNumber() {
		return authorizationNumber;
	}

	public void setAuthorizationNumber(String string) {
		authorizationNumber = string;
	}

	public Float getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(String amount) {
		transactionAmount = Float.parseFloat(amount);
	}

	public String getTransactionID() {
		return transactionID;
	}

	public void setTransactionID(String string) {
		transactionID = string;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String string) {
		referenceNumber = string;
	}

	public String getApprovalNumber() {
		return approvalNumber;
	}

	public void setApprovalNumber(String string) {
		approvalNumber = string;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String string) {
		transactionDate = string;
	}

	public String getTransactionTime() {
		return transactionTime;
	}

	public void setTransactionTime(String string) {
		transactionTime = string;
	}

	public Float getOriginalTransactionAmount() {
		return originalTransactionAmount;
	}

	public void setOriginalTransactionAmount(String amount) {
		originalTransactionAmount = Float.parseFloat(amount);
	}

	public String getOriginalTransactionID() {
		return originalTransactionID;
	}

	public void setOriginalTransactionID(String string) {
		originalTransactionID = string;
	}
}
