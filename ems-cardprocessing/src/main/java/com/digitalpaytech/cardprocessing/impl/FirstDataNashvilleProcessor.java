package com.digitalpaytech.cardprocessing.impl;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.TreeMap;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DPTResponseCodesMapping;
import com.digitalpaytech.util.iso8583.FixLengthBcdType;
import com.digitalpaytech.util.iso8583.Iso8583ProtocolFactory;


public class FirstDataNashvilleProcessor extends DatawireProcessor 
{
    private static Logger logger = Logger.getLogger(FirstDataNashvilleProcessor.class);
    private final static long TWENTYFIVE_MINUTES_MS = 25 * 60 * 1000;
    private final static long DEFAULT_MAX_REVERSAL_RETRIES = 120;
    
    private final static String AUTHORIZATION_REQUEST = "0100";
    private final static String AUTHORIZATION_RESPONSE = "0110";
    private final static String AUTHORIZATION_PROCESSING_CODE = "000000";
    private final static String AUTHORIZATION_REFERENCE_DATA = "0"; // authorization only
    private final static String AUTHORIZATION_APPROVED_CODE = "00";

    private final static String CAPTURE_REQUEST = "0100";
    private final static String CAPTURE_RESPONSE = "0110";
    private final static String CAPTURE_PROCESSING_CODE = "000000";
    private final static String CAPTURE_REFERENCE_DATA = "2"; // capture only
    private final static String CAPTURE_APPROVED_CODE = "00";

    private final static String SALE_REQUEST = "0100";
    private final static String SALE_RESPONSE = "0110";
    private final static String SALE_PROCESSING_CODE = "000000";
    private final static String SALE_REFERENCE_DATA = "1"; // authorization/capture
    private final static String SALE_APPROVED_CODE = "00";

    private final static String REFUND_REQUEST = "0100";
    private final static String REFUND_RESPONSE = "0110";
    private final static String REFUND_PROCESSING_CODE = "200000";
    private final static String REFUND_REFERENCE_DATA = "2"; // capture only
    private final static String REFUND_APPROVED_CODE = "00";

    private final static String REVERSAL_REQUEST = "0400";
    private final static String REVERSAL_RESPONSE = "0410";
    private final static String REVERSAL_PROCESSING_CODE = "000000";
    private final static String REVERSAL_APPROVED_CODE = "00";

    private static String MERCAHNT_CATEGORY_CODE = "7523";
    private final static String TIME_FORMAT = "HHmmss";
    private final static String ONE_ZERO_STRING = "0";
    private final static String VERSION_NUMBER = "633000";
    private final static String NETWORK_INTERNATIONAL_ID = "0001";

    private final static String MERCHANT_POSTCODE = "V5C6B4";

    private final static String TPPID = "TDP111";
    
    private final static String BIT_60_SWIPE = "32"; 
    private final static String BIT_60_RFID = "37"; 
    private final static String BIT_22_MANUAL = "0010"; 
    private final static String BIT_22_RFID = "0910"; 
    private final static String BIT_22_SWIPE = "0900";

    private String referenceNumber;
    private String processorTransactionId;
    private int merchantAccountId; // merchant table primary key
    private String selfRegistrationUrl_ = null;
    
    private String zipCode;
    private String merchantCategoryCode;


    public FirstDataNashvilleProcessor(MerchantAccount md, CommonProcessingService commonProcessingService, EmsPropertiesService emsPropertiesService,
                                       MerchantAccountService merchantAccountService, CustomerCardTypeService customerCardTypeService) 
    {
        super(md, commonProcessingService, emsPropertiesService);
        setMerchantAccountService(merchantAccountService);
        setCustomerCardTypeService(customerCardTypeService);
        
        DATAWIRE_APPLICATION_ID = "DIGITALPTEMSJAV2";
        DATAWIRE_SERVICE_ID = "115";
        merchantAccountId = md.getId();
        setMerchantId(md.getField1());
        setTerminalId(md.getField2());
        setDatawireId(md.getField3());
        String sd_url_list = md.getField4();
        zipCode = (md.getField5() == null || md.getField5().equals(""))? MERCHANT_POSTCODE : md.getField5();
        merchantCategoryCode = (md.getField6() == null || md.getField6().equals(""))? MERCAHNT_CATEGORY_CODE : md.getField6();
        List<String> url_list = new ArrayList<String>(4);

        logger.debug("+++ ZIP CODE: " + zipCode);
        logger.debug("+++ MERCAHNT CATEGORY CODE: " + merchantCategoryCode);
        if (getDatawireId() == null || getDatawireId().length() == 0)
        {
            boolean is_test_server = getCommonProcessingService().isProcessorTestMode(md.getProcessor().getId());
            if (md.getProcessor() != null)
            {
                if (md.getProcessor().isIsTest())
                {
                    selfRegistrationUrl_ = md.getProcessor().getTestUrl();
                }
                else
                {
                    selfRegistrationUrl_ = md.getProcessor().getProductionUrl();
                }
                selfRegisterMerchant(is_test_server, md);
            }
            else
            {
                logger.error("!!! No First Data Nashville info in Processor table  !!!");
            }
            
        }
        else
        // self-registration has already been done for this merchant account
        {
            StringTokenizer st = new StringTokenizer(sd_url_list, "|");
            while (st.hasMoreTokens())
            {
                String url = st.nextToken();
                url_list.add(url);
            }
            setUrlList(url_list);
        }
        
        StringBuilder bdr = new StringBuilder();
        bdr.append("Self-Discovery URL: ").append(url_list).append(", DID: ").append(getDatawireId()).append(", MerchantAccountId: ").append(merchantAccountId);
        logger.info(bdr.toString());
    }

    
    @Override
    public boolean processPreAuth(PreAuth pd) throws CardTransactionException, CardTransactionException
    {
        return processAuthorization(pd);
    }

    /**
     * message format: Message Type: 0100 <br/>
     * 
     * Primary Bitmap: <br/>
     * <p>
     * decimal: <br/>
     * setting: 0111 0000||0 0 1 1 0 0 0 0|| 0 1 0 0 0 1 0 1 || 1 0 0 0 0 0 1 0 || <br/>
     * Bit#: 1234 5678 9,10,11,12 13,14,15,16 17,18,19,20,21,22,23,24, 25,26,27,28 29,30,31,32, <br/>
     * 
     * decimal: <br/>
     * setting: 0 0 1 0 1 0 0 0 || 1 1 0 0 0 0 0 0 || 0 0 0 0 0 0 0 0 || <br/>
     * Bit#: 33,34,35,36 37,38,39,40, 41,42,43,44 45,46,47,48, 49,50,51,52 53,54,55,56 <br/>
     * 
     * decimal: <br/>
     * setting: 0 0 1 1 0 0 0 0|| <br/>
     * Bit#: 57,58,59,60 61,62,63,64 <br/>
     * 
     * <br/>
     * b2 PAN (.. 10 bytes, 19 digits): last 1/2 byte can pad binary zero, preceded by 1 byte length indicator <br/>
     * b3 Processing code (3 bytes, 6d): 000000 <br/>
     * b4 Amount (6 bytes, 12d): left padding with "0" <br/>
     * 
     * b11 System Trace(3 bytes, 6d): getNewReferenceNumber() - 6 digital of right, can not be 000000 <br/>
     * b12 Time, Local Transmission (3 bytes, 6 digits): hhmmss <br/>
     * b14 Card Expiration day (2 bytes, 4 digits): YYMM <br/>
     * b18 Merchant Category Code (2 bytes, 4d): 5999 <br/>
     * b22 POS entry Mode (2 byte, 3d): 0010(manual), 0900 (magnetic stripe) <br/>
     * 
     * b24 Network International ID (2 * bytes, 3d): 0001 <br/>
     * b25 POS Condition Code: (1 byte, 2d) 00 - present, 01 - not present <br/>
     * b31 Acquirer Reference Data 1 - Authorization/Capture <br/>
     * b35 Track 2 Data ( up to 37 byte) <br/>
     * b37 Retrieval Reference Number (12 bytes, 12chars): getNewReferenceNumber - 12 digital <br/>
     * b41 Terminal ID (8 bytes, 8 chars): left padding with "0" <br/>
     * b42 Merchant ID (15 bytes, 15 chars): left padding with "0" <br/>
     * b59 Merchant Zip/Postal Code (9 bytes): right padding with "0" -- V5C 6B4 <br/>
     * b60 Additional POS Information (1 byte, 2 digits): 
     * </p>
     */
    private boolean processAuthorization(PreAuth preAuth) throws CardTransactionException, CardTransactionException
    {
        String responseCode;
        CustomerCardType customerCardType = getCustomerCardTypeService().getCardTypeByTrack2Data(getMerchantAccount().getCustomer().getId(), preAuth.getCardData());
        Track2Card track2Card = new Track2Card(customerCardType, getMerchantAccount().getCustomer().getId(), preAuth.getCardData());

        // request data
        TreeMap<Integer, String> bitsData = new TreeMap();
        bitsData.put(0, AUTHORIZATION_REQUEST); // Message Type
        // primary bitmap
        bitsData.put(2, track2Card.getPrimaryAccountNumber()); // b2, --> not really need, used for mark CC number
                                                                                                                        // in log
        bitsData.put(3, AUTHORIZATION_PROCESSING_CODE); // b3, processing code
        bitsData.put(4, formatDollarAmount(preAuth.getAmount(), 12)); // b4, amount of transaction
        String ref = getSystemTrace();
        bitsData.put(11, ref); // b11, system trace 000000 -> not allowed
        bitsData.put(12, getTransactionTimeLocal(new Date())); // b12, Time, Local Transmission
        // bitsData.put(14, track2Card.getExpirationYear() + track2Card.getExpirationMonth()); // b14, card
        // expiration date
        bitsData.put(18, merchantCategoryCode); // b18, Merchant Category Code
        if (preAuth.isIsRfid())
        {
            bitsData.put(22, BIT_22_RFID);
        }
        else
        {
            bitsData.put(22, BIT_22_SWIPE); // b22, magnetic
        }
        bitsData.put(24, NETWORK_INTERNATIONAL_ID); // b24, Network International ID
        bitsData.put(25, "00"); // b25, POS condition code 00 - normal

        bitsData.put(31, AUTHORIZATION_REFERENCE_DATA); // b31,
        String t2d = track2Card.getDecryptedCardData().replaceAll("=", "D");
        bitsData.put(35, t2d); // b35,
        String retrievalReferenceNumber = getRetrievalReferenceNumber(ref);
        bitsData.put(37, retrievalReferenceNumber); // b37,
        bitsData.put(41, this.getTerminalId()); // b41,
        bitsData.put(42, this.getMerchantId()); // b42,
        bitsData.put(59, zipCode); // b59,
        if (preAuth.isIsRfid())
        {
            bitsData.put(60, BIT_60_RFID); // 
        }
        else
        {
            bitsData.put(60, BIT_60_SWIPE); // // b22, magnetic
        }
        if (logger.isDebugEnabled()) {
            logger.debug("+++++ CC card type: " + track2Card.getDisplayName() + " ++++");
        }
        String tables = "";
        if (track2Card.getDisplayName().equals(CardProcessingConstants.NAME_VISA))
        {
            tables = buildBit63ForVisa(AUTHORIZATION_REFERENCE_DATA, "", preAuth.getAmount(), true);
        }
        else if (track2Card.getDisplayName().equals(CardProcessingConstants.NAME_MASTERCARD))
        {
            tables = buildBit63ForMasterCard(AUTHORIZATION_REFERENCE_DATA, "", preAuth.getAmount(), true);
        }
        else if (track2Card.getDisplayName().equals(CardProcessingConstants.NAME_AMEX))
        {
            tables = buildBit63ForAE(AUTHORIZATION_REFERENCE_DATA, "", preAuth.getAmount(), true);
        }
        else if (track2Card.getDisplayName().equals(CardProcessingConstants.NAME_DISCOVER)
                || track2Card.getDisplayName().equals(CardProcessingConstants.NAME_DINERS))
        {
            tables = buildBit63ForDiscoverDiners(AUTHORIZATION_REFERENCE_DATA, "", preAuth.getAmount(), true);
        }
        else
        {
            tables = buildBit63ForOthers(AUTHORIZATION_REFERENCE_DATA);
        }
        if (tables != null && !tables.isEmpty())
        {
            bitsData.put(63, tables);
        }
        if (logger.isDebugEnabled())
        {
            logger.debug("+++++ sending fields +++++");
            Set set = bitsData.entrySet();
            Iterator i = set.iterator();
            StringBuffer sb = new StringBuffer();
            while (i.hasNext())
            {
                Map.Entry me = (Map.Entry)i.next();
                if (((Integer)me.getKey()).intValue() == 35 || ((Integer)me.getKey()).intValue() == 2)
                {
                    sb.append(me.getKey()).append("=").append("XXXXXXXXXXXXXX").append(", ");
                }
                else
                {
                    sb.append(me.getKey()).append("=").append(me.getValue()).append(", ");
                }
            }
            logger.debug(sb.toString());
        }
        // Send request
        try
        {
            char[] payLoad = Iso8583ProtocolFactory.getInstance().getFirstDataRequestBuilder().build(bitsData);
            String clientRef = getClientRef(ref);
            char[] response = sendRequest(payLoad, clientRef);
            logger.debug("++++ response hex string: " + CardProcessingUtil.toHexString(new String(response)));
            HashMap<Integer, String> rtnData = Iso8583ProtocolFactory.getInstance().getFirstDataResponseParser()
                    .parse(response);
            logger.info("==== Authorization response ===");
            logger.info(rtnData);
            logger.info("==================================");
            logger.info(parseTablesFromBit63(rtnData.get(63)));
            logger.info("==================================");
            preAuth.setProcessingDate(new Date());
            preAuth.setReferenceNumber(ref);
            //
            // bit 37, transaction id
            //
            String transactionId = rtnData.get(37);
            preAuth.setProcessorTransactionId(transactionId);
            // 
            // Bit 39, response code
            //
            responseCode = rtnData.get(39);
            logger.debug("response code: " + responseCode);
            if (responseCode.equals(AUTHORIZATION_APPROVED_CODE))
            {
                //
                // bit 38, authorization number
                //
                preAuth.setAuthorizationNumber(rtnData.get(38));
                preAuth.setIsApproved(true);
                preAuth.setCardData(track2Card.getCreditCardPanAndExpiry());
                logger.debug("++++ bit63(hex): " + CardProcessingUtil.toHexString(rtnData.get(63)));

                String st1 = "";
                try
                {
                    st1 = new String(Base64.encodeBase64(rtnData.get(63).getBytes("UTF-8")));
                    preAuth.setExtraData(st1);
                }
                catch (UnsupportedEncodingException e)
                {
                    // not possible
                }
                logger.debug("=========================");
                logger.debug("++++ bit63(encoded): " + st1);
                String st2 = "";
                try
                {
                    st2 = new String(Base64.decodeBase64(st1.getBytes()), "UTF-8");
                }
                catch (UnsupportedEncodingException e)
                {
                    // not possible
                }
                logger.debug("++++ bit63(decoded): " + st2);
                logger.debug("++++ bit63(hex after decoded): " + CardProcessingUtil.toHexString(st2));
            }
            else
            {
                preAuth.setAuthorizationNumber(null);
                preAuth.setIsApproved(false);
            }
            preAuth.setResponseCode(responseCode);
        }
        catch (CardTransactionException e)
        {
            logger.error("CardTransactionException: ", e);
            createAndSendTimeoutReversal(AUTHORIZATION_REFERENCE_DATA, "00", CardProcessingUtil.getCentsAmountInDollars(preAuth.getAmount()), ref,
                    track2Card, merchantAccountId, new Date(), 0, preAuth.getPointOfSale().getId());
            throw e;
        }
        return (preAuth.isIsApproved());
    }

    @Override
    public Object[] processPostAuth(PreAuth preAuth, ProcessorTransaction ptd) throws CardTransactionException,
            CardTransactionException
    {
        return processCapture(preAuth, ptd);
    }

    /**
     * message format: Message Type: 0100 <br/>
     * 
     * Primary Bitmap: <br/>
     * <p>
     * decimal: <br/>
     * setting: 0111 0000||0 0 1 1 0 0 0 0|| 0 1 0 0 0 1 0 1 || 1 0 0 0 0 0 1 0 || <br/>
     * Bit#: 1234 5678 9,10,11,12 13,14,15,16 17,18,19,20,21,22,23,24, 25,26,27,28 29,30,31,32, <br/>
     * 
     * decimal: <br/>
     * setting: 0 0 0 0 1 1 0 0 || 1 1 0 0 0 0 0 0 || 0 0 0 0 0 0 0 0 || <br/>
     * Bit#: 33,34,35,36 37,38,39,40, 41,42,43,44 45,46,47,48, 49,50,51,52 53,54,55,56 <br/>
     * 
     * decimal: <br/>
     * setting: 0 0 1 0 0 0 0 0|| <br/>
     * Bit#: 57,58,59,60 61,62,63,64 <br/>
     * 
     * <br/>
     * b2 PAN (.. 10 bytes, 19 digits): last 1/2 byte can pad binary zero, preceded by 1 byte length indicator <br/>
     * b3 Processing code (3 bytes, 6d): 000000 <br/>
     * b4 Amount (6 bytes, 12d): left padding with "0" <br/>
     * 
     * b11 System Trace(3 bytes, 6d): getNewReferenceNumber() - 6 digital of right, can not be 000000 <br/>
     * b12 Time, Local Transmission (3 bytes, 6 digits): hhmmss <br/>
     * b18 Merchant Category Code (2 bytes, 4d): 5999 <br/>
     * b22 POS entry Mode (2 byte, 3d): 0010(manual), 0900 (magnetic stripe) <br/>
     * 
     * b24 Network International ID (2 * bytes, 3d): 0001 <br/>
     * b25 POS Condition Code: (1 byte, 2d) 00 - present, 01 - not present <br/>
     * b31 Acquirer Reference Data 1 - Authorization/Capture <br/>
     * b37 Retrieval Reference Number (12 bytes, 12chars): getNewReferenceNumber - 12 digital <br/>
     * b38 Authorization Code (6 bytes, 6chars): <br/>
     * b41 Terminal ID (8 bytes, 8 chars): left padding with "0" <br/>
     * b42 Merchant ID (15 bytes, 15 chars): left padding with "0" <br/>
     * b59 Merchant Zip/Postal Code (9 bytes): right padding with "0" -- V5C 6B4 <br/>
     * b60 Additional POS Information (1 byte, 2 digits): 
     * </p>
     */
    private Object[] processCapture(PreAuth pd, ProcessorTransaction ptd) throws CardTransactionException,
            CardTransactionException
    {
        String responseCode;
        CustomerCardType customerCardType = getCustomerCardTypeService().getCardTypeByTrack2DataOrAccountNumber(getMerchantAccount().getCustomer().getId(), pd.getCardData());
        Track2Card track2Card = new Track2Card(customerCardType, getMerchantAccount().getCustomer().getId(), pd.getCardData());
        String b25PosConCode = "00"; // 00 - normal

        // request data
        TreeMap<Integer, String> bitsData = new TreeMap();
        bitsData.put(0, CAPTURE_REQUEST); // Message Type
        // primary bitmap
        bitsData.put(2, track2Card.getPrimaryAccountNumber()); // b2, --> not really need, used for mark CC number
                                                                                                                        // in log
        bitsData.put(3, CAPTURE_PROCESSING_CODE); // b3, processing code
        bitsData.put(4, formatDollarAmount(pd.getAmount(), 12)); // b4, amount of transaction
        String ref = getSystemTrace();
        bitsData.put(11, ref); // b11, system trace 000000 -> not allowed
        bitsData.put(12, getTransactionTimeLocal(ptd.getPurchasedDate())); // b12, Time, Local Transmission
        bitsData.put(18, merchantCategoryCode); // b18, Merchant Category Code
        bitsData.put(22, "0900"); // b22, magnetic
        if (pd.isIsRfid())
        {
            bitsData.put(22, BIT_22_RFID);
        }
        else
        {
            bitsData.put(22, BIT_22_SWIPE); // b22, magnetic
        }

        bitsData.put(24, NETWORK_INTERNATIONAL_ID); // b24, Network International ID
        bitsData.put(25, b25PosConCode); // b25, POS condition code 00 - normal

        bitsData.put(31, CAPTURE_REFERENCE_DATA); // b31,

        String retrievalReferenceNumber = getRetrievalReferenceNumber(ref);
        bitsData.put(37, retrievalReferenceNumber); // b37,
        bitsData.put(38, pd.getAuthorizationNumber()); // b38,
        bitsData.put(41, this.getTerminalId()); // b41,
        bitsData.put(42, this.getMerchantId()); // b42,
        bitsData.put(59, zipCode); // b59,
        if (pd.isIsRfid())
        {
            bitsData.put(60, BIT_60_RFID); 
        }
        else
        {
            bitsData.put(60, BIT_60_SWIPE); 
        }       
        logger.debug("+++++ CC card type: " + track2Card.getDisplayName() + " ++++");
        String tables = "";
        logger.debug("+++++++++++++++++ retrieved bit63 from database: " + pd.getExtraData());
        String bit63 = "";
        try
        {
            bit63 = new String(Base64.decodeBase64(pd.getExtraData().getBytes()), "UTF-8");
        }
        catch (UnsupportedEncodingException e1)
        {
            // not possible
        }
        logger.debug("++++++++++++++ retrieved bit63 from database (decoded): " + bit63);
        if (track2Card.getDisplayName().equals(CardProcessingConstants.NAME_VISA))
        {
            tables = buildBit63ForVisa(CAPTURE_REFERENCE_DATA, bit63, ptd.getAmount(), true);
        }
        else if (track2Card.getDisplayName().equals(CardProcessingConstants.NAME_MASTERCARD))
        {
            tables = buildBit63ForMasterCard(CAPTURE_REFERENCE_DATA, bit63, ptd.getAmount(), true);
        }
        else if (track2Card.getDisplayName().equals(CardProcessingConstants.NAME_AMEX))
        {
            tables = buildBit63ForAE(CAPTURE_REFERENCE_DATA, bit63, ptd.getAmount(), true);
        }
        else if (track2Card.getDisplayName().equals(CardProcessingConstants.NAME_DISCOVER)
                || track2Card.getDisplayName().equals(CardProcessingConstants.NAME_DINERS))
        {
            tables = buildBit63ForDiscoverDiners(CAPTURE_REFERENCE_DATA, bit63, ptd.getAmount(), true);
        }
        else
        {
            tables = buildBit63ForOthers(CAPTURE_REFERENCE_DATA);
        }
        if (tables != null && !tables.isEmpty())
        {
            bitsData.put(63, tables);
        }
        if (logger.isDebugEnabled())
        {
            logger.debug("+++++ sending fields +++++");
            Set set = bitsData.entrySet();
            Iterator i = set.iterator();
            StringBuffer sb = new StringBuffer();
            while (i.hasNext())
            {
                Map.Entry me = (Map.Entry)i.next();
                if (((Integer)me.getKey()).intValue() == 35 || ((Integer)me.getKey()).intValue() == 2)
                {
                    sb.append(me.getKey()).append("=").append("XXXXXXXXXXXXXX").append(", ");
                }
                else
                {
                    sb.append(me.getKey()).append("=").append(me.getValue()).append(", ");
                }
            }
            logger.debug(sb.toString());
        }
        // Send request
        try
        {
            char[] payLoad = Iso8583ProtocolFactory.getInstance().getFirstDataRequestBuilder().build(bitsData);
            String clientRef = getClientRef(ref);
            char[] response = sendRequest(payLoad, clientRef);
            logger.debug("++++ response hex string: " + CardProcessingUtil.toHexString(new String(response)));
            HashMap<Integer, String> rtnData = Iso8583ProtocolFactory.getInstance().getFirstDataResponseParser()
                    .parse(response);
            logger.info("==== Capture response ===");
            logger.info(rtnData);
            logger.info("==================================");

            ptd.setProcessingDate(new Date());
            ptd.setReferenceNumber(ref);
            ptd.setAmount(pd.getAmount());
            //
            // bit 37, transaction id
            //
            String transactionId = rtnData.get(37);
            ptd.setProcessorTransactionId(transactionId);
            // 
            // Bit 39, response code
            //
            responseCode = rtnData.get(39);
            logger.debug("response code: " + responseCode);
            if (responseCode.equals(CAPTURE_APPROVED_CODE))
            {
                //
                // bit 38, authorization number
                //
                ptd.setAuthorizationNumber(rtnData.get(38));
                ptd.setIsApproved(true);
            }
            else
            {
                ptd.setAuthorizationNumber(null);
                ptd.setIsApproved(false);
            }
        }
        catch (CardTransactionException e)
        {
            logger.error("CardTransactionException: ", e);
            createAndSendTimeoutReversal(CAPTURE_REFERENCE_DATA, b25PosConCode, CardProcessingUtil.getCentsAmountInDollars(pd.getAmount()), 
                    ref, track2Card, merchantAccountId, ptd.getPurchasedDate(), ptd.getTicketNumber(), ptd.getPointOfSale().getId());
            throw e;
        }
        return new Object[] {ptd.isIsApproved(), responseCode};
    }

    public Object[] processCharge(Track2Card track2Card, ProcessorTransaction chargePtd)
            throws CardTransactionException, CardTransactionException
    {
        return processSale(track2Card, chargePtd);
    }

    /**
     * message format: Message Type: 0100 <br/>
     * 
     * Primary Bitmap: <br/>
     * <p>
     * decimal: <br/>
     * setting: 0111 0000||0 0 1 1 0 1 0 0|| 0 1 0 0 0 1 0 1 || 1 0 0 0 0 0 1 0 || <br/>
     * Bit#: 1234 5678 9,10,11,12 13,14,15,16 17,18,19,20,21,22,23,24, 25,26,27,28 29,30,31,32, <br/>
     * 
     * decimal: <br/>
     * setting: 0 0 0/1 0 1 0 0 0 || 1 1 0 0 0 0 0 0 || 0 0 0 0 0 0 0 0 || <br/>
     * Bit#: 33,34,35,36 37,38,39,40, 41,42,43,44 45,46,47,48, 49,50,51,52 53,54,55,56 <br/>
     * 
     * decimal: <br/>
     * setting: 0 0 1 1 0 0 0 0|| <br/>
     * Bit#: 57,58,59,60 61,62,63,64 <br/>
     * 
     * <br/>
     * b2 PAN (.. 10 bytes, 19 digits): last 1/2 byte can pad binary zero, preceded by 1 byte length indicator <br/>
     * b3 Processing code (3 bytes, 6d): 000000 <br/>
     * b4 Amount (6 bytes, 12d): left padding with "0" <br/>
     * 
     * b11 System Trace(3 bytes, 6d): getNewReferenceNumber() - 6 digital of right, can not be 000000 <br/>
     * b12 Time, Local Transmission (3 bytes, 6 digits): hhmmss <br/>
     * b14 Card Expiration day (2 bytes, 4 digits): YYMM <br/>
     * b18 Merchant Category Code (2 bytes, 4d): 5999 <br/>
     * b22 POS entry Mode (2 byte, 3d): 0010(manual), 0900 (magnetic stripe) <br/>
     * 
     * b24 Network International ID (2 * bytes, 3d): 0001 <br/>
     * b25 POS Condition Code: (1 byte, 2d) 00 - present 01 - not present <br/>
     * b31 Acquirer Reference Data 1 - Authorization/Capture <br/>
     * b35 Track 2 Data ( up to 37 byte) <br/>
     * b37 Retrieval Reference Number (12 bytes, 12chars): getNewReferenceNumber - 12 digital <br/>
     * b41 Terminal ID (8 bytes, 8 chars): left padding with "0" <br/>
     * b42 Merchant ID (15 bytes, 15 chars): left padding with "0" <br/>
     * b59 Merchant Zip/Postal Code (9 bytes): right padding with "0" -- V5C 6B4 <br/>
     * b60 Additional POS Information (1 byte, 2 digits): 
     * </p>
     */
    private Object[] processSale(Track2Card track2Card, ProcessorTransaction chargePtd) throws CardTransactionException,
            CardTransactionException
    {
        String responseCode = "";
        String b25PosConCode = "00";
        HashMap<Integer, String> rtnData = null;
        // request data
        TreeMap<Integer, String> bitsData = new TreeMap();
        bitsData.put(0, SALE_REQUEST); // Message Type
        // primary bitmap
        if (!track2Card.isCardDataCreditCardTrack2Data())
        {
            bitsData.put(2, track2Card.getPrimaryAccountNumber()); // b2, Primary account number - manually entry
        }
        bitsData.put(3, SALE_PROCESSING_CODE); // b3, processing code
        bitsData.put(4, formatDollarAmount(chargePtd.getAmount(), 12)); // b4, amount of transaction
        // String reference_number = getNewReferenceNumber();
        String ref = getSystemTrace();
        bitsData.put(11, ref); // b11, system trace 000000 -> not allowed
        bitsData.put(12, getTransactionTimeLocal(chargePtd.getPurchasedDate())); // b12, Time, Local Transmission
        bitsData.put(14, track2Card.getExpirationYear() + track2Card.getExpirationMonth()); // b14, card
                                                                                                                                                                                // expiration date
        bitsData.put(18, merchantCategoryCode); // b18, Merchant Category Code
        if (track2Card.isCardDataCreditCardTrack2Data())
        {
            if (chargePtd.isIsRfid())
            {
                bitsData.put(22, BIT_22_RFID);
                bitsData.put(60, BIT_60_RFID);
            }
            else
            {
                bitsData.put(22, BIT_22_SWIPE); // b22, magnetic
                bitsData.put(60, BIT_60_SWIPE);
            }
        }
        else
        {
            bitsData.put(22, BIT_22_MANUAL); // b22, manually entry
            if (chargePtd.isIsRfid())
            {
                bitsData.put(60, BIT_60_RFID);
            }
            else
            {
                bitsData.put(60, BIT_60_SWIPE);
            }
        }
        bitsData.put(24, NETWORK_INTERNATIONAL_ID); // b24, Network International ID
        if (track2Card.isCardDataCreditCardTrack2Data())
        {
            bitsData.put(25, "00"); // b25, POS condition code 00 - normal
        }
        else
        {
            bitsData.put(25, "01"); // b25, POS condition code 01 - not present
            b25PosConCode = "01";
        }
        bitsData.put(31, SALE_REFERENCE_DATA); // b31,
        if (track2Card.isCardDataCreditCardTrack2Data())
        {
            String t2d = track2Card.getDecryptedCardData().replaceAll("=", "D");
            bitsData.put(35, t2d); // b35,
        }
        String retrievalReferenceNumber = getRetrievalReferenceNumber(ref);
        bitsData.put(37, retrievalReferenceNumber); // b37,
        bitsData.put(41, this.getTerminalId()); // b41,
        bitsData.put(42, this.getMerchantId()); // b42,
        bitsData.put(59, zipCode); // b59,
        logger.debug("+++++ CC card type: " + track2Card.getDisplayName() + " ++++");
        String tables = "";
        if (track2Card.getDisplayName().equals(CardProcessingConstants.NAME_VISA))
        {
            tables = buildBit63ForVisa(SALE_REFERENCE_DATA, "", chargePtd.getAmount(), true);
        }
        else if (track2Card.getDisplayName().equals(CardProcessingConstants.NAME_MASTERCARD))
        {
            tables = buildBit63ForMasterCard(SALE_REFERENCE_DATA, "", chargePtd.getAmount(), true);
        }
        else if (track2Card.getDisplayName().equals(CardProcessingConstants.NAME_AMEX))
        {
            tables = buildBit63ForAE(SALE_REFERENCE_DATA, "", chargePtd.getAmount(), true);
        }
        else if (track2Card.getDisplayName().equals(CardProcessingConstants.NAME_DISCOVER)
                || track2Card.getDisplayName().equals(CardProcessingConstants.NAME_DINERS))
        {
            tables = buildBit63ForDiscoverDiners(SALE_REFERENCE_DATA, "", chargePtd.getAmount(), true);
        }
        else
        {
            tables = buildBit63ForOthers(SALE_REFERENCE_DATA);
        }
        if (tables != null && !tables.isEmpty())
        {
            bitsData.put(63, tables);
        }
        if (logger.isDebugEnabled())
        {
            logger.debug("+++++ sending fields +++++");
            Set set = bitsData.entrySet();
            Iterator i = set.iterator();
            StringBuffer sb = new StringBuffer();
            while (i.hasNext())
            {
                Map.Entry me = (Map.Entry)i.next();
                if (((Integer)me.getKey()).intValue() == 35 || ((Integer)me.getKey()).intValue() == 2)
                {
                    sb.append(me.getKey()).append("=").append("XXXXXXXXXXXXXX").append(", ");
                }
                else
                {
                    sb.append(me.getKey()).append("=").append(me.getValue()).append(", ");
                }
            }
            logger.debug(sb.toString());
        }
        // Send request
        try
        {
            char[] payLoad = Iso8583ProtocolFactory.getInstance().getFirstDataRequestBuilder().build(bitsData);
            String clientRef = getClientRef(ref);
            char[] response = sendRequest(payLoad, clientRef);
            logger.debug("++++ response hex string: " + CardProcessingUtil.toHexString(new String(response)));
            rtnData = Iso8583ProtocolFactory.getInstance().getFirstDataResponseParser().parse(response);
            logger.info("=====  Sale response =====");
            logger.info(rtnData);
            logger.info("==================================");
            // Set processor transaction data values
            chargePtd.setProcessingDate(new Date());
            chargePtd.setReferenceNumber(ref);
            //
            // bit 37, transaction id
            //
            String transactionId = rtnData.get(37);
            chargePtd.setProcessorTransactionId(transactionId);
            // 
            // Bit 39, response code
            //
            responseCode = rtnData.get(39);
            logger.debug("response code: " + responseCode);
            if (responseCode.equals(SALE_APPROVED_CODE))
            {
                //
                // bit 38, authorization number
                //
                chargePtd.setAuthorizationNumber(rtnData.get(38));
                chargePtd.setIsApproved(true);
                return new Object[] {DPTResponseCodesMapping.CC_APPROVED_VAL__7000, responseCode};
            }
            else
            {
                chargePtd.setAuthorizationNumber(null);
                chargePtd.setIsApproved(false);
            }
        }
        catch (CardTransactionException e)
        {
            logger.error("CardTransactionException: ", e);
            createAndSendTimeoutReversal(SALE_REFERENCE_DATA, b25PosConCode, CardProcessingUtil.getCentsAmountInDollars(chargePtd.getAmount()), ref,
                    track2Card, merchantAccountId, chargePtd.getPurchasedDate(), chargePtd.getTicketNumber(), chargePtd.getPointOfSale().getId());
            throw e;
        }
        return new Object[] {mapErrorResponse(responseCode, rtnData.get(63)), responseCode};
    }

    /**
     * message format: Message Type: 0100 <br/>
     * 
     * Primary Bitmap: <br/>
     * <p>
     * decimal: <br/>
     * setting: 0111 0000||0 0 1 1 0 1 0 0|| 0 1 0 0 0 1 0 1 || 1 0 0 0 0 0 1 0 || <br/>
     * Bit#: 1234 5678 9,10,11,12 13,14,15,16 17,18,19,20,21,22,23,24, 25,26,27,28 29,30,31,32, <br/>
     * 
     * decimal: <br/>
     * setting: 0 0 0 0 1 0 0 0 || 1 1 0 0 0 0 0 0 || 0 0 0 0 0 0 0 0 || <br/>
     * Bit#: 33,34,35,36 37,38,39,40, 41,42,43,44 45,46,47,48, 49,50,51,52 53,54,55,56 <br/>
     * 
     * decimal: <br/>
     * setting: 0 0 1 0 0 0 0 0|| <br/>
     * Bit#: 57,58,59,60 61,62,63,64 <br/>
     * 
     * <br/>
     * b2 PAN (.. 10 bytes, 19 digits): last 1/2 byte can pad binary zero, preceded by 1 byte length indicator <br/>
     * b3 Processing code (3 bytes, 6d): 200000 <br/>
     * b4 Amount (6 bytes, 12d): left padding with "0" <br/>
     * 
     * b11 System Trace(3 bytes, 6d): getNewReferenceNumber() - 6 digital of right, can not be 000000 <br/>
     * b12 Time, Local Transmission (3 bytes, 6 digits): hhmmss <br/>
     * b14 Card Expiration day (2 bytes, 4 digits): YYMM <br/>
     * b18 Merchant Category Code (2 bytes, 4d): 5999 <br/>
     * b22 POS entry Mode (2 byte, 3d): 0010(manual), 0900 (magnetic stripe) <br/>
     * 
     * b24 Network International ID (2 * bytes, 3d): 0001 <br/>
     * b25 POS Condition Code: (1 byte, 2d) 00 - present 01 - not present <br/>
     * b31 Acquirer Reference Data 2 - Capture only <br/>
     * b37 Retrieval Reference Number (12 bytes, 12chars): getNewReferenceNumber - 12 digital <br/>
     * b41 Terminal ID (8 bytes, 8 chars): left padding with "0" <br/>
     * b42 Merchant ID (15 bytes, 15 chars): left padding with "0" <br/>
     * b59 Merchant Zip/Postal Code (9 bytes): right padding with "0" -- V5C 6B4 <br/>
     * 
     * </p>
     */
    public Object[] processRefund(boolean isNonEmsRequest, ProcessorTransaction origPtd,
            ProcessorTransaction refundPtd, String ccNumber, String expiryMMYY) throws CardTransactionException,
            CardTransactionException
    {
        String responseCode = "";
        String b25PosConCode = "01"; // 01 - not present
        HashMap<Integer, String> rtnData = null;
        
        StringBuilder bdr = new StringBuilder();
        // pan + TRACK_2_DELIMITER + expiryYY + expiryMM
        bdr.append(ccNumber).append(CardProcessingConstants.TRACK_2_DELIMITER).append(expiryMMYY.substring(2)).append(expiryMMYY.substring(0, 2));
        String cardData = bdr.toString();

        CustomerCardType customerCardType = getCustomerCardTypeService().getCardTypeByTrack2DataOrAccountNumber(getMerchantAccount().getCustomer().getId(), cardData);
        Track2Card track2Card = new Track2Card(customerCardType, getMerchantAccount().getCustomer().getId(), cardData);
        
        String ref = getSystemTrace();

        // request data
        TreeMap<Integer, String> bitsData = new TreeMap();
        bitsData.put(0, REFUND_REQUEST); // Message Type
        // primary bitmap
        int card_data_length = track2Card.getPrimaryAccountNumber().length();
        if (card_data_length < 10 || card_data_length > 40)
        {
            throw new IllegalArgumentException("Invalid card number length: " + card_data_length);
        }
        bitsData.put(2, track2Card.getPrimaryAccountNumber()); // b2, Primary account number - manually entry
        bitsData.put(3, REFUND_PROCESSING_CODE); // b3, processing code

        bitsData.put(4, formatDollarAmount(refundPtd.getAmount(), 12)); // b4, amount of transaction
        bitsData.put(11, ref); // b11, system trace 000000 -> not allowed
        bitsData.put(12, getTransactionTimeLocal(origPtd.getPurchasedDate())); // b12, Time, Local Transmission
        bitsData.put(14, track2Card.getExpirationYear() + track2Card.getExpirationMonth()); // b14, card
                                                                                                                                                                                // expiration date
        bitsData.put(18, merchantCategoryCode); // b18, Merchant Category Code
        bitsData.put(22, BIT_22_MANUAL); // b22, manually entry
        bitsData.put(24, NETWORK_INTERNATIONAL_ID); // b24, Network International ID
        bitsData.put(25, "01"); // b25, POS condition code 01 - not present
        bitsData.put(31, REFUND_REFERENCE_DATA); // b31,
        String retrievalReferenceNumber = this.getRetrievalReferenceNumber(ref);
        bitsData.put(37, retrievalReferenceNumber); // b37,
        bitsData.put(41, this.getTerminalId()); // b41,
        bitsData.put(42, this.getMerchantId()); // b42,
        bitsData.put(59, zipCode); // b59,
        if (refundPtd.isIsRfid())
        {
            bitsData.put(60, BIT_60_RFID);
        }
        else
        {
            bitsData.put(60, BIT_60_SWIPE);
        }
        if (logger.isDebugEnabled())
        {
            logger.debug("+++++ sending fields +++++");
            Set set = bitsData.entrySet();
            Iterator i = set.iterator();
            StringBuffer sb = new StringBuffer();
            while (i.hasNext())
            {
                Map.Entry me = (Map.Entry)i.next();
                if (((Integer)me.getKey()).intValue() == 35 || ((Integer)me.getKey()).intValue() == 2)
                {
                    sb.append(me.getKey()).append("=").append("XXXXXXXXXXXXXX").append(", ");
                }
                else
                {
                    sb.append(me.getKey()).append("=").append(me.getValue()).append(", ");
                }
            }
            logger.debug(sb.toString());
        }
        // Send request
        try
        {
            char[] payLoad = Iso8583ProtocolFactory.getInstance().getFirstDataRequestBuilder().build(bitsData);
            String clientRef = getClientRef(ref);
            char[] response = sendRequest(payLoad, clientRef);
            logger.debug("++++ response hex string: " + CardProcessingUtil.toHexString(new String(response)));
            rtnData = Iso8583ProtocolFactory.getInstance().getFirstDataResponseParser().parse(response);
            logger.info("======== Refund response =======");
            logger.info(rtnData);
            logger.info("==================================");

            // Process response
            String pan = track2Card.getPrimaryAccountNumber();

            // Set processor transaction data values
            refundPtd.setProcessingDate(new Date());
            refundPtd.setReferenceNumber(ref);
            //
            // bit 37, transaction id
            //
            String transactionId = rtnData.get(37);
            refundPtd.setProcessorTransactionId(transactionId);

            // 
            // Bit 39, response code
            //
            responseCode = rtnData.get(39);
            logger.debug("response code: " + responseCode);
            if (responseCode.equals(REFUND_APPROVED_CODE))
            {
                //
                // bit 38, authorization number
                //
                refundPtd.setAuthorizationNumber(rtnData.get(38));
                refundPtd.setIsApproved(true);
                return new Object[] {DPTResponseCodesMapping.CC_APPROVED_VAL__7000, responseCode};
            }
            // Declined
            refundPtd.setAuthorizationNumber(null);
            refundPtd.setIsApproved(false);
        }
        catch (CardTransactionException e)
        {
            createAndSendTimeoutReversal(REFUND_REFERENCE_DATA, b25PosConCode, CardProcessingUtil.getCentsAmountInDollars(refundPtd.getAmount()), ref,
                    track2Card, merchantAccountId, refundPtd.getPurchasedDate(), refundPtd.getTicketNumber(), 
                    refundPtd.getPointOfSale().getId());
            throw e;
        }
        // map error response
        return new Object[] {mapErrorResponse(responseCode, rtnData.get(63)), responseCode};
    }

    /**
     * message format: Message Type: 0400 <br/>
     * 
     * Primary Bitmap: <br/>
     * <p>
     * decimal: <br/>
     * setting: 0111 0010||0 0 1 1 0 1 0 0|| 0 1 0 0 0 1 0 1 || 1 0 0 0 0 0 1 0 || <br/>
     * Bit#: 1234 5678 9,10,11,12 13,14,15,16 17,18,19,20,21,22,23,24, 25,26,27,28 29,30,31,32, <br/>
     * 
     * decimal: <br/>
     * setting: 0 0 0 0 1 0 0 0 || 1 1 0 0 0 0 0 0 || 0 0 0 0 0 0 0 0 || <br/>
     * Bit#: 33,34,35,36 37,38,39,40, 41,42,43,44 45,46,47,48, 49,50,51,52 53,54,55,56 <br/>
     * 
     * decimal: <br/>
     * setting: 0 0 1 0 0 0 0 0|| <br/>
     * Bit#: 57,58,59,60 61,62,63,64 <br/>
     * 
     * <br/>
     * b2 PAN (.. 10 bytes, 19 digits): last 1/2 byte can pad binary zero, preceded by 1 byte length indicator <br/>
     * b3 Processing code (3 bytes, 6d): 000000 <br/>
     * b4 Amount (6 bytes, 12d): left padding with "0" <br/>
     * b7 Transmission Date/Time (5 bytes, 10 d) - MMDDhhmmss GMT b11 System Trace(3 bytes, 6d): -- passed in
     * (match Auth*Settle&Sale&Refund) <br/>
     * b12 Time, Local Transmission (3 bytes, 6 digits): hhmmss <br/>
     * b14 Card Expiration day (2 bytes, 4 digits): YYMM <br/>
     * b18 Merchant Category Code (2 bytes, 4d): 5999 <br/>
     * b22 POS entry Mode (2 byte, 3d): 0900 (magnetic stripe) <br/>
     * 
     * b24 Network International ID (2 * bytes, 3d): 0001 <br/>
     * b25 POS Condition Code: (1 byte, 2d) -- passed in (match Auth*Settle&Sale&Refund) <br/>
     * b31 Acquirer Reference Data -- passed in (match Auth*Settle&Sale&Refund) <br/>
     * b37 Retrieval Reference Number (12 bytes, 12chars): -- passed in (match Auth*Settle&Sale&Refund) <br/>
     * b38 Authorization Code (6 bytes, 6chars): <br/>
     * b41 Terminal ID (8 bytes, 8 chars): left padding with "0" <br/>
     * b42 Merchant ID (15 bytes, 15 chars): left padding with "0" <br/>
     * b59 Merchant Zip/Postal Code (9 bytes): right padding with "0" -- V5C 6B4 <br/>
     * 
     * </p>
     */
    public void processReversal(Reversal reversal, Track2Card track2Card) throws CardTransactionException
    {
        String responseCode = "";
        // request data
        TreeMap<Integer, String> bitsData = new TreeMap();
        bitsData.put(0, REVERSAL_REQUEST); // Message Type
        // primary bitmap
        bitsData.put(2, track2Card.getPrimaryAccountNumber()); // b2,
        bitsData.put(3, REVERSAL_PROCESSING_CODE); // b3, processing code
        bitsData.put(4, formatDollarAmount(reversal.getOriginalTransactionAmountInCents(), 12)); // b4, amount of transaction
        Date transmissionDate = new Date();
        bitsData.put(7, getTransactionDate(transmissionDate)); // b7, Transmission Date/Time
        String orgRef = reversal.getOriginalReferenceNumber();
        bitsData.put(11, orgRef); // b11, system trace 000000 -> not allowed
        bitsData.put(12, getTransactionTimeLocal(reversal.getPurchasedDate())); // b12, Time, Local Transmission
        bitsData.put(14, track2Card.getExpirationYear() + track2Card.getExpirationMonth()); // b14, card
                                                                                                                                                                                // expiration date
        bitsData.put(18, merchantCategoryCode); // b18, Merchant Category Code
        bitsData.put(22, BIT_22_MANUAL); // b22, manual

        bitsData.put(24, NETWORK_INTERNATIONAL_ID); // b24, Network International ID 0001
        bitsData.put(25, reversal.getOriginalProcessingCode()); // b25, POS condition code

        bitsData.put(31, reversal.getOriginalMessageType()); // b31, Acquirer Reference Data
        String retrievalReferenceNumber = getRetrievalReferenceNumber(orgRef);
        bitsData.put(37, retrievalReferenceNumber); // b37, Retrieval Reference Number
        bitsData.put(41, this.getTerminalId()); // b41,
        bitsData.put(42, this.getMerchantId()); // b42,
        bitsData.put(59, zipCode); // b59,
        if (reversal.isIsRfid())
        {
            bitsData.put(60, BIT_60_RFID);
        }
        else
        {
            bitsData.put(60, BIT_60_SWIPE);
        }
        if (logger.isDebugEnabled())
        {
            logger.debug("+++++ sending fields +++++");
            Set set = bitsData.entrySet();
            Iterator i = set.iterator();
            StringBuffer sb = new StringBuffer();
            while (i.hasNext())
            {
                Map.Entry me = (Map.Entry)i.next();
                if (((Integer)me.getKey()).intValue() == 35 || ((Integer)me.getKey()).intValue() == 2)
                {
                    sb.append(me.getKey()).append("=").append("XXXXXXXXXXXXXX").append(", ");
                }
                else
                {
                    sb.append(me.getKey()).append("=").append(me.getValue()).append(", ");
                }
            }
            logger.debug(sb.toString());
        }
        // Send request
        char[] payLoad = Iso8583ProtocolFactory.getInstance().getFirstDataRequestBuilder().build(bitsData);
        String ref = getSystemTrace();
        String clientRef = getClientRef(ref);
        char[] response = sendRequest(payLoad, clientRef);
        logger.debug("++++ response hex string: " + CardProcessingUtil.toHexString(new String(response)));
        HashMap<Integer, String> rtnData = Iso8583ProtocolFactory.getInstance().getFirstDataResponseParser()
                .parse(response);
        logger.info("==== Reversal response ===");
        logger.info(rtnData);
        logger.info("==================================");
        reversal.setLastRetryTime(new Date());
        reversal.incrementRetryAttempts();
        // 
        // Bit 39, response code
        //
        responseCode = rtnData.get(39);
        reversal.setLastResponseCode(responseCode);
        logger.debug("response code: " + responseCode);
        if (responseCode.equals(REVERSAL_APPROVED_CODE))
        {
            reversal.setIsSucceeded(true);
        }
    }

    private void createAndSendTimeoutReversal(String orgMti_AcqRefData, String processingCode_posConCode,
            BigDecimal amountInDollars, String refNum_sysTrace, Track2Card track2Card, int merchantAccountId, Date date,
            int ticketNumber, int pointOfSaleId)
    {
        Reversal reversal = null;
        try
        {
            reversal = getCardProcessingManager().createReversal(orgMti_AcqRefData, processingCode_posConCode,
                    amountInDollars, refNum_sysTrace, Long.toString(System.currentTimeMillis()), track2Card
                            .getCreditCardPanAndExpiry(), "", merchantAccountId, date, ticketNumber, pointOfSaleId);
        }
        catch (Exception e)
        {
            String msg = "Unable to create Reversal record";
            logger.error(msg, e);
            getCommonProcessingService().sendCardProcessingAdminErrorAlert("Unable to call 'createAndSendTimeoutReversal' in FirstDataNashvilleProcessor. ", msg, e);
            return;
        }

        try
        {
            getCardProcessingManager().processReversal(reversal);
        }
        catch (Exception e)
        {
            String msg = "Unable to update reversal after processing: " + reversal;
            logger.error(msg, e);
            getCommonProcessingService().sendCardProcessingAdminErrorAlert("Unable to call 'processReversal' in FirstDataNashvilleProcessor. ", msg, e);
        }
    }

    private String buildVisaTable14(String requestTypeRefData, String originalBit63,
            int transactionAmountCents, boolean isTrack2)
    {
        if (!CAPTURE_REFERENCE_DATA.equals(requestTypeRefData))
        {
            // if not capture
            StringBuffer sb = new StringBuffer();
            sb.append(new String(new FixLengthBcdType(0, 2).encodeValue("48"))); // table length
            sb.append("14"); // table id
            sb.append("Y"); // ACI
            sb.append("               "); // Tran ID
            sb.append("    "); // Validation Code
            sb.append(" "); // Market Specific Data Indicator - None
            sb.append(" "); // RPS - not used
            sb.append("000000000000"); // First Authorized Amount
            sb.append("000000000000"); // Total Authorized Amount
            return sb.toString();
        }
        else
        {
            // modify data from auth response
            HashMap<String, String> tableMap = parseTablesFromBit63(originalBit63);
            String t14Data = tableMap.get("14");
            if (t14Data != null)
            {
                String amountTemplate = "000000000000";
                String amountStr = Integer.toString(transactionAmountCents);
                String amountData = amountTemplate.substring(0, amountTemplate.length() - amountStr.length())
                        + amountStr;
                amountData = amountData + amountData;
                t14Data = t14Data.substring(0, 22) + amountData;
                int dataLength = 2 + t14Data.length();
                return new String(new FixLengthBcdType(0, 2).encodeValue(Integer.toString(dataLength))) + "14"
                        + t14Data;
            }
        }
        return "";
    }

    private String buildMasterTable14(String requestTypeRefData, String originalBit63,
            int transactionAmountCents, boolean isTrack2)
    {
        if (!CAPTURE_REFERENCE_DATA.equals(requestTypeRefData))
        {
            // if not capture
            StringBuffer sb = new StringBuffer();
            sb.append(new String(new FixLengthBcdType(0, 2).encodeValue("48"))); // table length
            sb.append("14"); // table id
            sb.append("Y"); // ACI
            sb.append("    "); // BankNET Date
            sb.append("         "); // BankNET Reference
            sb.append("  "); // Filler
            sb.append(" "); // CVC Error Code
            sb.append(" "); // POS Entry Mode Change
            sb.append(" "); // Transaction Edit Error Code
            sb.append(" "); // Filler
            sb.append(" "); // Market Specific Data Indicator
            sb.append("             "); // Filler
            sb.append("000000000000"); // Total Authorization Amount

            return sb.toString();
        }
        else
        {
            // modify data from auth response
            HashMap<String, String> tableMap = parseTablesFromBit63(originalBit63);
            String t14Data = tableMap.get("14");
            if (t14Data != null)
            {
                String amountTemplate = "000000000000";
                String amountStr = Integer.toString(transactionAmountCents);
                String amountData = amountTemplate.substring(0, amountTemplate.length() - amountStr.length())
                        + amountStr;
                t14Data = t14Data.substring(0, 21) + "             " + amountData;
                int dataLength = 2 + t14Data.length();
                return new String(new FixLengthBcdType(0, 2).encodeValue(Integer.toString(dataLength))) + "14"
                        + t14Data;
            }
        }
        return "";
    }

    // not used, leave it here in case we need it later
    private String buildDiscoverTable14(String requestTypeRefData, String originalBit63,
            int transactionAmountCents, boolean isTrack2)
    {
        if (!CAPTURE_REFERENCE_DATA.equals(requestTypeRefData))
        {
            // if not capture
            StringBuffer sb = new StringBuffer();
            sb.append(new String(new FixLengthBcdType(0, 2).encodeValue("48"))); // table length
            sb.append("14"); // table id
            sb.append("X"); // Discover Indicator
            sb.append("               "); // NRID/Transaction ID
            sb.append("      "); // Filler
            sb.append("000000000000"); // Filler
            sb.append("000000000000"); // Total Authorization Amount

            return sb.toString();
        }
        else
        {
            // modify data from auth response
            HashMap<String, String> tableMap = parseTablesFromBit63(originalBit63);
            String t14Data = tableMap.get("14");
            if (t14Data != null)
            {
                String amountTemplate = "000000000000";
                String amountStr = Integer.toString(transactionAmountCents);
                String amountData = amountTemplate.substring(0, amountTemplate.length() - amountStr.length())
                        + amountStr;
                t14Data = t14Data.substring(0, 34) + amountData;
                int dataLength = 2 + t14Data.length();
                return new String(new FixLengthBcdType(0, 2).encodeValue(Integer.toString(dataLength))) + "14"
                        + t14Data;
            }
        }
        return "";
    }

    private String buildAETable14(String requestTypeRefData, String originalBit63, int transactionAmountCents,
            boolean isTrack2)
    {
        if (!CAPTURE_REFERENCE_DATA.equals(requestTypeRefData))
        {
            // if not capture
            StringBuffer sb = new StringBuffer();
            sb.append(new String(new FixLengthBcdType(0, 2).encodeValue("48"))); // table length without seller ID
            sb.append("14"); // table id
            sb.append("X"); // American Express Indicator
            sb.append("               "); // Tran ID
            sb.append("      "); // Filler
            sb.append("            "); // POS Date
            sb.append("000000000000"); // Filler

            return sb.toString();
        }
        else
        {
            // extract and send table 14 from auth response
            HashMap<String, String> tableMap = parseTablesFromBit63(originalBit63);
            String t14Data = tableMap.get("14");
            if (t14Data != null)
            {
                int dataLength = 2 + t14Data.length();
                return new String(new FixLengthBcdType(0, 2).encodeValue(Integer.toString(dataLength))) + "14"
                        + t14Data;
            }
        }
        return "";
    }

    private String buildTable14(String cardType, String requestTypeRefData, String originalBit63,
            int transactionAmountCents, boolean isTrack2)
    {
        String table14 = "";
        if (CardProcessingConstants.NAME_VISA.equals(cardType))
        {
            table14 = buildVisaTable14(requestTypeRefData, originalBit63, transactionAmountCents, isTrack2);
        }
        else if (CardProcessingConstants.NAME_MASTERCARD.equals(cardType))
        {
            table14 = buildMasterTable14(requestTypeRefData, originalBit63, transactionAmountCents, isTrack2);
        }
        else if (CardProcessingConstants.NAME_DISCOVER.equals(cardType) || CardProcessingConstants.NAME_DINERS.equals(cardType)
                || CardProcessingConstants.NAME_JCB.equals(cardType))
        {
            table14 = buildDiscoverTable14(requestTypeRefData, originalBit63, transactionAmountCents, isTrack2);
        }
        else if (CardProcessingConstants.NAME_AMEX.equals(cardType))
        {
            table14 = buildAETable14(requestTypeRefData, originalBit63, transactionAmountCents, isTrack2);
        }
        if (logger.isDebugEnabled())
        {
            logger.debug("+++ table 14 content: " + table14.toString());
            logger.debug("+++ table 14 hex: " + CardProcessingUtil.toHexString(table14.toString()));
        }
        return table14;
    }

    /**
     * Table 68 additional account information
     * @param cardType
     * @param requestTypeRefData
     * @return
     */
    public String buildTable68(String cardType, String requestTypeRefData)
    {
        StringBuffer content = new StringBuffer();
        if (AUTHORIZATION_REFERENCE_DATA.equals(requestTypeRefData)
                || SALE_REFERENCE_DATA.equals(requestTypeRefData))
        {
            String tableID = "68"; // AN, 2 bytes
            String version = "2"; // AN, 1 bytes
            String balanceCapability = "0"; // AN, 1 byte, 0 - not supported
            String partialApprovalCapability = "0"; //AN, 1 byte, 0 - not supported
                                                                                                                                                                                                                // 6
            content.append(tableID).append(version).append(balanceCapability).append(partialApprovalCapability);
            int contentLength = content.toString().length();
            char[] tableLength = new FixLengthBcdType(0, 2).encodeValue(String.valueOf(contentLength));
            content.insert(0, tableLength);
            if (logger.isDebugEnabled())
            {
                logger.debug("+++ table 68 content: " + content.toString());
                logger.debug("+++ table 68 contentLength: " + contentLength);
                logger.debug("+++ table 68 hex: " + CardProcessingUtil.toHexString(content.toString()));
            }
        }
        return content.toString();
    }

    /**
     * Applicable to Authorization only, and Sale
     * 
     * @param cardType
     * @return
     */
    public String buildTable69(String cardType, String requestTypeRefData)
    {
        StringBuffer content = new StringBuffer();
        if (AUTHORIZATION_REFERENCE_DATA.equals(requestTypeRefData)
                || SALE_REFERENCE_DATA.equals(requestTypeRefData))
        {
            String tableID = "69"; // AN, 2 bytes, 2 spaces
            String version = "01"; // AN, 2 bytes, 2 spaces
            String numOfEntries = "1"; // AN, 1 byte, 1 space
            char visaID1 = (char)0x0B; // Hex, 1 byte -> "0B"
            String visaBID1 = "     "; // AN, 5 space
            char[] visaAUAR1 = new char[] {(char)0x0, (char)0x0, (char)0x0, (char)0x0, (char)0x0, (char)0x0}; // Hex,
                                                                                                                                                                                                                // 6
                                                                                                                                                                                                                // bytes,
                                                                                                                                                                                                                // hex
                                                                                                                                                                                                                // 0
            String TPPID1 = "TDP111"; // AN, 6 bytes
            content.append(tableID).append(version).append(numOfEntries).append(visaID1).append(visaBID1).append(
                    visaAUAR1).append(TPPID1);
            int contentLength = content.toString().length();
            char[] tableLength = new FixLengthBcdType(0, 2).encodeValue(String.valueOf(contentLength));
            content.insert(0, tableLength);
            if (logger.isDebugEnabled())
            {
                logger.debug("+++ table 69 content: " + content.toString());
                logger.debug("+++ table 69 contentLength: " + contentLength);
                logger.debug("+++ table 69 hex: " + CardProcessingUtil.toHexString(content.toString()));
            }
        }
        return content.toString();
    }

    private String buildTableMC(String requestTypeRefData)
    {
        StringBuffer content = new StringBuffer();
        if (requestTypeRefData.equals(AUTHORIZATION_REFERENCE_DATA)
                || requestTypeRefData.equals(SALE_REFERENCE_DATA)
                || requestTypeRefData.equals(CAPTURE_REFERENCE_DATA))
        {
            String tableID = "MC"; // AN, 2 bytes
            content.append(tableID);
            int contentLength = content.toString().length();

            char[] tableLength = new FixLengthBcdType(0, 2).encodeValue(String.valueOf(contentLength));
            content.insert(0, tableLength);
            if (logger.isDebugEnabled())
            {
                logger.debug("+++ table MC content: " + content.toString());
                logger.debug("+++ table MC contentLength: " + contentLength);
                logger.debug("+++ table MC(hex) : " + CardProcessingUtil.toHexString(content.toString()));
            }
        }
        return content.toString();
    }

    /**
     * (Table VI) Visa Compliance Field Identifier Table Applicable to Authorization only, Sale, and Capture
     * only
     * 
     * 
     * @param requestTypeRefData
     * @param originalBit63
     * @return
     */
    private String buildTableVI(String requestTypeRefData, String originalBit63)
    {
        StringBuffer content = new StringBuffer();
        if (requestTypeRefData.equals(this.AUTHORIZATION_REFERENCE_DATA)
                || requestTypeRefData.equals(this.SALE_REFERENCE_DATA)
                || requestTypeRefData.equals(this.CAPTURE_REFERENCE_DATA))
        {
            String tableID = "VI"; // AN, 2 bytes
            content.append(tableID);
            if (requestTypeRefData.equals(this.CAPTURE_REFERENCE_DATA))
            {
                HashMap<String, String> tableMap = parseTablesFromBit63(originalBit63);
                String tableVI = tableMap.get("VI");
                logger.debug("+++ table VI retrieved from origial bit 63: " + tableVI);
                if (tableVI != null)
                {
                    /*
                     * 
                     * 
                     * Need to double check here with Spec!!!!!!
                     */
                    String cr = tableVI.substring(tableVI.indexOf("CR"), tableVI.indexOf("CR") + 4);
                    logger.debug("+++ returned cr (hex): " + CardProcessingUtil.toHexString(cr));
                    content.append(cr);
                }
            }
            int contentLength = content.toString().length();

            char[] tableLength = new FixLengthBcdType(0, 2).encodeValue(String.valueOf(contentLength));
            content.insert(0, tableLength);
            if (logger.isDebugEnabled())
            {
                logger.debug("+++ table VI content: " + content.toString());
                logger.debug("+++ table VI contentLength: " + contentLength);
                logger.debug("+++ table VI(hex) : " + CardProcessingUtil.toHexString(content.toString()));
            }
        }
        return content.toString();
    }

    /**
     * (Table DS) Discover (JCB (US Domestic only), and Diners) Qualification Field Identifier Table
     * 
     * Applicable to Authorization only, Sale, and Capture only
     * 
     * @param requestTypeRefData
     * @param originalBit63
     * @return
     */
    private String buildTableDs(String requestTypeRefData, String originalBit63)
    {
        String tableDs = "";
        if (!CAPTURE_REFERENCE_DATA.equals(requestTypeRefData))
        {
            // if not capture
            StringBuffer sb = new StringBuffer();
            sb.append(new String(new FixLengthBcdType(0, 2).encodeValue("02"))); // table length
            sb.append("DS"); // table id

            tableDs = sb.toString();
        }
        else
        {
            // extract and send table DS from auth response
            HashMap<String, String> tableMap = parseTablesFromBit63(originalBit63);
            String t14Data = tableMap.get("DS");
            if (t14Data != null)
            {
                int dataLength = 2 + t14Data.length();
                tableDs = new String(new FixLengthBcdType(0, 2).encodeValue(Integer.toString(dataLength))) + "DS"
                        + t14Data;
            }
        }
        if (logger.isDebugEnabled())
        {
            logger.debug("+++ table DS content: " + tableDs.toString());
            logger.debug("+++ table DS hex: " + CardProcessingUtil.toHexString(tableDs.toString()));
        }
        return tableDs;
    }

    /**
     * Build Bit63 data for Visa requests
     * 
     * @param requestTypeRefData
     *          AUTHORIZATION_REFERENCE_DATA, CAPTURE_REFERENCE_DATA, SALE_REFERENCE_DATA
     * @param originalBit63
     *          The original Bit63 data we got from Authorization response 0110
     * @param transactionAmountCents
     *          The transaction amount in cents
     * @return
     */
    private String buildBit63ForVisa(String requestTypeRefData, String originalBit63,
            int transactionAmountCents, boolean isTrack2)
    {
        // table 14
        String t14 = buildTable14(CardProcessingConstants.NAME_VISA, requestTypeRefData, originalBit63,
                transactionAmountCents, isTrack2);

        // table 68
        String t68 = buildTable68(CardProcessingConstants.NAME_VISA, requestTypeRefData);

        // table 69
        String t69 = buildTable69(CardProcessingConstants.NAME_VISA, requestTypeRefData);

        // table VI
        String tvi = buildTableVI(requestTypeRefData, originalBit63);

        return t14 + t68 + t69 + tvi;
    }

    /**
     * Build Bit63 data for Master Card requests
     * 
     * @param requestTypeRefData
     *          AUTHORIZATION_REFERENCE_DATA, CAPTURE_REFERENCE_DATA, SALE_REFERENCE_DATA
     * @param originalBit63
     *          The original Bit63 data we got from Authorization response 0110
     * @param transactionAmountCents
     *          The transaction amount in cents
     * @return
     */
    private String buildBit63ForMasterCard(String requestTypeRefData, String originalBit63,
            int transactionAmountCents, boolean isTrack2)
    {
        // table 14
        String t14 = buildTable14(CardProcessingConstants.NAME_MASTERCARD, requestTypeRefData, originalBit63,
                transactionAmountCents, isTrack2);

        // table 68
        String t68 = buildTable68(CardProcessingConstants.NAME_MASTERCARD, requestTypeRefData);

        // table 69
        String t69 = buildTable69(CardProcessingConstants.NAME_MASTERCARD, requestTypeRefData);

        // table MC
        String tmc = buildTableMC(requestTypeRefData);

        return t14 + t68 + t69 + tmc;
    }

    /**
     * Build Bit63 data for Discover/Diners/JCB requests
     * 
     * @param requestTypeRefData
     *          AUTHORIZATION_REFERENCE_DATA, CAPTURE_REFERENCE_DATA, SALE_REFERENCE_DATA
     * @param originalBit63
     *          The original Bit63 data we got from Authorization response 0110
     * @param transactionAmountCents
     *          The transaction amount in cents
     * @return
     */
    private String buildBit63ForDiscoverDiners(String requestTypeRefData, String originalBit63,
            int transactionAmountCents, boolean isTrack2)
    {
        // table 14
        String t14 = buildTable14(CardProcessingConstants.NAME_DISCOVER, requestTypeRefData, originalBit63,
                transactionAmountCents, isTrack2);

        // table 68
        String t68 = buildTable68(CardProcessingConstants.NAME_DISCOVER, requestTypeRefData);

        // table 69
        String t69 = buildTable69(CardProcessingConstants.NAME_DISCOVER, requestTypeRefData);

        // table DS
        String tds = buildTableDs(requestTypeRefData, originalBit63);

        return t14 + t68 + t69 + tds;
    }

    /**
     * Build Bit63 data for AE requests
     * 
     * @param requestTypeRefData
     *          AUTHORIZATION_REFERENCE_DATA, CAPTURE_REFERENCE_DATA, SALE_REFERENCE_DATA
     * @param originalBit63
     *          The original Bit63 data we got from Authorization response 0110
     * @param transactionAmountCents
     *          The transaction amount in cents
     * @return
     */
    private String buildBit63ForAE(String requestTypeRefData, String originalBit63, int transactionAmountCents,
            boolean isTrack2)
    {
        // table 14
        String t14 = buildTable14(CardProcessingConstants.NAME_AMEX, requestTypeRefData, originalBit63,
                transactionAmountCents, isTrack2);
        
        // table 68
        String t68 = buildTable68(CardProcessingConstants.NAME_AMEX, requestTypeRefData);

        // table 69
        String t69 = buildTable69(CardProcessingConstants.NAME_AMEX, requestTypeRefData);

        return t14 + t68 + t69;
    }

    private String buildBit63ForOthers(String requestTypeRefData)
    {
        // table 68
        String t68 = buildTable68(CardProcessingConstants.NAME_UNKNOWN, requestTypeRefData);

        // table 69
        String t69 = buildTable69(CardProcessingConstants.NAME_UNKNOWN, requestTypeRefData);

        return t68 + t69;
    }

    private static HashMap<String, String> parseTablesFromBit63(String bit63Data)
    {
        HashMap<String, String> tableMap = new HashMap<String, String>();
        char[] bit63Chars = bit63Data.toCharArray();
        int index = 0;
        int lenIndicatorlength = 2;
        try
        {
            while (index < bit63Chars.length)
            {
                StringBuffer lenStr = new StringBuffer();
                for (int i = 0; i < lenIndicatorlength; i++)
                {
                    // append high nibble
                    lenStr.append(Integer.toString(bit63Chars[index + i] >> 4));

                    // append low nibble
                    lenStr.append(Integer.toString(bit63Chars[index + i] & 0x0F));
                }
                int dataLength = Integer.parseInt(lenStr.toString());
                tableMap.put(bit63Data.substring(index + 2, index + 4), bit63Data.substring(index + 4, index + 2
                        + dataLength));
                index = index + lenIndicatorlength + dataLength;
            }
        }
        catch (NumberFormatException e)
        {
            logger.debug("Unable to parse Bit 63 data " + bit63Data);
        }
        return tableMap;
    }

    protected void selfRegisterMerchant(boolean useTestServer, MerchantAccount md) throws CardTransactionException
    {
        // Check to see if already registered
        if (this.getDatawireId() != null && this.getDatawireId().length() > 0)
        {
            return;
        }

        // Self register with Datawire and activate the merchant
        try
        {
            logger.info("Self-registering (MID: " + this.getMerchantId() + ", TID: " + this.getTerminalId()
                    + ", SID: " + DATAWIRE_SERVICE_ID + ", ApplicationID: " + DATAWIRE_APPLICATION_ID + ") @ "
                    + selfRegistrationUrl_);

            System.setProperty("java.protocol.handler.pkgs", "javax.net.ssl");

            net.datawire.vxn3.SelfRegistration srs = new net.datawire.vxn3.SelfRegistration(selfRegistrationUrl_,
                    this.getMerchantId(), this.getTerminalId(), DATAWIRE_SERVICE_ID, DATAWIRE_APPLICATION_ID);

            srs.setMaxRegisterAttempts(10);
            srs.setRegisterAttemptsWaitMilliseconds(15000);

            setDatawireId(srs.registerMerchant());
            srs.activateMerchant();
            setUrlList(srs.getUrlList());

            if (logger.isDebugEnabled())
            {
                logger.debug("URL List received from self-registration: " + getUrlList());
                logger.debug("Datawire ID recieved from self-registration: " + getDatawireId());
            }
        }
        catch (net.datawire.vxn3.VXNException e)
        {
            logger.info("self-register merchant failure:", e);
            throw new CardTransactionException("Unable to self-register merchant due to: " + e.getMessage(),
                    e);
        }

        // Add DID entry to the database
        // Add service discovery URLs to the database
        md.setField3(getDatawireId());
        if (!getUrlList().isEmpty())
        {
            String sd_url_list_string = "";
            for (int i = 0; i < getUrlList().size(); i++)
            {
                sd_url_list_string += getUrlList().get(i).toString() + "|";
            }
            md.setField4(sd_url_list_string.substring(0, sd_url_list_string.length() - 1));
        }
        getMerchantAccountService().updateMerchantAccount(md);
    }

    private int mapErrorResponse(String responseCode, String displayText) throws CardTransactionException
    {
        logger.info("+++ First Data response code: " + responseCode + ", corresponding text: " + displayText);
        // place call
        if ("60".equals(responseCode.trim()) || "01".equals(responseCode.trim()))
        {
            return (DPTResponseCodesMapping.CC_PLACE_CALL_VAL__7022);
        }

        int response_code = -1;
        try
        {
            response_code = Integer.parseInt(responseCode);
        }
        catch (Exception e)
        {
            if (responseCode.equals("C2") || responseCode.equals("NU"))
            {
                return (DPTResponseCodesMapping.CC_DECLINED_VAL__7003);
            }
            else if (responseCode.equals("N3") && displayText.trim().toLowerCase().indexOf("declined") != -1)
            {
                return (DPTResponseCodesMapping.CC_DECLINED_VAL__7003);
            }
            else if (responseCode.equals("N3"))
            {
                return (DPTResponseCodesMapping.CC_INVALID_ACCOUNT_VAL__7088);
            }
            else if (responseCode.equals("CE"))
            {
                return (DPTResponseCodesMapping.CC_SYSTEM_ERROR_VAL__7047);
            }
            else if (responseCode.equals("NH") || responseCode.equals("NJ"))
            {
                return (DPTResponseCodesMapping.CC_INVALID_AMOUNT_VAL__7033);
            }
            else if (responseCode.equals("NP"))
            {
                return (DPTResponseCodesMapping.CC_MERCHANT_ACCOUNT_CONFIG_ERROR_VAL__7098);
            }
            else
            {
                throw new CardTransactionException("Unknow reason: (" + responseCode + ", " + displayText
                        + "): ", e);
            }
        }
        switch (response_code)
        {
            case 54:
                return (DPTResponseCodesMapping.CC_EXPIRED_VAL__7001);
            case 5:
                if (displayText.trim().toLowerCase().indexOf("declined") != -1)
                {
                    return (DPTResponseCodesMapping.CC_DECLINED_VAL__7003);
                }
            case 51:
                String text = displayText.trim().toLowerCase();
                if (text.indexOf("declined") != -1 || text.indexOf("do not honor") != -1
                        || text.indexOf("do not try again") != -1)
                {
                    return (DPTResponseCodesMapping.CC_DECLINED_VAL__7003);
                }
                else if (text.indexOf("try again later") != -1)
                {
                    return (DPTResponseCodesMapping.CC_CANNOT_PROCESS_PLEASE_RETRY_VAL__7004);
                }
                else if (text.indexOf("inv acct num") != -1)
                {
                    return (DPTResponseCodesMapping.CC_INVALID_ACCOUNT_VAL__7088);
                }
            case 63:
                if (displayText.trim().toLowerCase().indexOf("declined") != -1)
                {
                    return (DPTResponseCodesMapping.CC_DECLINED_VAL__7003);
                }
            case 57:
                return (DPTResponseCodesMapping.CC_DECLINED_VAL__7003);
            case 28:
            case 91:
                return (DPTResponseCodesMapping.CC_CANNOT_PROCESS_PLEASE_RETRY_VAL__7004);

            case 10:
            case 61:
                return (DPTResponseCodesMapping.CC_INSUFFICIENT_FUNDS_VAL__7017);

            case 94:
                return (DPTResponseCodesMapping.CC_DUPLICATE_TRANS_VAL__7019);

            case 13:
                return (DPTResponseCodesMapping.CC_INVALID_AMOUNT_VAL__7033);

            case 12:
                if (displayText.trim().toLowerCase().indexOf("referrral-inv tr1") != -1)
                {
                    return (DPTResponseCodesMapping.CC_BAD_CREDIT_CARD_DATA_VAL__7045);
                }
                else
                {
                    return (DPTResponseCodesMapping.CC_INVALID_REQUEST_VAL__7058);
                }
            case 55:
            case 75:
                return (DPTResponseCodesMapping.CC_UNABLE_TO_AUTHENTICATE_VAL__7055);

            case 14:
                if (displayText.trim().toLowerCase().indexOf("inv acct num") != -1)
                {
                    return (DPTResponseCodesMapping.CC_INVALID_ACCOUNT_VAL__7088);
                }
            case 3:
            case 89:
                return (DPTResponseCodesMapping.CC_MERCHANT_ACCOUNT_CONFIG_ERROR_VAL__7098);
        }

        throw new CardTransactionException("Unknown reason: " + responseCode + ", " + displayText);
    }

    private String getSystemTrace()
    {
        String reference_number = getCommonProcessingService().getNewReferenceNumber(getMerchantAccount());
        String ref = reference_number.substring(reference_number.length() - 6);
        return ref;
    }

    // transaction id
    private String getRetrievalReferenceNumber(String systemTrace)
    {
        return "0000" + this.getTerminalId().substring(getTerminalId().length() - 2) + systemTrace;
    }

    private String getClientRef(String systemTrace)
    {
        return systemTrace + "0V" + VERSION_NUMBER;
    }

    // MMddHHmmss - GMT time
    private String getTransactionDate(Date date)
    {
        if (date == null)
        {
            date = new Date();
        }
        SimpleDateFormat date_format = new SimpleDateFormat("MMddHHmmss");
        date_format.setTimeZone(TimeZone.getTimeZone("GMT"));
        String transaction_date = date_format.format(date);
        return transaction_date;
    }

    // HHmmss - local time
    private String getTransactionTimeLocal(Date date)
    {
        if (date == null)
        {
            date = new Date();
        }
        SimpleDateFormat date_format = new SimpleDateFormat("HHmmss");
        String transaction_date = date_format.format(date);
        return transaction_date;
    }

    
    @Override
    public boolean isReversalExpired(Reversal reversal)
    {
        try
        {
            Date orgDate = new Date(Long.parseLong(reversal.getOriginalTime()));
            return (orgDate.getTime() + TWENTYFIVE_MINUTES_MS) < System.currentTimeMillis();
        }
        catch (Exception e)
        {
            return (reversal.getRetryAttempts() >= DEFAULT_MAX_REVERSAL_RETRIES);
        }
    }   
}
