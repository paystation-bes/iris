package com.digitalpaytech.cardprocessing.impl;

/**
 * 
 * This is a POJO to represent the CBORD transaction reply XML. Ignore
 * the class variable naming conventions. The names represent the exact XML
 * Element formats.
 * 
 * @author harshd
 *
 */
public class CBordTransactionReply {

	private String Message;
	private String className;
	private String Provider;
	private String ResponseCode;
	private String PaymentMedia;
	private String Plan;
	private String ActialSVCbalance;
	private String EndSVCbalance;
	private String Patron;
	private String TransID;
	private String HostMessage;
	private String HostReceipt;
	private String SysTraceAuditNumber;

	public  String getMessage() {
		return Message;
	}

	public String getClassName() {
		return className;
	}

	public String getProvider() {
		return Provider;
	}

	public String getResponseCode() {
		return ResponseCode;
	}

	public String getPaymentMedia() {
		return PaymentMedia;
	}

	public String getPlan() {
		return Plan;
	}

	public String getActialSVCbalance() {
		return ActialSVCbalance;
	}

	public String getEndSVCbalance() {
		return EndSVCbalance;
	}

	public String getPatron() {
		return Patron;
	}

	public String getTransID() {
		return TransID;
	}

	public String getHostMessage() {
		return HostMessage;
	}

	public String getHostReceipt() {
		return HostReceipt;
	}

	public String getSysTraceAuditNumber() {
		return SysTraceAuditNumber;
	}

}