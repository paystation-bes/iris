package com.digitalpaytech.cardprocessing.impl;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.log4j.Logger;

import com.digitalpaytech.cardprocessing.CardProcessor;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DPTResponseCodesMapping;
import com.paygateway.TransactionProtocolException;

public class ParadataProcessor extends CardProcessor 
{
	private static Logger logger__ = Logger.getLogger(ParadataProcessor.class);
	// Provided by Paradata and hardcoded. (unique to our app??)
	// private final static String PARADATA_TEST_TOKEN =
	// "195325FCC230184964CAB3A8D93EEB31888C42C714E39CBBB2E541884485D04B";

	// Paradata response codes
	private final static int RESPONSE_CODE_SUCCESSFUL_TRANSACTION = 1;
	// private final static String RESPONSE_CODE_MISSING_REQUIRED_REQUEST_FIELD = "2";
	// private final static String RESPONSE_CODE_INVALID_REQUEST_FIELD = "3";
	// private final static String RESPONSE_CODE_ILLEGAL_TRANSACTION_REQUEST = "4";
	// private final static String RESPONSE_CODE_TRANSACTION_SERVER_ERRROR = "5";
	// private final static String RESPONSE_CODE_TRANSACTION_NOT_POSSIBLE = "6";
	// private final static String RESPONSE_CODE_INVALID_VERSION = "7";
	// private final static String RESPONSE_CODE_MISSING_REQUIRED_RESPONSE_FIELD = "8";
	// private final static String RESPONSE_CODE_INVALID_RESPONSE_FIELD = "9";
	// private final static String RESPONSE_CODE_TRANSACTION_CLIENT_ERROR = "10";
	// private final static String RESPONSE_CODE_CREDIT_CARD_DECLINED = "100";
	// private final static String RESPONSE_CODE_ACQUIRER_GATEWAY_ERROR = "101";
	// private final static String RESPONSE_CODE_PAYMENT_ENGINE_ERROR = "102";

	// provided by Paradata
	// common to payment processor/Paradata - configurable via Merchant Account
	// setup page
	private String token_;
	private String processor_;

	// parsed responses from the processor's server
	private int actionCode_;
	private String authOrErrorCode_;
	private String referenceNumber_;
	private String responseMessage_;
	private String processorTransactionId_;
	private String referenceId_;

	public ParadataProcessor(MerchantAccount md,
	        CommonProcessingService commonProcessingService, 
	        EmsPropertiesService emsPropertiesService, CustomerCardTypeService customerCardTypeService) {

		super(md, commonProcessingService, emsPropertiesService);
		setCustomerCardTypeService(customerCardTypeService);
		token_ = md.getField1();
		processor_ = md.getField2();
		
		StringBuilder bdr = new StringBuilder();
		bdr.append("Paradata Gateway - Merchant Account ID=").append(md.getId()).append(", Token=").append(token_).append(", Processor=").append(processor_);
		logger__.debug(bdr.toString());
	}

	public boolean processPreAuth(PreAuth pd) throws CardTransactionException {
		if (logger__.isDebugEnabled()) {
			logger__.debug("Performing a Pre-Auth through Paradata Gateway");
		}

		CustomerCardType customerCardType = getCustomerCardTypeService().getCardTypeByTrack2Data(getMerchantAccount().getCustomer().getId(), pd.getCardData());
		Track2Card track_2_card = new Track2Card(customerCardType, getMerchantAccount().getCustomer().getId(), pd.getCardData());
		com.paygateway.CreditCardRequest request = new com.paygateway.CreditCardRequest();
		String reference_number = getCommonProcessingService().getNewReferenceNumber(getMerchantAccount());

		// Create the pre-auth request
		// Required field are: Charge Total, Charge Type, Credit Card Number,
		// Expire Month, Expire Year
		try 
		{
			// need to strip off leading zeros from order id
			String order_id = (Integer.valueOf(reference_number)).toString();

			// EMS-1372. Do not set order id for global east processor
			if (!processor_.equals("Global East")) 
			{
				if (order_id.length() > 9) 
				{
					request.setOrderId(order_id.substring(order_id.length() - 9));
				} 
				else 
				{
					request.setOrderId(order_id);
				}
			}

			if (processor_.equals("Moneris")
			        || processor_.equals("First Data Merchant Services Nashville")
			        || processor_.equals("First Data Merchant Services North")
			        || processor_.equals("First Data Merchant Services South")
			        || processor_.equals("BCE Emergis")) {
				request.setCreditCardNumber(track_2_card.getPrimaryAccountNumber());
				request.setExpireMonth(track_2_card.getExpirationMonth());
				request.setExpireYear("20" + track_2_card.getExpirationYear());
			} 
			else 
			{
				request.setTrack2(pd.getCardData());
			}
			request.setChargeType(com.paygateway.CreditCardRequest.AUTH);
			request.setChargeTotal(CardProcessingUtil.getCentsAmountInDollars(pd.getAmount()).toString());

			if (processor_.equals("Paymentech Tampa") || processor_.equals("First Data Merchant Services North")) 
			{
				// 8 = Cardholder present. Retail order. Signature not obtained.
				request.setTransactionConditionCode(8);
			}
		} 
		catch (TransactionProtocolException e) 
		{
			throw new CardTransactionException(
			        "Exception during request creation", e);
		}

		// send the request
		sendRequestAndHandleResponse(request);

		// EMS-1374. CardData needed for post-auth
		// pd.setCardData(null); // CardData not needed for post-auths

		pd.setProcessingDate(new Date());

		// EMS-1372. Use processor returned order ID as reference number
		if (processor_.equals("Global East")) 
		{
			pd.setReferenceNumber(this.referenceNumber_);
		} 
		else 
		{
			pd.setReferenceNumber(reference_number);
		}
		pd.setProcessorTransactionId(processorTransactionId_);
		pd.setReferenceId(referenceId_);

		if (actionCode_ == RESPONSE_CODE_SUCCESSFUL_TRANSACTION) 
		{
			pd.setAuthorizationNumber(authOrErrorCode_);
			pd.setIsApproved(true);
		} 
		else 
		{
			pd.setIsApproved(false);
			pd.setResponseCode(String.valueOf(actionCode_));
		}

		return (pd.isIsApproved());
	}

	public Object[] processPostAuth(PreAuth pd, ProcessorTransaction ptd)
	        throws CardTransactionException {
		if (logger__.isDebugEnabled()) {
			logger__.debug("Settling a pre-authorized transaction via Paradata Gateway");
		}

		// Create request
		com.paygateway.CreditCardRequest request = new com.paygateway.CreditCardRequest();
		try 
		{
			// order ID has to be from original pre-auth transaction
			// need to strip off leading zeros from order id
			String order_id = (Integer.valueOf(pd.getReferenceNumber()))
			        .toString();
			if (order_id.length() > 9) 
			{
				request.setOrderId(order_id.substring(order_id.length() - 9));
			} 
			else 
			{
				request.setOrderId(order_id);
			}
			request.setChargeType(com.paygateway.CreditCardRequest.CAPTURE);
			request.setReferenceId(pd.getReferenceId());
			request.setChargeTotal(CardProcessingUtil.getCentsAmountInDollars(pd.getAmount()).toString());
			CustomerCardType customerCardType = getCustomerCardTypeService().getCardTypeByTrack2DataOrAccountNumber(getMerchantAccount().getCustomer().getId(), pd.getCardData());
			Track2Card track_2_card = new Track2Card(customerCardType, getMerchantAccount().getCustomer().getId(), pd.getCardData());
			request.setCreditCardNumber(track_2_card.getPrimaryAccountNumber());
		} 
		catch (TransactionProtocolException e) 
		{
			throw new CardTransactionException(
			        "Exception during request creation", e);
		}

		// Send request
		sendRequestAndHandleResponse(request);

		// Set ProcessorTransactionData values from processor
		ptd.setProcessingDate(new Date());
		ptd.setReferenceNumber(pd.getReferenceNumber());
		ptd.setProcessorTransactionId(processorTransactionId_);

		// Approved
		if (actionCode_ == RESPONSE_CODE_SUCCESSFUL_TRANSACTION) 
		{
			ptd.setAuthorizationNumber(authOrErrorCode_);
			ptd.setIsApproved(true);
			return new Object[] { true, actionCode_ };
		}

		// Declined
		ptd.setAuthorizationNumber(null);
		ptd.setIsApproved(false);
		return new Object[] { false, actionCode_ };
	}

	public Object[] processRefund(boolean isNonEmsRequest,
	        ProcessorTransaction origPtd, ProcessorTransaction refundPtd,
	        String ccNumber, String expiryMMYY) throws CardTransactionException {
		if (logger__.isDebugEnabled()) {
			logger__.debug("Performing a Refund transaction through Paradata Gateway");
		}
		// Create request
		com.paygateway.CreditCardRequest request = new com.paygateway.CreditCardRequest();
		try 
		{
			// need to strip off leading zeros from order id
			String order_id = (Integer.valueOf(origPtd.getReferenceNumber())).toString();
			if (order_id.length() > 9) 
			{
				request.setOrderId(order_id.substring(order_id.length() - 9));
			} 
			else 
			{
				request.setOrderId(order_id);
			}
			PreAuth ph = getCardProcessingManager().getPreAuth(origPtd);
			if(ph != null)
			{
			    request.setReferenceId(ph.getReferenceId());
			}
			request.setCreditCardNumber(ccNumber);
			request.setExpireMonth(expiryMMYY.substring(0, 2));
			request.setExpireYear("20" + expiryMMYY.substring(2));
			request.setChargeType(com.paygateway.CreditCardRequest.CREDIT);
			request.setChargeTotal(CardProcessingUtil.getCentsAmountInDollars(
			        refundPtd.getAmount()).toString());
		} 
		catch (TransactionProtocolException e) 
		{
			throw new CardTransactionException(
			        "Exception during request creation", e);
		}

		// Send request
		sendRequestAndHandleResponse(request);

		// Set processor transaction data values
		refundPtd.setProcessingDate(new Date());
		refundPtd.setReferenceNumber(referenceNumber_);
		refundPtd.setProcessorTransactionId(processorTransactionId_);

		// Approved
		if (actionCode_ == RESPONSE_CODE_SUCCESSFUL_TRANSACTION) 
		{
			refundPtd.setAuthorizationNumber(authOrErrorCode_);
			refundPtd.setIsApproved(true);
			return new Object[] {
			        DPTResponseCodesMapping.CC_APPROVED_VAL__7000, actionCode_ };
		}

		// Declined
		refundPtd.setAuthorizationNumber(null);
		refundPtd.setIsApproved(false);

		// if request is non-EMS (eg from BOSS) then map response.
		if (isNonEmsRequest) 
		{
			return new Object[] {mapErrorResponse(actionCode_, responseMessage_), actionCode_ };
		}

		// EMS Request
		return new Object[] { actionCode_, actionCode_ };
	}

	public Object[] processCharge(Track2Card track2Card,
	        ProcessorTransaction chargePtd) throws CardTransactionException 
	{
		com.paygateway.CreditCardRequest request = createChargeRequest(track2Card, CardProcessingUtil.getCentsAmountInDollars(chargePtd.getAmount()));
		com.paygateway.CreditCardResponse response = sendRequest(request);
		return handleChargeResponse(response, chargePtd);
	}

	private com.paygateway.CreditCardRequest createChargeRequest(
	        Track2Card track2Card, BigDecimal amountInDollars)
	        throws CardTransactionException 
	{
		com.paygateway.CreditCardRequest request = new com.paygateway.CreditCardRequest();

		// Create request
		try {
			// need to strip off leading zeros from order id
			String order_id = (Integer.valueOf(getCommonProcessingService().getNewReferenceNumber(getMerchantAccount()))).toString();

			// EMS-1372. Do not set order id for global east processor
			if (!processor_.equals("Global East")) 
			{
				if (order_id.length() > 9) 
				{
					request.setOrderId(order_id.substring(order_id.length() - 9));
				} 
				else 
				{
					request.setOrderId(order_id);
				}
			}
			request.setCreditCardNumber(track2Card.getPrimaryAccountNumber());
			request.setExpireMonth(track2Card.getExpirationMonth());
			request.setExpireYear("20" + track2Card.getExpirationYear());
			request.setChargeType(com.paygateway.CreditCardRequest.SALE);
			request.setChargeTotal(amountInDollars.toString());
		} 
		catch (TransactionProtocolException e) 
		{
			throw new CardTransactionException(
			        "Exception during request creation", e);
		}

		if (logger__.isInfoEnabled()) {
			StringBuilder sb = new StringBuilder(100);
			sb.append("Charge Request:  Type: ");
			sb.append(request.getChargeType());
			sb.append(", Order ID: ");
			sb.append(request.getOrderId());
			sb.append(", Charge total: ");
			sb.append(request.getChargeTotal());
			sb.append(", Transaction Condition Code: ");
			sb.append(request.getTransactionConditionCode());
			logger__.info(sb.toString());
		}
		return (request);
	}

	private com.paygateway.CreditCardResponse sendRequest(
	        com.paygateway.CreditCardRequest request)
	        throws CardTransactionException 
	{
		// Send the request
		try 
		{
			com.paygateway.CreditCardResponse response = (com.paygateway.CreditCardResponse) com.paygateway.TransactionClient.doTransaction(request, token_);
			return (response);
		} 
		catch (Exception e) 
		{
			throw new CardTransactionException("Paradata Processor is offline", e);
		}
	}

	private Object[] handleChargeResponse(
	        com.paygateway.CreditCardResponse response, ProcessorTransaction ptd)
	        throws CardTransactionException 
	{
		// Bank approval code: 6 alphanumeric chars generated by the issuer that
		// ids the transaction
		// Bank transaction ID: ID generated by the bank
		// Reference ID: Identifier for follow-on transactions
		if (response != null) {
			if (logger__.isDebugEnabled()) {
				StringBuilder bdr = new StringBuilder();
				bdr.append("Received Paradata Charge response: Response Code=").append(response.getResponseCode()).append(", Response Code Text=").append(response.getResponseCodeText());
				bdr.append(", Retry Recommended=").append(response.isRetryRecommended()).append(", Order ID=").append(response.getOrderId()).append(", AVS Code=");
				bdr.append(response.getAvsCode()).append(", Bank Approval Code=").append(response.getBankApprovalCode()).append(", Bank Transaction ID=");
				bdr.append(response.getBankTransactionId()).append(", Batch ID=").append(response.getBatchId()).append(", Reference ID=").append(response.getReferenceId());
				bdr.append(", CC Ver. Response=").append(response.getCreditCardVerificationResponse()).append(", ISO Code=").append(response.getIsoCode()).append(", Timestamp=");
				bdr.append(response.getTimeStamp());
				logger__.debug(bdr.toString());
			} else {
				StringBuilder sb = new StringBuilder(200);
				sb.append("Charge Response:  ResponseCode: ");
				sb.append(response.getResponseCode());
				sb.append(", ResponseCodeText: ");
				sb.append(response.getResponseCodeText());
				sb.append(", Authorization Number: ");
				sb.append(response.getBankApprovalCode());
				sb.append(", ReferenceNumber: ");
				sb.append(response.getOrderId());
				sb.append(", ProcessorTransactionId: ");
				sb.append(response.getBankTransactionId());
				logger__.info(sb.toString());
			}

			ptd.setProcessingDate(new Date());
			ptd.setReferenceNumber(response.getOrderId());

			// Approved
			if (RESPONSE_CODE_SUCCESSFUL_TRANSACTION == response.getResponseCode()) 
			{
				ptd.setAuthorizationNumber(response.getBankApprovalCode());
				ptd.setProcessorTransactionId(response.getBankTransactionId());
				ptd.setIsApproved(true);
				return new Object[] {DPTResponseCodesMapping.CC_APPROVED_VAL__7000, response.getResponseCode() };
			}

			// Declined
			ptd.setAuthorizationNumber(null);
			ptd.setIsApproved(false);
			return new Object[] {mapErrorResponse(response.getResponseCode(), response.getResponseCodeText()), response.getResponseCode()};
		}

		throw new CardTransactionException("Response is null");
	}

	private void sendRequestAndHandleResponse(com.paygateway.CreditCardRequest request)
	        throws CardTransactionException 
	{
		com.paygateway.CreditCardResponse response = null;

		// Send the request
		try 
		{
			StringBuilder bdr = new StringBuilder();
			bdr.append("Sending Paradata ").append(request.getChargeType()).append(" request: Order ID=").append(request.getOrderId()).append(", Charge total=");
			bdr.append(request.getChargeTotal()).append(", Reference ID=").append(request.getReferenceId()).append(", Transaction Condition Code=");
			bdr.append(request.getTransactionConditionCode());
			logger__.debug(bdr.toString());
		
			StringBuilder sb = new StringBuilder(100);
			sb.append("Request:  Type: ");
			sb.append(request.getChargeType());
			sb.append(", Order ID: ");
			sb.append(request.getOrderId());
			sb.append(", Charge total: ");
			sb.append(request.getChargeTotal());
			sb.append(", Transaction Condition Code: ");
			sb.append(request.getTransactionConditionCode());
			logger__.info(sb.toString());

			response = (com.paygateway.CreditCardResponse) com.paygateway.TransactionClient.doTransaction(request, token_);
		} 
		catch (Exception e) 
		{
			throw new CardTransactionException("Paradata Processor is offline", e);
		}

		// Bank approval code: 6 alphanumeric chars generated by the issuer that
		// ids the transaction
		// Bank transaction ID: ID generated by the bank
		// Reference ID: Identifier for follow-on transactions
		if (response != null) 
		{
			if (logger__.isDebugEnabled()) 
			{
				StringBuilder bdr = new StringBuilder();
				bdr.append("Received Paradata Charge response: Response Code=").append(response.getResponseCode()).append(", Response Code Text=");
				bdr.append(response.getResponseCodeText()).append(", Retry Recommended=").append(response.isRetryRecommended()).append(", Order ID=");
				bdr.append(response.getOrderId()).append(", AVS Code=").append(response.getAvsCode()).append(", Bank Approval Code=").append(response.getBankApprovalCode());
				bdr.append(", Bank Transaction ID=").append(response.getBankTransactionId()).append(", Batch ID=").append(response.getBatchId()).append(", Reference ID=");
				bdr.append(response.getReferenceId()).append(", CC Ver. Response=").append(response.getCreditCardVerificationResponse()).append(", ISO Code=");
				bdr.append(response.getIsoCode()).append(", Timestamp=").append(response.getTimeStamp());
				logger__.debug(bdr.toString());
			} 
			else 
			{
				StringBuilder sb = new StringBuilder(200);
				sb.append("Response:  ResponseCode: ");
				sb.append(response.getResponseCode());
				sb.append(", ResponseCodeText: ");
				sb.append(response.getResponseCodeText());
				sb.append(", Authorization Number: ");
				sb.append(response.getBankApprovalCode());
				sb.append(", ReferenceNumber: ");
				sb.append(response.getOrderId());
				sb.append(", ProcessorTransactionId: ");
				sb.append(response.getBankTransactionId());
				logger__.info(sb.toString());
			}

			// Extract data from the response
			actionCode_ = response.getResponseCode();
			responseMessage_ = response.getResponseCodeText();

			// actionCode_ = 1 means success
			if (actionCode_ == RESPONSE_CODE_SUCCESSFUL_TRANSACTION) 
			{
				authOrErrorCode_ = response.getBankApprovalCode();
				referenceNumber_ = response.getOrderId();
				referenceId_ = response.getReferenceId();
				processorTransactionId_ = response.getBankTransactionId();
			} 
			else 
			{
				authOrErrorCode_ = String.valueOf(actionCode_);
				processorTransactionId_ = null;
				referenceNumber_ = response.getOrderId();
			}
		} 
		else 
		{
			throw new CardTransactionException("Null response. Check Paradata MSC for transaction status.");
		}
	}

	private int mapErrorResponse(int responseCode, String responseText)
	        throws CardTransactionException 
	{
		switch (responseCode) 
		{
    		// CREDIT CARD DECLINED: Response code indicating that the credit card
    		// transaction was declined.
    		case 100:
    			return (DPTResponseCodesMapping.CC_DECLINED_VAL__7003);
    
    			// ACQUIRER GATEWAY ERROR: Response code indicating that the
    			// acquirer processor encountered an
    			// error.
    			// This is a software program that handles financial transactions
    			// and communicates with the
    			// private
    			// banking network.
    		case 101:
    			// PAYMENT ENGINE ERROR: Response code indicating that the payment
    			// service encountered an
    			// error.
    		case 102:
    			return (DPTResponseCodesMapping.CC_SYSTEM_ERROR_VAL__7047);
    
    			// INVALID REQUEST FIELD: Response code indicating that one of the
    			// values provided was not
    			// valid. The
    			// field will be identified in the response code text.
    		case 3:
    			// special check for return message
    			if ((responseText != null)
    			        && (responseText
    			                .indexOf("value of Account Token does not match expected format") > 0))
    				throw new CardTransactionException(
    				        "Response indicates invalid merchant: 3");
    			// MISSING REQUIRED REQUEST FIELD: Response code indicating that a
    			// required request field was
    			// not
    			// provided with the request. The required field will be identified
    			// in the response code text.
    		case 2:
    			// INVALID VERSION: Response code indicating that the version of the
    			// Java Transaction Client
    			// API being
    			// used is no longer valid.
    		case 7:
    			return (DPTResponseCodesMapping.CC_INVALID_REQUEST_VAL__7058);
    
    			// ILLEGAL TRANSACTION REQUEST: Response code indicating the
    			// transaction request was illegal.
    			// This can
    			// happen if a transaction is sent for an account that does not
    			// exist or if the account has
    			// not been
    			// configured to perform the requested transaction type.
    		case 4:
    			throw new CardTransactionException(
    			        "Response indicates invalid merchant: " + responseCode);
    
    			// TRANSACTION SERVER ERROR: Response code indicating that an error
    			// occurred within the
    			// transaction
    			// server. This type of error is temporary. If one occurs
    			// maintenance staff are immediately
    			// signalled
    			// to correct the problem.
    		case 5:
    			return (DPTResponseCodesMapping.CC_CANNOT_PROCESS_PLEASE_RETRY_VAL__7004);
    
    			// TRANSACTION NOT POSSIBLE: Response code indicating that the
    			// requested transaction is not
    			// possible.
    			// This can happen if the transaction request refers to a previous
    			// transaction that does not
    			// exist.
    			// For example, one possible request is to perform a capture of
    			// funds previously authorized
    			// from a
    			// customer�s credit card. If a capture request is sent that
    			// refers to an authorization that
    			// does not
    			// exist then this response code will be returned.
    		case 6:
    			return (DPTResponseCodesMapping.CC_UNABLE_TO_PROCESS_VAL__7084);
    
    			// MISSING REQUIRED RESPONSE FIELD: Response code indicating that a
    			// required field was not
    			// given a
    			// value.
    		case 8:
    			// INVALID RESPONSE FIELD: Response code indicating that a required
    			// field was given an invalid
    			// value.
    		case 9:
    			return (DPTResponseCodesMapping.CC_SYSTEM_ERROR_VAL__7047);
    
    		default:
    			throw new CardTransactionException("Unknown Response Code: " + responseCode);
		}
	}

	@Override
	public void processReversal(Reversal reversal, Track2Card track2Card) 
	{
		throw new UnsupportedOperationException("ParadataProcessor, Reversal not supported for this processor");
	}
	
	@Override
	public boolean isReversalExpired(Reversal reversal) {
		throw new UnsupportedOperationException("ParadataProcessor, isReversalExpired(Reversal) is not support!");
	}		
}
