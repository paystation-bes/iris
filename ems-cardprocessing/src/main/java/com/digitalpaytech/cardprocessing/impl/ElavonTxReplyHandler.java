package com.digitalpaytech.cardprocessing.impl;

import com.digitalpaytech.util.CardProcessingConstants;
/**
 * Stores the results of a response from Elavon virtual merchant.
 * No setters are needed as instances of this object are created
 * and populated by XStream
 * @author danielm
 *
 */
public class ElavonTxReplyHandler {
    // These fields are all used by com.thoughtworks.xstream.XStream, bypassing setters and getters.
    // Ignore the "unused field" warning as the jdk is not aware of xstream's dependency on these fields.
    // This is true even for fields which are never used in our business logic - xstream will still attempt to write to them.
	private String ssl_approval_code = "";
	private String ssl_invoice_number = "";
	private String ssl_cvv2_response = "";
	private String ssl_exp_date = "";
	private String ssl_amount = "";
	private String ssl_txn_id = "";
	private String ssl_result = "";
	private String ssl_card_number = "";
	private String ssl_account_balance = "";
	private String ssl_txn_time = "";
	private String ssl_result_message = "";
	private String ssl_avs_response = "";
	private String ssl_salestax = "";
	private String ssl_balance_due = "";
	
	private String errorCode = "";
	private String errorName = "";
	private String errorMessage = "";
	
	public String getApprovalCode(){
		return ssl_approval_code;
	}
	public String getInvoiceNumber(){
		return ssl_invoice_number;
	}
	public String getExpDate(){
		return ssl_exp_date;
	}
	public String getAmount(){
		return ssl_amount;
	}
	public String getTxnId(){
		return ssl_txn_id;
	}
	public String getResult(){
		return ssl_result;
	}
	public String getCardNumber(){
		return ssl_card_number;
	}
	public String getAccountBalance(){
		return ssl_account_balance;
	}
	public String getTxnTime(){
		return ssl_txn_time;
	}
	public String getResultMessage(){
		return ssl_result_message;
	}
	public String getBalanceDue(){
		return ssl_balance_due;
	}
	public String getErrorCode(){
		return errorCode;
	}
	public String getErrorMessage(){
		return errorMessage;
	}
	public String getErrorName(){
		return errorName;
	}
}
