package com.digitalpaytech.cardprocessing.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.digitalpaytech.cardprocessing.CardProcessor;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.DateUtil;

public class TotalCardProcessor extends CardProcessor {
	private final static Logger logger__ = Logger
	        .getLogger(TotalCardProcessor.class);

	private static final String QUERY_STRING = "Query";
	private static final String POST_STRING = "Post";
	private static final String TASK_STRING = "Task";
	private static final String STORE_NUMBER_STRING = "Storenumber";
	private static final String TERMINAL_NUMBER_STRING = "Terminalnumber";
	private static final String SECURITY_KEY_STRING = "securitykey";
	private static final String TRANSACTION_DATE_STRING = "Transactiondate";
	private static final String TRANSACTION_TIME_STRING = "Transactiontime";
	private static final String TRANSACTION_NUMBER_STRING = "Transactionnumber";
	private static final String TRANSACTION_AMOUNT_STRING = "Transactionamount";
	private static final String ACCOUNT_NUMBER_STRING = "Accountnumber";
	private static final String LOST_CARD_CODE_STRING = "Lostcardcode";
	private static final String RESPONSE_STATUS_OK_STRING = "OK";
	private static final String RESPONSE_STATUS_NO_STRING = "NO";

	// date format required to be sent in the message. date and time can be
	// tokenized
	public final static String DATE_TIME_FORMAT = "yyyyMMdd:HHmm";

	private static final String iPContext = "/tc/wi0012.pgm";
	private static final int QUERY_TYPE = 1;
	private static final int POST_TYPE = 2;

	// the variables that will carry the tokenized response
	private String responseStatus_ = null;
	private String responseMessage_ = null;
	private String responseAmount1_ = null;
	private String responseAmount2_ = null;

	// fields to be read from the db pertaining to merchant account
	private String securityKey = null;
	private String iPAddress = null;
	private String storeNumber = null;
	private String terminalNumber = null;

	private String transactionDate = null;
	private String transactionTime = null;

	public TotalCardProcessor(MerchantAccount md,
	        CommonProcessingService commonProcessingService,
	        EmsPropertiesService emsPropertiesService, String customerTimeZone) {

		super(md, commonProcessingService, emsPropertiesService);
		try {
			logger__.debug("Creating TotalCard processor object");

			securityKey = md.getField1();
			iPAddress = md.getField2();
			storeNumber = md.getField3();
			terminalNumber = md.getField4();

			// get the date and time depending on the time zone of the
			// paystation
			Date date = new Date();
			TimeZone tz = TimeZone.getTimeZone(customerTimeZone);
			String date_time = DateUtil.convertUTCTimeToDateTimeByTZ(tz, date, DATE_TIME_FORMAT);
			StringTokenizer stk = new StringTokenizer(date_time, ":");
			transactionDate = stk.nextToken();
			transactionTime = stk.nextToken();

		} catch (Exception e) {
			throw new CardTransactionException(
			        "Could not initialize TotalCard Processor!");
		}

		logger__.info("Created TotalCard processor object");

	}

	public boolean processPreAuth(PreAuth pd) throws CardTransactionException {
		logger__.debug("Performing Pre-Auth for TotalCard transaction");

		createAndSendRequest(QUERY_TYPE, pd.getCardData(), null);
		pd.setProcessingDate(new Date());

		if ((responseMessage_ == null)
		        || (responseMessage_.equalsIgnoreCase("NULL"))) {
			throw new CardTransactionException("TotalCard Processor is Offline");
		}

		String reference_number = getCommonProcessingService()
		        .getNewReferenceNumber(getMerchantAccount());
		pd.setReferenceNumber(reference_number);
		pd.setResponseCode(responseMessage_);

		// Get the balance from the response (response_amount_2) and compare
		// with requested amount
		if (Integer.parseInt(responseAmount2_) >= pd.getAmount()) {
			pd.setIsApproved(true);
			pd.setAuthorizationNumber(reference_number);
		} else {
			pd.setIsApproved(false);
		}

		return (pd.isIsApproved());
	}

	public Object[] processPostAuth(PreAuth pd, ProcessorTransaction ptd)
	        throws CardTransactionException {
		// Create and send request
		createAndSendRequest(POST_TYPE, pd.getCardData(),
		        Integer.toString(pd.getAmount()));
		ptd.setProcessingDate(new Date());

		if ((responseMessage_ == null)
		        || (responseMessage_.equalsIgnoreCase("NULL"))) {
			throw new CardTransactionException("TotalCard Processor is offline");
		}

		// Set ProcessorTransactionData values from processor

		ptd.setReferenceNumber(getCommonProcessingService()
		        .getNewReferenceNumber(getMerchantAccount()));
		ptd.setProcessorTransactionId(pd.getProcessorTransactionId());
		ptd.setAuthorizationNumber(pd.getAuthorizationNumber());

		// Check approval status
		ptd.setIsApproved(responseStatus_
		        .equalsIgnoreCase(RESPONSE_STATUS_OK_STRING));
		return new Object[] { ptd.isIsApproved(), responseStatus_ };
	}

	private void createAndSendRequest(int type, String cardData,
	        String transactionAmount) throws CardTransactionException {
		StringBuffer parameters = new StringBuffer();
		StringBuffer urlString = new StringBuffer("http://" + iPAddress);
		String responseString = "";
		try {
			urlString.append(iPContext);

			parameters.append(TASK_STRING + "=");
			if (type == QUERY_TYPE) {
				parameters.append(QUERY_STRING);
			} else if (type == POST_TYPE) {
				parameters.append(POST_STRING);
			}

			parameters.append("&" + STORE_NUMBER_STRING + "=");
			parameters.append(storeNumber);
			parameters.append("&" + TERMINAL_NUMBER_STRING + "=");
			parameters.append(terminalNumber);
			parameters.append("&" + SECURITY_KEY_STRING + "=");
			parameters.append(securityKey);
			parameters.append("&" + TRANSACTION_DATE_STRING + "=");
			parameters.append(transactionDate);
			parameters.append("&" + TRANSACTION_TIME_STRING + "=");
			parameters.append(transactionTime);

			StringTokenizer stk = new StringTokenizer(cardData, "=");
			String accountNumber = stk.nextToken();
			String lostCardCode = stk.nextToken();

			parameters.append("&" + ACCOUNT_NUMBER_STRING + "=");
			parameters.append(accountNumber);
			parameters.append("&" + LOST_CARD_CODE_STRING + "=");
			parameters.append(lostCardCode);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (type == POST_TYPE) {
			parameters.append("&" + TRANSACTION_AMOUNT_STRING + "=");
			parameters.append(transactionAmount);
		}

		// now send the request
		try {
			URL url = new URL(urlString.toString());
			URLConnection con = url.openConnection();

			con.setDoInput(true);
			con.setDoOutput(true);
			con.setUseCaches(false);
			con.setAllowUserInteraction(false);
			con.setRequestProperty("Content-Type", "text/xml");
			OutputStream out = con.getOutputStream();
			out.write(parameters.toString().getBytes());
			out.flush();
			out.close();
			InputStream in = null;
			in = con.getInputStream();

			BufferedReader rd = new BufferedReader(new InputStreamReader(in));

			String s = null;
			while ((s = rd.readLine()) != null) {
				responseString += s;
			}
			rd.close();
			logger__.info("Received response\n" + responseString
			        + "\n from TotalCard server...");
		} catch (IOException e) {
			throw new CardTransactionException(
			        "TotalCard Processor is offline", e);
		}

		extractResponses(responseString);
	}

	// this method extracts the responses from the html document returned by the
	// TotalCard processor
	private void extractResponses(String responseString)
	        throws CardTransactionException {
		// get the indices where the response message begins and ends inside the
		// html tags

		if (responseString != null
		        && (responseString.indexOf("ResponseStatus") == -1)) {
			// this means that "ResponseStatus" is not a string in the response.
			// The processor is not
			// returning reply as expected
			throw new CardTransactionException(
			        "Response from TotalCard processor not as expected!!");
		}

		int startIndex = responseString.indexOf("ResponseStatus");
		logger__.info("Response status index is " + startIndex);
		int endIndex = responseString.lastIndexOf("</body>");

		String response = responseString.substring(startIndex, endIndex);

		StringTokenizer stk = new StringTokenizer(response, "&");

		String responseStatusMessage = stk.nextToken();
		responseStatus_ = extractValueFromResponse(responseStatusMessage);

		if (stk.hasMoreTokens()) {
			String responseMessageMessage = stk.nextToken();
			responseMessage_ = extractValueFromResponse(responseMessageMessage);
		}

		if (stk.hasMoreTokens()) {
			String responseAmount1Message = stk.nextToken();
			responseAmount1_ = extractValueFromResponse(responseAmount1Message);
		}

		if (stk.hasMoreTokens()) {
			String responseAmount2Message = stk.nextToken();
			responseAmount2_ = extractValueFromResponse(responseAmount2Message);
		}

	}

	private String extractValueFromResponse(String responseStatusMessage) {
		StringTokenizer stk = new StringTokenizer(responseStatusMessage, "=");
		String key = stk.nextToken();
		return stk.nextToken();
	}

	@Override
	public void processReversal(Reversal reversal, Track2Card track2Card) {
		throw new UnsupportedOperationException(
		        "TotalCardProcessor, do not support reversal.");
	}

	@Override
	public Object[] processRefund(boolean isNonEmsRequest,
	        ProcessorTransaction origProcessorTransaction,
	        ProcessorTransaction refundProcessorTransaction,
	        String creditCardNumber, String expiryMMYY) {
		throw new UnsupportedOperationException(
		        "TotalCardProcessor, do not support refund.");
	}

	@Override
	public Object[] processCharge(Track2Card track2Card,
	        ProcessorTransaction chargeProcessorTransaction) {
		throw new UnsupportedOperationException(
		        "TotalCardProcessor, do not support charge.");
	}
	
	@Override
	public boolean isReversalExpired(Reversal reversal) {
		throw new UnsupportedOperationException("NuVisionProcessor, isReversalExpired(Reversal) is not support!");
	}			
}
