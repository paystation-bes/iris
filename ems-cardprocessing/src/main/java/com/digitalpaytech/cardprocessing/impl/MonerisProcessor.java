package com.digitalpaytech.cardprocessing.impl;

import java.util.Date;

import org.apache.log4j.Logger;

import com.digitalpaytech.cardprocessing.CardProcessor;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DPTResponseCodesMapping;

public class MonerisProcessor extends CardProcessor 
{
	private static Logger logger__ = Logger.getLogger(MonerisProcessor.class);
	
	// if the reference # / order_id is a duplicate or if API Token is wrong then the Moneris processor does
	// not
	// send us a response code. So have to
	// check this message in the code and create a special response codes...cannot be any of Moneris' return
	// codes
	private final static String DUPLICATE_ORDER_ID_MESSAGE = "The transaction was not sent to the host because of a duplicate order id";
	private final static String API_TOKEN_MISMATCH_MESSAGE = "API token mismatch";
	private final static String CRYPT_VALUE = "7";

	private String storeId;
	private String apiToken_;
	private String destinationUrlString_;

	
	public MonerisProcessor(MerchantAccount md, 
							CommonProcessingService commonProcessingService, 
							EmsPropertiesService emsPropertiesService, 
							CustomerCardTypeService customerCardTypeService) 
	{
		super(md, commonProcessingService, emsPropertiesService);
		setCustomerCardTypeService(customerCardTypeService);
		
		storeId = md.getField1();
		apiToken_ = md.getField2();
		destinationUrlString_ = commonProcessingService.getDestinationUrlString(md.getProcessor());
		if (logger__.isDebugEnabled()) {
			logger__.debug("MonerisProcessor, destinationUrlString: " + destinationUrlString_);
		}
	}

	
	public boolean processPreAuth(PreAuth pd) throws CardTransactionException 
	{
		CustomerCardType customerCardType = getCustomerCardTypeService().getCardTypeByTrack2Data(getMerchantAccount().getCustomer().getId(), pd.getCardData());
		Track2Card track_2_card = new Track2Card(customerCardType, getMerchantAccount().getCustomer().getId(), pd.getCardData());
		String card = track_2_card.getPrimaryAccountNumber();
		String exp = track_2_card.getExpirationYear() + track_2_card.getExpirationMonth();

		String reference_number = getCommonProcessingService().getNewReferenceNumber(getMerchantAccount());
		Double amt = CardProcessingUtil.getCentsAmountInDollars(pd.getAmount()).doubleValue();
	
		StringBuilder sb = new StringBuilder(100);
		sb.append("PreAuth Request:  Order ID: ");
		sb.append(reference_number);
		sb.append(", Charge total: ");
		sb.append(amt.toString());
		logger__.info(sb.toString());
	
		JavaAPI.HttpsPostRequest mpgReq = new JavaAPI.HttpsPostRequest(
				destinationUrlString_,
				storeId,
				apiToken_,
				new JavaAPI.PreAuth(reference_number, amt.toString(), card, exp, CRYPT_VALUE));

		JavaAPI.Receipt receipt = mpgReq.getReceipt();

		sb = new StringBuilder(100);
		sb.append("PreAuth Response:  Response Code: ");
		sb.append(receipt.getResponseCode());
		sb.append(", Response Message: ");
		sb.append(receipt.getMessage());
		sb.append(", Auth Code: ");
		sb.append(receipt.getAuthCode());
		sb.append(", ReferenceNumber: ");
		sb.append(reference_number);
		sb.append(", Charge total: ");
		sb.append(amt.toString());
		logger__.info(sb.toString());
		
		pd.setProcessingDate(new Date());
		pd.setReferenceNumber(reference_number);
		pd.setResponseCode(receipt.getResponseCode());
		pd.setProcessorTransactionId(receipt.getTxnNumber());

		if ((receipt.getResponseCode() == null) || (receipt.getResponseCode().equalsIgnoreCase("NULL"))) 
		{
			if (receipt.getMessage().trim().equalsIgnoreCase(DUPLICATE_ORDER_ID_MESSAGE)) 
			{
				throw new CardTransactionException("Duplicate Reference number/order id");
			} 
			else if (receipt.getMessage().trim().equalsIgnoreCase(API_TOKEN_MISMATCH_MESSAGE)) 
			{
				throw new CardTransactionException("API Token Mismatch");
			}

			throw new CardTransactionException("Moneris processor is offline: " + receipt.getMessage());
		}

		pd.setCardData(null);
		int responseCode = Integer.parseInt(receipt.getResponseCode());
		if (responseCode < 50) 
		{
			pd.setAuthorizationNumber(receipt.getAuthCode());
			pd.setIsApproved(true);
		} 
		else if (responseCode >= 50) 
		{
			pd.setIsApproved(false);
		}

		return (pd.isIsApproved());
	}
	
	
	public Object[] processCharge(Track2Card track2Card, ProcessorTransaction chargePtd) throws CardTransactionException 
	{
		String order_id = getCommonProcessingService().getNewReferenceNumber(getMerchantAccount());
		String card = track2Card.getPrimaryAccountNumber();
		String exp = track2Card.getExpirationYear() + track2Card.getExpirationMonth();
		String crypt = CRYPT_VALUE;
		Double amt = CardProcessingUtil.getCentsAmountInDollars(chargePtd.getAmount()).doubleValue();
	
		StringBuilder sb = new StringBuilder(100);
		sb.append("Charge Request:  Order ID: ");
		sb.append(order_id);
		sb.append(", Charge total: ");
		sb.append(amt.toString());
		logger__.info(sb.toString());
	
		
		JavaAPI.HttpsPostRequest mpgReq = new JavaAPI.HttpsPostRequest(destinationUrlString_, storeId, apiToken_,
				new JavaAPI.Purchase(order_id, amt.toString(), card, exp, crypt));

		JavaAPI.Receipt receipt = mpgReq.getReceipt();

		sb = new StringBuilder(100);
		sb.append("Charge Response:  Response Code: ");
		sb.append(receipt.getResponseCode());
		sb.append(", Response Message: ");
		sb.append(receipt.getMessage());
		sb.append(", Auth Code: ");
		sb.append(receipt.getAuthCode());
		sb.append(", ReferenceNumber: ");
		sb.append(order_id);
		sb.append(", Charge total: ");
		sb.append(amt.toString());
		logger__.info(sb.toString());	
		
		// Set ProcessorTransactionData values from processor
		chargePtd.setProcessingDate(new Date());
		chargePtd.setReferenceNumber(order_id);
		chargePtd.setProcessorTransactionId(receipt.getTxnNumber());

		String response_code_string = receipt.getResponseCode();

		if ((response_code_string == null) || (response_code_string.equalsIgnoreCase("NULL"))) 
		{
			if (receipt.getMessage().trim().equalsIgnoreCase(DUPLICATE_ORDER_ID_MESSAGE)) 
			{
				throw new CardTransactionException("Duplicate Reference number/order id");
			} 
			else if (receipt.getMessage().trim().equalsIgnoreCase(API_TOKEN_MISMATCH_MESSAGE)) 
			{
				throw new CardTransactionException("API Token Mismatch");
			} 
			else 
			{
				throw new CardTransactionException("Moneris Processor is Offline");
			}
		}

		int response_code = -1;
		try 
		{
			response_code = Integer.parseInt(response_code_string);
		} 
		catch (Exception e) {
			throw new CardTransactionException("Expecting numeric response, but recieved: " + response_code_string, e);
		}

		// Approved
		if (response_code < 50) 
		{
			chargePtd.setAuthorizationNumber(receipt.getAuthCode());
			chargePtd.setIsApproved(true);
			return new Object[] { DPTResponseCodesMapping.CC_APPROVED_VAL__7000, response_code };
		}

		// Declined
		chargePtd.setAuthorizationNumber(null);
		chargePtd.setIsApproved(false);
		return new Object[] {mapErrorResponse(response_code), response_code};
	}
	
	
	public Object[] processPostAuth(PreAuth pd, ProcessorTransaction ptd) throws CardTransactionException 
	{
		// Determine data to send to processor
		String order_id = pd.getReferenceNumber(); // referenceNumber; Moneris wants ORIGINAL ref number in
		// completions/post auths
		Double amt = CardProcessingUtil.getCentsAmountInDollars(pd.getAmount()).doubleValue();
		String amount = amt.toString();
		String txn_number = pd.getProcessorTransactionId();
		String crypt = CRYPT_VALUE;

		StringBuilder sb = new StringBuilder(100);
		sb.append("Settle Request:  Order ID: ");
		sb.append(order_id);
		sb.append(", Charge total: ");
		sb.append(amount);
		logger__.info(sb.toString());
				
		// Send request to Moneris
		JavaAPI.HttpsPostRequest mpgReq = new JavaAPI.HttpsPostRequest(destinationUrlString_, storeId, apiToken_,
				new JavaAPI.Completion(order_id, amount, txn_number, crypt));
		JavaAPI.Receipt receipt = mpgReq.getReceipt();

		String response_code_string = receipt.getResponseCode();

		sb = new StringBuilder(100);
		sb.append("Settle Response:  Response Code: ");
		sb.append(receipt.getResponseCode());
		sb.append(", Response Message: ");
		sb.append(receipt.getMessage());
		sb.append(", Auth Code: ");
		sb.append(receipt.getAuthCode());
		sb.append(", ReferenceNumber: ");
		sb.append(order_id);
		sb.append(", Charge total: ");
		sb.append(amount);
		logger__.info(sb.toString());
	
		
		// Processor is Offline
		if ((response_code_string == null) || (response_code_string.equalsIgnoreCase("NULL"))) {
			throw new CardTransactionException("Moneris Processor is Offline");
		}

		// Set ProcessorTransactionData values from processor
		ptd.setProcessingDate(new Date());
		ptd.setReferenceNumber(pd.getReferenceNumber());
		ptd.setProcessorTransactionId(receipt.getTxnNumber());

		int response_code = -1;
		try 
		{
			response_code = Integer.parseInt(response_code_string);
		} 
		catch (Exception e) 
		{
			throw new CardTransactionException("Expecting numeric response, but recieved: " + response_code_string, e);
		}

		// Approved
		if (response_code < 50) 
		{
			ptd.setAuthorizationNumber(receipt.getAuthCode());
			ptd.setIsApproved(true);
			return new Object[] { true, response_code };
		}

		// Declined
		ptd.setAuthorizationNumber(null);
		ptd.setIsApproved(false);
		return new Object[] {false, response_code};
	}
	
	
	public Object[] processRefund(boolean isNonEmsRequest, ProcessorTransaction origPtd,
			ProcessorTransaction refundPtd, String ccNumber, String expiryMMYY) throws CardTransactionException 
	{
		String order_id = origPtd.getReferenceNumber(); // referenceNumber; Moneris wants ORIGINAL ref number
		// in
		// completions/post auths
		Double amt = CardProcessingUtil.getCentsAmountInDollars(refundPtd.getAmount()).doubleValue();
		String amount = amt.toString();
		String txn_number = origPtd.getProcessorTransactionId();
		String crypt = CRYPT_VALUE;

		StringBuilder sb = new StringBuilder(100);
		sb.append("Refund Request:  Order ID: ");
		sb.append(order_id);
		sb.append(", Charge total: ");
		sb.append(amount);
		logger__.info(sb.toString());
		
		
		JavaAPI.HttpsPostRequest mpgReq = new JavaAPI.HttpsPostRequest(destinationUrlString_, storeId, apiToken_,
				new JavaAPI.Refund(order_id, amount, txn_number, crypt));

		JavaAPI.Receipt receipt = mpgReq.getReceipt();
		String response_code_string = receipt.getResponseCode();

		sb = new StringBuilder(100);
		sb.append("Refund Response:  Response Code: ");
		sb.append(receipt.getResponseCode());
		sb.append(", Response Message: ");
		sb.append(receipt.getMessage());
		sb.append(", Auth Code: ");
		sb.append(receipt.getAuthCode());
		sb.append(", ReferenceNumber: ");
		sb.append(order_id);
		sb.append(", Charge total: ");
		sb.append(amount);
		logger__.info(sb.toString());
	
	
		if ((response_code_string == null) || (response_code_string.equalsIgnoreCase("NULL"))) 
		{
			throw new CardTransactionException("Moneris Processor is offline");
		}

		// Set processor transaction data values
		refundPtd.setProcessingDate(new Date());
		refundPtd.setReferenceNumber(receipt.getReferenceNum());
		refundPtd.setProcessorTransactionId(receipt.getTxnNumber());

		int response_code = -1;
		try 
		{
			response_code = Integer.parseInt(response_code_string);
		} 
		catch (Exception e) 
		{
			throw new CardTransactionException("Expecting numeric response, but recieved: " + response_code_string, e);
		}

		// Approved
		if (response_code < 50) 
		{
			refundPtd.setAuthorizationNumber(receipt.getAuthCode());
			refundPtd.setIsApproved(true);
			return new Object[] { DPTResponseCodesMapping.CC_APPROVED_VAL__7000, response_code };
		}

		// Declined
		refundPtd.setAuthorizationNumber(null);
		refundPtd.setIsApproved(false);

		// if request is non-EMS (eg from BOSS) then map response.
		if (isNonEmsRequest) 
		{
			return new Object[] { mapErrorResponse(response_code), response_code };
		}

		// EMS Request
		return new Object[] {response_code, response_code};
	}
	
	
	private int mapErrorResponse(int responseCode) throws CardTransactionException 
	{
		switch (responseCode) 
		{
			// Invalid Exp. Date: Card has expired
			case 51:
				// Invalid expiration date
			case 208:
				// CREDIT CARD - Invalid expiration date
			case 475:
				// CREDIT CARD - Expired Card
			case 482:
				// CREDIT CARD - Expired card - refer
			case 484:
				// Capture - Expired Card
			case 901:
				return (DPTResponseCodesMapping.CC_EXPIRED_VAL__7001);

				// This transaction has been declined
			case 50:
				// Number of times used exceeded
			case 82:
				// CAF status inactive or closed
			case 89:
				// Number of times used exceeded
			case 110:
				// AMEX - Declined
			case 437:
				// AMEX - Declined
			case 438:
				// CREDIT CARD - Decline, Pick up card
			case 479:
				// CREDIT CARD - Decline, Pick up card
			case 480:
				// CREDIT CARD - Decline
			case 481:
				// CREDIT CARD - Not authorized
			case 485:
				// Capture - NEG Capture
			case 902:
				// Capture - CAF Status 3 , Card Restricted
			case 903:
				// Capture - Delinquent
			case 906:
				// Capture - Capture
			case 909:
				// Capture - Num Times Used
			case 905:
				return (DPTResponseCodesMapping.CC_DECLINED_VAL__7003);

				// PIN retries exceeded
			case 52:
				// PIN required
			case 96:
				// Administrative card needed
			case 203:
				// CREDIT CARD - CVV Cryptographic error
			case 486:
				// CREDIT CARD - Invalid CVV
			case 487:
				// CREDIT CARD - Invalid CVV
			case 489:
				// CREDIT CARD - Invalid CVV
			case 490:
				return (DPTResponseCodesMapping.CC_UNABLE_TO_AUTHENTICATE_VAL__7055);

				// No security module
			case 54:
				// PBF update error
			case 62:
				// Invalid authorization type
			case 63:
				// Invalid credit card advance increment
			case 66:
				// PTLF error
			case 68:
				// Card on National NEG file
			case 72:
				// Over floor limit
			case 86:
				// Referral file full
			case 90:
				// NEG file problem
			case 91:
				// Delinquent
			case 93:
				// Over table limit
			case 94:
				// Force Post
			case 98:
				// NEG file problem
			case 103:
				// CAF problem
			case 104:
				// CAF Problem
			case 108:
				// Delinquent
			case 111:
				// Over table limit
			case 112:
				// PTLF error
			case 115:
				// Administration file problem
			case 121:
				// Bad close
			case 809:
				// Bad response length
			case 821:
				return (DPTResponseCodesMapping.CC_CANNOT_PROCESS_PLEASE_RETRY_VAL__7004);

				// Unable to authorize
			case 74:
				// Timeout
			case 113:
				// Destination not available
			case 212:
				// AMEX - System down
			case 435:
				// System timeout
			case 810:
				// Terminal/Clerk table full
			case 970:
				throw new CardTransactionException("Response indicates processor offline: " + responseCode);

				// Lost or Stolen card
			case 57:
				return (DPTResponseCodesMapping.CC_LOST_STOLEN_VAL__7006);

				// Invalid transaction
				// CARD NOT SUPPORTED
			case 55:
				// No Support
			case 56:
				// No PBF, Card is not setup
			case 61:
				// Inquiry not allowed
			case 85:
				// CAF not found , Card is not setup
			case 206:
				// AMEX - Invalid merchant
			case 427:
				return (DPTResponseCodesMapping.CC_CARD_TYPE_NOT_SETUP_VAL__7076);

				// Invalid transaction date
			case 67:
				// Bad message error
			case 69:
				// Invalid transaction date
			case 207:
				// Invalid transaction code
			case 209:
				// Debit not supported
			case 252:
				// Credit Card - invalid transaction rejected
			case 476:
				// Bad format
			case 800:
				// MAC key sync error
			case 889:
				// Bad MAC value
			case 898:
				// Bad sequence number - resend transaction
			case 899:
				return (DPTResponseCodesMapping.CC_INVALID_REQUEST_VAL__7058);

				// Bad Track 2
			case 64:
				// No IDF
			case 70:
				// Invalid route authorization
			case 71:
				// Invalid route service (destination)
			case 73:
				// Invalid PAN length
			case 75:
				// Mod 10 check failure
			case 97:
				// Bad PBF
			case 99:
				// Card not support
			case 105:
				// Invalid account
			case 200:
				// Bad data
			case 801:
				return (DPTResponseCodesMapping.CC_BAD_CREDIT_CARD_DATA_VAL__7045);

				// Adjustment not allowed
			case 65:
				// Unable to process transaction
			case 100:
				return (DPTResponseCodesMapping.CC_UNABLE_TO_PROCESS_VAL__7084);

				// Low funds
			case 76:
				// Capture - Over Limit Table
			case 907:
				return (DPTResponseCodesMapping.CC_INSUFFICIENT_FUNDS_VAL__7017);

				// Pre-auth full
			case 77:
				// Maximum online refund reached
			case 79:
				// Maximum offline refund reached
			case 80:
				// Maximum credit per refund reached
			case 81:
				// Maximum refund credit reached
			case 83:
				// Maximum number of refund credit by retailer
			case 87:
				// Advance less than minimum
			case 92:
				// Advance less than minimum
			case 109:
				// Advance less than minimum
			case 202:
				// Invalid Advance amount
			case 205:
				// Error on cash amount
			case 251:
				// AMEX - Amount error
			case 441:
				// Capture - Advance < Minimum
			case 904:
				// Capture - Amount Over Maximum
			case 908:
				return (DPTResponseCodesMapping.CC_INVALID_AMOUNT_VAL__7033);

				// Duplicate transaction
			case 78:
				// Duplicate transaction - authorization number has already been corrected by host.
			case 84:
				return (DPTResponseCodesMapping.CC_DUPLICATE_TRANS_VAL__7019);

				// Invalid status
				// CARD USE LIMITED REFER TO BRANCH
			case 58:
				// Restricted Card
				// CARD USE LIMITED REFER TO BRANCH
			case 59:
				// Place call
			case 88:
				// Place call
			case 101:
				// Place call
			case 102:
				// AMEX - Denial 12
			case 426:
				// AMEX - Call Amex
			case 431:
				// AMEX - Call 03
			case 434:
				// AMEX - Call 05
			case 436:
				// AMEX - Call Amex
			case 440:
				// CREDIT CARD - Refer Call
			case 477:
				// CREDIT CARD - Decline, Pick up card, Call
			case 478:
				// CREDIT CARD - Refer
			case 483:
				return (DPTResponseCodesMapping.CC_PLACE_CALL_VAL__7022);

				// Amount over maximum
			case 95:
				// Amount over maximum
			case 106:
				// Amount over maximum
			case 204:
				return (DPTResponseCodesMapping.CC_AMOUNT_OVER_MAXIMUM_VAL__7028);

				// Over daily limit
			case 107:
				return (DPTResponseCodesMapping.CC_OVER_DAILY_LIMIT_VAL__7029);

				// Merchant not on file
			case 150:
				throw new CardTransactionException("Response indicates invalid merchant: " + responseCode);

				// Invalid software version number
			case 965:
				throw new CardTransactionException("Response indicates invalid software version number: "
						+ responseCode);

				// Duplicate terminal name
			case 966:
				throw new CardTransactionException("Response indicates duplicate terminal name: "
						+ responseCode);

				// AMEX - Service error
			case 439:
				// System error
			case 811:
				return (DPTResponseCodesMapping.CC_SYSTEM_ERROR_VAL__7047);

				// AMEX - Account error
			case 429:
				return (DPTResponseCodesMapping.CC_INVALID_ACCOUNT_VAL__7088);

			default:
				throw new CardTransactionException("Unknown Response Code: " + responseCode);
		}
	}

	@Override
	public void processReversal(Reversal reversal, Track2Card track2Card) 
	{
		throw new UnsupportedOperationException("MonerisProcessor, Reversal not supported for this processor");
	}
	
	@Override
	public boolean isReversalExpired(Reversal reversal) {
		throw new UnsupportedOperationException("MonerisProcessor, isReversalExpired(Reversal) is not support!");
	}
}
