package com.digitalpaytech.cardprocessing.impl;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Security;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.Logger;
import org.apache.commons.lang.StringUtils;
import com.digitalpaytech.cardprocessing.CardProcessor;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.WebCoreUtil;

public class BlackBoardProcessor extends CardProcessor {
    private final static Logger logger__ = Logger.getLogger(BlackBoardProcessor.class);
    // private final static Logger cc_logger__ = Logger.getLogger(DummyLogger.class);
    private final static String CURRENCY_STRING_USD = "USD";
    private final static String MANUAL_ID_BOOLEAN_STRING = "F";
    private final static String ONLINE_FLAG_STRING = "T";
    private final static String BLACKBOARD_VERSION_NUMBER = "2";
    private final static String PIN_VALUE = "0";
    private final static String DELIMITER = "~";
    private final static String LIMIT_STRING = "Limit";
    private final static String DEFAULT_VENDOR_NUMBER = "0165"; // vendor number provided by BlackBoard
    private final static String AES_CBC_NOPADDING = "AES/CBC/NoPadding";
    private final static String BOUNCY_CASTLE_PROVIDER = "BC";
    private final static String DEFAULT_TENDER_NUMBER = "0001";
    private final static String ENCRYPTION_WITH_CRC = "1";
    private final static String NO_ENCRYPTION_WITH_LRC = "0";
    private final static int BLOCK_SIZE = 16;
    private final static int ENCRYPT = 1;
    private final static int DECRYPT = 2;
    private final static int KEY_SIZE = 32;
    private final static int DEFAULT_SERVER_PORT = 9003;
    private final static int DEBIT_MESSAGE_TYPE = 1;
    private final static int AUTHORIZATION_LIMIT_MESSAGE_TYPE = 7;
    private final static int REFUND_MESSAGE_TYPE = 5;
    /* Test & prod server urls are in database Processor table.
     * private final static String TEST_SERVER_1 = "66.210.59.124";
     * private final static String TEST_SERVER_2 = "69.26.224.124";
     */
    
    private byte[] encrytion_decryption_key;
    private String iPAddress;
    private String tenderNumber;
    private String vendorNumber;
    private String encryption_error_checking_value;
    private String terminalNumber; // using the paystation's comm address as terminal number for bb
    // transactions

    private SecretKeySpec skeySpec;

    // response fields..same fields can be used for different request types
    private String response_message_length;
    private String reponse_encryption_error_check;
    private String response_vendor_number;
    private String response_terminal_number;
    private String response_encrypted_length;
    private String response_version_number;
    private String response_message_type;
    private String response_sequence_number;
    private String response_response_code;
    private String response_display_text;
    private String response_crc;

    
    public BlackBoardProcessor(MerchantAccount md, CommonProcessingService commonProcessingService,
            EmsPropertiesService emsPropertiesService, PointOfSale pointOfSale) {
    
        super(md, commonProcessingService, emsPropertiesService);
        try
        {
            logger__.debug("Creating BlackBoard object");

            String key = md.getField1();
            iPAddress = md.getField2();

            // BlackBoardProcessor processor has 2 urls separated by comma, e.g. '66.210.59.124,69.26.224.124'
            String testServerUrl1 = "";
            String testServerUrl2 = "";
            String urlStr = commonProcessingService.getDestinationUrlString(md.getProcessor());
            if (StringUtils.isNotBlank(urlStr)) {
                String[] urls = urlStr.split(",");
                testServerUrl1 = WebCoreUtil.returnEmptyIfBlank(urls[0]).trim();
                if (urls.length > 1) {
                    testServerUrl2 = WebCoreUtil.returnEmptyIfBlank(urls[1]).trim();
                }
            }

            
            // if key size is not the expected size (32) then no encryption is used
            if (key.length() != KEY_SIZE)
            {
                encryption_error_checking_value = NO_ENCRYPTION_WITH_LRC;
                if (iPAddress.equals(testServerUrl1) || iPAddress.equals(testServerUrl2))
                {
                    terminalNumber = "00000002";
                }
                else
                {
                    String comm_address = pointOfSale.getSerialNumber();
                    terminalNumber = comm_address.substring(comm_address.length() - 8);
                }
            }
            else
            {
                encryption_error_checking_value = ENCRYPTION_WITH_CRC;
                encrytion_decryption_key = hexStringToBytes(key);
                if (iPAddress.equals(testServerUrl1) || iPAddress.equals(testServerUrl2))
                {
                    terminalNumber = "00000003";
                }
                else
                {
                    String comm_address = pointOfSale.getSerialNumber();
                    terminalNumber = comm_address.substring(comm_address.length() - 8);
                }
            }

            tenderNumber = md.getField3();
            if (tenderNumber == null || tenderNumber.equals(""))
            {
                tenderNumber = DEFAULT_TENDER_NUMBER;
            }

            vendorNumber = md.getField4();
            if (vendorNumber == null || vendorNumber.equals(""))
            {
                vendorNumber = DEFAULT_VENDOR_NUMBER;
            }

            // check if port number is contained inside ipaddress
            StringTokenizer stk = new StringTokenizer(iPAddress, ":");
            iPAddress = stk.nextToken();

            // Security initializations
            Provider bc = new org.bouncycastle.jce.provider.BouncyCastleProvider();
            Security.addProvider(bc);
            if (encryption_error_checking_value.equals(ENCRYPTION_WITH_CRC))
            {
                skeySpec = new SecretKeySpec(encrytion_decryption_key, "AES/CBC/NoPadding");
            }
        }
        catch (Exception e)
        {
            throw new CardTransactionException("Could not initialize BlackBoard Processor!", e);
        }
        
    }
    
    
    
    public boolean processPreAuth(PreAuth pd) throws CardTransactionException
    {
        logger__.debug("Performing Pre-Auth for BB transaction");
        
        pd.setMerchantAccount(getMerchantAccount());
        // try posting the pre auth message a maximum of 3 times
        int trying = 1;
        while (trying < 4)
        {
            logger__.info("Posting pre-auth, try # " + trying);
            try
            {
                createAndSendRequest(AUTHORIZATION_LIMIT_MESSAGE_TYPE, getCommonProcessingService().getNewReferenceNumber(getMerchantAccount()),
                        pd.getAmount(), pd.getCardData(), 0);
                break;
            }
            catch (CardTransactionException e)
            {
                if (trying == 3)
                {
                    throw e;
                }
            }
            trying++;
        }

        if ((response_response_code == null) || (response_response_code.equalsIgnoreCase("NULL")))
        {
            throw new CardTransactionException("BlackBoard Processor is Offline");
        }

        pd.setProcessingDate(new Date());
        pd.setAuthorizationNumber(response_sequence_number);
        pd.setReferenceNumber(response_sequence_number);
        pd.setResponseCode(response_response_code);

        int responseCode = new Integer(response_response_code).intValue();

        // the amount available in this account is returned in the display text field. we need to parse it and
        // compare that value to the transaction amount
        // for BB pre-auth, good response code does not necessarily mean that the transaction amount is
        // available.
        if (responseCode < 100)
        {
            StringTokenizer stk = new StringTokenizer(response_display_text, " ");
            String limit = stk.nextToken();
            // limit variable should contain string "LIMIT"
            if (limit.equalsIgnoreCase(LIMIT_STRING))
            {
                String balance = stk.nextToken();
                if (Float.parseFloat(balance) >= CardProcessingUtil.getCentsAmountInDollars(pd.getAmount()).floatValue())
                {
                    pd.setIsApproved(true);
                }
                else
                {
                    pd.setIsApproved(false);
                }
            }
        }
        else if (responseCode >= 100)
        {
            pd.setIsApproved(false);

            // Check if response code between 100 and 199, and display following error
            if (responseCode <= 199)
            {
                logger__.error("Message to BlackBoard server not correctly formatted. Error code : " + responseCode);
                logger__.error("For details see page 28 BlackBoard TIA Developer Guide - Version 2 Protocol");
            }
        }

        return (pd.isIsApproved());
    }

    public Object[] processPostAuth(PreAuth pd, ProcessorTransaction ptd) throws CardTransactionException
    {
        // Create and send request
        int trying = 1;
        while (trying < 4)
        {
            logger__.info("Posting post-auth, try # " + trying);

            try
            {
                createAndSendRequest(DEBIT_MESSAGE_TYPE, getCommonProcessingService().getNewReferenceNumber(getMerchantAccount()), pd.getAmount(), pd
                        .getCardData(), 0);
                break;
            }
            catch (CardTransactionException e)
            {
                if (trying == 3)
                {
                    throw e;
                }
            }
            trying++;
        }

        if ((response_response_code == null) || (response_response_code.equalsIgnoreCase("NULL")))
        {
            throw new CardTransactionException("BlackBoard Processor is offline");
        }

        // Set ProcessorTransactionData values from processor
        ptd.setProcessingDate(new Date());
        ptd.setReferenceNumber(response_sequence_number);
        ptd.setProcessorTransactionId(pd.getProcessorTransactionId());
        ptd.setAuthorizationNumber(pd.getAuthorizationNumber());

        // Check approval status
        int responseCode = new Integer(response_response_code).intValue();
        ptd.setIsApproved(responseCode <= 99);
        if (responseCode >= 100 && responseCode <= 199)
        {
            logger__.error("Message to BlackBoard server incorrectly formatted. Error code : " + responseCode);
            logger__.error("For details see page 28 BlackBoard TIA Developer Guide - Version 2 Protocol");
        }
        return new Object[] {ptd.isIsApproved(), responseCode};
    }

    // this function pads the encrypted part of the message so that the message is a multiple of 16 bytes long
    // (as required by AES encryption standards)
    private StringBuffer padMessage(StringBuffer msg)
    {
        int len = msg.length();
        int a = len / BLOCK_SIZE;
        int b = a + 1;
        int charactersToPad = (BLOCK_SIZE * b) - len;

        for (int i = 0; i < charactersToPad - 1; i++)
        {
            msg.append("0");
        }
        msg.append("~");
        return msg;
    }

    private static byte[] hexStringToBytes(String sHexString)
    {
        byte[] output = new byte[sHexString.length() / 2];
        int j = 0;
        for (int i = 0; i < sHexString.length(); i = i + 2)
        {
            output[j] = (byte)(Byte.parseByte(sHexString.substring(i, i + 1), 16) << 4);
            output[j] = (byte)(output[j] | (Byte.parseByte(sHexString.substring(i + 1, i + 2), 16)));
            j++;
        }

        return output;
    }

    // private function to actually send the request to the BlackBoard server
    // returns a String which contains the response sent by the server
    private byte[] sendToBlackBoardServer(byte[] request) throws CardTransactionException
    {

        logger__.debug("Request : " + new String(request) + " \n");

        byte[] out_data = new byte[305];
        byte[] exact_out_data = null;
        try
        {
            Socket socket = new Socket(iPAddress, DEFAULT_SERVER_PORT);
            PrintStream out = new PrintStream(socket.getOutputStream(), false);
            out.write(request, 0, request.length);
            out.flush();
            // BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            DataInputStream in = new DataInputStream(socket.getInputStream());
            int bytesRead = in.read(out_data);
            exact_out_data = new byte[bytesRead];
            for (int i = 0; i < bytesRead; i++)
            {
                exact_out_data[i] = out_data[i];
            }
            logger__.info("\nRead " + bytesRead + " bytes");
            out.close();
            in.close();
        }
        catch (IOException e)
        {
            throw new CardTransactionException("Blackboard server: " + iPAddress + " is offline!", e);
        }

        return exact_out_data;
    }

    // extracts the responses from the bytes message received from the server for the request posted
    private void extractResponses(byte[] res) throws CardTransactionException
    {

        String response = new String(res);
        logger__.info("The response \n" + response + "\n!");
        StringTokenizer stk = new StringTokenizer(response, "~");

        int lengthSoFar = 0;

        if (stk.hasMoreTokens())
        {
            response_message_length = stk.nextToken();
        }
        lengthSoFar += response_message_length.length() + 1; // +1 for the delimiter

        // get rid of trailing spaces
        stk = new StringTokenizer(response, "~");
        stk.nextToken();

        if (stk.hasMoreTokens())
        {
            reponse_encryption_error_check = stk.nextToken();
        }
        lengthSoFar += reponse_encryption_error_check.length() + 1; // +1 for the delimiter

        if (stk.hasMoreTokens())
        {
            response_vendor_number = stk.nextToken();
        }
        lengthSoFar += response_vendor_number.length() + 1; // +1 for the delimiter

        if (stk.hasMoreTokens())
        {
            response_terminal_number = stk.nextToken();
        }
        lengthSoFar += response_terminal_number.length() + 1; // +1 for the delimiter

        if (stk.hasMoreTokens())
        {
            response_encrypted_length = stk.nextToken();
        }
        lengthSoFar += response_encrypted_length.length() + 1; // +1 for the delimiter

        int enc_error_check_val = (new Integer(reponse_encryption_error_check)).intValue();
        byte[] fields100To117 = null; // entire response without the CRC field
        byte[] fields105To117 = null; // the encrypted part of the response
        byte[] field118 = null; // the crc field of the response
        int index = 0; // this is where field 105 (encrypted or not) start

        if (enc_error_check_val == 1)
        {
            fields100To117 = new byte[(res.length - 2)];
            fields105To117 = new byte[(res.length - lengthSoFar - 2)];
            field118 = new byte[2];
        }
        else if (enc_error_check_val == 0)
        {
            fields100To117 = new byte[(res.length - 1)];
            fields105To117 = new byte[(res.length - lengthSoFar - 1)];
            field118 = new byte[1];
        }

        // get entire response without the CRC field
        for (int i = 0; i < fields100To117.length; i++)
        {
            fields100To117[i] = res[index];
            index++;
        }

        index = lengthSoFar;
        // get the encrypted part of the response
        for (int i = 0; i < fields105To117.length; i++)
        {
            fields105To117[i] = res[index];
            index++;
        }
        // get crc part of response
        for (int i = 0; i < field118.length; i++)
        {
            field118[i] = res[index];
            index++;
        }

        response_crc = new String(field118);

        // if encryption flag is set in response then do a CRC check
        if (enc_error_check_val == 1)
        {
            byte[] calcCRC = CardProcessingUtil.computeCRC16(fields100To117);

            logger__.info("Calculated CRC is " + calcCRC[0] + calcCRC[1]);

            if (!(calcCRC[0] == field118[0] && calcCRC[1] == field118[1]))
            {
                throw new CardTransactionException("Calculated CRC did not match expected CRC in response");
            }
        }
        // if encryption flag is not set in response then do a LRC check
        else if (enc_error_check_val == 0)
        {
            byte[] calcLRC = CardProcessingUtil.computeLRC(fields100To117);

            logger__.info("Calculated LRC is " + calcLRC[0]);

            if (!(calcLRC[0] == field118[0]))
            {
                throw new CardTransactionException("Calculated LRC did not match expected LRC in response");
            }
        }

        String restOfResponseWithout_CRC_LRC = new String(fields105To117);
        logger__.info("Response without crc_lrc: " + restOfResponseWithout_CRC_LRC);

        // if the enc flag in the response is set then we need to decrypt it first
        if (enc_error_check_val == 1)
        {
            byte[] decrypted = null;
            try
            {
                decrypted = encrypt_decrypt(fields105To117, DECRYPT);
            }
            catch (CardTransactionException ae)
            {
                logger__.info("Could not decrypt BlackBoard response");
                throw ae;
            }

            restOfResponseWithout_CRC_LRC = new String(decrypted);
            logger__.info("Decrypted : " + restOfResponseWithout_CRC_LRC);
        }

        // now need to tokenize the decrypted response
        stk = new StringTokenizer(restOfResponseWithout_CRC_LRC, "~");

        if (stk.hasMoreTokens())
        {
            response_version_number = stk.nextToken();
        }
        if (stk.hasMoreTokens())
        {
            response_message_type = stk.nextToken();
        }
        if (stk.hasMoreTokens())
        {
            response_sequence_number = stk.nextToken();
        }
        if (stk.hasMoreTokens())
        {
            response_response_code = stk.nextToken();
        }
        if (stk.hasMoreTokens())
        {
            response_display_text = stk.nextToken();
        }

        if (logger__.isInfoEnabled())
        {
            StringBuffer sb = new StringBuffer();
            sb.append("Len : " + response_message_length);
            sb.append(", Enc : " + reponse_encryption_error_check);
            sb.append(", Ven# : " + response_vendor_number);
            sb.append(", terminal# : " + response_terminal_number);
            sb.append(", enc len : " + response_encrypted_length);
            sb.append(", encrypted part of response : " + new String(fields105To117) + " with size "
                    + fields105To117.length);
            sb.append(", crc : " + response_crc);
            sb.append(", Error enc val : " + enc_error_check_val);
            sb.append(", Version # : " + response_version_number);
            sb.append(", Message type : " + response_message_type);
            sb.append(", sequence # : " + response_sequence_number);
            sb.append(", response code : " + response_response_code);
            sb.append(", display text : " + response_display_text);

            logger__.info(sb.toString());
        }
    }

    private byte[] encrypt_decrypt(byte[] msg, int mode) throws CardTransactionException
    {
        byte[] result = null;

        try
        {
            Cipher cipher = Cipher.getInstance(AES_CBC_NOPADDING, BOUNCY_CASTLE_PROVIDER);
            if (mode == ENCRYPT)
            {
                cipher.init(Cipher.ENCRYPT_MODE, skeySpec, new IvParameterSpec(new byte[16]));
            }
            else if (mode == DECRYPT)
            {
                cipher.init(Cipher.DECRYPT_MODE, skeySpec, new IvParameterSpec(new byte[16]));
            }

            logger__.info("Provider name " + cipher.getProvider().getName());

            result = cipher.doFinal(msg);
        }
        catch (NoSuchAlgorithmException ne)
        {
            logger__
                    .error(AES_CBC_NOPADDING + " algorithm could not be found during encryption/decryption process");
            ne.printStackTrace();
            throw new CardTransactionException(ne);
        }
        catch (NoSuchPaddingException ne)
        {
            logger__.error("Padding error during encryption/decryption process");
            ne.printStackTrace();
            throw new CardTransactionException(ne);
        }
        catch (InvalidKeyException ie)
        {
            logger__.error("Invalid key encountered during encryption/decryption process");
            ie.printStackTrace();
            throw new CardTransactionException(ie);
        }
        catch (InvalidAlgorithmParameterException ie)
        {
            logger__.error("Invalid algorithm parameter encountered during encryption/decryption process");
            ie.printStackTrace();
            throw new CardTransactionException(ie);
        }
        catch (BadPaddingException ne)
        {
            logger__.error("Bad padding encountered during encryption/decryption process");
            throw new CardTransactionException(ne);
        }
        catch (NoSuchProviderException npe)
        {
            logger__.error("Security provider not found during encryption/decryption process");
            npe.printStackTrace();
            throw new CardTransactionException(npe);
        }
        catch (IllegalBlockSizeException ibe)
        {
            logger__.error("Illegal block size during encryption/decryption process");
            ibe.printStackTrace();
            throw new CardTransactionException(ibe);
        }

        return result;
    }

    private void createAndSendRequest(int requestType, String referenceNumber, int chargeAmountInCents,
            String cardData, int refundAmountInCents) throws CardTransactionException
    {
        String sequenceNumber = referenceNumber.substring(referenceNumber.length() - 6, referenceNumber.length());
        DateFormat date_format = new SimpleDateFormat("yyyyMMddHHmmss");
        String date = date_format.format(new Date());
        String amount = null;
        String track2Data = cardData;

        switch (requestType)
        {
            case AUTHORIZATION_LIMIT_MESSAGE_TYPE:
            {
                // int amount_value = new Float(requestDataHandler.getChargeAmount()).intValue() * 100;
                // amount = new Integer(amount_value).toString();
                amount = "0";
                break;
            }

            case DEBIT_MESSAGE_TYPE:
            {
                amount = Integer.toString(chargeAmountInCents);
                break;
            }

            case REFUND_MESSAGE_TYPE:
            {
                amount = Integer.toString(refundAmountInCents);
                break;
            }

        } // end switch

        String type = new Integer(requestType).toString();

        StringBuffer messageForEncryption = new StringBuffer();
        messageForEncryption.append(BLACKBOARD_VERSION_NUMBER);
        messageForEncryption.append(DELIMITER + type);
        messageForEncryption.append(DELIMITER + sequenceNumber);
        messageForEncryption.append(DELIMITER + tenderNumber);
        messageForEncryption.append(DELIMITER + date);
        messageForEncryption.append(DELIMITER + ONLINE_FLAG_STRING);
        messageForEncryption.append(DELIMITER + amount);
        messageForEncryption.append(DELIMITER + CURRENCY_STRING_USD);
        messageForEncryption.append(DELIMITER + MANUAL_ID_BOOLEAN_STRING);
        messageForEncryption.append(DELIMITER + track2Data);
        messageForEncryption.append(DELIMITER + PIN_VALUE + DELIMITER);
        messageForEncryption.append(DELIMITER); // skipping cash equivalency field

        String encrypted_length = (new Integer(messageForEncryption.length())).toString();

        messageForEncryption = padMessage(messageForEncryption);

        logger__.info("Message to be encrypted:\n" + messageForEncryption.toString() + "\n");

        byte[] encrypted = null;
        if (encryption_error_checking_value.equals(ENCRYPTION_WITH_CRC))
        {
            encrypted = encrypt_decrypt(messageForEncryption.toString().getBytes(), ENCRYPT);
            logger__.info("Encrypted String : " + new String(encrypted) + "\n size : " + encrypted.length);
        }

        StringBuffer messageToSend = new StringBuffer();
        messageToSend.append(DELIMITER + encryption_error_checking_value);
        messageToSend.append(DELIMITER + vendorNumber);
        messageToSend.append(DELIMITER + terminalNumber);
        messageToSend.append(DELIMITER + encrypted_length + DELIMITER);

        byte[] fields101To104 = messageToSend.toString().getBytes();
        byte[] fields105To117 = null;

        if (encryption_error_checking_value.equals(ENCRYPTION_WITH_CRC))
        {
            fields105To117 = encrypted;
        }
        if (encryption_error_checking_value.equals(NO_ENCRYPTION_WITH_LRC))
        {
            fields105To117 = messageForEncryption.toString().getBytes();
        }

        int length = 0;
        length = fields101To104.length + fields105To117.length + 4; // added 4 to include length of 'message
        // length' field

        if (encryption_error_checking_value.equals(ENCRYPTION_WITH_CRC))
        {
            length += 2;
        }
        if (encryption_error_checking_value.equals(NO_ENCRYPTION_WITH_LRC))
        {
            length += 1;
        }

        String length_string = new String(new Integer(length).toString());

        StringBuffer messageLengthField = new StringBuffer();
        // padding the 'message length' field with 0..so that length of 'message length' field is always 4
        for (int i = 0; i < (4 - length_string.length()); i++)
        {
            messageLengthField.append("0");
        }
        messageLengthField.append(length);

        byte[] field100 = messageLengthField.toString().getBytes();
        int length100To117 = field100.length + fields101To104.length + fields105To117.length;
        byte[] fields100To117 = new byte[length100To117];
        int index = 0;

        // System.out.println("field 100 : " + new String(field100));
        // System.out.println("field 101 to 104 : " + new String(fields101To104));
        // System.out.println("field 105 to 117 : " + new String(fields105To117));
        //    
        for (int i = 0; i < field100.length; i++)
        {
            fields100To117[index] = field100[i];
            index++;
        }
        for (int i = 0; i < fields101To104.length; i++)
        {
            fields100To117[index] = fields101To104[i];
            index++;
        }
        for (int i = 0; i < fields105To117.length; i++)
        {
            fields100To117[index] = fields105To117[i];
            index++;
        }

        // System.out.println("field 100 to 117 : " + new String(fields100To117));

        byte[] entireMessage = null;

        // get the CRC for the message and append to the end of the request
        byte[] crc_lrc = null;
        if (encryption_error_checking_value.equals(ENCRYPTION_WITH_CRC))
        {
            crc_lrc = CardProcessingUtil.computeCRC16(fields100To117);
            entireMessage = new byte[(fields100To117.length + 2)];
        }
        if (encryption_error_checking_value.equals(NO_ENCRYPTION_WITH_LRC))
        {
            crc_lrc = CardProcessingUtil.computeLRC(fields100To117);
            entireMessage = new byte[(fields100To117.length + 1)];
        }

        // System.out.println("CRC/LRC : " + new String(crc_lrc));

        index = 0;
        for (int i = 0; i < fields100To117.length; i++)
        {
            entireMessage[index] = fields100To117[i];
            index++;
        }
        for (int i = 0; i < crc_lrc.length; i++)
        {
            entireMessage[index] = crc_lrc[i];
            index++;
        }

        // System.out.println("Size " + entireMessage.length + " " + index);

        byte[] serverResponse = sendToBlackBoardServer(entireMessage);

        extractResponses(serverResponse);
    }
    
    
    @Override
    public void processReversal(Reversal reversal, Track2Card track2Card) {
        throw new UnsupportedOperationException("BlackBoardProcessor, do not support reversal.");
    }
    
    @Override
    public Object[] processRefund(boolean isNonEmsRequest, 
               ProcessorTransaction origProcessorTransaction, 
               ProcessorTransaction refundProcessorTransaction, 
               String creditCardNumber, 
               String expiryMMYY) {
        throw new UnsupportedOperationException("BlackBoardProcessor, do not support refund.");
    }
    
    @Override
    public Object[] processCharge(Track2Card track2Card, ProcessorTransaction chargeProcessorTransaction) {
        throw new UnsupportedOperationException("BlackBoardProcessor, do not support charge.");
    }

    @Override
    public boolean isReversalExpired(Reversal reversal) {
        throw new UnsupportedOperationException("BlackBoardProcessor, isReversalExpired(Reversal) is not support!");
    }
}
