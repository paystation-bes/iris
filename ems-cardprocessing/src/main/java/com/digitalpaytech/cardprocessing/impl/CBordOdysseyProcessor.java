package com.digitalpaytech.cardprocessing.impl;

import org.apache.log4j.Logger;

import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.EmsPropertiesService;

public class CBordOdysseyProcessor extends CBordProcessor {

	private final static Logger logger = Logger
			.getLogger(CBordOdysseyProcessor.class);

	public CBordOdysseyProcessor(final MerchantAccount merchantAccount,
			final CommonProcessingService commonProcessingService,
			final EmsPropertiesService emsPropertiesService,
			final PointOfSale pointOfSale, final String timeZone) {
		super(merchantAccount, commonProcessingService, emsPropertiesService,
				pointOfSale, timeZone);

		currentProcessor = "Odyssey";

	}

}
