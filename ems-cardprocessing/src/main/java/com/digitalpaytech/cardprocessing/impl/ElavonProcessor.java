package com.digitalpaytech.cardprocessing.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;

import com.digitalpaytech.cardprocessing.CardProcessor;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.exception.XMLErrorException;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.CreditCardTestNumberService;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DPTResponseCodesMapping;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.xml.XMLElavonBuilder;
import com.digitalpaytech.util.xml.XMLElavonResponseHandler;
import com.digitalpaytech.util.xml.XMLUtil;

public class ElavonProcessor extends CardProcessor {
    
    private static final Logger LOG = Logger.getLogger(ElavonProcessor.class);
    
    private static final String DBA_DATE_FORMAT = "yyyy-MMM-dd";
    private static final String DBA_DATE_FORMAT_12 = "ddMMM"; // used when dynamic dba prefix is 12 characters - saves space for lot name
    private static final String DEFAULT_LOT_NAME = " ";
    
    private static final String ERROR_INVALID_TRANSACTION_ID = "5040";
    private static final int DEFAULT_CONNECTION_TIMEOUT = 5000;
    private static final int DEFAULT_DECLINE_AMOUNT = 121;
    private static final String RESPONSE_CODE_APPROVED_STRING = "0";
    private static final String PARTIAL_AUTH_DECLINED_STRING = "Not approved for full amount - reversed by EMS";
    private static final String INVALID_MSG = "This Merchant Account is Invalid, reason is ";
    
    private String merchantId;
    private String userId;
    private String pin;
    private String destinationUrlString;
    private int dbaPrefixLength;
    private int connectionTimeout;
    
    private CreditCardTestNumberService testCreditCardNumberService;
    
    public ElavonProcessor(final MerchantAccount merchantAccount, final CommonProcessingService commonProcessingService,
            final EmsPropertiesService emsPropertiesService, final CustomerCardTypeService customerCardTypeService,
            final MerchantAccountService merchantAccountService, final CreditCardTestNumberService testCreditCardNumberService) {
        super(merchantAccount, commonProcessingService, emsPropertiesService);
        setCustomerCardTypeService(customerCardTypeService);
        setMerchantAccountService(merchantAccountService);
        this.testCreditCardNumberService = testCreditCardNumberService;
        this.merchantId = merchantAccount.getField1();
        this.userId = merchantAccount.getField2();
        this.pin = merchantAccount.getField3();
        
        if (merchantAccount.getField4() == null) {
            this.dbaPrefixLength = 0;
        } else {
            try {
                this.dbaPrefixLength = Integer.parseInt(merchantAccount.getField4());
            } catch (NumberFormatException e) {
                this.dbaPrefixLength = 0;
            }
        }
        
        this.destinationUrlString = commonProcessingService.getDestinationUrlString(merchantAccount.getProcessor());
        LOG.info("ElavonProcessor, destinationUrlString: " + destinationUrlString);
        try {
            this.connectionTimeout = emsPropertiesService.getPropertyValueAsInt("ElavonConnectionTimeout", DEFAULT_CONNECTION_TIMEOUT);
        } catch (Exception e) {
            this.connectionTimeout = DEFAULT_CONNECTION_TIMEOUT;
        }
    }
    
    private XMLElavonBuilder initializeXmlBuilder() {
        final XMLElavonBuilder elavonXML = new XMLElavonBuilder();
        elavonXML.addCredentials(merchantId, userId, pin);
        return elavonXML;
    }
    
    @Override
    public boolean processPreAuth(PreAuth preAuth) {
        // LOG.info("Elavon processing preAuth");
        String dba = null;
        if (dbaPrefixLength != 0) {
            dba = createDynamicDba(preAuth.getPointOfSale());
        }
        if (LOG.isDebugEnabled()) {
            if (dba == null) {
                LOG.debug(" \n DYNAMIC DBA not configured");
            } else {
                LOG.debug(" \n DYNAMIC DBA = " + dba);
            }
        }
        XMLElavonBuilder elavonXML = initializeXmlBuilder();
        float amount = CardProcessingUtil.getCentsAmountInDollars(preAuth.getAmount()).floatValue();
        elavonXML.createPreAuthRequest(preAuth.getCardData(), amount, dba);
        String responseString = sendToElavonServer(elavonXML.toString());
        preAuth.setProcessingDate(new Date());
        responseString = removeCardDataFromXml(responseString);
        
        LOG.info("Elavon server responding: \n" + responseString);
        
        XMLElavonResponseHandler responseHandler = new XMLElavonResponseHandler();
        responseHandler.process(responseString);
        ElavonTxReplyHandler reply = responseHandler.getCurrentDataHandler();
        String responseCode = reply.getResult();
        preAuth.setProcessorTransactionId(reply.getTxnId());
        preAuth.setResponseCode(responseCode);
        // Determine from field "balance due" whether the transaction was
        // authorized for less than the full amount.
        // If not authorized for full amount, reverse the transaction by sending
        // a ccdelete request to Elavon.
        boolean partiallyAuthorized = true;
        if (StringUtils.isBlank(reply.getBalanceDue())) {
            partiallyAuthorized = false;
        }
        preAuth.setReferenceNumber(getCommonProcessingService().getNewReferenceNumber(preAuth.getMerchantAccount()));
        if (RESPONSE_CODE_APPROVED_STRING.equals(responseCode)) {
            if (partiallyAuthorized) {
                // LOG.debug("Elavon transaction authorized for partial amount - generating reversal");
                createAndSendReversal(preAuth);
                
                preAuth.setIsApproved(false);
                preAuth.setAuthorizationNumber(reply.getApprovalCode());
                preAuth.setResponseCode(PARTIAL_AUTH_DECLINED_STRING);
            } else {
                // fully approved transaction
                preAuth.setIsApproved(true);
                preAuth.setAuthorizationNumber(reply.getApprovalCode());
            }
        } else {
            preAuth.setIsApproved(false);
            preAuth.setResponseCode(reply.getErrorName());
            if (preAuth.getResponseCode() == null) {
                preAuth.setResponseCode(reply.getResultMessage());
            }
        }
        
        return preAuth.isIsApproved();
    }
    
    private void createAndSendReversal(final PreAuth preAuth) {
        Reversal reversal = null;
        try {
            
            final String panAndExp = Track2Card.getCreditCardPanAndExpiry(preAuth.getCardData());
            
            String lastResponseCode = (preAuth.getResponseCode() == null) ? "" : preAuth.getResponseCode();
            if (lastResponseCode.length() > 20) {
                // to fit in reversal table
                lastResponseCode = lastResponseCode.substring(0, 20);
            }
            reversal = this.getCardProcessingManager()
                    .createReversal("", "Auth Only", CardProcessingUtil.getCentsAmountInDollars(preAuth.getAmount()), preAuth.getReferenceNumber(),
                                    Long.toString(System.currentTimeMillis()), panAndExp, lastResponseCode, preAuth.getMerchantAccount().getId(),
                                    new Date(), 0, preAuth.getPointOfSale().getId().intValue());
            reversal.setOriginalProcessorTransactionId(preAuth.getProcessorTransactionId());
        } catch (CardTransactionException e) {
            String msg = "Unable to create Reversal record";
            LOG.error(msg, e);
            getCommonProcessingService().sendCardProcessingAdminErrorAlert("Unable to call 'createAndSendReversal' in ElavonProcessor. ", msg, e);
        }
        
        if (reversal == null) {
            StringBuilder msg = new StringBuilder();
            msg.append("Unable to retrieve REVERSAL record in Elavon Processor");
            LOG.error(msg.toString());
            return;
        }
        
        try {
            getCardProcessingManager().processReversal(reversal);
        } catch (Exception e) {
            String msg = "Unable to update reversal after processing: " + reversal;
            LOG.error(msg, e);
            getCommonProcessingService().sendCardProcessingAdminErrorAlert("Unable to call 'processReversal' in ElavonProcessor. ", msg, e);
        }
    }
    
    @Override
    public Object[] processPostAuth(PreAuth preAuth, ProcessorTransaction processorTransaction) {
        LOG.info("processing post auth");
        String dba = null;
        if (dbaPrefixLength != 0) {
            dba = createDynamicDba(preAuth.getPointOfSale());
        }
        if (LOG.isDebugEnabled()) {
            if (dba == null) {
                LOG.debug(" \n DYNAMIC DBA not configured");
            } else {
                LOG.debug(" \n DYNAMIC DBA = " + dba);
            }
        }
        XMLElavonBuilder elavonXML = initializeXmlBuilder();
        CustomerCardType customerCardType =
                getCustomerCardTypeService().getCardTypeByTrack2Data(getMerchantAccount().getCustomer().getId(), preAuth.getCardData());
        Track2Card track2Card = new Track2Card(customerCardType, getMerchantAccount().getCustomer().getId(), preAuth.getCardData());
        String pan = track2Card.getPrimaryAccountNumber();
        String exp = track2Card.getExpirationMonth() + track2Card.getExpirationYear();
        elavonXML.createPostAuthRequest(preAuth, processorTransaction, processorTransaction.getSanitizedAuthorizationNumber(), pan, exp, dba);
        String responseString = sendToElavonServer(elavonXML.toString());
        responseString = this.removeCardDataFromXml(responseString);
        
        LOG.info("Elavon server responding: \n" + responseString);
        
        XMLElavonResponseHandler responseHandler = new XMLElavonResponseHandler();
        responseHandler.process(responseString);
        ElavonTxReplyHandler reply = responseHandler.getCurrentDataHandler();
        processorTransaction.setProcessingDate(new Date());
        processorTransaction.setReferenceNumber(getCommonProcessingService().getNewReferenceNumber(preAuth.getMerchantAccount()));
        processorTransaction.setProcessorTransactionId(reply.getTxnId());
        String responseCode = reply.getResult();
        if (RESPONSE_CODE_APPROVED_STRING.equals(responseCode)) {
            processorTransaction.setIsApproved(true);
            processorTransaction.setAuthorizationNumber(reply.getApprovalCode());
            return new Object[] { true, responseCode };
        }
        String errorCode = reply.getErrorCode();
        if (errorCode == null) {
            errorCode = reply.getResultMessage();
        }
        processorTransaction.setAuthorizationNumber(null);
        processorTransaction.setIsApproved(false);
        return new Object[] { false, errorCode };
    }
    
    @Override
    public Object[] processRefund(boolean isNonEmsRequest, ProcessorTransaction origProcessorTransaction,
        ProcessorTransaction refundProcessorTransaction, String creditCardNumber, String expiryMMYY) {
        LOG.info("Elavon processing refund");
        String dba = null;
        if (dbaPrefixLength != 0) {
            dba = createDynamicDba(origProcessorTransaction.getPointOfSale());
        }
        if (LOG.isDebugEnabled()) {
            if (dba == null) {
                LOG.debug(" \n DYNAMIC DBA not configured");
            } else {
                LOG.debug(" \n DYNAMIC DBA = " + dba);
            }
        }
        
        XMLElavonBuilder elavonXML = initializeXmlBuilder();
        String txnId = origProcessorTransaction.getProcessorTransactionId();
        elavonXML.createRefundTransactionRequest(txnId, refundProcessorTransaction.getAmount(), dba);
        String responseString = sendToElavonServer(elavonXML.toString());
        XMLElavonResponseHandler responseHandler = new XMLElavonResponseHandler();
        responseString = this.removeCardDataFromXml(responseString);
        LOG.info("Elavon server responding to process refund request: \n" + responseString);
        responseHandler.process(responseString);
        ElavonTxReplyHandler reply = responseHandler.getCurrentDataHandler();
        String responseCodeString = reply.getResult();
        
        refundProcessorTransaction.setProcessingDate(new Date());
        refundProcessorTransaction.setReferenceNumber(getCommonProcessingService().getNewReferenceNumber(getMerchantAccount()));
        refundProcessorTransaction.setProcessorTransactionId(reply.getTxnId());
        
        if (RESPONSE_CODE_APPROVED_STRING.equals(responseCodeString)) {
            refundProcessorTransaction.setIsApproved(true);
            refundProcessorTransaction.setAuthorizationNumber(reply.getApprovalCode());
            return new Object[] { DPTResponseCodesMapping.CC_APPROVED_VAL__7000, responseCodeString };
        }
        final String errorCode = reply.getErrorCode();
        refundProcessorTransaction.setIsApproved(false);
        refundProcessorTransaction.setAuthorizationNumber(null);
        if (isNonEmsRequest) {
            if (NumberUtils.isNumber(errorCode)) {
                return new Object[] { mapErrorResponse(errorCode), errorCode };
            } else {
                return new Object[] { DPTResponseCodesMapping.CC_DECLINED_VAL__7003, reply.getResultMessage() };
            }
        }
        int response_code = -1;
        if (NumberUtils.isNumber(errorCode)) {
            try {
                response_code = Integer.parseInt(errorCode);
            } catch (Exception e) {
                String msg = "Error trying to map non-numeric response code: " + errorCode;
                LOG.error(msg, e);
                getCommonProcessingService().sendCardProcessingAdminErrorAlert("Unable to call 'processRefund' in ElavonProcessor. ", msg, e);
                return new Object[] { DPTResponseCodesMapping.CC_UNKNOWN_ERROR_VAL__7063, errorCode };
            }
            return new Object[] { response_code, errorCode };
        } else {
            return new Object[] { DPTResponseCodesMapping.CC_DECLINED_VAL__7003, reply.getResultMessage() };
        }
    }
    
    @Override
    public Object[] processCharge(Track2Card track2Card, ProcessorTransaction chargeProcessorTransaction) {
        LOG.info("Elavon processing transaction charge");
        String dba = null;
        if (dbaPrefixLength != 0) {
            dba = createDynamicDba(chargeProcessorTransaction.getPointOfSale());
        }
        if (LOG.isDebugEnabled()) {
            if (dba == null) {
                LOG.debug(" \n DYNAMIC DBA not configured");
            } else {
                LOG.debug(" \n DYNAMIC DBA = " + dba);
            }
        }
        XMLElavonBuilder elavonXML = initializeXmlBuilder();
        String refnum = getCommonProcessingService().getNewReferenceNumber(getMerchantAccount());
        float amount = CardProcessingUtil.getCentsAmountInDollars(chargeProcessorTransaction.getAmount()).floatValue();
        elavonXML.createChargeRequest(track2Card, amount, refnum, dba);
        String responseString = sendToElavonServer(elavonXML.toString());
        XMLElavonResponseHandler responseHandler = new XMLElavonResponseHandler();
        responseHandler.process(responseString);
        ElavonTxReplyHandler reply = responseHandler.getCurrentDataHandler();
        String responseCode = reply.getResult();
        responseString = this.removeCardDataFromXml(responseString);
        
        LOG.info("Elavon server responding to process charge request: \n" + responseString);
        
        chargeProcessorTransaction.setReferenceNumber(refnum);
        if (RESPONSE_CODE_APPROVED_STRING.equals(responseCode)) {
            chargeProcessorTransaction.setAuthorizationNumber(reply.getApprovalCode());
            chargeProcessorTransaction.setIsApproved(true);
            chargeProcessorTransaction.setProcessorTransactionId(reply.getTxnId());
            return new Object[] { DPTResponseCodesMapping.CC_APPROVED_VAL__7000, responseCode };
        }
        chargeProcessorTransaction.setAuthorizationNumber(null);
        chargeProcessorTransaction.setIsApproved(false);
        final String errorCode = reply.getErrorCode();
        if (NumberUtils.isNumber(errorCode)) {
            return new Object[] { mapErrorResponse(errorCode), errorCode };
        } else {
            return new Object[] { DPTResponseCodesMapping.CC_DECLINED_VAL__7003, reply.getResultMessage() };
        }
    }
    
    /**
     * Sends a transaction which is likely to be declined by Elavon based on Pre-Programmed Responses.
     * If the decline message is observed, returns true.
     * If any other message is observed, return false.
     */
    @Override
    public String testCharge(MerchantAccount ma) {
        LOG.info("Elavon processing a test charge");
        XMLElavonBuilder elavonXML = initializeXmlBuilder();
        String dynamicDba = null;
        if (dbaPrefixLength > 0) {
            int dbaNamePaddingLength = 20 - dbaPrefixLength;
            String extraPadding = "+";
            dynamicDba = "TEST";
            for (int i = 0; i < dbaNamePaddingLength; i++) {
                dynamicDba = dynamicDba + extraPadding;
            }
        }
        String expiry_MMYY = createExpiryMMYYInAMonth();
        String cardData = this.testCreditCardNumberService.getDefaultTestElavonCreditCardNumberDeclined();
        float amount = CardProcessingUtil.getCentsAmountInDollars(DEFAULT_DECLINE_AMOUNT).floatValue();
        String refnum = getCommonProcessingService().getNewReferenceNumber(getMerchantAccount());
        elavonXML.createChargeRequest(cardData, expiry_MMYY, amount, refnum, dynamicDba);
        String responseString = sendToElavonServer(elavonXML.toString());
        XMLElavonResponseHandler responseHandler = new XMLElavonResponseHandler();
        responseHandler.process(responseString);
        responseString = removeCardDataFromXml(responseString);
        
        LOG.info("Elavon server responding to process charge request: \n" + responseString);
        
        ElavonTxReplyHandler reply = responseHandler.getCurrentDataHandler();
        String responseCode = reply.getResult();
        if (RESPONSE_CODE_APPROVED_STRING.equals(responseCode)) {
            // Deliberately using a test card which should always decline in production.
            // Visa appears to always decline the number 4124939999999990
            // .. but just in case:
            Reversal reversal = new Reversal();
            reversal.setOriginalProcessorTransactionId(reply.getTxnId());
            processReversal(reversal, null);
            String message = "A test transaction with Elavon received an unexpected approval response from Virtual Merchant.  "
                             + "Approved test transactions will enter live batches and void any valid transactions causing loss of revenue.  "
                             + "A reversal request for this transaction has been sent to Elavon.";
            if (reversal.isIsSucceeded()) {
                message = message + "  The reversal request was successful.";
            } else {
                message = message + "  The reversal request was NOT successful.";
            }
            String merchantAccountDetails = "Merchant Account Details: \n Customer Name = " + ma.getCustomer().getName()
                                            + "\n Elavon VirtualMerchant Id = " + this.merchantId;
            
            getCommonProcessingService().sendCardProcessingAdminErrorAlert("Test transaction entered live batch through Elavon virtual merchant",
                                                                           message + "\n " + merchantAccountDetails, null);
            return WebCoreConstants.RESPONSE_FALSE;
        }
        if (isResponseFromElavon(responseString, reply)) {
            return WebCoreConstants.RESPONSE_TRUE;
        }
        ElavonTxReplyHandler errorXml = responseHandler.getCurrentDataHandler();
        String error = errorXml.getErrorName();
        String errorMessage = null;
        errorMessage =
                new StringBuffer(WebCoreConstants.RESPONSE_FALSE).append(StandardConstants.STRING_COLON).append(INVALID_MSG + error).toString();
        LOG.debug(errorMessage);
        return errorMessage;
    }
    
    /**
     * Check if Elavon processor returns a response, no matter if it's approved or declined. According to Elavon document in the response these two elements
     * will be returned:
     * - <ssl_txn_id> - Transaction identification number. This is a unique number used to identify the transaction.
     * - <ssl_result> - Outcome of a transaction.
     * 
     * @param responseString
     *            XML response come back from Elavon.
     * @param reply
     *            ElavonTxReplyHandler object contains response from Elavon.
     * @return boolean 'true' if in the response 'result' and 'txn id' exist.
     */
    private boolean isResponseFromElavon(String responseString, ElavonTxReplyHandler reply) {
        if (StringUtils.isBlank(responseString) || reply == null) {
            return false;
        }
        if (StringUtils.isNotBlank(reply.getResult()) && StringUtils.isNotBlank(reply.getTxnId())) {
            return true;
        }
        return false;
    }
    
    @Override
    public void processReversal(Reversal reversal, Track2Card track2Card) {
        LOG.info("Elavon processing reversal request");
        XMLElavonBuilder elavonXML = initializeXmlBuilder();
        elavonXML.createTransactionReversalRequest(reversal.getOriginalProcessorTransactionId());
        String responseString = sendToElavonServer(elavonXML.toString());
        XMLElavonResponseHandler responseHandler = new XMLElavonResponseHandler();
        responseHandler.process(responseString);
        ElavonTxReplyHandler reply = responseHandler.getCurrentDataHandler();
        String responseCode = reply.getResult();
        responseString = this.removeCardDataFromXml(responseString);
        
        LOG.info("Elavon server responding to reversal request: \n" + responseString);
        
        if (RESPONSE_CODE_APPROVED_STRING.equals(responseCode)) {
            reversal.setIsSucceeded(true);
        } else if (ERROR_INVALID_TRANSACTION_ID.equals(reply.getErrorCode())) {
            reversal.setIsExpired(true); // reversal probably succeeded previously but connection timed out
            reversal.setRetryAttempts(120);
            reversal.setLastResponseCode(ERROR_INVALID_TRANSACTION_ID);
        } else {
            reversal.incrementRetryAttempts();
            reversal.setLastRetryTime(DateUtil.getCurrentGmtDate());
        }
    }
    
    private String sendToElavonServer(String requestInXml) {
        OutputStream out = null;
        BufferedReader rd = null;
        try {
            final URL destinationUrl = new URL(destinationUrlString);
            String auth = null;
            String responseString = "";
            InputStream in = null;
            
            requestInXml = "xmldata=" + requestInXml;
            String loggerRequest = removeCardDataFromXml(requestInXml);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Request:   \n" + loggerRequest);
            }
            // Send message
            final byte[] request = requestInXml.getBytes();
            
            final URLConnection con = destinationUrl.openConnection();
            con.setConnectTimeout(connectionTimeout);
            con.setReadTimeout(connectionTimeout);
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setUseCaches(false);
            con.setAllowUserInteraction(false);
            
            if (auth != null) {
                con.setRequestProperty("Authorization", "Basic " + auth);
            }
            out = con.getOutputStream();
            
            out.write(request);
            out.flush();
            
            in = con.getInputStream();
            rd = new BufferedReader(new InputStreamReader(in));
            String s = null;
            while ((s = rd.readLine()) != null) {
                responseString += s;
            }
            
            if (!responseString.startsWith("<?xml")) {
                throw new XMLErrorException("Elavon Processor returned error message: " + responseString);
            }
            return responseString;
        } catch (IOException e) {
            throw new XMLErrorException("Elavon Processor is Offline", e);
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ioe) {
                    LOG.error(ioe);
                }
            }
            if (rd != null) {
                try {
                    rd.close();
                } catch (IOException ioe) {
                    LOG.error(ioe);
                }
            }
        }
    }
    
    private String removeCardDataFromXml(String input) {
        String output = null;
        output = XMLUtil.hideAllElementValues(input, XMLElavonBuilder.ACCOUNT_NUMBER_STRING);
        output = XMLUtil.hideAllElementValues(output, XMLElavonBuilder.CARD_EXP_STRING);
        output = XMLUtil.hideAllElementValues(output, XMLElavonBuilder.TRACK2_STRING);
        return output;
    }
    
    /**
     * Generates the DBA name for a transaction.
     * The generated name will include the lot name and the date, formatted based on the length
     * of the dba prefix. (Prefix can be 3, 7, or 12 characters; DBA name respectively will be
     * 21, 17, or 12 characters)
     * 
     * @param PointOfSale
     * @return null if no dba prefix configured on merchant account; " " if no lot name; dba name otherwise.
     */
    private String createDynamicDba(PointOfSale pointOfSale) {
        if (dbaPrefixLength == 0) {
            return null;
        }
        if (pointOfSale == null || pointOfSale.getLocation() == null) {
            return " ";
        }
        String lotName = pointOfSale.getLocation().getName();
        int lotNameMaxLength;
        if (dbaPrefixLength == 12) {
            lotNameMaxLength = 7;
        } else {
            lotNameMaxLength = 13 - dbaPrefixLength;
        }
        if (lotName.length() > lotNameMaxLength) {
            lotName = lotName.substring(0, lotNameMaxLength);
        }
        String timezone = null;
        
        Set<CustomerProperty> customerProperties = pointOfSale.getCustomer().getCustomerProperties();
        for (CustomerProperty p : customerProperties) {
            if (p.getCustomerPropertyType().getId() == 1) {
                timezone = p.getPropertyValue();
                break;
            }
        }
        
        String dateString = null;
        Date date = new Date();
        String dateFormat = (dbaPrefixLength == 12) ? DBA_DATE_FORMAT_12 : DBA_DATE_FORMAT;
        if (timezone == null) {
            dateString = DateUtil.createDateString(new SimpleDateFormat(dateFormat), date);
        } else {
            dateString = DateUtil.createDateString(new SimpleDateFormat(dateFormat), timezone, date);
        }
        return lotName + dateString;
    }
    
    private int mapErrorResponse(final String responseCode) throws XMLErrorException {
        // Every possible Elavon error as of 20 Nov 2013 is reflected in this switch statement.
        // Many of these should never occur during our transactions.
        int response_code = -1;
        try {
            response_code = Integer.parseInt(responseCode);
        } catch (Exception e) {
            throw new XMLErrorException("Expecting numeric response, but recieved: " + responseCode, e);
        }
        switch (response_code) {
            case 3000:
                // Gateway not responding
            case 3001:
                // Gateway generated error
            case 3002:
                // Adapter generated error
                return DPTResponseCodesMapping.CC_SYSTEM_ERROR_VAL__7047;
            
            case 4000:
                // Virtual Merchant Id not supplied
            case 4001:
                // VirtualMerchantID, UserId, and/or PIN invalid
            case 4002:
                // HTTP POST transactions are not allowed for this account
            case 4003:
                // HTTP POST transactions are not allowed for this HTTP referrer
                return DPTResponseCodesMapping.CC_MERCHANT_ACCOUNT_CONFIG_ERROR_VAL__7098;
            
            case 4005:
                // Invalid e-mail
                return DPTResponseCodesMapping.CC_INVALID_REQUEST_VAL__7058;
            
            case 4006:
                // the cvv2 indicator was not identified in the authorization
                // request
            case 4007:
                // cvv2 check cannot be performed as no data was supplied in the
                // authorization request
                return DPTResponseCodesMapping.CC_INVALID_REQUEST_VAL__7058;
            
            case 4009:
                // a required field was not supplied in the authorization request
            case 4010:
                // an invalid transaction type was supplied in the authorization
                // request
                return DPTResponseCodesMapping.CC_INVALID_REQUEST_VAL__7058;
            
            case 4011:
                // the receipt url supplied in the authorization request appears to
                // be blank or invalid
                return DPTResponseCodesMapping.CC_INVALID_REQUEST_VAL__7058;
            
            case 4012:
                // the virtual merchant id and or user id supplied in the
                // authorization request is invalid
                return DPTResponseCodesMapping.CC_MERCHANT_ACCOUNT_CONFIG_ERROR_VAL__7098;
            
            case 4013:
                // the pin was not supplied in the authorization request
                return DPTResponseCodesMapping.DPT_7082_VAL;
            case 4014:
                // this terminal or user id is not permitted to process this
                // transaction type
            case 4015:
                // the pin supplied in the authorization request is invalid
            case 4016:
                // this account does not have permission to process _____ transactions
                return DPTResponseCodesMapping.CC_MERCHANT_ACCOUNT_CONFIG_ERROR_VAL__7098;
            
            case 4017:
                // the request has timed out. the allotted time to complete the
                // request has ended. please try again.
            case 4018:
                // settlement is in progress, void failed
                return DPTResponseCodesMapping.CC_SYSTEM_ERROR_VAL__7047;
            
            case 4019:
                // the user id was not supplied in the authorization request
                return DPTResponseCodesMapping.CC_MERCHANT_ACCOUNT_CONFIG_ERROR_VAL__7098;
            
            case 4022:
                // the system is unavailable. this transaction request has not been
                // approved. please try again later;
                return DPTResponseCodesMapping.CC_SYSTEM_ERROR_VAL__7047;
            
            case 4023:
                // settlement is not allowed for this terminal.
                return DPTResponseCodesMapping.CC_MERCHANT_ACCOUNT_CONFIG_ERROR_VAL__7098;
            
            case 5000:
                // the credit card number supplied in the authorization request
                // appears to be invalid
            case 5001:
                // the credit card expiration date supplied in the authorization
                // request appears to be invalid.
                return DPTResponseCodesMapping.CC_BAD_CREDIT_CARD_DATA_VAL__7045;
            
            case 5002:
                // the amount supplied in the authorization request appears to be
                // invalid
                return DPTResponseCodesMapping.CC_INVALID_AMOUNT_VAL__7033;
            
            case 5003:
                // a force approval code was supplied for this transaction; however
                // the transaction type is not force
            case 5004:
                // the force approval code supplied in the authorization request
                // appears to be invalid or blank. the force approval code must be 6
                // or less alphanumeric characters.
                return DPTResponseCodesMapping.CC_UNABLE_TO_PROCESS_VAL__7084;
            
            case 5005:
                // the value for the field is too long
                return DPTResponseCodesMapping.CC_INVALID_REQUEST_VAL__7058;
            
            case 5006:
                // refund amount exceeds limit
                return DPTResponseCodesMapping.CC_ADJUSTMENT_NOT_ALLOWED_VAL__7065;
            
            case 5007:
                // the sales tax supplied in the authorization request appears to be
                // invalid
                return DPTResponseCodesMapping.CC_INVALID_REQUEST_VAL__7058;
            
            case 5008:
                // this pinless debit transaction contains invalid account type.
                // account type can be checking or saving
            case 5009:
                // invalid surcharge amount for the PIN less debit transaction
            case 5010:
                // an invalid EGC transaction type has been supplied with this
                // request
            case 5011:
                // an invalid egc tender type has been supplied with this request
                return DPTResponseCodesMapping.CC_INVALID_REQUEST_VAL__7058;
            case 5012:
                // the track data sent appears to be invalid
            case 5013:
                // transaction requires track2 data to be sent
                return DPTResponseCodesMapping.CC_BAD_CREDIT_CARD_DATA_VAL__7045;
            case 5014:
                // transaction requires a pin entry or encrypted pin device
            case 5015:
                // the value for the voucher number field should be 15 digits in
                // length
            case 5016:
                // the MICR data sent appears to be invalid
            case 5017:
                // the image uploaded doesn't match the MICR data sent for that
                // check
            case 5018:
                // the MAC value sent appears to be incorrect
            case 5019:
                // minimum field character limit not reached
            case 5020:
                // the field does not apply to this transaction type
            case 5021:
                // the value for the cvv2 field should be 3 or 4 digits in length.
                // this value must be numeric
            case 5022:
                // the value for the cvv2 indicator field should be 1 numeric
                // character only. Valid values are 0, 1, 2 9
            case 5023:
                // card present indicator sent is invalid
            case 5024:
                // the cash back amount supplied in the authorization request
                // appears to be invalid.
            case 5025:
                // the value for the key pointer field should be 1 character only.
                // valid value is t for TRIPLE-DES DUKPT
            case 5030:
                // the billing cycle specified is not a valid entry
            case 5031:
                // the next payment date specified is not a valid entry.
            case 5032:
                // the installment number specified is invalid
            case 5033:
                // the recurring ID is not valid
            case 5034:
                // the installment id is not valid
                return DPTResponseCodesMapping.CC_INVALID_REQUEST_VAL__7058;
            case 5035:
                // the recurring batch has exceeded the 20,000 transaction limit
                return DPTResponseCodesMapping.CC_UNABLE_TO_PROCESS_VAL__7084;
            case 5036:
                // installment payments completed
            case 5037:
                // invalid end of month value
            case 5038:
                // invalid half of month value
            case 5039:
                // the half of the month value provided doesn't correspond with the
                // next payment date
            case 5040:
                // transaction id is invalid for this transaction type
                return DPTResponseCodesMapping.CC_INVALID_REQUEST_VAL__7058;
            
            case 5041:
                // unable to void transaction, exceeds the 10 minute window
                return DPTResponseCodesMapping.CC_UNABLE_TO_PROCESS_VAL__7084;
            
            case 5042:
                // swipe data is not allowed for this market segment. please re-key
                // the card data
            case 5043:
                // the end of the month value provided doesn't correspond with the
                // next payment date
            case 5050:
                // tip amount is invalid
            case 5055:
                // the response file name is missing.
            case 5056:
                // the response file name is invalid
            case 5057:
                // the batch file is missing
            case 5058:
                // the batch file name you provided is too long.
            case 5059:
                // the batch file you uploaded is invalid
            case 5060:
                // the batch file you uploaded has an incorrect extension
            case 5061:
                // import batch in progress, retry later
            case 5062:
                // file exceeds max number of transactions
            case 5063:
                // file already imported
            case 5064:
                // the batch file you uploaded is invalid (invalid fields)
            case 5065:
                // the response file name you provided is too long
            case 5066:
                // the number of import batch files has exceeded the limit allowed
                // within 23 hours
            case 5067:
                // the batch file you uploaded is invalid
            case 5068:
                // there was an error in processing your batch
            case 5069:
                // invalid batch import request
            case 5070:
                // signature already in system
            case 5071:
                // signature format invalid
            case 5072:
                // signature type invalid
            case 5073:
                // signature image exceeds size limitation
            case 5074:
                // signature is not allowed for this market segment
            case 5080:
                // values for ssl_3dsecure and ssl_xid are required
            case 5081:
                // value for ssl_xid is required
                return DPTResponseCodesMapping.CC_INVALID_REQUEST_VAL__7058;
            
            case 5083:
                // invalid dba name
                return DPTResponseCodesMapping.CC_MERCHANT_ACCOUNT_CONFIG_ERROR_VAL__7098;
            
            case 5090:
                // invalid bin override value
            case 5091:
                // invalid amount
            case 5092:
                // invalid country code
            case 5093:
                // transaction time exceeded
            case 5094:
                // invalid travel information
            case 5095:
                // invalid search criteria
            case 6001:
                // manual transaction declined
                return DPTResponseCodesMapping.CC_INVALID_REQUEST_VAL__7058;
            
            case 6002:
                // declined: invalid card
            case 6003:
                // declined: pick up card
            case 6004:
                // declined: amount error
            case 6005:
                // declined: appl. type error
            case 6006:
                // declined
            case 6007:
                // declined: help
            case 6008:
                // declined: request exceeds balance
            case 6009:
                // declined: expired card
            case 6010:
                // declined: incorrect pin
            case 6011:
                // declined: invalid term id
            case 6012:
                // declined: invalid term id 1
            case 6013:
                // declined: invalid term id 2
            case 6014:
                // declined: invalid void data
            case 6015:
                // declined: must settle MMDD
            case 6016:
                // declined: not on file
            case 6017:
                // declined: record not found
            case 6018:
                // declined: serv not allowed
            case 6019:
                // declined: seq err pls call
            case 6020:
                // declined: call auth center
            case 6021:
                // declined: call ref
            case 6022:
                // declined: cvv2
            case 6023:
                // declined: please RetryXXXX
                return DPTResponseCodesMapping.CC_DECLINED_VAL__7003;
            
            case 6024:
                // card already active
                return DPTResponseCodesMapping.CC_INVALID_REQUEST_VAL__7058;
            
            case 6025:
                // request exceeds balance
                return DPTResponseCodesMapping.CC_INSUFFICIENT_FUNDS_VAL__7017;
            
            case 6026:
                // cannot load the amount specified
                return DPTResponseCodesMapping.CC_INVALID_AMOUNT_VAL__7033;
            
            case 6027:
                // card not activated
            case 6028:
                // card cannot be reloaded
                return DPTResponseCodesMapping.CC_INVALID_REQUEST_VAL__7058;
            
            case 6029:
                // declined: invalid reg key
            case 6030:
                // declined: invalid packet
            case 6031:
                // declined: invalid LRC
            case 6032:
                // declined: invalid Response
            case 6033:
                // declined: invalid LRC in Response
            case 6034:
                // declined: invalid record number in response
                return DPTResponseCodesMapping.CC_DECLINED_VAL__7003;
            
            case 6038:
                // System is temporarily unavailable
            case 6042:
                // Invalid request format - XML request is not well formed or
                // request is incomplete
                return DPTResponseCodesMapping.CC_SYSTEM_ERROR_VAL__7047;
            
            default:
                throw new XMLErrorException("Unknown Response Code: " + responseCode);
        }
    }
    
    @Override
    public boolean isReversalExpired(final Reversal reversal) {
        return (reversal.getRetryAttempts() >= EMS_MAX_REVERSAL_RETRIES);
    }
    
    private String createExpiryMMYYInAMonth() {
        final SimpleDateFormat format = new SimpleDateFormat("MMyy");
        final Calendar cal = new GregorianCalendar();
        cal.add(Calendar.MONTH, 1);
        final String mmyy = format.format(cal.getTime());
        return mmyy;
    }
}
