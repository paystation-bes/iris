package com.digitalpaytech.cardprocessing.impl;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.TimeZone;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import org.apache.log4j.Logger;

import com.digitalpaytech.cardprocessing.CardProcessor;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.xml.XMLCBordBuilder;
import com.digitalpaytech.util.xml.XMLCBordResponseHandler;

/**
 * CBordProcessor abstract class implements methods which are shared between the
 * CBord Gold & Odyssey processors.
 * 
 * The subclass sets the currentProcessor field which is used to create XML
 * requests based on the current processor.
 * 
 * @author harshd
 * 
 */
public abstract class CBordProcessor extends CardProcessor {

	public final static int AUTHORIZATION_TYPE_REQUEST = 1;
	public final static int ACKNOWLEDGE_TYPE_REQUEST = 2;
	public final static int BALANCE_TYPE_REQUEST = 3;

	public final static String DATE_TIME_FORMAT = "yyyy:MMdd:HHmmss";

	private final static Logger LOG = Logger.getLogger(CBordProcessor.class);

	private final static String RESPONSE_CODE_APPROVED_STRING = "00";
	private final static String RESPONSE_CODE_DENIED_STRING = "05";

	protected String dateTime;

	// date format required to be sent in the message. date and time can be
	// tokenized

	protected String iPAddress;
	protected String provider;
	protected String location;
	protected String codeMap;
	protected String currentProcessor;
	protected String port;
	protected String portSSL;

	public CBordProcessor(final MerchantAccount merchantAccount,
			final CommonProcessingService commonProcessingService,
			final EmsPropertiesService emsPropertiesService,
			final PointOfSale pointOfSale, final String timeZone) {
		super(merchantAccount, commonProcessingService, emsPropertiesService);
		try {
			this.iPAddress = merchantAccount.getField1();
			this.portSSL = merchantAccount.getField2();
			this.provider = merchantAccount.getField3();
			this.location = merchantAccount.getField4();
			this.codeMap = merchantAccount.getField5();

			final Date date = new Date();
			TimeZone tz = TimeZone.getTimeZone(timeZone);
			this.dateTime = DateUtil.convertUTCTimeToDateTimeByTZ(tz, date,
					DATE_TIME_FORMAT);

		} catch (Exception e) {
			LOG.warn("Unable to initialize CBORD processor");
			throw new CardTransactionException(
					"Could not initialize CBORD Processor!", e);
		}
	}

	private XMLCBordBuilder initializeXmlBuilder() {
		final XMLCBordBuilder cbordXML = new XMLCBordBuilder();
		return cbordXML;
	}

	@Override
	public boolean processPreAuth(final PreAuth preAuth) {

		String serverResponse = null;
		int balance = 0;
		int authorizationAmount;
		preAuth.setMerchantAccount(getMerchantAccount());

		final XMLCBordBuilder xmlBuilder = initializeXmlBuilder();
		final String referenceNumber = getCommonProcessingService()
				.getNewReferenceNumber(getMerchantAccount());

		final String balanceRequestXML = xmlBuilder.createBalanceRequestXML(
				this.provider, preAuth, this.codeMap, this.location, referenceNumber);

		serverResponse = sendToCBordServerViaSSL(balanceRequestXML.getBytes(),
				BALANCE_TYPE_REQUEST);

		final XMLCBordResponseHandler responseHandler = new XMLCBordResponseHandler();

		responseHandler.processBalanceRequest(serverResponse);

		final CBordTxReplyHandler reply = responseHandler
				.getCurrentDataHandler();

		final String responseCode = reply.getBalanceReq().getResponseCode();

		// preAuth.setProcessorTransactionId(preAuth.getProcessorTransactionId());
		preAuth.setProcessingDate(new Date());
		preAuth.setResponseCode(responseCode);
		preAuth.setReferenceNumber(referenceNumber);
		preAuth.setAuthorizationNumber(reply.getBalanceReq().getTransID());

		final String balanceInCard = reply.getBalanceReq()
				.getActualSVCbalance();

		authorizationAmount = preAuth.getAmount();
		if (!(balanceInCard == null || balanceInCard
				.equals(WebCoreConstants.BLANK))) {
			balance = Integer.parseInt(balanceInCard.substring(4));
		}

		if (authorizationAmount <= balance
				&& responseCode.equals(RESPONSE_CODE_APPROVED_STRING)) {
			preAuth.setIsApproved(true);
		}

		return preAuth.isIsApproved();

	}

	@Override
	public Object[] processPostAuth(final PreAuth preAuth,
			final ProcessorTransaction processorTransaction) {

		final String referenceNumber = getCommonProcessingService()
				.getNewReferenceNumber(getMerchantAccount());

		final XMLCBordBuilder xmlBuilder = initializeXmlBuilder();
		String preAuthXMLString = xmlBuilder
		        .createTransRequestXML(preAuth, this.dateTime, this.provider, this.location
		                               , this.codeMap, this.currentProcessor, referenceNumber);

		final String serverResponse = sendToCBordServerViaSSL(
				preAuthXMLString.getBytes(), AUTHORIZATION_TYPE_REQUEST);

		final XMLCBordResponseHandler responseHandler = new XMLCBordResponseHandler();
		responseHandler.processTransactionRequest(serverResponse);

		final CBordTxReplyHandler reply = responseHandler
				.getCurrentDataHandler();

		final String responseCode = reply.getReplyTx().getResponseCode();

		if (responseCode.equals(RESPONSE_CODE_APPROVED_STRING)) {

			/*
			 * preAuth.setAuthorizationNumber(reply.getReplyTx().getTransID());
			 * preAuth.setProcessorTransactionId(preAuth
			 * .getProcessorTransactionId());
			 */
			processorTransaction.setProcessingDate(new Date());
			processorTransaction.setProcessorTransactionId(preAuth
					.getProcessorTransactionId());
			processorTransaction.setAuthorizationNumber(preAuth
					.getAuthorizationNumber());
			processorTransaction.setReferenceNumber(referenceNumber);
			processorTransaction.setIsApproved(true);
			xmlBuilder.setPreAuthCBord(null);
			final String ackString = xmlBuilder.createAckXML(preAuth,
					processorTransaction, this.provider);
			sendToCBordServerViaSSL(ackString.getBytes(),
					ACKNOWLEDGE_TYPE_REQUEST);
		} else {
			processorTransaction.setIsApproved(false);
		}

		// If post Auth Fails due to duplicate transaction we need to disable
		// the merchant account
		return new Object[] { processorTransaction.isIsApproved(), responseCode };
	}

	private String sendToCBordServerViaSSL(final byte[] request,
			final int requestType) throws CardTransactionException {
	    if (LOG.isDebugEnabled()) {
	        LOG.debug("Request XML to CBORD : " + new String(request, Charset.defaultCharset()));
	    }

		final byte[] outData = new byte[1024];
		byte[] exactOutData = null;

		final SSLSocketFactory socketFactory = (SSLSocketFactory) SSLSocketFactory
				.getDefault();
		// create custom trust manager to ignore trust paths

		try {

			final SSLSocket sslSocket = (SSLSocket) socketFactory.createSocket(
					this.iPAddress, Integer.parseInt(portSSL));
			final PrintStream out = new PrintStream(sslSocket.getOutputStream(),
					false);
			out.write(request, 0, request.length);
			out.flush();
			if (requestType == ACKNOWLEDGE_TYPE_REQUEST) {
				out.close();
				sslSocket.close();
				return null;
			}
			final DataInputStream in = new DataInputStream(sslSocket.getInputStream());
			final int bytesRead = in.read(outData);
			exactOutData = new byte[bytesRead];
			for (int i = 0; i < bytesRead; i++) {
				exactOutData[i] = outData[i];
			}

			out.close();
			in.close();
			sslSocket.close();
		} catch (IOException e) {

			throw new CardTransactionException("CBORD server: " + this.iPAddress
					+ "Using SSL is offline!", e);
		}
		
		final String response = new String(exactOutData);
		
		if (LOG.isDebugEnabled()) {
		    LOG.debug("Response XML From CBORD : " + response);
		}

		return response;
	}


	@Override
	public Object[] processRefund(final boolean isNonEmsRequest,
			final ProcessorTransaction origProcessorTransaction,
			final ProcessorTransaction refundProcessorTransaction,
			final String creditCardNumber, final String expiryMMYY) {
		throw new UnsupportedOperationException(getClass().getSimpleName()
				+ ", process refund is not supported!");
	}

	@Override
	public Object[] processCharge(final Track2Card track2Card,
			final ProcessorTransaction chargeProcessorTransaction) {
		throw new UnsupportedOperationException(getClass().getSimpleName()
				+ ", process charge is not supported!");

	}

	@Override
	public void processReversal(final Reversal reversal, final Track2Card track2Card) {
		throw new UnsupportedOperationException(getClass().getSimpleName()
				+ ", process reversal is not supported!");

	}

	@Override
	public boolean isReversalExpired(final Reversal reversal) {
		throw new UnsupportedOperationException(getClass().getSimpleName()
				+ ", isReversalExpired(Reversal) is not support!");

	}

}
