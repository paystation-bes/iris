package com.digitalpaytech.cardprocessing.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.digitalpaytech.cardprocessing.CardProcessor;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.exception.XMLErrorException;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DPTResponseCodesMapping;
import com.digitalpaytech.util.xml.XMLConcordBuilder;
import com.digitalpaytech.util.xml.XMLConcordResponseHandler;
import com.digitalpaytech.util.xml.XMLUtil;

public final class ConcordProcessor extends CardProcessor {
    
    private static Logger logger__ = Logger.getLogger(ConcordProcessor.class);
    
    private final static String PRE_AUTH_ELEMENT_NAME_STRING = "CreditCardAuthorize";
    private final static String CREDIT_CARD_CHARGE_ELEMENT_NAME_STRING = "CreditCardCharge";
    private final static String POST_AUTH_ELEMENT_NAME_STRING = "CreditCardSettle";
    private final static String CREDIT_CARD_REFUND_ELEMENT_NAME_STRING = "CreditCardRefund";
    private final static String RESPONSE_CODE_APPROVED_STRING = "0";
    
    private String storeId;
    private String storeKey;
    private String destinationUrlString_;
    private String appId;
    
    public ConcordProcessor(MerchantAccount md, CommonProcessingService commonProcessingService, EmsPropertiesService emsPropertiesService) {
        super(md, commonProcessingService, emsPropertiesService);
        storeId = md.getField1();
        storeKey = md.getField2();
        
        appId = emsPropertiesService.getPropertyValue(EmsPropertiesService.CONCORD_EMS_APPLICATION_ID);
        if (StringUtils.isBlank(appId)) {
            appId = EmsPropertiesService.DEFAULT_CONCORD_EMS_APPLICATION_ID;
        }
        
        destinationUrlString_ = commonProcessingService.getDestinationUrlString(md.getProcessor());
        
        logger__.debug("ConcordProcessor, destinationUrlString: " + destinationUrlString_);
    }
    
    public boolean processPreAuth(PreAuth pd) throws XMLErrorException {
        XMLConcordBuilder xmlConcordBuilder = initializeXmlBuilder(PRE_AUTH_ELEMENT_NAME_STRING);
        
        // Create and send request
        xmlConcordBuilder.createPreAuthRequest(pd.getCardData(), CardProcessingUtil.getCentsAmountInDollars(pd.getAmount()).floatValue(),
                                               getCommonProcessingService().getNewReferenceNumber(getMerchantAccount()));
        String response_string = sendToConcordServer(xmlConcordBuilder.toString());
        
        // Process response
        pd.setProcessingDate(new Date());
        
        XMLConcordResponseHandler handler = new XMLConcordResponseHandler();
        try {
            handler.process(response_string.getBytes());
        } catch (ApplicationException ae) {
            throw new XMLErrorException("ConcordProcessor, unable to complete ConcordProcessor processPreAuth, ", ae);
        }            
        ConcordTxReplyHandler response = handler.getCurrentDataHandler();
        
        pd.setReferenceNumber(response.getReferenceNumber());
        pd.setResponseCode(response.getResponseCode());
        pd.setProcessorTransactionId(response.getTransactionID());
        pd.setCardData(null); // CardData not needed for Concord post-auths
        
        int responseCode = Integer.parseInt(pd.getResponseCode());
        if (responseCode == 0) {
            pd.setAuthorizationNumber(response.getAuthorizationNumber());
            pd.setIsApproved(true);
        } else {
            pd.setIsApproved(false);
        }
        
        return (pd.isIsApproved());
    }
    
    public Object[] processCharge(Track2Card track2Card, ProcessorTransaction chargePtd) throws XMLErrorException {
        XMLConcordBuilder xmlConcordBuilder = initializeXmlBuilder(CREDIT_CARD_CHARGE_ELEMENT_NAME_STRING);
        
        // Create Request
        xmlConcordBuilder.createChargeRequest(track2Card, CardProcessingUtil.getCentsAmountInDollars(chargePtd.getAmount()).floatValue(),
                                              getCommonProcessingService().getNewReferenceNumber(getMerchantAccount()));
        
        // Send Request
        String responseString = sendToConcordServer(xmlConcordBuilder.toString());
        
        // Parse Response
        XMLConcordResponseHandler handler = new XMLConcordResponseHandler();
        try {
            handler.process(responseString.getBytes());
        } catch (ApplicationException ae) {
            throw new XMLErrorException("ConcordProcessor, unable to complete ConcordProcessor processCharge, ", ae);
        }
        ConcordTxReplyHandler data_handler = handler.getCurrentDataHandler();
        
        // Set processor transaction data values
        chargePtd.setProcessingDate(new Date());
        chargePtd.setReferenceNumber(data_handler.getReferenceNumber());
        chargePtd.setProcessorTransactionId(data_handler.getTransactionID());
        
        String response_code = data_handler.getResponseCode();
        
        // Approved
        if (RESPONSE_CODE_APPROVED_STRING.equals(response_code)) {
            chargePtd.setAuthorizationNumber(data_handler.getAuthorizationNumber());
            chargePtd.setIsApproved(true);
            return new Object[] { DPTResponseCodesMapping.CC_APPROVED_VAL__7000, response_code };
        }
        
        // Declined
        chargePtd.setAuthorizationNumber(null);
        chargePtd.setIsApproved(false);
        return new Object[] { mapErrorResponse(response_code), response_code };
    }
    
    private XMLConcordBuilder initializeXmlBuilder(String msgType) throws XMLErrorException {
        XMLConcordBuilder xmlConcordBuilder = new XMLConcordBuilder(msgType);
        try {
            xmlConcordBuilder.initialize(storeId, storeKey, getEmsPropertiesService().getPropertyValue(EmsPropertiesService.CONCORD_EMS_APPLICATION_ID));
        } catch (XMLErrorException ide) {
            logger__.error("Could not initialize XMLConcordBuilder!");
            throw new XMLErrorException(ide);
        }
        
        return (xmlConcordBuilder);
    }
    
    public Object[] processPostAuth(PreAuth pd, ProcessorTransaction ptd) throws XMLErrorException {
        XMLConcordBuilder xmlConcordBuilder = initializeXmlBuilder(POST_AUTH_ELEMENT_NAME_STRING);
        
        // Create request
        xmlConcordBuilder.createPostAuthRequest(pd, ptd, getCommonProcessingService().getNewReferenceNumber(getMerchantAccount()));
        
        // Send request
        String responseString = sendToConcordServer(xmlConcordBuilder.toString());
        
        // Parse response
        XMLConcordResponseHandler handler = new XMLConcordResponseHandler();
        try {
            handler.process(responseString.getBytes());
        } catch (ApplicationException ae) {
            throw new XMLErrorException("ConcordProcessor, unable to complete ConcordProcessor processPostAuth, ", ae);
        }
        ConcordTxReplyHandler data_handler = handler.getCurrentDataHandler();
        
        // Set ProcessorTransactionData values from processor
        ptd.setProcessingDate(new Date());
        ptd.setReferenceNumber(data_handler.getReferenceNumber());
        ptd.setProcessorTransactionId(data_handler.getTransactionID());
        
        String response_code = data_handler.getResponseCode();
        
        // Approved
        if (RESPONSE_CODE_APPROVED_STRING.equals(response_code)) {
            ptd.setAuthorizationNumber(data_handler.getApprovalNumber());
            ptd.setIsApproved(true);
            return new Object[] { true, response_code };
        }
        
        // Declined
        ptd.setAuthorizationNumber(null);
        ptd.setIsApproved(false);
        return new Object[] { false, response_code };
    }
    
    public Object[] processRefund(boolean isNonEmsRequest, ProcessorTransaction origPtd, ProcessorTransaction refundPtd, String ccNumber, String expiryMMYY)
            throws XMLErrorException {
        
        XMLConcordBuilder xmlConcordBuilder = initializeXmlBuilder(CREDIT_CARD_REFUND_ELEMENT_NAME_STRING);
        
        // Create request
        xmlConcordBuilder.createRefundTransactionRequest(origPtd, CardProcessingUtil.getCentsAmountInDollars(refundPtd.getAmount()).floatValue(),
                                                         getCommonProcessingService().getNewReferenceNumber(getMerchantAccount()));
        
        // Send request
        String responseString = sendToConcordServer(xmlConcordBuilder.toString());
        
        // Parse response
        XMLConcordResponseHandler handler = new XMLConcordResponseHandler();
        try {
            handler.process(responseString.getBytes());
        } catch (ApplicationException ae) {
            throw new XMLErrorException("ConcordProcessor, unable to complete ConcordProcessor processRefund, ", ae);
        }
        ConcordTxReplyHandler data_handler = handler.getCurrentDataHandler();
        
        // Set processor transaction data values
        refundPtd.setProcessingDate(new Date());
        refundPtd.setReferenceNumber(data_handler.getReferenceNumber());
        refundPtd.setProcessorTransactionId(data_handler.getTransactionID());
        
        String response_code_string = data_handler.getResponseCode();
        
        // Approved
        if (RESPONSE_CODE_APPROVED_STRING.equals(response_code_string)) {
            refundPtd.setAuthorizationNumber(data_handler.getAuthorizationNumber());
            refundPtd.setIsApproved(true);
            return new Object[] { DPTResponseCodesMapping.CC_APPROVED_VAL__7000, response_code_string };
        }
        
        // Declined
        refundPtd.setAuthorizationNumber(null);
        refundPtd.setIsApproved(false);
        
        // map error response if request was not made by EMS
        if (isNonEmsRequest) {
            return new Object[] { mapErrorResponse(response_code_string), response_code_string };
        }
        
        // EMS request
        int response_code = -1;
        try {
            response_code = Integer.parseInt(response_code_string);
        } catch (Exception e) {
            String msg = "Error trying to map non-numeric response code: " + response_code_string;
            logger__.error(msg, e);
            getCommonProcessingService().sendCardProcessingAdminErrorAlert("Unable to call 'processRefund' in ConcordProcessor. ", msg, e);
            return new Object[] { DPTResponseCodesMapping.CC_UNKNOWN_ERROR_VAL__7063, response_code_string };
        }
        return new Object[] { response_code, response_code_string };
    }
    
    // private function to actually send the request in bytes to the Concord
    // server
    // returns a String which contains the response sent by the server
    private String sendToConcordServer(String requestInXML) throws XMLErrorException {
        try {
            URL destination_url = new URL(destinationUrlString_);
            String auth = null;
            String responseString = "";
            InputStream in = null;
            
            // remove the xml header tag from the request
            int start = requestInXML.indexOf(">");
            String xml_string_no_header = requestInXML.substring(start + 1, requestInXML.length());
            
            // Log outgoing messag
            String log_message = XMLUtil.hideAllElementValues(xml_string_no_header, XMLConcordBuilder.TRACK2_STRING);
            log_message = XMLUtil.hideAllElementValues(log_message, XMLConcordBuilder.ACCOUNT_NUMBER_STRING);
            logger__.info("Request:  " + log_message);
            
            // Send message
            byte[] request = xml_string_no_header.getBytes();
            URLConnection con = destination_url.openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setUseCaches(false);
            con.setAllowUserInteraction(false);
            con.setRequestProperty("Content-Length", Integer.toString(request.length));
            con.setRequestProperty("Content-Type", "text/xml");
            
            if (auth != null) {
                con.setRequestProperty("Authorization", "Basic " + auth);
            }
            
            OutputStream out = con.getOutputStream();
            out.write(request);
            out.flush();
            out.close();
            in = con.getInputStream();
            
            BufferedReader rd = new BufferedReader(new InputStreamReader(in));
            
            String s = null;
            while ((s = rd.readLine()) != null) {
                responseString += s;
            }
            rd.close();
            logger__.info("Response: " + responseString);
            return responseString;
        } catch (IOException e) {
            throw new XMLErrorException("Concord Processor is Offline", e);
        }
    }
    
    private int mapErrorResponse(String responseCode) throws XMLErrorException {
        int response_code = -1;
        try {
            response_code = Integer.parseInt(responseCode);
        } catch (Exception e) {
            throw new XMLErrorException("Expecting numeric response, but recieved: " + responseCode, e);
        }
        
        switch (response_code) {
        // RC_DECLINED: Transaction declined
            case 256:
                return (DPTResponseCodesMapping.CC_DECLINED_VAL__7003);
                
                // RC_INSUFFICIENTFUNDS: Insufficient funds
            case 257:
                return (DPTResponseCodesMapping.CC_INSUFFICIENT_FUNDS_VAL__7017);
                
                // RC_INVALIDCARD: Invalid card number, MICR number, or routing
                // number
            case 258:
                return (DPTResponseCodesMapping.CC_BAD_CREDIT_CARD_DATA_VAL__7045);
                
                // RC_EXPIREDCARD: Card expired
            case 259:
                return (DPTResponseCodesMapping.CC_EXPIRED_VAL__7001);
                
                // RC_DUPLICATEAPPROVE: Transaction previously approved
            case 1:
                // RC_DUPLICATE: Duplicate transaction
            case 1028:
                return (DPTResponseCodesMapping.CC_DUPLICATE_TRANS_VAL__7019);
                
                // RC_REFERRAL: Contact Financial Institution
            case 260:
                return (DPTResponseCodesMapping.CC_PLACE_CALL_VAL__7022);
                
                // RC_AUTHENTICATE: Authentication required
            case 128:
                // RC_BADAUTHENTICATION: Authentication failed
            case 261:
                // RC_UNABLETOAUTHENTICATE: Unable to authenticate
            case 262:
                return (DPTResponseCodesMapping.CC_UNABLE_TO_AUTHENTICATE_VAL__7055);
                
                // RC_STOPPAYMENT: Stop payment
            case 263:
                return (DPTResponseCodesMapping.CC_STOP_PAYMENT_VAL__7056);
                
                // RC_COMMERROR: Communications error, try again
            case 1026:
                // RC_COMMFAILURE: Communications failure
            case 1027:
                throw new XMLErrorException("Response indicates processor offline: " + responseCode);
                
                // RC_PROCESSORERROR: Generic processor error
            case 1023:
                // RC_ERROR: General error
            case 1024:
                // RC_INTERNALERROR: Internal error
            case 1025:
                return (DPTResponseCodesMapping.CC_SYSTEM_ERROR_VAL__7047);
                
                // RC_INVALIDREQUEST: Invalid request
            case 1030:
                // RC_INVALIDTRANSACTION: Invalid transaction
            case 1034:
                // RC_NOTPERMITTED: Transaction not permitted
            case 1035:
                return (DPTResponseCodesMapping.CC_INVALID_REQUEST_VAL__7058);
                
                // RC_INVALIDMERCHANT: Invalid merchant(ie. not setup for a
                // particular type)
            case 1029:
                return (DPTResponseCodesMapping.CC_CARD_TYPE_NOT_SETUP_VAL__7076);
                
                // RC_BADUSERNAME: Invalid StoreID
            case 1031:
                throw new XMLErrorException("Response indicates invalid StoreID: " + responseCode);
                
                // RC_BADPASSWORD: Invalid StoreKey
            case 1032:
                throw new XMLErrorException("Response indicates invalid StoreKey: " + responseCode);
                
                // RC_BADPROCESSOR: Invalid processor
            case 1033:
                return (DPTResponseCodesMapping.CC_BAD_PROCESSOR_VAL__7061);
                
                // RC_UNKNOWNERROR: Unknown error
            case 8191:
                return (DPTResponseCodesMapping.CC_UNKNOWN_ERROR_VAL__7063);
                
            default:
                throw new XMLErrorException("Unknown Response Code: " + responseCode);
        }
    }
    
    @Override
    public void processReversal(Reversal reversal, Track2Card track2Card) {
        throw new UnsupportedOperationException("ConcordProcessor, Reversal not supported for this processor");
    }
    
    @Override
    public boolean isReversalExpired(Reversal reversal) {
        throw new UnsupportedOperationException("ConcordProcessor, isReversalExpired(Reversal) is not support!");
    }
}
