package com.digitalpaytech.cardprocessing.impl;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Security;
import java.util.Date;
import java.util.StringTokenizer;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.Logger;

import com.digitalpaytech.cardprocessing.CardProcessor;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;


public class NuVisionProcessor extends CardProcessor {
	private final static Logger logger__ = Logger.getLogger(NuVisionProcessor.class);
	// private final static Logger cc_logger__ = Logger.getLogger(DummyLogger.class);

	private final static String UNDETERMINED_BALANCE = "Undetermined Balance";
	private final static String AES_ECB_PKCS5PADDING = "AES/ECB/PKCS5Padding";
	private final static String BOUNCY_CASTLE_PROVIDER = "BC";
	private final static int ENCRYPT = 1;
	private final static int DECRYPT = 2;
	private final static int KEY_SIZE = 32;
	private final static int DEBIT_MESSAGE_TYPE = 2;
	private final static int BALANCE_CHECK_MESSAGE_TYPE = 1;
	private final static int MAX_CARD_DATA_SIZE = 20;
	private final static int MAX_AMOUNT_SIZE = 7;
	private final static int MAX_MESSAGE_SIZE = 24;
	private final static int PACKET_HEADER_LENGTH = 8;
	private final static char FAIL_RESPONSE = 0x20;
	private final static char PASS_RESPONSE = 0x21;

	private String iPAddress;
	private String achAddress;
	private String packetType;
	private int portToUse_;

	private boolean isEncryptionUsed = false;

	private SecretKeySpec skeySpec;

	// response fields..same fields can be used for different request types
	private String responseAchAddress_;
	private char responseChPacketType_;
	private char responseChStatus_;
	private String responseAchStatus_;
	private String responseAchCard_;
	private String responseAchAmount_;
	private String responseAchMessage_;
	private String responseBalance_;

	public NuVisionProcessor(MerchantAccount md, CommonProcessingService commonProcessingService,
	        EmsPropertiesService emsPropertiesService, PointOfSale pointOfSale) {
	
		super(md, commonProcessingService, emsPropertiesService);
		try
		{
			logger__.debug("Creating NuVision object");

			String key = md.getField1();
			iPAddress = md.getField2();
			String port = md.getField3();
			packetType = md.getField4();

			// use last 4 characters of comm address as the address field
			String comm_address = pointOfSale.getSerialNumber();
			achAddress = comm_address.substring(comm_address.length() - 4);

			// if key size is not the expected size (32) then no encryption is used
			if (key.length() != KEY_SIZE)
			{
				isEncryptionUsed = false;
			}
			else
			{
				isEncryptionUsed = true;
			}

			try
			{
				portToUse_ = new Integer(port).intValue();
			}
			catch (NumberFormatException n)
			{
				throw new CardTransactionException(
						"Could not initialize NuVision Processor! Port number is not a valid integer!");
			}

			// Security initializations
			Provider bc = new org.bouncycastle.jce.provider.BouncyCastleProvider();
			Security.addProvider(bc);
			if (isEncryptionUsed)
			{
				skeySpec = new SecretKeySpec(key.getBytes(), AES_ECB_PKCS5PADDING);
			}
		}
		catch (Exception e)
		{
			throw new CardTransactionException("Could not initialize NuVision Processor!", e);
		}
	}
	
	
	
	public boolean processPreAuth(PreAuth pd) throws CardTransactionException
	{
		logger__.debug("Performing Pre-Auth for NuVision transaction");

		createAndSendRequest(BALANCE_CHECK_MESSAGE_TYPE, null, pd.getCardData());
		logger__.info("Response message : " + responseAchMessage_);

		// determine if card balance will cover transaction amount
		try
		{
			if (Float.parseFloat(responseBalance_) >= CardProcessingUtil.getCentsAmountInDollars(pd.getAmount()).floatValue())
			{
				responseChStatus_ = PASS_RESPONSE;
			}
			else
			{
				responseChStatus_ = FAIL_RESPONSE;
			}
		}
		catch (Exception e)
		{
			responseChStatus_ = FAIL_RESPONSE;
		}

		String reference_number = getCommonProcessingService().getNewReferenceNumber(getMerchantAccount());
		pd.setProcessingDate(new Date());
		pd.setReferenceNumber(reference_number);
		pd.setResponseCode(Character.toString(responseChStatus_));

		// approve if response is acceptable, else deny
		if (responseChStatus_ == PASS_RESPONSE)
		{
			pd.setIsApproved(true);
			pd.setAuthorizationNumber(reference_number);
		}
		else if (responseChStatus_ == FAIL_RESPONSE)
		{
			pd.setIsApproved(false);
		}

		return (pd.isIsApproved());
	}

	public Object[] processPostAuth(PreAuth pd, ProcessorTransaction ptd) throws CardTransactionException
	{
		createAndSendRequest(DEBIT_MESSAGE_TYPE, CardProcessingUtil.getCentsAmountInDollars(pd.getAmount()).floatValue(), pd.getCardData());

		// Set ProcessorTransactionData values from processor
		ptd.setProcessingDate(new Date());
		ptd.setReferenceNumber(getCommonProcessingService().getNewReferenceNumber(getMerchantAccount()));
		ptd.setProcessorTransactionId(pd.getProcessorTransactionId());
		ptd.setAuthorizationNumber(pd.getAuthorizationNumber());

		// Check approval status
		ptd.setIsApproved(responseChStatus_ == PASS_RESPONSE);
		return new Object[] {ptd.isIsApproved(), responseChStatus_};
	}

	// private function to actually send the request to the NuVision server
	// returns a byte array which contains the response sent by the server
	private byte[] sendToNuVisionServer(byte[] request) throws CardTransactionException
	{

		byte[] packet_header = new byte[8];
		byte[] packet = null;

		try
		{
			Socket socket = new Socket(iPAddress, portToUse_);
			PrintStream out = new PrintStream(socket.getOutputStream(), false);
			out.write(request, 0, request.length);
			out.flush();

			DataInputStream in = new DataInputStream(socket.getInputStream());
			in.read(packet_header, 0, PACKET_HEADER_LENGTH);

			int packet_length = packet_header[0];
			packet = new byte[packet_length];
			in.read(packet, 0, packet_length);

			out.close();
			in.close();
		}
		catch (IOException e)
		{
			throw new CardTransactionException("Nuvision processor is offline!", e);
		}

		return packet;
	}

	// extracts the responses from the bytes message received from the server for the request posted
	private void extractResponses(byte[] res) throws CardTransactionException
	{
		// decrypt response
		byte[] decrypted_response = encrypt_decrypt(res, DECRYPT);

		responseAchAddress_ = new String(decrypted_response, 0, 5);
		responseChPacketType_ = (char)decrypted_response[5];
		responseChStatus_ = (char)decrypted_response[6];
		responseAchStatus_ = new String(decrypted_response, 7, 2);
		responseAchCard_ = new String(decrypted_response, 9, 20);
		responseAchAmount_ = new String(decrypted_response, 29, 7);
		responseAchMessage_ = new String(decrypted_response, 36, 24);

		logger__.info("Response message : " + responseAchMessage_);

		// the amount available in this account is returned in the display text field. we need to parse it and
		// compare that value to the transaction amount
		try
		{
			StringTokenizer stk = new StringTokenizer(responseAchMessage_, "$");
			stk.nextToken();
			responseBalance_ = stk.nextToken().trim();
			logger__.info("Balance in response : " + responseBalance_);
		}
		catch (Exception e)
		{
			responseBalance_ = UNDETERMINED_BALANCE;
		}

	}

	private byte[] encrypt_decrypt(byte[] msg, int mode) throws CardTransactionException
	{
		byte[] result = null;

		try
		{
			Cipher cipher = Cipher.getInstance(AES_ECB_PKCS5PADDING, BOUNCY_CASTLE_PROVIDER);
			if (mode == ENCRYPT)
			{
				cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
			}
			else if (mode == DECRYPT)
			{
				cipher.init(Cipher.DECRYPT_MODE, skeySpec);
			}
			result = cipher.doFinal(msg);
		}
		catch (NoSuchAlgorithmException ne)
		{
			logger__.error(AES_ECB_PKCS5PADDING
					+ " algorithm could not be found during encryption/decryption process");
			ne.printStackTrace();
			throw new CardTransactionException(ne);
		}
		catch (NoSuchPaddingException ne)
		{
			logger__.error("Padding error during encryption/decryption process");
			ne.printStackTrace();
			throw new CardTransactionException(ne);
		}
		catch (InvalidKeyException ie)
		{
			logger__.error("Invalid key encountered during encryption/decryption process");
			ie.printStackTrace();
			throw new CardTransactionException(ie);
		}
		catch (BadPaddingException ne)
		{
			logger__.error("Bad padding encountered during encryption/decryption process");
			throw new CardTransactionException(ne);
		}
		catch (NoSuchProviderException npe)
		{
			logger__.error("Security provider not found during encryption/decryption process");
			npe.printStackTrace();
			throw new CardTransactionException(npe);
		}
		catch (IllegalBlockSizeException ibe)
		{
			logger__.error("Illegal block size during encryption/decryption process");
			ibe.printStackTrace();
			throw new CardTransactionException(ibe);
		}

		return result;
	}

	private void createAndSendRequest(int requestType, Float originalTransAmount, String cardData)
			throws CardTransactionException
	{
		String amount = null;
		char null_char = 0x00;
		String card_data = CardProcessingUtil.extractAccountNumber(cardData, CardProcessingConstants.TRACK_2_DELIMITER);

		switch (requestType)
		{
			case BALANCE_CHECK_MESSAGE_TYPE:
			{
				amount = "0000.00";
				break;
			}

			case DEBIT_MESSAGE_TYPE:
			{
				StringBuffer orig_amount = new StringBuffer(originalTransAmount.toString());

				int diff = MAX_AMOUNT_SIZE - (orig_amount.toString().length());

				for (int i = 0; i < diff; i++) // padding remaining spaces
				{
					orig_amount.append(null_char);
				}
				amount = orig_amount.toString();
				break;
			}

		} // end switch

		StringBuffer messageForEncryption = new StringBuffer();

		// achAddress
		messageForEncryption.append(achAddress);
		messageForEncryption.append(null_char);

		// chPacketType
		messageForEncryption.append(packetType);

		// packet status
		messageForEncryption.append("T");

		// ach status
		messageForEncryption.append("0");
		messageForEncryption.append(null_char);

		// achCard //card data
		messageForEncryption.append(card_data);
		for (int i = 0; i < (MAX_CARD_DATA_SIZE - card_data.length()); i++) // padding remaining spaces
		{
			messageForEncryption.append(null_char);
		}

		// achAmount
		messageForEncryption.append(amount);

		// achMessage //this is ignored by IAS..so sending nulls
		for (int i = 0; i < MAX_MESSAGE_SIZE; i++) // padding remaining spaces
		{
			messageForEncryption.append(null_char);
		}

		// Program Specific
		messageForEncryption.append(null_char);
		messageForEncryption.append(null_char);
		messageForEncryption.append(null_char);
		messageForEncryption.append(null_char);

		byte[] encrypted_message = encrypt_decrypt(messageForEncryption.toString().getBytes(), 1);
		byte[] packet_header_1 = intToDWord(encrypted_message.length); // packet length
		byte[] packet_header_2 = intToDWord(1); // encryption type
		byte[] entireMessage = new byte[packet_header_1.length + packet_header_2.length
				+ encrypted_message.length];

		for (int i = 0; i < packet_header_1.length; i++)
		{
			entireMessage[i] = packet_header_1[i];
		}
		for (int i = 0; i < packet_header_2.length; i++)
		{
			entireMessage[i + packet_header_1.length] = packet_header_2[i];
		}
		for (int i = 0; i < encrypted_message.length; i++)
		{
			entireMessage[i + packet_header_1.length + packet_header_2.length] = encrypted_message[i];
		}

		logger__.info("Request is : " + messageForEncryption.toString() + ", message size is "
				+ entireMessage.length);

		byte[] serverResponse = sendToNuVisionServer(entireMessage);

		extractResponses(serverResponse);
	}

	private static byte[] intToDWord(int i)
	{
		byte[] dword = new byte[4];
		dword[0] = (byte)(i & 0x00FF);
		dword[1] = (byte)((i >> 8) & 0x000000FF);
		dword[2] = (byte)((i >> 16) & 0x000000FF);
		dword[3] = (byte)((i >> 24) & 0x000000FF);
		return dword;
	}
	
	@Override
	public void processReversal(Reversal reversal, Track2Card track2Card) {
		throw new UnsupportedOperationException("NuVisionProcessor, do not support reversal.");
	}
	
	@Override
	public Object[] processRefund(boolean isNonEmsRequest, 
			   ProcessorTransaction origProcessorTransaction, 
			   ProcessorTransaction refundProcessorTransaction, 
			   String creditCardNumber, 
			   String expiryMMYY) {
		throw new UnsupportedOperationException("NuVisionProcessor, do not support refund.");
	}
	
	@Override
	public Object[] processCharge(Track2Card track2Card, ProcessorTransaction chargeProcessorTransaction) {
		throw new UnsupportedOperationException("NuVisionProcessor, do not support charge.");
	}
	
	@Override
	public boolean isReversalExpired(Reversal reversal) {
		throw new UnsupportedOperationException("NuVisionProcessor, isReversalExpired(Reversal) is not support!");
	}	
}
