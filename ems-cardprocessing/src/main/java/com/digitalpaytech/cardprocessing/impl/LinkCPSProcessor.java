package com.digitalpaytech.cardprocessing.impl;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.digitalpaytech.cardprocessing.CardProcessingManager;
import com.digitalpaytech.cardprocessing.CardProcessor;
import com.digitalpaytech.client.dto.corecps.Authorization;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.service.CardTypeService;
import com.digitalpaytech.service.CcFailLogService;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.ProcessorTransactionService;
import com.digitalpaytech.service.ProcessorTransactionTypeService;
import com.digitalpaytech.service.cps.CoreCPSService;
import com.digitalpaytech.service.kpi.KPIListenerService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.dto.Pair;
import com.netflix.hystrix.exception.HystrixRuntimeException;

public class LinkCPSProcessor extends CardProcessor {
    
    private static final Logger LOG = Logger.getLogger(LinkCPSProcessor.class);
    
    private CoreCPSService coreCPSService;
    private KPIListenerService kpiListenerService;
    private ProcessorTransactionService processorTransactionService;
    private ProcessorTransactionTypeService processorTransactionTypeService;
    private CardTypeService cardTypeService;
    private MailerService mailerService;
    private CcFailLogService ccFailLogService;
    
    public LinkCPSProcessor(final MerchantAccount md, final CommonProcessingService commonProcessingService,
            final EmsPropertiesService emsPropertiesService, final PointOfSale pointOfSale, final CoreCPSService coreCPSService) {
        
        super(md, commonProcessingService, emsPropertiesService);
        this.coreCPSService = coreCPSService;
    }
    
    public final void setServices(final KPIListenerService kpiListenerService, final ProcessorTransactionService processorTransactionService,
        final CardTypeService cardTypeService, final MailerService mailerService,
        final ProcessorTransactionTypeService processorTransactionTypeService, final CcFailLogService ccFailLogService) {
        this.kpiListenerService = kpiListenerService;
        this.processorTransactionService = processorTransactionService;
        this.cardTypeService = cardTypeService;
        this.mailerService = mailerService;
        this.processorTransactionTypeService = processorTransactionTypeService;
        this.ccFailLogService = ccFailLogService;
    }
    
    @Override
    public final Object[] processPostAuth(final PreAuth pd, final ProcessorTransaction ptd) throws CardTransactionException {
        
        final int status;
        final String msg;
        final Object[] ret;
        if (ptd != null && CardProcessingUtil.isNotNullAndIsLink(ptd.getMerchantAccount()) && pd != null && ptd.getPointOfSale() != null) {
            status = this.coreCPSService.sendPostAuthToCoreCPS(pd, ptd);
            if (status == CardProcessingConstants.STATUS_AUTHORIZED) {
                msg = "Succeeded processing the post-auth, ProcessorTransaction id: " + ptd.getId();
                LOG.info(msg);
                ret = new Object[] { true, status };
            } else {
                msg = "Failed to process post-auth, ProcessorTransaction id: " + ptd.getId();
                LOG.warn(msg);
                ret = new Object[] { false, status };
            }
        } else {
            msg = "Unable to process post-auth as PreAuth and/or PointOfSale not found, ProcessorTransaction id: " + ptd.getId();
            LOG.warn(msg);
            ret = new Object[] { false, CardProcessingConstants.STATUS_EMS_UNAVAILABLE };
        }
        
        return ret;
    }
    
    @Override
    public final boolean processPreAuth(final PreAuth preAuth) throws CardTransactionException, CryptoException {
        return this.coreCPSService.sendPreAuthToCoreCPS(preAuth, preAuth.getPointOfSale(), preAuth.getMerchantAccount());
    }
    
    @Override
    public final Object[] processRefund(final boolean isNonEmsRequest, final ProcessorTransaction origProcessorTransaction,
        final ProcessorTransaction refundProcessorTransaction, final String creditCardNumber, final String expiry) throws CryptoException {
        return new Object[] {
            this.coreCPSService.sendRefundToCoreCPS(this.getMerchantAccount(), origProcessorTransaction, refundProcessorTransaction), };
    }
    
    @Override
    public final Object[] processCharge(final Track2Card track2Card, final ProcessorTransaction chargeProcessorTransaction) throws CryptoException {
        return new Object[] { this.coreCPSService.sendChargeToCoreCPS(track2Card, chargeProcessorTransaction) };
    }
    
    @Override
    public final int processLinkStoreAndForward(final ProcessorTransaction processorTransaction, final CardRetryTransaction cardRetryTransaction)
        throws CryptoException {
        return this.coreCPSService.sendStoreAndForwardToCoreCPS(processorTransaction, cardRetryTransaction);
    }
    
    public Pair<String, Authorization> processLinkStoreAndForwardOrFailTransaction(final ProcessorTransaction processorTransaction,
        final CardRetryTransaction cardRetryTransaction, final String cardTypeName) throws CryptoException {
        return this.coreCPSService.sendStoreAndForwardToCoreCPSOrFailTransaction(processorTransaction, cardRetryTransaction, cardTypeName);
    }
    
    @Override
    public void updateObjectsAfterSuccessfulCoreCPSStoreAndForward(final ProcessorTransaction processorTransaction, final Authorization authorization,
        final CardRetryTransaction cardRetryTransaction, final String chargeToken) {
        this.coreCPSService.updateObjectsAfterSuccessfulCoreCPSStoreAndForward(processorTransaction, authorization, cardRetryTransaction,
                                                                               chargeToken);
    }
    
    @Override
    public final int processLinkCPSTransactionAuthorization(final MerchantAccount merchantAccount, final PointOfSale pointOfSale,
        final PreAuth preAuth, final String cardTypeName) throws CryptoException {
        
        preAuth.setPointOfSale(pointOfSale);
        preAuth.setMerchantAccount(merchantAccount);
        preAuth.setCardType(cardTypeName);
        preAuth.setPreAuthDate(new Date());
        preAuth.setIsApproved(false);
        
        try {
            if (processPreAuth(preAuth)) {
                return CardProcessingConstants.STATUS_AUTHORIZED;
            } else {
                return CardProcessingConstants.STATUS_DENIED;
            }
        } catch (HystrixRuntimeException | IllegalStateException hre) {
            // logged in service
            return CardProcessingConstants.CARD_STATUS_EMS_UNAVAILABLE;
        }
    }
    
    @Override
    public final ProcessorTransaction processLinkCPSTransactionRefund(final ProcessorTransaction origPtd, final MerchantAccount merchantAccount)
        throws CryptoException {
        return requestLinkCPSRefund(origPtd, merchantAccount);
    }
    
    @Override
    public final Object[] processLinkCPSTransactionCharge(final String cardToken, final long origProcTransId,
        final ProcessorTransaction chargeProcessorTransaction, final MerchantAccount merchantAccount, final PointOfSale pointOfSale,
        final Permit permit, final PaymentCard paymentCard) throws Exception {
        
        final Purchase purchase = permit.getPurchase();
        
        chargeProcessorTransaction.setProcessorTransactionType(this.processorTransactionService
                .getProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_CHARGE_WITH_NO_REFUNDS));
        chargeProcessorTransaction.setPointOfSale(pointOfSale);
        chargeProcessorTransaction.setPurchasedDate(purchase.getPurchaseGmt());
        chargeProcessorTransaction.setTicketNumber(purchase.getPurchaseNumber());
        chargeProcessorTransaction.setIsApproved(false);
        chargeProcessorTransaction.setMerchantAccount(merchantAccount);
        chargeProcessorTransaction.setIsRfid(chargeProcessorTransaction.isIsRfid());
        chargeProcessorTransaction.setCardHash(StandardConstants.STRING_EMPTY_STRING);
        chargeProcessorTransaction.setCardType(this.cardTypeService.getCardTypesMap().get(CardProcessingConstants.CARD_TYPE_UNKNOWN).getName());
        chargeProcessorTransaction.setProcessingDate(new Date());
        chargeProcessorTransaction.setReferenceNumber(StandardConstants.STRING_EMPTY_STRING);
        chargeProcessorTransaction.setCreatedGmt(new Date());
        chargeProcessorTransaction.setAmount(purchase.getChargedAmount());
        this.processorTransactionService.saveProcessorTransaction(chargeProcessorTransaction);
        
        final Track2Card track2Card = new Track2Card(cardToken);
        final Method method = LinkCPSProcessor.class.getDeclaredMethod("processCharge", new Class[] { Track2Card.class, ProcessorTransaction.class });
        method.setAccessible(true);
        
        try {
            return (Object[]) this.kpiListenerService.invoke(this, method, new Object[] { track2Card, chargeProcessorTransaction });
        } catch (NoSuchMethodException | SecurityException e) {
            LOG.error("Unable to process charge with KPI listener, trying without it.", e);
            return this.processCharge(track2Card, chargeProcessorTransaction);
        }
    }
    
    @Override
    public final Object[] processCharge(final ProcessorTransaction chargeProcessorTransaction, final UUID cardToken) throws CryptoException {
        return processCharge(new Track2Card(cardToken.toString()), chargeProcessorTransaction);
    }
    
    @Override
    public final void updateRefundDetails(final ProcessorTransaction processorTransaction) {
        throw new UnsupportedOperationException("LinkCPSProcessor, updateRefundDetails is not support!");
    }
    
    @Override
    public final boolean isReversalExpired(final Reversal reversal) {
        throw new UnsupportedOperationException("LinkCPSProcessor, isReversalExpired(Reversal) is not support!");
    }
    
    @Override
    public final void processReversal(final Reversal reversal, final Track2Card track2Card) {
        throw new UnsupportedOperationException("LinkCPSProcessor reversal is done within CoreCPSService.");
    }
    
    private ProcessorTransaction requestLinkCPSRefund(final ProcessorTransaction original, final MerchantAccount merchantAccount)
        throws CryptoException {
        
        Object[] response = null;
        ProcessorTransaction refund = CardProcessingUtil.buildRefundProcTrans(original);
        refund.setMerchantAccount(merchantAccount);
        refund.setProcessingDate(new Date());
        if (original.getProcessorTransactionType()
                .getId() == CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_CHARGE_WITH_NO_REFUNDS) {
            refund.setProcessorTransactionType(this.processorTransactionTypeService
                    .findProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_REFUND));
        } else {
            refund.setProcessorTransactionType(this.processorTransactionTypeService
                    .findProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_REFUND));
        }
        final String customerName = merchantAccount.getCustomer().getName();
        
        if (refund.getPointOfSale().getId() == null || refund.getPointOfSale().getId() == 0) {
            final StringBuilder bdr = new StringBuilder().append(CardProcessingManager.UNABLE_TO_INSERT).append(refund)
                    .append(CardProcessingManager.PS_NOT_EXIST).append(customerName).append(CardProcessingManager.ADD_PS_APP);
            LOG.warn(bdr.toString());
            refund.setAuthorizationNumber(CardProcessingManager.EXCEPTION_STRING);
            
        } else if (refund.getTicketNumber() == 0) {
            // BOSS version < 5.0.4 (ticket number is not available) AND transaction wasn't found in EMS
            final StringBuilder bdr = new StringBuilder();
            bdr.append(CardProcessingManager.UNABLE_TO_INSERT).append(refund).append(CardProcessingManager.AS_STR).append(customerName)
                    .append(CardProcessingManager.BOSS_VER_PRIOR).append(CardProcessingManager.PLS_UPGRADE);
            LOG.warn(bdr.toString());
            refund.setAuthorizationNumber(CardProcessingManager.EXCEPTION_STRING);
        } else {
            try {
                final Method method = LinkCPSProcessor.class.getDeclaredMethod(CardProcessingManager.PROCESS_REFUND, new Class[] { boolean.class,
                    ProcessorTransaction.class, ProcessorTransaction.class, String.class, String.class });
                method.setAccessible(true);
                response = (Object[]) this.kpiListenerService.invoke(this, method, new Object[] { false, original, refund, null, null });
            } catch (NoSuchMethodException | SecurityException e) {
                LOG.error("Not able to invoke " + CardProcessingManager.PROCESS_REFUND + " with KPI listener service, trying without KPI", e);
                response = processRefund(false, original, refund, null, null);
            } catch (Exception e) {
                // logged in service
                this.ccFailLogService.logFailedCcTransaction(refund.getPointOfSale(), refund.getMerchantAccount(), refund.getProcessingDate(),
                                                             refund.getPurchasedDate(), refund.getTicketNumber(),
                                                             CardProcessingConstants.CC_TYPE_REFUND, e.getMessage());
                
                final StringBuilder bdr = new StringBuilder().append(CardProcessingManager.PROCTRANS_REFUND_PROBLEM).append(original.toString())
                        .append(WebCoreConstants.NEXT_LINE_SYMBOL).append(CardProcessingManager.PROCTRANS_REFUND).append(refund)
                        .append(WebCoreConstants.NEXT_LINE_SYMBOL).append(CardProcessingManager.CUSTOMER_NAME).append(refund)
                        .append(WebCoreConstants.NEXT_LINE_SYMBOL);
                
                this.mailerService.sendAdminErrorAlert("CardProcessingManager error communicating with CoreCPS, ", bdr.toString(), e);
                
                refund.setAuthorizationNumber(CardProcessingManager.EXCEPTION_STRING);
                this.processorTransactionService.saveProcessorTransaction(refund);
            }
        }
        
        if (response != null) {
            refund = (ProcessorTransaction) response[0];
            if (refund != null && !refund.isIsApproved()) {
                this.ccFailLogService.logFailedCcTransaction(refund.getPointOfSale(), refund.getMerchantAccount(), refund.getProcessingDate(),
                                                             refund.getPurchasedDate(), refund.getTicketNumber(),
                                                             CardProcessingConstants.CC_TYPE_REFUND, refund.getAuthorizationNumber());
            }
        }
        return refund;
    }
}
