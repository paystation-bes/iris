package com.digitalpaytech.cardprocessing.impl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.digitalpaytech.cardprocessing.CardProcessor;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DPTResponseCodesMapping;

/**
 * First Horizon messages are based on TPS 600 Format; each message type is a
 * character string, with position-dependant fields.
 * 
 * For further details about the FHMS message format, refer to the following
 * documents: -First Horizon Merchant Services TPS 600 Message Specification
 * -First Horizon Internet Gateway Interface Specification for POS/IPOS
 * Terminals, V1.5
 */
public class FirstHorizonProcessor extends CardProcessor 
{

	private static Logger logger__ = Logger.getLogger(FirstHorizonProcessor.class);

	private final static int TRACK2_DATA_FIELD_LENGTH = 76;
	private final static String STX_CHAR = "\02";
	private final static String ETX_CHAR = "\03";
	private final static String PROCESSOR_ROUTING = "I1.";
	private final static String NETWORK_ROUTING_CODE = "A00700";
	private final static String TEST_MERCHANT_ID = "000192000904";
	private final static String TEST_TERMINAL_ID = "110";
	private final static String BANK_ID = "0001";
	private final static String NETWORK_MANAGEMENT_INFO_CODE = "000";
	private final static String POS_ENTRY_MODE__TRACK2 = "022";
	private final static String POS_ENTRY_MODE__PAN = "012";
	private final static String POINT_OF_SERVICE_CONDITION_CODE = "0000000000";
	private final static String POS_DEVICE_CAPABILITY_CODE = "02";
	private final static String EXTENDED_PAYMENT_CODE = "00";
	private final static String DOWNLOAD_FLAG = "0";
	private final static String ERROR_BITMAP_TYPE = "99";
	private final static String CC_AUTH_REQUEST_MESSAGE_TYPE = "0100";
	private final static String CC_AUTH_BITMAP_TYPE = "01";
	private final static String CC_AUTH_PROCESSING_CODE = "004000";
	private final static String CC_AUTH_DOWNLOAD_FLAG = "0";
	private final static String CC_AUTH_RESPONSE_MESSAGE_TYPE = "0110";
	private final static String CC_AUTH_APPROVED_BITMAP_TYPE = "90";
	private final static String CC_SALE_MESSAGE_TYPE = "0200";
	private final static String CC_SALE_BITMAP_TYPE = "02";
	private final static String CC_SALE_PROCESSING_CODE = "004000";
	private final static String CC_SALE_EXTENDED_PAYMENT_CODE = "00";
	private final static String CC_SALE_RESPONSE_MESSAGE_TYPE = "0210";
	private final static String CC_02x0_APPROVED_BITMAP_TYPE = "91";
	private final static String CC_RETURN_MESSAGE_TYPE = "0200";
	private final static String CC_RETURN_BITMAP_TYPE = "02";
	private final static String CC_RETURN_PROCESSING_CODE = "204000";
	private final static String CC_RETURN_RESPONSE_MESSAGE_TYPE = "0210";
	private final static String CC_RETURN_APPROVED_BITMAP_TYPE = "91";
	private final static String CC_POST_AUTH_MESSAGE_TYPE = "0220";
	private final static String CC_POST_AUTH_BITMAP_TYPE = "04";
	private final static String CC_POST_AUTH_PROCESSING_CODE = "004000";
	private final static String CC_POST_AUTH_RESPONSE_MESSAGE_TYPE = "0230";

	private String terminalId_;
	private String merchantId_;
	private String destinationUrlString_;

	// parsed responses from the processor's server
	private String actionCode_;
	private String authOrErrorCode_;
	private String responseMessage_;
	private String processorTransactionId_;
	private String ps2000Data_;

	public FirstHorizonProcessor(MerchantAccount md, CommonProcessingService commonProcessingService,
	        EmsPropertiesService emsPropertiesService) {
		super(md, commonProcessingService, emsPropertiesService);
		merchantId_ = md.getField1();
		terminalId_ = md.getField2();
		destinationUrlString_ = commonProcessingService.getDestinationUrlString(md.getProcessor());
		if (logger__.isDebugEnabled()) {
			logger__.debug("FirstHorizonProcessor, destinationUrlString: " + destinationUrlString_);
		}
	}

	public boolean processPreAuth(PreAuth pd) throws CardTransactionException
	{
		logger__.debug("Performing Pre-Auth for First Horizon transaction");
		String reference_number = getCommonProcessingService().getNewReferenceNumber(getMerchantAccount());
		StringBuffer request = createPreAuthRequest(pd.getCardData(), reference_number, CardProcessingUtil.getCentsAmountInDollars(pd.getAmount()).floatValue(), 0.0f);
		sendRequest(request);

		// Check for invalid response
		if ((actionCode_ == null) || (actionCode_.equals("")) || (actionCode_.length() == 0)) 
		{
			logger__.warn("Error in response - no action code");
			throw new CardTransactionException(
			        "First Horizon Processor is offline ");
		}

		pd.setProcessingDate(new Date());
		pd.setReferenceNumber(reference_number);
		pd.setProcessorTransactionId(processorTransactionId_);
		pd.setResponseCode(actionCode_);

        // The amount available in this account is returned in the display text field.
        // We need to parse it and compare that value to the transaction amount for FHMS pre-auth;
        // good response code does not necessarily mean that the transaction amount is available.
		if (actionCode_.equals(CC_AUTH_APPROVED_BITMAP_TYPE)) 
		{
			pd.setIsApproved(true);
			pd.setCardData(pd.getCardData() + "|" + ps2000Data_);
			pd.setAuthorizationNumber(authOrErrorCode_);
		} 
		else if (actionCode_.equals(ERROR_BITMAP_TYPE)) 
		{
			logger__.warn("Error in response : " + responseMessage_);
			pd.setIsApproved(false);
			pd.setResponseCode(authOrErrorCode_);
		}

		return (pd.isIsApproved());
	}

	public Object[] processPostAuth(PreAuth pd, ProcessorTransaction ptd) throws CardTransactionException 
	{
		// Determine data to send to processor
		String cardData = pd.getCardData().substring(0, pd.getCardData().lastIndexOf("|"));
		String ps2000Data = pd.getCardData().substring(pd.getCardData().lastIndexOf("|") + 1);

		// Create request
		StringBuffer request = createPostAuthRequest(cardData, pd.getReferenceNumber(), pd.getAuthorizationNumber(), CardProcessingUtil.getCentsAmountInDollars(pd.getAmount()).floatValue(), ps2000Data);

		// Send request
		sendRequest(request);

		// Set ProcessorTransactionData values from processor
		ptd.setProcessingDate(new Date());
		ptd.setReferenceNumber(pd.getReferenceNumber());
		ptd.setProcessorTransactionId(processorTransactionId_);

		// Approved
		if (actionCode_.equals(CC_02x0_APPROVED_BITMAP_TYPE)) 
		{
			ptd.setAuthorizationNumber(authOrErrorCode_);
			ptd.setIsApproved(true);
			return new Object[] {true, actionCode_};
		}

		// Declined
		ptd.setAuthorizationNumber(null);
		ptd.setIsApproved(false);
		return new Object[] {false, actionCode_};
	}

	public Object[] processRefund(boolean isNonEmsRequest, ProcessorTransaction origPtd, ProcessorTransaction refundPtd,
	        String ccNumber, String expiryMMYY) throws CardTransactionException 
	{
		logger__.debug("Performing Refund for First Horizon transaction");
		// Create request
		StringBuffer request = createRefundRequest(ccNumber, expiryMMYY, origPtd.getReferenceNumber(), CardProcessingUtil.getCentsAmountInDollars(refundPtd.getAmount()).floatValue());

		// Send request
		sendRequest(request);

		// Set processor transaction data values
		refundPtd.setProcessingDate(new Date());
		refundPtd.setReferenceNumber(origPtd.getReferenceNumber());
		refundPtd.setProcessorTransactionId(processorTransactionId_);

		// Approved
		if (actionCode_.equals(CC_02x0_APPROVED_BITMAP_TYPE)) 
		{
			refundPtd.setAuthorizationNumber(authOrErrorCode_);
			refundPtd.setIsApproved(true);
			return new Object[] {DPTResponseCodesMapping.CC_APPROVED_VAL__7000, actionCode_};
		}

		// Declined
		refundPtd.setAuthorizationNumber(null);
		refundPtd.setIsApproved(false);

		// if request is non-EMS (eg from BOSS) then map response.
		if (isNonEmsRequest) 
		{
			return new Object[] {mapErrorResponse(authOrErrorCode_, responseMessage_), actionCode_};
		}

		// EMS request
		int response_code = -1;
		try 
		{
			response_code = Integer.parseInt(authOrErrorCode_);
		} catch (Exception e) 
		{
			throw new CardTransactionException("Expecting numeric response, but recieved: " + authOrErrorCode_, e);
		}
		return new Object[] {response_code, actionCode_};
	}

	public Object[] processCharge(Track2Card track2Card, ProcessorTransaction chargePtd) 
			throws CardTransactionException 
	{
		String reference_number = getCommonProcessingService().getNewReferenceNumber(getMerchantAccount());

		String request = createChargeRequest(track2Card, reference_number, chargePtd.getAmount());

		String response = sendRequest(request);

		return handleChargeResponse(response, chargePtd, reference_number, track2Card);
	}

	/*
	 * This creates a Credit Card Authorization type message.
	 */
	private StringBuffer createPreAuthRequest(String track2Data, String referenceNumber, Float originalTransAmount,
	        Float amountToRefund) throws CardTransactionException 
	{
		StringBuffer message = new StringBuffer();

		message.append(STX_CHAR);
		message.append(PROCESSOR_ROUTING);
		message.append(NETWORK_ROUTING_CODE);
		message.append(CC_AUTH_REQUEST_MESSAGE_TYPE);
		message.append(CC_AUTH_BITMAP_TYPE);
		message.append(CC_AUTH_PROCESSING_CODE);
		message.append(formatDollarAmount(originalTransAmount, 9));
		message.append(POS_ENTRY_MODE__TRACK2);
		message.append(POINT_OF_SERVICE_CONDITION_CODE);
		message.append(BANK_ID);
		message.append(terminalId_);
		message.append(merchantId_);
		message.append(formatTrack2Data(track2Data)); // cardholder information needs to be 76 chars long
		message.append("00000000"); // all zeroes as requested by FHMS
		message.append(formatDollarAmount(amountToRefund, 9));
		message.append(NETWORK_MANAGEMENT_INFO_CODE);
		message.append(POS_DEVICE_CAPABILITY_CODE);
		message.append(STX_CHAR);
		message.append(ETX_CHAR);

		StringTokenizer stk = new StringTokenizer(message.toString(), STX_CHAR);
		String message_after_stx = stk.nextToken();
		byte[] message_bytes = message_after_stx.getBytes();
		byte[] lrc = null;
		lrc = CardProcessingUtil.computeLRC(message_bytes);
		message.append(new String(lrc));

		/*
		 * if ( cc_logger__.isDebugEnabled() ) {
		 * cc_logger__.info("FirstHorizon - Authorization request is: " +
		 * message.toString()); }
		 */
		String log_message = message.toString().replaceAll(track2Data, track2Data.replaceAll("[^=]", "X"));

		logger__.info("Authorization request to First Horizon processor is " + log_message);

		return message;
	}

	private static String SIX_SPACES = "      ";

	/*
	 * This creates a Credit Card Prior Authorization with PS2000 data type
	 * message.
	 */
	private StringBuffer createPostAuthRequest(String track2Data, String referenceNumber, String authNumber,
	        Float originalTransAmount, String ps2000Data)
	        throws CardTransactionException 
	{
		StringBuffer message = new StringBuffer();

		message.append(STX_CHAR);
		message.append(PROCESSOR_ROUTING);
		message.append(NETWORK_ROUTING_CODE);
		message.append(CC_POST_AUTH_MESSAGE_TYPE);
		message.append(CC_POST_AUTH_BITMAP_TYPE);
		message.append(CC_POST_AUTH_PROCESSING_CODE);
		message.append(formatDollarAmount(originalTransAmount, 9));
		message.append(POS_ENTRY_MODE__TRACK2);
		message.append(POINT_OF_SERVICE_CONDITION_CODE);
		message.append(BANK_ID);
		if (authNumber.length() == 6) 
		{
			message.append(authNumber);
		} 
		else if (authNumber.length() < 6) 
		{
			message.append(authNumber + SIX_SPACES.substring(authNumber.length()));
		}
		message.append(terminalId_);
		message.append(merchantId_);
		message.append(formatTrack2Data(track2Data)); // cardholder information needs to be 76 chars long
		message.append("00000000"); // all zeroes as requested by FHMS
		message.append("00"); // extended payment code (always "00")
		message.append(NETWORK_MANAGEMENT_INFO_CODE);
		message.append("00"); // Additional response data; don't need this (not doing AVS)
		message.append(ps2000Data.substring(1, 2));
		message.append(ps2000Data.substring(2, 17));
		message.append(ps2000Data.substring(17, ps2000Data.length() - 1));
		message.append(POS_DEVICE_CAPABILITY_CODE);
		message.append(STX_CHAR);
		message.append(ETX_CHAR);

		StringTokenizer stk = new StringTokenizer(message.toString(), STX_CHAR);
		String message_after_stx = stk.nextToken();
		byte[] message_bytes = message_after_stx.getBytes();
		byte[] lrc = null;
		lrc = CardProcessingUtil.computeLRC(message_bytes);
		message.append(new String(lrc));

		/*
		 * if ( cc_logger__.isDebugEnabled() ) {
		 * cc_logger__.info("FirstHorizon - Post-Auth request is: " +
		 * message.toString()); }
		 */
		String log_message = message.toString().replaceAll(track2Data, track2Data.replaceAll("[^=]", "X"));
		logger__.info("Post-authorization request to First Horizon processor is " + log_message);

		return message;
	}

	/*
	 * This creates a Credit Card Sale type message.
	 */
	private String createChargeRequest(Track2Card track2Card, String referenceNumber, int amountInCents)
	        throws CardTransactionException 
	        {
		StringBuilder sb = new StringBuilder();

		sb.append(STX_CHAR);
		sb.append(PROCESSOR_ROUTING);
		sb.append(NETWORK_ROUTING_CODE);
		sb.append(CC_SALE_MESSAGE_TYPE);
		sb.append(CC_SALE_BITMAP_TYPE);
		sb.append(CC_SALE_PROCESSING_CODE);
		sb.append(formatAmount(amountInCents));
		if (track2Card.isCardDataCreditCardTrack2Data()) 
		{
			sb.append(POS_ENTRY_MODE__TRACK2);
		} 
		else 
		{
			sb.append(POS_ENTRY_MODE__PAN);
		}
		sb.append(POINT_OF_SERVICE_CONDITION_CODE);
		sb.append(BANK_ID);
		sb.append(terminalId_);
		sb.append(merchantId_);
		String card_data = formatCardData(track2Card);
		sb.append(card_data); // cardholder information needs to be 76 chars long
		sb.append("00000000"); // all zeroes as requested by FHMS
		sb.append("000000000"); // Cash back amount is $0.00 since this is a sale
		sb.append(EXTENDED_PAYMENT_CODE);
		sb.append(NETWORK_MANAGEMENT_INFO_CODE);
		sb.append(POS_DEVICE_CAPABILITY_CODE);
		sb.append(STX_CHAR);
		sb.append(ETX_CHAR);

		StringTokenizer stk = new StringTokenizer(sb.toString(), STX_CHAR);
		String message_after_stx = stk.nextToken();
		byte[] message_bytes = message_after_stx.getBytes();
		byte[] lrc = null;
		lrc = CardProcessingUtil.computeLRC(message_bytes);
		sb.append(new String(lrc));

		String request = sb.toString();

		String log_message = request.replaceAll(card_data, card_data.replaceAll("[^=]", "X"));
		logger__.info("Charge Request: " + log_message);

		return request;
	}

	/*
	 * This creates a Credit Card Return type message.
	 */
	private StringBuffer createRefundRequest(String ccNumber, String expiryDate, String referenceNumber, Float transAmount)
	        throws CardTransactionException 
	{
		StringBuffer message = new StringBuffer();

		message.append(STX_CHAR);
		message.append(PROCESSOR_ROUTING);
		message.append(NETWORK_ROUTING_CODE);
		message.append(CC_RETURN_MESSAGE_TYPE);
		message.append(CC_RETURN_BITMAP_TYPE);
		message.append(CC_RETURN_PROCESSING_CODE);
		message.append(formatDollarAmount(transAmount, 9));
		message.append(POS_ENTRY_MODE__PAN);
		message.append(POINT_OF_SERVICE_CONDITION_CODE);
		message.append(BANK_ID);
		message.append(terminalId_);
		message.append(merchantId_);
		message.append(formatTrackData(ccNumber, expiryDate)); // track data
		message.append("00000000"); // all zeroes as requested by FHMS
		message.append("000000000"); // cash back amount (always zero filled)
		message.append(EXTENDED_PAYMENT_CODE);
		message.append(NETWORK_MANAGEMENT_INFO_CODE);
		message.append(POS_DEVICE_CAPABILITY_CODE);
		message.append(STX_CHAR);
		message.append(ETX_CHAR);

		StringTokenizer stk = new StringTokenizer(message.toString(), STX_CHAR);
		String message_after_stx = stk.nextToken();
		byte[] message_bytes = message_after_stx.getBytes();
		byte[] lrc = null;
		lrc = CardProcessingUtil.computeLRC(message_bytes);
		message.append(new String(lrc));

		/*
		 * if ( cc_logger__.isDebugEnabled() ) {
		 * cc_logger__.info("First Horizon Processor - Refund request is: " +
		 * message.toString()); }
		 */
		String track_data = ccNumber.trim() + "=" + expiryDate.trim().substring(2) + expiryDate.trim().substring(0, 2);
		String log_message = message.toString().replaceAll(track_data, track_data.replaceAll("[^=]", "X"));
		logger__.info("CC Refund request to First Horizon processor is " + log_message);

		return message;
	}

	// Private function to actually send the request to the First Horizon server
	// Sends message to the server and extracts fields from received response.
	private void sendRequest(StringBuffer message) throws CardTransactionException 
	{
		URLConnection connection = null;
		try 
		{
			URL theURL = new URL(destinationUrlString_);
			connection = theURL.openConnection();
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);
			connection.setAllowUserInteraction(false);
			connection.setRequestProperty("Content-Type", "x-TPS4.0/x-HostCapture-600");
			connection.setRequestProperty("Content-Length", Integer.toString(message.length()));

			DataOutputStream out = new DataOutputStream(connection.getOutputStream());
			connection.connect();

			out.writeBytes(message.toString());
			out.flush();
			out.close();

			// Get the response
			InputStream is = (connection.getInputStream());
			BufferedReader b = new BufferedReader(new InputStreamReader(is));
			String s = null;
			String responseString = "";
			while ((s = b.readLine()) != null) 
			{
				responseString += s;
			}
			b.close();

			// Extract data from the response
			extractResponses(responseString);

		} 
		catch (IOException e) 
		{
			throw new CardTransactionException("First Horizon is offline", e);
		}
	}

	private String sendRequest(String message) throws CardTransactionException 
	{
		URLConnection connection = null;
		try 
		{
			URL theURL = new URL(destinationUrlString_);
			connection = theURL.openConnection();
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);
			connection.setAllowUserInteraction(false);
			connection.setRequestProperty("Content-Type", "x-TPS4.0/x-HostCapture-600");
			connection.setRequestProperty("Content-Length", Integer.toString(message.length()));

			DataOutputStream out = new DataOutputStream(connection.getOutputStream());
			connection.connect();

			out.writeBytes(message);
			out.flush();
			out.close();

			// Get the response
			InputStream is = (connection.getInputStream());
			BufferedReader b = new BufferedReader(new InputStreamReader(is));
			String s = null;
			String response = "";
			while ((s = b.readLine()) != null) 
			{
				response += s;
			}
			b.close();

			return (response);
		} catch (IOException e) 
		{
			throw new CardTransactionException("First Horizon is offline", e);
		}
	}

	private Object[] handleChargeResponse(String response, ProcessorTransaction ptd, String reference_number,
	        Track2Card track2Card) throws CardTransactionException 
	{
		logger__.info("Charge Response: " + response);
	
		String auth_code = null;
		String response_message = "";
		String response_code = "";
		String proc_trans_id = null;
		String ps2000_data = null;

		String message = response.substring(1); // strip off leading STX character
		// String trans_type = message.substring(0, 4); // Message type indicator
		String action_code = message.substring(4, 6); // bitmap type

		if (action_code.equals(CC_02x0_APPROVED_BITMAP_TYPE)) 
		{
			proc_trans_id = message.substring(22, 30); // retrieval reference number
			auth_code = message.substring(30, 36); // auth id assigned by the authorization institution
			ps2000_data = "@" + message.substring(38, 58) + "@"; // payment svc indicator, transaction ID, Visa
		} 
		else 
		{
			response_message = message.substring(28, 48).trim();
			ps2000_data = "@" + message.substring(8, 28) + "@"; // payment svc indicator, transaction ID, Visa
			response_code = message.substring(48, 51); // response code for the error
		}

		StringBuilder sb = new StringBuilder(200);
		sb.append("Charge Response:  ActionCode: ");
		sb.append(action_code);
		sb.append(", Response Code: ");
		sb.append(response_code);
		sb.append(", Response Message: ");
		sb.append(response_message);
		sb.append(", Authorization Number: ");
		sb.append(auth_code);
		sb.append(", ProcessorTransactionId: ");
		sb.append(proc_trans_id);
		sb.append(", PS2000 Data: ");
		sb.append(ps2000_data);
		logger__.info(sb.toString());
		

		// Set ProcessorTransactionData values from processor
		ptd.setProcessingDate(new Date());
		ptd.setReferenceNumber(reference_number);
		ptd.setProcessorTransactionId(proc_trans_id);

		// Approved
		if (action_code.equals(CC_02x0_APPROVED_BITMAP_TYPE)) 
		{
			ptd.setAuthorizationNumber(auth_code);
			ptd.setIsApproved(true);
			return new Object[] {DPTResponseCodesMapping.CC_APPROVED_VAL__7000, action_code};
		}

		// Declined
		ptd.setAuthorizationNumber(null);
		ptd.setIsApproved(false);
		return new Object[] {mapErrorResponse(response_code, response_message), action_code};
	}

	private void extractResponses(String responseString) 
	{
		logger__.info("First Horizon Response : " + responseString);

		String message = responseString.substring(1); // strip off leading STX character
		String transactionType = message.substring(0, 4); // Message type indicator
		actionCode_ = message.substring(4, 6); // bitmap type

		if (actionCode_.equals(ERROR_BITMAP_TYPE)) 
		{
			responseMessage_ = message.substring(28, 48).trim();
			logger__.error("Error from First Horizon server: " + responseMessage_);
			ps2000Data_ = "@" + message.substring(8, 28) + "@"; // payment svc indicator, transaction ID, Visa validation code
			authOrErrorCode_ = message.substring(48, 51); // response code for the error
		} 
		else if (transactionType.equals(CC_AUTH_RESPONSE_MESSAGE_TYPE) && actionCode_.equals(CC_AUTH_APPROVED_BITMAP_TYPE)) // Authorization response
		{
			authOrErrorCode_ = message.substring(21, 27); // auth id assigned by the authorization institution
			processorTransactionId_ = "";
			ps2000Data_ = "@" + message.substring(29, 49) + "@"; // payment svc indicator, transaction ID, Visa  validation code
		} 
		else if (transactionType.equals(CC_RETURN_RESPONSE_MESSAGE_TYPE) && actionCode_.equals(CC_RETURN_APPROVED_BITMAP_TYPE)) 
		{
			processorTransactionId_ = message.substring(22, 30); // retrieval reference number
			authOrErrorCode_ = message.substring(30, 36); // auth id assigned by the authorization institution
			ps2000Data_ = "@" + message.substring(38, 58) + "@"; // payment svc indicator, transaction ID, Visa validation code
		} 
		else if ((transactionType.equals(CC_SALE_RESPONSE_MESSAGE_TYPE) || transactionType.equals(CC_POST_AUTH_RESPONSE_MESSAGE_TYPE))
		        && actionCode_.equals(CC_02x0_APPROVED_BITMAP_TYPE)) 
		{
			processorTransactionId_ = message.substring(22, 30); // retrieval reference number
			authOrErrorCode_ = message.substring(30, 36); // auth id assigned by the authorization institution
			ps2000Data_ = "@" + message.substring(38, 58) + "@"; // payment svc indicator, transaction ID, Visa validation code
		}
	}

	private final static String ERR_INVALID_DATA__130 = "INVLD DATA";
	private final static String ERR_PLEASE_RETRY__130 = "PLEASE RETRY";

	private int mapErrorResponse(String responseCode, String responseMsg) throws CardTransactionException 
	{
		int response_code = -1;
		try 
		{
			response_code = Integer.parseInt(responseCode);
		} 
		catch (Exception e) 
		{
			throw new CardTransactionException("Expecting numeric response, but recieved: " + responseCode, e);
		}

		switch (response_code) 
		{
    		// INVALID EXPIRATION DATE (FHMS docs)
    		case 107:
    			// EXPIRED CARD (FHMS docs)
    		case 654:
    			return DPTResponseCodesMapping.CC_EXPIRED_VAL__7001;
    
    			// INVALID TERM ID (FHMS prod server)
    			// MERCHANT ID ERR-CALL (FHMS prod server)
    			// CARD NUMBER NOT NUMERIC (FHMS docs - already checked when
    			// receiving request to ems)
    			// INVALID TERMINAL ID (FHMS docs)
    			// INVALID MERCHANT ID (FHMS docs)
    			// INVALID SE NUMBER (FHMS docs)
    		case 124:
    			// INVALID MERCHANT NO (FHMS prod server)
    		case 227:
    			throw new CardTransactionException(
    			        "Response indicates invalid merchant: " + responseCode + " & " + responseMsg);
    
    			// INVALID CARD (FHMS prod server)
    			// CAPTURE NOT ALLOWED (FHMS docs)
    			// INVALID ACCOUNT NUMBER (FHMS docs)
    		case 125:
    			return (DPTResponseCodesMapping.CC_CARD_TYPE_NOT_SETUP_VAL__7076);
    
    			// INVLD DATA (FHMS prod server)
    		case 130:
    			if (ERR_INVALID_DATA__130.equals(responseMsg))
    				return (DPTResponseCodesMapping.CC_BAD_CREDIT_CARD_DATA_VAL__7045);
    			else if (ERR_PLEASE_RETRY__130.equals(responseMsg))
    				return (DPTResponseCodesMapping.CC_CANNOT_PROCESS_PLEASE_RETRY_VAL__7004);
    
    			// If other msg, fall through to invalid response
    			break;
    
    		// CALL AUTH CENTER (FHMS prod server)
    		// CALL AUTH CENTER - REFERRAL (FHMS docs)
    		// SPECIAL REFERRAL WITH REF CODE (FHMS docs)
    		case 601:
    			// PICK UP CARD (FHMS prod server/docs)
    		case 641:
    			return (DPTResponseCodesMapping.CC_PLACE_CALL_VAL__7022);
    
    			// DECLINED (FHMS prod server/docs)
    		case 605:
    			// AUTH DECLINED (FHMS prod server)
    		case 614:
    			// AUTH DECLINED (FHMS prod server)
    		case 651:
    			// TRANS DENIED (FHMS prod server)
    		case 662:
    			return (DPTResponseCodesMapping.CC_DECLINED_VAL__7003);
    
    			// AMOUNT ERROR (FHMS prod server)
    			// INVALID AMOUNT (FHMS docs)
    		case 613:
    			return (DPTResponseCodesMapping.CC_INVALID_AMOUNT_VAL__7033);
		}

		throw new CardTransactionException("Unknown Response Code: "
		        + response_code + " & Response Message: " + responseMsg);
	}

	/*
	 * Returns a formatted credit card number String for inclusion in TPS 600
	 * type messages. Spaces are added to the end (right) of the String so that
	 * it is CC_NUMBER_FIELD_LENGTH long.
	 */
	private String formatTrackData(String ccNumber, String expiryDate)
	        throws CardTransactionException 
	{
		String track_data = ccNumber.trim() + "="
		        + expiryDate.trim().substring(2)
		        + expiryDate.trim().substring(0, 2);

		if (track_data.length() > TRACK2_DATA_FIELD_LENGTH) 
		{
			throw new CardTransactionException("Track data field too long: "
			        + track_data);
		}

		String td_trailing_spaces = "";
		for (int i = 0; i < TRACK2_DATA_FIELD_LENGTH; i++) 
		{
			td_trailing_spaces += " ";
		}

		return track_data + td_trailing_spaces.substring(track_data.length());
	}

	private final static char SPACE_CHAR = ' ';

	private final static String PADDING_FIELD;
	static 
	{
		StringBuilder sb = new StringBuilder(TRACK2_DATA_FIELD_LENGTH);
		for (int i = 0; i < TRACK2_DATA_FIELD_LENGTH; i++) 
		{
			sb.append(SPACE_CHAR);
		}
		PADDING_FIELD = sb.toString();
	}

	private String formatCardData(Track2Card track2Card) throws CardTransactionException
	{
		String card_data = null;
		if (track2Card.isCardDataCreditCardTrack2Data()) 
		{
			card_data = track2Card.getDecryptedCardData();
		} 
		else 
		{
			card_data = track2Card.getPrimaryAccountNumber()
			        + CardProcessingConstants.TRACK_2_DELIMITER
			        + track2Card.getExpirationYear()
			        + track2Card.getExpirationMonth();
		}

		if (card_data.length() > TRACK2_DATA_FIELD_LENGTH) 
		{
			throw new CardTransactionException("Card data field too long: "
			        + card_data.length());
		}

		return PADDING_FIELD.substring(card_data.length()) + card_data;
	}

	/*
	 * Returns a formatted Track2 data String for inclusion in TPS 600 type
	 * messages. Spaces are added to the front of the String so that it is
	 * TRACK2_DATA_FIELD_LENGTH long.
	 */
	private String formatTrack2Data(String track2Data) throws CardTransactionException 
	{
		if (track2Data.length() > TRACK2_DATA_FIELD_LENGTH) 
		{
			throw new CardTransactionException("Track 2 data too long!");
		}

		String track2_leading_spaces = "";
		for (int i = 0; i < TRACK2_DATA_FIELD_LENGTH; i++) 
		{
			track2_leading_spaces += " ";
		}

		return track2_leading_spaces.substring(track2Data.length()) + track2Data;
	}

	/*
	 * Returns a formatted dollar amount (String) for inclusion in TPS 600 type
	 * messages. Decimal point is removed and zeroes are padded on the front of
	 * the message to make the String the required length.
	 */
	private String formatDollarAmount(Float dollars, int fieldLength) 
	{

		int amount_value = Math.round(dollars.floatValue() * 100);
		String amount = new Integer(amount_value).toString();

		String leading_zeros = "";
		for (int i = 0; i < fieldLength; i++) 
		{
			leading_zeros += "0";
		}

		// Add some leading zeros so that the field length is fieldLength
		String formatted_dollar_amount = leading_zeros.substring(amount.trim().length()) + amount;

		return formatted_dollar_amount;
	}

	private final static char ZERO_CHAR = '0';

	private final static int TRANS_AMOUNT_FIELD_LENGTH = 9;
	private final static String TRANS_AMOUNT_PADDING_FIELD;
	static 
	{
		StringBuilder sb = new StringBuilder(TRANS_AMOUNT_FIELD_LENGTH);
		for (int i = 0; i < TRANS_AMOUNT_FIELD_LENGTH; i++) 
		{
			sb.append(ZERO_CHAR);
		}
		TRANS_AMOUNT_PADDING_FIELD = sb.toString();
	}

	private String formatAmount(int cents) 
	{
		String amount = Integer.toString(cents);

		// Add some leading zeros so that the field length is fieldLength
		String formatted_amount = TRANS_AMOUNT_PADDING_FIELD.substring(amount
		        .length()) + amount;

		return formatted_amount;
	}

	@Override
	public void processReversal(Reversal reversal, Track2Card track2Card) 
	{
		throw new UnsupportedOperationException("FirstHorizonProcessor, Reversal not supported for this processor");
	}

	@Override
	public boolean isReversalExpired(Reversal reversal) {
		throw new UnsupportedOperationException("FirstHorizonProcessor, isReversalExpired(Reversal) is not support!");
	}	
}
