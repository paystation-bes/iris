package com.digitalpaytech.cardprocessing;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.data.TransactionData;
import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.domain.ExtensiblePermit;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.PreAuthHolding;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.dto.ProcessorInfo;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.PaystationCommunicationException;

@SuppressWarnings("checkstyle:abbreviationaswordinname")
public interface CardProcessingManager {
    String CAN_NOT_LOCAT_PRC_FOR_MERCH_ACCT = "Could not locate Processor for Merchant Account: ";
    String EXCEPTION_STRING = "EXCEPTION";
    String UNABLE_TO_INSERT = "Unable to insert refund attempt into ProcessorTransaction: \n\n";
    String PROCESS_REFUND = "processRefund";
    String PS_NOT_EXIST = " as the PS does not exist for customer: ";
    String AS_STR = " as ";
    String BOSS_VER_PRIOR = " is using BOSS version prior to 5.0.4 -> ticket number is " + "unavailable AND transaction couldn't be found.  \n ";
    String ADD_PS_APP = ". \n PLEASE ADD PS ASAP.";
    String PLS_UPGRADE = "PLEASE UPGRADE ASAP.";
    String PROCTRANS_REFUND_PROBLEM = "processTransactionRefund problem, ProcessorTransaction origPtd:";
    String PROCTRANS_REFUND = "ProcessorTransaction refund_ptd: ";
    String CUSTOMER_NAME = "customerName: ";
    
    List<ProcessorTransaction> getRefundableTransactions(String authNumber, String cardNumber, String cardExpiryMMYY, Date startDate, Date endDate,
        Integer merchantAccountId, int customerId) throws CardTransactionException;
    
    Reversal createReversal(String originalMti, String originalProcessingCode, BigDecimal originalTransAmount, String originalReferenceNumber,
        String originalDate, String cardData, String lastResponseCode, int merchantAccountId, Date purchasedDate, int ticketNumber, int pointOfSaleId)
        throws CardTransactionException;
    
    void processReversal(Reversal reversal);
    
    ProcessorTransaction processLinkCPSTransactionRefund(ProcessorTransaction origProcessorTransaction, MerchantAccount merchantAccount)
        throws CryptoException;
    
    ProcessorTransaction processTransactionRefund(ProcessorTransaction origProcessorTransaction, String cardNumber, String cardExpiryMMYY,
        boolean isNonEmsRequest);
    
    ProcessorTransaction processCPSTransactionRefund(ProcessorTransaction origPtd, String chargeToken);
    
    // This method's modifier has been elevated to "public" only for transaction demarcation purpose!
    Object[] requestRefund(CardProcessor processor, String customerName, String cardNumber, String cardExpiryMMYY, ProcessorTransaction origPtd,
        ProcessorTransaction refundPtd, boolean isNonEmsRequest) throws Exception;
    
    Object[] requestCPSRefund(ProcessorTransaction origPtd, ProcessorTransaction refundPtd, String chargeToken) throws Exception;
    
    // This method's modifier has been elevated to "public" only for transaction demarcation purpose!
    void handleRefundResponse(Object[] responses, Exception refundException, String customerName, ProcessorTransaction origPtd,
        ProcessorTransaction refundPtd, boolean isTransInEms);
    
    int processTransactionAuthorization(PreAuth pd, PointOfSale pointOfSale, boolean isCreditCard);
    
    void processTransactionCharge(CardRetryTransaction crt, Date maxRetryTime) throws CryptoException;
    
    int processTransactionCharge(String creditcardData, PointOfSale pointOfSale, Permit permit, boolean isRFID, ProcessorTransaction chargePt,
        PaymentCard paymentCard, long origProcTransId);
    
    void processTransactionSettlement(ProcessorTransaction ptd);
    
    String processTransactionTestCharge(MerchantAccount merchantAccount, int amount, String testSerialNumberForValueCard, Integer testCardTypeId);
    
    void cleanupOverdueCardData();
    
    Date getMaxRetryTimeForTransaction(CardRetryTransaction cardRetryTransaction);
    
    PreAuth getPreAuth(ProcessorTransaction origPtd);
    
    int findTotalPreAuthsByCardData(String cardData);
    
    int findTotalReversalsByCardData(String cardNumber);
    
    int findTotalPreAuthHoldingsByCardData(String cardData);
    
    Collection<PreAuth> findPreAuthsByCardData(String cardData, boolean isEvict);
    
    Collection<Reversal> findReversalsByCardData(String cardNumber, boolean isEvict);
    
    Collection<PreAuthHolding> findPreAuthHoldingsByCardData(String cardData, boolean isEvict);
    
    void updateReversal(Reversal reversal);
    
    void updatePreAuth(PreAuth preAuth);
    
    void updatePreAuthHolding(PreAuthHolding preAuthHolding);
    
    int getProcessorCounter();
    
    void setNeedStopProcessing(boolean needStopProcessing);
    
    Collection<ProcessorTransaction> getAuthorizedProcessorTransactionsForSettlement(Date maxReceiveTime);
    
    void updateProcessorTransactionToUncloseable(Date maxHoldingDay);
    
    void cleanupCardAuthorizationBackup(Date maxHoldingDay);
    
    // This method's modifier has been elevated to "public" only for transaction demarcation purpose!
    void saveFailedCCTransaction(PointOfSale pointOfSale, MerchantAccount merchantAccount, Date processingDate, Date purchasedDate, int ticketNumber,
        int ccType, String reason);
    
    // This method's modifier has been elevated to "public" only for transaction demarcation purpose!
    void insertFailedRefundIntoProcessorTransaction(ProcessorTransaction ptd, String reason);
    
    void updateRefundStatus(ProcessorTransaction origPtd);
    
    boolean checkMigrationStatusOfCustomer(int pointOfSaleId);
    
    List<CardRetryTransaction> findCardRetryTransactionsForMigratedCustomers(List<CardRetryTransaction> cardRetryTransactions);
    
    // The only reason that it is public is for transaction control. Do not use this elsewhere!
    void handleChargeResponse(CardProcessor processor, CardRetryTransaction crt, ProcessorTransaction chargePt, Track2Card track2Card,
        ProcessorTransaction oldPt, Object[] responses) throws Exception;
    
    // The only reason that it is public is for transaction control. Do not use this elsewhere!
    void handleFailedCharge(ProcessorTransaction chargePt, CardRetryTransaction crt, String reason, int responseCode, Exception e);
    
    void removeCardRetryTransaction(Long id);
    
    boolean processCardForSingleTransaction(TransactionData transactionData, boolean isCreditCard) throws PaystationCommunicationException;
    
    boolean processTransactionAuthorizationSinglePhaseTransaction(TransactionData transactionData) throws PaystationCommunicationException;
    
    void processTransactionChargeManualSave(CardRetryTransaction crt, Date maxRetryTime);
    
    void routeTransactionChargeProcessing(CardRetryTransaction crt, Date maxRetryTime) throws CryptoException;
    
    boolean processCardForSingleTransactionManualSave(TransactionData transactionData, boolean isCreditCard) throws PaystationCommunicationException;
    
    void specificProcessorPostProcessing(ProcessorTransaction pt);
    
    boolean isManualSave(PointOfSale pointOfSale);
    
    void processTransactionChargeManualStoreAndForward(CardProcessor processor, CardRetryTransaction crt, ProcessorTransaction chargePt,
        Track2Card track2Card, ProcessorTransaction oldPt) throws Exception;
    
    void processElavonReversalCleanup(Collection<PreAuth> preauths);
    
    ProcessorInfo getEMVProcessorInfo(int processorId);
    
    void processPreAuth(Long authId) throws CryptoException;
    
    void cleanupOverdueCardDataForMigratedCustomers(PreAuth auth);
    
    void copyPreAuthToPreAuthHolding(PreAuth auth) throws CryptoException;
    
    int processLinkCPSTransactionAuthorization(MerchantAccount merchantAccount, PointOfSale pointOfSale, PreAuth preAuth, String cardTypeName)
        throws CryptoException;
    
    int processLinkCPSTransactionCharge(ExtensiblePermit extensiblePermit, long origProcTransId, ProcessorTransaction chargeProcessorTransaction,
        MerchantAccount merchantAccount, PointOfSale pointOfSale, Permit permit, PaymentCard paymentCard);
    
    void processLinkCPSStoreAndForward(ProcessorTransaction processorTransaction, CardRetryTransaction cardRetryTransaction) throws CryptoException;
}
