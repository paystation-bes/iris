package com.digitalpaytech.cardprocessing.support;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.cardprocessing.support.threads.CardTransactionThread;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.exception.InvalidCSVDataException;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.TransactionFileUploadService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;

/**
 * Process the card transaction data. the data is in a zip file, which contains 4 types of transaction data,
 * each type of transaction data is in a separate file.
 * 
 * The 4 types of transaction are:
 * 
 * <pre>
 * 
 *  1. Approved Card Transaction. For this type,
 * the first line of data is &quot;Approved&quot;, followed by the field header 
 *  2. Non-Approved Card Transaction. For this type, the first line of data is the
 * field header. 
 *  3. Retries Exceeded Card Transactions. For this type, the first line of
 * data is &quot;RetriesExceeded&quot;, followed by the field header
 *  4. Refunded Card Transaction. For this type, the first line of
 * data is &quot;Refunded&quot;, followed by the field header
 * </pre>
 * 
 * In addition, this class also process the bad credit card list upload. For this type of data, the first line
 * of data is &quot;BadCards&quot;, followed by the field header
 * 
 * Copied from EMS 6.3.11 com.digitalpioneer.rpc.cardtransactions.CardTransactionZipProcessor
 */

@Component("cardTransactionZipProcessor")
public class CardTransactionZipProcessor {
    private static final Logger logger = Logger.getLogger(CardTransactionZipProcessor.class);
    
    private final static int DEFAULT_THREAD_COUNT = 1;
    private static int threadCount_ = DEFAULT_THREAD_COUNT;
    
    private static CardTransactionThread[] cardTransactionThreads_ = null;
    private static int currentThread_ = 0;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    @Autowired
    private MailerService mailerService;
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    @Autowired
    private CardTransactionUploadHelper cardTransactionUploadHelper;
    @Autowired
    private ApprovedTransactionCsvProcessor approvedTransactionCsvProcessor;
    @Autowired
    private RefundedTransactionCsvProcessor refundedTransactionCsvProcessor;
    @Autowired
    private RetriesExceededTransactionCsvProcessor retriesExceededTransactionCsvProcessor;
    @Autowired
    private NonApprovedTransactionCsvProcessor nonApprovedTransactionCsvProcessor;
    @Autowired
    private NonApprovedWithRetriesTransactionCsvProcessor nonApprovedWithRetriesTransactionCsvProcessor;
    @Autowired
    private BadCardsCsvProcessor badCardsCsvProcessor;
    @Autowired
    private TransactionFileUploadService transactionFileUploadService;
    
    private static final String NAME_FROM_BOSS_VISA = "Visa";
    private static final String NAME_FROM_BOSS_MASTERCARD = "MasterCard";
    private static final String NAME_FROM_BOSS_AMEX = "AMEX";
    private static final String NAME_FROM_BOSS_DISCOVER = "Discover";
    private static final String NAME_FROM_BOSS_DINNERS = "Diners";
    
    private static final Map<String, String> creditCardNamesMap = new HashMap<String, String>();
    static {
        creditCardNamesMap.put(NAME_FROM_BOSS_VISA, CardProcessingConstants.NAME_VISA);
        creditCardNamesMap.put(NAME_FROM_BOSS_MASTERCARD, CardProcessingConstants.NAME_MASTERCARD);
        creditCardNamesMap.put(NAME_FROM_BOSS_AMEX, CardProcessingConstants.NAME_AMEX);
        creditCardNamesMap.put(NAME_FROM_BOSS_DISCOVER, CardProcessingConstants.NAME_DISCOVER);
        creditCardNamesMap.put(NAME_FROM_BOSS_DINNERS, CardProcessingConstants.NAME_DINERS);
    }
    
    private ThreadLocal<PointOfSale> threadLocal = new ThreadLocal<PointOfSale>();
    
    private PointOfSale getPointOfSale() {
        return threadLocal.get();
    }
    
    private void setPointOfSale(PointOfSale pointOfSale) {
        threadLocal.set(pointOfSale);
    }
    
    public synchronized boolean isDisableProcessing() {
        return emsPropertiesService.getPropertyValueAsBoolean(EmsPropertiesService.DISABLE_PROCESSING_UPLOADED_TRANSFILE, false);
    }
    
    /**
     * called by spring when startup
     * - Added @PostConstruct to run this method when starting Iris
     * - Removed 'synchronized' keyword to avoid thread-deadlock problem.
     */
    @PostConstruct
    public void addUnprocessedFileToQueue() {
        initializeThreads();
        
        boolean bDisableProc = isDisableProcessing();
        
        if (bDisableProc) {
            logger.error("Don't processing uploaded transaction file, cause flag be set in EmsProperties table");
            return;
        }
        
        String dirName = WebCoreConstants.DEFAULT_FILE_UPLOAD_LOCATION;
        if (logger.isDebugEnabled()) {
            logger.debug("dirName for file upload: " + dirName);
        }
        
        File file = new File(dirName);
        File[] allFiles = file.listFiles();
        if (allFiles != null && allFiles.length > 0) {
            for (File transFile : allFiles) {
                if (transFile.isFile() && transFile.getName().startsWith(CardTransactionUploadHelper.FILE_NAME_PREFIX_CC)
                    && transFile.getName().endsWith(".zip")) {
                    this.addFileToQueue(transFile.getAbsolutePath());
                    
                    if (logger.isDebugEnabled()) {
                        logger.debug("file " + transFile.getAbsolutePath() + " is added to queue when initialized.");
                    }
                }
            }
        }
    }
    
    /**
     * setup the process queue
     * 
     * @param zipFileFullName
     */
    public synchronized void addFileToQueue(String zipFileFullName) {
        initializeThreads();
        
        cardTransactionThreads_[currentThread_].addTransactionToQueue(zipFileFullName);
        
        // Rotate to next thread in round-robin format
        currentThread_++;
        if (currentThread_ == threadCount_) {
            currentThread_ = 0;
        }
    }
    
    /**
     * start to process the zip file
     * 
     * @param zipFileFullName
     */
    public void processCardTransaction(String zipFileFullName) {
        try {
            parseAndProcess(zipFileFullName);
            deleteFile(zipFileFullName);
            if (logger.isInfoEnabled()) {
                logger.info("file " + zipFileFullName + " is processed successfully.");
            }
        } catch (FileNotFoundException e) {
            logger.error(e.getMessage(), e);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            
            File zipFile = new File(zipFileFullName);
            File errDirFileName = new File(WebCoreConstants.DEFAULT_TRANSACTION_FILES_ERROR_LOCATION + zipFile.getName());
            try {
                FileUtils.copyFile(zipFile, errDirFileName);
            } catch (IOException ioe) {
                StringBuilder bdr = new StringBuilder();
                bdr.append("Cannot copy file: ").append(zipFileFullName).append(" to: ").append(WebCoreConstants.DEFAULT_TRANSACTION_FILES_ERROR_LOCATION);
                bdr.append(" for further retry processing.");
                logger.error(bdr.toString(), ioe);
            }
            boolean okFlag = errDirFileName.exists();
            if (okFlag) {
                deleteFile(zipFileFullName);
            }
            StringBuilder bdr = new StringBuilder();
            bdr.append("Cannot parse file ! ").append(zipFileFullName);
            if (okFlag) {
                bdr.append(", moved to : ").append(errDirFileName.getAbsolutePath());
            }
            logger.error(bdr.toString());
            
        }
    }
    
    private void deleteFile(final String zipFileFullName) {
        int customerId = 0;
        ZipFile zipFile = null;
        try {
            zipFile = new ZipFile(new File(zipFileFullName));
            customerId = getCustomerId(zipFile.getName());
            final Customer cust = new Customer();
            cust.setId(customerId);
            this.transactionFileUploadService.saveTransactionFileUpload(cust, zipFileFullName);
        } catch (IOException ioe) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("Cannot save TransactionFileUpload record, customerId:").append(customerId).append(", zipFileFullName: ").append(zipFileFullName);
            logger.error(bdr.toString());
        } finally {
            if (zipFile != null) {
                try {
                    zipFile.close();
                } catch (IOException ioe) {
                    logger.error(ioe);
                }
            }
        }
        
        WebCoreUtil.secureDelete(new File(zipFileFullName));
    }
    
    private synchronized void initializeThreads() {
        if (cardTransactionThreads_ != null) {
            return;
        }
        
        try {
            int transaction_thread_count = emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.FILE_PARSER_THREAD_COUNT, 0);
            if (transaction_thread_count > 0) {
                threadCount_ = transaction_thread_count;
            }
        } catch (Exception e) {
            // Log and leave value as default
            logger.warn(e.getMessage(), e);
        }
        
        // Initialize array and create settlement threads
        cardTransactionThreads_ = new CardTransactionThread[threadCount_];
        for (int i = 0; i < threadCount_; i++) {
            cardTransactionThreads_[i] = new CardTransactionThread(i, this);
            cardTransactionThreads_[i].start();
        }
    }
    
    /**
     * 
     * @param zipFileFullName
     *            the full name of the zip file
     * 
     * @throws FileNotFoundException
     * @throws IOException
     */
    private void parseAndProcess(String zipFileFullName) throws FileNotFoundException, IOException {
        if (logger.isInfoEnabled()) {
            logger.info("parsing zip file: " + zipFileFullName);
        }
        
        File file = new File(zipFileFullName);
        ZipFile zipFile = new ZipFile(file);
        int customerId = getCustomerId(zipFile.getName());
        boolean emailAlertNeeded = false;
        boolean backupNeededFlag = false;
        
        try {
            Enumeration zipEntries = zipFile.entries();
            
            while (zipEntries.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry) zipEntries.nextElement();
                
                if (cardTransactionUploadHelper.shouldParseZipEntry(zipEntry)) {
                    // find the commAddress
                    String serialNum = getSerialNumberFromCsvFileName(zipEntry.getName());
                    if (logger.isInfoEnabled()) {
                        logger.info("serialNum from csv file " + zipEntry.getName() + ": " + serialNum);
                    }
                    
                    // for bad credit card list
                    if (WebCoreConstants.FILE_NAME_PREFIX_BAD_CARDS.equals(serialNum)) {
                        try {
                            if (parseZipEntryForBadCards(zipFile, zipEntry, customerId)) {
                                emailAlertNeeded = true;
                            }
                            
                        } catch (InvalidCSVDataException e) {
                            StringBuilder bdr = new StringBuilder();
                            bdr.append("exception occurred while processing zip file ").append(zipFileFullName).append(", entry ").append(zipEntry.getName())
                                    .append(": ");
                            bdr.append(e.getMessage());
                            logger.error(bdr.toString(), e);
                            emailAlertNeeded = true;
                            backupNeededFlag = true;
                        }
                    }
                    // for credit card transactions
                    else {
                        PointOfSale pointOfSale = pointOfSaleService.findPointOfSaleBySerialNumber(serialNum);
                        // check if the PointOfSale exists and belongs to the correct customer
                        if (pointOfSale != null) {
                            // cache the PointOfSale
                            this.setPointOfSale(pointOfSale);
                            try {
                                if (parseZipEntry(zipFile, zipEntry, customerId)) {
                                    emailAlertNeeded = true;
                                }
                                
                            } catch (InvalidCSVDataException e) {
                                logger.error("exception occurred while processing zip file " + zipFileFullName + ", entry " + zipEntry.getName() + ": "
                                                     + e.getMessage(), e);
                                emailAlertNeeded = true;
                                backupNeededFlag = true;
                            }
                        } else {
                            logger.warn("serialNum " + serialNum + "(from csv file name) doesn't exist or doesn't belong to the correct customer");
                        }
                    }
                    
                }
            }
            
            if (emailAlertNeeded) {
                sendEmailAlert(customerId);
            }
            if (backupNeededFlag) {
                throw new IOException("Problem parsing file, see zipHpzErrors folder.");
            }
        } finally {
            try {
                zipFile.close();
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
    }
    
    /**
     * get the serial number from the csv file name, which has the following format: cc<commAddress>_YYMMDDhhmmss.csv
     * 
     * @param zipEntry
     * @return
     */
    private String getSerialNumberFromCsvFileName(String csvFileName) {
        return csvFileName.substring(2, csvFileName.indexOf("_", 2));
    }
    
    private boolean parseZipEntryForBadCards(ZipFile zipFile, ZipEntry zipEntry, int customerId) throws InvalidCSVDataException {
        if (logger.isInfoEnabled()) {
            logger.info("parsing zip entry: " + zipEntry.getName() + " for badCards");
        }
        boolean emailAlertNeeded = false;
        BufferedReader reader = null;
        
        try {
            reader = new BufferedReader(new InputStreamReader(zipFile.getInputStream(zipEntry)));
            
            // Read the first line
            String line = this.cardTransactionUploadHelper.readNextLine(reader);
            if (line != null && line.equals(CardTransactionUploadHelper.CARD_TRANSACTION_TYPE_BAD_CARDS)) {
                // bad credit card list
                emailAlertNeeded = badCardsCsvProcessor.processCardTransactions(zipFile, zipEntry, customerId, null);
            } else {
                logger.info("invalid entry for badCards, ignored.");
            }
        } catch (IOException e) {
            throw new InvalidCSVDataException("Unable to read file", e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e) {
                    logger.error("unable to close the bufferedReader", e);
                }
            }
        }
        
        return emailAlertNeeded;
    }
    
    private boolean parseZipEntry(ZipFile zipFile, ZipEntry zipEntry, int customerId) throws InvalidCSVDataException {
        logger.info("parsing zip entry: " + zipEntry.getName());
        boolean emailAlertNeeded = false;
        BufferedReader reader = null;
        
        try {
            reader = new BufferedReader(new InputStreamReader(zipFile.getInputStream(zipEntry)));
            
            // Read the first line
            String line = this.cardTransactionUploadHelper.readNextLine(reader);
            
            if (line != null) {
                // check the transaction data type
                if (line.equals(CardTransactionUploadHelper.CARD_TRANSACTION_TYPE_APPROVED)) {
                    // for approved card transaction
                    emailAlertNeeded = approvedTransactionCsvProcessor.processCardTransactions(zipFile, zipEntry, customerId, getPointOfSale());
                } else if (line.equals(CardTransactionUploadHelper.CARD_TRANSACTION_TYPE_REFUNDED)) {
                    // for refunded card transaction
                    emailAlertNeeded = refundedTransactionCsvProcessor.processCardTransactions(zipFile, zipEntry, customerId, getPointOfSale());
                } else if (line.equals(CardTransactionUploadHelper.CARD_TRANSACTION_TYPE_RETRIES_EXCEEDED)) {
                    // for retries exceeded card transaction
                    emailAlertNeeded = retriesExceededTransactionCsvProcessor.processCardTransactions(zipFile, zipEntry, customerId, getPointOfSale());
                } else {
                    // for non-approved card transaction
                    if (cardTransactionUploadHelper.isNonApprovedWithRetries(line)) {
                        // non-approved with retries
                        emailAlertNeeded = nonApprovedWithRetriesTransactionCsvProcessor.processCardTransactions(zipFile, zipEntry, customerId,
                                                                                                                 getPointOfSale());
                    } else {
                        // no exception
                        // must be non-approved without retries
                        emailAlertNeeded = nonApprovedTransactionCsvProcessor.processCardTransactions(zipFile, zipEntry, customerId, getPointOfSale());
                    }
                }
            }
        } catch (IOException e) {
            throw new InvalidCSVDataException("Unable to read file", e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e) {
                    logger.error("unable to close the bufferedReader", e);
                }
            }
        }
        
        return emailAlertNeeded;
    }
    
    /**
     * extract the customer id from the zip file name the fullFileName format(in Windows):
     * dir_name\cc(customerId)_xxxxxxx.zip
     * 
     * @param fullFileName
     * @return
     */
    private int getCustomerId(String fullFileName) {
        int customerId = 0;
        String fileSeparator = System.getProperty("file.separator");
        String fileName = fullFileName.substring(fullFileName.lastIndexOf(fileSeparator) + 1);
        String customerIDString = fileName.substring(2, fileName.indexOf("_", 2));
        customerId = Integer.parseInt(customerIDString);
        return customerId;
    }
    
    private void sendEmailAlert(int customerId) {
        String subject = "Transaction data error";
        StringBuilder content = new StringBuilder();
        content.append("Invalid transaction data is found while uploading approved card transaction data from BOSS for customer(id: " + customerId + ")\n\n");
        content.append("The data is saved to database. A warning message is also created in the log file.\n");
        
        mailerService.sendAdminErrorAlert(subject, content.toString(), null);
        if (logger.isInfoEnabled()) {
            logger.info("Admin error alert email is sent.");
        }
    }
    
    public void setPointOfSaleService(PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public void setMailerService(MailerService mailerService) {
        this.mailerService = mailerService;
    }
    
    public void setEmsPropertiesService(EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public void setCardTransactionUploadHelper(CardTransactionUploadHelper cardTransactionUploadHelper) {
        this.cardTransactionUploadHelper = cardTransactionUploadHelper;
    }
    
    public void setApprovedTransactionCsvProcessor(ApprovedTransactionCsvProcessor approvedTransactionCsvProcessor) {
        this.approvedTransactionCsvProcessor = approvedTransactionCsvProcessor;
    }
    
    public void setRefundedTransactionCsvProcessor(RefundedTransactionCsvProcessor refundedTransactionCsvProcessor) {
        this.refundedTransactionCsvProcessor = refundedTransactionCsvProcessor;
    }
    
    public void setRetriesExceededTransactionCsvProcessor(RetriesExceededTransactionCsvProcessor retriesExceededTransactionCsvProcessor) {
        this.retriesExceededTransactionCsvProcessor = retriesExceededTransactionCsvProcessor;
    }
    
    public void setNonApprovedTransactionCsvProcessor(NonApprovedTransactionCsvProcessor nonApprovedTransactionCsvProcessor) {
        this.nonApprovedTransactionCsvProcessor = nonApprovedTransactionCsvProcessor;
    }
    
    public void setNonApprovedWithRetriesTransactionCsvProcessor(NonApprovedWithRetriesTransactionCsvProcessor nonApprovedWithRetriesTransactionCsvProcessor) {
        this.nonApprovedWithRetriesTransactionCsvProcessor = nonApprovedWithRetriesTransactionCsvProcessor;
    }
    
    public void setBadCardsCsvProcessor(BadCardsCsvProcessor badCardsCsvProcessor) {
        this.badCardsCsvProcessor = badCardsCsvProcessor;
    }
    
    public void setTransactionFileUploadService(TransactionFileUploadService transactionFileUploadService) {
        this.transactionFileUploadService = transactionFileUploadService;
    }
}
