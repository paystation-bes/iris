package com.digitalpaytech.cardprocessing.support;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.digitalpaytech.exception.InvalidCSVDataException;
import com.digitalpaytech.util.DigitalAPIUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;

@Component("cardTransactionUploadHelper")
public class CardTransactionUploadHelper {
    private static final Logger logger = Logger.getLogger(CardTransactionUploadHelper.class);
    public static final String UNDER_SCORE = "_";
    public static final String FILE_NAME_PREFIX_CC = "cc";
    public static final String FILE_NAME_SUFFIX_CSV = ".csv";
    public static final String CARD_TRANSACTION_TYPE_APPROVED = "Approved";
    public static final String CARD_TRANSACTION_TYPE_REFUNDED = "Refunded";
    public static final String CARD_TRANSACTION_TYPE_RETRIES_EXCEEDED = "RetriesExceeded";
    public static final String CARD_TRANSACTION_TYPE_BAD_CARDS = "BadCards";
    
    public static final String DELIMITER = ",";
    
    public final static int EXACT_CARD_DATA_LENGTH = 350;
    
    /**
     * verify the headers of all the csv files included in the given zip file
     * 
     * @param zipFileFullName
     * @return true if all headers are correct
     */
    public boolean verifyHeadersForZipFile(String zipFileFullName) {
        boolean headersVerified = true;
        
        logger.info("verifying headers for zip file: " + zipFileFullName);
        
        File file = new File(zipFileFullName);
        ZipFile zipFile = null;
        
        try {
            zipFile = new ZipFile(file);
            
            Enumeration zipEntries = zipFile.entries();
            
            while (headersVerified && zipEntries.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry) zipEntries.nextElement();
                
                if (shouldParseZipEntry(zipEntry)) {
                    headersVerified = verifyHeadersForZipEntry(zipFile, zipEntry);
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            headersVerified = false;
        } finally {
            try {
                zipFile.close();
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                headersVerified = false;
            }
        }
        
        if (!headersVerified) {
            String name = zipFileFullName.substring(zipFileFullName.lastIndexOf(System.getProperty("file.separator")) + 1, zipFileFullName.length());
            File errDirFileName = new File(WebCoreConstants.DEFAULT_TRANSACTION_FILES_ERROR_LOCATION + name);
            try {
                FileUtils.copyFile(file, errDirFileName);
            } catch (IOException ioe) {
                StringBuilder bdr = new StringBuilder();
                bdr.append("Cannot copy file: ").append(zipFileFullName).append(" to: ").append(WebCoreConstants.DEFAULT_TRANSACTION_FILES_ERROR_LOCATION);
                bdr.append(" for further retry processing.");
                logger.error(bdr.toString(), ioe);
            }
        }
        return headersVerified;
    }
    
    private boolean verifyHeadersForZipEntry(ZipFile zipFile, ZipEntry zipEntry) {
        logger.info("parsing zip entry: " + zipEntry.getName());
        boolean headersVerified = true;
        BufferedReader reader = null;
        
        try {
            reader = new BufferedReader(new InputStreamReader(zipFile.getInputStream(zipEntry)));
            
            // Read the first line
            String line = readNextLine(reader);
            if (line != null) {
                // check the transaction data type
                if (line.equals(CARD_TRANSACTION_TYPE_APPROVED)) {
                    // for approved card transaction
                    headersVerified = verifyHeaders(reader, ApprovedTransactionCsvProcessor.COLUMN_HEADERS_APPROVED);
                } else if (line.equals(CARD_TRANSACTION_TYPE_REFUNDED)) {
                    // for refunded card transaction
                    headersVerified = verifyHeaders(reader, RefundedTransactionCsvProcessor.COLUMN_HEADERS_REFUNDED);
                } else if (line.equals(CARD_TRANSACTION_TYPE_RETRIES_EXCEEDED)) {
                    // for retries exceeded card transaction
                    headersVerified = verifyHeaders(reader, RetriesExceededTransactionCsvProcessor.COLUMN_HEADERS_RETRIES_EXCEEDED);
                } else if (line.equals(CARD_TRANSACTION_TYPE_BAD_CARDS)) {
                    // for bad credit card list
                    headersVerified = verifyHeaders(reader, BadCardsCsvProcessor.COLUMN_HEADERS_BAD_CREDIT_CARD_LIST);
                } else {
                    // for non-approved card transaction
                    verifyHeadersForNonApproved(line);
                }
            } else {
                headersVerified = false;
            }
        } catch (InvalidCSVDataException e) {
            logger.error("invalid headers: " + e.getMessage(), e);
            headersVerified = false;
        } catch (IOException e) {
            logger.error("Unable to read file.", e);
            headersVerified = false;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e) {
                    logger.error("unable to close the bufferedReader", e);
                    headersVerified = false;
                }
            }
        }
        
        return headersVerified;
    }
    
    private boolean verifyHeaders(BufferedReader reader, String[] columnHeaders) throws InvalidCSVDataException, IOException {
        String line = readNextLine(reader);
        if (line != null) {
            verifyHeaders(line, columnHeaders);
            return true;
        }
        
        return false;
    }
    
    public boolean isNonApprovedWithRetries(String header) throws InvalidCSVDataException {
        String[] columnHeaders = NonApprovedTransactionCsvProcessor.COLUMN_HEADERS_NON_APPROVED_WITHOUT_RETRIES;
        try {
            verifyHeaders(header, columnHeaders);
            return false;
        } catch (InvalidCSVDataException e) {
            columnHeaders = NonApprovedWithRetriesTransactionCsvProcessor.COLUMN_HEADERS_NON_APPROVED_WITH_RETRIES;
            verifyHeaders(header, columnHeaders);
            return true;
        }
    }
    
    /**
     * because 2 types of headers exist for non-approved transactions
     * 
     * @param header
     * @return
     * @throws InvalidCSVDataException
     */
    public String[] verifyHeadersForNonApproved(String header) throws InvalidCSVDataException {
        String[] columnHeaders = NonApprovedTransactionCsvProcessor.COLUMN_HEADERS_NON_APPROVED_WITHOUT_RETRIES;
        try {
            verifyHeaders(header, columnHeaders);
            return columnHeaders;
        } catch (InvalidCSVDataException e) {
            columnHeaders = NonApprovedWithRetriesTransactionCsvProcessor.COLUMN_HEADERS_NON_APPROVED_WITH_RETRIES;
            verifyHeaders(header, columnHeaders);
            return columnHeaders;
        }
    }
    
    public void verifyHeaders(String header, String[] columnHeaders) throws InvalidCSVDataException {
        String[] tokens = header.split(DELIMITER, columnHeaders.length);
        // check the number of columns
        if (tokens.length != columnHeaders.length) {
            throw new InvalidCSVDataException("Invalid Header Data - Incorrect # of columns");
        }
        
        // check each column
        for (int i = 0; i < columnHeaders.length; i++) {
            if (!columnHeaders[i].equals(tokens[i])) {
                throw new InvalidCSVDataException("Invalid Header Data - Header: " + i + " is: '" + WebCoreUtil.encodeHTMLTag(tokens[i]) + "', but should be '"
                                                  + columnHeaders[i] + "'");
            }
        }
    }
    
    /**
     * determine if we should parse the entry in the zip file. the transaction data will be in a csv file, whose
     * file name has the following format: cc<commAddress>_YYMMDDhhmmss.csv
     * 
     * @param zipEntry
     * @return
     */
    public boolean shouldParseZipEntry(ZipEntry zipEntry) {
        boolean valid = false;
        
        if (zipEntry != null && !zipEntry.isDirectory()) {
            String zipEntryName = zipEntry.getName();
            valid = isCsvFileNameValid(zipEntryName);
            if (!valid) {
                logger.info("invalid entry found, ignored it: " + zipEntryName);
            }
        }
        
        return valid;
    }
    
    private boolean isCsvFileNameValid(String csvFileName) {
        return csvFileName != null && csvFileName.toLowerCase().startsWith(FILE_NAME_PREFIX_CC) && csvFileName.toLowerCase().endsWith(FILE_NAME_SUFFIX_CSV)
               && csvFileName.indexOf(UNDER_SCORE) > 0;
        
    }
    
    public String getCommAddressFromZipFile(String zipFileFullName) {
        String commAddress = null;
        File file = new File(zipFileFullName);
        ZipFile zipFile = null;
        
        try {
            zipFile = new ZipFile(file);
            
            Enumeration zipEntries = zipFile.entries();
            
            while (commAddress == null && zipEntries.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry) zipEntries.nextElement();
                String csvFileName = zipEntry.getName();
                commAddress = csvFileName.substring(2, csvFileName.indexOf(UNDER_SCORE, 2));
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            try {
                zipFile.close();
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
        
        return commAddress;
    }
    
    public boolean isCsvFileInZipValid(String zipFileFullName) {
        boolean valid = false;
        File file = new File(zipFileFullName);
        ZipFile zipFile = null;
        
        try {
            zipFile = new ZipFile(file);
            
            Enumeration zipEntries = zipFile.entries();
            // must have one and only one element
            ZipEntry zipEntry = (ZipEntry) zipEntries.nextElement();
            
            valid = isCsvFileNameValid(zipEntry.getName());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            try {
                zipFile.close();
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
        
        return valid;
    }
    
    /**
     * read the next line and verify it for invalid characters
     * 
     * @param reader
     * @return
     * @throws InvalidCSVDataException
     * @throws IOException
     */
    public String readNextLine(final BufferedReader reader) throws InvalidCSVDataException, IOException {
        String line;
        line = reader.readLine();
        
        if (!validateString(line)) {
            throw new InvalidCSVDataException("invalid characters");
        } else {
            return line;
        }
    }
    
    // Validate information using regular expressions.
    public boolean validateString(String value) {
        return DigitalAPIUtil.validateStringWithRegEx(value, WebCoreConstants.REGEX_CSV_FILES);
    }
    
    public static short getShort(String value, int minValue, int maxValue, String fieldName, StringBuilder sbComments) {
        return (short) getInt(value, minValue, maxValue, fieldName, sbComments).intValue();
    }
    
    public static Integer getInt(String value, int minValue, int maxValue, String fieldName, StringBuilder sbComments) {
        if ((value == null) || value.length() == 0 || !value.matches(WebCoreConstants.REGEX_INTEGER)) {
            sbComments.append(fieldName).append(" is not an integer, ");
            return (null);
        }
        
        long long_value = Long.parseLong(value);
        if (long_value < minValue) {
            sbComments.append(fieldName).append("is < ").append(minValue).append(", ");
            return (null);
        } else if (long_value > maxValue) {
            sbComments.append(fieldName).append("is > ").append(minValue).append(", ");
            return (null);
        }
        
        return (new Integer((int) long_value));
    }
    
    public static String checkAndExtractString(String value, int minLength, int maxLength, String fieldName, StringBuilder sbComments) {
        if (value == null) {
            sbComments.append(fieldName).append(" is null, ");
            return (null);
        } else if (value.length() < minLength) {
            sbComments.append(fieldName).append(" is < ").append(minLength).append(" chars, ");
            return (null);
        } else if (value.length() > maxLength) {
            sbComments.append(fieldName).append(" is > ").append(minLength).append(" chars, ");
            return (null);
        }
        
        return (value);
    }
    
    public static String checkAndExtractString(String value, int exactLength, String fieldName, StringBuilder sbComments) {
        if (value == null) {
            sbComments.append(fieldName).append(" is null, ");
            return (null);
        } else if (value.length() != exactLength) {
            sbComments.append(fieldName).append(" is != ").append(exactLength).append(" chars, ");
            return (null);
        }
        
        return (value);
    }
    
}
