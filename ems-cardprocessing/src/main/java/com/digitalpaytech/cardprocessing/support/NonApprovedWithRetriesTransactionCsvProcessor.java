package com.digitalpaytech.cardprocessing.support;

import org.springframework.stereotype.Component;

/*
 * Copied from EMS 6.3.11 com.digitalpioneer.rpc.cardtransactions.NonApprovedWithRetriesTransactionCsvProcessor
 */
@Component("nonApprovedWithRetriesTransactionCsvProcessor")
public class NonApprovedWithRetriesTransactionCsvProcessor extends NonApprovedTransactionCsvProcessor {
    public static final String[] COLUMN_HEADERS_NON_APPROVED_WITH_RETRIES = { "CommAddress", "PurchaseDate", "TicketNumber", "Amount", "CardData", "Retries" };
    
    private static final int INDEX_RETRIES = 5;
    
    public NonApprovedWithRetriesTransactionCsvProcessor() {
        super();
    }
    
    @Override
    protected final String[] getColumnHeaders() {
        return COLUMN_HEADERS_NON_APPROVED_WITH_RETRIES;
    }
    
    @Override
    protected final int getNumRetries(final String[] tokens, final StringBuilder sbComments) {
        final String intString = tokens[INDEX_RETRIES];
        final String columnName = COLUMN_HEADERS_NON_APPROVED_WITH_RETRIES[INDEX_RETRIES];
        final int retValue = toInt(intString, columnName, sbComments);
        
        if (retValue < 0 || retValue > 65535) {
            sbComments.append(columnName).append(" ");
        }
        
        return retValue;
    }
    
    protected final int toInt(final String intString, final String columnName, final StringBuilder sbComments) {
        try {
            return Integer.parseInt(intString);
        } catch (NumberFormatException e) {
            sbComments.append(columnName).append(" ");
            return 0;
        }
    }
}
