package com.digitalpaytech.cardprocessing.support;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.DuplicateObjectException;
import com.digitalpaytech.exception.InvalidCSVDataException;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.crypto.CryptoConstants;

/*
 * Copied from EMS 6.3.11 com.digitalpioneer.rpc.cardtransactions.RetriesExceededTransactionCsvProcessor
 */

@Component("retriesExceededTransactionCsvProcessor")
public class RetriesExceededTransactionCsvProcessor extends CardTransactionCsvProcessor {
    public static final String[] COLUMN_HEADERS_RETRIES_EXCEEDED = { "CommAddress", "PurchaseDate", "TicketNumber", "Amount", "CardHash", "CardData" };
    
    private static final int INDEX_CARD_HASH = 4;
    private static final int INDEX_CARD_DATA_RETRIES_EXCEEDED = 5;
    private static final int MAX_NUMBER_RETRIES = Short.MAX_VALUE;
    
    private static Logger logger = Logger.getLogger(RetriesExceededTransactionCsvProcessor.class);
    
    public RetriesExceededTransactionCsvProcessor() {
        super();
    }
    
    @Override
    protected final String[] getColumnHeaders() {
        return COLUMN_HEADERS_RETRIES_EXCEEDED;
    }
    
    @Override
    protected final int getNumColumns() {
        return COLUMN_HEADERS_RETRIES_EXCEEDED.length;
    }
    
    @Override
    protected final boolean isHeaderLine(final int lineNum) {
        return lineNum == 2;
    }
    
    @Override
    protected final boolean isTransactionTypeLine(final int lineNum) {
        return lineNum == 1;
    }
    
    @Override
    public void processLine(final String line, final Object[] userArg) {
        boolean emailAlertNeeded = false;
        final int customerId = getCustomerId(userArg);
        CardRetryTransaction cardRetryTransaction = null;
        try {
            final String[] tokens = line.split(DELIMITER, getNumColumns());
            verifyColumnNumber(tokens, getNumColumns());
            
            cardRetryTransaction = getRetriesExceededTransaction(tokens, userArg);
            logger.debug("cardRetryTransaction: " + cardRetryTransaction);
            
            if (handleRetriesExceededCardTransaction(cardRetryTransaction, customerId, line)) {
                emailAlertNeeded = true;
            }
        } catch (Exception e) {
            if (cardRetryTransaction != null) {
                emailAlertNeeded = handleException(cardRetryTransaction.getPointOfSale(), cardRetryTransaction.getPointOfSale().getCustomer(), line, e);
            } else {
                logger.error("processLine, PROBLEM calling 'getRetriesExceededTransaction', line: " + line, e);
            }
        }
        if (emailAlertNeeded) {
            setSendEmailAlert(userArg);
        }
    }
    
    /**
     * construct a non-approved CardRetryTransaction object from the tokens
     * 
     * @param tokens
     * @return
     * @throws InvalidCSVDataException
     * @throws CryptoException
     */
    private CardRetryTransaction getRetriesExceededTransaction(final String[] tokens, final Object[] userArg) throws InvalidCSVDataException, CryptoException {
        // store the field names with invalid data
        final StringBuilder sbComments = new StringBuilder();
        final String[] headers = COLUMN_HEADERS_RETRIES_EXCEEDED;
        
        final PointOfSale pointOfSale = getPointOfSale(tokens[INDEX_COMM_ADDRESS], userArg);
        // PS exists, so extract values from CSV file
        final Date purchaseDate = toDate(tokens[INDEX_PURCHASE_DATE], headers[INDEX_PURCHASE_DATE], sbComments);
        final Integer ticketNumber = this.getTicketNumber(tokens[INDEX_TICKET_NUMBER], headers[INDEX_TICKET_NUMBER], sbComments);
        final Integer amountInCents = getAmountInCents(tokens[INDEX_AMOUNT], headers[INDEX_AMOUNT], sbComments);
        final String cardHash = this.getCardHash(tokens[INDEX_CARD_HASH], headers[INDEX_CARD_HASH], sbComments);
        Track2Card track2Card = null;
        
        try {
            // cardData is optional here
            track2Card = getCreditCardData(tokens[INDEX_CARD_DATA_RETRIES_EXCEEDED], headers[INDEX_CARD_DATA_RETRIES_EXCEEDED], sbComments, pointOfSale
                    .getCustomer().getId(), false);
        } catch (CryptoException e) {
            if (sbComments.length() == 0) {
                final ProcessorTransaction processorTransaction = getProcessorTransactionForInvalidEncryption(pointOfSale, purchaseDate
                                                                                                              , ticketNumber, amountInCents);
                super.getEntityDao().save(processorTransaction);
            }
            sbComments.append(headers[INDEX_CARD_DATA_RETRIES_EXCEEDED] + " cannot be decrypted");
            logger.error(sbComments.toString(), e);
        }
        
        // If parsing errors, throw exception
        if (sbComments.length() > 0) {
            sbComments.insert(0, "Parsing: ");
            throw new InvalidCSVDataException(sbComments.toString());
        }
        
        // now we can construct the processorTransaction object
        final CardRetryTransaction cardRetryTransaction = new CardRetryTransaction();
        cardRetryTransaction.setCardRetryTransactionType(getCardRetryTransactionType(CardProcessingConstants.CARD_RETRY_TX_TYPE_BATCH));
        cardRetryTransaction.setPointOfSale(pointOfSale);
        cardRetryTransaction.setPurchasedDate(purchaseDate);
        cardRetryTransaction.setTicketNumber(ticketNumber);
        
        // all other properties
        cardRetryTransaction.setAmount(amountInCents);
        final Date now = DateUtil.getCurrentGmtDate();
        cardRetryTransaction.setLastRetryDate(now);
        cardRetryTransaction.setCreationDate(now);
        
        // the cardData is optional
        if (track2Card != null) {
            final String cardHashRecreated = super.getCryptoAlgorithmFactory()
                    .getSha1Hash(track2Card.getCreditCardPanAndExpiry(), CryptoConstants.HASH_CREDIT_CARD_PROCESSING);
            final String badCardHash = super.getCryptoAlgorithmFactory()
                    .getSha1Hash(track2Card.getPrimaryAccountNumber(), CryptoConstants.HASH_BAD_CREDIT_CARD);
            
            // compare the cardHash received against the one re-generated
            if (!cardHash.equals(cardHashRecreated)) {
                sbComments.insert(0, "Parsing: " + headers[INDEX_CARD_HASH] + " not match");
                throw new InvalidCSVDataException(sbComments.toString());
            }
            
            cardRetryTransaction.setCardHash(cardHash);
            cardRetryTransaction.setBadCardHash(badCardHash);
            cardRetryTransaction.setCardType(track2Card.getDisplayName());
            cardRetryTransaction.setLast4digitsOfCardNumber(track2Card.getLast4DigitsOfAccountNumber());
            cardRetryTransaction.setCardExpiry(Short.parseShort(track2Card.getExpirationYear() + track2Card.getExpirationMonth()));
            cardRetryTransaction.setNumRetries(MAX_NUMBER_RETRIES);
            final String aesEncryptedCardData = this.cryptoService.encryptData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL, track2Card.getCreditCardPanAndExpiry());
            cardRetryTransaction.setCardData(aesEncryptedCardData);
        } else {
            cardRetryTransaction.setCardHash(cardHash);
            cardRetryTransaction.setNumRetries(MAX_NUMBER_RETRIES);
        }
        
        return cardRetryTransaction;
    }
    
    /**
     * handle retries exceeded card transactions
     * 
     * @param cardRetryTransaction
     * @param customerId
     * @param line
     * @return
     */
    private boolean handleRetriesExceededCardTransaction(final CardRetryTransaction cardRetryTransaction, final int customerId, final String line) {
        try {
            if (cardRetryTransaction.getId() == null) {
                // save it to db
                super.getEntityDao().save(cardRetryTransaction);
            } else {
                final CardRetryTransaction tx = super.getCardRetryTransactionService().findByPointOfSalePurchasedDateTicketNumber(cardRetryTransaction
                                                                                                                                    .getPointOfSale().getId(),
                                                                                                                            cardRetryTransaction
                                                                                                                                    .getPurchasedDate(),
                                                                                                                            cardRetryTransaction
                                                                                                                                    .getTicketNumber());
                if (tx == null) {
                    super.getEntityDao().save(cardRetryTransaction);
                }
            }
            
            // Add it to the in memory processing queue
            if (cardRetryTransaction.getNumRetries() < Short.MAX_VALUE) {
                this.cardProcessingMaster.addTransactionToStoreForwardQueue(cardRetryTransaction);
            }
        } catch (DuplicateObjectException e) {
            logger.warn("CardRetryTransaction already exists in the database");
        }
        
        return false;
    }
    
}
