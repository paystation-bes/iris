package com.digitalpaytech.cardprocessing.support;

import java.text.ParseException;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.cardprocessing.CardProcessingMaster;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.CardRetryTransactionType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.InvalidCSVDataException;
import com.digitalpaytech.service.CardRetryTransactionTypeService;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.ProcessorTransactionTypeService;
import com.digitalpaytech.service.ProcessorTransactionService;
import com.digitalpaytech.service.TransactionService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.CardTypeService;
import com.digitalpaytech.service.CardRetryTransactionService;
import com.digitalpaytech.service.crypto.CryptoAlgorithmFactory;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.dao.EntityDao;

/**
 * Base Card Transaction Csv Processor. Verify header and transaction type lines.<br>
 * Call subclass implemented processLine method to process a record.
 * 
 * Copied from EMS 6.3.11 com.digitalpioneer.rpc.cardtransactions.CardTransactionCsvProcessor
 */

@Component("cardTransactionCsvProcessor")
@Transactional(propagation = Propagation.SUPPORTS)
public abstract class CardTransactionCsvProcessor extends CsvProcessor {
    // for all transaction types
    protected static final int INDEX_COMM_ADDRESS = 0;
    protected static final int INDEX_PURCHASE_DATE = 1;
    protected static final int INDEX_TICKET_NUMBER = 2;
    protected static final int INDEX_AMOUNT = 3;
    
    protected static final String AUTH_VALUE_REFUNDED = "<Refunded>";
    protected static final String AUTH_VALUE_NA = "N/A";
    
    private static Logger logger = Logger.getLogger(CardTransactionCsvProcessor.class);
    
    @Autowired
    protected CardTransactionUploadHelper cardTransactionUploadHelper;
    
    @Autowired
    protected TransactionService transactionService;
    
    @Autowired
    protected CryptoService cryptoService;
    
    @Autowired
    protected CardProcessingMaster cardProcessingMaster;
    
    @Autowired
    protected CustomerCardTypeService customerCardTypeService;
    
    @Autowired
    private ProcessorTransactionTypeService processorTransactionTypeService;
    
    @Autowired
    private ProcessorTransactionService processorTransactionService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private CardTypeService cardTypeService;
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    @Autowired
    private CardRetryTransactionService cardRetryTransactionService;
    
    @Autowired
    private CardRetryTransactionTypeService cardRetryTransactionTypeService;
    
    @Autowired
    private CryptoAlgorithmFactory cryptoAlgorithmFactory;
    
    @Autowired
    private EntityDao entityDao;
    
    protected CardTransactionCsvProcessor() {
        super();
    }
    
    protected abstract void processLine(String line, Object[] userArg);
    
    protected abstract boolean isHeaderLine(int lineNum);
    
    protected abstract boolean isTransactionTypeLine(int lineNum);
    
    protected abstract String[] getColumnHeaders();
    
    protected abstract int getNumColumns();
    
    public final void setCustomerCardTypeService(final CustomerCardTypeService customerCardTypeService) {
        this.customerCardTypeService = customerCardTypeService;
    }
    
    public final CustomerCardTypeService getCustomerCardTypeService() {
        return this.customerCardTypeService;
    }
    
    public final void setCardTransactionUploadHelper(final CardTransactionUploadHelper cardTransactionUploadHelper) {
        this.cardTransactionUploadHelper = cardTransactionUploadHelper;
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final EntityDao getEntityDao() {
        return this.entityDao;
    }
    
    public final void setTransactionService(final TransactionService transactionService) {
        this.transactionService = transactionService;
    }
    
    public final TransactionService getTransactionService() {
        return this.transactionService;
    }
    
    public final void setCryptoService(final CryptoService cryptoService) {
        this.cryptoService = cryptoService;
    }
    
    public final CryptoService getCryptoService() {
        return this.cryptoService;
    }
    
    public final void setCardProcessingMaster(final CardProcessingMaster cardProcessingMaster) {
        this.cardProcessingMaster = cardProcessingMaster;
    }
    
    public final void setProcessorTransactionTypeService(final ProcessorTransactionTypeService processorTransactionTypeService) {
        this.processorTransactionTypeService = processorTransactionTypeService;
    }
    
    public final ProcessorTransactionTypeService getProcessorTransactionTypeService() {
        return this.processorTransactionTypeService;
    }
    
    public final ProcessorTransactionService getProcessorTransactionService() {
        return this.processorTransactionService;
    }
    
    public final void setProcessorTransactionService(final ProcessorTransactionService processorTransactionService) {
        this.processorTransactionService = processorTransactionService;
    }
    
    public final CustomerService getCustomerService() {
        return this.customerService;
    }
    
    public final void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }
    
    public final CardTypeService getCardTypeService() {
        return this.cardTypeService;
    }
    
    public final void setCardTypeService(final CardTypeService cardTypeService) {
        this.cardTypeService = cardTypeService;
    }
    
    public final MerchantAccountService getMerchantAccountService() {
        return this.merchantAccountService;
    }
    
    public final void setMerchantAccountService(final MerchantAccountService merchantAccountService) {
        this.merchantAccountService = merchantAccountService;
    }
    
    public final CardRetryTransactionService getCardRetryTransactionService() {
        return this.cardRetryTransactionService;
    }
    
    public final void setCardRetryTransactionService(final CardRetryTransactionService cardRetryTransactionService) {
        this.cardRetryTransactionService = cardRetryTransactionService;
    }
    
    public final CardRetryTransactionTypeService getCardRetryTransactionTypeService() {
        return this.cardRetryTransactionTypeService;
    }
    
    public final void setCardRetryTransactionTypeService(final CardRetryTransactionTypeService cardRetryTransactionTypeService) {
        this.cardRetryTransactionTypeService = cardRetryTransactionTypeService;
    }
    
    public final CryptoAlgorithmFactory getCryptoAlgorithmFactory() {
        return this.cryptoAlgorithmFactory;
    }
    
    public final void setCryptoAlgorithmFactory(final CryptoAlgorithmFactory cryptoAlgorithmFactory) {
        this.cryptoAlgorithmFactory = cryptoAlgorithmFactory;
    }
    
    /**
     * Parse the csv file and process the transactions.
     * 
     * @param zipFile
     * @param zipEntry
     * @param customerId
     * @return true if require email alert. false if file processed without problem.
     * @throws InvalidCSVDataException
     * @throws SystemException
     */
    public boolean processCardTransactions(final ZipFile zipFile, final ZipEntry zipEntry, final int customerId, final PointOfSale pointOfSale)
        throws InvalidCSVDataException {
        final ProcessingResult result = new ProcessingResult();
        processFile(zipFile, zipEntry, new Object[] { customerId, result, pointOfSale });
        return result.isSendEmail();
    }
    
    @Override
    public void processLine(final String line, final int lineNum, final Object[] userArg) throws InvalidCSVDataException {
        if (isHeaderLine(lineNum)) {
            // verify header
            verifyHeaders(line.split(DELIMITER, getNumColumns()), getColumnHeaders(), getNumColumns());
        } else if (isTransactionTypeLine(lineNum)) {
            // ignore
        } else {
            // process the record
            try {
                processLine(line, userArg);
                
            } catch (CardTransactionException cte) {
                throw new InvalidCSVDataException(cte);
            }
        }
    }
    
    protected boolean handleException(final PointOfSale pointOfSale, final Customer customer, final String line, final Exception e) {
        boolean emailAlertNeeded;
        // data invalid detected
        final StringBuilder bdr = new StringBuilder();
        bdr.append("Transaction data invalid: ").append(e.getMessage()).append(". ").append(line);
        logger.error(bdr.toString(), e);
        
        emailAlertNeeded = true;
        return emailAlertNeeded;
    }
    
    protected int toInt(final String intString, final String columnName, final StringBuilder sbComments) {
        try {
            return Integer.parseInt(intString);
        } catch (NumberFormatException e) {
            sbComments.append(columnName).append(" ");
            return 0;
        }
    }
    
    protected final Date toDate(final String dateString, final String columnName, final StringBuilder sbComments) {
        try {
            return DateUtil.getNewPs2DateFormat().parse(dateString);
        } catch (ParseException pe) {
            sbComments.append(columnName).append("is not a valid date, ");
            return null;
        }
    }
    
    protected final Integer getTicketNumber(final String value, final String columnName, final StringBuilder sbComments) {
        final Integer intValue = CardTransactionUploadHelper.getInt(value, WebCoreConstants.MIN_TICKET_NUMBER, WebCoreConstants.MAX_TICKET_NUMBER,
            columnName, sbComments);
        
        return intValue;
    }
    
    protected final Integer getAmountInCents(final String value, final String columnName, final StringBuilder sbComments) {
        final Integer intValue = CardTransactionUploadHelper.getInt(value, WebCoreConstants.MIN_AMOUNT_IN_CENTS, WebCoreConstants.MAX_AMOUNT_IN_CENTS,
            columnName, sbComments);
        
        return intValue;
    }
    
    protected final String getCardHash(final String value, final String columnName, final StringBuilder sbComments) {
        final String retVal = CardTransactionUploadHelper.checkAndExtractString(value, WebCoreConstants.EXACT_CARD_PROCESSING_HASH_LENGTH, columnName,
            sbComments);
        
        return retVal;
    }
    
    protected final Track2Card getCreditCardData(final String value, final String columnName, final StringBuilder sbComments
            , final int customerId, final boolean required) throws CryptoException {
        if ((value == null || value.length() == 0) && !required) {
            return null;
        }
        
        final String rsaEncryptedCardData = CardTransactionUploadHelper.checkAndExtractString(value, 0, CardTransactionUploadHelper.EXACT_CARD_DATA_LENGTH,
            columnName, sbComments);
        
        if (rsaEncryptedCardData == null) {
            return null;
        }
        
        final String rawCardData = this.cryptoService.decryptData(rsaEncryptedCardData);
        
        final CustomerCardType customerCardType = this.customerCardTypeService.getCardTypeByTrack2Data(customerId, rawCardData);
        final Track2Card track2Card = new Track2Card(customerCardType, customerId, rawCardData);
        if (!track2Card.isCreditCard()) {
            sbComments.append(columnName).append(" is not credit card, ");
            return null;
        }
        
        return track2Card;
    }
    
    protected final PointOfSale getPointOfSale(final String serialNumber, final StringBuilder sbComments, final Object[] userArg) {
        final PointOfSale ps = getPointOfSale(userArg);
        
        if (ps == null) {
            sbComments.append("No PS for serialNumber (").append(serialNumber);
            sbComments.append(") found ");
            return null;
        } else if (!ps.getSerialNumber().equals(serialNumber)) {
            sbComments.append("PS for line(").append(serialNumber);
            sbComments.append(") does not match PS(").append(ps.getSerialNumber());
            sbComments.append(") for file");
            return null;
        }
        
        return ps;
    }
    
    protected final PointOfSale getPointOfSale(final String serialNumber, final Object[] userArg) throws InvalidCSVDataException {
        final PointOfSale ps = getPointOfSale(userArg);
        if (ps == null || !ps.getSerialNumber().equals(serialNumber)) {
            throw new InvalidCSVDataException("invalid PS");
        }
        
        return ps;
    }
    
    /**
     * Create a new ProcessorTransaction object and set the following properties:
     * - isUploadedFromBoss(true)
     * - createdGmt(DateUtil.getCurrentGmtDate())
     */
    protected final ProcessorTransaction getProcessorTransaction() {
        final ProcessorTransaction processorTransaction = new ProcessorTransaction();
        processorTransaction.setIsUploadedFromBoss(true);
        processorTransaction.setCreatedGmt(DateUtil.getCurrentGmtDate());
        
        return processorTransaction;
    }
    
    protected final CardRetryTransactionType getCardRetryTransactionType(final int id) {
        return this.cardRetryTransactionTypeService.findCardRetryTransactionType(id);
    }
    
    // user argument functions
    protected final int getCustomerId(final Object[] userArg) {
        return (Integer) userArg[0];
    }
    
    protected final void setSendEmailAlert(final Object[] userArg) {
        final ProcessingResult result = (ProcessingResult) userArg[1];
        result.setSendEmail(true);
    }
    
    protected final PointOfSale getPointOfSale(final Object[] userArg) {
        return (PointOfSale) userArg[2];
    }
    
    protected final boolean comparePropertiesForApprovals(final ProcessorTransaction newPt, final ProcessorTransaction emsPt, final StringBuilder sbComments,
        final String refundProcessorTransactionId) {
        boolean transMatch = false;
        switch (emsPt.getProcessorTransactionType().getId()) {
            case CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_AUTHORIZED:
                transMatch = comparePropertiesForAuthorizations(newPt, emsPt, sbComments);
                break;
            
            case CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS:
            case CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_REFUNDS:
            case CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_UNCLOSEABLE:
                transMatch = comparePropertiesForSettlements(newPt, emsPt, sbComments);
                break;
            
            case CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_NO_REFUNDS:
            case CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_REFUNDS:
                transMatch = comparePropertiesForCharges(newPt, emsPt, sbComments, refundProcessorTransactionId);
                break;
            
            // See javadoc for TYPE_CANCELLED
            case CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CANCELLED:
                transMatch = true;
                break;
            
            default:
                transMatch = false;
                sbComments.append("Unexpected Processor Transaction Type");
                break;
        }
        
        if (!transMatch) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("Comparing ").append(emsPt.getProcessorTransactionType().getId()).append(": ");
            sbComments.insert(0, bdr.toString());
        }
        
        return transMatch;
    }
    
    private boolean comparePropertiesForAuthorizations(final ProcessorTransaction newPt, final ProcessorTransaction emsPt, final StringBuilder sbComments) {
        if (emsPt.getProcessorTransactionType().getId() != CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_AUTHORIZED) {
            throw new IllegalArgumentException("Authorization(typeId=1) expected but received typeId:" + emsPt.getProcessorTransactionType().getId());
        }
        
        boolean transMatch = true;
        
        // pointOfSaleId is part of the key
        // MerchantAccountId is not compared as incoming MerchantAccountId is unknown
        // TicketNumber is part of the key
        
        if (emsPt.getAmount() != newPt.getAmount()) {
            sbComments.append("Amount ");
            transMatch = false;
        }
        
        // CardType = "" for auths
        // Last4Digits = 0 for auths
        // CardChecksum is not used for new transactions
        // PurchasedDate is part of the key
        // ProcessingDate is not used for auths
        // ProcessorTransactionId = "" for auths
        
        if (!emsPt.getAuthorizationNumber().equals(newPt.getAuthorizationNumber())) {
            sbComments.append("AuthNumber ");
            transMatch = false;
        }
        
        // ReferenceNumber = "" for auths
        // Approved = true
        // CardHash = "" for auths
        // IsUploadedFromBoss = false for auths
        
        return transMatch;
    }
    
    private boolean comparePropertiesForSettlements(final ProcessorTransaction newPt, final ProcessorTransaction emsPt, final StringBuilder sbComments) {
        if (emsPt.getProcessorTransactionType().getId() != CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS
            && emsPt.getProcessorTransactionType().getId() != CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_REFUNDS
            && emsPt.getProcessorTransactionType().getId() != CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_UNCLOSEABLE) {
            throw new IllegalArgumentException("Settlement(typeId=2,6,8)) expected but received typeId:"
                                               + emsPt.getProcessorTransactionType().getId());
        }
        
        boolean transMatch = true;
        
        // pointOfSaleId is part of the key
        // MerchantAccountId is not compared as incoming MerchantAccountId is unknown
        // TicketNumber is part of the key
        
        if (emsPt.getAmount() != newPt.getAmount()) {
            sbComments.append("Amount ");
            transMatch = false;
        }
        
        if (!emsPt.getCardType().equals(newPt.getCardType())) {
            sbComments.append("Card Type ");
            transMatch = false;
        }
        
        if (emsPt.getLast4digitsOfCardNumber() != newPt.getLast4digitsOfCardNumber()) {
            sbComments.append("Last4DigitsOfCardNumber ");
            transMatch = false;
        }
        
        // CardChecksum is not used for new transactions
        // PurchasedDate is part of the key
        // ProcessingDate is not compared as BOSS processing date is different than EMS
        // ProcessorTransactionId is not compared as BOSS doesn't have this field for settlements
        if (!emsPt.getAuthorizationNumber().equals(newPt.getAuthorizationNumber())) {
            // in boss 5.0.4.1, the authNumber is replaced with <Refunded> (see jira EMS-1082)
            if (!newPt.getAuthorizationNumber().equals(AUTH_VALUE_NA)) {
                sbComments.append("AuthNumber ");
                transMatch = false;
            }
        }
        
        // ReferenceNumber is not compared as BOSS doesn't have this field for settlements
        // Approved = true
        
        // Card hash - only compare if existing transaction has one
        if (emsPt.getCardHash().length() > 0 && !emsPt.getCardHash().equals(newPt.getCardHash())) {
            sbComments.append("CardHash ");
            transMatch = false;
        }
        
        // IsUploadedFromBoss is not compared as it doesn't affect comparison
        
        return transMatch;
    }
    
    private boolean comparePropertiesForCharges(final ProcessorTransaction newPt, final ProcessorTransaction emsPt
            , final StringBuilder sbComments, final String refundProcessorTransactionId) {
        if (emsPt.getProcessorTransactionType().getId() != CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_NO_REFUNDS
            && emsPt.getProcessorTransactionType().getId() != CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_REFUNDS) {
            throw new IllegalArgumentException("Charge(typeId=3,7)) expected but received typeId:" + emsPt.getProcessorTransactionType().getId());
        }
        
        boolean transMatch = true;
        
        // pointOfSaleId is part of the key
        // MerchantAccountId is not compared as incoming MerchantAccountId is unknown
        // TicketNumber is part of the key
        
        if (emsPt.getAmount() != newPt.getAmount()) {
            sbComments.append("Amount ");
            transMatch = false;
        }
        
        if (!emsPt.getCardType().equals(newPt.getCardType())) {
            sbComments.append("Card Type ");
            transMatch = false;
        }
        
        if (emsPt.getLast4digitsOfCardNumber() != newPt.getLast4digitsOfCardNumber()) {
            sbComments.append("Last4DigitsOfCardNumber ");
            transMatch = false;
        }
        
        // CardChecksum is not used for new transactions
        // PurchasedDate is part of the key
        // ProcessingDate is not compared as BOSS processing date is different than EMS
        
        // ProcessorTransactionId - only compare if new transaction has one
        if (newPt.getProcessorTransactionId().length() > 0 && !emsPt.getProcessorTransactionId().equals(newPt.getProcessorTransactionId())) {
            // this additional condition is for Boss 5.0.4.1 since the processorTransactionId is replaced (see
            // jira EMS-1082)
            if (refundProcessorTransactionId == null
                || (refundProcessorTransactionId.length() != 0 && !newPt.getProcessorTransactionId().equals(refundProcessorTransactionId))) {
                sbComments.append("ProcessorTransactionId ");
                transMatch = false;
            }
        }
        
        if (!emsPt.getAuthorizationNumber().equals(newPt.getAuthorizationNumber())) {
            // in boss 5.0.4.1, the authNumber is replaced with <Refunded> (see jira EMS-1082)
            if (!newPt.getAuthorizationNumber().equals(AUTH_VALUE_NA)) {
                sbComments.append("AuthNumber ");
                transMatch = false;
            }
        }
        
        // ReferenceNumber - only compare if new transaction has one
        if (newPt.getReferenceNumber().length() > 0 && !emsPt.getReferenceNumber().equals(newPt.getReferenceNumber())) {
            sbComments.append("ReferenceNumber ");
            transMatch = false;
        }
        
        // Approved = true
        
        // Card hash - only compare if existing transaction has one
        if (emsPt.getCardHash().length() > 0 && !emsPt.getCardHash().equals(newPt.getCardHash())) {
            sbComments.append("CardHash ");
            transMatch = false;
        }
        
        // IsUploadedFromBoss is not compared as it doesn't affect comparison
        
        return transMatch;
    }
    
    protected final ProcessorTransaction getProcessorTransactionForInvalidEncryption(final PointOfSale pointOfSale, final Date purchasedDate
            , final int ticketNumber, final int amountInCents) {
        // now we can construct the processorTransaction object
        final ProcessorTransaction processorTransaction = getProcessorTransaction();
        
        // all the id properties
        processorTransaction.setIsApproved(true);
        // It is not really approved, but if we don't set it
        // to
        // approved it will cause issues with reporting, etc
        processorTransaction.setPointOfSale(pointOfSale);
        processorTransaction.setPurchasedDate(purchasedDate);
        processorTransaction.setProcessingDate(DateUtil.getCurrentGmtDate());
        processorTransaction.setTicketNumber(ticketNumber);
        processorTransaction.setProcessorTransactionType(this.processorTransactionTypeService
                                                         .findProcessorTransactionType(this.transactionService
                                                                                       .getMappingPropertiesValue("processorTransactionType.invalidData.id")));
        
        // all other properties
        processorTransaction.setAmount(amountInCents);
        processorTransaction.setAuthorizationNumber("");
        processorTransaction.setCardType(CardProcessingConstants.NAME_CREDIT_CARD);
        processorTransaction.setReferenceNumber("");
        processorTransaction.setCardHash("");
        processorTransaction.setMerchantAccount(null);
        processorTransaction.setProcessorTransactionId("");
        
        return processorTransaction;
    }
    
    private class ProcessingResult {
        private boolean sendEmail;
        
        public ProcessingResult() {
            this.sendEmail = false;
        }
        
        public void setSendEmail(final boolean sendEmail) {
            this.sendEmail = sendEmail;
        }
        
        public boolean isSendEmail() {
            return this.sendEmail;
        }
    }
}
