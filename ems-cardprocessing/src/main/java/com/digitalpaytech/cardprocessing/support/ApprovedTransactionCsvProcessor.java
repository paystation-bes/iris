package com.digitalpaytech.cardprocessing.support;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.exception.InvalidCSVDataException;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Component("approvedTransactionCsvProcessor")
public class ApprovedTransactionCsvProcessor extends CardTransactionCsvProcessor {
    public static final String[] COLUMN_HEADERS_APPROVED = { 
        "CommAddress", "PurchaseDate", "TicketNumber", "Amount", "CardHash", "Auth", "ProcessorTransID"
        , "ReferenceNumber", "CardLast4Digits", "ProcessDateTime", "CardType", };
    
    protected static final int INDEX_CARD_TYPE = 10;
    protected static final int INDEX_CARD_HASH = 4;
    protected static final int INDEX_CARD_LAST4_DIGITS = 8;
    
    private static Logger logger = Logger.getLogger(ApprovedTransactionCsvProcessor.class);
    
    private static final int INDEX_AUTH = 5;
    private static final int INDEX_PROCESSOR_TRANS_ID = 6;
    private static final int INDEX_REFERENCE_NUMBER = 7;
    private static final int INDEX_PROCESS_DATE_TIME = 9;
    
    private static final String NAME_FROM_BOSS_VISA = "Visa";
    private static final String NAME_FROM_BOSS_MASTERCARD = "MasterCard";
    private static final String NAME_FROM_BOSS_AMEX = "AMEX";
    private static final String NAME_FROM_BOSS_DISCOVER = "Discover";
    private static final String NAME_FROM_BOSS_DINNERS = "Diners";
    
    private static final Map<String, String> CREADIT_CARD_NAMES_MAP = new HashMap<String, String>();
    static {
        CREADIT_CARD_NAMES_MAP.put(NAME_FROM_BOSS_VISA, CardProcessingConstants.NAME_VISA);
        CREADIT_CARD_NAMES_MAP.put(NAME_FROM_BOSS_MASTERCARD, CardProcessingConstants.NAME_MASTERCARD);
        CREADIT_CARD_NAMES_MAP.put(NAME_FROM_BOSS_AMEX, CardProcessingConstants.NAME_AMEX);
        CREADIT_CARD_NAMES_MAP.put(NAME_FROM_BOSS_DISCOVER, CardProcessingConstants.NAME_DISCOVER);
        CREADIT_CARD_NAMES_MAP.put(NAME_FROM_BOSS_DINNERS, CardProcessingConstants.NAME_DINERS);
    }
    
    public ApprovedTransactionCsvProcessor() {
        super();
    }
    
    @Override
    protected final boolean isHeaderLine(final int lineNum) {
        return lineNum == 2;
    }
    
    @Override
    protected final boolean isTransactionTypeLine(final int lineNum) {
        return lineNum == 1;
    }
    
    @Override
    protected String[] getColumnHeaders() {
        return COLUMN_HEADERS_APPROVED;
    }
    
    @Override
    protected int getNumColumns() {
        return COLUMN_HEADERS_APPROVED.length;
    }
    
    @Override
    public void processLine(final String line, final Object[] userArg) {
        boolean emailAlertNeeded = false;
        final int customerId = getCustomerId(userArg);
        ProcessorTransaction processorTransaction = null;
        try {
            final String[] tokens = line.split(DELIMITER, getNumColumns());
            verifyColumnNumber(tokens, getNumColumns());
            
            processorTransaction = getApprovedTransaction(tokens, false, userArg);
            if (handleApprovedCardTransaction(processorTransaction, customerId, line)) {
                emailAlertNeeded = true;
            }
        } catch (Exception e) {
            if (processorTransaction != null) {
                emailAlertNeeded = handleException(processorTransaction.getPointOfSale(), processorTransaction.getPointOfSale().getCustomer(), line, e);
            } else {
                logger.error("processLine method, PROBLEM running 'getApprovedTransaction', line: " + line, e);
            }
        }
        if (emailAlertNeeded) {
            setSendEmailAlert(userArg);
        }
    }
    
    /**
     * construct an approved processotTransaction object from the tokens
     * 
     * @param tokens
     * @param refunded
     * @return
     * @throws InvalidCSVDataException
     */
    protected final ProcessorTransaction getApprovedTransaction(final String[] tokens, final boolean isRefunded
            , final Object[] userArg) throws InvalidCSVDataException {
        final StringBuilder sbComments = new StringBuilder();
        final String[] headers = COLUMN_HEADERS_APPROVED;
        
        // Get PointOfSale
        final PointOfSale pos = getPointOfSale(tokens[INDEX_COMM_ADDRESS], sbComments, userArg);
        if (pos == null) {
            throw new InvalidCSVDataException(sbComments.toString());
        }
        
        // PS exists, so extract values from CSV file
        final Date processingDate = toDate(tokens[INDEX_PROCESS_DATE_TIME], headers[INDEX_PROCESS_DATE_TIME], sbComments);
        final Date purchasedDate = toDate(tokens[INDEX_PURCHASE_DATE], headers[INDEX_PURCHASE_DATE], sbComments);
        final Integer ticketNum = getTicketNumber(tokens[INDEX_TICKET_NUMBER], headers[INDEX_TICKET_NUMBER], sbComments);
        final Integer amountInCents = getAmountInCents(tokens[INDEX_AMOUNT], headers[INDEX_AMOUNT], sbComments);
        final String authNum = getAuthorizationNumber(tokens[INDEX_AUTH], isRefunded, headers[INDEX_AUTH], sbComments);
        final String cardType = getCardType(tokens[INDEX_CARD_TYPE], headers[INDEX_CARD_TYPE], sbComments);
        final short last4Digits = getLast4Digits(tokens[INDEX_CARD_LAST4_DIGITS], headers[INDEX_CARD_LAST4_DIGITS], sbComments);
        final String refNumber = getReferenceNumber(tokens[INDEX_REFERENCE_NUMBER], headers[INDEX_REFERENCE_NUMBER], sbComments);
        final String procTransId = getProcessorTransactionId(tokens[INDEX_PROCESSOR_TRANS_ID], headers[INDEX_PROCESSOR_TRANS_ID], sbComments);
        final String cardHash = getCardHash(tokens[INDEX_CARD_HASH], headers[INDEX_CARD_HASH], sbComments);
        
        // If parsing errors, throw exception
        if (sbComments.length() > 0) {
            sbComments.insert(0, "Parsing: ");
            throw new InvalidCSVDataException(sbComments.toString());
        }
        
        // Now we can construct the processorTransaction object
        final ProcessorTransaction newPt = getProcessorTransaction();
        
        // all the id properties
     // because we're processing approved transaction
        newPt.setIsApproved(true);
        newPt.setPointOfSale(pos);
        newPt.setProcessingDate(processingDate);
        newPt.setPurchasedDate(purchasedDate);
        newPt.setTicketNumber(ticketNum);
        
        // determine the typeId
        int typeId = 0;
        if (!isRefunded) {
            typeId = CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_NO_REFUNDS;
            if (purchasedDate.equals(processingDate)) {
                typeId = CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS;
            }
        } else {
            typeId = CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_REFUNDS;
            if (purchasedDate.equals(processingDate)) {
                typeId = CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_REFUNDS;
            }
        }
        newPt.setProcessorTransactionType(super.getProcessorTransactionTypeService().findProcessorTransactionType(typeId));
        
        // all other properties
        newPt.setAmount(amountInCents);
        newPt.setAuthorizationNumber(authNum);
        newPt.setCardType(cardType);
        newPt.setLast4digitsOfCardNumber((short) last4Digits);
        newPt.setReferenceNumber(refNumber);
        newPt.setCardHash(cardHash);
        newPt.setMerchantAccount(super.getMerchantAccountService()
                                 .findByPointOfSaleIdAndCardTypeId(pos.getId(), CardProcessingConstants.CARD_TYPE_CREDIT_CARD));
        newPt.setProcessorTransactionId(procTransId);
        
        return newPt;
    }
    
    private boolean handleApprovedCardTransaction(final ProcessorTransaction processorTransaction, final int customerId, final String line) {
        if (logger.isDebugEnabled()) {
            logger.debug("processorTransaction: " + processorTransaction.getId());
        }
        boolean emailAlertNeeded = false;
        
        // search existing data
        final ProcessorTransaction existingPt = findExistingProcessorTransaction(processorTransaction);
        if (logger.isDebugEnabled()) {
            logger.debug("existingPt: " + existingPt);
        }
        
        if (existingPt == null) {
            // check if refund transacion exist
            final ProcessorTransaction refundPt = super.getProcessorTransactionService()
                    .findRefundProcessorTransaction(processorTransaction.getPointOfSale().getId(), processorTransaction.getPurchasedDate(),
                                                    processorTransaction.getTicketNumber());
            if (refundPt != null) {
                if (processorTransaction.getProcessorTransactionType().getId() == CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS) {
                    processorTransaction.setProcessorTransactionType(super.getProcessorTransactionTypeService()
                            .findProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_REFUNDS));
                } else if (processorTransaction.getProcessorTransactionType().getId() == CardProcessingConstants
                        .PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_NO_REFUNDS) {
                    processorTransaction.setProcessorTransactionType(super.getProcessorTransactionTypeService()
                            .findProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_REFUNDS));
                }
            }
            // not yet exist, save it to the database
            super.getEntityDao().save(processorTransaction);
        } else {
            // already exist, compare other fields
            
            final StringBuilder comments = new StringBuilder();
            final boolean transMatch = comparePropertiesForApprovals(processorTransaction, existingPt, comments, null);
            
            if (transMatch) {
                // Data matches
                logger.info("Uploaded trans matches existing trans: " + processorTransaction.getId());
            } else {
                // Data does not match
                logger.info("Transaction data mismatch found, from BOSS: " + processorTransaction + "; in database: " + existingPt);
                // save to ImportError table
                emailAlertNeeded = true;
            }
        }
        
        return emailAlertNeeded;
    }
    
    protected final ProcessorTransaction findExistingProcessorTransaction(final ProcessorTransaction processorTransaction) {
        return super.getProcessorTransactionService().getApprovedTransaction(processorTransaction.getPointOfSale().getId(),
                                                                             processorTransaction.getPurchasedDate(), processorTransaction.getTicketNumber());
    }
    
    protected final String getProcessorTransactionId(final String value, final String columnName, final StringBuilder sbComments) {
        // min length = 0 to indicate optional
        final String retVal = CardTransactionUploadHelper.checkAndExtractString(value, 0, WebCoreConstants.MAX_CARD_PROCESSING_PROCESSOR_TRANSACTION_ID_LENGTH,
                                                                           columnName, sbComments);
        
        return retVal;
    }
    
    protected final String getCardType(final String value, final String columnName, final StringBuilder sbComments) {
        final String cardTypeFromBoss = CardTransactionUploadHelper.checkAndExtractString(value, WebCoreConstants.MIN_CARD_TYPE_LENGTH,
                                                                                    WebCoreConstants.MAX_CARD_TYPE_LENGTH, columnName, sbComments);
        
        if (cardTypeFromBoss == null) {
            sbComments.append(columnName).append("is null, ");
            return null;
        }
        
        final String cardTypeForEms = CREADIT_CARD_NAMES_MAP.get(cardTypeFromBoss);
        if (cardTypeForEms == null) {
            sbComments.append(columnName).append("has no Digital Iris card type, ");
            return null;
        }
        
        return cardTypeForEms;
    }
    
    protected final short getLast4Digits(final String value, final String columnName, final StringBuilder sbComments) {
        final short shValue = CardTransactionUploadHelper.getShort(value, WebCoreConstants.MIN_LAST_4_DIGITS_OF_CREDIT_CARD_NUMBER,
                                                              WebCoreConstants.MAX_LAST_4_DIGITS_OF_CREDIT_CARD_NUMBER, columnName, sbComments);
        
        return shValue;
    }
    
    protected final String getReferenceNumber(final String value, final String columnName, final StringBuilder sbComments) {
        // min length = 0 to indicate optional
        final String retVal = CardTransactionUploadHelper
                .checkAndExtractString(value, 0, WebCoreConstants.MAX_CARD_PROCESSING_REFERENCE_NUMBER_LENGTH, columnName, sbComments);
        
        return retVal;
    }
    
    private String getAuthorizationNumber(final String authValue, final boolean isRefunded, final String columnName, final StringBuilder sbComments) {
        if (isRefunded && AUTH_VALUE_REFUNDED.equals(authValue)) {
            return AUTH_VALUE_NA;
        }
        
        // Min length = 1 to indicate required
        final String retVal = CardTransactionUploadHelper.checkAndExtractString(authValue, 1, WebCoreConstants.MAX_CARD_PROCESSING_AUTHORIZATION_NUMBER_LENGTH,
                                                                           columnName, sbComments);
        
        return retVal;
    }
}
