package com.digitalpaytech.cardprocessing.support;

import java.util.ArrayList;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.CustomerBadCard;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.DuplicateObjectException;
import com.digitalpaytech.exception.InvalidCSVDataException;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.crypto.CryptoConstants;

@Component("badCardsCsvProcessor")
public class BadCardsCsvProcessor extends CardTransactionCsvProcessor {
    private static Logger logger = Logger.getLogger(BadCardsCsvProcessor.class);
    private static final int INDEX_BADCARD_CARD_DATA = 0;
    private static final int INDEX_BADCARD_COMMENT = 1;
    
    private static final int MAX_LENGTH_BADCARD_COMMENT = 100;
    
    public static final String[] COLUMN_HEADERS_BAD_CREDIT_CARD_LIST = { "CardData", "Comment" };
    
    public BadCardsCsvProcessor() {
        super();
    }
    
    @Override
    protected String[] getColumnHeaders() {
        return COLUMN_HEADERS_BAD_CREDIT_CARD_LIST;
    }
    
    @Override
    protected int getNumColumns() {
        return COLUMN_HEADERS_BAD_CREDIT_CARD_LIST.length;
    }
    
    @Override
    protected boolean isHeaderLine(int lineNum) {
        return (lineNum == 2);
    }
    
    @Override
    protected boolean isTransactionTypeLine(int lineNum) {
        return (lineNum == 1);
    }
    
    @Override
    public void processLine(String line, Object[] userArg) {
        boolean emailAlertNeeded = false;
        int customerId = getCustomerId(userArg);
        CustomerBadCard badCreditCard = null;
        try {
            String tokens[] = line.split(DELIMITER, getNumColumns());
            verifyColumnNumber(tokens, getNumColumns());
            
            badCreditCard = getCustomerBadCreditCard(tokens, customerId);
            if (logger.isDebugEnabled()) {
                logger.debug("badCreditCard: " + badCreditCard);
            }
            
            if (handleBadCreditCard(badCreditCard)) {
                emailAlertNeeded = true;
            }
        } catch (Exception e) {
            if (badCreditCard != null) {
                Set<PointOfSale> set = badCreditCard.getCustomerCardType().getCustomer().getPointOfSales();
                if (set != null && !set.isEmpty()) {
                    PointOfSale pos = new ArrayList<PointOfSale>(set).get(0);
                    emailAlertNeeded = handleException(pos, badCreditCard.getCustomerCardType().getCustomer(), line, e);
                } else {
                    StringBuilder bdr = new StringBuilder();
                    bdr.append("processLine, PROBLEM finding PointOfSale from customer id: ").append(badCreditCard.getCustomerCardType().getCustomer().getId());
                    bdr.append(", line: ").append(line);
                    logger.error(bdr.toString(), e);
                }
            } else {
                logger.error("badCreditCard, PROBLEM calling 'getCustomerBadCreditCard', line: " + line, e);
            }
        }
        if (emailAlertNeeded) {
            setSendEmailAlert(userArg);
        }
    }
    
    private CustomerBadCard getCustomerBadCreditCard(String[] tokens, int customerId) throws InvalidCSVDataException, CryptoException {
        // store the field names with invalid data
        StringBuilder sbComments = new StringBuilder();
        String headers[] = COLUMN_HEADERS_BAD_CREDIT_CARD_LIST;
        
        String comment = CardTransactionUploadHelper.checkAndExtractString(tokens[INDEX_BADCARD_COMMENT], 0, MAX_LENGTH_BADCARD_COMMENT,
                                                                           headers[INDEX_BADCARD_COMMENT], sbComments);
        
        Track2Card track2Card = null;
        try {
            track2Card = getCreditCardData(tokens[INDEX_BADCARD_CARD_DATA], headers[INDEX_BADCARD_CARD_DATA], sbComments, customerId, true);
        } catch (CryptoException e) {
            sbComments.append(headers[INDEX_BADCARD_CARD_DATA] + " cannot be decrypted");
            logger.error(sbComments.toString(), e);
        }
        
        // If parsing errors, throw exception
        if (sbComments.length() > 0) {
            sbComments.insert(0, "Parsing: ");
            throw new InvalidCSVDataException(sbComments.toString());
        }
        
        CustomerCardType customerCardType = super.getCustomerCardTypeService().getCardTypeByTrack2Data(customerId, track2Card.getDecryptedCardData());
        String badCardHash = super.getCryptoAlgorithmFactory().getSha1Hash(track2Card.getPrimaryAccountNumber(), CryptoConstants.HASH_BAD_CREDIT_CARD);
        
        CustomerBadCard badCreditCard = new CustomerBadCard();
        
        badCreditCard.setCustomerCardType(customerCardType);
        badCreditCard.setCardNumberOrHash(badCardHash);
        badCreditCard.setCardExpiry(Short.parseShort(track2Card.getExpirationYear() + track2Card.getExpirationMonth()));
        badCreditCard.setAddedGmt(DateUtil.getCurrentGmtDate());
        badCreditCard.setComment(comment);
        String aesEncryptedCardData = cryptoService.encryptData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL, track2Card.getDecryptedCardData());
        badCreditCard.setCardData(aesEncryptedCardData);
        
        return badCreditCard;
    }
    
    private boolean handleBadCreditCard(CustomerBadCard badCreditCard) {
        // save it to db
        try {
            if (badCreditCard.getId() == null) {
                super.getEntityDao().save(badCreditCard);
            } else {
                Object obj = super.getEntityDao().get(CustomerBadCard.class, badCreditCard.getId());
                if (obj == null) {
                    super.getEntityDao().save(badCreditCard);
                }
            }
        } catch (DuplicateObjectException e) {
            logger.warn("BadCreditCard already exists in the database");
        }
        return false;
    }
}
