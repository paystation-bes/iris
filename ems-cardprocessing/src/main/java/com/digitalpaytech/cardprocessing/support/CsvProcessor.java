package com.digitalpaytech.cardprocessing.support;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.springframework.web.multipart.MultipartFile;

import com.digitalpaytech.exception.InvalidCSVDataException;
import com.digitalpaytech.exception.SystemException;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;

/**
 * Abstract CSV processor. Loop through every line and call the processLine function,<br>
 * which should be implemented by subclass.
 * 
 * Copied from EMS 6.3.11 com.digitalpioneer.util.csv.CsvProcessor
 */

public abstract class CsvProcessor {
    protected static final String DELIMITER = ",";
    protected static final String EMPTY_STRING = WebCoreConstants.EMPTY_STRING;
    
    /**
     * Process a multipart file. Should only be called by subclass.<br>
     * Each implementation should create their own public function to pares a file.<br>
     * So that userArg format is only known by the subclass itself.
     * 
     * @param multipartFile
     * @param userArg
     *            User arguments that will be passed to processLine()
     * @throws InvalidCSVDataException
     * @throws SystemException
     */
    protected void processFile(final MultipartFile multipartFile, final Object[] userArg, final int maxSize) throws InvalidCSVDataException {
        if (multipartFile.getSize() < 1) {
            throw new InvalidCSVDataException("The file is empty");
        }
        if (multipartFile.getSize() > maxSize) {
            throw new InvalidCSVDataException("The file size is greater than " + maxSize / StandardConstants.BYTES_PER_MEGABYTE + " MB ");
        }
        
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(multipartFile.getInputStream()));
            processFile(reader, userArg);
        } catch (IOException e) {
            throw new InvalidCSVDataException("Unable to read file.");
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e) {
                }
            }
        }
    }
    
    /**
     * Process a zip file entry. Should only be called by subclass.<br>
     * Each implementation should create their own public function to pares a file.<br>
     * So that userArg format is only known by the subclass itself.
     * 
     * @param zipFile
     * @param zipEntry
     * @param userArg
     *            User arguments that will be passed to processLine()
     * @throws InvalidCSVDataException
     */
    protected void processFile(final ZipFile zipFile, final ZipEntry zipEntry, final Object[] userArg) throws InvalidCSVDataException {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(zipFile.getInputStream(zipEntry)));
            processFile(reader, userArg);
        } catch (IOException e) {
            throw new InvalidCSVDataException("Unable to read file.");
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e) {
                }
            }
        }
    }
    
    /**
     * Process all lines
     * 
     * @param reader
     * @param userArg
     * @throws InvalidCSVDataException
     */
    private void processFile(final BufferedReader reader, final Object[] userArg) throws InvalidCSVDataException {
        try {
            // process each line
            int lineNum = 1;
            String line = reader.readLine();
            
            while (line != null) {
                if (line.trim().length() > 0) {
                    processLine(line, lineNum, userArg);
                }
                lineNum++;
                line = reader.readLine();
            }
        } catch (IOException e) {
            throw new InvalidCSVDataException("Unable to read file.");
        }
    }
    
    /**
     * Process a line.
     * 
     * @param line
     * @param lineNum
     * @param userArg
     * @throws InvalidCSVDataException
     */
    public abstract void processLine(String line, int lineNum, Object[] userArg) throws InvalidCSVDataException;
    
    /**
     * Verify the headers
     * 
     * @param tokens
     *            Headers read from CSV file.
     * @param headers
     *            Expected headers.
     * @param columns
     *            Number of columns.
     * @throws InvalidCSVDataException
     */
    protected void verifyHeaders(final String[] tokens, final String[] headers, final int columns) throws InvalidCSVDataException {
        // Incorrect number of columns
        if (tokens.length != columns) {
            throw new InvalidCSVDataException("Invalid Header Data - Incorrect # of columns");
        }
        
        for (int i = 0; i < columns; i++) {
            if (!headers[i].equals(tokens[i].trim())) {
                throw new InvalidCSVDataException("Invalid Header Data - Header: " + i + " is: '" + WebCoreUtil.encodeHTMLTag(tokens[i])
                                                  + "', but should be '" + headers[i] + "'");
            }
        }
    }
    
    /**
     * Verify the number of columns.
     * 
     * @param tokens
     * @param length
     * @throws InvalidCSVDataException
     */
    protected void verifyColumnNumber(final String[] tokens, final int length) throws InvalidCSVDataException {
        // check the number of columns
        if (tokens == null || tokens.length == 0) {
            throw new InvalidCSVDataException("Parsing: No data");
        }
        if (tokens.length != length) {
            throw new InvalidCSVDataException("Parsing: insufficient data");
        }
    }
    
    protected final boolean isEmpty(final String value) {
        return value == null || value.trim().length() == 0;
    }
    
    protected final boolean isInteger(final String value) {
        return isInteger(value, false);
    }
    
    protected final boolean isInteger(final String value, final boolean allowWildcards) {
        if (isEmpty(value)) {
            return false;
        }
        
        if (value.length() > 8) {
            return false;
        }
        
        for (int i = 0; i < value.length(); i++) {
            final char character = value.charAt(i);
            if (!Character.isDigit(character)) {
                return false;
            }
        }
        return true;
    }
    
    protected final int getInt(final String value) {
        if (isEmpty(value)) {
            return -1;
        } else if (!isInteger(value, false)) {
            return -1;
        }
        
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            return -1;
        }
    }
    
    protected final boolean getBoolean(final Object obj) {
        return getBoolean((String) obj);
    }
    
    protected final boolean getBoolean(final String value) {
        if (isEmpty(value)) {
            return false;
        }
        
        return "on".equalsIgnoreCase(value) || "true".equalsIgnoreCase(value) || "yes".equalsIgnoreCase(value);
    }
    
    protected final boolean isValidString(final String value) {
        if (value == null) {
            return false;
        }
        
        return value.matches(WebCoreConstants.REGEX_PRINTABLE_TEXT);
    }
    
    protected final boolean isAcceptedChar(final char character) {
        // Check if alphanumeric
        if (Character.isLetterOrDigit(character)) {
            return true;
        }
        
        // Check if allowed non-alphanumerics
        for (int i = 0; i < WebCoreConstants.ALLOWED_NON_ALPHANUM_CHARACTERS.length; i++) {
            if (character == WebCoreConstants.ALLOWED_NON_ALPHANUM_CHARACTERS[i]) {
                return true;
            }
        }
        // If we made it here, means it is not on accepted list
        return false;
    }
    
    protected final String trimString(final String str) {
        if ((str != null) && (str.length() > 0)) {
            return str.trim();
        } else {
            return str;
        }
    }
    
    protected final String appendStrings(final String... strings) {
        StringBuilder bdr = new StringBuilder();
        for (String s : strings) {
            bdr = bdr.append(s);
        }
        return bdr.toString();
    }
}
