package com.digitalpaytech.cardprocessing.support;

import java.util.Date;

import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.crypto.CryptoConstants;
import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.DuplicateObjectException;
import com.digitalpaytech.exception.InvalidCSVDataException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component("nonApprovedTransactionCsvProcessor")
@Transactional(propagation=Propagation.SUPPORTS)
public class NonApprovedTransactionCsvProcessor extends CardTransactionCsvProcessor {
    private static Logger logger = Logger.getLogger(NonApprovedTransactionCsvProcessor.class);
    private static final int INDEX_CARD_DATA = 4;
    
    public static final String[] COLUMN_HEADERS_NON_APPROVED_WITHOUT_RETRIES = { "CommAddress", "PurchaseDate", "TicketNumber", "Amount", "CardData" };
    
    public NonApprovedTransactionCsvProcessor() {
        super();
    }
    
    @Override
    protected String[] getColumnHeaders() {
        return COLUMN_HEADERS_NON_APPROVED_WITHOUT_RETRIES;
    }
    
    @Override
    protected int getNumColumns() {
        return getColumnHeaders().length;
    }
    
    @Override
    protected boolean isHeaderLine(int lineNum) {
        return (lineNum == 1);
    }
    
    @Override
    protected boolean isTransactionTypeLine(int lineNum) {
        // no transaction type line
        return false;
    }
    
    @Transactional(propagation=Propagation.REQUIRES_NEW)
    @Override
    public void processLine(String line, Object[] userArg) {
        boolean emailAlertNeeded = false;
        int customerId = getCustomerId(userArg);
        CardRetryTransaction cardRetryTransaction = null;
        try {
            String tokens[] = line.split(DELIMITER, getNumColumns());
            verifyColumnNumber(tokens, getNumColumns());
            
            cardRetryTransaction = getNonApprovedTransaction(tokens, getColumnHeaders(), userArg);
            
            if (logger.isDebugEnabled()) {
                logger.debug("cardRetryTransaction: " + cardRetryTransaction);
            }
            
            CardRetryTransaction tx = super.getCardRetryTransactionService().findByPointOfSalePurchasedDateTicketNumber(cardRetryTransaction
                                                                                                                                .getPointOfSale().getId(),
                                                                                                                        cardRetryTransaction
                                                                                                                                .getPurchasedDate(),
                                                                                                                        cardRetryTransaction
                                                                                                                                .getTicketNumber());
            if (tx == null) {
                // save it to db
                super.getEntityDao().save(cardRetryTransaction);
            } else {
                cardRetryTransaction = tx;
            }
            
            // Add it to the in memory processing queue
            if (cardRetryTransaction.getNumRetries() < Short.MAX_VALUE) {
                cardProcessingMaster.addTransactionToStoreForwardQueue(cardRetryTransaction);
            }
            
        } catch (DuplicateObjectException e) {
            logger.warn("CardRetryTransaction already exists in the database");
        
        } catch (Exception e) {
            if (cardRetryTransaction != null) {
                emailAlertNeeded = super.handleException(cardRetryTransaction.getPointOfSale(), cardRetryTransaction.getPointOfSale().getCustomer(), line, e);
            } else {
                logger.error("processLine, getNonApprovedTransaction, PROBLEM getting cardRetryTransaction, line: " + line, e);
            }
            if (emailAlertNeeded) {
                setSendEmailAlert(userArg);
            }
            throw new CardTransactionException(e);
        }
    }
    
    /**
     * construct a non-approved CardRetryTransaction object from the tokens
     * 
     * @param tokens
     * @param headers
     * @return
     * @throws InvalidCSVDataException
     * @throws CryptoException
     */
    private CardRetryTransaction getNonApprovedTransaction(String[] tokens, String[] headers, Object[] userArg) throws InvalidCSVDataException, CryptoException {
        // store the field names with invalid data
        StringBuilder sbComments = new StringBuilder();
        PointOfSale pointOfSale = super.getPointOfSale(tokens[INDEX_COMM_ADDRESS], userArg);
        
        // PS exists, so extract values from CSV file
        Date purchaseDate = toDate(tokens[INDEX_PURCHASE_DATE], headers[INDEX_PURCHASE_DATE], sbComments);
        Integer ticketNumber = this.getTicketNumber(tokens[INDEX_TICKET_NUMBER], headers[INDEX_TICKET_NUMBER], sbComments);
        Integer amountInCents = getAmountInCents(tokens[INDEX_AMOUNT], headers[INDEX_AMOUNT], sbComments);
        
        int numRetries = getNumRetries(tokens, sbComments);
        
        Track2Card track2Card = null;
        try {
            track2Card = this.getCreditCardData(tokens[INDEX_CARD_DATA], headers[INDEX_CARD_DATA], sbComments, pointOfSale.getCustomer().getId(), true);
        } catch (CryptoException e) {
            if (sbComments.length() == 0) {
                ProcessorTransaction processorTransaction = this.getProcessorTransactionForInvalidEncryption(pointOfSale, purchaseDate, ticketNumber,
                                                                                                             amountInCents);
                super.getEntityDao().save(processorTransaction);
            }
            sbComments.append(headers[INDEX_CARD_DATA] + " cannot be decrypted");
            logger.info(sbComments.toString(), e);
        }
        
        // If parsing errors, throw exception
        if (sbComments.length() > 0) {
            sbComments.insert(0, "Parsing: ");
            throw new InvalidCSVDataException(sbComments.toString());
        }
        // now we can construct the processorTransaction object
        CardRetryTransaction cardRetryTransaction = new CardRetryTransaction();
        
        cardRetryTransaction.setPointOfSale(pointOfSale);
        cardRetryTransaction.setPurchasedDate(purchaseDate);
        cardRetryTransaction.setTicketNumber(ticketNumber);
        
        // all other properties
        cardRetryTransaction.setAmount(amountInCents);
        Date now = DateUtil.getCurrentGmtDate();
        cardRetryTransaction.setLastRetryDate(now);
        cardRetryTransaction.setCreationDate(now);
        cardRetryTransaction.setNumRetries(numRetries);
        
        String cardHash = super.getCryptoAlgorithmFactory().getSha1Hash(track2Card.getCreditCardPanAndExpiry(), CryptoConstants.HASH_CREDIT_CARD_PROCESSING);
        String badCardHash = super.getCryptoAlgorithmFactory().getSha1Hash(track2Card.getPrimaryAccountNumber(), CryptoConstants.HASH_BAD_CREDIT_CARD);
        
        cardRetryTransaction.setCardHash(cardHash);
        cardRetryTransaction.setBadCardHash(badCardHash);
        cardRetryTransaction.setCardType(track2Card.getDisplayName());
        cardRetryTransaction.setLast4digitsOfCardNumber(track2Card.getLast4DigitsOfAccountNumber());
        cardRetryTransaction.setCardExpiry(Short.parseShort(track2Card.getExpirationYear() + track2Card.getExpirationMonth()));
        String aesEncryptedCardData = cryptoService.encryptData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL, track2Card.getCreditCardPanAndExpiry());
        cardRetryTransaction.setCardData(aesEncryptedCardData);
        cardRetryTransaction.setCardRetryTransactionType(super.getCardRetryTransactionTypeService()
                                                         .findCardRetryTransactionType(WebCoreConstants.CARD_RETRY_TRANSACTION_TYPE_BATCHED));
        
        return cardRetryTransaction;
    }
    
    /**
     * handle non approved card transactions
     * 
     * @param cardRetryTransaction
     * @param customerId
     * @param line
     * @return
     */
    private boolean handleNonApprovedCardTransaction(CardRetryTransaction cardRetryTransaction, int customerId, String line) {
        try {
            if (cardRetryTransaction.getId() == null) {
                // save it to db
                super.getEntityDao().save(cardRetryTransaction);
            } else {
                CardRetryTransaction tx = super.getCardRetryTransactionService().findByPointOfSalePurchasedDateTicketNumber(cardRetryTransaction
                                                                                                                                    .getPointOfSale().getId(),
                                                                                                                            cardRetryTransaction
                                                                                                                                    .getPurchasedDate(),
                                                                                                                            cardRetryTransaction
                                                                                                                                    .getTicketNumber());
                if (tx == null) {
                    super.getEntityDao().save(cardRetryTransaction);
                }
            }
            
            // Add it to the in memory processing queue
            if (cardRetryTransaction.getNumRetries() < Short.MAX_VALUE) {
                cardProcessingMaster.addTransactionToStoreForwardQueue(cardRetryTransaction);
            }
        } catch (DuplicateObjectException e) {
            logger.warn("CardRetryTransaction already exists in the database");
        }
        
        return false;
    }
    
    protected int getNumRetries(String[] tokens, StringBuilder sbComments) {
        return 0;
    }
    
}
