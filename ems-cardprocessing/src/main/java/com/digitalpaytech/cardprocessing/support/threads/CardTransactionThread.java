package com.digitalpaytech.cardprocessing.support.threads;

import java.util.Vector;

import org.apache.log4j.Logger;

import com.digitalpaytech.cardprocessing.support.CardTransactionZipProcessor;
import com.digitalpaytech.util.EMSThread;

/*
 * Copied from EMS 6.3.11 com.digitalpioneer.rpc.cardtransactions.CardTransactionThread
 */

public class CardTransactionThread extends EMSThread {
    private final static Logger logger = Logger.getLogger(CardTransactionThread.class);

    private Vector<String> transactions = new Vector<String>();

    private int threadId = 0;
    private CardTransactionZipProcessor cardTransactionZipProcessor;

    public CardTransactionThread(int threadId, CardTransactionZipProcessor cardTransactionZipProcessor)
    {
        super(CardTransactionZipProcessor.class.getName());

//        setDaemon(true);

        this.threadId = threadId;

        this.cardTransactionZipProcessor = cardTransactionZipProcessor;
    }

    public synchronized void addTransactionToQueue(String zipFileFullName)
    {
        transactions.add(zipFileFullName);

        // Wake up processor
        synchronized (this)
        {
            this.notify();
        }
    }

    private synchronized String getNextTransactionFromQueue()
    {
        if (isTransactionQueueEmpty())
        {
            return (null);
        }

        if (cardTransactionZipProcessor.isDisableProcessing())
        {
            if (logger.isInfoEnabled()) {
                logger.info("Don't processing next transaction file, cause flag be set in EmsProperties table");
            }
            transactions.clear(); 
            return (null);
        }
        
        String zipFileFullName = transactions.remove(0);
        return zipFileFullName;
    }

    private synchronized boolean isTransactionQueueEmpty()
    {
        return (transactions.isEmpty());
    }

    public void run()
    {
        if (logger.isInfoEnabled()) {
            logger.info("Starting CardTransactionThread: " + threadId);
        }
        String zipFileFullName = null;

        while (!isShutdown())
        {
            zipFileFullName = getNextTransactionFromQueue();
            if (zipFileFullName != null)
            {
                cardTransactionZipProcessor.processCardTransaction(zipFileFullName);
            }

            // if no more transactions, wait.
            if (isTransactionQueueEmpty())
            {
                try
                {
                    synchronized (this)
                    {
                        wait();
                    }
                }
                catch (InterruptedException e)
                {
                    // Empty block
                }
            }
        }
    }

}
