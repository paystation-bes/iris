package com.digitalpaytech.cardprocessing.support;

import java.util.Date;

import org.springframework.stereotype.Component;
import org.apache.log4j.Logger;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.exception.InvalidCSVDataException;
import com.digitalpaytech.util.CardProcessingConstants;

@Component("refundedTransactionCsvProcessor")
public class RefundedTransactionCsvProcessor extends ApprovedTransactionCsvProcessor {
    public static final String[] COLUMN_HEADERS_REFUNDED = { "CommAddress", "PurchaseDate", "TicketNumber", "Amount", "CardHash", "Auth", "ProcessorTransID"
        , "ReferenceNumber", "CardLast4Digits", "ProcessDateTime", "CardType", "RefundDate", "RefundAmount", "RefundReferenceNumber", "RefundTransID", };
    
    private static final int INDEX_REFUND_DATE = 11;
    private static final int INDEX_REFUND_AMOUNT = 12;
    private static final int INDEX_REFUND_REFERENCE_NUMBER = 13;
    private static final int INDEX_REFUND_TRANS_ID = 14;
    
    private static Logger logger = Logger.getLogger(RefundedTransactionCsvProcessor.class);
    
    public RefundedTransactionCsvProcessor() {
        super();
    }
    
    @Override
    protected final String[] getColumnHeaders() {
        return COLUMN_HEADERS_REFUNDED;
    }
    
    @Override
    protected final int getNumColumns() {
        return COLUMN_HEADERS_REFUNDED.length;
    }
    
    @Override
    public void processLine(final String line, final Object[] userArg) {
        boolean emailAlertNeeded = false;
        final int customerId = getCustomerId(userArg);
        ProcessorTransaction approvedTransaction = null;
        try {
            final String[] tokens = line.split(DELIMITER, getNumColumns());
            verifyColumnNumber(tokens, getNumColumns());
            
            approvedTransaction = getApprovedTransaction(tokens, true, userArg);
            final ProcessorTransaction refundedTransaction = getRefundedTransaction(tokens, userArg);
            logger.debug("approvedTransaction: " + approvedTransaction);
            logger.debug("refundedTransaction: " + refundedTransaction);
            
            if (handleRefundedCardTransaction(approvedTransaction, refundedTransaction, customerId, line)) {
                emailAlertNeeded = true;
            }
        } catch (Exception e) {
            if (approvedTransaction != null) {
                emailAlertNeeded = handleException(approvedTransaction.getPointOfSale(), approvedTransaction.getPointOfSale().getCustomer(), line, e);
            } else {
                logger.error("processLine method, PROBLEM running 'getApprovedTransaction', line: " + line, e);
            }
            
        }
        if (emailAlertNeeded) {
            setSendEmailAlert(userArg);
        }
    }
    
    /**
     * construct a refunded processotTransaction object from the tokens
     * 
     * @param tokens
     * @return
     * @throws InvalidCSVDataException
     */
    private ProcessorTransaction getRefundedTransaction(final String[] tokens, final Object[] userArg) throws InvalidCSVDataException {
        final StringBuilder sbComments = new StringBuilder();
        final String[] headers = COLUMN_HEADERS_REFUNDED;
        
        // Get PointOfSale
        final PointOfSale pos = super.getPointOfSale(tokens[INDEX_COMM_ADDRESS], sbComments, userArg);
        if (pos == null) {
            throw new InvalidCSVDataException(sbComments.toString());
        }
        
        // PS exists, so extract values from CSV file
        final Date processingDate = toDate(tokens[INDEX_REFUND_DATE], headers[INDEX_REFUND_DATE], sbComments);
        final Date purchasedDate = toDate(tokens[INDEX_PURCHASE_DATE], headers[INDEX_PURCHASE_DATE], sbComments);
        final Integer ticketNum = getTicketNumber(tokens[INDEX_TICKET_NUMBER], headers[INDEX_TICKET_NUMBER], sbComments);
        final Integer amountInCents = getAmountInCents(tokens[INDEX_REFUND_AMOUNT], headers[INDEX_REFUND_AMOUNT], sbComments);
        final String cardType = getCardType(tokens[INDEX_CARD_TYPE], headers[INDEX_CARD_TYPE], sbComments);
        final short last4Digits = getLast4Digits(tokens[INDEX_CARD_LAST4_DIGITS], headers[INDEX_CARD_LAST4_DIGITS], sbComments);
        final String refNumber = getReferenceNumber(tokens[INDEX_REFUND_REFERENCE_NUMBER], headers[INDEX_REFUND_REFERENCE_NUMBER], sbComments);
        final String procTransId = getProcessorTransactionId(tokens[INDEX_REFUND_TRANS_ID], headers[INDEX_REFUND_TRANS_ID], sbComments);
        final String cardHash = getCardHash(tokens[INDEX_CARD_HASH], headers[INDEX_CARD_HASH], sbComments);
        
        // If parsing errors, throw exception
        if (sbComments.length() > 0) {
            sbComments.insert(0, "Parsing: ");
            throw new InvalidCSVDataException(sbComments.toString());
        }
        
        // Now we can construct the processorTransaction object
        final ProcessorTransaction newPt = getProcessorTransaction();
        
        // all the id properties
        // because we're processing approved transaction
        newPt.setIsApproved(true);
        newPt.setPointOfSale(pos);
        newPt.setProcessingDate(processingDate);
        newPt.setPurchasedDate(purchasedDate);
        newPt.setTicketNumber(ticketNum);
        
        final int typeId = super.getTransactionService().getMappingPropertiesValue("processorTransactionType.refund.id");
        newPt.setProcessorTransactionType(super.getProcessorTransactionTypeService().findProcessorTransactionType(typeId));
        
        // all other properties
        newPt.setAmount(amountInCents);
        newPt.setCardType(cardType);
        newPt.setLast4digitsOfCardNumber(last4Digits);
        newPt.setReferenceNumber(refNumber);
        newPt.setCardHash(cardHash);
        newPt.setMerchantAccount(super.getMerchantAccountService()
                                 .findByPointOfSaleIdAndCardTypeId(pos.getId(), CardProcessingConstants.CARD_TYPE_CREDIT_CARD));
        newPt.setProcessorTransactionId(procTransId);
        
        return newPt;
    }
    
    /**
     * handle refunded card transactions
     * 
     * @param approvedTransaction
     * @param refundedTransaction
     * @param customerId
     *            the record read from the csv file
     * @return true if email alert is needed
     */
    private boolean handleRefundedCardTransaction(final ProcessorTransaction approvedTransaction
            , final ProcessorTransaction refundedTransaction, final int customerId, final String line) {
        boolean emailAlertNeeded = false;
        
        // search existing data
        final ProcessorTransaction existingApprovedPt = super.findExistingProcessorTransaction(approvedTransaction);
        final ProcessorTransaction existingRefundedPt = super.getProcessorTransactionService()
                .findRefundProcessorTransaction(refundedTransaction.getPointOfSale().getId(), refundedTransaction.getPurchasedDate()
                                                , refundedTransaction.getTicketNumber());
        
        if (logger.isDebugEnabled()) {
            logger.debug("existingApprovedPt: " + existingApprovedPt);
            logger.debug("existingRefundedPt: " + existingRefundedPt);
        }
        
        if (existingApprovedPt == null) {
            // approved transaction not exist yet
            if (existingRefundedPt == null) {
                // Refund transaction does not exist
                if (logger.isInfoEnabled()) {
                    logger.info("Approved and refund transation do not exist, creating new transactions");
                }
                super.getEntityDao().save(approvedTransaction);
                super.getEntityDao().save(refundedTransaction);
            } else {
                // Refund transaction exists
                if (logger.isInfoEnabled()) {
                    logger.info("Refund transaction exists, but approved transaction does not");
                }
                emailAlertNeeded = handleRefundedTransWhenOnlyRefundTransExist(approvedTransaction, refundedTransaction, existingRefundedPt, customerId, line);
            }
        } else {
            // Approved transaction exists
            if (existingRefundedPt == null) {
                // Refund transaction does not exist
                logger.info("Approved transaction exists, but refund transaction does not.");
                emailAlertNeeded = handleRefundedTransWhenOnlyApprovedTransExist(approvedTransaction, existingApprovedPt
                                                                                 , refundedTransaction, customerId, line);
            } else {
                // Refund transaction exists
                logger.info("Approved and refund transaction exist.");
                emailAlertNeeded = handleRefundedTransWhenApprovedAndRefundTransExist(approvedTransaction, existingApprovedPt, refundedTransaction,
                                                                                      existingRefundedPt, customerId, line);
            }
        }
        
        return emailAlertNeeded;
    }
    
    private boolean handleRefundedTransWhenOnlyRefundTransExist(final ProcessorTransaction newApprovedPt, final ProcessorTransaction newRefundPt
            , final ProcessorTransaction existingRefundPt, final int customerId, final String line) {
        boolean emailAlertNeeded = false;
        
        // Compare fields of refund transaction
        final StringBuilder comments = new StringBuilder();
        final boolean transMatch = comparePropertiesForRefunds(newRefundPt, existingRefundPt, comments);
        
        if (transMatch) {
            // Data matches
            if (logger.isInfoEnabled()) {
                logger.info("Uploaded refund trans matches existing trans: " + existingRefundPt.toString());
                logger.info("Saving new transaction to db: " + newApprovedPt.toString());
            }
            super.getEntityDao().save(newApprovedPt);
        } else {
            // Data does not match
            logger.warn("refunded transaction data mismatch found, from BOSS: " + newRefundPt + "; in database: " + existingRefundPt);
            
            super.getEntityDao().save(newApprovedPt);
                
            if (logger.isInfoEnabled()) {
                logger.info("approved transaction and import error are saved to the db.");
            }
            emailAlertNeeded = true;
        }
        
        return emailAlertNeeded;
    }
    
    private boolean handleRefundedTransWhenOnlyApprovedTransExist(final ProcessorTransaction newApprovedPt, final ProcessorTransaction existingApprovedPt
            , final ProcessorTransaction newRefundPt, final int customerId, final String line) {
        boolean emailAlertNeeded = false;
        
        // Compare fields of approved transaction
        final StringBuilder comments = new StringBuilder();
        final boolean transMatch = comparePropertiesForApprovals(newApprovedPt, existingApprovedPt, comments, "");
        
        if (transMatch) {
            // Data matches
            if (logger.isInfoEnabled()) {
                logger.info("Uploaded trans matches existing trans: " + existingApprovedPt.getId());
                logger.info("Saving new refund transaction to db: " + newRefundPt.getId());
            }
            super.getEntityDao().save(newRefundPt);
        } else {
            // Data does not match
            logger.warn("approved transaction data mismatch found, from BOSS: " + newApprovedPt + "; in database: " + existingApprovedPt);
            
            super.getEntityDao().save(newRefundPt);
                
            if (logger.isInfoEnabled()) {
                logger.info("refunded transaction and import error are saved to the db.");
            }
            emailAlertNeeded = true;
        }
        
        return emailAlertNeeded;
    }
    
    private boolean handleRefundedTransWhenApprovedAndRefundTransExist(final ProcessorTransaction newApprovedPt, final ProcessorTransaction existingApprovedPt
            , final ProcessorTransaction newRefundPt, final ProcessorTransaction existingRefundPt, final int customerId, final String line) {
        boolean emailAlertNeeded = false;
        
        final StringBuilder commentsApproval = new StringBuilder();
        final StringBuilder commentsRefund = new StringBuilder();
        
        final boolean approvalTransMatch = comparePropertiesForApprovals(newApprovedPt, existingApprovedPt, commentsApproval,
                                                                     existingRefundPt.getProcessorTransactionId());
        final boolean refundTransMatch = comparePropertiesForRefunds(newRefundPt, existingRefundPt, commentsRefund);
        
        logger.debug("approvedTransactionMatched: " + approvalTransMatch);
        logger.debug("refundedTransactionMatched: " + refundTransMatch);
        
        if (approvalTransMatch) {
            if (refundTransMatch) {
                if (logger.isInfoEnabled()) {
                    // both match
                    logger.info("both transactions match the ones in the database.");
                }
            } else {
                if (logger.isInfoEnabled()) {
                    logger.info("approved transaction matches the one in the database, but refunded transaction doesn't");
                }
                emailAlertNeeded = true;
            }
        } else {
            if (refundTransMatch) {
                if (logger.isInfoEnabled()) {
                    logger.info("refunded transaction matches the one in the database, but approved transaction doesn't");
                }
            } else {
                if (logger.isInfoEnabled()) {
                    logger.info("both transactions not match the ones in the database");
                }
            }
            emailAlertNeeded = true;
        }
        
        return emailAlertNeeded;
    }
    
    private boolean comparePropertiesForRefunds(final ProcessorTransaction newPt, final ProcessorTransaction emsPt, final StringBuilder sbComments) {
        if (emsPt.getProcessorTransactionType().getId() != transactionService.getMappingPropertiesValue("processorTransactionType.refund.id")) {
            throw new IllegalArgumentException("Refund(typeId=4)) expected but received typeId:" + emsPt.getProcessorTransactionType().getId());
        }
        
        boolean transMatch = true;
        
        // PaystationId is part of the key
        // MerchantAccountId is not compared as incoming MerchantAccountId is unknown
        // TicketNumber is part of the key
        
        if (emsPt.getAmount() != newPt.getAmount()) {
            sbComments.append("Amount ");
            transMatch = false;
        }
        
        if (!emsPt.getCardType().equals(newPt.getCardType())) {
            sbComments.append("Card Type ");
            transMatch = false;
        }
        
        if (emsPt.getLast4digitsOfCardNumber() != newPt.getLast4digitsOfCardNumber()) {
            sbComments.append("Last4DigitsOfCardNumber ");
            transMatch = false;
        }
        
        // CardChecksum is not used for new transactions
        // PurchasedDate is part of the key
        // ProcessingDate is not compared as BOSS processing date is different than EMS
        
        // ProcessorTransactionId - only compare if new transaction has one
        if (newPt.getProcessorTransactionId().length() > 0 && !emsPt.getProcessorTransactionId().equals(newPt.getProcessorTransactionId())) {
            sbComments.append("ProcessorTransactionId ");
            transMatch = false;
        }
        
        // AuthorizationNumber is not compared as BOSS doesn't store this for refunds
        
        // ReferenceNumber - only compare if new transaction has one
        if (newPt.getReferenceNumber().length() > 0 && !emsPt.getReferenceNumber().equals(newPt.getReferenceNumber())) {
            sbComments.append("ReferenceNumber ");
            transMatch = false;
        }
        
        // Approved = true
        
        // Card hash - only compare if existing transaction has one
        if (emsPt.getCardHash().length() > 0 && !emsPt.getCardHash().equals(newPt.getCardHash())) {
            sbComments.append("CardHash ");
            transMatch = false;
        }
        
        // IsUploadedFromBoss is not compared as it doesn't affect comparison
        
        if (!transMatch) {
            sbComments.insert(0, "Comparing " + emsPt.getProcessorTransactionType().getId() + ": ");
        }
        
        return transMatch;
    }
}
