package com.digitalpaytech.cardprocessing;

import java.io.IOException;
import java.util.UUID;

import com.digitalpaytech.client.dto.corecps.Authorization;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.ActiveBatchSummary;
import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.util.dto.Pair;

/**
 * CardProcessor abstract class combines abstract methods in EMS 6 com.digitalpioneer.cardprocessing.CardProcessor and
 * com.digitalpioneer.cardprocessin.CreditCardProcessor. For processors that don't have certain method implemented, a IllegalStateException will be thrown.
 * 
 * @author Allen Liang
 */

@SuppressWarnings("checkstyle:designforextension")
public abstract class CardProcessor {
    // will be defined in ems properties table
    protected static int EMS_MAX_REVERSAL_RETRIES;
    // in case of error retrieving from ems properties
    private static final int DEFAULT_MAX_REVERSAL_RETRIES = 120;
    
    private MerchantAccount merchantAccount;
    private CommonProcessingService commonProcessingService;
    private EmsPropertiesService emsPropertiesService;
    private CustomerCardTypeService customerCardTypeService;
    private CardProcessingManager cardProcessingManager;
    private MerchantAccountService merchantAccountService;
    
    private String timeZone;
    private boolean isManualSave;
    
    public CardProcessor() {
        
    }
    
    public CardProcessor(final MerchantAccount merchantAccount, final CommonProcessingService commonProcessingService,
            final EmsPropertiesService emsPropertiesService) {
        this.merchantAccount = merchantAccount;
        this.commonProcessingService = commonProcessingService;
        this.emsPropertiesService = emsPropertiesService;
        if (EMS_MAX_REVERSAL_RETRIES == 0) {
            EMS_MAX_REVERSAL_RETRIES = emsPropertiesService.getPropertyValueAsInt("DefaultMaxReversalRetries", DEFAULT_MAX_REVERSAL_RETRIES);
        }
    }
    
    public abstract boolean processPreAuth(PreAuth preAuth) throws CardTransactionException, CryptoException;
    
    public abstract Object[] processPostAuth(PreAuth preAuth, ProcessorTransaction processorTransaction);
    
    public abstract Object[] processRefund(boolean isNonEmsRequest, ProcessorTransaction origProcessorTransaction,
        ProcessorTransaction refundProcessorTransaction, String creditCardNumber, String expiryMMYY) throws CryptoException;
    
    public abstract Object[] processCharge(Track2Card track2Card, ProcessorTransaction chargeProcessorTransaction) throws CryptoException;
    
    public abstract void processReversal(Reversal reversal, Track2Card track2Card);
    
    public abstract boolean isReversalExpired(Reversal reversal);
    
    public String testCharge(final MerchantAccount ma) {
        throw new UnsupportedOperationException("The test charge method is not implemented for this processor");
    }
    
    public void processReversal(final PreAuth preAuth, final Track2Card track2Card) {
        throw new UnsupportedOperationException("The processReversal method is not implemented for this processor");
    }
    
    public void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public CommonProcessingService getCommonProcessingService() {
        return this.commonProcessingService;
    }
    
    public void setCommonProcessingService(final CommonProcessingService commonProcessingService) {
        this.commonProcessingService = commonProcessingService;
    }
    
    public EmsPropertiesService getEmsPropertiesService() {
        return this.emsPropertiesService;
    }
    
    public MerchantAccount getMerchantAccount() {
        return this.merchantAccount;
    }
    
    public void setMerchantAccount(final MerchantAccount merchantAccount) {
        this.merchantAccount = merchantAccount;
    }
    
    public CustomerCardTypeService getCustomerCardTypeService() {
        return this.customerCardTypeService;
    }
    
    public void setCustomerCardTypeService(final CustomerCardTypeService customerCardTypeService) {
        this.customerCardTypeService = customerCardTypeService;
    }
    
    public CardProcessingManager getCardProcessingManager() {
        return this.cardProcessingManager;
    }
    
    public void setCardProcessingManager(final CardProcessingManager cardProcessingManager) {
        this.cardProcessingManager = cardProcessingManager;
    }
    
    public MerchantAccountService getMerchantAccountService() {
        return this.merchantAccountService;
    }
    
    public void setMerchantAccountService(final MerchantAccountService merchantAccountService) {
        this.merchantAccountService = merchantAccountService;
    }
    
    public String getTimeZone() {
        return this.timeZone;
    }
    
    public void setTimeZone(final String timeZone) {
        this.timeZone = timeZone;
    }
    
    public boolean getIsManualSave() {
        return this.isManualSave;
    }
    
    public void setIsManualSave(final boolean isManualSave) {
        this.isManualSave = isManualSave;
    }
    
    public void savePreAuth(final PreAuth preAuth) {
    }
    
    public void updatePostAuthDetails(final PreAuth preAuth, final ProcessorTransaction processorTransaction) {
    }
    
    public void updateProcessorSpecificDetails(final ProcessorTransaction processorTransaction) {
    }
    
    public String processBatchClose(final MerchantAccount newMerchantAccount, final ActiveBatchSummary summary, final int batchCloseType)
        throws IOException {
        // TODO Auto-generated method stub
        return null;
    }
    
    // ----------- The following methods are overriding by Link CPS classes. -----------
    
    public int processLinkStoreAndForward(final ProcessorTransaction processorTransaction, final CardRetryTransaction cardRetryTransaction)
        throws CryptoException {
        throw new UnsupportedOperationException("The processStoreAndForward is implemented for Core CPS processor");
    }
    
    public Pair<String, Authorization> processLinkStoreAndForwardOrFailTransaction(final ProcessorTransaction processorTransaction,
        final CardRetryTransaction cardRetryTransaction, final String cardTypeName) throws CryptoException {
        throw new UnsupportedOperationException("The processLinkStoreAndForwardOrFailTransaction is implemented for Core CPS processor");
    }
    
    public void updateObjectsAfterSuccessfulCoreCPSStoreAndForward(final ProcessorTransaction processorTransaction, final Authorization authorization,
        final CardRetryTransaction cardRetryTransaction, final String chargeToken) {
        throw new UnsupportedOperationException("The updateObjectsAfterSuccessfulCoreCPSStoreAndForward is implemented for Core CPS processor");
    }
    
    public void updateRefundDetails(final ProcessorTransaction processorTransaction) {
        throw new UnsupportedOperationException("The updateRefundDetails is implemented for Core CPS processor");
    }
    
    public int processLinkCPSTransactionAuthorization(final MerchantAccount merchantAccount, final PointOfSale pointOfSale, final PreAuth preAuth,
        final String cardTypeName) throws CryptoException {
        throw new UnsupportedOperationException("The processLinkCPSTransactionAuthorization is implemented for Core CPS processor");
    }
    
    public ProcessorTransaction processLinkCPSTransactionRefund(final ProcessorTransaction origPtd, final MerchantAccount merchantAccount)
        throws CryptoException {
        throw new UnsupportedOperationException("The processLinkCPSTransactionRefund is implemented for Core CPS processor");
    }
    
    public Object[] processLinkCPSTransactionCharge(final String cardToken, final long origProcTransId,
        final ProcessorTransaction chargeProcessorTransaction, final MerchantAccount merchantAccount, final PointOfSale pointOfSale,
        final Permit permit, final PaymentCard paymentCard) throws Exception {
        throw new UnsupportedOperationException("The processLinkCPSTransactionCharge is implemented for Core CPS processor");
    }
    
    public Object[] processCharge(final ProcessorTransaction chargeProcessorTransaction, final UUID cardToken) throws CryptoException {
        throw new UnsupportedOperationException("The processCharge is implemented for Core CPS processor");
    }
}
