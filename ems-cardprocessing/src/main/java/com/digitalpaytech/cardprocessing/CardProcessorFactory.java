package com.digitalpaytech.cardprocessing;

import java.util.Map;
import java.util.Set;

import com.digitalpaytech.cardprocessing.impl.LinkCPSProcessor;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.dto.ProcessorInfo;
import com.digitalpaytech.exception.CardProcessorPausedException;

public interface CardProcessorFactory {
    CardProcessor getCardProcessor(MerchantAccount merchantAccount, PointOfSale pointOfSale) 
            throws CardProcessorPausedException;
    
    CardProcessor getCardProcessor(MerchantAccount merchantAccount, PointOfSale pointOfSale, boolean isForTestCharge)
            throws CardProcessorPausedException;
    
    boolean resumeProcessor(int processorId);
    
    boolean pauseProcessor(int processorId);
    
    Set<ProcessorInfo> getCreditCardProcessorInfo();

    Map<Integer, ProcessorInfo> getEMVProcessorInfo();
    
    ProcessorInfo getProcessorInfo(Integer processorId);
    
    Set<Integer> getProcessorIds();
    
    boolean isProcessorPaused(Integer processorId);

    LinkCPSProcessor getLinkCPSProcessor(MerchantAccount merchantAccount, PointOfSale pointOfSale);
}
