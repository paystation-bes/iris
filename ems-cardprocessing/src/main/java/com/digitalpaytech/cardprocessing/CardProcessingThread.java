package com.digitalpaytech.cardprocessing;

import java.util.Date;

import org.apache.log4j.Logger;

import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.util.EMSThread;

public class CardProcessingThread extends EMSThread {
    private final static Logger logger = Logger.getLogger(CardProcessingThread.class);
    
    protected final int threadId;
    private final int processingType;
    private String processingTypeName;
    private boolean processing = false;
    
    private CardProcessingManager cardProcessingManager;
    protected CardProcessingMaster cardProcessingMaster;
    
    protected static final String PROCESSING_TYPE_NAME_STORE_FORWARD = "Card Store and Forward";
    protected static final String PROCESSING_TYPE_NAME_CHARGE_RETRY = "Card Charge Retry";
    protected static final String PROCESSING_TYPE_NAME_SETTLEMENT = "Card Settlement";
    protected static final String PROCESSING_TYPE_NAME_REVERSAL = "Reversal";
    
    protected final static String STARTING = "Starting ";
    protected final static String STOPPING = "Stopping ";
    protected final static String THREAD = " Thread: ";
    
    private static final long THREE_SECONDS_MS = 1000 * 3;
    
    public CardProcessingThread(int threadId, int usage, CardProcessingManager cardProcessingManager, CardProcessingMaster master) {
        super("CardProcessingThread_" + usage + "_" + threadId);
        
        // Originally priority is Thread.NORM_PRIORITY - 2.
        setPriority(Thread.NORM_PRIORITY);
        
        this.cardProcessingManager = cardProcessingManager;
        cardProcessingMaster = master;
        
        this.threadId = threadId;
        this.processingType = usage;
        setProcessingTypeName();
    }
    
    public void run() {
        logStart();
        
        while (keepRunning()) {
            try {
                Object obj = getNextTransactionFromQueue();
                
                if (obj == null) {
                    processing = false;
                    waitForMoreData();
                    continue;
                } else {
                    
                    // TODO Customer Migration Check might not be necessary
//                    int pointOfSaleId = 0;
//                    switch (processingType) {
//                        case CardProcessingMaster.TYPE_STORE_FORWARD:
//                        case CardProcessingMaster.TYPE_CHARGE_RETRY:
//                            pointOfSaleId = ((CardRetryTransaction) obj).getPointOfSale().getId();
//                            break;
//                        
//                        case CardProcessingMaster.TYPE_SETTLEMENT:
//                            pointOfSaleId = ((ProcessorTransaction) obj).getPointOfSale().getId();
//                            break;
//                        
//                        case CardProcessingMaster.TYPE_SMART_CARD_SETTLEMENT:
//                            pointOfSaleId = ((ScRetry) obj).getPointOfSale().getId();
//                            break;
//                        
//                        case CardProcessingMaster.TYPE_REVERSAL:
//                            pointOfSaleId = ((Reversal) obj).getPointOfSaleId();
//                            break;
//                        default:
//                            break;
//                    }
//                    if (pointOfSaleId == 0 || !cardProcessingManager.checkMigrationStatusOfCustomer(pointOfSaleId)) {
//                        if ((processingType == CardProcessingMaster.TYPE_STORE_FORWARD) || (processingType == CardProcessingMaster.TYPE_CHARGE_RETRY)) {
//                            cardProcessingMaster.removeCardRetryTransFromSet((CardRetryTransaction) obj);
//                        } else if (processingType == CardProcessingMaster.TYPE_SMART_CARD_SETTLEMENT) {
//                            cardProcessingMaster.removeSmartCardRetryFromSet((ScRetry) obj);
//                        } else if (processingType == CardProcessingMaster.TYPE_REVERSAL) {
//                            cardProcessingMaster.removeReversalFromSet((Reversal) obj);
//                        }
//                        continue;
//                    }
                    processing = true;
                }
                
                // Obj is not null
                processNextObject(obj);
            } catch (RuntimeException e) {
                logger.error("Unable to processing transaction from " + processingTypeName + " queue.", e);
            }
        }
        
        logStop();
    }
    
    protected void logStart() {
        logger.info(STARTING + getProcessingTypeName() + THREAD + threadId);
    }
    
    protected void logStop() {
        logger.info(STOPPING + getProcessingTypeName() + THREAD + threadId);
    }
    
    public synchronized void wakeup() {
        notify();
    }
    
    protected Object getNextTransactionFromQueue() {
        return (cardProcessingMaster.getNextTransactionInQueue(processingType));
    }
    
    protected boolean keepRunning() {
        if (processingType == CardProcessingMaster.TYPE_CHARGE_RETRY) {
            return (cardProcessingMaster.isProcessingRunning(CardProcessingMaster.TYPE_CHARGE_RETRY));
        }
        return (!isShutdown());
    }
    
    private synchronized void waitForMoreData() {
        try {
            if (processingType == CardProcessingMaster.TYPE_CHARGE_RETRY) {
                wait(THREE_SECONDS_MS);
            } else {
                wait();
            }
        } catch (InterruptedException e) {
            // Empty block
        }
    }
    
    private void processNextObject(Object obj) {
        if (obj == null) {
            return;
        }
        
        logger.debug(getProcessingTypeName() + THREAD + threadId + " processing " + obj);
        
        try {
            switch (processingType) {
                case CardProcessingMaster.TYPE_STORE_FORWARD:
                    cardProcessingManager.routeTransactionChargeProcessing((CardRetryTransaction) obj, null);
                    break;
                
                case CardProcessingMaster.TYPE_CHARGE_RETRY:
                    Date charge_retry_start_time = cardProcessingMaster.getNightlyTransactionRetryStartTime();
                    //cardProcessingManager.processTransactionCharge((CardRetryTransaction) obj, charge_retry_start_time);
                    cardProcessingManager.routeTransactionChargeProcessing((CardRetryTransaction) obj, charge_retry_start_time);
                    break;
                
                case CardProcessingMaster.TYPE_SETTLEMENT:
                    cardProcessingManager.processTransactionSettlement((ProcessorTransaction) obj);
                    break;
                
                case CardProcessingMaster.TYPE_SMART_CARD_SETTLEMENT:
                    logger.warn("Unable to process smart card: the Parcxmart processor is no longer supported");
                    
                    break;
                
                case CardProcessingMaster.TYPE_REVERSAL:
                    cardProcessingManager.processReversal((Reversal) obj);
                    break;
                
                default:
                    break;
            }
        } catch (Exception e) {
            String msg = "Unable to process transaction: " + obj + " as there was a problem performing a " + getProcessingTypeName();
            logger.error(msg, e);
            cardProcessingMaster.sendAdminErrorAlert(MailerService.EXCEPTION, msg, null);
        }
        
        if ((processingType == CardProcessingMaster.TYPE_STORE_FORWARD) || (processingType == CardProcessingMaster.TYPE_CHARGE_RETRY)) {
            cardProcessingMaster.removeCardRetryTransFromSet((CardRetryTransaction) obj);
        } else if (processingType == CardProcessingMaster.TYPE_SMART_CARD_SETTLEMENT) {
            logger.warn("Unable to process smart card: the Parcxmart processor is no longer supported");
        } else if (processingType == CardProcessingMaster.TYPE_REVERSAL) {
            cardProcessingMaster.removeReversalFromSet((Reversal) obj);
        }
    }
    
    private void setProcessingTypeName() {
        if (processingType < 0 || processingType >= CardProcessingMaster.NUMBER_TRANSACTION_QUEUES) {
            throw new IllegalArgumentException("Unknown Card Processing Type: " + processingType);
        }
        
        switch (processingType) {
            case CardProcessingMaster.TYPE_STORE_FORWARD:
                processingTypeName = PROCESSING_TYPE_NAME_STORE_FORWARD;
                break;
            case CardProcessingMaster.TYPE_CHARGE_RETRY:
                processingTypeName = PROCESSING_TYPE_NAME_CHARGE_RETRY;
                break;
            case CardProcessingMaster.TYPE_SETTLEMENT:
                processingTypeName = PROCESSING_TYPE_NAME_SETTLEMENT;
                break;
            case CardProcessingMaster.TYPE_REVERSAL:
                processingTypeName = PROCESSING_TYPE_NAME_REVERSAL;
                break;
        }
    }
    
    protected String getProcessingTypeName() {
        return (processingTypeName);
    }
    
    public boolean isProcessing() {
        return processing;
    }
}
