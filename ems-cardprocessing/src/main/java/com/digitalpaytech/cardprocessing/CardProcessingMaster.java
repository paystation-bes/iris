package com.digitalpaytech.cardprocessing;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.service.ClusterMemberService;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.kpi.KPIService;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;

/**
 * This class manages card transaction background process.
 * (charge retry, S&F, settlement, reversal, and clean-up process etc.)
 * 
 * @author Brian Kim
 * 
 */
/* This class has to be extensible because it doesn't have interface. Otherwise, Spring will not override this properly on some cases. */
/*
 * Ignore illegal exception catching because it will change behavior too much for this patch.
 * Should be fix properly when this class get modified on the release sprint to make sure it will be tested properly during regression.
 */
@SuppressWarnings({ "checkstyle:designforextension", "checkstyle:illegalcatch" })
@Component("cardProcessingMaster")
public class CardProcessingMaster {
    /* Processing types */
    public static final int TYPE_STORE_FORWARD = 0;
    public static final int TYPE_CHARGE_RETRY = 1;
    public static final int TYPE_SETTLEMENT = 2;
    public static final int TYPE_SMART_CARD_SETTLEMENT = 3;
    public static final int TYPE_REVERSAL = 4;
    
    public static final int NUMBER_TRANSACTION_QUEUES = 5;
    public static final int DEFAULT_STORE_FORWARD_THREAD_COUNT = 10;
    public static final int DEFAULT_SETTLEMENT_THREAD_COUNT = 10;
    public static final int DEFAULT_REVERSAL_THREAD_COUNT = 1;
    
    public static final int INTERVAL_EMAIL_MILLIS = StandardConstants.MINUTES_IN_MILLIS_1 * 30;
    public static final int INTERVAL_BEFORE_SHUTDOWN_MILLIS = StandardConstants.SECONDS_IN_MILLIS_1 * 10;
    
    private static final Logger LOG = Logger.getLogger(CardProcessingMaster.class);
    
    private static final String MSG_PROCESSOR_COUNTER = "Current Processor Counter = ";
    private static final String MSG_FAILED_INIT_SETTLEMENT_THREADS = "Unable to initialize Card Settlement threads";
    private static final String MSG_INTERRUPTED_CHECKING_PROCESSOR_CNTR = "Interrupted while sleeping before checking processor counter size";
    private static final String MSG_SERVER = "Server ";
    
    /* default background transaction processing server */
    private static final String DEFAULT_PROCESS_SERVER = "EmsMain";
    
    private static final String CARD_PROCESSOR_ERROR = "Card Processing";
    
    /* last cluster error email alert time */
    private static Date lastClusterAlert;
    
    // Settlement
    private static int settlementThreadCount = DEFAULT_SETTLEMENT_THREAD_COUNT;
    private static CardProcessingThread[] settlementThread;
    private static int currSettlementThreadId;
    
    // Reversal
    private static int reversalThreadCount = DEFAULT_REVERSAL_THREAD_COUNT;
    private static CardProcessingThread[] reversalThreads;
    private static int currReversalThreadId;
    
    @Autowired
    private ClusterMemberService clusterMemberService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private CardProcessingManager cardProcessingManager;
    
    @Autowired
    private MailerService mailerService;
    
    @Autowired
    private CryptoService cryptoService;
    
    @Autowired
    private CustomerCardTypeService customerCardTypeService;
    
    @Autowired
    private EmsPropertiesService emsProperties;
    
    @Autowired
    private KPIService kpiService;
    
    /* Charge retry */
    private boolean isChargeRetryRunning;
    
    /* Stop Processing Flag */
    private boolean needStopProcessing;
    
    private Date chargeRetryStartTime;
    
    /* CardRetrytransaction Set */
    private Set<CardRetryTransaction> currentlyProcessingTrans = new HashSet<CardRetryTransaction>();
    
    /* Reversal Set */
    private Set<Reversal> currentlyProcessingReversals = new HashSet<Reversal>();
    
    /* Queues */
    @SuppressWarnings("rawtypes")
    private ConcurrentLinkedQueue[] transQueue = new ConcurrentLinkedQueue[NUMBER_TRANSACTION_QUEUES];
    private int[] numTransInQueue = new int[NUMBER_TRANSACTION_QUEUES];
    
    // Store and Forward
    private int storeForwardThreadCount = DEFAULT_STORE_FORWARD_THREAD_COUNT;
    private CardProcessingThread[] storeForwardThread;
    private int currStoreForwardThreadId;
    private int trackDataProcessingThreadCount = 1;
    private TrackDataProcessingThread[] trackDataProcessingThread;
    
    // Track data list
    private List<CardRetryTransaction> trackDataList = new CopyOnWriteArrayList<CardRetryTransaction>();
    
    @PostConstruct
    @SuppressWarnings("rawtypes")
    private void createTransactionQueue() {
        // Create transaction queues
        for (int i = 0; i < NUMBER_TRANSACTION_QUEUES; i++) {
            this.transQueue[i] = new ConcurrentLinkedQueue();
            // init count to 0
            this.numTransInQueue[i] = 0;
        }
    }
    
    private void validateProcessingType(final int processingType) {
        if (processingType < 0 || processingType >= NUMBER_TRANSACTION_QUEUES) {
            throw new IllegalArgumentException("Unknown Card Processing Type: " + processingType);
        }
    }
    
    public boolean isProcessingRunning(final int processingType) {
        validateProcessingType(processingType);
        
        switch (processingType) {
            case CardProcessingMaster.TYPE_STORE_FORWARD:
                return true;
                
            case CardProcessingMaster.TYPE_CHARGE_RETRY:
                return this.isChargeRetryRunning || !this.transQueue[TYPE_CHARGE_RETRY].isEmpty();
                
            case CardProcessingMaster.TYPE_SETTLEMENT:
                return true;
                
            case CardProcessingMaster.TYPE_SMART_CARD_SETTLEMENT:
                return true;
                
            case CardProcessingMaster.TYPE_REVERSAL:
                return true;
                
            default:
                return false;
        }
    }
    
    public boolean isScheduledCardProcessingServer() {
        try {
            // get the cluster member name
            final String clusterName = this.clusterMemberService.getClusterName();
            if (clusterName == null || clusterName.isEmpty()) {
                throw new ApplicationException("Cluster member name is empty");
            }
            
            // get the configured card processing server from database
            final String processServer = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.SCHEDULED_CARD_PROCESS_SERVER,
                                                                                    DEFAULT_PROCESS_SERVER, true);
            
            if (clusterName.equals(processServer)) {
                LOG.info(MSG_SERVER + clusterName + " configured for scheduled card processing tasks.");
                return true;
            } else {
                LOG.info(MSG_SERVER + clusterName + " not configured for scheduled card processing tasks.");
                return false;
            }
        } catch (Exception e) {
            String addr = null;
            try {
                addr = InetAddress.getLocalHost().getHostAddress();
            } catch (Exception e1) {
                LOG.error("Shouldn't happened", e1);
            }
            final String err = "Unable to read memeber name from cluster.properties for cluster member " + addr;
            LOG.error(err);
            
            // only send email alerts every 30 minutes
            if (lastClusterAlert == null || new Date().getTime() - lastClusterAlert.getTime() > INTERVAL_EMAIL_MILLIS) {
                lastClusterAlert = new Date();
                sendAdminErrorAlert(err, e);
            }
            return false;
        }
    }
    
    protected Object getTransactionFromNextStoreFowardQueue() {
        LOG.info("StoreFowardQueue size: " + this.numTransInQueue[TYPE_STORE_FORWARD]);
        Object obj = null;
        synchronized (this.transQueue[TYPE_STORE_FORWARD]) {
            obj = this.transQueue[TYPE_STORE_FORWARD].remove();
            adjustNumTransInQueue(TYPE_STORE_FORWARD, -1);
        }
        
        return obj;
    }
    
    /**
     * This method saves the track data into a file.
     */
    protected void processSaveTrackDataToList(final Object obj) {
        final CardRetryTransaction crt = (CardRetryTransaction) obj;
        LOG.info("Started saving card track data into file: " + crt);
        try {
            if (this.customerCardTypeService.isCreditCardTrack2(crt.getCardData())) {
                /* encrypt track data. */
                final String newCardData = this.cryptoService.encryptData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL, crt.getCardData());
                final String newDatabaseCardData = this.cryptoService.encryptData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL, crt.getCardData());
                
                crt.setCardData(newCardData);
                crt.setCardData(newDatabaseCardData);
                
                this.trackDataList.add(crt);
            }
        } catch (CryptoException e) {
            LOG.error("Unable to save card data into file because of encryption failure: " + crt, e);
        }
    }
    
    public synchronized void checkProcessingQueueAndStopServer() {
        this.needStopProcessing = true;
        this.cardProcessingManager.setNeedStopProcessing(true);
        
        int procCounter = this.cardProcessingManager.getProcessorCounter();
        int sleepTime = 0;
        LOG.info(MSG_PROCESSOR_COUNTER + procCounter);
        while (procCounter > 0 || isSaveTrackDataToFile()) {
            try {
                LOG.info(" Sleeping for one second before checking again");
                Thread.sleep(StandardConstants.SECONDS_IN_MILLIS_1);
                sleepTime++;
            } catch (InterruptedException e) {
                // Should never happen
                final String msg = MSG_INTERRUPTED_CHECKING_PROCESSOR_CNTR;
                LOG.warn(msg, e);
                sendAdminErrorAlert(msg, e);
                return;
            }
            procCounter = this.cardProcessingManager.getProcessorCounter();
            LOG.info(MSG_PROCESSOR_COUNTER + procCounter);
        }
        // Waiting 10s before shut down server
        try {
            Thread.sleep(INTERVAL_BEFORE_SHUTDOWN_MILLIS);
        } catch (InterruptedException e) {
            // Should never happen
            final String msg = MSG_INTERRUPTED_CHECKING_PROCESSOR_CNTR;
            LOG.warn(msg, e);
            sendAdminErrorAlert(msg, e);
            return;
        }
        // Stop Tomcat Server
        LOG.info("Tomcat server has shut down successfully.");
        System.exit(0);
    }
    
    public final CardProcessingManager getCardProcessingManager() {
        return this.cardProcessingManager;
    }
    
    /**
     * This function is for settlement, smart card settlement and store-foward.<br>
     * Charge retry will use
     * getNextCardRetryTransactionForCardProcessor(cardProcessorType).
     * 
     * @param processingType
     * @return
     */
    protected Object getNextTransactionInQueue(final int processingType) {
        validateProcessingType(processingType);
        
        // Stop get transaction from queue
        if (this.needStopProcessing) {
            return null;
        }
        
        synchronized (this.transQueue[processingType]) {
            Object obj = null;
            boolean alreadyProcessing = false;
            
            // if StoreForward or ChargeRetry
            if (processingType == TYPE_STORE_FORWARD) {
                alreadyProcessing = true;
                
                synchronized (this.currentlyProcessingTrans) {
                    while ((this.numTransInQueue[processingType] > 0) && alreadyProcessing) {
                        obj = this.transQueue[processingType].remove();
                        adjustNumTransInQueue(processingType, -1);
                        
                        final CardRetryTransaction cardRetryTrans = (CardRetryTransaction) obj;
                        alreadyProcessing = !this.currentlyProcessingTrans.add(cardRetryTrans);
                    }
                }
            } else if (processingType == TYPE_REVERSAL) {
                synchronized (this.currentlyProcessingReversals) {
                    if (this.numTransInQueue[processingType] > 0) {
                        obj = this.transQueue[processingType].remove();
                        adjustNumTransInQueue(processingType, -1);
                        
                        final Reversal reversalTran = (Reversal) obj;
                        this.currentlyProcessingReversals.add(reversalTran);
                    }
                }
            } else if (processingType == TYPE_SMART_CARD_SETTLEMENT) {
                LOG.warn("Unable to process smart card: the Parcxmart processor is no longer supported");
            } else if (this.numTransInQueue[processingType] > 0) {
                // else other queue, but only if there are records in that queue
                obj = this.transQueue[processingType].remove();
                adjustNumTransInQueue(processingType, -1);
            }
            
            if (alreadyProcessing) {
                // EMS-1693
                // that implies the last entry from the S&F queue is retrieved
                // but the transaction is being processed
                return null;
            } else {
                if (obj instanceof CardRetryTransaction) {
                    final CardRetryTransaction cardRetryTran = (CardRetryTransaction) obj;
                    LOG.debug("Retrieved Store and Forward: " + cardRetryTran);
                }
                return obj;
            }
        }
    }
    
    public void removeCardRetryTransFromSet(final CardRetryTransaction cardRetryTrans) {
        synchronized (this.currentlyProcessingTrans) {
            this.currentlyProcessingTrans.remove(cardRetryTrans);
        }
    }
    
    public void removeReversalFromSet(final Reversal reversal) {
        synchronized (this.currentlyProcessingReversals) {
            this.currentlyProcessingReversals.remove(reversal);
        }
    }
    
    protected Date getNightlyTransactionRetryStartTime() {
        return this.chargeRetryStartTime;
    }
    
    public void sendAdminErrorAlert(final String message, final Exception e) {
        this.mailerService.sendAdminErrorAlert(CARD_PROCESSOR_ERROR, message, e);
    }
    
    protected void sendAdminErrorAlert(final String subject, final String message, final Exception e) {
        this.mailerService.sendAdminErrorAlert(CARD_PROCESSOR_ERROR + subject, message, e);
    }
    
    public void adjustNumTransInQueue(final int type, final int numAdd) {
        synchronized (this) {
            this.numTransInQueue[type] += numAdd;
            if (this.numTransInQueue[type] < 0) {
                this.numTransInQueue[type] = 0;
            }
        }
    }
    
    public boolean getIsChargeRetryRunning() {
        return this.isChargeRetryRunning;
    }
    
    public void setChargeRetryRunning(final boolean chargeRetryRunning) {
        this.isChargeRetryRunning = chargeRetryRunning;
    }
    
    public Date getChargeRetryStartTime() {
        return this.chargeRetryStartTime;
    }
    
    public void setChargeRetryStartTime(final Date chargeRetryStartTime) {
        this.chargeRetryStartTime = chargeRetryStartTime;
    }
    
    public boolean getNeedStopProcessing() {
        return this.needStopProcessing;
    }
    
    public void setNeedStopProcessing(final boolean needStopProcessing) {
        this.needStopProcessing = needStopProcessing;
    }
    
    public Set<CardRetryTransaction> getCurrentlyProcessingTrans() {
        return this.currentlyProcessingTrans;
    }
    
    public void setCurrentlyProcessingTrans(final Set<CardRetryTransaction> currentlyProcessingTrans) {
        this.currentlyProcessingTrans = currentlyProcessingTrans;
    }
    
    public int getSettlementQueueSize() {
        if (this.transQueue[TYPE_SETTLEMENT] != null) {
            return this.numTransInQueue[TYPE_SETTLEMENT];
        } else {
            return 0;
        }
    }
    
    public int getStoreForwardQueueSize() {
        if (this.transQueue[TYPE_STORE_FORWARD] != null) {
            return this.numTransInQueue[TYPE_STORE_FORWARD];
        } else {
            return 0;
        }
    }
    
    public int getReversalQueueSize() {
        if (this.transQueue[TYPE_REVERSAL] != null) {
            return this.numTransInQueue[TYPE_REVERSAL];
        } else {
            return 0;
        }
    }
    
    /**
     * Transaction Store And Forward.
     * In EMS 6.3.11 Transaction pojo class has CardRetryTransaction property and in TransactionHandler.java it calls RpcPaystation2AppServiceImpl,
     * processTransactionStoreAndForward
     * method, which calls cardProcessingMaster.addTransactionToStoreForwardQueue.
     */
    @SuppressWarnings("unchecked")
    public void addTransactionToStoreForwardQueue(final CardRetryTransaction crt) {
        try {
            if (this.storeForwardThread == null) {
                initializeStoreForwardThreads();
            }
        } catch (Exception e) {
            final String msg = "Unable to initialize Card Store And Forward threads";
            LOG.error(msg, e);
            sendAdminErrorAlert(msg, e);
            return;
        }
        
        synchronized (this.transQueue[TYPE_STORE_FORWARD]) {
            this.transQueue[TYPE_STORE_FORWARD].offer(crt);
            adjustNumTransInQueue(TYPE_STORE_FORWARD, 1);
            
            this.storeForwardThread[this.currStoreForwardThreadId].wakeup();
            
            // Rotate to next thread in round-robin format
            this.currStoreForwardThreadId++;
            if (this.currStoreForwardThreadId == this.storeForwardThreadCount) {
                this.currStoreForwardThreadId = 0;
            }
        }
        
    }
    
    public void addTransactionToStoreForwardQueueIfNotExhausted(final CardRetryTransaction crt) {
        if (this.getStoreForwardQueueSize() > this.emsProperties.getPropertyValueAsInt(EmsPropertiesService.CARD_STORE_FORWARD_QUEUE_LIMIT,
                                                                                       EmsPropertiesService.DEFAULT_CARD_STORE_FORWARD_QUEUE_LIMIT)) {
            this.kpiService.incrementQueueExhaustingStoreAndForward();
            LOG.error("Store & Forward Queue has been exhausted: CardRetryTransaction: " + crt.getId()
                      + " should have been inserted to database but not the S&F Queue.");
        } else {
            addTransactionToStoreForwardQueue(crt);
        }
    }
    
    private synchronized void initializeStoreForwardThreads() {
        if (this.storeForwardThread != null) {
            return;
        }
        
        try {
            this.storeForwardThreadCount = this.emsPropertiesService
                    .getPropertyValueAsInt(EmsPropertiesService.CARD_STORE_FORWARD_THREAD_COUNT,
                                           EmsPropertiesService.DEFAULT_CARD_STORE_FORWARD_THREAD_COUNT, true);
        } catch (Exception e) {
            // Log and leave value as default
            LOG.warn(e.getMessage(), e);
        }
        
        // Initialize array and create threads
        this.storeForwardThread = new CardProcessingThread[this.storeForwardThreadCount];
        for (int i = 0; i < this.storeForwardThreadCount; i++) {
            this.storeForwardThread[i] = new CardProcessingThread(i, TYPE_STORE_FORWARD, this.cardProcessingManager, this);
            this.storeForwardThread[i].start();
        }
    }
    
    @SuppressWarnings("unchecked")
    public void addTransactionToSettlementQueue(final PointOfSale pointOfSale, final ProcessorTransaction ptd) {
        try {
            if (settlementThread == null) {
                initializeSettlementThreads();
            }
        } catch (Exception e) {
            final String msg = MSG_FAILED_INIT_SETTLEMENT_THREADS;
            LOG.error(msg, e);
            sendAdminErrorAlert(msg, e);
            return;
        }
        
        synchronized (this.transQueue[TYPE_SETTLEMENT]) {
            this.transQueue[TYPE_SETTLEMENT].offer(ptd);
            adjustNumTransInQueue(TYPE_SETTLEMENT, 1);
            
            settlementThread[currSettlementThreadId].wakeup();
            
            // Rotate to next settlement thread in round-robin format
            currSettlementThreadId++;
            if (currSettlementThreadId == settlementThreadCount) {
                currSettlementThreadId = 0;
            }
        }
        
    }
    
    private synchronized void initializeSettlementThreads() {
        if (settlementThread != null) {
            return;
        }
        
        try {
            settlementThreadCount = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.CARD_SETTLEMENT_THREAD_COUNT,
                                                                                    EmsPropertiesService.DEFAULT_CARD_SETTLEMENT_THREAD_COUNT, true);
        } catch (Exception e) {
            // Log and leave value as default
            LOG.warn(e.getMessage(), e);
        }
        
        // Initialize array and create settlement threads
        settlementThread = new CardProcessingThread[settlementThreadCount];
        for (int i = 0; i < settlementThreadCount; i++) {
            settlementThread[i] = new CardProcessingThread(i, TYPE_SETTLEMENT, this.cardProcessingManager, this);
            settlementThread[i].start();
        }
    }
    
    private synchronized void initializeTrackDataProcessingThreads() {
        if (this.trackDataProcessingThread != null) {
            return;
        }
        
        // Initialize array and create threads
        this.trackDataProcessingThread = new TrackDataProcessingThread[this.trackDataProcessingThreadCount];
        for (int i = 0; i < this.trackDataProcessingThreadCount; i++) {
            this.trackDataProcessingThread[i] = new TrackDataProcessingThread(this);
            this.trackDataProcessingThread[i].start();
        }
        LOG.info("+++ TrackDataProcessingThread started +++");
    }
    
    /**
     * This method initialize the track data processing threads and start them.
     */
    private boolean isSaveTrackDataToFile() {
        try {
            if (this.trackDataProcessingThread == null) {
                this.initializeTrackDataProcessingThreads();
            }
        } catch (Exception e) {
            final String msg = "Unable to initialize Track Data processing threads";
            LOG.error(msg, e);
            sendAdminErrorAlert(msg, e);
        }
        
        /* write track data into file. */
        return writeTrackDataIntoFile();
    }
    
    /**
     * write the track data to actual file.
     */
    private boolean writeTrackDataIntoFile() {
        LOG.info("++++ STARTING writeTrackDataIntoFile()");
        StringBuffer fileName = null;
        try {
            if (!this.transQueue[TYPE_STORE_FORWARD].isEmpty()) {
                return true;
            } else {
                if (this.trackDataList.size() > 0) {
                    /* Converts object to byte array. */
                    final byte[] bytes = WebCoreUtil.convertObjToBinary(this.trackDataList);
                    
                    /* File directory name. */
                    final String dirPath = WebCoreConstants.DEFAULT_TRACKDATAFILE_LOCATION;
                    
                    /* File name */
                    fileName = new StringBuffer(WebCoreConstants.SAVE_TRACKDATA_SUFFIX);
                    fileName.append(System.currentTimeMillis());
                    
                    /* Create a physical file in the specified directory. */
                    WebCoreUtil.createFile(dirPath, fileName.toString(), bytes);
                }
            }
        } catch (IOException e) {
            LOG.error("Failed to create a track data file: " + fileName.toString(), e);
        }
        LOG.info("++++ FINISHED writeTrackDataIntoFile()");
        return false;
    }
    
    @SuppressWarnings("unchecked")
    public void addTransactionToSettlementQueue(final ProcessorTransaction ptd) {
        try {
            if (settlementThread == null) {
                initializeSettlementThreads();
            }
        } catch (Exception e) {
            final String msg = MSG_FAILED_INIT_SETTLEMENT_THREADS;
            LOG.error(msg, e);
            sendAdminErrorAlert(msg, e);
            return;
        }
        
        synchronized (this.transQueue[TYPE_SETTLEMENT]) {
            this.transQueue[TYPE_SETTLEMENT].offer(ptd);
            adjustNumTransInQueue(TYPE_SETTLEMENT, 1);
            
            settlementThread[currSettlementThreadId].wakeup();
            
            // Rotate to next settlement thread in round-robin format
            currSettlementThreadId++;
            if (currSettlementThreadId == settlementThreadCount) {
                currSettlementThreadId = 0;
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    public void addReversalToReversalQueue(final Reversal reversal) {
        try {
            if (reversalThreads == null) {
                initializeReversalThreads();
            }
        } catch (Exception e) {
            final String msg = "Unable to initialize Card Reversal threads";
            LOG.error(msg, e);
            this.sendAdminErrorAlert(msg, e);
            return;
        }
        
        synchronized (this.transQueue[TYPE_REVERSAL]) {
            if (!this.transQueue[TYPE_REVERSAL].contains(reversal) && !this.currentlyProcessingReversals.contains(reversal)) {
                this.transQueue[TYPE_REVERSAL].offer(reversal);
                this.adjustNumTransInQueue(TYPE_REVERSAL, 1);
                
                reversalThreads[currReversalThreadId].wakeup();
                
                // Rotate to next reversal thread in round-robin format
                currReversalThreadId++;
                if (currReversalThreadId == reversalThreadCount) {
                    currReversalThreadId = 0;
                }
            }
        }
    }
    
    private synchronized void initializeReversalThreads() {
        if (reversalThreads != null) {
            return;
        }
        
        try {
            reversalThreadCount = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.CARD_REVERSAL_THREAD_COUNT,
                                                                                  EmsPropertiesService.DEFAULT_REVERSAL_THREAD_COUNT);
        } catch (Exception e) {
            // Log and leave value as default
            LOG.warn(e.getMessage(), e);
        }
        
        // Initialize array and create settlement threads
        reversalThreads = new CardProcessingThread[reversalThreadCount];
        for (int i = 0; i < reversalThreadCount; i++) {
            reversalThreads[i] = new CardProcessingThread(i, TYPE_REVERSAL, this.cardProcessingManager, this);
            reversalThreads[i].start();
        }
    }
}
