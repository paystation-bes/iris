package com.digitalpaytech.cardprocessing.cps;

import com.digitalpaytech.domain.emvrefund.EMVRefund;
import com.digitalpaytech.domain.emvrefund.EMVRefundResponse;

public interface EMVRefundService {
    void saveCharge(EMVRefund emvRefund);
    EMVRefund callPushEMVData(EMVRefund emvRefund);
    EMVRefundResponse callRefundsRealTime(EMVRefund emvRefund);
    boolean deleteEMVRefund(Integer emvRefundId);
    Integer count();
}
