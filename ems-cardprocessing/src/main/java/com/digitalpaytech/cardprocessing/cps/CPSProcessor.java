package com.digitalpaytech.cardprocessing.cps;

import com.digitalpaytech.dto.cps.CPSTransactionDto;
import com.digitalpaytech.ribbon.ClientFactory;
import com.digitalpaytech.util.json.CardTransactionResponseGenerator;

public interface CPSProcessor {
        
    CardTransactionResponseGenerator processRefund(CPSTransactionDto cpsTransactionDto);            
    
    CardTransactionResponseGenerator processAddOn(CPSTransactionDto cpsTransactionDto);
    
    String processTransactionTestCharge(CPSTransactionDto cpsTransactionDto);
    
    void setClientFactory(ClientFactory clientFactory);
}
