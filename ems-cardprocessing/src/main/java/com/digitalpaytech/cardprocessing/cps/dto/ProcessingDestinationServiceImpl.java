package com.digitalpaytech.cardprocessing.cps.dto;

import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.cardprocessing.cps.ProcessingDestinationService;
import com.digitalpaytech.cardprocessing.cps.impl.ProcessingDestinationDTO;
import com.digitalpaytech.domain.CreditCardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.service.CreditCardTypeService;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.LinkCardTypeService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.CustomerAdminUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.netflix.client.ClientException;

@Service("processingDestinationService")
public class ProcessingDestinationServiceImpl implements ProcessingDestinationService {
    
    @Autowired
    private LinkCardTypeService linkCardTypeService;
    
    @Autowired
    private CreditCardTypeService creditCardTypeService;
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private CustomerCardTypeService customerCardTypeService;
    
    @Autowired
    private CryptoService cryptoService;
    
    @Override
    public ProcessingDestinationDTO pickProcessingDest(final String encryptedCardData, final int customerId, final int pointOfSaleId)
        throws CryptoException {
        if (StringUtils.isBlank(encryptedCardData) || this.cryptoService.isLinkCryptoKey(encryptedCardData)) {
            return determineLinkEncDest(encryptedCardData, customerId, pointOfSaleId);
        } else {
            return determineScalaEncDest(encryptedCardData, pointOfSaleId);
        }
    }
    
    private ProcessingDestinationDTO determineLinkEncDest(final String linkEncryptedCardData, final int customerId, final int pointOfSaleId)
        throws CryptoException {
        
        //unknown credit card type will get the value card merchant account for now but will change when value cards are also managed in Link
        Integer cardTypeId = WebCoreConstants.CARD_TYPE_VALUE_CARD;
        String cardTypeName = null;
        
        final Customer customer = this.customerService.findCustomer(customerId);
        if (customer == null) {
            throw new IllegalArgumentException("Customer not found for transaction at PointOfSale " + pointOfSaleId);
        }
        final Optional<Integer> unifiId = CustomerAdminUtil.getUnifiId(customer);
        
        if (StringUtils.isNotBlank(linkEncryptedCardData)) {
            cardTypeName = this.linkCardTypeService.getCardType(unifiId.orElseGet(() -> 1), linkEncryptedCardData);
            
            final CreditCardType creditCardType = this.creditCardTypeService.getCreditCardTypeByName(cardTypeName);
            if (creditCardType != null && creditCardType.isIsValid()) {
                cardTypeId = WebCoreConstants.CARD_TYPE_CREDIT_CARD;
            }
        } else {
            // If card data is blank we compare the credit card merchant account for EMV. Cash transactions must ignore this result
            cardTypeId = WebCoreConstants.CARD_TYPE_CREDIT_CARD;
        }
        
        final MerchantAccount merchantAccount =
                this.merchantAccountService.findByPOSAndCardTypeAndNotDeletedAndMerchantStatusEnabled(pointOfSaleId, cardTypeId);
        
        final boolean processInLink = CardProcessingUtil.isNotNullAndIsLink(merchantAccount) && merchantAccount.getTerminalToken() != null
                                      && !StringUtils.isBlank(cardTypeName);
        
        final ProcessingDestinationDTO processingDTO = new ProcessingDestinationDTO();
        processingDTO.setProcessInLink(processInLink);
        processingDTO.setCardTypeName(cardTypeName);
        processingDTO.setMerchantAccount(merchantAccount);
        
        return processingDTO;
    }
    
    private ProcessingDestinationDTO determineScalaEncDest(final String scalaEncryptedCardData, final int pointOfSaleId) throws CryptoException {
        
        final boolean isScalaCreditCard = this.customerCardTypeService.isCreditCardTrack2(this.cryptoService.decryptData(scalaEncryptedCardData));
        
        final int cardTypeId;
        if (isScalaCreditCard) {
            cardTypeId = WebCoreConstants.CARD_TYPE_CREDIT_CARD;
        } else {
            cardTypeId = WebCoreConstants.CARD_TYPE_VALUE_CARD;
        }
        
        final MerchantAccount merchantAccount =
                this.merchantAccountService.findByPOSAndCardTypeAndNotDeletedAndMerchantStatusEnabled(pointOfSaleId, cardTypeId);
        
        final boolean lnikMa = CardProcessingUtil.isNotNullAndIsLink(merchantAccount);
        final ProcessingDestinationDTO processingDTO = new ProcessingDestinationDTO();
        processingDTO.setProcessInLink(lnikMa);
        processingDTO.setMerchantAccount(merchantAccount);
        if (lnikMa) {
            throw new CryptoException(new ClientException("Cannot process preauth in link that was encrypted in scala crypto"));
        }
        return processingDTO;
        
    }
}
