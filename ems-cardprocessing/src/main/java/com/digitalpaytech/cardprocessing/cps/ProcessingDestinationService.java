package com.digitalpaytech.cardprocessing.cps;

import com.digitalpaytech.cardprocessing.cps.impl.ProcessingDestinationDTO;
import com.digitalpaytech.exception.CryptoException;

public interface ProcessingDestinationService {
    
    ProcessingDestinationDTO pickProcessingDest(String encryptedCardData, int customerId, int pointOfSaleId) throws CryptoException;
    
}
