package com.digitalpaytech.cardprocessing.cps.impl;

import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.json.CardTransactionResponseGenerator;
import com.digitalpaytech.cardprocessing.cps.EMVRefundService;
import com.digitalpaytech.cardprocessing.cps.CPSProcessor;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.emvrefund.EMVRefund;
import com.digitalpaytech.domain.emvrefund.EMVRefundResponse;
import com.digitalpaytech.ribbon.ClientFactory;
import com.digitalpaytech.client.PaymentClient;
import com.digitalpaytech.dto.cps.CPSTransactionDto;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.Charset;
import java.util.List;

@Service("emvRefundService")
@Transactional(propagation = Propagation.REQUIRED)
public class EMVRefundServiceImpl implements EMVRefundService {
    private static final Logger LOG = Logger.getLogger(EMVRefundServiceImpl.class);
    
    @Autowired
    private EntityDao entityDao;

    @Autowired
    private CPSProcessor cpsProcessor;
    
    @Autowired
    private ClientFactory clientFactory;
    
    @Override
    public final void saveCharge(final EMVRefund emvRefund) {
        this.entityDao.save(emvRefund);
    }
    
    @Override
    public final EMVRefund callPushEMVData(final EMVRefund emvRefund) {
        final PaymentClient payService = this.clientFactory.from(PaymentClient.class);
        final String chargeToken = payService.pushEMVData(emvRefund.getCardTransactionJson()).execute().toString(Charset.defaultCharset());
        
        emvRefund.setChargeToken(chargeToken);
        this.entityDao.saveOrUpdate(emvRefund);
        
        LOG.info("EMVRefund is inserted, chargeToken: " + chargeToken);
        
        return emvRefund;
    }
    

    @Override
    public final EMVRefundResponse callRefundsRealTime(final EMVRefund emvRefund) {
        final MerchantAccount ma = new MerchantAccount();
        ma.setTerminalToken(emvRefund.getTerminalToken());

        final CPSTransactionDto cpsTransactionDto = new CPSTransactionDto(ma);
        cpsTransactionDto.setChargeToken(emvRefund.getChargeToken());
        
        try {
            final CardTransactionResponseGenerator resp = this.cpsProcessor.processRefund(cpsTransactionDto);
            LOG.info(resp);
            return createEMVRefundResponse(resp);
        } catch (RuntimeException re) {
            LOG.error(StandardConstants.STRING_STATUS_CODE_NOT_PROCESSED 
                      + " due to RuntimeException - com.netflix.hystrix.exception.HystrixBadRequestException ", re);
            
            final StringBuilder bdr = new StringBuilder();
            if (re.getCause() != null && re.getCause().getCause() != null) {
                bdr.append(re.getCause().getCause().toString());
            } else {
                bdr.append(re.getCause().toString());
            }
            return new EMVRefundResponse(WebCoreConstants.JSON_ERROR_STATUS_LABEL, bdr.toString());
        }
    }
    
    private EMVRefundResponse createEMVRefundResponse(final CardTransactionResponseGenerator resp) {
        if (resp != null && resp.getStatus() != null) {
            final String code = CardProcessingUtil.getAcceptedIfAcceptedOrAlreadyDone(resp.getStatus().getCode(), 
                                                                                      resp.getStatus().getMessage(), 
                                                                                      CardProcessingConstants.TX_ALREADY_REFUNDED_VOIDED_WITH_WITHOUT_SPACES);
            return new EMVRefundResponse(code, resp.getStatus().getMessage());
        } else {
            return new EMVRefundResponse(WebCoreConstants.JSON_ERROR_STATUS_LABEL, "No Status in CardTransactionResponseGenerator: " + resp);
        }
    }
    
    @Override
    public final boolean deleteEMVRefund(final Integer emvRefundId) {
        final EMVRefund refund = this.entityDao.get(EMVRefund.class, emvRefundId);
        if (refund != null) {
            this.entityDao.delete(refund);
            return true;
        }
        LOG.warn("EMVRefund ID: " + emvRefundId + " doesn't exist!");
        return false;
    }

    @Override
    public final Integer count() {
        final Criteria criteria = this.entityDao.createCriteria(EMVRefund.class);
        criteria.setProjection(Projections.rowCount());
        final List<Long> count = criteria.list();
        if (count != null) {
            return count.get(0).intValue();
        } else  {
            return StandardConstants.INT_MINUS_ONE;
        }
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }

    public final void setCPSProcessor(final CPSProcessor cpsprocessor) {
        this.cpsProcessor = cpsprocessor;
    }
}
