package com.digitalpaytech.cardprocessing.cps.impl;

import java.nio.charset.Charset;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.cardprocessing.cps.CPSProcessor;
import com.digitalpaytech.client.PaymentClient;
import com.digitalpaytech.dto.cps.CPSTransactionDto;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.ribbon.ClientFactory;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.json.CardTransactionResponseGenerator;
import com.digitalpaytech.util.json.Status;
import com.digitalpaytech.util.json.TestTerminalResponseGenerator;
import com.digitalpaytech.util.json.TokenGenerator;

/**
 * CPSProcessorImpl.java is a Spring-Managed component.
 */

@Component("cpsProcessor")
public class CPSProcessorImpl implements CPSProcessor {
    
    public static final String ERROR_CODE_HEADER_NAME = "Error-Code";
    private static final String NOT_EXIST_MSG_KEY = "cps.error.card.transaction.not.exist";
    private static final String JSON_CONVERSION_ERROR_MSG_KEY = "cps.error.json.conversion.error";
    private static final Logger LOG = Logger.getLogger(CPSProcessorImpl.class);
    
    @Autowired
    private ClientFactory clientFactory;
    
    @Autowired
    private MessageHelper messageHelper;
  
    @Override
    public final CardTransactionResponseGenerator processRefund(final CPSTransactionDto cpsTransactionDto) {
        String refundRespJson = null;
        try {
            final PaymentClient paymentClient = this.clientFactory.from(PaymentClient.class);
            final String refundReqJson = TokenGenerator.generate(new TokenGenerator(cpsTransactionDto.getChargeToken(),
                                                                                    cpsTransactionDto.getMerchantAccount().getTerminalToken()));
            
            refundRespJson = paymentClient.refundsRealTime(refundReqJson).execute().toString(Charset.defaultCharset());
            
            if (isInvalidTokenResponse(refundRespJson)) {
                return createNotExistResponse();
            }
            return CardTransactionResponseGenerator.generate(refundRespJson);
            
        } catch (JsonException je) {
            LOG.error(this.messageHelper.getMessage(JSON_CONVERSION_ERROR_MSG_KEY) + StandardConstants.STRING_DASH + refundRespJson, je);
            
            final Status status = new Status(StandardConstants.STRING_STATUS_CODE_NOT_PROCESSED,
                                             this.messageHelper.getMessage(JSON_CONVERSION_ERROR_MSG_KEY));
            return new CardTransactionResponseGenerator(status);
        }
    }

    @Override
    public final CardTransactionResponseGenerator processAddOn(final CPSTransactionDto cpsTransactionDto) {
        String respJson = null;
        try {
            final PaymentClient paymentClient = this.clientFactory.from(PaymentClient.class);
            final String addOnReqJson = 
                    TokenGenerator.generate(new TokenGenerator(cpsTransactionDto.getChargeToken(),
                                                               cpsTransactionDto.getMerchantAccount().getTerminalToken(),
                                                               cpsTransactionDto.getExtensionRefundProcessorTransaction().getAmount()));
            
            respJson = paymentClient.addOn(addOnReqJson).execute().toString(Charset.defaultCharset());
            if (isInvalidTokenResponse(respJson)) {
                return createNotExistResponse();
            }
            return CardTransactionResponseGenerator.generate(respJson);
            
        } catch (JsonException je) {
            LOG.error(this.messageHelper.getMessage(JSON_CONVERSION_ERROR_MSG_KEY) + StandardConstants.STRING_DASH + respJson, je);
            
            final Status status = new Status(StandardConstants.STRING_STATUS_CODE_NOT_PROCESSED,
                                             this.messageHelper.getMessage(JSON_CONVERSION_ERROR_MSG_KEY));
            return new CardTransactionResponseGenerator(status);
        }   
    } 
    
    @Override
    public final String processTransactionTestCharge(final CPSTransactionDto cpsTransactionDto) {
        final PaymentClient payService = this.clientFactory.from(PaymentClient.class);
        final String jsonResponse = payService.testTerminalRequest(cpsTransactionDto.getMerchantAccount().getTerminalToken()).execute()
                .toString(Charset.defaultCharset());
        try {
            final TestTerminalResponseGenerator testTerminalResponse = TestTerminalResponseGenerator.generate(jsonResponse);
            if (testTerminalResponse != null 
                    && isAccepted(testTerminalResponse.getStatusCode())) {
                return WebCoreConstants.RESPONSE_TRUE;    
            }
            
            final StringBuilder bdr = new StringBuilder();
            bdr.append(WebCoreConstants.RESPONSE_FALSE).append(StandardConstants.STRING_COLON).append(testTerminalResponse.getStatus().getMessage());
            return bdr.toString();
        } catch (JsonException e) {
            LOG.error("CreditcallProcessor.processTransactionTestCharge(): Unable to process JSON Mapping", e);
            return WebCoreConstants.RESPONSE_FALSE;
        }
    }
    
    @Override
    public final void setClientFactory(final ClientFactory clientFactory) {
        this.clientFactory = clientFactory;
    }
    
    public final void setMessageHelper(final MessageHelper messageHelper) {
        this.messageHelper = messageHelper;
    }    
    
    private boolean isInvalidTokenResponse(final String json) {
        if (json.equalsIgnoreCase(this.messageHelper.getMessage(NOT_EXIST_MSG_KEY))) {
            return true;
        }
        return false;
    }
    
    private CardTransactionResponseGenerator createNotExistResponse() {
        final Status status = new Status(StandardConstants.STRING_STATUS_CODE_NOT_PROCESSED,
                                         this.messageHelper.getMessage(NOT_EXIST_MSG_KEY));
        return new CardTransactionResponseGenerator(status);
    }
    
    private boolean isAccepted(final String statusCode) {
        if (StringUtils.isNotBlank(statusCode)
                && statusCode.equalsIgnoreCase(StandardConstants.STRING_STATUS_CODE_ACCEPTED)) {
            return true;
        }
        return false;
    }
}
