package com.digitalpaytech.cardprocessing.cps.impl;

import com.digitalpaytech.domain.MerchantAccount;

public class ProcessingDestinationDTO {
    private String cardTypeName;
    private MerchantAccount merchantAccount;
    private boolean processInLink;
    
    public String getCardTypeName() {
        return this.cardTypeName;
    }
    
    public void setCardTypeName(final String cardTypeName) {
        this.cardTypeName = cardTypeName;
    }
    
    public MerchantAccount getMerchantAccount() {
        return this.merchantAccount;
    }
    
    public void setMerchantAccount(final MerchantAccount merchantAccount) {
        this.merchantAccount = merchantAccount;
    }
    
    public boolean isProcessInLink() {
        return this.processInLink;
    }
    
    public void setProcessInLink(final boolean processInLink) {
        this.processInLink = processInLink;
    }
    
}
