package com.digitalpaytech.cardprocessing;

import org.apache.log4j.Logger;

public class TrackDataProcessingThread extends Thread {
    private final static Logger logger = Logger.getLogger(TrackDataProcessingThread.class);
    private CardProcessingMaster cardProcessingMaster;
    
    public TrackDataProcessingThread(CardProcessingMaster cardProcessingMaster) {
        super(TrackDataProcessingThread.class.getName());
        this.cardProcessingMaster = cardProcessingMaster;
        
        // set normal priority
        this.setPriority(Thread.NORM_PRIORITY);
    }
    
    public void run() {
        logger.info("STARTING SaveTrackDataProcessing");
        
        while (this.cardProcessingMaster.getStoreForwardQueueSize() > 0) {
            logger.info(this.cardProcessingMaster.getStoreForwardQueueSize() + " Store Forward Data in the StoreForwardQueue.");
            
            Object obj = this.cardProcessingMaster.getTransactionFromNextStoreFowardQueue();
            
            logger.debug(obj);
            
            this.cardProcessingMaster.processSaveTrackDataToList(obj);
        }
        logger.info("STOPPING SaveTrackDataProcessing");
    }
    
}
