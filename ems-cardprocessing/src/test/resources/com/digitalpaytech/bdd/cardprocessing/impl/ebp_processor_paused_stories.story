Scenario: Throw an exception while trying to extend and then CardProcessingManager should return processType of 2
Given a parking extension request is sent where the
spaceNumber is 10
mobileNumber is 7782223333
addTimeNum is 55111
permitBeginGMT is 2016:10:03:16:27:54:GMT
permitExpireGMT is 2016:10:03:16:57:54:GMT
When I add 30 minutes
Then my smartphone receives '-FAILURE-\nParking NOT extended.\nUnable to process credit card authorization.\nParking expires at 57:54'
