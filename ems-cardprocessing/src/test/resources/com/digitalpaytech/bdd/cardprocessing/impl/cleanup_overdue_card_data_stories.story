Narrative: Every day in the morning there is a schedule background job that removes PreAuthIds 
from expired ProcessorTransaction records.

Scenario: Remove expired PreAuthId in ProcessorTransaction records
Given there exists a preauth transaction data where the
preauth id is 1
And there exists a preauth transaction data where the 
preauth id is 2
And there exists a preauth transaction data where the 
preauth id is 3
And there exists a preauth transaction data where the 
preauth id is 4
And there exists a preauth transaction data where the 
preauth id is 5

And there exists a processor transaction where the 
id is 1
preauth transaction data is 1
And there exists a processor transaction where the 
id is 2
preauth transaction data is 2
And there exists a processor transaction where the 
id is 3
preauth transaction data is 3
And there exists a processor transaction where the 
id is 4
preauth transaction data is 4
And there exists a processor transaction where the 
id is 5
preauth transaction data is 5
When I clean the overdue card data
Then processor transaction id 1 AND preauth is null
And processor transaction id 2 AND preauth is null
And processor transaction id 3 AND preauth is null
And processor transaction id 4 AND preauth is null
And processor transaction id 5 AND preauth is null
