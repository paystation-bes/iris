package com.digitalpaytech.service.impl;

import com.digitalpaytech.service.MerchantAccountService;
import java.util.Map;
import java.util.HashMap;

import org.junit.Test;
import static org.junit.Assert.*;
import org.apache.log4j.Logger;

public class MerchantAccountServiceImplTest {

	private static Logger log = Logger.getLogger(MerchantAccountServiceImplTest.class);
	
	
	@Test
	public void createQueryString() {
		Map<String, String> map = new HashMap<String, String>();
		map.put(MerchantAccountService.PROCESSOR_ID, "56789");
		map.put(MerchantAccountService.FIELD_1, "1234");
		map.put(MerchantAccountService.FIELD_2, "5678");
		map.put(MerchantAccountService.FIELD_3, "9012");
		
		MerchantAccountServiceImpl serv = new MerchantAccountServiceImpl();
		String query = serv.createQueryString(map, false, false);
		log.info(query);
		
		int idx = query.indexOf(":processorId");
		assertTrue(idx != -1);
		
		idx = query.indexOf(":field1");
		assertTrue(idx != -1);
		
		idx = query.indexOf("AND ma.field2 = :field2");
		assertTrue(idx != -1);

		idx = query.indexOf("AND ma.field3 = :field3");
		assertTrue(idx != -1);
		
		assertEquals(-1, query.indexOf("field4"));
		assertEquals(-1, query.indexOf("field5"));
		assertEquals(-1, query.indexOf("field6"));

		
		map.put(MerchantAccountService.FIELD_4, "abc");
		map.put(MerchantAccountService.FIELD_5, "def");
		map.put(MerchantAccountService.FIELD_6, "ghi");
		query = serv.createQueryString(map, false, false);
		log.info(query);
		
		idx = query.indexOf("AND ma.field4 = :field4");
		assertTrue(idx != -1);

		idx = query.indexOf("AND ma.field5 = :field5");
		assertTrue(idx != -1);

		idx = query.indexOf("AND ma.field6 = :field6");
		assertTrue(idx != -1);
	}
}
