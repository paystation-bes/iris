package com.digitalpaytech.service.impl;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.impl.CardTypeServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;

public class CardTypeServiceImplTest {

	private EntityService entityService;
	
	@Test
	public void getCardTypesMap() {
		CardTypeServiceImpl serv = new CardTypeServiceImpl();
		serv.setEntityService(entityService);
		Map<Integer, CardType> map = serv.getCardTypesMap();
		
		assertNotNull(map);
		assertEquals(4, map.size());
		assertEquals("N/A", map.get(0).getName());
		assertEquals("Credit Card", map.get(1).getName());
		assertEquals("Smart Card", map.get(2).getName());
		assertEquals("Value Card", map.get(3).getName());
	}
	
	
	@Before
	public void setUpEntityService() {
		final List<CardType> list = new ArrayList<CardType>();
		list.add(createCardType(0, "N/A"));
		list.add(createCardType(1, "Credit Card"));
		list.add(createCardType(2, "Smart Card"));
		list.add(createCardType(3, "Value Card"));
		
		entityService = new EntityServiceImpl() {
			@Override
			public <T> List<T> loadAll(Class<T> entityClass) {
				return (List<T>) list; 
			}
		};
	}
	
	private CardType createCardType(int id, String name) {
		CardType ct = new CardType();
		ct.setId(id);
		ct.setName(name);
		return ct;
	}
}
