package com.digitalpaytech.service.impl;

import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.CustomerBadCard;

@org.junit.Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:ems-test.xml", "classpath:ems-security.xml"})
@Transactional
public class CustomerBadCardServiceImplTest {

	private static Logger log = Logger.getLogger(CustomerBadCardServiceImplTest.class);
	
	@Autowired
	private EntityDao entityDao;
	
	@Transactional(propagation=Propagation.SUPPORTS)
	@Test
	public void getCustomerBadCards() {

		//getCustomerBadCard(String cardNumber, int customerId, String cardType)
		
		String cardNumber = "4242424242424242";
		int customerId = 3;
		CardType cardType = new CardType();
		cardType.setId(1);
		cardType.setName("Credit Card");
		
		log.debug("@@@@@@@@@@@@@@@@ start -----------------");
		Criteria crit = entityDao.createCriteria(CustomerBadCard.class);
		// Prepares CustomerCardType table information.
		crit = crit.createAlias("customerCardType", "cbcCustomerCardType");
		
		crit = crit.add(Restrictions.eq("cbcCustomerCardType.customer.id", customerId)); 
		crit = crit.add(Restrictions.like("cardNumberOrHash", cardNumber));
		
		
		crit = crit.setFetchMode("cbcCustomerCardType", FetchMode.JOIN).addOrder(Order.asc("cbcCustomerCardType.id"));
		if (cardType != null) {
			crit = crit.add(Restrictions.eq("cbcCustomerCardType.cardType.id", cardType.getId()));
		}
		
		List<CustomerBadCard> list = crit.setCacheable(true).list();
		log.debug("(((((((((((((((((( list: " + list.size());
		for (int i = 0; i < list.size(); i++) {
			log.debug("::::::::: " + list.get(i));
		}
	}
	
}
