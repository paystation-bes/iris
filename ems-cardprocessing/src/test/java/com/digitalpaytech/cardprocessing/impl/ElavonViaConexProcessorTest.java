package com.digitalpaytech.cardprocessing.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Properties;
import java.util.Random;

import org.apache.log4j.Logger;
import org.junit.Test;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.CreditCardType;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.CreditCardTestNumberService;
import com.digitalpaytech.service.impl.CommonProcessingServiceImpl;
import com.digitalpaytech.service.impl.CustomerCardTypeServiceImpl;
import com.digitalpaytech.service.impl.EmsPropertiesServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.service.impl.CreditCardTestNumberServiceImpl;
import com.digitalpaytech.service.processor.ElavonViaConexService;
import com.digitalpaytech.service.processor.impl.ElavonViaConexServiceImpl;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.WebCoreConstants;

public class ElavonViaConexProcessorTest {
    private static final Logger LOG = Logger.getLogger(ElavonViaConexProcessorTest.class);
    private static final String TEST_URL = "https://testgate.viaconex.com/cgi-bin/encompass4.cgi";
    private static final int AMOUNT = 1046;
    private ElavonViaConexProcessor elavonProcessor;
    private boolean useMockFlag = true;
    
    private void createElavonViaConexProcessor() {
        if (this.useMockFlag) {
            this.elavonProcessor =
                    new MockElavonViaConexProcessor(createMerchantAccount(), createCommonProcessingService(), createEmsPropertiesService(),
                            createCustomerCardTypeService(), createMerchantAccountService(), createElavonViaConexService(), createMessageHelper());
        } else {
            this.elavonProcessor = new ElavonViaConexProcessor(createMerchantAccount(), createCommonProcessingService(), createEmsPropertiesService(),
                    createCustomerCardTypeService(), createMerchantAccountService(), createElavonViaConexService(), createMessageHelper(),
                    new CreditCardTestNumberServiceImpl());
            
        }
        final Properties prop = new Properties();
        prop.setProperty(EmsPropertiesService.ELAVON_VIACONEX_APPLICATION_ID, "TZKQ01GC");
        prop.setProperty(EmsPropertiesService.ELAVON_VIACONEX_REGISTRATION_KEY, "TEMP_PORTAL_FAKE_KEY");
        final EmsPropertiesServiceImpl serv = new EmsPropertiesServiceImpl();
        serv.setProperties(prop);
        //this.elavonProcessor.setEmsPropService(serv);
    }
    
    @Test
    public final void processPreAuth() {
        createElavonViaConexProcessor();
        
        final PreAuth preAuth = new PreAuth();
        preAuth.setAmount(AMOUNT);
        
        preAuth.setMerchantAccount(this.elavonProcessor.getMerchantAccount());
        
        final String panex = createPANExpiryInAYear("4242424242424242");
        preAuth.setCardData(panex);
        
        //        final boolean isApproved = this.elavonProcessor.processPreAuth(preAuth);
        //        LOG.info("PreAuth response, is approved? " + isApproved);
        
    }
    
    @Test
    public void processPostAuth() {
        createElavonViaConexProcessor();
        
        final PreAuth preAuth = new PreAuth();
        final ProcessorTransaction procTx = new ProcessorTransaction();
        
        preAuth.setAmount(AMOUNT);
        
        preAuth.setMerchantAccount(this.elavonProcessor.getMerchantAccount());
        
        final String exp = "1219";
        final String cc = "4124939999999990";
        final StringBuilder bdr = new StringBuilder();
        bdr.append(cc).append("=").append(exp);
        preAuth.setCardData(bdr.toString());
        
        final Object[] results = this.elavonProcessor.processPostAuth(preAuth, procTx);
        
        LOG.info("Settle result: ");
        //        for (Object o : results) {
        //            LOG.info(o.toString());
        //        }
    }
    
    @Test
    public final void processCharge() {
        createElavonViaConexProcessor();
        
        final ProcessorTransaction procTx = new ProcessorTransaction();
        procTx.setAmount(AMOUNT);
        procTx.setMerchantAccount(this.elavonProcessor.getMerchantAccount());
        
        final String exp = "1219";
        final String cc = "4124939999999990";
        final StringBuilder bdr = new StringBuilder();
        bdr.append(cc).append("=").append(exp);
        
        final CreditCardType cct = new CreditCardType();
        cct.setIsValid(true);
        final Track2Card track2Card = new Track2Card(cct, 1, bdr.toString());
        
        //        final Object[] results = this.elavonProcessor.processCharge(track2Card, procTx);
        //        LOG.info("S & F result: ");
        //        for (Object o : results) {
        //            LOG.info(o.toString());
        //        }        
    }
    
    @Test
    public final void testCharge() {
        createElavonViaConexProcessor();
        
        final String isApproved = this.elavonProcessor.testCharge(this.elavonProcessor.getMerchantAccount());
        LOG.info("testCharge response is? " + isApproved);
    }
    
    private MerchantAccount createMerchantAccount() {
        final MerchantAccount ma = new MerchantAccount();
        
        // field 1 = Merchant ID
        ma.setField1("99988811198");
        // field 2 = Terminal_ID
        ma.setField2("0017340009998881119888");
        
        final Processor processor = new Processor();
        processor.setTestUrl(TEST_URL);
        ma.setProcessor(processor);
        
        ma.setReferenceCounter(new Random().nextInt(Integer.MAX_VALUE));
        
        return ma;
    }
    
    private CommonProcessingService createCommonProcessingService() {
        return new CommonProcessingServiceImpl() {
            @Override
            public String getNewReferenceNumber(final MerchantAccount merchantAccount) {
                return String.valueOf(merchantAccount.getReferenceCounter());
            }
            
            @Override
            public String getDestinationUrlString(final Processor processor) {
                return TEST_URL;
            }
        };
    }
    
    private String createPANExpiryInAYear(final String pan) {
        final SimpleDateFormat format = new SimpleDateFormat("yyMM");
        final Calendar cal = new GregorianCalendar();
        cal.set(Calendar.MONTH, 12);
        cal.add(Calendar.YEAR, 1);
        final String yymm = format.format(cal.getTime());
        
        final StringBuilder bdr = new StringBuilder();
        bdr.append(pan).append(WebCoreConstants.EQUAL_SIGN).append(yymm);
        return bdr.toString();
    }
    
    private EmsPropertiesService createEmsPropertiesService() {
        final Properties prop = new Properties();
        prop.put(EmsPropertiesService.ELAVON_VIACONEX_SPEC_VERSION, EmsPropertiesService.DEFAULT_ELAVON_VIACONEX_SPEC_VERSION);
        
        final EmsPropertiesServiceImpl serv = new EmsPropertiesServiceImpl();
        serv.setProperties(prop);
        return serv;
    }
    
    private CustomerCardTypeService createCustomerCardTypeService() {
        return new CustomerCardTypeServiceImpl();
    }
    
    private MerchantAccountService createMerchantAccountService() {
        return new MerchantAccountServiceImpl();
    }
    
    private ElavonViaConexService createElavonViaConexService() {
        return new ElavonViaConexServiceImpl();
    }
    
    private MessageHelper createMessageHelper() {
        return new MessageHelper();
    }
    
    // =========================================================================================================
    // =========================================================================================================    
    
    class MockElavonViaConexProcessor extends ElavonViaConexProcessor {
        
        public MockElavonViaConexProcessor(final MerchantAccount merchantAccount, final CommonProcessingService commonProcessingService,
                final EmsPropertiesService emsPropertiesService, final CustomerCardTypeService customerCardTypeService,
                final MerchantAccountService merchantAccountService, final ElavonViaConexService elavonViaConexService,
                final MessageHelper messageHelper) {
            super(merchantAccount, commonProcessingService, emsPropertiesService, customerCardTypeService, merchantAccountService,
                    elavonViaConexService, messageHelper, new CreditCardTestNumberServiceImpl());
        }
        
        public boolean processPreAuth(final PreAuth preAuth) {
            return true;
        }
        
        public final Object[] processPostAuth(final PreAuth preAuth, final ProcessorTransaction processorTransaction) {
            return null;
        }
        
        public final Object[] processCharge(final Track2Card track2Card, final ProcessorTransaction chargeProcessorTransaction) {
            return null;
        }
        
        public final Object[] processRefund(final boolean isNonEmsRequest, final ProcessorTransaction origProcessorTransaction,
            final ProcessorTransaction refundProcessorTransaction, final String creditCardNumber, final String expiryMMYY) {
            return null;
        }
        
        public final String testCharge(final MerchantAccount ma) {
            return WebCoreConstants.RESPONSE_TRUE;
        }
        
        public final void processReversal(final Reversal reversal, final Track2Card track2Card) {
        }
        
        public void processReversal(final PreAuth preAuth, final Track2Card track2Card) {
        }
        
        public final boolean isReversalExpired(final Reversal reversal) {
            return false;
        }
        
        public final String processBatch(final MerchantAccount merchantAccount) {
            return null;
        }
    }
}
