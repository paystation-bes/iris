package com.digitalpaytech.cardprocessing.impl;

import com.digitalpaytech.cardprocessing.CardProcessor;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Reversal;

public class TDMerchantProcessorMockImpl extends CardProcessor {

    @Override
    public boolean getIsManualSave() {
        return false;
    }
    
    @Override
    public void savePreAuth(final PreAuth preAuth) {
    }
    
    @Override
    public boolean processPreAuth(final PreAuth preAuth) {
        final int psRefId = Integer.parseInt(preAuth.getPsRefId());
        // Scenario: Sending a PreAuthorization transaction for TD Merchant Services Processor for a Declined test card.
        if (psRefId == 255) {
            return false;
        
        // Scenario: Sending a PreAuthorization transaction for TD Merchant Services Processor with non user error returns EMS Unavailable
        } else if (psRefId == 315) {
            preAuth.setAuthorizationNumber("EXCEPTION");
            return false;
        
        // Scenario: Successful 2$ transaction using TD Merchant Processor (Pre-Auth / Post-Auth)
        } else if (psRefId == 535) {
            preAuth.setAuthorizationNumber("12345678");
            preAuth.setIsApproved(true);
        }
        return true;
    }

    @Override
    public Object[] processPostAuth(PreAuth preAuth, ProcessorTransaction processorTransaction) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object[] processRefund(boolean isNonEmsRequest, ProcessorTransaction origProcessorTransaction,
        ProcessorTransaction refundProcessorTransaction, String creditCardNumber, String expiryMMYY) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object[] processCharge(Track2Card track2Card, ProcessorTransaction chargeProcessorTransaction) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void processReversal(Reversal reversal, Track2Card track2Card) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean isReversalExpired(Reversal reversal) {
        // TODO Auto-generated method stub
        return false;
    }
}
