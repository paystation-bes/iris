package com.digitalpaytech.cardprocessing.impl;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.impl.CommonProcessingServiceImpl;

public class BlackBoardProcessorTest {
    private CommonProcessingService commonProcessingService;
    
    @Before
    public void before() {
        commonProcessingService = new CommonProcessingServiceImpl() {

            @Override
            public boolean isProcessorTestMode(int processorId) {
                return false;
            }
        };
    }
    
    @Test
    public void verifyUrlsForProduction() {
        final Processor processor = new Processor();
        processor.setIsTest(false);
        processor.setProductionUrl(null);
        processor.setTestUrl("66.210.59.124,69.26.224.124");
        
        commonProcessingService = new CommonProcessingServiceImpl() {
            @Override
            public boolean isProcessorTestMode(int processorId) {
                return false;
            }

            @Override
            public Processor getProcessor(int processorId) {
                return processor;
            }
        };
        
        // Simulate production database.
        MerchantAccount md = new MerchantAccount("PRODUCTION TESTING", "101.101.101.101", processor);
        String[] urls = null;
        try {
            urls = getUrls(md);
            assertNotNull(urls);
            assertEquals(2, urls.length);
            assertNotNull(urls[0]);
            assertNotNull(urls[1]);
            assertEquals("", urls[0]);
            assertEquals("", urls[1]);
        
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
    
    
    @Test
    public void verifyUrlsForQA() {
        final Processor processor = new Processor();
        processor.setIsTest(true);
        processor.setProductionUrl(null);
        processor.setTestUrl("66.210.59.124,69.26.224.124");
        
        commonProcessingService = new CommonProcessingServiceImpl() {
            @Override
            public boolean isProcessorTestMode(int processorId) {
                return true;
            }

            @Override
            public Processor getProcessor(int processorId) {
                return processor;
            }
        };
        
        // Simulate QA database.
        MerchantAccount md = new MerchantAccount("QA TESTING", "66.210.59.124", processor);
        String[] urls = null;
        try {
            urls = getUrls(md);
            assertNotNull(urls);
            assertEquals(2, urls.length);
            assertNotNull(urls[0]);
            assertNotNull(urls[1]);
            assertEquals("66.210.59.124", urls[0]);
            assertEquals("69.26.224.124", urls[1]);
        
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
    
    
    /*
     * Need verify the logic block below copied from BlackBoardProcessor in this method.
     */
    private String[] getUrls(MerchantAccount md) {
        //------- Begin copying from BlackBoardProcessor constructor --------------------------
        String key = md.getField1();
        String iPAddress = md.getField2();

        // BlackBoardProcessor processor has 2 urls separated by comma, e.g. '66.210.59.124,69.26.224.124'
        String testServerUrl1 = "";
        String testServerUrl2 = "";
        String urlStr = commonProcessingService.getDestinationUrlString(md.getProcessor());
        if (StringUtils.isNotBlank(urlStr)) {
            String[] urls = urlStr.split(",");
            testServerUrl1 = WebCoreUtil.returnEmptyIfBlank(urls[0]).trim();
            if (urls.length > 1) {
                testServerUrl2 = WebCoreUtil.returnEmptyIfBlank(urls[1]).trim();
            }
        }
        //------- End copying from BlackBoardProcessor constructor --------------------------
        
        return new String[] { testServerUrl1, testServerUrl2 };
    }
    
    class MerchantAccount {
        private String field1;
        private String field2;
        private Processor processor;
        
        MerchantAccount(String field1, String field2, Processor processor) {
            this.field1 = field1;
            this.field2 = field2;
            this.processor = processor;
        }

        Processor getProcessor() {
            return processor;
        }
        String getField1() {
            return field1;
        }
        void setField1(String field1) {
            this.field1 = field1;
        }
        String getField2() {
            return field2;
        }
        void setField2(String field2) {
            this.field2 = field2;
        }
    }
}
