package com.digitalpaytech.cardprocessing.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.digitalpaytech.cardprocessing.CardProcessor;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.dto.ProcessorInfo;
import com.digitalpaytech.service.ProcessorService;
import com.digitalpaytech.service.impl.ProcessorServiceImpl;
import com.digitalpaytech.exception.CardProcessorPausedException;

import org.junit.Test;
import org.junit.Before;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

public class CardProcessorFactoryImplTest {
    private static final int ID_1 = 1;
    private static final int ID_2 = 2;
    private static final int ID_3 = 3;
    private static final int ID_4 = 4;
    private static final int ID_6 = 6;
    private static final int ID_7 = 7;
    private static final int ID_8 = 8;
    private static final int ID_9 = 9;
    private static final int ID_10 = 10;
    private static final int ID_11 = 11;
    private static final int ID_12 = 12;
    private static final int ID_16 = 16;
    
    private static final String CONC_KEY = "processor.concord";
    private static final String FDN_KEY = "processor.firstDataNashville";
    private static final String HEART_KEY = "processor.heartland";
    private static final String LINK2 = "processor.link2gov";
    private static final String ALLI = "processor.alliance";
    private static final String PAYTE = "processor.paymentech";
    private static final String PARAD = "processor.paradata";
    private static final String MONE = "processor.moneris";
    private static final String HORI = "processor.firstHorizon";
    private static final String AUTH = "processor.authorize.net";
    private static final String DATA = "processor.datawire";
    private static final String ELAVON = "processor.elavon";
    
    private Object[] objs;
    
    @Test
    public final void getCardProcessor() {
        // CardProcessor getCardProcessor(final MerchantAccount merchantAccount, final PointOfSale pointOfSale)
        final CardProcessorFactoryImpl fac = new CardProcessorFactoryImpl();
        fac.setProcessorsMap((Map) this.objs[0]);
        fac.setCreditCardProcessor((Set) this.objs[1]);
        fac.setProcessorService(createProcessorService());
        
        final MerchantAccount ma = new MerchantAccount();
        ma.setProcessor(createProcessor(ID_1, CONC_KEY));
        fac.pauseProcessor(ID_1);
        try {
            // Concord
            final CardProcessor proc = fac.getCardProcessor(ma, null);
            
        } catch (CardProcessorPausedException cppe) {
            assertTrue(true);
        }
    }
    
    @Test
    public final void resumeProcessor() {
        final CardProcessorFactoryImpl fac = new CardProcessorFactoryImpl();
        fac.setProcessorsMap((Map) this.objs[0]);
        fac.setCreditCardProcessor((Set) this.objs[1]);
        fac.setProcessorService(createProcessorService());
        
        // Heartland
        boolean b = fac.resumeProcessor(ID_3);
        assertTrue(b);
        assertFalse(fac.isProcessorPaused(ID_3));
        
        // Link2Gov
        b = fac.resumeProcessor(ID_4);
        assertTrue(b);
        assertFalse(fac.isProcessorPaused(ID_4));
        
        // Alliance
        b = fac.resumeProcessor(ID_6);
        assertTrue(b);
        assertFalse(fac.isProcessorPaused(ID_6));
        
        // Paymentech
        b = fac.resumeProcessor(ID_7);
        assertTrue(b);
        assertFalse(fac.isProcessorPaused(ID_7));
        
        // Paradata
        b = fac.resumeProcessor(ID_8);
        assertTrue(b);
        assertFalse(fac.isProcessorPaused(ID_8));
        
        // Moneris
        b = fac.resumeProcessor(ID_9);
        assertTrue(b);
        assertFalse(fac.isProcessorPaused(ID_9));
        
        // First Hori
        b = fac.resumeProcessor(ID_10);
        assertTrue(b);
        assertFalse(fac.isProcessorPaused(ID_10));
        
        // Auth.Net
        b = fac.resumeProcessor(ID_11);
        assertTrue(b);
        assertFalse(fac.isProcessorPaused(ID_11));
        
        // DataWire
        b = fac.resumeProcessor(ID_12);
        assertTrue(b);
        assertFalse(fac.isProcessorPaused(ID_12));
        
        b = fac.resumeProcessor(ID_16);
        assertTrue(b);
        assertFalse(fac.isProcessorPaused(ID_16));
    }
    
    @Test
    public final void pauseProcessor() {
        final CardProcessorFactoryImpl fac = new CardProcessorFactoryImpl();
        fac.setProcessorsMap((Map) this.objs[0]);
        fac.setCreditCardProcessor((Set) this.objs[1]);
        fac.setProcessorService(createProcessorServiceAllPaused());
        
        final MerchantAccount ma = new MerchantAccount();
        
        // Concord
        final boolean b = fac.pauseProcessor(ID_1);
        assertTrue(b);
        ma.setProcessor(createProcessor(ID_1, CONC_KEY));
        
        try {
            CardProcessor carProc = fac.getCardProcessor(ma, null);
            
        } catch (CardProcessorPausedException cppe) {
            assertTrue(true);
        }
    }
    
    @Before
    public final void loadProcessorsMapCreditCardProcessor() {
        final Map<Integer, ProcessorInfo> map = new HashMap<Integer, ProcessorInfo>();
        final ProcessorInfo conc = new ProcessorInfo(createProcessor(1, CONC_KEY), null, null, null, false);
        map.put(ID_1, conc);
        final ProcessorInfo fdn = new ProcessorInfo(createProcessor(2, FDN_KEY), null, null, null, false);
        map.put(ID_2, fdn);
        final ProcessorInfo heart = new ProcessorInfo(createProcessor(3, HEART_KEY), null, null, null, false);
        map.put(ID_3, heart);
        final ProcessorInfo link2 = new ProcessorInfo(createProcessor(4, LINK2), null, null, null, false);
        map.put(ID_4, link2);
        final ProcessorInfo alli = new ProcessorInfo(createProcessor(6, ALLI), null, null, null, false);
        map.put(ID_6, alli);
        final ProcessorInfo ptec = new ProcessorInfo(createProcessor(7, PAYTE), null, null, null, false);
        map.put(ID_7, ptec);
        final ProcessorInfo parda = new ProcessorInfo(createProcessor(8, PARAD), null, null, null, false);
        map.put(ID_8, parda);
        final ProcessorInfo mon = new ProcessorInfo(createProcessor(9, MONE), null, null, null, false);
        map.put(ID_9, mon);
        final ProcessorInfo fhoz = new ProcessorInfo(createProcessor(10, HORI), null, null, null, false);
        map.put(ID_10, fhoz);
        final ProcessorInfo auth = new ProcessorInfo(createProcessor(11, AUTH), null, null, null, false);
        map.put(ID_11, auth);
        final ProcessorInfo dawi = new ProcessorInfo(createProcessor(12, DATA), null, null, null, false);
        map.put(ID_12, dawi);
        final ProcessorInfo ela = new ProcessorInfo(createProcessor(16, ELAVON), null, null, null, false);
        map.put(ID_16, ela);
        
        final Set<ProcessorInfo> set = new HashSet<ProcessorInfo>();
        set.add(conc);
        set.add(fdn);
        set.add(heart);
        set.add(link2);
        set.add(alli);
        set.add(ptec);
        set.add(parda);
        set.add(mon);
        set.add(fhoz);
        set.add(auth);
        set.add(dawi);
        set.add(ela);
        
        this.objs = new Object[] { map, set };
    }
    
    private Processor createProcessor(final int id, final String name) {
        final Processor p = new Processor();
        p.setId(id);
        p.setName(name);
        return p;
    }
    
    private ProcessorService createProcessorService() {
        final ProcessorService p = new ProcessorServiceImpl() {
            @Override
            public Processor findProcessor(final Integer id) {
                final Processor p = createProcessor(id, "TEST");
                if (id == 1) {
                    p.setIsPaused(true);
                }
                return p;
            }
            
            @Override
            public void updateProcessor(final Processor processor) {
                
            }
        };
        return p;
    }
    
    private ProcessorService createProcessorServiceAllPaused() {
        final ProcessorService p = new ProcessorServiceImpl() {
            @Override
            public Processor findProcessor(final Integer id) {
                final Processor p = createProcessor(id, "TEST");
                p.setIsPaused(true);
                return p;
            }
            
            @Override
            public void updateProcessor(final Processor processor) {
                
            }
        };
        return p;
    }
}
