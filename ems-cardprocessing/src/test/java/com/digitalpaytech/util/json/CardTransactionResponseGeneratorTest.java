package com.digitalpaytech.util.json;

import org.junit.Test;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;

import com.digitalpaytech.util.json.JSON;;

public class CardTransactionResponseGeneratorTest {
    private static final Logger LOG = Logger.getLogger(CardTransactionResponseGeneratorTest.class);
    
    @Test
    public void generate() {
        try {
            final String respJson = buildCardTransactionResponseGenerator();
            final CardTransactionResponseGenerator respObj = CardTransactionResponseGenerator.generate(respJson);
            assertNotNull(respObj);
            LOG.info("respObj data: status code: " + respObj.getStatus().getCode() + ", processedDate: " + respObj.getProcessedDate());
            LOG.info("chargeToekn: " + respObj.getChargeToken() + ", ref: " + respObj.getReferenceNumber() + ", auth: " + respObj.getAuthorizationNumber());
            
            assertEquals("1111-2222-3333-4444", respObj.getChargeToken());
            assertEquals("59595", respObj.getAuthorizationNumber());
            assertEquals("98765", respObj.getProcessorTransactionId());
            assertEquals("2016-09-21T16:21:33", respObj.getProcessedDate());
            assertEquals("555-666-777", respObj.getReferenceNumber());
            
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
    
    private String buildCardTransactionResponseGenerator() throws Exception {
        final CardTransactionResponseGenerator gen = new CardTransactionResponseGenerator();
        final Status st = new Status();
        st.setCode("accepted");
        st.setMessage("OK");
        gen.setStatus(st);
        gen.setChargeToken("1111-2222-3333-4444");
        gen.setProcessorTransactionId("98765");
        gen.setProcessedDate("2016-09-21T16:21:33");
        gen.setReferenceNumber("555-666-777");
        gen.setAuthorizationNumber("59595");
        final JSON json = new JSON();
        return json.serialize(gen);
    }
    
}
