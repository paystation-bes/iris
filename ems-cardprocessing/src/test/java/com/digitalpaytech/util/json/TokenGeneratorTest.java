package com.digitalpaytech.util.json;

import org.junit.Test;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;

import com.digitalpaytech.util.json.TokenGenerator;

public class TokenGeneratorTest {
    private static final Logger LOG = Logger.getLogger(TokenGeneratorTest.class);
            
    @Test
    public void generate() {
        final TokenGenerator tokenObject = new TokenGenerator("TRANS_ID", "TERM_ID");
        try {
            final String json = TokenGenerator.generate(tokenObject);
            LOG.info("------------> " + json);
            assertEquals("{\"chargeToken\":\"TRANS_ID\",\"terminalToken\":\"TERM_ID\"}", json);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
    
    @Test
    public void generateWithAmount() {
        final TokenGenerator tokenObject = new TokenGenerator("111-222-333", "aaa-bbb-ccc", 500);
        try {
            final String json = TokenGenerator.generate(tokenObject);
            LOG.info("------------> " + json);
            assertEquals("{\"chargeToken\":\"111-222-333\",\"terminalToken\":\"aaa-bbb-ccc\",\"amount\":500}", json);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
}
