package com.digitalpaytech.util.json;

import org.junit.Test;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;


public class TestTerminalResponseGeneratorTest {
    private static final Logger LOG = Logger.getLogger(TestTerminalResponseGeneratorTest.class);
    
    @Test
    public void generate() {
        
        TestTerminalResponseGenerator term = null;
        try {
            term = TestTerminalResponseGenerator.generate(getRespJson(false));
            LOG.info("status code is: " + term.getStatusCode());
            assertEquals("accepted", term.getStatusCode());

            term = TestTerminalResponseGenerator.generate(getRespJson(true));
            LOG.info("message: " + term.getStatus().getMessage());
            assertEquals("OK", term.getStatus().getMessage());
            
        } catch (Exception e) {
            fail(e.getMessage());
        }
        
        
    }
    
    private final String getRespJson(final boolean includeMessage) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("{\"status\":{\"code\":\"accepted\"");
        if (includeMessage) {
            bdr.append(",\"message\":\"OK\"");
        }
        bdr.append("}}");
        return bdr.toString();
    }
}
