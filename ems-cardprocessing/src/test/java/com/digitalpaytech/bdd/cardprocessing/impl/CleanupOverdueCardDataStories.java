package com.digitalpaytech.bdd.cardprocessing.impl;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;


@RunWith(JUnitReportingRunner.class)
public class CleanupOverdueCardDataStories extends AbstractStories {
    
    public CleanupOverdueCardDataStories() {
        super();
        this.testHandlers.registerTestHandler("overduecarddataclean", TestCleanupOverdueCardData.class);
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
        
    }
    
    // Here we specify the steps classes
    @Override
    public final InjectableStepsFactory stepsFactory() {
        // varargs, can have more than one steps classes
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new CleanupOverdueCardDataSteps(this.testHandlers));
    }    
}
