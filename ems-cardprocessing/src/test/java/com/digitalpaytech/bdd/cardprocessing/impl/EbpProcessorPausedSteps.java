package com.digitalpaytech.bdd.cardprocessing.impl;

import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.BeforeStory;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import static org.junit.Assert.*;
import java.util.Date;

import org.apache.log4j.Logger;

import com.digitalpaytech.bdd.util.AbstractSteps;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandlers;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.domain.SmsAlert;
import com.digitalpaytech.domain.MobileNumber;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.DateUtil;

public class EbpProcessorPausedSteps extends AbstractSteps {

    private static final Logger LOG = Logger.getLogger(EbpProcessorPausedSteps.class);
    private static final String OBJECT_NAME = "ebpprocessorpaused";
    private static final String ACTION_NAME = "paused";
    private static final String IS_BLANK = "is ";
    private SmsAlert smsAlert;
    
    public EbpProcessorPausedSteps(final JBehaveTestHandlers testHandlers) {
        super(testHandlers);
    }
    
    protected final Class<?>[] getStoryObjectTypes() {
        return new Class[] { ProcessorTransaction.class };
    }
    
    @BeforeStory
    public final void setup() {
        TestLookupTables.getExpressionParser().register(getStoryObjectTypes());
    }    
    
    @BeforeScenario
    public final void setUpForController() {
        final ControllerFieldsWrapper controllerFields = new ControllerFieldsWrapper();
        TestDBMap.saveControllerFieldsWrapperToMem(controllerFields);
        TestContext.getInstance().getObjectParser().register(getStoryObjectTypes());
    }    
    
    @Given("a parking extension request is sent where the $info")
    public final void setEbpTransaction(final String info) {
        final String[] infoArr = info.split(System.getProperty(StandardConstants.SYSTEM_NEW_LINE));
        this.smsAlert = new SmsAlert();
        for (String s : infoArr) {
            try {
                if (s.startsWith("spaceNumber")) {
                    this.smsAlert.setSpaceNumber(Integer.parseInt(getData(s)));
                } else if (s.startsWith("mobileNumber")) {
                    this.smsAlert.setMobileNumber(new MobileNumber(getData(s), new Date()));
                } else if (s.startsWith("addTimeNum")) {
                    this.smsAlert.setAddTimeNumber(Integer.parseInt(getData(s)));
                } else if (s.startsWith("permitBeginGMT")) {
                    this.smsAlert.setPermitBeginGmt(DateUtil.convertFromColonDelimitedDateStringToCalendar(getData(s)).getTime());
                } else if (s.startsWith("permitExpireGMT")) {
                    this.smsAlert.setPermitExpireGmt(DateUtil.convertFromColonDelimitedDateStringToCalendar(getData(s)).getTime());
                }
            } catch (InvalidDataException ide) {
                LOG.error(ide);
                fail(ide.toString());
            }
        }
        LOG.info(this.smsAlert.getPermitExpireGmt());
    }
    
    @When("I add $minutes minutes")
    public final void addMinutes(final String minutes) {
        testHandlers.invokePerformMethod(OBJECT_NAME, ACTION_NAME);
    }
    
    @Then("my smartphone receives $response")
    public final void failure(final String response) {
        // Assertion is in TestEbpProcessorPaused.java
    }
    
    private String getData(final String s) {
        return s.substring(s.lastIndexOf(IS_BLANK) + IS_BLANK.length());
    }
}
