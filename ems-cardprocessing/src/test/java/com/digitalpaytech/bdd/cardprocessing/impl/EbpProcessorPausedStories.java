package com.digitalpaytech.bdd.cardprocessing.impl;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;

@RunWith(JUnitReportingRunner.class)
public class EbpProcessorPausedStories extends AbstractStories {
    
    public EbpProcessorPausedStories() {
        super();
        this.testHandlers.registerTestHandler("ebpprocessorpausedpaused", TestEbpProcessorPaused.class);
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }
    
    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new EbpProcessorPausedSteps(this.testHandlers));
    }
}
