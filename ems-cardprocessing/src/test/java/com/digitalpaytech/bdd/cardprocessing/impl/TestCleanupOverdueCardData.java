package com.digitalpaytech.bdd.cardprocessing.impl;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.services.PreAuthServiceMockImpl;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.cardprocessing.CardProcessingManager;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.dao.impl.OpenSessionDaoTest;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.service.CardRetryTransactionService;
import com.digitalpaytech.service.PreAuthService;
import com.digitalpaytech.service.ReversalArchiveService;
import com.digitalpaytech.service.impl.LinkCardTypeServiceImpl;
import com.digitalpaytech.service.impl.ProcessorTransactionServiceImpl;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.DateUtil;

public class TestCleanupOverdueCardData implements JBehaveTestHandler {
    @Mock
    private PreAuthService preAuthService;
    @Mock
    private CardRetryTransactionService cardRetryTransactionService;
    @Mock
    private ReversalArchiveService reversalArchiveService;
    @InjectMocks
    private EntityDaoTest entityDao;
    @Mock
    private CardProcessingManager cardProcessingMgr;
    @InjectMocks
    private OpenSessionDaoTest openSessionDao;
    @InjectMocks
    private ProcessorTransactionServiceImpl processorTransactionService;
    @InjectMocks
    private LinkCardTypeServiceImpl linkCardTypeService;
    
    private void cleanupOverdueCardData() {
        MockitoAnnotations.initMocks(this);
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        if (controllerFields == null) {
            controllerFields = new ControllerFieldsWrapper();
            TestDBMap.saveControllerFieldsWrapperToMem(controllerFields);
        }
        
        final Date expiryDate = DateUtil.createDateByMilliseconds(System.currentTimeMillis() - CardProcessingConstants.MS_TO_ATTEMPT_SETTLEMENT
                                                                  - CardProcessingConstants.FIFTEEN_MINUTES_MS);
        
        /*
         * In CardProcessingManager, cleanupOverdueCardData, this.preAuthService.getExpiredCardAuthorizationBackUp is called.
         */
        when(this.preAuthService.getExpiredCardAuthorizationBackUp(expiryDate))
                .thenAnswer(PreAuthServiceMockImpl.getExpiredCardAuthorizationBackUp());
        
        /*
         * In CardProcessingManager, cleanupOverdueCardDataForMigratedCustomers,
         * this.preAuthService.cleanupExpiredPreauthsForMigratedCustomers(expiredDate);
         */
        Mockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(final InvocationOnMock invocation) throws Throwable {
                return null;
            }
        }).when(this.preAuthService).cleanupExpiredPreauthsForMigratedCustomers(expiryDate);
        
        /*
         * In CardProcessingManager, cleanupOverdueCardDataForMigratedCustomers,
         * this.preAuthService.deleteExpiredPreauthsForMigratedCustomers(expiredDate);
         */
        Mockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(final InvocationOnMock invocation) throws Throwable {
                return null;
            }
        }).when(this.preAuthService).deleteExpiredPreauthsForMigratedCustomers(expiryDate);
        
        /*
         * In CardProcessingManager, cleanupOverdueCardDataForMigratedCustomers,
         * this.cardRetryTransactionService.clearRetriesExceededCardDataForMigratedCustomers();
         */
        Mockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(final InvocationOnMock invocation) throws Throwable {
                return null;
            }
        }).when(this.cardRetryTransactionService).clearRetriesExceededCardDataForMigratedCustomers();
        
        /*
         * In CardProcessingManager, cleanupOverdueCardDataForMigratedCustomers,
         * this.reversalArchiveService.cleanupCardDataForMigratedCustomers();
         */
        Mockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(final InvocationOnMock invocation) throws Throwable {
                return null;
            }
        }).when(this.reversalArchiveService).cleanupCardDataForMigratedCustomers();
        
        /*
         * In PreAuthServiceMockImpl, Answer<List<PreAuth>> findExpiredPreAuthsForMigratedCustomers()
         * Prepare a mock first and then call the method.
         */
        when(this.preAuthService.findExpiredPreAuthsForMigratedCustomers(expiryDate))
                .thenAnswer(PreAuthServiceMockImpl.findExpiredPreAuthsForMigratedCustomers());
        final List<Long> ids = getIds(this.preAuthService.findExpiredPreAuthsForMigratedCustomers(expiryDate));
        
        this.processorTransactionService.setEntityDao(this.entityDao);
        final List<ProcessorTransaction> beforeList = TestContext.getInstance().getDatabase().find(ProcessorTransaction.class);
        haveOrDontHavePreAuths(true, beforeList);
        
        this.processorTransactionService.removePreAuthsInProcessorTransactions(ids);
        this.entityDao.getCurrentSession().clear();
        final List<ProcessorTransaction> afterList = TestContext.getInstance().getDatabase().find(ProcessorTransaction.class);
        haveOrDontHavePreAuths(false, afterList);
    }
    
    private void haveOrDontHavePreAuths(final boolean haveOrDont, final List<ProcessorTransaction> ptList) {
        for (ProcessorTransaction pt : ptList) {
            // ProcessorTransaction should have PreAuth.
            if (haveOrDont && pt.getPreAuth() == null) {
                fail("PreAuth should not be null!");
            } else if (!haveOrDont && pt.getPreAuth() != null) {
                // ProcessorTransaction should not have PreAuth
                fail("PreAuth should be null!");
            }
        }
    }
    
    @Override
    public final Object performAction(final Object... objects) {
        cleanupOverdueCardData();
        return null;
    }
    
    @Override
    public final Object assertSuccess(final Object... objects) {
        return null;
    }
    
    @Override
    public final Object assertFailure(final Object... objects) {
        return null;
    }
    
    private List<Long> getIds(final List<PreAuth> preAuths) {
        final List<Long> list = new ArrayList<Long>(preAuths.size());
        for (PreAuth p : preAuths) {
            list.add(p.getId());
        }
        return list;
    }
}
