package com.digitalpaytech.bdd.cardprocessing.impl;

import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.services.EbpProcessorPausedMockImpl;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.cardprocessing.impl.CardProcessingManagerImpl;
import com.digitalpaytech.cardprocessing.impl.CardProcessorFactoryImpl;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CcFailLog;
import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.service.CcFailLogService;
import com.digitalpaytech.service.ProcessorTransactionService;
import com.digitalpaytech.service.cps.impl.CPSDataServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.util.CardProcessingConstants;

public class TestEbpProcessorPaused implements JBehaveTestHandler {
    
    private static final Logger LOG = Logger.getLogger(TestEbpProcessorPaused.class);
    
    @Mock
    private EntityDao entityDao;
    
    @Mock
    private ProcessorTransactionService processorTransactionService;
    
    @Mock
    private CcFailLogService ccFailLogService;
    
    @InjectMocks
    private CardProcessingManagerImpl cardProcessingMgr;
    
    @InjectMocks
    private MerchantAccountServiceImpl merchantAccountService;
    
    @InjectMocks
    private PointOfSaleServiceImpl pointOfSaleService;
    
    @InjectMocks
    private CPSDataServiceImpl cpsDataService;
    
    @InjectMocks
    private CardProcessorFactoryImpl cardProcessorFactory;
    
    /*
     * Set up the mock data and purposely throw an exception from 'this.cardProcessingMgr.processTransactionCharge'
     * to create a wanted processResult of 2.
     */
    private void failTransaction() {
        MockitoAnnotations.initMocks(this);
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        if (controllerFields == null) {
            controllerFields = new ControllerFieldsWrapper();
            TestDBMap.saveControllerFieldsWrapperToMem(controllerFields);
        }
        
        final ProcessorTransaction pt = new ProcessorTransaction();
        final PointOfSale pos = new PointOfSale();
        pos.setId(1);
        final Permit permit = new Permit();
        permit.setId((long) 1);
        final PaymentCard pc = new PaymentCard();
        
        when(this.merchantAccountService.findByPointOfSaleIdAndCardTypeId(1, 1)).thenAnswer(EbpProcessorPausedMockImpl.getMerchantPOSsEcho());
        when(this.processorTransactionService
                .getProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_CHARGE_WITH_NO_REFUNDS))
                        .thenAnswer(EbpProcessorPausedMockImpl
                                .getProcessorTransactionTypeEcho(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_CHARGE_WITH_NO_REFUNDS));
        when(this.pointOfSaleService.findMerchPOSByPOSIdNoDeleted(1)).thenAnswer(EbpProcessorPausedMockImpl.findMerchPOSByPOSIdNoDeletedEcho());
        
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(final InvocationOnMock invocation) throws Throwable {
                return null;
            }
        }).when(this.ccFailLogService).logFailedCcTransaction(new CcFailLog());
        
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(final InvocationOnMock invocation) throws Throwable {
                return null;
            }
        }).when(this.entityDao).save(pt);
        
        final int processResult = this.cardProcessingMgr.processTransactionCharge("4242424242424242=2201", pos, permit, false, pt, pc, 0L);
        
        LOG.debug("--> processResult should be 2: " + processResult);
        
        final int SmsPermitAlertJobService_AURA_CHARGE_UNKNOWN = 2;
        Assert.assertEquals(SmsPermitAlertJobService_AURA_CHARGE_UNKNOWN, processResult);
    }
    
    @Override
    public final Object performAction(final Object... objects) {
        failTransaction();
        return null;
    }
    
    @Override
    public final Object assertSuccess(final Object... objects) {
        return null;
    }
    
    @Override
    public final Object assertFailure(final Object... objects) {
        return null;
    }
}
