package com.digitalpaytech.bdd.cardprocessing.impl;

import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import static org.junit.Assert.*;

import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.bdd.util.AbstractSteps;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandlers;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.PreAuth;

public class CleanupOverdueCardDataSteps extends AbstractSteps {
    
    public CleanupOverdueCardDataSteps(final JBehaveTestHandlers testHandlers) {
        super(testHandlers);
    }
    
    protected final Class<?>[] getStoryObjectTypes() {
        return new Class[] { PreAuth.class, ProcessorTransaction.class };
    }
    
    @BeforeScenario
    public final void setUpForController() {
        final ControllerFieldsWrapper controllerFields = new ControllerFieldsWrapper();
        TestDBMap.saveControllerFieldsWrapperToMem(controllerFields);
        TestContext.getInstance().getObjectParser().register(getStoryObjectTypes());
    }
    
    @When("Cleanup Overdue Card Data is triggered")
    public void whenCleanupOverdueCardDataIsTriggered() {
        // TODO
    }
    
    @Then("processor transaction id 1 AND preauth is null")
    public final void thenProcessorTransactionId1ANDPreauthIsNull() {
        verifyProcessorTransaction("1", HibernateConstants.ID);
    }

    @Then("processor transaction id 2 AND preauth is null")
    public final void thenProcessorTransactionId2ANDPreauthIsNull() {
        verifyProcessorTransaction("2", HibernateConstants.ID);
    }

    @Then("processor transaction id 3 AND preauth is null")
    public final void thenProcessorTransactionId3ANDPreauthIsNull() {
        verifyProcessorTransaction("3", HibernateConstants.ID);
    }

    @Then("processor transaction id 4 AND preauth is null")
    public final void thenProcessorTransactionId4ANDPreauthIsNull() {
        verifyProcessorTransaction("4", HibernateConstants.ID);
    }

    @Then("processor transaction id 5 AND preauth is null")
    public final void thenProcessorTransactionId5ANDPreauthIsNull() {
        verifyProcessorTransaction("5", HibernateConstants.ID);
    }

    private void verifyProcessorTransaction(final String id, final String fieldName) {
        final ProcessorTransaction p = (ProcessorTransaction) TestDBMap.findObjectByFieldFromMemory(id, fieldName, ProcessorTransaction.class);
        assertNotNull(p);
        assertNull(p.getPreAuth());
    }
}
